﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MicroMicroProfile20.ascx.cs"
    Inherits="Matchnet.Web.Framework.Ui.BasicElements.MicroMicroProfile20" %>
<%@ Register TagPrefix="uc1" TagName="NoPhoto" Src="../PageElements/NoPhoto20.ascx" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>

<%@ Register Src="LastTime.ascx" TagName="LastTime" TagPrefix="uc2" %>
<%@ Register TagPrefix="uc2" TagName="HighlightProfileInfoDisplay" Src="HighlightInfoDisplay20.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ContactHistory" Src="/Framework/Ui/ProfileElements/ContactHistory.ascx" %>
<%@ Register TagPrefix="mn" TagName="BasicInfo" Src="/Applications/MemberProfile/ProfileTabs30/Controls/BasicInfo.ascx" %>
<%@ Register TagPrefix="uc1" TagName="LastUpdateDate" Src="/Framework/Ui/BasicElements/LastUpdateDate.ascx" %>
<%@ Register TagPrefix="uc1" TagName="NameValueDataGroup" Src="/Applications/MemberProfile/ProfileTabs30/Controls/ProfileDataGroup/NameValueDataGroup.ascx" %>
<%@ Register TagPrefix="uc1" TagName="TabGroup" Src="/Applications/MemberProfile/ProfileTabs30/Controls/ProfileTab/TabGroup.ascx" %>
<%@ Register TagPrefix="uc1" TagName="LastLoginDate" Src="/Framework/Ui/BasicElements/LastLoginDate.ascx" %>
<%@ Register TagPrefix="mn" TagName="ProfileDetails" Src="/Applications/MemberProfile/ProfileTabs30/Controls/ProfileDetails.ascx" %>

<asp:Panel ID="panelMicroMicroProfile20" runat="server" CssClass="micro-micro-profile clearfix">
    <!--MicroMicroProfile20-->
  
    <div class="member-pic">
        <mn:Image runat="server" ID="imgThumb" class="profileImageHover" Border="0" ResourceConstant="ALT_PROFILE_PICTURE" />
        <uc1:NoPhoto runat="server" ID="noPhoto" class="no-photo" Visible="False" />
    </div>
    <div class="member-info">

        <mn:Txt ID="txtToLabel" runat="server" ResourceConstant="TXT_LABEL_TO" Visible="false"/>
        <mn:Txt ID="txtFromLabel" runat="server" ResourceConstant="TXT_LABEL_FROM" Visible="false"/>
        <!--username-->
        <h2><mn:Txt runat="server" ID="lnkUserName1" /></h2>

        <!--other basic info-->
        <p>
            <asp:Literal ID="literalAge" runat="server"></asp:Literal><br />
            <asp:Literal ID="literalLocation" runat="server"></asp:Literal>
        </p>
        
        <ul class="inline-menu clearfix">  
        <asp:PlaceHolder ID="phOverView" runat="server" Visible="false">
            <li><button id="profileOverviewBtn" type="button" class="textlink"><mn:txt runat="server" id="txtButtonOverview" resourceconstant="TXT_BUTTON_OVERVIEW" expandimagetokens="true" /></button>
                <div id="profileOverview" class="hide">
                    <%--Overview--%>
                    <%--Basic Info--%>
                    <mn:BasicInfo ID="basicInfo1" runat="server" />
        
                    <%--Timestamps --%>
                    <ul class="timestamp">
                        <li><uc1:LastLoginDate ID="lastLoginDate" runat="server"></uc1:LastLoginDate></li>
                        <li><uc1:LastUpdateDate ID="lastUpdateDate" runat="server"></uc1:LastUpdateDate></li>
                    </ul>

                    <!--DEALBREAKER INFO-->
                    <div class="profile30-details clearfix">
                        <uc1:NameValueDataGroup ID="nvDataGroupDealBreakerInfo" runat="server" DisplayMode="TwoColumn" />
                    </div>
                 </div>
            </li>
        </asp:PlaceHolder>  
        <asp:PlaceHolder ID="phDetail" runat="server" Visible="false">
            <li><button id="profileDetailsBtn" type="button" class="textlink"><mn:txt runat="server" id="txtButtonDetails" resourceconstant="TXT_BUTTON_DETAILS" expandimagetokens="true" /></button>
                <div id="profileDetails" class="hide">
                    <%--Detail--%>
                    <mn:ProfileDetails ID="profileDetails1" runat="server"></mn:ProfileDetails>
                </div>
            </li>
        </asp:PlaceHolder>
        <asp:PlaceHolder ID="phInMyOwnWords" runat="server" Visible="false">
            <li><button id="profileInMyOwnWordsBtn" type="button" class="textlink"><mn:txt runat="server" id="txtButtonEssays" resourceconstant="TXT_BUTTON_ESSAYS" expandimagetokens="true" /></button>
                <div id="profileInMyOwnWords" class="hide">
                    <%--In My Own Words--%>
                    <uc1:TabGroup ID="ProfileTabGroup1" runat="server" />
                </div>
            </li>
        </asp:PlaceHolder>
        </ul>
        
        <script type="text/javascript">
        if($j('.omni-invite-div').length > 0){
            $j('#profileOverview').find('script, link, .omni-invite-div').remove();
        }

        //<![CDATA[
        (function() {
            __addToNamespace__('spark.qanda', {            
                BrandId:<%=g.Brand.BrandID %>,
                MemberId:<%=MemberID %>,
                txtLike: '<%=GetResourceForJS("TXT_ANSWER_LIKE") %>',
                txtUnLike: '<%=GetResourceForJS("TXT_ANSWER_UNLIKE") %>',
                txtLikeNum: '<%=GetResourceForJS("TXT_LIKE_NUM") %>',
                __init__: function() { 
                    if (!spark || !spark.qanda || !spark.qanda.LikeAnswer) {
                        $j("head").append("<script type=\"text/javascript\" src=\"/Applications/QuestionAnswer/Controls/qanda.js\"><"+ "/script>");
                    }
                }        
            });
        })();
        //]]>
        

        function spark_microProfileDialogs(el, mtitle, hideUs){
            var $el = $j(el);
            var customClass = "prof-micromicro-dialogs";

            $j("html,body").scrollTop(0);
            $el.dialog({
                width: 488,
		        modal: true,
		        title: mtitle,
                position:['center','top'],
                dialogClass: customClass,
                open: function(event, ui){
                    //remove edit profile stuff if sending yourself an email
                    $j(this).find('.prof-edit-active').removeClass('prof-edit-active');
                    $j(this).find('.prof-edit-button').remove();

                    //hide unwanted objects being carried over from full profile
                    if(hideUs){
                        $el.find(hideUs).hide();
                    }
                }
	        });

            $j('.ui-dialog.' + customClass).css('margin-top', '12px');
        }

        $j('#profileOverviewBtn').bind('click',function(){
            spark_microProfileDialogs('#profileOverview', '<%=g.GetResource("TXT_BUTTON_OVERVIEW",this) %>');
        });
            
        $j('#profileDetailsBtn').bind('click',function(){
            $j('#profileDetails').find('.timestamp').remove();
            var grabTitle = $j('#profileDetails').find('.group-value h2:first').text();
            spark_microProfileDialogs('#profileDetails', grabTitle, '.group-value h2:first');
        });
            
        $j('#profileInMyOwnWordsBtn').bind('click',function(){
            spark_microProfileDialogs('#profileInMyOwnWords', '<%=g.GetResource("TXT_BUTTON_ESSAYS",this) %>', '#profileTabGroup > ul:first');
        });

        $j(function() {
            // Essay ellipsis
            $j('#profileTabGroup').find("#tab-Essays p").expander({
                slicePoint:1000,
                expandEffect: 'show',
                expandPrefix:'&nbsp;',
                expandText:'[...]',
                userCollapsePrefix:'&nbsp;',
                userCollapseText:'[^]'
            });

            var $tabEssays = $j("#tab-Essays");
            $tabEssays.find(".comment-block textarea").watermark("<%=g.GetResource("TXT_WATERMARK_COMMENT",this) %>");
            $tabEssays.bind('click',function(event){
                var $target = $j(event.target);
                $target.closest('.comment-label').toggleClass('hide').parents('.like-container').find('.comment-block').toggleClass('hide');
                $target.closest('.close').parents('.comment-block').toggleClass('hide').parents('.like-container').find('.comment-label').toggleClass('hide');
            });
        });
        </script>
        
        <% if (IsCommentsEnabled && IsLoggedIn){ %>
<script type="text/javascript" src="/Applications/QuestionAnswer/Controls/qanda.js"></script>
<script type="text/javascript">
    //<![CDATA[
    (function() {
        __addToNamespace__('spark.qanda', {
            RedirectUrl:'<%=GetRedirectUrl(888) %>',
            BrandId:<%=g.Brand.BrandID %>,
            txtViewMyProfile: '<%=GetResourceForJS("TXT_VIEW_MY_PROFILE") %>',
            txtCommentCloseBox: '<%=GetResourceForJS("TXT_COMMENT_CLOSE_BOX") %>',
            txtCommentLabel: '<%=GetResourceForJS("TXT_COMMENT_LABEL") %>',
            txtCommentSubmit:'<%=GetResourceForJS("TXT_COMMENT_SEND") %>',
            txtCommentWatermarkCopy:'<%=GetResourceForJS("TXT_WATERMARK_COMMENT") %>',
            txtCommentConfirmationCopy:'<%=GetResourceForJS("TXT_COMMENT_CONFIRM") %>',
            txtEmptyCommentErrorCopy:'<%=GetResourceForJS("TXT_EMPTY_COMMENT_ERROR") %>',
            txtCommentErrorCopy:'<%=GetResourceForJS("TXT_COMMENT_ERROR") %>',
            txtCommentMailSubject:'<%=GetResourceForJS("TXT_COMMENT_SUBJECT") %>',
            txtFreeTextCommentMailSubject:'<%=GetResourceForJS("TXT_FREE_COMMENT_SUBJECT") %>',
            __init__: function() {
                if (!$j.render) {
                    $j("head").append("<script type=\"text/javascript\" src=\"/javascript20/library/jquery.tmpl.js\"><"+ "/script>");
                }
            }        
       });
    })();

    $j(document).ready(function(){
        
            if (spark.qanda.RedirectUrl && spark.qanda.RedirectUrl.length > 0) {
                $j("#qanda-view-profile .link-label").live('click',function(event){
                    document.location.href = spark.qanda.RedirectUrl;
                });
                $j("#double-q-cont .link-label").live('click',function(event){
                    document.location.href = spark.qanda.RedirectUrl;
                });

            } else {
                $j("#qanda-view-profile").live('click',function(event){
                    $j(event.target).closest('label').parents('.answer-footer').toggleClass('hide')
                        .parents('.answer-profile').find('.member-comment').toggleClass('hide');
                    $j(event.target).closest('.close').parents('.member-comment').toggleClass('hide')
                        .parents('.answer-profile').find('.answer-footer').toggleClass('hide');
                });                    
                $j("#double-q-cont").live('click',function(event){
                    $j(event.target).closest('label').parents('.answer-info').toggleClass('hide')
                        .parents('.answer-container').find('.member-comment').toggleClass('hide');
                    $j(event.target).closest('.close').parents('.member-comment').toggleClass('hide')
                        .parents('.answer-container').find('.answer-info').toggleClass('hide');
                });                    
            }
                       
        $j("textarea", ".member-comment").watermark('<%=GetResourceForJS("TXT_WATERMARK_COMMENT") %>');

        $j("textarea", ".answer-yours").watermark('<%=GetResourceForJS("TXT_WATERMARK_ANSWER") %>');
            
        $j("input.commentSubmit").live("click",function(evt) {
            var ids=$j(this).attr('id').split("|");            
            var myFunc=function() {
                return spark.qanda.SendMail('comment-'+ids[1]+'-'+ids[2],ids[3],<%=MemberID%>,'<%=MemberUserName%>',{Event:"event51",Overwrite:true,Props:[{Num:38,Value:"Comment-Another Member Profile"}]});
            }
            spark.qanda.ajaxCallWithBlock($j(this).parent(),{ 
                  message: '<%=GetResourceForJS("HTML_BLOCK_ELEMENT_MSG") %>',
                  centerY: false,
                  css: {backgroundColor: 'transparent', border: 'none', top:'10%'},
                  overlayCSS: {backgroundColor: '#fff', opacity: 0.8 }
            },myFunc,'/Applications/API/IMailService.asmx/SendComment');            
            evt.stopPropagation();            
            return false;
        });    
        $j("input.freeTextCommentSubmit").live("click",function(evt) {
            var ids=$j(this).attr('id').split("-");            
            var myFunc=function() {
                return spark.qanda.FreeTextSendMail('comment-'+ids[1]+'-'+ids[2]+'-'+ids[3],ids[3],<%=MemberID%>,'<%=MemberUserName%>',{Event:"event51",Overwrite:true,Props:[{Num:38,Value:"Comment-Another Member Profile"}]});
            }
            spark.qanda.ajaxCallWithBlock($j(this).closest('.comment-block'),{ 
                  message: '<%=GetResourceForJS("HTML_BLOCK_ELEMENT_MSG") %>',
                  centerY: false,
                  css: {backgroundColor: 'transparent', border: 'none', top:'10%'},
                  overlayCSS: {backgroundColor: '#fff', opacity: 0.8 }
            },myFunc,'/Applications/API/IMailService.asmx/FreeTextSendComment');            
            evt.stopPropagation();            
            return false;
        });     
        $j("label.link-label").live('click',function(evt){
            spark.tracking.addEvent("event50", true);
            spark.tracking.addProp(38, "Comment-Another Member Profile", true);
            spark.tracking.track();                            
        });
    });
    //]]>
</script>
<% } %>

        <asp:PlaceHolder ID="phHistoryIcon" runat="server" Visible="false">
            <div id="profile30ToolBar" class="profile30-toolbar clearfix">
                <ul>
                    <li class="history item tabbed ">
		                <div class="tabbed-wrapper">
		                    <uc1:ContactHistory ID="contactHistory" runat="server" 
		                        HidePopupHeaderText = "true"
		                        IsContentLazyLoaded="false" 
		                        IconCSS="spr s-icon-hover-contact-active s-icon-hover-contact-performed" 
		                        IconNoHistoryCSS="spr s-icon-A-shover-contact">
		                    </uc1:ContactHistory>
		                </div>
                    </li>
                 </ul>
             </div>
             <script type="text/javascript">
                TabbedMenuToggler($j('#profile30ToolBar'), 'no');
            </script>
        </asp:PlaceHolder>
    </div>
</asp:Panel>
