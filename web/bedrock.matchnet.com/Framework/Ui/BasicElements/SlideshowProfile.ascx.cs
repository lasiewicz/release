﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Member.ServiceAdapters.Interfaces;
using Matchnet.Member.ValueObjects.Interfaces;
using Matchnet.Web.Applications.MemberProfile;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects.Photos;
using Matchnet.Member.ValueObjects;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.MemberSlideshow.ServiceAdapters;
using Matchnet.MemberSlideshow.ValueObjects;
using Matchnet.List.ServiceAdapters;
using Matchnet.List.ValueObjects;
using Matchnet.Web.Framework.Managers;
using Matchnet.Web.Framework.Util;

namespace Matchnet.Web.Framework.Ui.BasicElements
{
    public enum SlideShowModeType : int
    {
        None=0,
        HomePage=1,
        SecretAdmirerGame=2,
        SecretAdmirerGameConfigurable=3
    }
	public partial class SlideshowProfile : FrameworkControl
	{
        private int _slideshowMemberID;
        private IMemberDTO _slideshowMember;
        private List<Photo> _approvedPhotos;
        private const int SUSPENDED_FLAG = 1;
        private bool _displayedInRightControl = false;
        private SlideShowModeType _slideShowMode=SlideShowModeType.None;
        private string _resourceConstantByMode="";
        private BreadCrumbHelper.EntryPoint _entryPoint = BreadCrumbHelper.EntryPoint.MemberSlideshow;
        private bool _useConfiguration; 
        #region Properties
        public bool DisplayedInRightControl
        {
            get { return _displayedInRightControl; }
            set { _displayedInRightControl = value; }
        }
        public string ViewProfileLinkFormat { get; set; }
        public SlideShowModeType SlideShowMode { get { return _slideShowMode; } set { _slideShowMode = value; } }
        public BreadCrumbHelper.EntryPoint EntryPoint { get { return _entryPoint; } set { _entryPoint = value; } }
        public SlideshowDisplayType SlideshowDisplayType { get; set; }
        public string SlideshowOuterContainerDivID { get; set; }
        public bool ReloadAds { get; set; }
        public string OmniturePageName {get;set;}
        public bool PopulateYNMBuckets { get; set; }
        public bool DisplayPreferences { get; set; }
        
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            SlideshowOuterContainerDivID = "slideshow";
            ReloadAds = true;
            PopulateYNMBuckets = false;
            OmniturePageName = "Slideshow";
            SlideshowDisplayType = SlideshowDisplayType.Full;
        }

        public void Load(bool updatePosition)
        {
            try
            {
                MemberSlideshowManager manager = new MemberSlideshowManager(g.Member.MemberID, g.Brand);

                _useConfiguration = manager.SlideshowUsesConfiguration();
                    
                if (_slideShowMode > SlideShowModeType.HomePage)
                {
                    _resourceConstantByMode += "_" + _slideShowMode.ToString().ToUpper();
                    _entryPoint = BreadCrumbHelper.EntryPoint.SlideShowPage;
                }

                string txtHeaderResource = "TXT_HEADER" + _resourceConstantByMode;
                string txtDoWeClickResource = "TXT_DO_WE_CLICK" + _resourceConstantByMode;
                txtHeader.ResourceConstant = txtHeaderResource;
                txtDoWeClick.ResourceConstant = txtDoWeClickResource;
                bool loadSuccess = LoadSlideshowMember(updatePosition);
                if (loadSuccess)
                {
                    InitializePage();
                }
                else
                {
                    DisplaySlideshowEnd();
                }
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

	    public void LoadPreferences()
	    {
            if (_useConfiguration)
            {
                prefWrapper.Visible = true;
                theFormDiv.Visible = true;
            }
            
            slideshowPreferences.Visible = true;
            slideshowPreferences.LoadPreferences(SlideshowDisplayType.Full, _slideShowMode, SlideshowOuterContainerDivID);
	    }

        protected override void OnPreRender(EventArgs e)
        {
            if (_displayedInRightControl)
            {
                phDoWeClickTitle.Visible = false;
                lnkAboutMe.Visible = false;
                
            }
            base.OnPreRender(e);
        }

        private bool LoadSlideshowMember(bool updatePosition)
        {
            try
            {
                MemberSlideshowManager manager = new MemberSlideshowManager(g.Member.MemberID, g.Brand);
                IMember validMemberFromSlideshow = manager.GetValidMemberFromSlideshow(updatePosition);
                _slideshowMember = (null != validMemberFromSlideshow) ? validMemberFromSlideshow.GetIMemberDTO() : null;
                
                return _slideshowMember != null;
            }
            catch (Exception ex)
            { g.ProcessException(ex); return false; }
        }

        private string GetYNMParams()
        {
            YNMVoteParameters parameters = new YNMVoteParameters();

            parameters.SiteID = _g.Brand.Site.SiteID;
            parameters.CommunityID = _g.Brand.Site.Community.CommunityID;
            parameters.FromMemberID = _g.Member.MemberID;
            parameters.ToMemberID = _slideshowMember.MemberID;
            
            return parameters.EncodeParams();

        }

        private void InitializePage()
        {
            slideshowContainer.Attributes.Add("data-displaytype", SlideshowDisplayType.ToString("d"));
            slideshowContainer.Attributes.Add("data-urlparamslideshowdisplaytype", WebConstants.URL_PARAMETER_SLIDESHOW_DISPLAY_TYPE);
            slideshowContainer.Attributes.Add("data-urlparamslideshowoutercontainerdiv", WebConstants.URL_PARAMETER_SLIDESHOW_OUTER_CONTAINER_DIV);
            slideshowContainer.Attributes.Add("data-urlparamslideshowomniturepagename", WebConstants.URL_PARAMETER_SLIDESHOW_OMNITURE_PAGE_NAME);
            slideshowContainer.Attributes.Add("data-urlparamslideshowmode", WebConstants.URL_PARAMETER_SLIDESHOW_MODE);
            slideshowContainer.Attributes.Add("data-urlparamslideshowshowupdatedwidestyle", WebConstants.URL_PARAMETER_SLIDESHOW_SHOW_UPDATED_WIDE_STYLE);
            slideshowContainer.Attributes.Add("data-ynmparams", GetYNMParams());
            slideshowContainer.Attributes.Add("data-slideshowmemberid", _slideshowMember.MemberID.ToString());
            slideshowContainer.Attributes.Add("data-reloadcontent", "true");
            slideshowContainer.Attributes.Add("data-displayedinrightcontrol", "false");
            slideshowContainer.Attributes.Add("data-outercontainerdivid", SlideshowOuterContainerDivID);
            slideshowContainer.Attributes.Add("data-reloadads", ReloadAds.ToString().ToLower());
            slideshowContainer.Attributes.Add("data-populateynmbuckets", PopulateYNMBuckets.ToString().ToLower());
            slideshowContainer.Attributes.Add("data-omniturepagename", OmniturePageName);
            slideshowContainer.Attributes.Add("data-slideshowmode", SlideShowMode.ToString("d"));
            slideshowContainer.Attributes.Add("data-showupdatedwidestyle", "false");

            spanYes.Attributes.Add("data-clicktype", ((int) ClickListType.Yes).ToString());
            spanYes.Attributes.Add("data-containerdivid", slideshowContainer.ClientID);
            spanNo.Attributes.Add("data-clicktype", ((int)ClickListType.No).ToString());
            spanNo.Attributes.Add("data-containerdivid", slideshowContainer.ClientID);
            spanMaybe.Attributes.Add("data-clicktype", ((int)ClickListType.Maybe).ToString());
            spanMaybe.Attributes.Add("data-containerdivid", slideshowContainer.ClientID);

            spanYes.Attributes.Add("title", g.GetResource("TTL_YNM_BUTTON_YES", this));
            spanNo.Attributes.Add("title", g.GetResource("TTL_YNM_BUTTON_NO", this));
            spanMaybe.Attributes.Add("title", g.GetResource("TTL_YNM_BUTTON_MAYBE", this));

            /*WebConstants constants = new WebConstants();
            if (_slideShowMode != SlideShowModeType.SecretAdmirerGame)
            {
                spanYes.Attributes.Add("onclick", string.Format(constants.JSCRIPT_SLIDESHOW_VOTE_NO_SUBMIT, GetYNMParams(), (int)ClickListType.Yes, _slideshowMember.MemberID, _displayedInRightControl.ToString(), slideshowContainer.ClientID));
                spanYes.Attributes.Add("title", g.GetResource("TTL_YNM_BUTTON_YES", this));
                spanNo.Attributes.Add("onclick", string.Format(constants.JSCRIPT_SLIDESHOW_VOTE_NO_SUBMIT, GetYNMParams(), (int)ClickListType.No, _slideshowMember.MemberID, _displayedInRightControl.ToString(), slideshowContainer.ClientID));
                spanNo.Attributes.Add("title", g.GetResource("TTL_YNM_BUTTON_NO", this));
                spanMaybe.Attributes.Add("onclick", string.Format(constants.JSCRIPT_SLIDESHOW_VOTE_NO_SUBMIT, GetYNMParams(), (int)ClickListType.Maybe, _slideshowMember.MemberID, _displayedInRightControl.ToString(), slideshowContainer.ClientID));
                spanMaybe.Attributes.Add("title", g.GetResource("TTL_YNM_BUTTON_MAYBE", this));

            }
            else
            {
                spanYes.Attributes.Add("onclick", string.Format(constants.JSCRIPT_SLIDESHOW_VOTE_NO_SUBMIT_EX, GetYNMParams(), (int)ClickListType.Yes, _slideshowMember.MemberID, _displayedInRightControl.ToString(), slideshowContainer.ClientID));
                spanYes.Attributes.Add("title", g.GetResource("TTL_YNM_BUTTON_YES", this));
                spanNo.Attributes.Add("onclick", string.Format(constants.JSCRIPT_SLIDESHOW_VOTE_NO_SUBMIT_EX, GetYNMParams(), (int)ClickListType.No, _slideshowMember.MemberID, _displayedInRightControl.ToString(), slideshowContainer.ClientID));
                spanNo.Attributes.Add("title", g.GetResource("TTL_YNM_BUTTON_NO", this));
                spanMaybe.Attributes.Add("onclick", string.Format(constants.JSCRIPT_SLIDESHOW_VOTE_NO_SUBMIT_EX, GetYNMParams(), (int)ClickListType.Maybe, _slideshowMember.MemberID, _displayedInRightControl.ToString(), slideshowContainer.ClientID));
                spanMaybe.Attributes.Add("title", g.GetResource("TTL_YNM_BUTTON_MAYBE", this));

            }
            */

            string ViewProfileUrl = BreadCrumbHelper.MakeViewProfileLink(_entryPoint, _slideshowMember.MemberID);

            ViewProfileLinkFormat = ViewProfileUrl.Replace(_slideshowMember.MemberID.ToString(), "{0}");
            
            lnkUserName.NavigateUrl = ViewProfileUrl;
            lnkUserName.Attributes.Add("title", _slideshowMember.GetUserName(_g.Brand));
            lnkUserName.Text = FrameworkGlobals.Ellipsis(_slideshowMember.GetUserName(_g.Brand), 17);
            lnkPhoto.NavigateUrl = ViewProfileUrl;


            litAge.Text = FrameworkGlobals.GetAge(_slideshowMember, g.Brand).ToString();
            if (litAge.Text.Trim() == "")
            {
                litAge.Text = "&nbsp;";
            }
            else
            {
                if (FrameworkGlobals.isHebrewSite(g.Brand))
                {
                    litAge.Text = g.GetResource("TXT_YEARS_OLD", this) + " " + litAge.Text;
                }
                else
                {
                    litAge.Text += " " + g.GetResource("TXT_YEARS_OLD", this);
                }
            }
            litRelationshipMask.Text = ProfileDisplayHelper.GetMaskContent(_slideshowMember,g.Brand,"RelationshipMask");
            litGender.Text = ProfileDisplayHelper.GetMaritalStatusSeekingGenderDisplay(_slideshowMember, g);
            litLocation.Text = ProfileDisplayHelper.GetRegionDisplay(_slideshowMember, g);
            if (this.litLocation.Text.Trim() == "") this.litLocation.Text = "&nbsp;";

            //_approvedPhotos = FrameworkGlobals.GetApprovedPhotos(_slideshowMember, g);
            lnkAboutMe.NavigateUrl = ViewProfileUrl;
            lnkAboutMe.Text =
                FrameworkGlobals.GetMorePhotoLink(
                    MemberPhotoDisplayManager.Instance.GetApprovedPhotosCount(g.Member, _slideshowMember,g.Brand),
                    g, this);
            DisplayMemberPhoto();
        }

        private void DisplaySlideshowEnd()
        {
            if (_useConfiguration && SlideshowDisplayType != SlideshowDisplayType.ModalPopup)
            {
                pnlConfigurableSlideshowEnd.Visible = true;
                prefWrapper.Visible = true;
                theFormDiv.Visible = true;
            }
            else
            {
                pnlSlideshowEnd.Visible = true;
            }
            pnlSlideshowMainContent.Visible = false;
        }

        private void DisplayMemberPhoto()
        {
            var photo = MemberPhotoDisplayManager.Instance.GetRandomApprovedForMainPhoto(g.Member, _slideshowMember,
                                                                                         g.Brand);

            if (photo != null)
            {
                var imageUrl = MemberPhotoDisplayManager.Instance.GetPhotoDisplayURL(g.Member, _slideshowMember, g.Brand,
                                                                                     photo, PhotoType.Full,
                                                                                     PrivatePhotoImageType.Full);
                imgProfile.ImageUrl = imageUrl;
                //add javascript to resize photo
                imgProfile.Attributes.Add("onload", "profile_resizePhoto(this);");
            }

            imgProfile.Visible = (photo != null);
        }
        public string GetAgeFormat()
        {
            string age = "{0}";

            if (FrameworkGlobals.isHebrewSite(g.Brand))
            {
                age = g.GetResource("TXT_YEARS_OLD", this) + " " + age;
            }
            else
            {
                age += " " + g.GetResource("TXT_YEARS_OLD", this);
            }

            return age;
        }
       
	}
}
