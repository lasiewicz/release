﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Web;

using Matchnet.Web.Applications.Home;
using Matchnet.Member.ServiceAdapters;
using Matchnet.List.ValueObjects;
using Matchnet.List.ServiceAdapters;

namespace Matchnet.Web.Framework.Ui.BasicElements
{
    public enum YNMBucketType : int
    {
        DefaultBucket=0,
        YesBucket,
        NoBucket,
        MaybeBucket
    }
    public partial class MicroProfileList : FrameworkControl
    {

        List<ProfileHolder> _memberList;
        string _noResultContentResource = "";
        FrameworkControl _resourceControl = null;
        DisplayContextType _displayContext = DisplayContextType.Default;

        #region public properties
        public List<ProfileHolder> MemberList
        {
            set { _memberList = value; }
            get { return _memberList; }
        }

        public string NoResultContentResource
        {
            set { _noResultContentResource = value; }
            get { return _noResultContentResource; }
        }

        public FrameworkControl ResourceControl
        {
            set { _resourceControl = value; }
            get { return _resourceControl; }
        }

        public DisplayContextType DisplayContext
        {
            set { _displayContext = value; }
            get { return _displayContext; }
        }

      
        public YNMBucketType YNMBucket { get; set; }
        #endregion

        #region page events
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                
               
                
            }
            catch (Exception ex)
            { g.ProcessException(ex); }
        }

        protected override void OnPreRender(EventArgs e)
        {
            try
            {
                if (_memberList == null || _memberList.Count == 0)
                {
                    DisplayNoResultsContent();
                    if (DisplayContext != DisplayContextType.YNMList)
                        lnkViewMore.Visible = false;
                    return;
                }

                DisplayMemberList();

            }
            catch (Exception ex)
            { g.ProcessException(ex); }
        }

        protected void repeaterYourMatches_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Header)
            {
                if (YNMBucket > YNMBucketType.DefaultBucket)
                {
                    Literal litBucket = e.Item.FindControl("litYNMBucket") as Literal;
                    if (litBucket != null)
                    {
                        litBucket.Text = "<div id=\"divMicroProfileInject" + YNMBucket.ToString() + "\"></div>";
                    }
                }

            }
            else if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                ProfileHolder profile = e.Item.DataItem as ProfileHolder;

                MicroProfile20 microProfile = e.Item.FindControl("microProfileMatches") as MicroProfile20;
                microProfile.member = profile.Member;
                microProfile.HotListCategoryID = (int)profile.HotlistCategory;
                microProfile.Ordinal = profile.Ordinal;
                microProfile.MyEntryPoint = profile.MyEntryPoint;
                if (DisplayContext == DisplayContextType.YNMList)
                    microProfile.DisplayContext = MicroProfile20.DisplayContextType.YNMList;
                microProfile.LoadMemberProfile();

                if (microProfile.IsHighlighted)
                {
                    PlaceHolder phHighlight = e.Item.FindControl("phHighlight") as PlaceHolder;
                    phHighlight.Visible = true;
                }
                else
                {
                    PlaceHolder phSpotlight = e.Item.FindControl("phRegular") as PlaceHolder;
                    phSpotlight.Visible = true;
                }
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            try
            {
                InitializeComponent();
                base.OnInit(e);
            }
            catch (Exception ex)
            { g.ProcessException(ex); }
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
            this.repeaterYourMatches.ItemDataBound += new RepeaterItemEventHandler(repeaterYourMatches_ItemDataBound);
            //this.PreRender += new System.EventHandler(OnPreRender);
        }
        #endregion
        #endregion

        #region private methods

        private void DisplayNoResultsContent()
        {
            if (!String.IsNullOrEmpty(_noResultContentResource))
            {
                phNoResultDisplay.Visible = true;
                if (_resourceControl == null)
                    noResultDisplay.Text = g.GetResource(_noResultContentResource, this);
                else
                    noResultDisplay.Text = g.GetResource(_noResultContentResource, _resourceControl);
            }

            if (YNMBucket > YNMBucketType.DefaultBucket)
            {
                if (litYNMBucket != null)
                {
                    litYNMBucket.Text = "<div id=\"divMicroProfileInject" + YNMBucket.ToString() + "\"></div>";
                    phNoResultDisplay.Visible = true;
                }
            }
            if (_displayContext == DisplayContextType.YNMList && YNMBucket != YNMBucketType.DefaultBucket)
            {
                switch (YNMBucket)
                {
                    case YNMBucketType.YesBucket:
                        lnkViewMore.NavigateUrl = "/Applications/HotList/View.aspx?CategoryID=-20";
                        break;
                    case YNMBucketType.NoBucket:
                        lnkViewMore.NavigateUrl = "/Applications/HotList/View.aspx?CategoryID=-22";
                        break;
                    case YNMBucketType.MaybeBucket:
                        lnkViewMore.NavigateUrl = "/Applications/HotList/View.aspx?CategoryID=-21";
                        break;
                }
            }

        }

        private void DisplayMemberList()
        {

            repeaterYourMatches.DataSource = _memberList;
            repeaterYourMatches.DataBind();
            if (_displayContext == DisplayContextType.YNMList && YNMBucket != YNMBucketType.DefaultBucket)
            {
                switch (YNMBucket)
                {
                    case YNMBucketType.YesBucket:
                        lnkViewMore.NavigateUrl = "/Applications/HotList/View.aspx?CategoryID=-20";
                        break;
                    case YNMBucketType.NoBucket:
                        lnkViewMore.NavigateUrl = "/Applications/HotList/View.aspx?CategoryID=-22";
                        break;
                    case YNMBucketType.MaybeBucket:
                        lnkViewMore.NavigateUrl = "/Applications/HotList/View.aspx?CategoryID=-21";
                        break;
                }
            }
        }


        private void LoadProfile(MicroProfile20 microProfile, ProfileHolder profileHolder)
        {


            microProfile.member = profileHolder.Member;

            microProfile.MyEntryPoint = profileHolder.MyEntryPoint;
            microProfile.Ordinal = profileHolder.Ordinal;

            if (_displayContext == DisplayContextType.HotList)
            {
                microProfile.HotListCategoryID = (int)profileHolder.HotlistCategory;
                ListItemDetail viewedYouMemberListDetail = g.List.GetListItemDetail(profileHolder.HotlistCategory,
                                  g.Brand.Site.Community.CommunityID,
                                  g.Brand.Site.SiteID,
                                  profileHolder.Member.MemberID);

                if (viewedYouMemberListDetail != null)
                    microProfile.LastHotlistActionDate = viewedYouMemberListDetail.ActionDate;


            }


            microProfile.LoadMemberProfile();

        }
        #endregion
    }
}