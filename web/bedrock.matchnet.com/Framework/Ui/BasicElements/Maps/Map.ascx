<%@ Control Language="c#" AutoEventWireup="false" Codebehind="Map.ascx.cs" Inherits="Matchnet.Web.Framework.Ui.BasicElements.Maps.Map" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>

<div id="mapdiv" style="width: <%=Width%>px; height: <%=Height%>px"></div>

<%--

This should eventually be made dynamic to use a different key depending on the current Brand and App.  For now,
here are a couple of keys that I have registered:

http://www.jdate.com/Applications/Search/
ABQIAAAAdnGMllQ9dUu26AlPujcODBRElSJF25b1Den0HMZiIvmUSiLB6BTWRYuTef5mjw8pA9r9jOAEhkqJ0Q

http://www.jdate.com/Applications/MembersOnline/
ABQIAAAAdnGMllQ9dUu26AlPujcODBR5ptyM8ww7fiuUlcRmPFWObw4LIRTPcNWYfGXuHSs7uVBLXodgiBJxCQ

See http://www.google.com/apis/maps/signup.html to sign up for additional keys or get more info about how the keys work.

--%>

<script src="http://maps.google.com/maps?file=api&v=1&key=ABQIAAAAdnGMllQ9dUu26AlPujcODBR5ptyM8ww7fiuUlcRmPFWObw4LIRTPcNWYfGXuHSs7uVBLXodgiBJxCQ" type="text/javascript"></script>

<script type="text/javascript">
	var map;

	function MakeMap()
	{
		var lng = <asp:Literal ID="litLongitude" Runat="server" />;
		var lat = <asp:Literal ID="litLatitude" Runat="server" />;

		map = new GMap(document.getElementById("mapdiv"));
		//<![CDATA[
		map.addControl(new GSmallMapControl());
		map.centerAndZoom(new GPoint(lng, lat), 8);
		//]]>
	}
	
    $j(document).ready(function(){MakeMap()});
</script>