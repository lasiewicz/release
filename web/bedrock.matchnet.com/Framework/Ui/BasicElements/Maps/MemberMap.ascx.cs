using System;
using System.Collections;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.Region;
using Matchnet.Member.ServiceAdapters;

namespace Matchnet.Web.Framework.Ui.BasicElements.Maps
{
	/// <summary>
	///		Summary description for MemberMap.
	/// </summary>
	public class MemberMap : FrameworkControl
	{
		private ArrayList _members;

		protected Map MainMap;
		protected Repeater MapRepeater;

		private void MemberMap_Load(object sender, EventArgs e)
		{
			MapRepeater.DataSource = _members;
			MapRepeater.DataBind();
		}

		public ArrayList Members
		{
			get
			{
				return _members;
			}
			set
			{
				_members = value;
			}
		}

		public string Width
		{
			get
			{
				return MainMap.Width;
			}
			set
			{
				MainMap.Width = value;
			}
		}

		public string Height
		{
			get
			{
				return MainMap.Height;
			}
			set
			{
				MainMap.Height = value;
			}
		}

		public Region Region
		{
			get
			{
				return MainMap.Region;
			}
			set
			{
				MainMap.Region = value;
			}
		}


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new EventHandler(MemberMap_Load);
			MapRepeater.ItemDataBound +=new RepeaterItemEventHandler(MapRepeater_ItemDataBound);
		}
		#endregion

		private void MapRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
		{
			Matchnet.Member.ServiceAdapters.Member member = (Matchnet.Member.ServiceAdapters.Member) e.Item.DataItem;
			Region r = RegionSA.Instance.RetrieveRegionByID(member.GetAttributeInt(g.Brand, WebConstants.ATTRIBUTE_NAME_REGIONID), g.Brand.Site.LanguageID);

			MapPoint point = (MapPoint) e.Item.FindControl("Point");
			point.Region = r;
			point.Member = member;
			point.Url = BreadCrumbHelper.MakeViewProfileLink(member.MemberID);
		}
	}
}
