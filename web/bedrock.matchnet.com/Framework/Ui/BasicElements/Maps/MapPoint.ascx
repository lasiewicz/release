<%@ Control Language="c#" AutoEventWireup="false" Codebehind="MapPoint.ascx.cs" Inherits="Matchnet.Web.Framework.Ui.BasicElements.Maps.MapPoint" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<%@ Register TagPrefix="mn" TagName="GalleryMiniProfile" Src="..\GalleryMiniProfile.ascx" %>

<div style="visibility:hidden">
	<div id="Popup<%=Member.MemberID%>">
		<mn:GalleryMiniProfile runat="server" id="GalleryMiniProfile" />
	</div>
</div>

<script type="text/javascript">

	var marker<%=Member.MemberID%>;

	function onClick<%=Member.MemberID%>()
	{
		marker<%=Member.MemberID%>.openInfoWindow(document.getElementById("Popup<%=Member.MemberID%>"));
	}

	function DrawPoint<%=Member.MemberID%>()
	{
		var lng = <asp:Literal ID="litLongitude" Runat="server" />;
		var lat = <asp:Literal ID="litLatitude" Runat="server" />;
		
		var point = new GPoint(lng, lat);
		marker<%=Member.MemberID%> = new GMarker(point);
		GEvent.addListener(marker<%=Member.MemberID%>, "click", onClick<%=Member.MemberID%>);
		map.addOverlay(marker<%=Member.MemberID%>);
	}
	
    $j(document).ready(function() {DrawPoint<%=Member.MemberID%>()});

</script>
