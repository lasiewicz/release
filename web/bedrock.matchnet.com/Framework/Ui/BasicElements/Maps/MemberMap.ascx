<%@ Control Language="c#" AutoEventWireup="false" Codebehind="MemberMap.ascx.cs" Inherits="Matchnet.Web.Framework.Ui.BasicElements.Maps.MemberMap" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<%@ Register TagPrefix="mn" TagName="Map" Src="Map.ascx" %>
<%@ Register TagPrefix="mn" TagName="MapPoint" Src="MapPoint.ascx" %>

<mn:Map ID="MainMap" Runat="server" />

<asp:Repeater ID="MapRepeater" Runat="server">
	<ItemTemplate>
		<mn:MapPoint ID="Point" Runat="server" />
	</ItemTemplate>
</asp:Repeater>