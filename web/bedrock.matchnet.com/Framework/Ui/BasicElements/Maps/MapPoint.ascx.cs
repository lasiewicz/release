using System;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using Matchnet.Content.ValueObjects.Region;

namespace Matchnet.Web.Framework.Ui.BasicElements.Maps
{
	/// <summary>
	///		Summary description for MapPoint.
	/// </summary>
	public class MapPoint : System.Web.UI.UserControl
	{
		private Region _region;
		private string _url;
		private Matchnet.Member.ServiceAdapters.Member _member;

		#region Form Elements
		protected Literal litFunctionName;
		protected Literal litLongitude;
		protected Literal litLatitude;
		protected GalleryMiniProfile GalleryMiniProfile;
		#endregion

		#region Event Handlers
		private void MapPoint_PreRender(object sender, EventArgs e)
		{
			litLongitude.Text = Map.ToDegrees(_region.Longitude).ToString();
			litLatitude.Text = Map.ToDegrees(_region.Latitude).ToString();
		}
		#endregion

		public string GetRegionID()
		{
			return _region.RegionID.ToString();
		}

		public Region Region
		{
			get
			{
				return _region;
			}
			set
			{
				_region = value;
			}
		}

		public string Url
		{
			get
			{
				return _url;
			}
			set
			{
				_url = value;
			}
		}

		public Matchnet.Member.ServiceAdapters.Member Member
		{
			get
			{
				return _member;
			}
			set
			{
				_member = value;
				GalleryMiniProfile.Member = _member;
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.PreRender += new EventHandler(MapPoint_PreRender);
		}
		#endregion
	}
}
