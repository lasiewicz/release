using System;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using Matchnet.Content.ValueObjects.Region;

namespace Matchnet.Web.Framework.Ui.BasicElements.Maps
{
	/// <summary>
	///		Summary description for Map1.
	/// </summary>
	public class Map : System.Web.UI.UserControl
	{
		#region Private variables
		private string _width;
		private string _height;
		private Region _region;
		#endregion

		#region Form Elements
		protected Literal litLongitude;
		protected Literal litLatitude;
		#endregion

		#region Event Handlers
		private void Map_PreRender(object sender, EventArgs e)
		{
			litLongitude.Text = ToDegrees(_region.Longitude).ToString();
			litLatitude.Text = ToDegrees(_region.Latitude).ToString();
		}
		#endregion

		#region Properties
		public string Width
		{
			get
			{
				return _width;
			}
			set
			{
				_width = value;
			}
		}

		public string Height
		{
			get
			{
				return _height;
			}
			set
			{
				_height = value;
			}
		}

		public Region Region
		{
			get
			{
				return _region;
			}
			set
			{
				_region = value;
			}
		}
		#endregion

		public static Decimal ToDegrees(Decimal radians)
		{
			return (Decimal) ((double) radians * 180 / Math.PI);
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.PreRender += new EventHandler(Map_PreRender);
		}
		#endregion
	}
}
