﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Util;
using Matchnet.Content.ServiceAdapters.Links;

namespace Matchnet.Web.Framework.Ui.BasicElements
{
    /// <summary>
    /// This control is used to display the HighLight information layer, including an icon/text to hide/show the layer.
    /// </summary>
    public partial class HighlightInfoDisplay20 : FrameworkControl
    {
        private bool _memberHighlighted = false;
        private int _viewedMemberID = Constants.NULL_INT;
        private bool _showImageNoText = false;

        public bool MemberHighlighted
        {
            set
            {
                this._memberHighlighted = value;
            }
        }

        public int ViewedMemberID
        {
            set
            {
                this._viewedMemberID = value;
            }
        }

        public bool ShowImageNoText
        {
            set
            {
                this._showImageNoText = value;
            }
        }

        #region Event Handler
        private void Page_Load(object sender, System.EventArgs e)
        {
            
        }

        protected override void OnPreRender(EventArgs e)
        {
            if (this._memberHighlighted)
            {
                // Show Learn about Highlighted Profiles only for profiles that are highlighted
                System.Text.StringBuilder strShowPopup = new System.Text.StringBuilder();
                bool showDifferentInfoDisplayToSubscriber = false;
                if (MemberPrivilegeAttr.IsCureentSubscribedMember(g) && showDifferentInfoDisplayToSubscriber)
                {
                    // Viewing member that is actively subscribed see feature information
                    // but cannot upgrade to get highlighted profile
                    // Renders html and does not take up space
                    strShowPopup.Append("javascript: document.getElementById('" + pnlHighlightProfileInfoForMembersWithoutSubscription.ClientID + "').style.display = 'none';");
                    strShowPopup.Append("TogglePopupDivDisplay('" + pnlHighlightProfileInfoForMembersWithSubscription.ClientID + "');");
                    // Renders html and also takes up space
                    lnkHighlightProfileInfoForMembersWithSubscriptionCloseText.Text = g.GetResource("TXT_CLOSE", this);
                    lnkHighlightProfileInfoForMembersWithSubscriptionCloseImage.Text = g.GetResource("IMAGE_CLOSE",null,true, this);

                    lnkHighlightProfileInfoForMembersWithSubscriptionCloseText.Attributes.Add("href", strShowPopup.ToString());
                    lnkHighlightProfileInfoForMembersWithSubscriptionCloseImage.Attributes.Add("href", strShowPopup.ToString());
                }
                else
                {
                    // Need to set to PurchaseReasonType.ViewedHighlightProfileInfo
                    lnkHighlightProfileInfoSubscribe.NavigateUrl = LinkFactory.Instance.GetLink("/Applications/Subscription/Subscribe.aspx?prtid=" + ((int)PurchaseReasonType.ViewedHighlightProfileInfo).ToString(), g.Brand, System.Web.HttpContext.Current.Request.Url.ToString()) + "&srid=" + this._viewedMemberID.ToString();

                    string resKey = "HIGHLIGHTPROFILEINFO_TO_SUBSCRIPTION";
                    imgHighlightProfileInfoForMembersWithoutSubscription.ResourceConstant = resKey;

                    // Viewing member that is not actively subscribed will see feature
                    // explanation and link to upgrade to get highlighted profile
                    strShowPopup.Append("javascript: document.getElementById('" + pnlHighlightProfileInfoForMembersWithSubscription.ClientID + "').style.display = 'none';");
                    strShowPopup.Append("TogglePopupDivDisplay('" + pnlHighlightProfileInfoForMembersWithoutSubscription.ClientID + "');");

                    lnkHighlightProfileInfoForMembersWithoutSubscriptionCloseText.Text = g.GetResource("TXT_CLOSE", this);
                    lnkHighlightProfileInfoForMembersWithoutSubscriptionCloseImage.Text = g.GetResource("IMAGE_CLOSE", null, true, this);

                    lnkHighlightProfileInfoForMembersWithoutSubscriptionCloseText.Attributes.Add("href", strShowPopup.ToString());
                    lnkHighlightProfileInfoForMembersWithoutSubscriptionCloseImage.Attributes.Add("href", strShowPopup.ToString());
                }
                lnkHighlightProfileInfo.Attributes.Add("href", strShowPopup.ToString());

                if (this._showImageNoText)
                {
                    // Show image without text
                    string resourceKey = "IMAGE_LEARN_ABOUT_HIGHLIGHTED_PROFILES";
                    lnkHighlightProfileInfo.Text = g.GetResource(resourceKey, null, true, this);
                }
                else
                {
                    // Show the text without image
                    lnkHighlightProfileInfo.Text = g.GetResource("TXT_LEARN_ABOUT_HIGHLIGHTED_PROFILES", this);
                }
                lnkHighlightProfileInfo.Visible = true;
                pnlHighlightProfileInfoForMembersWithSubscription.Style.Add("display", "none");
                pnlHighlightProfileInfoForMembersWithSubscription.Style.Remove("visibility");
                pnlHighlightProfileInfoForMembersWithoutSubscription.Style.Add("display", "none");
                pnlHighlightProfileInfoForMembersWithoutSubscription.Style.Remove("visibility");

            }
            else
            {
                lnkHighlightProfileInfo.Visible = false;
                // Do not render html
                pnlHighlightProfileInfoForMembersWithSubscription.Visible = false;
                pnlHighlightProfileInfoForMembersWithoutSubscription.Visible = false;
            }

            base.OnPreRender(e);
        }
        #endregion

    }
}