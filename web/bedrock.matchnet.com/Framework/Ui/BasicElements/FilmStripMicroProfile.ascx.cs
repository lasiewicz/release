﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Member.ValueObjects;
using Matchnet.Member.ValueObjects.Interfaces;
using Matchnet.Member.ValueObjects.Photos;
using Matchnet.Web.Framework.Managers;
using Matchnet.Web.Framework.Ui.PageElements;
using Matchnet.List.ValueObjects;
using Matchnet.Content.ServiceAdapters.Links;
using Matchnet.Configuration.ServiceAdapters.Analitics;
using Matchnet.Web.Applications.ColorCode;

namespace Matchnet.Web.Framework.Ui.BasicElements
{
    /// <summary>
    /// Small profile used in the ProfileFilmStrip
    /// </summary>
    public partial class FilmStripMicroProfile : FrameworkControl
    {
        #region Fields
        private int _ordinal;
        private int _memberID;
        private BreadCrumbHelper.EntryPoint _myEntryPoint;
        private MOCollection _myMOCollection;
        private int _hotListCategoryID;
        private IMemberDTO _Member;
        private bool _isHighlighted = false;
        protected string viewProfileUrl;
        private DateTime _lastHotlistActionDate = DateTime.MinValue;
        private bool _displayExtendedDetail = false;
        private DisplayContextType _DisplayContext = DisplayContextType.Default;
        protected Color _Color = Color.none;
        protected string _ColorText = "";

        const int MAX_WORD_LENGTH = 10;
        const int MAX_STRING_LENGTH = 40;
        const int MAX_USERNAME_LENGTH = 16;
        #endregion

        #region Properties
        public IMemberDTO member
        {
            get { return _Member; }
            set
            {
                _Member = value;
                _memberID = _Member.MemberID;

                if (FrameworkGlobals.memberHighlighted(this._memberID, g.Brand))
                {
                    this._isHighlighted = true;
                }
                else
                {
                    this._isHighlighted = false;
                }
            }
        }

        public int MemberID
        {
            get { return (_memberID); }
        }

        public int Ordinal
        {
            get { return (_ordinal); }
            set { _ordinal = value; }
        }


        public BreadCrumbHelper.EntryPoint MyEntryPoint
        {
            get { return (_myEntryPoint); }
            set { _myEntryPoint = value; }
        }

        public MOCollection MyMOCollection
        {
            get { return (_myMOCollection); }
            set { _myMOCollection = value; }
        }

        public int HotListCategoryID
        {
            get { return (_hotListCategoryID); }
            set { _hotListCategoryID = value; }
        }

        public bool IsHighlighted
        {
            get { return (this._isHighlighted); }
            set { this._isHighlighted = value; }
        }

        public DateTime LastHotlistActionDate
        {
            get { return (this._lastHotlistActionDate); }
            set { this._lastHotlistActionDate = value; }
        }

        public bool DisplayExtendedDetail
        {
            get { return this._displayExtendedDetail; }
            set { this._displayExtendedDetail = value; }
        }

        public string TrackingParam { get; set; }
        #endregion

        #region Event Handler
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected override void OnPreRender(EventArgs e)
        {
            this.phExtendedDetail.Visible = _displayExtendedDetail;
            base.OnPreRender(e);
        }
        #endregion


        #region Methods
        /// <summary>
        /// Loads Member Profile based on properties already set into the control, including the Member object
        /// </summary>
        public void LoadMemberProfile()
        {
            try
            {
                SearchResultProfile searchResult = new SearchResultProfile(_Member, Ordinal);

                if (this.IsHighlighted)
                {
                    viewProfileUrl = BreadCrumbHelper.MakeViewProfileLink(this.MyEntryPoint, this.MemberID, this.Ordinal, this.MyMOCollection, this.HotListCategoryID, false, (int)BreadCrumbHelper.PremiumEntryPoint.Highlight);
                }
                else
                {
                    viewProfileUrl = BreadCrumbHelper.MakeViewProfileLink(this.MyEntryPoint, this.MemberID, this.Ordinal, this.MyMOCollection, this.HotListCategoryID, false, (int)BreadCrumbHelper.PremiumEntryPoint.NoPremium);
                }

                if (!String.IsNullOrEmpty(TrackingParam))
                {
                    viewProfileUrl = BreadCrumbHelper.AppendParamToProfileLink(viewProfileUrl, TrackingParam);
                }

                //set basic info
                lnkUserName1.NavigateUrl = viewProfileUrl;
                lnkUserName1.Text = FrameworkGlobals.Ellipsis(_Member.GetUserName(g.Brand), 12);

                noPhoto.MemberID = _Member.MemberID;
                //MemberPhotoHelper photoHelper = new MemberPhotoHelper(RuntimeSettings.Instance);
                noPhoto.NoPhotoFileName = MemberPhotoDisplayManager.Instance.GetNoPhotoFile(NoPhotoImageType.BackgroundThumb, true);
                searchResult.SetThumb(imgThumb, noPhoto, viewProfileUrl);

                searchResult.SetAge(literalAge, this);

                if (FrameworkGlobals.isHebrewSite(g.Brand))
                {
                    literalAge.Text = g.GetResource("TXT_YEARS_OLD", this) + " " + literalAge.Text;
                }
                else
                {
                    literalAge.Text += " " + g.GetResource("TXT_YEARS_OLD", this);
                }

                searchResult.SetRegion(g.Brand.Site.LanguageID, literalLocation, MAX_STRING_LENGTH);

                //color code
                if (ColorCodeHelper.IsColorCodeEnabled(g.Brand) && ColorCodeHelper.HasMemberCompletedQuiz(_Member, g.Brand) && !ColorCodeHelper.IsMemberColorCodeHidden(_Member, g.Brand))
                {
                    phColorCode.Visible = true;
                    _Color = ColorCodeHelper.GetPrimaryColor(_Member, g.Brand);
                    _ColorText = ColorCodeHelper.GetFormattedColorText(_Color);
                }

            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        #endregion
    }
}