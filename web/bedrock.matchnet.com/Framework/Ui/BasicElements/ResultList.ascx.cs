using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text.RegularExpressions;
using System.Collections;
using System.Collections.Generic;
using Matchnet.Lib;
using Matchnet.List.ServiceAdapters;
using Matchnet.List.ValueObjects;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Member.ServiceAdapters.Interfaces;
using Matchnet.Member.ValueObjects.Interfaces;
using Matchnet.UserNotifications.ServiceAdapters;
using Matchnet.UserNotifications.ValueObjects;
using Matchnet.Web.Applications.UserNotifications;

namespace Matchnet.Web.Framework.Ui.BasicElements
{
    /// <summary>
    ///		Summary description for ResultList.
    /// </summary>
    public class ResultList : FrameworkControl, IResultList
    {
        private MOCollection _myMOCollection;
        private MatchesCollection _myMatchesCollection; // For MatchMeter app (Jmeter)

        protected HtmlInputHidden hidStartRow;

        protected Txt noUsersText;
        protected Repeater galleryMemberRepeater;
        protected Repeater memberRepeater;
        protected Repeater photoGalleryRepeater;
        protected Matchnet.Web.Framework.Ui.BasicElements.NoResults noSearchResults;
        protected PlaceHolder phMembersOnlineNoResults;
        protected PlaceHolder plcPromotionalProfile;
        protected Matchnet.Web.Framework.Ui.BasicElements.MiniProfile promotionProfile;


        private DataTable dt = null;
        private ArrayList targetMemberIDs = new ArrayList();
        public string galleryView = String.Empty;

        private HotListDirection _hotListDirection;
        private HotListCategory _hotListCategory = HotListCategory.Default;
        private HotListType _hotListType;

        // MatchMeter
        private bool _isMatchMeterEnabled = false;
        private Dictionary<IMemberDTO, decimal> MatchResultsMemberScoreDictionary;

        private const int NUMBER_OF_TRACKED_PAGES = 6;	// On SAGE we have channels up to "Results_Page6plus".
        private List<int> searchIDs;
        private bool hasSpotlightProfile = false;

        bool _enableGalleryView = true;
        int memberNum = 0;
        private int count = 0;
        private int highlightedProfileCount = 0;
        int PAGE_SIZE = 12;
        int CHAPTER_SIZE = 3;
        public int _StartRow;
        private int _EndRow;
        private int _TotalRows;

        #region IResultList implementation
        public string StartRowClientID
        {
            get
            {
                return hidStartRow.ClientID;
            }
        }

        public ArrayList TargetMemberIDs
        {
            get { return targetMemberIDs; }
            set { targetMemberIDs = value; }
        }

        public string GalleryView
        {
            get { return galleryView; }
            set { galleryView = value; }
        }

        public FrameworkControl ThisControl
        {
            get { return this; }

        }

        public DataTable Dt
        {
            get { return dt; }
        }

        public Int32 MemberNum
        {
            get { return memberNum; }
            set { memberNum = value; }
        }

        public List<int> SearchIDs
        {

            get { return searchIDs; }
            set { searchIDs = value; }
        }

        public bool EnableMultipleSearchViews { get { return _enableGalleryView; } set { _enableGalleryView = value; } }
        public bool IgnoreSAFilteredSearchResultsCache { get; set; }
        public bool IgnoreSASearchResultsCache { get; set; }
        public bool IgnoreAllSearchResultsCache { get; set; }

        #region controls
        public Repeater GalleryMemberRepeater { get { return galleryMemberRepeater; } }
        public Repeater MemberRepeater { get { return memberRepeater; } }
        public Repeater PhotoGalleryRepeater { get { return photoGalleryRepeater; } }
        public Repeater JMeterRepeater { get { return new Repeater(); } }
        public Matchnet.Web.Framework.Ui.BasicElements.NoResults NoSearchResults { get { return noSearchResults; } }
        public PlaceHolder PLCMembersOnlineNoResults { get { return phMembersOnlineNoResults; } }
        public PlaceHolder PLCPromotionalProfile { get { return plcPromotionalProfile; } }
        public Matchnet.Web.Framework.Ui.BasicElements.MiniProfile PromotionProfile { get { return promotionProfile; } }
        public Txt NoUsersText { get { return noUsersText; } }
        #endregion
        # region context type for result list
        private ResultContextType _ResultListContextType;
        private bool _bEnableSingleSelect = false;

        public ResultContextType ResultListContextType
        {
            get { return (_ResultListContextType); }
            set { _ResultListContextType = value; }
        }


        public bool EnableSingleSelect
        {
            get { return _bEnableSingleSelect; }
            set { _bEnableSingleSelect = value; }
        }
        #endregion

        #region properties
        public int PageSize
        {
            get { return PAGE_SIZE; }
            set { PAGE_SIZE = value; }
        }

        public int ChapterSize
        {
            get { return CHAPTER_SIZE; }
            set { CHAPTER_SIZE = value; }
        }


        public int TotalRows
        {
            get { return _TotalRows; }
            set { _TotalRows = value; }
        }
        public Int32 StartRow
        {
            get
            {
                return Conversion.CInt(hidStartRow.Value);
            }
            set
            {
                hidStartRow.Value = value.ToString();
            }
        }

        public int EndRow
        {
            get { return _EndRow; }
            set { _EndRow = value; }
        }
        public MOCollection MyMOCollection
        {
            get { return (_myMOCollection); }
            set { _myMOCollection = value; }
        }

        /// <summary>
        /// Gets or sets the matches collection
        /// For matchmeter app (Jmeter).
        /// </summary>
        /// <value>My matches collection.</value>
        public MatchesCollection MyMatchesCollection
        {
            get { return _myMatchesCollection; }
            set { _myMatchesCollection = value; }
        }

        public HotListCategory HotListCategory
        {
            get
            {
                return _hotListCategory;
            }
            set
            {
                _hotListCategory = value;
            }
        }


        public HotListType HotListType
        {
            get
            {
                return _hotListType;
            }
            set
            {
                _hotListType = value;
            }
        }

        public HotListDirection HotListDirection
        {
            get
            {
                return _hotListDirection;
            }
            set
            {
                _hotListDirection = value;
            }
        }
        public bool PhotoOnly { get; set; }

        #endregion


        #endregion

        private ResultListHandler _resultListHandler = null;


        # region General Page Init
        /// <summary>
        /// This init method fires off three other init methods.  They are used to prepare the page with anything needed.
        /// The init method called is dependant upon the result list context type.
        /// </summary>
        private void Page_Init(object sender, System.EventArgs e)
        {
            try
            {


                if (ResultListContextType == ResultContextType.HotList)
                {
                    DoHotListInit();
                }
                else if (ResultListContextType == ResultContextType.SearchResult)
                {
                    DoSearchResultInit();
                }
                else if (ResultListContextType == ResultContextType.QuickSearchResult)
                {
                    DoQuickSearchResultInit();
                }
                else if (ResultListContextType == ResultContextType.MembersOnline)
                {
                    DoMembersOnlineInit();
                }
            }
            catch (Exception ex)
            {
                _g.ProcessException(ex);
            }
        }

        private void DoMembersOnlineInit()
        { }

        private void DoSearchResultInit()
        { }

        private void DoQuickSearchResultInit()
        { }

        private void DoHotListInit()
        {
            if (Request["CategoryID"] != null)
            {
                _hotListCategory = (HotListCategory)Enum.Parse(typeof(HotListCategory), Request["CategoryID"]);
            }
        }
        #endregion

        # region General page load
        /// <summary>
        /// The page load method initializes the start row and the end row for the results.
        /// Then, dependant upon resutl list context type, a page specific load is fired.
        /// </summary>
        private void Page_Load(object sender, System.EventArgs e)
        {
            try
            {
                _resultListHandler = new ResultListHandler(g, this);
                FrameworkGlobals.SaveBackToResultsURL(g);
                SetMatchMeter();

                int startRow = Constants.NULL_INT;

                //	Default to 1.
                _StartRow = 1;
                StartRow = 1;

                // Check the hidden input field.
                if (Request[hidStartRow.UniqueID] != null)
                {
                    startRow = Int32.Parse(Request[hidStartRow.UniqueID]);

                    if (startRow >= 1)
                    {
                        //	Use internal first, if available
                        _StartRow = startRow;
                        StartRow = startRow;
                    }
                }
                else
                {
                    // If there was no value in the hidden field, check the querystring.
                    if (Request.QueryString[ResultListHandler.QSPARAM_STARTROW] != null)
                    {
                        try
                        {
                            startRow = Int32.Parse(Request.QueryString[ResultListHandler.QSPARAM_STARTROW]);
                        }
                        catch
                        {
                            startRow = Constants.NULL_INT;
                        }

                        if (startRow >= 1)
                        {
                            //	Use internal first, if available
                            _StartRow = startRow;
                            StartRow = startRow;
                        }
                    }
                }

                _EndRow = _StartRow + PAGE_SIZE - 1;
                if (_EndRow > _TotalRows)
                {
                    _EndRow = _TotalRows;
                }

                // Reset the SearchPref override if we are on page 1.  So, if a visitor hits a visitor limit then logs in,
                // this flag is what keeps the search prefs from being overwritten by their saved search prefs.  However, if they
                // start a new search (startRow == 1) then we revert back to their saved search prefs.  There is a loophole (a user
                // navs back to page 1) but let's live with it for now.  See TT 12657.
                if (startRow == Constants.NULL_INT)
                {
                    g.Session.Remove(VisitorLimitHelper.SESSIONKEY_VISITORLIMIT_DONOTOVERWRITESEARCHPREFS);
                    // We need to load the search prefs again by setting it to null.
                    _g.ResetSearchPreferences();
                }

                if (ResultListContextType == ResultContextType.HotList)
                {
                    _resultListHandler.LoadHotListPage();
                    _resultListHandler.RenderHotListElements();
                    _resultListHandler.BindMemberRepeater();
                }
                else if (ResultListContextType == ResultContextType.MembersOnline)
                {
                    //Issue #10938
                    //if (! this.IsPostBack) 
                    //{
                    _resultListHandler.LoadMembersOnlinePage();
                    //}
                }
                else if (ResultListContextType == ResultContextType.SearchResult)
                {
                    _resultListHandler.LoadSearchResultPage();
                    // ((Matchnet.Web.Applications.Search.SearchResults)(this.Parent.Parent)).showPromotionalProfile();
                    showSpotlightProfile();

                }
                else if (ResultListContextType == ResultContextType.QuickSearchResult)
                {
                    _resultListHandler.LoadQuickSearchResultPage();
                }
                else if (ResultListContextType == ResultContextType.PhotoGallery)
                { _resultListHandler.LoadPhotoGalleryResultPage(); }
                else if (ResultListContextType == ResultContextType.MatchMeterMatches)
                { _resultListHandler.LoadMatchMeterMatchesResultPage(MyMatchesCollection, ref MatchResultsMemberScoreDictionary, StartRow, PAGE_SIZE, ref _TotalRows); }

                _resultListHandler.RenderPaging();
            }
            catch (Exception ex)
            {
                _g.ProcessException(ex);
            }
        }

        public DropDownList GetMOLSortDropDown()
        {
            return (DropDownList)this.Parent.FindControl("ddlMemberSort");
        }

        /// <summary>
        /// For Analytics
        /// </summary>
        /// <param name="e"></param>
        public void RenderAnalytics(ArrayList members, bool moreResultsAvailable)
        {
            int analyticsID = ConstantsTemp.PAGE_SEARCH_RESULTS;
            if (members.Count > 0)
            {
                // SAGE - each results page should have a different g.Analytics["p"] value.
                analyticsID += 1 + _StartRow / PAGE_SIZE;
                // SAGE has a limit to how many pages it tracks.
                if (analyticsID >= ConstantsTemp.PAGE_SEARCH_RESULTS + NUMBER_OF_TRACKED_PAGES)
                    analyticsID = ConstantsTemp.PAGE_SEARCH_RESULTS + NUMBER_OF_TRACKED_PAGES;
            }

            g.Analytics.SetAttribute("p", analyticsID.ToString());
        }

        private void SetMatchMeter()
        {
            // MatchMeter (jmeter)
            int mmsett = Int32.Parse(RuntimeSettings.GetSetting("MatchTest_App_Settings", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID));
            int mmenumval = (Int32)WebConstants.MatchMeterFlags.EnableApp;
            if ((mmsett & mmenumval) == mmenumval)
            {
                _isMatchMeterEnabled = true;
            }
        }
        #endregion


        private void showSpotlightProfile()
        {
            try
            {
                Control control = this.Parent;
                while (control != null && control.GetType().ToString() != "ASP.applications_search_searchresults_ascx")
                {
                    control = control.Parent;
                }
                //((Matchnet.Web.Applications.Search.SearchResults)(this.Parent.Parent)).showPromotionalProfile();
                hasSpotlightProfile = ((Matchnet.Web.Applications.Search.SearchResults20)control).showSpotlightProfile(searchIDs);
            }
            catch (Exception ex)
            { }

        }

        #region Member Repeater on item data bound
        public void MemberRepeater_OnItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
        {
            count++;

            Matchnet.Web.Framework.Ui.BasicElements.MiniProfile miniProfile = (Matchnet.Web.Framework.Ui.BasicElements.MiniProfile)e.Item.FindControl("MiniProfile");

            miniProfile.Counter = count;
            miniProfile.Ordinal = count + this._StartRow - 1;
            miniProfile.DisplayContext = ResultListContextType;
            miniProfile.EnableSingleSelect = EnableSingleSelect;
            // MatchMeter
            miniProfile.IsMatchMeterEnabled = _isMatchMeterEnabled;
            switch (ResultListContextType)
            {
                case ResultContextType.HotList:
                    miniProfile.MyEntryPoint = BreadCrumbHelper.EntryPoint.HotLists;
                    miniProfile.HotListCategory = this.HotListCategory;
                    miniProfile.SaveNote += new Matchnet.Web.Framework.Ui.BasicElements.MiniProfile.SaveNoteEventHandler(miniProfile_SaveNote);
                    break;
                case ResultContextType.MembersOnline:
                    miniProfile.MyEntryPoint = BreadCrumbHelper.EntryPoint.MembersOnline;
                    miniProfile.MyMOCollection = this.MyMOCollection;
                    break;
                case ResultContextType.SearchResult:
                    miniProfile.MyEntryPoint = BreadCrumbHelper.EntryPoint.SearchResults;
                    break;
                case ResultContextType.QuickSearchResult:
                    miniProfile.MyEntryPoint = BreadCrumbHelper.EntryPoint.QuickSearchResults;
                    break;
                case ResultContextType.MatchMeterMatches:
                    if (MatchResultsMemberScoreDictionary != null)
                    {
                        miniProfile.MatchesScore = MatchResultsMemberScoreDictionary[miniProfile.Member];
                    }
                    break;
            }
            miniProfile.CategoryID = _hotListCategory.ToString();

            if (miniProfile.IsHighlighted)
            {
                this.highlightedProfileCount++;
            }
        }
        #endregion

        # region Gallery Member Repeater on item data bound
        public void GalleryMemberRepeater_OnItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
        {
            count++;


            Matchnet.Web.Framework.Ui.BasicElements.GalleryMiniProfile GalleryMiniProfile = (Matchnet.Web.Framework.Ui.BasicElements.GalleryMiniProfile)e.Item.FindControl("GalleryMiniProfile");

            Literal rptrNewRow = (Literal)e.Item.FindControl("rptrNewRow");
            Literal rptrNewCell = (Literal)e.Item.FindControl("rptrNewCell");
            Literal rptrEndCell = (Literal)e.Item.FindControl("rptrEndCell");
            Literal rptrEndRow = (Literal)e.Item.FindControl("rptrEndRow");

            if (count % 3 == 1)
            {
                rptrNewRow.Visible = true;
                rptrNewCell.Visible = true;

                rptrEndCell.Visible = true;
            }
            else if (count % 3 == 2)
            {
                rptrNewCell.Visible = true;

                rptrEndCell.Visible = true;
            }
            else
            {
                rptrNewCell.Visible = true;

                rptrEndCell.Visible = true;
                rptrEndRow.Visible = true;
            }

            if (count == memberNum)
            {
                if (count % 3 == 1)
                {
                    ((Literal)e.Item.FindControl("rptrEndTwoCells")).Visible = true;
                }
                else if (count % 3 == 2)
                {
                    ((Literal)e.Item.FindControl("rptrEndOneCell")).Visible = true;
                }
            }

            GalleryMiniProfile.Counter = count;
            GalleryMiniProfile.Ordinal = this._StartRow + count - 1;
            GalleryMiniProfile.DisplayContext = ResultListContextType;
            GalleryMiniProfile.IsMatchMeterEnabled = _isMatchMeterEnabled;
            switch (ResultListContextType)
            {
                case ResultContextType.HotList:
                    GalleryMiniProfile.MyEntryPoint = BreadCrumbHelper.EntryPoint.HotLists;
                    GalleryMiniProfile.HotListCategoryID = (int)_hotListCategory;
                    break;
                case ResultContextType.MembersOnline:
                    GalleryMiniProfile.MyEntryPoint = BreadCrumbHelper.EntryPoint.MembersOnline;
                    GalleryMiniProfile.MyMOCollection = this.MyMOCollection;
                    break;
                case ResultContextType.SearchResult:
                    GalleryMiniProfile.MyEntryPoint = BreadCrumbHelper.EntryPoint.SearchResults;
                    break;
                case ResultContextType.QuickSearchResult:
                    GalleryMiniProfile.MyEntryPoint = BreadCrumbHelper.EntryPoint.QuickSearchResults;
                    break;
            }

            if (GalleryMiniProfile.IsHighlighted)
            {
                this.highlightedProfileCount++;
            }
        }
        #endregion

        # region Photo Gallery  Repeater on item data bound
        public void PhotoGalleryRepeater_OnItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
        {
            count++;

            Matchnet.Web.Applications.PhotoGallery.Controls.PhotoGalleryProfile20 GalleryProfile = (Matchnet.Web.Applications.PhotoGallery.Controls.PhotoGalleryProfile20)e.Item.FindControl("PhotoGalleryProfile");
            GalleryProfile.ResultMember = (Matchnet.PhotoSearch.ValueObjects.PhotoResultItem)e.Item.DataItem;
            GalleryProfile.row = Math.Abs(count - 1) / ChapterSize + 1;
            GalleryProfile.col = Math.Abs(count - 1) % ChapterSize;

            Literal rptrNewRow = (Literal)e.Item.FindControl("rptrNewRow");
            Literal rptrNewCell = (Literal)e.Item.FindControl("rptrNewCell");
            Literal rptrEndCell = (Literal)e.Item.FindControl("rptrEndCell");
            Literal rptrEndRow = (Literal)e.Item.FindControl("rptrEndRow");

            if (count % ChapterSize == 1)
            {
                rptrNewRow.Visible = true;
                rptrNewCell.Visible = true;

                rptrEndCell.Visible = true;
            }
            else if (count % ChapterSize == 0)
            {
                rptrNewCell.Visible = true;

                rptrEndCell.Visible = true;
                rptrEndRow.Visible = true;
            }
            else
            {
                rptrNewCell.Visible = true;
                rptrEndCell.Visible = true;
            }

            if (count == memberNum)
            {
                if (count % ChapterSize == 1)
                {
                    ((Literal)e.Item.FindControl("rptrEndThreeCells")).Visible = true;
                }
                else if (count % ChapterSize == 0)
                {
                    ((Literal)e.Item.FindControl("rptrEndOneCell")).Visible = true;
                }
                else if (count % ChapterSize == 2)
                {
                    ((Literal)e.Item.FindControl("rptrEndTwoCells")).Visible = true;
                }
                else
                {
                    ((Literal)e.Item.FindControl("rptrEndOneCell")).Visible = true;
                }
            }

            if (GalleryProfile.IsHighlighted)
            {
                this.highlightedProfileCount++;
            }
        }

        #endregion

        #region Web Form Designer generated code, unaltered
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
            this.Init += new System.EventHandler(this.Page_Init);
        }
        #endregion

        private void miniProfile_SaveNote(object sender, SaveNoteEventArgs e)
        {
            foreach (RepeaterItem item in MemberRepeater.Items)
            {
                MiniProfile miniProfile = (MiniProfile)item.FindControl("MiniProfile");

                string note = Regex.Replace(miniProfile.Note, @"<(.|\n)*?>", string.Empty);
                int memberID = miniProfile.MemberID;

                ListItemDetail listItemDetail = g.List.GetListItemDetail(_hotListCategory,
                    g.Brand.Site.Community.CommunityID,
                    g.Brand.Site.SiteID,
                    memberID);

                if (note != miniProfile.DefaultNote && note != listItemDetail.Comment)
                {
                    int creatorId = g.Session.GetInt(WebConstants.SESSION_PROPERTY_NAME_MEMBERID);
                    ListSA.Instance.AddListMember(_hotListCategory,
                        g.Brand.Site.Community.CommunityID,
                        g.Brand.Site.SiteID,
                        creatorId,
                        memberID,
                        note,
                        Constants.NULL_INT,
                        !((g.Member.GetAttributeInt(g.Brand, "HideMask", 0) & (Int32)WebConstants.AttributeOptionHideMask.HideHotLists) == (Int32)WebConstants.AttributeOptionHideMask.HideHotLists),
                        false);

                    if (UserNotificationFactory.IsUserNotificationsEnabled(g))
                    {
                        UserNotificationParams unParams = UserNotificationParams.GetParamsObject(memberID, g.Brand.Site.SiteID, g.Brand.Site.Community.CommunityID);
                        UserNotification notification = UserNotificationFactory.Instance.GetUserNotification(new AddedYouAsFavoriteUserNotification(), memberID, creatorId, g);
                        if (notification.IsActivated())
                        {
                            UserNotificationsServiceSA.Instance.AddUserNotification(unParams, notification.CreateViewObject());
                        }
                    }
                }
            }
        }

        private void showPromotionalProfile()
        {
            try
            {

                if (g.PromotionalProfileEnabled())
                {

                    int memberID = g.PromotionalProfileMember();
                    if (memberID > 0)
                    {
                        PromotionProfile.Member = Matchnet.Member.ServiceAdapters.MemberSA.Instance.GetMember(memberID, Matchnet.Member.ServiceAdapters.MemberLoadFlags.None);
                        if (PromotionProfile.Member != null)
                        {
                            plcPromotionalProfile.Visible = true;
                            PromotionProfile.IsMatchMeterEnabled = _isMatchMeterEnabled;
                        }
                    }
                }
            }
            catch (Exception ex) { }
        }

        protected override void OnPreRender(EventArgs e)
        {
            try
            {
                // Omniture Analytics - Event and property variable tracking
                if (Convert.ToBoolean(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ANALYTICS_OMNITURE", _g.Brand.Site.Community.CommunityID, _g.Brand.Site.SiteID)))
                {
                    // Search Depth
                    _g.AnalyticsOmniture.Evar25 = Convert.ToString(Math.Round(((double)(_StartRow - 1) / 12), 0) + 1);

                    // Track total profiles that are highlightde in the search result
                    _g.AnalyticsOmniture.AddEvent("event5");
                    _g.AnalyticsOmniture.AddProductEvent("event5=" + Convert.ToString(this.highlightedProfileCount));
                    //_g.AnalyticsOmniture.Products = Convert.ToString(this.highlightedProfileCount) + ";" + Convert.ToString(this.count) + ";event5=2"; 
                    if (hasSpotlightProfile)
                    {
                        _g.AnalyticsOmniture.AddEvent("event13");
                        _g.AnalyticsOmniture.AddProductEvent("event13=1");
                    }
                }

                base.OnPreRender(e);
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        #region Commented code moved to ResultListHandler for site redesign project not to dupplicate functionalities

        //private string BuildPagingURL(int startRow)
        //{
        //    //start with the name of the current control
        //    StringBuilder url = new StringBuilder(_g.AppPage.ControlName + ".aspx?");
        //    NameValueCollection qsParams = Request.QueryString;
        //    if (qsParams.Keys.Count > 0)
        //    {
        //        //rebuild the querystring portion of the URL 
        //        foreach (string key in qsParams.Keys)
        //        {
        //            //We're adding our own start row parameter, so ignore that part of the querystring. Also, 
        //            //we don't want to pass the resource constant around since it's a one time message, 
        //            // so strip it out as well. 
        //            if (!key.Equals(ResultListHandler.QSPARAM_STARTROW) && !key.Equals(ResultListHandler.QSPARAM_RESOURCECONSTANT))
        //            {
        //                url.Append(key + "=" + qsParams[key] + "&");
        //            }
        //        }
        //    }

        //    url.Append(ResultListHandler.QSPARAM_STARTROW + "=" + startRow.ToString());
        //    return url.ToString();
        //}

        //private void LoadMembersOnlinePage()
        //{
        //    DropDownList ddlSearchOption = (DropDownList)this.Parent.FindControl("ddlMemberSort");
        //    SortFieldType sortField;
        //    SortDirectionType sortDirection;

        //    try
        //    {
        //        sortField = (SortFieldType)Enum.Parse( typeof(SortFieldType), ddlSearchOption.SelectedValue );
        //    }
        //    catch  // Throws exception if bad enum type or null value, so use default
        //    {
        //        sortField = SortFieldType.InsertDate;
        //    }

        //    switch( sortField )
        //    {
        //        case SortFieldType.Age :
        //        case SortFieldType.HasPhoto :
        //        case SortFieldType.InsertDate :
        //            sortDirection = SortDirectionType.Desc;
        //            break;
        //        default :
        //            sortDirection = SortDirectionType.Asc;
        //            break;
        //    }

        //    Int16 genderMask = 0;

        //    if (this.MyMOCollection.GenderMask != Constants.NULL_INT)
        //    {
        //        genderMask = (Int16)this.MyMOCollection.GenderMask;
        //    }

        //    Matchnet.MembersOnline.ValueObjects.MOLQueryResult MOLResults = MembersOnlineSA.Instance.GetMemberIDs(g.Session.Key.ToString(),
        //        g.Brand.Site.Community.CommunityID,
        //        genderMask,
        //        this.MyMOCollection.RegionID,
        //        (Int16)this.MyMOCollection.AgeMin,
        //        (Int16)((Int16)this.MyMOCollection.AgeMax + 1),	// See TT 15298.
        //        this.MyMOCollection.LanguageMask,
        //        sortField,
        //        sortDirection,
        //        _StartRow - 1,
        //        PAGE_SIZE);

        //    ArrayList members = MemberSA.Instance.GetMembers(MOLResults.ToArrayList(),
        //        MemberLoadFlags.None);

        //    BindMemberList(members);
        //    RenderAnalytics( members, (MOLResults.TotalMembersOnline > MOLResults.MembersReturned) );
        //    _TotalRows = MOLResults.TotalMembersOnline;
        //    //renderAnalytics(members);
        //}



        //private void LoadSearchResultPage()
        //{

        //    g.SearchPreferences["DomainID"] = g.TargetCommunityID.ToString();

        //    MatchnetQueryResults searchResults = null;

        //    int memberId = (g.Member != null) ? g.Session.GetInt(WebConstants.SESSION_PROPERTY_NAME_MEMBERID) : 0;

        //    // default several required search preferences for access without cookies
        //    if(memberId == 0 || (g.Session.GetString(VisitorLimitHelper.SESSIONKEY_VISITORLIMIT_DONOTOVERWRITESEARCHPREFS, String.Empty) == "true")) 
        //    {
        //        if (!PrefsHaveValue(g.SearchPreferences, "SearchTypeID"))
        //        {
        //            g.SearchPreferences["SearchTypeID"] = g.Brand.Site.DefaultSearchTypeID.ToString();	
        //        }

        //        if (!PrefsHaveValue(g.SearchPreferences, "RegionID"))
        //        {
        //            g.SearchPreferences["RegionID"] = g.Brand.Site.DefaultRegionID.ToString();
        //        }

        //        if(!PrefsHaveValue(g.SearchPreferences, "GenderMask"))
        //        {
        //            // set the gender mask to M s M and M s F for all others
        //            if(g.Brand.Site.SiteID == (int) WebConstants.SITE_ID.Glimpse) 
        //            {
        //                g.SearchPreferences["GenderMask"] = (ConstantsTemp.GENDERID_MALE + ConstantsTemp.GENDERID_SEEKING_MALE).ToString();
        //            } 
        //            else 
        //            {
        //                g.SearchPreferences["GenderMask"] = (ConstantsTemp.GENDERID_MALE + ConstantsTemp.GENDERID_SEEKING_FEMALE).ToString();
        //            }
        //        }

        //        if(!PrefsHaveValue(g.SearchPreferences, "MinAge"))
        //        {
        //            g.SearchPreferences["MinAge"] = g.Brand.DefaultAgeMin.ToString();
        //        }

        //        if(!PrefsHaveValue(g.SearchPreferences, "MaxAge"))
        //        {
        //            g.SearchPreferences["MaxAge"] = g.Brand.DefaultAgeMax.ToString();
        //        }
        //        if(!PrefsHaveValue(g.SearchPreferences, "Distance"))
        //        {
        //            g.SearchPreferences["Distance"] = g.Brand.DefaultSearchRadius.ToString();
        //        }
        //    }

        //    if (g.SearchPreferences["CountryRegionID"] == string.Empty)
        //    {
        //        g.SearchPreferences.Add("CountryRegionID", g.Brand.Site.DefaultRegionID.ToString());
        //    }

        //    //wlybrand - Redirect to search prefs if the user does not have a valid set of preferences.  This is needed to avoid exceptions from Search.
        //    try 
        //    {	// Exception thrown if search prefernce validation fails.
        //        g.SaveSearchPreferences();
        //    }
        //    catch (Exception ex) 
        //    {
        //        g.ProcessException(ex);
        //        g.Transfer("/Applications/Search/SearchPreferences.aspx");
        //    }

        //    // TT13887 - Redirect to search prefs if the user does not have a valid set of preferences.  This is needed to avoid exceptions from Search.
        //    try
        //    {
        //        searchResults = MemberSearchSA.Instance.Search(g.SearchPreferences
        //            , g.Brand.Site.Community.CommunityID
        //            , g.Brand.BrandID
        //            , _StartRow -1
        //            , PAGE_SIZE);
        //    }
        //    catch (Exception ex)
        //    {
        //        g.ProcessException(ex);
        //        g.Transfer("/Applications/Search/SearchPreferences.aspx");
        //    }

        //    // RegionID may sometimes be null
        //    string regionID = g.SearchPreferences["RegionID"];
        //    if (regionID == null)
        //    {
        //        regionID = string.Empty;
        //    }

        //    // Store necessary values in session cookie for use in pixels.
        //    _g.Session.Add("PixelGenderMask_SearchPref", g.SearchPreferences["GenderMask"], SessionPropertyLifetime.Persistent);
        //    _g.Session.Add("PixelRegionID_SearchPref", regionID, SessionPropertyLifetime.Persistent);
        //    _g.Session.Add("PixelMinAge_SearchPref", g.SearchPreferences["MinAge"], SessionPropertyLifetime.Persistent);
        //    _g.Session.Add("PixelMaxAge_SearchPref", g.SearchPreferences["MaxAge"], SessionPropertyLifetime.Persistent);

        //    ArrayList members = MemberSA.Instance.GetMembers(searchResults.ToArrayList()
        //        , MemberLoadFlags.None);

        //    if (members != null && members.Count > 0)
        //    {
        //        searchIDs = new List<int>();
        //        for (int i = 0; i < members.Count; i++)
        //        {
        //            searchIDs.Add(((Matchnet.Member.ServiceAdapters.Member)members[i]).MemberID);
        //        }

        //    }
        //    BindMemberList(members);
        //    RenderAnalytics(members, searchResults.MoreResultsAvailable);
        //    _TotalRows = searchResults.MatchesFound;


        //}

        //        private void LoadQuickSearchResultPage()
        //        {
        ////            if( _StartRow==1)
        ////            {showPromotionalProfile();}
        //            g.QuickSearchPreferences["DomainID"] = g.TargetCommunityID.ToString();

        //            MatchnetQueryResults searchResults = null;

        //            if (!PrefsHaveValue(g.QuickSearchPreferences, "SearchTypeID"))
        //            {
        //                g.QuickSearchPreferences["SearchTypeID"] = g.Brand.Site.DefaultSearchTypeID.ToString();	
        //            }

        //            if (!PrefsHaveValue(g.QuickSearchPreferences, "RegionID"))
        //            {
        //                g.QuickSearchPreferences["RegionID"] = g.Brand.Site.DefaultRegionID.ToString();
        //            }

        //            if(!PrefsHaveValue(g.QuickSearchPreferences, "GenderMask"))
        //            {
        //                // set the gender mask to M s M and M s F for all others
        //                if(g.Brand.Site.SiteID == (int) WebConstants.SITE_ID.Glimpse) 
        //                {
        //                    g.QuickSearchPreferences["GenderMask"] = (ConstantsTemp.GENDERID_MALE + ConstantsTemp.GENDERID_SEEKING_MALE).ToString();
        //                } 
        //                else 
        //                {
        //                    g.QuickSearchPreferences["GenderMask"] = (ConstantsTemp.GENDERID_MALE + ConstantsTemp.GENDERID_SEEKING_FEMALE).ToString();
        //                }
        //            }

        //            if(!PrefsHaveValue(g.QuickSearchPreferences, "MinAge"))
        //            {
        //                g.QuickSearchPreferences["MinAge"] = g.Brand.DefaultAgeMin.ToString();
        //            }

        //            if(!PrefsHaveValue(g.QuickSearchPreferences, "MaxAge"))
        //            {
        //                g.QuickSearchPreferences["MaxAge"] = g.Brand.DefaultAgeMax.ToString();
        //            }
        //            if(!PrefsHaveValue(g.QuickSearchPreferences, "Distance"))
        //            {
        //                g.QuickSearchPreferences["Distance"] = g.Brand.DefaultSearchRadius.ToString();
        //            }

        ////            int memberId = (g.Member != null) ? g.Session.GetInt(WebConstants.SESSION_PROPERTY_NAME_MEMBERID) : 0;
        ////		      
        ////            // default several required search preferences for access without cookies
        ////            if(memberId == 0 || (g.Session.GetString(VisitorLimitHelper.SESSIONKEY_VISITORLIMIT_DONOTOVERWRITESEARCHPREFS, String.Empty) == "true")) 
        ////            {
        ////                if (!PrefsHaveValue(g.QuickSearchPreferences, "SearchTypeID"))
        ////                {
        ////                    g.QuickSearchPreferences["SearchTypeID"] = g.Brand.Site.DefaultSearchTypeID.ToString();	
        ////                }
        ////
        ////                if (!PrefsHaveValue(g.QuickSearchPreferences, "RegionID"))
        ////                {
        ////                    g.QuickSearchPreferences["RegionID"] = g.Brand.Site.DefaultRegionID.ToString();
        ////                }
        ////
        ////                if(!PrefsHaveValue(g.QuickSearchPreferences, "GenderMask"))
        ////                {
        ////                    // set the gender mask to M s M and M s F for all others
        ////                    if(g.Brand.Site.SiteID == (int) WebConstants.SITE_ID.Glimpse) 
        ////                    {
        ////                        g.QuickSearchPreferences["GenderMask"] = (ConstantsTemp.GENDERID_MALE + ConstantsTemp.GENDERID_SEEKING_MALE).ToString();
        ////                    } 
        ////                    else 
        ////                    {
        ////                        g.QuickSearchPreferences["GenderMask"] = (ConstantsTemp.GENDERID_MALE + ConstantsTemp.GENDERID_SEEKING_FEMALE).ToString();
        ////                    }
        ////                }
        ////
        ////                if(!PrefsHaveValue(g.QuickSearchPreferences, "MinAge"))
        ////                {
        ////                    g.QuickSearchPreferences["MinAge"] = g.Brand.DefaultAgeMin.ToString();
        ////                }
        ////
        ////                if(!PrefsHaveValue(g.QuickSearchPreferences, "MaxAge"))
        ////                {
        ////                    g.QuickSearchPreferences["MaxAge"] = g.Brand.DefaultAgeMax.ToString();
        ////                }
        ////                if(!PrefsHaveValue(g.QuickSearchPreferences, "Distance"))
        ////                {
        ////                    g.QuickSearchPreferences["Distance"] = g.Brand.DefaultSearchRadius.ToString();
        ////                }
        ////            }

        //            if (g.QuickSearchPreferences["CountryRegionID"] == string.Empty)
        //            {
        //                g.QuickSearchPreferences.Add("CountryRegionID", g.Brand.Site.DefaultRegionID.ToString());
        //            }

        //            //wlybrand - Redirect to search prefs if the user does not have a valid set of preferences.  This is needed to avoid exceptions from Search.
        ////            try 
        ////            {	// Exception thrown if search prefernce validation fails.
        ////                g.SaveQuickSearchPreferences();
        ////            }
        ////            catch (Exception ex) 
        ////            {
        ////                g.ProcessException(ex);
        ////                g.Transfer("/Applications/QuickSearch/QuickSearch.aspx");
        ////            }

        //            // TT13887 - Redirect to search prefs if the user does not have a valid set of preferences.  This is needed to avoid exceptions from Search.
        //            try
        //            {
        //                searchResults = MemberSearchSA.Instance.Search(g.QuickSearchPreferences
        //                    , g.Brand.Site.Community.CommunityID
        //                    , g.Brand.BrandID
        //                    , _StartRow -1
        //                    , PAGE_SIZE);
        //            }
        //            catch (Exception ex)
        //            {
        //                g.ProcessException(ex);
        //                g.Transfer("/Applications/QuickSearch/QuickSearch.aspx");
        //            }

        //            // RegionID may sometimes be null
        //            string regionID = g.QuickSearchPreferences["RegionID"];
        //            if (regionID == null)
        //            {
        //                regionID = string.Empty;
        //            }

        //            // Store necessary values in session cookie for use in pixels.
        ////            _g.Session.Add("PixelGenderMask_SearchPref", g.QuickSearchPreferences["GenderMask"], SessionPropertyLifetime.Persistent);
        ////            _g.Session.Add("PixelRegionID_SearchPref", regionID, SessionPropertyLifetime.Persistent);
        ////            _g.Session.Add("PixelMinAge_SearchPref", g.QuickSearchPreferences["MinAge"], SessionPropertyLifetime.Persistent);
        ////            _g.Session.Add("PixelMaxAge_SearchPref", g.QuickSearchPreferences["MaxAge"], SessionPropertyLifetime.Persistent);

        //            ArrayList members = MemberSA.Instance.GetMembers(searchResults.ToArrayList()
        //                , MemberLoadFlags.None);

        //            BindMemberList(members);
        ////            renderAnalytics(members, searchResults.MoreResultsAvailable);
        //            _TotalRows = searchResults.MatchesFound;
        //        }


        //private bool PrefsHaveValue(SearchPreferenceCollection prefs, string key)
        //{
        //    bool retVal = false;
        //    string myValue = prefs.Value(key);
        //    if(myValue != null && myValue != String.Empty && Matchnet.Conversion.CInt(myValue) != Constants.NULL_INT)
        //    {
        //        retVal = true;
        //    }

        //    return(retVal);
        //}

        //private void LoadHotListPage()
        //{

        //    switch (_hotListCategory)
        //    {
        //        case HotListCategory.WhoTeasedYou:
        //        case HotListCategory.WhoEmailedYou:
        //            if (g.Brand.Site.Community.CommunityID != (int) WebConstants.COMMUNITY_ID.Cupid &&
        //                g.Brand.Site.SiteID != (int) WebConstants.SITE_ID.JDateCoIL )
        //            {	// Issue #13777: Added back the condition for JdateCoIL from this section of code.
        //                if (!g.ValidatePageView())
        //                {	// Subscription required to view this hot list.

        //                    //g.Transfer(FrameworkGlobals.GetSubscriptionLink(false));

        //                    int iHotlistCategoryID = 0;

        //                    try
        //                    {
        //                        iHotlistCategoryID  = Convert.ToInt32(Request["CategoryID"]);
        //                    }
        //                    catch(Exception ex)
        //                    {

        //                    }

        //                    int prtid = Constants.NULL_INT;

        //                    switch (iHotlistCategoryID)
        //                    {
        //                        case (int) HotListCategory.WhoEmailedYou:
        //                            prtid = (int) Constants.PRTID.HotlistWhoseEmailedYou;
        //                            break;
        //                        case (int) Constants.HotlistCatID.FlirtedWithYou:
        //                            prtid = (int) Constants.PRTID.HotlistWhoFlirtedWithYou;
        //                            break;
        //                    }


        //                    Redirect.Subscription(g.Brand, prtid, g.Session.GetInt(WebConstants.SESSION_PROPERTY_NAME_MEMBERID), false, Server.UrlEncode(Context.Items["FullURL"].ToString()));



        //                }
        //            }
        //            break;
        //    }

        //    if(g.Session["GalleryView"] != null && g.Session["GalleryView"].ToString() == "true")
        //    {
        //        GalleryView = "true";
        //    }

        //    RenderHotListElements();
        //    BindMemberRepeater(dt, targetMemberIDs);
        //}

        //#region Load PhotoGallery ResultPage
        //public void LoadPhotoGalleryResultPage()
        //{

        //    try
        //    {
        //        PhotoSearch.ValueObjects.PhotoGalleryQuery query=null;
        //        query=Matchnet.Web.Applications.PhotoGallery.PhotoGalleryUtils.GetQueryFromSession(g);
        //        if(query==null)
        //        {return;}
        //        _TotalRows=0;
        //        int page=_StartRow/PageSize + 1;
        //        ArrayList res= PhotoGallerySA.Instance.Search(query,page,PAGE_SIZE, out _TotalRows);
        //        PhotoGallerySA.Instance.GetResultMembers(res,g.Brand.Site.Community.CommunityID,g.Brand.Site.SiteID,g.Brand.BrandID);

        //        memberNum=res.Count;
        //        if(memberNum > 0)
        //        {
        //            GalleryMemberRepeater.Visible = false;
        //            MemberRepeater.Visible = false;
        //            PhotoGalleryRepeater.Visible=true;
        //            PhotoGalleryRepeater.DataSource=res;
        //            PhotoGalleryRepeater.DataBind();
        //        }
        //        else
        //        {
        //            NoSearchResults.DisplayContext = Matchnet.Web.Framework.Ui.BasicElements.NoResults.ContextType.PhotoGallery;
        //            NoSearchResults.Visible = true;
        //        }

        //        query.ReloadFlag=PhotoSearch.ValueObjects.ReLoadFlags.load;
        //        Matchnet.Web.Applications.PhotoGallery.PhotoGalleryUtils.SaveQueryToSession(query,g);

        //    }
        //    catch (Exception ex)
        //    {
        //        g.ProcessException(ex);

        //    }
        //    #endregion
        //			
        //			// RegionID may sometimes be null
        //			string regionID = g.SearchPreferences["RegionID"];
        //			if (regionID == null)
        //			{
        //				regionID = string.Empty;
        //			}
        //
        //			// Store necessary values in session cookie for use in pixels.
        //			_g.Session.Add("PixelGenderMask_SearchPref", g.SearchPreferences["GenderMask"], SessionPropertyLifetime.Persistent);
        //			_g.Session.Add("PixelRegionID_SearchPref", regionID, SessionPropertyLifetime.Persistent);
        //			_g.Session.Add("PixelMinAge_SearchPref", g.SearchPreferences["MinAge"], SessionPropertyLifetime.Persistent);
        //			_g.Session.Add("PixelMaxAge_SearchPref", g.SearchPreferences["MaxAge"], SessionPropertyLifetime.Persistent);
        //			
        //			ArrayList members = MemberSA.Instance.GetMembers(searchResults.ToArrayList()
        //				, MemberLoadFlags.None);
        //
        //			BindMemberList(members);
        //			renderAnalytics(members, searchResults.MoreResultsAvailable);
        //			_TotalRows = searchResults.MatchesFound;
        //		}


        #region Hot List specific methods
        //private void RenderHotListElements()
        //{	
        //    int communityID=g.Brand.Site.Community.CommunityID;
        //    int siteID=g.Brand.Site.SiteID;



        //    targetMemberIDs = g.List.GetListMembers(_hotListCategory,
        //        communityID,siteID,
        //        _StartRow,
        //        PAGE_SIZE,
        //        out _TotalRows);

        //}
        #endregion

        # region Bind member repeater

        //private void BindMemberList(ArrayList members)
        //{
        //    memberNum = members.Count;
        //    if(memberNum != 0)
        //    {
        //        if(GalleryView == "true")
        //        {
        //            GalleryMemberRepeater.DataSource = members;
        //            GalleryMemberRepeater.DataBind();
        //            GalleryMemberRepeater.Visible = true;
        //            MemberRepeater.Visible = false;
        //        }
        //        else
        //        {
        //            MemberRepeater.DataSource = members;
        //            MemberRepeater.DataBind();
        //            MemberRepeater.Visible = true;
        //            GalleryMemberRepeater.Visible = false;
        //        }
        //    }
        //    else
        //    {
        //        MemberRepeater.Visible = false;
        //        GalleryMemberRepeater.Visible = false;

        //        NoSearchResults.Visible = true;
        //        if(ResultListContextType == ResultContextType.MembersOnline)
        //        {
        //            NoSearchResults.DisplayContext = Matchnet.Web.Framework.Ui.BasicElements.NoResults.ContextType.MembersOnline;
        //            NoSearchResults.Visible = false;
        //            this.phMembersOnlineNoResults.Visible = true;
        //        }
        //        else if(ResultListContextType == ResultContextType.QuickSearchResult)
        //        {
        //            NoSearchResults.DisplayContext = Matchnet.Web.Framework.Ui.BasicElements.NoResults.ContextType.QuickSearch;
        //            this.phMembersOnlineNoResults.Visible = false;
        //        }
        //    }
        //}

        //private void BindMemberRepeater(DataTable dt, ArrayList targetMemberIDs)
        //{
        //    ArrayList members = MemberSA.Instance.GetMembers(targetMemberIDs,
        //        MemberLoadFlags.None);

        //    if(GalleryView == "true")
        //    {
        //        memberNum = members.Count;
        //        GalleryMemberRepeater.DataSource = members;
        //        GalleryMemberRepeater.DataBind();
        //        GalleryMemberRepeater.Visible = true;
        //        MemberRepeater.Visible = false;
        //    }
        //    else
        //    {
        //        MemberRepeater.DataSource = members;
        //        MemberRepeater.DataBind();
        //        MemberRepeater.Visible = true;
        //        GalleryMemberRepeater.Visible = false;
        //    }

        //    if(members.Count == 0)
        //    {
        //        NoUsersText.Visible = true;
        //        NoUsersText.ExpandImageTokens = true;

        //        if(ResultListContextType == ResultContextType.HotList)
        //        {
        //            switch( this.HotListCategory )
        //            {
        //                case HotListCategory.MutualYes : // corresponds to the You Clicked! hotlist
        //                    NoUsersText.ResourceConstant = "TXT_NO_MEMBERS_IN_YOU_CLICKED_LIST";
        //                    break;
        //                case HotListCategory.MembersYouEmailed :
        //                    NoUsersText.ResourceConstant = "TXT_NO_MEMBERS_IN_YOU_EMAILED_LIST";
        //                    break;
        //                case HotListCategory.Default : // need Members you Hotlisted
        //                    NoUsersText.ResourceConstant = "TXT_NO_MEMBERS_IN_YOU_HOTLISTED_LIST";
        //                    break;
        //                case HotListCategory.MembersYouTeased : // i think this is same as Flirt
        //                    NoUsersText.ResourceConstant = "TXT_NO_MEMBERS_IN_YOU_FLIRTED_LIST";
        //                    break;
        //                case HotListCategory.MembersYouIMed :
        //                    NoUsersText.ResourceConstant = "TXT_NO_MEMBERS_IN_YOU_IMED_LIST";
        //                    break;
        //                case HotListCategory.MembersYouViewed :
        //                    NoUsersText.ResourceConstant = "TXT_NO_MEMBERS_IN_YOU_VIEWED_LIST";
        //                    break;
        //                case HotListCategory.IgnoreList :  // i think this is same as Blocked
        //                    NoUsersText.ResourceConstant = "TXT_NO_MEMBERS_IN_YOU_BLOCKED_LIST";
        //                    break;
        //                case HotListCategory.WhoAddedYouToTheirFavorites :
        //                    NoUsersText.ResourceConstant = "TXT_NO_MEMBERS_IN_HOTLISTED_YOU_LIST";
        //                    break;
        //                case HotListCategory.WhoTeasedYou :
        //                    NoUsersText.ResourceConstant = "TXT_NO_MEMBERS_IN_FLIRTED_YOU_LIST";
        //                    break;
        //                case HotListCategory.WhoIMedYou :
        //                    NoUsersText.ResourceConstant = "TXT_NO_MEMBERS_IN_IMED_YOU_LIST";
        //                    break;
        //                case HotListCategory.WhoEmailedYou :
        //                    NoUsersText.ResourceConstant = "TXT_NO_MEMBERS_IN_EMAILED_YOU_LIST";
        //                    break;
        //                case HotListCategory.WhoViewedYourProfile :
        //                    NoUsersText.ResourceConstant = "TXT_NO_MEMBERS_IN_VIEWED_YOU_LIST";
        //                    break;					
        //                case HotListCategory.YourMatches :
        //                    NoUsersText.ResourceConstant = "TXT_NO_MEMBERS_IN_MATCHES_SENT_BY_EMAIL";
        //                    break;
        //                case HotListCategory.MembersYouSentECards :
        //                    NoUsersText.ResourceConstant = "TXT_NO_MEMBERS_IN_YOU_ECARDED_LIST";
        //                    break;
        //                case HotListCategory.WhoSentYouECards :
        //                    NoUsersText.ResourceConstant = "TXT_NO_MEMBERS_SENT_YOU_ECARDS_LIST";
        //                    break;
        //                default: // if its not one of the Above then its a custom list
        //                    NoUsersText.ResourceConstant = "TXT_NO_MEMBERS_IN_CUSTOM_LIST";//"YOU_DO_NOT_HAVE_ANY_MEMBERS_IN_THIS_LIST";
        //                    break;
        //            }

        //        }
        //    }
        //}
        # endregion

        //# region Render Paging
        //private void RenderPaging()
        //{
        //    if(Request["TotalRows"] != null)
        //    {
        //        _TotalRows = Convert.ToInt32(Request["TotalRows"].ToString());
        //    }

        //    // if the start row is greater than the totalrows, set the startrow to the nearest round page_size less than totalrows
        //    if(_StartRow > _TotalRows)
        //    {
        //        _StartRow = (_TotalRows / PAGE_SIZE) * PAGE_SIZE + 1;
        //    }

        //    // define needed vars
        //    int pageCount = ((_TotalRows - 1) / PAGE_SIZE) + 1;
        //    int pageRequested = _StartRow / PAGE_SIZE + 1;

        //    int chapter;
        //    int pageCurrent = 0;
        //    int extraRows = _TotalRows - (CHAPTER_SIZE * PAGE_SIZE);
        //    int extraPagesNeeded = Convert.ToInt32(Math.Ceiling((double)extraRows / (double)PAGE_SIZE));

        //    // if the requested page is greater than the count, set the requested page to the count
        //    if(pageRequested > pageCount)
        //    {
        //        pageRequested = pageCount;
        //    }

        //    // set the chapter
        //    if(pageRequested > CHAPTER_SIZE)
        //    {
        //        chapter = (_StartRow / PAGE_SIZE / (CHAPTER_SIZE + extraPagesNeeded)) + 1;
        //    }
        //    else
        //    {			
        //        chapter = (_StartRow / PAGE_SIZE / CHAPTER_SIZE) + 1;
        //    }

        //    // set the start page depending on the startrow
        //    int startSize = 0;
        //    int maxChapter = CHAPTER_SIZE;
        //    if(_StartRow > PAGE_SIZE * 2)
        //    {
        //        startSize = (_StartRow - 1) / PAGE_SIZE;

        //        // if the start size is within the last two page elements of all of the pages,
        //        // decrement the start size.  This means that always three page links
        //        // will be shown at the end of the list.
        //        if((startSize > CHAPTER_SIZE && pageCount - startSize < CHAPTER_SIZE - 1) || pageCount == CHAPTER_SIZE)
        //        {
        //            startSize--;
        //        }

        //        maxChapter = ((_StartRow - 1) / PAGE_SIZE) + (CHAPTER_SIZE - 1);
        //    }
        //    else
        //    {
        //        startSize = 1;
        //    }

        //    // Set Visitor Limits.
        //    VisitorLimitHelper.CheckVisitorLimit(ref _g, ResultListContextType, pageRequested, _StartRow);

        //    // there must be more rows than the page size to show paging
        //    if(_TotalRows > PAGE_SIZE)
        //    {
        //        //AddBreadCrumbPaging(pageRequested, pageCount);

        //        // Add links for the ListNavigation (see SearchResults.ascx, MembersOnline.ascx, and Hotlist.View.ascx) if necessary.
        //        AddListNavigation(pageRequested, pageCount, pageCurrent, startSize, chapter, maxChapter);
        //    }
        //}

        //// define a spacer for between chapters
        //private const string CHAPTER_SPACER = "&nbsp;/&nbsp;";

        //private void AddListNavigation(int pageRequested, int pageCount, int pageCurrent, int startSize, int chapter, int maxChapter)
        //{
        //    if (g.ListNavigationTop != null && g.ListNavigationBottom != null)
        //    {
        //        // show back to start pipe and arrow if necessary.
        //        if(pageCount > CHAPTER_SIZE && _StartRow >= PAGE_SIZE * (CHAPTER_SIZE - 1))
        //        {
        //            g.ListNavigationTop.Controls.Add(CreateArrowLink(1, "breadCrumbPagesLink", "start"));
        //            g.ListNavigationBottom.Controls.Add(CreateArrowLink(1, String.Empty, "start"));			

        //            g.ListNavigationTop.Controls.Add(new LiteralControl(CHAPTER_SPACER));
        //            g.ListNavigationBottom.Controls.Add(new LiteralControl(CHAPTER_SPACER));
        //        }

        //        // Do not show a previous link if we are on the first page.
        //        if (pageRequested > 1)
        //        {
        //            g.ListNavigationTop.Controls.Add(CreateListArrowLink(pageRequested, String.Empty, false));
        //            g.ListNavigationTop.Controls.Add(new LiteralControl(CHAPTER_SPACER));
        //            g.ListNavigationBottom.Controls.Add(CreateListArrowLink(pageRequested, String.Empty, false));
        //            g.ListNavigationBottom.Controls.Add(new LiteralControl(CHAPTER_SPACER));
        //        }

        //        // display each page
        //        for (int page = startSize; (page <= maxChapter && page <= pageCount); page++)
        //        {
        //            pageCurrent = page * chapter;

        //            bool isCurrentPage = (page == pageRequested);

        //            g.ListNavigationTop.Controls.Add(CreatePagingLink(pageCurrent, "breadCrumbPagesLink", isCurrentPage));
        //            g.ListNavigationBottom.Controls.Add(CreatePagingLink(pageCurrent, String.Empty, isCurrentPage));

        //            // display the character spacer in between page links (but not at the end)
        //            if((page < maxChapter) && (pageCurrent != pageCount))
        //            {
        //                g.ListNavigationTop.Controls.Add(new LiteralControl(CHAPTER_SPACER));
        //                g.ListNavigationBottom.Controls.Add(new LiteralControl(CHAPTER_SPACER));
        //            }
        //        }

        //        // show end arrows if necessary.
        //        if(pageCount > CHAPTER_SIZE && ((_TotalRows - _StartRow) > (PAGE_SIZE * 2)))
        //        {
        //            // Do not show a next link if we are on the last page.
        //            if (pageRequested < pageCount)
        //            {
        //                g.ListNavigationTop.Controls.Add(new LiteralControl(CHAPTER_SPACER));
        //                g.ListNavigationBottom.Controls.Add(new LiteralControl(CHAPTER_SPACER));

        //                g.ListNavigationTop.Controls.Add(CreateListArrowLink(pageRequested, String.Empty, true));
        //                g.ListNavigationBottom.Controls.Add(CreateListArrowLink(pageRequested, String.Empty, true));
        //            }	
        //        }
        //    }
        //}

        //private string GetPageText(int pageCurrent, bool showBold)
        //{
        //    int start = (pageCurrent * PAGE_SIZE) - PAGE_SIZE + 1;
        //    int end = start + PAGE_SIZE - 1;

        //    // use the total rows instead of the normal last number if it is the last number
        //    if(_TotalRows < end && _TotalRows >= start)
        //    {
        //        end = _TotalRows;
        //    }

        //    if(showBold)
        //    {
        //        return "<strong>" + start.ToString() + "-" + end.ToString() + "</strong>";
        //    }
        //    else
        //    {
        //        return start.ToString() + "-" + end.ToString();
        //    }
        //}

        //private HyperLink CreateArrowLink(int nextPage, string cssClass, string direction)
        //{
        //    int pageToLink;
        //    HyperLink link = new HyperLink();
        //    if(cssClass != String.Empty)
        //    {
        //        link.CssClass = cssClass;
        //    }
        //    if(direction == "next")
        //    {
        //        link.Text = "&gt;&gt;";
        //        pageToLink = (nextPage - 1) * PAGE_SIZE + 1;
        //    }
        //    else if(direction == "end")
        //    {
        //        link.Text = "&gt;|";
        //        pageToLink = (nextPage - 1) * PAGE_SIZE + 1;
        //    }
        //    else if(direction == "start")
        //    {
        //        link.Text = "|&lt;";
        //        pageToLink = (nextPage - 1) * PAGE_SIZE + 1;
        //    }
        //    else
        //    {
        //        link.Text = "&lt;&lt;";
        //        pageToLink = nextPage;
        //    }

        //    link.NavigateUrl = BuildPagingURL(pageToLink);
        //    return link;
        //}

        ///// <summary>
        ///// This is used to create the Next and Previous arrows.
        ///// </summary>
        ///// <param name="page"></param>
        ///// <param name="cssClass"></param>
        ///// <param name="isNext"></param>
        ///// <returns></returns>
        //private HyperLink CreateListArrowLink(int page, string cssClass, bool isNext)
        //{
        //    int pageToLink;
        //    HyperLink link = new HyperLink();
        //    if (cssClass != String.Empty)
        //    {
        //        link.CssClass = cssClass;
        //    }

        //    if (isNext)
        //    {
        //        link.Text = g.GetResource("TXT_NEXT");
        //        pageToLink = page * PAGE_SIZE + 1;
        //    }
        //    else
        //    {
        //        link.Text = g.GetResource("TXT_PREVIOUS");
        //        pageToLink = (page - 2) * PAGE_SIZE + 1;
        //    }

        //    link.NavigateUrl = BuildPagingURL(pageToLink); 
        //    return link;
        //}

        //private HyperLink CreatePagingLink(int pageCurrent, string cssClass, bool showBold)
        //{
        //    int pageToLink = (pageCurrent - 1) * PAGE_SIZE + 1;
        //    HyperLink link = new HyperLink();
        //    if(cssClass != String.Empty)
        //    {
        //        link.CssClass = cssClass;
        //    }
        //    link.Text = GetPageText(pageCurrent, showBold);
        //    link.NavigateUrl = BuildPagingURL(pageToLink);
        //    return link;
        //}
        //#endregion

        #endregion


    }
}
