﻿<%@ Control Language="c#" AutoEventWireup="false" CodeBehind="ResultList.ascx.cs"
    Inherits="Matchnet.Web.Framework.Ui.BasicElements.ResultList" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn" TagName="GalleryMiniProfile" Src="GalleryMiniProfile.ascx" %>
<%@ Register TagPrefix="mn" TagName="miniProfile" Src="MiniProfile.ascx" %>
<%@ Register TagPrefix="mn" TagName="PhotoGalleryProfile" Src="../../../Applications/PhotoGallery/Controls/PhotoGalleryProfile.ascx" %>
<%@ Register TagPrefix="mn" TagName="noResults" Src="NoResults.ascx" %>
<%@ Register TagPrefix="mn" TagName="MemberMap" Src="Maps/MemberMap.ascx" %>
<input id="hidStartRow" type="hidden" name="hidStartRow" runat="server">
<asp:PlaceHolder ID="plcPromotionalProfile" runat="server" Visible="False">
    <mn:miniProfile runat="server" ID="promotionProfile" Visible="true" />
    <br />
</asp:PlaceHolder>
<!-- begin dynamic member loop -->
<asp:Repeater ID="memberRepeater" runat="server" Visible="True" OnItemDataBound="MemberRepeater_OnItemDataBound">
    <ItemTemplate>
        <mn:miniProfile runat="server" ID="MiniProfile" Member="<%# Container.DataItem %>" />
        <p>
        </p>
    </ItemTemplate>
</asp:Repeater>
<asp:Repeater ID="galleryMemberRepeater" runat="server" Visible="False" OnItemDataBound="GalleryMemberRepeater_OnItemDataBound">
    <ItemTemplate>
        <asp:Literal runat="server" ID="rptrNewRow" Visible="False">
<!-- new row -->			
				<table border="0" cellpadding="3" cellspacing="0" width="592">
					<tr>
        </asp:Literal>
        <asp:Literal runat="server" ID="rptrNewCell" Visible="False">
<!-- new cell -->
						<td align="left" valign="top" width="33%">
        </asp:Literal>
        <!-- profile -->
        <mn:GalleryMiniProfile runat="server" ID="GalleryMiniProfile" Member="<%# Container.DataItem %>" />
        <!-- endprofile -->
        <asp:Literal runat="server" ID="rptrEndCell" Visible="False">
<!-- end cell -->
						</td>
        </asp:Literal>
        <asp:Literal runat="server" ID="rptrEndRow" Visible="False">
					</tr>
				</table>
				<p></p>
<!-- end row -->			
        </asp:Literal>
        <asp:Literal runat="server" ID="rptrEndOneCell" Visible="False">
						<td align="left" valign="top" width="33%">
						</td>
					</tr>
				</table>
				<p></p>
<!-- end one cell and row -->			
        </asp:Literal>
        <asp:Literal runat="server" ID="rptrEndTwoCells" Visible="False">
						<td align="left" valign="top" width="33%">
						</td>
						<td align="left" valign="top" width="33%">
						</td>
					</tr>
				</table>
				<p></p>
<!-- end two cells and row -->			
        </asp:Literal>
    </ItemTemplate>
</asp:Repeater>
<asp:Repeater ID="photoGalleryRepeater" runat="server" Visible="False" OnItemDataBound="PhotoGalleryRepeater_OnItemDataBound">
    <ItemTemplate>
        <asp:Literal runat="server" ID="rptrNewRow" Visible="False">
<!-- new row -->			
				<table border="0" cellpadding="3" cellspacing="0" width="592">
					<tr>
        </asp:Literal>
        <asp:Literal runat="server" ID="rptrNewCell" Visible="False">
<!-- new cell -->
						<td align="left" valign="top" width="25%">
        </asp:Literal>
        <!-- profile -->
        <mn:PhotoGalleryProfile runat="server" ID="PhotoGalleryProfile" EwsultMember="<%# Container.DataItem %>" />
        <!-- endprofile -->
        <asp:Literal runat="server" ID="rptrEndCell" Visible="False">
<!-- end cell -->
						</td>
        </asp:Literal>
        <asp:Literal runat="server" ID="rptrEndRow" Visible="False">
					</tr>
				</table>
				<p></p>
<!-- end row -->			
        </asp:Literal>
        <asp:Literal runat="server" ID="rptrEndOneCell" Visible="False">
						<td align="left" valign="top" width="25%">
						</td>
					</tr>
				</table>
				<p></p>
<!-- end one cell and row -->			
        </asp:Literal>
        <asp:Literal runat="server" ID="rptrEndTwoCells" Visible="False">
						<td align="left" valign="top" width="25%">
						</td>
						<td align="left" valign="top" width="25%">
						</td>
					</tr>
				</table>
				<p></p>
<!-- end two cells and row -->			
        </asp:Literal>
        <asp:Literal runat="server" ID="rptrEndThreeCells" Visible="False">
						<td align="left" valign="top" width="25%">
						</td>
						<td align="left" valign="top" width="25%">
						</td>
						<td align="left" valign="top" width="25%">
						</td>
					</tr>
				</table>
				<p></p>
<!-- end two cells and row -->			
        </asp:Literal>
    </ItemTemplate>
</asp:Repeater>
<!-- end dynamic member loop -->
<mn:noResults runat="server" ID="noSearchResults" Visible="False" />
<asp:PlaceHolder ID="phMembersOnlineNoResults" runat="server" Visible="False">
    <mn:Txt runat="server" ExpandImageTokens="true" ID="txtMOL" ResourceConstant="NO_MEMBERS_ONLINE_FOUND_MATCHING_YOUR_SEARCH_CRITERIA__519973" />
</asp:PlaceHolder>
<!--<input 
type=hidden value="" name=ChangeVoteMessage>-->
<iframe tabindex="-1" name="FrameYNMVote" frameborder="0" width="1" scrolling="no"
    height="1">
    <layer name="FrameYNMVote" frameborder="0" scrolling="no" width="1" height="1"></layer>
</iframe>
<mn:Txt ID="noUsersText" Visible="false" runat="server"></mn:Txt>
