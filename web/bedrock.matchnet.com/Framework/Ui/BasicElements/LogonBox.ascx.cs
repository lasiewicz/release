namespace Matchnet.Web.Framework.Ui.BasicElements
{
	using System;

	/// <summary>
	///		Summary description for LogonBox.
	/// </summary>
	public class LogonBox : FrameworkControl
	{
		protected System.Web.UI.WebControls.HyperLink changeStatusLink;
		protected Matchnet.Web.Framework.Txt newStatusText;
		protected LogonResourceType _logonResource;

		public string CssClass;

		public enum LogonResourceType
		{
			Bracketed, Unbracketed
		}

		public LogonResourceType LogonResource
		{
			get { return(_logonResource); }
			set { _logonResource = value; }
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			try
			{
				if ( CssClass != null )
				{
					changeStatusLink.CssClass = CssClass;
				}

				if ( g.Member != null )
				{
					changeStatusLink.NavigateUrl = FrameworkGlobals.LinkHref("/Applications/Logon/Logoff.aspx", true);
					// newStatusText.Resource = Translator.Resources.LOGOUT;
					newStatusText.ResourceConstant = "LOGOUT";
				}
				else
				{
					changeStatusLink.NavigateUrl = FrameworkGlobals.LinkHref("/Applications/Logon/Logon.aspx", true);
					// newStatusText.Resource = Translator.Resources.NAVLOGIN;
					if(LogonResource == LogonResourceType.Unbracketed)
					{
						newStatusText.ResourceConstant = "TXT_LOWERCASE_LOGIN";
					}
					else
					{
						newStatusText.ResourceConstant = "NAVLOGIN";
					}
				}
			}
			catch (Exception ex)
			{
				g.ProcessException(ex);
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
	}
}
