﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MatchMeterInfoDisplay.ascx.cs"
    Inherits="Matchnet.Web.Framework.Ui.BasicElements.MatchMeterInfoDisplay" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="uc1" TagName="MatchMeterScoreBar" Src="~/Applications/CompatibilityMeter/Controls/MatchMeterScoreBar.ascx" %>
<div class="image-text-pair" runat="server" id="divLogoInfoPopup">
    <asp:HyperLink ID="lnkMatchMeter" Visible="true" runat="server" CssClass="lnkMatchMeter">
        <asp:PlaceHolder ID="plcMatchMeterLogo" runat="server">
            <mn:Image runat="server" ID="imgJmeter" FileName="jmeter_logo_galleryView.gif" Style="vertical-align: top;" />
            <span>
            <mn:Txt runat="server" ID="txtJmeter" ResourceConstant="TXT_JMETER" Visible="false" />
            </span>
        </asp:PlaceHolder>
        <asp:PlaceHolder ID="plcMathMeterScore" runat="server" Visible="false">
            <div runat="server" id="divScoreBar">
                <uc1:MatchMeterScoreBar ID="MatchMeterScoreBar" runat="server" DisplayHearts="false" />
                <mn:Txt ID="txtViewMatch" runat="server" ResourceConstant="TXT_VIEW_MATCH" TitleResourceConstant="TTL_VIEW_MATCH" />
            </div>
        </asp:PlaceHolder>
    </asp:HyperLink>
</div>
