﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GalleryMiniProfile20.ascx.cs"
    Inherits="Matchnet.Web.Framework.Ui.BasicElements.GalleryMiniProfile20" %>
<%@ Register TagPrefix="mn" TagName="YNMVoteBarSmall" Src="../YNMVoteBarSmall.ascx" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mnl" Namespace="Matchnet.Web.Framework.Ui.BasicElements.Links"
    Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="uc1" TagName="NoPhoto" Src="../PageElements/NoPhoto20.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Add2List" Src="Add2List.ascx" %>
<%@ Register TagPrefix="uc2" TagName="HighlightProfileInfoDisplay" Src="HighlightInfoDisplay20.ascx" %>
<%@ Register TagPrefix="uc2" TagName="MatchMeterInfoDisplay" Src="MatchMeterInfoDisplay.ascx" %>
<%@ Register TagPrefix="uc2" TagName="MiniProfileHotListBar" Src="MiniProfileHotListBar.ascx" %>
<%@ Register TagPrefix="uc3" TagName="OmnidateInvitationButton" Src="~/Applications/Omnidate/Controls/OmnidateInvitationButton.ascx" %>
<asp:PlaceHolder runat="server" ID="plcHotListMove" Visible="False">
    <div class="profileButtonAlign2">
        <div class="showSelectedDiv" id="hotlistShow<%= Counter.ToString() %>" style="margin-bottom: 5px;
            width: 188px">
            <input id="chkSelected" type="checkbox" name="chkSelected" runat="server"><span class="showSelect"
                id="hotlistSelectText<%= Counter.ToString() %>" />&nbsp;
            <mn:Txt ID="TxtSelectThisProfile" runat="server" ResourceConstant="TXT_SELECT_THIS_PROFILE">
            </mn:Txt>
            </span><span class="hideSelect" id="hotlistDeselectText<%= Counter.ToString() %>">
                <mn:Txt ID="TxtDeselectThisProfile" runat="server" ResourceConstant="TXT_DESELECT_THIS_PROFILE">
                </mn:Txt>
            </span>
        </div>
    </div>
</asp:PlaceHolder>
<div class="results gallery-view" id="boxContainerGallery" runat="server">
    <div class="wrapper wrapper-mingle">
        <div class="header clearfix">
            <h2>
                <mnl:Link runat="server" ID="lnkUserName" TitleResourceConstant="TTL_VIEW_MY_PROFILE" />&nbsp;<mn:Image
                    ID="imgIsNew" runat="server" FileName="icon-new-member.gif" Visible="false" />&nbsp;<mn:Image
                        ID="imgIsUpdate" runat="server" FileName="icon-updated.gif" Visible="false" /></h2>
            <div class="outside">
                <uc2:HighlightProfileInfoDisplay runat="server" ID="ucHighlightProfileInfoDisplay"
                    ShowImageNoText="true" />
            </div>
            <div class="jmeterOnHeader">
                <uc2:MatchMeterInfoDisplay runat="server" ID="ucMatchMeterInfoDisplay" DispalyType="2" />
            </div>
        </div>
        <div class="info">
            <div class="history" runat="server" id="divHistory">
                <div class="actions-email">
                    <asp:PlaceHolder runat="server" ID="PlaceHolderEmailMe">
                        <asp:HyperLink runat="server" ID="lnkEmail">
                            <mn:Image Border="0" FileName="btn-email.gif" ResourceConstant="ALT_EMAIL_ME" runat="server"
                                ID="imgEmail" />
                        </asp:HyperLink>
                    </asp:PlaceHolder>
                    <mn:Image Border="0" FileName="btn-email.gif" ResourceConstant="ALT_EMAIL_ME" runat="server"
                        CssClass="open-quick-message" ID="imgBtnQuickMessage" Visible="false" />
                    <asp:PlaceHolder runat="server" ID="PlaceHolderViewProfile">
                        <asp:HyperLink runat="server" ID="lnkViewProfile">
                            <mn:Image Border="0" FileName="btn-profile.gif" ResourceConstant="ALT_VIEW_MEMBER"
                                runat="server" ID="ImageViewProfile" />
                        </asp:HyperLink>
                    </asp:PlaceHolder>
                </div>
                <mn:Image ID="imgPremiumAuthenticated" runat="server" FileName="icon_premiumAuthenticated.gif"
                    Visible="false" TitleResourceConstant="ALT_PREMIUM_AUTHENTICATED" ResourceConstant="ALT_PREMIUM_AUTHENTICATED" />
                <asp:PlaceHolder ID="plcHotListBar" runat="server">
                    <uc2:MiniProfileHotListBar ID="HotListBar" runat="server" FriendIconFileName="icon-members-hotlisted-{0}.gif"
                        ContactedIconFileName="icon-members-emailed-{0}.gif" TeaseIconImageFileName="icon-members-flirted-{0}.gif"
                        IMIconFileName="icon-members-IM-{0}.gif" ViewProfileIconFileName="icon-members-viewed-{0}.gif"
                        OmnidateIconFileName="icon-members-omnidated-{0}.gif" />
                </asp:PlaceHolder>
            </div>
            <asp:PlaceHolder runat="server" ID="plcQuickMessage" Visible="false">
                <div class="quick-message" <%=GetMemberIDAttr() %>>
                    <div class="subject">
                        <asp:TextBox runat="server" ID="txtQuickMessageSubject" Text="נושא ההודעה" Columns="37" MaxLength="50"
                            CssClass="quick-message-subject" /></div>
                    <div class="body">
                        <asp:TextBox runat="server" ID="txtQuickMessageBody" Text="כתבו לי הודעה" Rows="2"
                            TextMode="MultiLine" CssClass="quick-message-body" Columns="27" /></div>
                    <div class="close">
                        <mn:Txt ID="Txt1" runat="server" ResourceConstant="CLOSE_BTN_TXT" />
                    </div>
                    <div class="send-message">
                        <mn:Txt ID="Txt2" runat="server" ResourceConstant="SEND_MESSAGE_TXT" />
                    </div>
                </div>
                <div class="send-message-loading" <%=GetMemberIDAttr() %>>
                    <mn:Image runat="server" FileName="ajax-loader-2.gif" />
                </div>
            </asp:PlaceHolder>
            <div class="overview">
                <p>
                    <mn:Txt ID="ageWordTxt" runat="server" ResourceConstant="TXT_AGE_WORD" />
                    <asp:Literal ID="txtAge" runat="server" />,
                    <asp:Literal ID="LiteralGender" runat="server" />
                    <asp:Literal ID="txtRegion" runat="server" />
                    <mn:Txt runat="server" ID="txtMore" CssClass="view-more" />
                    <mn:Image runat="server" ID="ImageMorePhotos" Visible="false" FileName="btn-more-photos.gif" />
                </p>
                <p>
                    <asp:Literal ID="TextHeadline" runat="server" Visible="false" />
                </p>
            </div>
        </div>
    </div>
    <div class="picture">
        <mn:Image runat="server" ID="imgThumb" Width="80" Height="104" BorderWidth="0" />
        <uc1:NoPhoto runat="server" ID="noPhoto" class="noPhoto" Visible="False"  />
        <asp:PlaceHolder ID="phColorCode" runat="server" Visible="false">
            <div class="cc-pic-tag cc-pic-tag-sm cc-pic-<%=_ColorText.ToLower() %>">
                <span>
                    <%=_ColorText%></span></div>
        </asp:PlaceHolder>
    </div>
    <asp:PlaceHolder runat="server" ID="PlaceHolderAboutMeEssay" Visible="false">
        <div class="excerpt clearfix" runat="server" id="divAboutMeEssay">
            <asp:Literal runat="server" ID="txtAboutMeEssay" />
            <mn:Txt runat="server" ID="txtReadMore" ResourceConstant="MORE" />
        </div>
    </asp:PlaceHolder>
    <div class="communications clearfix" runat="server" id="divCommunications">
        <asp:PlaceHolder runat="server" ID="PlaceHolderMingleSearch">
            <div class="actions">
                <mn:Image ID="imgOnline" runat="server" FileName="icon-chat.gif" Visible="true" ResourceConstant="ONLINE" EncodeResource="false" />
                <uc3:OmnidateInvitationButton runat="server" ID="btnOmnidate" DisplayContext="GalleryMiniProfile" />
                <a id="lnkTeaseMe" runat="server">
                    <mn:Image ID="icon_tease" runat="server" FileName="icon-flirt.gif" ResourceConstant="PRO_TEASE"
                        TitleResourceConstant="ALT_TEASE_ME" />&nbsp; </a>
                <asp:HyperLink runat="server" ID="lnkECard">
                    <mn:Image Border="0" FileName="icon-ecard.gif" ResourceConstant="ALT_ECARD" runat="server"
                        ID="imgECard" />
                </asp:HyperLink>
                <uc1:Add2List ID="add2List" cssClass="hebListMenu" runat="server" IconHotList="icon-hotlist-add.gif"
                    IconHotListRemove="icon-hotlist-remove.gif" />
            </div>
            <asp:PlaceHolder runat="server" ID="plcClick">
                <div class="click">
                    <span class="do-we-click">
                        <mn:Txt runat="server" ID="txtDoWeClick" ResourceConstant="TXT_DOWECLICK" />
                    </span>
                    <!-- MouseOver events are handled in spark.js, to make sure it works keep convention of filenames ending  with off/on -->
                    <span class="no-wrap-class">
                        <mn:YNMVoteBarSmall ID="VoteBar" runat="server" MaybeFileName="icon-click-m-off.gif"
                            MaybeFileNameSelected="icon-click-m-on.gif" NoFileName="icon-click-n-off.gif"
                            NoFileNameSelected="icon-click-n-on.gif" YesFileName="icon-click-y-off.gif" YesFileNameSelected="icon-click-y-on.gif"
                            YesMutualFileName="icon-click-yy.gif" />
                    </span>
                </div>
            </asp:PlaceHolder>
        </asp:PlaceHolder>
    </div>
    <input type="text" name="edit" runat="server" id="txtFavoriteNote" />
</div>
