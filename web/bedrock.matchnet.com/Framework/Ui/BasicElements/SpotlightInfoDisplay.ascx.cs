﻿using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Util;
using Matchnet.Content.ServiceAdapters.Links;

namespace Matchnet.Web.Framework.Ui.BasicElements
{
    /// <summary>
    ///		Summary description for SpotlightInfoDisplay.
    /// </summary>
    public partial class SpotlightInfoDisplay : FrameworkControl
    {
        protected System.Web.UI.WebControls.HyperLink lnkSpotlightInfo;
        protected System.Web.UI.WebControls.Panel pnlSpotlightInfoForMembersWithSubscription;
        protected System.Web.UI.WebControls.Panel pnlSpotlightInfoForMembersWithoutSubscription;
        protected System.Web.UI.WebControls.HyperLink lnkSpotlightInfoSubscribe;

        protected System.Web.UI.WebControls.HyperLink lnkSpotlightInfoForMembersWithSubscriptionCloseText;
        protected System.Web.UI.WebControls.HyperLink lnkSpotlightInfoForMembersWithSubscriptionCloseImage;
        protected System.Web.UI.WebControls.HyperLink lnkSpotlightInfoForMembersWithoutSubscriptionCloseText;
        protected System.Web.UI.WebControls.HyperLink lnkSpotlightInfoForMembersWithoutSubscriptionCloseImage;

        private bool _Spotlight = false;
        private int _viewedMemberID = Constants.NULL_INT;
        private bool _showImageNoText = false;

        private void Page_Load(object sender, System.EventArgs e)
        {
          
        }


        protected override void OnPreRender(EventArgs e)
        {
            try
            {
                
                if (this._Spotlight)
                {
                    // Show Learn about Highlighted Profiles only for profiles that are highlighted
                    System.Text.StringBuilder strShowPopup = new System.Text.StringBuilder();
					bool showDifferentInfoDisplayToSubscriber = false;
                    if (MemberPrivilegeAttr.IsCureentSubscribedMember(g) && showDifferentInfoDisplayToSubscriber)
                    {
                        // Viewing member that is actively subscribed see feature information
                        // but cannot upgrade to get highlighted profile
                        // Renders html and does not take up space
                        //strShowPopup.Append("document.getElementById('" + pnlSpotlightInfoForMembersWithSubscription.ClientID + "').style.display = '';");
                        strShowPopup.Append("javascript: document.getElementById('" + pnlSpotlightInfoForMembersWithoutSubscription.ClientID + "').style.display = 'none';");
                        strShowPopup.Append("TogglePopupDivDisplay('" + pnlSpotlightInfoForMembersWithSubscription.ClientID + "');");
                        // Renders html and also takes up space
                        //strShowPopup.Append("document.getElementById('" + pnlSpotlightInfoForMembersWithSubscription.ClientID + "').style.visibility = 'visible';");
                        //strShowPopup.Append("document.getElementById('" + pnlSpotlightInfoForMembersWithoutSubscription.ClientID + "').style.visibility = 'hidden';");

                        lnkSpotlightInfoForMembersWithSubscriptionCloseText.Text = g.GetResource("TXT_CLOSE", this);
                        lnkSpotlightInfoForMembersWithSubscriptionCloseImage.Text = g.GetResource("IMAGE_CLOSE", null, true, this);

                        lnkSpotlightInfoForMembersWithSubscriptionCloseText.Attributes.Add("href", strShowPopup.ToString());
                        lnkSpotlightInfoForMembersWithSubscriptionCloseImage.Attributes.Add("href", strShowPopup.ToString());
                    }
                    else
                    {
                        // Need to set to PurchaseReasonType.ViewedSpotlightInfo
                        lnkSpotlightInfoSubscribe.NavigateUrl = LinkFactory.Instance.GetLink("/Applications/Subscription/Subscribe.aspx?prtid=" + ((int)PurchaseReasonType.ViewedHighlightProfileInfo).ToString(), g.Brand, System.Web.HttpContext.Current.Request.Url.ToString()) + "&srid=" + this._viewedMemberID.ToString();
                        //lnkSpotlightInfoSubscribe.Text = g.GetResource("SpotlightInfo_TO_SUBSCRIPTION", this);					

                        // Viewing member that is not actively subscribed will see feature
                        // explanation and link to upgrade to get highlighted profile
                        strShowPopup.Append("javascript: document.getElementById('" + pnlSpotlightInfoForMembersWithSubscription.ClientID + "').style.display = 'none';");
                        strShowPopup.Append("TogglePopupDivDisplay('" + pnlSpotlightInfoForMembersWithoutSubscription.ClientID + "');");
                        //strShowPopup.Append("document.getElementById('" + pnlSpotlightInfoForMembersWithoutSubscription.ClientID + "').style.visibility = 'visible';");
                        //strShowPopup.Append("document.getElementById('" + pnlSpotlightInfoForMembersWithSubscription.ClientID + "').style.visibility = 'hidden';");

                        lnkSpotlightInfoForMembersWithoutSubscriptionCloseText.Text = g.GetResource("TXT_CLOSE", this);
                        lnkSpotlightInfoForMembersWithoutSubscriptionCloseImage.Text = g.GetResource("IMAGE_CLOSE", null, true, this);

                        lnkSpotlightInfoForMembersWithoutSubscriptionCloseText.Attributes.Add("href", strShowPopup.ToString());
                        lnkSpotlightInfoForMembersWithoutSubscriptionCloseImage.Attributes.Add("href", strShowPopup.ToString());
                    }
                    lnkSpotlightInfo.Attributes.Add("href", strShowPopup.ToString());
                    //lnkSpotlightInfo.Attributes.Add("onmouseover", strShowPopup.ToString());
                    if (g.LayoutVersion == WebConstants.LayoutVersions.versionWide)
                    {
                        imgSpotlightInfoForMembersWithoutSubscription.ResourceConstant = imgSpotlightInfoForMembersWithoutSubscription.ResourceConstant + "_20";


                    }

                    if (this._showImageNoText)
                    {
                        // Show image without text
                        string resKey = "IMG_LEARN_ABOUT_SPOTLIGHT_PROFILES";
                        if (g.LayoutVersion == WebConstants.LayoutVersions.versionWide)
                            resKey = resKey + "_20";

                        lnkSpotlightInfo.Text = g.GetResource(resKey, null, true, this);
                    }
                    else
                    {
                        // Show the text without image
                        lnkSpotlightInfo.Text = g.GetResource("TXT_LEARN_ABOUT_SPOTLIGHT_PROFILES", null, true, this);
                    }
                    lnkSpotlightInfo.Visible = true;
                    pnlSpotlightInfoForMembersWithSubscription.Style.Add("display", "none");
                    pnlSpotlightInfoForMembersWithSubscription.Style.Remove("visibility");
                    pnlSpotlightInfoForMembersWithoutSubscription.Style.Add("display", "none");
                    pnlSpotlightInfoForMembersWithoutSubscription.Style.Remove("visibility");
                    //				pnlSpotlightInfoForMembersWithSubscription.Style.Add("visibility", "hidden");
                    //				pnlSpotlightInfoForMembersWithoutSubscription.Style.Add("visibility", "hidden");
                }
                else
                {
                    lnkSpotlightInfo.Visible = false;
                    // Do not render html
                    pnlSpotlightInfoForMembersWithSubscription.Visible = false;
                    pnlSpotlightInfoForMembersWithoutSubscription.Visible = false;
                }
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        public bool Spotlight
        {
            set
            {
                this._Spotlight = value;
            }
        }

        public int ViewedMemberID
        {
            set
            {
                this._viewedMemberID = value;
            }
        }

        public bool ShowImageNoText
        {
            set
            {
                this._showImageNoText = value;
            }
        }


        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }
        #endregion
    }
}
