namespace Matchnet.Web.Framework.Ui.BasicElements
{
	using System;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;

	using Matchnet.Web.Framework;

	/// <summary>
	///		Summary description for NoResults.
	/// </summary>
	public class NoResults : FrameworkControl
	{
		protected Literal ltlDivTag;
		protected Literal ltlDivTagHome;
		protected Literal ltlPTagEnd;
		protected Matchnet.Web.Framework.Txt txtMore;
		protected Matchnet.Web.Framework.Txt txtMOL;
		protected Matchnet.Web.Framework.Txt txtPHOTOGALERY;
		protected Matchnet.Web.Framework.Txt txtIfYou;
		protected Matchnet.Web.Framework.Txt txtQuickSearchExpand;
		protected Matchnet.Web.Framework.Txt txtHint;
		protected System.Web.UI.WebControls.HyperLink lnkReviseSearchPreferences;
		protected Matchnet.Web.Framework.Txt txtRevise;
		protected Matchnet.Web.Framework.Image noResultsTip;
		private ContextType _displayContext;
		public enum ContextType
		{
			Default, Home, MembersOnline, PhotoGallery, QuickSearch, MatchMeter, AdvancedSearch
		}
		public ContextType DisplayContext
		{
			get
			{
				return _displayContext;
			}
			set
			{
				_displayContext = value;
			}
		}

		private void Page_Load(object sender, System.EventArgs e)
		{

			lnkReviseSearchPreferences.NavigateUrl = "/Applications/Search/SearchPreferences.aspx";

			try
			{

				if(DisplayContext == ContextType.Home)
				{
					ltlDivTag.Visible = false;
					ltlDivTagHome.Visible = true;
					ltlPTagEnd.Visible = false;
				}
				else if(DisplayContext == ContextType.MembersOnline)
                {
					txtMOL.Visible = true;
					txtMore.Visible = false;
				}
				else if(DisplayContext == ContextType.PhotoGallery)
                {
					txtPHOTOGALERY.Visible=true;
					txtMOL.Visible=false;
					txtMore.Visible=false;
					txtRevise.Visible=false;
					txtIfYou.Visible=false;
					txtHint.Visible=false;
                   // noResultsTip.Visible = false;
				}
				else if(DisplayContext == ContextType.QuickSearch)
				{
					txtQuickSearchExpand.Visible = true;
					txtPHOTOGALERY.Visible = false;
					txtMOL.Visible = false;
					txtMore.Visible = true;
					lnkReviseSearchPreferences.NavigateUrl = "/Applications/QuickSearch/QuickSearch.aspx";
					txtRevise.Visible = true;
					txtIfYou.Visible = false;
					txtHint.Visible = true;
					//noResultsTip.Visible = true;
				}
                else if (DisplayContext == ContextType.AdvancedSearch)
                {
                    txtPHOTOGALERY.Visible = false;
                    txtMOL.Visible = false;
                    txtMore.Visible = true;
                    lnkReviseSearchPreferences.NavigateUrl = "/Applications/AdvancedSearch/AdvancedSearch.aspx";
                    txtRevise.Visible = true;
                    txtIfYou.Visible = false;
                    txtHint.Visible = true;
               //     noResultsTip.Visible = true;
                }
                else if (DisplayContext == ContextType.MatchMeter)
                {
                    lnkReviseSearchPreferences.Visible = false;
                    txtHint.ResourceConstant = "TXT_MORE_RESULTS_HINT_MATCH_METER";
                }
			}
			catch (Exception ex)
			{
				g.ProcessException(ex);
			}
		}
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
