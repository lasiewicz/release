﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="QuickSearchBox.ascx.cs"
    Inherits="Matchnet.Web.Framework.Ui.BasicElements.QuickSearchBox" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<asp:PlaceHolder ID="PlaceHolderMain" runat="server">
    <!-- ================================================== START Quick Search Box ================================================== -->
    <div id="quick-search-box">
        <asp:PlaceHolder ID="plcQuickSearchBoxOld" runat="server">
            <h2>
                <mn:Txt runat="server" ResourceConstant="TXT_QUICK_SEARCH" />
            </h2>
            <dl class="quick-search-box">
                <mn:MultiValidator ID="AgeMinValidator" TabIndex="-1" runat="server" Display="Dynamic"
                    RequiredType="IntegerType" ControlToValidate="TextBoxAgeMin" MinValResourceConstant="MIN_AGE_NOTICE"
                    MaxValResourceConstant="MIN_AGE_NOTICE" FieldNameResourceConstant="MIN_AGE_NOTICE"
                    IsRequired="True" MinVal="18" CustomizeErrorMessage="false" MaxVal="99" AppendToErrorMessage="<br />"
                    ValidationGroup="QuickSearchValidationGroup" />
                <mn:MultiValidator ID="AgeMaxValidator" TabIndex="-1" runat="server" Display="Dynamic"
                    RequiredType="IntegerType" ControlToValidate="TextBoxAgeMax" MinValResourceConstant="MAX_AGE_NOTICE"
                    MaxValResourceConstant="MAX_AGE_NOTICE" FieldNameResourceConstant="MAX_AGE_NOTICE"
                    IsRequired="True" MinVal="18" CustomizeErrorMessage="false" MaxVal="99" AppendToErrorMessage="<br />"
                    ValidationGroup="QuickSearchValidationGroup" />
                <asp:Label runat="server" ID="LabelPostalCodeError" Visible="true"></asp:Label>
                <dt>
                    <label for="Gender">
                        <mn:Txt runat="server" ResourceConstant="TXT_YOURE_A" />
                    </label>
                </dt>
                <dd>
                    <asp:DropDownList ID="DropDownListGender" runat="server" CssClass="large" />
                </dd>
                <dt class="seeking">
                    <label for="SeekingGender">
                        <mn:Txt runat="server" ResourceConstant="TXT_SEEKING_A" />
                    </label>
                </dt>
                <dd>
                    <asp:DropDownList ID="DropDownListSeekingGender" runat="server" CssClass="large" />
                </dd>
                <dt>
                    <label for="AgeMin">
                        <mn:Txt runat="server" ResourceConstant="TXT_AGE" />
                    </label>
                </dt>
                <dd class="short">
                    <asp:TextBox ID="TextBoxAgeMin" CausesValidation="true" runat="server" MaxLength="2"
                        Text="18" CssClass="tiny" ValidationGroup="QuickSearchValidationGroup"></asp:TextBox>
                </dd>
                <dt>
                    <label for="AgeMax">
                        <mn:Txt runat="server" ResourceConstant="TXT_TO" />
                    </label>
                </dt>
                <dd class="short">
                    <asp:TextBox ID="TextBoxAgeMax" CausesValidation="true" runat="server" MaxLength="2"
                        Text="35" CssClass="tiny" ValidationGroup="QuickSearchValidationGroup"></asp:TextBox>
                </dd>
                <asp:PlaceHolder runat="server" ID="plcZipCode">
                    <dt>
                        <label for="Distance">
                            <mn:Txt runat="server" ResourceConstant="TXT_LOCATED_WITHIN" />
                        </label>
                    </dt>
                    <dd>
                        <asp:DropDownList ID="DropDownListDistance" runat="server" />
                    </dd>
                    <dt class="zip">
                        <label for="Zip">
                            <mn:Txt runat="server" ResourceConstant="TXT_OF" />
                        </label>
                    </dt>
                    <dd>
                        <asp:TextBox ID="TextBoxPostalCode" CausesValidation="true" runat="server" CssClass="short"
                            ValidationGroup="QuickSearchValidationGroup"></asp:TextBox>
                    </dd>
                    <dt></dt>
                </asp:PlaceHolder>
                <asp:PlaceHolder runat="server" ID="plcRegion" Visible="false">
                    <dt>
                        <label>
                            <mn:Txt runat="server" ResourceConstant="TXT_REGION" />
                        </label>
                    </dt>
                    <dd>
                        <asp:DropDownList ID="cupidRegionDropDownList" runat="server" CssClass="select-region">
                        </asp:DropDownList>
                    </dd>
                </asp:PlaceHolder>
                <dd class="submit">
                    <asp:Button ID="ButtonSearchNow" runat="server" CssClass="btn secondary" Text="Search Now! &raquo;"
                        OnClick="ButtonSearchNow_Click" CausesValidation="true" ValidationGroup="QuickSearchValidationGroup" />
                </dd>
            </dl>
        </asp:PlaceHolder>
        <asp:PlaceHolder runat="server" ID="plcQuickSearchBoxRedesign" Visible="false">
            <div id="header">
                <mn:Txt ID="Txt1" runat="server" ResourceConstant="TXT_QUICK_SEARCH" />
            </div>
            <div id="content">
                <mn:MultiValidator ID="MultiValidator1" TabIndex="-1" runat="server" Display="Dynamic"
                    RequiredType="IntegerType" ControlToValidate="dropDownMinAge" MinValResourceConstant="MIN_AGE_NOTICE"
                    MaxValResourceConstant="MIN_AGE_NOTICE" FieldNameResourceConstant="MIN_AGE_NOTICE"
                    IsRequired="True" MinVal="18" CustomizeErrorMessage="false" MaxVal="99" AppendToErrorMessage="<br />"
                    ValidationGroup="QuickSearchValidationGroup" />
                <mn:MultiValidator ID="MultiValidator2" TabIndex="-1" runat="server" Display="Dynamic"
                    RequiredType="IntegerType" ControlToValidate="dropDownMaxAge" MinValResourceConstant="MAX_AGE_NOTICE"
                    MaxValResourceConstant="MAX_AGE_NOTICE" FieldNameResourceConstant="MAX_AGE_NOTICE"
                    IsRequired="True" MinVal="18" CustomizeErrorMessage="false" MaxVal="99" AppendToErrorMessage="<br />"
                    ValidationGroup="QuickSearchValidationGroup" />
                <asp:Label runat="server" ID="LabelPostalCodeErrorRedesign" Visible="true"></asp:Label>
                <div id="row1-header">
                    <div>
                        <mn:Txt ID="Txt2" runat="server" ResourceConstant="TXT_YOURE_A" />
                    </div>
                    <div>
                        <mn:Txt ID="Txt3" runat="server" ResourceConstant="TXT_AGE" />
                    </div>
                </div>
                <div id="row1-controls">
                    <div>
                        <asp:DropDownList ID="DropDownListGenderSeeking" runat="server" CssClass="large" />
                    </div>
                    <div>
                        <asp:DropDownList ID="dropDownMinAge" runat="server" ValidationGroup="QuickSearchValidationGroup"/>
                        <span>
                        <mn:Txt ID="Txt4" runat="server" ResourceConstant="TXT_TO" />
                        </span>
                        <asp:DropDownList ID="dropDownMaxAge" runat="server" ValidationGroup="QuickSearchValidationGroup"/>
                    </div>
                </div>
                <asp:PlaceHolder runat="server" ID="plcZipCodeRedesign">
                    <div id="row2-header">
                        <div>
                            <mn:Txt ID="Txt5" runat="server" ResourceConstant="TXT_LOCATED_WITHIN" />
                        </div>
                        <div>
                            <mn:Txt ID="Txt6" runat="server" ResourceConstant="TXT_OF" />
                        </div>
                    </div>
                    <div id="row2-controls">
                        <div>
                            <asp:DropDownList ID="DropDownListDistanceRedesign" runat="server" />
                        </div>
                        <div>
                            <asp:TextBox ID="TextBoxPostalCodeRedesign" CausesValidation="true" runat="server"
                                CssClass="short" ValidationGroup="QuickSearchValidationGroup"></asp:TextBox>
                        </div>
                    </div>
                </asp:PlaceHolder>
                <asp:PlaceHolder runat="server" ID="plcRegionRedesign" Visible="false">
                    <div id="row3-header">
                    <mn:Txt ID="Txt7" runat="server" ResourceConstant="TXT_REGION" />
                    </div>
                    <div id="row3-controls">
                    <asp:DropDownList ID="cupidRegionDropDownListRedesign" runat="server" CssClass="select-region">
                    </asp:DropDownList>
                    </div>
                </asp:PlaceHolder>
                <div><asp:CheckBox ID="cbHasPhoto" Runat="server" /></div>
                <div id="search-button">
                    <asp:Button ID="ButtonSearchNowRedesign" runat="server" CssClass="btn secondary" Text="Search Now! &raquo;"
                        OnClick="ButtonSearchNow_Click" CausesValidation="true" ValidationGroup="QuickSearchValidationGroup" />
                </div>
            </div>
        </asp:PlaceHolder>
    </div>
    <!-- ================================================== END Quick Search Box ================================================== -->
</asp:PlaceHolder>
