using Matchnet.List.ValueObjects;

namespace Matchnet.Web.Framework.Ui.BasicElements
{
	/// <summary>
	/// Summary description for MutualYesIcon.
	/// </summary>
	public class MutualYesIcon : Image
	{
		public int _memberID;

		public MutualYesIcon()
		{
			this.PreRender += new System.EventHandler(MutualYesIcon_PreRender);
		}

		public int MemberID
		{
			get
			{
				return _memberID;
			}
			set
			{
				_memberID = value;
			}
		}

		private void MutualYesIcon_PreRender(object sender, System.EventArgs e)
		{
			this.Visible = false;

			ClickMask clickMask = _g.List.GetClickMask(_g.Brand.Site.Community.CommunityID, _g.Brand.Site.SiteID, _memberID);

			if ((clickMask & ClickMask.MemberYes) == ClickMask.MemberYes
				&& (clickMask & ClickMask.TargetMemberYes) == ClickMask.TargetMemberYes)
			{
				this.Visible = true;
			}
		}
	}
}
