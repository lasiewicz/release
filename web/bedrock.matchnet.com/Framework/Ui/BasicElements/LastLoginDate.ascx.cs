using System;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace Matchnet.Web.Framework.Ui.BasicElements
{
	/// <summary>
	///		Summary description for LastLoginDate.
	/// </summary>
	public class LastLoginDate : FrameworkControl
	{
		protected Literal lastLoginDateCtrl;
		protected Label	lblLastLoginDate;
		private string _LastLoginDateLabel;
		private bool _showLabel = true;
		private DateTime _lastLoginDate;
		private bool _showCurrentlyOnline = true;

		private void Page_Load(object sender, System.EventArgs e)
		{
			try
			{
				lastLoginDateCtrl.Text = BuildLastLoginDate();
				if( _showLabel )
				{
					if( _LastLoginDateLabel != null )
					{
						lblLastLoginDate.Text = _LastLoginDateLabel;
					}
					else
					{
						lblLastLoginDate.Text = g.GetResource("LOGGED_IN", this);
					}
				}
				else
				{
					lblLastLoginDate.Visible = false;
				}
			}
			catch(Exception ex)
			{
				g.ProcessException(ex);
			}
		}

		public String BuildLastLoginDate()
		{
			String ago = g.GetResource("PRO_AGO", this);
			String and = g.GetResource("TXT_AND", this);
			String minute = g.GetResource("PRO_MINUTE", this);
			String minutes = g.GetResource("PRO_MINUTES", this);
			String hour = g.GetResource("PRO_HOUR", this);
			String hours = g.GetResource("PRO_HOURS", this);
			String day = g.GetResource("PRO_DAY", this);
			String days = g.GetResource("PRO_DAYS", this);
            String before = g.GetResource("PRO_BEFORE", this);

			if (_lastLoginDate != DateTime.MinValue)
			{
				DateTime now				= DateTime.Now;
				TimeSpan timeSpan			= now.Subtract(_lastLoginDate);
				
				if (timeSpan.Days >= 60)
				{
                    if (g.Brand.Site.Direction.ToString() == "rtl")
                    {
                        return String.Format("{0} {1}", before, g.GetResource("PRO_MORE_THAN_60_DAYS_AGO", this));
                    }
                    else
                    {
                        return g.GetResource("PRO_MORE_THAN_60_DAYS_AGO", this);
                    }                    
				}
				else if (timeSpan.Days >= 1)
				{
					String outday = (timeSpan.Days == 1) ? day : days;

                    if (g.Brand.Site.SiteID == (int) WebConstants.SITE_ID.JDateFR)
                    {
                        return String.Format("{0} {1} {2}", ago, timeSpan.Days, outday);
                    }
                    else if (g.Brand.Site.Direction.ToString()=="rtl")
                    {
                        return String.Format("{0} {1} {2} {3}", before, timeSpan.Days, outday, ago);
                    }    
                    else
                    {
                        return String.Format("{0} {1} {2}", timeSpan.Days, outday, ago);
                    }
                }
                else if (timeSpan.Hours >= 1)
                {
                    String outHour = (timeSpan.Hours == 1) ? hour : hours;

                    if (g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.JDateFR)
                    {
                        return String.Format("{0} {1} {2}", ago, timeSpan.Hours, outHour);
                    }
                    else if (g.Brand.Site.Direction.ToString() == "rtl")
                    {
                        return String.Format("{0} {1} {2} {3}", before, timeSpan.Hours, outHour, ago);
                    }  
                    else
                    {
                        return String.Format("{0} {1} {2}", timeSpan.Hours, outHour, ago);
                    }
                }
                else
                {
                    int totalMinutes = (timeSpan.Minutes > 1) ? timeSpan.Minutes : 1;
                    String outminute = (totalMinutes == 1) ? minute : minutes;

                    if (g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.JDateFR)
                    {
                        return String.Format("{0} {1} {2}", ago, totalMinutes, outminute);
                    }
                    else if (g.Brand.Site.Direction.ToString() == "rtl")
                    {
                        return String.Format("{0} {1} {2} {3}", before, totalMinutes, outminute, ago);
                    }  
                    else
                    {
                        return String.Format("{0} {1} {2}", totalMinutes, outminute, ago);
                    }
                }

			}
			else
			{
				return Matchnet.Constants.NULL_STRING;
			}
		}
		
		
		#region Public properties
		public bool ShowLabel 
		{
			set
			{
				_showLabel = value;
			}
		}
		public DateTime LastLoginDateValue
		{
			set
			{
				_lastLoginDate = value;
			}			
		}

		public string LastLoginDateLabel
		{
			set
			{
				_LastLoginDateLabel = value;
			}
		}

		public bool ShowCurrentlyOnline
		{
			set
			{
				_showCurrentlyOnline = value;
			}
		}
		#endregion


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
