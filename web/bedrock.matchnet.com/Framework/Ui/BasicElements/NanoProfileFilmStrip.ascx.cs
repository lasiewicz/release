﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Web.Applications.ColorCode;
using Matchnet.Search.ValueObjects;
using Matchnet.Web.Applications.Home;
using System.Web.UI.HtmlControls;
using Matchnet.List.ValueObjects;
using System.Collections;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Search.ServiceAdapters;

namespace Matchnet.Web.Framework.Ui.BasicElements
{
    public partial class NanoProfileFilmStrip : FrameworkControl
    {
        #region Fields
        protected string _ColorText = "";
        private List<ProfileHolder> _memberList;
        private bool _hasMatches = false;
        private const string DOMAINID = "DomainID";
        private int _MaxDisplay = 17;
        private MOCollection _myMOCollection;
        #endregion

        #region Properties
        public bool HasMatches
        {
            get { return _hasMatches; }
            set { _hasMatches = value; }
        }

        public int MaxDisplay
        {
            get { return _MaxDisplay; }
            set { _MaxDisplay = value; }
        }
        public List<ProfileHolder> MemberList
        {
            get { return _memberList; }
            set { _memberList = value; }
        }
        public string TrackingParam { get; set; }

        public MOCollection MyMOCollection
        {
            get { return (_myMOCollection); }
            set { _myMOCollection = value; }
        }
        #endregion

        #region Event Handlers
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void repeaterProfiles_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                ProfileHolder profileHolder = e.Item.DataItem as ProfileHolder;

                FilmStripNanoProfile FilmStripNanoProfile1 = e.Item.FindControl("FilmStripNanoProfile1") as FilmStripNanoProfile;
                FilmStripNanoProfile1.member = profileHolder.Member;
                FilmStripNanoProfile1.MyEntryPoint = profileHolder.MyEntryPoint;
                FilmStripNanoProfile1.Ordinal = profileHolder.Ordinal;
                FilmStripNanoProfile1.HotListCategoryID = (int)profileHolder.HotlistCategory;
                FilmStripNanoProfile1.MyMOCollection = MyMOCollection;
                FilmStripNanoProfile1.TrackingParam = TrackingParam;
                
                FilmStripNanoProfile1.LoadMemberProfile();

                if (FrameworkGlobals.memberHighlighted(profileHolder.Member, g.Brand))
                {
                    //CSS
                    HtmlGenericControl liBegin = e.Item.FindControl("liBegin") as HtmlGenericControl;
                    liBegin.Attributes["class"] += " highlighted";
                }
            }
        }

        #endregion

        #region Public Methods

        public void BindMemberList()
        {
          try{
              BreadCrumbHelper.EntryPoint entrypoint = BreadCrumbHelper.EntryPoint.SearchResults;

              if (_memberList != null && _memberList.Count > 0)
              {
                  phHasProfiles.Visible = true;
                  entrypoint = _memberList[0].MyEntryPoint;

                  _hasMatches = true;

                  if (_memberList.Count < _MaxDisplay)
                  {
                      phViewMore.Visible = false;
                  }
                  repeaterProfiles.DataSource = _memberList;
                  repeaterProfiles.DataBind();
              }

              if (entrypoint == BreadCrumbHelper.EntryPoint.HomeHeroProfileFilmStripMatches)
              {
                
                  lnkViewMore.NavigateUrl = "/Applications/Search/SearchResults.aspx";
              }
              else if (entrypoint == BreadCrumbHelper.EntryPoint.HomeHeroProfileFilmStripMOL)
              {
                  
                  lnkViewMore.NavigateUrl = "/Applications/MembersOnline/MembersOnline.aspx";
              }
          }
         catch(Exception ex)
        {
            g.ProcessException(ex);    
        }




        }
     
        #endregion

    }
}