﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Member.ValueObjects;
using Matchnet.Member.ValueObjects.Interfaces;
using Matchnet.Member.ValueObjects.Photos;
using Matchnet.Web.Framework.Managers;
using Matchnet.Web.Framework.Ui.PageElements;
using Matchnet.List.ValueObjects;
using Matchnet.Content.ServiceAdapters.Links;
using Matchnet.Configuration.ServiceAdapters.Analitics;
using Matchnet.Web.Applications.ColorCode;

namespace Matchnet.Web.Framework.Ui.BasicElements
{
    /// <summary>
    /// This is a profile control displaying a small version of the profile, used 
    /// on the homepage for Your Matches and Hotlists.
    /// </summary>
    public partial class MicroProfile20 : FrameworkControl
    {
        #region Fields
        private int _ordinal;
        private int _memberID;
        private BreadCrumbHelper.EntryPoint _myEntryPoint;
        private MOCollection _myMOCollection;
        private int _hotListCategoryID;
        private IMemberDTO _Member;
        private bool _isHighlighted = false;
        protected string viewProfileUrl;
        private DateTime _lastHotlistActionDate = DateTime.MinValue;
        private bool _displayCommunication = true;
        private DisplayContextType _DisplayContext = DisplayContextType.Default;
        protected Color _Color = Color.none;
        protected string _ColorText = "";

        const int MAX_WORD_LENGTH = 10;
        const int MAX_STRING_LENGTH = 40;
        const int MAX_USERNAME_LENGTH = 16;
        #endregion

        #region Properties
        public IMemberDTO member
        {
            get { return _Member; }
            set
            {
                _Member = value;
                _memberID = _Member.MemberID;

                if (FrameworkGlobals.memberHighlighted(this._memberID, g.Brand))
                {
                    this._isHighlighted = true;
                }
                else
                {
                    this._isHighlighted = false;
                }
            }
        }

        public enum DisplayContextType
        {
            Default,
            Carrot,
            HotList,
            Search,
            MOL,
            YNMList
        }

        public DisplayContextType DisplayContext
        {
            get { return _DisplayContext; }
            set { _DisplayContext = value; }
        }

        public int MemberID
        {
            get { return (_memberID); }
        }

        public int Ordinal
        {
            get { return (_ordinal); }
            set { _ordinal = value; }
        }


        public BreadCrumbHelper.EntryPoint MyEntryPoint
        {
            get { return (_myEntryPoint); }
            set { _myEntryPoint = value; }
        }

        public MOCollection MyMOCollection
        {
            get { return (_myMOCollection); }
            set { _myMOCollection = value; }
        }

        public int HotListCategoryID
        {
            get { return (_hotListCategoryID); }
            set { _hotListCategoryID = value; }
        }

        public bool IsHighlighted
        {
            get { return (this._isHighlighted); }
            set { this._isHighlighted = value; }
        }

        public DateTime LastHotlistActionDate
        {
            get { return (this._lastHotlistActionDate); }
            set { this._lastHotlistActionDate = value; }
        }

        public bool DisplayCommunication
        {
            get { return this._displayCommunication; }
            set { this._displayCommunication = value; }
        }

        public string TrackingParam { get; set; }
        #endregion

        #region Event Handler
        protected void Page_Load(object sender, EventArgs e)
        {
            plcClick.Visible = g.IsYNMEnabled;
        }

        protected override void OnPreRender(EventArgs e)
        {
            this.panelCommunication.Visible = _displayCommunication;
            base.OnPreRender(e);
        }
        #endregion


        #region Methods
        /// <summary>
        /// Loads Member Profile based on properties already set into the control, including the Member object
        /// </summary>
        public void LoadMemberProfile()
        {
            try
            {
                SearchResultProfile searchResult = new SearchResultProfile(_Member, Ordinal);

                if (this.IsHighlighted)
                {
                    viewProfileUrl = BreadCrumbHelper.MakeViewProfileLink(this.MyEntryPoint, this.MemberID, this.Ordinal, this.MyMOCollection, this.HotListCategoryID, false, (int)BreadCrumbHelper.PremiumEntryPoint.Highlight);
                    this.phHighlightedInfoLink.Visible = true;
                    this.ucHighlightProfileInfoDisplay.Visible = true;
                    this.ucHighlightProfileInfoDisplay.ViewedMemberID = _memberID;
                    this.ucHighlightProfileInfoDisplay.MemberHighlighted = true;
                    this.ucHighlightProfileInfoDisplay.ShowImageNoText = true;
                }
                else
                {
                    viewProfileUrl = BreadCrumbHelper.MakeViewProfileLink(this.MyEntryPoint, this.MemberID, this.Ordinal, this.MyMOCollection, this.HotListCategoryID, false, (int)BreadCrumbHelper.PremiumEntryPoint.NoPremium);
                    this.panelMicroProfile20.CssClass = "micro-profile clearfix"; //css for container div, non-highlight
                    this.ucHighlightProfileInfoDisplay.Visible = false;
                }

                if(!String.IsNullOrEmpty(TrackingParam)) {
                    viewProfileUrl = BreadCrumbHelper.AppendParamToProfileLink(viewProfileUrl, TrackingParam);
                }

                //set basic info
                lnkUserName1.NavigateUrl = viewProfileUrl;
                lnkUserName1.Text = FrameworkGlobals.Ellipsis(_Member.GetUserName(_g.Brand), 12);

                //MemberPhotoHelper photoHelper = new MemberPhotoHelper(RuntimeSettings.Instance);
                noPhoto.NoPhotoFileName = MemberPhotoDisplayManager.Instance.GetNoPhotoFile(NoPhotoImageType.TinyThumbV2, true);
                searchResult.SetThumb(imgThumb, noPhoto, viewProfileUrl);

                searchResult.SetAge(literalAge, this);
                
                if(FrameworkGlobals.isHebrewSite(g.Brand))
                {
                    literalAge.Text = g.GetResource("TXT_YEARS_OLD", this) + " " + literalAge.Text;
                }
                else
                {
                    literalAge.Text += " " + g.GetResource("TXT_YEARS_OLD", this);
                }

                searchResult.SetRegion(g.Brand.Site.LanguageID, literalLocation, MAX_STRING_LENGTH);

                LinkParent parent = LinkParent.HomePageMicroProfile; 

                switch (_DisplayContext)
                {
                    case DisplayContextType.Default:
                        parent = LinkParent.HomePageMicroProfile;
                        break;
                    case DisplayContextType.Carrot:
                    case DisplayContextType.YNMList:
                        panelCommunication.Visible = false;
                        phHotlistAction.Visible = false;
                        phCommunication.Visible = false;
                        break;
                }

                lnkEmail.NavigateUrl = String.Format(FrameworkGlobals.GetEmailLink(this.MemberID, false) + "&LinkParent={0}", parent.ToString("d"));
                
                VoteBar.MemberID = this._memberID;

                //hotlist info
                if (this._lastHotlistActionDate != DateTime.MinValue)
                {
                    this.phHotlistAction.Visible = true;
                    this.lnkHotlistAction.NavigateUrl = "/Applications/HotList/View.aspx?CategoryID=" + this.HotListCategoryID.ToString();
                    switch (this.HotListCategoryID)
                    {
                        case (int)HotListCategory.MembersYouViewed:
                            this.literalHotlistAction.ResourceConstant = "TXT_HOTLIST_ACTION_YOU_VIEWED";
                            break;
                        case (int)HotListCategory.WhoViewedYourProfile:
                            this.literalHotlistAction.ResourceConstant = "TXT_HOTLIST_ACTION_VIEWED_YOU";
                            break;
                        case (int)HotListCategory.MembersYouIMed:
                            this.literalHotlistAction.ResourceConstant = "TXT_HOTLIST_ACTION_YOU_IMED";
                            break;
                        case (int)HotListCategory.WhoIMedYou:
                            this.literalHotlistAction.ResourceConstant = "TXT_HOTLIST_ACTION_IMED_YOU";
                            break;
                        case (int)HotListCategory.MembersYouTeased:
                            this.literalHotlistAction.ResourceConstant = "TXT_HOTLIST_ACTION_YOU_TEASED";
                            break;
                        case (int)HotListCategory.WhoTeasedYou:
                            this.literalHotlistAction.ResourceConstant = "TXT_HOTLIST_ACTION_TEASED_YOU";
                            break;
                        case (int)HotListCategory.WhoAddedYouToTheirFavorites:
                            this.literalHotlistAction.ResourceConstant = "TXT_HOTLIST_ACTION_FAVORITED_YOU";
                            break;
                        case (int)HotListCategory.MembersYouEmailed:
                            this.literalHotlistAction.ResourceConstant = "TXT_HOTLIST_ACTION_YOU_EMAILED";
                            break;
                        case (int)HotListCategory.WhoEmailedYou:
                            this.literalHotlistAction.ResourceConstant = "TXT_HOTLIST_ACTION_EMAILED_YOU";
                            break;
                        case (int)HotListCategory.MembersYouSentECards:
                            this.literalHotlistAction.ResourceConstant = "TXT_HOTLIST_ACTION_YOU_ECARDED";
                            break;
                        case (int)HotListCategory.WhoSentYouECards:
                            this.literalHotlistAction.ResourceConstant = "TXT_HOTLIST_ACTION_ECARDED_YOU";
                            break;
                    }

                    //time
                    lastTimeAction.LoadLastTime(this._lastHotlistActionDate);
                    
                }

                SetEmailButtonABTest();

                //color code
                if (ColorCodeHelper.IsColorCodeEnabled(g.Brand) && ColorCodeHelper.HasMemberCompletedQuiz(_Member, g.Brand) && !ColorCodeHelper.IsMemberColorCodeHidden(_Member, g.Brand))
                {
                    phColorCode.Visible = true;
                    _Color = ColorCodeHelper.GetPrimaryColor(_Member, g.Brand);
                    _ColorText = ColorCodeHelper.GetFormattedColorText(_Color);
                }

            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        private void SetEmailButtonABTest()
        {
            Scenario scenario = g.GetABScenario("MINIPROFILE_BUTTON");
            if (scenario == Scenario.B)
            {
                imgEmail.FileName = "btn-view-sm.gif";
                imgEmail.ResourceConstant = "ALT_VIEW_MEMBER";
                imgEmail.TitleResourceConstant = "ALT_VIEW_MEMBER";
                lnkEmail.NavigateUrl = g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.ViewProfile, WebConstants.Action.ViewMemberButton, lnkUserName1.NavigateUrl, string.Empty);
            }
            else
            {
                lnkEmail.NavigateUrl = g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.Compose, WebConstants.Action.EmailMeNowButton, lnkEmail.NavigateUrl, string.Empty);
            }
        }

        #endregion
    }
}
