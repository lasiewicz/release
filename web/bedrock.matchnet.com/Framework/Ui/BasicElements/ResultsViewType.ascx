﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ResultsViewType.ascx.cs" Inherits="Matchnet.Web.Framework.Ui.BasicElements.ResultsViewType" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>

<mn:txt runat="server" id="txt1" resourceconstant="TXT_VIEW_AS" ExpandImageTokens="true" />
<mn:txt runat="server" id="txtListOff" resourceconstant="TXT_LIST" ExpandImageTokens="true" />
<asp:hyperlink runat="server" id="lnkViewType">
    <mn:txt runat="server" id="txtViewAs" resourceconstant="TXT_VIEW_AS_GALLERY" ExpandImageTokens="true" />
</asp:hyperlink>
<mn:txt runat="server" id="txtGalleryOff" resourceconstant="TXT_GALLERY" ExpandImageTokens="true" />		  
