﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="IceBreaker.ascx.cs" Inherits="Matchnet.Web.Framework.Ui.BasicElements.IceBreaker" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>


<asp:PlaceHolder ID="phIcebreaker" runat="server" Visible="false">
    <div id="iceLauncher" class="ice-launcher float-outside">
        <mn:Txt runat="server" id="txtIcebreakersButton" resourceconstant="TXT_ICEBREAKERS_BUTTON" expandimagetokens="true" />
    </div>
    <div id="iceCont" class="ice-cont hide">
        <mn:Txt runat="server" id="iceBreakersHeader" resourceconstant="TXT_ICEBREAKERS_HEADER" expandimagetokens="true" />
        <p><mn:txt runat="server" id="iceBreakersTagline" resourceconstant="TXT_ICEBREAKERS_TAGLINE" expandimagetokens="true" /></p>
        <ul class="ice-list">
            <asp:Repeater ID="repeaterSectionDynamic" runat="server" OnItemDataBound="repeaterSectionDynamic_ItemDataBound">
                <ItemTemplate>
                    <li><button class="ice-inject textlink" type="button">Insert</button> <div class="ice-breaker"><asp:Literal ID="iceBreakerItemDynamic" runat="server"></asp:Literal></div></li>
                </ItemTemplate>
            </asp:Repeater>
            <asp:Repeater ID="repeaterSectionStatic" runat="server" OnItemDataBound="repeaterSectionStatic_ItemDataBound">
                <ItemTemplate>
                    <li><button class="ice-inject textlink" type="button">Insert</button> <div class="ice-breaker"><asp:Literal ID="iceBreakerItemStatic" runat="server"></asp:Literal></div></li>
                </ItemTemplate>
            </asp:Repeater>
        </ul>
    </div>

    <script type="text/javascript">
        $j('#iceCont').bind('click', function (event) {
            var $eventTarget = $j(event.target);

            if ($eventTarget.hasClass('ice-inject')) {
                if ($j('body.sub-page-compose').length === 1) {
                    var $textarea = $j("textarea[id*='messageBodyTextField']");
                }
                if ($j('body.page-memberprofile').length === 1) {
                    var $textarea = $j("textarea[id*='txtEmailMessage']");
                }
                //var $textarea = $j("textarea[id*='txtEmailMessage']");
                var txtMessage = $textarea.val();
                var txtMessage = txtMessage.replace(/^\s+|\s+$/g, '');
                var iceCopy = $eventTarget.next().html();
                var iceCopy = iceCopy.replace('&nbsp;', ' ');
                var newMessage = "";

                if (txtMessage === "") {
                    newMessage = iceCopy;
                }
                else {
                    //newMessage = iceCopy + "\n\n" + txtMessage;
                    newMessage = iceCopy + "\r\n\r\n" + txtMessage;
                }

                //$textarea.val(newMessage).scrollTop(0);
                $textarea.val(newMessage);

                var textareaID = $textarea.attr('id');

                //Set position of cursor
                var textareaEl = document.getElementById(textareaID);
                if (textareaEl.setSelectionRange) {
                    textareaEl.setSelectionRange((iceCopy.length - 3), iceCopy.length);
                    //textareaEl.setSelectionRange(iceCopy.length, iceCopy.length);
                }
                else {
                    e = textareaEl.createTextRange();
                    e.collapse(true);
                    e.moveEnd('character', iceCopy.length);
                    e.moveStart('character', iceCopy.length - 3);
                    e.select();
                }
                //Set focus on textarea - Firefox only likes POJ for some reason
                document.getElementById(textareaID).focus();
                $j('#iceCont').dialog('close');

                //update omniture
                if (s != null) {
                    PopulateS(true); //clear existing values in omniture "s" object

                    s.prop3 = "<%=OmnitureInsertString %>";
                    s.t(); //send omniture updated values as page load
                }
            }
        });

        $j('#iceLauncher').find('button').bind('click', function(event){
                
            $j("#iceCont").dialog({
                width: 590,
		        modal: true,
                title:'',
                dialogClass: 'ui-dialog-rev icebreakers',
                open:function(){
                    var reversedClose = $j('<div class="ui-dialog-titlebar-close-rev link-style"><%=g.GetResource("TXT_CLOSE",this) %> <span class="spr s-icon-closethick-color"></span></div>').bind('click',function(){
                        $j('#iceCont').dialog('close');
                    });
                    $j(this).parent().find('.ui-dialog-titlebar-close').replaceWith(reversedClose);

                    //update omniture
                    if (s != null) {
                        PopulateS(true); //clear existing values in omniture "s" object

                        s.prop3 = "<%=OmnitureStartString %>";
                        s.t(); //send omniture updated values as page load
                    }
                }
	        });
        });
    </script>
</asp:PlaceHolder>