﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProfileActivity.ascx.cs" Inherits="Matchnet.Web.Framework.Ui.BasicElements.ProfileActivity" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mnl" Namespace="Matchnet.Web.Framework.Ui.BasicElements.Links" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mnbe" Namespace="Matchnet.Web.Framework.Ui.BasicElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="ft" TagName="FavoritesTab" Src="~/Applications/Favorites/FavoritesClientTab.ascx" %>
<%@ Register TagPrefix="uc2" Src="LastTime.ascx" TagName="LastTime" %>


<script type="text/javascript">

    var previousSelectedTab = 'activityFeed'; //default tab
    
    function profileActivityTabClick(newTab) {

        //hide previous tab
        $j('#' + previousSelectedTab + 'TabContent').css('display', 'none');
        $j('#' + previousSelectedTab + 'Tab').removeClass('selected');

        //show new tab
        $j('#' + newTab + 'TabContent').css('display', 'block');
        $j('#' + newTab + 'Tab').addClass('selected');
        previousSelectedTab = newTab;

        return false;
    }

    jQuery(document).ready(function() {
        jQuery(".onlineNewsFeed a[href*='ViewProfile.aspx']").live("click", function(evt) {
            var href = jQuery(evt.target).attr("href");
            if (href.indexOf("?") > -1) {
                href = ReplaceSingle(href, "?", "?ClickFrom=" + escape("Homepage - Profile Activity - Newsfeed") + "&");
            } else {
                href = ReplaceSingle(href, "ViewProfile.aspx", "ViewProfile.aspx?ClickFrom=" + escape("Homepage - Profile Activity - Newsfeed"));
            }
            jQuery(evt.target).attr("href", href);
        });

        jQuery(".onlineNewsFeed a").live("click", function(evt) {
            spark.tracking.addEvent("event47", true);
            spark.tracking.addLinkTrackEvent("event47", true);
            spark.tracking.addLinkTrackVar("events", true);
            spark.tracking.addLinkTrackVar("prop37");
            var type = jQuery(evt.target).closest("[ot]").attr("ot");
            spark.tracking.addProp(37, "Profile Activity Module - News feed - " + type);
            spark.tracking.trackLink({ firstParam: true, linkName: "Click on Newsfeed Item" });
        });
    });
</script>

<div id="profile-activity" class="grid-block-full">
    <h2 class="component-header"><mn2:FrameworkLiteral ID="FrameworkLiteral4" runat="server" ResourceConstant="TXT_PROFILE_ACTIVITY"></mn2:FrameworkLiteral></h2>
    <ul class="nav-rounded-tabs click clearfix">
	    <li class="tab" id="favoritesOnlineTab">
	        <a href="#" onclick="return profileActivityTabClick('favoritesOnline');" title="<mn2:FrameworkLiteral ID='FrameworkLiteralFavoritesOnlineTitle' runat='server' ResourceConstant='TITLE_FAVORITES_ONLINE'></mn2:FrameworkLiteral>" class="spr-parent">
	            <span class="underline"><mn2:FrameworkLiteral ID="FrameworkLiteral1" runat="server" ResourceConstant="TXT_FAVORITES_ONLINE"></mn2:FrameworkLiteral></span>
	        </a>
	    </li>
	    <li class="tab" id="sentTab">
	        <a href="#" onclick="return profileActivityTabClick('sent');" title="<mn2:FrameworkLiteral ID='FrameworkLiteralSentTitle' runat='server' ResourceConstant='TITLE_SENT'></mn2:FrameworkLiteral>">
	            <mn2:FrameworkLiteral ID="FrameworkLiteral2" runat="server" ResourceConstant="TXT_SENT"></mn2:FrameworkLiteral>
	        </a>
	    </li>
	    <li class="tab" id="receivedTab">
	        <a href="#" onclick="return profileActivityTabClick('received');" title="<mn2:FrameworkLiteral ID='FrameworkLiteralReceivedTitle' runat='server' ResourceConstant='TITLE_RECEIVED'></mn2:FrameworkLiteral>">
	            <mn2:FrameworkLiteral ID="FrameworkLiteral3" runat="server" ResourceConstant="TXT_RECEIVED"></mn2:FrameworkLiteral>
	        </a>
	    </li>
	    <li class="tab selected" id="activityFeedTab">
	        <a href="#" onclick="return profileActivityTabClick('activityFeed');" title="<mn2:FrameworkLiteral ID='FrameworkLiteralActivityFeedTitle' runat='server' ResourceConstant='TITLE_ACTIVITYFEED'></mn2:FrameworkLiteral>">
	            <mn2:FrameworkLiteral ID="FrameworkLiteral5" runat="server" ResourceConstant="TXT_ACTIVITYFEED"></mn2:FrameworkLiteral>
	        </a>
	    </li>
    </ul>

    <!--Activity Feed-->
    <div id="activityFeedTabContent" class="outer-wrapper">
        <asp:PlaceHolder ID="phActivityFeedContent" runat="server">
            <div class="vertstrip-news feed-wrapper">
                <div class="ajax-loading">
                    <mn:image id="mnimage4704" runat="server" filename="ajax-loader.gif" titleresourceconstant="" resourceconstant="" />
                    <p><mn:txt id="mntxt7220" runat="server" resourceconstant="TXT_LOADING" expandimagetokens="true" /></p>
                </div>
	            <div class="vertstrip-news-feed feed">
	            <ul>
                        <asp:Repeater ID="repeaterNotificationItems" runat="server" OnItemDataBound="repeaterNotificationItems_ItemDataBound">
	                    <ItemTemplate>
                                <li class="item onlineNewsFeed" ot="<asp:Literal ID='omnitureTag' runat='server'/>">	                        
                                    <mn2:FrameworkLiteral ID="imgThumbnail" runat="server"></mn2:FrameworkLiteral>
                                    <span class="message">
                                        <mn2:FrameworkLiteral ID="literalUserNotification" runat="server"></mn2:FrameworkLiteral>
                                    </span> 
                                    <span class="date"><mn2:FrameworkLiteral ID="lastTimeUserNotification" runat="server" /></span>	                                
                                </li>
                            </ItemTemplate>
                        </asp:Repeater>
           	            <asp:PlaceHolder ID="phNotificationsFeedViewMore" runat="server">
                            <li class="item"><div class="view-more"><a href="/Applications/UserNotifications/Notifications.aspx"><mn2:FrameworkLiteral ID="FrameworkLiteral37" runat="server" ResourceConstant="TXT_VIEW_ALL_PROFILE_ACTIVITY"></mn2:FrameworkLiteral></a></div></li>
                        </asp:PlaceHolder>
                        <asp:Repeater ID="repeaterActivityItems" runat="server" OnItemDataBound="repeaterActivityItems_ItemDataBound">
                            <ItemTemplate>
	                        <li class="item">
	                            <div class="message"><asp:HyperLink runat="server" ID="lnkUserName1" /></div> 
	                            <div class="date"><mn2:FrameworkLiteral ID="literalActivityAction" runat="server"></mn2:FrameworkLiteral><span class="timestamp"> <uc2:LastTime ID="lastTimeAction" runat="server" /></span></div>
	                        </li>
	                    </ItemTemplate>
	                </asp:Repeater>
		           	<asp:PlaceHolder ID="phActivityFeedViewMore" runat="server">
	                        <li class="item"><div class="view-more"><a href="/Applications/HotList/View.aspx?CategoryID=-1"><mn2:FrameworkLiteral ID="FrameworkLiteral6" runat="server" ResourceConstant="TXT_VIEW_ALL_PROFILE_ACTIVITY"></mn2:FrameworkLiteral></a>
	                        </div></li>
		            </asp:PlaceHolder>
	            </ul>
	            </div>
	            <div class="vertstrip-news-controls control">
		            <button class="vertstrip-prev disabled">&#9650;</button>
		            <button class="vertstrip-next">&#9660;</button>
	            </div>
            </div>
        </asp:PlaceHolder>
        <asp:PlaceHolder ID="phActivityFeedNoContent" runat="server" Visible="false">
            <mn2:FrameworkLiteral ID="FrameworkLiteral7" runat="server" ResourceConstant="TXT_NO_ACTIVITYFEED_COPY"></mn2:FrameworkLiteral>
        </asp:PlaceHolder>
    </div>

    <!--Received-->
    <div id="receivedTabContent" style="display:none;">
        <asp:PlaceHolder ID="phReceivedContent" runat="server">
            <ul class="profile-activity-stats clearfix">
	            <li class="item">
	                <a href="/Applications/HotList/View.aspx?CategoryID=-1"><mn2:FrameworkLiteral ID="FrameworkLiteral11" runat="server" ResourceConstant="TXT_HOTLIST_VIEWED_YOU"></mn2:FrameworkLiteral> (<asp:Literal ID="literalViewedYou" runat="server"></asp:Literal>)</a> 
	                <asp:PlaceHolder ID="phViewedYouNew" runat="server" Visible="false">
	                    <em class="accent"><asp:Literal ID="literalViewedYouNew" runat="server"></asp:Literal>&nbsp;<mn2:FrameworkLiteral ID="FrameworkLiteral18" runat="server" ResourceConstant="TXT_NEW"></mn2:FrameworkLiteral></em>
	                </asp:PlaceHolder>
	            </li>
	            <li class="item">
	                <a href="/Applications/HotList/View.aspx?CategoryID=-4"><mn2:FrameworkLiteral ID="FrameworkLiteral12" runat="server" ResourceConstant="TXT_HOTLIST_EMAILED_YOU"></mn2:FrameworkLiteral> (<asp:Literal ID="literalEmailedYou" runat="server"></asp:Literal>)</a> 
	                <asp:PlaceHolder ID="phEmailedYouNew" runat="server" Visible="false">
	                    <em class="accent"><asp:Literal ID="literalEmailedYouNew" runat="server"></asp:Literal>&nbsp;<mn2:FrameworkLiteral ID="FrameworkLiteral19" runat="server" ResourceConstant="TXT_NEW"></mn2:FrameworkLiteral></em>
	                </asp:PlaceHolder>
	            </li>
	            <li class="item">
	                <a href="/Applications/HotList/View.aspx?CategoryID=-3"><mn2:FrameworkLiteral ID="FrameworkLiteral13" runat="server" ResourceConstant="TXT_HOTLIST_FLIRTED_YOU"></mn2:FrameworkLiteral> (<asp:Literal ID="literalFlirtedYou" runat="server"></asp:Literal>)</a> 
	                <asp:PlaceHolder ID="phFlirtedYouNew" runat="server" Visible="false">
	                    <em class="accent"><asp:Literal ID="literalFlirtedYouNew" runat="server"></asp:Literal>&nbsp;<mn2:FrameworkLiteral ID="FrameworkLiteral23" runat="server" ResourceConstant="TXT_NEW"></mn2:FrameworkLiteral></em>
	                </asp:PlaceHolder>
	            </li>
	            <li class="item">
	                <a href="/Applications/HotList/View.aspx?CategoryID=-2"><mn2:FrameworkLiteral ID="FrameworkLiteral14" runat="server" ResourceConstant="TXT_HOTLIST_ADDED_YOU_FAVORITES"></mn2:FrameworkLiteral> (<asp:Literal ID="literalFavoritedYou" runat="server"></asp:Literal>)</a> 
	                <asp:PlaceHolder ID="phFavoritedYouNew" runat="server" Visible="false">
	                    <em class="accent"><asp:Literal ID="literalFavoritedYouNew" runat="server"></asp:Literal>&nbsp;<mn2:FrameworkLiteral ID="FrameworkLiteral20" runat="server" ResourceConstant="TXT_NEW"></mn2:FrameworkLiteral></em>
	                </asp:PlaceHolder>
	            </li>
	            <li class="item">
	                <a href="/Applications/HotList/View.aspx?CategoryID=-5"><mn2:FrameworkLiteral ID="FrameworkLiteral15" runat="server" ResourceConstant="TXT_HOTLIST_IM_YOU"></mn2:FrameworkLiteral> (<asp:Literal ID="literalIMYou" runat="server"></asp:Literal>)</a> 
	                <asp:PlaceHolder ID="phIMYouNew" runat="server" Visible="false">
	                    <em class="accent"><asp:Literal ID="literalIMYouNew" runat="server"></asp:Literal>&nbsp;<mn2:FrameworkLiteral ID="FrameworkLiteral21" runat="server" ResourceConstant="TXT_NEW"></mn2:FrameworkLiteral></em>
	                </asp:PlaceHolder>
	            </li>
	            <li class="item">
	                <a href="/Applications/HotList/View.aspx?CategoryID=-25"><mn2:FrameworkLiteral ID="FrameworkLiteral16" runat="server" ResourceConstant="TXT_HOTLIST_ECARD_YOU"></mn2:FrameworkLiteral> (<asp:Literal ID="literalEcardYou" runat="server"></asp:Literal>)</a> 
	                <asp:PlaceHolder ID="phEcardYouNew" runat="server" Visible="false">
	                    <em class="accent"><asp:Literal ID="literalEcardYouNew" runat="server"></asp:Literal>&nbsp;<mn2:FrameworkLiteral ID="FrameworkLiteral24" runat="server" ResourceConstant="TXT_NEW"></mn2:FrameworkLiteral></em>
	                </asp:PlaceHolder>
	            </li>
	            <li class="item">
	                <a href="/Applications/HotList/View.aspx?CategoryID=-15"><mn2:FrameworkLiteral ID="FrameworkLiteral17" runat="server" ResourceConstant="TXT_HOTLIST_BOTH_SAID_YES"></mn2:FrameworkLiteral> (<asp:Literal ID="literalBothYes" runat="server"></asp:Literal>)</a> 
	                <asp:PlaceHolder ID="phBothYesNew" runat="server" Visible="false">
	                    <em class="accent"><asp:Literal ID="literalBothYesNew" runat="server"></asp:Literal>&nbsp;<mn2:FrameworkLiteral ID="FrameworkLiteral22" runat="server" ResourceConstant="TXT_NEW"></mn2:FrameworkLiteral></em>
	                </asp:PlaceHolder>
	            </li>
            </ul>
        </asp:PlaceHolder>
        <asp:PlaceHolder ID="phReceivedNoContent" runat="server" Visible="false">
            <mn2:FrameworkLiteral ID="FrameworkLiteral8" runat="server" ResourceConstant="TXT_NO_RECEIVED_COPY"></mn2:FrameworkLiteral>
        </asp:PlaceHolder>
    </div>
    
    <!--Sent-->
    <div id="sentTabContent" style="display:none;">
        <asp:PlaceHolder ID="phSentContent" runat="server">
            <ul class="profile-activity-stats clearfix">
	            <li class="item">
	                <a href="/Applications/HotList/View.aspx?CategoryID=-9"><mn2:FrameworkLiteral ID="FrameworkLiteral25" runat="server" ResourceConstant="TXT_HOTLIST_YOU_VIEWED"></mn2:FrameworkLiteral> (<asp:Literal ID="literalYouViewed" runat="server"></asp:Literal>)</a> 
	                <asp:PlaceHolder ID="phYouViewedNew" runat="server" Visible="false">
	                    <em class="accent"><asp:Literal ID="literalYouViewedNew" runat="server"></asp:Literal>&nbsp;<mn2:FrameworkLiteral ID="FrameworkLiteral26" runat="server" ResourceConstant="TXT_NEW"></mn2:FrameworkLiteral></em>
	                </asp:PlaceHolder>
	            </li>
	            <li class="item">
	                <a href="/Applications/HotList/View.aspx?CategoryID=-7"><mn2:FrameworkLiteral ID="FrameworkLiteral27" runat="server" ResourceConstant="TXT_HOTLIST_YOU_EMAILED"></mn2:FrameworkLiteral> (<asp:Literal ID="literalYouEmailed" runat="server"></asp:Literal>)</a> 
	                <asp:PlaceHolder ID="phYouEmailedNew" runat="server" Visible="false">
	                    <em class="accent"><asp:Literal ID="literalYouEmailedNew" runat="server"></asp:Literal>&nbsp;<mn2:FrameworkLiteral ID="FrameworkLiteral28" runat="server" ResourceConstant="TXT_NEW"></mn2:FrameworkLiteral></em>
	                </asp:PlaceHolder>
	            </li>
	            <li class="item">
	                <a href="/Applications/HotList/View.aspx?CategoryID=-6"><mn2:FrameworkLiteral ID="FrameworkLiteral29" runat="server" ResourceConstant="TXT_HOTLIST_YOU_FLIRTED"></mn2:FrameworkLiteral> (<asp:Literal ID="literalYouFlirted" runat="server"></asp:Literal>)</a> 
	                <asp:PlaceHolder ID="phYouFlirtedNew" runat="server" Visible="false">
	                    <em class="accent"><asp:Literal ID="literalYouFlirtedNew" runat="server"></asp:Literal>&nbsp;<mn2:FrameworkLiteral ID="FrameworkLiteral30" runat="server" ResourceConstant="TXT_NEW"></mn2:FrameworkLiteral></em>
	                </asp:PlaceHolder>
	            </li>
	            <li class="item">
	                <a href="/Applications/HotList/View.aspx?CategoryID=0"><mn2:FrameworkLiteral ID="FrameworkLiteral31" runat="server" ResourceConstant="TXT_HOTLIST_YOU_ADDED_FAVORITES"></mn2:FrameworkLiteral> (<asp:Literal ID="literalYouFavorited" runat="server"></asp:Literal>)</a> 
	                <asp:PlaceHolder ID="phYouFavoritedNew" runat="server" Visible="false">
	                    <em class="accent"><asp:Literal ID="literalYouFavoritedNew" runat="server"></asp:Literal>&nbsp;<mn2:FrameworkLiteral ID="FrameworkLiteral32" runat="server" ResourceConstant="TXT_NEW"></mn2:FrameworkLiteral></em>
	                </asp:PlaceHolder>
	            </li>
	            <li class="item">
	                <a href="/Applications/HotList/View.aspx?CategoryID=-8"><mn2:FrameworkLiteral ID="FrameworkLiteral33" runat="server" ResourceConstant="TXT_HOTLIST_YOU_IM"></mn2:FrameworkLiteral> (<asp:Literal ID="literalYouIM" runat="server"></asp:Literal>)</a> 
	                <asp:PlaceHolder ID="phYouIMNew" runat="server" Visible="false">
	                    <em class="accent"><asp:Literal ID="literalYouIMNew" runat="server"></asp:Literal>&nbsp;<mn2:FrameworkLiteral ID="FrameworkLiteral34" runat="server" ResourceConstant="TXT_NEW"></mn2:FrameworkLiteral></em>
	                </asp:PlaceHolder>
	            </li>
	            <li class="item">
	                <a href="/Applications/HotList/View.aspx?CategoryID=-24"><mn2:FrameworkLiteral ID="FrameworkLiteral35" runat="server" ResourceConstant="TXT_HOTLIST_YOU_ECARD"></mn2:FrameworkLiteral> (<asp:Literal ID="literalYouEcard" runat="server"></asp:Literal>)</a> 
	                <asp:PlaceHolder ID="phYouEcardNew" runat="server" Visible="false">
	                    <em class="accent"><asp:Literal ID="literalYouEcardNew" runat="server"></asp:Literal>&nbsp;<mn2:FrameworkLiteral ID="FrameworkLiteral36" runat="server" ResourceConstant="TXT_NEW"></mn2:FrameworkLiteral></em>
	                </asp:PlaceHolder>
	            </li>
            </ul>
        </asp:PlaceHolder>
        <asp:PlaceHolder ID="phSentNoContent" runat="server" Visible="false">
            <mn2:FrameworkLiteral ID="FrameworkLiteral9" runat="server" ResourceConstant="TXT_NO_SENT_COPY"></mn2:FrameworkLiteral>
        </asp:PlaceHolder>
    </div>
    
    <!--Favorites Online-->
    <div id="favoritesOnlineTabContent" class="outer-wrapper" style="display:none;">
        <asp:PlaceHolder ID="phFavoritesOnlineContent" runat="server">
            <ft:FavoritesTab ID="FavoritesTab" ClickableElementId="favoritesOnlineTab" OnlineIndicatorElementId="favoritesOnlineTab" runat="server" />
        </asp:PlaceHolder>
        <asp:PlaceHolder ID="phFavoritesOnlineNoContent" runat="server" Visible="false">
            <mn2:FrameworkLiteral ID="FrameworkLiteral10" runat="server" ResourceConstant="TXT_NO_FAVORITES_COPY"></mn2:FrameworkLiteral>
        </asp:PlaceHolder>
    </div>
    
</div>

