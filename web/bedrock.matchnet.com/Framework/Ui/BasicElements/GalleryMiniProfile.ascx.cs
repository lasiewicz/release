using System;
using System.Collections.Specialized;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Web.Framework.Ui.PageElements;
using Matchnet.Web.Framework.Ui.BasicElements.Links;
using Matchnet.Content.ServiceAdapters.Links;
using Matchnet.Member.ServiceAdapters;

namespace Matchnet.Web.Framework.Ui.BasicElements
{
    /// <summary>
    ///		Summary description for GalleryMiniProfile.
    /// </summary>
    public class GalleryMiniProfile : FrameworkControl, IPostBackDataHandler
    {
        protected Add2List add2List;
        protected Add2List add2List1;
        private Member.ServiceAdapters.Member _Member;
        private int _memberID;
        private int _ordinal;
        private BreadCrumbHelper.EntryPoint _myEntryPoint;
        private MOCollection _myMOCollection;
        private int _hotListCategoryID;
        protected Link lnkUserName;
        protected Literal txtAge;
        protected Literal txtRegion;
        protected NoPhoto noPhoto;
        protected HyperLink lnkEmail;
        protected HyperLink lnkECard;
        protected Txt txtMore;
        protected Matchnet.Web.Framework.Image imgThumb;
        protected PlaceHolder plcMutualYesIcon;
        protected PlaceHolder plcYNMVoteButtons;
        protected PlaceHolder plcTransparent;

        protected System.Web.UI.HtmlControls.HtmlTableCell cProfileTop;
        protected System.Web.UI.HtmlControls.HtmlTableCell cProfileBottom;
        protected System.Web.UI.HtmlControls.HtmlGenericControl boxContainerGallery;

        private bool _isHighlighted = false;

        // conditional image icons
        protected Image imgIsNew;
        protected Image imgIsUpdate;
        protected Image imgPremiumAuthenticated;
        protected Image imgOnline;
        protected MutualYesIcon imgBothSaidYes;
        protected Image imgFlirt;
        
        // static image icons
        protected Image imgEmail;

        protected PlaceHolder plcHotListMove;
        protected System.Web.UI.HtmlControls.HtmlInputCheckBox chkSelected;
        protected System.Web.UI.HtmlControls.HtmlInputText txtFavoriteNote;
        protected PlaceHolder plcEmailMe;
        protected HyperLink lnkEmailMeTop;

        protected Matchnet.Web.Framework.Ui.BasicElements.HighlightProfileInfoDisplay ucHighlightProfileInfoDisplay;

        protected Matchnet.Web.Framework.Ui.BasicElements.MatchMeterInfoDisplay ucMatchMeterInfoDisplay;
        private bool _IsMatchMeterEnabled;
        protected bool _transparency = false;


        // constants
        protected int MAX_STRING_LENGTH = 12;

        public SearchResultProfile searchResult;

        public Member.ServiceAdapters.Member Member
        {
            get { return _Member; }
            set
            {
                _Member = value;
                _memberID = _Member.MemberID;

                if (FrameworkGlobals.memberHighlighted(this._memberID, g.Brand))
                {
                    this._isHighlighted = true;
                }
                else
                {
                    this._isHighlighted = false;
                }
            }
        }

        public int MemberID
        {
            get { return (_memberID); }
            set
            {
                _memberID = value;
                if (value != 0)
                {
                    _Member = MemberSA.Instance.GetMember(_memberID,
                                MemberLoadFlags.None);
                }
                else
                {
                    _Member = null;
                }
            }
        }

        public int Ordinal
        {
            get { return (_ordinal); }
            set { _ordinal = value; }
        }

        public BreadCrumbHelper.EntryPoint MyEntryPoint
        {
            get { return (_myEntryPoint); }
            set { _myEntryPoint = value; }
        }

        public MOCollection MyMOCollection
        {
            get { return (_myMOCollection); }
            set { _myMOCollection = value; }
        }

        public int HotListCategoryID
        {
            get { return (_hotListCategoryID); }
            set { _hotListCategoryID = value; }
        }

        public bool IsHighlighted
        {
            get { return (this._isHighlighted); }
            set { this._isHighlighted = value; }
        }
        public bool IsMatchMeterEnabled
        {
            get { return _IsMatchMeterEnabled; }
            set { _IsMatchMeterEnabled = value; }
        }

        public bool Transparency
        {
            get { return (_transparency); }
            set { _transparency = value; }
        }

        # region Context Type public property and enumerator
        private ResultContextType _DisplayContext = ResultContextType.SearchResult;
        protected Txt TxtSelectThisProfile;
        protected Txt TxtDeselectThisProfile;
        protected YNMVoteBarSmall VoteBar;

        public ResultContextType DisplayContext
        {
            set
            {
                _DisplayContext = value;
            }
        }
        #endregion

        #region public counter property
        public int _counter = 0;

        public int Counter
        {
            get
            {
                return _counter;
            }
            set
            {
                _counter = value;
            }
        }
        #endregion

        private void Page_Load(object sender, EventArgs e)
        {
            int brandId = 0;
            _Member.GetLastLogonDate(g.Brand.Site.Community.CommunityID, out brandId);

            /*if(brandId == (int)WebConstants.BRAND_ID.JDateCoIL)
            {
                lnkECard.Visible = false;
            }
            */
            try
            {
                add2List.Orientation = Add2ListOrientation.IconOnly;
                add2List.MemberID = this.MemberID;
                add2List1.Orientation = Add2ListOrientation.IconOnly;
                add2List1.MemberID = this.MemberID;

                txtFavoriteNote.Visible = false;
                searchResult = new SearchResultProfile(_Member, Ordinal);
                chkSelected.Value = _Member.MemberID.ToString();

                SetData();
                ToggleDisplayType();
                // Alter the z-index here
                boxContainerGallery.Style.Add("z-index", Convert.ToString(100 - Counter));

                //Set jmeter controls
                SetJmeter();
                plcTransparent.Visible = Transparency;
            }
            catch (Exception ex)
            {
                _g.ProcessException(ex);
            }
        }

        private void SetJmeter()
        {
            if (IsMatchMeterEnabled)
            {
                ucMatchMeterInfoDisplay.Visible = true;
                ucMatchMeterInfoDisplay.IsMatchMeterEnabled = true;
                ucMatchMeterInfoDisplay.MemberID = MemberID;
                ucMatchMeterInfoDisplay.IsMemberHighlighted = IsHighlighted;
            }
            else
            {
                ucMatchMeterInfoDisplay.IsMatchMeterEnabled = false;
                ucMatchMeterInfoDisplay.Visible = false;
            }
        }

        bool IPostBackDataHandler.LoadPostData(string postDataKey, NameValueCollection values)
        {
            return false;
        }

        void IPostBackDataHandler.RaisePostDataChangedEvent()
        {
        }

        private void ToggleDisplayType()
        {
            switch (_DisplayContext)
            {
                case ResultContextType.HotList:
                    ShowHotListMember();
                    break;
                case ResultContextType.SearchResult:
                    ShowSearchResultMember();
                    break;
                case ResultContextType.MembersOnline:
                    ShowOnlineMember();
                    break;
            }
        }

        private void ShowOnlineMember()
        {
            plcHotListMove.Visible = false;
            txtFavoriteNote.Visible = false;
        }

        private void ShowSearchResultMember()
        {
            plcHotListMove.Visible = false;
            txtFavoriteNote.Visible = false;
        }

        private void ShowHotListMember()
        {
            //	This has been replaced with the new Add2List control
            //plcHotListMove.Visible = true;
            plcHotListMove.Visible = false;
            // chkSelected.Visible = true;
            chkSelected.Visible = false;
            chkSelected.Attributes.Add("OnClick", searchResult.showOnClickJavascript(_counter));
            searchResult.SetNotes(txtFavoriteNote);

            // When in any of the hot list categories page, a Delete from list displays.
            // When this displays, do not display Learn about Highlighted Profiles.
            ucHighlightProfileInfoDisplay.Visible = false;
        }


        private void SetData()
        {
            // TPJ-94
            imgIsNew.Visible = _Member.IsNewMember(g.Brand.Site.Community.CommunityID);
            imgIsUpdate.Visible = !imgIsNew.Visible && _Member.IsUpdatedMember(g.Brand.Site.Community.CommunityID);

            if (g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.JDateCoIL)
            {
                imgPremiumAuthenticated.Visible = FrameworkGlobals.IsMemberPremiumAuthenticated(_Member.MemberID, g.Brand);
            }
            else
            {
                imgPremiumAuthenticated.Visible = false;
            }

            //if (FrameworkGlobals.memberHighlighted(_Member.MemberID, g.Brand))     
            if (this._isHighlighted)
            {
                boxContainerGallery.Attributes.Remove("class");
                boxContainerGallery.Attributes.Add("class", "boxGallery highlightedProfile");

                /*
                    cProfileTop.Attributes.Remove("class");
                    cProfileTop.Attributes.Add("class", "gp1Highlighted");

                    cProfileBottom.Attributes.Remove("class");
                    cProfileBottom.Attributes.Add("class", "gpBottomHighLighted");            
                */

                ucHighlightProfileInfoDisplay.MemberHighlighted = true;
                ucHighlightProfileInfoDisplay.ViewedMemberID = _Member.MemberID;
            }
            else
            {
                ucHighlightProfileInfoDisplay.Visible = false;
            }

            noPhoto.MemberID = g.Session.GetInt(WebConstants.SESSION_PROPERTY_NAME_MEMBERID);

            VoteBar.MemberID = _Member.MemberID;
            if (g.Member != null)
            {
                if (g.Member.MemberID == _Member.MemberID)
                {
                    VoteBar.Visible = false;
                }
            }
            else
            {
                VoteBar.Visible = false;
            }

            if (VoteBar.Visible)
            {
                if (!g.IsYNMEnabled)
                {
                    VoteBar.Visible = false;
                    //plcEmailMe.Visible = true;
                    
                }
            }


            string userDestinationUrl = Constants.NULL_STRING;
            if (this._isHighlighted)
            {
                userDestinationUrl = BreadCrumbHelper.MakeViewProfileLink(this.MyEntryPoint, this.MemberID, this.Ordinal, this.MyMOCollection, this.HotListCategoryID, false, (int)BreadCrumbHelper.PremiumEntryPoint.Highlight);
            }
            else
            {
                userDestinationUrl = BreadCrumbHelper.MakeViewProfileLink(this.MyEntryPoint, this.MemberID, this.Ordinal, this.MyMOCollection, this.HotListCategoryID, false, (int)BreadCrumbHelper.PremiumEntryPoint.NoPremium);
                //userDestinationUrl = BreadCrumbHelper.MakeViewProfileLink(this.MyEntryPoint, this.MemberID, this.Ordinal, this.MyMOCollection, this.HotListCategoryID);
            }

            // A different LinkParent value will be given to Match Meter (Jmeter) One On One page
            // In order to use different PRTID when a redirection to sub page is needed
            LinkParent miniProfileLinkParent = LinkParent.GalleryMiniProfile;
            if (this.NamingContainer is Applications.CompatibilityMeter.OneOnOne30)
            {
                miniProfileLinkParent = LinkParent.MatchMeterOneOnOne;
            }

            string emailUrl = "/Applications/Email/Compose.aspx?MemberId=" + _Member.MemberID + "&LinkParent=" + (int)miniProfileLinkParent + "&StartRow=" + GetStartRow();

            // Render the appropriate image and URL for the IM link
            OnlineLinkHelper.SetOnlineLink(_DisplayContext, searchResult._Member, this.Member, imgOnline, miniProfileLinkParent);

            txtMore.Href = userDestinationUrl;
            lnkEmail.NavigateUrl = emailUrl;

            lnkEmailMeTop.NavigateUrl = "/Applications/Email/Compose.aspx?MemberId=" + _Member.MemberID + "&LinkParent=" +
                                     (int)LinkParent.GalleryMiniProfileTop;

            searchResult.SetUserName(lnkUserName, userDestinationUrl);
            searchResult.SetThumb(imgThumb, noPhoto, userDestinationUrl);
            searchResult.SetAge(txtAge, g.GetResource("AGE_UNSPECIFIED", this));
            searchResult.SetRegion(g.Brand.Site.LanguageID, txtRegion, 25);

            #region ECARD IMPLEMENTATION  5-22-06: Steve C.

            // Show hide send ECard link based on setting from database.  (ON or OFF)
            if (g.EcardsEnabled)
            {

                // Generate mingle ecard page url. (to parameter is the username of the reciever)
                string destPageUrl = FrameworkGlobals.BuildConnectFrameworkLink("cards/categories.html?to=" + _Member.GetUserName(_g.Brand) + "&MemberID=" + _Member.MemberID + "&return=1", g, true);

                // Assign url to ECard link.
                lnkECard.NavigateUrl = destPageUrl;
            }
            else
            {
                lnkECard.Visible = false;
            }

            #endregion

            //set flirt link
            imgFlirt.NavigateUrl = "/Applications/Email/Tease.aspx?MemberId=" + _memberID;
        }

        // Get startRow value for persisting result index
        private int GetStartRow()
        {
            // Recurse through the parent controls.  If one of the controls is "ResultList" retrieve the StartRow value.
            Control parentCtrl = ((Control)(this)).Parent;
            while (parentCtrl != null)
            {
                if (parentCtrl.GetType().Name == "ResultList_ascx")
                {
                    return ((ResultList)parentCtrl)._StartRow;
                }

                parentCtrl = parentCtrl.Parent;
            }

            return Constants.NULL_INT;
        }



        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);

        }
        #endregion
    }
}
