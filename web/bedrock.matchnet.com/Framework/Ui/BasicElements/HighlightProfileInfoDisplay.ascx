<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Control Language="c#" AutoEventWireup="false" Codebehind="HighlightProfileInfoDisplay.ascx.cs" Inherits="Matchnet.Web.Framework.Ui.BasicElements.HighlightProfileInfoDisplay" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>

<asp:HyperLink ID="lnkHighlightProfileInfo" Visible="False" Runat="server"></asp:HyperLink>	
<asp:Panel ID="pnlHighlightProfileInfoForMembersWithSubscription" Runat="server" CssClass="highlightProfileInfo">
	<div class="highlightProfileInfoClose">
		<asp:HyperLink ID="lnkHighlightProfileInfoForMembersWithSubscriptionCloseText" Runat="server"></asp:HyperLink>	
		<asp:HyperLink ID="lnkHighlightProfileInfoForMembersWithSubscriptionCloseImage" Runat="server"></asp:HyperLink>							
	</div>
	
	<mn:Txt id="txtHighlightProfileInfoForMembersWithSubscription" runat="server" ResourceConstant="TXT_HIGHLIGHTPROFILEINFO_SUBSCRIBER" expandImageTokens="True" />
</asp:Panel>
<asp:Panel ID="pnlHighlightProfileInfoForMembersWithoutSubscription" Runat="server" CssClass="highlightProfileInfo">
	<div class="highlightProfileInfoClose">
		<asp:HyperLink ID="lnkHighlightProfileInfoForMembersWithoutSubscriptionCloseText" Runat="server"></asp:HyperLink>	
		<asp:HyperLink ID="lnkHighlightProfileInfoForMembersWithoutSubscriptionCloseImage" Runat="server"></asp:HyperLink>							
	</div>

	<mn:Txt id="txtHighlightProfileInfoForMembersWithoutSubscription" runat="server" ResourceConstant="TXT_HIGHLIGHTPROFILEINFO_REGISTERED" expandImageTokens="True" />
	<asp:HyperLink ID="lnkHighlightProfileInfoSubscribe" Runat="server"><mn:Txt id="imgHighlightProfileInfoForMembersWithoutSubscription" runat="server" ResourceConstant="HIGHLIGHTPROFILEINFO_TO_SUBSCRIPTION" expandImageTokens="True" /></asp:HyperLink>		
</asp:Panel>