﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ColorCodeSelect.ascx.cs" Inherits="Matchnet.Web.Framework.Ui.BasicElements.ColorCodeSelect" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn1" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>

<asp:Panel runat="server" id="pnlColorCodeContainer" CssClass="rel-layer-container rel-layer-colorcode clearfix">
    <asp:HyperLink runat="server" ID="lnkColorCodeCollapse"/>
    
    <asp:Panel runat="server" ID="pnlColorCodeExpand" CssClass="rel-layer-div">
        <p class="clearfix">
		    <asp:HyperLink ID="lnkCloseText" Runat="server" CssClass="click-close float-outside"></asp:HyperLink>	
		    <%-- <asp:HyperLink ID="lnkCloseImage" Runat="server" CssClass="click-close float-outside"></asp:HyperLink> --%>							
	    </p>
	    <fieldset class="clearfix">
            <label><asp:CheckBox runat="server"  ID="chkYellow"/><mn:Txt ID="txtYellow" runat="server" ResourceConstant="TXT_YELLOW"/></label>
            <label><asp:CheckBox runat="server"  ID="chkWhite"/><mn:Txt ID="txtWhite" runat="server" ResourceConstant="TXT_WHITE"/></label>
            <label><asp:CheckBox runat="server"  ID="chkRed"/><mn:Txt ID="txtRed" runat="server" ResourceConstant="TXT_RED"/></label>
            <label><asp:CheckBox runat="server"  ID="chkBlue"/><mn:Txt ID="txtBlue" runat="server" ResourceConstant="TXT_BLUE"/></label>
	        <mn1:FrameworkButton  runat="server" ResourceConstant="SHOW_RESULTS" OnClick="showResults" id="btnShowResults" CssClass="btn primary clear-both" />
	    </fieldset>	
    </asp:Panel>
    
</asp:Panel>