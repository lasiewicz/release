using System;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Matchnet.Member.ValueObjects;
using Matchnet.Member.ValueObjects.Photos;
using Matchnet.Web.Framework.Ui.PageElements;
using Matchnet.Content.ServiceAdapters.Links;

namespace Matchnet.Web.Framework.Ui.BasicElements
{
    /// <summary>
    ///		Summary description for MicroProfile.
    /// </summary>
    public class MicroProfile : FrameworkControl
    {
        private int _ordinal;
        private int _memberID;
        private BreadCrumbHelper.EntryPoint _myEntryPoint;
        private MOCollection _myMOCollection;
        private int _hotListCategoryID;

        private Member.ServiceAdapters.Member _Member;

        private DisplayContextType _DisplayContext = DisplayContextType.Default;
        private bool _isHighlighted = false;
        private bool _isSpotlighted = false;
        protected Matchnet.Web.Framework.Ui.BasicElements.Links.Link lnkUsername;
        protected Matchnet.Web.Framework.Image imgThumb;
        //protected System.Web.UI.HtmlControls.HtmlTable tblProfile;
        //protected System.Web.UI.HtmlControls.HtmlTableRow rProfile;
        protected System.Web.UI.HtmlControls.HtmlGenericControl tblProfile;
        protected Literal txtAge;
        protected Literal txtRegion;
        protected Literal txtHeadline;
        protected string viewProfileUrl;
        protected Txt txtViewProfile;
        protected Txt txtViewProfileMatchMeter;
        protected NoPhoto noPhoto;
        //protected HtmlInputHidden YNMVoteStatus;
        //protected YNMVoteButton ImageYes;
        //protected YNMVoteButton ImageNo;
        //protected YNMVoteButton ImageMaybe;
        protected YNMVoteBarSmall VoteBar;
        protected PlaceHolder plcEmailMe;
        protected HyperLink lnkEmail;

        protected Matchnet.Web.Framework.Ui.BasicElements.HighlightProfileInfoDisplay ucHighlightProfileInfoDisplay;
        //protected Matchnet.Web.Framework.Ui.BasicElements.SpotlightInfoDisplay ucSpotlightProfileInfoDisplay;
        const int MAX_WORD_LENGTH = 10;
        const int MAX_STRING_LENGTH = 40;
        protected System.Web.UI.WebControls.HyperLink lnkThumb;
        const int MAX_USERNAME_LENGTH = 16;
        protected Matchnet.Web.Framework.Ui.BasicElements.MatchMeterInfoDisplay ucMatchMeterInfoDisplay;

        bool _isPromotional;
        SearchResultProfile spotProfile = null;
        public enum DisplayContextType
        {
            Default,
            MatchMeter
            //HotList,
            //Search,
            //MOL
        }

        public DisplayContextType DisplayContext
        {
            get { return _DisplayContext; }
            set { _DisplayContext = value; }
        }

        public Member.ServiceAdapters.Member member
        {
            get { return _Member; }
            set
            {
                _Member = value;
                _memberID = _Member.MemberID;

                if (FrameworkGlobals.memberHighlighted(this._memberID, g.Brand))
                {
                    this._isHighlighted = true;
                }
                else
                {
                    this._isHighlighted = false;
                }
            }
        }

        public int MemberID
        {
            get { return (_memberID); }
        }

        public int Ordinal
        {
            get { return (_ordinal); }
            set { _ordinal = value; }
        }


        public BreadCrumbHelper.EntryPoint MyEntryPoint
        {
            get { return (_myEntryPoint); }
            set { _myEntryPoint = value; }
        }

        public MOCollection MyMOCollection
        {
            get { return (_myMOCollection); }
            set { _myMOCollection = value; }
        }

        public int HotListCategoryID
        {
            get { return (_hotListCategoryID); }
            set { _hotListCategoryID = value; }
        }

        public bool IsHighlighted
        {
            get { return (this._isHighlighted); }
            set { this._isHighlighted = value; }
        }


        public bool IsSpotlighted
        {
            get { return (this._isSpotlighted); }
            set { this._isSpotlighted = value; }
        }


        public bool IsPromotionalMember
        {
            get { return (this._isPromotional); }
            set { this._isPromotional = value; }
        }

        public decimal MatchScore { get; set; }

        private void BindDefaultData()
        {
            SearchResultProfile searchResult = new SearchResultProfile(_Member, Ordinal);

            viewProfileUrl = BreadCrumbHelper.MakeViewProfileLink(MyEntryPoint, MemberID, Ordinal, MyMOCollection, HotListCategoryID);

            searchResult.SetUserName(lnkUsername, viewProfileUrl);

            searchResult.SetThumb(imgThumb, noPhoto, viewProfileUrl);

            VoteBar.MemberID = _Member.MemberID;

            //Use MAX_STRING_LENGTH for MAXWORDLENGTH param since the headline is displayed on one line
            searchResult.SetHeadline(txtHeadline, g.Member.MemberID, MAX_STRING_LENGTH, MAX_WORD_LENGTH);

            searchResult.SetAge(txtAge, this);
            searchResult.SetRegion(g.Brand.Site.LanguageID, txtRegion, MAX_STRING_LENGTH);

            // view profile link
            txtViewProfile.Href = viewProfileUrl;

            if (FrameworkGlobals.memberHighlighted(MemberID, g.Brand) && !_isSpotlighted)
            {
                tblProfile.Attributes.Remove("class");
                tblProfile.Attributes.Add("class", "highlightedProfile");

                ucHighlightProfileInfoDisplay.MemberHighlighted = true;
                ucHighlightProfileInfoDisplay.ViewedMemberID = _Member.MemberID;

            }
            else
            {
                ucHighlightProfileInfoDisplay.Visible = false;
            }
            ////spotProfile = searchResult;
            if (_isSpotlighted)
            {
                tblProfile.Attributes.Remove("class");
                tblProfile.Attributes.Add("class", "spotlightedProfile");

                ucHighlightProfileInfoDisplay.Visible = false;

                viewProfileUrl = BreadCrumbHelper.MakeViewProfileLink(MyEntryPoint, MemberID, Ordinal, MyMOCollection, HotListCategoryID, false, (int)BreadCrumbHelper.PremiumEntryPoint.Spotlight);
                txtViewProfile.Href = viewProfileUrl;
                searchResult.SetUserName(lnkUsername, viewProfileUrl);

                searchResult.SetThumb(imgThumb, noPhoto, viewProfileUrl);
            }
            else
            {
                //ucSpotlightProfileInfoDisplay.Visible = false;
                //plcSpotlight.Visible = false;
            }

            if (!g.IsYNMEnabled)
            {
                VoteBar.Visible = false;
                plcEmailMe.Visible = true;
                string emailURL = "/Applications/Email/Compose.aspx?MemberId=" + _Member.MemberID + "&LinkParent=" +
                                  (int)LinkParent.HomePageMicroProfileMatches;
                lnkEmail.NavigateUrl = emailURL;
            }
        }


        //protected override void Render(System.Web.UI.HtmlTextWriter writer)
        //{
        //    if (_isSpotlighted)
        //    {
        //        BindDefaultData();
        //        tblProfile.Attributes.Remove("class");
        //        tblProfile.Attributes.Add("class", "spotlightedProfile");

        //        ucHighlightProfileInfoDisplay.Visible = false;

        //        viewProfileUrl = BreadCrumbHelper.MakeViewProfileLink(MyEntryPoint, MemberID, Ordinal, MyMOCollection, HotListCategoryID, false, (int)BreadCrumbHelper.PremiumEntryPoint.Spotlight);
        //        txtViewProfile.Href = viewProfileUrl;
        //        spotProfile.SetUserName(lnkUsername, viewProfileUrl);

        //        spotProfile.SetThumb(imgThumb, noPhoto, viewProfileUrl);
        //       // Matchnet.Web.Applications.PremiumServices.VelocityHandler.DebugTrace("MicroProfile", "Render:" + spotProfile._Member.MemberID,g);
        //    }
        //    base.Render(writer);

        //}
        private void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (_Member == null)
                {

                    g.Notification.AddError("PAGE_MEMBER_PROFILE_TEMPORARILY_UNAVAILABLE");
                    return;
                }
                else
                {
                    switch (DisplayContext)
                    {
                        case DisplayContextType.Default:
                            BindDefaultData();
                            break;
                        case DisplayContextType.MatchMeter:
                            BindDefaultData();
                            ucMatchMeterInfoDisplay.Visible = true;
                            ucMatchMeterInfoDisplay.IsMatchMeterEnabled = true;
                            ucMatchMeterInfoDisplay.IsMemberHighlighted = IsHighlighted;
                            ucMatchMeterInfoDisplay.MemberID = MemberID;
                            ucMatchMeterInfoDisplay.IsMatchesPage = true;
                            ucMatchMeterInfoDisplay.MatchScore = MatchScore;
                            txtViewProfile.Visible = false;
                            txtViewProfileMatchMeter.Href = txtViewProfile.Href;
                            txtViewProfileMatchMeter.Visible = true;
                            break;
                        default:
                            BindDefaultData();
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                _g.ProcessException(ex);
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);

        }
        #endregion
    }
}
