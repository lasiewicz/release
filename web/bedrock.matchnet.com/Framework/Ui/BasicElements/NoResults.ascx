<%@ Control Language="c#" AutoEventWireup="false" Codebehind="NoResults.ascx.cs" Inherits="Matchnet.Web.Framework.Ui.BasicElements.NoResults" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>

<asp:Literal Runat="server" ID="ltlDivTag">
	<div class="box" id="sort" style="padding:0">
</asp:Literal>
<asp:Literal Runat="server" ID="ltlDivTagHome" Visible=False></asp:Literal>
	<div id="noMatchesBorder" style="margin:0;border:0">
		<div id="noMatches" class="border-gen tips">
			<h2>
			    <mn:txt runat="server" id="txtMore" ExpandImageTokens="true" ResourceConstant="TXT_MORE_MATCHES_ARE_WAITING" />
				<mn:txt runat="server" id="txtMOL" ResourceConstant="NO_MEMBERS_ONLINE_FOUND_MATCHING_YOUR_SEARCH_CRITERIA__519973" Visible=False ExpandImageTokens="true" />
				<mn:txt runat="server" id="txtPHOTOGALERY" ResourceConstant="NO_PHOTOGALLERY_RESULTS" Visible=False ExpandImageTokens="true" />
			</h2>        		
			<p><mn:txt runat="server" id="txtIfYou" ResourceConstant="TXT_TRY_BROADENING_YOUR_SEARCH_CRITERIA" ExpandImageTokens="true" /></p>
			<p><mn:txt runat="server" id="txtQuickSearchExpand" ResourceConstant="TXT_TRY_BROADENING_YOUR_QUICKSEARCH_CRITERIA" Visible="false" ExpandImageTokens="true" /></p>
			<p><mn:txt id="txtHint" runat="server" ResourceConstant="TXT_MORE_RESULTS_HINT" ExpandImageTokens="true" /></p>
			<div align="right">
				<asp:HyperLink ID="lnkReviseSearchPreferences" Runat="server"><mn:txt runat="server" id="txtRevise" ResourceConstant="TXT_REVISE_SEARCH_SETTINGS_ARROWS" /></asp:HyperLink>
			</div>
		</div>
		     		
	</div>
	</div>
	<asp:Literal Runat="server" ID="ltlPTagEnd"></asp:Literal>
 
