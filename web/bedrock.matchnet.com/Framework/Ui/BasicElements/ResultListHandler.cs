﻿using System;
using System.Linq;
using System.Text;
using System.Web;
using System.Collections.Generic;
using System.Data;
using System.Web.Caching;
using System.Web.UI.WebControls;
using System.Collections;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.Region;
using Matchnet.Lib;
using Matchnet.Member.ServiceAdapters.Interfaces;
using Matchnet.Member.ValueObjects.Interfaces;
using Matchnet.Member.ValueObjects.MemberDTO;
using Matchnet.PhotoSearch.ValueObjects;
using Matchnet.Web.Framework.Managers;
using Matchnet.Web.Framework.Util;
using Matchnet.Member.ServiceAdapters;
using Matchnet.List.ValueObjects;
using Matchnet.Session.ValueObjects;
using Matchnet.Search.ServiceAdapters;
using Matchnet.Search.ValueObjects;
using Matchnet.MembersOnline.ServiceAdapters;
using Matchnet.MembersOnline.ValueObjects;
using Matchnet.PhotoSearch.ServiceAdapters;
using Matchnet.MatchTest.ServiceAdapters;
using Matchnet.Content.ServiceAdapters.Links;
using Matchnet.Configuration.ServiceAdapters;

using Matchnet.AffiliationGroups.ServiceAdapters;
using Matchnet.Search.Interfaces;
namespace Matchnet.Web.Framework.Ui.BasicElements
{


    /// <summary>
    /// The type of result context.
    /// </summary>
    [Serializable]
    public enum ResultContextType
    {
        /// <summary>
        /// Hot List.
        /// </summary>
        HotList,
        /// <summary>
        /// Search Result.
        /// </summary>
        SearchResult,
        /// <summary>
        /// Members Online.
        /// </summary>
        MembersOnline,
        /// <summary>
        /// Photo Gallery.
        /// </summary>
        PhotoGallery,
        /// <summary>
        /// Quick Search Result.
        /// </summary>
        QuickSearchResult,
        /// <summary>
        /// Match Meter Matches.
        /// </summary>
        MatchMeterMatches,
        /// <summary>
        /// Registration Carrot.
        /// </summary>
        RegistrationCarrot,
        /// <summary>
        /// Keyword Search Result.
        /// </summary>
        KeywordSearchResult,
        /// <summary>
        /// Reverse Search Result.
        /// </summary>
        ReverseSearchResult,
        /// <summary>
        /// GroupsMembers
        /// </summary>
        AffiliationGroupMembers,
        /// <summary>
        /// Advanced Search Result.
        /// </summary>
        AdvancedSearchResult,
        /// <summary>
        /// Academic visitor Search Result Men.
        /// </summary>
        AcademicVisitorSearchResultMen,
        /// <summary>
        /// Academic visitor Search Result Women.
        /// </summary>
        AcademicVisitorSearchResultWomen,
        /// <summary>
        /// Sectors search results
        /// </summary>
        SectorSearchResult,
        /// <summary>
        /// None.
        /// </summary>
        None
    }

    public class ResultListHandler
    {
        private readonly IResultList resultList;

        private ContextGlobal g;

        public const string CHAPTER_SPACER = "&nbsp;/&nbsp;";
        public const string QSPARAM_STARTROW = "StartRow";
        public const string QSPARAM_RESOURCECONSTANT = "rc";
        public const String SESSIONOBJ_GALLERYVIEW = "IsGalleryViewVal";
        public Dictionary<int, int> MatchPercentages = new Dictionary<int, int>();
        private static SettingsManager _settingsManager;

        public static SettingsManager SettingsManagerObj
        {
            get
            {
                if (null == _settingsManager) _settingsManager = new SettingsManager();
                return _settingsManager;
            }
            set { _settingsManager = value; }
        }

        public static int GetReverseSearchMinimumResults(ContextGlobal contextGlobal)
        {
            return SettingsManagerObj.GetSettingInt(SettingConstants.REVERSE_SEARCH_MIN_RESULTS, contextGlobal.Brand);
        }


        public ResultListHandler(ContextGlobal context, IResultList resList)
        {
            resultList = resList;
            g = context;
        }

        #region Hot List specific methods
        public bool LoadHotListPage()
        {
            bool loadedHotlistPage = true;
            switch (resultList.HotListCategory)
            {
                case HotListCategory.WhoTeasedYou:
                case HotListCategory.WhoEmailedYou:
                case HotListCategory.WhoIMedYou:
                case HotListCategory.WhoSentYouECards:
                    if (g.Brand.Site.Community.CommunityID != (int)WebConstants.COMMUNITY_ID.Cupid &&
                        g.Brand.Site.SiteID != (int)WebConstants.SITE_ID.JDateCoIL &&
                        g.Brand.Site.SiteID != (int)WebConstants.SITE_ID.AmericanSingles)
                    {
                        // Issue #13777: Added back the condition for JdateCoIL from this section of code.
                        if (!g.ValidatePageView())
                        {
                            // Subscription required to view this hot list.

                            //g.Transfer(FrameworkGlobals.GetSubscriptionLink(false));

                            int iHotlistCategoryID = 0;

                            try
                            {
                                iHotlistCategoryID = Convert.ToInt32(HttpContext.Current.Request["CategoryID"]);
                            }
                            catch (Exception ex)
                            {

                            }

                            int prtid = Constants.NULL_INT;
                            bool subredirect = true;
                            switch (iHotlistCategoryID)
                            {
                                case (int)HotListCategory.WhoEmailedYou:
                                    prtid = (int)Constants.PRTID.HotlistWhoseEmailedYou;
                                    break;
                                case (int)Constants.HotlistCatID.FlirtedWithYou:
                                    prtid = (int)Constants.PRTID.HotlistWhoFlirtedWithYou;
                                    break;
                                case (int)HotListCategory.WhoIMedYou:
                                    prtid = (int)PurchaseReasonType.AttemptToAccessWhosIMdYou;
                                    break;
                                case (int)HotListCategory.WhoSentYouECards:
                                    //07/20/2010  we are adding this restriction but requirements was to
                                    //restrict only if site is JDate
                                    prtid = (int)PurchaseReasonType.AttemptToAccessWhoSentYouECard;
                                    subredirect = g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.JDate;
                                    break;

                            }

                            if (subredirect)
                            {
                                loadedHotlistPage = false;
                                Redirect.Subscription(g.Brand, prtid,
                                                      g.Session.GetInt(WebConstants.SESSION_PROPERTY_NAME_MEMBERID),
                                                      false,
                                                      HttpContext.Current.Server.UrlEncode(
                                                          HttpContext.Current.Items["FullURL"].ToString()));
                                 
                            }
                        }
                    }
                    break;
            }

            if (g.Member != null)
            {
                //get list view type from member attribute
                Matchnet.Web.Framework.Ui.BasicElements.ResultsViewType.ListViewType lvType =
                    ResultsViewType.GetMemberListType(g);
                if (lvType == Matchnet.Web.Framework.Ui.BasicElements.ResultsViewType.ListViewType.gallery)
                    resultList.GalleryView = "true";
            }
            else
            {
                if (g.Session["GalleryView"] != null && g.Session["GalleryView"].ToString() == "true")
                {
                    resultList.GalleryView = "true";
                }
            }

            return loadedHotlistPage;
        }

        

        public void RenderHotListElements()
        {
            int communityID = g.Brand.Site.Community.CommunityID;
            int siteID = g.Brand.Site.SiteID;
            int totalrows = 0;
            resultList.TargetMemberIDs = g.List.GetListMembers(resultList.HotListCategory, communityID, siteID,
                                                               resultList.StartRow, resultList.PageSize, out totalrows);
            resultList.TotalRows = totalrows;

        }

        #endregion
        private static ArrayList GetMembersFromResults(MatchnetQueryResults searchResults, ContextGlobal context)
        {
            return MemberDTOManager.Instance.GetIMemberDTOsFromResults(searchResults, context, MemberType.Search, MemberLoadFlags.None);
        }


        public void LoadMembersOnlinePage()
        {
            DropDownList ddlSearchOption = resultList.GetMOLSortDropDown();
            SortFieldType sortField;
            SortDirectionType sortDirection;

            if (SettingsManagerObj.GetSettingBool("PHOTO_REQUIRED_SITE", g.Brand.Site.Community.CommunityID,
                                                             g.Brand.Site.SiteID, g.Brand.BrandID))
            {
                sortField = SortFieldType.HasPhoto;
            }
            else
            {
                try
                {
                    sortField = (SortFieldType)Enum.Parse(typeof(SortFieldType), ddlSearchOption.SelectedValue);
                }
                catch // Throws exception if bad enum type or null value, so use default
                {
                    sortField = SortFieldType.InsertDate;
                }
            }

            switch (sortField)
            {
                case SortFieldType.Age:
                case SortFieldType.HasPhoto:
                case SortFieldType.InsertDate:
                    sortDirection = SortDirectionType.Desc;
                    break;
                default:
                    sortDirection = SortDirectionType.Asc;
                    break;
            }

            Int16 genderMask = 0;

            if (resultList.MyMOCollection.GenderMask != Constants.NULL_INT)
            {
                genderMask = (Int16)resultList.MyMOCollection.GenderMask;
            }
            string regionchanged = g.Session.Get("MembersOnlineRegionChanged") != null
                                       ? g.Session.Get("MembersOnlineRegionChanged").ToString()
                                       : "false";
            if (regionchanged == "true")
            {
                resultList.StartRow = 1;
                g.Session.Remove("MembersOnlineRegionChanged");
            }
            Matchnet.MembersOnline.ValueObjects.MOLQueryResult MOLResults =
                MembersOnlineSA.Instance.GetMemberIDs(g.Session.Key.ToString(),
                                                      g.Brand.Site.Community.CommunityID,
                                                      genderMask,
                                                      resultList.MyMOCollection.RegionID,
                                                      (Int16)resultList.MyMOCollection.AgeMin,
                                                      (Int16)((Int16)resultList.MyMOCollection.AgeMax + 1),
                                                      resultList.MyMOCollection.LanguageMask,
                                                      sortField,
                                                      sortDirection,
                                                      resultList.StartRow - 1,
                                                      resultList.PageSize,
                                                      g.Member == null ? Constants.NULL_INT : g.Member.MemberID);

            ArrayList members = MemberSA.Instance.GetMembers(MOLResults.ToArrayList(), MemberLoadFlags.None);

            BindMemberList(members);
            resultList.RenderAnalytics(members, (MOLResults.TotalMembersOnline > MOLResults.MembersReturned));
            resultList.TotalRows = MOLResults.TotalMembersOnline;
        }

        public void LoadMembersOnlinePageConsolidated(bool ignoreSAFilteredCacheOnly, bool ignoreSACache, bool ignoreAllCache)
        {
            g.MembersOnlineSearchPreferences["DomainID"] = g.TargetCommunityID.ToString();

            MatchnetQueryResults searchResults = null;

            if (!PrefsHaveValue(g.MembersOnlineSearchPreferences, "SearchTypeID"))
            {
                g.MembersOnlineSearchPreferences["SearchTypeID"] = g.Brand.Site.DefaultSearchTypeID.ToString();
            }

            if (!PrefsHaveValue(g.MembersOnlineSearchPreferences, "RegionID"))
            {
                g.MembersOnlineSearchPreferences["RegionID"] = g.Brand.Site.DefaultRegionID.ToString();
            }

            if (!PrefsHaveValue(g.MembersOnlineSearchPreferences, "MinAge"))
            {
                g.MembersOnlineSearchPreferences["MinAge"] = g.Brand.DefaultAgeMin.ToString();
            }

            if (!PrefsHaveValue(g.MembersOnlineSearchPreferences, "MaxAge"))
            {
                g.MembersOnlineSearchPreferences["MaxAge"] = g.Brand.DefaultAgeMax.ToString();
            }
            if (!PrefsHaveValue(g.MembersOnlineSearchPreferences, "Distance"))
            {
                g.MembersOnlineSearchPreferences["Distance"] = g.Brand.DefaultSearchRadius.ToString();
            }

            //pass flag to determine if search redesign is enabled
            g.MembersOnlineSearchPreferences.Add("SearchRedesign30", "1");


            try
            {
                searchResults = MemberSearchSA.Instance.Search(g.MembersOnlineSearchPreferences
                                                               , g.Brand.Site.Community.CommunityID
                                                               , g.Brand.Site.SiteID
                                                               , resultList.StartRow - 1
                                                               , resultList.PageSize
                                                               ,
                                                               g.Member == null ? Constants.NULL_INT : g.Member.MemberID
                                                               , Matchnet.Search.Interfaces.SearchEngineType.FAST
                                                               ,
                                                               Matchnet.Search.Interfaces.SearchType.MembersOnline
                                                               ,
                                                               Matchnet.Search.Interfaces.SearchEntryPoint.None
                                                               , ignoreSACache
                                                               , ignoreSAFilteredCacheOnly
                                                               , ignoreAllCache
                    );
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
                g.Transfer("/Applications/Home/Default.aspx");
            }

            ArrayList members = GetMembersFromResults(searchResults, g);

            BindMemberList(members);
            resultList.TotalRows = searchResults.MatchesFound;
        }


        public void LoadSearchResultPage()
        {
            LoadSearchResultPage(false, false, false);
        }

        public void LoadSearchResultPage(bool ignoreSAFilteredCacheOnly, bool ignoreSACache, bool ignoreAllCache)
        {
            g.SearchPreferences["DomainID"] = g.TargetCommunityID.ToString();

            MatchnetQueryResults searchResults = null;

            int memberId = (g.Member != null) ? g.Session.GetInt(WebConstants.SESSION_PROPERTY_NAME_MEMBERID) : 0;

            // default several required search preferences for access without cookies
            if (memberId == 0 ||
                (g.Session.GetString(VisitorLimitHelper.SESSIONKEY_VISITORLIMIT_DONOTOVERWRITESEARCHPREFS, String.Empty) ==
                 "true"))
            {
                if (!PrefsHaveValue(g.SearchPreferences, "SearchTypeID"))
                {
                    g.SearchPreferences["SearchTypeID"] = g.Brand.Site.DefaultSearchTypeID.ToString();
                }

                if (!PrefsHaveValue(g.SearchPreferences, "RegionID"))
                {
                    g.SearchPreferences["RegionID"] = g.Brand.Site.DefaultRegionID.ToString();
                }

                if (!PrefsHaveValue(g.SearchPreferences, "GenderMask"))
                {
                    // set the gender mask to M s M and M s F for all others
                    if (g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.Glimpse)
                    {
                        g.SearchPreferences["GenderMask"] =
                            (ConstantsTemp.GENDERID_MALE + ConstantsTemp.GENDERID_SEEKING_MALE).ToString();
                    }
                    else
                    {
                        g.SearchPreferences["GenderMask"] =
                            (ConstantsTemp.GENDERID_MALE + ConstantsTemp.GENDERID_SEEKING_FEMALE).ToString();
                    }
                }

                if (!PrefsHaveValue(g.SearchPreferences, "MinAge"))
                {
                    g.SearchPreferences["MinAge"] = g.Brand.DefaultAgeMin.ToString();
                }

                if (!PrefsHaveValue(g.SearchPreferences, "MaxAge"))
                {
                    g.SearchPreferences["MaxAge"] = g.Brand.DefaultAgeMax.ToString();
                }
                if (!PrefsHaveValue(g.SearchPreferences, "Distance"))
                {
                    g.SearchPreferences["Distance"] = g.Brand.DefaultSearchRadius.ToString();
                }
            }

            if (g.SearchPreferences["CountryRegionID"] == string.Empty)
            {
                g.SearchPreferences.Add("CountryRegionID", g.Brand.Site.DefaultRegionID.ToString());
            }

            // if this is a photo required site, modify the QuickSearchPreferences object to return members with photos only
            if (SettingsManagerObj.GetSettingBool("PHOTO_REQUIRED_SITE", g.Brand.Site.Community.CommunityID,
                                                             g.Brand.Site.SiteID, g.Brand.BrandID))
            {
                g.QuickSearchPreferences["HasPhotoFlag"] = "1";
            }
            System.Diagnostics.Trace.WriteLine("SearchOrderBy:" + g.SearchPreferences["SearchOrderBy"]);
            if (Conversion.CInt(g.SearchPreferences["SearchOrderBy"]) ==
                (int)Matchnet.Search.Interfaces.QuerySorting.ColorCode)
            {
                if (String.IsNullOrEmpty(g.Session.GetString("COLORCODE_SEARCH_MASK")))
                {
                    g.Session.Add("COLORCODE_SEARCH_MASK", ((int)ColorCodeSelect.ColorCodeAll).ToString(),
                                  SessionPropertyLifetime.Temporary);
                }
                g.SearchPreferences.Add("ColorCode", g.Session.GetString("COLORCODE_SEARCH_MASK"));
            }
            else
            {
                g.SearchPreferences.Add("ColorCode", "");
            }
            System.Diagnostics.Trace.WriteLine("SearchPrefColorCode:" + g.SearchPreferences["ColorCode"]);

            //pass flag to determine if search redesign is enabled
            if (BetaHelper.IsSearchRedesign30Enabled(g))
            {
                g.SearchPreferences.Add("SearchRedesign30", "1");
            }
            else
            {
                g.SearchPreferences.Add("SearchRedesign30", "0");
            }

            //wlybrand - Redirect to search prefs if the user does not have a valid set of preferences.  This is needed to avoid exceptions from Search.
            try
            {
                // Exception thrown if search prefernce validation fails.
                g.SaveSearchPreferences();
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
                g.Transfer("/Applications/Search/SearchPreferences.aspx");
            }

            // TT13887 - Redirect to search prefs if the user does not have a valid set of preferences.  This is needed to avoid exceptions from Search.
            try
            {
                searchResults = MemberSearchSA.Instance.Search(g.SearchPreferences
                                                               , g.Brand.Site.Community.CommunityID
                                                               , g.Brand.Site.SiteID
                                                               , resultList.StartRow - 1
                                                               , resultList.PageSize
                                                               ,
                                                               g.Member == null ? Constants.NULL_INT : g.Member.MemberID
                                                               , Matchnet.Search.Interfaces.SearchEngineType.FAST
                                                               ,
                                                               Matchnet.Search.Interfaces.SearchType.
                                                                   YourMatchesWebSearch
                                                               ,
                                                               Matchnet.Search.Interfaces.SearchEntryPoint.
                                                                   YourMatchesPage
                                                               , ignoreSACache
                                                               , ignoreSAFilteredCacheOnly
                                                               , ignoreAllCache
                    );
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
                g.Transfer("/Applications/Search/SearchPreferences.aspx");
            }

            // RegionID may sometimes be null
            string regionID = g.SearchPreferences["RegionID"];
            if (regionID == null)
            {
                regionID = string.Empty;
            }

            // Store necessary values in session cookie for use in pixels.
            g.Session.Add("PixelGenderMask_SearchPref", g.SearchPreferences["GenderMask"],
                          SessionPropertyLifetime.Persistent);
            g.Session.Add("PixelRegionID_SearchPref", regionID, SessionPropertyLifetime.Persistent);
            g.Session.Add("PixelMinAge_SearchPref", g.SearchPreferences["MinAge"], SessionPropertyLifetime.Persistent);
            g.Session.Add("PixelMaxAge_SearchPref", g.SearchPreferences["MaxAge"], SessionPropertyLifetime.Persistent);

            ArrayList members = GetMembersFromResults(searchResults, g);
            if (members != null && members.Count > 0)
            {
                resultList.SearchIDs = new List<int>();
                for (int i = 0; i < members.Count; i++)
                {
                    resultList.SearchIDs.Add(((IMemberDTO)members[i]).MemberID);
                }

                if (g.Member != null)
                {
                    foreach (IMatchnetResultItem resultItem in searchResults.Items.List)
                    {
                        if (!MatchPercentages.ContainsKey(resultItem.MemberID))
                        {
                            MatchPercentages.Add(resultItem.MemberID, resultItem.MatchScore);
                        }
                    }
                }
            }
            BindMemberList(members);
            resultList.RenderAnalytics(members, searchResults.MoreResultsAvailable);
            resultList.TotalRows = searchResults.MatchesFound;
        }

        public static string GetYourMatchesCacheKey(ContextGlobal context)
        {
            return context.Member.MemberID + "-yourmatchesIds";
        }

        public static ArrayList GetYourMatchesIds(ContextGlobal context, int maxMatchCount)
        {
            MatchnetQueryResults searchResults = null;
            int memberId = (context.Member != null)
                               ? context.Session.GetInt(WebConstants.SESSION_PROPERTY_NAME_MEMBERID)
                               : 0;
            //Ensure that domainID is set on search preferences for the correct Community.
            context.SearchPreferences.Add("DomainID", context.Brand.Site.Community.CommunityID.ToString());
            context.SearchPreferences.Add("ColorCode", "");
            //pass flag to determine if search redesign is enabled
            if (BetaHelper.IsSearchRedesign30Enabled(context))
            {
                context.SearchPreferences.Add("SearchRedesign30", "1");
            }
            else
            {
                context.SearchPreferences.Add("SearchRedesign30", "0");
            }

            try
            {

                searchResults = MemberSearchSA.Instance.Search(context.SearchPreferences
                                                               , context.Brand.Site.Community.CommunityID
                                                               , context.Brand.Site.SiteID
                                                               , 0
                                                               , maxMatchCount
                                                               ,
                                                               context.Member == null
                                                                   ? Constants.NULL_INT
                                                                   : context.Member.MemberID
                                                               , false
                                                               ,
                                                               Matchnet.Search.Interfaces.SearchType.
                                                                   YourMatchesWebSearch);
            }
            catch (Exception ex)
            {
                context.ProcessException(ex);
            }

            if (null != searchResults && searchResults.Items.Count > 0)
            {
                string key = GetYourMatchesCacheKey(context);
                context.Cache.Add(key, searchResults.ToArrayList(), null, DateTime.Now.Add((new TimeSpan(8, 0, 0))),
                                  Cache.NoSlidingExpiration, CacheItemPriority.Normal, null);
                return searchResults.ToArrayList();
            }
            else
            {
                return null;
            }

        }

        public void LoadReverseSearchResultPage()
        {
            LoadReverseSearchResultPage(false, false, false);
        }

        public void LoadReverseSearchResultPage(bool ignoreSAFilteredCacheOnly, bool ignoreSACache, bool ignoreAllCache)
        {
            MatchnetQueryResults searchResults = null;
            var reverseSearchPrefs = CreateReverseSearchPreferences(g, resultList.PhotoOnly);

            try
            {
                searchResults = MemberSearchSA.Instance.Search(reverseSearchPrefs
                                                               , g.Brand.Site.Community.CommunityID
                                                               , g.Brand.Site.SiteID
                                                               , resultList.StartRow - 1
                                                               , resultList.PageSize
                                                               ,
                                                               g.Member == null ? Constants.NULL_INT : g.Member.MemberID
                                                               , Matchnet.Search.Interfaces.SearchEngineType.FAST
                                                               ,
                                                               Matchnet.Search.Interfaces.SearchType.
                                                                   ReverseSearchWebSearch
                                                               ,
                                                               Matchnet.Search.Interfaces.SearchEntryPoint.
                                                                   ReverseSearchPage
                                                               , ignoreSACache
                                                               , ignoreSAFilteredCacheOnly
                                                               , ignoreAllCache
                    );
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
                g.Transfer("/Applications/QuickSearch/QuickSearch.aspx");
            }

            //if not enough results, run secondary search with less restrictions
            if (null == searchResults || searchResults.MatchesFound < GetReverseSearchMinimumResults(g))
            {
                ConvertToLaxReverseSearchPreferences(reverseSearchPrefs);

                try
                {
                    searchResults = MemberSearchSA.Instance.Search(reverseSearchPrefs
                                                                   , g.Brand.Site.Community.CommunityID
                                                                   , g.Brand.Site.SiteID
                                                                   , resultList.StartRow - 1
                                                                   , resultList.PageSize
                                                                   ,
                                                                   g.Member == null
                                                                       ? Constants.NULL_INT
                                                                       : g.Member.MemberID
                                                                   , Matchnet.Search.Interfaces.SearchEngineType.FAST
                                                                   ,
                                                                   Matchnet.Search.Interfaces.SearchType.
                                                                       ReverseSearchWebSearch
                                                                   ,
                                                                   Matchnet.Search.Interfaces.SearchEntryPoint.
                                                                       ReverseSearchPage
                                                                   , ignoreSACache
                                                                   , ignoreSAFilteredCacheOnly
                                                                   , ignoreAllCache
                        );
                }
                catch (Exception ex)
                {
                    g.ProcessException(ex);
                    g.Transfer("/Applications/QuickSearch/QuickSearch.aspx");
                }
            }

            ArrayList members = GetMembersFromResults(searchResults, g);

            if (members != null && members.Count > 0)
            {
                resultList.SearchIDs = new List<int>();
                for (int i = 0; i < members.Count; i++)
                {
                    resultList.SearchIDs.Add(((IMemberDTO)members[i]).MemberID);
                }

            }
            BindMemberList(members);
            resultList.RenderAnalytics(members, searchResults.MoreResultsAvailable);
            resultList.TotalRows = searchResults.MatchesFound;
        }

        public static void ConvertToLaxReverseSearchPreferences(SearchPreferenceCollection reverseSearchPrefs)
        {
            if (reverseSearchPrefs.ContainsKey("preferredsynagogueattendance"))
                reverseSearchPrefs.Add("preferredsynagogueattendance", string.Empty);
            if (reverseSearchPrefs.ContainsKey("preferredjdatereligion"))
                reverseSearchPrefs.Add("preferredjdatereligion", string.Empty);
            if (reverseSearchPrefs.ContainsKey("preferredsmokinghabit"))
                reverseSearchPrefs.Add("preferredsmokinghabit", string.Empty);
            if (reverseSearchPrefs.ContainsKey("preferreddrinkinghabit"))
                reverseSearchPrefs.Add("preferreddrinkinghabit", string.Empty);
            if (reverseSearchPrefs.ContainsKey("preferredkosherstatus"))
                reverseSearchPrefs.Add("preferredkosherstatus", string.Empty);
            if (reverseSearchPrefs.ContainsKey("preferrededucationlevel"))
                reverseSearchPrefs.Add("preferrededucationlevel", string.Empty);
            if (reverseSearchPrefs.ContainsKey("preferredactivitylevel"))
                reverseSearchPrefs.Add("preferredactivitylevel", string.Empty);
            if (reverseSearchPrefs.ContainsKey("preferredmorechildrenflag"))
                reverseSearchPrefs.Add("preferredmorechildrenflag", string.Empty);
            if (reverseSearchPrefs.ContainsKey("preferredlanguagemask"))
                reverseSearchPrefs.Add("preferredlanguagemask", string.Empty);
            if (reverseSearchPrefs.ContainsKey("preferredheight"))
                reverseSearchPrefs.Add("preferredheight", string.Empty);
            if (reverseSearchPrefs.ContainsKey("preferredjdateethnicity"))
                reverseSearchPrefs.Add("preferredjdateethnicity", string.Empty);
            if (reverseSearchPrefs.ContainsKey("preferredrelocation"))
                reverseSearchPrefs.Add("preferredrelocation", string.Empty);
            if (reverseSearchPrefs.ContainsKey("preferreddistance")) reverseSearchPrefs.Add("preferreddistance", "160");
        }

        public static SearchPreferenceCollection CreateReverseSearchPreferences(ContextGlobal contextGlobal,
                                                                                bool photoOnly)
        {
            //this method needs a valid member to proceed
            if (contextGlobal.Member == null) return null;
            
            SearchPreferenceCollection reverseSearchPrefs = new SearchPreferenceCollection();
            reverseSearchPrefs.Add("DomainID", contextGlobal.Brand.Site.Community.CommunityID.ToString());
            reverseSearchPrefs.Add("searchorderby", contextGlobal.SearchPreferences["SearchOrderBy"]);
            reverseSearchPrefs.Add("searchtypeid", contextGlobal.SearchPreferences["SearchTypeID"]);
            reverseSearchPrefs.Add("hasphotoflag", ((photoOnly) ? "1" : ""));
            reverseSearchPrefs.Add("preferredage",
                                   FrameworkGlobals.GetAge(contextGlobal.Member, contextGlobal.Brand).ToString());
            try
            {
                int genderMask = contextGlobal.Member.GetAttributeInt(contextGlobal.Brand,
                                                                      Web.Framework.Search.Constants.Attributes.
                                                                          GenderMask);
                reverseSearchPrefs.Add("preferredgendermask", genderMask.ToString());
            }
            catch (Exception ge)
            {
                contextGlobal.ProcessException(ge);
            }

            if (contextGlobal.SearchPreferences["SearchTypeID"] == ((int)SearchTypeID.AreaCode).ToString())
            {
                RegionAreaCodeDictionary areaCodeDictionary = RegionSA.Instance.RetrieveAreaCodesByRegion();
                ArrayList areaCodes =
                    areaCodeDictionary[
                        contextGlobal.Member.GetAttributeInt(contextGlobal.Brand, Search.Constants.Attributes.RegionId)];
                reverseSearchPrefs.Add("preferredareacodes",
                                       string.Join(",",
                                                   Array.ConvertAll(areaCodes.ToArray(),
                                                                    p => (p ?? String.Empty).ToString())));
            }
            else
            {
                reverseSearchPrefs.Add("preferredregionid",
                                       contextGlobal.Member.GetAttributeInt(contextGlobal.Brand,
                                                                            Search.Constants.Attributes.RegionId).
                                           ToString());
                reverseSearchPrefs.Add("preferreddistance", contextGlobal.SearchPreferences["Distance"]);
            }

            int synagogueAttendance = contextGlobal.Member.GetAttributeInt(contextGlobal.Brand,
                                                                           Search.Constants.Attributes.
                                                                               SynagogueAttendance);
            if (synagogueAttendance > 0)
            {
                reverseSearchPrefs.Add("preferredsynagogueattendance", synagogueAttendance.ToString());
            }

            int jdateReligion = contextGlobal.Member.GetAttributeInt(contextGlobal.Brand,
                                                                     Search.Constants.Attributes.JDateReligion);
            if (jdateReligion > 0)
            {
                reverseSearchPrefs.Add("preferredjdatereligion", jdateReligion.ToString());
            }

            int smokingHabits = contextGlobal.Member.GetAttributeInt(contextGlobal.Brand,
                                                                     Search.Constants.Attributes.SmokingHabits);
            if (smokingHabits > 0)
            {
                reverseSearchPrefs.Add("preferredsmokinghabit", smokingHabits.ToString());
            }

            int drinkingHabits = contextGlobal.Member.GetAttributeInt(contextGlobal.Brand,
                                                                      Search.Constants.Attributes.DrinkingHabits);
            if (drinkingHabits > 0)
            {
                reverseSearchPrefs.Add("preferreddrinkinghabit", drinkingHabits.ToString());
            }

            int keepKosher = contextGlobal.Member.GetAttributeInt(contextGlobal.Brand,
                                                                  Search.Constants.Attributes.KeepKosher);
            if (keepKosher > 0)
            {
                reverseSearchPrefs.Add("preferredkosherstatus", keepKosher.ToString());
            }

            int educationLevel = contextGlobal.Member.GetAttributeInt(contextGlobal.Brand,
                                                                      Search.Constants.Attributes.EducationLevel);
            if (educationLevel > 0)
            {
                reverseSearchPrefs.Add("preferrededucationlevel", educationLevel.ToString());
            }

            int activityLevel = contextGlobal.Member.GetAttributeInt(contextGlobal.Brand,
                                                                     Search.Constants.Attributes.ActivityLevel);
            if (activityLevel > 0)
            {
                reverseSearchPrefs.Add("preferredactivitylevel", activityLevel.ToString());
            }

            int moreChildrenFlag = contextGlobal.Member.GetAttributeInt(contextGlobal.Brand,
                                                                        Search.Constants.Attributes.HaveChildren);
            if (moreChildrenFlag > 0)
            {
                reverseSearchPrefs.Add("preferredmorechildrenflag", moreChildrenFlag.ToString());
            }

            int languageMask = contextGlobal.Member.GetAttributeInt(contextGlobal.Brand,
                                                                    Search.Constants.Attributes.LanguageMask);
            if (languageMask > 0)
            {
                reverseSearchPrefs.Add("preferredlanguagemask", languageMask.ToString());
            }

            int height = contextGlobal.Member.GetAttributeInt(contextGlobal.Brand, Search.Constants.Attributes.Height);
            if (height > 0)
            {
                reverseSearchPrefs.Add("preferredheight", height.ToString());
            }

            if (contextGlobal.Brand.Site.Community.CommunityID == (int)WebConstants.COMMUNITY_ID.JDate)
            {
                int preferredjdateethnicity = contextGlobal.Member.GetAttributeInt(contextGlobal.Brand,
                                                                            Search.Constants.Attributes.Ethnicity);
                if (preferredjdateethnicity >= 0)
                {
                    reverseSearchPrefs.Add("preferredjdateethnicity", preferredjdateethnicity.ToString());
                }
            }

            int preferredRelocation = contextGlobal.Member.GetAttributeInt(contextGlobal.Brand,
                                                                        Search.Constants.Attributes.WillingToRelocate);
            if (preferredRelocation >= 0)
            {
                reverseSearchPrefs.Add("preferredrelocation", preferredRelocation.ToString());
            }


            //pass flag to determine if search redesign is enabled
            if (BetaHelper.IsSearchRedesign30Enabled(contextGlobal))
            {
                reverseSearchPrefs.Add("SearchRedesign30", "1");
            }
            else
            {
                reverseSearchPrefs.Add("SearchRedesign30", "0");
            }
            return reverseSearchPrefs;
        }

        public void LoadKeywordSearchResultPage()
        {
            g.QuickSearchPreferences["DomainID"] = g.TargetCommunityID.ToString();

            MatchnetQueryResults searchResults = null;

            if (!PrefsHaveValue(g.QuickSearchPreferences, "SearchTypeID"))
            {
                g.QuickSearchPreferences["SearchTypeID"] = g.Brand.Site.DefaultSearchTypeID.ToString();
            }

            if (!PrefsHaveValue(g.QuickSearchPreferences, "RegionID"))
            {
                g.QuickSearchPreferences["RegionID"] = g.Brand.Site.DefaultRegionID.ToString();
            }

            if (!PrefsHaveValue(g.QuickSearchPreferences, "GenderMask"))
            {
                // set the gender mask to M s M and M s F for all others
                if (g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.Glimpse)
                {
                    g.QuickSearchPreferences["GenderMask"] =
                        (ConstantsTemp.GENDERID_MALE + ConstantsTemp.GENDERID_SEEKING_MALE).ToString();
                }
                else
                {
                    g.QuickSearchPreferences["GenderMask"] =
                        (ConstantsTemp.GENDERID_MALE + ConstantsTemp.GENDERID_SEEKING_FEMALE).ToString();
                }
            }

            if (!PrefsHaveValue(g.QuickSearchPreferences, "MinAge"))
            {
                g.QuickSearchPreferences["MinAge"] = g.Brand.DefaultAgeMin.ToString();
            }

            if (!PrefsHaveValue(g.QuickSearchPreferences, "MaxAge"))
            {
                g.QuickSearchPreferences["MaxAge"] = g.Brand.DefaultAgeMax.ToString();
            }
            if (!PrefsHaveValue(g.QuickSearchPreferences, "Distance"))
            {
                g.QuickSearchPreferences["Distance"] = g.Brand.DefaultSearchRadius.ToString();
            }

            if (g.QuickSearchPreferences["CountryRegionID"] == string.Empty)
            {
                g.QuickSearchPreferences.Add("CountryRegionID", g.Brand.Site.DefaultRegionID.ToString());
            }

            //pass flag to determine if search redesign is enabled
            if (BetaHelper.IsSearchRedesign30Enabled(g))
            {
                g.QuickSearchPreferences.Add("SearchRedesign30", "1");
            }
            else
            {
                g.QuickSearchPreferences.Add("SearchRedesign30", "0");
            }


            try
            {
                searchResults = MemberSearchSA.Instance.Search(g.QuickSearchPreferences
                                                               , g.Brand.Site.Community.CommunityID
                                                               , g.Brand.Site.SiteID
                                                               , resultList.StartRow - 1
                                                               , resultList.PageSize
                                                               ,
                                                               g.Member == null ? Constants.NULL_INT : g.Member.MemberID
                                                               , Matchnet.Search.Interfaces.SearchEngineType.FAST
                                                               ,
                                                               Matchnet.Search.Interfaces.SearchType.
                                                                   KeywordSearchWebSearch
                                                               ,
                                                               Matchnet.Search.Interfaces.SearchEntryPoint.
                                                                   KeywordSearchPage
                                                               , false
                                                               , false
                                                               , false
                    );

            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
                g.Transfer("/Applications/QuickSearch/QuickSearch.aspx");
            }

            // RegionID may sometimes be null
            string regionID = g.QuickSearchPreferences["RegionID"];

            if (regionID == null)
            {
                regionID = string.Empty;
            }

            var members = MemberSA.Instance.GetMembers(searchResults.ToArrayList(), MemberLoadFlags.None);

            BindMemberList(members);

            resultList.TotalRows = searchResults.MatchesFound;
        }

        public void LoadQuickSearchResultPage()
        {
            LoadQuickSearchResultPage(false, false, false);
        }

        public void LoadQuickSearchResultPage(bool ignoreSAFilteredCacheOnly, bool ignoreSACache, bool ignoreAllCache)
        {
            g.QuickSearchPreferences["DomainID"] = g.TargetCommunityID.ToString();

            MatchnetQueryResults searchResults = null;

            if (!PrefsHaveValue(g.QuickSearchPreferences, "SearchTypeID"))
            {
                g.QuickSearchPreferences["SearchTypeID"] = g.Brand.Site.DefaultSearchTypeID.ToString();
            }

            if (!PrefsHaveValue(g.QuickSearchPreferences, "RegionID"))
            {
                g.QuickSearchPreferences["RegionID"] = g.Brand.Site.DefaultRegionID.ToString();
            }

            if (!PrefsHaveValue(g.QuickSearchPreferences, "GenderMask"))
            {
                // set the gender mask to M s M and M s F for all others
                if (g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.Glimpse)
                {
                    g.QuickSearchPreferences["GenderMask"] =
                        (ConstantsTemp.GENDERID_MALE + ConstantsTemp.GENDERID_SEEKING_MALE).ToString();
                }
                else
                {
                    g.QuickSearchPreferences["GenderMask"] =
                        (ConstantsTemp.GENDERID_MALE + ConstantsTemp.GENDERID_SEEKING_FEMALE).ToString();
                }
            }

            if (!PrefsHaveValue(g.QuickSearchPreferences, "MinAge"))
            {
                g.QuickSearchPreferences["MinAge"] = g.Brand.DefaultAgeMin.ToString();
            }

            if (!PrefsHaveValue(g.QuickSearchPreferences, "MaxAge"))
            {
                g.QuickSearchPreferences["MaxAge"] = g.Brand.DefaultAgeMax.ToString();
            }
            if (!PrefsHaveValue(g.QuickSearchPreferences, "Distance"))
            {
                g.QuickSearchPreferences["Distance"] = g.Brand.DefaultSearchRadius.ToString();
            }

            if (g.QuickSearchPreferences["CountryRegionID"] == string.Empty)
            {
                g.QuickSearchPreferences.Add("CountryRegionID", g.Brand.Site.DefaultRegionID.ToString());
            }

            // if this is a photo required site, modify the QuickSearchPreferences object to return members with photos only
            if (SettingsManagerObj.GetSettingBool("PHOTO_REQUIRED_SITE", g.Brand.Site.Community.CommunityID,
                                                             g.Brand.Site.SiteID, g.Brand.BrandID))
            {
                g.QuickSearchPreferences["HasPhotoFlag"] = "1";
            }


            if (PrefsHaveValue(g.QuickSearchPreferences, "JDateReligion"))
            {
                if (Conversion.CInt(g.QuickSearchPreferences["JDateReligion"]) ==
                    Matchnet.Web.Framework.Search.Constants.QS_JewishOnly_Unchecked)
                {
                    g.QuickSearchPreferences["JDateReligion"] = "";

                }
            }

            System.Diagnostics.Trace.WriteLine("QuickSearchOrderBy:" + g.QuickSearchPreferences["SearchOrderBy"]);
            if (Conversion.CInt(g.QuickSearchPreferences["SearchOrderBy"]) ==
                (int)Matchnet.Search.Interfaces.QuerySorting.ColorCode)
            {
                if (String.IsNullOrEmpty(g.Session.GetString("COLORCODE_SEARCH_MASK")))
                {
                    g.Session.Add("COLORCODE_SEARCH_MASK", ((int)ColorCodeSelect.ColorCodeAll).ToString(),
                                  SessionPropertyLifetime.Temporary);
                }
                g.QuickSearchPreferences.Add("ColorCode", g.Session.GetString("COLORCODE_SEARCH_MASK"));
                System.Diagnostics.Trace.WriteLine("QuickSearchColorCode:" + g.QuickSearchPreferences["ColorCode"]);
            }
            else
            {
                g.QuickSearchPreferences.Add("ColorCode", "");
                System.Diagnostics.Trace.WriteLine("QuickSearchColorCode:" + g.QuickSearchPreferences["ColorCode"]);
            }

            //pass flag to determine if search redesign is enabled
            if (BetaHelper.IsSearchRedesign30Enabled(g))
            {
                g.QuickSearchPreferences.Add("SearchRedesign30", "1");
            }
            else
            {
                g.QuickSearchPreferences.Add("SearchRedesign30", "0");
            }

            try
            {
                searchResults = MemberSearchSA.Instance.Search(g.QuickSearchPreferences
                                                               , g.Brand.Site.Community.CommunityID
                                                               , g.Brand.Site.SiteID
                                                               , resultList.StartRow - 1
                                                               , resultList.PageSize
                                                               ,
                                                               g.Member == null ? Constants.NULL_INT : g.Member.MemberID
                                                               , Matchnet.Search.Interfaces.SearchEngineType.FAST
                                                               ,
                                                               Matchnet.Search.Interfaces.SearchType.
                                                                   QuickSearchWebSearch
                                                               ,
                                                               Matchnet.Search.Interfaces.SearchEntryPoint.
                                                                   QuickSearchPage
                                                               , ignoreSACache
                                                               , ignoreSAFilteredCacheOnly
                                                               , ignoreAllCache
                    );
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
                g.Transfer("/Applications/QuickSearch/QuickSearch.aspx");
            }

            ArrayList members = GetMembersFromResults(searchResults, g);

            BindMemberList(members);
            resultList.TotalRows = searchResults.MatchesFound;
        }

        public void LoadAdvancedSearchResultPage()
        {
            LoadAdvancedSearchResultPage(false, false, false);
        }

        public void LoadAdvancedSearchResultPage(bool ignoreSAFilteredCacheOnly, bool ignoreSACache, bool ignoreAllCache)
        {
            g.AdvancedSearchPreferences["DomainID"] = g.TargetCommunityID.ToString();

            MatchnetQueryResults searchResults = null;

            int memberId = (g.Member != null) ? g.Session.GetInt(WebConstants.SESSION_PROPERTY_NAME_MEMBERID) : 0;

            // default several required search preferences for access without cookies
            if (memberId == 0 ||
                (g.Session.GetString(VisitorLimitHelper.SESSIONKEY_VISITORLIMIT_DONOTOVERWRITESEARCHPREFS, String.Empty) ==
                 "true"))
            {
                if (!PrefsHaveValue(g.AdvancedSearchPreferences, "SearchTypeID"))
                {
                    g.AdvancedSearchPreferences["SearchTypeID"] = g.Brand.Site.DefaultSearchTypeID.ToString();
                }

                if (!PrefsHaveValue(g.AdvancedSearchPreferences, "RegionID"))
                {
                    g.AdvancedSearchPreferences["RegionID"] = g.Brand.Site.DefaultRegionID.ToString();
                }

                if (!PrefsHaveValue(g.AdvancedSearchPreferences, "GenderMask"))
                {
                    // set the gender mask to M s M and M s F for all others
                    if (g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.Glimpse)
                    {
                        g.AdvancedSearchPreferences["GenderMask"] =
                            (ConstantsTemp.GENDERID_MALE + ConstantsTemp.GENDERID_SEEKING_MALE).ToString();
                    }
                    else
                    {
                        g.AdvancedSearchPreferences["GenderMask"] =
                            (ConstantsTemp.GENDERID_MALE + ConstantsTemp.GENDERID_SEEKING_FEMALE).ToString();
                    }
                }

                if (!PrefsHaveValue(g.AdvancedSearchPreferences, "MinAge"))
                {
                    g.AdvancedSearchPreferences["MinAge"] = g.Brand.DefaultAgeMin.ToString();
                }

                if (!PrefsHaveValue(g.AdvancedSearchPreferences, "MaxAge"))
                {
                    g.AdvancedSearchPreferences["MaxAge"] = g.Brand.DefaultAgeMax.ToString();
                }
                if (!PrefsHaveValue(g.AdvancedSearchPreferences, "Distance"))
                {
                    g.AdvancedSearchPreferences["Distance"] = g.Brand.DefaultSearchRadius.ToString();
                }
            }

            if (g.AdvancedSearchPreferences["CountryRegionID"] == string.Empty)
            {
                g.AdvancedSearchPreferences.Add("CountryRegionID", g.Brand.Site.DefaultRegionID.ToString());
            }

            // if this is a photo required site, modify the QuickSearchPreferences object to return members with photos only
            if (SettingsManagerObj.GetSettingBool("PHOTO_REQUIRED_SITE", g.Brand.Site.Community.CommunityID,
                                                             g.Brand.Site.SiteID, g.Brand.BrandID))
            {
                g.AdvancedSearchPreferences["HasPhotoFlag"] = "1";
            }
            System.Diagnostics.Trace.WriteLine("AdvancedSearchOrderBy:" + g.AdvancedSearchPreferences["SearchOrderBy"]);
            if (Conversion.CInt(g.AdvancedSearchPreferences["SearchOrderBy"]) ==
                (int)Matchnet.Search.Interfaces.QuerySorting.ColorCode)
            {
                if (String.IsNullOrEmpty(g.Session.GetString("COLORCODE_SEARCH_MASK")))
                {
                    g.Session.Add("COLORCODE_SEARCH_MASK", ((int)ColorCodeSelect.ColorCodeAll).ToString(),
                                  SessionPropertyLifetime.Temporary);
                }
                g.AdvancedSearchPreferences.Add("ColorCode", g.Session.GetString("COLORCODE_SEARCH_MASK"));
            }
            else
            {
                g.AdvancedSearchPreferences.Add("ColorCode", "");
            }
            System.Diagnostics.Trace.WriteLine("AdvancedSearchColorCode:" + g.AdvancedSearchPreferences["ColorCode"]);

            //pass flag to determine if search redesign is enabled
            if (BetaHelper.IsSearchRedesign30Enabled(g))
            {
                g.AdvancedSearchPreferences.Add("SearchRedesign30", "1");
            }
            else
            {
                g.AdvancedSearchPreferences.Add("SearchRedesign30", "0");
            }


            // TT13887 - Redirect to search prefs if the user does not have a valid set of preferences.  This is needed to avoid exceptions from Search.
            try
            {

                searchResults = MemberSearchSA.Instance.Search(g.AdvancedSearchPreferences
                                                               , g.Brand.Site.Community.CommunityID
                                                               , g.Brand.Site.SiteID
                                                               , resultList.StartRow - 1
                                                               , resultList.PageSize
                                                               ,
                                                               g.Member == null ? Constants.NULL_INT : g.Member.MemberID
                                                               , Matchnet.Search.Interfaces.SearchEngineType.FAST
                                                               ,
                                                               Matchnet.Search.Interfaces.SearchType.
                                                                   AdvancedSearchWebSearch
                                                               ,
                                                               Matchnet.Search.Interfaces.SearchEntryPoint.
                                                                   AdvancedSearchPage
                                                               , ignoreSACache
                                                               , ignoreSAFilteredCacheOnly
                                                               , ignoreAllCache
                    );
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
                g.Transfer("/Applications/AdvancedSearch/AdvancedSearch.aspx");
            }

            ArrayList members = GetMembersFromResults(searchResults, g);

            if (members != null)
            {
                resultList.SearchIDs = new List<int>();
                for (int i = 0; i < members.Count; i++)
                {
                    resultList.SearchIDs.Add(((IMemberDTO)members[i]).MemberID);
                }
            }

            BindMemberList(members);

            resultList.TotalRows = searchResults.MatchesFound;
        }

        public void LoadPhotoGalleryResultPage()
        {
            try
            {
                PhotoSearch.ValueObjects.PhotoGalleryQuery query;
                query = Applications.PhotoGallery.PhotoGalleryUtils.GetQueryFromSession(g.Brand, g.Member == null ? Constants.NULL_INT : g.Member.MemberID);
                if (query == null)
                {
                    return;
                }
                resultList.TotalRows = 0;
                query.SortBy = SortByType.newest;
                PhotoGalleryResultList photoResultList = PhotoGalleryManager.Instance.SearchPhotoGallery(g.Brand,
                                                                    g.Member == null ? Constants.NULL_INT : g.Member.MemberID,
                                                                    resultList.StartRow,
                                                                    resultList.PageSize,
                                                                    query,
                                                                    null);
                                                                   
                ArrayList res = photoResultList.PhotoGalleryResults;
                resultList.TotalRows = photoResultList.MatchesFound;

                resultList.MemberNum = res.Count;
                if (resultList.MemberNum > 0)
                {
                    resultList.GalleryMemberRepeater.Visible = false;
                    resultList.MemberRepeater.Visible = false;
                    resultList.PhotoGalleryRepeater.Visible = true;
                    resultList.PhotoGalleryRepeater.DataSource = res;
                    resultList.PhotoGalleryRepeater.DataBind();
                }
                else
                {
                    resultList.NoSearchResults.DisplayContext = NoResults.ContextType.PhotoGallery;
                    resultList.NoSearchResults.Visible = true;
                }

                query.ReloadFlag = PhotoSearch.ValueObjects.ReLoadFlags.load;
                Applications.PhotoGallery.PhotoGalleryUtils.SaveQueryToSession(query, g.Brand, g.Member != null ? g.Member.MemberID : int.MinValue);

            }
            catch (Exception ex)
            {
                g.ProcessException(ex);

            }
        }

        public void LoadMatchMeterMatchesResultPage(MatchesCollection matchesCollection,
                                                    ref Dictionary<IMemberDTO, decimal>
                                                        matchResultsMemberScoreDictionary, int startRow, int page_Size,
                                                    ref int totalRows)
        {
            try
            {
                Int16 genderMask = 0;

                if (matchesCollection != null)
                {
                    if (matchesCollection.GenderMask != Constants.NULL_INT)
                    {
                        genderMask = (Int16)matchesCollection.GenderMask;
                    }

                    var bmresults = MatchMeterSA.Instance.GetBestMatches(g.Member.MemberID, g.Brand,
                                                                         matchesCollection.RegionID,
                                                                         matchesCollection.AgeMin,
                                                                         matchesCollection.AgeMax,
                                                                         matchesCollection.GenderMask,
                                                                         MemberPrivilegeAttr.IsCureentSubscribedMember(g),
                                                                         startRow - 1, page_Size, matchesCollection.PhotoRequired);
                    if (bmresults != null && bmresults.MatchesCount > 0)
                    {
                        matchResultsMemberScoreDictionary = bmresults.MatchResultsDic;
                        ArrayList members = new ArrayList(matchResultsMemberScoreDictionary.Keys);
                        BindMemberList(members);
                        totalRows = bmresults.MatchesCount;
                    }
                    else
                    {
                        resultList.NoSearchResults.DisplayContext = NoResults.ContextType.MatchMeter;
                        resultList.NoSearchResults.Visible = true;
                    }
                }

                //renderAnalytics(members, (members.Count < bmresults.MatchesCount));
            }
            catch (Exception ex)
            {
                if (!g.IsDevMode && !(ex is System.Threading.ThreadAbortException))
                {
                    g.Transfer("/Applications/CompatibilityMeter/Error.aspx");
                }
                else
                {
                    throw ex;
                }
            }
        }

        public void BindMemberList(ArrayList members)
        {
            resultList.MemberNum = members.Count;
            if (resultList.MemberNum > 0)
            {
                if (resultList.GalleryView == "true")
                {
                    resultList.GalleryMemberRepeater.DataSource = members;
                    resultList.GalleryMemberRepeater.DataBind();
                    resultList.GalleryMemberRepeater.Visible = true;
                    resultList.MemberRepeater.Visible = false;
                }
                else
                {
                    if (resultList.ResultListContextType == ResultContextType.MatchMeterMatches)
                    {
                        resultList.JMeterRepeater.DataSource = members;
                        resultList.JMeterRepeater.DataBind();
                        resultList.MemberRepeater.Visible = false;
                        resultList.GalleryMemberRepeater.Visible = false;
                        resultList.JMeterRepeater.Visible = true;
                        if (resultList.EnableMultipleSearchViews)
                            resultList.GalleryMemberRepeater.Visible = false;
                    }
                    else
                    {
                        resultList.MemberRepeater.DataSource = members;
                        resultList.MemberRepeater.DataBind();
                        resultList.MemberRepeater.Visible = true;
                        if (resultList.EnableMultipleSearchViews)
                            resultList.GalleryMemberRepeater.Visible = false;
                    }
                }
            }
            else
            {
                resultList.MemberRepeater.Visible = false;
                if (resultList.EnableMultipleSearchViews)
                    resultList.GalleryMemberRepeater.Visible = false;

                resultList.NoSearchResults.Visible = true;
                if (resultList.ResultListContextType == ResultContextType.MembersOnline)
                {
                    resultList.NoSearchResults.DisplayContext = NoResults.ContextType.MembersOnline;
                    resultList.NoSearchResults.Visible = false;
                    resultList.PLCMembersOnlineNoResults.Visible = true;
                }
                else if (resultList.ResultListContextType == ResultContextType.QuickSearchResult)
                {
                    resultList.NoSearchResults.DisplayContext = NoResults.ContextType.QuickSearch;
                    resultList.PLCMembersOnlineNoResults.Visible = false;
                }
                else if (resultList.ResultListContextType == ResultContextType.AdvancedSearchResult)
                {
                    resultList.NoSearchResults.DisplayContext = NoResults.ContextType.AdvancedSearch;
                    resultList.PLCMembersOnlineNoResults.Visible = false;
                }
            }
        }

        public void BindMemberList(List<int> members)
        {
            ArrayList membersList = new ArrayList();
            for (int i = 0; i < members.Count; i++)
            {
                Member.ServiceAdapters.Member m = MemberSA.Instance.GetMember(members[i], MemberLoadFlags.None);
                if (m != null)
                    membersList.Add(m);

            }
            BindMemberList(membersList);
        }

        public void BindMemberRepeater()
        {
            DataTable dt = resultList.Dt;
            ArrayList targetMemberIDs = resultList.TargetMemberIDs;

            ArrayList members = MemberDTOManager.Instance.GetIMemberDTOs(g, targetMemberIDs, MemberType.Search,
                                                                         MemberLoadFlags.None);

            if (resultList.GalleryView == "true")
            {
                resultList.MemberNum = members.Count;
                resultList.GalleryMemberRepeater.DataSource = members;
                resultList.GalleryMemberRepeater.DataBind();
                resultList.GalleryMemberRepeater.Visible = true;
                resultList.MemberRepeater.Visible = false;
            }
            else
            {
                resultList.MemberRepeater.DataSource = members;
                resultList.MemberRepeater.DataBind();
                resultList.MemberRepeater.Visible = true;
                if (resultList.EnableMultipleSearchViews)
                    resultList.GalleryMemberRepeater.Visible = false;
            }

            if (members.Count == 0)
            {
                resultList.NoUsersText.Visible = true;
                resultList.NoUsersText.ExpandImageTokens = true;

                if (resultList.ResultListContextType == ResultContextType.HotList)
                {
                    switch (resultList.HotListCategory)
                    {
                        case HotListCategory.MutualYes: // corresponds to the You Clicked! hotlist
                            resultList.NoUsersText.ResourceConstant = "TXT_NO_MEMBERS_IN_YOU_CLICKED_LIST";
                            break;
                        case HotListCategory.MembersYouEmailed:
                            resultList.NoUsersText.ResourceConstant = "TXT_NO_MEMBERS_IN_YOU_EMAILED_LIST";
                            break;
                        case HotListCategory.Default: // need Members you Hotlisted
                            resultList.NoUsersText.ResourceConstant = "TXT_NO_MEMBERS_IN_YOU_HOTLISTED_LIST";
                            break;
                        case HotListCategory.MembersYouTeased: // i think this is same as Flirt
                            resultList.NoUsersText.ResourceConstant = "TXT_NO_MEMBERS_IN_YOU_FLIRTED_LIST";
                            break;
                        case HotListCategory.MembersYouIMed:
                            resultList.NoUsersText.ResourceConstant = "TXT_NO_MEMBERS_IN_YOU_IMED_LIST";
                            break;
                        case HotListCategory.MembersYouViewed:
                            resultList.NoUsersText.ResourceConstant = "TXT_NO_MEMBERS_IN_YOU_VIEWED_LIST";
                            break;
                        case HotListCategory.IgnoreList: // i think this is same as Blocked
                            resultList.NoUsersText.ResourceConstant = "TXT_NO_MEMBERS_IN_YOU_BLOCKED_LIST";
                            break;
                        case HotListCategory.WhoAddedYouToTheirFavorites:
                            resultList.NoUsersText.ResourceConstant = "TXT_NO_MEMBERS_IN_HOTLISTED_YOU_LIST";
                            break;
                        case HotListCategory.WhoTeasedYou:
                            resultList.NoUsersText.ResourceConstant = "TXT_NO_MEMBERS_IN_FLIRTED_YOU_LIST";
                            break;
                        case HotListCategory.WhoIMedYou:
                            resultList.NoUsersText.ResourceConstant = "TXT_NO_MEMBERS_IN_IMED_YOU_LIST";
                            break;
                        case HotListCategory.WhoEmailedYou:
                            resultList.NoUsersText.ResourceConstant = "TXT_NO_MEMBERS_IN_EMAILED_YOU_LIST";
                            break;
                        case HotListCategory.WhoViewedYourProfile:
                            resultList.NoUsersText.ResourceConstant = "TXT_NO_MEMBERS_IN_VIEWED_YOU_LIST";
                            break;
                        case HotListCategory.YourMatches:
                            resultList.NoUsersText.ResourceConstant = "TXT_NO_MEMBERS_IN_MATCHES_SENT_BY_EMAIL";
                            break;
                        case HotListCategory.MembersYouSentECards:
                            resultList.NoUsersText.ResourceConstant = "TXT_NO_MEMBERS_IN_YOU_ECARDED_LIST";
                            break;
                        case HotListCategory.WhoSentYouECards:
                            resultList.NoUsersText.ResourceConstant = "TXT_NO_MEMBERS_SENT_YOU_ECARDS_LIST";
                            break;
                        case HotListCategory.MemberYouJmetered:
                            resultList.NoUsersText.ResourceConstant = "TXT_NO_MEMBERS_YOU_JMETERED_LIST";
                            break;
                        case HotListCategory.WhoJmeteredYou:
                            resultList.NoUsersText.ResourceConstant = "TXT_NO_MEMBERS_JMETERD_YOU_LIST";
                            break;
                        case HotListCategory.WhoOmnidatedYou:
                            resultList.NoUsersText.ResourceConstant = "TXT_NO_MEMBERS_OMNIDATED_YOU_LIST";
                            break;
                        case HotListCategory.MembersYouOmnidated:
                            resultList.NoUsersText.ResourceConstant = "TXT_NO_MEMBERS_YOU_OMNIDATED_LIST";
                            break;
                        case HotListCategory.MembersClickedMaybe:
                        case HotListCategory.MembersClickedYes:
                        case HotListCategory.MembersClickedNo:
                            resultList.NoUsersText.ResourceConstant = "TXT_NO_MEMBERS_CLICKED_LIST";
                            break;
                        default: // if its not one of the Above then its a custom list
                            resultList.NoUsersText.ResourceConstant = "TXT_NO_MEMBERS_IN_CUSTOM_LIST";
                            //"YOU_DO_NOT_HAVE_ANY_MEMBERS_IN_THIS_LIST";
                            break;
                    }

                }
            }
        }

        public bool PrefsHaveValue(SearchPreferenceCollection prefs, string key)
        {
            bool retVal = false;
            string myValue = prefs.Value(key);
            if (!string.IsNullOrEmpty(myValue) && Conversion.CInt(myValue) != Constants.NULL_INT)
            {
                retVal = true;
            }

            return (retVal);
        }

        # region Render Paging

        public void RenderPaging()
        {
            //if made from an API request, apppage will be null, so skip this. 
            if (g.AppPage == null) return;
            
            
            if (HttpContext.Current.Request["TotalRows"] != null)
            {
                resultList.TotalRows = Convert.ToInt32(HttpContext.Current.Request["TotalRows"]);
            }

            // if the start row is greater than the totalrows, set the startrow to the nearest round page_size less than totalrows
            if (resultList.StartRow > resultList.TotalRows)
            {
                resultList.StartRow = (resultList.TotalRows / resultList.PageSize) * resultList.PageSize + 1;
            }


            int pageRequested = resultList.StartRow / resultList.PageSize + 1;

            // Set Visitor Limits.
            VisitorLimitHelper.CheckVisitorLimit(ref g, resultList.ResultListContextType, pageRequested,
                                                 resultList.StartRow);

            // there must be more rows than the page size to show paging
            if (resultList.TotalRows > resultList.PageSize)
            {
                // Add links for the ListNavigation (see SearchResults.ascx, MembersOnline.ascx, and Hotlist.View.ascx) if necessary.
                string pageName = g.AppPage.PageName;
                PagingHelper.LoadPageNavigation(g.ListNavigationTop, g.ListNavigationBottom, pageName,
                                                resultList.TotalRows, resultList.ChapterSize, resultList.PageSize,
                                                resultList.StartRow, g.GetResource("TXT_NEXT"),
                                                g.GetResource("TXT_PREVIOUS"));
            }

            if (g.ListNavigationTop != null)
            {
                //add hidden total
                Literal hidTotal = new Literal();
                hidTotal.Text = "<span id=\"spanPagingTotalRows\" style=\"display:none;\">" +
                                resultList.TotalRows.ToString() + "</span>";
                g.ListNavigationTop.Controls.Add(hidTotal);
            }
        }


        #endregion

        #region affiliationgroup

        public void LoadGroupMemberPage()
        {
            try
            {
                int groupid = Conversion.CInt(HttpContext.Current.Request["groupid"]);
                if (groupid <= 0)
                    return;
                resultList.TotalRows = 0;
                int totalrows = 0;
                int page = resultList.StartRow / resultList.PageSize + 1;
                List<int> members = AffiliationGroupMemberSA.Instance.GetGroupMembers(groupid, page, resultList.PageSize,
                                                                                      out totalrows);
                resultList.TotalRows = totalrows;
                if (members != null)
                    BindMemberList(members);

            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }

        }

        #endregion

        public void LoadAcademicVisitorMenResultPage()
        {
            g.SearchPreferences["DomainID"] = g.TargetCommunityID.ToString();

            MatchnetQueryResults searchResults = null;
            MatchnetQueryResults searchResultsMen = null;
            MatchnetQueryResults searchResultsWomen = null;

            if (!PrefsHaveValue(g.SearchPreferences, "SearchTypeID"))
            {
                g.SearchPreferences["SearchTypeID"] = g.Brand.Site.DefaultSearchTypeID.ToString();
            }

            if (!PrefsHaveValue(g.SearchPreferences, "RegionID"))
            {
                g.SearchPreferences["RegionID"] = g.Brand.Site.DefaultRegionID.ToString();
            }

            g.SearchPreferences["MinAge"] = "25";

            g.SearchPreferences["MaxAge"] = "36";

            if (!PrefsHaveValue(g.SearchPreferences, "Distance"))
            {
                g.SearchPreferences["Distance"] = g.Brand.DefaultSearchRadius.ToString();
            }


            if (g.SearchPreferences["CountryRegionID"] == string.Empty)
            {
                g.SearchPreferences.Add("CountryRegionID", g.Brand.Site.DefaultRegionID.ToString());
            }

            g.QuickSearchPreferences["HasPhotoFlag"] = "1";

            g.SearchPreferences.Add("ColorCode", "");

            g.SearchPreferences.Add("EducationLevel", "120");
            g.SearchPreferences.Add("AreaCode1", "03");

            g.SearchPreferences.Add("SearchRedesign30", "1");


            //wlybrand - Redirect to search prefs if the user does not have a valid set of preferences.  This is needed to avoid exceptions from Search.
            try
            {
                // Exception thrown if search prefernce validation fails.
                g.SaveSearchPreferences();
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
                g.Transfer("/Applications/Search/SearchPreferences.aspx");
            }




            g.SearchPreferences["GenderMask"] =
            (ConstantsTemp.GENDERID_MALE + ConstantsTemp.GENDERID_SEEKING_FEMALE).ToString();




            // TT13887 - Redirect to search prefs if the user does not have a valid set of preferences.  This is needed to avoid exceptions from Search.
            try
            {
                searchResultsMen = MemberSearchSA.Instance.Search(g.SearchPreferences
                                                                  , g.Brand.Site.Community.CommunityID
                                                                  , g.Brand.Site.SiteID
                                                                  , resultList.StartRow - 1
                                                                  , 4
                                                                  ,
                                                                  g.Member == null
                                                                      ? Constants.NULL_INT
                                                                      : g.Member.MemberID
                                                                  , Matchnet.Search.Interfaces.SearchEngineType.FAST
                                                                  ,
                                                                  Matchnet.Search.Interfaces.SearchType.
                                                                      YourMatchesWebSearch
                                                                  ,
                                                                  Matchnet.Search.Interfaces.SearchEntryPoint.
                                                                      YourMatchesPage
                                                                  , false
                                                                  , false
                                                                  , false
                    );
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
                g.Transfer("/Applications/Search/SearchPreferences.aspx");
            }





            // RegionID may sometimes be null
            string regionID = g.SearchPreferences["RegionID"];
            if (regionID == null)
            {
                regionID = string.Empty;
            }

            // Store necessary values in session cookie for use in pixels.
            g.Session.Add("PixelGenderMask_SearchPref", g.SearchPreferences["GenderMask"],
                          SessionPropertyLifetime.Persistent);
            g.Session.Add("PixelRegionID_SearchPref", regionID, SessionPropertyLifetime.Persistent);
            g.Session.Add("PixelMinAge_SearchPref", g.SearchPreferences["MinAge"], SessionPropertyLifetime.Persistent);
            g.Session.Add("PixelMaxAge_SearchPref", g.SearchPreferences["MaxAge"], SessionPropertyLifetime.Persistent);

            ArrayList membersMen = MemberSA.Instance.GetMembers(searchResultsMen.ToArrayList(), MemberLoadFlags.None);

            ArrayList members = membersMen;

            if (members != null && members.Count > 0)
            {
                resultList.SearchIDs = new List<int>();
                for (int i = 0; i < members.Count; i++)
                {
                    resultList.SearchIDs.Add(((Member.ServiceAdapters.Member)members[i]).MemberID);
                }

            }
            BindMemberList(members);
            resultList.RenderAnalytics(members, searchResults.MoreResultsAvailable);
            resultList.TotalRows = searchResults.MatchesFound;
        }

        public void LoadAcademicVisitorWomenResultPage()
        {
            g.SearchPreferences["DomainID"] = g.TargetCommunityID.ToString();

            MatchnetQueryResults searchResults = null;
            MatchnetQueryResults searchResultsMen = null;
            MatchnetQueryResults searchResultsWomen = null;

            if (!PrefsHaveValue(g.SearchPreferences, "SearchTypeID"))
            {
                g.SearchPreferences["SearchTypeID"] = g.Brand.Site.DefaultSearchTypeID.ToString();
            }

            if (!PrefsHaveValue(g.SearchPreferences, "RegionID"))
            {
                g.SearchPreferences["RegionID"] = g.Brand.Site.DefaultRegionID.ToString();
            }

            g.SearchPreferences["MinAge"] = "25";

            g.SearchPreferences["MaxAge"] = "36";

            if (!PrefsHaveValue(g.SearchPreferences, "Distance"))
            {
                g.SearchPreferences["Distance"] = g.Brand.DefaultSearchRadius.ToString();
            }


            if (g.SearchPreferences["CountryRegionID"] == string.Empty)
            {
                g.SearchPreferences.Add("CountryRegionID", g.Brand.Site.DefaultRegionID.ToString());
            }

            g.QuickSearchPreferences["HasPhotoFlag"] = "1";

            g.SearchPreferences.Add("ColorCode", "");

            g.SearchPreferences.Add("EducationLevel", "120");
            g.SearchPreferences.Add("AreaCode1", "03");

            g.SearchPreferences.Add("SearchRedesign30", "1");


            //wlybrand - Redirect to search prefs if the user does not have a valid set of preferences.  This is needed to avoid exceptions from Search.
            try
            {
                // Exception thrown if search prefernce validation fails.
                g.SaveSearchPreferences();
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
                g.Transfer("/Applications/Search/SearchPreferences.aspx");
            }

            g.SearchPreferences["GenderMask"] =
           (ConstantsTemp.GENDERID_FEMALE + ConstantsTemp.GENDERID_SEEKING_MALE).ToString();



            // TT13887 - Redirect to search prefs if the user does not have a valid set of preferences.  This is needed to avoid exceptions from Search.
            try
            {
                searchResultsWomen = MemberSearchSA.Instance.Search(g.SearchPreferences
                                                                  , g.Brand.Site.Community.CommunityID
                                                                  , g.Brand.Site.SiteID
                                                                  , resultList.StartRow - 1
                                                                  , 4
                                                                  ,
                                                                  g.Member == null
                                                                      ? Constants.NULL_INT
                                                                      : g.Member.MemberID
                                                                  , Matchnet.Search.Interfaces.SearchEngineType.FAST
                                                                  ,
                                                                  Matchnet.Search.Interfaces.SearchType.
                                                                      YourMatchesWebSearch
                                                                  ,
                                                                  Matchnet.Search.Interfaces.SearchEntryPoint.
                                                                      YourMatchesPage
                                                                  , false
                                                                  , false
                                                                  , false
                    );
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
                g.Transfer("/Applications/Search/SearchPreferences.aspx");
            }


            // RegionID may sometimes be null
            string regionID = g.SearchPreferences["RegionID"];
            if (regionID == null)
            {
                regionID = string.Empty;
            }

            // Store necessary values in session cookie for use in pixels.
            g.Session.Add("PixelGenderMask_SearchPref", g.SearchPreferences["GenderMask"],
                          SessionPropertyLifetime.Persistent);
            g.Session.Add("PixelRegionID_SearchPref", regionID, SessionPropertyLifetime.Persistent);
            g.Session.Add("PixelMinAge_SearchPref", g.SearchPreferences["MinAge"], SessionPropertyLifetime.Persistent);
            g.Session.Add("PixelMaxAge_SearchPref", g.SearchPreferences["MaxAge"], SessionPropertyLifetime.Persistent);

            ArrayList membersWomen = MemberSA.Instance.GetMembers(searchResultsWomen.ToArrayList(), MemberLoadFlags.None);
            //ArrayList membersWomen = MemberSA.Instance.GetMembers(searchResultsWomen.ToArrayList(), MemberLoadFlags.None);

            // foreach (var memberMen in membersMen)
            // {
            //     membersWomen.Add(memberMen);
            // }

            ArrayList members = membersWomen;

            if (members != null && members.Count > 0)
            {
                resultList.SearchIDs = new List<int>();
                for (int i = 0; i < members.Count; i++)
                {
                    resultList.SearchIDs.Add(((Member.ServiceAdapters.Member)members[i]).MemberID);
                }

            }
            BindMemberList(members);
            resultList.RenderAnalytics(members, searchResults.MoreResultsAvailable);
            resultList.TotalRows = searchResults.MatchesFound;
        }

        public void LoadSectorResultPage(bool showFemaleSeekingMale = false)
        {
            MatchnetQueryResults searchResults = null;

            try
            {
                searchResults = MemberSearchSA.Instance.Search(SectorsManager.GetSearchpreferences(SectorsManager.GetSector(), g, showFemaleSeekingMale)
                                                               , g.Brand.Site.Community.CommunityID
                                                               , g.Brand.Site.SiteID
                                                               , resultList.StartRow - 1
                                                               , resultList.PageSize
                                                               ,
                                                               g.Member == null ? Constants.NULL_INT : g.Member.MemberID
                                                               , Matchnet.Search.Interfaces.SearchEngineType.FAST
                                                               ,
                                                               Matchnet.Search.Interfaces.SearchType.YourMatchesWebSearch
                                                               ,
                                                               Matchnet.Search.Interfaces.SearchEntryPoint.YourMatchesPage
                                                               , false
                                                               , false
                                                               , false
                    );
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
                //   g.Transfer("/Applications/Search/SearchPreferences.aspx");
            }

            // RegionID may sometimes be null
            string regionID = g.SearchPreferences["RegionID"];
            if (regionID == null)
            {
                regionID = string.Empty;
            }

            // Store necessary values in session cookie for use in pixels.
            g.Session.Add("PixelGenderMask_SearchPref", g.SearchPreferences["GenderMask"],
                          SessionPropertyLifetime.Persistent);
            g.Session.Add("PixelRegionID_SearchPref", regionID, SessionPropertyLifetime.Persistent);
            g.Session.Add("PixelMinAge_SearchPref", g.SearchPreferences["MinAge"], SessionPropertyLifetime.Persistent);
            g.Session.Add("PixelMaxAge_SearchPref", g.SearchPreferences["MaxAge"], SessionPropertyLifetime.Persistent);

            ArrayList members = MemberSA.Instance.GetMembers(searchResults.ToArrayList(), MemberLoadFlags.None);

            if (members != null && members.Count > 0)
            {
                resultList.SearchIDs = new List<int>();
                for (int i = 0; i < members.Count; i++)
                {
                    resultList.SearchIDs.Add(((Member.ServiceAdapters.Member)members[i]).MemberID);
                }

            }
            BindMemberList(members);
            resultList.RenderAnalytics(members, searchResults.MoreResultsAvailable);
            resultList.TotalRows = searchResults.MatchesFound;
        }
    }
}
