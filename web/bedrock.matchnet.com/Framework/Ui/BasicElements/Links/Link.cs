using System.Web;
using System.Web.UI;
using System.ComponentModel;

using Matchnet.Content.ServiceAdapters.Links;

namespace Matchnet.Web.Framework.Ui.BasicElements.Links
{
	/// <summary>
	/// Summary description for Link.
	/// </summary>
	[DefaultProperty("Text"), 
		ToolboxData("<{0}:Link runat=server></{0}:Link>")]
	public class Link : System.Web.UI.WebControls.HyperLink
	{
		private string _titleResourceConstant;
		private bool _newWindow;
		private string _name;
		private int _width;
		private int _height;
		private string _properties;
		private bool _ssl;
		private ContextGlobal _g;
		private bool _expandUrl;
		private string _resourceConstant;
		private string _text;

		public Link()
		{	
			if ( Context != null )
			{
				if ( Context.Items["g"] != null )
					_g = (ContextGlobal) Context.Items["g"];
			}
			else
			{
				_g = new ContextGlobal( null );
			}
			this.PreRender += new System.EventHandler(Link_PreRender);
		}
	
		#region Properties
		[Bindable(true), 
		Category("Appearance"), 
		DefaultValue("")] 
		public string TitleResourceConstant 
		{
			get
			{
				return _titleResourceConstant;
			}

			set
			{
				_titleResourceConstant = value;
			}
		}

		[Bindable(true), 
		Category("Appearance"), 
		DefaultValue("")] 
		public bool NewWindow
		{
			get
			{
				return _newWindow;
			}
			set
			{
				_newWindow = value;
			}
		}

		[Bindable(true), 
		Category("Appearance"), 
		DefaultValue("")] 
		public string Name
		{
			get
			{
				return _name;
			}
			set
			{
				_name = value;
			}
		}

		[Bindable(true), 
		Category("Appearance"), 
		DefaultValue("")]
		public string Properties
		{
			get
			{
				return _properties;
			}
			set
			{
				_properties = value;
			}
		}

		[Bindable(true), 
		Category("Appearance"), 
		DefaultValue("")]
		public bool SSL
		{
			get
			{
				return _ssl;
			}
			set
			{
				_ssl = value;
			}
		}

		public bool ExpandUrl
		{
			get
			{
				return _expandUrl;
			}
			set
			{
				_expandUrl = value;
			}
		}

		public override string Text
		{
			get
			{
				if(ResourceConstant != null)
				{
					return _g.GetResource(ResourceConstant, this);
				}
				else
				{
					return _text;
				}
			}
			set
			{
				_text = value;
			}
		}

		[Bindable(true), 
		Category("Appearance"), 
		DefaultValue("")] 
		public new int Width
		{
			get
			{
				return _width;
			}
			set
			{
				_width = value;
			}
		}

		[Bindable(true), 
		Category("Appearance"), 
		DefaultValue("")] 
		public new int Height
		{
			get
			{
				return _height;
			}
			set
			{
				_height = value;
			}
		}

		[Bindable(true), 
		Category("Appearance"), 
		DefaultValue("")] 
		public string ResourceConstant
		{
			get
			{
				return _resourceConstant;
			}
			set
			{
				_resourceConstant = value;
			}
		}
		#endregion

		protected override void AddAttributesToRender(HtmlTextWriter writer)
		{
			if(TitleResourceConstant != null)
			{
				writer.AddAttribute(HtmlTextWriterAttribute.Title, System.Web.HttpUtility.HtmlEncode(_g.GetResource(TitleResourceConstant, this)));
			}
			base.AddAttributesToRender(writer);
		}

		private void Link_PreRender(object sender, System.EventArgs e)
		{
			if (_expandUrl)
			{
				base.NavigateUrl = LinkFactory.Instance.GetLink(base.NavigateUrl, new ParameterCollection(), _g.Brand, HttpContext.Current.Request.Url.ToString(), _ssl, _newWindow, _name, _width, _height, _properties);
			}
		}
	}
}
