﻿using System;
using Matchnet.Session.ValueObjects;
using Matchnet.Member.ServiceAdapters;

namespace Matchnet.Web.Framework.Ui.BasicElements
{
	public partial class ResultsViewType : FrameworkControl
	{
        public enum ListViewType
        {
            none = 0,
            gallery = 1,
            list = 2
        }

		string galleryViewType = String.Empty;

		public string GalleryViewFlag
		{
			get { return galleryViewType; }
			set { galleryViewType = value; }
		}

        public bool HideTextLink { get; set; }
        public string ListResourceConstant { get; set; }
        public string ListSelectedResourceConstant { get; set; }
        public string GalleryResourceConstant { get; set; }
        public string GallerySelectedResourceConstant { get; set; }

		protected void Page_Load(object sender, EventArgs e)
		{
			try
			{
                if (!String.IsNullOrEmpty(ListSelectedResourceConstant))
                {
                    txtListOff.ResourceConstant = ListSelectedResourceConstant;
                }

                if (!String.IsNullOrEmpty(GallerySelectedResourceConstant))
                {
                    txtGalleryOff.ResourceConstant = GallerySelectedResourceConstant;
                }
			}
			catch (Exception ex)
			{ g.ProcessException(ex); }
		}

		private void Page_PreRender(object sender, EventArgs e)
		{
			//ResultViewChanged.Value = "false";
		}

		public ListViewType GetSearchResultViewMode()
		{
			var defaultView = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("IS_SEARCH_RESULT_DEFAULT_VIEW_GALLERY", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID);
			// TT #13433 - make galleryview the default for search results

			var viewMode = Request["ViewMode"];
			if (viewMode != null)
			{
				if (viewMode.ToLower() == "gallery")
				{
					galleryViewType = "true";
				}
				else if (viewMode.ToLower() == "list")
				{
					galleryViewType = "false";
				}

                bool hasViewTypeChanged = false;
                if (g.Member != null)
                {
                    ListViewType lvType = GetMemberListType(g);
                    string currentGalleryViewType = "true";
                    if (lvType != ResultsViewType.ListViewType.gallery)
                    {
                        currentGalleryViewType = "false";
                    }
                    hasViewTypeChanged = currentGalleryViewType != galleryViewType;
                }
                else
                {
                    string currentGalleryViewType = g.Session[ResultListHandler.SESSIONOBJ_GALLERYVIEW];
                    if (!string.IsNullOrEmpty(currentGalleryViewType) && currentGalleryViewType != galleryViewType)
                        hasViewTypeChanged = true;
                }

                // TT #15790 - set a session flag to specify whether the list view changed, so we don't reset the VisitorLimit inappropriately (VisitorLimitHelper.cs)
                if (hasViewTypeChanged)
                {
                    g.Session.Add(VisitorLimitHelper.SEARCHRESULTS_VIEW_CHANGED_SESSION_KEY, "ViewChanged", SessionPropertyLifetime.Temporary);
                }
                else
                {
                    g.Session.Remove(VisitorLimitHelper.SEARCHRESULTS_VIEW_CHANGED_SESSION_KEY);
                }
			}
			else
			{
				galleryViewType = defaultView;

                if (g.Member != null)
                {
                    //get list view type from member attribute
                    ListViewType lvType = GetMemberListType(g);
                    if (lvType != ResultsViewType.ListViewType.none)
                    {
                        if (lvType == ListViewType.gallery)
                            galleryViewType = "true";
                        else
                            galleryViewType = "false";
                    }
                }
                else if (g.Session[ResultListHandler.SESSIONOBJ_GALLERYVIEW] != null)
                {
                    galleryViewType = g.Session[ResultListHandler.SESSIONOBJ_GALLERYVIEW];
                    if (galleryViewType == String.Empty)
                    {
                        // TT #13433 & #13466 - make galleryview the default for search result based on setting
                        galleryViewType = defaultView;
                    }
                }
			}

            if (g.Member != null)
            {
                ListViewType lvType = galleryViewType == "true" ? ListViewType.gallery : ListViewType.list;
                SaveMemberListType(g, lvType);
            }

            if (g.Session[ResultListHandler.SESSIONOBJ_GALLERYVIEW] != null &&
                            g.Session[ResultListHandler.SESSIONOBJ_GALLERYVIEW] != galleryViewType)
                g.Session.Add(ResultListHandler.SESSIONOBJ_GALLERYVIEW, galleryViewType, SessionPropertyLifetime.Temporary);

            string currentURL = Request.RawUrl;
			if (galleryViewType == "true")
			{
                txtViewAs.ResourceConstant = string.IsNullOrEmpty(ListResourceConstant) ? "TXT_VIEW_AS_LIST" : ListResourceConstant;
				txtListOff.Visible = false;

				//lnkViewType.NavigateUrl = "javascript:document.getElementById('" + ResultViewChanged.ClientID + "').value='true';";  // TT #15790 - set value to hidden field so we know the view has changed
				//lnkViewType.NavigateUrl += "document.getElementById('" + GalleryView.ClientID + "').value='false';document." + WebConstants.HTML_FORM_ID + ".submit();";
                lnkViewType.NavigateUrl = FrameworkGlobals.AppendParamToURL(currentURL, "ViewMode", "list");
			}
			else
			{
                txtViewAs.ResourceConstant = string.IsNullOrEmpty(GalleryResourceConstant) ? "TXT_VIEW_AS_GALLERY" : GalleryResourceConstant;
				txtGalleryOff.Visible = false;
                 
				//lnkViewType.NavigateUrl = "javascript:document.getElementById('" + ResultViewChanged.ClientID + "').value='true';";  // TT #15790 - set value to hidden field so we know the view has changed
				//lnkViewType.NavigateUrl += "document.getElementById('" + GalleryView.ClientID + "').value='true';document." + WebConstants.HTML_FORM_ID + ".submit();";
                lnkViewType.NavigateUrl = FrameworkGlobals.AppendParamToURL(currentURL, "ViewMode", "gallery");
			}

            return galleryViewType == "true" ? ListViewType.gallery : ListViewType.list;
		}

        public static ListViewType GetMemberListType(ContextGlobal g)
        {
            ListViewType lvtype = ListViewType.none;

            if (g != null && g.Member != null)
            {
                try
                {
                    lvtype = (ListViewType)g.Member.GetAttributeInt(g.Brand, "SearchResultView", 0);
                }
                catch (Exception ex)
                {
                    g.ProcessException(ex);
                }
            }

            return lvtype;
        }

        public static void SaveMemberListType(ContextGlobal g, ListViewType lvtype)
        {
            if (g != null && g.Member != null)
            {
                try
                {
                    if (lvtype != GetMemberListType(g) && lvtype != ListViewType.none)
                    {
                        g.Member.SetAttributeInt(g.Brand, "SearchResultView", (int)lvtype);
                        MemberSA.Instance.SaveMember(g.Member);
                    }
                }
                catch (Exception ex)
                {
                    g.ProcessException(ex);
                }
            }
        }
	}
}