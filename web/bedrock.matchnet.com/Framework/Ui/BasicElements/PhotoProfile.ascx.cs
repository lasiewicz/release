﻿using System;
using Matchnet.Web.Framework.Managers;
using Matchnet.Web.Applications.MemberProfile;

namespace Matchnet.Web.Framework.Ui.BasicElements
{
    public partial class PhotoProfile : FrameworkControl
    {
        int _brandID;
        int _communityiID;
        int _siteID;
        FrameworkControl _resourceControl;
        public FrameworkControl ResourceControl
        {
            get
            {
                if (_resourceControl == null)
                    return this;
                else
                { return _resourceControl; }
            }

            set { _resourceControl = value; }


        }
        public Member.ServiceAdapters.Member ProfileMember { get; set; }
        string _viewProfileUrl = "";
        public bool EnableProfileLinks { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {

            

        }


        private void DisplayMemberPhoto()
        {
            var photo = MemberPhotoDisplayManager.Instance.GetDefaultPhoto(g.Member, ProfileMember,
                                                                           g.Brand);

            if(photo == null)
            {
                string imageURL = MemberPhotoDisplayManager.Instance.GetPhotoDisplayURL(g.Member, ProfileMember, g.Brand,
                                                                    photo, PhotoType.Thumbnail,
                                                                    PrivatePhotoImageType.Thumb, NoPhotoImageType.Thumb);

                imgThumb.ImageUrl = imageURL;
            }
            else
            {
                imgThumb.ImageUrl = MemberPhotoDisplayManager.Instance.GetNoPhotoFile(NoPhotoImageType.ThumbV2, ProfileMember, g.Brand);
            }

        }


        protected override void OnPreRender(EventArgs e)
        {  try{
            if (ProfileMember == null)
                    return;
                 _viewProfileUrl = BreadCrumbHelper.MakeViewProfileLink(ProfileMember.MemberID);
                //Member.ServiceAdapters.Member member= MemberSA.Instance.GetMember(_resultMember.MemberID,Member.ServiceAdapters.MemberLoadFlags.None);

                _siteID = g.Brand.Site.SiteID;
                _brandID = g.Brand.BrandID;
                _communityiID = g.Brand.Site.Community.CommunityID;

                DateTime birthDate = ProfileMember.GetAttributeDate(g.Brand, "BirthDate", DateTime.MinValue);
                txtAge.Text = FrameworkGlobals.GetAge(birthDate).ToString();
                if (this.txtAge.Text.Trim() == "")
                {
                    this.txtAge.Text = "&nbsp;";
                }
                else
                {
                    if (FrameworkGlobals.isHebrewSite(g.Brand))
                    {
                        txtAge.Text = g.GetResource("TXT_YEARS_OLD", ResourceControl) + " " + txtAge.Text;
                    }
                    else
                    {
                        txtAge.Text += " " + g.GetResource("TXT_YEARS_OLD", ResourceControl);
                    }
                }
                lnkUserName.Text = ProfileMember.GetUserName(g.Brand);
               
                txtLocation.Text = ProfileDisplayHelper.GetRegionDisplay(ProfileMember, g);
                if (EnableProfileLinks)
                {
                    lnkUserName.NavigateUrl = _viewProfileUrl;
                    imgThumb.NavigateUrl = _viewProfileUrl;
                }
                DisplayMemberPhoto();
            }
            catch (Exception ex)
            { g.ProcessException(ex); }

        }
    }
}