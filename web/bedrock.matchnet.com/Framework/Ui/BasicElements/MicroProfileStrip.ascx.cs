﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Web;

using Matchnet.Web.Applications.Home;
using Matchnet.Member.ServiceAdapters;
using Matchnet.List.ValueObjects;
using Matchnet.List.ServiceAdapters;
using Matchnet.Web.Framework.Globalization;

namespace Matchnet.Web.Framework.Ui.BasicElements
{

    public partial class MicroProfileStrip :FrameworkControl
    {

        List<ProfileHolder> _memberList;
        string _noResultContentResource = "";
        FrameworkControl _resourceControl = null;
        DisplayContextType _displayContext = DisplayContextType.Default;

        #region public properties
        public List<ProfileHolder> MemberList
        {
            set { _memberList = value; }
            get { return _memberList; }
        }

        public string NoResultContentResource
        {
            set { _noResultContentResource = value; }
            get { return _noResultContentResource; }
        }

        public FrameworkControl ResourceControl
        {
            set { _resourceControl = value; }
            get { return _resourceControl; }
        }

        public DisplayContextType DisplayContext
        {
            set { _displayContext = value; }
            get { return _displayContext; }
        }

        public string TrackingParam {get;set;}
        #endregion

        #region page events
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
               

            }
            catch (Exception ex)
            { g.ProcessException(ex); }
        }

        protected override void OnPreRender(EventArgs e)
        {
            try
            {
                if (_memberList == null || _memberList.Count == 0)
                {
                    DisplayNoResultsContent();
                    return;
                }

                DisplayMemberList();

            }
            catch (Exception ex)
            { g.ProcessException(ex); }
        }
        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            try{
            InitializeComponent();
            base.OnInit(e);
            }
            catch (Exception ex)
            { g.ProcessException(ex); }
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
          this.Load += new System.EventHandler(this.Page_Load);
          //this.PreRender += new System.EventHandler(OnPreRender);
        }
        #endregion
        #endregion

        #region private methods

        private void DisplayNoResultsContent()
        {
            if (!String.IsNullOrEmpty(_noResultContentResource))
            {
                phNoResultDisplay.Visible = true;
                if (_resourceControl == null)
                    noResultDisplay.Text = Localizer.ExpandImageTokens( g.GetResource(_noResultContentResource, this),g.Brand);
                else
                    noResultDisplay.Text = Localizer.ExpandImageTokens(g.GetResource(_noResultContentResource, _resourceControl),g.Brand);
               }
            
        }

        private void DisplayMemberList()
        {

          // phMember.Visible = true;
            //left item
            //this.phMemberWhoHave1.Visible = true;
            memberProfile11.Visible = true;
            LoadProfile(memberProfile11, _memberList[0]);

            //2nd item
            if (_memberList.Count > 1)
            {
                //this.phMemberWhoHave2.Visible = true;
                memberProfile21.Visible = true;
                LoadProfile(memberProfile21, _memberList[1]);
            }

            //3rd item
            if (_memberList.Count > 2)
            {
                //this.phMemberWhoHave3.Visible = true;
                memberProfile31.Visible = true;
                LoadProfile(memberProfile31, _memberList[2]);
            }

            ////4th item
            //if (_memberList.Count > 3)
            //{
            //    memberProfile41.Visible = true;
            //    LoadProfile(memberProfile41, _memberList[3]);
            //}
        }

        private void LoadProfile(MicroProfile20 microProfile, ProfileHolder profileHolder)
        {

           
            microProfile.member = profileHolder.Member;
           
            microProfile.MyEntryPoint = profileHolder.MyEntryPoint;
            microProfile.Ordinal = profileHolder.Ordinal;
            if(!String.IsNullOrEmpty(TrackingParam)) {
                microProfile.TrackingParam = TrackingParam;
            }
            if (_displayContext == DisplayContextType.HotList)
            {
                microProfile.HotListCategoryID = (int)profileHolder.HotlistCategory;
                ListItemDetail viewedYouMemberListDetail = g.List.GetListItemDetail(profileHolder.HotlistCategory,
                                  g.Brand.Site.Community.CommunityID,
                                  g.Brand.Site.SiteID,
                                  profileHolder.Member.MemberID);

                if (viewedYouMemberListDetail != null)
                    microProfile.LastHotlistActionDate = viewedYouMemberListDetail.ActionDate;
            }


            microProfile.LoadMemberProfile();

        }
        #endregion
    }
}