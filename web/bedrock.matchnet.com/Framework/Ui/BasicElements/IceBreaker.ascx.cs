﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Member.ValueObjects.Interfaces;
using Matchnet.Web.Framework;
using Matchnet.Lib;
using Matchnet.Web.Framework.Ui;
using Matchnet.Web.Framework.Util;
using Matchnet.List.ServiceAdapters;
using Matchnet.List.ValueObjects;
using Matchnet.Content.ServiceAdapters.Links;
using Matchnet.Web.Framework.Ui.BasicElements;
using Matchnet.Web.Applications.Email;
using Matchnet.Web.Framework.TemplateControls;
using System.Xml.Serialization;
using System.IO;


using System.Text;
using System.Web.UI.HtmlControls;

namespace Matchnet.Web.Framework.Ui.BasicElements
{
    public partial class IceBreaker : FrameworkControl
    {
        private string omnitureStartStr = "Icebreak Message Start";
        private string omnitureInsertStr = "Icebreak Message Insert";

        private IMemberDTO _profileMember;
        private List<string> myInterests;

        public string OmnitureStartString
        {
            get
            {
                return omnitureStartStr;
            }
            set
            {
                omnitureStartStr = value;
            }
        }

        public string OmnitureInsertString
        {
            get
            {
                return omnitureInsertStr;
            }
            set
            {
                omnitureInsertStr = value;
            }
        }

        public IMemberDTO ProfileMember
        {
            get
            {
                return _profileMember;
            }
            set
            {
                _profileMember = value;
            }
        }

        public IceBreaker()
        {
            ;
        }

        protected override void OnInit(EventArgs e)
        {
            myInterests = new List<string>();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            
            bool isIcebreakerEnabled = Conversion.CBool(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("SHOW_ICEBREAKER", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID));
            bool isIcebreakerEnabledForNonsubs = Conversion.CBool(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("SHOW_ICEBREAKER_TO_NONSUBS", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID));
            
            if (isIcebreakerEnabled && (g.Member != null) && (g.Member.IsPayingMember(g.Brand.Site.SiteID) || (isIcebreakerEnabledForNonsubs && ComposeHandler.canNonsubSaveAsDraft(g))))
            {
                if (ProfileMember != null && IceBreakerFeatureThrottler.IsFeatureEnabled(g.Member, g))
                {
                    phIcebreaker.Visible = true;
                    IceBreakers h = GetIceBreakers(g);

                    bool isDynamicIcebreakerEnabled = Conversion.CBool(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("SHOW_DYNAMIC_ICEBREAKERS", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID));
                    if (isDynamicIcebreakerEnabled)
                    {
                        List<string> Cuisine = Option.GetDescriptions("Cuisine", ProfileMember.GetAttributeInt(g.Brand, "Cuisine"), g);
                        collectInfo("Cuisine", Cuisine);

                        List<string> EntertainmentLocation = Option.GetDescriptions("EntertainmentLocation", ProfileMember.GetAttributeInt(g.Brand, "EntertainmentLocation"), g);
                        collectInfo("EntertainmentLocation", EntertainmentLocation);

                        List<string> LeisureActivity = Option.GetDescriptions("LeisureActivity", ProfileMember.GetAttributeInt(g.Brand, "LeisureActivity"), g);
                        collectInfo("LeisureActivity", LeisureActivity);

                        List<string> Music = Option.GetDescriptions("Music", ProfileMember.GetAttributeInt(g.Brand, "Music"), g);
                        collectInfo("Music", Music);

                        List<string> IndustryType = Option.GetDescriptions("IndustryType", ProfileMember.GetAttributeInt(g.Brand, "IndustryType"), g);
                        collectInfo("IndustryType", IndustryType);

                        List<string> PersonalityTrait = Option.GetDescriptions("PersonalityTrait", ProfileMember.GetAttributeInt(g.Brand, "PersonalityTrait"), g);
                        collectInfo("PersonalityTrait", PersonalityTrait);

                        List<string> Pets = Option.GetDescriptions("Pets", ProfileMember.GetAttributeInt(g.Brand, "Pets"), g);
                        collectInfo("Pets", Pets);

                        List<string> PhysicalActivity = Option.GetDescriptions("PhysicalActivity", ProfileMember.GetAttributeInt(g.Brand, "PhysicalActivity"), g);
                        collectInfo("PhysicalActivity", PhysicalActivity);

                        List<string> Reading = Option.GetDescriptions("Reading", ProfileMember.GetAttributeInt(g.Brand, "Reading"), g);
                        collectInfo("Reading", Reading);

                        repeaterSectionDynamic.DataSource = h.DynamicIceBreakers.FilteredItems(myInterests);
                        repeaterSectionDynamic.DataBind();
                    }

                    repeaterSectionStatic.DataSource = h.StaticIceBreakers.Items;
                    repeaterSectionStatic.DataBind();

                }
            }
            else
            {
                phIcebreaker.Visible = false;
            }
        }

        private void collectInfo(string category, List<string> items)
        {
            foreach (string item in items)
            {
                myInterests.Add(category + "|" + item);
            }
        }

        protected void repeaterSectionDynamic_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                IceBreakerItem item = e.Item.DataItem as IceBreakerItem;
                
                if (myInterests.Contains(item.AttributeName))
                {
                    Literal IceBreakerItemLiteral = e.Item.FindControl("iceBreakerItemDynamic") as Literal;

                    IceBreakerItemLiteral.Text = item.Value;
                }
            }
        }

        protected void repeaterSectionStatic_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                IceBreakerItem item = e.Item.DataItem as IceBreakerItem;

                Literal IceBreakerItemLiteral = e.Item.FindControl("iceBreakerItemStatic") as Literal;

                IceBreakerItemLiteral.Text = item.Value;
            }
        }

        //private static Hashtable GetAdSlotsWhiteList(ContextGlobal g)
        //{
        //    string ICEBREAKER_FILE_PATH = "/Framework/Ui/BasicElements/IceBreakers/XML/DynamicIceBreaker_{0}.xml";
        //    string ICEBREAKER_CACHE_KEY = "DYNAMIC_ICEBREAKER_CACHE_KEY_" + g.Brand.Site.SiteID.ToString();
        //    Hashtable iceBreakerConfigs = g.Cache[ICEBREAKER_CACHE_KEY] as Hashtable;

        //    try
        //    {
        //        if (iceBreakerConfigs == null)
        //        {
        //            iceBreakerConfigs = new Hashtable();

        //            string xmlFile = HttpContext.Current.Server.MapPath(string.Format(ICEBREAKER_FILE_PATH, g.Brand.Site.SiteID.ToString()));
        //            DynamicIceBreakers iceBreakerConfigs2 = Utility.FromXmlFile<DynamicIceBreakers>(xmlFile);

        //            // Converts the list into a Hashtable for fast access
        //            foreach (IceBreakerItem item in iceBreakerConfigs2.IceBreakerItemList)
        //            {
        //                string attributeName = item.AttributeName.ToLower();
        //                if (!iceBreakerConfigs.ContainsKey(attributeName))
        //                {
        //                    iceBreakerConfigs.Add(attributeName, item.Value);
        //                }
        //            }

        //            System.Web.Caching.CacheDependency cacheFileDependencies = new System.Web.Caching.CacheDependency(new string[] { xmlFile });
        //            g.Cache.Insert(ICEBREAKER_CACHE_KEY, iceBreakerConfigs, cacheFileDependencies);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        g.ProcessException(ex);
        //    }

        //    return iceBreakerConfigs;
        //}

        public static IceBreakers GetIceBreakers(ContextGlobal g)
        {
            string ICEBREAKER_FILE_PATH = "/Framework/Ui/BasicElements/IceBreakers/XML/DynamicIceBreaker_{0}.xml";
            string ICEBREAKER_CACHE_KEY = "DYNAMIC_ICEBREAKER_CACHE_KEY_" + g.Brand.Site.SiteID.ToString();
            IceBreakers iceBreakers = null;

            try
            {
                //check cache
                iceBreakers = g.Cache[ICEBREAKER_CACHE_KEY] as IceBreakers;

                if (iceBreakers == null)
                {
                    //load from xml
                    string xmlFile = HttpContext.Current.Server.MapPath(string.Format(ICEBREAKER_FILE_PATH, g.Brand.Site.SiteID.ToString()));
                    TextReader textReader = new StreamReader(xmlFile);

                    XmlSerializer deserializer = new XmlSerializer(typeof(IceBreakers));
                    iceBreakers = deserializer.Deserialize(textReader) as IceBreakers;
                    textReader.Close();

                    //save to cache
                    if (iceBreakers != null)
                    {
                        g.Cache.Insert(ICEBREAKER_CACHE_KEY, iceBreakers);
                    }
                }

                

            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }

            return iceBreakers;
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
        }

        [Serializable]
        public class IceBreakers
        {
            [XmlElement("DynamicIceBreakers")]
            public IceBreakerType DynamicIceBreakers { get; set; }

            [XmlElement("StaticIceBreakers")]
            public IceBreakerType StaticIceBreakers { get; set; }
        }

        [Serializable]
        public class IceBreakerType
        {
            [XmlElement("Item")]
            public List<IceBreakerItem> Items { get; set; }

            public List<IceBreakerItem> FilteredItems(List<string> aList)
            {
                List<IceBreakerItem> cleanDynamicList = new List<IceBreakerItem>();
                foreach (IceBreakerItem item in Items)
                {
                    if (aList.Contains(item.AttributeName))
                    {
                        cleanDynamicList.Add(item);
                    }
                }

                return cleanDynamicList;
            }
        }

        [Serializable]
        public class IceBreakerItem
        {
            [XmlAttribute()]
            public string AttributeName { get; set; }
            [XmlText()]
            public string Value { get; set; }
        }
        
    }
}
