﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HeroProfile.ascx.cs"
    Inherits="Matchnet.Web.Framework.Ui.BasicElements.HeroProfile" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mnl" Namespace="Matchnet.Web.Framework.Ui.BasicElements.Links"
    Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mnbe" Namespace="Matchnet.Web.Framework.Ui.BasicElements"
    Assembly="Matchnet.Web" %>
<%@ Register Src="LastTime.ascx" TagName="LastTime" TagPrefix="uc2" %>
<!--Hero Profile-->
<div id="hp-hero-profile">
    <div class="picture">
        <!--large photo-->
        <table cellspacing="0" cellpadding="0">
            <tbody>
                <tr>
                    <td>
                        <mn:Image ID="imgProfile" runat="server" Border="0" CssClass="centered" />
                        <!--no photo with link to upload-->
                        <asp:Panel ID="pnlNoPhoto" runat="server" CssClass="no-photo" Visible="false">
                            <asp:HyperLink ID="lnkRequestPhotoUpload" runat="server" NavigateUrl="/Applications/Email/Compose.aspx?MemberId=">
                                <mn:Image ID="imgRequestPhoto" runat="server" FileName="btn-click-here-to-request-my-photo.gif"
                                    TitleResourceConstant="TTL_CLICK_HERE_TO_REQUEST" ResourceConstant="ALT_CLICK_HERE_TO_REQUEST" />
                            </asp:HyperLink>
                        </asp:Panel>
                    </td>
                </tr> 
                <asp:PlaceHolder ID="phColorCode" runat="server" Visible="false">
                    <tr>
                        <td style="height: auto;">
                            <div class="cc-pic-tag cc-pic-tag-lrg cc-<%=_ColorText.ToLower() %>-bg">
                                Color: <strong>
                                    <%=_ColorText %></strong>
                                <div class="cc-pic-tag-help">
                                    <b>What does this mean?</b><br />
                                    Click the bar to find out!</div>
                            </div>
                                <asp:PlaceHolder ID="phBlueTip" runat="server" Visible="false"><mn:txt id="phBlueTipHtml" runat="server" resourceconstant="HTML_TIP_BLUE" expandimagetokens="true" /></asp:PlaceHolder>
                                <asp:PlaceHolder ID="phRedTip" runat="server" Visible="false"><mn:txt id="phRedTipHtml" runat="server" resourceconstant="HTML_TIP_RED" expandimagetokens="true" /></asp:PlaceHolder>
                                <asp:PlaceHolder ID="phWhiteTip" runat="server" Visible="false"><mn:txt id="phWhiteTipHtml" runat="server" resourceconstant="HTML_TIP_WHITE" expandimagetokens="true" /></asp:PlaceHolder>
                                <asp:PlaceHolder ID="phYellowTip" runat="server" Visible="false"><mn:txt id="phYellowTipHtml" runat="server" resourceconstant="HTML_TIP_YELLOW" expandimagetokens="true" /></asp:PlaceHolder>
                        </td>
                    </tr>
                </asp:PlaceHolder>
            </tbody>
        </table>
    </div>
    <asp:Panel ID="panelHeroProfileContent" runat="server" CssClass="hero-profile-content-div hero-profile-spotlight">
        <h2 class="link-to-full-profile-big">
            <asp:HyperLink runat="server" ID="lnkUserName1" /></h2>
        <asp:PlaceHolder ID="phViewedProfile" runat="server" Visible="false">
            <div class="communicated-info">
                <strong>
                    <mn2:FrameworkLiteral ID="literalSheViewed" runat="server" ResourceConstant="TXT_SHE_VIEWED_YOUR_PROFILE"
                        Visible="false"></mn2:FrameworkLiteral>
                    <mn2:FrameworkLiteral ID="literalHeViewed" runat="server" ResourceConstant="TXT_HE_VIEWED_YOUR_PROFILE"
                        Visible="false"></mn2:FrameworkLiteral>
                    <mn2:FrameworkLiteral ID="literalUndefinedViewed" runat="server" ResourceConstant="TXT_VIEWED_YOUR_PROFILE"
                        Visible="false"></mn2:FrameworkLiteral>
                    &nbsp; </strong><span class="dim-text">
                        <uc2:LastTime ID="lastTimeAction" runat="server" />
                    </span>
            </div>
        </asp:PlaceHolder>
        <div class="info-basics">
            <!--basic info-->
            <ul class="overview">
                <li id="hp-hero-basics-seeking">
                    <asp:Literal ID="literalSeeking" runat="server"></asp:Literal></li>
                <li id="hp-hero-basics-fora">
                    <mn2:FrameworkLiteral ID="literalForA" ResourceConstant="FOR_A" runat="server"></mn2:FrameworkLiteral>&nbsp;
                    <asp:Literal ID="literalSeekingFor" runat="server"></asp:Literal></li>
                <li>
                    <asp:Literal ID="literalAge" runat="server"></asp:Literal>&nbsp;
                    <%--<mn2:FrameworkLiteral id="literalAgeDisplay" ResourceConstant="TXT_YEARS_OLD" runat="server"></mn2:FrameworkLiteral>--%>
                </li>
                <li>
                    <asp:Literal ID="literalLocation" runat="server"></asp:Literal></li>
            </ul>
            <asp:PlaceHolder ID="phAboutMe" runat="server" Visible="false">
                <div id="about-me">
                    <h3>
                        <mn2:FrameworkLiteral ID="literalAboutMeTitle" runat="server" ResourceConstant="TXT_ABOUT_ME"></mn2:FrameworkLiteral></h3>
                    <p>
                        <asp:Literal ID="literalAboutMeText" runat="server"></asp:Literal>&nbsp;
                        <mnl:Link runat="server" ID="lnkAboutMe" TitleResourceConstant="TTL_VIEW_MY_PROFILE" />
                    </p>
                </div>
            </asp:PlaceHolder>
            <asp:PlaceHolder ID="phWeBothLike" runat="server" Visible="false">
                <div id="we-both-like">
                    <mn:Image ID="imgIconBothLike" runat="server" FileName="icon-both-like.gif" ImageAlign="Left"
                        CssClass="icon-we-both-like" />
                    <h3>
                        <mn2:FrameworkLiteral ID="literalWeBothLikeTitle" runat="server" ResourceConstant="TXT_WE_BOTH_LIKE"></mn2:FrameworkLiteral></h3>
                    <p>
                        <asp:Literal ID="literalWeBothLikeText" runat="server"></asp:Literal>&nbsp;
                        <mnl:Link runat="server" ID="lnkWeBothLikeMore" TitleResourceConstant="TTL_VIEW_MY_PROFILE"
                            ResourceConstant="MORE_ARROWS" />
                    </p>
                </div>
            </asp:PlaceHolder>
        </div>
        <!--email link-->
        <div class="action-email">
            <asp:HyperLink ID="lnkEmail" runat="server">
                <mn:Image ID="imgEmail" runat="server" FileName="btn-email-fullprofile.png" Border="0"
                    ResourceConstant="ALT_EMAIL" TitleResourceConstant="TTL_EMAIL" />
            </asp:HyperLink>
        </div>
    </asp:Panel>
    <div class="communications clearfix">
        <asp:PlaceHolder ID="phCommunicate" runat="server" Visible="false">
            <%--NOTE: The names of these YNM controls are important as they are explicitly specified in YNM Javascript code--%>
            <!--mutual Yes-->
            <asp:Panel ID="divBothSaidYes" runat="server" Visible="false">
                <mn:Image ID="imgYY" runat="server" FileName="icon-click-yy.gif" />
                <mn2:FrameworkLiteral ID="literalYY" runat="server" ResourceConstant="TXT_MUTUAL_YY"></mn2:FrameworkLiteral>
            </asp:Panel>
            <!--vote buttons-->
            <div class="click">
                <span class="do-we-click">
                    <mn2:FrameworkLiteral ID="literalClick" runat="server" ResourceConstant="CLICK"></mn2:FrameworkLiteral></span>
                <span class="no-wrap-class">
                    <!--yes button-->
                    <mnbe:YNMVoteButton ID="ImageYes" runat="server" ResourceConstant="YNM_Y_IMG_ALT_TEXT"
                        Type="Yes" SelectedFileName="icon-click-y-on.gif" FileName="icon-click-y-off.gif">
                    </mnbe:YNMVoteButton>
                    &nbsp;<mn:Txt ID="TxtYes" runat="server" ResourceConstant="YNM_YES"></mn:Txt>
                    <!--no button-->
                    <mnbe:YNMVoteButton ID="ImageNo" runat="server" ResourceConstant="YNM_N_IMG_ALT_TEXT"
                        Type="No" SelectedFileName="icon-click-n-on.gif" FileName="icon-click-n-off.gif">
                    </mnbe:YNMVoteButton>
                    &nbsp;<mn:Txt ID="TxtNo" runat="server" ResourceConstant="YNM_NO"></mn:Txt>                    
                    <!--maybe button-->
                    <mnbe:YNMVoteButton ID="ImageMaybe" runat="server" ResourceConstant="YNM_M_IMG_ALT_TEXT"
                        Type="Maybe" SelectedFileName="icon-click-m-on.gif" FileName="icon-click-m-off.gif">
                    </mnbe:YNMVoteButton>
                    &nbsp;<mn:Txt ID="TxtMaybe" runat="server" ResourceConstant="YNM_MAYBE"></mn:Txt>
                    <%--input to hold current vote status, used by js--%>
                    <input id="YNMVoteStatus" type="hidden" name="YNMVoteStatus" runat="server" />
                </span>
            </div>
        </asp:PlaceHolder>
    </div>
</div>
