﻿using System;
using System.Collections;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Matchnet.Lib;
using Matchnet.List.ServiceAdapters;
using Matchnet.List.ValueObjects;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Member.ServiceAdapters.Interfaces;
using Matchnet.Member.ValueObjects.Interfaces;
using Matchnet.UserNotifications.ServiceAdapters;
using Matchnet.UserNotifications.ValueObjects;
using Matchnet.Web.Applications.CompatibilityMeter.Controls;
using Matchnet.Web.Applications.UserNotifications;
using Matchnet.Web.Framework.Managers;
using Matchnet.Content.ServiceAdapters.Links;
using System.Web;

namespace Matchnet.Web.Framework.Ui.BasicElements
{
    partial class ResultList20 : FrameworkControl, IResultList
    {
        private string galleryView = String.Empty;

        // MatchMeter
        private bool isMatchMeterEnabled;
        private bool _isMatchMeterInfoDisplay;
        private MatchesCollection _myMatchesCollection; // For MatchMeter app (Jmeter)
        private Dictionary<IMemberDTO, decimal> MatchResultsMemberScoreDictionary;

        private const int NumberOfTrackedPages = 6;	// On SAGE we have channels up to "Results_Page6plus".
        private bool hasSpotlightProfile;


        private int count;
        private int highlightedProfileCount;
        int pageSize = 12;
        int chapterSize = 3;
        private int startRow;
        bool _enableGalleryView = true;
        private ResultListHandler resultListHandler;

        /// <summary>
        /// Specifies whether it is a partial page render
        /// </summary>
        public bool IsPartialRender { get; set; }

        public bool IgnoreSAFilteredSearchResultsCache { get; set; }
        public bool IgnoreSASearchResultsCache { get; set; }
        public bool IgnoreAllSearchResultsCache { get; set; }

        public ResultList20()
        {
            TargetMemberIDs = new ArrayList();
            HotListCategory = HotListCategory.Default;
        }

        #region IResultList implementation

        public string StartRowClientID
        {
            get
            {
                return hidStartRow.ClientID;
            }
        }

        public ArrayList TargetMemberIDs { get; set; }

        public List<int> MemberIDList { get; set; }
        public string GalleryView
        {
            get { return galleryView; }
            set { galleryView = value; }
        }

        public FrameworkControl ThisControl
        {
            get { return this; }

        }

        public DataTable Dt { get; set; }

        public Int32 MemberNum { get; set; }

        public List<int> SearchIDs { get; set; }

        #region controls
        public Repeater GalleryMemberRepeater { get { return galleryMemberRepeater; } }

        public Repeater MemberRepeater { get { return memberRepeater; } }

        public Repeater JMeterRepeater { get { return jmeterRepeater; } }

        public Repeater PhotoGalleryRepeater { get { return photoGalleryRepeater; } }

        public NoResults NoSearchResults { get { return noSearchResults; } }

        public PlaceHolder PLCMembersOnlineNoResults { get { return phMembersOnlineNoResults; } }

        public PlaceHolder PLCPromotionalProfile { get { return plcPromotionalProfile; } }

        public Txt NoUsersText { get { return noUsersText; } }

        public bool EnableMultipleSearchViews { get { return _enableGalleryView; } set{ _enableGalleryView = value; } }
        #endregion

        # region context type for result list

        public ResultContextType ResultListContextType { get; set; }

        public bool EnableSingleSelect { get; set; }

        #endregion context type for result list

        #region properties

        public Int32 PageSize
        {
            get { return pageSize; }
            set { pageSize = value; }
        }

        public Int32 ChapterSize
        {
            get { return chapterSize; }
            set { chapterSize = value; }
        }

        public Int32 TotalRows
        {
            get { return _TotalRows; }
            set { _TotalRows = value; }
        }

        private Int32 _TotalRows;

        public Int32 StartRow
        {
            get
            {
                return Conversion.CInt(hidStartRow.Value);
            }
            set
            {
                hidStartRow.Value = value.ToString();
            }
        }

        public int EndRow { get; set; }

        public MOCollection MyMOCollection { get; set; }

        public HotListCategory HotListCategory { get; set; }

        public HotListType HotListType { get; set; }

        public HotListDirection HotListDirection { get; set; }
        public bool PhotoOnly { get; set; }

        #endregion


        #endregion


        # region General Page Init

        /// <summary>
        /// This init method fires off three other init methods.  They are used to prepare the page with anything needed.
        /// The init method called is dependant upon the result list context type.
        /// </summary>
        private void Page_Init(object sender, EventArgs e)
        {
            try
            {


                switch (ResultListContextType)
                {
                    case ResultContextType.HotList:
                        DoHotListInit();
                        break;
                    case ResultContextType.SearchResult:
                        DoSearchResultInit();
                        break;
                    case ResultContextType.QuickSearchResult:
                        DoQuickSearchResultInit();
                        break;
                    case ResultContextType.KeywordSearchResult:
                        DoQuickSearchResultInit();
                        break;
                    case ResultContextType.ReverseSearchResult:
                        DoQuickSearchResultInit();
                        break;
                    case ResultContextType.MembersOnline:
                        DoMembersOnlineInit();
                        break;
                    case ResultContextType.AdvancedSearchResult:
                        DoAdvancedSearchResultInit();
                        break;
                }
            }
            catch (Exception ex)
            {
                _g.ProcessException(ex);
            }
        }

        private void DoMembersOnlineInit()
        {

        }

        private void DoSearchResultInit()
        {

        }

        private void DoQuickSearchResultInit()
        {

        }

        private void DoAdvancedSearchResultInit()
        {

        }

        private void DoAffiliatoinGroupMemberInit()
        {

        }

        private void DoHotListInit()
        {
            if (Request["CategoryID"] != null)
            {
                try
                {
                    HotListCategory = (HotListCategory)Enum.Parse(typeof(HotListCategory), Request["CategoryID"]);
                }
                catch (Exception ex)
                {
                    //ignore invalid category
                }
            }
        }

        /// <summary>
        /// Gets or sets the matches collection
        /// For matchmeter app (Jmeter).
        /// </summary>
        /// <value>My matches collection.</value>
        public MatchesCollection MyMatchesCollection
        {
            get { return _myMatchesCollection; }
            set { _myMatchesCollection = value; }
        }

        #endregion

        # region General page load
        /// <summary>
        /// The page load method initializes the start row and the end row for the results.
        /// Then, dependant upon resutl list context type, a page specific load is fired.
        /// </summary>
        private void Page_Load(object sender, EventArgs e)
        {
            try
            {
                resultListHandler = new ResultListHandler(g, this);
                SetMatchMeter();
                FrameworkGlobals.SaveBackToResultsURL(g);
                int startRow = Constants.NULL_INT;

                //	Default to 1.
                this.startRow = 1;
                StartRow = 1;

                if (!g.ResetSearchStartRow)
                {
                    // Check the hidden input field.
                    if (Request[hidStartRow.UniqueID] != null)
                    {
                        startRow = Int32.Parse(Request[hidStartRow.UniqueID]);

                        if (startRow >= 1)
                        {
                            //	Use internal first, if available
                            this.startRow = startRow;
                            StartRow = startRow;
                        }
                    }
                    else
                    {
                        // If there was no value in the hidden field, check the querystring.
                        if (Request.QueryString[ResultListHandler.QSPARAM_STARTROW] != null)
                        {
                            try
                            {
                                startRow = Int32.Parse(Request.QueryString[ResultListHandler.QSPARAM_STARTROW]);
                            }
                            catch
                            {
                                startRow = Constants.NULL_INT;
                            }

                            if (startRow >= 1)
                            {
                                //	Use internal first, if available
                                this.startRow = startRow;
                                StartRow = startRow;
                            }
                        }
                    }
                }

                EndRow = this.startRow + pageSize - 1;
                if (EndRow > TotalRows)
                {
                    EndRow = TotalRows;
                }

                // Reset the SearchPref override if we are on page 1.  So, if a visitor hits a visitor limit then logs in,
                // this flag is what keeps the search prefs from being overwritten by their saved search prefs.  However, if they
                // start a new search (startRow == 1) then we revert back to their saved search prefs.  There is a loophole (a user
                // navs back to page 1) but let's live with it for now.  See TT 12657.
                if (startRow == Constants.NULL_INT)
                {
                    g.Session.Remove(VisitorLimitHelper.SESSIONKEY_VISITORLIMIT_DONOTOVERWRITESEARCHPREFS);
                    // We need to load the search prefs again by setting it to null.
                    _g.ResetSearchPreferences();
                }

                switch (ResultListContextType)
                {
                    case ResultContextType.HotList:
                        if (resultListHandler.LoadHotListPage())
                        {
                            //check if "who viewed your profile is now a subscriber feature"
                            ListManager listManager = new ListManager();
                            if (this.HotListCategory == List.ValueObjects.HotListCategory.WhoViewedYourProfile
                                && !listManager.IsViewedYourProfileListAvailableToMember(g.Member, g.Brand))
                            {
                                //show subscriber only feature copy
                                phViewedYourProfileForSubscribersOnlyCopy.Visible = true;
                                string subscribeLink = FrameworkGlobals.GetSubscriptionLink(false, (int)PurchaseReasonType.HotlistViewedYourProfile);
                                string nothanksLink = "/Applications/Home/Default.aspx";
                                if (Request.UrlReferrer != null)
                                {
                                    nothanksLink = Request.UrlReferrer.AbsoluteUri;
                                }
                                litViewedYourProfileForSubscribersOnly.Text = string.Format(g.GetResource(litViewedYourProfileForSubscribersOnly.ResourceConstant, this), subscribeLink, nothanksLink);

                                if (g.Member != null)
                                {
                                    int genderMask = g.Member.GetAttributeInt(g.Brand, "gendermask");
                                    if ((genderMask & ConstantsTemp.GENDERID_SEEKING_MALE) == ConstantsTemp.GENDERID_SEEKING_MALE)
                                    {
                                        imgBlurredMale.Visible = true;
                                        imgBlurredFemale.Visible = false;
                                    }
                                }
                            }
                            else
                            {
                                resultListHandler.RenderHotListElements();
                                resultListHandler.BindMemberRepeater();
                            }
                        }
                        break;
                    case ResultContextType.MembersOnline:
                        if (MembersOnlineManager.Instance.IsE2MOLEnabled(g.Brand))
                        {
                            resultListHandler.LoadMembersOnlinePageConsolidated(IgnoreSAFilteredSearchResultsCache, IgnoreSASearchResultsCache, IgnoreAllSearchResultsCache);
                        }
                        else
                        {
                            resultListHandler.LoadMembersOnlinePage();
                        }
                        break;
                    case ResultContextType.SearchResult:
                        resultListHandler.LoadSearchResultPage(IgnoreSAFilteredSearchResultsCache, IgnoreSASearchResultsCache, IgnoreAllSearchResultsCache);
                        showSpotlightProfile();
                        break;
                    case ResultContextType.QuickSearchResult:
                        resultListHandler.LoadQuickSearchResultPage(IgnoreSAFilteredSearchResultsCache, IgnoreSASearchResultsCache, IgnoreAllSearchResultsCache);
                        break;
                    case ResultContextType.KeywordSearchResult:
                        resultListHandler.LoadKeywordSearchResultPage();
                        break;
                    case ResultContextType.ReverseSearchResult:
                        resultListHandler.LoadReverseSearchResultPage(IgnoreSAFilteredSearchResultsCache, IgnoreSASearchResultsCache, IgnoreAllSearchResultsCache);
                        break;
                    case ResultContextType.PhotoGallery:
                        resultListHandler.LoadPhotoGalleryResultPage();
                        break;
                    case ResultContextType.MatchMeterMatches:
                        resultListHandler.LoadMatchMeterMatchesResultPage(MyMatchesCollection, ref MatchResultsMemberScoreDictionary, StartRow, PageSize, ref _TotalRows);
                        break;
                    case ResultContextType.AffiliationGroupMembers:
                        resultListHandler.LoadGroupMemberPage();
                        break;
                    case ResultContextType.AdvancedSearchResult:
                        resultListHandler.LoadAdvancedSearchResultPage();
                        break;
                }

                resultListHandler.RenderPaging();
            }
            catch (Exception ex)
            {
                _g.ProcessException(ex);
            }
        }
        #endregion
        public DropDownList GetMOLSortDropDown()
        {
            return (DropDownList)Parent.FindControl("ddlMemberSort");
        }

        /// <summary>
        /// For Analytics
        /// </summary>
        /// <param name="e"></param>
        public void RenderAnalytics(ArrayList members, bool moreResultsAvailable)
        {
            if (!IsPartialRender)
            {
                int analyticsID = ConstantsTemp.PAGE_SEARCH_RESULTS;
                if (members.Count > 0)
                {
                    // SAGE - each results page should have a different g.Analytics["p"] value.
                    analyticsID += 1 + startRow / pageSize;
                    // SAGE has a limit to how many pages it tracks.
                    if (analyticsID >= ConstantsTemp.PAGE_SEARCH_RESULTS + NumberOfTrackedPages)
                        analyticsID = ConstantsTemp.PAGE_SEARCH_RESULTS + NumberOfTrackedPages;
                }

                g.Analytics.SetAttribute("p", analyticsID.ToString());
            }
        }

        private void SetMatchMeter()
        {
            // MatchMeter (jmeter)
            int mmsett = Int32.Parse(RuntimeSettings.GetSetting("MatchTest_App_Settings", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID));
            int mmenumval = (Int32)WebConstants.MatchMeterFlags.EnableApp;
            if ((mmsett & mmenumval) == mmenumval)
            {
                isMatchMeterEnabled = true;
            }
        }


        #region Member Repeater on item data bound
        public void MemberRepeater_OnItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            count++;

            MiniProfile20 miniProfile = (MiniProfile20)e.Item.FindControl("MiniProfile");

            miniProfile.Counter = count;
            miniProfile.Ordinal = count + startRow - 1;
            miniProfile.DisplayContext = ResultListContextType;
            miniProfile.EnableSingleSelect = EnableSingleSelect;
            // MatchMeter
            miniProfile.IsMatchMeterEnabled = isMatchMeterEnabled;
            switch (ResultListContextType)
            {
                case ResultContextType.HotList:
                    miniProfile.MyEntryPoint = BreadCrumbHelper.EntryPoint.HotLists;
                    miniProfile.HotListCategory = HotListCategory;
                    miniProfile.SaveNote += miniProfile_SaveNote;
                    break;
                case ResultContextType.MembersOnline:
                    miniProfile.MyEntryPoint = BreadCrumbHelper.EntryPoint.MembersOnline;
                    miniProfile.MyMOCollection = this.MyMOCollection;
                    break;
                case ResultContextType.SearchResult:
                    miniProfile.MyEntryPoint = BreadCrumbHelper.EntryPoint.SearchResults;
                    break;
                case ResultContextType.QuickSearchResult:
                    miniProfile.MyEntryPoint = BreadCrumbHelper.EntryPoint.QuickSearchResults;
                    break;
                case ResultContextType.ReverseSearchResult:
                    miniProfile.MyEntryPoint = BreadCrumbHelper.EntryPoint.ReverseSearchResults;
                    break;
                case ResultContextType.MatchMeterMatches:
                    if (MatchResultsMemberScoreDictionary != null)
                    {
                        miniProfile.MatchesScore = MatchResultsMemberScoreDictionary[miniProfile.Member];
                    }
                    break;
                case ResultContextType.AdvancedSearchResult:
                    miniProfile.MyEntryPoint = BreadCrumbHelper.EntryPoint.AdvancedSearchResults;
                    break;

            }
            miniProfile.CategoryID = HotListCategory.ToString();

            if (miniProfile.IsHighlighted)
            {
                highlightedProfileCount++;
            }
        }
        #endregion

        #region JMeter Repeater on item data bound
        public void JMeterRepeater_OnItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            count++;

            JMeterMiniProfile miniProfile = (JMeterMiniProfile)e.Item.FindControl("jmeterMiniProfile");

            miniProfile.Counter = count;
            miniProfile.Ordinal = count + startRow - 1;
            miniProfile.DisplayContext = ResultListContextType;
            miniProfile.EnableSingleSelect = EnableSingleSelect;
            // MatchMeter
            miniProfile.IsMatchMeterEnabled = isMatchMeterEnabled;
            switch (ResultListContextType)
            {
                case ResultContextType.HotList:
                    miniProfile.MyEntryPoint = BreadCrumbHelper.EntryPoint.HotLists;
                    miniProfile.HotListCategory = HotListCategory;
                    miniProfile.SaveNote += miniProfile_SaveNote;
                    break;
                case ResultContextType.MembersOnline:
                    miniProfile.MyEntryPoint = BreadCrumbHelper.EntryPoint.MembersOnline;
                    miniProfile.MyMOCollection = this.MyMOCollection;
                    break;
                case ResultContextType.SearchResult:
                    miniProfile.MyEntryPoint = BreadCrumbHelper.EntryPoint.SearchResults;
                    break;
                case ResultContextType.QuickSearchResult:
                    miniProfile.MyEntryPoint = BreadCrumbHelper.EntryPoint.QuickSearchResults;
                    break;
                case ResultContextType.ReverseSearchResult:
                    miniProfile.MyEntryPoint = BreadCrumbHelper.EntryPoint.ReverseSearchResults;
                    break;
                case ResultContextType.MatchMeterMatches:
                    if (MatchResultsMemberScoreDictionary != null)
                    {
                        miniProfile.MatchesScore = MatchResultsMemberScoreDictionary[miniProfile.Member];
                    }
                    break;
                case ResultContextType.AdvancedSearchResult:
                    miniProfile.MyEntryPoint = BreadCrumbHelper.EntryPoint.AdvancedSearchResults;
                    break;

            }
            miniProfile.CategoryID = HotListCategory.ToString();

            if (miniProfile.IsHighlighted)
            {
                highlightedProfileCount++;
            }
        }
        #endregion

        # region Gallery Member Repeater on item data bound
        public void GalleryMemberRepeater_OnItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            count++;


            var GalleryMiniProfile = (GalleryMiniProfile20)e.Item.FindControl("GalleryMiniProfile");

            Literal rptrNewRow = (Literal)e.Item.FindControl("rptrNewRow");
            Literal rptrNewCell = (Literal)e.Item.FindControl("rptrNewCell");
            Literal rptrEndCell = (Literal)e.Item.FindControl("rptrEndCell");
            Literal rptrEndRow = (Literal)e.Item.FindControl("rptrEndRow");

            GalleryMiniProfile.Counter = count;
            GalleryMiniProfile.Ordinal = this.startRow + count - 1;
            GalleryMiniProfile.DisplayContext = ResultListContextType;
            GalleryMiniProfile.IsMatchMeterInfoDisaplay = _isMatchMeterInfoDisplay;
            GalleryMiniProfile.IsMatchMeterEnabled = isMatchMeterEnabled;
            switch (ResultListContextType)
            {
                case ResultContextType.HotList:
                    GalleryMiniProfile.MyEntryPoint = BreadCrumbHelper.EntryPoint.HotLists;
                    GalleryMiniProfile.HotListCategoryID = (int)HotListCategory;
                    break;
                case ResultContextType.MembersOnline:
                    GalleryMiniProfile.MyEntryPoint = BreadCrumbHelper.EntryPoint.MembersOnline;
                    GalleryMiniProfile.MyMOCollection = this.MyMOCollection;
                    break;
                case ResultContextType.SearchResult:
                    GalleryMiniProfile.MyEntryPoint = BreadCrumbHelper.EntryPoint.SearchResults;
                    break;
                case ResultContextType.QuickSearchResult:
                    GalleryMiniProfile.MyEntryPoint = BreadCrumbHelper.EntryPoint.QuickSearchResults;
                    break;
                case ResultContextType.ReverseSearchResult:
                    GalleryMiniProfile.MyEntryPoint = BreadCrumbHelper.EntryPoint.ReverseSearchResults;
                    break;
                case ResultContextType.AffiliationGroupMembers:
                    GalleryMiniProfile.MyEntryPoint = BreadCrumbHelper.EntryPoint.SearchResults;
                    break;
                case ResultContextType.AdvancedSearchResult:
                    GalleryMiniProfile.MyEntryPoint = BreadCrumbHelper.EntryPoint.AdvancedSearchResults;
                    break;
            }

            if (GalleryMiniProfile.IsHighlighted)
            {
                highlightedProfileCount++;
            }
        }
        #endregion

        # region Photo Gallery  Repeater on item data bound
        public void PhotoGalleryRepeater_OnItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            count++;

            var GalleryProfile = (Applications.PhotoGallery.Controls.PhotoGalleryProfile20)e.Item.FindControl("PhotoGalleryProfile");
            GalleryProfile.ResultMember = (PhotoSearch.ValueObjects.PhotoResultItem)e.Item.DataItem;
            GalleryProfile.row = Math.Abs(count - 1) / ChapterSize + 1;
            GalleryProfile.col = Math.Abs(count - 1) % ChapterSize;
            GalleryProfile.Ordinal = startRow + count - 1;

            #region old layout
            //Literal rptrNewRow = (Literal)e.Item.FindControl("rptrNewRow");
            //Literal rptrNewCell = (Literal)e.Item.FindControl("rptrNewCell");
            //Literal rptrEndCell = (Literal)e.Item.FindControl("rptrEndCell");
            //Literal rptrEndRow = (Literal)e.Item.FindControl("rptrEndRow");

            //if (count % ChapterSize == 1)
            //{
            //    rptrNewRow.Visible = true;
            //    rptrNewCell.Visible = true;

            //    rptrEndCell.Visible = true;
            //}
            //else if (count % ChapterSize == 0)
            //{
            //    rptrNewCell.Visible = true;

            //    rptrEndCell.Visible = true;
            //    rptrEndRow.Visible = true;
            //}
            //else
            //{
            //    rptrNewCell.Visible = true;
            //    rptrEndCell.Visible = true;
            //}

            //if (count == memberNum)
            //{
            //    if (count % ChapterSize == 1)
            //    {
            //        ((Literal)e.Item.FindControl("rptrEndThreeCells")).Visible = true;
            //    }
            //    else if (count % ChapterSize == 0)
            //    {
            //        ((Literal)e.Item.FindControl("rptrEndOneCell")).Visible = true;
            //    }
            //    else if (count % ChapterSize == 2)
            //    {
            //        ((Literal)e.Item.FindControl("rptrEndTwoCells")).Visible = true;
            //    }
            //    else
            //    {
            //        ((Literal)e.Item.FindControl("rptrEndOneCell")).Visible = true;
            //    }
            //}
            #endregion
            if (GalleryProfile.IsHighlighted)
            {
                highlightedProfileCount++;
            }
        }

        #endregion

        #region Web Form Designer generated code, unaltered
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Load += Page_Load;
            Init += Page_Init;
        }
        #endregion

        private void miniProfile_SaveNote(object sender, SaveNoteEventArgs e)
        {
            foreach (RepeaterItem item in MemberRepeater.Items)
            {
                MiniProfile20 miniProfile = (MiniProfile20)item.FindControl("MiniProfile");

                string note = Regex.Replace(miniProfile.Note, @"<(.|\n)*?>", string.Empty);
                int memberID = miniProfile.MemberID;

                ListItemDetail listItemDetail = g.List.GetListItemDetail(HotListCategory,
                     g.Brand.Site.Community.CommunityID,
                     g.Brand.Site.SiteID,
                     memberID);

                if (note != miniProfile.DefaultNote && note != listItemDetail.Comment)
                {

                    int creatorId = g.Session.GetInt(WebConstants.SESSION_PROPERTY_NAME_MEMBERID);
                    ListSA.Instance.AddListMember(HotListCategory,
                         g.Brand.Site.Community.CommunityID,
                         g.Brand.Site.SiteID,
                         creatorId,
                         memberID,
                         note,
                         Constants.NULL_INT,
                         !((g.Member.GetAttributeInt(g.Brand, "HideMask", 0) & (Int32)WebConstants.AttributeOptionHideMask.HideHotLists) == (Int32)WebConstants.AttributeOptionHideMask.HideHotLists),
                         false);

                    if (HotListCategory == HotListCategory.Default)
                    {
                        if (UserNotificationFactory.IsUserNotificationsEnabled(g))
                        {
                            UserNotificationParams unParams = UserNotificationParams.GetParamsObject(memberID, g.Brand.Site.SiteID, g.Brand.Site.Community.CommunityID);
                            UserNotification notification = UserNotificationFactory.Instance.GetUserNotification(new AddedYouAsFavoriteUserNotification(), memberID, creatorId, g);
                            if (notification.IsActivated())
                            {
                                UserNotificationsServiceSA.Instance.AddUserNotification(unParams, notification.CreateViewObject());
                            }
                        }
                    }
                }
            }
        }

        private void showPromotionalProfile()
        {
            try
            {

                if (g.PromotionalProfileEnabled())
                {

                    int memberID = g.PromotionalProfileMember();
                    if (memberID > 0)
                    {
                        promotionProfile.Member = Matchnet.Member.ServiceAdapters.MemberSA.Instance.GetMember(memberID, Matchnet.Member.ServiceAdapters.MemberLoadFlags.None);
                        if (promotionProfile.Member != null)
                        {
                            plcPromotionalProfile.Visible = true;
                            promotionProfile.IsMatchMeterEnabled = isMatchMeterEnabled;
                        }
                    }
                }
            }
            catch (Exception ex) { }
        }


        /// <summary>
        /// For Analytics
        /// </summary>
        /// <param name="e"></param>
        protected override void OnPreRender(EventArgs e)
        {
            try
            {
                if (!IsPartialRender)
                {
                    // Omniture Analytics - Event and property variable tracking
                    if (Convert.ToBoolean(RuntimeSettings.GetSetting("ANALYTICS_OMNITURE", _g.Brand.Site.Community.CommunityID, _g.Brand.Site.SiteID)))
                    {
                        // Search Depth
                        _g.AnalyticsOmniture.Evar25 = Convert.ToString(Math.Round(((double)(startRow - 1) / 12), 0) + 1);

                        // Track total profiles that are highlighted in the search result
                        _g.AnalyticsOmniture.AddEvent("event5");
                        _g.AnalyticsOmniture.AddProductEvent("event5=" + Convert.ToString(this.highlightedProfileCount));
                        //_g.AnalyticsOmniture.Products = Convert.ToString(this.highlightedProfileCount) + ";" + Convert.ToString(this.count) + ";event5=2"; 
                        if (hasSpotlightProfile)
                        {
                            _g.AnalyticsOmniture.AddEvent("event13");

                            _g.AnalyticsOmniture.AddProductEvent("event13=1");
                        }
                    }
                }

                base.OnPreRender(e);
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        private void showSpotlightProfile()
        {
            try
            {
                Control control = Parent;
                while (control != null && control.GetType().ToString() != "ASP.applications_search_searchresults20_ascx")
                {
                    control = control.Parent;
                }
                //((Matchnet.Web.Applications.Search.SearchResults)(this.Parent.Parent)).showPromotionalProfile();
                hasSpotlightProfile = ((Applications.Search.SearchResults20)control).showSpotlightProfile(SearchIDs);
            }
            catch (Exception ex)
            { }

        }


    }
}
