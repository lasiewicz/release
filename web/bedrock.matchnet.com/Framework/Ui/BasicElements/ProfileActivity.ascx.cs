﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Web.Applications.HotList;
using Matchnet.Web.Framework.Ui.FormElements;
using Matchnet.Member.ServiceAdapters;
using Matchnet.List.ValueObjects;
using Matchnet.Web.Applications.UserNotifications;
using Matchnet.Web.Applications.UserNotifications.Controls;
using Matchnet.UserNotifications.ValueObjects;

namespace Matchnet.Web.Framework.Ui.BasicElements
{
    public partial class ProfileActivity : FrameworkControl
    {
        #region Fields
        private bool _hotlistNotificationEnabled = false;
        private int _communityID;
        private int _siteID;
        private BreadCrumbHelper.EntryPoint _myEntryPoint = BreadCrumbHelper.EntryPoint.HomePage;
        bool _hasHotListActivity = false;

        #endregion

        #region Properties
        public BreadCrumbHelper.EntryPoint MyEntryPoint
        {
            get { return (_myEntryPoint); }
            set { _myEntryPoint = value; }
        }

        public bool HasHotListActivity
        {
            get { return _hasHotListActivity; }
            set { _hasHotListActivity = value; }
        }

        public string TrackingParam { get; set; }
        #endregion

        #region Event Handlers
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void repeaterActivityItems_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                HotlistActivityItem item = e.Item.DataItem as HotlistActivityItem;
                HyperLink lnkUserName1 = e.Item.FindControl("lnkUserName1") as HyperLink;
                Member.ServiceAdapters.Member member = MemberSA.Instance.GetMember(item.MemberID, MemberLoadFlags.None);
                lnkUserName1.Text = member.GetUserName(g.Brand);
                lnkUserName1.NavigateUrl = BreadCrumbHelper.MakeViewProfileLink(_myEntryPoint, item.MemberID);
                if (!String.IsNullOrEmpty(TrackingParam))
                {
                    lnkUserName1.NavigateUrl = BreadCrumbHelper.AppendParamToProfileLink(lnkUserName1.NavigateUrl, TrackingParam + Server.UrlEncode(" - Activity feed"));
                }
                FrameworkLiteral literalActivityAction = e.Item.FindControl("literalActivityAction") as FrameworkLiteral;
                LastTime lastTimeAction = e.Item.FindControl("lastTimeAction") as LastTime;

                //hotlist info
                if (item.ActionDate != DateTime.MinValue)
                {
                switch ((int)item.HotlistCategory)
                {
                    case (int)HotListCategory.WhoViewedYourProfile:
                        literalActivityAction.ResourceConstant = "TXT_HOTLIST_ACTION_VIEWED_YOU";
                        break;
                    case (int)HotListCategory.WhoIMedYou:
                        literalActivityAction.ResourceConstant = "TXT_HOTLIST_ACTION_IMED_YOU";
                        break;
                    case (int)HotListCategory.WhoTeasedYou:
                        literalActivityAction.ResourceConstant = "TXT_HOTLIST_ACTION_TEASED_YOU";
                        break;
                    case (int)HotListCategory.WhoAddedYouToTheirFavorites:
                        literalActivityAction.ResourceConstant = "TXT_HOTLIST_ACTION_FAVORITED_YOU";
                        break;
                    case (int)HotListCategory.WhoEmailedYou:
                        literalActivityAction.ResourceConstant = "TXT_HOTLIST_ACTION_EMAILED_YOU";
                        break;
                    case (int)HotListCategory.WhoSentYouECards:
                        literalActivityAction.ResourceConstant = "TXT_HOTLIST_ACTION_ECARDED_YOU";
                        break;
                }
                    //time
                    lastTimeAction.LoadLastTime(item.ActionDate);
                }
            }
        }

        protected void repeaterNotificationItems_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                UserNotificationViewObject un = e.Item.DataItem as UserNotificationViewObject;
                FrameworkLiteral thumbnail = e.Item.FindControl("imgThumbnail") as FrameworkLiteral;
                FrameworkLiteral literalActivityAction = e.Item.FindControl("literalUserNotification") as FrameworkLiteral;
                FrameworkLiteral lastTimeUserNotification = e.Item.FindControl("lastTimeUserNotification") as FrameworkLiteral;
                Literal omnitureTag = e.Item.FindControl("omnitureTag") as Literal;

                thumbnail.Text = un.Thumbnail;
                literalActivityAction.Text = un.Text;
                lastTimeUserNotification.Text = un.Timestamp;
                omnitureTag.Text = un.OmnitureTag;
            }
        }
        #endregion

        #region Public Methods
        public void LoadProfileActivity()
        {
            _hotlistNotificationEnabled = HotListViewHandler.IsNotificationsEnabled(g);
            _communityID = g.Brand.Site.Community.CommunityID;
            _siteID = g.Brand.Site.SiteID;

            LoadSentHotlist();
            LoadReceivedHotlist();
            LoadActivityFeed();
        }

        #endregion

        #region Private Methods
        private void LoadSentHotlist()
        {
            bool hasHotlistData = true;
            literalYouViewed.Text = _g.List.GetCount(Matchnet.List.ValueObjects.HotListCategory.MembersYouViewed, _communityID, _siteID).ToString();
            literalYouEmailed.Text = _g.List.GetCount(Matchnet.List.ValueObjects.HotListCategory.MembersYouEmailed, _communityID, _siteID).ToString();
            literalYouFlirted.Text = _g.List.GetCount(Matchnet.List.ValueObjects.HotListCategory.MembersYouTeased, _communityID, _siteID).ToString();
            literalYouFavorited.Text = _g.List.GetCount(Matchnet.List.ValueObjects.HotListCategory.Default, _communityID, _siteID).ToString();
            literalYouIM.Text = _g.List.GetCount(Matchnet.List.ValueObjects.HotListCategory.MembersYouIMed, _communityID, _siteID).ToString();
            literalYouEcard.Text = _g.List.GetCount(Matchnet.List.ValueObjects.HotListCategory.MembersYouSentECards, _communityID, _siteID).ToString();

            if (literalYouViewed.Text == "0" && literalYouEmailed.Text == "0" && literalYouFlirted.Text == "0"
                && literalYouFavorited.Text == "0" && literalYouIM.Text == "0" && literalYouEcard.Text == "0")
            {
                hasHotlistData = false;
                phSentContent.Visible = false;
                phSentNoContent.Visible = true;
            }
            else
            {
                HasHotListActivity = true; //omniture, referenced by homepage
            }

            if (_hotlistNotificationEnabled && hasHotlistData)
            {
                int newNotifications = 0;
                newNotifications = HotListViewHandler.GetHotlistNewNotificationCount(Matchnet.List.ValueObjects.HotListCategory.MembersYouViewed, g);
                if (newNotifications > 0)
                {
                    phYouViewedNew.Visible = true;
                    literalYouViewedNew.Text = newNotifications.ToString();
                }

                newNotifications = HotListViewHandler.GetHotlistNewNotificationCount(Matchnet.List.ValueObjects.HotListCategory.MembersYouEmailed, g);
                if (newNotifications > 0)
                {
                    phYouEmailedNew.Visible = true;
                    literalYouEmailedNew.Text = newNotifications.ToString();
                }

                newNotifications = HotListViewHandler.GetHotlistNewNotificationCount(Matchnet.List.ValueObjects.HotListCategory.MembersYouTeased, g);
                if (newNotifications > 0)
                {
                    phYouFlirtedNew.Visible = true;
                    literalYouFlirtedNew.Text = newNotifications.ToString();
                }

                newNotifications = HotListViewHandler.GetHotlistNewNotificationCount(Matchnet.List.ValueObjects.HotListCategory.Default, g);
                if (newNotifications > 0)
                {
                    phYouFavoritedNew.Visible = true;
                    literalYouFavoritedNew.Text = newNotifications.ToString();
                }

                newNotifications = HotListViewHandler.GetHotlistNewNotificationCount(Matchnet.List.ValueObjects.HotListCategory.MembersYouIMed, g);
                if (newNotifications > 0)
                {
                    phYouIMNew.Visible = true;
                    literalYouIMNew.Text = newNotifications.ToString();
                }

                newNotifications = HotListViewHandler.GetHotlistNewNotificationCount(Matchnet.List.ValueObjects.HotListCategory.MembersYouSentECards, g);
                if (newNotifications > 0)
                {
                    phYouEcardNew.Visible = true;
                    literalYouEcardNew.Text = newNotifications.ToString();
                }

            }

        }

        private void LoadReceivedHotlist()
        {
            bool hasHotlistData = true;
            literalViewedYou.Text = _g.List.GetCount(Matchnet.List.ValueObjects.HotListCategory.WhoViewedYourProfile, _communityID, _siteID).ToString();
            literalEmailedYou.Text = _g.List.GetCount(Matchnet.List.ValueObjects.HotListCategory.WhoEmailedYou, _communityID, _siteID).ToString();
            literalFlirtedYou.Text = _g.List.GetCount(Matchnet.List.ValueObjects.HotListCategory.WhoTeasedYou, _communityID, _siteID).ToString();
            literalFavoritedYou.Text = _g.List.GetCount(Matchnet.List.ValueObjects.HotListCategory.WhoAddedYouToTheirFavorites, _communityID, _siteID).ToString();
            literalIMYou.Text = _g.List.GetCount(Matchnet.List.ValueObjects.HotListCategory.WhoIMedYou, _communityID, _siteID).ToString();
            literalEcardYou.Text = _g.List.GetCount(Matchnet.List.ValueObjects.HotListCategory.WhoSentYouECards, _communityID, _siteID).ToString();
            literalBothYes.Text = _g.List.GetCount(Matchnet.List.ValueObjects.HotListCategory.MutualYes, _communityID, _siteID).ToString();

            if (literalViewedYou.Text == "0" && literalEmailedYou.Text == "0" && literalFlirtedYou.Text == "0"
                && literalFavoritedYou.Text == "0" && literalIMYou.Text == "0" && literalEcardYou.Text == "0" && literalBothYes.Text == "0")
            {
                hasHotlistData = false;
                phReceivedContent.Visible = false;
                phReceivedNoContent.Visible = true;
            }
            else
            {
                HasHotListActivity = true; //omniture, referenced by homepage
            }
            

            if (_hotlistNotificationEnabled && hasHotlistData)
            {
                int newNotifications = 0;
                newNotifications = HotListViewHandler.GetHotlistNewNotificationCount(Matchnet.List.ValueObjects.HotListCategory.WhoViewedYourProfile, g);
                if (newNotifications > 0)
                {
                    phViewedYouNew.Visible = true;
                    literalViewedYouNew.Text = newNotifications.ToString();
                }

                newNotifications = HotListViewHandler.GetHotlistNewNotificationCount(Matchnet.List.ValueObjects.HotListCategory.WhoEmailedYou, g);
                if (newNotifications > 0)
                {
                    phEmailedYouNew.Visible = true;
                    literalEmailedYouNew.Text = newNotifications.ToString();
                }

                newNotifications = HotListViewHandler.GetHotlistNewNotificationCount(Matchnet.List.ValueObjects.HotListCategory.WhoTeasedYou, g);
                if (newNotifications > 0)
                {
                    phFlirtedYouNew.Visible = true;
                    literalFlirtedYouNew.Text = newNotifications.ToString();
                }

                newNotifications = HotListViewHandler.GetHotlistNewNotificationCount(Matchnet.List.ValueObjects.HotListCategory.WhoAddedYouToTheirFavorites, g);
                if (newNotifications > 0)
                {
                    phFavoritedYouNew.Visible = true;
                    literalFavoritedYouNew.Text = newNotifications.ToString();
                }

                newNotifications = HotListViewHandler.GetHotlistNewNotificationCount(Matchnet.List.ValueObjects.HotListCategory.WhoIMedYou, g);
                if (newNotifications > 0)
                {
                    phIMYouNew.Visible = true;
                    literalIMYouNew.Text = newNotifications.ToString();
                }

                newNotifications = HotListViewHandler.GetHotlistNewNotificationCount(Matchnet.List.ValueObjects.HotListCategory.WhoSentYouECards, g);
                if (newNotifications > 0)
                {
                    phEcardYouNew.Visible = true;
                    literalEcardYouNew.Text = newNotifications.ToString();
                }

                newNotifications = HotListViewHandler.GetHotlistNewNotificationCount(Matchnet.List.ValueObjects.HotListCategory.MutualYes, g);
                if (newNotifications > 0)
                {
                    phBothYesNew.Visible = true;
                    literalBothYesNew.Text = newNotifications.ToString();
                }
            }
        }

        private void LoadActivityFeed()
        {
            UserNotificationsControl unCtrl = this.LoadControl("/Applications/UserNotifications/Controls/UserNotificationsControl.ascx") as UserNotificationsControl;
            if (UserNotificationFactory.IsUserNotificationsEnabled(g))
            {
                repeaterActivityItems.Visible = false;
                phActivityFeedViewMore.Visible = false;

                repeaterNotificationItems.Visible = true;
                int numberOfActivities = 10;
                try
                {
                    numberOfActivities = Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("NUMBER_OF_RECENT_USER_NOTIFICATIONS", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID));
                }
                catch (Exception ex)
                {
                    g.ProcessException(ex);
                }

                List<UserNotificationViewObject> notifications = unCtrl.GetUserNotficationViewObjects(g.Member.MemberID);
                if (null != notifications && notifications.Count > 0)
                {
                    if (notifications.Count < numberOfActivities)
                    {
                        phNotificationsFeedViewMore.Visible = false;
                    }
                    notifications.Sort();
                    repeaterNotificationItems.DataSource = notifications.Take(numberOfActivities);
                    repeaterNotificationItems.DataBind();
                }
                else
                {
                    phActivityFeedContent.Visible = false;
                    phActivityFeedNoContent.Visible = true;
                }
            }
            else
            {
                repeaterActivityItems.Visible = true;
                repeaterNotificationItems.Visible = false;
                phNotificationsFeedViewMore.Visible = false;


                int numberOfActivities = 5;
                try
                {
                    numberOfActivities = Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("NUMBER_OF_RECENT_ACTIVITY_FEEDS", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID));
                }
                catch (Exception ex)
                {
                    g.ProcessException(ex);
                }

                //list of categories to include in activity feed
                List<Matchnet.List.ValueObjects.HotListCategory> categories = new List<Matchnet.List.ValueObjects.HotListCategory>();
                categories.Add(Matchnet.List.ValueObjects.HotListCategory.WhoViewedYourProfile);
                categories.Add(Matchnet.List.ValueObjects.HotListCategory.WhoTeasedYou);
                categories.Add(Matchnet.List.ValueObjects.HotListCategory.WhoEmailedYou);
                categories.Add(Matchnet.List.ValueObjects.HotListCategory.WhoSentYouECards);
                categories.Add(Matchnet.List.ValueObjects.HotListCategory.WhoAddedYouToTheirFavorites);
                categories.Add(Matchnet.List.ValueObjects.HotListCategory.WhoIMedYou);
            
                //get recent activities
                List<HotlistActivityItem> listActivities = HotListViewHandler.GetLatestHotlistActivity(categories.ToArray(), numberOfActivities, g);
                if (listActivities.Count > 0)
                    if (null != listActivities && listActivities.Count > 0)
                {
                    if (listActivities.Count < numberOfActivities)
                    {
                        phActivityFeedViewMore.Visible = false;
                    }
                    listActivities.Sort();
                    repeaterActivityItems.DataSource = listActivities.Take(numberOfActivities);
                    repeaterActivityItems.DataBind();
                }
                else
                {
                    phActivityFeedContent.Visible = false;
                    phActivityFeedNoContent.Visible = true;
                }
            }
        }

        #endregion
    }
}
