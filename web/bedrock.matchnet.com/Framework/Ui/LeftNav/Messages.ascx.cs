using System;
using System.Web.UI;
using System.Web.UI.WebControls;

using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Ui.BasicElements;

using Matchnet.Email.ServiceAdapters;
using Matchnet.Member.ServiceAdapters;

namespace Matchnet.Web.Framework.Ui.LeftNav
{
	/// <summary>
	///		Summary description for Messages.
	/// </summary>
	public class Messages : LeftNavHandler
	{
		protected const string CURRENT_STEP_PREFIX = "&#187;&nbsp;";
		private const int MAX_MAILBOX_NAME_SIZE = 10;
		private const int INBOX_IDENTIFIER = 1;
		private const int SENT_FOLDER_IDENTIFIER = 3;
		protected System.Web.UI.WebControls.Panel leftNavEmailFolders;

		protected LeftNavigationLink lnlMessageSettings;
		protected LeftNavigationLink lnlFolderSettings;
		protected Matchnet.Web.Framework.Txt txtMessages;
		protected System.Web.UI.WebControls.Panel pnlQuickTip;		
		protected Image imgTip;
		protected Txt txtTip;

		private void Page_Load(object sender, System.EventArgs e)
		{
			try
			{
				LEFT_NAV_SELECTION currentSelection = LEFT_NAV_SELECTION.UNKNOWN;
				
				currentSelection = handleLeftNavSelection();
				
				buildFoldersList(currentSelection);
			}
			catch (Exception ex)
			{
				g.ProcessException(ex);
			}
		}

		private LEFT_NAV_SELECTION handleLeftNavSelection() 
		{
			LEFT_NAV_SELECTION currentSelection = determineCurrentLeftNavSelection();

			if(currentSelection != LEFT_NAV_SELECTION.UNKNOWN) 
			{
				switch(currentSelection) 
				{
					case LEFT_NAV_SELECTION.MESSAGE_SETTINGS:
						lnlMessageSettings.IsSelected = true;
						pnlQuickTip.Visible = true;
						txtTip.ResourceConstant = "TXT_MESSAGE_SETTINGS_TIP";
						break;
					case LEFT_NAV_SELECTION.FOLDER_SETTINGS:
						lnlFolderSettings.IsSelected = true;
						break;
				}
			}

			return currentSelection;
		}

		private LEFT_NAV_SELECTION determineCurrentLeftNavSelection()
		{
			string controlName = g.AppPage.ControlName;

			switch(controlName.ToLower().Trim()) 
			{
				case "messagesettings":
					return LEFT_NAV_SELECTION.MESSAGE_SETTINGS;
				case "foldersettings":
					return LEFT_NAV_SELECTION.FOLDER_SETTINGS;
				case "compose":
					return LEFT_NAV_SELECTION.COMPOSE_MESSAGE;
				case "mailbox":
					return LEFT_NAV_SELECTION.FOLDER;
			}

			return LEFT_NAV_SELECTION.UNKNOWN;
		}

		// build out the list of known member folders for the left navigation bar
		private void buildFoldersList(LEFT_NAV_SELECTION currentSelection)
		{
			System.Web.UI.WebControls.Image newMessageImage = null;
			int currentFolderSelected = -1;
			
			// set the current selected folder for left nav selection indication
			if(currentSelection == LEFT_NAV_SELECTION.FOLDER) 
			{
				if(Request["MemberFolderID"] != null && !Request["MemberFolderID"].Equals(string.Empty)) 
				{
					currentFolderSelected = Conversion.CInt(Request["MemberFolderID"]);
				}
				else 
				{
					currentFolderSelected = Messages.INBOX_IDENTIFIER;
				}
			}

			// set quicktip if necessary
			if( currentFolderSelected == Messages.INBOX_IDENTIFIER )
			{
				pnlQuickTip.Visible = true;
				txtTip.ResourceConstant = "TXT_INBOX_TIP";
			}
			else if( currentFolderSelected == Messages.SENT_FOLDER_IDENTIFIER )
			{
				pnlQuickTip.Visible = true;
				txtTip.ResourceConstant = "TXT_SENT_FOLDER_TIP";
			}

			// find and insert all the member's folders
			foreach(Matchnet.Email.ValueObjects.EmailFolder currentFolder in g.MemberEmailFolders)
			{
				LeftNavigationLink folderLink = (LeftNavigationLink)LoadControl("../BasicElements/LeftNavigationLink.ascx");
				folderLink.Href = "/Applications/Email/MailBox.aspx?MemberFolderID=" + currentFolder.MemberFolderID;
				folderLink.CommunitiesForBullet = "CI;";
								
				if(currentFolder.SystemFolder) 
				{
					switch(currentFolder.MemberFolderID) 
					{
						case (int) Matchnet.Email.ValueObjects.SystemFolders.Inbox:
							folderLink.ResourceConstant = "TOK_INBOX";
							break;
						case (int)Matchnet.Email.ValueObjects.SystemFolders.Draft:
							folderLink.ResourceConstant = "TXT_DRAFT";
							break;
						case (int)Matchnet.Email.ValueObjects.SystemFolders.Sent:
							folderLink.ResourceConstant = "TXT_SENT";
							break;
						case (int)Matchnet.Email.ValueObjects.SystemFolders.Trash:
							folderLink.ResourceConstant = "TXT_TRASH";
							break;
						default:
							// unknown system folder found...fall-thru and set to database description field
							break;
					}
				} 
				
				if(folderLink.Text == null) 
				{
					if(g.Brand.Site.LanguageID == (int)Matchnet.Language.Hebrew) 
					{
						folderLink.Text = FrameworkGlobals.GetUnicodeText(currentFolder.Description);
					} 
					else 
					{
						folderLink.Text = currentFolder.Description;
					}
				}
				
				if(!currentFolder.SystemFolder && folderLink.Text.Trim().Length > MAX_MAILBOX_NAME_SIZE) 
				{
					folderLink.Text = folderLink.Text.Substring(0, MAX_MAILBOX_NAME_SIZE) + "...";
				} 

				Int32 messageCount = EmailMessageSA.Instance.RetrieveMessages(g.Member.MemberID, g.Brand.Site.Community.CommunityID, currentFolder.MemberFolderID, g.Brand.Site.SiteID).Count;
				folderLink.TextToAppend += ": <strong>" + messageCount.ToString() + "</strong>";

				// Indicate the selected folder in left nav with indicator arrow(s)
				if(currentFolderSelected > -1 && currentFolderSelected == currentFolder.MemberFolderID) 
				{
					folderLink.IsSelected = true;
				} 
				
				// check to see if there are any new messages in the default inbox
				if(currentFolder.MemberFolderID == INBOX_IDENTIFIER) 
				{
					if (g.hasNewMessage)
					{
						newMessageImage = new System.Web.UI.WebControls.Image();
						newMessageImage.ImageUrl = Framework.Image.GetURLFromFilename("icon_newMessages.gif");
						newMessageImage.BorderWidth = 0;
						newMessageImage.CssClass = "leftNavImageAlign";
						newMessageImage.Attributes.Add("class", "leftNavImageAlign");
						leftNavEmailFolders.Controls.Add(newMessageImage);
					}
				}

				leftNavEmailFolders.Controls.Add(folderLink);
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
