<%@ Control Language="c#" AutoEventWireup="false" CodeBehind="Messages.ascx.cs" Inherits="Matchnet.Web.Framework.Ui.LeftNav.Messages"
    TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn" TagName="LeftNavigationLink" Src="../BasicElements/LeftNavigationLink.ascx" %>
<%@ Register TagPrefix="mn" TagName="AdUnit" Src="/Framework/UI/Advertising/AdUnit.ascx" %>
<div id="leftNav">
    <span class="leftNavTitle">
        <mn:Txt runat="server" ID="txtMessages" ResourceConstant="NAV_MESSAGES" />
    </span>
    <asp:Panel runat="server" ID="leftNavEmailFolders" />
    <mn:LeftNavigationLink runat="server" ID="lnlMessageSettings" Href="/Applications/Email/MessageSettings.aspx"
        ResourceConstant="NAV_SUB_ALERTS_SETTINGS" CommunitiesForBullet="CI;" />
    <mn:LeftNavigationLink runat="server" ID="lnlFolderSettings" Href="/Applications/Email/FolderSettings.aspx"
        ResourceConstant="NAV_LEFT_FOLDER_SETTINGS" CommunitiesForBullet="CI;" />
</div>
<mn:AdUnit id="adVerticalBanner" Size="VerticalBanner" runat="server" expandImageTokens="true"
    GAMAdSlot="inbox_left_158x245" />
<div style="width: 100%; text-align: center;">
    <mn:AdUnit id="adChatSupport" Size="SMALLSQUARECHAT" visible="false" runat="server" />
</div>
<asp:Panel runat="server" ID="pnlQuickTip" Visible="False">
    <div id="tipAppLeft">
        <mn:Image runat="server" ID="imgTip" FileName="icon_tip.gif" Hspace="3" Vspace="0"
            align="absmiddle" />
        <mn:Txt runat="server" ID="txtQuickTip" ResourceConstant="TXT_QUICK_TIP" CssClass="tipAppTitle" />
        <br />
        <mn:Txt runat="server" ExpandImageTokens="true" ID="txtTip" />
    </div>
</asp:Panel>
