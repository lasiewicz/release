﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.ExternalMail.ServiceAdapters;

namespace Matchnet.Web.Framework.Ui.Feedback
{
    public class FeedbackHelper
    {
        public enum FeedbackType
        {
            None = 0,
            BetaProfile = 1,
            BetaMatches = 2,
            BetaProfilePopup = 3
        }

        public static void SendBetaFeedbackEmail(FeedbackType feedbackType, int brandID, string senderName, string senderEmailAddress, int senderMemberID, string feedbackText)
        {
            string subject = "";
            if (feedbackType != FeedbackType.None)
            {
                switch (feedbackType)
                {
                    case FeedbackType.BetaProfile:
                        subject = "Profile Beta Feedback (" + senderMemberID.ToString() + ")";
                        break;
                    case FeedbackType.BetaProfilePopup:
                        subject = "Profile Popup Beta Feedback (" + senderMemberID.ToString() + ")";
                        break;
                    case FeedbackType.BetaMatches:
                        subject = "Matches Beta Feedback (" + senderMemberID.ToString() + ")";
                        break;
                }

                ExternalMailSA.Instance.SendBetaFeedback(brandID,
                                                    senderName,
                                                    senderEmailAddress,
                                                    senderMemberID.ToString(),
                                                    HttpContext.Current.Request.ServerVariables["HTTP_USER_AGENT"],
                                                    HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"],
                                                    subject,
                                                    feedbackText,
                                                    (int)feedbackType);
            }
        }

        public static bool IsProfile30FeedbackEnabled(int communityID, int siteID, int brandID)
        {
            string isEnabled = "false";
            try
            {
                isEnabled = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ENABLE_PROFILE30_FEEDBACK", communityID, siteID, brandID);
            }
            catch (Exception ex)
            {
                //setting missing
                isEnabled = "false";
            }

            if (isEnabled.ToLower().Trim() == "true")
                return true;
            else
                return false;
        }

        public static bool IsSearchRedesignFeedbackEnabled(int communityID, int siteID, int brandID)
        {
            string isEnabled = "false";
            try
            {
                isEnabled = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ENABLE_SEARCH_REDESIGN_FEEDBACK", communityID, siteID, brandID);
            }
            catch (Exception ex)
            {
                //setting missing
                isEnabled = "false";
            }

            if (isEnabled.ToLower().Trim() == "true")
                return true;
            else
                return false;
        }
    }
}
