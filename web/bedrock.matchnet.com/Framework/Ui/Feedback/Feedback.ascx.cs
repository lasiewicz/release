﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Matchnet.Web.Framework.Ui.Feedback
{
    public partial class Feedback : FrameworkControl
    {
        protected FeedbackHelper.FeedbackType _feedbackType = FeedbackHelper.FeedbackType.None;
        public string TitleResourceConstant 
        { 
            get { return literalFeedbackTitle.ResourceConstant; } 
            set { literalFeedbackTitle.ResourceConstant = value; } 
        }

        public FeedbackHelper.FeedbackType FeedbackType
        {
            get { return _feedbackType; }
            set { _feedbackType = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (g.Member != null)
                {
                    txtYourEmail.Text = g.Member.EmailAddress;
                    txtYourName.Text = g.Member.GetUserName(g.Brand);
                }
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            if (_feedbackType == FeedbackHelper.FeedbackType.None)
                this.Visible = false;
            else if (_feedbackType == FeedbackHelper.FeedbackType.BetaMatches)
                this.literalFeedbackTitle.ResourceConstant = "TXT_FEEDBACK_TITLE_MATCHES_BETA";
            

            base.OnPreRender(e);
        }
    }
}