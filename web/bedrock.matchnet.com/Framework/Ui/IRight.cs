﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Session.ServiceAdapters;
using System.Text;
using Matchnet.Session.ValueObjects;
using Matchnet.Web.Framework.Ui.BasicElements;
using Matchnet.Web.Framework.Ui.ProfileElements;
using Matchnet.Web.Applications.MemberProfile.ProfileTabs30.Controls;

namespace Matchnet.Web.Framework.Ui
{
    public interface IRight
    {
        void HideUserNameLogout();
        void ShowWelcomeText();
        void HideLoginLink();
        void ShowColorCodeProfileExamples();
        void ShowColorCodeTestHelp();
        void ShowColorCodeAbout();
        void ShowColorCodeFeedback();
        void HideGamChannels();
        //void ShowMemberSlideShow();
        void ShowMiniMemberSlideShow();
        void ShowMiniMemberSlideShowBelowAd();
        string MovieUrl {get;}
        MiniSearch MiniSearchControl {get;}
        ActivityCenter ActivityCenter { get; }
        ProfileToolBar ProfileToolBar { get; }
        bool IsVisible { get; set; }
        FrameworkControl Control { get; }
        ProfileDetails ProfileDetails { get; }
    }
}
