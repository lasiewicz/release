﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Lib;
using Matchnet.Web.Framework.Managers;

namespace Matchnet.Web.Framework.Ui.PageAnnouncements
{
    public partial class SubscriptionExpirationAlert : FrameworkControl
    {
        //The alert will be shown to members every x hours.
        protected int CookieExpirationHours
        {
            get { return SettingsManager.GetSettingInt("SUBSCRIPTION_EXPIRATION_ALERT_SHOW_EVERY_X_HOURS", _g.Brand); }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                int memGenderMask = g.Member.GetAttributeInt(g.Brand, WebConstants.ATTRIBUTE_NAME_GENDERMASK);
                string currentMemberSex = ((memGenderMask & ConstantsTemp.GENDERID_MALE) == ConstantsTemp.GENDERID_MALE) ? "M" : "F";
                if (currentMemberSex == "M")
                {
                    lblAnnouncementIntro.Text = g.GetResource("TXT_ANNOUNCEMENT_INTRO_FOR_MALE", this);
                    lblAnnouncementMessage.Text = g.GetResource("TXT_ANNOUNCEMENT_MESSAGE_MALE", this);

                }
                else
                {
                    lblAnnouncementIntro.Text = g.GetResource("TXT_ANNOUNCEMENT_INTRO_FOR_FEMALE", this);
                    lblAnnouncementMessage.Text = g.GetResource("TXT_ANNOUNCEMENT_MESSAGE_FEMALE", this);
                }

                int daysTillRenwal = SubscriptionExpirationManager.Instance.GetDaysTillRenewal(g);
                lblAnnouncementTitle.Text = g.GetResource("TXT_ANNOUNCEMENT_TITLE_X", this).Replace("[X]", daysTillRenwal.ToString());
                if (daysTillRenwal == 0)
                {
                    lblAnnouncementTitle.Text = g.GetResource("TXT_ANNOUNCEMENT_TITLE_ZERO_DAYS", this);
                }
                if (daysTillRenwal == 1)
                {
                    lblAnnouncementTitle.Text = g.GetResource("TXT_ANNOUNCEMENT_TITLE_ONE_DAY", this);
                }
                else if (daysTillRenwal == 2)
                {
                    lblAnnouncementTitle.Text = g.GetResource("TXT_ANNOUNCEMENT_TITLE_TWO_DAYS", this);
                }

                //plcPremiumButton.Visible = SubscriptionExpirationManager.Instance.ShowUpgradeLink(g);

                g.AnalyticsOmniture.Prop75 = "displayreminder";
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }

        }
    }
}
