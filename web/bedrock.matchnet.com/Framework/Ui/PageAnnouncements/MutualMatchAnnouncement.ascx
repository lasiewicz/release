﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MutualMatchAnnouncement.ascx.cs" Inherits="Matchnet.Web.Framework.Ui.PageAnnouncements.MutualMatchAnnouncement" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>

<%--Mutual Match prompt / you only see it once until you hit close if you want to see it again, add this to url &showMatchRatingAnnouncement=true--%>
<div id="MutualMatchAnnouncement" class="feature-announcement image">
    <a class="click-close" href="#"><mn:Txt ID="txtClose" ResourceConstant="TXT_CLOSE" runat="server" /></a>
    <h2><mn:Txt ID="txtAnnouncementTitle" ResourceConstant="TXT_ANNOUNCEMENT_TITLE" runat="server" /></h2>
    <h3><mn:Txt ID="txtAnnouncementTagLine" ResourceConstant="TXT_TAG_LINE" runat="server" /></h3>
    <a href="#" class="trigger"><mn:Txt ID="txtLearnMore" ResourceConstant="TXT_LEARN_MORE" runat="server" /></a>
    <blockquote><mn:Txt ID="txtAnnouncementDesc" ResourceConstant="TXT_ANNOUNCEMENT_DESC" runat="server" /></blockquote>
    <%--<p class="desc"><mn:Txt ID="txtAnnouncementDisc" ResourceConstant="TXT_ANNOUNCEMENT_DISC" runat="server" /></p>--%>
    
    <div class="bubble-layer mutual-match left spr-parent">
        <span class="spr s-icon-close"></span>
        <blockquote><mn:Txt ID="txt2" ResourceConstant="TXT_TOOLTIP_TITLE" runat="server" /></blockquote>
        <p><mn:Txt ID="txt1" ResourceConstant="TXT_TOOLTIP_BODY" runat="server" /></p>
    </div>
</div>


<script type="text/javascript">
    var featureAnnouncement = $j("#MutualMatchAnnouncement");

    $j(".click-close", featureAnnouncement).click(function (e) {
        e.preventDefault();
        MutualMatchClosePrompt();
    });

    //close prompt
    function MutualMatchClosePrompt() {
        //hide prompt
        featureAnnouncement.hide();

        //update member info that prompt was displayed and closed
        MutualMatchUpdatePromptSeen();

        return false;
    }

    function MutualMatchUpdatePromptSeen() {
        $j.ajax({
            type: "POST",
            url: "/Applications/API/Member.asmx/UpdateAttributeInt",
            data: "{memberID:" + <%=MemberID.ToString() %> + ", attributeName: \"PageAnnouncementViewedMask\", attributeValue:1}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function(result) {
                var updateResult = (typeof result.d == 'undefined') ? result : result.d;
                if (updateResult.Status != 2) {
                    alert(updateResult.StatusMessage);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert(textStatus);
            }
        });
    }
    
    var trigger = $j('#MutualMatchAnnouncement .trigger'),
        layer = $j('.bubble-layer.mutual-match'),
        close = layer.find('.s-icon-close');

    trigger.click(function (e) {
        layer.toggle();
        e.preventDefault();
    });
    close.click(function () {
        layer.hide();
    });
        

</script>