﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Matchnet.Web.Framework.Ui.PageAnnouncements
{
    public partial class FacebookFeatureAnnouncement : FrameworkControl
    {
        protected int MemberID = Constants.NULL_INT;

        #region Event Handlers
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        protected override void OnPreRender(EventArgs e)
        {
            try
            {
                if (this.Visible)
                {
                    //omniture
                    g.AnalyticsOmniture.Evar57 = "FB_Connect_Prompt";
                }

                base.OnPreRender(e);
            }
            catch (Exception ex)
            { g.ProcessException(ex); }
        }

        #endregion

        #region Public Methods
        public void LoadAnnouncement()
        {
            if (g.Member != null)
            {
                MemberID = g.Member.MemberID;
                MemberID.ToString();
            }

            lnkFBPhoto.Title = g.GetResource("TXT_FB_PHOTOS_TITLE", this);
            lnkLikes.Title = g.GetResource("TXT_FB_LIKES_TITLE", this);
        }
        #endregion
    }
}