﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ServiceAdapters.Interfaces;
using Matchnet.Member.ValueObjects.Interfaces;
using Matchnet.Web.Framework.Managers;

namespace Matchnet.Web.Framework.Ui.PageElements
{
    public partial class NoPhoto20 : FrameworkControl
    {
        public enum PhotoMode { UploadPhotoNow, UploadPhotoNowSmallText, NoPhoto, NoPhotoNoText, NoPhotoSmallText, OnlyMembers, NoPrivatePhoto, MiniSearch, NoPhotoNoTextNoURL, NoPhotoNoLink };

        private bool _enableLink = true;
        private int _memberID;
        private IMemberDTO _member;
        private string _destUrl;
        private PhotoMode _Mode = PhotoMode.NoPhoto;
        private string _thumbNoPhotoFileName = string.Empty;
        private string _thumbPrivateFileName = string.Empty;
        private string _width = "";
        private string _height = "";
        private string _linkCSSClass = "";
        private string _divCSSClas = "";

        //protected Matchnet.Web.Framework.Txt txtAskForPhoto;
        //protected System.Web.UI.HtmlControls.HtmlAnchor lnkNoPhoto;
        //protected System.Web.UI.HtmlControls.HtmlGenericControl tblNoPhoto;

        public bool EnableLink
        {
            get { return _enableLink; }
            set { _enableLink = value; }
        }


        public string NoPhotoFileName
        {
            get { return _thumbNoPhotoFileName; }
            set { _thumbNoPhotoFileName = value; }

        }

        public string PrivatePhotoFileName
        {
            get { return _thumbPrivateFileName; }
            set { _thumbPrivateFileName = value; }

        }

        public string Width
        {
            get { return _width; }
            set { _width = value; }
        }

        public string Height
        {
            get { return _height; }
            set { _height = value; }
        }

        public string Username
        {
            get { return literalUserName.Text; }
            set { literalUserName.Text = value; }
        }

        public string LinkCSSClass
        {
            get { return _linkCSSClass; }
            set { _linkCSSClass = value; }
        }

        public string DivCSSClass
        {
            set { this.tblNoPhoto.Attributes.Add("class", value); }
        }

        public string TextResourceConstant { get; set; }

        protected override void OnPreRender(EventArgs e)
        {
            if (this.Visible)
            {
                string style_format = "width: {0}; height: {1}; background-image: url({2})";
                string style = "";
                switch (_Mode)
                {
                    case PhotoMode.NoPhotoNoLink:
                        tblNoPhoto.Visible = false;
                        noPhotoNoLink.Visible = true;
                        if (!string.IsNullOrEmpty(TextResourceConstant))
                            txtNoPhotoNoLink.ResourceConstant = TextResourceConstant;

                        //tblNoPhoto.Attributes.Add("background", NoPhotoFileName);
                        style = String.Format(style_format, _width, _height, NoPhotoFileName);
                        noPhotoNoLink.Attributes.Add("style", style);
                        break;
                    case PhotoMode.UploadPhotoNow:
                        txtAskForPhoto.ResourceConstant = "TXT_UPLOAD_A_PHOTO_NOW";
                        //tblNoPhoto.Attributes.Add("background", NoPhotoFileName);
                        style = String.Format(style_format, _width, _height, NoPhotoFileName);
                        tblNoPhoto.Attributes.Add("style", style);
                        break;

                    case PhotoMode.UploadPhotoNowSmallText:
                        txtAskForPhoto.ResourceConstant = "TXT_UPLOAD_A_PHOTO_NOW_SMALL";
                        //tblNoPhoto.Attributes.Add("background", NoPhotoFileName);
                        style = String.Format(style_format, _width, _height, NoPhotoFileName);
                        tblNoPhoto.Attributes.Add("style", style);
                        break;

                    case PhotoMode.NoPhoto:
                        txtAskForPhoto.ResourceConstant = "TXT_ASK_ME_FOR_MY_PHOTO";
                        //tblNoPhoto.Attributes.Add("background", Matchnet.Web.Framework.Image.GetURLFromFilename(WebConstants.IMAGE_NOPHOTO_BACKGROUND_THUMB));
                        style = String.Format(style_format, _width, _height, NoPhotoFileName);
                        tblNoPhoto.Attributes.Add("style", style);
                        break;

                    case PhotoMode.NoPhotoSmallText:
                        txtAskForPhoto.ResourceConstant = "TXT_ASK_ME_FOR_MY_PHOTO_SMALL";
                        //tblNoPhoto.Attributes.Add("background", Matchnet.Web.Framework.Image.GetURLFromFilename(WebConstants.IMAGE_NOPHOTO_BACKGROUND_THUMB));
                        style = String.Format(style_format, _width, _height, NoPhotoFileName);
                        tblNoPhoto.Attributes.Add("style", style);
                        break;

                    case PhotoMode.NoPhotoNoText:
                        txtAskForPhoto.Visible = false;
                        //tblNoPhoto.Attributes.Add("background", NoPhotoFileName);
                        style = String.Format(style_format, _width, _height, NoPhotoFileName);
                        tblNoPhoto.Attributes.Add("style", style);
                        break;

                    case PhotoMode.NoPhotoNoTextNoURL:
                        txtAskForPhoto.Visible = false;
                        lnkNoPhoto.Visible = false;
                        noPhotoStatic.Visible = true;
                        //tblNoPhoto.Attributes.Add("background", NoPhotoFileName);
                        //style = String.Format(style_format, _width, _height, NoPhotoFileName);
                        //tblNoPhoto.Attributes.Add("style", style);
                        EnableLink = false;
                        break;

                    case PhotoMode.OnlyMembers:
                        txtAskForPhoto.ResourceConstant = "TXT_ONLY_MEMBERS_CAN_SEE_MY_PHOTOS";
                        //tblNoPhoto.Attributes.Add("background", NoPhotoFileName);
                        style = String.Format(style_format, _width, _height, NoPhotoFileName);
                        tblNoPhoto.Attributes.Add("style", style);
                        break;

                    case PhotoMode.NoPrivatePhoto:
                        txtAskForPhoto.ResourceConstant = "TXT_CUPID_PRIVATE_PHOTO";
                        //tblNoPhoto.Attributes.Add("background",  Matchnet.Web.Framework.Image.GetURLFromFilename(PrivatePhotoFileName));
                        style = String.Format(style_format, _width, _height, NoPhotoFileName);
                        tblNoPhoto.Attributes.Add("style", style);
                        break;

                    case PhotoMode.MiniSearch:
                        txtAskForPhoto.ResourceConstant = "TXT_ASK_ME_FOR_MY_PHOTO";
                        style = String.Format(style_format, _width, _height, NoPhotoFileName);
                        tblNoPhoto.Attributes.Add("style", style);
                        //this.lnkNoPhoto2.Attributes.Add("class", LinkCSSClass);
                        break;
                }
            }

            if (EnableLink == true)
            {
                if (!string.IsNullOrEmpty(DestURL) && !lnkNoPhoto.Disabled)
                {
                    lnkNoPhoto.HRef = DestURL;
                    if (!BreadCrumbHelper.IsJavascriptProfileLink(DestURL))
                        tblNoPhoto.Attributes.Add("onclick", "matchnetYNM4_checkWindowOpener('" + lnkNoPhoto.ClientID + "');");
                }
                else
                {
                    tblNoPhoto.Attributes.Add("style", "cursor: default;");
                }
            }

            if (noPhotoStatic.Visible)
            {

                if (noPhotoStatic.FileName.Contains(Matchnet.Web.Framework.Image.ImageUrlRoot))
                {

                    noPhotoStatic.FileName = noPhotoStatic.FileName.Substring(noPhotoStatic.FileName.IndexOf(Matchnet.Web.Framework.Image.ImageUrlRoot) + 5);
                }
            }

            base.OnPreRender(e);
        }

        public PhotoMode Mode
        {
            get { return _Mode; }
            set { _Mode = value; }
        }

        public int MemberID
        {
            get { return (_memberID); }
            set { _memberID = value; }
        }

        public IMemberDTO Member
        {
            get
            {
                if (_member == null)
                {
                    _member = MemberSA.Instance.GetMember(_memberID, MemberLoadFlags.None);
                }
                return (_member);
            }
            set { _member = value; }
        }

        public void AddAttribute(string Attribute, string AttributeValue)
        {
            lnkNoPhoto.Attributes.Add(Attribute, AttributeValue);
        }

        public string DestURL
        {
            get { return (_destUrl); }
            set
            {
                _destUrl = value;
                lnkNoPhoto.HRef = _destUrl;
                //if( value == String.Empty )
                //	lnkNoPhoto.Disabled = true;
            }
        }

        public void disableLink()
        {
            //TT 18639. Need to "disable" link for FireFox UI compatibility (so underline and outline don't appear onmouseover)
            // Must cause it not to render an underline at all.  Only way I found was to remove the hyperlink and add the table back.
            // HACK alert. This control is a mess. But what else is new.
            this.Controls.Remove(lnkNoPhoto);
            this.Controls.Add(tblNoPhoto);

            //The following Hacks did not produce desired results.
            //lnkNoPhoto.Style.Add("text-decoration","none");  //this made the underline go away, but not the table border
            //tblNoPhoto.Style.Add("border-width","0px"); //this plain just didn't work, cuz it got overridden by class perhaps?
            //lnkNoPhoto.Disabled = true; // this causes text to be grayed out in IE, so unless that's desired, don't use it.
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            //this.lnkNoPhoto.ServerClick += new EventHandler(lnkNoPhoto_ServerClick);
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            //MemberPhotoHelper photoHelper = new MemberPhotoHelper(RuntimeSettings.Instance);
            _thumbPrivateFileName = MemberPhotoDisplayManager.Instance.GetPrivatePhotoFile(PrivatePhotoImageType.BackgroundThumb, true);
            noPhotoStatic.FileName = MemberPhotoDisplayManager.Instance.GetNoPhotoFile(NoPhotoImageType.ThumbV2, true);

            if (_thumbNoPhotoFileName == string.Empty)
            {
                _thumbNoPhotoFileName = MemberPhotoDisplayManager.Instance.GetNoPhotoFile(NoPhotoImageType.BackgroundThumb, true);
            }
            else
            {
                noPhotoStatic.FileName = _thumbNoPhotoFileName;
            }
        }

        private string GetFemaleImages(string currentMaleImage)
        {
            //MemberPhotoHelper photoHelper = new MemberPhotoHelper(RuntimeSettings.Instance);
            string femaleImage = string.Empty;
            
            if (maleImageEqualsNoPhotoImage(currentMaleImage, NoPhotoImageType.BackgroundThumb))
            {
                femaleImage = MemberPhotoDisplayManager.Instance.GetNoPhotoFile(NoPhotoImageType.BackgroundThumb, false);
            }
            else if (maleImageEqualsNoPhotoImage(currentMaleImage, NoPhotoImageType.BackgroundTinyThumb))
            {
                femaleImage = MemberPhotoDisplayManager.Instance.GetNoPhotoFile(NoPhotoImageType.BackgroundTinyThumb, false);
            }
            else if (maleImageEqualsNoPhotoImage(currentMaleImage, NoPhotoImageType.Thumb))
            {
                femaleImage = MemberPhotoDisplayManager.Instance.GetNoPhotoFile(NoPhotoImageType.Thumb, false);
            }
            else if (maleImageEqualsNoPhotoImage(currentMaleImage, NoPhotoImageType.ThumbV2))
            {
                femaleImage = MemberPhotoDisplayManager.Instance.GetNoPhotoFile(NoPhotoImageType.ThumbV2, false);
            }
            else if (maleImageEqualsNoPhotoImage(currentMaleImage, NoPhotoImageType.TinyThumbV2))
            {
                femaleImage = MemberPhotoDisplayManager.Instance.GetNoPhotoFile(NoPhotoImageType.TinyThumbV2, false);
            }
            else if (maleImageEqualsPrivatePhotoImage(currentMaleImage, PrivatePhotoImageType.BackgroundThumb))
            {
                femaleImage = MemberPhotoDisplayManager.Instance.GetPrivatePhotoFile(PrivatePhotoImageType.BackgroundThumb, false);
            }
            else if (maleImageEqualsPrivatePhotoImage(currentMaleImage, PrivatePhotoImageType.Full))
            {
                femaleImage = MemberPhotoDisplayManager.Instance.GetPrivatePhotoFile(PrivatePhotoImageType.Full, false);
            }
            else if (maleImageEqualsPrivatePhotoImage(currentMaleImage, PrivatePhotoImageType.Thumb))
            {
                femaleImage = MemberPhotoDisplayManager.Instance.GetPrivatePhotoFile(PrivatePhotoImageType.Thumb, false);
            }
            else if (maleImageEqualsPrivatePhotoImage(currentMaleImage, PrivatePhotoImageType.TinyThumb))
            {
                femaleImage = MemberPhotoDisplayManager.Instance.GetPrivatePhotoFile(PrivatePhotoImageType.TinyThumb, false);
            }
            else
            {
                femaleImage = MemberPhotoDisplayManager.Instance.GetNoPhotoFile(NoPhotoImageType.ThumbV2, false);
            }

            return femaleImage;

        }

        private bool maleImageEqualsNoPhotoImage(string currentMaleImage, NoPhotoImageType noPhotoImageType)
        {
            //MemberPhotoHelper photoHelper = new MemberPhotoHelper(RuntimeSettings.Instance);
            return (currentMaleImage.ToLower() == MemberPhotoDisplayManager.Instance.GetNoPhotoFile(noPhotoImageType, true).ToLower());
        }

        private bool maleImageEqualsPrivatePhotoImage(string currentMaleImage, PrivatePhotoImageType privatePhotoImageType)
        {
            //MemberPhotoHelper photoHelper = new MemberPhotoHelper(RuntimeSettings.Instance);
            return (currentMaleImage.ToLower() == MemberPhotoDisplayManager.Instance.GetPrivatePhotoFile(privatePhotoImageType, true).ToLower());
        }

        /*private void lnkNoPhoto_ServerClick(object sender, EventArgs e)
		{
			switch (this.Mode)
			{
				case PhotoMode.OnlyMembers:
					g.LogonRedirect(this.DestURL);
					break;
				case PhotoMode.NoPhoto:
					g.Transfer(this.DestURL);
					break;
			}
		}*/
    }

}
