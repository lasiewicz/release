﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.UI;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Web.Framework;
using System.Web.UI.WebControls;

using Matchnet.Web.Framework;

namespace Matchnet.Web.Framework.Ui.PageElements
{
    public partial class ExternalFeedback : FrameworkControl
    {
        public enum OutputTagEnum
        {
            Head,
            Body
        }


        public OutputTagEnum OutputTag { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (ExternalFeedbackEnabled())
                {
                    string resourceString = string.Empty;

                    switch (OutputTag)
                    {
                        case OutputTagEnum.Head:
                            resourceString = "HEAD_CODE";
                            break;
                        case OutputTagEnum.Body:
                            resourceString = "BODY_CODE";
                            break;
                        default:
                            resourceString = string.Empty;
                            break;
                    }

                    if (g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.JDateCoIL)
                    {
                        if (g.AppPage.ID == (int)WebConstants.PageIDs.MembersOnline || g.AppPage.ID == (int)WebConstants.PageIDs.MailBox)
                        {
                            resourceString += "_" + g.AppPage.ControlName;
                        }
                    }
                    litOutput.Text = g.GetResource(resourceString, this);


                    if (OutputTag == OutputTagEnum.Body)
                    {

                        // Add member's data
                        StringBuilder sb = new StringBuilder();

                        sb.AppendLine(litOutput.Text);

                        sb.AppendLine("\n<!--Start Kampyle Custom Variables Code-->");
                        sb.AppendLine("<script type=\"text/javascript\">");
                        sb.AppendLine(string.Format("k_button.setCustomVariable(154,{0});", g.Member.MemberID.ToString()));
                        sb.AppendLine("</script>");
                        sb.AppendLine("<!--End Kampyle Custom Variables Code-->");
                        /*
                         * 
                    sb.AppendLine("\n<script type=\"text/javascript\">");
                    sb.AppendLine("k_button.extra_params = ");
                    sb.AppendLine("{");
                    sb.AppendLine(
                        string.Format("\"u_code\": '{0}',", GetKampyleFeedbackUcode(g.Member.MemberID.ToString())));
                    sb.AppendLine(string.Format("\"u_id\": \"{0}\",", g.Member.MemberID.ToString()));
                    sb.AppendLine(string.Format("\"u_disp\": \"{0}\",", g.Member.GetUserName(g.Brand)));
                    sb.AppendLine(string.Format("\"u_email\": \"{0}\",", g.Member.EmailAddress));
                    sb.AppendLine("\"u_notify_email\": 1 ");
                    sb.AppendLine("}");
                    sb.AppendLine("</script>");
                   */

                        litOutput.Text = sb.ToString();
                    }


                }
                else
                {
                    this.Visible = false;
                }

            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        private bool ExternalFeedbackEnabled()
        {
            bool result = true;
            if (FrameworkGlobals.IsSecureRequest())
            {
                result = false;
            }
            else
            {
                if (g.Member == null || RuntimeSettings.GetSetting("EXTERNAL_FEEDBACK_ENABLED", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID).ToLower() == "false")
                {
                    result = false;
                }
            }

            if (result)
            {
                if ((_g.AppPage.ControlName == "RegistrationStep1") || (g.AppPage.ControlName == "MembersOnline" && FrameworkGlobals.IsRegOverlay())) result = false;
            }

            return result;
        }

        private string MakeHashSHA256(string str)
        {
            byte[] data = Encoding.UTF8.GetBytes(str);
            byte[] result;
            string resultStr = string.Empty;
            HashAlgorithm sha256 = new SHA256Managed();
            result = sha256.ComputeHash(data);

            resultStr = BitConverter.ToString(result).Replace("-", "").ToLower();

            return resultStr;
        }

        private string GetKampyleFeedbackUcode(string str)
        {
            string key = RuntimeSettings.GetSetting("KAMPYLE_PRIVATE_KEY", g.Brand.Site.Community.CommunityID,
                                                    g.Brand.Site.SiteID);

            return MakeHashSHA256(key + str);
        }
    }
}