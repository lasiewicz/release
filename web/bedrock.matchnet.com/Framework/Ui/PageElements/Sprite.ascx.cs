﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Matchnet.Web.Framework
{
    public partial class Sprite : FrameworkControl
    { 
        FrameworkControl _resourceControl;

        public FrameworkControl ResourceControl
        { get{if(_resourceControl==null){ return this;}else{return _resourceControl;}}
          set{_resourceControl=value;} 
        }

        public string Title { get; set; }
        public string TitleResourceConstant { get; set; }
        public string Text { get; set; }
        public string TextResourceConstant { get; set; }
        public string SpriteClass { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public string GetTitle()
        {
            if (!String.IsNullOrEmpty(Title))
                return Title;
            else if (!String.IsNullOrEmpty(TitleResourceConstant))
                return g.GetResource(TitleResourceConstant, ResourceControl);
            else
                return "";
        }

        public string GetText()
        {
            if (!String.IsNullOrEmpty(Text))
                return Text;
            else if (!String.IsNullOrEmpty(TextResourceConstant))
                return g.GetResource(TextResourceConstant, ResourceControl);
            else
                return "";
        }
    }
}