namespace Matchnet.Web.Framework.Ui.PageElements
{
	using System;
	using System.Collections;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;

	using Matchnet.Web.Framework;

	/// <summary>
	///		Summary description for DynamicBreadCrumbTrail.
	/// </summary>
	public class DynamicBreadCrumbTrail : FrameworkControl
	{
		private const string NAV_ARROW = " > ";
		private const string LINK_CLASS = "stdSmall";
		private const string NON_LINK_CLASS = "stdBoldSmall";
		private Hashtable _navElements = new Hashtable();
		protected Panel DBreadCrumbTrailPanel;

		private void Page_Init(object sender, System.EventArgs e)
		{
		}

		/// <summary>
		/// Adds a nav link.  Provide text, a link, and what level of the nav to add.
		/// </summary>
		/// <param name="Text">The text for the navigation element.</param>
		/// <param name="Href">The link to go to.  If this is set to String.Empty, the text is bolded and unlinked.</param>
		public bool AddNavLink(string Text, string Href)
		{
			Hashtable navElement = new Hashtable();

			navElement["Text"] = Text;
			navElement["Href"] = Href;

			_navElements.Add(_navElements.Count + 1, navElement);
			
			return true;
		}

		public void BuildNavLinks()
		{
			Label lastNavElement = new Label();

			// Loop through all navElements up to the last one.  The last one will be handled differently.
			for (int i = 1; i <= _navElements.Count - 1; i++)
			{
				// Build arrow.
				Label navArrow = new Label();
				navArrow.CssClass = LINK_CLASS;
				navArrow.Text = NAV_ARROW;

				Hashtable navElement = (Hashtable)_navElements[i];

				HyperLink navLink = new HyperLink();

				navLink.Text = navElement["Text"].ToString();
				navLink.NavigateUrl = navElement["Href"].ToString();
				navLink.CssClass = LINK_CLASS;
				navArrow.Visible = true;

				// Add to Panel.
				DBreadCrumbTrailPanel.Controls.Add(navLink);
				DBreadCrumbTrailPanel.Controls.Add(navArrow);
			}

			// Last element.
			lastNavElement.CssClass = NON_LINK_CLASS;
			lastNavElement.Text = ((Hashtable)(_navElements[_navElements.Count]))["Text"].ToString();
			DBreadCrumbTrailPanel.Controls.Add(lastNavElement);
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Init += new System.EventHandler(this.Page_Init);

		}
		#endregion
	}
}
