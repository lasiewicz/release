﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NoPhoto20.ascx.cs" Inherits="Matchnet.Web.Framework.Ui.PageElements.NoPhoto20" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>

<div id="tblNoPhoto"  runat="server" class="no-photo-container">
    <a runat="server" id="lnkNoPhoto" class="no-photo">
        <asp:Literal ID="literalUserName" runat="server"></asp:Literal>
	    <mn:txt runat="server" id="txtAskForPhoto" ResourceConstant="TXT_ASK_ME_FOR_MY_PHOTO" />
    </a>
    <mn:image id="noPhotoStatic" runat="server" visible="false"/>
</div>

<div id="noPhotoNoLink" runat="server" class="no-photo-nolink" visible="false">
    <mn:txt runat="server" id="txtNoPhotoNoLink" ResourceConstant="TXT_ASK_ME_FOR_MY_PHOTO" />
</div>