﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.List.ValueObjects;
using Matchnet.Member.ValueObjects.Interfaces;

namespace Matchnet.Web.Framework.Ui.ProfileElements
{
    public partial class ContactHistory : FrameworkControl
    {
        private bool isControlLoaded = false;
        private bool isControlValid = false;

        public IMemberDTO Member { get; set; }
        public string LinkResourceConstant { get; set; }
        public string LinkTitleResourceConstant { get; set; }
        public string IconCSS { get; set; }
        public string IconNoHistoryCSS { get; set; }
        public bool HideLinkText { get; set; }
        public bool HidePopupHeaderText { get; set; }
        public bool HasContactHistory { get; private set; }

        //Set to true will load content via ajax (using ui templating) on demand
        public bool IsContentLazyLoaded { get; set; }

        public ContactHistory()
        {
            IsContentLazyLoaded = false;
        }

        #region Event Handlers
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Member != null && !isControlLoaded)
            {
                LoadContactHistory(Member);
            }
        }

        protected void repeaterContactHistoryDetailed_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
            {
                ContactHistoryItem chItem = e.Item.DataItem as ContactHistoryItem;
                if (chItem != null)
                {
                    Literal literalContactHistoryItemText = e.Item.FindControl("literalContactHistoryItemText") as Literal;
                    Literal literalContactHistoryItemTimestampText = e.Item.FindControl("literalContactHistoryItemTimestampText") as Literal;

                    literalContactHistoryItemText.Text = chItem.ItemText;
                    literalContactHistoryItemTimestampText.Text = " " + chItem.ItemTimeStampText;
                }
            }
        }

        protected void repeaterContactHistoryMatched_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
            {
                ContactHistoryItem chItem = e.Item.DataItem as ContactHistoryItem;
                if (chItem != null)
                {
                    Literal literalMatchedItemText = e.Item.FindControl("literalMatchedItemText") as Literal;
                    literalMatchedItemText.Text = chItem.ItemText;
                }
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            this.spanHeaderDesc.Visible = !HidePopupHeaderText;
            this.spanLinkDesc.Visible = !HideLinkText;
            this.Visible = isControlValid;

            base.OnPreRender(e);
        }

        #endregion

        public void LoadContactHistory(IMemberDTO member)
        {
            Member = member;
            isControlLoaded = true;

            if (Member != null && g.Member != null)
            {
                isControlValid = true;

                if (!String.IsNullOrEmpty(LinkResourceConstant))
                {
                    literalContactHistoryHeader.Text = g.GetResource(LinkResourceConstant, this);
                    literalContactHistoryLink.Text = g.GetResource(LinkResourceConstant, this);
                }
                else
                {
                    literalContactHistoryHeader.Text = g.GetResource("TXT_CONTACTHISTORY", this);
                    literalContactHistoryLink.Text = g.GetResource("TXT_CONTACTHISTORY", this);
                }

                if (!String.IsNullOrEmpty(LinkTitleResourceConstant))
                    lnkContactHistory.Title = g.GetResource(LinkTitleResourceConstant, this);
                else
                    lnkContactHistory.Title = g.GetResource("ALT_CONTACTHISTORY", this);

                //Determine if there are any contact history
                if (ProfileElementHelper.HasAnyContactHistory(g, Member.MemberID))
                {
                    if (String.IsNullOrEmpty(IconCSS))
                        spanIcon.Attributes.Add("class", "spr s-icon-A-hover-contact");
                    else
                        spanIcon.Attributes.Add("class", IconCSS);

                    //Get contact history detail
                    if (IsContentLazyLoaded)
                    {
                        phContactHistoryUITemplateContent.Visible = true;
                        //TODO: Lazy load not completed
                        lnkContactHistory.Attributes.Add("onclick", "LoadContactHistory('" + divContactHistory.ClientID + "','" + divContactHistory.ClientID + "'," + member.MemberID.ToString() + ",true,false);return false;");
                        //Client will call JSON API
                    }
                    else
                    {
                        //lnkContactHistory.Attributes.Add("onclick", "LoadContactHistory('" + divContactHistory.ClientID + "','" + divContactHistory.ClientID + "'," + member.MemberID.ToString() + ",false,true);return false;");
                        phContactHistoryContent.Visible = true;

                        Control resourceControl = Page.LoadControl("/Framework/Ui/ProfileElements/ProfileElementsAPIResource.ascx");
                        List<ContactHistoryItem> contactHistoryItemList = ProfileElementHelper.GetContactHistoryItemList(g, Member.MemberID, resourceControl);
                        List<ContactHistoryItem> contactHistoryItemMatchedOnly = new List<ContactHistoryItem>();
                        List<ContactHistoryItem> contactHistoryItemDetailedOnly = new List<ContactHistoryItem>();

                        foreach (ContactHistoryItem chi in contactHistoryItemList)
                        {
                            if (chi.IsMatched)
                                contactHistoryItemMatchedOnly.Add(chi);
                            else
                                contactHistoryItemDetailedOnly.Add(chi);
                        }

                        if (contactHistoryItemMatchedOnly.Count > 0)
                        {
                            repeaterContactHistoryMatched.DataSource = contactHistoryItemMatchedOnly;
                            repeaterContactHistoryMatched.DataBind();
                            HasContactHistory = true;
                        }
                        
                        if (contactHistoryItemDetailedOnly.Count > 0)
                        {
                            repeaterContactHistoryDetailed.DataSource = contactHistoryItemDetailedOnly;
                            repeaterContactHistoryDetailed.DataBind();
                            HasContactHistory = true;
                        }

                    }

                }
                else
                {
                    if (String.IsNullOrEmpty(IconCSS))
                        spanIcon.Attributes.Add("class", "spr s-icon-A-hover-contact");
                    else
                        spanIcon.Attributes.Add("class", IconNoHistoryCSS);

                    phContactHistoryEmptyContent.Visible = true;
                    //lnkContactHistory.Attributes.Add("onclick", "LoadContactHistory('" + divContactHistory.ClientID + "','" + divContactHistory.ClientID + "'," + member.MemberID.ToString() + ",false, false);return false;");
                }


            }
        }


    }
}