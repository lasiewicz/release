﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HighlightInfo.ascx.cs" Inherits="Matchnet.Web.Framework.Ui.ProfileElements.HighlightInfo" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="uc2" TagName="Sprite" Src="../PageElements/Sprite.ascx" %>
<asp:Placeholder ID="phHighlightInfo" Runat="server" >
    <div runat="server" id="divLink"  class="cluetip click highlight-info">
	    <a href="<%= InfoClientID %>" rel=".cluetip-body" >
		    <mn:Txt id="txtInfoLink" runat="server"  expandImageTokens="True" />
	    </a>	
    </div>

    <asp:Placeholder ID="phInfo" Runat="server">
        <div runat="server" id="divInfo"  class="cluetip-body highlight-info-body" style="display:none" >
            <div id="Div2" class="cluetip-close"  runat="server">
		        <a href="#"><uc2:Sprite TitleResourceConstant="TXT_CLOSE"  TextResourceConstant="TXT_CLOSE"  SpriteClass="spr s-icon-closethick" runat="server"/></a>							
	        </div>
	        <mn:Txt id="txtNonSubscribersInfo" runat="server" ResourceConstant="TXT_HIGHLIGHTPROFILEINFO" expandImageTokens="True" />
	        <asp:HyperLink ID="lnkSubscribe" Runat="server" CssClass="cta">
	            <mn:Txt id="imgSubscribe" runat="server" ResourceConstant="HIGHLIGHTPROFILEINFO_TO_SUBSCRIPTION" expandImageTokens="True" />
	        </asp:HyperLink>		
        </div>
    </asp:Placeholder>
</asp:Placeholder>