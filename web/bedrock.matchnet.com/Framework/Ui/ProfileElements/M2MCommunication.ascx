﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="M2MCommunication.ascx.cs" Inherits="Matchnet.Web.Framework.Ui.ProfileElements.M2MCommunication" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mnbe" Namespace="Matchnet.Web.Framework.Ui.BasicElements" Assembly="Matchnet.Web" %>

<div id="profile30Comm" class="profile30-comm">
		<h2><asp:Literal ID="literalM2MTitle" runat="server"></asp:Literal></h2>

		<ul>
		    <li>
		        <asp:HyperLink ID="lnkEmail" runat="server" CssClass="btn link-primary large">
		            <mn2:FrameworkLiteral ID="literalEmail" ResourceConstant="PRO_EMAIL" runat="server"></mn2:FrameworkLiteral>
		        </asp:HyperLink>
		    </li>
		    <li>
		        <asp:HyperLink ID="lnkFlirt" runat="server">
		            <mn2:FrameworkLiteral ID="literalFlirt" ResourceConstant="PRO_TEASE" runat="server"></mn2:FrameworkLiteral>
		        </asp:HyperLink>
		    </li>
		    <asp:PlaceHolder ID="phEcardLink" runat="server">
		    <li>
		        <asp:HyperLink ID="lnkEcard" runat="server">
		            <mn2:FrameworkLiteral ID="literalEcard" ResourceConstant="PRO_ECARDS" runat="server"></mn2:FrameworkLiteral>
		        </asp:HyperLink>
		    </li>
		    </asp:PlaceHolder>
		    <li>
		        <asp:HyperLink ID="lnkIM" runat="server">
		            <mn2:FrameworkLiteral ID="literalIM" ResourceConstant="PRO_IM_ONLINE" runat="server"></mn2:FrameworkLiteral>
		        </asp:HyperLink>
		    </li>
		    
		</ul>
		
		<asp:PlaceHolder ID="phYNM" runat="server">
		<div class="click">
            <span class="do-we-click">
                <em><mn2:FrameworkLiteral id="literalClick" runat="server" resourceconstant="CLICK"></mn2:FrameworkLiteral></em>
            </span>
            
            <span class="no-wrap-class"> 
                <div id="divMutualYes" runat="server">
				    <mn:Image ID="imgBothSaidYes" runat="server" FileName="icon-click-yy.gif" TitleResourceConstant="TTL_CLICK_YOU_BOTH_SAID_YES_EXCLAMATION" />
			    </div>
				
			    <div style="display: inline;" id="divYNMButtons">
                    <!--yes button-->
                    <mnbe:YNMVoteButton id="ImageYes" runat="server" resourceconstant="YNM_Y_IMG_ALT_TEXT" type="Yes" selectedFilename="icon-click-y-on.gif"
                    filename="icon-click-y-off.gif" source="fullprofile"></mnbe:YNMVoteButton>
                    <!--no button-->
                    <mnbe:YNMVoteButton id="ImageNo" runat="server" resourceconstant="YNM_N_IMG_ALT_TEXT" type="No" selectedFilename="icon-click-n-on.gif"
                    filename="icon-click-n-off.gif" source="fullprofile"></mnbe:YNMVoteButton>
                    <!--maybe button-->
                    <mnbe:YNMVoteButton id="ImageMaybe" runat="server" resourceconstant="YNM_M_IMG_ALT_TEXT" type="Maybe" selectedFilename="icon-click-m-on.gif"
                    filename="icon-click-m-off.gif" source="fullprofile"></mnbe:YNMVoteButton>
	                
                    <%--input to hold current vote status, used by js--%>
                    <input id="YNMVoteStatus" type="hidden" name="YNMVoteStatus" runat="server"/>
                </div>
	        </span>
        </div>
        </asp:PlaceHolder>
</div>
