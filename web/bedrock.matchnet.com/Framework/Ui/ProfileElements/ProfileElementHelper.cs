﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.List.ValueObjects;
using Matchnet.Lib;
using Matchnet.List.ServiceAdapters;

namespace Matchnet.Web.Framework.Ui.ProfileElements
{
    public class ProfileElementHelper
    {
        #region Contact History Related
        public static List<HotListType> GetContactHistoryHotListTypes()
        {
            List<HotListType> hotListTypeList = new List<HotListType>();
            hotListTypeList.Add(HotListType.Friend);
            hotListTypeList.Add(HotListType.ViewProfile);
            hotListTypeList.Add(HotListType.Email);
            hotListTypeList.Add(HotListType.ECard);
            hotListTypeList.Add(HotListType.Tease);
            hotListTypeList.Add(HotListType.IM);
            hotListTypeList.Add(HotListType.Ignore);
            hotListTypeList.Add(HotListType.ExcludeList);
            hotListTypeList.Add(HotListType.ExcludeFromList);
            hotListTypeList.Add(HotListType.YNM);

            return hotListTypeList;
        }

        public static List<ContactHistoryItem> GetContactHistoryItemList(ContextGlobal g, int targetMemberID, System.Web.UI.Control ResourceControl)
        {
            List<ContactHistoryItem> listItems = new List<ContactHistoryItem>();
            List<HotListType> hotListTypeList = GetContactHistoryHotListTypes();

            foreach (HotListType hlType in hotListTypeList)
            {
                //you're checking out
                ContactHistoryItem sourceHistoryItem = GetContactHistoryItem(g, targetMemberID, hlType, HotListDirection.OnYourList, ResourceControl);
                if (sourceHistoryItem != null)
                    listItems.Add(sourceHistoryItem);

                //checking you out
                ContactHistoryItem targetHistoryItem = GetContactHistoryItem(g, targetMemberID, hlType, HotListDirection.OnTheirList, ResourceControl);
                if (targetHistoryItem != null)
                    listItems.Add(targetHistoryItem);

                //check for "mutual"
                if (sourceHistoryItem != null && hlType == HotListType.YNM)
                {
                    if (sourceHistoryItem.MutualYNM)
                    {
                        ContactHistoryItem historyItem = new ContactHistoryItem();
                        historyItem.IsMatched = true;
                        historyItem.ActionDate = DateTime.MaxValue;
                        historyItem.HotlistCategory = HotListCategory.MutualYes;
                        historyItem.HotlistType = hlType;
                        historyItem.ItemText = GetContactHistoryItemText(g, ResourceControl, hlType, HotListDirection.None, true, targetMemberID, "");
                        listItems.Add(historyItem);
                    }
                }
                else if (sourceHistoryItem != null && targetHistoryItem != null)
                {
                    ContactHistoryItem historyItem = new ContactHistoryItem();
                    historyItem.IsMatched = true;
                    historyItem.ActionDate = DateTime.MaxValue;
                    historyItem.HotlistType = hlType;
                    historyItem.ItemText = GetContactHistoryItemText(g, ResourceControl, hlType, HotListDirection.None, true, targetMemberID, "");
                    listItems.Add(historyItem);
                }
            }

            listItems.Sort();
            return listItems;
        }

        public static ContactHistoryItem GetContactHistoryItem(ContextGlobal g, int targetMemberID, HotListType hotListType, HotListDirection hotlistDirection, System.Web.UI.Control ResourceControl)
        {
            ContactHistoryItem historyItem = null;

            if (hotListType == HotListType.YNM)
            {
                if (hotlistDirection == HotListDirection.OnYourList)
                {
                    historyItem = new ContactHistoryItem();
                    
                    ClickMask clickMask = g.List.GetClickMask(g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, targetMemberID);

                    if ((((clickMask & ClickMask.TargetMemberYes) == ClickMask.TargetMemberYes) && ((clickMask & ClickMask.MemberYes) == ClickMask.MemberYes)))
                    {
                        historyItem.MutualYNM = true;
                        historyItem.YNM = "Y";
                        historyItem.HotlistCategory = HotListCategory.MembersClickedYes;
                    }
                    else if ((clickMask & ClickMask.MemberYes) == ClickMask.MemberYes)
                    {
                        historyItem.YNM = "Y";
                        historyItem.HotlistCategory = HotListCategory.MembersClickedYes;
                    }
                    else if ((clickMask & ClickMask.MemberNo) == ClickMask.MemberNo)
                    {
                        historyItem.YNM = "N";
                        historyItem.HotlistCategory = HotListCategory.MembersClickedNo;
                    }
                    else if ((clickMask & ClickMask.MemberMaybe) == ClickMask.MemberMaybe)
                    {
                        historyItem.YNM = "M";
                        historyItem.HotlistCategory = HotListCategory.MembersClickedMaybe;
                    }
                    else
                    {
                        historyItem = null;
                    }

                    if (historyItem != null)
                    {
                        historyItem.HotlistType = hotListType;
                        historyItem.ItemText = GetContactHistoryItemText(g, ResourceControl, hotListType, hotlistDirection, false, targetMemberID, historyItem.YNM);

                        ListItemDetail listDetail = g.List.GetListItemDetail(historyItem.HotlistCategory,
                                        g.Brand.Site.Community.CommunityID,
                                        g.Brand.Site.SiteID,
                                        targetMemberID);

                        if (listDetail != null)
                        {
                            historyItem.ActionDate = listDetail.ActionDate;
                            historyItem.ItemTimeStampText = GetContactHistoryItemTimestampText(g, historyItem.ActionDate);
                        }
                    }
                }
                else
                {
                    //we don't show YNM from others' list
                }
            }
            else if ((hotListType == HotListType.Ignore || hotListType == HotListType.ExcludeFromList || hotListType == HotListType.ExcludeList) && hotlistDirection == HotListDirection.OnTheirList)
            {
                //we won't allow contact history for blocks on other people's list
                historyItem = null;
            }
            else
            {
                HotListCategory listCategory = ListInternal.MapListCategory(hotListType, hotlistDirection);

                if (hotListType != HotListType.Friend && listCategory == HotListCategory.Default)
                {
                    //this indicates the hotListType does not have any mapping
                    historyItem = null;
                }
                else
                {
                    ListItemDetail listDetail = g.List.GetListItemDetail(listCategory,
                                        g.Brand.Site.Community.CommunityID,
                                        g.Brand.Site.SiteID,
                                        targetMemberID);

                    if (listDetail != null)
                    {
                        bool isValid = true;
                        if (hotListType == HotListType.ViewProfile && hotlistDirection == HotListDirection.OnTheirList)
                        {
                            //check if target member has blocked you, don't show that they "viewed your profile"
                            if (HasTargetBlockedYou(g, targetMemberID))
                                isValid = false;
                        }

                        if (isValid)
                        {
                            historyItem = new ContactHistoryItem();
                            historyItem.ActionDate = listDetail.ActionDate;
                            historyItem.HotlistCategory = listCategory;
                            historyItem.HotlistType = hotListType;
                            historyItem.ItemText = GetContactHistoryItemText(g, ResourceControl, hotListType, hotlistDirection, false, targetMemberID, "");
                            historyItem.ItemTimeStampText = GetContactHistoryItemTimestampText(g, historyItem.ActionDate);
                        }
                        else
                        {
                            historyItem = null;
                        }
                    }
                }
            }

            return historyItem;

        }

        public static bool HasAnyContactHistory(ContextGlobal g, int targetMemberID)
        {
            bool sourceList = false;
            bool targetList = false;

            List<HotListType> hotListTypeList = GetContactHistoryHotListTypes();
            foreach (HotListType hlType in hotListTypeList)
            {
                HasContactHistory(g, targetMemberID, hlType, out sourceList, out targetList);
                if (sourceList || targetList)
                    return true;
            }

            return false;
        }

        public static void HasContactHistory(ContextGlobal _g, int targetMemberID, HotListType hotListType, out bool sourceList, out bool targetList)
        {
            HotListCategory listCategorySource = ListInternal.MapListCategory(hotListType, HotListDirection.OnYourList);
            HotListCategory listCategoryTarget = ListInternal.MapListCategory(hotListType, HotListDirection.OnTheirList);

            if (hotListType != HotListType.Friend && listCategorySource == HotListCategory.Default && listCategoryTarget == HotListCategory.Default)
            {
                //this indicates the hotListType does not have any mapping
                sourceList = false;
                targetList = false;
            }
            else
            {
                //you're checking out
                sourceList = _g.List.IsHotListed(listCategorySource, _g.Brand.Site.Community.CommunityID, targetMemberID);
                //checking you out
                targetList = _g.List.IsHotListed(listCategoryTarget, _g.Brand.Site.Community.CommunityID, targetMemberID);
            }
        }

        public static bool HasTargetBlockedYou(ContextGlobal _g, int targetMemberID)
        {
            Matchnet.List.ServiceAdapters.List targetMemberList = ListSA.Instance.GetList(targetMemberID, ListLoadFlags.None);
            bool isblocked = targetMemberList.IsHotListed(HotListCategory.ExcludeFromList, _g.Brand.Site.Community.CommunityID, _g.Member.MemberID);
            if (!isblocked)
            {
                isblocked = targetMemberList.IsHotListed(HotListCategory.IgnoreList, _g.Brand.Site.Community.CommunityID, _g.Member.MemberID);
            }
            if (!isblocked)
            {
                isblocked = targetMemberList.IsHotListed(HotListCategory.ExcludeList, _g.Brand.Site.Community.CommunityID, _g.Member.MemberID);
            }

            return isblocked;

        }

        public static string GetContactHistoryItemText(ContextGlobal _g, System.Web.UI.Control resourceControl, HotListType listType, HotListDirection listDirection, bool isMutual, int targetMemberID, string ynm)
        {
            string s = "";

            if (isMutual)
            {
                s = _g.GetResource("HISTORY_MUTUAL_" + listType.ToString(), resourceControl);
            }
            else if (listType == HotListType.YNM)
            {
                s = _g.GetResource("HISTORY_" + listType.ToString() + "_" + listDirection.ToString() + "_" + ynm, resourceControl);
            }
            else if (listType == HotListType.Ignore || listType == HotListType.ExcludeFromList || listType == HotListType.ExcludeList)
            {
                s = _g.GetResource("HISTORY_" + listType.ToString() + "_" + listDirection.ToString(), resourceControl);
            }
            else
            {
                Matchnet.Member.ServiceAdapters.Member targetMember = Matchnet.Member.ServiceAdapters.MemberSA.Instance.GetMember(targetMemberID, Matchnet.Member.ServiceAdapters.MemberLoadFlags.None);
                int genderMask = targetMember.GetAttributeInt(_g.Brand, "gendermask");
                if ((genderMask & ConstantsTemp.GENDERID_MALE) == ConstantsTemp.GENDERID_MALE)
                    s = _g.GetResource("HISTORY_M_" + listType.ToString() + "_" + listDirection.ToString(), resourceControl);
                else
                    s = _g.GetResource("HISTORY_F_" + listType.ToString() + "_" + listDirection.ToString(), resourceControl);

            }

            return s;
        }

        public static string GetContactHistoryItemTimestampText(ContextGlobal _g, DateTime actionDate)
        {
            System.Web.UI.Page page = new System.Web.UI.Page();
            Matchnet.Web.Framework.Ui.BasicElements.LastTime lastTimeControl = page.LoadControl("/Framework/Ui/BasicElements/LastTime.ascx") as Matchnet.Web.Framework.Ui.BasicElements.LastTime;
            lastTimeControl.LoadLastTime(actionDate);

            return lastTimeControl.LiteralTimeControl.Text;
        }

        #endregion
    }
}
