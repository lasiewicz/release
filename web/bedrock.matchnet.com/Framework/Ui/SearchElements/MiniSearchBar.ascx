﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MiniSearchBar.ascx.cs" Inherits="Matchnet.Web.Framework.Ui.SearchElements.MiniSearchBar" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mnl" Namespace="Matchnet.Web.Framework.Ui.BasicElements.Links" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mnbe" Namespace="Matchnet.Web.Framework.Ui.BasicElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="uc1" TagName="NoPhoto" Src="../PageElements/NoPhoto20.ascx" %>

<asp:PlaceHolder ID="phDivMiniSearchOpen" runat="server">
<div id="divMiniSearchBar" class="mini-search clearfix <%=GetFishEyeCSS() %> <%=GetArrowsNavCSSClass() %>">  
</asp:PlaceHolder>

    <div class="nav-back">
        <span class="desc"><mn2:FrameworkLiteral ID="literalBack" runat="server" ResourceConstant="TXT_CURRENTLY_VIEWING"></mn2:FrameworkLiteral></span>
        <asp:HyperLink ID="lnkBack" runat="server" CssClass="back">
            <mn2:FrameworkLiteral ID="literalTitle" runat="server" ResourceConstant="TXT_TITLE_DEFAULT"></mn2:FrameworkLiteral>
            <span class="default-copy"><mn2:FrameworkLiteral ID="literalBackToResults" runat="server" ResourceConstant="TXT_BACK_TO_RESULTS"></mn2:FrameworkLiteral></span>
        </asp:HyperLink>
    </div>
    <div class="nav-profile">
        <%--Previous and Next Profile links --%>
        <asp:PlaceHolder ID="phPreviousProfile" runat="server" Visible="false">
            <asp:HyperLink ID="lnkPreviousProfile" runat="server" CssClass="prev-profile">
                <mn2:FrameworkLiteral ID="literalPreviousProfile" runat="server" ResourceConstant="TXT_PREVIOUS_PROFILE"></mn2:FrameworkLiteral>
            </asp:HyperLink>
        </asp:PlaceHolder>
        
        <asp:PlaceHolder ID="phNextProfile" runat="server" Visible="false">
            <asp:HyperLink ID="lnkNextProfile" runat="server" CssClass="next-profile">
                <mn2:FrameworkLiteral ID="literalNextProfile" runat="server" ResourceConstant="TXT_NEXT_PROFILE"></mn2:FrameworkLiteral>
            </asp:HyperLink>
        </asp:PlaceHolder>
    </div>
     
    <%--Profile30 Beta Toggler --%>
    <asp:PlaceHolder ID="phProfile30BetaToggler" runat="server" Visible="false">
        <div id="beta-toggler" class="beta-toggler float-inside clear-both">
             <asp:HyperLink CssClass="beta-toggler30 spr-parent" ID="lnkProfile30BetaToggler" runat="server"></asp:HyperLink>
             <mn2:FrameworkLiteral ID="litTourLink" runat="server" ResourceConstant="LNK_WHATS_THIS"></mn2:FrameworkLiteral>
             <%--<div id="betalayer" class="hide editorial">
                <mn:Txt ID="htmlBetaLayerContent" runat="server" ResourceConstant="HTML_BETA_LAYER_CONTENT" ExpandImageTokens="true" />
                <mn:Txt ID="htmlBetaLayerLegal" runat="server" ResourceConstant="HTML_BETA_LAYER_LEGAL" ExpandImageTokens="true" /> 
            </div>--%>
        </div>
    </asp:PlaceHolder>
    <div id="divMiniSearchBarCont">
    <asp:PlaceHolder ID="phPreviousPage" runat="server" Visible="false">
        <div id="tdPrevious" runat="server" class="prev" >
            <button class="spr-btn btn-arrow-lg-prev"><span>&#9668;</span></button>
        </div>
    </asp:PlaceHolder>
    
    <asp:PlaceHolder ID="phDockDivOpen" runat="server">
    <div id="dock">
    </asp:PlaceHolder>
        <ul id="photos-min" class="photo-list clearfix">
            <asp:Repeater ID="repeaterProfiles" runat="server" OnItemDataBound="repeaterProfiles_ItemDataBound">
                <ItemTemplate>
                    <li>
                        <asp:Panel ID="panelImgThumb" runat="server"><mn:Image runat="server" alt="" id="imgThumb" /></asp:Panel>
                        <uc1:NoPhoto runat="server" id="noPhoto" class="no-photo" Width="" Height="" visible="False" />
                        
                        <%--YNM Indicator--%>
                        <asp:PlaceHolder ID="phY" runat="server" Visible="false"><span id="spanSecretAdmirerY" runat="server" class="spr s-icon-click-y-on"><span><mn:Txt ID="txtY" runat="server" ResourceConstant="YNM_Y" /></span></span></asp:PlaceHolder>
                        <asp:PlaceHolder ID="phN" runat="server" Visible="false"><span id="spanSecretAdmirerN" runat="server" class="spr s-icon-click-n-on"><span><mn:Txt ID="txtN" runat="server" ResourceConstant="YNM_N" /></span></span></asp:PlaceHolder>
                        <asp:PlaceHolder ID="phM" runat="server" Visible="false"><span id="spanSecretAdmirerM" runat="server" class="spr s-icon-click-m-on"><span><mn:Txt ID="txtM" runat="server" ResourceConstant="YNM_M" /></span></span></asp:PlaceHolder>
                        <asp:PlaceHolder ID="phYY" runat="server" Visible="false"><span id="spanSecretAdmirerYY" runat="server" class="spr s-icon-click-yy"><span><mn:Txt ID="txtYY" runat="server" ResourceConstant="YNM_YY" /></span></span></asp:PlaceHolder>
                    </li>
                </ItemTemplate>
            </asp:Repeater>
        </ul>
    <asp:PlaceHolder ID="phDockDivClose" runat="server">
    </div>
    </asp:PlaceHolder>
    
    <asp:PlaceHolder ID="phFisheyeJS" runat="server">
    <script type="text/javascript">
        MiniSearchFisheye(<%= g.Brand.Site.Direction == Matchnet.Content.ValueObjects.BrandConfig.DirectionType.rtl ? "true" : string.Empty %>);
    </script>
    </asp:PlaceHolder>
    
    <asp:PlaceHolder ID="phLowMessaging" runat="server" Visible="false">
        <div class="low-messaging">
            <mn2:FrameworkLiteral ID="literalLowMessaging" runat="server" ResourceConstant="TXT_LOW_MESSAGING"></mn2:FrameworkLiteral>
        </div>
    </asp:PlaceHolder>
    
    <asp:PlaceHolder ID="phAjaxLoading" runat="server">
        <!--ajax loading-->
        <div id="divMiniSearchAjaxLoading" style="display:none;" class="loading">
            <p>
                <mn2:FrameworkLiteral ID="literalLoading" runat="server" ResourceConstant="TXT_LOADING"></mn2:FrameworkLiteral>
                <mn2:FrameworkLiteral ID="literalLoadingTitle" runat="server" ResourceConstant="TXT_TITLE_DEFAULT"></mn2:FrameworkLiteral>
                &hellip;
            </p>
            <mn:Image ID="Image1" runat="server" FileName="ajax-loader.gif" alt="" />
            <div class="visual-blocker"></div>
        </div>
    </asp:PlaceHolder> 	                   

    <asp:PlaceHolder ID="phNextPage" runat="server" Visible="false">
        <div id="tdNext" runat="server" class="next">
            <button class="spr-btn btn-arrow-lg-next"><span>&#9658;</span></button>
        </div>
    </asp:PlaceHolder>
    </div>
<asp:PlaceHolder ID="phDivMiniSearchClose" runat="server">
</div>
</asp:PlaceHolder>
