﻿using System;
using System.Collections;
using System.Data;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using Matchnet.Lib;
using Matchnet.List.ValueObjects;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Member.ValueObjects.Interfaces;
using Matchnet.Web.Framework.Ui.BasicElements;
using Matchnet.Web.Framework.Managers;
namespace Matchnet.Web.Framework.Ui.SearchElements
{
    partial class ResultList : FrameworkControl, IResultList
    {
        private string galleryView = String.Empty;

        // MatchMeter
        private bool isMatchMeterEnabled;
        private bool _isMatchMeterInfoDisplay;
        private MatchesCollection _myMatchesCollection; // For MatchMeter app (Jmeter)
        private Dictionary<IMemberDTO, decimal> MatchResultsMemberScoreDictionary;

        private const int NumberOfTrackedPages = 6;	// On SAGE we have channels up to "Results_Page6plus".
        private bool hasSpotlightProfile;

        private int count;
        private int highlightedProfileCount;
        private int pageSize = 12;
        private int chapterSize = 3;
        private int startRow;
        private Int32 _TotalRows;
        private ResultListHandler resultListHandler;
        private bool _resultsLoaded = false;

        //Result loaded event
        public delegate void ResultsLoadedEventHandler(List<int> SearchIDs);
        public event ResultsLoadedEventHandler ResultsLoaded;

        public Repeater JMeterRepeater { get { return new Repeater(); } }

        public ResultList()
        {
            TargetMemberIDs = new ArrayList();
            HotListCategory = HotListCategory.Default;
            LoadControlAutomatically = true;
        }

        /// <summary>
        /// Gets or sets the matches collection
        /// For matchmeter app (Jmeter).
        /// </summary>
        /// <value>My matches collection.</value>
        public MatchesCollection MyMatchesCollection
        {
            get { return _myMatchesCollection; }
            set { _myMatchesCollection = value; }
        }

        /// <summary>
        /// This indicates whether control will load automatically during page load,
        /// set this to false if you need to orchestrate when control loads
        /// </summary>
        public bool LoadControlAutomatically { get; set; }
        public bool IgnoreSAFilteredSearchResultsCache { get; set; }
        public bool IgnoreSASearchResultsCache { get; set; }
        public bool IgnoreAllSearchResultsCache { get; set; }

        /// <summary>
        /// Specifies whether it is a partial page render
        /// </summary>
        public bool IsPartialRender { get; set; }

        public bool PhotoOnly { get; set; }
        public bool DisplayMatchRating { get; set; }

        #region IResultList implementation

        public string StartRowClientID
        {
            get
            {
                return hidStartRow.ClientID;
            }
        }

        public BreadCrumbHelper.EntryPoint MyEntryPoint { get; set; }
        public ArrayList TargetMemberIDs { get; set; }
        public List<int> MemberIDList { get; set; }
        public string GalleryView
        {
            get { return galleryView; }
            set { galleryView = value; }
        }

        public FrameworkControl ThisControl
        {
            get { return this; }

        }

        public DataTable Dt { get; set; }
        public Int32 MemberNum { get; set; }
        public List<int> SearchIDs { get; set; }

        #region controls

        public Repeater MemberRepeater { get { return memberListRepeater; } }
        public NoResults NoSearchResults { get { return noSearchResults; } }
        public PlaceHolder PLCPromotionalProfile { get { return null; } }
        public Txt NoUsersText { get { return null; } }
        public Repeater GalleryMemberRepeater { get { return memberGalleryRepeater; } }
        public Repeater PhotoGalleryRepeater { get { return null; } }
        public PlaceHolder PLCMembersOnlineNoResults { get { return null; } }
        public bool EnableMultipleSearchViews { get; set; }

        #endregion

        # region context type for result list

        public ResultContextType ResultListContextType { get; set; }

        public bool EnableSingleSelect { get; set; }

        #endregion context type for result list

        #region properties

        public Int32 PageSize
        {
            get { return pageSize; }
            set { pageSize = value; }
        }

        public Int32 ChapterSize
        {
            get { return chapterSize; }
            set { chapterSize = value; }
        }

        public Int32 TotalRows
        {
            get { return _TotalRows; }
            set { _TotalRows = value; }
        }

        public Int32 StartRow
        {
            get
            {
                return Conversion.CInt(hidStartRow.Value);
            }
            set
            {
                hidStartRow.Value = value.ToString();
            }
        }

        public int EndRow { get; set; }
        public MOCollection MyMOCollection { get; set; }
        public HotListCategory HotListCategory { get; set; }
        public HotListType HotListType { get; set; }
        public HotListDirection HotListDirection { get; set; }

        #endregion


        #endregion

        #region Event Handlers
        /// <summary>
        /// This init method fires off three other init methods.  They are used to prepare the page with anything needed.
        /// The init method called is dependant upon the result list context type.
        /// </summary>
        private void Page_Init(object sender, EventArgs e)
        {
            try
            {
                switch (ResultListContextType)
                {
                    case ResultContextType.HotList:
                        DoHotListInit();
                        break;
                    case ResultContextType.SearchResult:
                        DoSearchResultInit();
                        break;
                    case ResultContextType.QuickSearchResult:
                        DoQuickSearchResultInit();
                        break;
                    case ResultContextType.KeywordSearchResult:
                        DoQuickSearchResultInit();
                        break;
                    case ResultContextType.ReverseSearchResult:
                        DoQuickSearchResultInit();
                        break;
                    case ResultContextType.MembersOnline:
                        DoMembersOnlineInit();
                        break;
                    case ResultContextType.AdvancedSearchResult:
                        DoAdvancedSearchResultInit();
                        break;
                }
            }
            catch (Exception ex)
            {
                _g.ProcessException(ex);
            }
        }

        private void DoAcademicVisitorSearchResultInit()
        {

        }

        /// <summary>
        /// The page load method initializes the start row and the end row for the results.
        /// Then, dependant upon resutl list context type, a page specific load is fired.
        /// </summary>
        private void Page_Load(object sender, EventArgs e)
        {
            if (LoadControlAutomatically)
            {
                LoadResultList();
            }
        }

        /// <summary>
        /// For Analytics
        /// </summary>
        /// <param name="e"></param>
        protected override void OnPreRender(EventArgs e)
        {
            try
            {
                if (!IsPartialRender)
                {
                    // Omniture Analytics - Event and property variable tracking
                    if (Convert.ToBoolean(RuntimeSettings.GetSetting("ANALYTICS_OMNITURE", _g.Brand.Site.Community.CommunityID, _g.Brand.Site.SiteID)))
                    {
                        // Search Depth ([current page] - [total page])
                        _g.AnalyticsOmniture.Evar25 = Convert.ToString(Math.Round(((double)(startRow - 1) / pageSize), 0) + 1) + " - " + Convert.ToString(Math.Round(((double)(TotalRows - 1) / pageSize), 0) + 1);

                        // Track total profiles that are highlighted in the search result
                        _g.AnalyticsOmniture.AddEvent("event5");
                        _g.AnalyticsOmniture.AddProductEvent("event5=" + Convert.ToString(this.highlightedProfileCount));
                        //_g.AnalyticsOmniture.Products = Convert.ToString(this.highlightedProfileCount) + ";" + Convert.ToString(this.count) + ";event5=2"; 

                    }
                }

                base.OnPreRender(e);
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        public void MemberRepeater_OnItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            count++;

            IMemberDTO member = e.Item.DataItem as IMemberDTO;

            if (galleryView == "true")
            {
                ProfileElements.MiniProfile miniProfile = (ProfileElements.MiniProfile)e.Item.FindControl("miniProfile");
                miniProfile.Visible = true;
                miniProfile.Member = member;
                miniProfile.Counter = count;
                miniProfile.Ordinal = count + startRow - 1;
                miniProfile.DisplayContext = ResultListContextType;
                miniProfile.PhotoOnly = PhotoOnly;
                miniProfile.DisplayMatchRating = DisplayMatchRating;
                if (resultListHandler.MatchPercentages.ContainsKey(member.MemberID))
                {
                    miniProfile.MatchRating = resultListHandler.MatchPercentages[member.MemberID];
                }

                // MatchMeter
                //miniProfile.IsMatchMeterEnabled = isMatchMeterEnabled;

                switch (ResultListContextType)
                {
                    case ResultContextType.SearchResult:
                        miniProfile.MyEntryPoint = BreadCrumbHelper.EntryPoint.SearchResults;
                        break;
                    case ResultContextType.QuickSearchResult:
                        miniProfile.MyEntryPoint = BreadCrumbHelper.EntryPoint.QuickSearchResults;
                        break;
                    case ResultContextType.ReverseSearchResult:
                        miniProfile.MyEntryPoint = BreadCrumbHelper.EntryPoint.ReverseSearchResults;
                        break;
                    case ResultContextType.AdvancedSearchResult:
                        miniProfile.MyEntryPoint = BreadCrumbHelper.EntryPoint.AdvancedSearchResults;
                        break;
                }

                miniProfile.LoadMiniProfile();
            }
            else
            {
                ProfileElements.MiniProfileListView miniProfileListView = (ProfileElements.MiniProfileListView)e.Item.FindControl("MiniProfileListView1");
                miniProfileListView.Visible = true;
                miniProfileListView.Member = member;
                miniProfileListView.Counter = count;
                miniProfileListView.Ordinal = count + startRow - 1;
                miniProfileListView.DisplayContext = ResultListContextType;
                miniProfileListView.PhotoOnly = PhotoOnly;
                miniProfileListView.DisplayMatchRating = DisplayMatchRating;
                if (resultListHandler.MatchPercentages.ContainsKey(member.MemberID))
                {
                    miniProfileListView.MatchRating = resultListHandler.MatchPercentages[member.MemberID];
                }

                // MatchMeter
                //miniProfileListView.IsMatchMeterEnabled = isMatchMeterEnabled;

                switch (ResultListContextType)
                {
                    case ResultContextType.SearchResult:
                        miniProfileListView.MyEntryPoint = BreadCrumbHelper.EntryPoint.SearchResults;
                        break;
                    case ResultContextType.QuickSearchResult:
                        miniProfileListView.MyEntryPoint = BreadCrumbHelper.EntryPoint.QuickSearchResults;
                        break;
                    case ResultContextType.ReverseSearchResult:
                        miniProfileListView.MyEntryPoint = BreadCrumbHelper.EntryPoint.ReverseSearchResults;
                        break;
                    case ResultContextType.AdvancedSearchResult:
                        miniProfileListView.MyEntryPoint = BreadCrumbHelper.EntryPoint.AdvancedSearchResults;
                        break;
                }

                miniProfileListView.LoadMiniProfile();
            }

            //if (FrameworkGlobals.memberHighlighted(member.MemberID, g.Brand))
            if (FrameworkGlobals.memberHighlighted(member, g.Brand))
            {
                highlightedProfileCount++;
            }
        }
        #endregion

        #region Page Init Related
        private void DoMembersOnlineInit()
        {

        }

        private void DoSearchResultInit()
        {

        }

        private void DoQuickSearchResultInit()
        {

        }

        private void DoAdvancedSearchResultInit()
        {

        }

        private void DoAffiliatoinGroupMemberInit()
        {

        }

        private void DoHotListInit()
        {
            if (Request["CategoryID"] != null)
            {
                HotListCategory = (HotListCategory)Enum.Parse(typeof(HotListCategory), Request["CategoryID"]);
            }
        }



        #endregion

        #region Public Methods
        public void LoadResultList()
        {
            if (!_resultsLoaded)
            {
                try
                {
                    resultListHandler = new ResultListHandler(g, this);
                    SetMatchMeter();
                    FrameworkGlobals.SaveBackToResultsURL(g);
                    int startRow = Constants.NULL_INT;

                    //	Default to 1.
                    this.startRow = 1;
                    StartRow = 1;

                    if (!g.ResetSearchStartRow)
                    {
                        // Check the hidden input field.
                        if (Request[hidStartRow.UniqueID] != null)
                        {
                            startRow = Int32.Parse(Request[hidStartRow.UniqueID]);

                            if (startRow >= 1)
                            {
                                //	Use internal first, if available
                                this.startRow = startRow;
                                StartRow = startRow;
                            }
                        }
                        else
                        {
                            // If there was no value in the hidden field, check the querystring.
                            if (Request.QueryString[ResultListHandler.QSPARAM_STARTROW] != null)
                            {
                                try
                                {
                                    startRow = Int32.Parse(Request.QueryString[ResultListHandler.QSPARAM_STARTROW]);
                                }
                                catch
                                {
                                    startRow = Constants.NULL_INT;
                                }

                                if (startRow >= 1)
                                {
                                    //	Use internal first, if available
                                    this.startRow = startRow;
                                    StartRow = startRow;
                                }
                            }
                        }
                    }

                    EndRow = this.startRow + pageSize - 1;
                    if (EndRow > TotalRows)
                    {
                        EndRow = TotalRows;
                    }

                    // Reset the SearchPref override if we are on page 1.  So, if a visitor hits a visitor limit then logs in,
                    // this flag is what keeps the search prefs from being overwritten by their saved search prefs.  However, if they
                    // start a new search (startRow == 1) then we revert back to their saved search prefs.  There is a loophole (a user
                    // navs back to page 1) but let's live with it for now.  See TT 12657.
                    if (startRow == Constants.NULL_INT)
                    {
                        g.Session.Remove(VisitorLimitHelper.SESSIONKEY_VISITORLIMIT_DONOTOVERWRITESEARCHPREFS);
                        // We need to load the search prefs again by setting it to null.
                        _g.ResetSearchPreferences();
                    }

                    switch (ResultListContextType)
                    {
                        case ResultContextType.HotList:
                            resultListHandler.LoadHotListPage();
                            resultListHandler.RenderHotListElements();
                            resultListHandler.BindMemberRepeater();
                            break;
                        case ResultContextType.MembersOnline:
                            if (MembersOnlineManager.Instance.IsE2MOLEnabled(g.Brand))
                            {
                                resultListHandler.LoadMembersOnlinePageConsolidated(IgnoreSAFilteredSearchResultsCache, IgnoreSASearchResultsCache, IgnoreAllSearchResultsCache);
                            }
                            else
                            {
                                resultListHandler.LoadMembersOnlinePage();
                            }
                            break;
                        case ResultContextType.SearchResult:
                            resultListHandler.LoadSearchResultPage(IgnoreSAFilteredSearchResultsCache, IgnoreSASearchResultsCache, IgnoreAllSearchResultsCache);
                            break;
                        case ResultContextType.QuickSearchResult:
                            resultListHandler.LoadQuickSearchResultPage(IgnoreSAFilteredSearchResultsCache, IgnoreSASearchResultsCache, IgnoreAllSearchResultsCache);
                            break;
                        case ResultContextType.KeywordSearchResult:
                            resultListHandler.LoadKeywordSearchResultPage();
                            break;
                        case ResultContextType.ReverseSearchResult:
                            resultListHandler.LoadReverseSearchResultPage(IgnoreSAFilteredSearchResultsCache, IgnoreSASearchResultsCache, IgnoreAllSearchResultsCache);
                            break;
                        case ResultContextType.PhotoGallery:
                            resultListHandler.LoadPhotoGalleryResultPage();
                            break;
                        case ResultContextType.MatchMeterMatches:
                            resultListHandler.LoadMatchMeterMatchesResultPage(MyMatchesCollection, ref MatchResultsMemberScoreDictionary, StartRow, PageSize, ref _TotalRows);
                            break;
                        case ResultContextType.AffiliationGroupMembers:
                            resultListHandler.LoadGroupMemberPage();
                            break;
                        case ResultContextType.AdvancedSearchResult:
                            resultListHandler.LoadAdvancedSearchResultPage(IgnoreSAFilteredSearchResultsCache, IgnoreSASearchResultsCache, IgnoreAllSearchResultsCache);
                            break;
                        case ResultContextType.AcademicVisitorSearchResultMen:
                            resultListHandler.LoadSectorResultPage();
                            break;
                        case ResultContextType.AcademicVisitorSearchResultWomen:
                            resultListHandler.LoadSectorResultPage(true);
                            break;
                        case ResultContextType.SectorSearchResult:
                            resultListHandler.LoadSectorResultPage();
                            break;
                    }

                    //Notify listeners that results have loaded
                    _resultsLoaded = true;

                    if (ResultsLoaded != null)
                    {
                        ResultsLoaded(SearchIDs);
                    }

                    //Render paging
                    resultListHandler.RenderPaging();
                }
                catch (Exception ex)
                {
                    _g.ProcessException(ex);
                }
            }
        }

        public DropDownList GetMOLSortDropDown()
        {
            return (DropDownList)Parent.FindControl("ddlMemberSort");
        }

        /// <summary>
        /// For Analytics
        /// </summary>
        /// <param name="e"></param>
        public void RenderAnalytics(ArrayList members, bool moreResultsAvailable)
        {
            if (!IsPartialRender)
            {
                int analyticsID = ConstantsTemp.PAGE_SEARCH_RESULTS;
                if (members.Count > 0)
                {
                    // SAGE - each results page should have a different g.Analytics["p"] value.
                    analyticsID += 1 + startRow / pageSize;
                    // SAGE has a limit to how many pages it tracks.
                    if (analyticsID >= ConstantsTemp.PAGE_SEARCH_RESULTS + NumberOfTrackedPages)
                        analyticsID = ConstantsTemp.PAGE_SEARCH_RESULTS + NumberOfTrackedPages;
                }

                g.Analytics.SetAttribute("p", analyticsID.ToString());
            }
        }
        #endregion

        #region Private Methods
        private void SetMatchMeter()
        {
            // MatchMeter (jmeter)
            int mmsett = Int32.Parse(RuntimeSettings.GetSetting("MatchTest_App_Settings", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID));
            int mmenumval = (Int32)WebConstants.MatchMeterFlags.EnableApp;
            if ((mmsett & mmenumval) == mmenumval)
            {
                isMatchMeterEnabled = true;
            }
        }

        #endregion


        #region Web Form Designer generated code, unaltered
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Load += Page_Load;
            Init += Page_Init;
        }
        #endregion


    }
}
