﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Member.ValueObjects.Interfaces;
using Matchnet.Member.ValueObjects.MemberDTO;
using Matchnet.PhotoSearch.ValueObjects;
using Matchnet.Web.Applications.MembersOnline.Controls;
using Matchnet.Web.Framework.Managers;
using Matchnet.Web.Framework.Search;
using Matchnet.List.ValueObjects;
using Matchnet.List.ServiceAdapters;
using Matchnet.Web.Applications.Home;
using Matchnet.Search.ValueObjects;
using Matchnet.Search.ServiceAdapters;
using System.Collections;
using Matchnet.Web.Framework.Test;
using Matchnet.Member.ServiceAdapters;
using Matchnet.PhotoSearch.ServiceAdapters;
using Matchnet.Search.Interfaces;
using Matchnet.MembersOnline.ValueObjects;
using Matchnet.MembersOnline.ServiceAdapters;
using Matchnet.Lib;
using Matchnet.Web.Facelink.Search;
using Matchnet.Web.Framework.Ui.BasicElements;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Session.ValueObjects;

namespace Matchnet.Web.Framework.Ui.SearchElements
{
    public class MiniSearchBase : FrameworkControl
    {
        #region Fields
        protected string DomainId = "DomainID";
        protected string HasPhotoFlag = "HasPhotoFlag";
        protected string MarketingFlagAttribute = "MiniSearchMarketingFlag";
        protected int PageSize = 12;

        protected Boolean displayMiniSearchProfile = false;
        protected BreadCrumbHelper.EntryPoint entryPoint = BreadCrumbHelper.EntryPoint.Unknown;
        protected BreadCrumbHelper.PremiumEntryPoint premiumEntryPoint = BreadCrumbHelper.PremiumEntryPoint.NoTracking;
        protected MOCollection memberOrderCollection;
        protected Int32 hotListCategoryId;
        protected Int32 ordinal = Constants.NULL_INT; //1-index
        protected Int32 startRow = Constants.NULL_INT; //1-index; used to determine page number
        protected Int32 totalCount = 0;
        protected Int32 selectedMemberId = Constants.NULL_INT;
        protected Int32 nextMemberID = Constants.NULL_INT;
        protected Int32 prevMemberID = Constants.NULL_INT;
        protected String titleResource = "TXT_TITLE_DEFAULT";
        protected String titleName = String.Empty;
        protected String backToResultUrl = String.Empty;
        protected Boolean omnitureEnabled;
        protected Boolean isVisitor;
        protected Boolean showNew;
        protected Boolean includeNextPrevProfile = false;
        protected SearchType searchType = SearchType.None;
        protected Dictionary<int, int> MatchPercentages = new Dictionary<int, int>();
        #endregion

        #region Properties
        /// <summary>
        /// Specifies whether this minisearch is being loaded based on 
        /// page navigation (set by MiniSearchAPI or applicable container)
        /// </summary>
        public Boolean IsPageNav { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to display the mini search-containing div.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if the mini search-containing div should be displayed; otherwise, <c>false</c>.
        /// </value>
        public Boolean DisplayMiniSearchContainingDiv { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to display the mini search ajax loading div.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if the display mini search ajax loading div should be displayed; otherwise, <c>false</c>.
        /// </value>
        public Boolean DisplayMiniSearchAjaxLoadingDiv { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to display the mini search marketing div.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if the display mini search marketing div should be displayed; otherwise, <c>false</c>.
        /// </value>
        public Boolean DisplayMiniSearchMarketingDiv { get; set; }
        public int ResultsPageSize { get { return PageSize; } }

        /// <summary>
        /// Specifies whether this minisearch is being loaded from JDIL Bachelor page
        /// </summary>
        public Boolean IsBachelorPage { get; set; }
        private static SettingsManager _settingsManager;

        public static SettingsManager SettingsManagerObj
        {
            get
            {
                if (null == _settingsManager) _settingsManager = new SettingsManager();
                return _settingsManager;
            }
            set { _settingsManager = value; }
        }
        #endregion

        #region Methods
        public virtual void LoadMiniSearch()
        {
        }

        private ArrayList GetMembersFromResults(MatchnetQueryResults searchResults, ContextGlobal context)
        {
            return MemberDTOManager.Instance.GetIMemberDTOsFromResults(searchResults, context, MemberType.Search, MemberLoadFlags.None);
        }

        protected virtual String GetReverseSearchUrl()
        {
            var viewProfileUrl = "/Applications/Search/ReverseSearch.aspx?";

            Int32 page;
            if (Int32.TryParse(HttpContext.Current.Request["rp"], out page) == false || page < 1) page = 1;

            viewProfileUrl += "pg=" + page;

            if (String.IsNullOrEmpty(HttpContext.Current.Request["sl"]) == false)
            {
                viewProfileUrl += "&sl=" + HttpContext.Current.Request["sl"];
            }

            if (String.IsNullOrEmpty(HttpContext.Current.Request["op"]) == false)
            {
                viewProfileUrl += "&op=" + HttpContext.Current.Request["op"];
            }

            return viewProfileUrl;
        }

        protected virtual String GetUrl(String currentlyAssembledUrl)
        {
            return entryPoint == BreadCrumbHelper.EntryPoint.KeywordSearchResults ?
                SearchPageHelper.GetKeywordSearchResultsUrl(Request) :
                entryPoint == BreadCrumbHelper.EntryPoint.ReverseSearchResults ?
                    GetReverseSearchUrl() :
                    currentlyAssembledUrl;
        }

        protected virtual void SetStartRow()
        {
            if (IsPageNav)
            {
                startRow = Conversion.CInt(Request["StartRow"], 0); //from url
            }

            if (startRow < 1)
            {
                //determine startRow based on ordinal
                int page = ((ordinal % PageSize) == 0) ? (ordinal / PageSize) : (ordinal / PageSize + 1);
                startRow = ((page - 1) * PageSize) + 1;
            }

            if (startRow < 1) startRow = 1;
        }

        protected virtual void SetEntryPoints()
        {
            if (g.AppPage.ControlName.ToLower() == "bachelor" || IsBachelorPage)
            {
                entryPoint = BreadCrumbHelper.EntryPoint.Bachelor;

            }
            else
            {
                entryPoint = BreadCrumbHelper.GetEntryPoint(
                   selectedMemberId,
                   Conversion.CInt(Request[WebConstants.URL_PARAMETER_NAME_ENTRYPOINT], Constants.NULL_INT)
                   );
            }

            premiumEntryPoint = (BreadCrumbHelper.PremiumEntryPoint)Conversion.CInt(Request[WebConstants.URL_PARAMETER_NAME_PREMIUM_ENTRYPOINT], 0);
        }

        protected virtual Int32 GetPageNumber()
        {
            Int32 pageFromParameter;

            if (Int32.TryParse(HttpContext.Current.Request.Params[
                    Search.Constants.ReverseSearch.Parameter.Page],
                    out pageFromParameter))
            {
                return pageFromParameter < 1 ? Search.Constants.Paging.DefaultPage : pageFromParameter;
            }
            return Search.Constants.Paging.DefaultPage;
        }

        protected virtual Boolean OnlyPhotos
        {
            get
            {
                return String.IsNullOrEmpty(SearchPageHelper.GetParamValue(
                    Search.Constants.ReverseSearch.Parameter.OnlyPhotos,
                    HttpContext.Current.Request)) == false;
            }
        }

        protected virtual void SetSelectedMemberId()
        {
            switch (entryPoint)
            {
                case BreadCrumbHelper.EntryPoint.KeywordSearchResults:
                    selectedMemberId = SearchPageHelper.GetMemberIdFromOrdinal(g.Member == null ? Constants.NULL_INT : g.Member.MemberID
                        , ordinal, Request, g.Brand.Site.Community.CommunityID);
                    break;
                default:
                    selectedMemberId = Conversion.CInt(Request.Params.Get("MemberId"));
                    break;
            }
        }

        protected virtual List<ProfileHolder> GetProfileList()
        {
            searchType = Matchnet.Web.Applications.Search.SearchUtil.GetSearchTypeForActivityLogging(entryPoint);

            switch (entryPoint)
            {
                case BreadCrumbHelper.EntryPoint.SearchResults:
                case BreadCrumbHelper.EntryPoint.HomeHeroProfileMatches:
                case BreadCrumbHelper.EntryPoint.HomeHeroProfileFilmStripMatches:
                case BreadCrumbHelper.EntryPoint.HomeProfileFilmstripWithVoting:
                    if (premiumEntryPoint == BreadCrumbHelper.PremiumEntryPoint.Spotlight)
                    {
                        titleResource = "TXT_TITLE_NEW_MEMBERS";
                    }

                    return GetProfilesForSearchResults();

                case BreadCrumbHelper.EntryPoint.HotLists:
                case BreadCrumbHelper.EntryPoint.HomeViewedYouWidget:
                    if (hotListCategoryId <= 0)
                    {
                        titleResource = "TXT_TITLE_HOTLIST_" + (HotListCategory)hotListCategoryId;
                    }
                    else
                    {
                        SetCustomCategories();
                    }

                    return GetProfilesForHotList();

                case BreadCrumbHelper.EntryPoint.MembersOnline:
                case BreadCrumbHelper.EntryPoint.HomeHeroProfileMOL:
                case BreadCrumbHelper.EntryPoint.HomeHeroProfileFilmStripMOL:
                case BreadCrumbHelper.EntryPoint.HomeProfileFilmstripWithVotingMOL:
                    titleResource = "TXT_TITLE_MEMBERS_ONLINE";
                    return GetProfilesForMembersOnline();

                case BreadCrumbHelper.EntryPoint.Bachelor:
                    titleResource = "TXT_TITLE_BACHELOR_MEMBERS_ONLINE";
                    return GetProfilesForMembersOnline();

                case BreadCrumbHelper.EntryPoint.HomePage:
                    if (premiumEntryPoint == BreadCrumbHelper.PremiumEntryPoint.Spotlight)
                    {
                        titleResource = "TXT_TITLE_NEW_MEMBERS";
                    }

                    return GetProfilesForHomePage();

                case BreadCrumbHelper.EntryPoint.PhotoGallery:
                    titleResource = "TXT_TITLE_PHOTO_GALLERY";
                    return GetProfilesForPhotoGallery();

                case BreadCrumbHelper.EntryPoint.QuickSearchResults:
                    titleResource = "TXT_TITLE_QUICK_SEARCH";
                    return GetProfilesForQuickSearchResults();

                case BreadCrumbHelper.EntryPoint.KeywordSearchResults:
                    titleResource = "TXT_TITLE_KEYWORD_SEARCH";
                    return GetProfilesForKeywordSearchResults();

                case BreadCrumbHelper.EntryPoint.ReverseSearchResults:
                    titleResource = "TXT_TITLE_REVERSE_SEARCH";
                    return GetProfilesForReverseSearch30Results();

                case BreadCrumbHelper.EntryPoint.AdvancedSearchResults:
                    titleResource = "TXT_TITLE_ADVANCED_SEARCH";
                    return GetProfilesForAdvancedSearchResults();

                default:

                    if (Request.QueryString["mmvid"] == null)
                    {
                        //we want to leave the title and sorting to default if the member is coming from a MatchMail email, 
                        //so we only go into this block if code if there's no "mmvid" in the querystring.
                        showNew = true;
                        titleResource = "TXT_TITLE_NEW_MEMBERS";
                    }
                    return GetProfilesForSearchResults();
            }
        }

        protected virtual void SetCustomCategories()
        {
            var customCategories = ListSA.Instance.GetCustomListCategories(
                g.Member.MemberID, g.Brand.Site.Community.CommunityID);

            if (customCategories != null && customCategories.Count > 0)
            {
                foreach (CustomCategory currentCategory in customCategories)
                {
                    if (currentCategory.CategoryID != hotListCategoryId) continue;

                    titleName = currentCategory.Description;
                    titleResource = String.Empty;
                    break;
                }

            }
        }

        protected virtual List<ProfileHolder> GetProfilesForSearchResults()
        {
            var profileList = new List<ProfileHolder>();
            MatchnetQueryResults searchResults;

            //Set search order
            // We've been getting a lot of exceptions on production saying SearchOrderBy is missing
            if (!PrefsHaveValue(g.SearchPreferences, "SearchOrderBy"))
            {
                g.SearchPreferences.Add("SearchOrderBy", ((Int32)QuerySorting.JoinDate).ToString());
            }

            //Set to sort by newest and has photos only for matches in filmstrip on new homepage
            string currentPhotoPreference = g.SearchPreferences["HasPhotoFlag"];
            string currentOrderByPreference = g.SearchPreferences["SearchOrderBy"];
            if (entryPoint == BreadCrumbHelper.EntryPoint.HomeHeroProfileFilmStripMatches
                && HomeUtil.IsHome40Enabled(g.Brand))
            {
                g.SearchPreferences["SearchOrderBy"] = ((Int32)QuerySorting.LastLogonDate).ToString();
                g.SearchPreferences["HasPhotoFlag"] = "1";
            }
            else if (entryPoint == BreadCrumbHelper.EntryPoint.HomeProfileFilmstripWithVoting)
            {
                g.SearchPreferences["SearchOrderBy"] = ((Int32)QuerySorting.LastLogonDate).ToString();
                g.SearchPreferences["HasPhotoFlag"] = "1";

                //set exclude ynm no list
                g.SearchPreferences["excludeynmnoflag"] = "1";
            }

            g.SearchPreferences["DomainID"] = g.TargetCommunityID.ToString();

            int memberId = (g.Member != null) ? g.Session.GetInt(WebConstants.SESSION_PROPERTY_NAME_MEMBERID) : 0;

            // default several required search preferences for access without cookies
            if (memberId == 0 || (g.Session.GetString(VisitorLimitHelper.SESSIONKEY_VISITORLIMIT_DONOTOVERWRITESEARCHPREFS, String.Empty) == "true"))
            {
                if (!PrefsHaveValue(g.SearchPreferences, "SearchTypeID"))
                {
                    g.SearchPreferences["SearchTypeID"] = g.Brand.Site.DefaultSearchTypeID.ToString();
                }

                if (!PrefsHaveValue(g.SearchPreferences, "RegionID"))
                {
                    g.SearchPreferences["RegionID"] = g.Brand.Site.DefaultRegionID.ToString();
                }

                if (!PrefsHaveValue(g.SearchPreferences, "GenderMask"))
                {
                    // set the gender mask to M s M and M s F for all others
                    if (g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.Glimpse)
                    {
                        g.SearchPreferences["GenderMask"] = (ConstantsTemp.GENDERID_MALE + ConstantsTemp.GENDERID_SEEKING_MALE).ToString();
                    }
                    else
                    {
                        g.SearchPreferences["GenderMask"] = (ConstantsTemp.GENDERID_MALE + ConstantsTemp.GENDERID_SEEKING_FEMALE).ToString();
                    }
                }

                if (!PrefsHaveValue(g.SearchPreferences, "MinAge"))
                {
                    g.SearchPreferences["MinAge"] = g.Brand.DefaultAgeMin.ToString();
                }

                if (!PrefsHaveValue(g.SearchPreferences, "MaxAge"))
                {
                    g.SearchPreferences["MaxAge"] = g.Brand.DefaultAgeMax.ToString();
                }
                if (!PrefsHaveValue(g.SearchPreferences, "Distance"))
                {
                    g.SearchPreferences["Distance"] = g.Brand.DefaultSearchRadius.ToString();
                }
            }

            if (g.SearchPreferences["CountryRegionID"] == string.Empty)
            {
                g.SearchPreferences.Add("CountryRegionID", g.Brand.Site.DefaultRegionID.ToString());
            }

            //pass flag to determine if search redesign is enabled
            if (BetaHelper.IsSearchRedesign30Enabled(g))
            {
                g.SearchPreferences.Add("SearchRedesign30", "1");
            }
            else
            {
                g.SearchPreferences.Add("SearchRedesign30", "0");
            }

            SearchPreferenceCollection searchPreferences = g.SearchPreferences;
            try
            {
                searchResults = MemberSearchSA.Instance.Search(searchPreferences
                     , g.Brand.Site.Community.CommunityID
                     , g.Brand.Site.SiteID
                     , startRow - 1// startRow is 1-indexed and this search is 0-indexed
                     , PageSize
                     , g.Member == null ? Constants.NULL_INT : g.Member.MemberID
                     , SearchEngineType.FAST
                     , searchType
                     , SearchEntryPoint.MiniSearchProfilePage
                     , false
                     , false
                     , false);

                //get members
                if (searchResults != null && searchResults.ToArrayList().Count > 0)
                {
                    totalCount = searchResults.MatchesFound;

                    ArrayList members = GetMembersFromResults(searchResults, g);

                    if (members != null)
                    {
                        if (g.Member != null)
                        {
                            foreach (IMatchnetResultItem resultItem in searchResults.Items.List)
                            {
                                if (!MatchPercentages.ContainsKey(resultItem.MemberID))
                                {
                                    MatchPercentages.Add(resultItem.MemberID, resultItem.MatchScore);
                                }
                            }
                        }

                        for (int i = 0; i < members.Count; i++)
                        {
                            ProfileHolder profileMWH = new ProfileHolder();
                            profileMWH.MyEntryPoint = entryPoint;
                            profileMWH.Ordinal = startRow + i; //ordinals start at 1
                            //profileMWH.HotlistCategory = cat;
                            profileMWH.Member = members[i] as IMemberDTO;
                            if (MatchPercentages.ContainsKey(profileMWH.Member.MemberID))
                            {
                                profileMWH.MatchRating = MatchPercentages[profileMWH.Member.MemberID];
                            }
                            profileList.Add(profileMWH);

                        }
                    }
                }

                if (profileList.Count > 0 && includeNextPrevProfile)
                {
                    SetNextPrevMemberID(profileList);

                    //prev member
                    if (prevMemberID <= 0 && ordinal > 1)
                    {
                        searchResults = MemberSearchSA.Instance.Search(searchPreferences
                         , g.Brand.Site.Community.CommunityID
                         , g.Brand.Site.SiteID
                         , ordinal - 2
                         , 1
                         , g.Member == null ? Constants.NULL_INT : g.Member.MemberID);

                        if (searchResults != null && searchResults.ToArrayList().Count > 0)
                        {
                            prevMemberID = searchResults.Items[0].MemberID;
                            if (!MatchPercentages.ContainsKey(prevMemberID))
                            {
                                MatchPercentages.Add(prevMemberID, searchResults.Items[0].MatchScore);
                            }
                        }
                    }

                    //next member
                    if (nextMemberID <= 0 && ordinal < totalCount)
                    {
                        searchResults = MemberSearchSA.Instance.Search(searchPreferences
                         , g.Brand.Site.Community.CommunityID
                         , g.Brand.Site.SiteID
                         , ordinal
                         , 1
                         , g.Member == null ? Constants.NULL_INT : g.Member.MemberID);

                        if (searchResults != null && searchResults.ToArrayList().Count > 0)
                        {
                            nextMemberID = searchResults.Items[0].MemberID;
                            if (!MatchPercentages.ContainsKey(nextMemberID))
                            {
                                MatchPercentages.Add(nextMemberID, searchResults.Items[0].MatchScore);
                            }
                        }
                    }
                }

                //set temp preference back to old value
                g.SearchPreferences["HasPhotoFlag"] = currentPhotoPreference;
                g.SearchPreferences["SearchOrderBy"] = currentOrderByPreference;
                g.SearchPreferences["excludeynmnoflag"] = "";
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }

            return profileList;
        }

        protected virtual List<ProfileHolder> GetProfilesForHotList()
        {
            List<ProfileHolder> profileList = new List<ProfileHolder>();

            HotListCategory category = (HotListCategory)hotListCategoryId;

            ArrayList targetMemberIDs = new ArrayList();
            try
            {
                targetMemberIDs = g.List.GetListMembers(category,
                                                          g.Brand.Site.Community.CommunityID,
                                                          g.Brand.Site.SiteID,
                                                          startRow, //this search is 1-index
                                                          PageSize,
                                                          out totalCount);

                //get members
                if (targetMemberIDs != null && targetMemberIDs.Count > 0)
                {
                    ArrayList members = MemberDTOManager.Instance.GetIMemberDTOs(g, targetMemberIDs, MemberType.Search, MemberLoadFlags.None);

                    if (members != null)
                    {
                        for (int i = 0; i < members.Count; i++)
                        {
                            profileList.Add(
                                new ProfileHolder
                                {
                                    MyEntryPoint = BreadCrumbHelper.EntryPoint.HotLists,
                                    Ordinal = startRow + i,
                                    HotlistCategory = category,
                                    Member = members[i] as IMemberDTO
                                }
                            );
                        }
                    }
                }

                if (profileList.Count > 0 && includeNextPrevProfile)
                {
                    SetNextPrevMemberID(profileList);

                    int tempCount = 0;
                    //prev member
                    if (prevMemberID <= 0 && ordinal > 1)
                    {
                        targetMemberIDs = g.List.GetListMembers(category,
                                                          g.Brand.Site.Community.CommunityID,
                                                          g.Brand.Site.SiteID,
                                                          ordinal - 1,
                                                          1,
                                                          out tempCount);

                        if (targetMemberIDs != null && targetMemberIDs.Count > 0)
                        {
                            prevMemberID = Convert.ToInt32(targetMemberIDs[0]);
                        }
                    }

                    //next member
                    if (nextMemberID <= 0 && ordinal < totalCount)
                    {
                        targetMemberIDs = g.List.GetListMembers(category,
                                                          g.Brand.Site.Community.CommunityID,
                                                          g.Brand.Site.SiteID,
                                                          ordinal + 1,
                                                          1,
                                                          out tempCount);

                        if (targetMemberIDs != null && targetMemberIDs.Count > 0)
                        {
                            nextMemberID = Convert.ToInt32(targetMemberIDs[0]);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }


            return profileList;
        }

        protected virtual List<ProfileHolder> GetProfilesForMembersOnline()
        {
            List<ProfileHolder> profileList = new List<ProfileHolder>();

            Int16 genderMask = 0;
            SortFieldType sortField = memberOrderCollection.OrderBy;
            SortDirectionType sortDirection;
            List<HotListCategory> filterOutHotlist = null;

            if (memberOrderCollection.GenderMask > 0)
            {
                genderMask = (Int16)memberOrderCollection.GenderMask;
            }

            if (entryPoint == BreadCrumbHelper.EntryPoint.HomeProfileFilmstripWithVotingMOL)
            {
                filterOutHotlist = new List<HotListCategory>();
                filterOutHotlist.Add(HotListCategory.MembersClickedNo);

                sortField = SortFieldType.HasPhoto;
            }

            switch (sortField)
            {
                case SortFieldType.Age:
                case SortFieldType.HasPhoto:
                case SortFieldType.InsertDate:
                    sortDirection = SortDirectionType.Desc;
                    break;
                default:
                    sortDirection = SortDirectionType.Asc;
                    break;
            }

            try
            {
                MOLQueryResult MOLResults;
                if (entryPoint == BreadCrumbHelper.EntryPoint.Bachelor)
                {
                    MOLResults = GetProfilesFromMOLFilter();
                }
                else
                {
                    MOLResults = MembersOnlineSA.Instance.GetMemberIDs(_g.Session.Key.ToString(),
                  _g.Brand.Site.Community.CommunityID,
                  genderMask,
                  memberOrderCollection.RegionID,
                  (Int16)memberOrderCollection.AgeMin,
                  (Int16)memberOrderCollection.AgeMax,
                  memberOrderCollection.LanguageMask,
                  sortField,
                  sortDirection,
                  startRow - 1, //search is 0 index
                  PageSize,
                  g.Member == null ? Constants.NULL_INT : g.Member.MemberID,
                  filterOutHotlist);
                }



                totalCount = MOLResults.TotalMembersOnline;
                ArrayList memberIDs = MOLResults.ToArrayList();
                //get members
                if (memberIDs != null && memberIDs.Count > 0)
                {
                    ArrayList members = MemberSA.Instance.GetMembers(memberIDs, MemberLoadFlags.None);

                    if (members != null)
                    {
                        for (var i = 0; i < members.Count; i++)
                        {
                            profileList.Add(
                                new ProfileHolder
                                {
                                    MyEntryPoint = BreadCrumbHelper.EntryPoint.MembersOnline,
                                    Ordinal = startRow + i,
                                    Member = members[i] as Member.ServiceAdapters.Member
                                }
                            );
                        }
                    }
                }

                if (profileList.Count > 0 && includeNextPrevProfile)
                {
                    SetNextPrevMemberID(profileList);

                    //prev member
                    if (prevMemberID <= 0 && ordinal > 1)
                    {
                        MOLResults = MembersOnlineSA.Instance.GetMemberIDs(_g.Session.Key.ToString(),
                         _g.Brand.Site.Community.CommunityID,
                         genderMask,
                         memberOrderCollection.RegionID,
                         (Int16)memberOrderCollection.AgeMin,
                         (Int16)memberOrderCollection.AgeMax,
                         memberOrderCollection.LanguageMask,
                         sortField,
                         sortDirection,
                         ordinal - 2,
                         1,
                         g.Member == null ? Constants.NULL_INT : g.Member.MemberID);

                        memberIDs = MOLResults.ToArrayList();
                        if (memberIDs != null && memberIDs.Count > 0)
                        {
                            prevMemberID = Convert.ToInt32(memberIDs[0]);
                        }
                    }

                    //next member
                    if (nextMemberID <= 0 && ordinal < totalCount)
                    {
                        MOLResults = MembersOnlineSA.Instance.GetMemberIDs(_g.Session.Key.ToString(),
                         _g.Brand.Site.Community.CommunityID,
                         genderMask,
                         memberOrderCollection.RegionID,
                         (Int16)memberOrderCollection.AgeMin,
                         (Int16)memberOrderCollection.AgeMax,
                         memberOrderCollection.LanguageMask,
                         sortField,
                         sortDirection,
                         ordinal,
                         1,
                         g.Member == null ? Constants.NULL_INT : g.Member.MemberID);

                        memberIDs = MOLResults.ToArrayList();
                        if (memberIDs != null && memberIDs.Count > 0)
                        {
                            nextMemberID = Convert.ToInt32(memberIDs[0]);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }

            return profileList;
        }

        private MOLQueryResult GetProfilesFromMOLFilter()
        {
            SortFieldType sortField = SortFieldType.HasPhoto;
            SortDirectionType sortDirection = SortDirectionType.Desc;

            System.Web.UI.Page page = new System.Web.UI.Page();
            MembersOnlineFilter20 molf = (MembersOnlineFilter20)page.LoadControl("/Applications/MembersOnline/Controls/MembersOnlineFilter20.ascx");
            System.Web.UI.HtmlControls.HtmlForm Form1 = new System.Web.UI.HtmlControls.HtmlForm();
            Form1.Controls.Add(molf);
            page.Controls.Add(Form1);
            StringWriter sw = new StringWriter();
            HttpContext.Current.Server.Execute(page, sw, false);


            var genderMask = molf.GetGenderMask();
            var regionID = molf.intRegionID;
            var ageMin = molf.intAgeMin;
            var ageMax = molf.intAgeMax;
            var languageMask = molf.intLanguageMask;

            var collection = new MOCollection(SortFieldType.HasPhoto.ToString(), genderMask, regionID, ageMin, ageMax, languageMask);

            Int16 genderMaskShort = 0;

            if (collection.GenderMask != Constants.NULL_INT)
            {
                genderMaskShort = (Int16)collection.GenderMask;
            }


            Matchnet.MembersOnline.ValueObjects.MOLQueryResult MOLResults =
                                            MembersOnlineSA.Instance.GetMemberIDs(_g.Session.Key.ToString(),
                                            _g.Brand.Site.Community.CommunityID,
                                            genderMaskShort,
                                            collection.RegionID,
                                            (Int16)collection.AgeMin,
                                            (Int16)((Int16)collection.AgeMax + 1),
                                            collection.LanguageMask,
                                            sortField,
                                            sortDirection,
                                            startRow - 1,
                                            PageSize,
                                            _g.Member == null ? Constants.NULL_INT : _g.Member.MemberID);

            return MOLResults;
        }


        protected virtual List<ProfileHolder> GetProfilesForHomePage()
        {
            List<ProfileHolder> profileList = new List<ProfileHolder>();
            SearchPreferenceCollection prefs = g.SearchPreferences;
            MatchnetQueryResults searchResults;

            //get current photo preference before we modify it
            string photoPreference = prefs.Value(HasPhotoFlag);
            prefs.Add(HasPhotoFlag, "1");

            //Ensure that domainID is set on search preferences for the correct Community.
            prefs.Add(DomainId, g.Brand.Site.Community.CommunityID.ToString());

            //Set search order
            int SearchOrderBy;
            if (premiumEntryPoint == BreadCrumbHelper.PremiumEntryPoint.Spotlight || showNew)
            {
                //spotlight profile should should profiles based on newest
                SearchOrderBy = (Int32)QuerySorting.JoinDate;
                g.SearchPreferences["SearchOrderBy"] = SearchOrderBy.ToString();
            }

            //pass flag to determine if search redesign is enabled
            if (BetaHelper.IsSearchRedesign30Enabled(g))
            {
                g.SearchPreferences.Add("SearchRedesign30", "1");
            }
            else
            {
                g.SearchPreferences.Add("SearchRedesign30", "0");
            }

            try
            {
                // Potential for exception due to invalid search preferences on this member's stored set of preferences.
                searchResults = MemberSearchSA.Instance.Search(prefs
                     , g.Brand.Site.Community.CommunityID
                     , g.Brand.Site.SiteID
                     , startRow - 1// startRow is 1-indexed and this search is 0-indexed
                     , PageSize
                     , g.Member == null ? Constants.NULL_INT : g.Member.MemberID
                     , SearchEngineType.FAST
                     , searchType
                     , SearchEntryPoint.MiniSearchProfilePage
                     , false
                     , false
                     , false);

                //get members
                if (searchResults != null && searchResults.ToArrayList().Count > 0)
                {
                    totalCount = searchResults.MatchesFound;

                    ArrayList members = GetMembersFromResults(searchResults, g);

                    if (members != null)
                    {
                        for (int i = 0; i < members.Count; i++)
                        {
                            profileList.Add(
                                new ProfileHolder
                                {
                                    MyEntryPoint = BreadCrumbHelper.EntryPoint.HomePage,
                                    Ordinal = startRow + i,
                                    Member = members[i] as IMemberDTO
                                }
                            );
                        }
                    }
                }

                if (profileList.Count > 0 && includeNextPrevProfile)
                {
                    SetNextPrevMemberID(profileList);

                    //prev member
                    if (prevMemberID <= 0 && ordinal > 1)
                    {
                        searchResults = MemberSearchSA.Instance.Search(prefs
                         , g.Brand.Site.Community.CommunityID
                         , g.Brand.Site.SiteID
                         , ordinal - 2
                         , 1
                         , g.Member == null ? Constants.NULL_INT : g.Member.MemberID);

                        if (searchResults != null && searchResults.ToArrayList().Count > 0)
                        {
                            prevMemberID = Convert.ToInt32(searchResults.ToArrayList()[0]);
                        }
                    }

                    //next member
                    if (nextMemberID <= 0 && ordinal < totalCount)
                    {
                        searchResults = MemberSearchSA.Instance.Search(prefs
                         , g.Brand.Site.Community.CommunityID
                         , g.Brand.Site.SiteID
                         , ordinal
                         , 1
                         , g.Member == null ? Constants.NULL_INT : g.Member.MemberID);

                        if (searchResults != null && searchResults.ToArrayList().Count > 0)
                        {
                            nextMemberID = Convert.ToInt32(searchResults.ToArrayList()[0]);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
                searchResults = new MatchnetQueryResults(0);
            }

            //set photo preference back to old value
            prefs.Add(HasPhotoFlag, photoPreference);


            return profileList;
        }

        protected virtual List<ProfileHolder> GetProfilesForPhotoGallery()
        {
            var profileList = new List<ProfileHolder>();

            PhotoSearch.ValueObjects.PhotoGalleryQuery query =
                Applications.PhotoGallery.PhotoGalleryUtils.GetQueryFromSession(g.Brand, g.Member != null ? g.Member.MemberID : int.MinValue);

            if (query != null)
            {
                int page = startRow / PageSize + 1;

                try
                {
                    PhotoGalleryResultList photoResultList = PhotoGalleryManager.Instance.SearchPhotoGallery(g.Brand,
                                                                    g.Member == null ? Constants.NULL_INT : g.Member.MemberID,
                                                                    startRow,
                                                                    PageSize,
                                                                    query,
                                                                    null);

                    ArrayList res = photoResultList.PhotoGalleryResults;
                    GetPhotoGalleryMembers(res, profileList);

                    query.ReloadFlag = PhotoSearch.ValueObjects.ReLoadFlags.load;
                    Applications.PhotoGallery.PhotoGalleryUtils.SaveQueryToSession(query, g.Brand, g.Member != null ? g.Member.MemberID : int.MinValue);

                    if (profileList.Count > 0 && includeNextPrevProfile)
                    {
                        SetNextPrevMemberID(profileList);

                        int tempCount = 0;
                        //prev member
                        if (prevMemberID <= 0 && ordinal > 1 && page > 1)
                        {
                            res = PhotoGallerySA.Instance.Search(query, g.Member == null ? Constants.NULL_INT : g.Member.MemberID,
                                    g.Brand.Site.Community.CommunityID, page - 1, PageSize, out tempCount);

                            if (res != null && res.Count > 0)
                            {
                                prevMemberID = Convert.ToInt32(res[res.Count - 1]);
                            }
                        }

                        //next member
                        if (nextMemberID <= 0 && ordinal < totalCount)
                        {
                            res = PhotoGallerySA.Instance.Search(query, g.Member == null ? Constants.NULL_INT : g.Member.MemberID,
                                    g.Brand.Site.Community.CommunityID, page + 1, PageSize, out tempCount);

                            if (res != null && res.Count > 0)
                            {
                                nextMemberID = Convert.ToInt32(res[0]);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    g.ProcessException(ex);
                }

            }

            return profileList;
        }

        public void GetPhotoGalleryResultMembers(ArrayList resultlist, int communityid, int siteid, int brandid)
        {
            try
            {
                //MemberPhotoHelper photoHelper = new MemberPhotoHelper(RuntimeSettings.Instance);

                ArrayList memberIDs = new ArrayList();
                if (resultlist == null)
                    return;

                for (int i = 0; i < resultlist.Count; i++)
                {
                    memberIDs.Add(((PhotoResultItem)resultlist[i]).MemberID);
                }

                ArrayList members = MemberSA.Instance.GetMembers(memberIDs, MemberLoadFlags.None);
                if (members == null)
                    return;

                for (int i = 0; i < members.Count; i++)
                {
                    Member.ServiceAdapters.Member member = (Member.ServiceAdapters.Member)members[i];
                    if (member == null)
                        continue;
                    PhotoGalleryMember gmember = new PhotoGalleryMember();
                    gmember.MemberID = member.MemberID;
                    gmember.BirthDate = member.GetAttributeDate(communityid, siteid, brandid, "Birthdate", DateTime.MinValue);
                    Matchnet.Content.ValueObjects.BrandConfig.Brand brand = BrandConfigSA.Instance.GetBrandByID(brandid);
                    gmember.UserName = member.GetUserName(brand);
                    var photos = MemberPhotoDisplayManager.Instance.GetAllPhotos(member, communityid);
                    if (photos != null)
                    {
                        gmember.PhotoCount = photos.Count;
                        for (int l = 0; l < photos.Count; l++)
                        {
                            if (photos[l].MemberPhotoID == ((PhotoResultItem)resultlist[i]).MemberPhotoID)
                            {

                                gmember.ThumbWebPath = MemberPhotoDisplayManager.Instance.GetPhotoDisplayURL(g.Member, member, g.Brand, photos[l],
                                                               PhotoType.Thumbnail, PrivatePhotoImageType.Thumb,
                                                               NoPhotoImageType.Thumb, false, true);
                                gmember.FileWebPath = MemberPhotoDisplayManager.Instance.GetPhotoDisplayURL(g.Member, member, g.Brand, photos[l],
                                                               PhotoType.Thumbnail, PrivatePhotoImageType.Thumb,
                                                               NoPhotoImageType.Thumb, false, true);

                                break;
                            }
                        }
                    }
                    ((PhotoResultItem)resultlist[i]).GalleryMember = gmember;
                }
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }


        protected virtual void GetPhotoGalleryMembers(ArrayList res, ICollection<ProfileHolder> profileList)
        {
            if (res == null || res.Count <= 0) return;

            var memberIDs = new ArrayList();

            foreach (PhotoSearch.ValueObjects.PhotoResultItem item in res)
            {
                memberIDs.Add(item.MemberID);
            }

            if (memberIDs.Count <= 0) return;

            var members = MemberSA.Instance.GetMembers(memberIDs, MemberLoadFlags.None);

            if (members == null) return;

            for (var i = 0; i < members.Count; i++)
            {
                profileList.Add(
                    new ProfileHolder
                    {
                        MyEntryPoint = BreadCrumbHelper.EntryPoint.PhotoGallery,
                        Ordinal = startRow + i,
                        Member = members[i] as Member.ServiceAdapters.Member
                    }
                );
            }
        }

        protected virtual SearchParameters GetKeywordSearchPreferences()
        {
            var cookie = Request.Cookies[Search.Constants.Session.CookieKeyName];

            return (cookie == null || cookie.Values[Search.Constants.Session.SearchPreferencesKey] == null) ?
                        String.Empty : cookie.Values[Search.Constants.Session.SearchPreferencesKey];
        }

        protected virtual Int32 GetCurrentPage()
        {
            Int32 page;

            return Int32.TryParse(Request["pg"], out page) ?
                page > 0 ?
                    page :
                    Search.Constants.Paging.DefaultPage :
                startRow / PageSize + 1;
        }

        protected virtual Int32 GetCurrentPage(SearchParameters parameters)
        {
            Int32 currentPage;

            if (Int32.TryParse(Request.Params["StartRow"], out currentPage) == false &&
                Int32.TryParse(Request.Params["Ordinal"], out currentPage) == false)
            {
                Int32.TryParse(parameters.Page, out currentPage);
            }

            return currentPage;
        }

        protected static Int32 GetFloor(Int32 ordinalValue, Int32 pageSize)
        {
            var nMinus1 = ordinalValue - 1;
            var floorValue = nMinus1 / pageSize;
            var timesPageSize = floorValue * pageSize;

            return timesPageSize;
        }

        protected virtual List<ProfileHolder> GetProfilesForReverseSearch30Results()
        {
            var profileList = new List<ProfileHolder>();
            MatchnetQueryResults searchResults = null;
            SetQuickSearchPreferences();
            var reverseSearchPrefs = ResultListHandler.CreateReverseSearchPreferences(g, OnlyPhotos);

            try
            {
                try
                {
                    searchResults = MemberSearchSA.Instance.Search(reverseSearchPrefs
                     , g.Brand.Site.Community.CommunityID
                     , g.Brand.Site.SiteID
                     , startRow - 1// startRow is 1-indexed and this search is 0-indexed
                     , PageSize
                     , g.Member == null ? Constants.NULL_INT : g.Member.MemberID
                     , SearchEngineType.FAST
                     , searchType
                     , SearchEntryPoint.MiniSearchProfilePage
                     , false
                     , false
                     , false);
                }
                catch (Exception fe)
                {
                    g.ProcessException(fe);
                }

                if (null == searchResults ||
                    searchResults.MatchesFound < ResultListHandler.GetReverseSearchMinimumResults(g))
                {
                    try
                    {
                        ResultListHandler.ConvertToLaxReverseSearchPreferences(reverseSearchPrefs);

                        searchResults = MemberSearchSA.Instance.Search(reverseSearchPrefs
                         , g.Brand.Site.Community.CommunityID
                         , g.Brand.Site.SiteID
                         , startRow - 1// startRow is 1-indexed and this search is 0-indexed
                         , PageSize
                         , g.Member == null ? Constants.NULL_INT : g.Member.MemberID
                         , SearchEngineType.FAST
                         , searchType
                         , SearchEntryPoint.MiniSearchProfilePage
                         , false
                         , false
                         , false);
                    }
                    catch (Exception se)
                    {
                        g.ProcessException(se);
                    }
                }

                GetMembers(searchResults, profileList);

                if (profileList.Count > 0 && includeNextPrevProfile)
                {
                    SetNextPrevMemberID(profileList);

                    //prev member
                    if (prevMemberID <= 0 && ordinal > 1)
                    {
                        searchResults = MemberSearchSA.Instance.Search(reverseSearchPrefs
                                                                       , g.Brand.Site.Community.CommunityID
                                                                       , g.Brand.BrandID
                                                                       , ordinal - 2
                                                                       , 1
                                                                       , g.Member == null
                                                                           ? Constants.NULL_INT
                                                                           : g.Member.MemberID);

                        if (searchResults != null && searchResults.ToArrayList().Count > 0)
                        {
                            prevMemberID = Convert.ToInt32(searchResults.ToArrayList()[0]);
                        }
                    }

                    //next member
                    if (nextMemberID <= 0 && ordinal < totalCount)
                    {
                        searchResults = MemberSearchSA.Instance.Search(g.QuickSearchPreferences
                                                                       , g.Brand.Site.Community.CommunityID
                                                                       , g.Brand.Site.SiteID
                                                                       , ordinal
                                                                       , 1
                                                                       , g.Member == null
                                                                           ? Constants.NULL_INT
                                                                           : g.Member.MemberID);

                        if (searchResults != null && searchResults.ToArrayList().Count > 0)
                        {
                            nextMemberID = Convert.ToInt32(searchResults.ToArrayList()[0]);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
            return profileList;
        }

        protected virtual List<ProfileHolder> GetProfilesForKeywordSearchResults()
        {
            var profileList = new List<ProfileHolder>();

            var preference = GetKeywordSearchPreferences();

            SetQuickSearchPreferences();

            int page = GetCurrentPage(preference);
            var results = KeywordSearcher.GetKeywordSearchResults(
                g.Member == null ? Constants.NULL_INT : g.Member.MemberID,
                    Int32.Parse(preference.AgeRange),
                    preference.Location,
                    String.Join(Search.Constants.Literals.Two, new[] { preference.Seeker, preference.Seeking }),
                    preference.Query,
                    GetFloor(page, PageSize),
                    PageSize, g.Brand.Site.Community.CommunityID);

            var memberIds = GetMemberIds(preference, ordinal, results);

            if (memberIds.Count < 1) return profileList;

            var members = MemberSA.Instance.GetMembers(memberIds, MemberLoadFlags.None);

            if (members == null) return profileList;

            for (var i = 0; i < members.Count; i++)
            {
                profileList.Add(
                    new ProfileHolder
                    {
                        MyEntryPoint = BreadCrumbHelper.EntryPoint.KeywordSearchResults,
                        Ordinal = startRow + i,
                        Member = members[i] as Member.ServiceAdapters.Member
                    }
                );
            }

            if (profileList.Count > 0 && includeNextPrevProfile)
            {
                SetNextPrevMemberID(profileList);

                //prev member
                if (prevMemberID <= 0 && ordinal > 1 && page > 1)
                {
                    results = KeywordSearcher.GetKeywordSearchResults(
                                g.Member == null ? Constants.NULL_INT : g.Member.MemberID,
                                Int32.Parse(preference.AgeRange),
                                preference.Location,
                                String.Join(Search.Constants.Literals.Two, new[] { preference.Seeker, preference.Seeking }),
                                preference.Query,
                                GetFloor(page - 1, PageSize),
                                PageSize, g.Brand.Site.Community.CommunityID);

                    if (results != null && results.getSearchResult.Length > 0)
                    {
                        prevMemberID = Convert.ToInt32((results.getSearchResult)[results.getSearchResult.Length - 1].memberId);
                    }
                }

                //next member
                if (nextMemberID <= 0 && ordinal < totalCount)
                {
                    results = KeywordSearcher.GetKeywordSearchResults(
                                g.Member == null ? Constants.NULL_INT : g.Member.MemberID,
                                Int32.Parse(preference.AgeRange),
                                preference.Location,
                                String.Join(Search.Constants.Literals.Two, new[] { preference.Seeker, preference.Seeking }),
                                preference.Query,
                                GetFloor(page + 1, PageSize),
                                PageSize, g.Brand.Site.Community.CommunityID);

                    if (results != null && results.getSearchResult.Length > 0)
                    {
                        nextMemberID = Convert.ToInt32((results.getSearchResult)[0].memberId);
                    }
                }
            }

            return profileList;
        }

        protected virtual ArrayList GetMemberIds(SearchParameters preference, int currentPage, searchResultResponse results)
        {
            ArrayList memberIds;

            if (IsTestData(preference.Query))
            {
                memberIds = new ArrayList(
                    SearchResultsTestData.GetTestMemberIds(
                        (currentPage / Search.Constants.Paging.DefaultPageSize) + 1
                    ).ToArray()
                );
                totalCount = SearchResultsTestData.Total;
            }
            else
            {
                memberIds = (results == null || results.getSearchResult.Length < 1) ?
                    new ArrayList() : new ArrayList(results.getSearchResult.Select(i => Int32.Parse(i.memberId)).ToArray());
                totalCount = results == null ? 0 : results.total;
            }

            return memberIds;
        }

        protected virtual Boolean IsTestData(String query)
        {
            return String.IsNullOrEmpty(query) == false && query.ToLower().Contains(Search.Constants.Controls.TestSearchTerm);
        }

        protected virtual List<ProfileHolder> GetProfilesForQuickSearchResults()
        {
            var profileList = new List<ProfileHolder>();
            MatchnetQueryResults searchResults;

            SetQuickSearchPreferences();

            try
            {
                searchResults = MemberSearchSA.Instance.Search(g.QuickSearchPreferences
                     , g.Brand.Site.Community.CommunityID
                     , g.Brand.Site.SiteID
                     , startRow - 1// startRow is 1-indexed and this search is 0-indexed
                     , PageSize
                     , g.Member == null ? Constants.NULL_INT : g.Member.MemberID
                     , SearchEngineType.FAST
                     , searchType
                     , SearchEntryPoint.MiniSearchProfilePage
                     , false
                     , false
                     , false);

                GetMembers(searchResults, profileList);

                if (profileList.Count > 0 && includeNextPrevProfile)
                {
                    SetNextPrevMemberID(profileList);

                    //prev member
                    if (prevMemberID <= 0 && ordinal > 1)
                    {
                        searchResults = MemberSearchSA.Instance.Search(g.QuickSearchPreferences
                         , g.Brand.Site.Community.CommunityID
                         , g.Brand.BrandID
                         , ordinal - 2
                         , 1);

                        if (searchResults != null && searchResults.ToArrayList().Count > 0)
                        {
                            prevMemberID = Convert.ToInt32(searchResults.ToArrayList()[0]);
                        }
                    }

                    //next member
                    if (nextMemberID <= 0 && ordinal < totalCount)
                    {
                        searchResults = MemberSearchSA.Instance.Search(g.QuickSearchPreferences
                         , g.Brand.Site.Community.CommunityID
                         , g.Brand.Site.SiteID
                         , ordinal
                         , 1);

                        if (searchResults != null && searchResults.ToArrayList().Count > 0)
                        {
                            nextMemberID = Convert.ToInt32(searchResults.ToArrayList()[0]);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
                searchResults = new MatchnetQueryResults(0);
            }

            return profileList;
        }

        protected virtual List<ProfileHolder> GetProfilesForAdvancedSearchResults()
        {
            var profileList = new List<ProfileHolder>();
            MatchnetQueryResults searchResults;

            SetAdvancedSearchPreferences();

            try
            {
                searchResults = MemberSearchSA.Instance.Search(g.AdvancedSearchPreferences
                     , g.Brand.Site.Community.CommunityID
                     , g.Brand.Site.SiteID
                     , startRow - 1// startRow is 1-indexed and this search is 0-indexed
                     , PageSize
                     , g.Member == null ? Constants.NULL_INT : g.Member.MemberID
                     , SearchEngineType.FAST
                     , searchType
                     , SearchEntryPoint.MiniSearchProfilePage
                     , false
                     , false
                     , false);

                GetMembers(searchResults, profileList);

                if (profileList.Count > 0 && includeNextPrevProfile)
                {
                    SetNextPrevMemberID(profileList);

                    //prev member
                    if (prevMemberID <= 0 && ordinal > 1)
                    {
                        searchResults = MemberSearchSA.Instance.Search(g.AdvancedSearchPreferences
                         , g.Brand.Site.Community.CommunityID
                         , g.Brand.Site.SiteID
                         , ordinal - 2
                         , 1);

                        if (searchResults != null && searchResults.ToArrayList().Count > 0)
                        {
                            prevMemberID = Convert.ToInt32(searchResults.ToArrayList()[0]);
                        }
                    }

                    //next member
                    if (nextMemberID <= 0 && ordinal < totalCount)
                    {
                        searchResults = MemberSearchSA.Instance.Search(g.AdvancedSearchPreferences
                         , g.Brand.Site.Community.CommunityID
                         , g.Brand.Site.SiteID
                         , ordinal
                         , 1);

                        if (searchResults != null && searchResults.ToArrayList().Count > 0)
                        {
                            nextMemberID = Convert.ToInt32(searchResults.ToArrayList()[0]);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
                searchResults = new MatchnetQueryResults(0);
            }

            return profileList;
        }

        protected virtual void GetMembers(MatchnetQueryResults searchResults, ICollection<ProfileHolder> profileList)
        {
            if (searchResults == null || searchResults.ToArrayList().Count <= 0) return;

            totalCount = searchResults.MatchesFound;

            var members = GetMembersFromResults(searchResults, g);

            if (members == null) return;

            for (var i = 0; i < members.Count; i++)
            {
                profileList.Add(
                    new ProfileHolder
                    {
                        MyEntryPoint = entryPoint,
                        Ordinal = startRow + i,
                        Member = members[i] as IMemberDTO
                    }
                    );
            }
        }

        protected virtual void SetQuickSearchPreferences()
        {
            if (!PrefsHaveValue(g.QuickSearchPreferences, "SearchOrderBy"))
            {
                g.QuickSearchPreferences["SearchOrderBy"] = ((Int32)Matchnet.Search.Interfaces.QuerySorting.JoinDate).ToString();
            }

            g.QuickSearchPreferences["DomainID"] = g.TargetCommunityID.ToString();

            if (!PrefsHaveValue(g.QuickSearchPreferences, "SearchTypeID"))
            {
                g.QuickSearchPreferences["SearchTypeID"] = g.Brand.Site.DefaultSearchTypeID.ToString();
            }

            if (!PrefsHaveValue(g.QuickSearchPreferences, "RegionID"))
            {
                g.QuickSearchPreferences["RegionID"] = g.Brand.Site.DefaultRegionID.ToString();
            }

            if (!PrefsHaveValue(g.QuickSearchPreferences, "GenderMask"))
            {
                // set the gender mask to M s M and M s F for all others
                if (g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.Glimpse)
                {
                    g.QuickSearchPreferences["GenderMask"] = (ConstantsTemp.GENDERID_MALE + ConstantsTemp.GENDERID_SEEKING_MALE).ToString();
                }
                else
                {
                    g.QuickSearchPreferences["GenderMask"] = (ConstantsTemp.GENDERID_MALE + ConstantsTemp.GENDERID_SEEKING_FEMALE).ToString();
                }
            }

            if (!PrefsHaveValue(g.QuickSearchPreferences, "MinAge"))
            {
                g.QuickSearchPreferences["MinAge"] = g.Brand.DefaultAgeMin.ToString();
            }

            if (!PrefsHaveValue(g.QuickSearchPreferences, "MaxAge"))
            {
                g.QuickSearchPreferences["MaxAge"] = g.Brand.DefaultAgeMax.ToString();
            }
            if (!PrefsHaveValue(g.QuickSearchPreferences, "Distance"))
            {
                g.QuickSearchPreferences["Distance"] = g.Brand.DefaultSearchRadius.ToString();
            }

            if (g.QuickSearchPreferences["CountryRegionID"] == string.Empty)
            {
                g.QuickSearchPreferences.Add("CountryRegionID", g.Brand.Site.DefaultRegionID.ToString());
            }

            // if this is a photo required site, modify the QuickSearchPreferences object to return members with photos only
            if (Convert.ToBoolean(RuntimeSettings.GetSetting("PHOTO_REQUIRED_SITE", g.Brand.Site.Community.CommunityID,
                    g.Brand.Site.SiteID, g.Brand.BrandID)))
            {
                g.QuickSearchPreferences["HasPhotoFlag"] = "1";
            }


            if (PrefsHaveValue(g.QuickSearchPreferences, "JDateReligion"))
            {
                if (Conversion.CInt(g.QuickSearchPreferences["JDateReligion"]) == Matchnet.Web.Framework.Search.Constants.QS_JewishOnly_Unchecked)
                {
                    g.QuickSearchPreferences["JDateReligion"] = "";

                }
            }

            if (Conversion.CInt(g.QuickSearchPreferences["SearchOrderBy"]) == (int)Matchnet.Search.Interfaces.QuerySorting.ColorCode)
            {
                g.QuickSearchPreferences.Add("ColorCode", g.Session.GetString("COLORCODE_SEARCH_MASK"));
            }
            else
            {
                g.QuickSearchPreferences.Add("ColorCode", "");
            }

            //pass flag to determine if search redesign is enabled
            if (BetaHelper.IsSearchRedesign30Enabled(g))
            {
                g.QuickSearchPreferences.Add("SearchRedesign30", "1");
            }
            else
            {
                g.QuickSearchPreferences.Add("SearchRedesign30", "0");
            }

        }

        /// <summary>
        /// This is really only useful if the user's session times out and ends up on the view profile page right after. Defaulting logic
        /// just in case for that.
        /// </summary>
        protected virtual void SetAdvancedSearchPreferences()
        {
            g.AdvancedSearchPreferences["DomainID"] = g.TargetCommunityID.ToString();

            if (!PrefsHaveValue(g.AdvancedSearchPreferences, "SearchTypeID"))
            {
                g.AdvancedSearchPreferences["SearchTypeID"] = g.Brand.Site.DefaultSearchTypeID.ToString();
            }

            if (!PrefsHaveValue(g.AdvancedSearchPreferences, "RegionID"))
            {
                g.AdvancedSearchPreferences["RegionID"] = g.Brand.Site.DefaultRegionID.ToString();
            }

            if (!PrefsHaveValue(g.AdvancedSearchPreferences, "GenderMask"))
            {
                // set the gender mask to M s M and M s F for all others
                if (g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.Glimpse)
                {
                    g.AdvancedSearchPreferences["GenderMask"] = (ConstantsTemp.GENDERID_MALE + ConstantsTemp.GENDERID_SEEKING_MALE).ToString();
                }
                else
                {
                    g.AdvancedSearchPreferences["GenderMask"] = (ConstantsTemp.GENDERID_MALE + ConstantsTemp.GENDERID_SEEKING_FEMALE).ToString();
                }
            }

            if (!PrefsHaveValue(g.AdvancedSearchPreferences, "MinAge"))
            {
                g.AdvancedSearchPreferences["MinAge"] = g.Brand.DefaultAgeMin.ToString();
            }

            if (!PrefsHaveValue(g.AdvancedSearchPreferences, "MaxAge"))
            {
                g.AdvancedSearchPreferences["MaxAge"] = g.Brand.DefaultAgeMax.ToString();
            }
            if (!PrefsHaveValue(g.AdvancedSearchPreferences, "Distance"))
            {
                g.AdvancedSearchPreferences["Distance"] = g.Brand.DefaultSearchRadius.ToString();
            }

            if (g.AdvancedSearchPreferences["CountryRegionID"] == string.Empty)
            {
                g.AdvancedSearchPreferences.Add("CountryRegionID", g.Brand.Site.DefaultRegionID.ToString());
            }

            //pass flag to determine if search redesign is enabled
            if (BetaHelper.IsSearchRedesign30Enabled(g))
            {
                g.AdvancedSearchPreferences.Add("SearchRedesign30", "1");
            }
            else
            {
                g.AdvancedSearchPreferences.Add("SearchRedesign30", "0");
            }
        }

        protected static bool PrefsHaveValue(ISearchPreferences prefs, string key)
        {
            var retVal = false;

            string myValue = prefs.Value(key);

            if (!string.IsNullOrEmpty(myValue) && Conversion.CInt(myValue) != Constants.NULL_INT)
            {
                retVal = true;
            }

            return (retVal);
        }

        protected void SetNextPrevMemberID(List<ProfileHolder> profileList)
        {
            if (profileList != null && profileList.Count > 0)
            {
                for (int i = 0; i < profileList.Count; i++)
                {
                    if (ordinal == profileList[i].Ordinal)
                    {
                        //prev
                        if (i > 0)
                        {
                            prevMemberID = profileList[i - 1].Member.MemberID;
                        }

                        //next
                        if (i < (profileList.Count - 1))
                        {
                            nextMemberID = profileList[i + 1].Member.MemberID;
                        }

                        break;
                    }
                }
            }
        }

        #endregion

    }
}
