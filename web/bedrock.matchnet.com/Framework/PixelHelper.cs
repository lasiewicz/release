﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace Matchnet.Web.Framework
{
    public class PixelHelper
    {
        public static bool FetchbackPixelVisible(int communityID, int siteID, string pageName, Matchnet.Member.ServiceAdapters.Member member)
        {
            if (member == null)
                return false;

            if (pageName == "RegistrationWelcome" || pageName == "RegistrationWelcome20" || pageName == "RegistrationWelcomeOnePage"
                || pageName == "Subscribe" || pageName == "SubscribeNew")
                return false;

            string settingValue = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("FETCHBACK_PIXEL_ON", communityID, siteID);
            bool enabled = false;

            if (settingValue != null && settingValue != string.Empty)
            {
                enabled = Convert.ToBoolean(settingValue);
            }

            return enabled;
        }

        public static Literal GetFetchbackPixel(Matchnet.Member.ServiceAdapters.Member member, int siteID)
        {
            string ret = string.Empty;
            if (member == null)
                return null;

            if (member.IsPayingMember(siteID))
            {
                // opt out
                ret = "<iframe src='http://pixel.fetchback.com/serve/fb/pdj?cat=&name=landing&sid=3991&fb_key=optout' scrolling='no' width='1' height='1' marginheight='0' marginwidth='0' frameborder='0'></iframe>";
            }
            else
            {
                // opt in
                ret = "<iframe src='http://pixel.fetchback.com/serve/fb/pdj?cat=&name=landing&sid=3991' scrolling='no' width='1' height='1' marginheight='0' marginwidth='0' frameborder='0'></iframe>";
            }

            Literal lit = new Literal();
            lit.Text = ret;

            return lit;
        }
    }
}
