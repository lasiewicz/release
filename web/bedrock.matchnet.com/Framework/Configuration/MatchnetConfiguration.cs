using System;
using System.Configuration;
using System.Diagnostics;
using System.Xml;

namespace Matchnet.Web.Framework.Configuration
{
	/// <summary>
	/// Summary description for MatchnetConfiguration.
	/// </summary>
	internal sealed class MatchnetConfiguration
	{
		private LocalizationConfiguration	_localizationConfiguration = new LocalizationConfiguration();

		public LocalizationConfiguration Localization
		{
			get { return _localizationConfiguration; }
		}

		internal static MatchnetConfiguration Settings
		{
			get
			{
				MatchnetConfiguration configuration = (MatchnetConfiguration) ConfigurationSettings.GetConfig("matchnetSettings");

				if (null == configuration)
				{
					throw new Exception("Config file is missing the \"matchnetSettings\" section.");
				}

				return configuration;
			}
		}

		internal MatchnetConfiguration(XmlNodeList configurationNodes)
		{
			foreach (XmlNode configurationNode in configurationNodes)
			{
				Debug.Assert(configurationNode.NodeType == XmlNodeType.Element);

				switch (configurationNode.Name)
				{
					case "localization":
						_localizationConfiguration = new LocalizationConfiguration(configurationNode);
						break;

					default:
						throw new ConfigurationException(string.Format("Unknown configuration element ({0}).", configurationNode.Name), configurationNode);
				}
			}
		}
	}
}
