using System;
using System.Configuration;
using System.Diagnostics;
using System.Xml;

namespace Matchnet.Web.Framework.Configuration
{
	/// <summary>
	/// Summary description for LocalizationConfiguration.
	/// </summary>
	internal sealed class LocalizationConfiguration
	{
		private int		_maxRecursionCount = 5;
		private bool	_showMissingResources = false;

		public int MaxRecursionCount
		{
			get { return _maxRecursionCount; }
			set { _maxRecursionCount = value; }
		}
		public bool ShowMissingResources
		{
			get { return _showMissingResources; }
			set { _showMissingResources = value; }
		}

		/// <summary>
		///		Create configuration using default values.
		/// </summary>
		internal LocalizationConfiguration() {}

		/// <summary>
		///		Create configuration based on values specified in the config file.
		/// </summary>
		/// <param name="localizationNode"></param>
		internal LocalizationConfiguration(XmlNode localizationNode)
		{
			XmlNodeList	matchingNodes;
			XmlElement	configElement;

			// retrieve Max Recursion Count
			matchingNodes = localizationNode.SelectNodes("maxRecursionCount");
			if (matchingNodes.Count > 1)
			{
				throw new ConfigurationException("Only one maxRecursionCount element may be specified.", localizationNode);
			}
			if (matchingNodes.Count == 1)
			{
				Debug.Assert(matchingNodes[0].NodeType == XmlNodeType.Element);
				configElement = (XmlElement) matchingNodes[0];

				if (null != configElement && null != configElement.InnerText && configElement.InnerText.Length > 0)
				{
					int configValue;

					// retrieve value, check integer
					try
					{
						configValue = Convert.ToInt32(configElement.InnerText);
					}
					catch (Exception ex)
					{
						throw new System.Configuration.ConfigurationException("maxRecursionCount must contain an integer value.", ex, localizationNode);
					}

					// make sure value is within reasonable limits
					if (configValue < 1 || configValue > 10)
					{
						throw new System.Configuration.ConfigurationException("maxRecursionCount must be between 1 and 10, inclusive.", localizationNode);
					}

					_maxRecursionCount = configValue;
				}
			}

			// retrieve Show Missing Resources setting
			matchingNodes = localizationNode.SelectNodes("showMissingResources");
			if (matchingNodes.Count > 1)
			{
				throw new ConfigurationException("Only one showMissingResources element may be specified.", localizationNode);
			}
			if (matchingNodes.Count == 1)
			{
				Debug.Assert(matchingNodes[0].NodeType == XmlNodeType.Element);
				configElement = (XmlElement) matchingNodes[0];

				if (null != configElement && null != configElement.InnerText && configElement.InnerText.Length > 0)
				{
					_showMissingResources = Convert.ToBoolean(configElement.InnerText);
				}
			}
		}
	}
}
