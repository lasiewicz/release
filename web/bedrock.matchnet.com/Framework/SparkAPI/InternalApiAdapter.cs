﻿using Matchnet.Member.ServiceAdapters.Interfaces;
using Matchnet.Web.Framework.Managers;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Web;

namespace Matchnet.Web.Framework.SparkAPI
{
    public class InternalApiAdapter
    {
        #region Singleton manager
        private static readonly InternalApiAdapter _Instance = new InternalApiAdapter();
        private InternalApiAdapter()
        {
            //utah api uses ssl and may be self-signed
            ServicePointManager.ServerCertificateValidationCallback += (sender, certificate, chain, sslPolicyErrors) => true;
        }
        public static InternalApiAdapter Instance
        {
            get { return _Instance; }
        }

        #endregion

        public Spark.Common.RestConsumer.V2.Models.OAuth2.OAuthTokens CreateApiAccessToken(ContextGlobal g, IMember member, int adminMemberId)
        {
            try
            {
                LogonManager logonManager = new LogonManager(g);
                int applicationId = logonManager.ApplicationID;
                string jsonBody = JsonConvert.SerializeObject(new { lifetime = "short", adminMemberId = adminMemberId });
                string internalApiUrl = ConfigurationManager.AppSettings["InternalApiUrl"];

                var restClient = new RestClient(internalApiUrl);
                var restRequest = new RestRequest("integration/createaccesstoken/{brandId}/{applicationId}/{memberId}", Method.POST);
                restRequest.AddHeader("Content-Type", "application/json");
                restRequest.AddParameter("brandId", g.Brand.BrandID, ParameterType.UrlSegment);
                restRequest.AddParameter("applicationId", applicationId, ParameterType.UrlSegment);
                restRequest.AddParameter("memberId", member.MemberID, ParameterType.UrlSegment);
                restRequest.AddParameter("application/json", jsonBody, ParameterType.RequestBody);

                var response = restClient.Execute(restRequest);
                g.LoggingManager.LogInfoMessage("CreateApiAccessToken", string.Format("Called Internal API to create access token. MemberId: {0}, AdminId: {1}, BrandId: {2}, Internal Api Url: {3}, Response: {4}", member.MemberID, adminMemberId, g.Brand.BrandID, internalApiUrl, response.Content));
                Spark.Common.RestConsumer.V2.Models.OAuth2.OAuthTokens oAuthTokens = JsonConvert.DeserializeObject<Spark.Common.RestConsumer.V2.Models.OAuth2.OAuthTokens>(response.Content);

                return oAuthTokens;
            }
            catch (Exception ex)
            {
                g.LoggingManager.LogException(ex, member, g.Brand, "InternalApiAdapter");
            }

            return null;
        }
    }
}