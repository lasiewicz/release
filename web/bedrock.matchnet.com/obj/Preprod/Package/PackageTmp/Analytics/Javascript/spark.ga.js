﻿spark.ga = (function () {
	var self = {};
	var global = {};

	//self.tid = 'UA-342787-1';

	// Tracking Events
	self.trackEventWith4Params = function (category, action, label, value) {
		ga('send', 'event', category, action, label, value);  // value is a number.
	};

	self.trackEventWith3Params = function (category, action, label) {
		ga('send', 'event', category, action, label);
	};

	self.trackEventWith2Params = function (category, action) {
		ga('send', 'event', category, action);
	};

	self.trackEventWithParams = function (category, action, params) {
		ga('send', 'event', category, action, params);
	};

	// Tracking Pages
	self.trackPageByDefault = function () {
		ga('send', 'pageview');
	};

	self.trackPageWithPath = function (path) {
		ga('send', 'pageview', path);
	};

	self.trackPageWith2Params = function (path, title) {
		ga('send', 'pageview', {
			'page': path,
			'title': title
		});
	};

	self.getAllTrackers = function () {
		if (typeof gaTracker != "undefined") {
			return gaTracker.getAll();
		} else if (typeof ga != "undefined") {
			return ga.getAll();
		}
		if (typeof _gaq != "undefined") {
		    getClientIDByTackerID_ga
			_gaq.push(function () {
				return _gat._getTrackers(); // Array of all trackers
			});
		}
	};

	self.getClientIDByTackerID_ga = function (trackerId) {
		//get default tracker ID or param
		trackerId = trackerId === undefined ? self.tid : trackerId;

		var trackers = spark.ga.getAllTrackers();

		for (var i = 0; i < trackers.length; ++i) {
			var tracker = trackers[i];
			//console.log("tracker :: ", i, tracker);

			var universal = (tracker.get('name') == "" ? "(unnamed)" : tracker.get('name'));
			universal = universal + " - " + tracker.get('trackingId');
			//console.log('self.tid: ', self.tid);
			//console.log('GA clientId: ', tracker.get('clientId'));
			//console.log("Universal: ", universal);

			if (universal.indexOf(self.tid) > -1) {
				//console.log('universal (tid),  tracker.getclientId: ', universal, tracker.get('clientId'));
				return tracker.get('clientId');
			}
		}
	};

	self.getClientIDByTackerID_gaTracker = function (trackerId) {
		//get default tracker ID or param
		trackerId = trackerId === undefined ? self.tid : trackerId;

		var trackers = spark.ga.getAllTrackers();

		for (var i = 0; i < trackers.length; ++i) {
			var tracker = trackers[i];
			var universal = (tracker.get('name') == "" ? "(unnamed)" : tracker.get('name'));
			universal = universal + " - " + tracker.get('trackingId');
			//console.log("Universal #" + (i + 1) + ": " + universal);

			if (universal.indexOf(self.tid) > -1) {
				return tracker.get('clientId');
			}
		}
	};

	self.getClientIDByTackerID_gaq = function (trackerId) {
		//get default tracker ID or param
		trackerId = trackerId === undefined ? self.tid : trackerId;

		var trackers = spark.ga.getAllTrackers();

		for (var i = 0; i <= trackers.length; i++) {
			try {
				if (trackers[i]._getName().length > 0) {
					//console.log("Classic #" + (i + 1) + ": " + trackers[i]._getName() + " - " + trackers[i]._getAccount());
				} else {
					//console.log("Classic #" + (i + 1) + ": (unnamed) - " + trackers[i]._getAccount());
				}
			} catch (e) { }
		}
	}

	self.trackPageSuccessLogin = function (data, ga_clientId) {
		var params = {
			'tid': self.tid,
			't': 'pageview',
			'cid': ga_clientId,
			'dp': '/login/success',
			'cd3': data.memberId,
			'cd4': data.gender,
			'cd5': data.seekingGender,
			'cd6': data.age,
			'cd7': data.maritalStatus,
			'cd8': data.location,
			'cd15': data.isPayingMember,
			'cd10': data.jDateReligion,
			'cd11': data.educationLevel,
			'cd12': data.jDateEthnicity,
			'cd13': data.incomeLevel,
			'cd14': data.occupation,
			'cd9': data.country

		};
		var sendParams = jQuery.param(params);

		jQuery.ajax({
			url: 'http://www.google-analytics.com/collect?v=1&' + sendParams,
			dataType: 'json',
			crossDomain: true,
			success: function (data) {
				//
			}
		});
	};

	return self;
})();