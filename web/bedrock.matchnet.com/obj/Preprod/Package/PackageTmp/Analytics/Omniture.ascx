<%@ Control Language="c#" AutoEventWireup="false" CodeBehind="Omniture.ascx.cs" Inherits="Matchnet.Web.Analytics.Omniture"
    TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<asp:PlaceHolder ID="PlaceHolderOmniture" runat="server">
    <script type="text/javascript">
        var s_account = "<%=S_AccountName%>"
    </script>
    <asp:PlaceHolder ID="PlaceHolderMbox" runat="server" Visible="false">
        <script type="text/javascript" src="/Analytics/Javascript/mbox.js"></script>
    </asp:PlaceHolder>
    <script type="text/javascript" src="/Analytics/Javascript/Omniture.js"></script>
    <script type="text/javascript"><!--2
    var disableFireOmnitureCode = <%=DisableFireOmnitureCode.ToString().ToLower() %>;

    function PopulateS(clearValue) {
        s.linkInternalFilters = "<%=S_LinkInternalFilters%>";
        /*Traffic Variables*/
        s.pageName = "<%=PageName%>";
        s.server = "<%=Server%>";
        s.channel="";
        s.pageType="";
        s.referrer = "<%=Referrer%>";
        s.pageURL = "<%=PageURL%>";
        s.prop1 = (clearValue) ? "" : "<%=Prop1%>";
        s.prop4 = (clearValue) ? "" : "<%=Prop4%>";
        s.prop7 = (clearValue) ? "" : "<%=Prop7%>";
        s.prop6 = (clearValue) ? "" : "<%=Prop6%>";
        s.prop8 = (clearValue) ? "" : "<%=Prop8%>";
        s.prop9 = (clearValue) ? "" : "<%=Prop9%>";
        s.prop10 = (clearValue) ? "" : "<%=Prop10%>";
        s.prop11 = "";
        s.prop12 = "";
        s.prop13 = "";
        s.prop14 = (clearValue) ? "" : "<%=Prop14%>";
        s.prop15 = "";
        s.prop16 = "";
        s.prop17 = (clearValue) ? "" : "<%=Prop17%>";
        s.prop18 = (clearValue) ? "" : "<%=Prop18%>";
        s.prop19 = (clearValue) ? "" : "<%=Prop19%>";
        s.prop20 = (clearValue) ? "" : "<%=Prop20%>";
        s.prop21 = (clearValue) ? "" : "<%=Prop21%>";
        s.prop22 = (clearValue) ? "" : "<%=Prop22%>";
        s.prop23 = (clearValue) ? "" : "<%=Prop23%>";
        s.prop24 = (clearValue) ? "" : "<%=Prop24%>";
        s.prop27 = (clearValue) ? "" : "<%=Prop27%>";
        s.prop29 = (clearValue) ? "" : "<%=Prop29%>";
        s.prop30 = (clearValue) ? "" : "<%=Prop30%>";
        s.prop31 = (clearValue) ? "" : "<%=Prop31%>";
        s.prop32 = (clearValue) ? "" : "<%=Prop32%>";
        s.prop33 = (clearValue) ? "" : "<%=Prop33%>";
        s.prop35 = (clearValue) ? "" : "<%=Prop35%>";
        s.prop36 = (clearValue) ? "" : "<%=Prop36%>";
        s.prop38 = (clearValue) ? "" : "<%=Prop38%>";
        s.prop40 = "<%=Prop40%>";
        s.prop45 = (clearValue) ? "" : "<%=Prop45%>";
        s.prop46 = (clearValue) ? "" : "<%=Prop46%>";
        s.prop55 = (clearValue) ? "" : "<%=Prop55%>";
        s.prop75 = (clearValue) ? "" : "<%=Prop75%>";
        /* E-commerce Variables */
        s.state = "";
        s.zip = "";
        s.events = (clearValue) ? "" : "<%=Events%>";
        s.products = (clearValue) ? "" : "<%=Products%>";
        s.purchaseID = (clearValue) ? "" : "<%=PurchaseID%>";
        s.campaign = (clearValue) ? "" : "<%=PRM%>";
        s.eVar1 = (clearValue) ? "" : "<%=Evar1%>";
        s.eVar2 = (clearValue) ? "" : "<%=Evar2%>";
        s.eVar3 = (clearValue) ? "" : "<%=Evar3%>";
        s.eVar4 = (clearValue) ? "" : "<%=Evar4%>";
        s.eVar5 = (clearValue) ? "" : "<%=Evar5%>";
        s.eVar6 = (clearValue) ? "" : "<%=Evar6%>";
        s.eVar7 = (clearValue) ? "" : "<%=SR_ID%>";
        s.eVar8 = (clearValue) ? "" : "<%=Evar8%>";
        s.eVar9 = (clearValue) ? "" : "<%=Evar9%>";
        s.eVar10 = (clearValue) ? "" : "<%=Evar10%>";
        s.eVar11 = (clearValue) ? "" : "<%=PRM%>";
        s.eVar12 = (clearValue) ? "" : "<%=EID%>";
        s.eVar13 = (clearValue) ? "" : "<%=LGID%>";
        s.eVar14 = (clearValue) ? "" : "<%=Evar14%>";
        s.eVar15 = (clearValue) ? "" : "<%=Evar15%>";
        s.eVar16 = (clearValue) ? "" : "<%=Evar16%>";
        s.evar17 = (clearValue) ? "" : "<%=Evar17%>";
        s.eVar20 = (clearValue) ? "" : "<%=Evar20%>";
        s.eVar22 = (clearValue) ? "" : "<%=Evar22%>";
        s.eVar25 = (clearValue) ? "" : "<%=Evar25%>";
        s.eVar26 = (clearValue) ? "" : "<%=Evar26%>";
        s.eVar30 = (clearValue) ? "" : "<%=LP_ID%>";
        s.eVar32 = (clearValue) ? "" : "<%=Evar32%>";
        s.eVar33 = (clearValue) ? "" : "<%=Evar33%>";
        s.eVar34 = (clearValue) ? "" : "<%=Evar34%>";
        s.eVar40 = (clearValue) ? "" : "<%=TB_Link%>";
        s.eVar29 = (clearValue) ? "" : "<%=Evar29%>";
        s.eVar27 = (clearValue) ? "" : "<%=Evar27%>";
        s.eVar28 = (clearValue) ? "" : "<%=Evar28%>";
        s.eVar31 = (clearValue) ? "" : "<%=Evar31%>";
        s.eVar35 = (clearValue) ? "" : "<%=Evar35%>";
        s.eVar36 = "<%=Evar36%>";
        s.eVar40 = (clearValue) ? "" : "<%=Evar40%>";
        s.eVar41 = (clearValue) ? "" : "<%=Evar41%>";
        s.eVar42 = (clearValue) ? "" : "<%=Evar42%>";
	s.eVar43 = (clearValue) ? "" : "<%=Evar43%>";
        s.eVar44 = (clearValue) ? "" : "<%=Evar44%>";
        s.eVar45 = (clearValue) ? "" : "<%=Evar45%>";
        s.eVar46 = (clearValue) ? "" : "<%=Evar46%>";
        s.eVar47 = (clearValue) ? "" : "<%=Evar47%>";
        s.eVar48 = (clearValue) ? "" : "<%=Evar48%>";
        s.eVar50 = (clearValue) ? "" : "<%=Evar50%>";
        s.eVar55 = "<%=Evar55%>";
        s.eVar56 = "<%=Evar56%>";
        s.eVar57 = (clearValue) ? "" : "<%=Evar57%>";
        s.eVar64 = (clearValue) ? "" : "<%=Evar64%>";
        s.eVar65 = (clearValue) ? "" : "<%=Evar65%>";
        s.eVar67 = (clearValue) ? "" : "<%=Evar67%>";

        <%=GetTNTVesrionSet()%>
        }

PopulateS(false);

var originalOmniturePageName = s.pageName;
var tempOmniturePageNameSuffix = "<%=TempPageNameSuffix%>";

if (tempOmniturePageNameSuffix != "") {
    s.pageName = s.pageName + ' - ' + tempOmniturePageNameSuffix;
}
/************* DO NOT ALTER ANYTHING BELOW THIS LINE ! **************/

if(!disableFireOmnitureCode) {
    var s_code=s.t();
    if(s_code) document.write(s_code);
}

if (tempOmniturePageNameSuffix != "") {
    s.pageName = originalOmniturePageName;
}
    //-->

    </script>

<script language="JavaScript" type="text/javascript"><!--
    if (navigator.appVersion.indexOf('MSIE') >= 0) document.write(unescape('%3C') + '\!-' + '-')
//--></script>
<noscript>
    <img src="http://sparknetworks.112.2o7.net/b/ss/sparkjdatecom/1/H.24.1--NS/0" height="1"
        width="1" border="0" alt="" /></noscript><!--/DO NOT REMOVE/-->
<!-- End SiteCatalyst code version: H.24.1. -->
</asp:PlaceHolder> 
