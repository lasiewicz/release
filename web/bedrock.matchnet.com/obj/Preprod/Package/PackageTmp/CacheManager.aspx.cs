using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Globalization;

namespace Matchnet.Web
{
	/*
	This page is called by the middle tier when it needs to expire a cache item.
	The dispatch is configured to let this page execute without following the framework
	page processing pipeline (session, GUI rendering, etc.)
	*/
	public class CacheManager : System.Web.UI.Page
	{
		private void Page_Load(object sender, System.EventArgs e)
		{
            //Check if webserver uses Membase cache
            bool useMembase = Convert.ToBoolean(RuntimeSettings.GetSetting(WebConstants.MEMBASE_SETTING_CONSTANT));
            
			string key = Request.QueryString[WebConstants.URL_PARAMETER_NAME_DELETEKEY];

            if(useMembase)
            {
                System.Diagnostics.EventLog.WriteEntry("WWW",string.Format("This server {0} uses membase and recieved synchronization call for key {1}", System.Environment.MachineName,(string.IsNullOrEmpty(key) ? "" : key)));
            }

			if (key != null)
			{
				Matchnet.Caching.Cache.Instance.Remove(key);
                //if (key.ToLower().StartsWith("cachedmemberaccess"))
                //    System.Diagnostics.EventLog.WriteEntry("WWW", "Bedrock CacheManager Removed: " + key);
				return;
			}

			string resourceFileName = Request.QueryString[WebConstants.URL_PARAMETER_NAME_RESOURCEFILENAME];
			string resourceFileFullPath = Request.QueryString[WebConstants.URL_PARAMETER_NAME_RESOURCEFILEFULLPATH];

			if (resourceFileName != null && resourceFileFullPath != null)
			{
				try 
				{
					LocalizedResourceSet.ResourceCache.Instance.RemoveFromCache(Server.UrlDecode(resourceFileName),Server.UrlDecode(resourceFileFullPath));
			
					Response.Write(string.Format("Successfully deleted resource cache for {0} at full path: {1}",resourceFileName,resourceFileFullPath));
				}
				catch (Exception ex) 
				{
					Response.Write(string.Format("Failed to delete resource cache for {0} at full path: {1}.  Error was: {2}",resourceFileName,resourceFileFullPath, ex.ToString()));
				}

				return;
			}

			string imageFileName = Request.QueryString[WebConstants.URL_PARAMETER_NAME_IMAGEFILENAME];

			if (imageFileName != null)
			{
				try 
				{
					ImageUrlCache.Instance.RemoveUrl(imageFileName);
			
					Response.Write("Successfully deleted image url cache for " + imageFileName + ".");
				}
				catch (Exception ex) 
				{
					Response.Write("Failed to delete image url cache for " + imageFileName + ".  Error was: " + ex.ToString());
				}
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
