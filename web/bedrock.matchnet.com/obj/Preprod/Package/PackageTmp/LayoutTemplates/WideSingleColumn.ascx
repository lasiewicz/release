﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WideSingleColumn.ascx.cs" Inherits="Matchnet.Web.LayoutTemplates.WideSingleColumn" %>
<%@ Register TagPrefix="mn" TagName="AdUnit" Src="/Framework/UI/Advertising/AdUnit.ascx" %>
<%@ Register TagPrefix="mn1" TagName="Footer20" Src="/Framework/Ui/Footer20.ascx" %>
<%@ Register TagPrefix="MNFramework" TagName="Right" Src="/Framework/Ui/Right.ascx" %>
<%@ Register TagPrefix="mn1" TagName="Head20" Src="/Framework/Ui/Head20.ascx" %>
<%@ Register TagPrefix="mn1" TagName="TopAuxNav" Src="/Framework/Ui/HeaderNavigation20/TopAuxNav.ascx" %>
<%@ Register TagPrefix="mn" TagName="Header" Src="/Framework/Ui/Header20.ascx" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="uc1" TagName="ExternalFeedback" Src="~/Framework/Ui/PageElements/ExternalFeedback.ascx" %>
<%@ Register TagPrefix="fb" TagName="API" Src="~/Framework/Facebook/FacebookAPI.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Feedback" Src="~/Framework/Ui/Feedback/Feedback.ascx" %>
<%@ Register TagPrefix="mn1" TagName="ActivitySupport" Src="/Framework/Ui/ActivitySupport.ascx" %>
<%@ Register TagPrefix="fp" TagName="ForcedPage" Src="/Applications/PremiumServices/Controls/ForcedPage.ascx" %>
<%@ Register tagPrefix="mn1"  tagName="LivePersonCustomVars" src="/Applications/LivePerson/CustomVariables.ascx"%>
<%@ Register TagPrefix="mn1" TagName="FooterMobileAnnouncement" Src="/Framework/Ui/PageAnnouncements/FooterMobileAnnouncement.ascx" %>
<%@ Register TagPrefix="uc1" TagName="TealiumScript" Src="~/Applications/Tealium/TealiumScript.ascx" %>
<!doctype html>
<!--[if lte IE 7]> <html class="ie ie7 lte9 lte8 lte7 no-js" xml:lang="<%=GetPageLang() %>" lang="<%=GetPageLang() %>" dir="<%=GetPageDirection() %>"> <![endif]--> 
<!--[if IE 8]> <html class="ie ie8 lte9 lte8 no-js" xml:lang="<%=GetPageLang() %>" lang="<%=GetPageLang() %>" dir="<%=GetPageDirection() %>"> <![endif]--> 
<!--[if IE 9]> <html class="ie ie9 lte9 no-js" xml:lang="<%=GetPageLang() %>" lang="<%=GetPageLang() %>" dir="<%=GetPageDirection() %>"> <![endif]--> 
<!--[if (gte IE 10)|!(IE)]><!--><html class="no-js" xml:lang="<%=GetPageLang() %>" lang="<%=GetPageLang() %>" dir="<%=GetPageDirection() %>"><!--<![endif]--> 
<mn1:Head20 id="Head20" runat="server" />
<%-- attention everyone who will work on this file, we don't want NotificationControlPlaceholder to be declared in WideReset.ascx.designer.cs but unfortunately VS does it automatically. It screws up Notifications, so  please see if there is a line: protected global::System.Web.UI.WebControls.PlaceHolder NotificationControlPlaceholder; in WideReset.ascx.designer.cs and comment it. Thanks --%>
<body runat="server" id="bdyMain">
     <uc1:TealiumScript runat="server" ID="TealiumScript" />
    <mn:Txt ID="txtGoogleTagManagerCode" runat="server" ResourceConstant="GOOGLE_TAG_MANAGER_CODE" Visible="False"/>
    <mn:Txt ID="txtDynamicYieldBodyCode" runat="server" ResourceConstant="TXT_DYNAMIC_YIELD_BODY_CODE" Visible="False" />
     <asp:PlaceHolder runat="server" Visible="false" ID="plcLivePerson">
        <mn:Txt ID="txtLivePersonBodyTag" runat="server" ResourceConstant="TXT_LIVEPERSON_BODY_TAG"  />
        <mn1:LivePersonCustomVars runat="server" ID="lpCustomVars"/>
    </asp:PlaceHolder>
<fb:API runat="server" ID="fbAPI" visible="false"></fb:API>
<asp:PlaceHolder ID="plcSage" Runat="server" />
<div id="site-container">
    <mn:AdUnit Visible="false" GAMAdSlot="overlay_1x1" Size="Overlay" runat="server" GAMPageMode="Auto" ID="adUnitOverlay"></mn:AdUnit>

    <form id="Form1" method="post" runat="server">
    <asp:PlaceHolder runat="server" ID="plcBlockUIContent" />
    <div id="min-max-container">
    
        <mn1:TopAuxNav id="topAuxNav" runat="server" />
        <div id="content-container" class="one-column your-profile clearfix">
      
	        <mn:Header runat="server" id="Header" />
            
            <!--Activity Support for IM notifier and session-->
            <asp:PlaceHolder runat="server" ID="plcActivitySupport">
                <mn1:ActivitySupport runat="server" id="ActivitySupport1"></mn1:ActivitySupport>
            </asp:PlaceHolder>

	        <div class="wrapper">
	            <div id="content-main" <asp:Literal id="litCntMain" runat="server" />>
				    <asp:PlaceHolder ID="plcLeft" Runat="server" />	
				    <asp:PlaceHolder ID="notificationControlPlaceholder" Runat="server" EnableViewState="False" />
                    <asp:PlaceHolder ID="PageAnnouncementPlaceholder" runat="server" />
				    <asp:PlaceHolder ID="AppControlPlaceholder" Runat="server" />
        				  
				    <asp:PlaceHolder ID="plcRight" Runat="server" />
                    <asp:PlaceHolder ID="plcSubscribeBanner" Runat="server" />

                    <asp:PlaceHolder ID="phSubNowBannerContainer" runat="server" Visible="false">
                        <div id="SubNowTopBanner" class="ribbon-fold-both">
                            <asp:PlaceHolder ID="phSubNowTopBanner" runat="server"></asp:PlaceHolder>
                        </div>
                    </asp:PlaceHolder>
	   	

			    </div>
            </div>
		    <div style="clear: both"></div>    	    	
        </div>
        <asp:PlaceHolder ID="phTopLeaderboardAd" runat="server">
            <mn:AdUnit id="adLeaderboardHeaderTop"  runat="server" expandImageTokens="true" Size="LeaderboardHeaderAboveMenu" GAMAdSlot="top_728x90" GAMPageMode="Auto" />
        </asp:PlaceHolder>
    </div>
    <div id="footer">
        <div id="footer-container">
            <mn1:Footer20 ID="Footer20" Runat="server" />
        </div>
    </div>
    <uc1:Feedback ID="Feedback1" runat="server" Visible="false" />
    </form>
	
	<asp:PlaceHolder ID="plcPixels" Runat="server" />
	<asp:PlaceHolder ID="plcFetchbackPixel" Runat="server" />
			
	<%--checkIM--%>
	<asp:PlaceHolder ID="plcIM" Runat="server" />
	<asp:Literal ID="litPlanSurvey" Runat="server" />

    <script type="text/javascript">
        $j(document).ready(function(){init()});
    </script>
	 
	<mn:AdUnit id="adHomePageForced" Size="ForcedPage" visible=false runat="server" />
	<fp:ForcedPage DisplayCount=1 SpotlightResourceConstant="TXT_FORCED_SPOTLIGHT_PHOTOS" TimedResourceConstant="TXT_FORCED_TIMED_PROMOTION" ForcedPageCondition=6 AppID=-1 runat="server" id="fpTimedPromoAndSpotlight" />
	<uc1:ExternalFeedback ID="ExternalFeedback1" runat="server" OutputTag="Body" />

    <%--Wallpaper Adunit--%>
    <mn:AdUnit Visible="false" GAMAdSlot="wallpaper_1x1" Size="Wallpaper" runat="server" GAMPageMode="Auto" ID="adUnitWallpaper" ></mn:AdUnit>
    <asp:PlaceHolder ID="plcFooterMobile" runat="server" Visible="false" >
       <mn1:FooterMobileAnnouncement runat="server" />
    </asp:PlaceHolder>
</div>
<mn:Txt ID="txtGoogleRemarketingCode" runat="server" ResourceConstant="JAVASCRIPT_GOOGLE_REMARKETING_TAG"/>
<mn:Txt ID="txtGoogleRemarketingCodeFree" runat="server" ResourceConstant="JAVASCRIPT_GOOGLE_REMARKETING_TAG_FREE" Visible="False"/>
<mn:Txt ID="txtGoogleRemarketingCodeSubscriber" runat="server" ResourceConstant="JAVASCRIPT_GOOGLE_REMARKETING_TAG_SUBSCRIBER" Visible="False"/>
<mn:Txt ID="txtFacebookRemarketingCode" runat="server" ResourceConstant="JAVASCRIPT_FACEBOOK_REMARKETING_TAG" Visible="False"/>
</body>
</html>