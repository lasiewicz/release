namespace Matchnet.Web.LayoutTemplates
{
	using System;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;

	using Matchnet.Web.Framework;

	/// <summary>
	///		Summary description for Simple.
	/// </summary>
	public class LandingPage : Lib.LayoutTemplateBase
	{
		protected System.Web.UI.WebControls.PlaceHolder plcSage;
        protected System.Web.UI.WebControls.PlaceHolder plcFetchbackPixel;

		private void Page_Init(object sender, System.EventArgs e)
		{
			// Add the Sage Analytics control.
			Lib.Util util = new Lib.Util();
			plcSage.Controls.Add(util.RenderAnalytics(base.SaveSession));

			// Omniture Analytics - Use Sage Place Holder for now.
			if (Convert.ToBoolean(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ANALYTICS_OMNITURE", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID)))
			{
				Analytics.Omniture omniture = (Analytics.Omniture) Page.LoadControl("/Analytics/Omniture.ascx");
				plcSage.Controls.Add(omniture);
				g.AnalyticsOmniture = omniture;
			}

            // Fetchback pixel
            if (PixelHelper.FetchbackPixelVisible(g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.AppPage.ControlName, g.Member))
            {
                plcFetchbackPixel.Controls.Add(PixelHelper.GetFetchbackPixel(g.Member, g.Brand.Site.SiteID));
            }
		}

		private void Page_Load(object sender, EventArgs e)
		{
			base.BustaFrames();
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Init += new System.EventHandler(this.Page_Init);
			this.Load += new EventHandler(this.Page_Load);
		}
		#endregion
	}
}
