namespace Matchnet.Web.LayoutTemplates
{
	using System;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;

	using Matchnet.Web.Framework;
	using Matchnet.Web.Framework.PagePixels;
	using Matchnet.Web.Applications.Search;
    using Matchnet.Web.Framework.Ui;

	/// <summary>
	///		Summary description for Standard.
	/// </summary>
	public class Standard : Lib.LayoutTemplateBase
	{
		protected Matchnet.Web.Framework.Ui.Header20 Header;
		protected System.Web.UI.WebControls.PlaceHolder plcIM;
		protected System.Web.UI.WebControls.PlaceHolder plcPixels;
		protected System.Web.UI.WebControls.PlaceHolder plcSage;
        protected System.Web.UI.WebControls.PlaceHolder plcFetchbackPixel;
        protected System.Web.UI.WebControls.PlaceHolder plcBlockUIContent;


		protected Boolean allowFramesFlag = false;

		protected Literal litPlanSurvey;

        protected Txt txtStaticHeader;
        protected Txt txt3rdPartyBodyCode;

		private void Page_Init(object sender, System.EventArgs e)
		{			
			// Add ExitPopup if applicable.
			Matchnet.Web.Framework.Popup popup = GalleryExitPopup.CreatePopup(g);
			if (popup != null)
				_g.Page.Controls.Add(popup);

			// Add the PagePixel control.
			plcPixels.Controls.Add(new PagePixelControl());

			// Add the Sage Analytics control.
			Lib.Util util = new Lib.Util();
			plcSage.Controls.Add(util.RenderAnalytics(base.SaveSession));

			// Omniture Analytics - Use Sage Place Holder for now.
			if (Convert.ToBoolean(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ANALYTICS_OMNITURE", _g.Brand.Site.Community.CommunityID, _g.Brand.Site.SiteID)))
			{
				Analytics.Omniture omniture = (Analytics.Omniture) Page.LoadControl("/Analytics/Omniture.ascx");
				plcSage.Controls.Add(omniture);
				_g.AnalyticsOmniture = omniture;
			}

            // Fetchback pixel
            if (PixelHelper.FetchbackPixelVisible(g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.AppPage.ControlName, g.Member))
            {
                plcFetchbackPixel.Controls.Add(PixelHelper.GetFetchbackPixel(g.Member, g.Brand.Site.SiteID));
            }

			g.HeaderControl = Header;
			//Remove after plan survey is done
			GetPlanSurveyScript();
		
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			if (!allowFramesFlag)
				base.BustaFrames();

            if (Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ENABLE_TEMPLATE_VERTICAL_LINES", _g.Brand.Site.Community.CommunityID, _g.Brand.Site.SiteID).ToLower() == "true");
            {
                if (plcVerticalLine1 != null && plcVerticalLine2 != null)
                {
                    plcVerticalLine1.Visible = true;
                    plcVerticalLine2.Visible = true;
                }
             
            }

            if (g.IsSecureRequest())
            {
                txtStaticHeader.Visible = false;
                txt3rdPartyBodyCode.Visible = false;
            }
        }

        private void Page_PreRender(object sender, System.EventArgs e)
        {
            if (plcBlockUIContent!= null && plcBlockUIContent.Controls.Count > 0)
            {
                Literal lit = new Literal();
                lit.Text = "<div id=\"blockUI\" style=\"display: none;\"></div>";
                plcBlockUIContent.Controls.Add(lit);
            }
        }

//Remove after plan survey is done
		private  void GetPlanSurveyScript()
		{
			string ret="";	
			try
			{
				int siteid=g.Brand.Site.SiteID;
				int communityid=g.Brand.Site.Community.CommunityID;
				int brandid=g.Brand.BrandID;
				if(g.Member==null)
					return;

				string surveystr=Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("PLAN_SURVEY_FLAG",communityid,siteid,brandid);

				bool surveyflag=Conversion.CBool(surveystr,false);

				if(surveyflag)
				{
					ret=g.GetResource("PLAN_SURVEY");
				}

				litPlanSurvey.Text=ret;
			}
			catch(Exception ex)
			{}

		}
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
			this.Init += new System.EventHandler(this.Page_Init);
		    this.PreRender     += new System.EventHandler(this.Page_PreRender);

		}
		#endregion
	}
}
