<%@ Register TagPrefix="MNFramework" TagName="Head" Src="/Framework/Ui/Head20.ascx" %>
<%@ Control Language="c#" AutoEventWireup="false" Codebehind="LandingPage.ascx.cs" Inherits="Matchnet.Web.LayoutTemplates.LandingPage" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>

<MNFramework:Head ID="Head" Runat="server" />

<body leftmargin="0" topmargin="0" rightmargin="0" marginwidth="0" marginheight="0" text="#000000" runat="server" id="BodyTag" dir="<asp:Literal id=litDirection runat=server/>" style="background:;">
	<asp:PlaceHolder ID="plcSage" Runat="server" />
	<form method="post" runat="server">
		<asp:PlaceHolder ID="NotificationControlPlaceholder" Runat="server" />
		<asp:PlaceHolder ID="AppControlPlaceholder" Runat="server" />
	</form>
	<asp:PlaceHolder ID="plcFetchbackPixel" Runat="server" />
</body>