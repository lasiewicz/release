﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Ui;

using Matchnet.Web.Framework.PagePixels;
using Matchnet.Web.Applications.Search;
using Matchnet.Web.Applications.MemberProfile.ProfileTabs30;
using Matchnet.Web.Applications.MemberProfile.ProfileTabs30.XMLProfileTabConfig;
using Matchnet.Web.Applications.MemberProfile;

namespace Matchnet.Web.LayoutTemplates
{
    public partial class WidePopup : Lib.LayoutTemplateBase
    {
        bool allowFramesFlag = false;
        private bool renderAdHeaderRightIFrameIDToJS = false;

        public override Matchnet.Web.Framework.Image ImageLogo
        {
            get
            {
                return imgLogo;
            }
        }

        private void Page_Init(object sender, System.EventArgs e)
        {
            //for dynamic css classes
            string cssformat = "page-{0} sub-page-{1}";
            string[] appname = _g.AppPage.App.Path.Split('/');
            int upperInd = appname.Length - 1;
            string app = appname[upperInd];
            string subpage = _g.AppPage.ControlName.ToLower();


            bdyMain.Attributes["class"] = String.Format(cssformat, app, subpage);
            g.RightNavigationVisible = false;
            g.LayoutTemplateBase = this;

            plcPixels.Controls.Add(new PagePixelControl());

            // Add the Sage Analytics control.
            Lib.Util util = new Lib.Util();
            plcSage.Controls.Add(util.RenderAnalytics(base.SaveSession));

            //// Omniture Analytics - Use Sage Place Holder for now.
            if (Convert.ToBoolean(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ANALYTICS_OMNITURE", _g.Brand.Site.Community.CommunityID, _g.Brand.Site.SiteID)))
            {
                Analytics.Omniture omniture = (Analytics.Omniture)Page.LoadControl("/Analytics/Omniture.ascx");
                plcSage.Controls.Add(omniture);
                _g.AnalyticsOmniture = omniture;
            }

            // Fetchback pixel
            if (PixelHelper.FetchbackPixelVisible(g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.AppPage.ControlName, g.Member))
            {
                plcFetchbackPixel.Controls.Add(PixelHelper.GetFetchbackPixel(g.Member, g.Brand.Site.SiteID));
            }

            _g.LayoutVersion = WebConstants.LayoutVersions.versionWide;
            g.RightNavigationVisible = true;
            if(g.AppPage.ID==(int) WebConstants.PageIDs.CupidQuest || g.AppPage.App.ID== (int)WebConstants.APP.ColorCode)
                g.RightNavigationVisible = false;

            if (g.AppPage.ID == (int)WebConstants.PageIDs.ViewProfile)
            {
                string host = HttpContext.Current.Request.Url.Host;

                string url;
                if (g.Member != null)
                    url = "http://" + host + "/Applications/Home/Default.aspx";
                else
                    url = "http://" + host + "/";

                imgLogo.NavigateUrl = url;

                g.RightNavigationVisible = false;
                this.phFooter.Visible = true;
                this.phTopLeaderboardAd.Visible = true;
                this.adLeaderboardHeaderTop.SetTopLeaderBoardMenuIFrameSupport();

                if (ProfileUtility.HideProfile30RightAd(g))
                {
                    this.adSquareRight.Disabled = true;
                }
                else
                {
                    this.phRightSquareAd.Visible = true;
                    this.adSquareRight.GAMAdSlot = "profile_basics_right_300x250";
                    this.adSquareRight.GAMPageMode = Matchnet.Web.Framework.Ui.Advertising.AdUnit.GAMPageModeType.None;
                    this.adSquareRight.GAMIframe = true;
                    this.adSquareRight.GAMIframeHeight = 250;
                    this.adSquareRight.GAMIframeWidth = 300;
                    this.renderAdHeaderRightIFrameIDToJS = true;

                    if (ProfileUtility.IsDisplayingProfile30(g))
                    {
                        ProfileTabConfigs tabConfigs = ProfileUtility.GetProfileTabConfigs(g.Brand.Site.SiteID, g);
                        if (tabConfigs != null && tabConfigs.ProfileTabConfigList != null && tabConfigs.ProfileTabConfigList.Count > 0)
                        {
                            ProfileTabEnum activeTab = ViewProfileTabUtility.DetermineActiveTab(tabConfigs.ProfileTabConfigList[0].ProfileTabType);
                            if (activeTab != ProfileTabEnum.None)
                            {
                                ProfileTabConfig tabConfig = ProfileUtility.GetProfileTabConfig(g.Brand.Site.SiteID, g, activeTab);
                                if (tabConfig != null && !String.IsNullOrEmpty(tabConfig.GAMAdSlotRight))
                                {
                                    this.adSquareRight.GAMAdSlot = tabConfig.GAMAdSlotRight;
                                }
                            }
                        }

                        if (ProfileUtility.IsHideProfile30RightAdSlotEnabled(g))
                        {
                            this.adSquareRight.ShowRightAdMessageForNonSubs();
                        }

                    }
                }
            }

            if (g.RightNavigationVisible)
            {
                allowFramesFlag = true;
                if (g.RightNavControl == null)
                {
                    plcRight.Controls.Add(LoadControl("/Framework/Ui/Right.ascx"));
                }
                else
                {
                    plcRight.Controls.Add(g.RightNavControl.Control);
                }
            }
            //Remove after plan survey is done

            // _g.FooterControl.Visible = false;

            if (g.Brand.Site.LanguageID == (int)Matchnet.Language.Hebrew)
            {
                bdyMain.Attributes["dir"] = base.Direction;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!allowFramesFlag)
                base.BustaFrames();		
        }

        protected override void OnPreRender(EventArgs e)
        {
            if (this.renderAdHeaderRightIFrameIDToJS)
            {
                Page.ClientScript.RegisterClientScriptBlock(typeof(System.Web.UI.Page), "RightJSAd",
                       "<script type=\"text/javascript\">"
                       + "var rightAd300x250IFrameID = \"" + this.adSquareRight.AdIFrameID + "\";"
                       + "var rightAdProfilePopup = \"true\";"
                       + "</script>");

                //For profiles on popup, we'll hide right ad initially except for specific tabs
                Matchnet.Web.Applications.MemberProfile.ProfileTabEnum activeTab = Matchnet.Web.Applications.MemberProfile.ViewProfileTabUtility.DetermineActiveTab();
                if (activeTab != Matchnet.Web.Applications.MemberProfile.ProfileTabEnum.Basic
                    && activeTab != Matchnet.Web.Applications.MemberProfile.ProfileTabEnum.Lifestyle
                    && activeTab != Applications.MemberProfile.ProfileTabEnum.Essays
                    && activeTab != Applications.MemberProfile.ProfileTabEnum.MoreEssays
                    && activeTab != Applications.MemberProfile.ProfileTabEnum.TheDetails)
                {
                    Page.ClientScript.RegisterClientScriptBlock(typeof(System.Web.UI.Page), "HideRightJSAd",
                       "<script type=\"text/javascript\">"
                       + "jQuery(document).ready(function() {HidePopupDiv('" + this.adSquareRight.AdIFrameID + "');});"
                       + "</script>");
                }
            }

            base.OnPreRender(e);
        }

        protected string GetPageLang()
        {
            string lang = "en";
            if (g.Brand.Site.LanguageID == (int)Matchnet.Language.Hebrew)
            {
                lang = "he";
            }
            return lang;
        }

        protected string GetPageDirection()
        {
            string direction = "ltr";
            if (g.Brand.Site.LanguageID == (int)Matchnet.Language.Hebrew)
            {
                direction = base.Direction;
            }
            return direction;
        }

        public override void ShowFeedback(Matchnet.Web.Framework.Ui.Feedback.FeedbackHelper.FeedbackType feedbackType)
        {
            if (feedbackType == Framework.Ui.Feedback.FeedbackHelper.FeedbackType.BetaProfile)
                feedbackType = Framework.Ui.Feedback.FeedbackHelper.FeedbackType.BetaProfilePopup;

            Feedback1.FeedbackType = feedbackType;
            Feedback1.Visible = true;
        }
    }
}
