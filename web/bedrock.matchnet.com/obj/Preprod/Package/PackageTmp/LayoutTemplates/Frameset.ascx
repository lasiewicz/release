<%@ Control Language="c#" AutoEventWireup="false" Codebehind="Frameset.ascx.cs" Inherits="Matchnet.Web.LayoutTemplates.Frameset" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>

<%@ Register TagPrefix="MNFramework" TagName="Head" Src="/Framework/Ui/Head20.ascx" %>

<MNFramework:Head ID="Head" Runat="server" />

<asp:PlaceHolder ID="NotificationControlPlaceholder" Runat="server" />
<asp:PlaceHolder ID="AppControlPlaceholder" Runat="server" />
<asp:PlaceHolder ID="plcSage" Runat="server" />
<asp:PlaceHolder ID="plcFetchbackPixel" Runat="server" />