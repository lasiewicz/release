﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WideT.ascx.cs" Inherits="Matchnet.Web.LayoutTemplates.WideT" %>
<%@ Register TagPrefix="mn" TagName="AdUnit" Src="/Framework/UI/Advertising/AdUnit.ascx" %>
<%@ Register TagPrefix="mn1" TagName="Footer20" Src="/Framework/Ui/Footer20.ascx" %>
<%@ Register TagPrefix="mn1" TagName="Head20" Src="/Framework/Ui/Head20.ascx" %>
<%@ Register TagPrefix="mn1" TagName="TopAuxNav" Src="/Framework/Ui/HeaderNavigation20/TopAuxNav.ascx" %>
<%@ Register TagPrefix="mn" TagName="Header" Src="/Framework/Ui/Header20.ascx" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="fp" TagName="ForcedPage" Src="/Applications/PremiumServices/Controls/ForcedPage.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ExternalFeedback" Src="~/Framework/Ui/PageElements/ExternalFeedback.ascx" %>
<%@ Register TagPrefix="uc2" TagName="OmnidateBodyCode" Src="~/Applications/Omnidate/Controls/BodyCode.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Feedback" Src="~/Framework/Ui/Feedback/Feedback.ascx" %>
<%@ Register TagPrefix="fb" TagName="API" Src="~/Framework/Facebook/FacebookAPI.ascx" %>
<%@ Register TagPrefix="mn1" TagName="ActivitySupport" Src="/Framework/Ui/ActivitySupport.ascx" %>
<%@ Register tagPrefix="mn1"  tagName="LivePersonCustomVars" src="/Applications/LivePerson/CustomVariables.ascx"%>
<%@ Register TagPrefix="mn1" TagName="FooterMobileAnnouncement" Src="/Framework/Ui/PageAnnouncements/FooterMobileAnnouncement.ascx" %>
<%@ Register TagPrefix="uc1" TagName="TealiumScript" Src="~/Applications/Tealium/TealiumScript.ascx" %>
<!doctype html>
<!--[if lte IE 7]> <html class="ie ie7 lte9 lte8 lte7 no-js" xml:lang="<%=GetPageLang() %>" lang="<%=GetPageLang() %>" dir="<%=GetPageDirection() %>" xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://ogp.me/ns/fb#"> <![endif]--> 
<!--[if IE 8]> <html class="ie ie8 lte9 lte8 no-js" xml:lang="<%=GetPageLang() %>" lang="<%=GetPageLang() %>" dir="<%=GetPageDirection() %>" xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://ogp.me/ns/fb#"> <![endif]--> 
<!--[if IE 9]> <html class="ie ie9 lte9 no-js" xml:lang="<%=GetPageLang() %>" lang="<%=GetPageLang() %>" dir="<%=GetPageDirection() %>" xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://ogp.me/ns/fb#"> <![endif]--> 
<!--[if (gte IE 10)|!(IE)]><!--><html class="no-js" xml:lang="<%=GetPageLang() %>" lang="<%=GetPageLang() %>" dir="<%=GetPageDirection() %>" xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://ogp.me/ns/fb#"><!--<![endif]--> 
<mn1:Head20 id="Head20" runat="server" />
<%-- attention everyone who will work on this file, we don't want NotificationControlPlaceholder to be declared in Wide2Columns.ascx.designer.cs but unfortunately VS does it automatically. It screws up Notifications, so  please see if there is a line: protected global::System.Web.UI.WebControls.PlaceHolder NotificationControlPlaceholder; in Wide2Columns.ascx.designer.cs and comment it. Thanks --%>
<body runat="server" id="bdyMain">
    <uc1:TealiumScript runat="server" ID="TealiumScript" />
<mn:Txt ID="txtGoogleTagManagerCode" runat="server" ResourceConstant="GOOGLE_TAG_MANAGER_CODE" Visible="False"/>
<asp:PlaceHolder runat="server" Visible="false" ID="plcLivePerson">
    <mn:Txt ID="txtLivePersonBodyTag" runat="server" ResourceConstant="TXT_LIVEPERSON_BODY_TAG"  />
    <mn1:LivePersonCustomVars runat="server" ID="lpCustomVars"/>
</asp:PlaceHolder>
<fb:API runat="server" ID="fbAPI" visible="false"></fb:API>

<asp:PlaceHolder ID="plcSage" Runat="server" />
<div id="site-container">
    <mn:AdUnit Visible="false" GAMAdSlot="overlay_1x1" Size="Overlay" runat="server" GAMPageMode="Auto" ID="adUnitOverlay" ></mn:AdUnit>
    <asp:PlaceHolder runat="server" ID="plcPeekBanner" Visible="false">
        <div class="plcPeekBnrFlash">
            <mn:AdUnit ID="AdUnit1" Visible="false" GAMAdSlot="peek_655x655" runat="server" GAMPageMode="Auto" ></mn:AdUnit>
        </div>
    </asp:PlaceHolder>

    <uc2:OmnidateBodyCode runat="server" ID="OmnidateBodyCode" />

    <form id="Form1" method="post" runat="server">

    <asp:PlaceHolder runat="server" ID="plcBlockUIContent" />

    <div id="min-max-container">
        <mn1:TopAuxNav id="topAuxNav" runat="server" />
        <div id="content-container" class="t-layout two-column-lf your-profile clearfix">
	        <mn:Header runat="server" id="Header" />
            
            <!--Activity Support for IM notifier and session-->
            <asp:PlaceHolder runat="server" ID="plcActivitySupport">
                <mn1:ActivitySupport runat="server" id="ActivitySupport1"></mn1:ActivitySupport>
            </asp:PlaceHolder>
    	       
            <div class="wrapper">
                <div id="content-main-primary">
                    <!--top full width page content-->
                    <asp:PlaceHolder ID="notificationControlPlaceholder" Runat="server" EnableViewState="False" />
                    <asp:PlaceHolder ID="PageAnnouncementPlaceholder" runat="server" />
                    <asp:PlaceHolder ID="AppControlTopPlaceholder" runat="server"></asp:PlaceHolder>
                </div>
            </div>
			<div class="wrapper">
                <!--regular page content-->
				<div id="content-main">				    
                    <asp:PlaceHolder ID="AppControlPlaceholder" Runat="server" />
		            <asp:PlaceHolder ID="plcSubscribeBanner" Runat="server" />
		            <asp:PlaceHolder ID="plcGAMBottom" runat="server">
		                <div class="gamBottom_520_125_div">
		                    <mn:AdUnit ID="adUnitBottom2" runat="server" expandImageTokens="true" GAMAdSlot="bottom2_520X125" GAMPageMode="Auto" />
		                </div>
		                <div class="gamBottom_520_125_div">
		                    <mn:AdUnit ID="adUnitBottom3" runat="server" expandImageTokens="true" GAMAdSlot="bottom3_520X125" GAMPageMode="Auto" />
		                </div>
                        <div class="gamBottom_468_60_div">
		                    <mn:AdUnit ID="adUnitBottom4" runat="server" expandImageTokens="true" GAMAdSlot="bottom_468X60" GAMPageMode="Auto" />
		                </div>
		            </asp:PlaceHolder>
				</div>
			</div>
            
			<div id="content-promo" class="clearfix">
                <!--right rail content-->
				<asp:PlaceHolder ID="plcRight" Runat="server" />

				<mn:Txt ID="rightJDateMobileGuidelines" runat="server"  ExpandImageTokens="true" />
				<mn:Txt ID="txtIconListLink" runat="server" ResourceConstant="TXT_ICON_LIST_LINK" ExpandImageTokens="true" />
				<mn:Txt ID="rightPhotoGuidelines" runat="server"  ExpandImageTokens="true" />
			</div>
			<div style="clear: both"></div>
            <asp:PlaceHolder ID="phSubNowBannerContainer" runat="server" Visible="false">
                <div id="SubNowTopBanner" class="hide">
                    <asp:PlaceHolder ID="phSubNowTopBanner" runat="server"></asp:PlaceHolder>
                </div>
            </asp:PlaceHolder>
	   	
        </div>
        <asp:PlaceHolder ID="phTopLeaderboardAd" runat="server">
            <mn:AdUnit id="adLeaderboardHeaderTop"  runat="server" expandImageTokens="true" Size="LeaderboardHeaderAboveMenu" GAMAdSlot="top_728x90" GAMPageMode="Auto"  />
        </asp:PlaceHolder>
    </div>
    <div id="footer">
        <div id="footer-container">
            <mn1:Footer20 ID="Footer20" Runat="server" />
        </div>
    </div>

    <uc1:Feedback ID="Feedback1" runat="server" Visible="false" />
    </form>
	
    <asp:PlaceHolder ID="plcPixels" Runat="server" />
    <asp:PlaceHolder ID="plcFetchbackPixel" Runat="server" />

    <%--checkIM--%>
    <asp:PlaceHolder ID="plcIM" Runat="server" />
    <asp:Literal ID="litPlanSurvey" Runat="server" />
	
    <script type="text/javascript">
        $j(document).ready(function () { init() });
    </script>
	<mn:AdUnit id="adHomePageForced" Size="ForcedPage" visible="false" runat="server" />
	<fp:ForcedPage DisplayCount="1" SpotlightResourceConstant="TXT_FORCED_SPOTLIGHT_PHOTOS" TimedResourceConstant="TXT_FORCED_TIMED_PROMOTION" ForcedPageCondition=6 AppID=-1 runat="server" id="fpTimedPromoAndSpotlight" />
    <uc1:ExternalFeedback ID="ExternalFeedback1" runat="server" OutputTag="Body" />

    <%--Wallpaper Adunit--%>
    <mn:AdUnit Visible="false" GAMAdSlot="wallpaper_1x1" Size="Wallpaper" runat="server" GAMPageMode="Auto" ID="adUnitWallpaper" ></mn:AdUnit>

    <asp:PlaceHolder ID="plcFooterMobile" runat="server" Visible="false" >
       <mn1:FooterMobileAnnouncement runat="server" />
    </asp:PlaceHolder>

</div>
<mn:Txt ID="txtGoogleRemarketingCode" runat="server" ResourceConstant="JAVASCRIPT_GOOGLE_REMARKETING_TAG"/>
<mn:Txt ID="txtGoogleRemarketingCodeFree" runat="server" ResourceConstant="JAVASCRIPT_GOOGLE_REMARKETING_TAG_FREE" Visible="False"/>
<mn:Txt ID="txtGoogleRemarketingCodeSubscriber" runat="server" ResourceConstant="JAVASCRIPT_GOOGLE_REMARKETING_TAG_SUBSCRIBER" Visible="False"/>
<mn:Txt ID="txtFacebookRemarketingCode" runat="server" ResourceConstant="JAVASCRIPT_FACEBOOK_REMARKETING_TAG" Visible="False"/>
</body>
</html>
