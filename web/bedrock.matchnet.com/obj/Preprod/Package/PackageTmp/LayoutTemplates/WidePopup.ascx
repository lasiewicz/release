﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WidePopup.ascx.cs" Inherits="Matchnet.Web.LayoutTemplates.WidePopup" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn1" TagName="Head20" Src="~/Framework/Ui/Head20.ascx" %>
<%@ Register TagPrefix="mn1" TagName="AdUnit" Src="~/Framework/Ui/Advertising/AdUnit.ascx" %>
<%@ Register TagPrefix="mn1" TagName="Footer20" Src="~/Framework/Ui/Footer20.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Feedback" Src="~/Framework/Ui/Feedback/Feedback.ascx" %>
<!doctype html>
<!--[if lte IE 7]> <html class="ie ie7 lte9 lte8 lte7 no-js" xml:lang="<%=GetPageLang() %>" lang="<%=GetPageLang() %>" dir="<%=GetPageDirection() %>"> <![endif]--> 
<!--[if IE 8]> <html class="ie ie8 lte9 lte8 no-js" xml:lang="<%=GetPageLang() %>" lang="<%=GetPageLang() %>" dir="<%=GetPageDirection() %>"> <![endif]--> 
<!--[if IE 9]> <html class="ie ie9 lte9 no-js" xml:lang="<%=GetPageLang() %>" lang="<%=GetPageLang() %>" dir="<%=GetPageDirection() %>"> <![endif]--> 
<!--[if (gte IE 10)|!(IE)]><!--><html class="no-js" xml:lang="<%=GetPageLang() %>" lang="<%=GetPageLang() %>" dir="<%=GetPageDirection() %>"><!--<![endif]--> 
<mn1:Head20 id="Head20" runat="server" />
<body runat="server" id="bdyMain">
<asp:PlaceHolder ID="plcSage" Runat="server" />
<div id="site-container">
<form id="Form1" method="post" runat="server">
    <div id="min-max-container">
        <asp:PlaceHolder ID="phTopLeaderboardAd" runat="server" Visible="false">
            <mn1:AdUnit id="adLeaderboardHeaderTop"  runat="server" expandImageTokens="true" Size="LeaderboardHeaderAboveMenu" GAMAdSlot="top_728x90" GAMPageMode="Auto"  />
        </asp:PlaceHolder>
        <div id="header-logo">
	        <mn:image CssClass="logo" id="imgLogo" runat="server" titleResourceConstant="SITE_LOGO_ALT" ResourceConstant="SITE_LOGO_ALT" FileName="trans.gif"/>
        </div>
        <div id="content-container" class="clearfix">
            <asp:PlaceHolder ID="plcRight" Runat="server" />
            <asp:PlaceHolder ID="plcLeft" Runat="server" />
            <asp:PlaceHolder ID="notificationControlPlaceholder" Runat="server" EnableViewState="False" />
            <asp:PlaceHolder ID="AppControlPlaceholder" Runat="server" />
            <asp:PlaceHolder ID="phRightSquareAd" runat="server" Visible="false">
                <mn1:AdUnit id="adSquareRight" runat="server" expandImageTokens="true" Size="Square"
                    GAMAdSlot="right_300x250" GAMPageMode="Auto" />
            </asp:PlaceHolder>
        </div>
    </div>
    <asp:PlaceHolder ID="phFooter" runat="server" Visible="false">
        <div id="footer">
            <div id="footer-container">
                <mn1:Footer20 ID="pFooter20" Runat="server" />
            </div>
        </div>
    </asp:PlaceHolder>
    <uc1:Feedback ID="Feedback1" runat="server" Visible="false" />
</form>
<asp:PlaceHolder ID="plcPixels" Runat="server" />
<asp:PlaceHolder ID="plcFetchbackPixel" Runat="server" />
</div>
</body>
</html>