﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Session.ValueObjects;
using Matchnet.Web.Applications.CompatibilityMeter;
using Matchnet.Web.Applications.MemberProfile.Registration;
using Matchnet.Web.Applications.MemberProfile;
using Matchnet.Web.Applications.Home;
using Matchnet.Web.Applications.Search;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Ui;
using Matchnet.Web.Framework.PagePixels;
using Matchnet.Web.Framework.Menus;

using Matchnet.Web.Framework.Managers;
using Matchnet.Web.Framework.Ui.PageAnnouncements;

namespace Matchnet.Web.LayoutTemplates
{
    public partial class Wide2ColumnsJMeter : Lib.LayoutTemplateBase
    {
        private Boolean allowFramesFlag = false;

        public override PlaceHolder NotificationControlPlaceholder
        {
            get
            {
                return notificationControlPlaceholder;
            }
            set
            {
                notificationControlPlaceholder = value;
            }
        }
        private void Page_Init(object sender, System.EventArgs e)
        {
            try
            {
                if (g.Member == null)
                {
                    // Check if reg overlay should be displayed
                    if (FrameworkGlobals.IsRegOverlay())
                    {
                        if (Convert.ToBoolean(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("REGISTRATION_OVERLAY_ENABLED", _g.Brand.Site.Community.CommunityID, _g.Brand.Site.SiteID)))
                        {
                            Control ctrl = LoadControl("/Applications/Registration/RegistrationOverlay.ascx");
                            if (ctrl != null)
                            {
                                plcBlockUIContent.Controls.Add(ctrl);
                            }
                        }

                    }
                }
                else
                {
                    if (g.AppPage.ControlName != "RegistrationWelcomeOnePage" && OnePageCompleteProfile2.ShowProfileCompleteOverlay(g))
                    {
                        Control ctrl = LoadControl("/Applications/MemberProfile/OnePageCompleteProfile2.ascx");
                        var constructor = ctrl.GetType().BaseType.GetConstructor(new Type[] { typeof(bool) });
                        if (constructor != null)
                        {
                            constructor.Invoke(ctrl, new object[] { true });
                        }

                        if (ctrl != null)
                        {
                            plcBlockUIContent.Controls.Add(ctrl);
                        }
                    }


                    
                    if (Convert.ToBoolean(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("FOOTER_MOBILE_ANNOUNCEMENT", g.Brand.Site.SiteID)))
                    {
                        //lets take care of the footer mobile announcement
                        bool showMobileFooterAnnouncementOverride = false;
                        if (!Page.IsPostBack && !String.IsNullOrEmpty(Request["showMobileFooter"]) && Request["showMobileFooter"] == "true")
                        {
                            showMobileFooterAnnouncementOverride = true;
                        }
                        if (showMobileFooterAnnouncementOverride || !PageAnnouncementHelper.HasViewedPageAnnouncement(g.Member, g.Brand.Site.SiteID, PageAnnouncementHelper.PageAnnouncementMask.FooterMobileV2))
                        {
                            plcFooterMobile.Visible = true;
                        }
                    }
                    
                

                }


                //for dynamic css classes
                string cssformat = "page-{0} sub-page-{1}";
                string[] appname = _g.AppPage.App.Path.Split('/');
                int upperInd = appname.Length - 1;
                string app = appname[upperInd];
                string subpage = _g.AppPage.ControlName.ToLower();
                bool isDisplayingProile30 = false;
                bool isDisplayingFullSubNowBaner = false;

                bdyMain.Attributes["class"] = String.Format(cssformat, app, subpage);
                if (MenuUtils.IsSocialNavEnabled(g))
                {
                    bdyMain.Attributes["class"] += " socialnav"; //Additional CSS for Social Nav changes
                }
                if (Matchnet.Web.Applications.MemberProfile.ProfileTabs30.ProfileUtility.IsDisplayingProfile30(g))
                {
                    if (g.Member != null && g.Member.MemberID == ViewProfileTabUtility.GetMemberIdForProfileView(g, true))
                        bdyMain.Attributes["class"] += " view-profile-yours";
                    else
                        bdyMain.Attributes["class"] += " view-profile-not-yours";

                    bdyMain.Attributes["class"] += " profile30"; //Additional CSS for Profile30 redesign page, may generalize later to support new styles across other pages of site
                    isDisplayingProile30 = true;
                }
                if (FrameworkGlobals.DisplayTopLargeSubscribeBanner(g))
                {
                    if (!(g.AppPage.App.ID == (int)WebConstants.APP.Home))
                    {
                        bdyMain.Attributes["class"] += " sub-banner-full";
                            //Additional CSS for the full top sub now banner
                    }
                    isDisplayingFullSubNowBaner = true;
                }
                if (isDisplayingFullSubNowBaner && isDisplayingProile30)
                {
                    BreadCrumbHelper.EntryPoint entryPoint = BreadCrumbHelper.GetEntryPoint(Conversion.CInt(Request[WebConstants.URL_PARAMETER_NAME_ENTRYPOINT]));
                    if (entryPoint == BreadCrumbHelper.EntryPoint.MemberSlideshow 
                        || entryPoint == BreadCrumbHelper.EntryPoint.SlideShowPage
                        || HomeUtil.DisplaySecretAdmirerGameFromHomepageFilmstrip(entryPoint))
                    {
                        bdyMain.Attributes["class"] += " secret-admirer-bar"; //Additional CSS for showing secret admirer game
                    }
                }

                // Set css class name to content-main div if the user is a registered user with no sub
                // This creates space for the small sub now banner
                if (_g.Member != null)
                {
                    litCntMain.Text = (_g.Member.IsPayingMember(_g.Brand.Site.SiteID) && subpage != "registeredlogoff") ? string.Empty : " class=\"registered\"";

                    if (litCntMain.Text != String.Empty && !FrameworkGlobals.DisplaySmallSubscribeBanner(g))
                    {
                        litCntMain.Text = String.Empty;
                    }

                    CompatibilityMeterHandler cmh = new CompatibilityMeterHandler(_g);
                    if (cmh.GetCurrentPage() == CompatibilityMeterHandler.JmeterPage.PersonalityTest)
                    {
                        if (string.IsNullOrEmpty(litCntMain.Text))
                        {
                            litCntMain.Text = " class=\"jmeter-test\"";
                        }
                        else
                        {
                            litCntMain.Text = " class=\"registered jmeter-test\"";
                        }
                    }
                }

                if (g.AppPage.ControlName.ToLower() == "memberphotoedit20")
                {
                    rightPhotoGuidelines.Style.Add("display", "block");
                    rightPhotoGuidelines.Text = g.GetResource("RIGHT_PHOTO_GUIDE", this);
                }
                else if (g.AppPage.ControlName.ToLower() == "mobilesettings")
                {
                    rightJDateMobileGuidelines.Style.Add("display", "block");
                    rightJDateMobileGuidelines.Text = g.GetResource("RIGHT_JDATE_MOBILE_GUIDE", this);
                }
                else
                {
                    rightPhotoGuidelines.Style.Add("display", "none");
                }

                //Set IFrame support for top leaderboard
                if (MenuUtils.IsSocialNavEnabled(g))
                {
                    if (MenuUtils.IsTopLeaderboardAdEnabled(g))
                    {
                        this.phTopLeaderboardAd.Visible = true;
                        this.adLeaderboardHeaderTop.SetTopLeaderBoardMenuIFrameSupport();
                    }
                    else
                    {
                        this.phTopLeaderboardAd.Visible = false;
                        this.adLeaderboardHeaderTop.Disabled = true;
                    }
                }
                else
                {
                    if (g.Brand.Site.SiteID != (int)WebConstants.SITE_ID.Cupid || MenuUtils.IsTopLeaderboardAdEnabled(g))
                    {
                        this.phTopLeaderboardAd.Visible = true;
                        this.adLeaderboardHeaderTop.SetTopLeaderBoardMenuIFrameSupport();
                    }
                    else
                    {
                        this.phTopLeaderboardAd.Visible = false;
                        this.adLeaderboardHeaderTop.Disabled = true;
                    }
                }

                if (g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.Cupid
                    || (g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.JDate && app == "home"))
                {
                    adLeaderboardHeaderTop.HideAd = true;
                    adLeaderboardHeaderTop.Disabled = true;
                }

                //These are ads for IL initiatives, Sean confirmed to not show on Spark
                if (g.Brand.Site.SiteID != (int)WebConstants.SITE_ID.JDateCoIL)
                {
                    plcGAMBottom.Visible = false;
                    adUnitBottom2.Disabled = true;
                    adUnitBottom3.Disabled = true;
                }

                g.RightNavigationVisible = false;

                if (g.RightNavigationVisible)
                {
                    plcRight.Controls.Add(LoadControl("/Applications/CompatibilityMeter/Controls/Right.ascx"));
                }
                
                //// Add ExitPopup if applicable.
                Matchnet.Web.Framework.Popup popup = GalleryExitPopup.CreatePopup(g);
                if (popup != null)
                    _g.Page.Controls.Add(popup);

                // Add the PagePixel control.
                plcPixels.Controls.Add(new PagePixelControl());

                // Add the Sage Analytics control.
                Lib.Util util = new Lib.Util();
                plcSage.Controls.Add(util.RenderAnalytics(base.SaveSession));

                //// Omniture Analytics - Use Sage Place Holder for now.
                if (Convert.ToBoolean(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ANALYTICS_OMNITURE", _g.Brand.Site.Community.CommunityID, _g.Brand.Site.SiteID)))
                {
                    Analytics.Omniture omniture = (Analytics.Omniture)Page.LoadControl("/Analytics/Omniture.ascx");
                    plcSage.Controls.Add(omniture);
                    _g.AnalyticsOmniture = omniture;
                }

                // Add Google analytics
                if (!string.IsNullOrEmpty(SettingsManager.GetSettingString(SettingConstants.GOOGLE_ANALYTICS_ACCOUNT, g.Brand)))
                {
                    var googleAnalytics =
                        (Analytics.GoogleAnalytics)Page.LoadControl("/Analytics/GoogleAnalytics.ascx");
                    plcSage.Controls.Add(googleAnalytics);
                }

                //google marketing pixel
                if (g.Member != null)
                {
                    if (g.Session.GetInt(WebConstants.SESSION_GOOGLE_PIXEL_FIRED) != 1)
                    {
                        // In JDIL, we are going to add the gender mask in the pixel code
                        string genderMask = g.Member.GetAttributeInt(g.Brand, "GenderMask").ToString();
                        string[] pixelArgs = new string[2] { genderMask, genderMask };

                        //fire paid/subscriber pixel only once per login
                        if (g.Member.IsPayingMember(g.Brand.Site.SiteID))
                        {
                            txtGoogleRemarketingCodeSubscriber.Visible = true;
                            txtGoogleRemarketingCodeSubscriber.Args = pixelArgs;
                        }
                        else
                        {
                            txtGoogleRemarketingCodeFree.Visible = true;
                            txtGoogleRemarketingCodeFree.Args = pixelArgs;
                        }
                        g.Session.Add(WebConstants.SESSION_GOOGLE_PIXEL_FIRED, "1", SessionPropertyLifetime.Temporary);
                    }
                }

                //facebook marketing pixel
                if (g.Member != null)
                {
                    if (g.Session.GetInt(WebConstants.SESSION_FACEBOOK_PIXEL_FIRED) != 1)
                    {
                        //fire pixel only once per login
                        txtFacebookRemarketingCode.Visible = true;
                        g.Session.Add(WebConstants.SESSION_FACEBOOK_PIXEL_FIRED, "1", SessionPropertyLifetime.Temporary);
                    }
                }

                g.HeaderControl = Header;
                g.TopAuxilaryMenu = topAuxNav;
                _g.LayoutVersion = WebConstants.LayoutVersions.versionWide;

                //temporary hack to fix il IM issue
                if (g.Brand.Site.LanguageID == (int)Matchnet.Language.Hebrew)
                {
                    bdyMain.Attributes["dir"] = base.Direction;
                }

                if (LivePersonManager.Instance.IsLivePersonEnabled(g.Member, g.Brand))
                {
                    plcLivePerson.Visible = true;
                }

                txtGoogleTagManagerCode.Visible = SettingsManager.GetSettingBool("ENABLE_GOOGLE_TAG_MANAGER", g.Brand,
                   false);

                txtDynamicYieldBodyCode.Visible = SettingsManager.GetSettingBool("ENABLE_DYNAMIC_YIELD_PIXEL", g.Brand, false);
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        private void Page_Load(object sender, System.EventArgs e)
        {
            if (!allowFramesFlag && !g.LoadingRightOnly)
                base.BustaFrames();

            g.Head20 = Head20;
        }

        protected override void OnPreRender(EventArgs e)
        {
            if (g.LoadingRightOnly)
            {
                this.Header.Visible = false;
                this.Header.Visible = false;
                this.Footer20.Visible = false;
            }

            if (plcBlockUIContent.Controls.Count > 0)
            {
                Literal lit = new Literal();
                lit.Text = "<div id=\"blockUI\" style=\"display: none;\"></div>";
                plcBlockUIContent.Controls.Add(lit);
            }

            if (g.Member == null)
            {
                plcActivitySupport.Visible = false;
            }

            base.OnPreRender(e);
        }

        protected string GetPageLang()
        {
            string lang = "en";
            if (g.Brand.Site.LanguageID == (int)Matchnet.Language.Hebrew)
            {
                lang = "he";
            }
            return lang;
        }

        protected string GetPageDirection()
        {
            string direction = "ltr";
            if (g.Brand.Site.LanguageID == (int)Matchnet.Language.Hebrew)
            {
                direction = base.Direction;
            }
            return direction;
        }


        public override void ShowFeedback(Matchnet.Web.Framework.Ui.Feedback.FeedbackHelper.FeedbackType feedbackType)
        {
            Feedback1.FeedbackType = feedbackType;
            Feedback1.Visible = true;
        }

        public override void AppendBodyCSS(string cssClass)
        {
            bdyMain.Attributes["class"] += " " + cssClass;
        }
    }

}