<%@ Register TagPrefix="mn" TagName="AdUnit" Src="/Framework/UI/Advertising/AdUnit.ascx" %>
<%@ Register TagPrefix="MNFramework" TagName="Footer" Src="/Framework/Ui/Footer20.ascx" %>
<%@ Register TagPrefix="MNFramework" TagName="Header" Src="/Framework/Ui/Header20.ascx" %>
<%@ Register TagPrefix="MNFramework" TagName="Head" Src="/Framework/Ui/Head20.ascx" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="fp" TagName="ForcedPage" Src="/Applications/PremiumServices/Controls/ForcedPage.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ExternalFeedback" Src="~/Framework/Ui/PageElements/ExternalFeedback.ascx" %>
<%@ Control Language="c#" AutoEventWireup="false" CodeBehind="Standard.ascx.cs" Inherits="Matchnet.Web.LayoutTemplates.Standard"
    TargetSchema="http://schemas.microsoft.com/intellisense/ie5" EnableViewState="false" %>
<%@ Import Namespace="Matchnet.Web.Framework.Ui.Advertising" %>
<MNFramework:Head ID="Head" Runat="server" />
<body id="BodyTag" runat="server">
    <asp:PlaceHolder ID="plcSage" runat="server" />
    <form id="Form1" method="post" runat="server">
    <%-- these IE conditional comments have been added to eliminate the use of css hacks in the stylesheets --%>
    <!--[if IE]><div id="ieAll"><![endif]-->
    <!--[if IE 8]><div id="ie8only"><![endif]-->
    <!--[if IE 7]><div id="ie7only"><![endif]-->
    <!--[if (IE 6)|(IE 7)]><div id="ie6-7"><![endif]-->
    <!--[if IE 6]><div id="ie6only"><![endif]-->
    <%--<mn:AdUnit Visible="false" GAMAdSlot="overlay_1x1" runat="server" GAMPageMode="Auto" ></mn:AdUnit>--%>
    <div class="siteLayout">
        <!-- added to center Cupid design RY, closing tag in Standard.ascx -->
        <div class="siteContainer">
            <div>
            </div>
            <mn:Txt runat="server" ID="txtStaticHeader" ResourceConstant="TXT_STATIC_HEADER" />
            <!-- added to accomodate left margin banner ad for CU -->
            <MNFramework:Header ID="Header" Runat="server" visible="true" />
                        <asp:PlaceHolder runat="server" ID="plcVerticalLine1" Visible="false">
                <div id="divVerticalLine1" class="verticalLine">
                </div>
                <div id="mainContentContainer">
            </asp:PlaceHolder>
            <asp:PlaceHolder ID="NotificationControlPlaceholder" runat="server" EnableViewState="False" />
            <asp:PlaceHolder ID="plcSubscribeBanner" runat="server" />
            <asp:PlaceHolder ID="AppControlPlaceholder" runat="server" />
            <asp:PlaceHolder runat="server" ID="plcVerticalLine2" Visible="false">
        </div>
        <div id="divVerticalLine2" class="verticalLine">
        </div>
    </div>
    </asp:PlaceHolder>
    <mn:AdUnit id="adTextLinks" Size="TextLinkFooter" visible="false" runat="server"
        GAMAdSlot="textlinks_bottom_728x90" GAMPageMode="Auto" />
    <MNFramework:Footer ID="Footer" Runat="server" />
    <asp:PlaceHolder ID="plcPixels" runat="server" />
    <asp:PlaceHolder ID="plcFetchbackPixel" Runat="server" />
    <!-- checkIM -->
    <asp:PlaceHolder ID="plcIM" runat="server" />
    <div>
    </div>
    </div>
    <!-- end site Container div to accomodate CU left margin banner ad -->
    </div>
    <!-- closing "siteLayout" div -->
    <asp:Literal ID="litPlanSurvey" runat="server" />

    <script type="text/javascript">
        $j(document).ready(function(){init()});
    </script>

    <mn:AdUnit id="adHomePageForced" Size="ForcedPage" visible="false" runat="server" />
    <fp:ForcedPage DisplayCount="1" SpotlightResourceConstant="TXT_FORCED_SPOTLIGHT_PHOTOS"
        TimedResourceConstant="TXT_FORCED_TIMED_PROMOTION" ForcedPageCondition="6" AppID="-1"
        runat="server" id="fpTimedPromoAndSpotlight" />
    <%--<mn:AdUnit size="BlockUI" visible="false" runat="server" />--%>
    <asp:PlaceHolder runat="server" ID="plcBlockUIContent" />
    <!--[if IE]></div><![endif]-->
    <!--[if IE 8]></div><![endif]-->
    <!--[if IE 7]></div><![endif]-->
    <!--[if (IE 6)|(IE 7)]></div><![endif]-->
    <!--[if IE 6]></div><![endif]-->
    </form>
    <uc1:ExternalFeedback runat="server" OutputTag="Body" />
    <mn:Txt runat="server" ID="txt3rdPartyBodyCode" ResourceConstant="TXT_3RD_PARTY_BODY_CODE" />
</body>
