﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Matchnet.Web.Framework;
using Matchnet.Web.Framework.PagePixels;
using Matchnet.Web.Applications.Search;
using Matchnet.Web.Framework.Menus;
using Matchnet.Web.Applications.MemberProfile;
using Matchnet.Session.ValueObjects;
using Matchnet.Web.Framework.Managers;
using Matchnet.Web.Framework.Ui.PageAnnouncements;

namespace Matchnet.Web.LayoutTemplates
{
    public partial class FullWidthColumn : Lib.LayoutTemplateBase
    {
        private Boolean allowFramesFlag = false;
        private bool renderAdHeaderTopIFrameIDToJS = false;

        public override PlaceHolder NotificationControlPlaceholder
        {
            get
            {
                return notificationControlPlaceholder;
            }
            set
            {
                notificationControlPlaceholder = value;
            }
        }

        private void Page_Init(object sender, System.EventArgs e)
        {
            if (g.Member == null)
            {
                // Check if reg overlay should be displayed
                if (FrameworkGlobals.IsRegOverlay())
                {
                    if (Convert.ToBoolean(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("REGISTRATION_OVERLAY_ENABLED", _g.Brand.Site.Community.CommunityID, _g.Brand.Site.SiteID)))
                    {
                        Control ctrl = LoadControl("/Applications/Registration/RegistrationOverlay.ascx");
                        if (ctrl != null)
                        {
                            plcBlockUIContent.Controls.Add(ctrl);
                        }
                    }

                }
            }
            else
            {
                if (g.AppPage.ControlName != "RegistrationWelcomeOnePage" && OnePageCompleteProfile2.ShowProfileCompleteOverlay(g))
                {
                    Control ctrl = LoadControl("/Applications/MemberProfile/OnePageCompleteProfile2.ascx");
                    var constructor = ctrl.GetType().BaseType.GetConstructor(new Type[] { typeof(bool) });
                    if (constructor != null)
                    {
                        constructor.Invoke(ctrl, new object[] { true });
                    }

                    if (ctrl != null)
                    {
                        plcBlockUIContent.Controls.Add(ctrl);
                    }
                }

                if (Convert.ToBoolean(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("FOOTER_MOBILE_ANNOUNCEMENT", g.Brand.Site.SiteID)))
                {
                    //lets take care of the footer mobile announcement
                    bool showMobileFooterAnnouncementOverride = false;
                    if (!Page.IsPostBack && !String.IsNullOrEmpty(Request["showMobileFooter"]) && Request["showMobileFooter"] == "true")
                    {
                        showMobileFooterAnnouncementOverride = true;
                    }
                    if (showMobileFooterAnnouncementOverride || !PageAnnouncementHelper.HasViewedPageAnnouncement(g.Member, g.Brand.Site.SiteID, PageAnnouncementHelper.PageAnnouncementMask.FooterMobile))
                    {
                        plcFooterMobile.Visible = true;
                    }
                }
                



            }

            //for dynamic css classes
            string cssBodyExisting = "";
            string cssformat = "page-{0} sub-page-{1}";
            string[] appname = _g.AppPage.App.Path.Split('/');
            int upperInd = appname.Length - 1;
            string app = appname[upperInd];
            string subpage = _g.AppPage.ControlName.ToLower();

            if (!string.IsNullOrEmpty(bdyMain.Attributes["class"]))
            {
                cssBodyExisting = bdyMain.Attributes["class"];
            }

            bdyMain.Attributes["class"] = cssBodyExisting + " " + String.Format(cssformat, app, subpage);
            if (MenuUtils.IsSocialNavEnabled(g))
            {
                bdyMain.Attributes["class"] += " socialnav"; //Additional CSS for Social Nav changes
            }
            if (FrameworkGlobals.DisplayTopLargeSubscribeBanner(g))
            {
                if (!(g.AppPage.App.ID == (int)WebConstants.APP.Home))
                {
                    bdyMain.Attributes["class"] += " sub-banner-full"; //Additional CSS for the full top sub now banner
                }
            }

            // Set css class name to content-main div if the user is a registered user with no sub
            if (_g.Member != null && !g.HideSubNowBanner)
            {
                litCntMain.Text = (_g.Member.IsPayingMember(_g.Brand.Site.SiteID) && subpage != "registeredlogoff") ? string.Empty : " class=\"registered\"";
            }

            //Set IFrame support for top leaderboard
            if (MenuUtils.IsSocialNavEnabled(g))
            {
                if (MenuUtils.IsTopLeaderboardAdEnabled(g))
                {
                    this.phTopLeaderboardAd.Visible = true;
                    this.adLeaderboardHeaderTop.SetTopLeaderBoardMenuIFrameSupport();
                }
                else
                {
                    this.phTopLeaderboardAd.Visible = false;
                }
            }
            else
            {
                this.phTopLeaderboardAd.Visible = true;
                this.adLeaderboardHeaderTop.SetTopLeaderBoardMenuIFrameSupport();
            }

            g.RightNavigationVisible = true;

            //// Add ExitPopup if applicable.
            Matchnet.Web.Framework.Popup popup = GalleryExitPopup.CreatePopup(g);
            if (popup != null)
                _g.Page.Controls.Add(popup);

            // Add the PagePixel control.
            plcPixels.Controls.Add(new PagePixelControl());

            // Add the Sage Analytics control.
            Lib.Util util = new Lib.Util();
            plcSage.Controls.Add(util.RenderAnalytics(base.SaveSession));

            //// Omniture Analytics - Use Sage Place Holder for now.
            if (Convert.ToBoolean(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ANALYTICS_OMNITURE", _g.Brand.Site.Community.CommunityID, _g.Brand.Site.SiteID)))
            {
                Analytics.Omniture omniture = (Analytics.Omniture)Page.LoadControl("/Analytics/Omniture.ascx");
                plcSage.Controls.Add(omniture);
                _g.AnalyticsOmniture = omniture;
            }

            // Fetchback pixel
            if (PixelHelper.FetchbackPixelVisible(g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.AppPage.ControlName, g.Member))
            {
                plcFetchbackPixel.Controls.Add(PixelHelper.GetFetchbackPixel(g.Member, g.Brand.Site.SiteID));
            }

            //google marketing pixel
            if (g.Member != null)
            {
                if (g.Session.GetInt(WebConstants.SESSION_GOOGLE_PIXEL_FIRED) != 1)
                {
                    // In JDIL, we are going to add the gender mask in the pixel code
                    string genderMask = g.Member.GetAttributeInt(g.Brand, "GenderMask").ToString();
                    string[] pixelArgs = new string[2] { genderMask, genderMask };

                    //fire paid/subscriber pixel only once per login
                    if (g.Member.IsPayingMember(g.Brand.Site.SiteID))
                    {
                        txtGoogleRemarketingCodeSubscriber.Visible = true;
                        txtGoogleRemarketingCodeSubscriber.Args = pixelArgs;
                    }
                    else
                    {
                        txtGoogleRemarketingCodeFree.Visible = true;
                        txtGoogleRemarketingCodeFree.Args = pixelArgs;
                    }
                    g.Session.Add(WebConstants.SESSION_GOOGLE_PIXEL_FIRED, "1", SessionPropertyLifetime.Temporary);
                }
            }

            //facebook marketing pixel
            if (g.Member != null)
            {
                if (g.Session.GetInt(WebConstants.SESSION_FACEBOOK_PIXEL_FIRED) != 1)
                {
                    //fire pixel only once per login
                    txtFacebookRemarketingCode.Visible = true;
                    g.Session.Add(WebConstants.SESSION_FACEBOOK_PIXEL_FIRED, "1", SessionPropertyLifetime.Temporary);
                }
            }

            if (LivePersonManager.Instance.IsLivePersonEnabled(g.Member, g.Brand))
            {
                plcLivePerson.Visible = true;
            }

            g.HeaderControl = Header;
            g.TopAuxilaryMenu = topAuxNav;
            _g.LayoutVersion = WebConstants.LayoutVersions.versionWide;

            if (g.Brand.Site.LanguageID == (int)Matchnet.Language.Hebrew)
            {
                bdyMain.Attributes["dir"] = base.Direction;
            }
        }

        private void Page_Load(object sender, System.EventArgs e)
        {
            if (!allowFramesFlag && !g.LoadingRightOnly)
                base.BustaFrames();

            g.Head20 = Head20;

        }


        protected override void OnPreRender(EventArgs e)
        {
            if (g.LoadingRightOnly)
            {
                this.Header.Visible = false;
                this.Footer20.Visible = false;
            }

            if (plcBlockUIContent.Controls.Count > 0)
            {
                Literal lit = new Literal();
                lit.Text = "<div id=\"blockUI\" style=\"display: none;\"></div>";
                plcBlockUIContent.Controls.Add(lit);
            }

            if (g.Member == null)
            {
                plcActivitySupport.Visible = false;
            }

            base.OnPreRender(e);
        }

        public override void LoadFacebookScript()
        {
            fbAPI.Visible = true;
        }

        protected string GetPageLang()
        {
            string lang = "en";
            if (g.Brand.Site.LanguageID == (int)Matchnet.Language.Hebrew)
            {
                lang = "he";
            }
            return lang;
        }

        protected string GetPageDirection()
        {
            string direction = "ltr";
            if (g.Brand.Site.LanguageID == (int)Matchnet.Language.Hebrew)
            {
                direction = base.Direction;
            }
            return direction;
        }

        public override void ShowFeedback(Matchnet.Web.Framework.Ui.Feedback.FeedbackHelper.FeedbackType feedbackType)
        {
            Feedback1.FeedbackType = feedbackType;
            Feedback1.Visible = true;
        }

        public override void AppendBodyCSS(string cssClass)
        {
            bdyMain.Attributes["class"] += " " + cssClass;
        }

        public override void DisableGamOverlay()
        {
            adUnitOverlay.Disabled = true;
            g.GAM.UnregisterAdSlot(adUnitOverlay.GAMAdSlot);
        }

        public override void AddPageAccouncementControl(FrameworkControl control)
        {
            PageAnnouncementPlaceholder.Controls.Add(control);
        }
    }
}
