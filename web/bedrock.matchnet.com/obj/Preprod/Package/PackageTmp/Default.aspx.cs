﻿using System;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.UI.WebControls;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Content.ValueObjects.PageConfig;
using Matchnet.Configuration.ServiceAdapters.Analitics;
using Matchnet.Session.ServiceAdapters;
using Matchnet.Session.ValueObjects;
using Matchnet.Web.Applications.CompatibilityMeter;
using Matchnet.Web.Applications.Subscription;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Managers;
using Matchnet.Web.Lib;
using Matchnet.Web.Framework.Diagnostics;
using Matchnet.Member.ValueObjects;
using Matchnet.Content.ServiceAdapters.Links;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Web.Applications.Email;
using Spark.Common.MemberLevelTracking;
using Matchnet.Web.Applications.Search;
using Matchnet.Web.Framework.Util;

namespace Matchnet.Web
{
    /// <summary>
    /// Summary description for Default2.
    /// </summary>
    public class BasePage : System.Web.UI.Page
    {
        private LayoutTemplateBase layoutTemplate;
        string NotificationTemplatePath;
        ContextGlobal _g;
        private SettingsManager _settingsManager = new SettingsManager();

        private void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Request["p"] != null)
                {
                    Response.Redirect("/404Form.aspx?404;" + Request.Url.ToString());
                }
                //Note: the code below was an attempt to detect external searches that do not link to SearchResults.aspx
                //or specify a "p" or "a" in the URL.  This had to be disabled because the SearchTypeID parameter is also used
                //to tell the Splash page to default to a particular tab (search type).  Any external search forms/links that
                //this breaks should be modified to link directly to /Applications/Search/SearchResults.aspx.
                // TT#16657 - re-enabled because now the SearchTypeID url parm has been changed to SearchInitTab
                else if (Request[WebConstants.URL_PARAMETER_NAME_SEARCHTYPEID] != null && Request.RawUrl.IndexOf("/Applications/Search/SearchResults.aspx") < 0)
                {
                    Response.Redirect("/Applications/Search/SearchResults.aspx?" + Request.QueryString.ToString());
                }

                Response.AddHeader("Cache-Control", "no-store");

                InitializeG();
                //System.Diagnostics.Trace.WriteLine("Default.aspx: Finished initializing G");

                // set up CultureInfo for the current thread. Depends on _Brand being set.
                InitializeCultureInfo();

                Lib.Util util = new Lib.Util();
                util.DetectRemoteRegistration();

                SetupAppPage();

                //Added to redirect to "don't go" subscription page if conditions are met
                ProcessDontGoRedirect();

                string pagePath = (string)Context.Items["PagePath"];

                #region Redirect 301


                // The following code is used to redirect users that entered a URL without www to URL that contains www
                // This is for SEO purposes MPR-255 and MPR-219
                // MPR-1216 Redirect 301 for /default.aspx , /splash.aspx , /splash20.aspx to www.jdate.co.il/fr/uk & cupid
                string currentURL = Request.Url.AbsoluteUri.ToLower();

                if (!String.IsNullOrEmpty(Request.QueryString["sector"]))
                {
                    string host = "http://" + Request.Url.Host;

                    if (Request.QueryString["sector"] == "1")
                    {
                        _g.Transfer(host + "/academic-dating", "301");
                        Response.End();
                    }
                    else if (Request.QueryString["sector"] == "2")
                    {
                        _g.Transfer(host + "/religious-dating", "301");
                        Response.End();
                    }
                }

                bool enableSplashRedirect =
                               Boolean.Parse(
                                   Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting(
                                       "ENABLE_DEFAULT_AND_SPLASH_REDIRECT", _g.Brand.Site.Community.CommunityID, _g.Brand.Site.SiteID));

                bool enable301Redirect = false;
                string newURL = string.Empty;

                if (Boolean.Parse(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ENABLE_WWW_REDIRECT")))
                {
                    if (_g.Brand.Uri.Length == Request.Url.Host.Length) // sub domain is not present
                    {
                        string envString = FrameworkGlobals.GetEnvironmentString().ToLower();

                        if (envString == string.Empty)
                        {
                            envString = "www";
                        }

                        // Redirect the user to the same URL with envString
                        Matchnet.Content.ServiceAdapters.Links.ParameterCollection paramColl = new Matchnet.Content.ServiceAdapters.Links.ParameterCollection();
                        foreach (string key in Request.QueryString)
                        {
                            paramColl.Add(key, Request.QueryString[key]);
                        }
                        _g.Session.Add(Matchnet.Web.Analytics.Omniture.OMNITURE_ALIAS_SESSION_KEY, currentURL, SessionPropertyLifetime.Temporary);

                        newURL = LinkFactory.Instance.GetLink(pagePath, paramColl, _g.Brand, currentURL);
                        newURL = newURL.Insert(newURL.IndexOf("//") + 2, envString + '.');
                        if (pagePath == "/default.aspx")
                        {
                            newURL = newURL.Replace(pagePath, string.Empty);
                        }
                        else
                        {
                            if (enableSplashRedirect && (pagePath == "/splash.aspx" || pagePath == "/splash20.aspx"))
                            {
                                newURL = newURL.Replace(pagePath, string.Empty);
                            }
                        }

                        enable301Redirect = true;
                    }
                }
                if (!enable301Redirect)
                {
                    if (!IsPostBack && enableSplashRedirect && (pagePath == "/default.aspx" || pagePath == "/splash.aspx" || pagePath == "/splash20.aspx"))
                    {
                        bool enableRedirect = true;
                        if (pagePath == "/default.aspx")
                        {
                            if (!string.IsNullOrEmpty(_g.Session["REDIRECTED_FROM_DEFAULT"]))
                            {
                                enableRedirect = false;
                                _g.Session.Remove("REDIRECTED_FROM_DEFAULT");
                            }
                            else
                            {
                                _g.Session.Add("REDIRECTED_FROM_DEFAULT", "true", SessionPropertyLifetime.Temporary);
                            }
                        }
                        if (enableRedirect)
                        {
                            // Redirect the user to the same URL with envString
                            Matchnet.Content.ServiceAdapters.Links.ParameterCollection paramColl = new Matchnet.Content.ServiceAdapters.Links.ParameterCollection();
                            foreach (string key in Request.QueryString)
                            {
                                paramColl.Add(key, Request.QueryString[key]);
                            }

                            _g.Session.Add(Matchnet.Web.Analytics.Omniture.OMNITURE_ALIAS_SESSION_KEY, currentURL, SessionPropertyLifetime.Temporary);

                            newURL = LinkFactory.Instance.GetLink(pagePath, paramColl, _g.Brand, currentURL);

                            if (newURL.Contains(pagePath))
                            {
                                enable301Redirect = true;
                                newURL = newURL.Replace(pagePath, string.Empty);
                            }
                        }
                    }
                }
                if (enable301Redirect && !string.IsNullOrEmpty(newURL))
                {
                    _g.Transfer(newURL, "301");
                    Response.End();
                }

                #endregion

                #region add url param to home/default.aspx for remarketing

                bool addURLParamforRemarketing = Boolean.Parse(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ADD_URL_PARAM_FOR_REMARKETING", _g.Brand.Site.Community.CommunityID, _g.Brand.Site.SiteID));

                if (_g.Member != null && addURLParamforRemarketing && pagePath == "/applications/home/default.aspx" && string.IsNullOrEmpty(Request.QueryString["sub"]))
                {
                    // Redirect the user to the same URL with envString
                    Matchnet.Content.ServiceAdapters.Links.ParameterCollection paramColl = new Matchnet.Content.ServiceAdapters.Links.ParameterCollection();
                    foreach (string key in Request.QueryString)
                    {
                        paramColl.Add(key, Request.QueryString[key]);
                    }

                    int memberSubscriptionStatus = (FrameworkGlobals.GetMemberSubcriptionStatus(_g.Member, _g.TargetBrand));

                    bool isPaused = memberSubscriptionStatus == (int)Matchnet.Purchase.ValueObjects.SubscriptionStatus.PausedSubscriber;

                    paramColl.Add("sub", (_g.Member.IsPayingMember(_g.Brand.Site.SiteID) || isPaused) ? "1" : "0");

                    newURL = LinkFactory.Instance.GetLink(pagePath, paramColl, _g.Brand, currentURL);
                    _g.Transfer(newURL, "301");
                    Response.End();
                }


                #endregion



                // Make sure only FacebookWelcome page is disaplyed for members that view the site in a Facebook iframe
                if (!string.IsNullOrEmpty(Request.QueryString[WebConstants.URL_PARAMETER_FACEBOOK_REGISTRATION]) && string.IsNullOrEmpty(Request.QueryString["member"]) && string.IsNullOrEmpty(Request.QueryString["new"]))
                {
                    if (_g.Member != null)
                    {
                        string targetURL = "/Applications/MemberProfile/FacebookWelcome.aspx?" +
                                           WebConstants.URL_PARAMETER_FACEBOOK_REGISTRATION + "=1";

                        if (!string.IsNullOrEmpty(Request.QueryString["new"]))
                        {
                            targetURL += "&new=1";
                        }
                        else
                        {
                            targetURL += "&member=1";
                        }
                        Response.Redirect(targetURL, true);
                    }
                }
                // Redirect beta users to beta site.
                util.BetaRedirect(pagePath);
                util.PrivilegeRedirect(pagePath);

                InitializeViewState();

                // Check if we need to toggle beta test requests
                BetaHelper.ToggleBetaRequest(_g);

                //	3)	page
                RenderPage();

                if (Convert.ToBoolean((RuntimeSettings.GetSetting("IS_USER_SESSION_TRACKING_ENABLED", this._g.Brand.Site.Community.CommunityID,
                    this._g.Brand.Site.SiteID, this._g.Brand.BrandID))))
                {
                    SaveUserSessionDetails();
                }

            }
            catch (ThreadAbortException ex)
            {
                // threadabortexception caused by Redirect() with EndResponse set to true
                // we cant set EndResponse to false or it will process a full request after a redirect
                // we cant log threadabortexception because it occurs too frequently
                // this is currently the best known way to handle this issue

                
            }
            catch (Exception ex)
            {
                ContextExceptions _ContextExceptions = new ContextExceptions();

                _ContextExceptions.LogException(ex, true);
                FrameworkGlobals.RedirectToErrorPage(true);
            }
        }

        /// <summary>
        /// Override for the Page processing of viewstate information
        /// </summary>
        /// <returns></returns>
        protected override object LoadPageStateFromPersistenceMedium()
        {
            object objViewstate = null;

            if (Request["__VIEWSTATE"] != null && Request["__VIEWSTATE"].Trim().StartsWith("dDw", StringComparison.InvariantCultureIgnoreCase))
            {
                //bypass viewstate processing for invalid viewstate 1.1
            }
            else
            {
                //continue with viewstate processing 2.0
                objViewstate = base.LoadPageStateFromPersistenceMedium();
            }

            return objViewstate;
        }

        #region Private Methods

        private void ProcessDontGoRedirect()
        {
            CookieHelper cookieHelper = new CookieHelper();
            if (_g.AppPage.ID != (int)WebConstants.PageIDs.CheckIM && _g.AppPage.ID != (int)WebConstants.PageIDs.DontGo &&
                _g.AppPage.App.ID != (int)WebConstants.APP.Subscription && _g.LayoutTemplate != LayoutTemplate.Popup &&
                _g.AppPage.App.ID != (int)WebConstants.APP.Article &&
                _g.Member != null && cookieHelper.DoesDontGoCookieExist() && !MemberPrivilegeAttr.IsCureentSubscribedMember(_g) && SubscriptionDontGoThrottler.ShowDontGoPage(_g))
            {
                Response.Redirect("/Applications/Subscription/DontGo.aspx");
            }
        }

        private string GetPath()
        {
            //Use a specialized control to display landing pages
            //TODO: Check whether this is a valid lpid

            string fullURL = HttpUtility.UrlDecode(Context.Request.Url.AbsoluteUri);

            string checkForFriendlyUrl = FrameworkGlobals.CheckForFriendlyURL();

            if (!string.IsNullOrEmpty(checkForFriendlyUrl))
            {
                if (fullURL.Contains("religious-dating-profiles") || fullURL.Contains("academic-dating-profiles") ||
                    fullURL.Contains("Femmes-juives") || fullURL.Contains("Les-hommes-juifs"))
                {
                    return "/Applications/Search/SearchResults.aspx";
                }
                else if (fullURL.Contains("student-dating-signup") || fullURL.Contains("religious-dating-signup") ||
                    fullURL.Contains("dating-for-divorced-signup") || fullURL.Contains("silver-dating-signup"))
                {
                    return "/Applications/Home/Splash20.aspx";
                }
                else if (fullURL.Contains("religious-dating") || fullURL.Contains("academic-dating") ||
                         fullURL.Contains("Divorced-dating") || fullURL.Contains("Senior-dating"))
                {
                    return "/Applications/Article/Lobby.aspx";
                }
            }

            //else if (fullURL.Contains("academic-dating"))
            //{
            //    return "/Applications/Search/SearchResults.aspx?SearchTypeID=2&SearchOrderBy=1&CountryRegionID=105&HasPhotoFlag=1&GenderID=1&SeekingGenderID=8&MinAge=21&MaxAge=30&EducationLevel=120&AreaCode1=03&Sector=1";
            //}
            if (Request[WebConstants.URL_PARAMETER_NAME_LANDINGPAGEID] != null)
            {
                _g.LandingPage = LandingPageSA.Instance.GetLandingPage(Conversion.CInt(HttpContext.Current.Request[WebConstants.URL_PARAMETER_NAME_LANDINGPAGEID]));

                if (_g.LandingPage != null && _g.LandingPage.IsActive)
                {
                    if (_g.LandingPage.IsStaticLandingPage)
                    {
                        return "/Applications/MemberProfile/LandingPageHolderStatic.aspx";
                    }
                    else
                    {
                        return "/Applications/MemberProfile/LandingPageHolder.aspx";
                    }
                }
            }

            /*
            //Note: This is necessary for legacy external registration pages that use ?a=7500 on the URL to redirect to registration
            //New external registration forms should point directly to /Applications/MemberProfile/RegistrationStep1.aspx
            if (_g.IsRemoteRegistration)
            {
                return "/Applications/MemberProfile/RegistrationStep1.aspx";
            }*/

            string path = new UriBuilder("http://foo" + HttpContext.Current.Request.RawUrl).Path;
            if (path.ToLower().IndexOf("jpegimage.aspx") > 0)
            {
            }

            if (path.ToLower() == "/default.aspx" || path.ToLower() == "/splash.aspx" || path.ToLower() == "/splash20.aspx")
            {
                if (_g.Session.GetInt(WebConstants.SESSION_PROPERTY_NAME_MEMBERID, 0) > 0)
                {
                    path = "/Applications/Home/Default.aspx";
                }
                else
                {
                    path = "/Applications/Home/Splash20.aspx";
                }
            }

            // add a case for Jdate.com/Mobile
            if (path.ToLower() == "/mobile" || path.ToLower() == "/mobile/" || path.ToLower()== "/mobilelanding.aspx") 
            {
                path = "/Applications/Mobile/MobileLanding.aspx";
            }
             
            

            return path;
        }

        private void InitializeG()
        {
            _g = new ContextGlobal(Context);
        }

        private void SetupAppPage()
        {
            string path = GetPath();
            string pageName = String.Empty;
            _g.AppPage = PageConfigSA.Instance.GetPage(_g.Brand.Site.SiteID, path);

            if (_g.AppPage != null)
            {
                pageName = _g.AppPage.ControlName;
            }

            if (_g.Member != null)
            {

                RedirectEmailVerification();
                RedirectAdminSuspended();

                RedirectPhotoRequirement();

                int suspended = _g.Member.GetAttributeInt(_g.Brand, "SelfSuspendedFlag");
                int genderMask = _g.Member.GetAttributeInt(_g.Brand, "GenderMask");
                string emailAddress = _g.Member.EmailAddress;
                string userName = _g.Member.GetUserName(_g.Brand);
                int brandLogonCount = _g.Member.GetAttributeInt(_g.Brand, "BrandLogonCount", 0);
                //quick and dirty fix for migrated site users who have 0 brandlogon counts on bh platform
                if (FrameworkGlobals.IsMingleSite(_g.Brand.Site.Community.CommunityID) && brandLogonCount == 0)
                {
                    brandLogonCount = 1;

                }
                DateTime brandInsertDate = _g.Member.GetAttributeDate(_g.Brand, "BrandInsertDate", DateTime.MinValue);
                bool hasJdateReligion = true;

                if (_g.Brand.Site.Community.CommunityID == (int)WebConstants.COMMUNITY_ID.JDate)
                {
                    if (_g.Member.GetAttributeInt(_g.Brand, "JdateReligion", Constants.NULL_INT) == Constants.NULL_INT)
                    {
                        hasJdateReligion = false;
                    }
                }

                // Check if their suspended, as well, TT15756 - require: username, emailaddress, gendermask 
                if (suspended == 1 && pageName != "Logoff")
                {
                    _g.AppPage = PageConfigSA.Instance.GetPage(_g.Brand.Site.SiteID, "/Applications/MemberServices/Unsuspend.aspx");
                }

                //as of pm240, viewprofile page is now used for profile edit (if enabled)
                string mode = (string.IsNullOrEmpty(Request["Mode"])) ? "view" : Request["Mode"].ToLower();

                // If the member has missing information in required fields, force the user to go to those pages where the information can be requested
                if (!IsPostBack && string.IsNullOrEmpty(Request["ShowSplashPageForPostLoginUsers"]) && string.IsNullOrEmpty(Request.Params["imptoken"]))
                {
                    if (pageName != "RegistrationStep2" && pageName != "CheckIM" && pageName != "Logoff"
                        && pageName != "RegistrationStep1" && pageName != "AttributeEdit" && pageName != "CityList"
                        && !(pageName == "ViewTabbedProfile20" && "edit".Equals(mode)) && pageName != "GAMIframe" && pageName != "RegistrationWelcomeOnePage"
                        && pageName != "RegistrationWelcome" && pageName != "ViewProfile" && _g.AppPage != null &&_g.AppPage.App.ID != (int)WebConstants.APP.Subscription && string.IsNullOrEmpty(Request["overlayreg"]))
                    {

                        bool memberHasUploadedPhotos = MemberPhotoDisplayManager.Instance.GetAllPhotosCount(_g.Member,
                                                                             _g.Brand.Site.Community.CommunityID) > 0;

                        bool photoNudgeEnabled = Convert.ToBoolean(RuntimeSettings.GetSetting("IS_PHOTO_NUDGE_ENABLED", _g.Brand.Site.Community.CommunityID, _g.Brand.Site.SiteID));
                        bool alreadyPassedOnPhotoNudge = Convert.ToBoolean(_g.Session.GetString(WebConstants.SESSION_PROPERTY_NUDGE_PHOTO_LATER, "false"));
                        bool forceAboutMeRequirement = Convert.ToBoolean(RuntimeSettings.GetSetting("IS_ABOUT_ME_REQUIRED", _g.Brand.Site.Community.CommunityID, _g.Brand.Site.SiteID));
                        bool alreadyPassedOnAboutmeNudge = Convert.ToBoolean(_g.Session.GetString(WebConstants.SESSION_PROPERTY_NUDGE_ABOUTME_LATER, "false"));
                        bool forceFirstNameRequirement = Convert.ToBoolean(RuntimeSettings.GetSetting("IS_FIRST_NAME_REQUIRED", _g.Brand.Site.Community.CommunityID, _g.Brand.Site.SiteID));
                        int minThreshold = Convert.ToInt32(RuntimeSettings.GetSetting("FORCED_PAGE_LOGON_COUNT_MIN_THRESHOLD", _g.Brand.Site.Community.CommunityID, _g.Brand.Site.SiteID));

                        bool pledgeToCounterFroudEnabled = Convert.ToBoolean(RuntimeSettings.GetSetting("PledgeToCounterFroudEnabled", _g.Brand.Site.Community.CommunityID, _g.Brand.Site.SiteID));

                        if (forceFirstNameRequirement) // if first name is required.
                        {
                            string firstname = _g.Member.GetAttributeText(_g.Brand, "SiteFirstName").ToString();

                            if (firstname == null || firstname == string.Empty)
                            {
                                _g.Transfer("/Applications/MemberProfile/AttributeEdit.aspx?Attribute=SITEFIRSTNAME&DestinationURL=" + Server.UrlEncode((string)Context.Items["PagePath"]));
                            }
                        }

                        // About Me can not be empty
                        if (forceAboutMeRequirement && !alreadyPassedOnAboutmeNudge && brandLogonCount > minThreshold) // if About Me is required.
                        {
                            string txtAboutme = _g.Member.GetAttributeText(_g.Brand, "AboutMe").ToString();

                            if (txtAboutme == null || txtAboutme == string.Empty)
                            {
                                if (photoNudgeEnabled && !memberHasUploadedPhotos && !alreadyPassedOnPhotoNudge)
                                {
                                    _g.Transfer("/Applications/MemberProfile/AttributeEdit.aspx?Attribute=Photo&SecondAttribute=AboutMe&DestinationURL=" + Server.UrlEncode(Context.Request.RawUrl));
                                }
                                else
                                {
                                    _g.Transfer("/Applications/MemberProfile/AttributeEdit.aspx?Attribute=AboutMe&DestinationURL=" + Server.UrlEncode((string)Context.Items["PagePath"]));
                                }
                            }
                        }


                        if (photoNudgeEnabled && !memberHasUploadedPhotos && !alreadyPassedOnPhotoNudge && brandLogonCount > minThreshold)
                        {
                            _g.Transfer("/Applications/MemberProfile/AttributeEdit.aspx?Attribute=Photo&DestinationURL=" + Server.UrlEncode((string)Context.Items["PagePath"]));
                        }

                        //if we're doing sub stuff, don't redirect to pledge page

                        if (pledgeToCounterFroudEnabled && !Convert.ToBoolean(_g.Session.GetString(WebConstants.SESSION_PROPERTY_PLEDGE_FRAUD_LATER, "false")) && brandLogonCount > minThreshold)
                        {
                            // if PledgeToCounterFroud is required.
                            bool alreadyPledgedToCounterFroud = Conversion.CBool(_g.Member.GetAttributeInt(_g.Brand.Site.Community.CommunityID,
                                                                            _g.Brand.Site.SiteID, _g.Brand.BrandID,
                                                                            "PledgeToCounterFroud", 0));

                            if (!alreadyPledgedToCounterFroud && ((string)Context.Items["FullURL"]).IndexOf("RedirectToSubPage=1") == -1)
                            {
                                _g.Transfer("/Applications/MemberProfile/AttributeEdit.aspx?Attribute=PledgeToCounterFroud&DestinationURL=" +
                                    Server.UrlEncode(((string)Context.Items["FullURL"]).Substring(((string)Context.Items["FullURL"]).IndexOf('/', 8))));
                            }
                        }


                        if (!hasJdateReligion)
                        {
                            string _alreadyOffered = _g.Session.GetString("ATTR_EDIT_JDATERELIGION");
                            if (String.IsNullOrEmpty(_alreadyOffered))
                            {
                                Page attrForcedPage = PageConfigSA.Instance.GetPage(_g.Brand.Site.SiteID, "/Applications/MemberProfile/AttributeEdit.aspx");
                                if (attrForcedPage != null)
                                {
                                    _g.Transfer("/Applications/MemberProfile/AttributeEdit.aspx?Attribute=JDatereligion&DestinationURL=" + Server.UrlEncode(_g.GetDestinationURL()));
                                }
                                else
                                {
                                    _g.Transfer("/Applications/MemberProfile/RegistrationStep2.aspx?rc=TXT_JDATERELIGION_INFO_REQUIRED");
                                }
                            }
                        }

                    }


                    if (pageName != "AttributeEdit" && (pageName != "RegistrationStep1" && pageName != "CheckIM" || pageName != "ChangeEmail" || pageName != "CityList" || !(pageName == "ViewTabbedProfile20" && "edit".Equals(mode))))
                    {
                        if (genderMask < 0 || userName == null || userName == string.Empty || brandLogonCount == 0 || brandInsertDate == DateTime.MinValue)
                        {
                            if (pageName != "RegistrationStep1" && pageName != "CheckIM" && pageName != "Logoff" && pageName != "CityList" && !(pageName == "ViewTabbedProfile20" && "edit".Equals(mode))
                            && !(pageName == "ViewProfile" && "edit".Equals(mode)) && pageName != "GAMIframe")
                            {
                                _g.Transfer("/Applications/MemberProfile/ViewProfile.aspx?Mode=Edit&rc=TXT_MEMBER_INFO_REQUIRED");
                                /*if (_settingsManager.GetSettingBool(SettingConstants.EDIT_PROFILE_TABS_ENABLED, _g.Brand))
                                {
                                    _g.Transfer("/Applications/MemberProfile/ViewProfile.aspx?Mode=Edit&rc=TXT_MEMBER_INFO_REQUIRED");
                                }
                                else
                                {
                                    _g.Transfer("/Applications/MemberProfile/RegistrationStep1.aspx?rc=TXT_MEMBER_INFO_REQUIRED");
                                }*/
                            }
                        }
                        else if (emailAddress == null || emailAddress == string.Empty)
                        {
                            if (_g.Session != null)
                            {
                                if (_g.Session["EmailAddress"] != null && _g.Session["EmailAddress"] != String.Empty)
                                {
                                    string sessionEmail = _g.Session["EmailAddress"].ToString();
                                    _g.Member.EmailAddress = sessionEmail;
                                    Member.ServiceAdapters.MemberSA.Instance.SaveMember(_g.Member);
                                }
                                else
                                {
                                    _g.Transfer("/Applications/MemberProfile/ChangeEmail.aspx");
                                }
                            }
                        }


                    }
                }
            }

            //If we can't find the page, redirect to the home page with an error message.
            //TODO: log this?
            if (_g.AppPage == null)
            {
                _g.Notification.AddError("WE_WERE_UNABLE_TO_FIND_THE_PAGE_YOU_ARE_LOOKING_FOR_X");
                _g.AppPage = PageConfigSA.Instance.GetPage(_g.Brand.Site.SiteID, "/Applications/Home/Default.aspx");
            }

            _g.Page = this;
        }

        private void RedirectPhotoRequirement()
        {
            try
            {
                if (_g.Member == null)
                    return;

                if (_g.MemberPhotoRequire.IsPhotoRequiredSite)
                {
                    if (_g.MemberPhotoRequire.IsMemberBlocked(_g.Member, _g.Session))
                    {
                        if (_g.AppPage.ControlName != "MemberPhotoSingleUpload" && _g.AppPage.PageName != "ArticleView" && _g.AppPage.PageName != "ArticleDefault" && _g.AppPage.PageName != "FAQMain" && _g.AppPage.ControlName != "Logoff" && _g.AppPage.ControlName != "Splash")
                        {
                            //Fix for OI-1603 - Login to Spark.com by members who have no approved photo and have not accepted to the counter fraud pledge causes infinite redirects.
                            if (_g.AppPage.ControlName == "AttributeEdit" && Request.QueryString["Attribute"] != null &&
                                                    Request.QueryString["Attribute"] == "PledgeToCounterFroud")
                                return;
                            _g.Transfer("/Applications/MemberProfile/MemberPhotoSingleUpload.aspx");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _g.ProcessException(ex);
            }
        }

        private void RedirectEmailVerification()
        {

            try
            {
                if (_g.Member == null)
                    return;

                if (_g.EmailVerify.EnableEmailVerification)
                {
                    if (_g.EmailVerify.IsMemberBlocked(_g.Member))
                    {
                        if (_g.AppPage.ControlName != "VerifyEmail" && _g.AppPage.PageName != "ArticleView" && _g.AppPage.PageName != "ArticleDefault" && _g.AppPage.PageName != "FAQMain" && _g.AppPage.ControlName != "Logoff" && _g.AppPage.ControlName != "Splash")
                            _g.Transfer("/Applications/MemberServices/VerifyEmail.aspx");
                    }
                }

            }
            catch (Exception ex)
            {
                Matchnet.Web.Applications.PremiumServices.VelocityHandler.DebugTrace("default.aspx", "Exception:" + ex.ToString(), _g);
                _g.ProcessException(ex);
            }
        }

        private void RedirectAdminSuspended()
        {
            //check the user's GlobalStatusMask to see if they've been admin suspended, and redirect to the logon page with an 
            //error message. Previously we only checked this mask on logon, which enabled members who had been suspended to still access the site 
            //until their next logon. 

            try
            {
                GlobalStatusMask globalStatusMask = (GlobalStatusMask)_g.Member.GetAttributeInt(_g.TargetBrand, WebConstants.ATTRIBUTE_NAME_GLOBALSTATUSMASK);

                if (globalStatusMask != (GlobalStatusMask)Constants.NULL_INT && ((globalStatusMask & GlobalStatusMask.AdminSuspended) == GlobalStatusMask.AdminSuspended) && !_g.AppPage.ControlName.Equals("Logon"))
                {
                    _g.Transfer("/Applications/Logon/Logon.aspx?rc=ADMIN_SUSPENDED_ACTION");
                }
            }
            catch (Exception ex)
            {
                _g.ProcessException(ex);
            }
        }

        private void RenderPage()
        {
            // load the Content Page
            Page page = _g.AppPage;

            // Hack for UPS (Back button functionality)
            if (!(page.ControlName.ToLower().Contains("subscribenew") || page.ControlName.ToLower().Contains("premiumservices")))
            {
                // Delete the cookie
                HttpCookie die = new HttpCookie("SparkUPS");

                die.Expires = DateTime.Now.AddDays(-1d);
                HttpContext.Current.Response.Cookies.Add(die);
            }

            // make a new templatebase
            layoutTemplate = GetLayoutTemplate(page);
            _g.LayoutTemplateBase = layoutTemplate;

            //Get the path for the correct notification template
            NotificationTemplatePath = page.GetFullNotificationTemplatePath();

            // get the app control
            string controlPath = page.GetFullControlPath();

            controlPath = getOverrideControlPath(page, controlPath);
            //load application control to template
            // Matchnet.Web.Applications.PremiumServices.VelocityHandler.DebugTrace("Default.aspx:RenderPage" + controlPath, "Load control from referrer: " + _g.GetReferrerUrl(),_g);
            FrameworkControl appControl = (FrameworkControl)LoadControl(controlPath);
            appControl.AppInit();
            // Matchnet.Web.Applications.PremiumServices.VelocityHandler.DebugTrace("Default.aspx:RenderPage" + controlPath, "AppInit " , _g);

            //add app control
            layoutTemplate.AppControl = appControl;

            // add the templatebase to the current page
            this.Controls.Add(layoutTemplate);

            // Matchnet.Web.Applications.PremiumServices.VelocityHandler.DebugTrace("Default.aspx:RenderPage" + controlPath, "Add layout template ", _g);
            Context.RewritePath(GetPath());
            if (_g.EmailVerify.IsMemberBlocked(_g.Member))
            {
                _g.HeaderControl.ShowBlankHeader(Matchnet.Web.Framework.Ui.HeaderImageType.Default);
                _g.SetTopAuxMenuVisibility(false, true);
                _g.BreadCrumbTrailHeader.Visible = false;
                _g.BreadCrumbTrailFooter.Visible = false;

            }
        }

        private LayoutTemplateBase GetLayoutTemplate(Page pPage)
        {
            string layoutTemplatePath;

            int layoutTemplateID = Conversion.CInt(Request["LayoutTemplateID"]);

            //validate layoutTemplateID
            if (!Enum.IsDefined(typeof(LayoutTemplate), layoutTemplateID))
            {
                layoutTemplateID = Constants.NULL_INT;
            }

            if (layoutTemplateID == (int)LayoutTemplate.PopupProfile || layoutTemplateID == (int)LayoutTemplate.StandardAllowFrames
                || layoutTemplateID == (int)LayoutTemplate.Plain)
            {
                //these layouttemplates have been removed, so don't honor requests for them
                layoutTemplateID = Constants.NULL_INT;
            }

            //Ignore LayoutTemplateID param if we are in the Home app
            if (layoutTemplateID != Constants.NULL_INT && pPage.App.ID != (int)WebConstants.APP.Home)
            {

                string layoutTemplateName = Enum.GetName(typeof(LayoutTemplate), layoutTemplateID);
                _g.LayoutTemplate = (LayoutTemplate)layoutTemplateID;
                layoutTemplatePath = Matchnet.Content.ServiceAdapters.Page.GetFullLayoutTemplatePath(layoutTemplateName);

            }
            else
            {
                _g.LayoutTemplate = (LayoutTemplate)Enum.Parse(typeof(LayoutTemplate), pPage.LayoutTemplateName);
                layoutTemplatePath = pPage.GetFullLayoutTemplatePath();
            }

            if (string.IsNullOrEmpty(Request.QueryString["SkipLayoutOverride"]))
            {
                //added for site redesign
                int overrideLayoutTemplateID = getLayoutTemplateOverride(pPage);
                //
                if (overrideLayoutTemplateID != Constants.NULL_INT)
                {
                    string layoutTemplateName = Enum.GetName(typeof(LayoutTemplate), overrideLayoutTemplateID);
                    _g.LayoutTemplate = (LayoutTemplate)overrideLayoutTemplateID;
                    layoutTemplatePath = Matchnet.Content.ServiceAdapters.Page.GetFullLayoutTemplatePath(layoutTemplateName);
                }
            }

            return (LayoutTemplateBase)LoadControl(layoutTemplatePath);
        }

        private void Page_PreRender(object sender, EventArgs e)
        {
            //Insert any messages/errors using the layout-specific notification template
            Lib.Util util = new Lib.Util();

            bool emailVerifiedMsg = Conversion.CBool(_g.Session.GetString(Web.Applications.MemberProfile.EmailVerifyHelper.SESSION_EMAIL_VERIFY_NOTIFICATION_DISPLAY, "false"));
            if (emailVerifiedMsg)
            {
                _g.Notification.AddMessage(Web.Applications.MemberProfile.EmailVerifyHelper.RESX_EMAIL_VERIFY_CONFIRMATION);
                _g.Session.Remove(Web.Applications.MemberProfile.EmailVerifyHelper.SESSION_EMAIL_VERIFY_NOTIFICATION_DISPLAY);
            }

            if (_g.Member != null)
            {
                ComposeHandler.SendQuickMessage(_g);
            }

            util.InsertNotifications(layoutTemplate, NotificationTemplatePath);

            if (Boolean.Parse(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("PAGEVIEWSVC_ENABLED")))
            {
                Matchnet.PageView.ServiceAdapters.PageViewSA.Instance.AddImpression(_g.Session.GetInt(WebConstants.SESSION_PROPERTY_NAME_MEMBERID, 0),
                    _g.Brand.BrandID,
                    _g.Session.Key,
                    _g.AppPage.ID,
                    _g.ClientIP,
                    new Int32[0]);
            }
        }

        private void Page_Unload(object sender, EventArgs e)
        {
            //The line below is performed in the Application_EndRequest method of dispatch.
            //SessionSA.Instance.SaveSession(_userSession, Constants.NULL_STRING, true);

            // failure in Page_Load can result in GlobalContext (_g) not being initialized.
            // check for null before calling Clear()
            if (null != _g)
            {
                _g.Notification.Clear();
            }
        }

        private void InitializeViewState()
        {

            if (_g.AppPage.App.ID == (int)WebConstants.APP.FreeTextApproval ||
                _g.AppPage.App.ID == (int)WebConstants.APP.Resource ||
                _g.AppPage.App.ID == (int)WebConstants.APP.PixelAdministrator ||
                _g.AppPage.App.ID == (int)WebConstants.APP.Options ||
                _g.AppPage.App.ID == (int)WebConstants.APP.DNE
                )
                this.EnableViewState = true;
        }

        private void InitializeCultureInfo()
        {
            System.Globalization.DateTimeFormatInfo dtfInfo = _g.Brand.Site.CultureInfo.DateTimeFormat;

            if (_g.Brand.Site.CultureInfo.LCID == Matchnet.Lib.ConstantsTemp.LOCALE_US_ENGLISH)
            {

                dtfInfo.FullDateTimePattern = "MM/dd/yyyy hh:mm tt";
            }
            else
            {
                dtfInfo.FullDateTimePattern = "dd/MM/yyyy hh:mm tt";
            }

            Thread.CurrentThread.CurrentUICulture = _g.Brand.Site.CultureInfo;
            Thread.CurrentThread.CurrentCulture = Thread.CurrentThread.CurrentUICulture;
        }

        //site redesign hacks
        private int getLayoutTemplateOverride(Page pPage)
        {
            int layouttemplateid = Constants.NULL_INT;

            if (pPage.ID == (int)WebConstants.PageIDs.CheckIM)
                return layouttemplateid;

            int layoutImpersonateMemberID;


            //visitors continue enjoying old layout
            if (_g.Member == null)
            {
                //100%
                layoutImpersonateMemberID = 9;
                //return layouttemplateid;
            }
            else
            {
                if (_g.IsAdmin && (_g.ImpersonateContext && pPage.App.ID != (int)WebConstants.APP.BillingInformation))
                {
                    // AnaliticsScenarioBase scenarioAdmin = new AnaliticsScenarioBase("JDATEREDESIGN", _g.TargetBrand.Site.Community.CommunityID, _g.TargetBrand.Site.SiteID, _g.TargetMember.MemberID);
                    // Scenario scenAdmin = Configuration.ServiceAdapters.RuntimeSettings.GetAnaliticsScenario(scenarioAdmin);
                    // if (scenAdmin == Scenario.A)
                    return layouttemplateid;

                }
                else
                {
                    layoutImpersonateMemberID = _g.Member.MemberID;
                }
            }

            //Override to support popup profiles
            if ((pPage.ControlName.ToLower() == "viewtabbedprofile20" || pPage.ControlName.ToLower() == "viewprofile") && pPage.App.ID == (int)WebConstants.APP.MemberProfile
                && Matchnet.Web.Framework.Ui.BreadCrumbHelper.IsProfilePopupEnabledForSite(_g.Brand) &&
                Conversion.CInt(Request["LayoutTemplateID"]) == (int)LayoutTemplate.WidePopup)
            {
                _g.LayoutVersion = WebConstants.LayoutVersions.versionWide;
                _g.IsSite20Enabled = true;
                return layouttemplateid;
            }

            string requestOverride = HttpContext.Current.Request["LayoutOverride"];
            bool overrideAllPagesLayout = false;
            bool overrideSelectedPagesLayout = false;
            if (String.IsNullOrEmpty(requestOverride))
            {
                requestOverride = _g.Session["LayoutOverride"];
            }
            if (!String.IsNullOrEmpty(requestOverride))
            {
                switch (requestOverride.ToLower())
                {
                    case WebConstants.LAYOUT_OVERRIDE_PAGES:
                        overrideSelectedPagesLayout = true;
                        _g.Session.Add(WebConstants.SESSION_LAYOUT_OVERRIDE, WebConstants.LAYOUT_OVERRIDE_PAGES, SessionPropertyLifetime.Temporary);
                        break;
                    case WebConstants.LAYOUT_OVERRIDE_ALL:
                        overrideAllPagesLayout = true;
                        _g.Session.Add("LayoutOverride", WebConstants.LAYOUT_OVERRIDE_ALL, SessionPropertyLifetime.Temporary);
                        break;
                    case WebConstants.LAYOUT_OVERRIDE_NONE:
                        _g.Session.Add("LayoutOverride", WebConstants.LAYOUT_OVERRIDE_NONE, SessionPropertyLifetime.Temporary);
                        return layouttemplateid;
                    default:
                        break;
                }
            }

            if (overrideAllPagesLayout)
            {
                _g.LayoutVersion = WebConstants.LayoutVersions.versionWide;
                _g.IsSite20Enabled = true;
                return (int)Content.ValueObjects.PageConfig.LayoutTemplate.Wide2Columns;
            }

            string pageName = pPage.ControlName.ToLower();

            if (pageName == "sparkhome" && pPage.App.ID == (int)WebConstants.APP.Home)
            {
                layouttemplateid = (int)Content.ValueObjects.PageConfig.LayoutTemplate.Wide2Columns;
                ControlVersion version = Matchnet.Web.Applications.Home.HomeUtil.GetHomepageVersion(_g.Member, _g.Brand);
                if (version == ControlVersion.Version40a || version == ControlVersion.Version40b)
                {
                    layouttemplateid = (int)Content.ValueObjects.PageConfig.LayoutTemplate.WideT;
                }
                else if (version == ControlVersion.Version50 || version == ControlVersion.Version50b)
                {
                    layouttemplateid = (int)Content.ValueObjects.PageConfig.LayoutTemplate.FullWidthColumn;
                }
            }
            else if (pageName.Contains("minisearchapi"))
                layouttemplateid = (int)Content.ValueObjects.PageConfig.LayoutTemplate.Binary;
            else if (pPage.App.ID == (int)WebConstants.APP.Email)
                layouttemplateid = (int)Content.ValueObjects.PageConfig.LayoutTemplate.Wide2Columns;
            else if (pageName.Contains("registration") && pPage.App.ID == (int)WebConstants.APP.MemberProfile && !pageName.Contains("registrationwelcome") && _g.Member != null)
                layouttemplateid = (int)Content.ValueObjects.PageConfig.LayoutTemplate.Wide2Columns;
            else if (pageName.Contains("registrationwelcome") && _g.Member != null && !pageName.Contains("onepage"))
                layouttemplateid = (int)Content.ValueObjects.PageConfig.LayoutTemplate.WideSingleColumn;

            if (layouttemplateid == (int)Content.ValueObjects.PageConfig.LayoutTemplate.Wide2Columns || layouttemplateid == (int)Content.ValueObjects.PageConfig.LayoutTemplate.WideSingleColumn || layouttemplateid == (int)Content.ValueObjects.PageConfig.LayoutTemplate.Wide2ColumnsJMeter || layouttemplateid == (int)Content.ValueObjects.PageConfig.LayoutTemplate.WideT)
                _g.LayoutVersion = WebConstants.LayoutVersions.versionWide;

            if (pageName == "sendinvitation")
            {
                if (_g.LayoutTemplate == LayoutTemplate.Standard)
                {
                    layouttemplateid = (int)Content.ValueObjects.PageConfig.LayoutTemplate.Wide2Columns;
                }
            }

            if (pageName == "messagesent")
            {
                if (_g.LayoutTemplate == LayoutTemplate.Standard)
                {
                    layouttemplateid = (int)Content.ValueObjects.PageConfig.LayoutTemplate.Wide2Columns;
                }
            }

            if (pageName == "photogallery20" && PhotoGalleryManager.Instance.IsPhotoGallery30Enabled(_g.Brand))
            {
                layouttemplateid = (int)Content.ValueObjects.PageConfig.LayoutTemplate.WideSingleColumn;
            }

            _g.IsSite20Enabled = true;

            return layouttemplateid;
        }

        private string getOverrideControlPath(Page pPage, string path)
        {
            string overridePath = path;
            string pageName = pPage.ControlName.ToLower();

            if (pageName == "sparkhome")
            {
                ControlVersion version = Matchnet.Web.Applications.Home.HomeUtil.GetHomepageVersion(_g.Member, _g.Brand);
                switch (version)
                {
                    case ControlVersion.Version30:
                        overridePath = "/Applications/Home/SparkHome30.ascx";
                        break;
                    case ControlVersion.Version40a:
                        overridePath = "/Applications/Home/SparkHome40.ascx";
                        break;
                    case ControlVersion.Version40b:
                        overridePath = "/Applications/Home/SparkHome40b.ascx";
                        break;
                    case ControlVersion.Version50:
                    case ControlVersion.Version50b:
                        overridePath = "/Applications/Home/SparkHome50.ascx";
                        break;
                }
            }

            if (pageName == "compose")
                overridePath = "/Applications/Email/Compose20.ascx";
            else if (pageName == "foldersettings")
                overridePath = "/Applications/Email/FolderSettings20.ascx";
            else if (pageName == "tease")
            {
                overridePath = "/Applications/Email/Flirt20.ascx";

                //TL: 4/16/2010
                //temp homepage override while developing new homepage for Mingle, which is still being migrated
                if (Matchnet.Conversion.CBool(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("TEMPORARY_OVERRIDE_TO_USE_COLLAPSIBLE_FLIRT_PAGE", _g.Brand.Site.Community.CommunityID, _g.Brand.Site.SiteID)))
                    overridePath = "/Applications/Email/FlirtCollapsible.ascx";
            }
            else if (pageName == "subscribenew")
                overridePath = "/Applications/subscription/subscribenew20.ascx";
            else if (pageName == "logon")
                overridePath = "/Applications/Logon/Logon20.ascx";
            else if (pageName == "logoff")
                overridePath = "/Applications/Logon/Logoff20.ascx";
            else if (pageName == "teasesent" || pageName == "teasesentalready" || pageName == "teasetoomany" || pageName == "messagesent")
                overridePath = "/Applications/Email/" + pPage.ControlName + "20.ascx";
            else if (pageName == "messagesettingsnew")
            {
                ControlVersion version = Matchnet.Web.Applications.Email.EmailUtil.GetMessageSettingsVersion(_g.Member, _g.Brand);
                if (version == ControlVersion.Version30)
                    overridePath = "/Applications/Email/MessageSettingsNew30.ascx";
                else
                    overridePath = "/Applications/Email/MessageSettingsNew20.ascx";
            }

            if (pageName == "registration" || pageName == "registrationstep1" || pageName == "registrationstep2" || pageName == "registrationstep3"
                || pageName == "registrationstep4" || pageName == "registrationstep5")
            {
                overridePath = "/Applications/Registration/Registration.ascx";
            }

            if (pageName == "question" && _g.AppPage.App.ID == (int)WebConstants.APP.QuestionAnswer)
            {
                if (_settingsManager.GetSettingBool(SettingConstants.ENABLE_QNA_HOME_PAGE, _g.Brand))
                {
                    overridePath = "/Applications/QuestionAnswer/Home.ascx";
                }
            }

            string urlVal = FrameworkGlobals.GetCookie(WebConstants.SESSION_PROPERTY_NAME_BACKTORESULT_URL, null, true);
            if (urlVal != null && urlVal != "")
            {
                //any page that links to profile that does not set the back to results
                switch (pageName)
                {
                    //case "mailbox":
                    case "lookupprofile":
                    case "upinstantcommunicator":
                    case "upchat":
                        FrameworkGlobals.RemoveCookie(WebConstants.SESSION_PROPERTY_NAME_BACKTORESULT_URL);
                        break;
                }
            }

            if (pageName.ToLower() == "searchresults20")
            {
                if (BetaHelper.IsSearchRedesign30Enabled(_g))
                {
                    overridePath = "/Applications/Search/SearchResults30.ascx";
                }
            }

            if (pageName.ToLower() == "membersonline20")
            {
                if (MembersOnlineManager.Instance.IsMOLRedesign30Enabled(_g.Member, _g.Brand))
                {
                    overridePath = "/Applications/MembersOnline/MembersOnline30.ascx";    
                }
            }

            if (pageName == "photogallery20" && PhotoGalleryManager.Instance.IsPhotoGallery30Enabled(_g.Brand))
            {
                overridePath = "/Applications/PhotoGallery/PhotoGallery30.ascx";
            }

            return overridePath;
        }

        private void SaveUserSessionDetails()
        {
            SessionProperty visitorSessionProperty = _g.Session.Properties[WebConstants.LAST_SAVED_VISITOR_SESSIONDETAILS_SESSION_KEY] as SessionProperty;
            bool saveVisitorSessionDetails = false;
            if (visitorSessionProperty == null)
            {
                saveVisitorSessionDetails = true;
            }
            else if (Convert.ToString(visitorSessionProperty.Val) != _g.Session.Key.ToString())
            {
                saveVisitorSessionDetails = true;
            }

            // Member level tracking info
            _g.Session.Add(Key.TrackingLastApplication.ToString(), (int)Spark.Common.MemberLevelTracking.Application.FullSite, SessionPropertyLifetime.TemporaryDurable);
            _g.Session.Add(Key.TrackingRegApplication.ToString(), MemberLevelTrackingHelper.GetOs(Request), SessionPropertyLifetime.TemporaryDurable);
            _g.Session.Add(Key.TrackingRegFormFactor.ToString(), MemberLevelTrackingHelper.GetFormFactor(Request), SessionPropertyLifetime.TemporaryDurable);

            if (saveVisitorSessionDetails)
            {
                int nextVisitorSessionTrackingID = SessionSA.Instance.GetNextSessionTrackingID(_g.Session);
                _g.Session.Add(WebConstants.SESSION_TRACKING_ID, Convert.ToString(nextVisitorSessionTrackingID), Matchnet.Session.ValueObjects.SessionPropertyLifetime.TemporaryDurable);

                _g.Session.Add(WebConstants.LAST_SAVED_VISITOR_SESSIONDETAILS_SESSION_KEY, _g.Session.Key.ToString(), Matchnet.Session.ValueObjects.SessionPropertyLifetime.TemporaryDurable);
                SessionSA.Instance.SaveSessionDetails(_g.Session, _g.Brand.Site.SiteID, SessionType.Visitor);

                //System.Diagnostics.EventLog.WriteEntry("WWW", "Visitor data saved, Key=" + _g.Session.Key.ToString() + ", ID=" + Convert.ToString(nextVisitorSessionTrackingID), System.Diagnostics.EventLogEntryType.Warning);
            }

            SessionProperty memberSessionProperty = _g.Session.Properties[WebConstants.LAST_SAVED_MEMBER_SESSIONDETAILS_SESSION_KEY] as SessionProperty;
            bool saveMemberSessionDetails = false;
            if (memberSessionProperty == null)
            {
                if (_g.Session.Properties[WebConstants.SESSION_PROPERTY_NAME_MEMBERID] != null)
                {
                    saveMemberSessionDetails = true;
                }
            }
            else if (Convert.ToString(memberSessionProperty.Val) != _g.Session.Key.ToString())
            {
                if (_g.Session.Properties[WebConstants.SESSION_PROPERTY_NAME_MEMBERID] != null)
                {
                    saveMemberSessionDetails = true;
                }
            }

            if (saveMemberSessionDetails)
            {
                _g.Session.Add(WebConstants.LAST_SAVED_MEMBER_SESSIONDETAILS_SESSION_KEY, _g.Session.Key.ToString(), Matchnet.Session.ValueObjects.SessionPropertyLifetime.TemporaryDurable);
                SessionSA.Instance.SaveSessionDetails(_g.Session, _g.Brand.Site.SiteID, SessionType.Member);
            }
        }

        #endregion

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new EventHandler(this.Page_Load);
            this.PreRender += new EventHandler(this.Page_PreRender);
            this.Unload += new EventHandler(this.Page_Unload);
        }
        #endregion
    }
}
