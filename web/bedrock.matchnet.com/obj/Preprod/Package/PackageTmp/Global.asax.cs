using System;
using System.Collections;
using System.Web;
using System.Web.SessionState;
using System.Diagnostics;

using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Diagnostics;
using Matchnet.PageView.ServiceAdapters;
using Matchnet.SABackgroundWorker;
using Spark.Common.Adapter;


namespace global.matchnet.com 
{
	/// <summary>
	/// Summary description for Global.
	/// </summary>
	public class Global : System.Web.HttpApplication
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		public Global()
		{
			InitializeComponent();
		}	
		
		protected void Application_Start(Object sender, EventArgs e)
		{
		}
 
		protected void Session_Start(Object sender, EventArgs e)
		{

		}

		protected void Application_BeginRequest(Object sender, EventArgs e)
		{

		}

		protected void Application_EndRequest(Object sender, EventArgs e)
		{
            OrderHistoryServiceWebAdapter.CloseProxyInstance();
            AccessServiceWebAdapter.CloseProxyInstance();
            RenewalServiceWebAdapter.CloseProxyInstance();
            PaymentProfileServiceWebAdapter.CloseProxyInstance();
            CatalogServiceWebAdapter.CloseProxyInstance();
            DiscountServiceWebAdapter.CloseProxyInstance();
        }

		protected void Application_AuthenticateRequest(Object sender, EventArgs e)
		{

		}

		protected void Application_Error(Object sender, EventArgs e)
		{
			try
			{
				//Display the generic error page if we aren't in dev mode.
				if (!Convert.ToBoolean(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("IS_DEVELOPMENT_MODE")))
				{
					Exception ex = Server.GetLastError();
					if(ex != null)
					{
						ContextExceptions _ContextExceptions = new ContextExceptions();
						_ContextExceptions.LogException(ex,true);
					}

					FrameworkGlobals.RedirectToErrorPage(true);			
				}
			}
			catch
			{
				Exception ex = Server.GetLastError();
				if(ex != null)
				{
					ContextExceptions _ContextExceptions = new ContextExceptions();
					_ContextExceptions.LogException(ex);
				}
				FrameworkGlobals.RedirectToErrorPage(true);	
			}
		}

		protected void Session_End(Object sender, EventArgs e)
		{

		}

		protected void Application_End(Object sender, EventArgs e)
		{

        }

		#region Web Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.components = new System.ComponentModel.Container();
		}
		#endregion
	}
}
