﻿//initial definitions for manager and likes
function FBLikeObj() {
    this.id = 0;
    this.name = '';
    this.category = '';
    this.categoryID = 0;
    this.categoryGroupID = 0;
    this.picture = '';
    this.link = '';
    this.type = '';
    this.selected = false;
    this.chkEditID = '';
    this.saved = false;
    this.approved = false;
    this.imageID = function () {
        return "imgFBLike" + this.id;
    }
    this.checkedHTML = function () {
        if (this.selected) {
            return "checked=\"checked\"";
        }
        else {
            return "";
        }
    }
    this.selectedClass = function () {
        if (this.selected) {
            return fbLikeManager.cssClassLikeItemSelected;
        }
        else {
            return "";
        }
    }
    this.savedClass = function () {
        if (this.saved) {
            return fbLikeManager.cssClassLikeItemSaved;
        }
        else {
            return "";
        }
    }
    this.approvedClass = function () {
        if (this.approved && fbLikeManager.isOwnProfile) {
            return fbLikeManager.cssClassLikeItemApproved;
        }
        else {
            return "";
        }
    }
}

function FBLikeCategoryObj() {
    this.categoryName = '';
    this.categoryID = 0;
}

function FBLikeCategoryGroupObj() {
    this.categoryGroupID = 0;
    this.categoryGroupName = '';
    this.containerID = '';
    this.containerEditID = '';
    this.fbLikeCategoryObjList = [];
    this.fbLikeObjList = [];
    this.fbLikeInitialCountAll = 0;
    this.fbLikeFullyLoaded = true;
}

FBLikeCategoryGroupObj.prototype.getFBCategory = function (category) {
    var categoryObj = null;
    if (this.fbLikeCategoryObjList.length > 0) {
        for (var i = 0; i < this.fbLikeCategoryObjList.length; i++) {
            if (this.fbLikeCategoryObjList[i].categoryName.toLowerCase() == category.toLowerCase()) {
                categoryObj = this.fbLikeCategoryObjList[i];
                break;
            }
        }
    }

    return categoryObj;
}
FBLikeCategoryGroupObj.prototype.getFBCategoryByID = function (categoryID) {
    var categoryObj = null;
    if (this.fbLikeCategoryObjList.length > 0) {
        for (var i = 0; i < this.fbLikeCategoryObjList.length; i++) {
            if (this.fbLikeCategoryObjList[i].categoryID == categoryID) {
                categoryObj = this.fbLikeCategoryObjList[i];
                break;
            }
        }
    }

    return categoryObj;
}
FBLikeCategoryGroupObj.prototype.addFBLike = function (fbLikeObj, ignoreCategoryCheck) {
    if (fbLikeObj != null && fbLikeObj.id > 0) {
        var existingFBLikeObj = null;
        if (fbLikeObj.saved != true) {
            existingFBLikeObj = this.getFBLike(fbLikeObj.id);
        }
        if (existingFBLikeObj == null) {
            if (ignoreCategoryCheck) {
                spark.util.addItem(this.fbLikeObjList, fbLikeObj);
                return true;
            }
            else {
                var categoryObj = null;
                if (fbLikeObj.categoryID > 0) {
                    categoryObj = this.getFBCategoryByID(fbLikeObj.categoryID);
                }
                else {
                    categoryObj = this.getFBCategory(fbLikeObj.category);
                }

                if (categoryObj != null) {
                    fbLikeObj.categoryID = categoryObj.categoryID;
                    //fbLikeObj.category = categoryObj.categoryName;
                    spark.util.addItem(this.fbLikeObjList, fbLikeObj);
                    return true;
                }
                else {
                    return false;
                }
            }
        }
        else {
            return false;
        }
    }
    else {
        return false;
    }
}
FBLikeCategoryGroupObj.prototype.getFBLike = function (id) {
    var fbLikeObj = null;
    if (id != null && id > 0) {
        if (this.fbLikeObjList.length > 0) {
            for (var i = 0; i < this.fbLikeObjList.length; i++) {
                if (this.fbLikeObjList[i].id == id) {
                    fbLikeObj = this.fbLikeObjList[i];
                    break;
                }
            }
        }
    }

    return fbLikeObj;
}
FBLikeCategoryGroupObj.prototype.getFBLikeCountAll = function () {
    if (this.fbLikeFullyLoaded) {
        return this.getFBLikeCountLoaded();
    }
    else {
        return this.fbLikeInitialCountAll;
    }
}
FBLikeCategoryGroupObj.prototype.getFBLikeCountLoaded = function () {
    return this.fbLikeObjList.length;
}
FBLikeCategoryGroupObj.prototype.getFBLikeCountSaved = function () {
    var savedCount = 0;
    if (this.fbLikeObjList.length > 0) {
        for (var j = 0; j < this.fbLikeObjList.length; j++) {
            if (this.fbLikeObjList[j].saved == true) {
                savedCount++;
            }
        }
    }
    return savedCount;
}

function FBLikeManager() {
    this.memberID = 0;
    this.isOwnProfile = false;
    this.fbConnectStatusID = 0;
    this.fbUserID = 0;
    this.fbHasSavedLikes = false;
    this.fbHasUnapprovedLikes = false;
    this.fbLikesPerRow = 0;
    this.fbLikeCategoryGroupObjList = [];
    this.likesWidgetControlContainerID = '';
    this.likesContainerID = '';
    this.likesContainerEditID = '';
    this.likesRegionContainerEditID = '';
    this.widgetNoFBDataContainerID = '';
    this.widgetNonConnectedContainerID = '';
    this.widgetNoSavedDataContainerID = '';
    this.widgetHasDataContainerID = '';
    this.widgetHasDataEditContainerID = '';
    this.pendingApprovalContainerID = '';
    this.fbLogoutLinkIDList = null;
    this.tabContainerId = '';
    this.isBlocked = false;
    this.blockDivId = '';
    this.scrollToDiv = null;
    this.editHasDataLoginCancelPromptContainerID = '';
    this.showEditHashDataLoginCancelPrompt = false;
    this.radioPublishID = '';
    this.fbNotPublishedStatusContainerID = '';
    this.fbPublishedStatusContainerID = '';
    this.connectStatusIDNotConnected = 0;
    this.connectStatusIDNoFBData = 0;
    this.connectStatusIDNoSavedData = 0;
    this.connectStatusIDShowData = 0;
    this.connectStatusIDHideData = 0;
    this.updateInProgress = false;
    this.ajaxTotalLength = 0;
    this.ajaxCurrentNum = 0;
    this.isEdit = false;
    this.isVerbose = false;
    this.textLikesCollapse = "";
    this.textLikesExpand = "";
    this.textCategoriesCollapse = "";
    this.textCategoriesExpand = "";
    this.textLogoutMessage = "";
    this.cssClassLikeItemSelected = "selected";
    this.cssClassLikeItemSaved = "saved";
    this.cssClassLikeItemApproved = "approved";
    this.templateGroup = "<li id=\"{{containerID}}\" class=\"clearfix\"><h3>{{categoryGroupName}}</h3><ol></ol></li>";
    this.templateGroupEdit = "<li id=\"{{containerEditID}}\" class=\"clearfix\"><h3>{{categoryGroupName}}</h3><ol></ol></li>";
    this.templateLike = "<li class=\"{{approvedClass}}\"><span class=\"spr s-icon-check-active\"></span><a title=\"{{name}}\" href=\"{{link}}\" target=\"_blank\"><span class=\"image\"><img id=\"{{imageID}}\" src=\"{{picture}}\"></span><span class=\"name\">{{name}}</span></a></li>";
    this.templateLikeEdit = "<li class=\"{{approvedClass}} {{selectedClass}}\"><span class=\"spr s-icon-check-active\"></span><a title=\"{{name}}\" href=\"{{link}}\"><span class=\"image\"><img id=\"{{imageID}}\" src=\"{{picture}}\"></span><span class=\"name\">{{name}}</span></a><input id=\"{{chkEditID}}\" type=\"checkbox\" {{checkedHTML}} onclick=\"fbLikeManager.updateLikeSelect('{{chkEditID}}','{{id}}', '{{categoryGroupID}}');\"></input></li>";
    this.addFBLike = function (fbLikeObj) {
        var fbLikeCategoryGroupObj = null;
        if (fbLikeObj.categoryGroupID > 0) {
            fbLikeCategoryGroupObj = fbLikeManager.getFBLikeCategoryGroupByID(fbLikeObj.categoryGroupID);
        }
        else {
            fbLikeCategoryGroupObj = fbLikeManager.getFBLikeCategoryGroup(fbLikeObj.category);
        }

        if (fbLikeCategoryGroupObj != null) {
            fbLikeObj.categoryGroupID = fbLikeCategoryGroupObj.categoryGroupID;
            return fbLikeCategoryGroupObj.addFBLike(fbLikeObj, false);
        }
        else {
            return false;
        }
    }
    this.getFBLike = function (id) {
        var fbLikeObj = null;
        if (this.fbLikeCategoryGroupObjList.length > 0) {
            for (var i = 0; i < this.fbLikeCategoryGroupObjList.length; i++) {
                fbLikeObj = this.fbLikeCategoryGroupObjList[i].getFBLike(id);
                if (fbLikeObj != null) {
                    break;
                }
            }
        }
        return fbLikeObj;
    }
    this.getFBLikeCategoryGroup = function (category) {
        var fbLikeCategoryGroupObj = null;
        if (category != null && category != '') {
            if (this.fbLikeCategoryGroupObjList.length > 0) {
                for (var i = 0; i < this.fbLikeCategoryGroupObjList.length; i++) {
                    if (this.fbLikeCategoryGroupObjList[i].getFBCategory(category) != null) {
                        fbLikeCategoryGroupObj = this.fbLikeCategoryGroupObjList[i];
                        break;
                    }
                }
            }
        }

        return fbLikeCategoryGroupObj;
    }
    this.getFBLikeCategoryGroupByID = function (id) {
        var fbLikeCategoryGroupObj = null;
        if (id > 0) {
            if (this.fbLikeCategoryGroupObjList.length > 0) {
                for (var i = 0; i < this.fbLikeCategoryGroupObjList.length; i++) {
                    if (this.fbLikeCategoryGroupObjList[i].categoryGroupID == id) {
                        fbLikeCategoryGroupObj = this.fbLikeCategoryGroupObjList[i];
                        break;
                    }
                }
            }
        }

        return fbLikeCategoryGroupObj;
    }
    this.ajaxBlockLoading = function (blockID) {
        this.ajaxBlock('<div class="loading spinner-only" /><blockquote>Loading...</blockquote>', blockID);
    }
    this.ajaxBlockSaving = function (blockID) {
        this.ajaxBlock('<div class="loading spinner-only" /><blockquote>Saving...</blockquote>', blockID);
    }
    this.ajaxBlock = function (_message, blockID) {
        if (!this.isBlocked) {
            this.isBlocked = true;
            var blockParams = {
                message: _message,
                centerY: false,
                css: { backgroundColor: 'transparent', border: 'none', height: '85px', top: '30%' },
                overlayCSS: { backgroundColor: '#fff', opacity: 0.8 }
            };
            var blockObj = $j('#' + blockID);
            blockObj.block(blockParams);
        }
    }
    this.ajaxUnBlock = function (blockID) {
        if (this.isBlocked) {
            var blockObj = $j('#' + blockID);
            blockObj.unblock();
        }
        this.isBlocked = false;
    }
    this.GoToByScroll = function () {
        if (null != this.scrollToDiv) {
            $j('html,body').animate({ scrollTop: this.scrollToDiv.offset().top }, 'fast');
        }
    }
    this.fbLikesConnectStatusInitialize = function () {
        try {
            if (this.isOwnProfile && this.fbConnectStatusID > 0 && this.fbConnectStatusID != this.connectStatusIDNotConnected) {
                FB.getLoginStatus(function (response) {
                    if (response.status === 'connected' || response.status === 'not_authorized') {
                        fbLikeManager.showFBLogout();
                    }
                });
            }
        }
        catch (e) {
            if (fbLikeManager.isVerbose) {
                alert(e);
            }
        }
    }
    this.fbLogout = function () {
        FB.logout(function (response) {
            // user is now logged out
            fbLikeManager.hideFBLogout();
            alert(fbLikeManager.textLogoutMessage);
        });
    }
    this.fbLikesConnect = function () {
        //fbLikeManager.ajaxBlockLoading();
        try {
            FB.getLoginStatus(function (response) {
                if (response.status === 'connected') {
                    fbLikeManager.fbLikesConnectSuccess(response);
                } else {
                    spark.facebook.fbAuth(function () { fbLikeManager.fbLikesConnectSuccess(response) }, function () { fbLikeManager.fbLikesConnectFail(response) }, 'user_birthday,user_photos,email,user_likes');
                }
            }, true);
        }
        catch (e) {
            if (fbLikeManager.isVerbose) {
                alert(e);
            }
            fbLikeManager.fbLikesConnectFail(null);
        }
    }
    this.fbLikesConnectSuccess = function (connectResponse) {
        fbLikeManager.loadEdit(true);
        fbLikeManager.showFBLogout();
    }
    this.fbLikesConnectFail = function (connectResponse) {
        if (fbLikeManager.fbHasSavedLikes == true) {
            fbLikeManager.showEditHashDataLoginCancelPrompt = true;
            fbLikeManager.loadEdit(false);
            //alert(fbLikeManager.textFBCancelPromptMessage);
        }
        else {
            //do nothing
            fbLikeManager.ajaxUnBlock();
        }
        fbLikeManager.hideFBLogout();
    }
    this.hideAllWidgets = function () {
        if (fbLikeManager.widgetNoFBDataContainerID != '') {
            $j('#' + fbLikeManager.widgetNoFBDataContainerID).hide();
        }
        if (fbLikeManager.widgetNonConnectedContainerID != '') {
            $j('#' + fbLikeManager.widgetNonConnectedContainerID).hide();
        }
        if (fbLikeManager.widgetNoSavedDataContainerID != '') {
            $j('#' + fbLikeManager.widgetNoSavedDataContainerID).hide();
        }
        if (fbLikeManager.widgetHasDataEditContainerID != '') {
            $j('#' + fbLikeManager.widgetHasDataEditContainerID).hide();
        }
        if (fbLikeManager.widgetHasDataContainerID != '') {
            $j('#' + fbLikeManager.widgetHasDataContainerID).hide();
        }
    }
    this.showFBLogout = function () {
        if (fbLikeManager.fbLogoutLinkIDList != null) {
            for (var i = 0; i < fbLikeManager.fbLogoutLinkIDList.length; i++) {
                $j('#' + fbLikeManager.fbLogoutLinkIDList[i]).show();
            }
        }
    }
    this.hideFBLogout = function () {
        if (fbLikeManager.fbLogoutLinkIDList != null) {
            for (var i = 0; i < fbLikeManager.fbLogoutLinkIDList.length; i++) {
                $j('#' + fbLikeManager.fbLogoutLinkIDList[i]).hide();
            }
        }
    }
    //redraws view widget
    this.loadView = function () {
        this.isEdit = false;
        fbLikeManager.hideAllWidgets();
        fbLikeManager.ajaxBlockLoading(this.blockDivId);
        var likesContainer = document.getElementById(fbLikeManager.likesContainerID);
        if (fbLikeManager.fbHasSavedLikes == true) {
            //has saved data view
            likesContainer.innerHTML = "";
            // generate HTML holder for each category defined in categories
            for (var i = 0; i < fbLikeManager.fbLikeCategoryGroupObjList.length; i++) {
                likesContainer.innerHTML += Mustache.to_html(fbLikeManager.templateGroup, fbLikeManager.fbLikeCategoryGroupObjList[i]);
            }

            // load saved likes
            var savedCount = 0;
            var unapprovedCount = 0;
            if (fbLikeManager.fbLikeCategoryGroupObjList.length > 0) {
                for (var i = 0; i < fbLikeManager.fbLikeCategoryGroupObjList.length; i++) {
                    var fbLikeCategoryGroupObj = fbLikeManager.fbLikeCategoryGroupObjList[i];
                    if (fbLikeCategoryGroupObj.fbLikeObjList.length > 0) {
                        var target = document.getElementById(fbLikeCategoryGroupObj.containerID).childNodes[1];
                        for (var j = 0; j < fbLikeCategoryGroupObj.fbLikeObjList.length; j++) {
                            var fbLikeObj = fbLikeCategoryGroupObj.fbLikeObjList[j];
                            if (fbLikeObj.saved == true) {
                                fbLikeObj.selected = true;
                                target.innerHTML += Mustache.to_html(this.templateLike, fbLikeObj);
                                savedCount++;
                                if (fbLikeObj.approved == false) {
                                    unapprovedCount++;
                                }
                            }
                            else {
                                fbLikeObj.selected = false;
                            }
                        }
                    }
                }
            }

            if (savedCount > 0) {
                $j('#' + fbLikeManager.widgetHasDataContainerID).show();
                if (unapprovedCount > 0) {
                    $j('#' + fbLikeManager.pendingApprovalContainerID).show();
                    fbLikeManager.fbHasUnapprovedLikes = true;
                }
                else {
                    $j('#' + fbLikeManager.pendingApprovalContainerID).hide();
                    fbLikeManager.fbHasUnapprovedLikes = false;
                }
                fbLikeManager.uiControl();
            }
            else {
                //no saved data view
                if (fbLikeManager.widgetNoSavedDataContainerID != '') {
                    $j('#' + fbLikeManager.widgetNoSavedDataContainerID).show();
                }
            }
        }
        else if (fbLikeManager.fbConnectStatusID == fbLikeManager.connectStatusIDNotConnected) {
            //not connected
            if (fbLikeManager.widgetNonConnectedContainerID != '') {
                $j('#' + fbLikeManager.widgetNonConnectedContainerID).show();
            }
        }
        else if (fbLikeManager.fbConnectStatusID == fbLikeManager.connectStatusIDNoFBData) {
            //no fb data
            if (fbLikeManager.widgetNoFBDataContainerID != '') {
                $j('#' + fbLikeManager.widgetNoFBDataContainerID).show();
            }
        }
        else {
            //no saved data view
            if (fbLikeManager.widgetNoSavedDataContainerID != '') {
                $j('#' + fbLikeManager.widgetNoSavedDataContainerID).show();
            }
        }

        fbLikeManager.ajaxUnBlock(this.blockDivId);
    }
    //on-demand chunking for view
    this.loadViewCategory = function ($category) {
        $j("#" + $category).find('.toggler').click();
    }
    this.loadViewChunking = function (categoryGroupObj) {
        if (!categoryGroupObj.fbLikeFullyLoaded) {
            fbLikeManager.ajaxBlockLoading(categoryGroupObj.containerID);

            categoryGroupObj.fbLikeFullyLoaded = true;
            fbLikesPerRow = 0;
            //get rest of likes for category group to display
            if (!fbLikeManager.updateInProgress) {
                $j.ajax({
                    type: "POST",
                    url: "/Applications/API/Facebook.asmx/GetFacebookLikesByCategoryGroup",
                    data: "{memberID:" + fbLikeManager.memberID + ", chunkStartIndex:" + fbLikeManager.fbLikesPerRow + ", categoryGroupID: " + categoryGroupObj.categoryGroupID + "}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    beforeSend: function () {
                        fbLikeManager.updateInProgress = true;
                    },
                    complete: function () {
                        fbLikeManager.updateInProgress = false;
                    },
                    success: function (result) {
                        var getFBLikesResult = (typeof result.d == 'undefined') ? result : result.d;
                        if (getFBLikesResult.Status != 2) {
                            if (fbLikeManager.isVerbose) {
                                alert(getFBLikesResult.StatusMessage);
                            }
                        }
                        else {

                            if (fbLikeManager.isVerbose) {
                                try {
                                    console.log('Returned likes: ' + getFBLikesResult.FacebookLikes);
                                }
                                catch (e) { }
                            }

                            ////load likes
                            if (getFBLikesResult.FacebookLikes.length > 0) {
                                var targetContainer = document.getElementById(categoryGroupObj.containerID).childNodes[1];
                                for (var i = 0; i < getFBLikesResult.FacebookLikes.length; i++) {
                                    var obj = getFBLikesResult.FacebookLikes[i];
                                    var fbLikeObj = new FBLikeObj();
                                    fbLikeObj.id = obj.FacebookId;
                                    fbLikeObj.name = obj.FacebookName;
                                    fbLikeObj.categoryGroupID = obj.FacebookLikeCategoryGroupId;
                                    fbLikeObj.categoryID = obj.FacebookLikeCategoryId;
                                    fbLikeObj.link = obj.FacebookLinkHref;
                                    fbLikeObj.saved = true;
                                    fbLikeObj.approved = true;
                                    fbLikeObj.selected = true;
                                    fbLikeObj.picture = obj.FacebookImageUrl;
                                    var addSuccess = categoryGroupObj.addFBLike(fbLikeObj, true);

                                    if (addSuccess) {
                                        targetContainer.innerHTML += Mustache.to_html(fbLikeManager.templateLike, fbLikeObj);
                                    }
                                }

                                //Jay, how do we refresh the category group, bind, and expand?
                                //TEMP: Redraw entire likes widget to verify data
                                fbLikeManager.loadViewCategory(categoryGroupObj.containerID);
                            }

                            fbLikeManager.ajaxUnBlock(categoryGroupObj.containerID);
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        if (fbLikeManager.isVerbose) {
                            alert(textStatus);
                        }
                        fbLikeManager.ajaxUnBlock(categoryGroupObj.containerID);
                    }
                });
            }
        }
    }
    //redraws edit widget
    this.loadEdit = function (getUpdatedFBData) {
        fbLikeManager.ajaxBlockLoading(this.blockDivId);
        var likesContainerEdit = document.getElementById(fbLikeManager.likesContainerEditID);
        if (fbLikeManager.isOwnProfile == true) {
            this.isEdit = true;
            fbLikeManager.hideAllWidgets();
            likesContainerEdit.innerHTML = "";
            $j('#' + fbLikeManager.editHasDataLoginCancelPromptContainerID).hide();

            // generate HTML holder for each category defined in categories
            for (var i = 0; i < fbLikeManager.fbLikeCategoryGroupObjList.length; i++) {
                likesContainerEdit.innerHTML += Mustache.to_html(fbLikeManager.templateGroupEdit, fbLikeManager.fbLikeCategoryGroupObjList[i]);
            }

            //update connect status to no saved data
            if (fbLikeManager.fbHasSavedLikes == false) {
                fbLikeManager.updateFBConnectStatus(fbLikeManager.connectStatusIDNoSavedData);
            }

            // load current likes
            if (fbLikeManager.fbHasSavedLikes == true) {
                //no likes data from FB but has saved likes on our side
                if (fbLikeManager.showEditHashDataLoginCancelPrompt == true) {
                    $j('#' + fbLikeManager.editHasDataLoginCancelPromptContainerID).show();
                    fbLikeManager.showEditHashDataLoginCancelPrompt = false;
                }
                $j('#' + fbLikeManager.widgetHasDataEditContainerID).show();
            }

            if (fbLikeManager.fbLikeCategoryGroupObjList.length > 0) {
                for (var i = 0; i < fbLikeManager.fbLikeCategoryGroupObjList.length; i++) {
                    var fbLikeCategoryGroupObj = fbLikeManager.fbLikeCategoryGroupObjList[i];
                    if (fbLikeCategoryGroupObj.fbLikeObjList.length > 0) {
                        var targetContainer = document.getElementById(fbLikeCategoryGroupObj.containerEditID).childNodes[1];
                        for (var j = 0; j < fbLikeCategoryGroupObj.fbLikeObjList.length; j++) {
                            var fbLikeObj = fbLikeCategoryGroupObj.fbLikeObjList[j];
                            targetContainer.innerHTML += Mustache.to_html(this.templateLikeEdit, fbLikeObj);
                        }
                    }
                }
            }

            if (getUpdatedFBData) {
                //get likes from FB
                FB.api('/me/likes', function (response) {
                    //console.log(response);
                    if (response.data && response.data.length > 0) {
                        $j('#' + fbLikeManager.widgetHasDataEditContainerID).show();

                        //has likes data from FB
                        var dataRaw = response.data;

                        fbLikeManager.ajaxCurrentNum = 0;
                        fbLikeManager.ajaxTotalLength = 0;
                        for (var i = 0, l = dataRaw.length; i < l; i++) {
                            // sort out unnecessary categories (cough reduce http request cough)
                            if (!fbLikeManager.isAllowedCategory(dataRaw[i].category) || dataRaw[i] === null) continue;

                            fbLikeManager.fbGetObj(dataRaw[i].id, function (response) { fbLikeManager.fbGetLikeObjCallback(response) });
                            fbLikeManager.ajaxTotalLength++;
                        }

                        if (fbLikeManager.ajaxTotalLength == 0) {
                            if (fbLikeManager.fbHasSavedLikes == true) {
                                //no likes data from FB but has saved likes on our side
                                $j('#' + fbLikeManager.widgetHasDataEditContainerID).show();
                                fbLikeManager.uiControl();
                            }
                            else {
                                //no likes data from FB
                                fbLikeManager.updateFBConnectStatus(fbLikeManager.connectStatusIDNoFBData);
                                fbLikeManager.loadView();
                            }
                        }
                    }
                    else {
                        if (response.error && fbLikeManager.isVerbose) {
                            alert('Error: ' + response.error.message);
                        }

                        if (fbLikeManager.fbHasSavedLikes == true) {
                            //no likes data from FB but has saved likes on our side
                            $j('#' + fbLikeManager.widgetHasDataEditContainerID).show();
                            fbLikeManager.uiControl();
                        }
                        else {
                            //no likes data from FB
                            fbLikeManager.updateFBConnectStatus(fbLikeManager.connectStatusIDNoFBData);
                            fbLikeManager.loadView();
                        }
                    }
                });
            }
            else if (fbLikeManager.fbHasSavedLikes == true) {
                $j('#' + fbLikeManager.widgetHasDataEditContainerID).show();
                fbLikeManager.uiControl();
            }
            else {
                fbLikeManager.loadView();
            }
        }
        else {
            fbLikeManager.loadView();
        }
    }
    // retreive each obj by FB api asynchronously and return it as a callback 
    this.fbGetObj = function (id, callback) {
        //Previous Approach: Get full like object (note: fb changes has a bug and is no longer returning picture property)
        //FB.api('/' + id, function (response) {
        //    callback(response);
        //});

        //Current Approach: Get like object data via parameters
        FB.api('/' + id + '?fields=picture.type(normal), id, name, link, category', function (response) {
            callback(response);
        });
    }
    this.fbGetLikeObjCallback = function (obj) {
        // add it to category
        if (obj && obj.id && obj.id > 0) {
            var fbLikeObj = new FBLikeObj();
            fbLikeObj.id = obj.id;
            fbLikeObj.name = obj.name;
            fbLikeObj.category = obj.category;
            fbLikeObj.link = obj.link;

            //08202012 TL - Looks like FB changed the JSON for locating image url
            fbLikeObj.picture = 'http://profile.ak.fbcdn.net/static-ak/rsrc.php/v1/yr/r/fwJFrO5KjAQ.png';
            if (obj.picture && obj.picture.data && obj.picture.data.url) {
                fbLikeObj.picture = obj.picture.data.url;
            }
            else if (obj.picture && obj.picture.url) {
                fbLikeObj.picture = obj.picture.url;
            }
            else if (obj.picture) {
                fbLikeObj.picture = obj.picture;
            }

            fbLikeObj.type = ''; //obj.type;
            fbLikeObj.chkEditID = 'chk' + obj.id;
            var addSuccess = fbLikeManager.addFBLike(fbLikeObj);
            //console.log(fbLikeObj);
            try {
                console.log(obj);
            } catch (e) { }

            if (addSuccess) {
                //var elm = document.createElement("li");
                var fbLikeCategoryGroupObj = fbLikeManager.getFBLikeCategoryGroup(fbLikeObj.category);
                var targetContainer = document.getElementById(fbLikeCategoryGroupObj.containerEditID).childNodes[1];
                targetContainer.innerHTML += Mustache.to_html(fbLikeManager.templateLikeEdit, fbLikeObj);
                fbLikeManager.fbHasUnapprovedLikes = true;
            }
        }

        fbLikeManager.ajaxCurrentNum++; // update current ajax call number

        //console.log('ajaxTotalLength: ', this.ajaxTotalLength, ' ajaxCurrentNum: ', this.ajaxCurrentNum);
        // if all ajax are done call now it's jquery time
        if (fbLikeManager.ajaxCurrentNum == fbLikeManager.ajaxTotalLength) {
            fbLikeManager.uiControl();
            //alert('uiControl time');
        }

    }
    this.getCategoryIndex = function (category) {
        var indexVal = 0;
        if (category != null && category != '') {
            if (this.categories.length > 0) {
                for (var i = 0; i < this.categories.length; i++) {
                    if (this.categories[i].toLowerCase() == category.toLowerCase()) {
                        indexVal = i;
                        break;
                    }
                }
            }
        }
        return indexVal;
    }
    this.isAllowedCategory = function (category) {
        var isAllowed = false;
        if (this.getFBLikeCategoryGroup(category) != null) {
            isAllowed = true;
        }
        return isAllowed;
    }
    this.changePublishStatusClick = function () {
        var radGroupName = $j("#" + this.radioPublishID).attr('name');
        if (radGroupName != '') {
            var selectedVal = $j("input:radio[name=" + radGroupName + "]:checked").val();
            if (selectedVal == 'show') {
                fbLikeManager.updateFBConnectStatus(fbLikeManager.connectStatusIDShowData);
                $j('#' + this.fbNotPublishedStatusContainerID).hide();
                $j('#' + this.fbPublishedStatusContainerID).show();
            }
            else {
                fbLikeManager.updateFBConnectStatus(fbLikeManager.connectStatusIDHideData);
                $j('#' + this.fbPublishedStatusContainerID).hide();
                $j('#' + this.fbNotPublishedStatusContainerID).show();
            }
        }
    }
    this.updateFBConnectStatus = function (statusID) {
        if (fbLikeManager.fbConnectStatusID != statusID) {
            var loadAjaxBlock = !fbLikeManager.isBlocked;
            if (loadAjaxBlock == true) {
                fbLikeManager.ajaxBlockSaving(this.blockDivId);
            }
            var prevStatusID = fbLikeManager.fbConnectStatusID;
            fbLikeManager.fbConnectStatusID = statusID;
            $j.ajax({
                type: "POST",
                url: "/Applications/API/Member.asmx/UpdateAttributeInt",
                data: "{memberID:" + this.memberID + ", attributeName: \"FacebookConnectStatusID\", attributeValue:" + statusID + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                complete: function () {
                    if (loadAjaxBlock == true) {
                        fbLikeManager.ajaxUnBlock(fbLikeManager.blockDivId);
                    }
                    //fbLikeManager.GoToByScroll();
                },
                success: function (result) {
                    var updateResult = (typeof result.d == 'undefined') ? result : result.d;
                    if (updateResult.Status != 2) {
                        //error
                        if (fbLikeManager.isVerbose) {
                            alert(updateResult.StatusMessage);
                        }
                        fbLikeManager.fbConnectStatusID = prevStatusID;
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    if (fbLikeManager.isVerbose) {
                        alert(textStatus);
                    }
                }
            });
        }
    }
    this.updateLikeSelect = function (chkID, likeID, categoryGroupID) {
        //select or unselect a specific like            
        var categoryGroupObj = fbLikeManager.getFBLikeCategoryGroupByID(categoryGroupID);
        if (categoryGroupObj != null) {
            var likesObj = categoryGroupObj.getFBLike(likeID);
            if (likesObj != null) {
                //if ($j("#" + chkID).is(':checked')){
                if ($j("#" + chkID).closest('li').hasClass('selected')) {
                    likesObj.selected = false;
                    if (fbLikeManager.isVerbose) {
                        try {
                            console.log('Unselected like - ' + chkID + ', ' + likeID + ', ' + categoryGroupID);
                        }
                        catch (e) { }
                    }
                }
                else {
                    likesObj.selected = true;
                    if (fbLikeManager.isVerbose) {
                        try {
                            console.log('Selected like - ' + chkID + ', ' + likeID + ', ' + categoryGroupID);
                        }
                        catch (e) { }
                    }
                }
            }
        }
    }
    this.processSaveLikes = function () {
        fbLikeManager.ajaxBlockSaving(this.blockDivId);

        //save all selected likes
        var fbLikesData = "";
        var fbLikesSelected = 0;
        var fbObjSeparator = "sparkobj&&";
        var fbObjNVSeparator = "==";
        var fbObjParamSeparator = "&&";
        if (fbLikeManager.fbLikeCategoryGroupObjList.length > 0) {
            for (var i = 0; i < fbLikeManager.fbLikeCategoryGroupObjList.length; i++) {
                var fbLikeCategoryGroupObj = fbLikeManager.fbLikeCategoryGroupObjList[i];
                if (fbLikeCategoryGroupObj.fbLikeObjList.length > 0) {
                    for (var j = 0; j < fbLikeCategoryGroupObj.fbLikeObjList.length; j++) {
                        var fbLikeObj = fbLikeCategoryGroupObj.fbLikeObjList[j];
                        if (fbLikeObj.selected == true && fbLikeObj.id > 0) {
                            fbLikeObj.saved = true;
                            fbLikesData = fbLikesData + "id" + fbObjNVSeparator + fbLikeObj.id + fbObjParamSeparator;
                            fbLikesData = fbLikesData + "name" + fbObjNVSeparator + fbLikeObj.name + fbObjParamSeparator;
                            fbLikesData = fbLikesData + "categoryID" + fbObjNVSeparator + fbLikeObj.categoryID + fbObjParamSeparator;
                            fbLikesData = fbLikesData + "categoryGroupID" + fbObjNVSeparator + fbLikeObj.categoryGroupID + fbObjParamSeparator;
                            fbLikesData = fbLikesData + "picture" + fbObjNVSeparator + fbLikeObj.picture + fbObjParamSeparator;
                            fbLikesData = fbLikesData + "link" + fbObjNVSeparator + fbLikeObj.link + fbObjParamSeparator;
                            fbLikesData = fbLikesData + "type" + fbObjNVSeparator + fbLikeObj.type + fbObjParamSeparator;
                            fbLikesData = fbLikesData + "approved" + fbObjNVSeparator + fbLikeObj.approved + fbObjParamSeparator;
                            fbLikesData = fbLikesData + fbObjSeparator;
                            fbLikesSelected++;
                        } else {
                            fbLikeObj.selected = false;
                            fbLikeObj.saved = false;
                            fbLikeObj.approved = false;
                        }
                    }
                }
            }
        }

        if (fbLikesSelected == 0 && !fbLikeManager.updateInProgress) {
            $j.ajax({
                type: "POST",
                url: "/Applications/API/Facebook.asmx/ClearFacebookLikes",
                data: "{memberID:" + this.memberID + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    fbLikeManager.updateInProgress = true;
                },
                complete: function () {
                    fbLikeManager.updateInProgress = false;
                },
                success: function (result) {
                    var updateResult = (typeof result.d == 'undefined') ? result : result.d;
                    if (updateResult.Status != 2) {
                        if (fbLikeManager.isVerbose) {
                            alert(updateResult.StatusMessage);
                        }
                    }
                    else {
                        fbLikeManager.fbHasSavedLikes = false;
                        fbLikeManager.fbConnectStatusID = fbLikeManager.connectStatusIDNoSavedData;
                        fbLikeManager.loadView();
                        fbLikeManager.GoToByScroll();
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    if (fbLikeManager.isVerbose) {
                        alert(textStatus);
                    }
                    fbLikeManager.ajaxUnBlock(fbLikeManager.blockDivId);
                }
            });
        }
        else if (fbLikesData.length > 0 && !fbLikeManager.updateInProgress) {
            $j.ajax({
                type: "POST",
                url: "/Applications/API/Facebook.asmx/SaveFacebookLikes",
                data: "{memberID:" + this.memberID + ", fbLikes: \"" + fbLikesData + "\"}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    fbLikeManager.updateInProgress = true;
                },
                complete: function () {
                    fbLikeManager.updateInProgress = false;
                },
                success: function (result) {
                    var facebookLikeSaveResult = (typeof result.d == 'undefined') ? result : result.d;
                    if (facebookLikeSaveResult.Status != 2) {
                        if (fbLikeManager.isVerbose) {
                            alert(facebookLikeSaveResult.StatusMessage);
                        }
                    }
                    else {
                        fbLikeManager.fbHasSavedLikes = true;
                        if (fbLikeManager.fbConnectStatusID != fbLikeManager.connectStatusIDShowData) {
                            fbLikeManager.fbConnectStatusID = fbLikeManager.connectStatusIDHideData;
                        }

                        //check for likes that failed to save
                        if (facebookLikeSaveResult.FailedFacebookLikeIDs.length > 0) {
                            for (var i = 0; i < facebookLikeSaveResult.FailedFacebookLikeIDs.length; i++) {
                                var failedLikeID = facebookLikeSaveResult.FailedFacebookLikeIDs[i];
                                if (failedLikeID > 0) {
                                    var fbFailedLikeObj = fbLikeManager.getFBLike(failedLikeID);
                                    if (fbFailedLikeObj != null) {
                                        fbFailedLikeObj.saved = false;
                                    }
                                }
                            }
                        }

                        if (fbLikeManager.isVerbose) {
                            try {
                                console.log('Attempted Saved likes: ' + fbLikesData);
                                console.log('Failed LikeIDs: ' + facebookLikeSaveResult.FailedFacebookLikeIDs);
                            }
                            catch (e) { }
                            //alert('Saved your likes, but you will stay in edit mode until we get to next story');
                        }
                        //Refresh view
                        fbLikeManager.loadView();
                        fbLikeManager.GoToByScroll();
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    if (fbLikeManager.isVerbose) {
                        alert(textStatus);
                    }
                    fbLikeManager.ajaxUnBlock(fbLikeManager.blockDivId);
                }
            });
        }
    }
    this.toggleTrigger = function ($trigger) {
        // toggle arrow/copy
        $trigger.find('.spr').toggleClass('s-icon-arrow-down-color').toggleClass('s-icon-arrow-up-color');
        $trigger.find('.word.more').toggle().next().toggle();
    }
    this.toggleWidget = function ($widgetContainer, newWidgetHeight) {
        $widgetContainer.animate({
            height: newWidgetHeight,
            maxHeight: newWidgetHeight
        }, 200, false, function () {

        });
    }
    this.toggleCategory = function ($container, $widgetContainer, $trigger, itemHeight, newHeight) {
        //console.log('toggleCategory');
        $trigger.toggle(
            function () {
                $container.animate({
                    height: newHeight
                }, 200, false, function () {
                    $container.addClass('open');
                    fbLikeManager.toggleTrigger($trigger);
                });

                fbLikeManager.toggleWidget($widgetContainer, $widgetContainer.height() + (newHeight - itemHeight));
            },
            function () {
                $container.animate({
                    height: itemHeight
                }, 200, false, function () {
                    $container.removeClass('open');
                    fbLikeManager.toggleTrigger($trigger);
                });

                fbLikeManager.toggleWidget($widgetContainer, $widgetContainer.height() - (newHeight - itemHeight));
            }
        );
    }
    // jQuery ui manipulation
    this.uiControl = function () {
        // collapse & expand - display counter
        var fbLikeCategoryGroupObjListWithLikes = [],
            fbLikeColNum = fbLikeManager.fbLikesPerRow,
            fbLikeGroupNum = 5,
            $widgetContainer = (fbLikeManager.isEdit == true) ? $j('#' + fbLikeManager.likesContainerEditID) : $j('#' + fbLikeManager.likesContainerID);

        for (var i = 0; i < fbLikeManager.fbLikeCategoryGroupObjList.length; i++) {
            var fbLikeCategoryGroupObj = fbLikeManager.fbLikeCategoryGroupObjList[i],
                fbLikeTotalItemNum = (fbLikeManager.isEdit == true) ? fbLikeCategoryGroupObj.getFBLikeCountAll() : fbLikeCategoryGroupObj.getFBLikeCountSaved(),
                fbLikeDisplayItemNum = fbLikeTotalItemNum - fbLikeColNum;

            if (!fbLikeManager.isOwnProfile && !fbLikeCategoryGroupObj.fbLikeFullyLoaded) {
                fbLikeDisplayItemNum = fbLikeCategoryGroupObj.getFBLikeCountAll() - fbLikeColNum;
            }

            //remove empty categories
            if (fbLikeTotalItemNum <= 0) {
                if (fbLikeManager.isEdit == true) {
                    $j('#' + fbLikeCategoryGroupObj.containerEditID).remove();
                }
                else {
                    $j('#' + fbLikeCategoryGroupObj.containerID).remove();
                }
            }
            else {
                spark.util.addItem(fbLikeCategoryGroupObjListWithLikes, fbLikeCategoryGroupObj);
            }

            //collapse/expand within category
            if (fbLikeDisplayItemNum > 0) {
                var moreHtml = "<div class='toggler more'><span class='spr s-icon-arrow-down-color'></span> " + fbLikeDisplayItemNum + " <span class='word more'>" + fbLikeManager.textLikesExpand + "</span><span class='word less'>" + fbLikeManager.textLikesCollapse + "</span></div>",
                    $categoryContainer = (fbLikeManager.isEdit == true) ? $j('#' + fbLikeCategoryGroupObj.containerEditID).find('ol') : $j('#' + fbLikeCategoryGroupObj.containerID).find('ol');

                $categoryContainer.after(moreHtml);

                if (!fbLikeManager.isOwnProfile && !fbLikeCategoryGroupObj.fbLikeFullyLoaded) {
                    toggleLikes($categoryContainer, $categoryContainer.next('.toggler'), fbLikeTotalItemNum, function (fbLikeCategoryGroupObj) { fbLikeManager.loadViewChunking(fbLikeCategoryGroupObj); }, fbLikeCategoryGroupObj);
                }
                else {
                    toggleLikes($categoryContainer, $categoryContainer.next('.toggler'), fbLikeTotalItemNum, null, null);
                }
            }
        }

        //console.log('fbLikeCategoryGroupObjListWithLikes.length:', fbLikeCategoryGroupObjListWithLikes.length);

        //collapse/expand category groups
        if (fbLikeCategoryGroupObjListWithLikes.length > fbLikeGroupNum) {
            var moreHtml = "<div class='toggler more widget'><span class='spr s-icon-arrow-down-color'></span> <span class='word more'> " + fbLikeManager.textCategoriesExpand + "</span><span class='word less'> " + fbLikeManager.textCategoriesCollapse + "</span></div>";
            var fbLikeTotalGroupNum = fbLikeCategoryGroupObjListWithLikes.length;

            $widgetContainer.next('.toggler').remove().end().after(moreHtml);
            toggleLikes($widgetContainer, $widgetContainer.next('.toggler'), fbLikeTotalGroupNum, null, null);
        } else {
            $widgetContainer.css('height', 'auto').next('.toggler').remove();
        }

        // collapse/expand - toggler
        function toggleLikes(container, trigger, totalItemNum, toggleFunc, toggleFuncParam) {
            var $container = container,
                $trigger = trigger,
                totalItemNum = totalItemNum,
                itemHeight = null,
                newHeight = null,
                widgetRowHeight = null,
                widgetResetHeight = null;

            function getNewHeight() {
                if ($container.hasClass('fb-likes-widget')) {
                    widgetRowHeight = $container.find('li:first').outerHeight();
                    newHeight = widgetRowHeight * totalItemNum;
                } else {
                    newHeight = (Math.ceil(fbLikeCategoryGroupObj.getFBLikeCountAll() / fbLikeColNum)) * itemHeight;
                }
            }

            function getResetHeight() {
                if ($container.hasClass('fb-likes-widget')) {
                    if (totalItemNum > fbLikeGroupNum) {
                        widgetResetHeight = widgetRowHeight * (fbLikeGroupNum - 1);
                    } else if (totalItemNum == fbLikeGroupNum) {
                        widgetResetHeight = widgetRowHeight * fbLikeGroupNum;
                    } else {
                        widgetResetHeight = widgetRowHeight * totalItemNum;
                    }
                }
            }

            if (fbLikeManager.isEdit == false) {
                // Check if it's inside of tab
                if (fbLikeManager.tabContainerId != '') {
                    itemHeight = $j('#' + fbLikeManager.tabContainerId).css('cssText', 'visibility:hidden; position:absolute; left:-9999em; display:block !important').find($container).outerHeight();
                    getNewHeight();
                    getResetHeight();
                    $j('#' + fbLikeManager.tabContainerId).css('cssText', 'visibility:visible; position:static;');
                } else {
                    itemHeight = $container.outerHeight();
                    getNewHeight();
                    getResetHeight();
                }
            } else {
                itemHeight = $container.outerHeight();
                getNewHeight();
                getResetHeight();
            }

            //console.log('totalItemNum:', totalItemNum,'widgetRowHeight:', widgetRowHeight,  'widgetResetHeight:', widgetResetHeight, '$container:', $container, 'itemHeight:', itemHeight, 'newHeight:', newHeight);

            if ($trigger.hasClass('widget')) {
                // collapse/expand widget
                $trigger.toggle(
                        function () {
                            fbLikeManager.toggleWidget($widgetContainer, $widgetContainer.height() + (newHeight - widgetResetHeight));
                            fbLikeManager.toggleTrigger($trigger);
                        },
                        function () {
                            //collapse all opened categories
                            $j('.open', $widgetContainer).next('.toggler').trigger('click');
                            fbLikeManager.toggleWidget($widgetContainer, widgetResetHeight);
                            fbLikeManager.toggleTrigger($trigger);
                        });

                if (fbLikeCategoryGroupObjListWithLikes.length > fbLikeGroupNum) {
                    fbLikeManager.toggleWidget($widgetContainer, widgetResetHeight);

                    //in edit, we will default to expand widget
                    if (fbLikeManager.isEdit && fbLikeManager.fbHasSavedLikes) {
                        $widgetContainer.css('height', widgetResetHeight);
                        $trigger.click();
                    }
                }

            } else {
                // collapse/expand within categories
                if (toggleFunc) {
                    $trigger.one('click', function (event) {
                        event.preventDefault();
                        toggleFunc(toggleFuncParam);
                        fbLikeManager.toggleCategory($container, $widgetContainer, $trigger, itemHeight, newHeight);
                    });
                }
                else {
                    fbLikeManager.toggleCategory($container, $widgetContainer, $trigger, itemHeight, newHeight);
                }
            } //else

            if (fbLikeManager.isEdit == true) {
                $j('#' + fbLikeManager.likesContainerEditID + ' ~ input').one('click', function () {
                    $j('#' + fbLikeManager.likesContainerEditID + ', #' + fbLikeManager.likesContainerID).css({ 'height': 'auto', 'max-height': itemHeight });
                });
            }

        } //toggleLikes   

        // control edit hover
        if (fbLikeManager.isOwnProfile) {
            $j('#' + fbLikeManager.likesRegionContainerEditID).addClass('prof-edit-active');
            //.find('.prof-edit-button').css('display','block');
        }

        // select/unselect fb-likes
        if (fbLikeManager.isEdit == true) {
            $j('#' + fbLikeManager.likesContainerEditID + ' ol > li').click(function (e) {
                var $target = $j(e.target),
                        $this = $j(this),
                        $input = $this.find('input');

                e.stopPropagation();

                if ($target.is('input')) {
                    $this.toggleClass(fbLikeManager.cssClassLikeItemSelected);

                } else {
                    e.preventDefault();
                    $input.trigger("click");
                }
            });
        }

        //alert('UIControl manipulation done');
        fbLikeManager.ajaxUnBlock(this.blockDivId);
    }
}