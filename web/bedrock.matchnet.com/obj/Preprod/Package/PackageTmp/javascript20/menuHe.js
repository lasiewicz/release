var currentMenuItem; //the currently moused over top level menu Item ("LI").
var dropDelayTimer; //variable that holds the timer to delay hide the currently showing menu.
var dropDelayLength = 500; //number of milliseconds to display drop down menus before disappearing onmouseout (1 second was a bit long. Trying 1/2 a second)

function elementContains(elmOuter, elmInner)
{
  while (elmInner && elmInner != elmOuter)
  {
    elmInner = elmInner.parentNode;
  }
  if (elmInner == elmOuter)
  {
    return true;
  }
  return false;
}

function getPageXY(elm)
{
  var point = { x: 0, y: 0 };
  while (elm)
  {
    point.x = 0;
    point.y += elm.offsetTop;
    elm = elm.offsetParent;
  }
  return point;
}

function setPageXY(elm, y)
{
  elm.style.top  = y + 'px';
}

function cssjsmenu(menuid)
{
  var i;
  var j;
  var node;
  var child;
  var parent;

  // if the browser doesn't even support
  // document.getElementById, give up now.
  if (!document.getElementById)
  {
    return true;
  }

  // check for downlevel browsers
  // Opera 6, IE 5/Mac are not supported

  var version;
  var offset;

  offset = navigator.userAgent.indexOf('Opera');
  if (offset != -1)
  {
    version = parseInt('0' + navigator.userAgent.substr(offset + 6), 10);
    if (version < 7)
    {
      return true;
    }
  }

  offset = navigator.userAgent.indexOf('MSIE');
  if (offset != -1)
  {
    if (navigator.userAgent.indexOf('Mac') != -1)
    {
      return true;
    }
  }

  var menudiv = document.getElementById(menuid);

  // ul
  var ul = new Array();
	if(menudiv != null)
	{
		for (i = 0; i < menudiv.childNodes.length; i++)
		{
			node = menudiv.childNodes[i];
			if (node.nodeName == 'UL')
			{
			ul[ul.length] = node;
			}
		}
	}

  // ul > li
  var ul_gt_li = new Array();

  for (i = 0; i < ul.length; i++)
  {
    node = ul[i];
    for (j = 0; j < node.childNodes.length; j++)
    {
      child = node.childNodes[j];
      if (child.nodeName == 'LI')
      {
        ul_gt_li[ul_gt_li.length] = child;
        child.style.display = 'inline';
        child.style.listStyle = 'none';
        child.style.position = 'static';
      }
      
		child.onmouseover = function (e)
        {
            //As soon as a menu item is moused over, we want to accomplish three things: (TT 18124 modified by Adam)
            //(1) Turn off any unnecessary delayed hiding of last known visible menu
            if (typeof dropDelayTimer!="undefined")
            {
                clearTimeout(dropDelayTimer);
            }
            //(2) hide the last known menu that may be showing immediately
            hideCurrentMenuItemChildren();
            //(3) set a new current menu item - only if it has submenu items
            currentMenuItem = this;
        
          var i;
          var child;
          var point;

          // stop the pure css hover effect
          this.style.paddingBottom = '0';

          for (i = 0; i < this.childNodes.length; i++)
          {
            child = this.childNodes[i];
            if (child.nodeName == 'UL')
            {
              point = getPageXY(this);
              setPageXY(child, point.y + this.offsetHeight);
              child.style.marginRight = "-" + (this.offsetWidth - 10);
              child.style.visibility = 'visible';
            }
          }
          return false;
        };
    }
  }

  // ul > li > ul
  var ul_gt_li_gt_ul = new Array();

  for (i = 0; i < ul_gt_li.length; i++)
  {
    node = ul_gt_li[i];
    for (j = 0; j < node.childNodes.length; j++)
    {
      child = node.childNodes[j];
      if (child.nodeName == 'UL')
      {
        ul_gt_li_gt_ul[ul_gt_li_gt_ul.length] = child;
        child.style.position = 'absolute';
        //child.style.left = '50px';
        child.style.visibility = 'hidden';

        // attach hover to parent li
        parent = child.parentNode;
        
        parent.onmouseout = function (e)
        {
          var relatedTarget = null;
          if (e)
          {
            relatedTarget = e.relatedTarget;
            // work around Gecko Linux only bug where related target is null
            // when clicking on menu links or when right clicking and moving
            // into a context menu.
	    if (navigator.product == 'Gecko' && navigator.platform.indexOf('Linux') != -1 && !relatedTarget)
	    {
	      relatedTarget = e.originalTarget;
	    }
          }
          else if (window.event)
          {
            relatedTarget = window.event.toElement;
          }

          if (elementContains(this, relatedTarget))
          {
            return false;
          }
        //set a new current menu item
        currentMenuItem = this;  
		//18124 we don't want to hide child menus immediately onmouseout, but rather after short delay (modified by Adam 18124)
		hideCurrentMenuItemChildrenAfterDelay();
        };
      }
    }
  }
  return true;
}

///Hides the current Menu item after a short delay. This is important so that the menu doesn't 
///disappear in the blank space between a menu item and it's sub-menu... (TT 18124)
function hideCurrentMenuItemChildrenAfterDelay()
{
    if (typeof currentMenuItem!="undefined")
    {
         dropDelayTimer = setTimeout("hideCurrentMenuItemChildren()",dropDelayLength);
    }
}

///Loops through current menu item, finds all child "UL"s and hides them.
function hideCurrentMenuItemChildren() {

    if (typeof currentMenuItem!="undefined")
    {
          var i;
          var child;
          for (i = 0; i < currentMenuItem.childNodes.length; i++)
          {
            child = currentMenuItem.childNodes[i];
            if (child.nodeName == 'UL')
            {
                child.style.visibility = 'hidden';
            }
          }
      }

}