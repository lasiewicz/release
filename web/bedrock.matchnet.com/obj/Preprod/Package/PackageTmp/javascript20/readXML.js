jQuery(document).ready(function() {

    jQuery.getJSON(url + '?callback=?', function(data) {

        var myHTMLOutput = '';
        var imgContents = jQuery("#img_contents");
            	
        jQuery.each(data, function(entryIndex, entry) {
            var imgSrc = entry['imgsrc'];
            var imgTitle = entry['imgtitle'];
            var imgAlt = entry['imgalt'];
            var imgLink = entry['imglink'];
            var imgTarget = entry['target'];
			
            var mydata = BuildRotatorHTML(imgSrc, imgTitle, imgAlt, imgLink, imgTarget);
        myHTMLOutput += mydata;
        });
            		
        imgContents.prepend(myHTMLOutput);
            jQuery("#img_contents img:last,#img_controls a:last").remove(); // delete default image
            imgContents.cycle({ fx: 'fade', timeout:6000, speed: 1600, pager: '#img_controls'});
        		
        });
    });

     function BuildRotatorHTML(imgSrc,imgTitle,imgAlt,imgLink){
        // Build HTML string and return
        var output = '';
        output += '<img src="' + imgSrc + '" alt="' + imgAlt + '" title="' + imgTitle + '" />';
        return output;
    }

