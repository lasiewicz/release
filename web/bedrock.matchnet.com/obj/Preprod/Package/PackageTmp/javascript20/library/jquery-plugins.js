﻿/**
* hoverIntent r6 // 2011.02.26 // jQuery 1.5.1+
* <http://cherne.net/brian/resources/jquery.hoverIntent.html>
* 
* @param  f  onMouseOver function || An object with configuration options
* @param  g  onMouseOut function  || Nothing (use configuration options object)
* @author    Brian Cherne brian(at)cherne(dot)net
*/
(function($){$.fn.hoverIntent=function(f,g){var cfg={sensitivity:7,interval:100,timeout:0};cfg=$.extend(cfg,g?{over:f,out:g}:f);var cX,cY,pX,pY;var track=function(ev){cX=ev.pageX;cY=ev.pageY};var compare=function(ev,ob){ob.hoverIntent_t=clearTimeout(ob.hoverIntent_t);if((Math.abs(pX-cX)+Math.abs(pY-cY))<cfg.sensitivity){$(ob).unbind("mousemove",track);ob.hoverIntent_s=1;return cfg.over.apply(ob,[ev])}else{pX=cX;pY=cY;ob.hoverIntent_t=setTimeout(function(){compare(ev,ob)},cfg.interval)}};var delay=function(ev,ob){ob.hoverIntent_t=clearTimeout(ob.hoverIntent_t);ob.hoverIntent_s=0;return cfg.out.apply(ob,[ev])};var handleHover=function(e){var ev=jQuery.extend({},e);var ob=this;if(ob.hoverIntent_t){ob.hoverIntent_t=clearTimeout(ob.hoverIntent_t)}if(e.type=="mouseenter"){pX=ev.pageX;pY=ev.pageY;$(ob).bind("mousemove",track);if(ob.hoverIntent_s!=1){ob.hoverIntent_t=setTimeout(function(){compare(ev,ob)},cfg.interval)}}else{$(ob).unbind("mousemove",track);if(ob.hoverIntent_s==1){ob.hoverIntent_t=setTimeout(function(){delay(ev,ob)},cfg.timeout)}}};return this.bind('mouseenter',handleHover).bind('mouseleave',handleHover)}})(jQuery);

/* Superfish v1.4.8 - jQuery menu widget * Copyright (c) 2008 Joel Birch * Dual licensed under the MIT and GPL licenses: * 	http://www.opensource.org/licenses/mit-license.php * http://www.gnu.org/licenses/gpl.html * CHANGELOG: http://users.tpg.com.au/j_birch/plugins/superfish/changelog.txt */
; (function ($) { $.fn.superfish = function (op) { var sf = $.fn.superfish, c = sf.c, $arrow = $(['<span class="', c.arrowClass, '"> &#187;</span>'].join('')), over = function () { var $$ = $(this), menu = getMenu($$); clearTimeout(menu.sfTimer); $$.showSuperfishUl().siblings().hideSuperfishUl() }, out = function () { var $$ = $(this), menu = getMenu($$), o = sf.op; clearTimeout(menu.sfTimer); menu.sfTimer = setTimeout(function () { o.retainPath = ($.inArray($$[0], o.$path) > -1); $$.hideSuperfishUl(); if (o.$path.length && $$.parents(['li.', o.hoverClass].join('')).length < 1) { over.call(o.$path) } }, o.delay) }, getMenu = function ($menu) { var menu = $menu.parents(['ul.', c.menuClass, ':first'].join(''))[0]; sf.op = sf.o[menu.serial]; return menu }, addArrow = function ($a) { $a.addClass(c.anchorClass).append($arrow.clone()) }; return this.each(function () { var s = this.serial = sf.o.length; var o = $.extend({}, sf.defaults, op); o.$path = $('li.' + o.pathClass, this).slice(0, o.pathLevels).each(function () { $(this).addClass([o.hoverClass, c.bcClass].join(' ')).filter('li:has(ul)').removeClass(o.pathClass) }); sf.o[s] = sf.op = o; $('li:has(ul)', this)[($.fn.hoverIntent && !o.disableHI) ? 'hoverIntent' : 'hover'](over, out).each(function () { if (o.autoArrows) addArrow($('>a:first-child', this)) }).not('.' + c.bcClass).hideSuperfishUl(); var $a = $('a', this); $a.each(function (i) { var $li = $a.eq(i).parents('li'); $a.eq(i).focus(function () { over.call($li) }).blur(function () { out.call($li) }) }); o.onInit.call(this) }).each(function () { menuClasses = [c.menuClass]; if (sf.op.dropShadows && !($.browser.msie && $.browser.version < 7)) menuClasses.push(c.shadowClass); $(this).addClass(menuClasses.join(' ')) }) }; var sf = $.fn.superfish; sf.o = []; sf.op = {}; sf.IE7fix = function () { var o = sf.op; if ($.browser.msie && $.browser.version > 6 && o.dropShadows && o.animation.opacity != undefined) this.toggleClass(sf.c.shadowClass + '-off') }; sf.c = { bcClass: 'sf-breadcrumb', menuClass: 'sf-js-enabled', anchorClass: 'sf-with-ul', arrowClass: 'sf-sub-indicator', shadowClass: 'sf-shadow' }; sf.defaults = { hoverClass: 'sfHover', pathClass: 'overideThisToUse', pathLevels: 1, delay: 800, animation: { opacity: 'show' }, speed: 'normal', autoArrows: true, dropShadows: true, disableHI: false, onInit: function () { }, onBeforeShow: function () { }, onShow: function () { }, onHide: function () { } }; $.fn.extend({ hideSuperfishUl: function () { var o = sf.op, not = (o.retainPath === true) ? o.$path : ''; o.retainPath = false; var $ul = $(['li.', o.hoverClass].join(''), this).add(this).not(not).removeClass(o.hoverClass).find('>ul').hide().css('visibility', 'hidden'); o.onHide.call($ul); return this }, showSuperfishUl: function () { var o = sf.op, sh = sf.c.shadowClass + '-off', $ul = this.addClass(o.hoverClass).find('>ul:hidden').css('visibility', 'visible'); sf.IE7fix.call($ul); o.onBeforeShow.call($ul); $ul.animate(o.animation, o.speed, function () { sf.IE7fix.call($ul); o.onShow.call($ul) }); return this } }) })(jQuery);

/* tableDnD - ver .5 compressed with yahoo compressor */
jQuery.tableDnD = { currentTable: null, dragObject: null, mouseOffset: null, oldY: 0, build: function (a) { this.each(function () { this.tableDnDConfig = jQuery.extend({ onDragStyle: null, onDropStyle: null, onDragClass: "tDnD_whileDrag", onDrop: null, onDragStart: null, scrollAmount: 5, serializeRegexp: /[^\-]*$/, serializeParamName: null, dragHandle: null }, a || {}); jQuery.tableDnD.makeDraggable(this) }); jQuery(document).bind("mousemove", jQuery.tableDnD.mousemove).bind("mouseup", jQuery.tableDnD.mouseup); return this }, makeDraggable: function (c) { var b = c.tableDnDConfig; if (c.tableDnDConfig.dragHandle) { var a = jQuery("td." + c.tableDnDConfig.dragHandle, c); a.each(function () { jQuery(this).mousedown(function (e) { jQuery.tableDnD.dragObject = this.parentNode; jQuery.tableDnD.currentTable = c; jQuery.tableDnD.mouseOffset = jQuery.tableDnD.getMouseOffset(this, e); if (b.onDragStart) { b.onDragStart(c, this) } return false }) }) } else { var d = jQuery("tr", c); d.each(function () { var e = jQuery(this); if (!e.hasClass("nodrag")) { e.mousedown(function (f) { if (f.target.tagName == "TD") { jQuery.tableDnD.dragObject = this; jQuery.tableDnD.currentTable = c; jQuery.tableDnD.mouseOffset = jQuery.tableDnD.getMouseOffset(this, f); if (b.onDragStart) { b.onDragStart(c, this) } return false } }).css("cursor", "move") } }) } }, updateTables: function () { this.each(function () { if (this.tableDnDConfig) { jQuery.tableDnD.makeDraggable(this) } }) }, mouseCoords: function (a) { if (a.pageX || a.pageY) { return { x: a.pageX, y: a.pageY} } return { x: a.clientX + document.body.scrollLeft - document.body.clientLeft, y: a.clientY + document.body.scrollTop - document.body.clientTop} }, getMouseOffset: function (d, c) { c = c || window.event; var b = this.getPosition(d); var a = this.mouseCoords(c); return { x: a.x - b.x, y: a.y - b.y} }, getPosition: function (c) { var b = 0; var a = 0; if (c.offsetHeight == 0) { c = c.firstChild } while (c.offsetParent) { b += c.offsetLeft; a += c.offsetTop; c = c.offsetParent } b += c.offsetLeft; a += c.offsetTop; return { x: b, y: a} }, mousemove: function (g) { if (jQuery.tableDnD.dragObject == null) { return } var d = jQuery(jQuery.tableDnD.dragObject); var b = jQuery.tableDnD.currentTable.tableDnDConfig; var i = jQuery.tableDnD.mouseCoords(g); var f = i.y - jQuery.tableDnD.mouseOffset.y; var c = window.pageYOffset; if (document.all) { if (typeof document.compatMode != "undefined" && document.compatMode != "BackCompat") { c = document.documentElement.scrollTop } else { if (typeof document.body != "undefined") { c = document.body.scrollTop } } } if (i.y - c < b.scrollAmount) { window.scrollBy(0, -b.scrollAmount) } else { var a = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : document.body.clientHeight; if (a - (i.y - c) < b.scrollAmount) { window.scrollBy(0, b.scrollAmount) } } if (f != jQuery.tableDnD.oldY) { var e = f > jQuery.tableDnD.oldY; jQuery.tableDnD.oldY = f; if (b.onDragClass) { d.addClass(b.onDragClass) } else { d.css(b.onDragStyle) } var h = jQuery.tableDnD.findDropTargetRow(d, f); if (h) { if (e && jQuery.tableDnD.dragObject != h) { jQuery.tableDnD.dragObject.parentNode.insertBefore(jQuery.tableDnD.dragObject, h.nextSibling) } else { if (!e && jQuery.tableDnD.dragObject != h) { jQuery.tableDnD.dragObject.parentNode.insertBefore(jQuery.tableDnD.dragObject, h) } } } } return false }, findDropTargetRow: function (f, g) { var j = jQuery.tableDnD.currentTable.rows; for (var e = 0; e < j.length; e++) { var h = j[e]; var b = this.getPosition(h).y; var a = parseInt(h.offsetHeight) / 2; if (h.offsetHeight == 0) { b = this.getPosition(h.firstChild).y; a = parseInt(h.firstChild.offsetHeight) / 2 } if ((g > b - a) && (g < (b + a))) { if (h == f) { return null } var c = jQuery.tableDnD.currentTable.tableDnDConfig; if (c.onAllowDrop) { if (c.onAllowDrop(f, h)) { return h } else { return null } } else { var d = jQuery(h).hasClass("nodrop"); if (!d) { return h } else { return null } } return h } } return null }, mouseup: function (c) { if (jQuery.tableDnD.currentTable && jQuery.tableDnD.dragObject) { var b = jQuery.tableDnD.dragObject; var a = jQuery.tableDnD.currentTable.tableDnDConfig; if (a.onDragClass) { jQuery(b).removeClass(a.onDragClass) } else { jQuery(b).css(a.onDropStyle) } jQuery.tableDnD.dragObject = null; if (a.onDrop) { a.onDrop(jQuery.tableDnD.currentTable, b) } jQuery.tableDnD.currentTable = null } }, serialize: function () { if (jQuery.tableDnD.currentTable) { return jQuery.tableDnD.serializeTable(jQuery.tableDnD.currentTable) } else { return "Error: No Table id set, you need to set an id on your table and every row" } }, serializeTable: function (d) { var a = ""; var c = d.id; var e = d.rows; for (var b = 0; b < e.length; b++) { if (a.length > 0) { a += "&" } var f = e[b].id; if (f && f && d.tableDnDConfig && d.tableDnDConfig.serializeRegexp) { f = f.match(d.tableDnDConfig.serializeRegexp)[0] } a += c + "[]=" + f } return a }, serializeTables: function () { var a = ""; this.each(function () { a += jQuery.tableDnD.serializeTable(this) }); return a } }; jQuery.fn.extend({ tableDnD: jQuery.tableDnD.build, tableDnDUpdate: jQuery.tableDnD.updateTables, tableDnDSerialize: jQuery.tableDnD.serializeTables });

/* blockui ver 2.39 compressed with dean edwards packer */
;(function($){if(/1\.(0|1|2)\.(0|1|2)/.test($.fn.jquery)||/^1.1/.test($.fn.jquery)){alert('blockUI requires jQuery v1.2.3 or later!  You are using v'+$.fn.jquery);return}$.fn._fadeIn=$.fn.fadeIn;var noOp=function(){};var mode=document.documentMode||0;var setExpr=$.browser.msie&&(($.browser.version<8&&!mode)||mode<8);var ie6=$.browser.msie&&/MSIE 6.0/.test(navigator.userAgent)&&!mode;$.blockUI=function(opts){install(window,opts)};$.unblockUI=function(opts){remove(window,opts)};$.growlUI=function(title,message,timeout,onClose){var $m=$('<div class="growlUI"></div>');if(title)$m.append('<h1>'+title+'</h1>');if(message)$m.append('<h2>'+message+'</h2>');if(timeout==undefined)timeout=3000;$.blockUI({message:$m,fadeIn:700,fadeOut:1000,centerY:false,timeout:timeout,showOverlay:false,onUnblock:onClose,css:$.blockUI.defaults.growlCSS})};$.fn.block=function(opts){return this.unblock({fadeOut:0}).each(function(){if($.css(this,'position')=='static')this.style.position='relative';if($.browser.msie)this.style.zoom=1;install(this,opts)})};$.fn.unblock=function(opts){return this.each(function(){remove(this,opts)})};$.blockUI.version=2.39;$.blockUI.defaults={message:'<h1>Please wait...</h1>',title:null,draggable:true,theme:false,css:{padding:0,margin:0,width:'30%',top:'40%',left:'35%',textAlign:'center',color:'#000',border:'3px solid #aaa',backgroundColor:'#fff',cursor:'wait'},themedCSS:{width:'30%',top:'40%',left:'35%'},overlayCSS:{backgroundColor:'#000',opacity:0.6,cursor:'wait'},growlCSS:{width:'350px',top:'10px',left:'',right:'10px',border:'none',padding:'5px',opacity:0.6,cursor:'default',color:'#fff',backgroundColor:'#000','-webkit-border-radius':'10px','-moz-border-radius':'10px','border-radius':'10px'},iframeSrc:/^https/i.test(window.location.href||'')?'javascript:false':'about:blank',forceIframe:false,baseZ:1000,centerX:true,centerY:true,allowBodyStretch:true,bindEvents:true,constrainTabKey:true,fadeIn:200,fadeOut:400,timeout:0,showOverlay:true,focusInput:true,applyPlatformOpacityRules:true,onBlock:null,onUnblock:null,quirksmodeOffsetHack:4,blockMsgClass:'blockMsg'};var pageBlock=null;var pageBlockEls=[];function install(el,opts){var full=(el==window);var msg=opts&&opts.message!==undefined?opts.message:undefined;opts=$.extend({},$.blockUI.defaults,opts||{});opts.overlayCSS=$.extend({},$.blockUI.defaults.overlayCSS,opts.overlayCSS||{});var css=$.extend({},$.blockUI.defaults.css,opts.css||{});var themedCSS=$.extend({},$.blockUI.defaults.themedCSS,opts.themedCSS||{});msg=msg===undefined?opts.message:msg;if(full&&pageBlock)remove(window,{fadeOut:0});if(msg&&typeof msg!='string'&&(msg.parentNode||msg.jquery)){var node=msg.jquery?msg[0]:msg;var data={};$(el).data('blockUI.history',data);data.el=node;data.parent=node.parentNode;data.display=node.style.display;data.position=node.style.position;if(data.parent)data.parent.removeChild(node)}$(el).data('blockUI.onUnblock',opts.onUnblock);var z=opts.baseZ;var lyr1=($.browser.msie||opts.forceIframe)?$('<iframe class="blockUI" style="z-index:'+(z++)+';display:none;border:none;margin:0;padding:0;position:absolute;width:100%;height:100%;top:0;left:0" src="'+opts.iframeSrc+'"></iframe>'):$('<div class="blockUI" style="display:none"></div>');var lyr2=opts.theme?$('<div class="blockUI blockOverlay ui-widget-overlay" style="z-index:'+(z++)+';display:none"></div>'):$('<div class="blockUI blockOverlay" style="z-index:'+(z++)+';display:none;border:none;margin:0;padding:0;width:100%;height:100%;top:0;left:0"></div>');var lyr3,s;if(opts.theme&&full){s='<div class="blockUI '+opts.blockMsgClass+' blockPage ui-dialog ui-widget ui-corner-all" style="z-index:'+(z+10)+';display:none;position:fixed">'+'<div class="ui-widget-header ui-dialog-titlebar ui-corner-all blockTitle">'+(opts.title||'&nbsp;')+'</div>'+'<div class="ui-widget-content ui-dialog-content"></div>'+'</div>'}else if(opts.theme){s='<div class="blockUI '+opts.blockMsgClass+' blockElement ui-dialog ui-widget ui-corner-all" style="z-index:'+(z+10)+';display:none;position:absolute">'+'<div class="ui-widget-header ui-dialog-titlebar ui-corner-all blockTitle">'+(opts.title||'&nbsp;')+'</div>'+'<div class="ui-widget-content ui-dialog-content"></div>'+'</div>'}else if(full){s='<div class="blockUI '+opts.blockMsgClass+' blockPage" style="z-index:'+(z+10)+';display:none;position:fixed"></div>'}else{s='<div class="blockUI '+opts.blockMsgClass+' blockElement" style="z-index:'+(z+10)+';display:none;position:absolute"></div>'}lyr3=$(s);if(msg){if(opts.theme){lyr3.css(themedCSS);lyr3.addClass('ui-widget-content')}else lyr3.css(css)}if(!opts.theme&&(!opts.applyPlatformOpacityRules||!($.browser.mozilla&&/Linux/.test(navigator.platform))))lyr2.css(opts.overlayCSS);lyr2.css('position',full?'fixed':'absolute');if($.browser.msie||opts.forceIframe)lyr1.css('opacity',0.0);var layers=[lyr1,lyr2,lyr3],$par=full?$('body'):$(el);$.each(layers,function(){this.appendTo($par)});if(opts.theme&&opts.draggable&&$.fn.draggable){lyr3.draggable({handle:'.ui-dialog-titlebar',cancel:'li'})}var expr=setExpr&&(!$.boxModel||$('object,embed',full?null:el).length>0);if(ie6||expr){if(full&&opts.allowBodyStretch&&$.boxModel)$('html,body').css('height','100%');if((ie6||!$.boxModel)&&!full){var t=sz(el,'borderTopWidth'),l=sz(el,'borderLeftWidth');var fixT=t?'(0 - '+t+')':0;var fixL=l?'(0 - '+l+')':0}$.each([lyr1,lyr2,lyr3],function(i,o){var s=o[0].style;s.position='absolute';if(i<2){full?s.setExpression('height','Math.max(document.body.scrollHeight, document.body.offsetHeight) - (jQuery.boxModel?0:'+opts.quirksmodeOffsetHack+') + "px"'):s.setExpression('height','this.parentNode.offsetHeight + "px"');full?s.setExpression('width','jQuery.boxModel && document.documentElement.clientWidth || document.body.clientWidth + "px"'):s.setExpression('width','this.parentNode.offsetWidth + "px"');if(fixL)s.setExpression('left',fixL);if(fixT)s.setExpression('top',fixT)}else if(opts.centerY){if(full)s.setExpression('top','(document.documentElement.clientHeight || document.body.clientHeight) / 2 - (this.offsetHeight / 2) + (blah = document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop) + "px"');s.marginTop=0}else if(!opts.centerY&&full){var top=(opts.css&&opts.css.top)?parseInt(opts.css.top):0;var expression='((document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop) + '+top+') + "px"';s.setExpression('top',expression)}})}if(msg){if(opts.theme)lyr3.find('.ui-widget-content').append(msg);else lyr3.append(msg);if(msg.jquery||msg.nodeType)$(msg).show()}if(($.browser.msie||opts.forceIframe)&&opts.showOverlay)lyr1.show();if(opts.fadeIn){var cb=opts.onBlock?opts.onBlock:noOp;var cb1=(opts.showOverlay&&!msg)?cb:noOp;var cb2=msg?cb:noOp;if(opts.showOverlay)lyr2._fadeIn(opts.fadeIn,cb1);if(msg)lyr3._fadeIn(opts.fadeIn,cb2)}else{if(opts.showOverlay)lyr2.show();if(msg)lyr3.show();if(opts.onBlock)opts.onBlock()}bind(1,el,opts);if(full){pageBlock=lyr3[0];pageBlockEls=$(':input:enabled:visible',pageBlock);if(opts.focusInput)setTimeout(focus,20)}else center(lyr3[0],opts.centerX,opts.centerY);if(opts.timeout){var to=setTimeout(function(){full?$.unblockUI(opts):$(el).unblock(opts)},opts.timeout);$(el).data('blockUI.timeout',to)}};function remove(el,opts){var full=(el==window);var $el=$(el);var data=$el.data('blockUI.history');var to=$el.data('blockUI.timeout');if(to){clearTimeout(to);$el.removeData('blockUI.timeout')}opts=$.extend({},$.blockUI.defaults,opts||{});bind(0,el,opts);if(opts.onUnblock===null){opts.onUnblock=$el.data('blockUI.onUnblock');$el.removeData('blockUI.onUnblock')}var els;if(full)els=$('body').children().filter('.blockUI').add('body > .blockUI');else els=$('.blockUI',el);if(full)pageBlock=pageBlockEls=null;if(opts.fadeOut){els.fadeOut(opts.fadeOut);setTimeout(function(){reset(els,data,opts,el)},opts.fadeOut)}else reset(els,data,opts,el)};function reset(els,data,opts,el){els.each(function(i,o){if(this.parentNode)this.parentNode.removeChild(this)});if(data&&data.el){data.el.style.display=data.display;data.el.style.position=data.position;if(data.parent)data.parent.appendChild(data.el);$(el).removeData('blockUI.history')}if(typeof opts.onUnblock=='function')opts.onUnblock(el,opts)};function bind(b,el,opts){var full=el==window,$el=$(el);if(!b&&(full&&!pageBlock||!full&&!$el.data('blockUI.isBlocked')))return;if(!full)$el.data('blockUI.isBlocked',b);if(!opts.bindEvents||(b&&!opts.showOverlay))return;var events='mousedown mouseup keydown keypress';b?$(document).bind(events,opts,handler):$(document).unbind(events,handler)};function handler(e){if(e.keyCode&&e.keyCode==9){if(pageBlock&&e.data.constrainTabKey){var els=pageBlockEls;var fwd=!e.shiftKey&&e.target===els[els.length-1];var back=e.shiftKey&&e.target===els[0];if(fwd||back){setTimeout(function(){focus(back)},10);return false}}}var opts=e.data;if($(e.target).parents('div.'+opts.blockMsgClass).length>0)return true;return $(e.target).parents().children().filter('div.blockUI').length==0};function focus(back){if(!pageBlockEls)return;var e=pageBlockEls[back===true?pageBlockEls.length-1:0];if(e)e.focus()};function center(el,x,y){var p=el.parentNode,s=el.style;var l=((p.offsetWidth-el.offsetWidth)/2)-sz(p,'borderLeftWidth');var t=((p.offsetHeight-el.offsetHeight)/2)-sz(p,'borderTopWidth');if(x)s.left=l>0?(l+'px'):'0';if(y)s.top=t>0?(t+'px'):'0'};function sz(el,p){return parseInt($.css(el,p))||0}})(jQuery);

/* visualize plugin has been customized for color-code. do not replace with updated version of plugin without applying customizations - search for "color code" */
/**
* --------------------------------------------------------------------
* jQuery-Plugin "visualize"
* by Scott Jehl, scott@filamentgroup.com
* http://www.filamentgroup.com
* Copyright (c) 2009 Filament Group 
* Dual licensed under the MIT (filamentgroup.com/examples/mit-license.txt) and GPL (filamentgroup.com/examples/gpl-license.txt) licenses.
* 	
* --------------------------------------------------------------------
*/
(function ($) {
    $.fn.visualize = function (options, container) {
        return $(this).each(function () {
            //configuration
            var o = $.extend({
                type: 'bar', //also available: area, pie, line, 
                width: $(this).width(), //height of canvas - defaults to table height
                height: $(this).height(), //height of canvas - defaults to table height
                appendTitle: true, //table caption text is added to chart
                title: null, //grabs from table caption if null
                appendKey: true, //color key is added to chart
                //colors: ['#be1e2d','#666699','#92d5ea','#ee8310','#8d10ee','#5a3b16','#26a4ed','#f45a90','#e9e744'],
                colors: colorCodeColorOrder(this), //color code customization
                textColors: [], //corresponds with colors array. null/undefined items will fall back to CSS
                parseDirection: 'x', //which direction to parse the table data
                pieMargin: 20, //pie charts only - spacing around pie
                pieLabelPos: 'inside',
                lineWeight: 4, //for line and area - stroke weight
                barGroupMargin: 10,
                barMargin: 1 //space around bars in bar chart (added to both sides of bar)
            }, options);

            //reset width, height to numbers
            o.width = parseInt(o.width, 10);
            o.height = parseInt(o.height, 10);


            var self = $(this);

            //function to scrape data from html table
            function scrapeTable() {
                var colors = o.colors;
                var textColors = o.textColors;
                var tableData = {
                    dataGroups: function () {
                        var dataGroups = [];
                        if (o.parseDirection == 'x') {
                            self.find('tr:gt(0)').each(function (i) {
                                dataGroups[i] = {};
                                dataGroups[i].points = [];
                                dataGroups[i].color = colors[i];
                                if (textColors[i]) { dataGroups[i].textColor = textColors[i]; }
                                $(this).find('td').each(function () {
                                    dataGroups[i].points.push(parseInt($(this).text(), 10));
                                });
                            });
                        }
                        else {
                            var cols = self.find('tr:eq(1) td').size();
                            for (var i = 0; i < cols; i++) {
                                dataGroups[i] = {};
                                dataGroups[i].points = [];
                                dataGroups[i].color = colors[i];
                                if (textColors[i]) { dataGroups[i].textColor = textColors[i]; }
                                self.find('tr:gt(0)').each(function () {
                                    dataGroups[i].points.push($(this).find('td').eq(i).text() * 1);
                                });
                            };
                        }
                        return dataGroups;
                    },
                    allData: function () {
                        var allData = [];
                        $(this.dataGroups()).each(function () {
                            allData.push(this.points);
                        });
                        return allData;
                    },
                    dataSum: function () {
                        var dataSum = 0;
                        var allData = this.allData().join(',').split(',');
                        $(allData).each(function () {
                            dataSum += parseInt(this, 10);
                        });
                        return dataSum
                    },
                    topValue: function () {
                        var topValue = 0;
                        var allData = this.allData().join(',').split(',');
                        $(allData).each(function () {
                            if (parseInt(this, 10) > topValue) topValue = parseInt(this, 10);
                        });
                        return topValue;
                    },
                    bottomValue: function () {
                        var bottomValue = this.topValue();
                        var allData = this.allData().join(',').split(',');
                        $(allData).each(function () {
                            if (this < bottomValue) bottomValue = parseInt(this, 10);
                        });
                        return bottomValue;
                    },
                    memberTotals: function () {
                        var memberTotals = [];
                        var dataGroups = this.dataGroups();
                        $(dataGroups).each(function (l) {
                            var count = 0;
                            $(dataGroups[l].points).each(function (m) {
                                count += dataGroups[l].points[m];
                            });
                            memberTotals.push(count);
                        });
                        return memberTotals;
                    },
                    yTotals: function () {
                        var yTotals = [];
                        var dataGroups = this.dataGroups();
                        var loopLength = this.xLabels().length;
                        for (var i = 0; i < loopLength; i++) {
                            yTotals[i] = [];
                            var thisTotal = 0;
                            $(dataGroups).each(function (l) {
                                yTotals[i].push(this.points[i]);
                            });
                            yTotals[i].join(',').split(',');
                            $(yTotals[i]).each(function () {
                                thisTotal += parseInt(this);
                            });
                            yTotals[i] = thisTotal;

                        }
                        return yTotals;
                    },
                    topYtotal: function () {
                        var topYtotal = 0;
                        var yTotals = this.yTotals().join(',').split(',');
                        $(yTotals).each(function () {
                            if (parseInt(this, 10) > topYtotal) topYtotal = parseInt(this, 10);
                        });
                        return topYtotal;
                    },
                    totalYRange: function () {
                        return this.topValue() + Math.abs(this.bottomValue());
                    },
                    xLabels: function () {
                        var xLabels = [];
                        if (o.parseDirection == 'x') {
                            self.find('tr:eq(0) th').each(function () {
                                xLabels.push($(this).html());
                            });
                        }
                        else {
                            self.find('tr:gt(0) th').each(function () {
                                xLabels.push($(this).html());
                            });
                        }
                        return xLabels;
                    },
                    yLabels: function () {
                        var yLabels = [];
                        var chartHeight = o.height;
                        var numLabels = Math.round(chartHeight / 30);
                        //var totalRange = this.topValue() + Math.abs(this.bottomValue());
                        var loopInterval = Math.round(this.totalYRange() / Math.floor(numLabels)); //fix provided from lab
                        loopInterval = Math.max(loopInterval, 1);
                        for (var j = this.bottomValue(); j <= topValue; j += loopInterval) {
                            yLabels.push(j);
                        }
                        if (yLabels[yLabels.length - 1] != this.topValue()) {
                            yLabels.pop();
                            yLabels.push(this.topValue());
                        }
                        return yLabels;
                    }
                };

                return tableData;
            };


            //function to create a chart
            var createChart = {
                pie: function () {

                    canvasContain
					.addClass('visualize-pie');

                    if (o.pieLabelPos == 'outside') { canvasContain.addClass('visualize-pie-outside'); }

                    var centerx = Math.round(canvas.width() / 2);
                    var centery = Math.round(canvas.height() / 2);
                    var radius = centery - o.pieMargin;
                    var counter = 0.0;
                    var toRad = function (integer) { return (Math.PI / 180) * integer; };
                    var labels = $('<ul class="visualize-labels"></ul>')
					.insertAfter(canvas);

                    //draw the pie pieces
                    $.each(memberTotals, function (i) {

                        if (memberTotals[i] <= 0) { return; } //ie fix for zero values - color code customization

                        var fraction = (this < 0) ? 0 : this / dataSum;
                        ctx.beginPath();
                        ctx.moveTo(centerx, centery);
                        ctx.arc(centerx, centery, radius,
						counter * Math.PI * 2 - Math.PI * 0.5,
						(counter + fraction) * Math.PI * 2 - Math.PI * 0.5,
		                false);
                        ctx.lineTo(centerx, centery);
                        ctx.closePath();
                        ctx.fillStyle = dataGroups[i].color;
                        ctx.fill();
                        // draw labels
                        var sliceMiddle = (counter + fraction / 2);
                        var distance = o.pieLabelPos == 'inside' ? radius / 1.5 : radius + radius / 5;
                        var labelx = Math.round(centerx + Math.sin(sliceMiddle * Math.PI * 2) * (distance));
                        var labely = Math.round(centery - Math.cos(sliceMiddle * Math.PI * 2) * (distance));
                        var leftRight = (labelx > centerx) ? 'right' : 'left';
                        var topBottom = (labely > centery) ? 'bottom' : 'top';
                        var labeltext = $('<span class="visualize-label">' + Math.round(fraction * 100) + '%</span>')
			        	.css(leftRight, 0)
			        	.css(topBottom, 0);
                        var label = $('<li class="visualize-label-pos"></li>')
			       			.appendTo(labels)
			        		.css({ left: labelx, top: labely })
			        		.append(labeltext);
                        labeltext
			        	.css('font-size', radius / 8)
			        	.css('margin-' + leftRight, -labeltext.width() / 2)
			        	.css('margin-' + topBottom, -labeltext.outerHeight() / 2);

                        if (dataGroups[i].textColor) { labeltext.css('color', dataGroups[i].textColor); }
                        counter += fraction;
                    });
                },

                line: function (area) {

                    if (area) { canvasContain.addClass('visualize-area'); }
                    else { canvasContain.addClass('visualize-line'); }

                    //write X labels
                    var xInterval = canvas.width() / (xLabels.length - 1);
                    var xlabelsUL = $('<ul class="visualize-labels-x"></ul>')
					.width(canvas.width())
					.height(canvas.height())
					.insertBefore(canvas);
                    $.each(xLabels, function (i) {
                        var thisLi = $('<li><span>' + this + '</span></li>')
						.prepend('<span class="line" />')
						.css('left', xInterval * i)
						.appendTo(xlabelsUL);
                        var label = thisLi.find('span:not(.line)');
                        var leftOffset = label.width() / -2;
                        if (i == 0) { leftOffset = 0; }
                        else if (i == xLabels.length - 1) { leftOffset = -label.width(); }
                        label
						.css('margin-left', leftOffset)
						.addClass('label');
                    });

                    //write Y labels
                    var yScale = canvas.height() / totalYRange;
                    var liBottom = canvas.height() / (yLabels.length - 1);
                    var ylabelsUL = $('<ul class="visualize-labels-y"></ul>')
					.width(canvas.width())
					.height(canvas.height())
					.insertBefore(canvas);

                    $.each(yLabels, function (i) {
                        var thisLi = $('<li><span>' + this + '</span></li>')
						.prepend('<span class="line"  />')
						.css('bottom', liBottom * i)
						.prependTo(ylabelsUL);
                        var label = thisLi.find('span:not(.line)');
                        var topOffset = label.height() / -2;
                        if (i == 0) { topOffset = -label.height(); }
                        else if (i == yLabels.length - 1) { topOffset = 0; }
                        label
						.css('margin-top', topOffset)
						.addClass('label');
                    });

                    //start from the bottom left
                    ctx.translate(0, zeroLoc);
                    //iterate and draw
                    $.each(dataGroups, function (h) {
                        ctx.beginPath();
                        ctx.lineWidth = o.lineWeight;
                        ctx.lineJoin = 'round';
                        var points = this.points;
                        var integer = 0;
                        ctx.moveTo(0, -(points[0] * yScale));
                        $.each(points, function () {
                            ctx.lineTo(integer, -(this * yScale));
                            integer += xInterval;
                        });
                        ctx.strokeStyle = this.color;
                        ctx.stroke();
                        if (area) {
                            ctx.lineTo(integer, 0);
                            ctx.lineTo(0, 0);
                            ctx.closePath();
                            ctx.fillStyle = this.color;
                            ctx.globalAlpha = .3;
                            ctx.fill();
                            ctx.globalAlpha = 1.0;
                        }
                        else { ctx.closePath(); }
                    });
                },

                area: function () {
                    createChart.line(true);
                },

                bar: function () {
                    /**
                    * We can draw horizontal or vertical bars depending on the
                    * value of the 'barDirection' option (which may be 'vertical' or
                    * 'horizontal').
                    */

                    var horizontal = (o.barDirection == 'horizontal');

                    canvasContain.addClass('visualize-bar');

                    /**
                    * Write labels along the bottom of the chart.	If we're drawing
                    * horizontal bars, these will be the yLabels, otherwise they
                    * will be the xLabels.	The positioning also varies slightly:
                    * yLabels are values, hence they will span the whole width of
                    * the canvas, whereas xLabels are supposed to line up with the
                    * bars.
                    */
                    var bottomLabels = horizontal ? yLabels : xLabels;

                    var xInterval = canvas.width() / (bottomLabels.length - (horizontal ? 1 : 0));

                    var xlabelsUL = $('<ul class="visualize-labels-x"></ul>')
					.width(canvas.width())
					.height(canvas.height())
					.insertBefore(canvas);

                    $.each(bottomLabels, function (i) {
                        var thisLi = $('<li><span class="label">' + this + '</span></li>')
						.prepend('<span class="line" />')
						.css('left', xInterval * i)
						.width(xInterval)
						.appendTo(xlabelsUL);

                        if (horizontal) {
                            var label = thisLi.find('span.label');
                            label.css("margin-left", -label.width() / 2);
                        }
                    });

                    /**
                    * Write labels along the left of the chart.	Follows the same idea
                    * as the bottom labels.
                    */
                    leftLabels = horizontal ? xLabels : yLabels;
                    var liBottom = canvas.height() / (leftLabels.length - (horizontal ? 0 : 1));

                    var ylabelsUL = $('<ul class="visualize-labels-y"></ul>')
					.width(canvas.width())
					.height(canvas.height())
					.insertBefore(canvas);

                    $.each(leftLabels, function (i) {
                        var thisLi = $('<li><span>' + this + '</span></li>').prependTo(ylabelsUL);

                        var label = thisLi.find('span:not(.line)').addClass('label');

                        if (horizontal) {
                            /**
                            * For left labels, we want to vertically align the text
                            * to the middle of its container, but we don't know how
                            * many lines of text we will have, since the labels could
                            * be very long.
                            *
                            * So we set a min-height of liBottom, and a max-height
                            * of liBottom + 1, so we can then check the label's actual
                            * height to determine if it spans one line or more lines.
                            */
                            label.css({
                                'min-height': liBottom,
                                'max-height': liBottom + 1,
                                'vertical-align': 'middle'
                            });
                            thisLi.css({ 'top': liBottom * i, 'min-height': liBottom });

                            r = label[0].getClientRects()[0];
                            if (r.bottom - r.top == liBottom) {
                                /* This means we have only one line of text; hence
                                * we can centre the text vertically by setting the line-height,
                                * as described at:
                                *   http://www.ampsoft.net/webdesign-l/vertical-aligned-nav-list.html
                                *
                                * (Although firefox has .height on the rectangle, IE doesn't,
                                * so we use r.bottom - r.top rather than r.height.)
                                */
                                label.css('line-height', parseInt(liBottom) + 'px');
                            }
                            else {
                                /*
                                * If there is more than one line of text, then we shouldn't
                                * touch the line height, but we should make sure the text
                                * doesn't overflow the container.
                                */
                                label.css("overflow", "hidden");
                            }
                        }
                        else {
                            thisLi.css('bottom', liBottom * i).prepend('<span class="line" />');
                            label.css('margin-top', -label.height() / 2)
                        }
                    });

                    // Draw bars

                    if (horizontal) {
                        // for horizontal, keep the same code, but rotate everything 90 degrees
                        // clockwise.
                        ctx.rotate(Math.PI / 2);
                    }
                    else {
                        // for vertical, translate to the top left corner.
                        ctx.translate(0, zeroLoc);
                    }

                    // Don't attempt to draw anything if all the values are zero,
                    // otherwise we will get weird exceptions from the canvas methods.
                    if (totalYRange <= 0)
                        return;

                    var yScale = (horizontal ? canvas.width() : canvas.height()) / totalYRange;
                    var barWidth = horizontal ? (canvas.height() / xLabels.length) : (canvas.width() / (bottomLabels.length));
                    var linewidth = (barWidth - o.barGroupMargin * 2) / dataGroups.length;

                    for (var h = 0; h < dataGroups.length; h++) {
                        ctx.beginPath();

                        var strokeWidth = linewidth - (o.barMargin * 2);
                        ctx.lineWidth = strokeWidth;
                        var points = dataGroups[h].points;
                        var integer = 0;
                        for (var i = 0; i < points.length; i++) {
                            // If the last value is zero, IE will go nuts and not draw anything,
                            // so don't try to draw zero values at all.
                            if (points[i] != 0) {
                                var xVal = (integer - o.barGroupMargin) + (h * linewidth) + linewidth / 2;
                                xVal += o.barGroupMargin * 2;

                                ctx.moveTo(xVal, 0);
                                ctx.lineTo(xVal, Math.round(-points[i] * yScale));
                            }
                            integer += barWidth;
                        }
                        ctx.strokeStyle = dataGroups[h].color;
                        ctx.stroke();
                        ctx.closePath();
                    }
                }
            };

            //create new canvas, set w&h attrs (not inline styles)
            var canvasNode = document.createElement("canvas");
            var canvas = $(canvasNode)
			.attr({
			    'height': o.height,
			    'width': o.width
			});



            //create canvas wrapper div, set inline w&h, append
            var canvasContain = (container || $('<div class="visualize" role="presentation" />'))
			.height(o.height)
			.width(o.width)
			.append(canvas);

            //scrape table (this should be cleaned up into an obj)
            var tableData = scrapeTable();
            var dataGroups = tableData.dataGroups();
            var allData = tableData.allData();
            var dataSum = tableData.dataSum();
            var topValue = tableData.topValue();
            var bottomValue = tableData.bottomValue();
            var memberTotals = tableData.memberTotals();
            var totalYRange = tableData.totalYRange();


            var zeroLoc = o.height * (topValue / totalYRange);
            //	console.log(zeroLoc);

            var xLabels = tableData.xLabels();
            var yLabels = tableData.yLabels();

            //title/key container
            if (o.appendTitle || o.appendKey) {
                var infoContain = $('<div class="visualize-info"></div>')
				.appendTo(canvasContain);
            }

            //append title
            if (o.appendTitle) {
                var title = o.title || self.find('caption').text();
                $('<div class="visualize-title">' + title + '</div>')
				.appendTo(infoContain);
            }

            //append key
            if (o.appendKey) {
                var newKey = $('<ul class="visualize-key"></ul>');
                var selector = (o.parseDirection == 'x') ? 'tr:gt(0) th' : 'tr:eq(0) th';
                self.find(selector).each(function (i) {
                    //$('<li><span class="visualize-key-color" style="background: '+dataGroups[i].color+'"></span><span class="visualize-key-label">'+ $(this).text() +'</span></li>')
                    $('<li><span class="visualize-key-color" style="background: ' + dataGroups[i].color + '"></span><span class="visualize-key-label">' + $(this).text() + '&nbsp;<span class="visualize-key-value">' + memberTotals[i] + '%</span></span></li>')//color-code customization
					.appendTo(newKey);
                });
                newKey.appendTo(infoContain);
            };

            //append new canvas to page

            if (!container) { canvasContain.insertAfter(this); }
            if (typeof (G_vmlCanvasManager) != 'undefined') { G_vmlCanvasManager.initElement(canvas[0]); }

            //set up the drawing board	
            var ctx = canvas[0].getContext('2d');

            //create chart
            createChart[o.type]();

            //clean up some doubled lines that sit on top of canvas borders (done via JS due to IE)
            $('.visualize-line li:first-child span.line, .visualize-line li:last-child span.line, .visualize-area li:first-child span.line, .visualize-area li:last-child span.line, .visualize-bar li:first-child span.line,.visualize-bar .visualize-labels-y li:last-child span.line').css('border', 'none');
            if (!container) {
                //add event for updating
                canvasContain.bind('visualizeRefresh', function () {
                    self.visualize(o, $(this).empty());
                });
            }
        }).next(); //returns canvas(es)
    };
})(jQuery);

/*
Watermark v3.1.1 (January 10, 2011) plugin for jQuery
http://jquery-watermark.googlecode.com/
Copyright (c) 2009-2011 Todd Northrop
http://www.speednet.biz/
Dual licensed under the MIT or GPL Version 2 licenses.
*/
(function (b, j, y) { var p = "function", k = "password", d = "maxLength", g = "type", a = "", c = true, o = "placeholder", e = false, w = "watermark", l = w, i = "watermarkClass", t = "watermarkFocus", n = "watermarkSubmit", r = "watermarkMaxLength", h = "watermarkPassword", f = "watermarkText", m = /\r/g, v = ":data(" + l + ")", q = ":text,:password,:search,textarea", s = ["Page_ClientValidate"], u = e, x = o in document.createElement("input"); b.watermark = b.watermark || { version: "3.1.1", runOnce: c, options: { className: w, useNative: c, hideBeforeUnload: c }, hide: function (a) { b(a).filter(v).each(function () { b.watermark._hide(b(this)) }) }, _hide: function (b, q) { var o = b[0], p = (o.value || a).replace(m, a), l = b.data(f) || a, n = b.data(r) || 0, k = b.data(i); if (l.length && p == l) { o.value = a; if (b.data(h)) if ((b.attr(g) || a) === "text") { var e = b.data(h) || [], c = b.parent() || []; if (e.length && c.length) { c[0].removeChild(b[0]); c[0].appendChild(e[0]); b = e } } if (n) { b.attr(d, n); b.removeData(r) } if (q) { b.attr("autocomplete", "off"); j.setTimeout(function () { b.select() }, 1) } } k && b.removeClass(k) }, show: function (a) { b(a).filter(v).each(function () { b.watermark._show(b(this)) }) }, _show: function (e) { var p = e[0], v = (p.value || a).replace(m, a), j = e.data(f) || a, q = e.attr(g) || a, s = e.data(i); if ((v.length == 0 || v == j) && !e.data(t)) { u = c; if (e.data(h)) if (q === k) { var o = e.data(h) || [], n = e.parent() || []; if (o.length && n.length) { n[0].removeChild(e[0]); n[0].appendChild(o[0]); e = o; e.attr(d, j.length); p = e[0] } } if (q === "text" || q === "search") { var l = e.attr(d) || 0; if (l > 0 && j.length > l) { e.data(r, l); e.attr(d, j.length) } } s && e.addClass(s); p.value = j } else b.watermark._hide(e) }, hideAll: function () { if (u) { b.watermark.hide(q); u = e } }, showAll: function () { b.watermark.show(q) } }; b.fn.watermark = b.fn.watermark || function (s, r) { var u = "string"; if (!this.length) return this; var w = e, v = typeof s === u; if (v) s = s.replace(m, a); if (typeof r === "object") { w = typeof r.className === u; r = b.extend({}, b.watermark.options, r) } else if (typeof r === u) { w = c; r = b.extend({}, b.watermark.options, { className: r }) } else r = b.watermark.options; if (typeof r.useNative !== p) r.useNative = r.useNative ? function () { return c } : function () { return e }; return this.each(function () { var z = "dragleave", y = "dragenter", B = this, e = b(B); if (!e.is(q)) return; if (e.data(l)) { if (v || w) { b.watermark._hide(e); v && e.data(f, s); w && e.data(i, r.className) } } else { if (x && r.useNative.call(B, e) && (e.attr("tagName") || a) !== "TEXTAREA") { v && e.attr(o, s); return } e.data(f, v ? s : a); e.data(i, r.className); e.data(l, 1); if ((e.attr(g) || a) === k) { var C = e.wrap("<span>").parent(), p = b(C.html().replace(/type=["']?password["']?/i, 'type="text"')); p.data(f, e.data(f)); p.data(i, e.data(i)); p.data(l, 1); p.attr(d, s.length); p.focus(function () { b.watermark._hide(p, c) }).bind(y, function () { b.watermark._hide(p) }).bind("dragend", function () { j.setTimeout(function () { p.blur() }, 1) }); e.blur(function () { b.watermark._show(e) }).bind(z, function () { b.watermark._show(e) }); p.data(h, e); e.data(h, p) } else e.focus(function () { e.data(t, 1); b.watermark._hide(e, c) }).blur(function () { e.data(t, 0); b.watermark._show(e) }).bind(y, function () { b.watermark._hide(e) }).bind(z, function () { b.watermark._show(e) }).bind("dragend", function () { j.setTimeout(function () { b.watermark._show(e) }, 1) }).bind("drop", function (d) { var c = e[0], b = d.originalEvent.dataTransfer.getData("Text"); if ((c.value || a).replace(m, a).replace(b, a) === e.data(f)) c.value = b; e.focus() }); if (B.form) { var u = B.form, A = b(u); if (!A.data(n)) { A.submit(b.watermark.hideAll); if (u.submit) { A.data(n, u.submit); u.submit = function (c, a) { return function () { var d = a.data(n); b.watermark.hideAll(); if (d.apply) d.apply(c, Array.prototype.slice.call(arguments)); else d() } } (u, A) } else { A.data(n, 1); u.submit = function (a) { return function () { b.watermark.hideAll(); delete a.submit; a.submit() } } (u) } } } } b.watermark._show(e) }) }; if (b.watermark.runOnce) { b.watermark.runOnce = e; b.extend(b.expr[":"], { search: function (b) { return "search" === (b.type || a) }, data: function (c, d, a) { return !!b.data(c, a[3]) } }); (function (c) { b.fn.val = function () { var d = this; if (!d.length) return arguments.length ? d : y; if (!arguments.length) if (d.data(l)) { var e = (d[0].value || a).replace(m, a); return e === (d.data(f) || a) ? a : e } else return c.apply(d, arguments); else { c.apply(d, arguments); b.watermark.show(d); return d } } })(b.fn.val); s.length && b(function () { for (var a, c, d = s.length - 1; d >= 0; d--) { a = s[d]; c = j[a]; if (typeof c === p) j[a] = function (a) { return function () { b.watermark.hideAll(); return a.apply(null, Array.prototype.slice.call(arguments)) } } (c) } }); b(j).bind("beforeunload", function () { b.watermark.options.hideBeforeUnload && b.watermark.hideAll() }) } })(jQuery, window);

//// JQUERY PLUGIN -jcarousellite

/* ============== PLEASE NOTICE THAT THIS JQUERY PLUGIN -jcarousellite WAS "MODIFIED" =========================
================= DO NOT UPGRADES AND OVERWRITES THE CHANGES                          ========================= */

/**
* jCarouselLite - jQuery plugin to navigate images/any content in a carousel style widget.
* @requires jQuery v1.2 or above
*
* http://gmarwaha.com/jquery/jcarousellite/
*
* Copyright (c) 2007 Ganeshji Marwaha (gmarwaha.com)
* Dual licensed under the MIT and GPL licenses:
* http://www.opensource.org/licenses/mit-license.php
* http://www.gnu.org/licenses/gpl.html
*
* Version: 1.0.1
* Note: Requires jquery 1.2 or above from version 1.0.1
*/

/**
* Creates a carousel-style navigation widget for images/any-content from a simple HTML markup.
*
* The HTML markup that is used to build the carousel can be as simple as...
*
*  <div class="carousel">
*      <ul>
*          <li><img src="image/1.jpg" alt="1"></li>
*          <li><img src="image/2.jpg" alt="2"></li>
*          <li><img src="image/3.jpg" alt="3"></li>
*      </ul>
*  </div>
*
* As you can see, this snippet is nothing but a simple div containing an unordered list of images.
* You don't need any special "class" attribute, or a special "css" file for this plugin.
* I am using a class attribute just for the sake of explanation here.
*
* To navigate the elements of the carousel, you need some kind of navigation buttons.
* For example, you will need a "previous" button to go backward, and a "next" button to go forward.
* This need not be part of the carousel "div" itself. It can be any element in your page.
* Lets assume that the following elements in your document can be used as next, and prev buttons...
*
* <button class="prev">&lt;&lt;</button>
* <button class="next">&gt;&gt;</button>
*
* Now, all you need to do is call the carousel component on the div element that represents it, and pass in the
* navigation buttons as options.
*
* $(".carousel").jCarouselLite({
*      btnNext: ".next",
*      btnPrev: ".prev"
* });
*
* That's it, you would have now converted your raw div, into a magnificient carousel.
*
* There are quite a few other options that you can use to customize it though.
* Each will be explained with an example below.
*
* @param an options object - You can specify all the options shown below as an options object param.
*
* @option btnPrev, btnNext : string - no defaults
* @example
* $(".carousel").jCarouselLite({
*      btnNext: ".next",
*      btnPrev: ".prev"
* });
* @desc Creates a basic carousel. Clicking "btnPrev" navigates backwards and "btnNext" navigates forward.
*
* @option btnGo - array - no defaults
* @example
* $(".carousel").jCarouselLite({
*      btnNext: ".next",
*      btnPrev: ".prev",
*      btnGo: [".0", ".1", ".2"]
* });
* @desc If you don't want next and previous buttons for navigation, instead you prefer custom navigation based on
* the item number within the carousel, you can use this option. Just supply an array of selectors for each element
* in the carousel. The index of the array represents the index of the element. What i mean is, if the
* first element in the array is ".0", it means that when the element represented by ".0" is clicked, the carousel
* will slide to the first element and so on and so forth. This feature is very powerful. For example, i made a tabbed
* interface out of it by making my navigation elements styled like tabs in css. As the carousel is capable of holding
* any content, not just images, you can have a very simple tabbed navigation in minutes without using any other plugin.
* The best part is that, the tab will "slide" based on the provided effect. :-)
*
* @option mouseWheel : boolean - default is false
* @example
* $(".carousel").jCarouselLite({
*      mouseWheel: true
* });
* @desc The carousel can also be navigated using the mouse wheel interface of a scroll mouse instead of using buttons.
* To get this feature working, you have to do 2 things. First, you have to include the mouse-wheel plugin from brandon.
* Second, you will have to set the option "mouseWheel" to true. That's it, now you will be able to navigate your carousel
* using the mouse wheel. Using buttons and mouseWheel or not mutually exclusive. You can still have buttons for navigation
* as well. They complement each other. To use both together, just supply the options required for both as shown below.
* @example
* $(".carousel").jCarouselLite({
*      btnNext: ".next",
*      btnPrev: ".prev",
*      mouseWheel: true
* });
*
* @option auto : number - default is null, meaning autoscroll is disabled by default
* @example
* $(".carousel").jCarouselLite({
*      auto: 800,
*      speed: 500
* });
* @desc You can make your carousel auto-navigate itself by specfying a millisecond value in this option.
* The value you specify is the amount of time between 2 slides. The default is null, and that disables auto scrolling.
* Specify this value and magically your carousel will start auto scrolling.
*
* @option speed : number - 200 is default
* @example
* $(".carousel").jCarouselLite({
*      btnNext: ".next",
*      btnPrev: ".prev",
*      speed: 800
* });
* @desc Specifying a speed will slow-down or speed-up the sliding speed of your carousel. Try it out with
* different speeds like 800, 600, 1500 etc. Providing 0, will remove the slide effect.
*
* @option easing : string - no easing effects by default.
* @example
* $(".carousel").jCarouselLite({
*      btnNext: ".next",
*      btnPrev: ".prev",
*      easing: "bounceout"
* });
* @desc You can specify any easing effect. Note: You need easing plugin for that. Once specified,
* the carousel will slide based on the provided easing effect.
*
* @option vertical : boolean - default is false
* @example
* $(".carousel").jCarouselLite({
*      btnNext: ".next",
*      btnPrev: ".prev",
*      vertical: true
* });
* @desc Determines the direction of the carousel. true, means the carousel will display vertically. The next and
* prev buttons will slide the items vertically as well. The default is false, which means that the carousel will
* display horizontally. The next and prev items will slide the items from left-right in this case.
*
* @option circular : boolean - default is true
* @example
* $(".carousel").jCarouselLite({
*      btnNext: ".next",
*      btnPrev: ".prev",
*      circular: false
* });
* @desc Setting it to true enables circular navigation. This means, if you click "next" after you reach the last
* element, you will automatically slide to the first element and vice versa. If you set circular to false, then
* if you click on the "next" button after you reach the last element, you will stay in the last element itself
* and similarly for "previous" button and first element.
*
* @option visible : number - default is 3
* @example
* $(".carousel").jCarouselLite({
*      btnNext: ".next",
*      btnPrev: ".prev",
*      visible: 4
* });
* @desc This specifies the number of items visible at all times within the carousel. The default is 3.
* You are even free to experiment with real numbers. Eg: "3.5" will have 3 items fully visible and the
* last item half visible. This gives you the effect of showing the user that there are more images to the right.
*
* @option start : number - default is 0
* @example
* $(".carousel").jCarouselLite({
*      btnNext: ".next",
*      btnPrev: ".prev",
*      start: 2
* });
* @desc You can specify from which item the carousel should start. Remember, the first item in the carousel
* has a start of 0, and so on.
*
* @option scrool : number - default is 1
* @example
* $(".carousel").jCarouselLite({
*      btnNext: ".next",
*      btnPrev: ".prev",
*      scroll: 2
* });
* @desc The number of items that should scroll/slide when you click the next/prev navigation buttons. By
* default, only one item is scrolled, but you may set it to any number. Eg: setting it to "2" will scroll
* 2 items when you click the next or previous buttons.
*
* @option beforeStart, afterEnd : function - callbacks
* @example
* $(".carousel").jCarouselLite({
*      btnNext: ".next",
*      btnPrev: ".prev",
*      beforeStart: function(a) {
*          alert("Before animation starts:" + a);
*      },
*      afterEnd: function(a) {
*          alert("After animation ends:" + a);
*      }
* });
* @desc If you wanted to do some logic in your page before the slide starts and after the slide ends, you can
* register these 2 callbacks. The functions will be passed an argument that represents an array of elements that
* are visible at the time of callback.
*
*
* @cat Plugins/Image Gallery
* @author Ganeshji Marwaha/ganeshread@gmail.com
*/

/* ============== PLEASE NOTICE THAT THIS JQUERY PLUGIN -jcarousellite WAS "MODIFIED" =========================
================= DO NOT UPGRADES AND OVERWRITES THE CHANGES                          ========================= */

(function ($) {                                          // Compliant with jquery.noConflict()
    $.fn.jCarouselLite = function (o) {
        o = $.extend({
            btnPrev: null,
            btnNext: null,
            btnGo: null,
            mouseWheel: false,
            auto: null,
            speed: 200,
            easing: null,
            vertical: false,
            circular: true,
            visible: 3,
            start: 0,
            scroll: 1,
            colNum: 1, //J - for the # of columns, it'll control height of container div.
            beforeStart: null,
            afterEnd: null,
            pageSize: 1 //number of visible items per page
        }, o || {});

        //unbind since this will be getting called alot
        $(o.btnPrev).unbind('click');
        $(o.btnNext).unbind('click');
        return this.each(function () {                           // Returns the element collection. Chainable.

            var running = false,
			animCss = o.vertical ? "top" : "left",
			sizeCss = o.vertical ? "height" : "width";

            var div = $(this),
			ul = $("ul", div),
			tLi = $("li", ul),
			tl = tLi.size(),
			v = o.visible;

            if (o.circular) {
                ul.prepend(tLi.slice(tl - v - 1 + 1).clone())
              .append(tLi.slice(0, v).clone());
                o.start += v;
            }
            var siteDirection = $("html").attr("dir");

            var li = $("li", ul),
			itemLength = li.size(),
			curr = (div.attr("currIdx")) ? parseInt(div.attr("currIdx")) : o.start;

            div.css("visibility", "visible");
            if (siteDirection == "rtl") {
                li.css({ overflow: "hidden", float: o.vertical ? "none" : "right" });
            }
            else {
                li.css({ overflow: "hidden", float: o.vertical ? "none" : "left" });
            }
            ul.css({ margin: "0", padding: "0", position: "relative", "list-style-type": "none", "z-index": "1" });
            div.css({ overflow: "hidden", position: "relative", "z-index": "2", left: "0px" });

            var liSize = o.vertical ? height(li) : width(li);   // Full li size(incl margin)-Used for animation
            var ulSize = liSize * Math.round(itemLength / o.colNum); //J
            //var ulSize = liSize * itemLength;                   // size of full ul(total length, not just for the visible items)
            var divSize = liSize * v;                           // size of entire div(total length for just the visible items)

            //console.log("itemLength:", itemLength, "liSize:",liSize, "ulSize:",ulSize, "divSize:",divSize);

            li.css({ width: li.width(), height: li.height() });
            ul.css(sizeCss, ulSize + "px").css(animCss, -(curr * liSize));

            var pos = ul.offset();
            //console.log(pos.top);

            div.css(sizeCss, divSize + "px");                     // Width of the DIV. length of visible images

            if (o.btnPrev)
                $(o.btnPrev).click(function () {
                    return go(curr - o.scroll);
                });

            if (o.btnNext)
                $(o.btnNext).click(function () {
                    return go(curr + o.scroll)
                    //, console.log("curr:", curr); //J
                });

            if (o.btnGo)
                $.each(o.btnGo, function (i, val) {
                    $(val).click(function () {
                        return go(o.circular ? o.visible + i : i);
                    });
                });

            if (o.mouseWheel && div.mousewheel)
                div.mousewheel(function (e, d) {
                    return d > 0 ? go(curr - o.scroll) : go(curr + o.scroll);
                });

            if (o.auto)
                setInterval(function () {
                    go(curr + o.scroll);
                }, o.auto + o.speed);

            function vis() {
                return li.slice(curr).slice(0, v);
            };

            function go(to) {
                if (!running) {

                    if (o.beforeStart)
                        o.beforeStart.call(this, vis());

                    if (o.circular) {            // If circular we are in first or last, then goto the other end
                        if (to <= o.start - v - 1) {           // If first, then goto last
                            ul.css(animCss, -((itemLength - (v * 2)) * liSize) + "px");
                            // If "scroll" > 1, then the "to" might not be equal to the condition; it can be lesser depending on the number of elements.
                            curr = to == o.start - v - 1 ? itemLength - (v * 2) - 1 : itemLength - (v * 2) - o.scroll;
                        } else if (to >= itemLength - v + 1) { // If last, then goto first
                            ul.css(animCss, -((v) * liSize) + "px");
                            // If "scroll" > 1, then the "to" might not be equal to the condition; it can be greater depending on the number of elements.
                            curr = to == itemLength - v + 1 ? v + 1 : v + o.scroll;
                        } else curr = to;
                    } else {                    // If non-circular and to points to first or last, we just return.
                        if (to < 0 || to > Math.round((itemLength - v) / o.colNum) || itemLength <= o.pageSize) return false; //J
                        //if(to < 0 || to > itemLength-v) return;
                        else curr = to;
                        //console.log("to:", to);
                    }
                    // If neither overrides it, the curr will still be "to" and we can proceed.
                    running = true;

                    if (siteDirection == "rtl") {
                        ul.animate(
                        animCss == "left" ? { left: +(curr * liSize)} : { top: -(curr * liSize) }, o.speed, o.easing,
                        function () {
                            if (o.afterEnd)
                                o.afterEnd.call(this, vis());
                            running = false;
                        }
                        );
                    }
                    else {
                        ul.animate(
                        animCss == "left" ? { left: -(curr * liSize)} : { top: -(curr * liSize) }, o.speed, o.easing,
                        function () {
                            if (o.afterEnd)
                                o.afterEnd.call(this, vis());
                            running = false;
                        }
                        );
                    }
                    //, console.log("curr * liSize:", curr*liSize) // J

                    // Disable buttons when the carousel reaches the last/first, and enable when not
                    if (!o.circular) {
                        $(o.btnPrev + "," + o.btnNext).removeClass("disabled");
                        $((curr - o.scroll < 0 && o.btnPrev)
                        ||
						(curr + o.scroll > Math.round((itemLength - v) / o.colNum) && o.btnNext)	//J
                        //(curr + o.scroll > itemLength - v && o.btnNext)
                        ||
                       []
                     ).addClass("disabled");
                        //console.log("curr:", curr, "curr + o.scroll:", curr + o.scroll, "itemLength - v", itemLength - v);
                    }
                }
                div.attr("currIdx", curr);
                return false;
            };
            //added by arod
            if (!o.circular) {
                $(o.btnNext).removeClass("disabled");
                if (curr + (o.scroll) > Math.round((itemLength - v) / o.colNum)
                || itemLength <= o.pageSize) { $(o.btnNext).addClass("disabled"); }
            }
            //            console.log("curr:", curr, "curr + o.scroll:", curr + o.scroll, "curr + 2*o.scroll:", curr + (2*o.scroll), "Math.round((itemLength - v) / o.colNum):", Math.round((itemLength - v) / o.colNum));
        });
    };

    function css(el, prop) {
        return parseInt($.css(el[0], prop)) || 0;
    };
    function width(el) {
        return el[0].offsetWidth + css(el, 'marginLeft') + css(el, 'marginRight');
    };
    function height(el) {
        return el[0].offsetHeight + css(el, 'marginTop') + css(el, 'marginBottom');
    };
})(jQuery);

//// JQUERY PLUGIN - MOUSE WHEEL
(function($){var types=['DOMMouseScroll','mousewheel'];if($.event.fixHooks){for(var i=types.length;i;){$.event.fixHooks[types[--i]]=$.event.mouseHooks}}$.event.special.mousewheel={setup:function(){if(this.addEventListener){for(var i=types.length;i;){this.addEventListener(types[--i],handler,false)}}else{this.onmousewheel=handler}},teardown:function(){if(this.removeEventListener){for(var i=types.length;i;){this.removeEventListener(types[--i],handler,false)}}else{this.onmousewheel=null}}};$.fn.extend({mousewheel:function(fn){return fn?this.bind("mousewheel",fn):this.trigger("mousewheel")},unmousewheel:function(fn){return this.unbind("mousewheel",fn)}});function handler(event){var orgEvent=event||window.event,args=[].slice.call(arguments,1),delta=0,returnValue=true,deltaX=0,deltaY=0;event=$.event.fix(orgEvent);event.type="mousewheel";if(orgEvent.wheelDelta){delta=orgEvent.wheelDelta/120}if(orgEvent.detail){delta=-orgEvent.detail/3}deltaY=delta;if(orgEvent.axis!==undefined&&orgEvent.axis===orgEvent.HORIZONTAL_AXIS){deltaY=0;deltaX=-1*delta}if(orgEvent.wheelDeltaY!==undefined){deltaY=orgEvent.wheelDeltaY/120}if(orgEvent.wheelDeltaX!==undefined){deltaX=-1*orgEvent.wheelDeltaX/120}args.unshift(event,delta,deltaX,deltaY);return($.event.dispatch||$.event.handle).apply(this,args)}})(jQuery);

/* --- Expander --- truncates text and adds a more/less link <http://plugins.learningjquery.com/expander/#getting-started> */
(function ($) { $.fn.expander = function (options) { var opts = $.extend({}, $.fn.expander.defaults, options); var delayedCollapse; return this.each(function () { var $this = $(this); var o = $.meta ? $.extend({}, opts, $this.data()) : opts; var cleanedTag, startTags, endTags; var allText = $this.html(); var startText = allText.slice(0, o.slicePoint).replace(/\w+$/, ''); startTags = startText.match(/<\w[^>]*>/g); if (startTags) { startText = allText.slice(0, o.slicePoint + startTags.join('').length).replace(/\w+$/, '') } if (startText.lastIndexOf('<') > startText.lastIndexOf('>')) { startText = startText.slice(0, startText.lastIndexOf('<')) } var endText = allText.slice(startText.length); if (!$('span.details', this).length) { if (endText.replace(/\s+$/, '').split(' ').length < o.widow) { return } if (endText.indexOf('</') > -1) { endTags = endText.match(/<(\/)?[^>]*>/g); for (var i = 0; i < endTags.length; i++) { if (endTags[i].indexOf('</') > -1) { var startTag, startTagExists = false; for (var j = 0; j < i; j++) { startTag = endTags[j].slice(0, endTags[j].indexOf(' ')).replace(/(\w)$/, '$1>'); if (startTag == rSlash(endTags[i])) { startTagExists = true } } if (!startTagExists) { startText = startText + endTags[i]; var matched = false; for (var s = startTags.length - 1; s >= 0; s--) { if (startTags[s].slice(0, startTags[s].indexOf(' ')).replace(/(\w)$/, '$1>') == rSlash(endTags[i]) && matched == false) { cleanedTag = cleanedTag ? startTags[s] + cleanedTag : startTags[s]; matched = true } } } } } endText = cleanedTag && cleanedTag + endText || endText } $this.html([startText, '<span class="read-more">', o.expandPrefix, '<a href="#">', o.expandText, '</a>', '</span>', '<span class="details">', endText, '</span>'].join('')) } var $thisDetails = $('span.details', this), $readMore = $('span.read-more', this); $thisDetails.hide(); $readMore.find('a').click(function () { $readMore.hide(); if (o.expandEffect === 'show' && !o.expandSpeed) { o.beforeExpand($this); $thisDetails.show(); o.afterExpand($this); delayCollapse(o, $thisDetails) } else { o.beforeExpand($this); $thisDetails[o.expandEffect](o.expandSpeed, function () { $thisDetails.css({ zoom: '' }); o.afterExpand($this); delayCollapse(o, $thisDetails) }) } return false }); if (o.userCollapse) { $this.find('span.details').append('<span class="re-collapse">' + o.userCollapsePrefix + '<a href="#">' + o.userCollapseText + '</a></span>'); $this.find('span.re-collapse a').click(function () { clearTimeout(delayedCollapse); var $detailsCollapsed = $(this).parents('span.details'); reCollapse($detailsCollapsed); o.onCollapse($this, true); return false }) } }); function reCollapse(el) { el.hide().prev('span.read-more').show() } function delayCollapse(option, $collapseEl) { if (option.collapseTimer) { delayedCollapse = setTimeout(function () { reCollapse($collapseEl); option.onCollapse($collapseEl.parent(), false) }, option.collapseTimer) } } function rSlash(rString) { return rString.replace(/\//, '') } }; $.fn.expander.defaults = { slicePoint: 100, widow: 4, expandText: 'read more', expandPrefix: '&hellip; ', collapseTimer: 0, expandEffect: 'fadeIn', expandSpeed: '', userCollapse: true, userCollapseText: '[collapse expanded text]', userCollapsePrefix: ' ', beforeExpand: function ($thisEl) { }, afterExpand: function ($thisEl) { }, onCollapse: function ($thisEl, byUser) { } } })(jQuery);

/* jQuery.NobleCount v 1.0 http://tpgblog.com/noblecount/
compiled by http://yui.2clics.net/ */
(function (c) { c.fn.NobleCount = function (i, h) { var j; var g = false; if (typeof i == "string") { j = c.extend({}, c.fn.NobleCount.settings, h); if (typeof h != "undefined") { g = ((typeof h.max_chars == "number") ? true : false) } return this.each(function () { var k = c(this); f(k, i, j, g) }) } return this }; c.fn.NobleCount.settings = { on_negative: null, on_positive: null, on_update: null, max_chars: 140, block_negative: false, cloak: false, in_dom: false }; function f(g, m, n, h) { var l = n.max_chars; var j = c(m); if (!h) { var k = j.text(); var i = (/^[1-9]\d*$/).test(k); if (i) { l = k } } b(g, j, n, l, true); c(g).keydown(function (o) { b(g, j, n, l, false); if (a(o, g, n, l) == false) { return false } }); c(g).keyup(function (o) { b(g, j, n, l, false); if (a(o, g, n, l) == false) { return false } }) } function a(k, g, l, j) { if (l.block_negative) { var h = k.which; var i; if (typeof document.selection != "undefined") { i = (document.selection.createRange().text.length > 0) } else { i = (g[0].selectionStart != g[0].selectionEnd) } if ((!((e(g, j) < 1) && (h > 47 || h == 32 || h == 0 || h == 13) && !k.ctrlKey && !k.altKey && !i)) == false) { return false } } return true } function e(g, h) { return h - (c(g).val()).length } function b(g, i, l, j, h) { var k = e(g, j); if (k < 0) { d(l.on_negative, l.on_positive, g, i, l, k) } else { d(l.on_positive, l.on_negative, g, i, l, k) } if (l.cloak) { if (l.in_dom) { i.attr("data-noblecount", k) } } else { i.text(k) } if (!h && jQuery.isFunction(l.on_update)) { l.on_update(g, i, l, k) } } function d(i, g, h, j, l, k) { if (i != null) { if (typeof i == "string") { j.addClass(i) } else { if (jQuery.isFunction(i)) { i(h, j, l, k) } } } if (g != null) { if (typeof g == "string") { j.removeClass(g) } } } })(jQuery);

/*
* Vekz http://www.callmekev.com/jquery.autobox.js
* autobox plugin - Used on text inputs with default values. Clears the default value on focus.
* Restores the default value on blur if empty or the same overlays text inputs for password boxes and swaps them on focus use straight CSS for styling the focus of the text input
*/
(function ($) {
    jQuery.fn.autobox = function (options) {
        var settings = $.extend({ defaultClass: 'default', filledClass: 'filled' }, options); return this.each(function () {
            var textInput = $(this); var defaultVal = textInput.val(); textInput.addClass(settings.defaultClass); if (textInput.attr('type') == 'password') { var newInput = $('<input type="text" class="' + settings.defaultClass + '"value="' + textInput.val() + '" />'); newInput.css({ 'position': 'absolute', 'z-index': 10, 'top': textInput.position().top + 'px', 'left': textInput.position().left + 'px' }); $(window).resize(function () { newInput.css({ 'top': textInput.position().top + 'px', 'left': textInput.position().left + 'px' }); }); newInput.bind('focus', function () { var $this = $(this); $this.hide(); textInput.show(); textInput.css({ 'visibility': 'visible' }); textInput.focus(); }); textInput.before(newInput); }
            textInput.bind('focus', function () { var $this = $(this); $this.removeClass(settings.defaultClass); $this.addClass(settings.filledClass); if ($this.val() == defaultVal) { $this.val(''); } }); textInput.bind('blur', function () { var $this = $(this); if ($this.val() == '') { $this.val(defaultVal); $this.addClass(settings.defaultClass); $this.removeClass(settings.filledClass); if ($this.attr('type') == 'password') { newInput.show(); } } else { $this.addClass(settings.filledClass); } });
        });
    };
})(jQuery);


/* sPop - modifed popeye http://dev.herr-schuessler.de/jquery/popeye/demo.html for Spark */
(function ($) {

    ////////////////////////////////////////////////////////////////////////////
    //
    // $.fn.spop
    // spop definition
    //
    ////////////////////////////////////////////////////////////////////////////
    $.fn.spop = function (options) { 
        
        // build main options before element iteration
        //----------------------------------------------------------------------
        var opts = $.extend({}, $.fn.spop.defaults, options);

        ////////////////////////////////////////////////////////////////////////////////
        //
        // firebug console output
        // @param text String the debug message
        // @param type String the message type [info | warn] (optional)
        //
        ////////////////////////////////////////////////////////////////////////////////
        function debug(text, type) {
            if (window.console && window.console.log && opts.debug) {
                if (type == 'info' && window.console.info) {
                    window.console.info(text);
                }
                else if (type == 'warn' && window.console.warn) {
                    window.console.warn(text);
                }
                else {
                    window.console.log(text);
                }
            }
        }

        //----------------------------------------------------------------------
        return this.each(function () {

            // first thing to do: make ppy html visible           
            $(this).addClass('spop-active');

            // cache object
            var $self = $(this),

            // images
			img = $(opts.imgObjList), //get image list from outside
            a = $(opts.imgObjList),
            tot = img.length,

            photoList = $(opts.photoList),

            // single image mode
            singleImageMode = (tot == 1) ? true : false,

            // start in compact mode
            enlarged = false,

            // counter vars
			cur = opts.curIndex || 0, //get current index from Spark namespace

            // extra classes
            eclass = 'spop-expanded',       //class to be applied to enlarged spop-box
            lclass = 'spop-loading',        //class to be applied to stage while loading image
            sclass = 'spop-single-image',   //class to be applied to spop-box if there's only one image to display

            //isInit          = false,

            // html nodes
            ppyPlaceholder = $('<div class="spop-placeholder"></div>'),
            ppyStageWrap = $('<div class="spop-stagewrap"></div>'),
            ppyCaptionWrap = $('<div class="spop-captionwrap"></div>'),
            ppyOuter = $self.find('.spop-outer'),
            ppyStage = $self.find('.spop-stage'),
            ppyNav = $self.find('.spop-nav'),
            ppyPrev = $self.find('.spop-prev'),
            ppyNext = $self.find('.spop-next'),
            //ppySwitchEnlarge    = $self.find('.spop-switch-enlarge'),
            ppySwitchCompact = $self.find('.spop-switch-compact').addClass('spop-hidden'),
            ppyCaption = $self.find('.spop-caption'),
            ppyText = $self.find('.spop-text'),
            ppyCounter = $self.find('.spop-counter'),
            ppyCurrent = $self.find('.spop-current'),
            ppyTotal = $self.find('.spop-total'),

            // css objects
            cssSelf = {
                position: 'absolute',
                width: 'auto',
                height: 'auto',
                margin: 0,
                top: 0,
                left: (opts.direction == 'right') ? 0 : 'auto',
                right: (opts.direction == 'left') ? 0 : 'auto'
            },
            cssStage = {
                height: ppyStage.height(),
                width: ppyStage.width()
            },
            cssCaption = {
                height: ppyCaption.height()
            },
            cssPlaceholder = {
                height: (opts.caption == 'hover' || false) ? ppyOuter.outerHeight() : $self.outerHeight(),
                width: (opts.caption == 'hover' || false) ? ppyOuter.outerWidth() : $self.outerWidth(),
                float: $self.css('float'),
                marginTop: $self.css('margin-top'),
                marginRight: $self.css('margin-right'),
                marginBottom: $self.css('margin-bottom'),
                marginLeft: $self.css('margin-left')
            };

            // make caption array from caption element or alt tag           
            var cap = opts.capList;

            // check for html errors
            if (!ppyStage.length || !ppyNav.length || !ppyOuter.length) {
                debug('$.fn.spop: Incorrect HTML structure', 'warn');
            }

            // check for images
            else if (tot === 0) {
                debug('$.fn.spop: No images found', 'warn');
            }

            // no errors, setup done! 
            //------------------------------------------------------------------
            else {
                singleImageMode ? debug('$.fn.spop -> SingleImageMode started') : debug('$.fn.spop -> ' + tot + ' thumbnails found.');
                init();
            }

            ////////////////////////////////////////////////////////////////////
            //
            // $.fn.spop.showThumb
            // show thumbnail
            // @param i Int the index of the thumbnail to show (optional)
            // @param transition Bool show transition between images (optional)
            //
            ////////////////////////////////////////////////////////////////////
            function showThumb(i, transition) {

                // optional parameters
                transition = transition || false;
                i = i || cur;

                // set selected thumb as background image of stage
                var cssStageImage = {
                    backgroundImage: 'url(' + img[i].src + ')'
                };
                // bogus animation css for IE 
                var cssTemp = {
                    height: '+=0'
                };

                // unbind arrowControl from document.documentElement
                $(document.documentElement).unbind('keyup');
                ppyNext.unbind('click');
				ppyPrev.unbind('click');
                ppyStage.unbind('click');

                // if we are in enlarged mode, return to thumb mode
                if (enlarged) {

                    hideCaption();

                    // fade image out and compact stage with transition
                    ppyStage.fadeTo((opts.duration / 2), 0).animate(cssStage, {
                        queue: false,
                        duration: opts.duration,
                        easing: opts.easing,
                        complete: function () {

                            enlarged = false;
                            debug('$.fn.showThumb: Entering COMPACT MODE', 'info');

                            // remove extra styling and reset z-index
                            $self.removeClass(eclass);
                            $self.css('z-index', '1');

                            // switch buttons
                            //ppySwitchEnlarge.removeClass('spop-hidden');
                            ppySwitchCompact.addClass('spop-hidden');

                            // recursive function call
                            showThumb();

                            // fade the stage back in
                            $(this).fadeTo((opts.duration / 2), 1);

                            //$('.spop-placeholder').addClass('compact');
                        }
                    })
                }
                else {

                    // if we navigate from one image to the next, fade out the stage
                    if (transition) {

                        // fade out image so that background shines through
                        // background can contain loading gfx
                        ppyStageWrap.addClass(lclass);
                        ppyStage.fadeTo((opts.duration / 2), 0);

                        // once thumb has loadded...
                        var thumbPreloader = new Image();
                        thumbPreloader.onload = function () {
                            debug('$.fn.spop.showThumb: Thumbnail ' + i + ' loaded', 'info');

                            // remove loading indicator
                            ppyStageWrap.removeClass(lclass);

                            // add all upcoming animations to the queue so that 
                            // they won't start when the preolader has loaded but when the fadeOut has finished
                            ppyStage.animate(cssTemp, 1, 'linear', function () {

                                // set the new image
                                ppyStage.css(cssStageImage);
                                // fade the stage back in
                                $(this).fadeTo((opts.duration / 2), 1);

                                // update counter and caption
                                if (opts.caption == 'hover') {
                                    showCaption(cap[i]);
                                }
                                else if (opts.caption == 'permanent') {
                                    updateCaption(cap[i]);
                                }
                                updateCounter();
                            });

                            //  fix IE animated gif bug
                            thumbPreloader.onload = function () { };
                        };
                        // preload thumb
                        thumbPreloader.src = img[i].src;
                    }

                    // or just drag the image to the stage
                    else {
                        ppyStage.css(cssStageImage);
                        updateCounter();
                        showCaption(cap[i], true);
                    }

                    // preload big image for instant availability
                    var preloader = new Image();

                    preloader.onload = function () {
                        debug('$.fn.spop.showThumb: Image ' + i + ' loaded', 'info');
                        preloader.onload = function () { };
                    };

                    preloader.src = a[i].href;
                }
            }


            ////////////////////////////////////////////////////////////////////
            //
            // $.fn.spop.showImage
            // show large image
            // @param i Int the index of the image to show (optional)
            //
            ////////////////////////////////////////////////////////////////////
            function showImage(i) {

                // optional parameter i
                i = i || cur;
                cap[i] = $j("#" + photoList[i].commentAndLikeDivID).html() + photoList[i].photoCaption;
                // fade out image so that background shines through
                // background can contain loading gfx
                ppyStageWrap.addClass(lclass);
                ppyStage.fadeTo((opts.duration / 2), 0);

                // if there are multiple spops opened at the same time,
                // make sure the current one gets a higher z-index
                var allPpy = $('.' + eclass);
                allPpy.css('z-index', opts.zindex - 1);
                $self.css('z-index', opts.zindex);

                // once image has loadded...
                var preloader = new Image();
                preloader.onload = function () {

                    // remove loading class
                    ppyStageWrap.removeClass(lclass);

                    //check preloaded image width & height 
                    var cssStageTo = {}
                    // set css
                    if (opts.minW != null || opts.minH != null) {
                        var imgSize = imgSizeControl(preloader);

                        cssStageTo = {
                            width: imgSize[0],
                            height: imgSize[1]
                        }
                    } else {
                        cssStageTo = {
                            width: preloader.width,
                            height: preloader.height
                        }
                    }
                    var cssStageIm = {
                        backgroundImage: 'url(' + a[i] + ')',
                        backgroundPosition: 'left top'
                    };

                    hideCaption();

                    // show transitional animation
                    ppyStage.animate(cssStageTo, {
                        queue: false,
                        duration: opts.duration,
                        easing: opts.easing,
                        complete: function () {

                            if (opts.navigation == 'hover') {
                                showNav();
                            }

                            enlarged = true;
                            debug('$.fn.spop.showImage: Entering ENLARGED MODE', 'info');

                            // add extra class, expanded box can be styled accordingly
                            $self.addClass(eclass);

                            // switch buttons
                            ppySwitchCompact.removeClass('spop-hidden');
                            //ppySwitchEnlarge.addClass('spop-hidden');

                            updateCounter();

                            // set new bg image and fade it in
                            $(this).css(cssStageIm).fadeTo((opts.duration / 2), 1);

                            // show caption
                            showCaption(cap[i]);

                            preloadNeighbours();
                        }
                    });
                };

                // preload image
                preloader.src = a[i];

            } // end showImage

            //Detect preloaded image size & set minimum width/height
            function imgSizeControl(obj) {
                var horSize, verSize;
                obj.width <= opts.minW ? horSize = opts.minW : horSize = obj.width;
                obj.height <= opts.minH ? verSize = opts.minH : verSize = obj.height;
                return [horSize, verSize];
            }

            ////////////////////////////////////////////////////////////////////
            //
            // $.fn.spop.updateCounter
            // update image counter
            // @param i Int the index of the image (optional)
            //
            ////////////////////////////////////////////////////////////////////
            function updateCounter(i) {

                // optional parameter
                i = i || cur;

                ppyTotal.text(tot);        // total images
                ppyCurrent.text(i + 1);    // current image number
                debug('$.fn.spop.updateCounter: Displaying image ' + (i + 1) + ' of ' + tot);
            }

            ////////////////////////////////////////////////////////////////////
            //
            // $.fn.spop.preloadNeighbours
            // preload next and previos image
            // @param i Int the index of the current image (optional)
            //
            ////////////////////////////////////////////////////////////////////
            function preloadNeighbours(i) {

                // optional parameter
                i = i || cur;

                var preloaderNext = new Image();
                var preloaderPrev = new Image();

                var neighbour = i;

                // next image
                if (neighbour < (tot - 1)) {
                    neighbour++;
                } else {
                    neighbour = 0;
                }
                //preloaderNext.src = a[i].href[neighbour];
                preloaderNext.src = a[neighbour];

                // previous image
                neighbour = i;
                if (neighbour <= 0) {
                    neighbour = tot - 1;
                } else {
                    neighbour--;
                }
                //preloaderPrev.src = a[i].href[neighbour];
                preloaderPrev.src = a[neighbour];

            }

            ////////////////////////////////////////////////////////////////////
            //
            // $.fn.spop.showNav
            //
            ////////////////////////////////////////////////////////////////////
            function showNav() {
                ppyNav.stop().fadeTo(150, opts.opacity);
            }

            ////////////////////////////////////////////////////////////////////
            //
            // $.fn.spop.hideNav
            //
            ////////////////////////////////////////////////////////////////////
            function hideNav() {
                ppyNav.stop().fadeTo(150, 0);
            }

            ////////////////////////////////////////////////////////////////////
            //
            // $.fn.spop.updateCaption
            // @param caption String the caption string
            //
            ////////////////////////////////////////////////////////////////////
            function updateCaption(caption) {

                if (opts.caption) {
                    // update text box
                    ppyText.html(caption);
                }
            }

            ////////////////////////////////////////////////////////////////////
            //
            // $.fn.spop.showCaption
            // @param caption String the caption string
            // @param force Boolean force caption display even if caption string is empty
            //
            ////////////////////////////////////////////////////////////////////
            function showCaption(caption, force) {
                // if caption string is not empty...
                if (caption && opts.caption) {
                    updateCaption(caption);

                    debug('$.fn.spop.showCaption -> ppyCaptionWrap.outerHeight(true): ' + ppyCaptionWrap.outerHeight(true));

                    // make caption box visible
                    var cssTempCaption = {
                        visibility: 'visible'
                    };
                    ppyCaption.css(cssTempCaption);

                    if (opts.caption === 'permanent' && !enlarged) {
                        // return to original caption height
                        ppyCaption.css(cssCaption);
                    }
                    else {
                        // or animate it to its childs height
                        ppyCaption.animate(
                            { 'height': ppyText.outerHeight(true) }, {
                                queue: false,
                                duration: 90,
                                easing: opts.easing
                            });
                    }
                }
                // if there's no caption to show...
                else if (!caption && !force) {
                    hideCaption();
                }
            }

            ////////////////////////////////////////////////////////////////////
            //
            // $.fn.spop.hideCaption
            //
            ////////////////////////////////////////////////////////////////////
            function hideCaption() {

                // css to hide caption but allow its inner text box to expand to content height
                var cssTempCaption = {
                    visibility: 'hidden',
                    overflow: 'hidden'
                };

                // slide up caption box and hide it when done
                ppyCaption.animate({ 'height': '0px' }, {
                    queue: false,
                    duration: 90,
                    easing: opts.easing,
                    complete: function () {
                        ppyCaption.css(cssTempCaption);
                    }
                });
            }

            ////////////////////////////////////////////////////////////////////
            //
            // $.fn.spop.previous
            // show previous image
            //
            ////////////////////////////////////////////////////////////////////
            function previous(e) {
                if (cur <= 0) {
                    cur = tot - 1;
                } else {
                    cur--;
                }
                if (enlarged) {
                    showImage(cur);
                } else {
                    showThumb(cur, true);
                }
                if (e) {
                    e.stopPropagation();
                }
                //console.log($(document.documentElement).data('events'));
                return cur;
            }

            ////////////////////////////////////////////////////////////////////
            //
            // $.fn.spop.next
            // show next image
            //
            ////////////////////////////////////////////////////////////////////
            function next(e) {
                if (cur < (tot - 1)) {
                    cur++;
                } else {
                    cur = 0;
                }
                if (enlarged) {
                    showImage(cur);
                } else {
                    showThumb(cur, true);
                }
                if (e) {
                    e.stopPropagation();
                }
                return cur;
            }

            ////////////////////////////////////////////////////////////////////
            //
            // Arrow control
            //
            ////////////////////////////////////////////////////////////////////

            function arrowControl() {
                $(document.documentElement).bind('keyup', function (event) {
                    // handle cursor keys
                    if (event.keyCode == 37) {
                        previous();

                    } else if (event.keyCode == 39) {
                        next();

                    } else if (event.keyCode == 27) {
                        // escape key
                        showThumb(cur);
                        $self.addClass('initialized');
                    }

                    // Update omniture and Refresh Ad inside of sPop
                    if(photoManager){
                        photoManager.onSlideShowClick();
                    }
                });
            }

            ////////////////////////////////////////////////////////////////////
            //
            // $.fn.spop.init
            // setup of spop DOM and events
            //
            ////////////////////////////////////////////////////////////////////

            //console.log("isInit before init() " + isInit);

            function updateInit() {
                return isInit = true;
            }

            function init() {

                //console.log('init() initiated');
                // spop dom setup
                //--------------------------------------------------------------

                // add css 
                //ppyPlaceholder.css(cssPlaceholder);
                $self.css(cssSelf);

                if ($self.is('.initialized')) {
                    //console.log('activated already');
                } else {
                    // wrap spop in placeholder 
                    //$self.wrap(ppyPlaceholder);

                    // wrap stage in container for extra styling (e.g. loading gfx)
                    ppyStageWrap = ppyStage.wrap(ppyStageWrap).parent();

                    // wrap caption contents in wrapper (can't use wrap() here...)
                    ppyCaptionWrap = ppyCaption.wrapInner(ppyCaptionWrap).children().eq(0);
                }

                // display first image
                if (opts.enlarge == true) {
                    showImage();
                } else {
                    showThumb();
                }

                // add event handlers
                //--------------------------------------------------------------
                // bind click event to 'body' for detecting click outside of this slidshow

                $('body').one('click', function () {
                    showThumb(cur);
                    $self.addClass('initialized');
                });

                $self.click(function (e) {
                    e.stopPropagation();
                });


                // hover behaviour for navigation
                if (opts.navigation == 'hover') {
                    hideNav();
                    $self.hover(
                            function () {
                                showNav();
                            },
                            function () {
                                hideNav();
                            }
                        );
                    ppyNav.hover(
                            function () {
                                showNav();
                            },
                            function () {
                                hideNav();
                            }
                        );
                }
                if (!singleImageMode) {

                    arrowControl();

                    // previous image button
                    ppyPrev.click(previous);

                    // next image button
                    ppyNext.click(next);

                    // clicking photo itself
                    ppyStage.css({ cursor: 'pointer' }).click(next);

                }
                else {
                    $self.addClass(sclass);
                    ppyPrev.remove();
                    ppyNext.remove();
                    ppyCounter.remove();
                }

                // hover behaviour for caption
                if (opts.caption == 'hover') {
                    hideCaption();
                    $self.hover(
                            function () {
                                showCaption(cap[cur]);
                            },
                            function () {
                                hideCaption(true);
                            }
                        );
                }

                // enlarge image button
                //                    ppySwitchEnlarge.click(function(){
                //                        showImage();
                //                        return false;
                //                    });

                // compact image button                          
                ppySwitchCompact.click(function () {
                    showThumb(cur);
                    $self.addClass('initialized');      //avoid html wrapping repetition
                    return false;
                });
            }

        });
    };

    ////////////////////////////////////////////////////////////////////////////
    //
    // $.fn.spop.defaults
    // set default  options
    //
    ////////////////////////////////////////////////////////////////////////////
    $.fn.spop.defaults = {
        caption: 'hover',            //visibility of caption, based on image title - can be false, 'permanent' or 'hover'
        debug: false,              //turn on console output (slows down IE8!)
        direction: 'right',            //direction that spop-box opens, can be 'left' or 'right'
        duration: 240,                //duration of transitional effect when enlarging or closing the box
        easing: 'swing',            //easing type, can be 'swing', 'linear' or any of jQuery Easing Plugin types (Plugin required)
        navigation: 'hover',            //visibility of navigation - can be 'permanent' or 'hover'
        opacity: 0.8,                //opacity of navigational overlay (only applicable if 'navigation' is set to 'hover'
        zindex: 100000,             //z-index of the expanded spop-box. enter a z-index that works well with your site and doesn't overlay your site's navigational elements like dropdowns

        imbObjList: null, 			//provided image list
        capList: null, 				//provided caption list
        curIndex: null, 				//provided current image index
        enlarge: false, 				//bypass showthumb when set it 'true'
        minW: null, 					//set minimum width 
        maxW: null, 					//set max width
        minH: null, 					//set minimum height
        maxH: null						//set max height
    };

    // end of closure, bind to jQuery Object
})(jQuery);


/*
* jQuery clueTip plugin
* Version 1.0.7  (January 28, 2010)
* @requires jQuery v1.3+
*
* Dual licensed under the MIT and GPL licenses:
* http://www.opensource.org/licenses/mit-license.php
* http://www.gnu.org/licenses/gpl.html
*
*/
; (function ($) {
    $.cluetip = { version: '1.0.6' }; var $cluetip, $cluetipInner, $cluetipOuter, $cluetipTitle, $cluetipArrows, $cluetipWait, $dropShadow, imgCount; $.fn.cluetip = function (js, options) {
        if (typeof js == 'object') { options = js; js = null; }
        if (js == 'destroy') { return this.removeData('thisInfo').unbind('.cluetip'); }
        return this.each(function (index) {
            var link = this, $this = $(this); var opts = $.extend(true, {}, $.fn.cluetip.defaults, options || {}, $.metadata ? $this.metadata() : $.meta ? $this.data() : {}); var cluetipContents = false; var cluezIndex = +opts.cluezIndex; $this.data('thisInfo', { title: link.title, zIndex: cluezIndex }); var isActive = false, closeOnDelay = 0; if (!$('#cluetip').length) {
                $(['<div id="cluetip">', '<div id="cluetip-outer">', '<h3 id="cluetip-title"></h3>', '<div id="cluetip-inner"></div>', '</div>', '<div id="cluetip-extra"></div>', '<div id="cluetip-arrows" class="cluetip-arrows"></div>', '</div>'].join(''))
[insertionType](insertionElement).hide(); $cluetip = $('#cluetip').css({ position: 'absolute' }); $cluetipOuter = $('#cluetip-outer').css({ position: 'relative', zIndex: cluezIndex }); $cluetipInner = $('#cluetip-inner'); $cluetipTitle = $('#cluetip-title'); $cluetipArrows = $('#cluetip-arrows'); $cluetipWait = $('<div id="cluetip-waitimage"></div>').css({ position: 'absolute' }).insertBefore($cluetip).hide();
            }
            var dropShadowSteps = (opts.dropShadow) ? +opts.dropShadowSteps : 0; if (!$dropShadow) {
                $dropShadow = $([]); for (var i = 0; i < dropShadowSteps; i++) { $dropShadow = $dropShadow.add($('<div></div>').css({ zIndex: cluezIndex - 1, opacity: .1, top: 1 + i, left: 1 + i })); }
                $dropShadow.css({ position: 'absolute', backgroundColor: '#000' }).prependTo($cluetip);
            }
            var tipAttribute = $this.attr(opts.attribute), ctClass = opts.cluetipClass; if (!tipAttribute && !opts.splitTitle && !js) { return true; }
            if (opts.local && opts.localPrefix) { tipAttribute = opts.localPrefix + tipAttribute; }
            if (opts.local && opts.hideLocal) { $(tipAttribute + ':first').hide(); }
            var tOffset = parseInt(opts.topOffset, 10), lOffset = parseInt(opts.leftOffset, 10); var tipHeight, wHeight, defHeight = isNaN(parseInt(opts.height, 10)) ? 'auto' : (/\D/g).test(opts.height) ? opts.height : opts.height + 'px'; var sTop, linkTop, posY, tipY, mouseY, baseline; var tipInnerWidth = parseInt(opts.width, 10) || 275, tipWidth = tipInnerWidth + (parseInt($cluetip.css('paddingLeft'), 10) || 0) + (parseInt($cluetip.css('paddingRight'), 10) || 0) + dropShadowSteps, linkWidth = this.offsetWidth, linkLeft, posX, tipX, mouseX, winWidth; var tipParts; var tipTitle = (opts.attribute != 'title') ? $this.attr(opts.titleAttribute) : ''; if (opts.splitTitle) {
                if (tipTitle == undefined) { tipTitle = ''; }
                tipParts = tipTitle.split(opts.splitTitle); tipTitle = tipParts.shift();
            }
            if (opts.escapeTitle) { tipTitle = tipTitle.replace(/&/g, '&').replace(/>/g, '>').replace(/</g, '<'); }
            var localContent; function returnFalse() { return false; }
            var activate = function (event) {
                if (!opts.onActivate($this)) { return false; }
                isActive = true; $cluetip.removeClass().css({ width: tipInnerWidth }); if (tipAttribute == $this.attr('href')) { $this.css('cursor', opts.cursor); }
                if (opts.hoverClass) { $this.addClass(opts.hoverClass); }
                linkTop = posY = $this.offset().top; linkLeft = $this.offset().left; mouseX = event.pageX; mouseY = event.pageY; if (link.tagName.toLowerCase() != 'area') { sTop = $(document).scrollTop(); winWidth = $(window).width(); }
                if (opts.positionBy == 'fixed') { posX = linkWidth + linkLeft + lOffset; $cluetip.css({ left: posX }); } else {
                    posX = (linkWidth > linkLeft && linkLeft > tipWidth) || linkLeft + linkWidth + tipWidth + lOffset > winWidth ? linkLeft - tipWidth - lOffset : linkWidth + linkLeft + lOffset; if (link.tagName.toLowerCase() == 'area' || opts.positionBy == 'mouse' || linkWidth + tipWidth > winWidth) { if (mouseX + 20 + tipWidth > winWidth) { $cluetip.addClass(' cluetip-' + ctClass); posX = (mouseX - tipWidth - lOffset) >= 0 ? mouseX - tipWidth - lOffset - parseInt($cluetip.css('marginLeft'), 10) + parseInt($cluetipInner.css('marginRight'), 10) : mouseX - (tipWidth / 2); } else { posX = mouseX + lOffset; } }
                    var pY = posX < 0 ? event.pageY + tOffset : event.pageY; $cluetip.css({ left: (posX > 0 && opts.positionBy != 'bottomTop') ? posX : (mouseX + (tipWidth / 2) > winWidth) ? winWidth / 2 - tipWidth / 2 : Math.max(mouseX - (tipWidth / 2), 0), zIndex: $this.data('thisInfo').zIndex }); $cluetipArrows.css({ zIndex: $this.data('thisInfo').zIndex + 1 });
                }
                wHeight = $(window).height(); if (js) {
                    if (typeof js == 'function') { js = js.call(link); }
                    $cluetipInner.html(js); cluetipShow(pY);
                }
                else if (tipParts) {
                    var tpl = tipParts.length; $cluetipInner.html(tpl ? tipParts[0] : ''); if (tpl > 1) { for (var i = 1; i < tpl; i++) { $cluetipInner.append('<div class="split-body">' + tipParts[i] + '</div>'); } }
                    cluetipShow(pY);
                }
                else if (!opts.local && tipAttribute.indexOf('#') !== 0) {
                    if (/\.(jpe?g|tiff?|gif|png)$/i.test(tipAttribute)) { $cluetipInner.html('<img src="' + tipAttribute + '" alt="' + tipTitle + '" />'); cluetipShow(pY); } else if (cluetipContents && opts.ajaxCache) { $cluetipInner.html(cluetipContents); cluetipShow(pY); } else {
                        var optionBeforeSend = opts.ajaxSettings.beforeSend, optionError = opts.ajaxSettings.error, optionSuccess = opts.ajaxSettings.success, optionComplete = opts.ajaxSettings.complete; var ajaxSettings = { cache: false, url: tipAttribute, beforeSend: function (xhr) {
                            if (optionBeforeSend) { optionBeforeSend.call(link, xhr, $cluetip, $cluetipInner); }
                            $cluetipOuter.children().empty(); if (opts.waitImage) { $cluetipWait.css({ top: mouseY + 20, left: mouseX + 20, zIndex: $this.data('thisInfo').zIndex - 1 }).show(); }
                        }, error: function (xhr, textStatus) { if (isActive) { if (optionError) { optionError.call(link, xhr, textStatus, $cluetip, $cluetipInner); } else { $cluetipInner.html('<i>sorry, the contents could not be loaded</i>'); } } }, success: function (data, textStatus) {
                            cluetipContents = opts.ajaxProcess.call(link, data); if (isActive) {
                                if (optionSuccess) { optionSuccess.call(link, data, textStatus, $cluetip, $cluetipInner); }
                                $cluetipInner.html(cluetipContents);
                            }
                        }, complete: function (xhr, textStatus) {
                            if (optionComplete) { optionComplete.call(link, xhr, textStatus, $cluetip, $cluetipInner); }
                            var imgs = $cluetipInner[0].getElementsByTagName('img'); imgCount = imgs.length; for (var i = 0, l = imgs.length; i < l; i++) { if (imgs[i].complete) { imgCount--; } }
                            if (imgCount && !$.browser.opera) { $(imgs).bind('load error', function () { imgCount--; if (imgCount < 1) { $cluetipWait.hide(); if (isActive) { cluetipShow(pY); } } }); } else { $cluetipWait.hide(); if (isActive) { cluetipShow(pY); } }
                        }
                        }; var ajaxMergedSettings = $.extend(true, {}, opts.ajaxSettings, ajaxSettings); $.ajax(ajaxMergedSettings);
                    }
                } else if (opts.local) { var $localContent = $(tipAttribute + (/#\S+$/.test(tipAttribute) ? '' : ':eq(' + index + ')')).clone(true).show(); $cluetipInner.html($localContent); cluetipShow(pY); }
            }; var cluetipShow = function (bpY) {
                $cluetip.addClass('cluetip-' + ctClass); if (opts.truncate) { var $truncloaded = $cluetipInner.text().slice(0, opts.truncate) + '...'; $cluetipInner.html($truncloaded); }
                function doNothing() { }; tipTitle ? $cluetipTitle.show().html(tipTitle) : (opts.showTitle) ? $cluetipTitle.show().html(' ') : $cluetipTitle.hide(); if (opts.sticky) { var $closeLink = $('<div id="cluetip-close"><a href="#">' + opts.closeText + '</a></div>'); (opts.closePosition == 'bottom') ? $closeLink.appendTo($cluetipInner) : (opts.closePosition == 'title') ? $closeLink.prependTo($cluetipTitle) : $closeLink.prependTo($cluetipInner); $closeLink.bind('click.cluetip', function () { cluetipClose(); return false; }); if (opts.mouseOutClose) { $cluetip.bind('mouseleave.cluetip', function () { cluetipClose(); }); } else { $cluetip.unbind('mouseleave.cluetip'); } }
                var direction = ''; $cluetipOuter.css({ zIndex: $this.data('thisInfo').zIndex, overflow: defHeight == 'auto' ? 'visible' : 'auto', height: defHeight }); tipHeight = defHeight == 'auto' ? Math.max($cluetip.outerHeight(), $cluetip.height()) : parseInt(defHeight, 10); tipY = posY; baseline = sTop + wHeight; if (opts.positionBy == 'fixed') { tipY = posY - opts.dropShadowSteps + tOffset; } else if ((posX < mouseX && Math.max(posX, 0) + tipWidth > mouseX) || opts.positionBy == 'bottomTop') { if (posY + tipHeight + tOffset > baseline && mouseY - sTop > tipHeight + tOffset) { tipY = mouseY - tipHeight - tOffset; direction = 'top'; } else { tipY = mouseY + tOffset; direction = 'bottom'; } } else if (posY + tipHeight + tOffset > baseline) { tipY = (tipHeight >= wHeight) ? sTop : baseline - tipHeight - tOffset; } else if ($this.css('display') == 'block' || link.tagName.toLowerCase() == 'area' || opts.positionBy == "mouse") { tipY = bpY - tOffset; } else { tipY = posY - opts.dropShadowSteps; }
                if (direction == '') { posX < linkLeft ? direction = 'left' : direction = 'right'; }
                $cluetip.css({ top: tipY + 'px' }).removeClass().addClass('clue-' + direction + '-' + ctClass).addClass(' cluetip-' + ctClass); if (opts.arrows) { var bgY = (posY - tipY - opts.dropShadowSteps); $cluetipArrows.css({ top: (/(left|right)/.test(direction) && posX >= 0 && bgY > 0) ? bgY + 'px' : /(left|right)/.test(direction) ? 0 : '' }).show(); } else { $cluetipArrows.hide(); }
                $dropShadow.hide(); $cluetip.hide()[opts.fx.open](opts.fx.openSpeed || 0); if (opts.dropShadow) { $dropShadow.css({ height: tipHeight, width: tipInnerWidth, zIndex: $this.data('thisInfo').zIndex - 1 }).show(); }
                if ($.fn.bgiframe) { $cluetip.bgiframe(); }
                if (opts.delayedClose > 0) { closeOnDelay = setTimeout(cluetipClose, opts.delayedClose); }
                opts.onShow.call(link, $cluetip, $cluetipInner);
            }; var inactivate = function (event) {
                isActive = false; $cluetipWait.hide(); if (!opts.sticky || (/click|toggle/).test(opts.activation)) { cluetipClose(); clearTimeout(closeOnDelay); }
                if (opts.hoverClass) { $this.removeClass(opts.hoverClass); }
            }; var cluetipClose = function () {
                $cluetipOuter.parent().hide().removeClass(); opts.onHide.call(link, $cluetip, $cluetipInner); $this.removeClass('cluetip-clicked'); if (tipTitle) { $this.attr(opts.titleAttribute, tipTitle); }
                $this.css('cursor', ''); if (opts.arrows) { $cluetipArrows.css({ top: '' }); }
            }; $(document).bind('hideCluetip', function (e) { cluetipClose(); }); if ((/click|toggle/).test(opts.activation)) {
                $this.bind('click.cluetip', function (event) {
                    if ($cluetip.is(':hidden') || !$this.is('.cluetip-clicked')) { activate(event); $('.cluetip-clicked').removeClass('cluetip-clicked'); $this.addClass('cluetip-clicked'); } else { inactivate(event); }
                    this.blur(); return false;
                });
            } else if (opts.activation == 'focus') { $this.bind('focus.cluetip', function (event) { activate(event); }); $this.bind('blur.cluetip', function (event) { inactivate(event); }); } else {
                $this[opts.clickThrough ? 'unbind' : 'bind']('click', returnFalse); var mouseTracks = function (evt) { if (opts.tracking == true) { var trackX = posX - evt.pageX; var trackY = tipY ? tipY - evt.pageY : posY - evt.pageY; $this.bind('mousemove.cluetip', function (evt) { $cluetip.css({ left: evt.pageX + trackX, top: evt.pageY + trackY }); }); } }; if ($.fn.hoverIntent && opts.hoverIntent) { $this.hoverIntent({ sensitivity: opts.hoverIntent.sensitivity, interval: opts.hoverIntent.interval, over: function (event) { activate(event); mouseTracks(event); }, timeout: opts.hoverIntent.timeout, out: function (event) { inactivate(event); $this.unbind('mousemove.cluetip'); } }); } else { $this.bind('mouseenter.cluetip', function (event) { activate(event); mouseTracks(event); }).bind('mouseleave.cluetip', function (event) { inactivate(event); $this.unbind('mousemove.cluetip'); }); }
                $this.bind('mouseover.cluetip', function (event) { $this.attr('title', ''); }).bind('mouseleave.cluetip', function (event) { $this.attr('title', $this.data('thisInfo').title); });
            }
        });
    }; $.fn.cluetip.defaults = { width: 275, height: 'auto', cluezIndex: 97, positionBy: 'auto', topOffset: 15, leftOffset: 15, local: false, localPrefix: null, hideLocal: true, attribute: 'rel', titleAttribute: 'title', splitTitle: '', escapeTitle: false, showTitle: true, cluetipClass: 'default', hoverClass: '', waitImage: true, cursor: 'help', arrows: false, dropShadow: true, dropShadowSteps: 6, sticky: false, mouseOutClose: false, activation: 'hover', clickThrough: false, tracking: false, delayedClose: 0, closePosition: 'top', closeText: 'Close', truncate: 0, fx: { open: 'show', openSpeed: '' }, hoverIntent: { sensitivity: 3, interval: 50, timeout: 0 }, onActivate: function (e) { return true; }, onShow: function (ct, ci) { }, onHide: function (ct, ci) { }, ajaxCache: true, ajaxProcess: function (data) { data = data.replace(/<(script|style|title)[^<]+<\/(script|style|title)>/gm, '').replace(/<(link|meta)[^>]+>/g, ''); return data; }, ajaxSettings: { dataType: 'html' }, debug: false }; var insertionType = 'appendTo', insertionElement = 'body'; $.cluetip.setup = function (options) {
        if (options && options.insertionType && (options.insertionType).match(/appendTo|prependTo|insertBefore|insertAfter/)) { insertionType = options.insertionType; }
        if (options && options.insertionElement) { insertionElement = options.insertionElement; }
    };
})(jQuery);


/**
* Interface Elements for jQuery
* Fisheye menu
* 
* http://interface.eyecon.ro
* 
* Copyright (c) 2006 Stefan Petre
* Dual licensed under the MIT (MIT-LICENSE.txt) 
* and GPL (GPL-LICENSE.txt) licenses.
*
*/

/**
* Build a Fisheye menu from a list of links
*
* @name Fisheye
* @description Build a Fisheye menu from a list of links
* @param Hash hash A hash of parameters
* @option String items items selection
* @option String container container element
* @option Integer itemWidth the minimum width for each item
* @option Integer maxWidth the maximum width for each item
* @option String itemsText selection of element that contains the text for each item
* @option Integer proximity the distance from element that make item to interact
* @option String valign vertical alignment
* @option String halign horizontal alignment
*
* @type jQuery
* @cat Plugins/Interface
* @author Stefan Petre
*/
jQuery.iFisheye = {

    build: function (options) {

        return this.each(
			function () {
			    var el = this;
			    el.fisheyeCfg = {
			        items: jQuery(options.items, this),
			        container: jQuery(options.container, this),
			        pos: jQuery.iUtil.getPosition(this),
			        itemWidth: options.itemWidth,
			        itemsText: options.itemsText,
			        proximity: options.proximity,
			        valign: options.valign,
			        halign: options.halign,
			        maxWidth: options.maxWidth,
			        margin: options.margin
			    };
			    jQuery.iFisheye.positionContainer(el, 0);
			    jQuery(window).bind('resize', function () {
			        el.fisheyeCfg.pos = jQuery.iUtil.getPosition(el);
			        jQuery.iFisheye.positionContainer(el, 0);
			        jQuery.iFisheye.positionItems(el);
			    }
				);
			    jQuery.iFisheye.positionItems(el);
			    /*
			    el.fisheyeCfg.items.bind('mouseover',function(){
			    jQuery(el.fisheyeCfg.itemsText, this).get(0).style.display = 'block';
			    })
			    .bind('mouseout',function(){
			    jQuery(el.fisheyeCfg.itemsText, this).get(0).style.display = 'none';
			    });
			    */
			    /*
			    el.fisheyeCfg.items.bind('mouseover', function() {
			    jQuery(this).get(0).style.zIndex = '1';
			    })
			    .bind('mouseout', function() {
			    jQuery(this).get(0).style.zIndex = '0';
			    });
			    */

			    jQuery(document).bind('mousemove', function (e) {
			        var pointer = jQuery.iUtil.getPointer(e);
			        var toAdd = 0;
			       
			        if (el.fisheyeCfg.halign && el.fisheyeCfg.halign == 'center')
			            var posx = pointer.x - el.fisheyeCfg.pos.x - (el.offsetWidth - el.fisheyeCfg.itemWidth * el.fisheyeCfg.items.size()) / 2 - el.fisheyeCfg.itemWidth / 2;
			        else if (el.fisheyeCfg.halign && el.fisheyeCfg.halign == 'right')
			            var posx = pointer.x - el.fisheyeCfg.pos.x - el.offsetWidth + el.fisheyeCfg.itemWidth * el.fisheyeCfg.items.size();
			        else if (el.fisheyeCfg.halign && el.fisheyeCfg.halign == 'rtl') {
			            var posx = (-(pointer.x - (el.fisheyeCfg.pos.x + jQuery(el.fisheyeCfg.container['selector']).width()))) - el.fisheyeCfg.itemWidth;
			        } else {
			            var posx = pointer.x - el.fisheyeCfg.pos.x;
			        }
			        var posy = Math.pow(pointer.y - el.fisheyeCfg.pos.y - el.offsetHeight / 2, 2);

			        el.fisheyeCfg.items.each(
							function (nr) {
							    distance = Math.sqrt(Math.pow(posx - nr * el.fisheyeCfg.itemWidth, 2) + posy);
							    distance -= el.fisheyeCfg.itemWidth / 2;

							    distance = distance < 0 ? 0 : distance;
							    distance = distance > el.fisheyeCfg.proximity ? el.fisheyeCfg.proximity : distance;
							    distance = el.fisheyeCfg.proximity - distance;

							    extraWidth = el.fisheyeCfg.maxWidth * distance / el.fisheyeCfg.proximity;

							    this.style.width = el.fisheyeCfg.itemWidth + extraWidth + 'px';
							    //this.style.left = el.fisheyeCfg.itemWidth * nr + toAdd + 'px';'

							    if (el.fisheyeCfg.halign && el.fisheyeCfg.halign == 'rtl') {
							        this.style.right = el.fisheyeCfg.itemWidth * nr + toAdd + el.fisheyeCfg.margin + 'px';
							    } else {
							        this.style.left = el.fisheyeCfg.itemWidth * nr + toAdd + el.fisheyeCfg.margin + 'px';
							    }
							    
							    toAdd += extraWidth;
							}
						);
			        jQuery.iFisheye.positionContainer(el, toAdd);
			    }
				);
			}
		)
    },

    positionContainer: function (el, toAdd) {
        if (el.fisheyeCfg.halign)
            if (el.fisheyeCfg.halign == 'center')
                el.fisheyeCfg.container.get(0).style.left = (el.offsetWidth - el.fisheyeCfg.itemWidth * el.fisheyeCfg.items.size()) / 2 - toAdd / 2 + 'px';
            else if (el.fisheyeCfg.halign == 'left')
                el.fisheyeCfg.container.get(0).style.left = -toAdd / el.fisheyeCfg.items.size() + 'px';
            else if (el.fisheyeCfg.halign == 'right')
                el.fisheyeCfg.container.get(0).style.left = (el.offsetWidth - el.fisheyeCfg.itemWidth * el.fisheyeCfg.items.size()) - toAdd / 2 + 'px';
        el.fisheyeCfg.container.get(0).style.width = el.fisheyeCfg.itemWidth * el.fisheyeCfg.items.size() + toAdd + 'px';
    },

    positionItems: function (el) {
        el.fisheyeCfg.items.each(
			function (nr) {
			    this.style.width = el.fisheyeCfg.itemWidth + 'px';

			    if (el.fisheyeCfg.halign && el.fisheyeCfg.halign == 'rtl') {
			        this.style.right = el.fisheyeCfg.itemWidth * nr + 'px';
			    } else {
			        this.style.left = el.fisheyeCfg.itemWidth * nr + 'px';
			    }
			}
		);
    }
};

jQuery.fn.Fisheye = jQuery.iFisheye.build;

/**
* Interface Elements for jQuery utility function
*
* http://interface.eyecon.ro
*
* Copyright (c) 2006 Stefan Petre
* Dual licensed under the MIT (MIT-LICENSE.txt)
* and GPL (GPL-LICENSE.txt) licenses.
*
*/

jQuery.iUtil = {
    getPosition: function (e) {
        var x = 0;
        var y = 0;
        var es = e.style;
        var restoreStyles = false;
        if (jQuery(e).css('display') == 'none') {
            var oldVisibility = es.visibility;
            var oldPosition = es.position;
            restoreStyles = true;
            es.visibility = 'hidden';
            es.display = 'block';
            es.position = 'absolute';
        }
        var el = e;
        while (el) {
            x += el.offsetLeft + (el.currentStyle && !jQuery.browser.opera ? parseInt(el.currentStyle.borderLeftWidth) || 0 : 0);
            y += el.offsetTop + (el.currentStyle && !jQuery.browser.opera ? parseInt(el.currentStyle.borderTopWidth) || 0 : 0);
            el = el.offsetParent;
        }
        el = e;
        while (el && el.tagName && el.tagName.toLowerCase() != 'body') {
            x -= el.scrollLeft || 0;
            y -= el.scrollTop || 0;
            el = el.parentNode;
        }
        if (restoreStyles == true) {
            es.display = 'none';
            es.position = oldPosition;
            es.visibility = oldVisibility;
        }
        return { x: x, y: y };
    },
    getPositionLite: function (el) {
        var x = 0, y = 0;
        while (el) {
            x += el.offsetLeft || 0;
            y += el.offsetTop || 0;
            el = el.offsetParent;
        }
        return { x: x, y: y };
    },
    getSize: function (e) {
        var w = jQuery.css(e, 'width');
        var h = jQuery.css(e, 'height');
        var wb = 0;
        var hb = 0;
        var es = e.style;
        if (jQuery(e).css('display') != 'none') {
            wb = e.offsetWidth;
            hb = e.offsetHeight;
        } else {
            var oldVisibility = es.visibility;
            var oldPosition = es.position;
            es.visibility = 'hidden';
            es.display = 'block';
            es.position = 'absolute';
            wb = e.offsetWidth;
            hb = e.offsetHeight;
            es.display = 'none';
            es.position = oldPosition;
            es.visibility = oldVisibility;
        }
        return { w: w, h: h, wb: wb, hb: hb };
    },
    getSizeLite: function (el) {
        return {
            wb: el.offsetWidth || 0,
            hb: el.offsetHeight || 0
        };
    },
    getClient: function (e) {
        var h, w, de;
        if (e) {
            w = e.clientWidth;
            h = e.clientHeight;
        } else {
            de = document.documentElement;
            w = window.innerWidth || self.innerWidth || (de && de.clientWidth) || document.body.clientWidth;
            h = window.innerHeight || self.innerHeight || (de && de.clientHeight) || document.body.clientHeight;
        }
        return { w: w, h: h };
    },
    getScroll: function (e) {
        var t = 0, l = 0, w = 0, h = 0, iw = 0, ih = 0;
        if (e && e.nodeName.toLowerCase() != 'body') {
            t = e.scrollTop;
            l = e.scrollLeft;
            w = e.scrollWidth;
            h = e.scrollHeight;
            iw = 0;
            ih = 0;
        } else {
            if (document.documentElement) {
                t = document.documentElement.scrollTop;
                l = document.documentElement.scrollLeft;
                w = document.documentElement.scrollWidth;
                h = document.documentElement.scrollHeight;
            } else if (document.body) {
                t = document.body.scrollTop;
                l = document.body.scrollLeft;
                w = document.body.scrollWidth;
                h = document.body.scrollHeight;
            }
            iw = self.innerWidth || document.documentElement.clientWidth || document.body.clientWidth || 0;
            ih = self.innerHeight || document.documentElement.clientHeight || document.body.clientHeight || 0;
        }
        return { t: t, l: l, w: w, h: h, iw: iw, ih: ih };
    },
    getMargins: function (e, toInteger) {
        var el = jQuery(e);
        var t = el.css('marginTop') || '';
        var r = el.css('marginRight') || '';
        var b = el.css('marginBottom') || '';
        var l = el.css('marginLeft') || '';
        if (toInteger)
            return {
                t: parseInt(t) || 0,
                r: parseInt(r) || 0,
                b: parseInt(b) || 0,
                l: parseInt(l)
            };
        else
            return { t: t, r: r, b: b, l: l };
    },
    getPadding: function (e, toInteger) {
        var el = jQuery(e);
        var t = el.css('paddingTop') || '';
        var r = el.css('paddingRight') || '';
        var b = el.css('paddingBottom') || '';
        var l = el.css('paddingLeft') || '';
        if (toInteger)
            return {
                t: parseInt(t) || 0,
                r: parseInt(r) || 0,
                b: parseInt(b) || 0,
                l: parseInt(l)
            };
        else
            return { t: t, r: r, b: b, l: l };
    },
    getBorder: function (e, toInteger) {
        var el = jQuery(e);
        var t = el.css('borderTopWidth') || '';
        var r = el.css('borderRightWidth') || '';
        var b = el.css('borderBottomWidth') || '';
        var l = el.css('borderLeftWidth') || '';
        if (toInteger)
            return {
                t: parseInt(t) || 0,
                r: parseInt(r) || 0,
                b: parseInt(b) || 0,
                l: parseInt(l) || 0
            };
        else
            return { t: t, r: r, b: b, l: l };
    },
    getPointer: function (event) {
        var x = event.pageX || (event.clientX + (document.documentElement.scrollLeft || document.body.scrollLeft)) || 0;
        var y = event.pageY || (event.clientY + (document.documentElement.scrollTop || document.body.scrollTop)) || 0;
        return { x: x, y: y };
    },
    traverseDOM: function (nodeEl, func) {
        func(nodeEl);
        nodeEl = nodeEl.firstChild;
        while (nodeEl) {
            jQuery.iUtil.traverseDOM(nodeEl, func);
            nodeEl = nodeEl.nextSibling;
        }
    },
    purgeEvents: function (nodeEl) {
        jQuery.iUtil.traverseDOM(
			nodeEl,
			function (el) {
			    for (var attr in el) {
			        if (typeof el[attr] === 'function') {
			            el[attr] = null;
			        }
			    }
			}
		);
    },
    centerEl: function (el, axis) {
        var clientScroll = jQuery.iUtil.getScroll();
        var windowSize = jQuery.iUtil.getSize(el);
        if (!axis || axis == 'vertically')
            jQuery(el).css(
				{
				    top: clientScroll.t + ((Math.max(clientScroll.h, clientScroll.ih) - clientScroll.t - windowSize.hb) / 2) + 'px'
				}
			);
        if (!axis || axis == 'horizontally')
            jQuery(el).css(
				{
				    left: clientScroll.l + ((Math.max(clientScroll.w, clientScroll.iw) - clientScroll.l - windowSize.wb) / 2) + 'px'
				}
			);
    },
    fixPNG: function (el, emptyGIF) {
        var images = jQuery('img[@src*="png"]', el || document), png;
        images.each(function () {
            png = this.src;
            this.src = emptyGIF;
            this.style.filter = "progid:DXImageTransform.Microsoft.AlphaImageLoader(src='" + png + "')";
        });
    }
};

// Helper function to support older browsers!
[ ].indexOf || (Array.prototype.indexOf = function (v, n) {
    n = (n == null) ? 0 : n;
    var m = this.length;
    for (var i = n; i < m; i++)
        if (this[i] == v)
            return i;
    return -1;
});

/**
* TextAreaExpander plugin for jQuery
* v1.0
* Expands or contracts a textarea height depending on the
* quatity of content entered by the user in the box.
*
* By Craig Buckler, Optimalworks.net
*
* As featured on SitePoint.com:
* http://www.sitepoint.com/blogs/2009/07/29/build-auto-expanding-textarea-1/
*
* Please use as you wish at your own risk.
*/
(function ($) { $.fn.TextAreaExpander = function (minHeight, maxHeight) { var hCheck = !($.browser.msie || $.browser.opera); function ResizeTextarea(e) { e = e.target || e; var vlen = e.value.length, ewidth = e.offsetWidth; if (vlen != e.valLength || ewidth != e.boxWidth) { if (hCheck && (vlen < e.valLength || ewidth != e.boxWidth)) e.style.height = "0px"; var h = Math.max(e.expandMin, Math.min(e.scrollHeight, e.expandMax)); e.style.overflow = (e.scrollHeight > h ? "auto" : "hidden"); e.style.height = h + "px"; e.valLength = vlen; e.boxWidth = ewidth } return true }; this.each(function () { if (this.nodeName.toLowerCase() != "textarea") return; var p = this.className.match(/expand(\d+)\-*(\d+)*/i); this.expandMin = minHeight || (p ? parseInt('0' + p[1], 10) : 0); this.expandMax = maxHeight || (p ? parseInt('0' + p[2], 10) : 99999); ResizeTextarea(this); if (!this.Initialized) { this.Initialized = true; $(this).css("padding-top", 0).css("padding-bottom", 0); $(this).bind("keyup", ResizeTextarea).bind("focus", ResizeTextarea) } }); return this } })(jQuery);

/**
* SearchHighlight plugin for jQuery http://www.jquery.info/spip.php?article50
* 
* Thanks to Scott Yang <http://scott.yang.id.au/> for the original idea and some code
*    
* @author Renato Formato <renatoformato@virgilio.it> 
*   
*/
(function (a) { jQuery.fn.SearchHighlight = function (c) { var d = c.debug_referrer || document.referrer; if (!d && c.keys == undefined) return this; b.options = a.extend({ exact: "exact", style_name: "hilite", style_name_suffix: true }, c); if (c.engines) b.engines.unshift(c.engines); var e = c.keys != undefined ? c.keys.toLowerCase().split(/[\s,\+\.]+/) : b.decodeURL(d, b.engines); if (e && e.join("")) { b.buildReplaceTools(e); return this.each(function () { var c = this; if (c == document) c = a("body")[0]; b.hiliteElement(c, e) }) } else return this }; var b = { options: {}, regex: [], engines: [[/^http:\/\/(www\.)?google\./i, /q=([^&]+)/i], [/^http:\/\/(www\.)?search\.yahoo\./i, /p=([^&]+)/i], [/^http:\/\/(www\.)?search\.msn\./i, /q=([^&]+)/i], [/^http:\/\/(www\.)?search\.live\./i, /query=([^&]+)/i], [/^http:\/\/(www\.)?search\.aol\./i, /userQuery=([^&]+)/i], [/^http:\/\/(www\.)?ask\.com/i, /q=([^&]+)/i], [/^http:\/\/(www\.)?altavista\./i, /q=([^&]+)/i], [/^http:\/\/(www\.)?feedster\./i, /q=([^&]+)/i], [/^http:\/\/(www\.)?search\.lycos\./i, /q=([^&]+)/i], [/^http:\/\/(www\.)?alltheweb\./i, /q=([^&]+)/i], [/^http:\/\/(www\.)?technorati\.com/i, /([^\?\/]+)(?:\?.*)$/i]], subs: {}, decodeURL: function (b, c) { b = decodeURIComponent(b); var d = null; a.each(c, function (a, c) { if (c[0].test(b)) { var e = b.match(c[1]); if (e) { d = e[1].toLowerCase(); return false } } }); if (d) { d = d.replace(/(\'|")/, "$1"); d = d.split(/[\s,\+\.]+/) } return d }, regexAccent: [[/[\xC0-\xC5\u0100-\u0105]/ig, "a"], [/[\xC7\u0106-\u010D]/ig, "c"], [/[\xC8-\xCB]/ig, "e"], [/[\xCC-\xCF]/ig, "i"], [/\xD1/ig, "n"], [/[\xD2-\xD6\xD8]/ig, "o"], [/[\u015A-\u0161]/ig, "s"], [/[\u0162-\u0167]/ig, "t"], [/[\xD9-\xDC]/ig, "u"], [/\xFF/ig, "y"], [/[\x91\x92\u2018\u2019]/ig, "'"]], matchAccent: /[\x91\x92\xC0-\xC5\xC7-\xCF\xD1-\xD6\xD8-\xDC\xFF\u0100-\u010D\u015A-\u0167\u2018\u2019]/ig, replaceAccent: function (a) { b.matchAccent.lastIndex = 0; if (b.matchAccent.test(a)) { for (var c = 0, d = b.regexAccent.length; c < d; c++) a = a.replace(b.regexAccent[c][0], b.regexAccent[c][1]) } return a }, escapeRegEx: /((?:\\{2})*)([[\]{}*?|])/g, buildReplaceTools: function (c) { var d = [], e; a.each(c, function (a, c) { if (c = b.replaceAccent(c).replace(b.escapeRegEx, "$1\\$2")) d.push(c) }); e = d.join("|"); switch (b.options.exact) { case "exact": e = "\\b(?:" + e + ")\\b"; break; case "whole": e = "\\b\\w*(" + e + ")\\w*\\b"; break } b.regex = new RegExp(e, "gi"); a.each(d, function (a, c) { b.subs[c] = b.options.style_name + (b.options.style_name_suffix ? a + 1 : "") }) }, nosearch: /s(?:cript|tyle)|textarea/i, hiliteElement: function (c, d) { var e = b.options, f, g; f = e.highlight ? a(e.highlight) : a("body"); if (!f.length) f = a("body"); g = e.nohighlight ? a(e.nohighlight) : a([]); f.each(function () { b.hiliteTree(this, d, g) }) }, hiliteTree: function (c, d, e) { if (e.index(c) != -1) return; var f = b.options.exact == "whole" ? 1 : 0; for (var g = 0, h = c.childNodes.length; g < h; g++) { var i = c.childNodes[g]; if (i.nodeType != 8) { if (i.nodeType == 3) { var j = i.data, k = b.replaceAccent(j); var l = "", m, n = 0; b.regex.lastIndex = 0; while (m = b.regex.exec(k)) { l += j.substr(n, m.index - n) + '<span class="' + b.subs[m[f].toLowerCase()] + '">' + j.substr(m.index, m[0].length) + "</span>"; n = m.index + m[0].length } if (l) { l += j.substring(n); var o = a.merge([], a("<span>" + l + "</span>")[0].childNodes); h += o.length - 1; g += o.length - 1; a(i).before(o).remove() } } else { if (i.nodeType == 1 && i.nodeName.search(b.nosearch) == -1) b.hiliteTree(i, d, e) } } } } } })(jQuery);

/**
 * Isotope v1.5.21
 * An exquisite jQuery plugin for magical layouts
 * http://isotope.metafizzy.co
 *
 * Commercial use requires one-time license fee
 * http://metafizzy.co/#licenses
 *
 * Copyright 2012 David DeSandro / Metafizzy
 */
(function(a,b,c){"use strict";var d=a.document,e=a.Modernizr,f=function(a){return a.charAt(0).toUpperCase()+a.slice(1)},g="Moz Webkit O Ms".split(" "),h=function(a){var b=d.documentElement.style,c;if(typeof b[a]=="string")return a;a=f(a);for(var e=0,h=g.length;e<h;e++){c=g[e]+a;if(typeof b[c]=="string")return c}},i=h("transform"),j=h("transitionProperty"),k={csstransforms:function(){return!!i},csstransforms3d:function(){var a=!!h("perspective");if(a){var c=" -o- -moz- -ms- -webkit- -khtml- ".split(" "),d="@media ("+c.join("transform-3d),(")+"modernizr)",e=b("<style>"+d+"{#modernizr{height:3px}}"+"</style>").appendTo("head"),f=b('<div id="modernizr" />').appendTo("html");a=f.height()===3,f.remove(),e.remove()}return a},csstransitions:function(){return!!j}},l;if(e)for(l in k)e.hasOwnProperty(l)||e.addTest(l,k[l]);else{e=a.Modernizr={_version:"1.6ish: miniModernizr for Isotope"};var m=" ",n;for(l in k)n=k[l](),e[l]=n,m+=" "+(n?"":"no-")+l;b("html").addClass(m)}if(e.csstransforms){var o=e.csstransforms3d?{translate:function(a){return"translate3d("+a[0]+"px, "+a[1]+"px, 0) "},scale:function(a){return"scale3d("+a+", "+a+", 1) "}}:{translate:function(a){return"translate("+a[0]+"px, "+a[1]+"px) "},scale:function(a){return"scale("+a+") "}},p=function(a,c,d){var e=b.data(a,"isoTransform")||{},f={},g,h={},j;f[c]=d,b.extend(e,f);for(g in e)j=e[g],h[g]=o[g](j);var k=h.translate||"",l=h.scale||"",m=k+l;b.data(a,"isoTransform",e),a.style[i]=m};b.cssNumber.scale=!0,b.cssHooks.scale={set:function(a,b){p(a,"scale",b)},get:function(a,c){var d=b.data(a,"isoTransform");return d&&d.scale?d.scale:1}},b.fx.step.scale=function(a){b.cssHooks.scale.set(a.elem,a.now+a.unit)},b.cssNumber.translate=!0,b.cssHooks.translate={set:function(a,b){p(a,"translate",b)},get:function(a,c){var d=b.data(a,"isoTransform");return d&&d.translate?d.translate:[0,0]}}}var q,r;e.csstransitions&&(q={WebkitTransitionProperty:"webkitTransitionEnd",MozTransitionProperty:"transitionend",OTransitionProperty:"oTransitionEnd otransitionend",transitionProperty:"transitionend"}[j],r=h("transitionDuration"));var s=b.event,t;s.special.smartresize={setup:function(){b(this).bind("resize",s.special.smartresize.handler)},teardown:function(){b(this).unbind("resize",s.special.smartresize.handler)},handler:function(a,b){var c=this,d=arguments;a.type="smartresize",t&&clearTimeout(t),t=setTimeout(function(){jQuery.event.handle.apply(c,d)},b==="execAsap"?0:100)}},b.fn.smartresize=function(a){return a?this.bind("smartresize",a):this.trigger("smartresize",["execAsap"])},b.Isotope=function(a,c,d){this.element=b(c),this._create(a),this._init(d)};var u=["width","height"],v=b(a);b.Isotope.settings={resizable:!0,layoutMode:"masonry",containerClass:"isotope",itemClass:"isotope-item",hiddenClass:"isotope-hidden",hiddenStyle:{opacity:0,scale:.001},visibleStyle:{opacity:1,scale:1},containerStyle:{position:"relative",overflow:"hidden"},animationEngine:"best-available",animationOptions:{queue:!1,duration:800},sortBy:"original-order",sortAscending:!0,resizesContainer:!0,transformsEnabled:!0,itemPositionDataEnabled:!1},b.Isotope.prototype={_create:function(a){this.options=b.extend({},b.Isotope.settings,a),this.styleQueue=[],this.elemCount=0;var c=this.element[0].style;this.originalStyle={};var d=u.slice(0);for(var e in this.options.containerStyle)d.push(e);for(var f=0,g=d.length;f<g;f++)e=d[f],this.originalStyle[e]=c[e]||"";this.element.css(this.options.containerStyle),this._updateAnimationEngine(),this._updateUsingTransforms();var h={"original-order":function(a,b){return b.elemCount++,b.elemCount},random:function(){return Math.random()}};this.options.getSortData=b.extend(this.options.getSortData,h),this.reloadItems(),this.offset={left:parseInt(this.element.css("padding-left")||0,10),top:parseInt(this.element.css("padding-top")||0,10)};var i=this;setTimeout(function(){i.element.addClass(i.options.containerClass)},0),this.options.resizable&&v.bind("smartresize.isotope",function(){i.resize()}),this.element.delegate("."+this.options.hiddenClass,"click",function(){return!1})},_getAtoms:function(a){var b=this.options.itemSelector,c=b?a.filter(b).add(a.find(b)):a,d={position:"absolute"};return this.usingTransforms&&(d.left=0,d.top=0),c.css(d).addClass(this.options.itemClass),this.updateSortData(c,!0),c},_init:function(a){this.$filteredAtoms=this._filter(this.$allAtoms),this._sort(),this.reLayout(a)},option:function(a){if(b.isPlainObject(a)){this.options=b.extend(!0,this.options,a);var c;for(var d in a)c="_update"+f(d),this[c]&&this[c]()}},_updateAnimationEngine:function(){var a=this.options.animationEngine.toLowerCase().replace(/[ _\-]/g,""),b;switch(a){case"css":case"none":b=!1;break;case"jquery":b=!0;break;default:b=!e.csstransitions}this.isUsingJQueryAnimation=b,this._updateUsingTransforms()},_updateTransformsEnabled:function(){this._updateUsingTransforms()},_updateUsingTransforms:function(){var a=this.usingTransforms=this.options.transformsEnabled&&e.csstransforms&&e.csstransitions&&!this.isUsingJQueryAnimation;a||(delete this.options.hiddenStyle.scale,delete this.options.visibleStyle.scale),this.getPositionStyles=a?this._translate:this._positionAbs},_filter:function(a){var b=this.options.filter===""?"*":this.options.filter;if(!b)return a;var c=this.options.hiddenClass,d="."+c,e=a.filter(d),f=e;if(b!=="*"){f=e.filter(b);var g=a.not(d).not(b).addClass(c);this.styleQueue.push({$el:g,style:this.options.hiddenStyle})}return this.styleQueue.push({$el:f,style:this.options.visibleStyle}),f.removeClass(c),a.filter(b)},updateSortData:function(a,c){var d=this,e=this.options.getSortData,f,g;a.each(function(){f=b(this),g={};for(var a in e)!c&&a==="original-order"?g[a]=b.data(this,"isotope-sort-data")[a]:g[a]=e[a](f,d);b.data(this,"isotope-sort-data",g)})},_sort:function(){var a=this.options.sortBy,b=this._getSorter,c=this.options.sortAscending?1:-1,d=function(d,e){var f=b(d,a),g=b(e,a);return f===g&&a!=="original-order"&&(f=b(d,"original-order"),g=b(e,"original-order")),(f>g?1:f<g?-1:0)*c};this.$filteredAtoms.sort(d)},_getSorter:function(a,c){return b.data(a,"isotope-sort-data")[c]},_translate:function(a,b){return{translate:[a,b]}},_positionAbs:function(a,b){return{left:a,top:b}},_pushPosition:function(a,b,c){b=Math.round(b+this.offset.left),c=Math.round(c+this.offset.top);var d=this.getPositionStyles(b,c);this.styleQueue.push({$el:a,style:d}),this.options.itemPositionDataEnabled&&a.data("isotope-item-position",{x:b,y:c})},layout:function(a,b){var c=this.options.layoutMode;this["_"+c+"Layout"](a);if(this.options.resizesContainer){var d=this["_"+c+"GetContainerSize"]();this.styleQueue.push({$el:this.element,style:d})}this._processStyleQueue(a,b),this.isLaidOut=!0},_processStyleQueue:function(a,c){var d=this.isLaidOut?this.isUsingJQueryAnimation?"animate":"css":"css",f=this.options.animationOptions,g=this.options.onLayout,h,i,j,k;i=function(a,b){b.$el[d](b.style,f)};if(this._isInserting&&this.isUsingJQueryAnimation)i=function(a,b){h=b.$el.hasClass("no-transition")?"css":d,b.$el[h](b.style,f)};else if(c||g||f.complete){var l=!1,m=[c,g,f.complete],n=this;j=!0,k=function(){if(l)return;var b;for(var c=0,d=m.length;c<d;c++)b=m[c],typeof b=="function"&&b.call(n.element,a,n);l=!0};if(this.isUsingJQueryAnimation&&d==="animate")f.complete=k,j=!1;else if(e.csstransitions){var o=0,p=this.styleQueue[0],s=p&&p.$el,t;while(!s||!s.length){t=this.styleQueue[o++];if(!t)return;s=t.$el}var u=parseFloat(getComputedStyle(s[0])[r]);u>0&&(i=function(a,b){b.$el[d](b.style,f).one(q,k)},j=!1)}}b.each(this.styleQueue,i),j&&k(),this.styleQueue=[]},resize:function(){this["_"+this.options.layoutMode+"ResizeChanged"]()&&this.reLayout()},reLayout:function(a){this["_"+this.options.layoutMode+"Reset"](),this.layout(this.$filteredAtoms,a)},addItems:function(a,b){var c=this._getAtoms(a);this.$allAtoms=this.$allAtoms.add(c),b&&b(c)},insert:function(a,b){this.element.append(a);var c=this;this.addItems(a,function(a){var d=c._filter(a);c._addHideAppended(d),c._sort(),c.reLayout(),c._revealAppended(d,b)})},appended:function(a,b){var c=this;this.addItems(a,function(a){c._addHideAppended(a),c.layout(a),c._revealAppended(a,b)})},_addHideAppended:function(a){this.$filteredAtoms=this.$filteredAtoms.add(a),a.addClass("no-transition"),this._isInserting=!0,this.styleQueue.push({$el:a,style:this.options.hiddenStyle})},_revealAppended:function(a,b){var c=this;setTimeout(function(){a.removeClass("no-transition"),c.styleQueue.push({$el:a,style:c.options.visibleStyle}),c._isInserting=!1,c._processStyleQueue(a,b)},10)},reloadItems:function(){this.$allAtoms=this._getAtoms(this.element.children())},remove:function(a,b){this.$allAtoms=this.$allAtoms.not(a),this.$filteredAtoms=this.$filteredAtoms.not(a);var c=this,d=function(){a.remove(),b&&b.call(c.element)};a.filter(":not(."+this.options.hiddenClass+")").length?(this.styleQueue.push({$el:a,style:this.options.hiddenStyle}),this._sort(),this.reLayout(d)):d()},shuffle:function(a){this.updateSortData(this.$allAtoms),this.options.sortBy="random",this._sort(),this.reLayout(a)},destroy:function(){var a=this.usingTransforms,b=this.options;this.$allAtoms.removeClass(b.hiddenClass+" "+b.itemClass).each(function(){var b=this.style;b.position="",b.top="",b.left="",b.opacity="",a&&(b[i]="")});var c=this.element[0].style;for(var d in this.originalStyle)c[d]=this.originalStyle[d];this.element.unbind(".isotope").undelegate("."+b.hiddenClass,"click").removeClass(b.containerClass).removeData("isotope"),v.unbind(".isotope")},_getSegments:function(a){var b=this.options.layoutMode,c=a?"rowHeight":"columnWidth",d=a?"height":"width",e=a?"rows":"cols",g=this.element[d](),h,i=this.options[b]&&this.options[b][c]||this.$filteredAtoms["outer"+f(d)](!0)||g;h=Math.floor(g/i),h=Math.max(h,1),this[b][e]=h,this[b][c]=i},_checkIfSegmentsChanged:function(a){var b=this.options.layoutMode,c=a?"rows":"cols",d=this[b][c];return this._getSegments(a),this[b][c]!==d},_masonryReset:function(){this.masonry={},this._getSegments();var a=this.masonry.cols;this.masonry.colYs=[];while(a--)this.masonry.colYs.push(0)},_masonryLayout:function(a){var c=this,d=c.masonry;a.each(function(){var a=b(this),e=Math.ceil(a.outerWidth(!0)/d.columnWidth);e=Math.min(e,d.cols);if(e===1)c._masonryPlaceBrick(a,d.colYs);else{var f=d.cols+1-e,g=[],h,i;for(i=0;i<f;i++)h=d.colYs.slice(i,i+e),g[i]=Math.max.apply(Math,h);c._masonryPlaceBrick(a,g)}})},_masonryPlaceBrick:function(a,b){var c=Math.min.apply(Math,b),d=0;for(var e=0,f=b.length;e<f;e++)if(b[e]===c){d=e;break}var g=this.masonry.columnWidth*d,h=c;this._pushPosition(a,g,h);var i=c+a.outerHeight(!0),j=this.masonry.cols+1-f;for(e=0;e<j;e++)this.masonry.colYs[d+e]=i},_masonryGetContainerSize:function(){var a=Math.max.apply(Math,this.masonry.colYs);return{height:a}},_masonryResizeChanged:function(){return this._checkIfSegmentsChanged()},_fitRowsReset:function(){this.fitRows={x:0,y:0,height:0}},_fitRowsLayout:function(a){var c=this,d=this.element.width(),e=this.fitRows;a.each(function(){var a=b(this),f=a.outerWidth(!0),g=a.outerHeight(!0);e.x!==0&&f+e.x>d&&(e.x=0,e.y=e.height),c._pushPosition(a,e.x,e.y),e.height=Math.max(e.y+g,e.height),e.x+=f})},_fitRowsGetContainerSize:function(){return{height:this.fitRows.height}},_fitRowsResizeChanged:function(){return!0},_cellsByRowReset:function(){this.cellsByRow={index:0},this._getSegments(),this._getSegments(!0)},_cellsByRowLayout:function(a){var c=this,d=this.cellsByRow;a.each(function(){var a=b(this),e=d.index%d.cols,f=Math.floor(d.index/d.cols),g=(e+.5)*d.columnWidth-a.outerWidth(!0)/2,h=(f+.5)*d.rowHeight-a.outerHeight(!0)/2;c._pushPosition(a,g,h),d.index++})},_cellsByRowGetContainerSize:function(){return{height:Math.ceil(this.$filteredAtoms.length/this.cellsByRow.cols)*this.cellsByRow.rowHeight+this.offset.top}},_cellsByRowResizeChanged:function(){return this._checkIfSegmentsChanged()},_straightDownReset:function(){this.straightDown={y:0}},_straightDownLayout:function(a){var c=this;a.each(function(a){var d=b(this);c._pushPosition(d,0,c.straightDown.y),c.straightDown.y+=d.outerHeight(!0)})},_straightDownGetContainerSize:function(){return{height:this.straightDown.y}},_straightDownResizeChanged:function(){return!0},_masonryHorizontalReset:function(){this.masonryHorizontal={},this._getSegments(!0);var a=this.masonryHorizontal.rows;this.masonryHorizontal.rowXs=[];while(a--)this.masonryHorizontal.rowXs.push(0)},_masonryHorizontalLayout:function(a){var c=this,d=c.masonryHorizontal;a.each(function(){var a=b(this),e=Math.ceil(a.outerHeight(!0)/d.rowHeight);e=Math.min(e,d.rows);if(e===1)c._masonryHorizontalPlaceBrick(a,d.rowXs);else{var f=d.rows+1-e,g=[],h,i;for(i=0;i<f;i++)h=d.rowXs.slice(i,i+e),g[i]=Math.max.apply(Math,h);c._masonryHorizontalPlaceBrick(a,g)}})},_masonryHorizontalPlaceBrick:function(a,b){var c=Math.min.apply(Math,b),d=0;for(var e=0,f=b.length;e<f;e++)if(b[e]===c){d=e;break}var g=c,h=this.masonryHorizontal.rowHeight*d;this._pushPosition(a,g,h);var i=c+a.outerWidth(!0),j=this.masonryHorizontal.rows+1-f;for(e=0;e<j;e++)this.masonryHorizontal.rowXs[d+e]=i},_masonryHorizontalGetContainerSize:function(){var a=Math.max.apply(Math,this.masonryHorizontal.rowXs);return{width:a}},_masonryHorizontalResizeChanged:function(){return this._checkIfSegmentsChanged(!0)},_fitColumnsReset:function(){this.fitColumns={x:0,y:0,width:0}},_fitColumnsLayout:function(a){var c=this,d=this.element.height(),e=this.fitColumns;a.each(function(){var a=b(this),f=a.outerWidth(!0),g=a.outerHeight(!0);e.y!==0&&g+e.y>d&&(e.x=e.width,e.y=0),c._pushPosition(a,e.x,e.y),e.width=Math.max(e.x+f,e.width),e.y+=g})},_fitColumnsGetContainerSize:function(){return{width:this.fitColumns.width}},_fitColumnsResizeChanged:function(){return!0},_cellsByColumnReset:function(){this.cellsByColumn={index:0},this._getSegments(),this._getSegments(!0)},_cellsByColumnLayout:function(a){var c=this,d=this.cellsByColumn;a.each(function(){var a=b(this),e=Math.floor(d.index/d.rows),f=d.index%d.rows,g=(e+.5)*d.columnWidth-a.outerWidth(!0)/2,h=(f+.5)*d.rowHeight-a.outerHeight(!0)/2;c._pushPosition(a,g,h),d.index++})},_cellsByColumnGetContainerSize:function(){return{width:Math.ceil(this.$filteredAtoms.length/this.cellsByColumn.rows)*this.cellsByColumn.columnWidth}},_cellsByColumnResizeChanged:function(){return this._checkIfSegmentsChanged(!0)},_straightAcrossReset:function(){this.straightAcross={x:0}},_straightAcrossLayout:function(a){var c=this;a.each(function(a){var d=b(this);c._pushPosition(d,c.straightAcross.x,0),c.straightAcross.x+=d.outerWidth(!0)})},_straightAcrossGetContainerSize:function(){return{width:this.straightAcross.x}},_straightAcrossResizeChanged:function(){return!0}},b.fn.imagesLoaded=function(a){function h(){a.call(c,d)}function i(a){var c=a.target;c.src!==f&&b.inArray(c,g)===-1&&(g.push(c),--e<=0&&(setTimeout(h),d.unbind(".imagesLoaded",i)))}var c=this,d=c.find("img").add(c.filter("img")),e=d.length,f="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///ywAAAAAAQABAAACAUwAOw==",g=[];return e||h(),d.bind("load.imagesLoaded error.imagesLoaded",i).each(function(){var a=this.src;this.src=f,this.src=a}),c};var w=function(b){a.console&&a.console.error(b)};b.fn.isotope=function(a,c){if(typeof a=="string"){var d=Array.prototype.slice.call(arguments,1);this.each(function(){var c=b.data(this,"isotope");if(!c){w("cannot call methods on isotope prior to initialization; attempted to call method '"+a+"'");return}if(!b.isFunction(c[a])||a.charAt(0)==="_"){w("no such method '"+a+"' for isotope instance");return}c[a].apply(c,d)})}else this.each(function(){var d=b.data(this,"isotope");d?(d.option(a),d._init(c)):b.data(this,"isotope",new b.Isotope(a,this,c))});return this}})(window,jQuery);

// ColorBox v1.3.20.1 - jQuery lightbox plugin
// (c) 2012 Jack Moore - jacklmoore.com
// License: http://www.opensource.org/licenses/mit-license.php
(function (e, t, n) { function G(n, r, i) { var o = t.createElement(n); return r && (o.id = s + r), i && (o.style.cssText = i), e(o) } function Y(e) { var t = T.length, n = (U + e) % t; return n < 0 ? t + n : n } function Z(e, t) { return Math.round((/%/.test(e) ? (t === "x" ? tt() : nt()) / 100 : 1) * parseInt(e, 10)) } function et(e) { return B.photo || /\.(gif|png|jp(e|g|eg)|bmp|ico)((#|\?).*)?$/i.test(e) } function tt() { return n.innerWidth || N.width() } function nt() { return n.innerHeight || N.height() } function rt() { var t, n = e.data(R, i); n == null ? (B = e.extend({}, r), console && console.log && console.log("Error: cboxElement missing settings object")) : B = e.extend({}, n); for (t in B) e.isFunction(B[t]) && t.slice(0, 2) !== "on" && (B[t] = B[t].call(R)); B.rel = B.rel || R.rel || "nofollow", B.href = B.href || e(R).attr("href"), B.title = B.title || R.title, typeof B.href == "string" && (B.href = e.trim(B.href)) } function it(t, n) { e.event.trigger(t), n && n.call(R) } function st() { var e, t = s + "Slideshow_", n = "click." + s, r, i, o; B.slideshow && T[1] ? (r = function () { M.text(B.slideshowStop).unbind(n).bind(f, function () { if (B.loop || T[U + 1]) e = setTimeout(J.next, B.slideshowSpeed) }).bind(a, function () { clearTimeout(e) }).one(n + " " + l, i), g.removeClass(t + "off").addClass(t + "on"), e = setTimeout(J.next, B.slideshowSpeed) }, i = function () { clearTimeout(e), M.text(B.slideshowStart).unbind([f, a, l, n].join(" ")).one(n, function () { J.next(), r() }), g.removeClass(t + "on").addClass(t + "off") }, B.slideshowAuto ? r() : i()) : g.removeClass(t + "off " + t + "on") } function ot(t) { V || (R = t, rt(), T = e(R), U = 0, B.rel !== "nofollow" && (T = e("." + o).filter(function () { var t = e.data(this, i), n; return t && (n = t.rel || this.rel), n === B.rel }), U = T.index(R), U === -1 && (T = T.add(R), U = T.length - 1)), W || (W = X = !0, g.show(), B.returnFocus && e(R).blur().one(c, function () { e(this).focus() }), m.css({ opacity: +B.opacity, cursor: B.overlayClose ? "pointer" : "auto" }).show(), B.w = Z(B.initialWidth, "x"), B.h = Z(B.initialHeight, "y"), J.position(), d && N.bind("resize." + v + " scroll." + v, function () { m.css({ width: tt(), height: nt(), top: N.scrollTop(), left: N.scrollLeft() }) }).trigger("resize." + v), it(u, B.onOpen), H.add(A).hide(), P.html(B.close).show()), J.load(!0)) } function ut() { !g && t.body && (Q = !1, N = e(n), g = G(K).attr({ id: i, "class": p ? s + (d ? "IE6" : "IE") : "" }).hide(), m = G(K, "Overlay", d ? "position:absolute" : "").hide(), L = G(K, "LoadingOverlay").add(G(K, "LoadingGraphic")), y = G(K, "Wrapper"), b = G(K, "Content").append(C = G(K, "LoadedContent", "width:0; height:0; overflow:hidden"), A = G(K, "Title"), O = G(K, "Current"), _ = G(K, "Next"), D = G(K, "Previous"), M = G(K, "Slideshow").bind(u, st), P = G(K, "Close")), y.append(G(K).append(G(K, "TopLeft"), w = G(K, "TopCenter"), G(K, "TopRight")), G(K, !1, "clear:left").append(E = G(K, "MiddleLeft"), b, S = G(K, "MiddleRight")), G(K, !1, "clear:left").append(G(K, "BottomLeft"), x = G(K, "BottomCenter"), G(K, "BottomRight"))).find("div div").css({ "float": "left" }), k = G(K, !1, "position:absolute; width:9999px; visibility:hidden; display:none"), H = _.add(D).add(O).add(M), e(t.body).append(m, g.append(y, k))) } function at() { return g ? (Q || (Q = !0, j = w.height() + x.height() + b.outerHeight(!0) - b.height(), F = E.width() + S.width() + b.outerWidth(!0) - b.width(), I = C.outerHeight(!0), q = C.outerWidth(!0), g.css({ "padding-bottom": j, "padding-right": F }), _.click(function () { J.next() }), D.click(function () { J.prev() }), P.click(function () { J.close() }), m.click(function () { B.overlayClose && J.close() }), e(t).bind("keydown." + s, function (e) { var t = e.keyCode; W && B.escKey && t === 27 && (e.preventDefault(), J.close()), W && B.arrowKey && T[1] && (t === 37 ? (e.preventDefault(), D.click()) : t === 39 && (e.preventDefault(), _.click())) }), e("." + o, t).live("click", function (e) { e.which > 1 || e.shiftKey || e.altKey || e.metaKey || (e.preventDefault(), ot(this)) })), !0) : !1 } var r = { transition: "elastic", speed: 300, width: !1, initialWidth: "600", innerWidth: !1, maxWidth: !1, height: !1, initialHeight: "450", innerHeight: !1, maxHeight: !1, scalePhotos: !0, scrolling: !0, inline: !1, html: !1, iframe: !1, fastIframe: !0, photo: !1, href: !1, title: !1, rel: !1, opacity: .9, preloading: !0, current: "image {current} of {total}", previous: "previous", next: "next", close: "close", xhrError: "This content failed to load.", imgError: "This image failed to load.", open: !1, returnFocus: !0, reposition: !0, loop: !0, slideshow: !1, slideshowAuto: !0, slideshowSpeed: 2500, slideshowStart: "start slideshow", slideshowStop: "stop slideshow", onOpen: !1, onLoad: !1, onComplete: !1, onCleanup: !1, onClosed: !1, overlayClose: !0, escKey: !0, arrowKey: !0, top: !1, bottom: !1, left: !1, right: !1, fixed: !1, data: undefined }, i = "colorbox", s = "cbox", o = s + "Element", u = s + "_open", a = s + "_load", f = s + "_complete", l = s + "_cleanup", c = s + "_closed", h = s + "_purge", p = !e.support.opacity && !e.support.style, d = p && !n.XMLHttpRequest, v = s + "_IE6", m, g, y, b, w, E, S, x, T, N, C, k, L, A, O, M, _, D, P, H, B, j, F, I, q, R, U, z, W, X, V, $, J, K = "div", Q; if (e.colorbox) return; e(ut), J = e.fn[i] = e[i] = function (t, n) { var s = this; t = t || {}, ut(); if (at()) { if (!s[0]) { if (s.selector) return s; s = e("<a/>"), t.open = !0 } n && (t.onComplete = n), s.each(function () { e.data(this, i, e.extend({}, e.data(this, i) || r, t)) }).addClass(o), (e.isFunction(t.open) && t.open.call(s) || t.open) && ot(s[0]) } return s }, J.position = function (e, t) { function f(e) { w[0].style.width = x[0].style.width = b[0].style.width = e.style.width, b[0].style.height = E[0].style.height = S[0].style.height = e.style.height } var n, r = 0, i = 0, o = g.offset(), u, a; N.unbind("resize." + s), g.css({ top: -9e4, left: -9e4 }), u = N.scrollTop(), a = N.scrollLeft(), B.fixed && !d ? (o.top -= u, o.left -= a, g.css({ position: "fixed" })) : (r = u, i = a, g.css({ position: "absolute" })), B.right !== !1 ? i += Math.max(tt() - B.w - q - F - Z(B.right, "x"), 0) : B.left !== !1 ? i += Z(B.left, "x") : i += Math.round(Math.max(tt() - B.w - q - F, 0) / 2), B.bottom !== !1 ? r += Math.max(nt() - B.h - I - j - Z(B.bottom, "y"), 0) : B.top !== !1 ? r += Z(B.top, "y") : r += Math.round(Math.max(nt() - B.h - I - j, 0) / 2), g.css({ top: o.top, left: o.left }), e = g.width() === B.w + q && g.height() === B.h + I ? 0 : e || 0, y[0].style.width = y[0].style.height = "9999px", n = { width: B.w + q, height: B.h + I, top: r, left: i }, e === 0 && g.css(n), g.dequeue().animate(n, { duration: e, complete: function () { f(this), X = !1, y[0].style.width = B.w + q + F + "px", y[0].style.height = B.h + I + j + "px", B.reposition && setTimeout(function () { N.bind("resize." + s, J.position) }, 1), t && t() }, step: function () { f(this) } }) }, J.resize = function (e) { W && (e = e || {}, e.width && (B.w = Z(e.width, "x") - q - F), e.innerWidth && (B.w = Z(e.innerWidth, "x")), C.css({ width: B.w }), e.height && (B.h = Z(e.height, "y") - I - j), e.innerHeight && (B.h = Z(e.innerHeight, "y")), !e.innerHeight && !e.height && (C.css({ height: "auto" }), B.h = C.height()), C.css({ height: B.h }), J.position(B.transition === "none" ? 0 : B.speed)) }, J.prep = function (t) { function o() { return B.w = B.w || C.width(), B.w = B.mw && B.mw < B.w ? B.mw : B.w, B.w } function u() { return B.h = B.h || C.height(), B.h = B.mh && B.mh < B.h ? B.mh : B.h, B.h } if (!W) return; var n, r = B.transition === "none" ? 0 : B.speed; C.remove(), C = G(K, "LoadedContent").append(t), C.hide().appendTo(k.show()).css({ width: o(), overflow: B.scrolling ? "auto" : "hidden" }).css({ height: u() }).prependTo(b), k.hide(), e(z).css({ "float": "none" }), d && e("select").not(g.find("select")).filter(function () { return this.style.visibility !== "hidden" }).css({ visibility: "hidden" }).one(l, function () { this.style.visibility = "inherit" }), n = function () { function y() { p && g[0].style.removeAttribute("filter") } var t, n, o = T.length, u, a = "frameBorder", l = "allowTransparency", c, d, v, m; if (!W) return; c = function () { clearTimeout($), L.detach().hide(), it(f, B.onComplete) }, p && z && C.fadeIn(100), A.html(B.title).add(C).show(); if (o > 1) { typeof B.current == "string" && O.html(B.current.replace("{current}", U + 1).replace("{total}", o)).show(), _[B.loop || U < o - 1 ? "show" : "hide"]().html(B.next), D[B.loop || U ? "show" : "hide"]().html(B.previous), B.slideshow && M.show(); if (B.preloading) { t = [Y(-1), Y(1)]; while (n = T[t.pop()]) m = e.data(n, i), m && m.href ? (d = m.href, e.isFunction(d) && (d = d.call(n))) : d = n.href, et(d) && (v = new Image, v.src = d) } } else H.hide(); B.iframe ? (u = G("iframe")[0], a in u && (u[a] = 0), l in u && (u[l] = "true"), u.name = s + +(new Date), B.fastIframe ? c() : e(u).one("load", c), u.src = B.href, B.scrolling || (u.scrolling = "no"), e(u).addClass(s + "Iframe").appendTo(C).one(h, function () { u.src = "//about:blank" })) : c(), B.transition === "fade" ? g.fadeTo(r, 1, y) : y() }, B.transition === "fade" ? g.fadeTo(r, 0, function () { J.position(0, n) }) : J.position(r, n) }, J.load = function (t) { var n, r, i = J.prep; X = !0, z = !1, R = T[U], t || rt(), it(h), it(a, B.onLoad), B.h = B.height ? Z(B.height, "y") - I - j : B.innerHeight && Z(B.innerHeight, "y"), B.w = B.width ? Z(B.width, "x") - q - F : B.innerWidth && Z(B.innerWidth, "x"), B.mw = B.w, B.mh = B.h, B.maxWidth && (B.mw = Z(B.maxWidth, "x") - q - F, B.mw = B.w && B.w < B.mw ? B.w : B.mw), B.maxHeight && (B.mh = Z(B.maxHeight, "y") - I - j, B.mh = B.h && B.h < B.mh ? B.h : B.mh), n = B.href, $ = setTimeout(function () { L.show().appendTo(b) }, 100), B.inline ? (G(K).hide().insertBefore(e(n)[0]).one(h, function () { e(this).replaceWith(C.children()) }), i(e(n))) : B.iframe ? i(" ") : B.html ? i(B.html) : et(n) ? (e(z = new Image).addClass(s + "Photo").error(function () { B.title = !1, i(G(K, "Error").html(B.imgError)) }).load(function () { var e; z.onload = null, B.scalePhotos && (r = function () { z.height -= z.height * e, z.width -= z.width * e }, B.mw && z.width > B.mw && (e = (z.width - B.mw) / z.width, r()), B.mh && z.height > B.mh && (e = (z.height - B.mh) / z.height, r())), B.h && (z.style.marginTop = Math.max(B.h - z.height, 0) / 2 + "px"), T[1] && (B.loop || T[U + 1]) && (z.style.cursor = "pointer", z.onclick = function () { J.next() }), p && (z.style.msInterpolationMode = "bicubic"), setTimeout(function () { i(z) }, 1) }), setTimeout(function () { z.src = n }, 1)) : n && k.load(n, B.data, function (t, n, r) { i(n === "error" ? G(K, "Error").html(B.xhrError) : e(this).contents()) }) }, J.next = function () { !X && T[1] && (B.loop || T[U + 1]) && (U = Y(1), J.load()) }, J.prev = function () { !X && T[1] && (B.loop || U) && (U = Y(-1), J.load()) }, J.close = function () { W && !V && (V = !0, W = !1, it(l, B.onCleanup), N.unbind("." + s + " ." + v), m.fadeTo(200, 0), g.stop().fadeTo(300, 0, function () { g.add(m).css({ opacity: 1, cursor: "auto" }).hide(), it(h), C.remove(), setTimeout(function () { V = !1, it(c, B.onClosed) }, 1) })) }, J.remove = function () { e([]).add(g).add(m).remove(), g = null, e("." + o).removeData(i).removeClass(o).die() }, J.element = function () { return e(R) }, J.settings = r })(jQuery, document, this);

/*!
 * jScrollPane - v2.0.19 - 2013-11-16
 * http://jscrollpane.kelvinluck.com/
 *
 * Copyright (c) 2013 Kelvin Luck
 * Dual licensed under the MIT or GPL licenses.
 */
!function(a,b,c){a.fn.jScrollPane=function(d){function e(d,e){function f(b){var e,h,j,l,m,n,q=!1,r=!1;if(P=b,Q===c)m=d.scrollTop(),n=d.scrollLeft(),d.css({overflow:"hidden",padding:0}),R=d.innerWidth()+tb,S=d.innerHeight(),d.width(R),Q=a('<div class="jspPane" />').css("padding",sb).append(d.children()),T=a('<div class="jspContainer" />').css({width:R+"px",height:S+"px"}).append(Q).appendTo(d);else{if(d.css("width",""),q=P.stickToBottom&&C(),r=P.stickToRight&&D(),l=d.innerWidth()+tb!=R||d.outerHeight()!=S,l&&(R=d.innerWidth()+tb,S=d.innerHeight(),T.css({width:R+"px",height:S+"px"})),!l&&ub==U&&Q.outerHeight()==V)return d.width(R),void 0;ub=U,Q.css("width",""),d.width(R),T.find(">.jspVerticalBar,>.jspHorizontalBar").remove().end()}Q.css("overflow","auto"),U=b.contentWidth?b.contentWidth:Q[0].scrollWidth,V=Q[0].scrollHeight,Q.css("overflow",""),W=U/R,X=V/S,Y=X>1,Z=W>1,Z||Y?(d.addClass("jspScrollable"),e=P.maintainPosition&&(ab||db),e&&(h=A(),j=B()),g(),i(),k(),e&&(y(r?U-R:h,!1),x(q?V-S:j,!1)),H(),E(),N(),P.enableKeyboardNavigation&&J(),P.clickOnTrack&&o(),L(),P.hijackInternalLinks&&M()):(d.removeClass("jspScrollable"),Q.css({top:0,left:0,width:T.width()-tb}),F(),I(),K(),p()),P.autoReinitialise&&!rb?rb=setInterval(function(){f(P)},P.autoReinitialiseDelay):!P.autoReinitialise&&rb&&clearInterval(rb),m&&d.scrollTop(0)&&x(m,!1),n&&d.scrollLeft(0)&&y(n,!1),d.trigger("jsp-initialised",[Z||Y])}function g(){Y&&(T.append(a('<div class="jspVerticalBar" />').append(a('<div class="jspCap jspCapTop" />'),a('<div class="jspTrack" />').append(a('<div class="jspDrag" />').append(a('<div class="jspDragTop" />'),a('<div class="jspDragBottom" />'))),a('<div class="jspCap jspCapBottom" />'))),eb=T.find(">.jspVerticalBar"),fb=eb.find(">.jspTrack"),$=fb.find(">.jspDrag"),P.showArrows&&(jb=a('<a class="jspArrow jspArrowUp" />').bind("mousedown.jsp",m(0,-1)).bind("click.jsp",G),kb=a('<a class="jspArrow jspArrowDown" />').bind("mousedown.jsp",m(0,1)).bind("click.jsp",G),P.arrowScrollOnHover&&(jb.bind("mouseover.jsp",m(0,-1,jb)),kb.bind("mouseover.jsp",m(0,1,kb))),l(fb,P.verticalArrowPositions,jb,kb)),hb=S,T.find(">.jspVerticalBar>.jspCap:visible,>.jspVerticalBar>.jspArrow").each(function(){hb-=a(this).outerHeight()}),$.hover(function(){$.addClass("jspHover")},function(){$.removeClass("jspHover")}).bind("mousedown.jsp",function(b){a("html").bind("dragstart.jsp selectstart.jsp",G),$.addClass("jspActive");var c=b.pageY-$.position().top;return a("html").bind("mousemove.jsp",function(a){r(a.pageY-c,!1)}).bind("mouseup.jsp mouseleave.jsp",q),!1}),h())}function h(){fb.height(hb+"px"),ab=0,gb=P.verticalGutter+fb.outerWidth(),Q.width(R-gb-tb);try{0===eb.position().left&&Q.css("margin-left",gb+"px")}catch(a){}}function i(){Z&&(T.append(a('<div class="jspHorizontalBar" />').append(a('<div class="jspCap jspCapLeft" />'),a('<div class="jspTrack" />').append(a('<div class="jspDrag" />').append(a('<div class="jspDragLeft" />'),a('<div class="jspDragRight" />'))),a('<div class="jspCap jspCapRight" />'))),lb=T.find(">.jspHorizontalBar"),mb=lb.find(">.jspTrack"),bb=mb.find(">.jspDrag"),P.showArrows&&(pb=a('<a class="jspArrow jspArrowLeft" />').bind("mousedown.jsp",m(-1,0)).bind("click.jsp",G),qb=a('<a class="jspArrow jspArrowRight" />').bind("mousedown.jsp",m(1,0)).bind("click.jsp",G),P.arrowScrollOnHover&&(pb.bind("mouseover.jsp",m(-1,0,pb)),qb.bind("mouseover.jsp",m(1,0,qb))),l(mb,P.horizontalArrowPositions,pb,qb)),bb.hover(function(){bb.addClass("jspHover")},function(){bb.removeClass("jspHover")}).bind("mousedown.jsp",function(b){a("html").bind("dragstart.jsp selectstart.jsp",G),bb.addClass("jspActive");var c=b.pageX-bb.position().left;return a("html").bind("mousemove.jsp",function(a){t(a.pageX-c,!1)}).bind("mouseup.jsp mouseleave.jsp",q),!1}),nb=T.innerWidth(),j())}function j(){T.find(">.jspHorizontalBar>.jspCap:visible,>.jspHorizontalBar>.jspArrow").each(function(){nb-=a(this).outerWidth()}),mb.width(nb+"px"),db=0}function k(){if(Z&&Y){var b=mb.outerHeight(),c=fb.outerWidth();hb-=b,a(lb).find(">.jspCap:visible,>.jspArrow").each(function(){nb+=a(this).outerWidth()}),nb-=c,S-=c,R-=b,mb.parent().append(a('<div class="jspCorner" />').css("width",b+"px")),h(),j()}Z&&Q.width(T.outerWidth()-tb+"px"),V=Q.outerHeight(),X=V/S,Z&&(ob=Math.ceil(1/W*nb),ob>P.horizontalDragMaxWidth?ob=P.horizontalDragMaxWidth:ob<P.horizontalDragMinWidth&&(ob=P.horizontalDragMinWidth),bb.width(ob+"px"),cb=nb-ob,u(db)),Y&&(ib=Math.ceil(1/X*hb),ib>P.verticalDragMaxHeight?ib=P.verticalDragMaxHeight:ib<P.verticalDragMinHeight&&(ib=P.verticalDragMinHeight),$.height(ib+"px"),_=hb-ib,s(ab))}function l(a,b,c,d){var e,f="before",g="after";"os"==b&&(b=/Mac/.test(navigator.platform)?"after":"split"),b==f?g=b:b==g&&(f=b,e=c,c=d,d=e),a[f](c)[g](d)}function m(a,b,c){return function(){return n(a,b,this,c),this.blur(),!1}}function n(b,c,d,e){d=a(d).addClass("jspActive");var f,g,h=!0,i=function(){0!==b&&vb.scrollByX(b*P.arrowButtonSpeed),0!==c&&vb.scrollByY(c*P.arrowButtonSpeed),g=setTimeout(i,h?P.initialDelay:P.arrowRepeatFreq),h=!1};i(),f=e?"mouseout.jsp":"mouseup.jsp",e=e||a("html"),e.bind(f,function(){d.removeClass("jspActive"),g&&clearTimeout(g),g=null,e.unbind(f)})}function o(){p(),Y&&fb.bind("mousedown.jsp",function(b){if(b.originalTarget===c||b.originalTarget==b.currentTarget){var d,e=a(this),f=e.offset(),g=b.pageY-f.top-ab,h=!0,i=function(){var a=e.offset(),c=b.pageY-a.top-ib/2,f=S*P.scrollPagePercent,k=_*f/(V-S);if(0>g)ab-k>c?vb.scrollByY(-f):r(c);else{if(!(g>0))return j(),void 0;c>ab+k?vb.scrollByY(f):r(c)}d=setTimeout(i,h?P.initialDelay:P.trackClickRepeatFreq),h=!1},j=function(){d&&clearTimeout(d),d=null,a(document).unbind("mouseup.jsp",j)};return i(),a(document).bind("mouseup.jsp",j),!1}}),Z&&mb.bind("mousedown.jsp",function(b){if(b.originalTarget===c||b.originalTarget==b.currentTarget){var d,e=a(this),f=e.offset(),g=b.pageX-f.left-db,h=!0,i=function(){var a=e.offset(),c=b.pageX-a.left-ob/2,f=R*P.scrollPagePercent,k=cb*f/(U-R);if(0>g)db-k>c?vb.scrollByX(-f):t(c);else{if(!(g>0))return j(),void 0;c>db+k?vb.scrollByX(f):t(c)}d=setTimeout(i,h?P.initialDelay:P.trackClickRepeatFreq),h=!1},j=function(){d&&clearTimeout(d),d=null,a(document).unbind("mouseup.jsp",j)};return i(),a(document).bind("mouseup.jsp",j),!1}})}function p(){mb&&mb.unbind("mousedown.jsp"),fb&&fb.unbind("mousedown.jsp")}function q(){a("html").unbind("dragstart.jsp selectstart.jsp mousemove.jsp mouseup.jsp mouseleave.jsp"),$&&$.removeClass("jspActive"),bb&&bb.removeClass("jspActive")}function r(a,b){Y&&(0>a?a=0:a>_&&(a=_),b===c&&(b=P.animateScroll),b?vb.animate($,"top",a,s):($.css("top",a),s(a)))}function s(a){a===c&&(a=$.position().top),T.scrollTop(0),ab=a;var b=0===ab,e=ab==_,f=a/_,g=-f*(V-S);(wb!=b||yb!=e)&&(wb=b,yb=e,d.trigger("jsp-arrow-change",[wb,yb,xb,zb])),v(b,e),Q.css("top",g),d.trigger("jsp-scroll-y",[-g,b,e]).trigger("scroll")}function t(a,b){Z&&(0>a?a=0:a>cb&&(a=cb),b===c&&(b=P.animateScroll),b?vb.animate(bb,"left",a,u):(bb.css("left",a),u(a)))}function u(a){a===c&&(a=bb.position().left),T.scrollTop(0),db=a;var b=0===db,e=db==cb,f=a/cb,g=-f*(U-R);(xb!=b||zb!=e)&&(xb=b,zb=e,d.trigger("jsp-arrow-change",[wb,yb,xb,zb])),w(b,e),Q.css("left",g),d.trigger("jsp-scroll-x",[-g,b,e]).trigger("scroll")}function v(a,b){P.showArrows&&(jb[a?"addClass":"removeClass"]("jspDisabled"),kb[b?"addClass":"removeClass"]("jspDisabled"))}function w(a,b){P.showArrows&&(pb[a?"addClass":"removeClass"]("jspDisabled"),qb[b?"addClass":"removeClass"]("jspDisabled"))}function x(a,b){var c=a/(V-S);r(c*_,b)}function y(a,b){var c=a/(U-R);t(c*cb,b)}function z(b,c,d){var e,f,g,h,i,j,k,l,m,n=0,o=0;try{e=a(b)}catch(p){return}for(f=e.outerHeight(),g=e.outerWidth(),T.scrollTop(0),T.scrollLeft(0);!e.is(".jspPane");)if(n+=e.position().top,o+=e.position().left,e=e.offsetParent(),/^body|html$/i.test(e[0].nodeName))return;h=B(),j=h+S,h>n||c?l=n-P.horizontalGutter:n+f>j&&(l=n-S+f+P.horizontalGutter),isNaN(l)||x(l,d),i=A(),k=i+R,i>o||c?m=o-P.horizontalGutter:o+g>k&&(m=o-R+g+P.horizontalGutter),isNaN(m)||y(m,d)}function A(){return-Q.position().left}function B(){return-Q.position().top}function C(){var a=V-S;return a>20&&a-B()<10}function D(){var a=U-R;return a>20&&a-A()<10}function E(){T.unbind(Bb).bind(Bb,function(a,b,c,d){var e=db,f=ab,g=a.deltaFactor||P.mouseWheelSpeed;return vb.scrollBy(c*g,-d*g,!1),e==db&&f==ab})}function F(){T.unbind(Bb)}function G(){return!1}function H(){Q.find(":input,a").unbind("focus.jsp").bind("focus.jsp",function(a){z(a.target,!1)})}function I(){Q.find(":input,a").unbind("focus.jsp")}function J(){function b(){var a=db,b=ab;switch(c){case 40:vb.scrollByY(P.keyboardSpeed,!1);break;case 38:vb.scrollByY(-P.keyboardSpeed,!1);break;case 34:case 32:vb.scrollByY(S*P.scrollPagePercent,!1);break;case 33:vb.scrollByY(-S*P.scrollPagePercent,!1);break;case 39:vb.scrollByX(P.keyboardSpeed,!1);break;case 37:vb.scrollByX(-P.keyboardSpeed,!1)}return e=a!=db||b!=ab}var c,e,f=[];Z&&f.push(lb[0]),Y&&f.push(eb[0]),Q.focus(function(){d.focus()}),d.attr("tabindex",0).unbind("keydown.jsp keypress.jsp").bind("keydown.jsp",function(d){if(d.target===this||f.length&&a(d.target).closest(f).length){var g=db,h=ab;switch(d.keyCode){case 40:case 38:case 34:case 32:case 33:case 39:case 37:c=d.keyCode,b();break;case 35:x(V-S),c=null;break;case 36:x(0),c=null}return e=d.keyCode==c&&g!=db||h!=ab,!e}}).bind("keypress.jsp",function(a){return a.keyCode==c&&b(),!e}),P.hideFocus?(d.css("outline","none"),"hideFocus"in T[0]&&d.attr("hideFocus",!0)):(d.css("outline",""),"hideFocus"in T[0]&&d.attr("hideFocus",!1))}function K(){d.attr("tabindex","-1").removeAttr("tabindex").unbind("keydown.jsp keypress.jsp")}function L(){if(location.hash&&location.hash.length>1){var b,c,d=escape(location.hash.substr(1));try{b=a("#"+d+', a[name="'+d+'"]')}catch(e){return}b.length&&Q.find(d)&&(0===T.scrollTop()?c=setInterval(function(){T.scrollTop()>0&&(z(b,!0),a(document).scrollTop(T.position().top),clearInterval(c))},50):(z(b,!0),a(document).scrollTop(T.position().top)))}}function M(){a(document.body).data("jspHijack")||(a(document.body).data("jspHijack",!0),a(document.body).delegate("a[href*=#]","click",function(c){var d,e,f,g,h,i,j=this.href.substr(0,this.href.indexOf("#")),k=location.href;if(-1!==location.href.indexOf("#")&&(k=location.href.substr(0,location.href.indexOf("#"))),j===k){d=escape(this.href.substr(this.href.indexOf("#")+1));try{e=a("#"+d+', a[name="'+d+'"]')}catch(l){return}e.length&&(f=e.closest(".jspScrollable"),g=f.data("jsp"),g.scrollToElement(e,!0),f[0].scrollIntoView&&(h=a(b).scrollTop(),i=e.offset().top,(h>i||i>h+a(b).height())&&f[0].scrollIntoView()),c.preventDefault())}}))}function N(){var a,b,c,d,e,f=!1;T.unbind("touchstart.jsp touchmove.jsp touchend.jsp click.jsp-touchclick").bind("touchstart.jsp",function(g){var h=g.originalEvent.touches[0];a=A(),b=B(),c=h.pageX,d=h.pageY,e=!1,f=!0}).bind("touchmove.jsp",function(g){if(f){var h=g.originalEvent.touches[0],i=db,j=ab;return vb.scrollTo(a+c-h.pageX,b+d-h.pageY),e=e||Math.abs(c-h.pageX)>5||Math.abs(d-h.pageY)>5,i==db&&j==ab}}).bind("touchend.jsp",function(){f=!1}).bind("click.jsp-touchclick",function(){return e?(e=!1,!1):void 0})}function O(){var a=B(),b=A();d.removeClass("jspScrollable").unbind(".jsp"),d.replaceWith(Ab.append(Q.children())),Ab.scrollTop(a),Ab.scrollLeft(b),rb&&clearInterval(rb)}var P,Q,R,S,T,U,V,W,X,Y,Z,$,_,ab,bb,cb,db,eb,fb,gb,hb,ib,jb,kb,lb,mb,nb,ob,pb,qb,rb,sb,tb,ub,vb=this,wb=!0,xb=!0,yb=!1,zb=!1,Ab=d.clone(!1,!1).empty(),Bb=a.fn.mwheelIntent?"mwheelIntent.jsp":"mousewheel.jsp";"border-box"===d.css("box-sizing")?(sb=0,tb=0):(sb=d.css("paddingTop")+" "+d.css("paddingRight")+" "+d.css("paddingBottom")+" "+d.css("paddingLeft"),tb=(parseInt(d.css("paddingLeft"),10)||0)+(parseInt(d.css("paddingRight"),10)||0)),a.extend(vb,{reinitialise:function(b){b=a.extend({},P,b),f(b)},scrollToElement:function(a,b,c){z(a,b,c)},scrollTo:function(a,b,c){y(a,c),x(b,c)},scrollToX:function(a,b){y(a,b)},scrollToY:function(a,b){x(a,b)},scrollToPercentX:function(a,b){y(a*(U-R),b)},scrollToPercentY:function(a,b){x(a*(V-S),b)},scrollBy:function(a,b,c){vb.scrollByX(a,c),vb.scrollByY(b,c)},scrollByX:function(a,b){var c=A()+Math[0>a?"floor":"ceil"](a),d=c/(U-R);t(d*cb,b)},scrollByY:function(a,b){var c=B()+Math[0>a?"floor":"ceil"](a),d=c/(V-S);r(d*_,b)},positionDragX:function(a,b){t(a,b)},positionDragY:function(a,b){r(a,b)},animate:function(a,b,c,d){var e={};e[b]=c,a.animate(e,{duration:P.animateDuration,easing:P.animateEase,queue:!1,step:d})},getContentPositionX:function(){return A()},getContentPositionY:function(){return B()},getContentWidth:function(){return U},getContentHeight:function(){return V},getPercentScrolledX:function(){return A()/(U-R)},getPercentScrolledY:function(){return B()/(V-S)},getIsScrollableH:function(){return Z},getIsScrollableV:function(){return Y},getContentPane:function(){return Q},scrollToBottom:function(a){r(_,a)},hijackInternalLinks:a.noop,destroy:function(){O()}}),f(e)}return d=a.extend({},a.fn.jScrollPane.defaults,d),a.each(["arrowButtonSpeed","trackClickSpeed","keyboardSpeed"],function(){d[this]=d[this]||d.speed}),this.each(function(){var b=a(this),c=b.data("jsp");c?c.reinitialise(d):(a("script",b).filter('[type="text/javascript"],:not([type])').remove(),c=new e(b,d),b.data("jsp",c))})},a.fn.jScrollPane.defaults={showArrows:!1,maintainPosition:!0,stickToBottom:!1,stickToRight:!1,clickOnTrack:!0,autoReinitialise:!1,autoReinitialiseDelay:500,verticalDragMinHeight:0,verticalDragMaxHeight:99999,horizontalDragMinWidth:0,horizontalDragMaxWidth:99999,contentWidth:c,animateScroll:!1,animateDuration:300,animateEase:"linear",hijackInternalLinks:!1,verticalGutter:4,horizontalGutter:4,mouseWheelSpeed:3,arrowButtonSpeed:0,arrowRepeatFreq:50,arrowScrollOnHover:!1,trackClickSpeed:0,trackClickRepeatFreq:70,verticalArrowPositions:"split",horizontalArrowPositions:"split",enableKeyboardNavigation:!0,hideFocus:!1,keyboardSpeed:0,initialDelay:300,speed:30,scrollPagePercent:.8}}(jQuery,this);

/*! todo - This was added as a quick fix on preprod - Move this plugin into its own file. 3/2/14
* jQuery Cookie Plugin v1.4.0
* https://github.com/carhartl/jquery-cookie
*
* Copyright 2013 Klaus Hartl
* Released under the MIT license
*/
(function (factory) {
    if (typeof define === 'function' && define.amd) {
        // AMD
        define(['jquery'], factory);
    } else if (typeof exports === 'object') {
        // CommonJS
        factory(require('jquery'));
    } else {
        // Browser globals
        factory(jQuery);
    }
} (function ($) {

    var pluses = /\+/g;

    function encode(s) {
        return config.raw ? s : encodeURIComponent(s);
    }

    function decode(s) {
        return config.raw ? s : decodeURIComponent(s);
    }

    function stringifyCookieValue(value) {
        return encode(config.json ? JSON.stringify(value) : String(value));
    }

    function parseCookieValue(s) {
        if (s.indexOf('"') === 0) {
            // This is a quoted cookie as according to RFC2068, unescape...
            s = s.slice(1, -1).replace(/\\"/g, '"').replace(/\\\\/g, '\\');
        }

        try {
            // Replace server-side written pluses with spaces.
            // If we can't decode the cookie, ignore it, it's unusable.
            // If we can't parse the cookie, ignore it, it's unusable.
            s = decodeURIComponent(s.replace(pluses, ' '));
            return config.json ? JSON.parse(s) : s;
        } catch (e) { }
    }

    function read(s, converter) {
        var value = config.raw ? s : parseCookieValue(s);
        return $.isFunction(converter) ? converter(value) : value;
    }

    var config = $.cookie = function (key, value, options) {

        // Write

        if (value !== undefined && !$.isFunction(value)) {
            options = $.extend({}, config.defaults, options);

            if (typeof options.expires === 'number') {
                var days = options.expires, t = options.expires = new Date();
                t.setTime(+t + days * 864e+5);
            }

            return (document.cookie = [
				encode(key), '=', stringifyCookieValue(value),
				options.expires ? '; expires=' + options.expires.toUTCString() : '', // use expires attribute, max-age is not supported by IE
				options.path ? '; path=' + options.path : '',
				options.domain ? '; domain=' + options.domain : '',
				options.secure ? '; secure' : ''
			].join(''));
        }

        // Read

        var result = key ? undefined : {};

        // To prevent the for loop in the first place assign an empty array
        // in case there are no cookies at all. Also prevents odd result when
        // calling $.cookie().
        var cookies = document.cookie ? document.cookie.split('; ') : [];

        for (var i = 0, l = cookies.length; i < l; i++) {
            var parts = cookies[i].split('=');
            var name = decode(parts.shift());
            var cookie = parts.join('=');

            if (key && key === name) {
                // If second argument (value) is a function it's a converter...
                result = read(cookie, value);
                break;
            }

            // Prevent storing a cookie that we couldn't decode.
            if (!key && (cookie = read(cookie)) !== undefined) {
                result[name] = cookie;
            }
        }

        return result;
    };

    config.defaults = {};

    $.removeCookie = function (key, options) {
        if ($.cookie(key) === undefined) {
            return false;
        }

        // Must not alter options, thus extending a fresh object...
        $.cookie(key, '', $.extend({}, options, { expires: -1 }));
        return !$.cookie(key);
    };

}));

/* jquery-debouce
* https://github.com/diaspora/jquery-debounce
*/
(function (a) { function b(a, b) { var c = this, d, e; return function () { return e = Array.prototype.slice.call(arguments, 0), d = clearTimeout(d, e), d = setTimeout(function () { a.apply(c, e), d = 0 }, b), this } } a.extend(a.fn, { debounce: function (a, c, d) { this.bind(a, b.apply(this, [c, d])) } }) })(jQuery);