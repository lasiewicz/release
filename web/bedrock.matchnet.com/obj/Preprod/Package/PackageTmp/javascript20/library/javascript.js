﻿// set jquery ui overrides
// global fix for broken chrome scrollbar when modal is present
// http://stackoverflow.com/questions/7519489/jqueryui-modal-add-function-to-defaults#answer-7520103
$j.ui.dialog.overlay.events = $j.map('focus,keydown,keypress'.split(','), function (event) { return event + '.dialog-overlay'; }).join(' ');
$j.ui.dialog.prototype.options.resizable = false;
$j.ui.dialog.prototype.options.draggable = false;

// set jquery plugin overrides
// blockUI settings
$j.blockUI.defaults.css.cursor = 'default';
$j.blockUI.defaults.overlayCSS.cursor = 'default';

//patch IE7 bug sizzle selector engine bug in jQuery 1.7.1
//this will probably be fixed in 1.7.2; see bug ticket for more info
//http://bugs.jquery.com/ticket/10570
(function () {
    if ($j('html').hasClass('ie7')) {
        jQuery.find.selectors.filters.text = function (elem) {
            var attr, type;
            // IE6 and 7 will map elem.type to 'text' for new HTML5 types (search, etc) 
            // use getAttribute instead to test this case
            return elem.nodeName.toLowerCase() === "input" && (type = elem.type, attr = elem.getAttribute("type"), "text" === type) && (attr === type || attr === null);
        }
    }
} ());

// set global spark event bindings
$j(spark).bind("simpleTooltip", function (event, obj) {

    $j(obj).delegate('.tooltip', 'mouseover mouseout', function (e) {
        var $target = $j(e.target);
        var tipTitle = $target.attr('title');

        if (tipTitle != '') {
            $target.data('tipTitleData', tipTitle);
        }

        $target.attr('title', '');

        if (e.type == 'mouseover') {
            var tipHtml = '<div class="tooltip-content"><p>' + $target.data('tipTitleData') + '</p></div>';
            $j(tipHtml).appendTo('body').css({ left: e.pageX + 'px', top: (e.pageY + 12) + 'px' }).show();
        }
        if (e.type == 'mouseout') {
            $j('.tooltip-content').remove();
        }
    });
});

$j(spark).bind("jqueryUIDialogMessaging", function (event, customMessage, messageClass) {
    var $messaging = $j('<div />').addClass(messageClass);

    $messaging.html(customMessage).prependTo('.ui-dialog-content:visible');
});

// Trigger custom events onDomReady
$j(function () {
    $j(spark).trigger("simpleTooltip", $j('body'));

});


var sendRegEventOnInputFocusDefaultReg = "notsent";

//MiniSearch
jQuery(document).ready(function () {

    if (jQuery('#MarketingClose').length) {
        jQuery('#profile-full-comm .action-hot-list .listMenuContainer').css({ display: "none" });
    }

    jQuery('#MarketingClose').click(function () {
        jQuery('#divMiniSearchMarketingCopy').hide();
        jQuery('#profile-full-comm .action-hot-list .listMenuContainer').css({ display: "block" });
        return false;
    });
});

// rounded corner tab style enhancements
jQuery(document).ready(function () {

    var indHover = '<div class="nav-profile-full-indicator"></div>';
    var indSelected = '<div class="nav-profile-full-selector-indicator"></div>';

    jQuery('.nav-rounded-tabs li.tab.selected').not('#video-tabs .nav-rounded-tabs li.tab.selected').append(indSelected);

    jQuery('.nav-rounded-tabs li.tab a').hover(function () {
        jQuery('.nav-rounded-tabs li.tab .nav-profile-full-indicator').remove();
        jQuery(this).not('#video-tabs ul.nav-rounded-tabs li.tab a').not('.nav-rounded-tabs li.tab.selected a').append(indHover);
        jQuery('.nav-rounded-tabs li.tab.selected.hover').append(indSelected);
    }, function () {
        jQuery('.nav-rounded-tabs li.tab .nav-profile-full-insdicator').hide();
    });

    jQuery('.nav-rounded-tabs.click li.tab a').click(function (event) {
        if (!window['isEdit']) {  //added for profedit project                        
            jQuery('.nav-rounded-tabs li.tab div.nav-profile-full-selector-indicator').remove();
            jQuery(this).append(indSelected);
        }
    });

    // Change the down arrow on the tabs. 
    jQuery('.thumbs li').click(function (event) {
        jQuery('.nav-rounded-tabs li.tab div.nav-profile-full-selector-indicator').remove();
        var relatedTab = jQuery(".tab[id*=" + this.className + "]");
        jQuery(relatedTab).append(indSelected);
    });
});

// UI enhancements not based on browser or element widths
//all
jQuery(document).ready(function () {
    //open links that go off-site in new window
    jQuery('a[rel*=external]').attr('target', '_blank');
    jQuery('a[rel*=popup]').click(function () {
        var href = jQuery(this).attr('href');
        window.open(href, 'popup', 'height=500,width=646,toolbar=no,scrollbars=yes,resizable=yes,menubar=yes,location=yes,directories=no,toolbar=yes');
        return false;
    });

    jQuery('[rel*=hover]').live('mouseover mouseout', function (event) {
        if (event.type == 'mouseover') {
            var $hoverE = jQuery(this).next('div.rel-layer-div');
            jQuery($hoverE).show();
        } else {
            jQuery(this).next('div.rel-layer-div').hide().css({ left: '0' });
            return false;
        }
    });


    jQuery('[rel*=click]').click(function () {
        var swid = (jQuery('#content-container').width()) - 22;
        var $hoverE = jQuery(this).next('div.rel-layer-div');
        var lmar = jQuery('#content-container').offset();

        jQuery($hoverE).show();

        var hwid = $hoverE.width();
        var iposRaw = jQuery(this).offset();
        var ipos = iposRaw.left - lmar.left

        if (hwid > swid) {
        }
        else if (ipos + hwid > swid) {
            var move = swid - (ipos + hwid);
            $hoverE.css({ left: move });
        }
        return false;
    });

    jQuery('.rel-layer-div a.click-close').click(function () {
        jQuery(this).parent().parent().hide();
        return false;
    });
    $j('span.spr, span.spr-btn').parent().addClass('spr-parent');
});

//generic jQuery onDocReady adjustments
jQuery(document).ready(function () {
    // add element around results member spotlight
    jQuery('.results.spotlight-header').wrap('<div class="spotlighted-header-wrap"></div>');
    jQuery('.results.list-view.spotlighted').wrap('<div class="spotlighted-profile-wrap"></div>');
});

// z-index fixes - these should be made into functions at some point
if ($j.browser.msie && parseInt($j.browser.version) <= "8") {
    jQuery(document).ready(function () {
        var zIndexNumber = 100;
        //        jQuery('#content-main .listMenuContainer').each(function() {
        //            jQuery(this).css('zIndex', zIndexNumber);
        //            zIndexNumber -= 2;
        //        });
        jQuery('#content-main .results.photo-view').each(function () {
            jQuery(this).css('zIndex', zIndexNumber);
            zIndexNumber -= 2;
        });
        //	    jQuery('#content-main ul li[class^=plansRow]').each(function() {
        //		    jQuery(this).css('zIndex', zIndexNumber);
        //		    zIndexNumber -= 2;
        //	    });
        jQuery('#content-main .sub-item').each(function () {
            jQuery(this).css('zIndex', zIndexNumber);
            zIndexNumber -= 2;
        });
    });
}

// zebra stripe for articles
jQuery(document).ready(function () {
    jQuery('#article table.zebra-striping tr:even').addClass('odd');
});

// zebra stripe inbox
jQuery(document).ready(function () {
    jQuery('#mail-list .mail-table tr:even').addClass('odd');
});


// transparent layer over mini-profile
// compose message
jQuery(document).ready(function () {
    try {
        var blockProfiles = jQuery('body.page-email.sub-page-viewmessage .results.list-view')
            .add('body.page-sendtofriend.sub-page-sendtofriend .results.list-view')
            .add('body.page-memberservices.sub-page-reportmember .results.list-view')
            .add('body.page-email.sub-page-tease .results.list-view')
            .add('#profile-mini-blocked .results.list-view');
        jQuery(blockProfiles).block({
                message: null,
                overlayCSS: { backgroundColor: '#96b1cc', opacity: '0.3', cursor: 'auto' }
            }
        );
        jQuery('body.page-subscription .carrot-profile .member-pic, body.page-subscription .carrot-profile .member-info').block({
                message: null,
                overlayCSS: { backgroundColor: '#96b1cc', opacity: '0.0', cursor: 'auto' }
            }
        );
    } catch(e) {
        if (typeof (console) !== "undefined") {
            console.log(e.message);
        }
    }
});

//edit profile adjustments
jQuery(document).ready(function () {

    jQuery('body.page-memberprofile.sub-page-registrationstep1 #prefBorder tr')
        .add('body.page-memberprofile.sub-page-registrationstep2 #prefBorder tr')
        .add('body.page-memberprofile.sub-page-registrationstep3 #prefBorder tr')
        .add('body.page-memberprofile.sub-page-registrationstep4 #prefBorder tr')
        .css({ height: '2.6em' })
    ;
    jQuery('body.page-memberprofile.sub-page-registrationstep1 #prefBorder td')
        .add('body.page-memberprofile.sub-page-registrationstep2 #prefBorder td')
        .add('body.page-memberprofile.sub-page-registrationstep3 #prefBorder td')
        .add('body.page-memberprofile.sub-page-registrationstep4 #prefBorder td')
        .css({ paddingRight: '.3em' })
    ;
    jQuery('body.page-memberprofile.sub-page-registrationstep3 table.twoColumnCheckList td:even')
        .add('body.page-memberprofile.sub-page-registrationstep4 table.twoColumnCheckList td:even')
        .add('body.page-memberprofile.sub-page-registrationstep3 table.threeColumnCheckList td:even')
        .css({ width: '18em' })
    ;
    jQuery('body.page-memberprofile.sub-page-registrationstep1 #prefBorder .edit-profile-form-table')
        .add('body.page-memberprofile.sub-page-registrationstep2 #prefBorder .edit-profile-form-table')
        .add('body.page-memberprofile.sub-page-registrationstep3 #prefBorder .edit-profile-form-table')
        .add('body.page-memberprofile.sub-page-registrationstep4 #prefBorder .edit-profile-form-table')
        .css({ marginLeft: '1em' })
    ;
    jQuery('body.page-memberprofile.sub-page-registrationstep1 #prefBorder textarea')
        .add('body.page-memberprofile.sub-page-registrationstep2 #prefBorder textarea')
        .add('body.page-memberprofile.sub-page-registrationstep3 #prefBorder textarea')
        .add('body.page-memberprofile.sub-page-registrationstep4 #prefBorder textarea')
        .css({ width: '98%' })
    ;

    jQuery('body.page-memberprofile.sub-page-registrationstep1 #prefBorder table.full-width').css({ width: '98%' });
    jQuery('body.page-memberprofile.sub-page-registrationstep1 #prefBorder [width]')
        .add('body.page-memberprofile.sub-page-registrationstep2 #prefBorder [width]')
        .add('body.page-memberprofile.sub-page-registrationstep3 #prefBorder [width]')
        .add('body.page-memberprofile.sub-page-registrationstep4 #prefBorder [width]')
        .removeAttr('width')
    ;

    jQuery('body.page-memberprofile #content-main #prefBorder').show();
});

// Classic overrides

jQuery(document).ready(function () {
    // Mail make view profile table 100%
    jQuery('#_ctl0__ctl4_viewProfileInfo_ViewProfileTable').css({ width: "100%" });
    jQuery('#_ctl0__ctl4_viewProfileInfo__ctl1_tbInterests td, #_ctl0__ctl4_viewProfileInfo__ctl0_tbRelationship td').css({ paddingBottom: "1em" });
});

jQuery(document).ready(function () {
    // toggle FAQ answer block
    jQuery('#faq-wrapper #top-ten h3').click(function () {
        jQuery(this).next("div.answer-block").toggle();
    });
});


//Show/Hide message setting help blocks
jQuery(document).ready(function () {

    if (jQuery('#msgPrefContainer').length > 0) {
        jQuery(".whatsthis").click(function () {
            //This grabs the first class name of the (What's this?) link, 
            //which should be the same as the first part of the id. It will
            //also be the first part of the corresponding answer box id.
            var answerBox = "#" + jQuery(this).attr("class").split(' ').slice(0)[0] + "-answer";

            //Which (What's this?) link was clicked.
            var whatsThisLink = jQuery(this);

            //Hide the (What's this?) link and show corresponding answer box
            jQuery(whatsThisLink).removeClass('showSpan').addClass('hide');
            jQuery(answerBox).removeClass('hide').addClass('show');

            //Add listener to "[Hide]" link
            var hideLink = answerBox + " .messageSettingsHelpParagraphHide a";
            jQuery(hideLink).click(function () {
                jQuery(answerBox).removeClass('show').addClass('hide');
                jQuery(whatsThisLink).removeClass('hide').addClass('showSpan');
            });
        });
    }
});

jQuery(document).ready(function () {

    function showslider() {
        var windowHeight = jQuery(document).height();
        jQuery('#ie6only #slider-container').width(jQuery(document).width());
        jQuery('#slider-container').height(windowHeight).show();
        jQuery('#emislider').show().animate({ top: "100px" }, 2000);   // modify the "2000" value to dictate animation speed; higher is slower
    }

    function setOmnitureValues(propertyValue, pageName) {
        PopulateS(true); //clear existing values in omniture "s" object
        s.prop14 = propertyValue;
        s.pageName = pageName;
        s.tl(); //send omniture updated values as page load
    }
    if (jQuery("#emislider").length > 0) {
        // the close button
        jQuery("map area#slider_close").click(function () {
            jQuery('#emislider').hide();
            jQuery('#slider-container').hide();
            return false;
        });

        // the continue button
        jQuery("map area#slider_continue, map area#slider_partner, map area#slider_claim").click(function () {
            var thehref = jQuery(this).attr("href");
            jQuery('#emislider').hide();
            jQuery('#slider-container').hide();
            setOmnitureValues("JRewards Slider – Continue", "jrewards");
            window.open(thehref);
            return false;
        });
        showslider();
    }
});

//Close jrewards slider			
function hideSlider() {
    jQuery(document).ready(function () {
        jQuery('#emislider').hide();
        jQuery('#slider-container').hide();
    });
}

function colorCodeCharts() {
    //	$j('table.cc-chart-hori-small').visualize({
    //		type: 'bar',
    //		barDirection: 'horizontal',
    //		title: 'Color Code',
    //		appendTitle: false,
    //		appendKey: true,
    //		width: 70,
    //		height: 88,
    //		textColors: ['#000000','#000000','#000000','#000000']
    //	}).addClass('cc-chart-hori-small-container');

    if ($j('body.sub-page-registrationwelcome').length) {
        var pieWidth = 190;
        var pieHeight = 170;
        var pieClass = 'cc-chart-pie-container cc-chart-pie-small';
    } else {
        var pieWidth = 240;
        var pieHeight = 240;
        var pieClass = 'cc-chart-pie-container';
    }

    $j('table.cc-chart-pie').visualize({
        type: 'pie',
        pieMargin: 0,
        title: 'Color Code',
        appendTitle: false,
        appendKey: true,
        width: pieWidth,
        height: pieHeight,
        pieLabelPos: 'inside',
        textColors: ['#000000', '#000000', '#000000', '#000000']
    }).addClass(pieClass);

    $j('table.cc-chart-vert-large').visualize({
        type: 'bar',
        barDirection: 'vertical',
        title: 'Color Code',
        appendTitle: false,
        appendKey: true,
        width: 240,
        height: 140,
        textColors: ['#000000', '#000000', '#000000', '#000000']
    }).addClass('cc-chart-vert-large-container');

    $j('#cc-pie-chart-loading').css({ display: 'none' });
}

function colorCodeColorOrder(element) {
    var thisElement = $j(element).find('th:gt(0)');
    var colorOrder = $j.map($j(thisElement), function (e) { return $j(e).text(); })

    $j(colorOrder).each(function (i) {
        switch (colorOrder[i]) {
            case "Blue":
                colorOrder[i] = "#6380e4";
                break;
            case "BLUE":
                colorOrder[i] = "#6380e4";
                break;
            case "White":
                colorOrder[i] = "#ebeff2";
                break;
            case "WHITE":
                colorOrder[i] = "#ebeff2";
                break;
            case "Yellow":
                colorOrder[i] = "#fad350";
                break;
            case "YELLOW":
                colorOrder[i] = "#fad350";
                break;
            case "Red":
                colorOrder[i] = "#b13736";
                break;
            case "RED":
                colorOrder[i] = "#b13736";
                break;
        }

        //		if (colorOrder[i] == "Blue"){
        //			colorOrder[i] = "#6380e4";
        //		}
        //		if (colorOrder[i] == "White"){
        //			colorOrder[i] = "#ebeff2";
        //		}
        //		if (colorOrder[i] == "Yellow"){
        //			colorOrder[i] = "#fad350";
        //		}
        //		if (colorOrder[i] == "Red"){
        //			colorOrder[i] = "#b13736";
        //		}
    });
    return colorOrder;
}

$j(document).ready(function () {
    if ($j.browser.msie && parseInt($j.browser.version) <= "8") {
        setTimeout(function () { colorCodeCharts() }, 500);
    }
    else {
        colorCodeCharts();
    }
});

function ccFPShowExample(element) {
    $j(element).appendTo('body');
    $j.blockUI({
        message: element,
        overlayCSS: {
            backgroundColor: '#000',
            opacity: '0.1',
            cursor: 'pointer'
        },
        css: {
            cursor: 'pointer',
            top: '100px',
            width: '744px',
            left: '22%',
            border: 'none',
            backgroundColor: 'transparent',
            position: 'fixed'
        }
    }
	);
}

//Peek Banner Hover
$j(document).ready(function () {
    $j('.plcPeekBnrFlash').hover(function () {
        $j(this).css({
            'clip': 'rect(0px 655px 655px 0px)'
        });
    }, function () {
        $j(this).css({
            'clip': 'rect(0px 100px 100px 0px)'
        });
    });
});

$j(document).ready(function () {
    $j('#cc-promo-examples').click(function (event) {
        var $red = $j('#cc-fp-example-red');
        var $white = $j('#cc-fp-example-white');
        var $blue = $j('#cc-fp-example-blue');
        var $yellow = $j('#cc-fp-example-yellow');

        if ($j(event.target).hasClass('cc-promo-pic cc-spr-red')) {
            ccFPShowExample($red);
        }
        if ($j(event.target).hasClass('cc-promo-pic cc-spr-white')) {
            ccFPShowExample($white);
        }
        if ($j(event.target).hasClass('cc-promo-pic cc-spr-blue')) {
            ccFPShowExample($blue);
        }
        if ($j(event.target).hasClass('cc-promo-pic cc-spr-yellow')) {
            ccFPShowExample($yellow);
        }
        $j('.blockOverlay, .blockUI').attr('title', 'Click to close').click($j.unblockUI);
    });
});

// Color Code Comprehensive menu
$j(document).ready(function () {

    function megaHoverOver() {
        $j(this).find(".sub").stop().fadeTo('fast', 1).show();

        //Calculate width of all ul's
        (function ($) {
            jQuery.fn.calcSubWidth = function () {
                rowWidth = 0;
                //Calculate row
                $j(this).find("ul").each(function () {
                    rowWidth += $j(this).width();
                });
            };
        })(jQuery);

        if ($j(this).find(".row").length > 0) { //If row exists...
            var biggestRow = 0;
            //Calculate each row
            $j(this).find(".row").each(function () {
                $j(this).calcSubWidth();
                //Find biggest row
                if (rowWidth > biggestRow) {
                    biggestRow = rowWidth;
                }
            });
            //Set width
            $j(this).find(".sub").css({ 'width': biggestRow });
            $j(this).find(".row:last").css({ 'margin': '0' });

        } else { //If row does not exist...

            $j(this).calcSubWidth();
            //Set Width
            $j(this).find(".sub").css({ 'width': rowWidth });

        }
    }

    function megaHoverOut() {
        $j(this).find(".sub").stop().fadeTo('fast', 0, function () {
            $j(this).hide();
        });
    }


    var config = {
        sensitivity: 2, // number = sensitivity threshold (must be 1 or higher)    
        interval: 0, // number = milliseconds for onMouseOver polling interval    
        over: megaHoverOver, // function = onMouseOver callback (REQUIRED)    
        timeout: 0, // number = milliseconds delay before onMouseOut    
        out: megaHoverOut // function = onMouseOut callback (REQUIRED)    
    };

    $j("ul#ccc-nav li .sub").css({ 'opacity': '0' });
    $j("ul#ccc-nav li").hoverIntent(config);

});

// Modal-popup for Color Code on Registration page
$j(document).ready(function () {

    $j('a.modal-link').live('click', function () {

        var modalcontent = $j('#cc-info-modal');

        $j.blockUI({
            message: modalcontent,
            overlayCSS: {
                backgroundColor: '#000',
                opacity: '0.5',
                cursor: 'pointer'
            },
            css: {
                cursor: 'auto',
                top: '15%',
                left: '15%',
                width: '70%',
                border: 'none',
                backgroundColor: 'transparent',
                textAlign: 'left'
            },
            fadeIn: 0,
            fadeOut: 0,
            timeout: 0
        });
        $j('.blockOverlay, .ui-modal-window .close').click($j.unblockUI);
    });
});

function querySt(yourId, queryString) {
    yourId = yourId.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regexStr = "[\\?&]" + yourId + "=([^&#]*)";
    var regex = new RegExp(regexStr);
    var results = regex.exec(queryString);
    if (results == null)
        return "";
    else
        return results[1];
}


$j(document).ready(function () {
    $j('body').mouseover(function (e) {
        if ($j(e.target).is('.cc-pic-tag')) {
            $j('#cc-pic-tag-help').css({ left: e.pageX + 'px', top: (e.pageY + 12) + 'px', zIndex: '1003' }).show();
        }
    }).mouseout(function (e) {
        if ($j(e.target).is('.cc-pic-tag')) {
            $j('#cc-pic-tag-help').hide();
        }
    }).click(function (e) {
        if ($j(e.target).is('.cc-pic-tag')) {
            var modalcontent = $j('#cc-info-modal');
            $j.blockUI({
                message: modalcontent,
                overlayCSS: {
                    backgroundColor: '#000',
                    opacity: '0.5',
                    cursor: 'pointer'
                },
                css: {
                    cursor: 'auto',
                    top: '20%',
                    width: '436px',
                    left: '35%',
                    border: 'none',
                    backgroundColor: 'transparent',
                    textAlign: 'left'
                }
            });
            $j('.blockOverlay, .ui-modal-window .close').click($j.unblockUI);
            if ($j('.page-memberprofile.sub-page-viewprofile').length) {
                $j('#cc-info-modal-fp-link').hide();
            } else {
                var hrefValue = $j(e.target).parent().closest("div").find('a').attr('href');
                var MemberID = querySt("memberid", hrefValue.toLowerCase());
                $j('#cc-goto-fp-cctab').attr("href", '/Applications/MemberProfile/ViewProfile.aspx?MemberID=' + MemberID);
            }
        }
    });

    $j('.cc-quiz-help').click(function () {
        var modalcontent = $j('#cc-help-modal');
        $j.blockUI({
            message: modalcontent,
            overlayCSS: {
                backgroundColor: '#000',
                opacity: '0.5',
                cursor: 'pointer'
            },
            css: {
                cursor: 'auto',
                top: '20%',
                width: '436px',
                left: '35%',
                border: 'none',
                backgroundColor: 'transparent',
                textAlign: 'left'
            }
        });
        $j('.blockOverlay, .ui-modal-window .close').click($j.unblockUI);
        return false;
    });
});

$j(document).ready(function () {
    // homepage filmstrip
    var filmStripItems = $j(".filmstrip li.item").size();
    if (filmStripItems <= 6) {
        $j('button.filmstrip-next').addClass('disabled');
    }
    $j(".filmstrip").jCarouselLite({
        btnNext: ".filmstrip-next",
        btnPrev: ".filmstrip-prev",
        circular: false,
        mouseWheel: true,
        scroll: 6,
        speed: 600,
        visible: 6
    });

    // homepage activity items
    var vertStripItems = $j(".vertstrip-news li.item").size();
    if (vertStripItems <= 5) {
        $j('button.vertstrip-next').addClass('disabled');
    }
    $j(".vertstrip-news").jCarouselLite({
        btnNext: ".vertstrip-next",
        btnPrev: ".vertstrip-prev",
        circular: false,
        mouseWheel: true,
        visible: 3,
        vertical: true
    });
    $j('button.filmstrip-prev.disabled, button.filmstrip-next.disabled, button.vertstrip-prev.disabled, button.vertstrip-next.disabled').live('click', function () { return false; });

    $j(".ajax-loading").hide();

});

__addToNamespace__('spark.util', {
    ajaxCallWithBlock: function (blockElemId, blockParams, ajaxFunc, ajaxUrl) {
        var bool = ajaxFunc();  //ajaxFunc will return false to indicate that ajax was not called in this case, don't block
        if (bool + '' != 'false') {
            var blockObj = (typeof (blockElemId) == 'string') ? $j('#' + blockElemId) : $j(blockElemId);
            blockObj.block(blockParams);
            blockObj.ajaxSuccess(function (e, xhr, settings) {
                if (settings.url == ajaxUrl) {
                    blockObj.unblock();
                }
            }).ajaxComplete(function (e, xhr, settings) {
                if (settings.url == ajaxUrl) {
                    blockObj.unblock();
                }
            });
        }
        return bool;
    },
    __init__: function () { }
});

$j(document).ready(function () {
    $j('.autobox').autobox();
});

// Send reg start event only after focus on reg field when coming from media
$j(document).ready(function () {
    if (getUrlParam(location.href, 'media') == "1") {
        jQuery("#content-main input,#content-main select").focusin(function () {
            if (sendRegEventOnInputFocusDefaultReg == "notsent") {
                sendRegEventOnInputFocusDefaultReg = "sent"
                s.events = s.apl(s.events, omnitureRegStartEvent, ',', 1);
                s.t();
            }

        });
    }
});


/******* header menu - no cache for subscription image ********/

//track start/end editing attributes vars
var trackStartEditAttribute = false;
var trackEndEditingAttributes = false;

//is already bind to save details click event
var isSaveDetailsBind = false;

jQuery(document).ready(function () {
    var src = jQuery(".subli-with-icon img").attr("src");
    var numRand = Math.floor(Math.random() * 9999999);
    src += "?" + numRand;
    jQuery(".subli-with-icon img").attr("src", src);
});

/******* registration welcome one page reg ********/
jQuery(document).ready(function () {
    coverBody();
    showOverlayBox();
    var attribeCount = 0;
    //count total attributes
    jQuery(".send-attribute ul").each(function () {
        attribeCount++;
    });

    var selectedAttr = 0;

    jQuery(".send-attribute ul li").click(function () {

        //track start editing attributes
        if (!trackStartEditAttribute) {
            var s = s_gi(s_account); s.linkTrackVars = 'events';
            s.linkTrackEvents = 'event57';
            s.events = 'event57';
            s.tl(true, 'o', 'Overlay complete profile - Start: Type=Counter');
            trackStartEditAttribute = true;
        }

        jQuery(".success-message").css("visibility", "hidden");
        jQuery(this).parent().children().each(function () {
            jQuery(this).removeClass("selected");
        });
        jQuery(this).addClass("selected");
        selectedAttr = 0;
        jQuery(".send-attribute ul li.selected").each(function () {
            selectedAttr++;
            if (selectedAttr >= attribeCount) {
                showBtn();
                return false;
            }
        });
    });

    //scroll height to 160cm
    var heightOptionsToScroll = 10;
    var liHeight = jQuery(".Height ul li:last").attr("scrollHeight");
    heightTab = liHeight * heightOptionsToScroll;
    jQuery(".Height ul").animate({ scrollTop: heightTab }, 0);

    //scroll to selected value
    jQuery(".send-attribute").each(function () {
        var i = 0;
        jQuery(this).children("ul").children("li").each(function () {
            if (jQuery(this).hasClass("selected")) {
                if (i == 1 != i == 0) {
                    jQuery(this).parent().animate({ scrollTop: 0 }, 0);
                }
                else {
                    jQuery(this).parent().animate({ scrollTop: i * liHeight }, 0);
                }
            }
            else {
                i++;
            }
        });
    });
});

jQuery(document).ready(function () {
    jQuery('.overlayBox .reg-overlay-osr').parents('.overlayBox').addClass('osr-cont');
    jQuery('.overlayBox.osr-cont #progressbar').remove();
    jQuery('.overlayBox.osr-cont .JDateReligion').next().css({ minHeight: '74px' });
    jQuery('.overlayBox.osr-cont #overlay-error').remove();
});

function showBtn() {
    jQuery("#send-attr-btn").addClass("enabled");

    if (!isSaveDetailsBind) {
        jQuery("#send-attr-btn.enabled").click(function () {
            jQuery("#send-attr-btn").removeClass("enabled");
            var data = "{attributesList:'[";
            jQuery(".send-attribute ul li.selected").each(function () {
                key = jQuery(this).parent().parent().attr("key");
                value = jQuery(this).attr("value");
                data += '{"Key":"' + key + '","Value":"' + value + '"},';
            });
            data = data.substring(0, data.length - 1);
            data += "]'}";
            jQuery.ajax({
                type: "POST",
                url: "/Applications/API/UpdateMember.asmx/SaveMemberAttributes",
                contentType: "application/json; charset=utf-8",
                data: data,
                datatype: "json",
                timeout: 10000,
                success: function (msg) {
                    if (typeof (msg) == "object") {
                        jQuery("#leftbox-1").addClass("enabled");
                        jQuery("#leftbox-2").addClass("enabled");

                        //disable links
                        jQuery(".disabled-matches-btn").hide();
                        jQuery(".enabled-matches-btn").show();
                        jQuery(".success-message").css("visibility", "visible");
                        jQuery(".box-content").css({
                            opacity: 1
                        });

                        jQuery("#send-attr-btn").addClass("enabled");

                        if (jQuery('.fill-details-box #profile-overlay-content').length == 0 && jQuery('#one-page-reg-welcome-container #page-content').length == 0) {
                            setTimeout(function () { closeOverlay() }, 2000);
                            window.location.reload();
                        }

                        //track end editing attributes
                        if (!trackEndEditingAttributes) {
                            var s = s_gi(s_account); s.linkTrackVars = 'events';
                            s.linkTrackEvents = 'event58';
                            s.events = 'event58';
                            s.tl(true, 'o', 'Overlay complete profile - Complete: Type=Counter');
                            trackEndEditingAttributes = true;
                        }
                    }
                }
            });
        });
        isSaveDetailsBind = true;
    }
}
function coverBody() {
    if ($j.browser.msie && parseInt($j.browser.version) <= "8") {
        jQuery('.bg-cover-fill-details').css({ opacity: 0.5, backgroundColor: '#888888' });
    }
    else {
        jQuery('.bg-cover-fill-details').css({ opacity: 0.5, backgroundColor: '#000' });
    }

}
function showOverlayBox() {
    //if box is not set to open then don't do anything
    // set the properties of the overlay box, the left and top positions
    jQuery('.fill-details-box').has('#profile-overlay-content').css({
        background: 'none',
        width: 1000
    });
    topToBePositive = (jQuery(window).height() - jQuery('.fill-details-box').height()) / 2 - 100
    if (topToBePositive < 40) { topToBePositive = 40 }
    jQuery('.fill-details-box').css({
        display: 'block',
        left: (jQuery(window).width() - jQuery('.fill-details-box').width()) / 2,
        top: topToBePositive,
        position: 'absolute'
    });
    // set the window background for the overlay. i.e the body becomes darker
    jQuery('.bg-cover-fill-details').css({
        display: 'block',
        width: jQuery(document).width(),
        height: jQuery(document).height()
    });
}
function closeOverlay() {
    jQuery('.fill-details-box').css('display', 'none');
    // now animate the background to fade out to opacity 0
    // and then hide it after the animation is complete.
    jQuery('.bg-cover-fill-details').animate({ opacity: 0 }, null, null, function () { jQuery(this).hide(); });
}

/********* Quick Message ************/
var mainSubject;
var mainBody;
var defaultLoaderHtml;
var trackInitiateQuickMessage = new Object;
trackInitiateQuickMessage.openQuickMessage = [];
trackInitiateQuickMessage.startQuickMessage = [];
var quickMessage = [];
var alreadyOpen = new Object();
var mainTimer = [];
jQuery(document).ready(function () {
    mainSubject = jQuery(".quick-message-subject").val();
    mainBody = jQuery(".quick-message-body").val();
    defaultLoaderHtml = jQuery(".send-message-loading").html();
    jQuery(".close", ".quick-message").click(function () {
        jQuery(this).parent().slideUp();
    });

    //open/close quick message box
    jQuery(".open-quick-message").click(function () {
        var memberid = jQuery(this).attr("memberid");

        clearSubjectAndBody(memberid);

        quickMessage[memberid] = jQuery(".quick-message[memberid='" + memberid + "']");

        jQuery(quickMessage[memberid]).slideDown();
        jQuery(this).unbind('mouseout.myHover');

        //track initiate quick message
        if (trackInitiateQuickMessage.openQuickMessage[memberid] == undefined) {
            trackInitiateQuickMessage.openQuickMessage[memberid] = false;
        }
        if (trackInitiateQuickMessage.startQuickMessage[memberid] == undefined) {
            trackInitiateQuickMessage.startQuickMessage[memberid] = false;
        }
        if (trackInitiateQuickMessage.openQuickMessage[memberid] == false) {
            var s = s_gi(s_account); s.linkTrackVars = 'events';
            s.linkTrackEvents = 'event60';
            s.events = 'event60';
            s.tl(true, 'o', 'click to Initiate Quick Message -IL: Type=Counter');
            trackInitiateQuickMessage.openQuickMessage[memberid] = true;
            clearTracking(trackInitiateQuickMessage.startQuickMessage, memberid);
        }

    });
    jQuery(".open-quick-message").bind('mouseover.myHover', function () {
        var memberid = jQuery(this).attr("memberid");

        quickMessage[memberid] = jQuery(".quick-message[memberid='" + memberid + "']");
        if (!jQuery(quickMessage[memberid]).is(":visible")) {
            if (mainTimer[memberid]) {
                clearTimeout(mainTimer[memberid]);
                mainTimer[memberid] = null;
            }
            mainTimer[memberid] = setTimeout(function () {
                jQuery(quickMessage[memberid]).slideDown();
            }, 500);
            clearSubjectAndBody(memberid);
            jQuery(".open-quick-message[memberid='" + memberid + "']").bind('mouseout.myHover', function () {

                quickMessage[memberid] = jQuery(".quick-message[memberid='" + memberid + "']");
                if (mainTimer[memberid]) {
                    clearTimeout(mainTimer[memberid]);
                    mainTimer[memberid] = null;
                }
                mainTimer[memberid] = setTimeout(function () {
                    jQuery(quickMessage[memberid]).slideUp();
                }, 750);
            });
        }
    });


    //Mouse hover on quick message box
    jQuery(".quick-message").hover(function () {
        var memberid = jQuery(this).attr("memberid");

        if (mainTimer[memberid]) {
            clearTimeout(mainTimer[memberid]);
            mainTimer[memberid] = null;
        }

        jQuery(".open-quick-message[memberid='" + memberid + "']").unbind('mouseout.myHover');

    }, function () {
        var memberid = jQuery(this).attr("memberid");


        var subject = jQuery(".quick-message[memberid='" + memberid + "'] .quick-message-subject").val();
        var message = jQuery(".quick-message[memberid='" + memberid + "'] .quick-message-body").val();
        if (subject == mainSubject && message == mainBody) {
            if (mainTimer[memberid]) {
                clearTimeout(mainTimer[memberid]);
                mainTimer[memberid] = null;
            }
            mainTimer[memberid] = setTimeout(function () {
                jQuery(".quick-message[memberid='" + memberid + "']").slideUp();
            }, 750);
        }
    });



    //send message
    jQuery(".send-message").click(function () {
        if (!validateQuickMessageForm(this)) {
            jQuery(this).parent().hide();
            var memberid = jQuery(this).parent().attr("memberid");
            jQuery(".send-message-loading[memberid='" + memberid + "']").show();

            var subject = jQuery(".quick-message[memberid='" + memberid + "'] .quick-message-subject").val();
            var message = jQuery(".quick-message[memberid='" + memberid + "'] .quick-message-body").val();
            var data = '{"targetMemberID":"' + memberid + '","messageSubject":"' + subject + '","message":"' + message + '","profileType":"' + quickMessageProfileType + '"}';

            jQuery.ajax({
                type: "POST",
                url: "/Applications/API/IMailService.asmx/SendMessageLocale",
                contentType: "application/json; charset=utf-8",
                data: data,
                datatype: "json",
                timeout: 30000,
                success: function (msg) {
                    var s = s_gi(s_account); s.linkTrackVars = 'events';
                    s.linkTrackEvents = 'event11';
                    s.events = 'event11';
                    s.eVar26 = "imail";
                    s.tl();

                    clearTracking(trackInitiateQuickMessage.openQuickMessage, memberid);
                    clearTracking(trackInitiateQuickMessage.startQuickMessage, memberid);
                    if (typeof (msg) == "object") {
                        var resultStatus = (typeof msg.d == 'undefined') ? msg.Status : msg.d.Status;

                        if (resultStatus == 2) {
                            showSendMessage(msg.d.StatusMessage, memberid);
                        }
                        else if (resultStatus == 4) {
                            window.location = msg.d.SubPageURL;
                        }
                        else if (resultStatus == 5) {
                            //session expired
                            window.location.href = '/Applications/Logon/Logon.aspx?DestinationURL=' + encodeURIComponent(window.location.href.replace('http://' + window.location.host, ''));
                        }
                        else {
                            generalError();
                        }
                    }
                    else {
                        generalError();
                    }
                },
                error: function () {
                    generalError();
                }
            });

        }
    });


    //clear subject/body values
    jQuery(".quick-message-subject,.quick-message-body").bind("click focus", function () {
        jQuery(this).removeClass("error");
        var cls = jQuery(this).attr("class");
        if (jQuery(this).val() == mainSubject || jQuery(this).val() == mainBody) {
            jQuery(this).val("");
        }
        for (key in quickMessageValidator[cls]) {
            if (quickMessageValidator[cls][key].message == jQuery(this).val()) {
                jQuery(this).val("");
            }
        }


        jQuery(this).addClass("black");
        var memberid = jQuery(this).parent().parent().attr("memberid");
        //track start typing message
        if (trackInitiateQuickMessage.startQuickMessage[memberid] == undefined) {
            trackInitiateQuickMessage.startQuickMessage[memberid] = false;
        }
        if (trackInitiateQuickMessage.startQuickMessage[memberid] == false) {
            var s = s_gi(s_account); s.linkTrackVars = 'events';
            s.linkTrackEvents = 'event61';
            s.events = 'event61';
            s.tl(true, 'o', 'Quick Message –Start- IL : Type=Counter');
            trackInitiateQuickMessage.startQuickMessage[memberid] = true;
        }
    });
});

function showSendMessage(msg, memberid) {
    jQuery(".send-message-loading[memberid='" + memberid + "']").html(msg)
    timer = setTimeout(function () {
        jQuery(".send-message-loading[memberid='" + memberid + "']").fadeOut();
        timer2 = setTimeout(function () {
            jQuery(".send-message-loading[memberid='" + memberid + "']").html(defaultLoaderHtml);
        }, 5000);
    }, 5000);
}

function validateQuickMessageForm(elem) {
    var elementErrorFlag = false;
    jQuery(elem).parent().find("input,textarea").each(function () {
        if (elementErrorFlag) {
            return elementErrorFlag;
        }
        jQuery(this).removeClass("error");
        jQuery(this).removeClass("black");
        var cls = jQuery(this).attr("class");
        if (quickMessageValidator[cls]) {
            value = jQuery(this).val();
            for (key in quickMessageValidator[cls]) {
                switch (quickMessageValidator[cls][key].errorKey) {
                    case "required":

                        if (value == "" || value.length == 0 || value == " " || value == quickMessageValidator[cls][key].message) {
                            elementErrorFlag = true;
                            jQuery(this).val(quickMessageValidator[cls][key].message);
                            jQuery(this).addClass("error");
                        }
                        break;
                    case "noChange":
                        if (value == mainSubject || value == mainBody || value == quickMessageValidator[cls][key].message) {
                            elementErrorFlag = true;
                            jQuery(this).val(quickMessageValidator[cls][key].message);
                            jQuery(this).addClass("error");
                        }
                        break;
                }
                if (elementErrorFlag) {
                    break;
                }
            }
        }

    });

    return elementErrorFlag;
}

function clearSubjectAndBody(memberid) {
    var val = jQuery(".quick-message[memberid='" + memberid + "'] .quick-message-subject").val();
    if (val != mainSubject) {
        jQuery(".quick-message[memberid='" + memberid + "'] .quick-message-subject").val(mainSubject);
        jQuery(".quick-message[memberid='" + memberid + "'] .quick-message-subject").removeClass("black");
    }
    val = jQuery(".quick-message[memberid='" + memberid + "'] .quick-message-body").val();
    if (val != mainBody) {
        jQuery(".quick-message[memberid='" + memberid + "'] .quick-message-body").val(mainBody);
        jQuery(".quick-message[memberid='" + memberid + "'] .quick-message-body").removeClass("black");
    }
}

function generalError() {
    jQuery(".send-message-loading").addClass("error");
    jQuery(".send-message-loading").html(quickMessageGeneralError);
    timer = setTimeout(function () {
        jQuery(".send-message-loading").fadeOut();
        timer2 = setTimeout(function () {
            jQuery(".send-message-loading").html(defaultLoaderHtml);
        }, 5000);
    }, 5000);
}

//clear trackong flag
function clearTracking(obj, memberid) {
    obj[memberid] = false;
}
/******* Bubble popup ********/
$j(function () {
    $j('.bubble-area').each(function () {
        // options
        var distance = 10;
        var time = 300;
        var hideDelay = 500;

        var hideDelayTimer = null;
        var triggerOnClick = $j(this).hasClass('trigger-on-click');

        // tracker
        var beingShown = false;
        var shown = false;

        var trigger = $j('.trigger', this);
        var popup = $j('.bubble-layer', this).css('display', 'none');

        if (triggerOnClick === true) {
            popup.append('<span class="spr s-icon-close"><span></span></span>');
            popup.children('.s-icon-close', this).click(function () {
                trigger.trigger('click');
            })
            trigger.toggle(function () {
                $j(this).closest('.profile30-comm').addClass('poped');
                popup.css({
                    bottom: 25,
                    left: -100,
                    display: 'block'
                });
            }, function () {
                $j(this).closest('.profile30-comm').removeClass('poped');
                popup.css('display', 'none');
            });
        }
        if (triggerOnClick === false) {
            // set the mouseover and mouseout on both element
            $j([trigger.get(0), popup.get(0)]).hoverIntent(function () {
                // stops the hide event if we move from the trigger to the popup element
                if (hideDelayTimer) clearTimeout(hideDelayTimer);

                // don't trigger the animation again if we're being shown, or already visible
                if (beingShown || shown) {
                    return;
                } else {
                    beingShown = true;

                    // reset position of popup box
                    popup.css({
                        bottom: 50,
                        left: -100,
                        display: 'block' // brings the popup back in to view
                    })

                    // (we're using chaining on the popup) now animate it's opacity and position
        .animate({
            bottom: '-=' + distance + 'px',
            opacity: 1
        }, time, 'swing', function () {
            // once the animation is complete, set the tracker variables
            beingShown = false;
            shown = true;
        });
                }
            },
	function () {
	    // reset the timer if we get fired again - avoids double animations
	    if (hideDelayTimer) clearTimeout(hideDelayTimer);

	    // store the timer so that it can be cleared in the mouseover if required
	    hideDelayTimer = setTimeout(function () {
	        hideDelayTimer = null;
	        popup.animate({
	            bottom: '-=' + distance + 'px',
	            opacity: 0
	        }, time, 'swing', function () {
	            // once the animate is complete, set the tracker variables
	            shown = false;
	            // hide the popup entirely after the effect (opacity alone doesn't do the job)
	            popup.css('display', 'none');
	        });
	    }, hideDelay);
	});
        }

    });
});

/* Cluetip */
$j(function () {
    $j('.cluetip.click > a').cluetip({
        activation: 'click',
        cluezIndex: 1000,
        cursor: 'pointer',
        dropShadow: false,
        local: true,
        showTitle: false,
        sticky: true,
        waitImage: false,
        onShow: function () {
            $j('.cluetip-close a').click(function (e) { e.preventDefault(); $j('#cluetip-close').triggerHandler('click'); })
        }

    });
});


function deviceIsiPhone() {
    if (navigator.userAgent.match(/iPod/i) || navigator.userAgent.match(/iPhone/i)) {
        return true;
    }
    return false;
}

/******* Prompt for Switching to Mobile ********/
if (screen.width <= 699 || deviceIsiPhone() || navigator.userAgent.match(/Android/i)) {

    function inString(pages, value) {
        var result = false;
        if (typeof value === 'string') {
            value = value.split();
        }

        for (var i = '0'; i < value.length; i++) {
            if (pages.indexOf(value[i]) != -1) {
                result = true;
            }
        }
        return result;
    }

    function addMobileHeaderLink(link, copy) {
        var linkToMobile = $j('<a id="linkToMobile">');
        var offset = 0;

        linkToMobile.attr('href', link).text(copy).prependTo('body');
        $j('body').css('background-position', '0 ' + linkToMobile.outerHeight() + 'px');
        $j('#site-container').css('margin-top', linkToMobile.outerHeight() - offset);
        /*window.onscroll = function () {
        document.getElementById('linkToMobile').style.top = (window.pageYOffset) + 'px';
        };*/
    }

    function addMobileFooterLinks(links) {
        var mobileLinksUl = '<ul class="nav-footer" id="footer-mobile-nav">\n';
        for (var i = 0, len = links.length; i < len; i++) {
            var linkData = links[i];
            var htmlLink = '<li><a href="' + linkData.url + '"';
            if (linkData.target) {
                htmlLink += ' target="' + linkData.target + '"';
            }
            htmlLink += '>' + linkData.text + '</a>';
            if (i + 1 < len) {
                htmlLink += ' | ';
            }
            mobileLinksUl += htmlLink;
        }
        mobileLinksUl += '</ul>\n';
        $j("#footer > div").prepend(mobileLinksUl);
    }

    function MakeMobileVisitToFWSTemporary() {
        // for existing cookies with expiration date of a year from session, clear expire date, so cookie becomes session only
        // this code can be removed after March 2013 when all cookies will have been cleared out
        // starting march 2012, no more year-long cookies were set, only session cookies
        var cookieName = 'sofs'; // stay on full site (from mobile device)
        var sofsValue = spark_getCookie(cookieName);
        if (sofsValue) {
            var site = $j('meta[name="author"]').attr('content') || "";
            site = site.toLowerCase();
            var siteDomain = site.replace("http://www.", "");
            document.cookie = cookieName + '=' + escape(sofsValue) + ';expires=;path=/;domain=.' + siteDomain;
        }
    }

    jQuery(document).ready(function () {
        var site = $j('meta[name="author"]').attr('content') || "";
        site = site.toLowerCase();
        var siteDomain = site.replace("http://www.", "");
        if (site == 'http://www.jdate.com' || site == 'http://www.jdate.co.uk') {

            var pages = document.getElementsByTagName('body')[0].className;
            var pageTest = inString(pages, ['sub-page-default', 'page-logon', 'sub-page-splash20', 'page-home']);
            var onSplashOrLoginPage = inString(pages, ['sub-page-splash20', 'page-logon']);
            if (pageTest) { //see if the page is sub-page page logon or sub-page splash
                MakeMobileVisitToFWSTemporary();
                if (onSplashOrLoginPage) { // make sure it's not the home page and show the nav
                    addMobileHeaderLink('http://m.' + siteDomain + '/home', 'Switch to Mobile Site');
                }
            }
            var links = [
			{
			    'text': 'Back to Mobile Site',
			    'url': 'http://m.' + siteDomain + '/home'
			}];

            if (site == 'http://www.jdate.com') {
                links.push({
                    'text': 'JDate Mobile',
                    'url': 'http://www.jdatemobile.com'
                });

                if (deviceIsiPhone()) {
                    links.push({
                        'text': 'Download JDate iPhone App',
                        'url': 'http://itunes.apple.com/us/app/jdate/id484715536?mt=8&uo=4',
                        'target': 'itunes_store'
                    });
                }
            }

            addMobileFooterLinks(links);

        } else {
            return;
        }



    });
}

function spark_launchTeaseDialog(obj, title, close) {
    var href = obj.href;

    if ($j('#flirtContent').length === 0) {
        $j('<div id="flirtContent" class="hide"></div>').appendTo('body');
    }

    $j('#flirtContent')
        .dialog({
            width: 590,
            modal: true,
            title: title,
            position: ['center', 'top'],
            dialogClass: 'ui-dialog-rev modal-flirts',
            minHeight: 400,
            open: function () {
                var $this = $j(this),
                    ajax_load = '<div class="loading spinner-only" />',
                    reversedClose = $j('<div class="ui-dialog-titlebar-close-rev link-style">' + close + ' <span class="spr s-icon-closethick-color"></span></div>').bind('click', function () {
                        $j('#flirtContent').dialog('close');
                    });

                $this.html(ajax_load).load(href, function () {
                    $j(this).find('.category:eq(0)').addClass('open');
                });

                $this.parent().find('.ui-dialog-titlebar-close').replaceWith(reversedClose);
                $j('.ui-widget-overlay').bind('click', function () {
                    $j('#flirtContent').dialog('close');
                });
            }
        })
        .parent('.ui-dialog').css('top', function () {
            var x = $j(this).position();
            return x.top + 12;
        });

    //return false;
}

$j(document).ready(function () {

    //smiles toggler
    var $smiles = $j('#smiles');

    //$smiles.find('ul li:first-child a').addClass('open');
    $smiles.find(".category").live('click', function (event) {
        event.preventDefault();
        if ($j(this).siblings().is(':hidden')) {
            $j(this).addClass('open');
            $j(this).siblings().show();
            $j('.ui-widget-overlay').css({ 'height': $j(document).height() });
        } else {
            $j(this).removeClass('open');
            $j(this).siblings().hide();
            // uncomment to shrink the modal height when closing 
            // causes a slight jump in certain situations. perhaps it is better to just let the page get long
            //$j('.ui-widget-overlay').css({'height': 'auto'}).css({'height':$j(document).height()});
        }
    });

    //    var labelText = '';
    //    $smiles.find('label').each(function() {
    //        var labelArray = $j(this).text().split(' '),
    //        labelLast = labelArray.pop();
    //        
    //        labelText = labelArray.join(' ') + '&nbsp;' + labelLast;
    //        $j(this).html(labelText);
    //    });
});

//replace image when hover on date online
$j(document).ready(function () {
    $j('.results30-profile.gallery-view-30 .hover-menu li.action-omnidate, .results30-profile.list-view-30 .profile-body li.action-omnidate').hover(function () {
        var newSrc = $j(this).find('.item.omnidate a img').attr('src');
        newSrc = newSrc.replace('30-s', '30-h-s');
        $j(this).find('.item.omnidate a img').attr('src', newSrc);
    }, function () {
        var newSrc = $j(this).find('.item.omnidate a img').attr('src');
        newSrc = newSrc.replace('30-h-s', '30-s');
        $j(this).find('.item.omnidate a img').attr('src', newSrc);
    });
});

// Subscription Expiration Alert
function displayAnnouncementLoader(show) {
    if (show) {
        jQuery('.content-announcement .announcement-loader').css({
            height: jQuery('.content-announcement .welcome-box').height() + 8
        }).show()
    }
    else {
        jQuery('.content-announcement .announcement-loader').hide();
    }
}

function announcementDetails() {
    jQuery('.content-announcement > div').hide();
    jQuery('.content-announcement .thankyou-box').show();
}

function announcementError() {
    jQuery('.content-announcement > div').hide();
    jQuery('.content-announcement .error-box').show();
}

jQuery(document).ready(function () {
    jQuery('.content-announcement a.click-close').click(function (e) {
        e.preventDefault();
        jQuery(this).parents('.content-announcement').slideUp();
    });
});

function CreateSubAlertCookie(expirehours) {
    var cookiename = 'subexpalert';
    var value = 'true';
    var date = new Date();
    date.setTime(date.getTime() + (expirehours * 60 * 60 * 1000));
    var expires = "; expires=" + date.toGMTString();
    document.cookie = cookiename + "=" + value + expires + "; path=/";
}

jQuery(".content-announcement .click-close").live('click', function (e) {
    e.preventDefault();
    var alertdata = jQuery(this).closest('[data-expires]').data();
    //Create a cookie that expires in few hours. Do not show alert box until this cookie expires.
    //If expirehours is equal to 0, this means the alert is shown per session. Otherwise, the alert is shown every x hours.
    if (alertdata.expires > 0) {
        CreateSubAlertCookie(alertdata.expires);
    }
});

jQuery(".content-announcement .btn-renewal a").live('click', function (e) {
    e.preventDefault();
    var memberid = spark.favorites.MemberId;
    //var memberid = -2012;
    var data = '{"memberID":"' + memberid + '"}';

    jQuery.ajax({
        type: "POST",
        url: "/Applications/API/AutoRenewal.asmx/EnableAutoRenewal?callback=?",
        contentType: "application/json; charset=utf-8",
        data: data,
        datatype: "json",
        timeout: 30000,
        beforeSend: function () {
            displayAnnouncementLoader(true);
        },
        success: function (msg) {

            // console.log(msg.d);
            displayAnnouncementLoader(false);
            if (typeof (msg) == "object") {
                if (msg.d.Status == 2) {
                    s.prop75 = 'autorenew_on';
                    s.t();
                    jQuery('.content-announcement .renewal-amount').text(msg.d.RenewalAmount);
                    jQuery('.content-announcement .renewal-date').text(msg.d.NextRenewalDate);
                    jQuery('.content-announcement .renewal-total').text(msg.d.TotalAmount);
                    if (typeof (msg.d.PremiumServicesList) == "object" && msg.d.PremiumServicesList != null) {
                        jQuery.each(msg.d.PremiumServicesList, function (i, item) {
                            jQuery('<span>' + msg.d.PremiumServicesList[i].ServiceName + ': $' + msg.d.PremiumServicesList[i].RenewalAmount + '</span><br />').appendTo('.content-announcement .renewal-services');
                        });
                    }
                    announcementDetails();
                }
                else if (msg.d.Status == 3) {
                    announcementError();
                }
                else {
                    generalError();
                }
            }
            else {
                generalError();
            }
        },
        error: function () {
            displayAnnouncementLoader(false);
            generalError();
        }
    });
});

// show alert when message is empty on message reply
jQuery(document).ready(function () {
    if (jQuery('.reply-box.compose-email .compose-email-content').length > 0) {
        var textMarkText = jQuery('.compose-email-content textarea').val();
        jQuery('.compose-email-content input.btn.primary').live('click', function (e) {
            var textMessageArea = jQuery('.compose-email-content textarea');
            if (textMessageArea.val() == textMarkText || jQuery.trim(textMessageArea.val()) == '') {
                e.preventDefault();
                jQuery('#emailEmpty').css({
                    width: textMessageArea.outerWidth(),
                    height: textMessageArea.outerHeight() / 2 + 10,
                    paddingTop: textMessageArea.outerHeight() / 2 - 10,
                    display: 'block'
                }).delay('2000').fadeOut('slow')
            }
        });
    }
});

//set offset cookie if not already present
$j(document).ready(function () {
    var cookieName = 'tzo'; // stay on full site (from mobile device)
    var sofsValue = spark_getCookie(cookieName);
    if (!sofsValue) {
        var site = $j('meta[name="author"]').attr('content') || "";
        site = site.toLowerCase();
        var domain = site.replace("http://www.", "");
        var expires = new Date(new Date().getTime() + 24 * 60 * 60 * 1000);
        spark_setCookie('tzo', getTimeZoneOffset(), expires, "/", domain, false);
    }
});

function getTimeZoneOffset() {
    var rightNow = new Date();
    var temp = rightNow.toGMTString();
    var offsetRightNow = new Date(temp.substring(0, temp.lastIndexOf(" ") - 1));
    var std_time_offset = (rightNow - offsetRightNow) / (1000 * 60 * 60);

    return Math.round(std_time_offset);
}

// JMeter V3
function displayBoxLoader(show, boxType) {
    if (show) {
        boxType.find('.jmeter-box-matches-profiles-run').css({
            opacity: 0.1
        });
        boxType.find('.jmeter-box-matches-loader').show()
    }
    else {
        boxType.find('.jmeter-box-matches-loader').hide();
        boxType.find('.jmeter-box-matches-profiles-run').css({
            opacity: 1
        });
    }
}

function moveBoxStart(boxType) {
    boxType.find('.jmeter-box-arrow-button').addClass('disable');
    var pr = boxType.find('.jmeter-box-matches-profiles-run');
    pr.animate({
        left: 0
    }, 600, function () {
        activateBoxButton('right', true, boxType);
        activateBoxButton('left', false, boxType);
    });
}

function addBoxContainer(boxType, callFirst) {
    if (callFirst) {
        boxType.find('.jmeter-box-matches-profiles-run .mb-container').removeClass('last current');
        boxType.find('.jmeter-box-matches-profiles-run .mb-container:first-child').addClass('current');
    } else {
        boxType.find('.jmeter-box-matches-profiles-run .mb-container.last').removeClass('last');
    }
    boxType.find('.jmeter-box-matches-profiles-run').append('<div class="mb-container last"></div>');
}

function showNoResultsBox(show, boxType) {
    if (show) {
        displayBoxLoader(false, boxType);
        boxType.find('.jmeter-box-matches-profiles .jmeter-box-error').show();
    }
    else {
        boxType.find('.jmeter-box-matches-profiles .jmeter-box-error').hide();
    }
}

function activateBoxButton(direction, active, boxType) {
    if (active) {
        boxType.find('.jmeter-box-arrow-button#matches-' + direction).removeClass('disable');
    }
    else {
        boxType.find('.jmeter-box-arrow-button#matches-' + direction).addClass('disable');
    }
}

function moveBoxRight(boxType) {
    var pr = boxType.find('.jmeter-box-matches-profiles-run');
    boxType.find('.jmeter-box-matches-profiles-run .mb-container.current').removeClass('current').next().addClass('current');
    pr.animate({
        left: (parseFloat(pr.css('left')) - 228) + 'px'
    }, 600, function () {
        activateBoxButton('right', true, boxType);
        if (parseInt(pr.css('left')) < 0) {
            activateBoxButton('left', true, boxType);
            if (boxType.find('.jmeter-box-matches-profiles-run .mb-container.current.last').length == 0) { activateBoxButton('right', true, boxType); }
        }
    });
}

function moveBoxLeft(boxType) {
    var pr = boxType.find('.jmeter-box-matches-profiles-run');
    pr.animate({
        left: (parseFloat(pr.css('left')) + 228) + 'px'
    }, 600, function () {
        activateBoxButton('left', true, boxType);
    });
}

function getOldMatchesProfiles(boxType) {
    moveBoxRight(boxType);
}

function getNewMatchesProfiles(isFirst, countCalls, boxType) {
    //alert('countCalls: ' + countCalls + '|' + jQuery('.jmeter-box-matches-profiles-run .mb-container'));
    if (boxType.is('.matches')) {
        var profilesCount = 8;
        var useAPI = 'GetJMeterMatches';
    } else {
        var profilesCount = 12;
        var useAPI = 'GetJMeterMOL';
    }
    showNoResultsBox(false, boxType);
    var isNewProfilesAjax = false;
    var profilesStartFrom = boxType.find('.jmeter-box-matches-profiles-run .mb-profile').length + 1;
    if (boxType.find('.jmeter-box-matches-profiles-run .mb-container').eq(-3).is('.current') || countCalls > 1 || isFirst || !boxType.find('.jmeter-box-matches-profiles-run.scroll-end')) {
        isNewProfilesAjax = true;
    }
    if (isFirst) {
        profilesCount = profilesCount * 3;
        var isRunning = false;
        var profilesStartFrom = 1;
    }
    if (isNewProfilesAjax) {
        var data = '{"startRow":"' + profilesStartFrom + '", "pageSize":"' + profilesCount + '"}';
        jQuery.ajax({
            type: "POST",
            url: "/Applications/API/SearchResults.asmx/" + useAPI,
            contentType: "application/json; charset=utf-8",
            data: data,
            datatype: "json",
            timeout: 30000,
            beforeSend: function () {
                if (isFirst) {
                    displayBoxLoader(true, boxType);
                } else {
                    boxType.find('.jmeter-box-matches-profiles-run .mb-container.current').removeClass('current').prev().addClass('current');
                    moveBoxLeft(boxType);
                }
            },
            success: function (msg) {
                if (typeof (msg) == "object") {
                    if (msg.d.Status == 2) {
                        if (typeof (msg.d.ProfilesList) == "object" && msg.d.ProfilesList != null && !jQuery.isEmptyObject(msg.d.ProfilesList) && jQuery.trim(msg.d.ProfilesList) != '') {
                            addBoxContainer(boxType, false);
                            var increaseContainerWidth = 228;
                            jQuery.each(msg.d.ProfilesList, function (i, item) {
                                if (isFirst && i < profilesCount && i > 0 && i % (profilesCount / 3) == 0) { addBoxContainer(boxType, true) }
                                boxType.find('.mb-container.last').append('<div class="mb-profile"><a href="' + msg.d.ProfilesList[i].Link + '"><img alt="UserName" src="' + msg.d.ProfilesList[i].ThumbnailURL + '" /></a></div>');
                            });
                            if (isFirst) {
                                activateBoxButton('right', true, boxType);
                                increaseContainerWidth *= 2;
                            }
                            boxType.find('.jmeter-box-matches-profiles-run').width(function (i, width) {
                                return width + increaseContainerWidth;
                            });
                            isRunning = true;
                            displayBoxLoader(false, boxType);
                        } else {
                            boxType.find('.jmeter-box-matches-profiles-run').addClass('scroll-end');
                        }
                    }
                    else if (msg.d.Status == 3) {
                        if (isRunning) {
                            moveBoxLeft(boxType);
                            activateBoxButton('right', false, boxType);
                            isRunning = false;
                        }
                        else {
                            if (countCalls >= 5) {
                                if (!isFirst) {
                                    moveBoxStart(boxType);
                                    showNoResultsBox(true, boxType);
                                } else {
                                    showNoResultsBox(true, boxType);
                                }
                            }
                            else {
                                countCalls++;
                                //setTimeout(function () { getNewMatchesProfiles(isFirst, countCalls, boxType) }, 2000);
                            }
                        }
                    }
                    else {
                        showNoResultsBox(true, boxType);
                    }
                }
                else {
                    showNoResultsBox(true, boxType);
                }
            },
            error: function () {
                showNoResultsBox(true, boxType);
            }
        });
    }
    else {
        boxType.find('.jmeter-box-matches-profiles-run .mb-container.current').removeClass('current').prev().addClass('current');
        moveBoxLeft(boxType);
    }
}

function increasePercentage(perc) {
    jQuery('.bar').show()
    var clockBarPerc = jQuery('.bar').attr('src').replace('.png', '');
    var pos = clockBarPerc.lastIndexOf('/');
    newClockBarPerc = parseInt(clockBarPerc.substring(pos + 1));
    if (newClockBarPerc == perc) {
        return false;
    } else {
        jQuery('.bar').attr('src', clockBarPerc.substring(0, pos + 1) + '' + parseInt(newClockBarPerc + 1) + '.png');
    }
}

function preloadClock() {
    jQuery('.bar').attr('src', '/img/site/jdate-co-il/clock/1.png').hide(); ;
    if (jQuery('.lte8').length == 0) {
        jQuery('.pan').css({ rotate: -2.25 });
        for (i = 0; i <= 7; i++) {
            jQuery('<img>').attr('src', '/img/site/jdate-co-il/clock/' + i + '.png').appendTo('body').hide();
            if (i == 7) {
                jQuery('.jmeter-clock > .loader').delay(3000).fadeOut(1500, function () {
                    runClock();
                });
            }
        }
    } else {
        jQuery('.jmeter-clock > .loader').fadeOut(1500);
        runClock();
    }
}

function runClock() {
    var percToPan = jQuery('.jmeter-clock > input').val();
    var valueRatio = 100 / 7;
    percToPan = percToPan / valueRatio;
    if (jQuery('.jmeter-clock').is('.personal, .mini')) {
        percToPan = jQuery('.jmeter-clock > input').val();
    }
    var percToBar = parseInt(Math.round(percToPan));
    if (percToBar == 1) { rotateRadius = -1.64; }
    if (percToBar == 2) { rotateRadius = -1; }
    if (percToBar == 3) { rotateRadius = -0.4; }
    if (percToBar == 4) { rotateRadius = 0.4; }
    if (percToBar == 5) { rotateRadius = 1; }
    if (percToBar == 6) { rotateRadius = 1.64; }
    if (percToBar == 7) { rotateRadius = 2.25; }
    /*
    var maxRadius = 4.5;
    var rotateRadius = parseFloat((maxRadius * percToPan) / 100);
    if (rotateRadius > maxRadius / 2) {
    var fixPositive = ((((100 - percToPan) / 100) * 40) / 100)
    rotateRadius = (rotateRadius - (maxRadius / 2)) + fixPositive;
    } else {
    if (rotateRadius == maxRadius / 2) {
    rotateRadius = 0
    } else {
    rotateRadius = -((maxRadius / 2 - rotateRadius) + ((rotateRadius * 10) / 100))
    }
    }
    */
    var clockInt = setInterval(function () { increasePercentage(percToBar) }, 100);
    var panSpeed = 3000 / percToBar;
    if (jQuery('.lte8').length == 0) {
        jQuery('.pan').animate({ rotate: rotateRadius }, panSpeed, 'linear');
    } else {
        jQuery('.pan').delay(2000).css({ rotate: rotateRadius }, panSpeed, 'linear');
    }
}

function slideShowMad() {
    var scoreIncrease;
    jQuery('.jmeter-mad').each(function () {
        scoreIncrease = 46.4285;
        if (jQuery(this).is('.small')) { scoreIncrease = 14.14285 }
        if (jQuery(this).is('.medium')) { scoreIncrease = 16.41428 }
        if (jQuery('#tab-JMeter').length > 0) { scoreIncrease = 9.85714 }
        var thisScore = jQuery(this).find('input').val();
        var madToPixels = Math.round(parseInt(thisScore) * scoreIncrease);
        jQuery(this).find('.mad-main').animate({
            width: madToPixels
        }, 3000);
    });
}

function placeMatchOnboxes() {
    jQuery('.details-boxes').each(function () {
        var user_score = jQuery(this).find('input.user-score').val();
        var userX = user_score.substring(0, user_score.indexOf('.')) * 2;
        var userY = 200 - (user_score.substring(user_score.indexOf('.') + 1, user_score.length) * 2);

        jQuery(this).find('img.dot-user').css({
            top: userY,
            left: userX
        });
    });
}

function jmeterDimensions() {
    if (jQuery('#jmeter-side').height() < jQuery('.jmeter-content').outerHeight()) {
        jQuery('#jmeter-side').css({ height: jQuery('.jmeter-content').outerHeight() });
    } else {
        jQuery('.jmeter-content').css({ height: jQuery('#jmeter-side').height() });
    }
}

jQuery(document).ready(function () {
    // JMeter left boxes
    jQuery('.jmeter-left-box .jmeter-box-arrow-button').addClass('disable');

    jQuery('.jmeter-left-box').each(function () {
        var boxHeight = jQuery(this).find('.jmeter-box-matches-profiles').height();
        jQuery(this).find('.jmeter-box-matches-loader, .jmeter-box-error').css({
            height: boxHeight / 2 + 10,
            paddingTop: boxHeight / 2 - 20
        });
        getNewMatchesProfiles(true, 1, jQuery(this));
    });

    jQuery(document).delegate(".jmeter-box-arrow-button:not(.disable)", "click", function () {
        jQuery(this).parent().find('.jmeter-box-arrow-button').addClass('disable');
        if (jQuery(this).attr('id') == 'matches-left') {
            getNewMatchesProfiles(false, 1, jQuery(this).parent());
        } else {
            getOldMatchesProfiles(jQuery(this).parent());
        }
    });

    // JMeter invatition box
    var inviteEmailInput = jQuery('#jmeter-side input.invite-email');
    var inviteEmailText = inviteEmailInput.val();
    inviteEmailInput.live('focus', function () {
        if (jQuery(this).val() == inviteEmailText) { jQuery(this).val('') }
    });
    inviteEmailInput.live('focusout', function () {
        if (jQuery(this).val() == '') { jQuery(this).val(inviteEmailText) }
    });

    //JMeter tab adjustments
    jQuery('.tab-nav > li').live('click', function () {
        if (parseInt(jQuery('.jmeter-tab-main').height()) > 10) {
            jQuery('#tab-JMeter .jmeter-tab-left').css({ height: jQuery('.jmeter-tab-main').height() });
            jQuery('#tab-JMeter .jmeter-clock .loader').delay(2000).fadeOut(1500);
        }
    });

    //JMeter clock
    if (jQuery('.jmeter-clock').length > 0) {
        preloadClock();
    }

    //JMeter MAD
    if (jQuery('.jmeter-mad').length > 0) {
        slideShowMad();
    }

    if (jQuery('.details-boxes').length > 0) {
        placeMatchOnboxes();
    }

    //1 on 1 long names fix
    jQuery('.jmeter-1on1-box-profile .title').each(function () {
        newValue = jQuery(this).text().replace(/\s+/g, " ").replace(/(\r\n|\n|\r)/gm, " ");
        if (newValue.length > 15) {
            jQuery(this).text(newValue.substring(0, 13) + '...');
        }
    });

    //JMeter open review details
    jQuery('.category-button').toggle(function () {
        jQuery(this).parents('.jmeter-review-category').find('.review-details').show();
        jQuery(this).find('span').toggle();
        jQuery('#jmeter-side').css({
            height: jQuery('#jmeter-main').height()
        });
    }, function () {
        jQuery(this).parents('.jmeter-review-category').find('.review-details').hide();
        jQuery(this).find('span').toggle();
        jQuery('#jmeter-side').css({
            height: jQuery('#jmeter-main').height()
        });
    });

    //JMeter matches preferences
    jQuery('.JMselect').each(function () {
        jQuery(this).text(jQuery(this).prev('select').find(':selected').text());
    });
    jQuery('#sort-online-now .floatInside select').change(function () {
        jQuery(this).next('.JMselect').text(jQuery(this).find(':selected').text());
    });

    //JMeter collapse banner place holder
    if (jQuery('#jmeter-side .jmater-side-content .adunit > div').length == 0) {
        jQuery('#jmeter-side .jmater-side-content .adunit').hide();
    }

    jmeterDimensions();
});

/*
* Live Person
*/
$j(function () {
    jQuery(".lpchat-button, .lpchat-container").live('click', function (event) {
        PopulateS(true); //clear existing values in omniture "s" object

        s.eVar27 = "LivePersonChatSession - (" + s.pageName + ")";
        s.pageName = "LivePersonChatSession";
        s.events = "event2";
        s.eVar2 = "LivePersonChatSession";
        s.eVar10 = gLivepersonUserStatus;
        s.eVar15 = gLivepersonV15;
        s.eVar36 = gLivepersonV36;
        s.prop10 = window.location.pathname;
        s.prop17 = gLivepersonC17;
        s.prop18 = gLivepersonC18;
        s.prop19 = gLivepersonC19;
        s.prop23 = spark.favorites.MemberId;

        s.t(); //send omniture updated values as page load
        s.events = '';

        return false;
    });

    //Top Navigation - Live Person button
    if (jQuery("ul#nav-auxiliary .live-person-topnav-wrap").length > 0) {
        //if the button exists, display it and it's link in the top aux menu.
        jQuery("ul#nav-auxiliary li.live-person-topnav-wrap").show().css({ 'width': 'auto', 'height': 'auto' });
        jQuery("ul#nav-auxiliary .live-person-topnav-btn").show().css({ 'width': '75px', 'height': '15px', 'margin': '-14px 10px 0 0px' });
    }
});

/*
* New YNM
*/
$j(function () {
    //apply it when new YNM is ready. currently BBW, BS & SP are not ready ['1001', '90410', '90510']
    var isNewYNMReday = $j.inArray(spark.favorites.BrandId, ['1003', '1004', '1105', '1107']) > -1;

    if (isNewYNMReday) {
        SecretAdmirer_UiUpdateYNM($j(".click20-horizontal, [id$='secretAdmirerPopup']"));
    }
});