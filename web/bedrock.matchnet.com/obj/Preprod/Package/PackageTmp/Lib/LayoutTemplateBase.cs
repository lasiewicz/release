#region System References
using System;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
#endregion

#region MN web app references
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Enumerations;
using Matchnet.Web.Framework.Ui;
using Matchnet.Web.Applications.Subscription;
#endregion

#region MN Service references
using Matchnet.Session.ValueObjects;
using Matchnet.Web.Framework.Ui.Advertising;
using Matchnet.Configuration.ServiceAdapters;
#endregion

namespace Matchnet.Web.Lib
{
    public class LayoutTemplateBase : FrameworkControl
    {
        #region Member Variables
        private FrameworkControl _appControl;
        protected PlaceHolder AppControlPlaceholder;
        protected PlaceHolder plcSubscribeBanner;
        private PlaceHolder _notificationControlPlaceholder;
        protected Literal litDirection;
        protected Footer20 Footer20;
        protected PlaceHolder plcVerticalLine1;
        protected PlaceHolder plcVerticalLine2;
        protected PlaceHolder phSubNowTopBanner;
        protected PlaceHolder phSubNowBannerContainer;
        #endregion

        #region Private Methods
        private void Page_Init(object sender, System.EventArgs e)
        {
            LoadBanner();

            // Add the application control containing the actual content.
            AppControlPlaceholder.Controls.Add(_appControl);

            // referenced in spark.js - lucena
            Page.RegisterHiddenField("js_ynmConfirmText", g.GetResource("TXT_ARE_YOU_SURE_YOU_WANT_TO_CHANGE", null));

            // Set up the text direction (ltr or rtl)
            if (litDirection != null)
            {
                litDirection.Text = Direction;
            }

            if (Footer20 != null)
            {
                g.FooterControl20 = Footer20;
            }
        }
        protected override void OnPreRender(EventArgs e)
        {
            if (g.Head20 != null && !g.Head20.IsBlank)
            {
                //Add javascript to force a particular layout template to be appended to all link URLs
                Page.RegisterClientScriptBlock("LayoutScript", "<script>$j(document).ready(function(){delegateClickEvents()});</script>");
            }

            base.OnPreRender(e);
        }
        protected virtual void LoadBanner()
        {
            // Display the bannercontrol if the user is logged in
            if (g.Member != null && !g.HideSubNowBanner)
            {
                if (FrameworkGlobals.DisplayBannerArea(g))
                {
                    if (!MemberPrivilegeAttr.IsCureentSubscribedMember(g)
                        || SubscriptionHelper.IsPaymentExpiring(g))
                    {
                        Matchnet.Web.Framework.Banner.BannerControl banner = (Matchnet.Web.Framework.Banner.BannerControl)this.LoadControl("/Framework/Banner/BannerControl.ascx");
                        if (plcSubscribeBanner != null)
                        {
                            plcSubscribeBanner.Controls.Add(banner);
                        }

                        //Display new top large subscribe now banner
                        if (phSubNowBannerContainer != null && phSubNowTopBanner != null)
                        {
                            if (FrameworkGlobals.DisplayTopLargeSubscribeBanner(g))
                            {
                                bool subnow_gam_ad_enabled = false;
                                try
                                {
                                    subnow_gam_ad_enabled =
                                        Convert.ToBoolean(
                                            (RuntimeSettings.GetSetting("ENABLE_SUBNOW_AD",
                                                                        g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID,
                                                                        g.Brand.BrandID)));
                                }
                                catch (Exception ex)
                                {
                                    subnow_gam_ad_enabled = false;
                                    g.ProcessException(ex);
                                }

                                if (g.AppPage.App.ID == (int)WebConstants.APP.Home && subnow_gam_ad_enabled)
                                {
                                    AdUnit adUnit = LoadControl("/Framework/UI/Advertising/AdUnit.ascx") as AdUnit;
                                    adUnit.Size = AdUnit.AdSize.LargeTopSubBanner;
                                    adUnit.GAMAdSlot = "home_subnow_top_990x40";
                                    adUnit.GAMIframe = false;
                                    adUnit.Disabled = false;
                                    phSubNowTopBanner.Controls.Add(adUnit);
                                    phSubNowBannerContainer.Visible = true;
                                }
                                else
                                {
                                    AdUnit adUnit = LoadControl("/Framework/UI/Advertising/AdUnit.ascx") as AdUnit;
                                    adUnit.Size = AdUnit.AdSize.LargeTopSubBanner;
                                    adUnit.GAMAdSlot = "subnow_top_860x40";
                                    adUnit.GAMIframe = false;
                                    adUnit.Disabled = false;
                                    phSubNowTopBanner.Controls.Add(adUnit);
                                    phSubNowBannerContainer.Visible = true;
                                }

                            }
                        }
                    }
                }
                else
                {
                    if (plcSubscribeBanner != null)
                    {
                        plcSubscribeBanner.Visible = false;
                    }
                    if (phSubNowBannerContainer != null)
                    {
                        phSubNowBannerContainer.Visible = false;
                    }
                }
            }
        }


        /// <summary>
        /// Don't like this method If you don't want to show controls on an admin page, use a different template for christ's sake!
        /// Oh well...
        /// </summary>
        public static bool IsAdminPage(ContextGlobal g)
        {
            switch (g.AppPage.App.ID)
            {
                case 540:
                case 543:
                case 545:
                case 548:
                case 541:
                case 523:
                case 507:
                    return true;

                default:
                    return false;
            }
        }

        //Emit javascript that forces the site to be rendered in the top frame.
        protected void BustaFrames()
        {
            // do not allow frame busting when admin pages are accessed. provo is embedding our admin pages in their apps.
            if (Convert.ToBoolean(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ENABLE_WEB_ADMIN"))
                || g.IsDevMode || _g.AppPage.ControlName.IndexOf("GAMIframe") > -1 ||
                !string.IsNullOrEmpty(Request.QueryString[WebConstants.URL_PARAMETER_FACEBOOK_REGISTRATION])
                || (!string.IsNullOrEmpty(Request.QueryString["composeoverlay"]) &&
                        ((_g.AppPage.ControlName.IndexOf("Compose") > -1) || (_g.AppPage.ControlName.IndexOf("MessageSent") > -1) || (_g.AppPage.ControlName.IndexOf("MailBox") > -1))))
                return;

            StringBuilder script = new StringBuilder();
            script.Append("<script>");
            script.Append("$j(document).ready(function(){RemoveFrames()});");
            script.Append("</script>");

            Page.RegisterStartupScript("BustaFrames", script.ToString());
        }

        public virtual void HideIconLinks()
        {
        }

        public virtual void ShowFeedback(Matchnet.Web.Framework.Ui.Feedback.FeedbackHelper.FeedbackType feedbackType)
        {
        }

        public virtual void AppendBodyCSS(string cssClass)
        {

        }

        #endregion

        #region Public Properties
        public virtual Matchnet.Web.Framework.Image ImageLogo
        {
            get { return null; }
        }

        /// <summary>
        /// This placeholder will be populated with Notifications by Default.
        /// </summary>
        public virtual PlaceHolder NotificationControlPlaceholder
        {
            get
            {
                return _notificationControlPlaceholder;
            }
            set
            {
                _notificationControlPlaceholder = value;
            }
        }

        /// <summary>
        /// This is the frameworkcontrol that will contain the actual content of the web page.
        /// </summary>
        public FrameworkControl AppControl
        {
            get
            {
                return _appControl;
            }
            set
            {
                _appControl = value;
            }
        }

        public string Direction
        {
            get
            {
                return g.Brand.Site.Direction.ToString();
            }
        }

        public virtual void DisableGamOverlay()
        {

        }

        public virtual void LoadFacebookScript()
        {

        }

        public virtual void AddPageAccouncementControl(FrameworkControl control)
        {
        }

        public virtual void AddPageTopControl(FrameworkControl control)
        {
        }

        public string GetPageCssClass()
        {
            if (null == this.AppControl) return string.Empty;
            return this.AppControl.ToString().Replace("ASP.", string.Empty).Replace("_ascx", string.Empty).Replace("_", "-");
        }
        #endregion

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            InitializeComponent();
            base.OnInit(e);
        }

        private void InitializeComponent()
        {
            this.Init += new System.EventHandler(this.Page_Init);
        }
        #endregion
    }
}
