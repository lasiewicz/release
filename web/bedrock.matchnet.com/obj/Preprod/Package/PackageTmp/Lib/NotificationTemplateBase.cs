#region System References
using System;
#endregion

namespace Matchnet.Web.Lib
{
	/// <summary>
	/// The layout base inherited by Notification template controls.
	/// Provides public control over the text, css, and image of the Notification control.
	/// </summary>
	public class NotificationTemplateBase : System.Web.UI.UserControl
	{
		#region Member Variables
		public const string DEFAULT_NOTIFICATION_PATH = "NotificationTemplates/StandardNotification.ascx";
		private string _imagePath;
		private string _message;
		private string _cssClass;
		#endregion

		#region Constructors
		public NotificationTemplateBase()
		{
		}

		public NotificationTemplateBase(string imagePath, string message)
		{
			_imagePath = imagePath;
			_message = message;
		}
		#endregion

		#region Public Properties
		/// <summary>
		/// The string representing the path of the image for the notification template
		/// </summary>
		public string ImagePath
		{
			get {return _imagePath;}
			set {_imagePath = value;}
		}

		/// <summary>
		/// The string representing the text message for the notification template
		/// </summary>
		public string Message
		{
			get {return _message;}
			set {_message = value;}
		}

		/// <summary>
		/// The string representing the css class of the notification template
		/// </summary>
		public string CssClass
		{
			get {return _cssClass;}
			set {_cssClass = value;}
		}
		#endregion


	}
}
