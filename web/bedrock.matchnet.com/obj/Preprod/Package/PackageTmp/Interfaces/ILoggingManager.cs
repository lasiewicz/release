﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Member.ServiceAdapters.Interfaces;

namespace Matchnet.Web.Interfaces
{
    public interface ILoggingManager
    {
        void LogException(Exception exception, IMember member, Brand brand, string className);
        void LogException(Exception exception, IMember member, int communityId, int siteID, int brandID, string className);
        void LogInfoMessage(string className, string message);
    }
}