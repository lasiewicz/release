﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.Web.Interfaces
{
    public interface ISettingsManager
    {
        int GetSettingInt(string settingConstant);
        bool GetSettingBool(string settingConstant);
        string GetSettingString(string settingConstant);
    }
}
