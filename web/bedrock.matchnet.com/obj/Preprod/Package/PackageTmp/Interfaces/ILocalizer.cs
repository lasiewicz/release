﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using Matchnet.Content.ValueObjects.BrandConfig;

namespace Matchnet.Web.Interfaces
{
    public interface ILocalizer
    {
        string GetFormattedStringResource(string resourceName, object caller, Brand brand, StringDictionary tokenMap,
                                          string[] args, bool showResourceHintsBySetting);

        string GetStringResource(string key, object caller, Brand brand);
        string GetStringResource(string key, object caller, Brand brand, StringDictionary tokenMap);
        string GetStringResourceWithNoTokeMap(string key, object caller, Brand brand);

        string GetFormattedStringResource(string resourceName, object caller, Brand brand, StringDictionary tokenMap,
                                          string[] args);

        string ConvertTypeToResourcePath(Type type);
        string ExpandTokens(string source, StringDictionary tokenMap);
        string ExpandImageTokens(string source, Brand brand);
    }
}
