﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.Member.ValueObjects.Admin;

namespace Matchnet.Web.Interfaces
{
    public interface IImpersonationManager
    {
        string GenerateImpersonateToken(int adminMemberId, int impersonateMemberId, int impersonateBrandId);
        ImpersonateToken ValidateImpersonateToken(string impersonateTokenGUID);
        void ExpireImpersonateToken(int adminMemberId);
    }
}