﻿
using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using Matchnet.Lib;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Ui;
using Matchnet.Web.Framework.Ui.BasicElements;

namespace Matchnet.Web.Applications.SiteMap
{
    public partial class SiteMap20 : FrameworkControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            g.SetPageTitle(g.GetResource("TXT_PAGE_TITLE", this));

            if (g.BreadCrumbTrailHeader != null)
            {
                g.BreadCrumbTrailHeader.SetTwoLinkCrumb(g.GetResource("TXT_SITE_MAP", this),
                                                        g.AppPage.App.DefaultPagePath);

            }
            if (g.BreadCrumbTrailFooter != null)
            {
                g.BreadCrumbTrailFooter.SetTwoLinkCrumb(g.GetResource("TXT_SITE_MAP", this),
                                                       g.AppPage.App.DefaultPagePath);
            }			

        }
        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }
        #endregion
    }
}