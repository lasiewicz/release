﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="SiteMap20.ascx.cs" Inherits="Matchnet.Web.Applications.SiteMap.SiteMap20" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>

<asp:PlaceHolder id="plcSiteMapHtml" visible="true" runat="server">
				<mn:txt id="txtSiteMapHtml" runat="server" ExpandImageTokens="True" ResourceConstant="SITEMAP_HTML"></mn:txt>
</asp:PlaceHolder>