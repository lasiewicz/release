﻿using System;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Web;

using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Lib;
using Matchnet.PromoEngine.ServiceAdapters;
using Matchnet.PromoEngine.ServiceAdapters.TokenReplacement;
using Matchnet.PromoEngine.ValueObjects;
using Matchnet.PromoEngine.ValueObjects.ResourceTemplate;
using Matchnet.Purchase.ValueObjects;
using Matchnet.Web.Framework;
using Newtonsoft.Json;
using Matchnet.Content.ValueObjects.Region;
using Matchnet.Purchase.ServiceAdapters;
using Spark.Common.UPS;
using Spark.Common;
using CryptoHelperRSA;
using Spark.Common.RestConsumer.V2.Models.Subscription;
using Matchnet.Web.Framework.Managers;

namespace Matchnet.Web.Applications.Subscription.UnifiedPaymentSystem
{
    /// <summary>
    /// This class is used for redirecting (using client side POST) subscription requests to PaymentUI as part of UPS.
    /// This places any requests to subscription pages. 7/09
    /// </summary>
    public class PaymentUIConnector : FrameworkControl
    {
        #region Constants

        private const string PRIMARYKEY_UPSLEGACYDATAID = "UPSLegacyDataID";
        private const string PRIMARYKEY_UPSGLOBALLOGID = "UPSGlobalLogID";

        #endregion

        /// <summary>
        /// This is the only public method in this class.
        /// </summary>
        public void RedirectToPaymentUI(IPaymentUIJSON paymentUIJSON)
        {
    
            try
            {
                System.Diagnostics.Trace.WriteLine("Start PaymentUIConnector.cs RedirectToPaymentUI()");
                if (Convert.ToBoolean((RuntimeSettings.GetSetting("IS_SUBSCRIPTION_FREEZE_ENABLED", this.g.Brand.Site.Community.CommunityID,
                    this.g.Brand.Site.SiteID, this.g.Brand.BrandID))))
                {
                    int memberSubscriptionStatus = FrameworkGlobals.GetMemberSubcriptionStatus(g.Member.MemberID, g.Brand);
                    SubscriptionStatus enumSubscriptionStatus = (SubscriptionStatus)memberSubscriptionStatus;

                    if (enumSubscriptionStatus == SubscriptionStatus.PausedSubscriber)
                    {
                        // Do not allow the member to get to the subscription page if he has a frozen subscription 
                        RedirectToDestination();
                    }
                }

                System.Diagnostics.Trace.WriteLine("PaymentUIConnector.cs RedirectToPaymentUI() - Get Version and TemplateID");
                // Get Version & Template ID.
                paymentUIJSON = PUI_PageData.Hydrate(paymentUIJSON,
                    base.Context.Server.MapPath("/Applications/Subscription/UnifiedPaymentSystem/PUI_Page.xml"));

                int newLegacyDataID = Constants.NULL_INT;
                int newGlobalLogID = Constants.NULL_INT;          

                if (paymentUIJSON.SaveLegacyData == true)
                {
                    System.Diagnostics.Trace.WriteLine("PaymentUIConnector.cs RedirectToPaymentUI() - SaveLegacyData");
                    newLegacyDataID = KeySA.Instance.GetKey(PRIMARYKEY_UPSLEGACYDATAID);
                    //newLegacyDataID = KeySA.Instance.GetKey("MemberID");
                    newGlobalLogID = KeySA.Instance.GetKey(PRIMARYKEY_UPSGLOBALLOGID);

                    paymentUIJSON.UPSLegacyDataID = newLegacyDataID;
                    paymentUIJSON.UPSGlobalLogID = newGlobalLogID;
                }

                System.Diagnostics.Trace.WriteLine("PaymentUIConnector.cs RedirectToPaymentUI() - PaymentUIJson.GetJSONObject()");

                paymentUIJSON.PremiumType = GetRequestedPackageToDisplay();
                object payment = paymentUIJSON.GetJSONObject();

                System.Diagnostics.Trace.WriteLine("PaymentUIConnector.cs RedirectToPaymentUI() - (PaymentJson)payment");

                string localPaymentUIURL = string.Empty;
                int localNewGlobalLogID = Constants.NULL_INT;

                PaymentJson tempPaymentJson = null;

                if (payment is PaymentUiPostData)
                {
                    var p = (PaymentUiPostData)payment;
                    localPaymentUIURL = p.PaymentUiUrl;
                    localNewGlobalLogID = p.GlobalLogId;

                    //decrypt p to get package information the deserialize
                    tempPaymentJson = JsonConvert.DeserializeObject<PaymentJson>(p.Json.Decrypt());

                    System.Diagnostics.Trace.WriteLine("(PaymentUiPostData)  post to URL: " + p.PaymentUiUrl);
                }
                else
                {
                    string paymentUIURL = (RuntimeSettings.GetSetting("UPS_PAYMENT_UI_URL", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID));
                    paymentUIURL += g.Brand.Site.Name.Replace(".", String.Empty).ToLower();

                    localPaymentUIURL = paymentUIURL;
                    localNewGlobalLogID = newGlobalLogID;

                    System.Diagnostics.Trace.WriteLine("(PaymentJson) post URL: " + paymentUIURL);

                    //Add Kount MerchantID
                    ((PaymentJson)payment).KountMerchantID = SubscriberManager.Instance.GetKountMerchantID(g.Brand);
                }

                if (tempPaymentJson != null)
                {
                    payment = null;
                    payment = tempPaymentJson;
                }
               
                payment = PackagesToDisplay(payment);

                HandleNoPackagesToDisplay((PaymentJson)payment, paymentUIJSON.TemplateID);

                System.Diagnostics.Trace.WriteLine("PaymentUIConnector.cs RedirectToPaymentUI() - JSon Serialize payment object");

                string postString = JsonConvert.SerializeObject(payment);

                System.Diagnostics.Trace.WriteLine("UPS PostString:" + postString);
                g.LoggingManager.LogInfoMessage("PaumentUIConnector", "UPS PostString:" + postString);

                if (paymentUIJSON.SaveLegacyData == true)
                {
                    System.Diagnostics.Trace.WriteLine("PaymentUIConnector.cs RedirectToPaymentUI() - UPSLegacyDataSave");
                    // Using the text compression method from Spark.Common. Cuts from 12kb to 2kb.
                    PurchaseSA.Instance.UPSLegacyDataSave(postString.Compress(), newLegacyDataID);
                }

                System.Diagnostics.Trace.WriteLine("PaymentUIConnector.cs RedirectToPaymentUI() - POstData");
                // Redirect to user's browser which will then POST to PUI on load.

                if (!string.IsNullOrEmpty(postString) && !string.IsNullOrEmpty(localPaymentUIURL) && localNewGlobalLogID != Constants.NULL_INT)
                {
                    System.Diagnostics.Trace.WriteLine("(POSTData) Successfully posted to URL: " + localPaymentUIURL);

                    POSTData(postString.Encrypt(), localPaymentUIURL, localNewGlobalLogID);
                }
                else
                    g.ProcessException(new Exception("Error redirecting to Payment UI. something was wrong with localPostString localPaymentUIURL localNewGlobalLogID .." + postString + " " + localPaymentUIURL + " " + localNewGlobalLogID));
            }
            catch (ThreadAbortException)
            {
                //we don't care about thread abort exceptions, so just throw it away
            }
            catch (Exception ex)
            {
                g.ProcessException(new Exception("Error redirecting to Payment UI.", ex));
            }
        }


        private void HandleNoPackagesToDisplay(PaymentJson paymentJSON, int templateID)
        {
            if (paymentJSON.Data.Packages.Count <= 0 && templateID != 110)
            {
                HandleNoPackagesToDisplayRedirect();
            }
        }

        private void HandleNoPackagesToDisplayRedirect()
        {
            System.Diagnostics.Trace.WriteLine("PaymentUIConnector.cs RedirectToPaymentUI() - No packages, return to confirmation page");
            // Since there are no packages for upsale, than return back to the confirmation page 
            string href;
            if (Context.Request["oid"] != null)
            {
                if (g.GetDestinationURL() != null && g.GetDestinationURL().Length > 0)
                {
                    href = "/Applications/Subscription/SubscriptionConfirmation.aspx?oid=" + Context.Request["oid"] + "&DestinationURL=" + System.Web.HttpUtility.UrlEncode(g.GetDestinationURL());
                }
                else
                {
                    href = "/Applications/Subscription/SubscriptionConfirmation.aspx?oid=" + Context.Request["oid"];
                }

                System.Diagnostics.Trace.WriteLine("No upsale packages exists so go back to the following URL: " + href);

                Context.Response.Redirect(href);
            }
            else
            {
                RedirectToDestination();
            }
        }

        private PremiumType GetRequestedPackageToDisplay()
        {
            PremiumType premiumType = PremiumType.None;

            if (!string.IsNullOrEmpty(Context.Request["PackagesToDisplay"]))
            {
                string package = Context.Request["PackagesToDisplay"].Trim().ToUpper();
                switch (package)
                {
                    case "AA":
                        return PremiumType.AllAccess;
                    case "ALAE":
                        return PremiumType.AllAccessEmail;
                    case "CC":
                        return PremiumType.ColorCode;
                    case "HLP":
                        return PremiumType.HighlightedProfile;
                    case "JM":
                        return PremiumType.JMeter;
                    case "SF":
                        return PremiumType.ServiceFee;
                    case "SM":
                        return PremiumType.SpotlightMember;
                    case "RR":
                        return PremiumType.ReadReceipt;
                }

            }

            return premiumType;
        }

        // Removes all packages that are not in PackagesToDisplay string
        private object PackagesToDisplay(object payment)
        {
            try
            {
                if (!string.IsNullOrEmpty(Context.Request["PackagesToDisplay"]))
                {
                    string packagesToDisplayList = Context.Request["PackagesToDisplay"].ToUpper();
                    PaymentJson paymentJson = payment as PaymentJson;
                    if (paymentJson != null)
                    {
                        for (int i = paymentJson.Data.Packages.Count - 1; i >= 0; i--)
                        {
                            var paymentJsonPackage = ((PaymentJson)payment).Data.Packages[i];
                            bool removePackage = false;
                            string[] premiumTypesArray = ((string)paymentJsonPackage.Items["PremiumType"]).Split(new char[] { ',', ' ' },
                                                                                     StringSplitOptions
                                                                                         .RemoveEmptyEntries);

                            foreach (var premiumTypeString in premiumTypesArray)
                            {
                                switch (premiumTypeString)
                                {
                                    case "AllAccess":
                                        removePackage = RemovePackage(packagesToDisplayList, "AA"); break;
                                    case "AllAccessEmail":
                                        removePackage = RemovePackage(packagesToDisplayList, "ALAE"); break;
                                    case "ColorCode":
                                        removePackage = RemovePackage(packagesToDisplayList, "CC"); break;
                                    case "HighlightedProfile":
                                        removePackage = RemovePackage(packagesToDisplayList, "HLP"); break;
                                    case "JMeter":
                                        removePackage = RemovePackage(packagesToDisplayList, "JM"); break;
                                    case "ServiceFee":
                                        removePackage = RemovePackage(packagesToDisplayList, "SF"); break;
                                    case "SpotlightMember":
                                        removePackage = RemovePackage(packagesToDisplayList, "SM"); break;
                                    case "ReadReceipt":
                                        removePackage = RemovePackage(packagesToDisplayList, "RR"); break;
                                }
                            }

                            if (removePackage)
                            {
                                ((PaymentJson)payment).Data.Packages.Remove(paymentJsonPackage);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                g.ProcessException(new Exception("Error in PackagesToDisplay.", ex));
            }

            return payment;
        }

        // Check if PremiumTypeAbbreviation is found in PackagesToDisplay
        private bool RemovePackage(string PackagesToDisplay, string PremiumTypeAbbreviation)
        {
            return !PackagesToDisplay.Contains(PremiumTypeAbbreviation);
        }

        private void RedirectToDestination()
        {
            if (g.GetDestinationURL() != null && g.GetDestinationURL().Length > 0)
            {
                Context.Response.Redirect(g.GetDestinationURL());
            }
            else
            {
                Context.Response.Redirect("/Applications/Home/Default.aspx");
            }
            HttpContext.Current.Response.End();
        }

        #region Private Methods

        /// <summary>
        /// This renders a POST form on the clients browser which then triggers a submit on body onload.
        /// Not the greatest idea.. Ugh.
        /// </summary>
        /// <param name="postString"></param>
        /// <param name="url"></param>
        private void POSTData(string postString, string url, int newGlobalLogID)
        {
            try
            {
                HttpContext.Current.Response.Clear();

                System.Diagnostics.Trace.WriteLine("Begin posting to URL: " + url);

                // If admin, disallow the "back button" process.
                if (!g.ImpersonateContext)
                {
                    // This block is to imitate a back button issue coming from PUI. This has an issue if the
                    // member does not nav away from the PUI page by not hitting the back button.
                    HttpCookie cookie = HttpContext.Current.Request.Cookies["SparkUPS"];

                    // Visiting back
                    if (cookie != null && cookie.HasKeys)
                    {
                        if (cookie["Visited"] == "True")
                        {
                            // Delete the cookie
                            HttpCookie die = new HttpCookie("SparkUPS");

                            die.Expires = DateTime.Now.AddDays(-1d);
                            HttpContext.Current.Response.Cookies.Add(die);
                        }

                        // Allow check page to render w/o getting redirected.
                        if ((HttpContext.Current.Request["PaymentTypeID"] == null)
                            && (HttpContext.Current.Request["PromoID"] == null))
                        {
                            string backToURL;

                            backToURL = cookie["BackToURL"];

                            HttpContext.Current.Response.Redirect(backToURL);
                        }

                        System.Diagnostics.Trace.WriteLine("Processed returning user for posting to URL: " + url);
                    }
                    // First time visit
                    else
                    {
                        if (g.Page.Request.UrlReferrer != null)
                        {
                            HttpCookie brownie = new HttpCookie("SparkUPS");

                            brownie.Expires = DateTime.Now.AddMinutes(20);

                            brownie.Values["Visited"] = "True";
                            brownie.Values["BackToURL"] = g.Page.Request.UrlReferrer.AbsoluteUri;

                            HttpContext.Current.Response.Cookies.Add(brownie);
                        }

                        System.Diagnostics.Trace.WriteLine("Processed new user for posting to URL: " + url);
                    }
                }

                StringBuilder sb = new StringBuilder();

                //added shortcut icon metadata to head, because lack of it was causing background requests to favicon.ico
                //which doesn't exists and was redirecting to home page. 

                sb.Append("<html><head>" + Environment.NewLine);
                sb.Append("<link rel=\"shortcut icon\" href=\"" + FrameworkGlobals.GetShortcutIconURL() + "\" />");
                sb.Append(string.Format("</head><body onload=\"document.{0}.submit()\">", "form1"));
                sb.Append(string.Format("<form id=\"form1\" name=\"{0}\" method=\"{1}\" action=\"{2}\" >" + Environment.NewLine, "form1", "POST", url));
                sb.Append(string.Format("<input type=\"hidden\" name=\"{0}\" ID=\"{0}\" value=\"{1}\" />" + Environment.NewLine, "requestData", postString));
                sb.Append(string.Format("<input type=\"hidden\" name=\"{0}\" ID=\"{0}\" value=\"{1}\" />" + Environment.NewLine, "globalLogID", Convert.ToString(newGlobalLogID)));
                sb.Append("</form>" + Environment.NewLine);
                sb.Append("</body></html>");

                System.Diagnostics.Trace.WriteLine("Post data: " + sb.ToString());

                System.Diagnostics.Trace.WriteLine("Created a form to submit to URL: " + url);

                HttpContext.Current.Response.Write(sb.ToString());

                System.Diagnostics.Trace.WriteLine("Submitted the form to go to to URL: " + url);

                HttpContext.Current.Response.End();
            }
            catch (ThreadAbortException)
            {
                //we don't care about thread abort exceptions, so don't log, just throw it away                   
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.WriteLine("LogException  Error message   \r\n :" + ex.Message);
                System.Diagnostics.Trace.WriteLine("LogException  Error   \r\n :" + ex.ToString());

                throw new Exception("Error in POST to Payment UI. Error Message: " + ex.Message + ", Error: " + ex.ToString() + ", PostString:" + postString.ToString()
                    + "URL:" + url.ToString(), ex);
            }
        }

        #endregion

        /// <summary>
        /// This is a hack(woohoo) to map Currency Enum to the proper abbreviation.
        /// </summary>
        /// <param name="description"></param>
        public static string GetCurrencyAbbreviation(CurrencyType currencyType)
        {
            switch (currencyType)
            {
                case CurrencyType.USDollar:
                    return "USD";
                case CurrencyType.Euro:
                    return "EUR";
                case CurrencyType.CanadianDollar:
                    return "CAD";
                case CurrencyType.Pound:
                    return "GBP";
                case CurrencyType.AustralianDollar:
                    return "AUD";
                case CurrencyType.Shekels:
                    return "NIS";
                case CurrencyType.VerifiedByVisa:
                    return "VBV";
                default:
                    return "None";
            }
        }
    }
}


