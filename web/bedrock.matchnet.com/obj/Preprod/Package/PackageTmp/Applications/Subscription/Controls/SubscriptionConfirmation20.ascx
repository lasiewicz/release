﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SubscriptionConfirmation20.ascx.cs" Inherits="Matchnet.Web.Applications.Subscription.Controls.SubscriptionConfirmation20" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>

<h1><mn:txt runat="server" id="Txt22" ResourceConstant="TXT_H1_SUB_CONFIRMATION" /></h1>
<asp:PlaceHolder runat="server" Visible="false" ID="phMicroProfile">
<div class="carrot-profile" id="divMiniProfile" runat="server">
    <div class="rbox-style-clear clearfix">
        <asp:PlaceHolder id="phMiniProfile" Runat="server" />
    </div>
</div>
</asp:PlaceHolder>
<asp:PlaceHolder ID="plcConfirmation" Runat="server"><mn:Txt id="txtRecordInfo" runat="server" /></asp:PlaceHolder>

<div id="sub-conf-continue">
    <mn2:frameworkbutton id="ProcessAttributesAndContinue" Visible="false" runat="server" class="btn primary" ResourceConstant="BTN_CONTINUE_RAQUO" OnClick="ProcessAttributesAndContinue_Click"/>
    <mn2:FrameworkHtmlButton runat="server" id="Continue" ResourceConstant="BTN_CONTINUE" class="btn primary" />
</div>

<div class="tips border-gen items-03 rbox-style-clear">
    <h2><mn:txt runat="server" id="txtNextStep" ResourceConstant="TXT_NEXT_STEP" /></h2>
    <p><mn:txt id="txtPlan" runat="server" ResourceConstant="TXT_MAKE_THE_MOST" /></p>
    <div class="item">
	    <mn:txt runat="server" id="txtCell1" ExpandImageTokens="true" ResourceConstant="HTML_PHOTO_UPLOAD" />
    </div>
    <div class="item cell2">
	    <mn:txt runat="server" id="txtCell2" ExpandImageTokens="true" ResourceConstant="HTML_EMAIL" />
    </div>
    <div class="item last">
	    <mn:txt runat="server" id="txtCell3" ExpandImageTokens="true" ResourceConstant="HTML_HURRYDATE" />
    </div>
</div>

<mn:txt runat="server" ID="txtSlider" ExpandImageTokens="true" Visible="false" />


