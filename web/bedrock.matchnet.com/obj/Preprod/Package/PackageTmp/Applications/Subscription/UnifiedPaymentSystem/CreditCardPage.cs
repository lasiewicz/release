﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Content.ServiceAdapters.Links;
using Matchnet.Web.Framework;
using Matchnet.Purchase.ValueObjects;
using Matchnet.PromoEngine.ServiceAdapters;
using Matchnet.PromoEngine.ServiceAdapters.TokenReplacement;
using Matchnet.PromoEngine.ValueObjects;
using Matchnet.PromoEngine.ValueObjects.ResourceTemplate;
using Matchnet.Web.Framework.Managers;
using Matchnet.Web.Framework.Util;
using MPVO = Matchnet.Purchase.ValueObjects;
using Matchnet.Purchase.ServiceAdapters;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.Region;
using Matchnet.Lib;
using Spark.Common.MemberLevelTracking;
using Spark.Common.UPS;
using Spark.Common.Adapter;
using Matchnet.Member.ServiceAdapters;
using Spark.Common.RestConsumer.V2.Models.Subscription;
using Spark.Common.RestConsumer.V2.Models.OAuth2;
using Brand = Matchnet.Content.ValueObjects.BrandConfig.Brand;

namespace Matchnet.Web.Applications.Subscription.UnifiedPaymentSystem
{
    public class CreditCardPage : FrameworkControl, IPaymentUIJSON
    {
        private int version = Constants.NULL_INT;
        private int templateID = Constants.NULL_INT;
        private PaymentType paymentType;
        private int upsLegacyDataID = Constants.NULL_INT;
        private int UPSGlobalLogID = Constants.NULL_INT;		
        private bool saveLegacyData = true;
        public bool IsSubscriptionConfirmationEmailEnabled { get; set; }
        private Promo _promo;
        private PremiumType _premiumType = PremiumType.None;

        public HttpContext Context { get; set; }
        public Dictionary<String, String> OrderAttributes { get; set; }
        public int SelectedPlanID { get; set; }
        public int PromoID
        {
            get { return _promo.PromoID; }
 }
        /// <summary>
        /// Reference to the calling page.
        /// </summary>
        public object Page;

        public CreditCardPage(HttpContext context, PaymentType paymentType)
        {
            this.Context = context;
            this.paymentType = paymentType;
            InitPromo();
        }

        public CreditCardPage(HttpContext context, PaymentType paymentType, int promoID)
        {
            this.Context = context;
            this.paymentType = paymentType;
            InitPromo(promoID);
        }

        #region IPaymentUIJSON Members
        PremiumType IPaymentUIJSON.PremiumType
        {
            get
            {
                return _premiumType;
            }
            set
            {
                _premiumType = value;
            }
        }

        bool IPaymentUIJSON.SaveLegacyData
        {
            get
            {
                return saveLegacyData;
            }
            set
            {
                saveLegacyData = true;
            }
        }

        int IPaymentUIJSON.Version
        {
            get
            {
                return version;
            }
            set
            {
                version = value;
            }
        }

        int IPaymentUIJSON.TemplateID
        {
            get
            {
                return templateID;
            }
            set
            {
                templateID = value;
            }
        }

        PaymentType IPaymentUIJSON.PaymentType
        {
            get
            {
                return paymentType;
            }
            set
            {
                paymentType = value;
            }
        }

        int IPaymentUIJSON.UPSLegacyDataID
        {
            get
            {
                return upsLegacyDataID;
            }
            set
            {
                upsLegacyDataID = value;
            }
        }

        int IPaymentUIJSON.UPSGlobalLogID
        {
            get
            {
                return UPSGlobalLogID;
            }
            set
            {
                UPSGlobalLogID = value;
            }
        }

        private bool UseAPIForPaymentJSON()
        { 
            SettingsManager settingsManager = new SettingsManager();
            return settingsManager.GetSettingBool("USE_API_FOR_UPS_SUBSCRIPTION_PAYMENT_JSON", g.TargetCommunityID, g.Brand.Site.SiteID, g.TargetBrandID);
        }
		
        object IPaymentUIJSON.GetJSONObject() 
        {
            if (UseAPIForPaymentJSON())
                return GetAPIPaymentJson();
            else
                return GetPaymentJson();
	    }

        private PaymentUiPostData GetAPIPaymentJson()
        {
            try
            {
                saveLegacyData = false;
            
                //version and template are set in payment ui connector and paymenttype by subscribenew page. 
                if (version == Constants.NULL_INT)
                    throw new Exception("Version cannot be null.");

                //set up the access token for the API. 
                PaymentUiPostData paymentData = null;
                LogonManager logonManager = new LogonManager(_g);
                OAuthTokens oAuthTokens = logonManager.GetTokensFromCookies();
                string apiToken = string.Empty;
                if (oAuthTokens != null)
                {
                    apiToken =  oAuthTokens.AccessToken;

                    string cancelUrl = string.Empty;
                    if (g.Page.Request.UrlReferrer != null)
                        cancelUrl = g.Page.Request.UrlReferrer.AbsoluteUri;
                    else
                        cancelUrl = "http://" + g.Page.Request.Url.Host;

                    string evar6;
                    string originalPrtId = GetOriginalPrtid(out evar6);

                    var request = new PaymentUiPostDataRequest
                    {
                        PaymentUiPostDataType = "1",
                        CancelUrl = cancelUrl,
                        ConfirmationUrl = "http://" + g.Page.Request.Url.Host + "/Applications/Subscription/SubscriptionConfirmation.aspx",
                        DestinationUrl = (Context.Request["DestinationURL"] != null) ? Context.Server.UrlEncode(Convert.ToString(Context.Request["DestinationURL"])) : string.Empty,
                        ReturnUrl = "http://" + g.Page.Request.Url.Host,
                        ClientIp = g.ClientIP,
                        PrtTitle = this.GetPRTTitle(),
                        SrId = (Context.Request["srid"] != null) ? Convert.ToString(Context.Request["srid"]).Replace(",", ";") : string.Empty,
                        PrtId = originalPrtId,
                        OmnitureVariables = new Dictionary<string, string>(),
                        MemberLevelTrackingLastApplication = "1",
                        MemberLevelTrackingIsMobileDevice = "false",
                        MemberLevelTrackingIsTablet = "false",
                        PromoType = "0",
                        GiftDestinationUrl = "http://" + g.Page.Request.Url.Host + "/Applications/MemberServices/ManageGifts.aspx",
                        EID = g.Session[WebConstants.SESSION_PROPERTY_NAME_EID]
                    };

                    if(Context.Request["PromoID"] != null)
                    {
                        request.PromoID =  Convert.ToInt32(Context.Request["PromoID"]);
                    }
                
                    #region Omniture Variables
                    System.Diagnostics.Trace.WriteLine("CreditCardPage.cs GetJSONObject() - Start Omniture Variables");

                    request.OmnitureVariables.Add("linkInternalFilters", g.AnalyticsOmniture.S_LinkInternalFilters);
                    request.OmnitureVariables.Add("pageName", "Subscribe - SubscribeNew");
                    request.OmnitureVariables.Add("prop17", OmnitureHelper.OmnitureHelper.GetProfileCompetionPercentage(_g.TargetMember, _g.TargetBrand));
                    request.OmnitureVariables.Add("prop18", OmnitureHelper.OmnitureHelper.GetGender(_g.TargetMember.GetAttributeInt(_g.TargetBrand, Matchnet.Web.Framework.WebConstants.ATTRIBUTE_NAME_GENDERMASK)));
                    request.OmnitureVariables.Add("prop19", OmnitureHelper.OmnitureHelper.GetAge(_g.TargetMember.GetAttributeDate(_g.TargetBrand, Matchnet.Web.Framework.WebConstants.ATTRIBUTE_NAME_BIRTHDATE)).ToString());
                    if (_g.TargetBrand.Site.Community.CommunityID == (int)Matchnet.Web.Framework.WebConstants.COMMUNITY_ID.JDate)
                    {
                        request.OmnitureVariables.Add("prop20", MemberProfile.ProfileDisplayHelper.GetOptionValue(_g.TargetMember, _g, "JDateEthnicity", "JDateEthnicity"));
                    }
                    else
                    {
                        request.OmnitureVariables.Add("prop20", MemberProfile.ProfileDisplayHelper.GetOptionValue(_g.TargetMember, _g, "Ethnicity", "Ethnicity"));
                    }
                    request.OmnitureVariables.Add("prop21", OmnitureHelper.OmnitureHelper.GetRegionString(_g.TargetMember, _g.TargetBrand, _g.TargetBrand.Site.LanguageID, false, true, false).Replace("\"", string.Empty));
                    request.OmnitureVariables.Add("prop23", _g.TargetMember.MemberID.ToString());
                    request.OmnitureVariables.Add("events", "event9");
               
                    request.OmnitureVariables.Add("eVar4", PromoID.ToString());
                    request.OmnitureVariables.Add("eVar44", string.Format(WebConstants.OMNITURE_EVAR44_MEMBERID, (_g.TargetMember.MemberID % 10).ToString()));

                    request.OmnitureVariables.Add("eVar6", evar6);
                    request.OmnitureVariables.Add("eVar7", (g.RecipientMemberID != 0)
                                                       ? g.RecipientMemberID.ToString()
                                                       : string.Empty);

                    int memberSubscriptionStatus = FrameworkGlobals.GetMemberSubcriptionStatus(_g.TargetMember, _g.TargetBrand);
                    SubscriptionStatus enumSubscriptionStatus = (SubscriptionStatus)memberSubscriptionStatus;
                    request.OmnitureVariables.Add("eVar8", FrameworkGlobals.GetAbbreviatedMemberSubscriptionStatusForOmniture(enumSubscriptionStatus));

                    string paymentUIURL = (RuntimeSettings.GetSetting("UPS_PAYMENT_UI_URL", g.TargetBrand.Site.Community.CommunityID,
                                g.TargetBrand.Site.SiteID, g.TargetBrand.BrandID));

                    paymentUIURL += g.TargetBrand.Site.Name.Replace(".", String.Empty).ToLower();
                    request.OmnitureVariables.Add("prop10", paymentUIURL);
                    request.OmnitureVariables.Add("prop14", g.AnalyticsOmniture.Prop14);
                    request.OmnitureVariables.Add("prop29", g.AnalyticsOmniture.Prop29);

                    request.OmnitureVariables.Add("evar11", g.Session[WebConstants.SESSION_PROPERTY_NAME_PROMOTIONID]);
                    request.OmnitureVariables.Add("evar12", g.Session[WebConstants.SESSION_PROPERTY_NAME_EID]);
                    request.OmnitureVariables.Add("evar13", g.Session[WebConstants.SESSION_PROPERTY_NAME_LUGGAGE]);

                    // Send Google Analytics client id for tracking continuity 
                    if (!string.IsNullOrEmpty(SettingsManager.GetSettingString(SettingConstants.GOOGLE_ANALYTICS_ACCOUNT,
                            g.Brand)))
                    {
                        if (HttpContext.Current.Request.Cookies["_ga"] != null && !string.IsNullOrEmpty(HttpContext.Current.Request.Cookies["_ga"].Value))
                        {
                            request.OmnitureVariables.Add("GoogleAnalyticsClientID", HttpContext.Current.Request.Cookies["_ga"].Value.Remove(0, 6));
                        }
                    }

                    // passing the mbox name if it's present for this site
                    string mBoxNamePrefix = (RuntimeSettings.GetSetting("T_N_T_SUB_CONFIRM_MBOX_NAME_PREFIX", g.TargetBrand.Site.Community.CommunityID,
                                g.TargetBrand.Site.SiteID, g.TargetBrand.BrandID));

                    if (mBoxNamePrefix == null)
                        mBoxNamePrefix = string.Empty;

                    request.OmnitureVariables.Add("mboxname", mBoxNamePrefix == string.Empty ? string.Empty : mBoxNamePrefix + "_sub_new");
                    #endregion

                    APIPaymentUIJSON apiPaymentUIPSON = new APIPaymentUIJSON();
                    paymentData = apiPaymentUIPSON.PostPayment(request, apiToken,g.Brand.BrandID.ToString());

                    if (paymentData == null)
                        throw new Exception("Unable to get payment data from Subcription. CreditCard page");

                    TraceLogConnectorServiceWebAdapter.InsertEndpointTraceLog(UPSGlobalLogID, "PaymentUIConnector", "GetAPIPaymentJson", "NO ERROR", null, Constants.NULL_INT, Spark.Common.TraceLogConnectorService.UPSEndpointProcessingStatus.OK, DateTime.Now);

                }
                else
                    throw new Exception("CreditCardPage...GetAPIPaymentJson...oAuthTokens was null");

                return paymentData;  
            }
            catch (Exception ex)
            {
                TraceLogConnectorServiceWebAdapter.InsertEndpointTraceLog(UPSGlobalLogID, "PaymentUIConnector", "GetAPIPaymentJson", "ERROR DETECTED: " + ex.Message, null, Constants.NULL_INT, Spark.Common.TraceLogConnectorService.UPSEndpointProcessingStatus.ERROR, DateTime.Now);

                throw new Exception("Error in getting JSON from API. MemberID:" +
                    g.TargetMember.MemberID.ToString(), ex);
            }
            finally
            {
                System.Diagnostics.Trace.WriteLine("CreditCardPage.cs GetJSONObject() - Finally clause");
                TraceLogConnectorServiceWebAdapter.InsertEndpointTraceLog(UPSGlobalLogID, "PaymentUIConnector", "GetAPIPaymentJson", "EXIT PaymentUIConnector jump page", null, 0, Spark.Common.TraceLogConnectorService.UPSEndpointProcessingStatus.NONE, DateTime.Now);
            }		
        }

        #region OLD PAYMENT JSON 
        private PaymentJson GetPaymentJson()
        {
            System.Diagnostics.Trace.WriteLine("Start CreditCardPage.cs GetJSONObject()");
            SettingsManager settingsManager = new SettingsManager();

            if (version == Constants.NULL_INT)
                throw new Exception("Version cannot be null.");

            if (templateID == Constants.NULL_INT)
                throw new Exception("TemplateID cannot be null.");

            if (upsLegacyDataID == Constants.NULL_INT)
                throw new Exception("UPS Legacy Data ID cannot be null.");

            Dictionary<string, string> colDataAttributes = new Dictionary<string, string>();

            try
            {
                if (g.TargetMember == null)
                    throw new Exception("Member cannot null.");

                TraceLogConnectorServiceWebAdapter.InsertEndpointTraceLog(UPSGlobalLogID, "PaymentUIConnector", "GetJSONObject", "ENTER PaymentUIConnector jump page", null, 1, Spark.Common.TraceLogConnectorService.UPSEndpointProcessingStatus.NONE, DateTime.Now);
                System.Diagnostics.Trace.WriteLine("CreditCardPage.cs GetJSONObject() - Create new PaymentJson()");
                int memberID = g.TargetMember.MemberID;

                PaymentJson payment = new PaymentJson();

                payment.UPSLegacyDataID = upsLegacyDataID;
                payment.UPSGlobalLogID = UPSGlobalLogID;
                payment.GetCustomerPaymentProfile = (paymentType == PaymentType.CreditCard && !_promo.DisableOneClick) ? 1 : 0;
                payment.Version = version;
                payment.CallingSystemID = g.TargetBrand.Site.SiteID;

                payment.IsSubscriptionConfirmationEmailEnabled = settingsManager.GetSettingBool(SettingConstants.ENABLE_SUBSCRIPTION_CONFIRMATION_EMAIL, g.Brand);

                if ((_promo.UPSTemplateID != Constants.NULL_INT) && (_promo.UPSTemplateID != 0))
                    payment.TemplateID = _promo.UPSTemplateID;
                else
                    payment.TemplateID = templateID;
                payment.TimeStamp = DateTime.Now;

                if (g.Page.Request.UrlReferrer != null)
                    payment.Data.Navigation.CancelURL = g.Page.Request.UrlReferrer.AbsoluteUri;
                else
                    payment.Data.Navigation.CancelURL = "http://" + g.Page.Request.Url.Host;
                payment.Data.Navigation.ReturnURL = "http://" + g.Page.Request.Url.Host;
                // Temporary fix until SLL cert is deployed
                payment.Data.Navigation.ConfirmationURL = "http://" + g.Page.Request.Url.Host + "/Applications/Subscription/SubscriptionConfirmation.aspx";

                if (_promo.GiftTypeID != Constants.NULL_INT)
                {
                    //For now, all gift subscriptions should ultimately end up on the manage gifts page
                    payment.Data.Navigation.DestinationURL = Context.Server.UrlEncode("/Applications/MemberServices/ManageGifts.aspx");
                }
                else
                {
                    payment.Data.Navigation.DestinationURL = (Context.Request["DestinationURL"] != null)
                                                                 ? Context.Server.UrlEncode(
                                                                     Convert.ToString(Context.Request["DestinationURL"]))
                                                                 : string.Empty;
                }

                int memberSubscriptionStatus = FrameworkGlobals.GetMemberSubcriptionStatus(_g.TargetMember, _g.TargetBrand);
                SubscriptionStatus enumSubscriptionStatus = (SubscriptionStatus)memberSubscriptionStatus;

                System.Diagnostics.Trace.WriteLine("CreditCardPage.cs GetJSONObject() - Start Order Attributes");
                // Order Attributes
                OrderAttributes = new Dictionary<string, string>();
                string evar6;
                string originalPrtId = GetOriginalPrtid(out evar6);

                //Commenting this line. MPR-3423 - storing original prtid to Orderattributes and appending additional looping prtids suchs as 'Dont Go' in a separate node.
                //OrderAttributes.Add("PRTID", (Context.Request["prtid"] != null) ? Convert.ToString(Context.Request["prtid"]).Replace(",", ";") : string.Empty);
                OrderAttributes.Add("PRTID", originalPrtId);
                string secondaryPrtIds = string.Empty;
                if (evar6.Contains('-'))
                {
                    secondaryPrtIds = evar6.Substring(evar6.IndexOf('-') + 1);
                }
                OrderAttributes.Add("SecondaryPRTIDs", secondaryPrtIds.Replace('-', '|'));

                OrderAttributes.Add("SRID", (Context.Request["srid"] != null) ? Convert.ToString(Context.Request["srid"]).Replace(",", ";") : string.Empty);
                OrderAttributes.Add("PricingGroupID", _promo.PromoID.ToString());
                OrderAttributes.Add("OriginalSubscriptionStatusBeforeUpsale", FrameworkGlobals.GetAbbreviatedMemberSubscriptionStatusForOmniture(enumSubscriptionStatus));
                OrderAttributes.Add("OriginalTemplateIDBeforeUpsale", Convert.ToString(payment.TemplateID));

                try
                {
                    OrderAttributes.Add(Key.TrackingLastApplication.ToString(), ((int)Spark.Common.MemberLevelTracking.Application.FullSite).ToString());
                    OrderAttributes.Add(Key.TrackingLastOs.ToString(), MemberLevelTrackingHelper.GetOs(HttpContext.Current.Request));

                    var bc = HttpContext.Current.Request.Browser;

                    OrderAttributes.Add("DeviceProperties_IsMobileDevice", bc.IsMobileDevice.ToString().ToLower());
                    OrderAttributes.Add("DeviceProperties_IsTablet", (bc["is_tablet"] == null ? "false" : bc["is_tablet"].ToLower()));
                    OrderAttributes.Add("DeviceProperties_Manufacturer", bc.MobileDeviceManufacturer);
                    OrderAttributes.Add("DeviceProperties_Model", bc.MobileDeviceModel);
                    OrderAttributes.Add("DeviceProperties_ScreenPixelsHeight", bc.ScreenPixelsHeight.ToString());
                    OrderAttributes.Add("DeviceProperties_ScreenPixelsWidth", bc.ScreenPixelsWidth.ToString());
                }
                catch (Exception ex)
                {
                    g.ProcessException(new Exception("Error setting up member level tracking variables.", ex));
                }

                foreach (string key in OrderAttributes.Keys)
                {
                    if (OrderAttributes[key] != string.Empty)
                        payment.Data.OrderAttributes += key + "=" + OrderAttributes[key] + ",";
                }

                if (payment.Data.OrderAttributes.Length > 0)
                    payment.Data.OrderAttributes = payment.Data.OrderAttributes.TrimEnd(',');

                System.Diagnostics.Trace.WriteLine("CreditCardPage.cs GetJSONObject() - Start Member Info");
                // Member Info
                payment.Data.MemberInfo.CustomerID = g.TargetMemberID;
                payment.Data.MemberInfo.RegionID = g.TargetMember.GetAttributeInt(g.TargetBrand, "RegionID");
                if (payment.Data.MemberInfo.RegionID == Constants.NULL_INT
                    || payment.Data.MemberInfo.RegionID < 0)
                {
                    // There must be a valid RegionID to price the cart so get it from the database if the cache does not have a valid RegionID  
                    Member.ServiceAdapters.Member refreshedMember = MemberSA.Instance.GetMember(35, MemberLoadFlags.IngoreSACache);
                    payment.Data.MemberInfo.RegionID = refreshedMember.GetAttributeInt(g.TargetBrand, "RegionID");
                }
                payment.Data.MemberInfo.IPAddress = g.ClientIP;
                payment.Data.MemberInfo.Language = ((Language)Enum.Parse(typeof(Language), g.TargetBrand.Site.LanguageID.ToString())).ToString();
                RegionLanguage regionLanguage = RegionSA.Instance.RetrievePopulatedHierarchy(payment.Data.MemberInfo.RegionID, (int)Language.English);
                if (regionLanguage.CountryRegionID == ConstantsTemp.REGIONID_USA)
                    payment.Data.MemberInfo.State = regionLanguage.StateAbbreviation.Trim().ToUpper();

                //LC: Adding extra fields needed for Kount RIS
                payment.Data.MemberInfo.EmailAddress = g.Member.GetAttributeText(g.Brand, "EmailAddress");
                payment.Data.MemberInfo.GenderMask = SubscriberManager.Instance.GetGender(g.Member.GetAttributeInt(g.Brand, "gendermask"));
                payment.Data.MemberInfo.BirthDate = g.Member.GetAttributeDate(g.Brand, "birthdate", DateTime.MinValue);
                payment.Data.MemberInfo.BrandInsertDate = g.Member.GetAttributeDate(g.Brand, "BrandInsertDate", DateTime.MinValue);

                #region UDFS
                //JS-1228 LC: Adding UDF fields needed for Kount
                payment.Data.MemberInfo.UserName = g.Member.GetUserName(g.Brand);
                payment.Data.MemberInfo.MaritalStatus = Option.GetDescription("MaritalStatus", g.Member.GetAttributeInt(g.Brand, "MaritalStatus"), g);
                payment.Data.MemberInfo.Occupation = g.Member.GetAttributeText(g.Brand, "OccupationDescription");
                payment.Data.MemberInfo.Education = Option.GetDescription("EducationLevel", g.Member.GetAttributeInt(g.Brand, "EducationLevel"), g);
                payment.Data.MemberInfo.PromotionID = g.Member.GetAttributeInt(g.Brand, "PromotionID").ToString();
                payment.Data.MemberInfo.Eyes = Option.GetDescription("EyeColor", g.Member.GetAttributeInt(g.Brand, "EyeColor"), g);
                payment.Data.MemberInfo.Hair = Option.GetDescription("HairColor", g.Member.GetAttributeInt(g.Brand, "HairColor"), g);
                payment.Data.MemberInfo.Height = g.Member.GetAttributeInt(g.Brand, "Height").ToString();

                if (g.Brand.Site.Community.CommunityID == (int)WebConstants.COMMUNITY_ID.JDate)
                {
                    payment.Data.MemberInfo.Ethnicity = Option.GetDescription("JDateEthnicity", g.Member.GetAttributeInt(g.Brand, "JDateEthnicity"), g);
                    payment.Data.MemberInfo.Religion = Option.GetDescription("JDateReligion", g.Member.GetAttributeInt(g.Brand, "JDateReligion"), g);
                }
                else
                {
                    payment.Data.MemberInfo.Ethnicity = Option.GetDescription("Ethnicity", g.Member.GetAttributeInt(g.Brand, "Ethnicity"), g);
                    payment.Data.MemberInfo.Religion = Option.GetDescription("Religion", g.Member.GetAttributeInt(g.Brand, "Religion"), g);
                }

                string aboutMe = g.Member.GetAttributeText(g.Brand, "AboutMe");
                payment.Data.MemberInfo.AboutMe = aboutMe.Length < 251 ? aboutMe : aboutMe.Substring(0, 250);

                #endregion UDF
                
                System.Diagnostics.Trace.WriteLine("CreditCardPage.cs GetJSONObject() - Start Promo Info");
                // Promo Info
                PaymentJsonArray promoInfo = new PaymentJsonArray();
                promoInfo.Add("PromoID", _promo.PromoID);
                promoInfo.Add("PageTitle", _promo.PageTitle);
                var promoEndDateLocalTime = _promo.EndDate.ToUniversalTime().AddHours(g.Brand.Site.GMTOffset);

                var promoEndDateLocalTimeFormatted = promoEndDateLocalTime.ToString("d",
                    g.Brand.Site.CultureInfo.DateTimeFormat);
                promoInfo.Add("EndDate", promoEndDateLocalTimeFormatted);
                payment.Data.PromoInfo.Add(promoInfo);

                System.Diagnostics.Trace.WriteLine("CreditCardPage.cs GetJSONObject() - Start Page Info");
                // Page Info
                PaymentJsonArray pageInfo = new PaymentJsonArray();
                pageInfo.Add("PRTTitle", this.GetPRTTitle());
                payment.Data.PageInfo.Add(pageInfo);

                #region Omniture Variables
                System.Diagnostics.Trace.WriteLine("CreditCardPage.cs GetJSONObject() - Start Omniture Variables");
                PaymentJsonArray omnitureVariables = new PaymentJsonArray();

                omnitureVariables.Add("linkInternalFilters", g.AnalyticsOmniture.S_LinkInternalFilters);
                omnitureVariables.Add("pageName", "Subscribe - SubscribeNew");
                omnitureVariables.Add("prop17", OmnitureHelper.OmnitureHelper.GetProfileCompetionPercentage(_g.TargetMember, _g.TargetBrand));
                omnitureVariables.Add("prop18", OmnitureHelper.OmnitureHelper.GetGender(_g.TargetMember.GetAttributeInt(_g.TargetBrand, Matchnet.Web.Framework.WebConstants.ATTRIBUTE_NAME_GENDERMASK)));
                omnitureVariables.Add("prop19", OmnitureHelper.OmnitureHelper.GetAge(_g.TargetMember.GetAttributeDate(_g.TargetBrand, Matchnet.Web.Framework.WebConstants.ATTRIBUTE_NAME_BIRTHDATE)).ToString());
                if (_g.TargetBrand.Site.Community.CommunityID == (int)Matchnet.Web.Framework.WebConstants.COMMUNITY_ID.JDate)
                {
                    omnitureVariables.Add("prop20", MemberProfile.ProfileDisplayHelper.GetOptionValue(_g.TargetMember, _g, "JDateEthnicity", "JDateEthnicity"));
                }
                else
                {
                    omnitureVariables.Add("prop20", MemberProfile.ProfileDisplayHelper.GetOptionValue(_g.TargetMember, _g, "Ethnicity", "Ethnicity"));
                }
                omnitureVariables.Add("prop21", OmnitureHelper.OmnitureHelper.GetRegionString(_g.TargetMember, _g.TargetBrand, _g.TargetBrand.Site.LanguageID, false, true, false).Replace("\"", string.Empty));
                omnitureVariables.Add("prop23", _g.TargetMember.MemberID.ToString());
                omnitureVariables.Add("events", "event9");
                if (_promo.PromoPlans == null)
                    throw new Exception("PromoPlanCollection cannot be null. PromoID:" + _promo.PromoID.ToString());
                omnitureVariables.Add("eVar3", GetPlanIDListForOmniture(_promo.PromoPlans));
                omnitureVariables.Add("eVar4", _promo.PromoID);
                omnitureVariables.Add("eVar44", string.Format(WebConstants.OMNITURE_EVAR44_MEMBERID, (_g.TargetMember.MemberID % 10).ToString()));

                omnitureVariables.Add("eVar6", evar6);
                omnitureVariables.Add("eVar7", (g.RecipientMemberID != 0)
                                                   ? g.RecipientMemberID.ToString()
                                                   : string.Empty);

                omnitureVariables.Add("eVar8", FrameworkGlobals.GetAbbreviatedMemberSubscriptionStatusForOmniture(enumSubscriptionStatus));

                string paymentUIURL = (RuntimeSettings.GetSetting("UPS_PAYMENT_UI_URL", g.TargetBrand.Site.Community.CommunityID,
                            g.TargetBrand.Site.SiteID, g.TargetBrand.BrandID));

                paymentUIURL += g.TargetBrand.Site.Name.Replace(".", String.Empty).ToLower();
                omnitureVariables.Add("prop10", paymentUIURL);
                omnitureVariables.Add("prop14", g.AnalyticsOmniture.Prop14);
                omnitureVariables.Add("prop29", g.AnalyticsOmniture.Prop29);

                omnitureVariables.Add("evar11", g.Session[WebConstants.SESSION_PROPERTY_NAME_PROMOTIONID]);
                omnitureVariables.Add("evar12", g.Session[WebConstants.SESSION_PROPERTY_NAME_EID]);
                omnitureVariables.Add("evar13", g.Session[WebConstants.SESSION_PROPERTY_NAME_LUGGAGE]);

                omnitureVariables.Add("eVar31", Convert.ToString(payment.TemplateID));

                // passing the mbox name if it's present for this site
                string mBoxNamePrefix = (RuntimeSettings.GetSetting("T_N_T_SUB_CONFIRM_MBOX_NAME_PREFIX", g.TargetBrand.Site.Community.CommunityID,
                            g.TargetBrand.Site.SiteID, g.TargetBrand.BrandID));

                if (mBoxNamePrefix == null)
                    mBoxNamePrefix = string.Empty;

                omnitureVariables.Add("mboxname", mBoxNamePrefix == string.Empty ? string.Empty : mBoxNamePrefix + "_sub_new");

                payment.Data.OmnitureVariables.Add(omnitureVariables);

                #endregion

                System.Diagnostics.Trace.WriteLine("CreditCardPage.cs GetJSONObject() - Start Legacy Data Info");
                // Legacy Data Info
                if (g.Member.MemberID != g.TargetMemberID)
                    payment.Data.LegacyDataInfo.AdminMemberID = g.Member.MemberID;
                payment.Data.LegacyDataInfo.Member = g.TargetMemberID;
                payment.Data.LegacyDataInfo.BrandID = g.TargetBrand.BrandID;
                payment.Data.LegacyDataInfo.SiteID = g.TargetBrand.Site.SiteID;

                #region Header Template Collections
                System.Diagnostics.Trace.WriteLine("CreditCardPage.cs GetJSONObject() - Start Header Template Collections");
                // Column Header Template Collection
                foreach (HeaderResourceTemplate template in _promo.ColHeaderResourceTemplateCollection)
                {
                    PaymentJsonArray headerTemplate = new PaymentJsonArray();

                    string content = ResourceTemplateSA.Instance.GetResource(template.ResourceTemplateID, new ColumnHeaderTokenReplacer());
                    headerTemplate.Add("ResourceTemplateContent", content);
                    headerTemplate.Add("HeaderNumber", template.HeaderNumber);

                    payment.Data.ColHeaderTemplates.Add(headerTemplate);
                }

                // Row Header Template Collection
                foreach (RowHeaderResourceTemplate template in _promo.RowHeaderResourceTemplateCollection)
                {
                    PaymentJsonArray headerTemplate = new PaymentJsonArray();

                    string content = ResourceTemplateSA.Instance.GetResource(template.ResourceTemplateID, new RowHeaderTokenReplacer(template.Duration));
                    headerTemplate.Add("ResourceTemplateContent", content);
                    headerTemplate.Add("HeaderNumber", template.HeaderNumber);

                    payment.Data.RowHeaderTemplates.Add(headerTemplate);
                }

                #endregion

                #region Add Promo Plans
                System.Diagnostics.Trace.WriteLine("CreditCardPage.cs GetJSONObject() - Start Add Promo Plans");
                // Promo Plans
                foreach (PromoPlan promoPlan in _promo.PromoPlans)
                {
                    // Admin Selected
                    if (SelectedPlanID > 0)
                    {
                        if (SelectedPlanID != promoPlan.PlanID)
                        {
                            // Not Available Package...
                            PaymentJsonPackage dummy = new PaymentJsonPackage();
                            dummy.ID = 0;
                            dummy.Description = "";
                            dummy.Items.Add("EnabledForMember", false);
                            dummy.Items.Add("ResourceTemplateContent",
                                            "<div class=\"cell-plan sub-none\"><p>Not available</p></div>");
                            dummy.Items.Add("ListOrder", promoPlan.ListOrder);
                            dummy.Items.Add("ColumnGroup", promoPlan.ColumnGroup);

                            payment.Data.Packages.Add(dummy);

                            continue;
                        }
                    }

                    PaymentJsonPackage package = new PaymentJsonPackage();

                    package.ID = promoPlan.PlanID;
                    package.Description = "";
                    package.SytemID = g.TargetBrand.Site.SiteID;
                    package.StartDate = promoPlan.StartDate;
                    package.ExpiredDate = promoPlan.EndDate;
                    package.CurrencyType = PaymentUIConnector.GetCurrencyAbbreviation(promoPlan.CurrencyType);

                    string content = ResourceTemplateSA.Instance.GetResource(promoPlan.ResourceTemplateID, new PlanTokenReplacer(promoPlan, 2));
                    package.Items.Add("ResourceTemplateContent", content);
                    package.Items.Add("EnabledForMember", promoPlan.EnabledForMember.ToString());
                    package.Items.Add("InitialCost", promoPlan.InitialCost);
                    package.Items.Add("InitialCostPerDuration", promoPlan.InitialCostPerDuration);
                    package.Items.Add("ListOrder", promoPlan.ListOrder);
                    package.Items.Add("ColumnGroup", promoPlan.ColumnGroup);
                    package.Items.Add("PlanID", promoPlan.PlanID);
                    package.Items.Add("CurrencyType", PaymentUIConnector.GetCurrencyAbbreviation(promoPlan.CurrencyType));
                    package.Items.Add("BestValueFlag", promoPlan.BestValueFlag);
                    package.Items.Add("DefaultPackage", (promoPlan.PlanID == _promo.DefaultPlanID) ? true : false);
                    package.Items.Add("PaymentTypeMask", promoPlan.PaymentTypeMask);
                    package.Items.Add("PlanResourcePaymentTypeID", promoPlan.PlanResourcePaymentTypeID);
                    package.Items.Add("CreditAmount", promoPlan.CreditAmount);
                    package.Items.Add("PurchaseMode", promoPlan.PurchaseMode.ToString());
                    package.Items.Add("PlanServices", promoPlan.PlanServices);
                    package.Items.Add("IsPurchaseAllowedWithRemainingCredit", promoPlan.EnabledForMember);
                    package.Items.Add("RenewCost", promoPlan.RenewCost);
                    payment.Data.Packages.Add(package);
                };

                #endregion

                System.Diagnostics.Trace.WriteLine("CreditCardPage.cs GetJSONObject() - Check for dummy packages");
                foreach (PaymentJsonArray col in payment.Data.ColHeaderTemplates)
                {
                    int colNumber = Convert.ToInt32(col["HeaderNumber"]);

                    foreach (PaymentJsonArray row in payment.Data.RowHeaderTemplates)
                    {
                        int rowNumber = Convert.ToInt32(row["HeaderNumber"]);

                        if (!DoesPackageExistAtRowCol(rowNumber, colNumber, payment.Data.Packages))
                        {
                            // Not Available Package...
                            PaymentJsonPackage dummy = new PaymentJsonPackage();
                            dummy.ID = 0;
                            dummy.Description = "";
                            dummy.Items.Add("EnabledForMember", false);
                            dummy.Items.Add("ResourceTemplateContent",
                                            "<div class=\"cell-plan sub-none\"><p>Not available</p></div>");
                            dummy.Items.Add("ListOrder", rowNumber);
                            dummy.Items.Add("ColumnGroup", colNumber);

                            payment.Data.Packages.Add(dummy);
                        }
                    }
                }

                System.Diagnostics.Trace.WriteLine("CreditCardPage.cs GetJSONObject() - Start colDataAttributes.Add()");
                colDataAttributes.Add("UPSLegacyDataID", Convert.ToString(upsLegacyDataID));
                colDataAttributes.Add("CustomerID", Convert.ToString(memberID));
                colDataAttributes.Add("CallingSystemID", Convert.ToString(g.TargetBrand.Site.SiteID));
                colDataAttributes.Add("TransactionType", "1");
                colDataAttributes.Add("PRTID", OrderAttributes["PRTID"]);
                colDataAttributes.Add("SRID", OrderAttributes["SRID"]);
                colDataAttributes.Add("PricingGroupID", OrderAttributes["PricingGroupID"]);
                colDataAttributes.Add("RegionID", Convert.ToString(payment.Data.MemberInfo.RegionID));
                colDataAttributes.Add("IPAddress", payment.Data.MemberInfo.IPAddress);
                colDataAttributes.Add("TemplateID", Convert.ToString(payment.TemplateID));
                colDataAttributes.Add("CancelURL", payment.Data.Navigation.CancelURL);
                colDataAttributes.Add("ConfirmationURL", payment.Data.Navigation.ConfirmationURL);
                colDataAttributes.Add("DestinationURL", payment.Data.Navigation.DestinationURL);
                colDataAttributes.Add("PlanIDList", GetPlanIDListForOmniture(_promo.PromoPlans));
                colDataAttributes.Add("PaymentUIConnectorType", "CreditCardPage");

                TraceLogConnectorServiceWebAdapter.InsertEndpointTraceLog(UPSGlobalLogID, "PaymentUIConnector", "GetPaymentJson", "Save data attributes", colDataAttributes, Constants.NULL_INT, Spark.Common.TraceLogConnectorService.UPSEndpointProcessingStatus.NONE, DateTime.Now);
                TraceLogConnectorServiceWebAdapter.InsertEndpointTraceLog(UPSGlobalLogID, "PaymentUIConnector", "GetPaymentJson", "NO ERROR", null, Constants.NULL_INT, Spark.Common.TraceLogConnectorService.UPSEndpointProcessingStatus.OK, DateTime.Now);


                return payment;
            }
            catch (Exception ex)
            {
                TraceLogConnectorServiceWebAdapter.InsertEndpointTraceLog(UPSGlobalLogID, "PaymentUIConnector", "GetPaymentJson", "Save data attributes", colDataAttributes, Constants.NULL_INT, Spark.Common.TraceLogConnectorService.UPSEndpointProcessingStatus.NONE, DateTime.Now);
                TraceLogConnectorServiceWebAdapter.InsertEndpointTraceLog(UPSGlobalLogID, "PaymentUIConnector", "GetPaymentJson", "ERROR DETECTED: " + ex.Message, null, Constants.NULL_INT, Spark.Common.TraceLogConnectorService.UPSEndpointProcessingStatus.ERROR, DateTime.Now);

                throw new Exception("Error in building JSON for PaymentUI. MemberID:" +
                    g.TargetMember.MemberID.ToString(), ex);
            }
            finally
            {
                System.Diagnostics.Trace.WriteLine("CreditCardPage.cs GetPaymentJson() - Finally clause");
                //TraceLogConnectorServiceWebAdapter.GetProxyInstance().InsertEndpointTraceLog(UPSGlobalLogID, "PaymentUIConnector", "GetJSONObject", "Save data attributes", colDataAttributes, Constants.NULL_INT, Spark.Common.TraceLogConnectorService.UPSEndpointProcessingStatus.NONE, DateTime.Now);
                TraceLogConnectorServiceWebAdapter.InsertEndpointTraceLog(UPSGlobalLogID, "PaymentUIConnector", "GetPaymentJson", "EXIT PaymentUIConnector jump page", null, 0, Spark.Common.TraceLogConnectorService.UPSEndpointProcessingStatus.NONE, DateTime.Now);
            }		
        }

        private bool DoesPackageExistAtRowCol(int row, int col, PaymentJsonPackages packages)
        {
            foreach (PaymentJsonPackage package in packages)
            {
                if (Convert.ToInt32(package.Items["ColumnGroup"]) == col &&
                    Convert.ToInt32(package.Items["ListOrder"]) == row)
                {
                    return true;
                }
            }

            return false;
        }

        private string GetPlanIDListForOmniture(Matchnet.PromoEngine.ValueObjects.PromoPlanCollection plans)
        {
            System.Text.StringBuilder planIDList = new System.Text.StringBuilder();

            foreach (Purchase.ValueObjects.Plan displayedPlan in plans)
            {
                if (displayedPlan != null)
                {
                    if (displayedPlan.PlanID > 0)
                    {
                        planIDList.Append(displayedPlan.PlanID.ToString() + ",");
                    }
                }
            }

            return planIDList.ToString().TrimEnd(',');
        }
        #endregion


        #region Private Methods

        private void InitPromo()
        {
            InitPromo(Constants.NULL_INT);
        }

        private void InitPromo(int promoID)
        {
            //if (!UseAPIForPaymentJSON())  unfortunatly we need the promoID for the session so we comment out this check. we are scraping this code in the redesign
            {
                if (g.TargetMember == null)
                    throw new Exception("Member cannot be null.");

                if (promoID != Constants.NULL_INT)
                {
                    _promo = PromoEngineSA.Instance.GetPromoByID(promoID, g.TargetBrand.BrandID, g.TargetMember.MemberID);
                }
                else if (g.PromoPlan.Promo != null)
                {
                    if (g.PromoPlan.FailedValidation)
                    {
                        g.Notification.Clear();
                        g.Notification.AddError("PROMOPLAN_EXPIRED");
                        throw new Exception("Promo Plan Expired.");
                    }
                    else
                    {
                        _promo = g.PromoPlan.Promo;
                    }
                }
                else if (Context.Request["PromoID"] == null)
                {
                    _promo = PromoEngineSA.Instance.GetMemberPromo(g.TargetMember.MemberID, g.TargetBrand, paymentType);
                }
                else
                {
                    _promo = PromoEngineSA.Instance.GetPromoByID(Convert.ToInt32(Context.Request["PromoID"]), g.TargetBrand.BrandID, g.TargetMember.MemberID, false);
                }

                if (_promo == null)
                {
                    throw new Exception("Promo cannot be null.");
                }
            }
        }

        private string GetPRTTitle()
        {
            object subpage = this.Page;

            if (g.Brand.BrandID == (int)WebConstants.BRAND_ID.JDate)
            {
                return GetPRTTitle20(subpage);
            }
            else
            {
                return GetPRTTitleOld(subpage);
            }
        }
        
        /// <summary>
        /// This is copied from SubscribeNew => RenderPrtIdBasedText
        /// </summary>
        private string GetPRTTitleOld(object subpage)
        {
            try
            {
                // PRTID might return 17,17 if more than one prtid parameter in query string.
                string prtID = Context.Request.QueryString["prtid"];
                if (prtID.IndexOf(",") > -1)
                {
                    prtID = prtID.Split(',')[0];
                }

                // Is PrtId availble?
                if (prtID != null && prtID != "")
                {
                    switch (prtID)
                    {
                        case "13":
                        case "21":
                        case "23":
                            int memberOnline = Matchnet.Session.ServiceAdapters.SessionSA.Instance.GetSessionCount(g.Brand.Site.Community.CommunityID);
                            g.ExpansionTokens.Add("MOLCOUNT", memberOnline.ToString("n0"));
                            return g.GetResource("PRTID" + prtID, subpage);
                            break;

                        case "9":
                        case "10":
                        case "12":
                        case "30":
                            // These prt ids show member inbox message count.
                            if (base.g.Member != null && base.g.Member.MemberID > 0)
                            {
                                // Get total mesasge count.
                                int msgCount = base.g.Member.GetAttributeInt(base.g.Brand, "CommunityEmailCount");

                                if (msgCount == 0)
                                {
                                    // Show default.
                                    return g.GetResource("PRTIDDEFAULT", subpage);
                                }
                                else if (msgCount == 1)
                                {
                                    // Show one message global value.
                                    return g.GetResource("PRTIDONEMESSAGE" + prtID, subpage);
                                }
                                else
                                {
                                    // Show custom.
                                    g.ExpansionTokens.Add("MESSAGECOUNT", msgCount.ToString());
                                    return g.GetResource("PRTID" + prtID, subpage);
                                }
                            }
                            break;

                        case "27":
                        case "20":
                        case "19":
                        case "5":
                        case "4":
                        case "1":
                            // Show default text for missing prtid.
                            return g.GetResource("PRTIDDEFAULT", subpage);
                            break;
                        default:
                            /*
                             * this is polish way to check if a resource is there,
                             * but its the only way I know of without modifying the resource code
                             * */
                            string resourceStr = g.GetResource("PRTID" + prtID, subpage);
                            if (resourceStr.IndexOf("PRTID" + prtID) > -1)
                                return g.GetResource("PRTIDDEFAULT", subpage);

                            // Show custom.
                            return g.GetResource("PRTID" + prtID, subpage);
                    }
                }
                else
                {
                    // Show default text if prtid not available.
                    return g.GetResource("PRTIDDEFAULT", subpage);
                }
            }
            catch
            {
                return g.GetResource("PRTIDDEFAULT", subpage);
            }

            return null;
        }


        /// <summary>
        /// This is copied from SubscribeNew20 => RenderPrtIdBasedNotification
        /// </summary>
        /// <returns></returns>
        private string GetPRTTitle20(object subpage)
        {
            try
            {
                // PRTID might return 17,17 if more than one prtid parameter in query string.
                string prtID = Context.Request.QueryString["prtid"];
                string resourceConst = "PRTIDDEFAULT";    
                string resource = g.GetResource(resourceConst, subpage);

                if (prtID.IndexOf(",") > -1)
                {
                    prtID = prtID.Split(',')[0];
                }
                if (prtID != null && prtID != "")
                {

                    int msgCount = -1;
                    resourceConst = "PRTID" + prtID;
                    resource = g.GetResource(resourceConst, subpage);
                    if (resource.ToLower().IndexOf(resourceConst) > -1)
                    {
                        resourceConst = "PRTIDDEFAULT";
                        return resourceConst;
                    }

                    if (resource.IndexOf("((MOLCOUNT))") > -1)
                    {
                        int memberOnline = Matchnet.Session.ServiceAdapters.SessionSA.Instance.GetSessionCount(g.Brand.Site.Community.CommunityID);
                        g.ExpansionTokens.Add("MOLCOUNT", memberOnline.ToString("n0"));

                    }
                    if (resource.IndexOf("((MESSAGECOUNT))") > -1)
                    {

                        msgCount = base.g.Member.GetAttributeInt(base.g.Brand, "CommunityEmailCount");
                        if (msgCount == 0)
                        {   // Show default.
                            resourceConst = "PRTIDDEFAULT";
                        }
                        else if (msgCount == 1)
                        {
                            // Show one message global value.
                            resourceConst = "PRTIDONEMESSAGE";
                        }

                        g.ExpansionTokens.Add("MESSAGECOUNT", msgCount.ToString());
                    }
                    if (resource.IndexOf("((TOUSERNAME))") > -1)
                    {

                        string userName = string.Empty;
                        try
                        {
                            int memberID = Convert.ToInt32(Context.Request.QueryString["srid"]);
                            userName = Matchnet.Member.ServiceAdapters.MemberSA.Instance.GetMember(memberID, Member.ServiceAdapters.MemberLoadFlags.None).GetUserName(base.g.Brand);
                        }
                        catch (Exception ex)
                        {
                            ;
                        }

                        if (userName == string.Empty)
                        {   // Show default.
                            resourceConst = "PRTIDDEFAULT";
                        }
                        else
                        {
                            g.ExpansionTokens.Add("TOUSERNAME", userName);
                        }
                    }
                }
                
                return g.GetResource(resourceConst, subpage);
            }
            catch
            {
                return g.GetResource("PRTIDDEFAULT", subpage);
            }
        }
        
        private string GetOriginalPrtid(out string evar6)
        {
            evar6 = string.Empty;
            string prtId = (Context.Request["prtid"] != null)
                                   ? Convert.ToString(Context.Request["prtid"])
                                   : string.Empty;
            if (prtId.IndexOf(",") > -1)
            {
                prtId = prtId.Split(',')[0];
            }
            if(!string.IsNullOrEmpty(prtId))
            {
                evar6 = prtId;
                if (!string.IsNullOrEmpty(g.Session[WebConstants.SESSION_PROPERTY_ORIGINAL_SUB_PRTID]))
                {
                    if (Convert.ToInt32(prtId) != (int)PurchaseReasonType.DontGo)
                        g.Session.Remove(WebConstants.SESSION_PROPERTY_ORIGINAL_SUB_PRTID);
                    else
                        evar6 = string.Format("{0}-{1}", g.Session[WebConstants.SESSION_PROPERTY_ORIGINAL_SUB_PRTID], prtId);
                }
                return evar6.Contains('-') ? evar6.Split(new[] {'-'})[0] : evar6;
            }
            return string.Empty;
        }
        
        #endregion

        #endregion

    }
}

