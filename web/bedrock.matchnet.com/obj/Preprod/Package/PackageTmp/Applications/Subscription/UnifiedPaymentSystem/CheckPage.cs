﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.Purchase.ValueObjects;

namespace Matchnet.Web.Applications.Subscription.UnifiedPaymentSystem
{
    public class CheckPage : CreditCardPage
    {
        public CheckPage(HttpContext context, PaymentType paymentType)
            : base(context, paymentType)
        {

        }
    }
}
