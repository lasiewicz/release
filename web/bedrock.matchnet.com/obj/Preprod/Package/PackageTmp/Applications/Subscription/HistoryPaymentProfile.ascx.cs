﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Web.Framework;
using Matchnet.Content.ValueObjects.PageConfig;
using Spark.Common.Adapter;
using Spark.Common.PaymentProfileService;

namespace Matchnet.Web.Applications.Subscription
{
    public partial class HistoryPaymentProfile : FrameworkControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (g.LayoutTemplate.Equals(LayoutTemplate.Popup))
                {
                    AlignOverrideDiv.Attributes.Add("class", "rightNewAlignOverride");
                }

                //get parameters
                int siteID = Convert.ToInt32(Request.QueryString[WebConstants.URL_PARAMETER_NAME_SITEID]);
                int targetMemberID = Convert.ToInt32(Request.QueryString[WebConstants.IMPERSONATE_MEMBERID]);
                int tranID = Convert.ToInt32(Request.QueryString[WebConstants.URL_PARAMETER_NAME_TRANID]);
                int orderID = Convert.ToInt32(Request.QueryString[WebConstants.URL_PARAMETER_NAME_ORDERID]);

                //orderID = 1001272;

                string paramString = String.Format("SiteID: {0}, TargetMemberID: {1}, TranID: {2}, OrderID: {3}", siteID.ToString(), targetMemberID.ToString(), tranID.ToString(), orderID.ToString());
                if (siteID > 0 && targetMemberID > 0 && tranID > 0 && orderID > 0)
                {
                    ObsfucatedPaymentProfileResponse paymentProfileResponse = PaymentProfileServiceWebAdapter.GetProxyInstance().GetObsfucatedPaymentProfileByOrderID(orderID, targetMemberID, siteID);
                    if (paymentProfileResponse != null && paymentProfileResponse.Code == "0" && paymentProfileResponse.PaymentProfile != null)
                    {
                        literalTranID.Text = tranID.ToString();
                        literalOrderID.Text = orderID.ToString();
                        literalOrderCode.Text = paymentProfileResponse.OrderStatusCode;
                        literalOrderStatusMessage.Text = paymentProfileResponse.OrderStatusDesc;
                        literalPaymentProfileID.Text = paymentProfileResponse.PaymentProfile.PaymentProfileID.ToString();

                        if (paymentProfileResponse.PaymentProfile is ObsfucatedCreditCardPaymentProfile)
                        {
                            phCreditCardInfo.Visible = true;
                            ObsfucatedCreditCardPaymentProfile creditCardPaymentProfile = (ObsfucatedCreditCardPaymentProfile)paymentProfileResponse.PaymentProfile;
                            literalPaymentType.Text = creditCardPaymentProfile.PaymentType;
                            ccCardType.Text = creditCardPaymentProfile.CardType;
                            ccCity.Text = creditCardPaymentProfile.City;
                            ccCountry.Text = creditCardPaymentProfile.Country;
                            ccExpMonth.Text = creditCardPaymentProfile.ExpMonth.ToString();
                            ccExpYear.Text = creditCardPaymentProfile.ExpYear.ToString();
                            ccFirstName.Text = creditCardPaymentProfile.FirstName;
                            ccLastFour.Text = creditCardPaymentProfile.LastFourDigitsCreditCardNumber;
                            ccLastName.Text = creditCardPaymentProfile.LastName;
                            ccPhone.Text = creditCardPaymentProfile.PhoneNumber;
                            ccPostal.Text = creditCardPaymentProfile.PostalCode;
                            ccState.Text = creditCardPaymentProfile.State;
                            ccStreet.Text = creditCardPaymentProfile.StreetAddress;
                            
                        }
                        else if (paymentProfileResponse.PaymentProfile is ObfuscatedCreditCardShortFormFullWebSitePaymentProfile)
                        {
                            phCreditCardInfo.Visible = true;
                            ObfuscatedCreditCardShortFormFullWebSitePaymentProfile creditCardShortFormFullWebSitePaymentProfile = (ObfuscatedCreditCardShortFormFullWebSitePaymentProfile)paymentProfileResponse.PaymentProfile;
                            literalPaymentType.Text = creditCardShortFormFullWebSitePaymentProfile.PaymentType;
                            ccCardType.Text = creditCardShortFormFullWebSitePaymentProfile.CardType;
                            ccExpMonth.Text = creditCardShortFormFullWebSitePaymentProfile.ExpMonth.ToString();
                            ccExpYear.Text = creditCardShortFormFullWebSitePaymentProfile.ExpYear.ToString();
                            ccLastFour.Text = creditCardShortFormFullWebSitePaymentProfile.LastFourDigitsCreditCardNumber;
                            ccFirstName.Text = creditCardShortFormFullWebSitePaymentProfile.FirstName;
                            ccLastName.Text = creditCardShortFormFullWebSitePaymentProfile.LastName;
                            ccPostal.Text = creditCardShortFormFullWebSitePaymentProfile.PostalCode;
                        }
                        else if (paymentProfileResponse.PaymentProfile is ObfuscatedCreditCardShortFormMobileSitePaymentProfile)
                        {
                            phCreditCardInfo.Visible = true;
                            ObfuscatedCreditCardShortFormMobileSitePaymentProfile creditCardShortFormMobileSitePaymentProfile = (ObfuscatedCreditCardShortFormMobileSitePaymentProfile)paymentProfileResponse.PaymentProfile;
                            literalPaymentType.Text = creditCardShortFormMobileSitePaymentProfile.PaymentType;
                            ccCardType.Text = creditCardShortFormMobileSitePaymentProfile.CardType;
                            ccExpMonth.Text = creditCardShortFormMobileSitePaymentProfile.ExpMonth.ToString();
                            ccExpYear.Text = creditCardShortFormMobileSitePaymentProfile.ExpYear.ToString();
                            ccLastFour.Text = creditCardShortFormMobileSitePaymentProfile.LastFourDigitsCreditCardNumber;
                            ccFirstName.Text = creditCardShortFormMobileSitePaymentProfile.FirstName;
                            ccLastName.Text = creditCardShortFormMobileSitePaymentProfile.LastName;
                            ccPostal.Text = creditCardShortFormMobileSitePaymentProfile.PostalCode;
                        }
                        else if (paymentProfileResponse.PaymentProfile is ObsfucatedCheckPaymentProfile)
                        {
                            phCheckInfo.Visible = true;
                            ObsfucatedCheckPaymentProfile checkPaymentProfile = (ObsfucatedCheckPaymentProfile)paymentProfileResponse.PaymentProfile;
                            literalPaymentType.Text = checkPaymentProfile.PaymentType;
                            ckAccountLastFour.Text = checkPaymentProfile.BankAccountNumberLastFour;
                            ckAccountType.Text = checkPaymentProfile.BankAccountType;
                            ckBankName.Text = checkPaymentProfile.BankName;
                            ckBankState.Text = checkPaymentProfile.BankState;
                            ckCity.Text = checkPaymentProfile.City;
                            ckEmail.Text = checkPaymentProfile.EmailAddress;
                            ckFirstName.Text = checkPaymentProfile.FirstName;
                            ckLastName.Text = checkPaymentProfile.LastName;
                            ckPhone.Text = checkPaymentProfile.Phone;
                            ckPostal.Text = checkPaymentProfile.Zip;
                            ckRoutingLastFour.Text = checkPaymentProfile.BankRoutingNumberLastFour;
                            ckState.Text = checkPaymentProfile.State;
                            ckStreet.Text = checkPaymentProfile.Street;
                        }
                        else if (paymentProfileResponse.PaymentProfile is ObsfucatedPaypalPaymentProfile)
                        {
                            phPaypalInfo.Visible = true;
                            ObsfucatedPaypalPaymentProfile paypalPaymentProfile = (ObsfucatedPaypalPaymentProfile)paymentProfileResponse.PaymentProfile;
                            literalPaymentType.Text = paypalPaymentProfile.PaymentType;
                            paypalFirstName.Text = paypalPaymentProfile.FirstName;
                            paypalLastName.Text = paypalPaymentProfile.LastName;
                            paypalEmail.Text = paypalPaymentProfile.Email;
                            paypalCountry.Text = paypalPaymentProfile.Country;
                            paypalPayerStatus.Text = paypalPaymentProfile.PayerStatus;
                            paypalToken.Text = paypalPaymentProfile.PaypalToken;
                            paypalBillingAgreementID.Text = paypalPaymentProfile.BillingAgreementID;
                        }
                        else if (paymentProfileResponse.PaymentProfile is ObfuscatedManualPaymentProfile)
                        {
                            phManualPaymentInfo.Visible = true;
                            ObfuscatedManualPaymentProfile manualPaymentProfile = (ObfuscatedManualPaymentProfile)paymentProfileResponse.PaymentProfile;
                            literalPaymentType.Text = manualPaymentProfile.PaymentType;
                            manualPaymentFirstName.Text = manualPaymentProfile.FirstName;
                            manualPaymentLastName.Text = manualPaymentProfile.LastName;
                        }
                        else if (paymentProfileResponse.PaymentProfile is ObfuscatedElectronicFundsTransferProfile)
                        {
                            phElectronicFundsTransferInfo.Visible = true;
                            ObfuscatedElectronicFundsTransferProfile electronicFundsTransferProfile = (ObfuscatedElectronicFundsTransferProfile)paymentProfileResponse.PaymentProfile;
                            literalPaymentType.Text = electronicFundsTransferProfile.PaymentType;
                            electronicFundsTransferAccountLastFour.Text = electronicFundsTransferProfile.BankAccountNumberLastFour;
                            electronicFundsTransferAccountType.Text = electronicFundsTransferProfile.BankAccountType;
                            //ckBankName.Text = electronicFundsTransferProfile.BankName;
                            //ckBankState.Text = electronicFundsTransferProfile.BankState;
                            electronicFundsTransferCity.Text = electronicFundsTransferProfile.City;
                            electronicFundsTransferEmail.Text = electronicFundsTransferProfile.EmailAddress;
                            electronicFundsTransferFirstName.Text = electronicFundsTransferProfile.FirstName;
                            electronicFundsTransferLastName.Text = electronicFundsTransferProfile.LastName;
                            electronicFundsTransferPhone.Text = electronicFundsTransferProfile.Phone;
                            electronicFundsTransferPostal.Text = electronicFundsTransferProfile.Zip;
                            electronicFundsTransferRoutingLastFour.Text = electronicFundsTransferProfile.BankRoutingNumberLastFour;
                            electronicFundsTransferState.Text = electronicFundsTransferProfile.State;
                            electronicFundsTransferStreet.Text = electronicFundsTransferProfile.Street;
                        }
                        else if (paymentProfileResponse.PaymentProfile is ObfuscatedPaymentReceivedProfile)
                        {
                            phPaymentReceivedInfo.Visible = true;
                            ObfuscatedPaymentReceivedProfile paymentReceivedProfile = (ObfuscatedPaymentReceivedProfile)paymentProfileResponse.PaymentProfile;
                            literalPaymentType.Text = paymentReceivedProfile.PaymentType;
                            paymentReceivedFirstName.Text = paymentReceivedProfile.FirstName;
                            paymentReceivedLastName.Text = paymentReceivedProfile.LastName;
                        }
                        else if (paymentProfileResponse.PaymentProfile is ObfuscatedDebitCardPaymentProfile)
                        {
                            phCreditCardInfo.Visible = true;
                            ObfuscatedDebitCardPaymentProfile debitCardPaymentProfile = (ObfuscatedDebitCardPaymentProfile)paymentProfileResponse.PaymentProfile;
                            literalPaymentType.Text = debitCardPaymentProfile.PaymentType;
                            ccCardType.Text = debitCardPaymentProfile.CardType;
                            ccCity.Text = debitCardPaymentProfile.City;
                            ccCountry.Text = debitCardPaymentProfile.Country;
                            ccExpMonth.Text = debitCardPaymentProfile.ExpMonth.ToString();
                            ccExpYear.Text = debitCardPaymentProfile.ExpYear.ToString();
                            ccFirstName.Text = debitCardPaymentProfile.FirstName;
                            ccLastFour.Text = debitCardPaymentProfile.LastFourDigitsCreditCardNumber;
                            ccLastName.Text = debitCardPaymentProfile.LastName;
                            ccPhone.Text = debitCardPaymentProfile.PhoneNumber;
                            ccPostal.Text = debitCardPaymentProfile.PostalCode;
                            ccState.Text = debitCardPaymentProfile.State;
                            ccStreet.Text = debitCardPaymentProfile.StreetAddress;
                        }
                        else
                        {
                            g.Notification.AddErrorString("Unknown payment profile type returned from UPS. PaymentType: " + paymentProfileResponse.PaymentProfile.PaymentType + ". " + paramString);
                        }
                    }
                    else if (paymentProfileResponse == null)
                    {
                        g.Notification.AddErrorString("GetObsfucatedPaymentProfile error. " + paramString);
                    }
                    else if (paymentProfileResponse.Code != "0")
                    {
                        g.Notification.AddErrorString("GetObsfucatedPaymentProfile error. Code: " + paymentProfileResponse.Code + ", " + paymentProfileResponse.Message + ". " + paramString);
                    }
                    else
                    {
                        g.Notification.AddErrorString("GetObsfucatedPaymentProfile did not find a payment profile in UPS. " + paramString);
                    }
                }
                else
                {
                    g.Notification.AddErrorString("Missing parameter values. " + paramString);
                }
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }
    }
}
