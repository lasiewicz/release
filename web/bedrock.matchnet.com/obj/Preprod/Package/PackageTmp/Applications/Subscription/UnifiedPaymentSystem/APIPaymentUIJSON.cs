﻿using Matchnet.Web.Framework.SparkAPI;
using Newtonsoft.Json;
using Spark.Common.RestConsumer.V2.Models.Subscription;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Matchnet.Web.Applications.Subscription.UnifiedPaymentSystem
{
    public class APIPaymentUIJSON : ApiBase
    {

        public PaymentUiPostData PostPayment(PaymentUiPostDataRequest data, string accessToken, string brandID)
        {
            string prtid = null;
            var customprtid = prtid;


            var paymentDictionary = new Dictionary<string, string>
            {
                   {"PaymentUiPostDataType", data.PaymentUiPostDataType},
                   {"CancelUrl", data.CancelUrl},
                   {"ConfirmationUrl", data.ConfirmationUrl },
                   {"DestinationUrl", data.DestinationUrl },
                   {"ReturnUrl", data.ReturnUrl},
                   {"ClientIp", data.ClientIp},
                   {"PrtTitle", data.PrtTitle},
                   {"SrId", data.SrId},
                   {"PrtId",data.PrtId},
                   {"MemberLevelTrackingLastApplication", "1"},
                   {"MemberLevelTrackingIsMobileDevice", data.MemberLevelTrackingIsMobileDevice},
                   {"MemberLevelTrackingIsTablet", data.MemberLevelTrackingIsTablet},
                   {"PromoType",data.PromoType}, 
                   {"OrderID",data.OrderID},
                   {"TemplateId", data.TemplateId},
                   {"GiftDestination", data.GiftDestinationUrl},
                   {"PremiumType", data.PremiumType}
            };

            if (data.PromoID > 0)
                paymentDictionary.Add("PromoID", data.PromoID.ToString());

            if (!String.IsNullOrEmpty(data.EID))
                paymentDictionary.Add("EID", data.EID);

            paymentDictionary.Add("OmnitureVariablesJson", JsonConvert.SerializeObject(data.OmnitureVariables));


            var response = Post<PaymentUiPostData>(accessToken, "/brandId/" + brandID + RestUrls.POST_PAYMENT, null, paymentDictionary);
            return response.Data;
        }


    }
}