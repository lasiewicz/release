using System;
using System.Web;

using Matchnet.Lib.Encryption;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Purchase.ServiceAdapters;
using Matchnet.Purchase.ValueObjects;
using Matchnet.Web.Framework;
using Matchnet.PromoEngine.ValueObjects;

namespace Matchnet.Web.Applications.Subscription
{
	/// <summary>
	/// PromoPlansHelper contains method which are used to handle promotion plans in subscription page.
	/// </summary>
	/// <remarks>
	/// 1.	Retrieves Promo plan string from cookie or query string
	/// 2.	Decrypts Promo Plan string : string strKey=Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("KEY");
	/// 3.	Parses decrypted promo plan string to extract promo 
	///		1.	Extract promo list
	/// </remarks>
	/// 
	/// 
	public class PromoPlansHelper  :  FrameworkControl
	{
		# region Private constants
			
		private const string COOKIE_KEY_PROMOPLAN="spp-urhr";
		private const string COOKIE_KEY_PROMOPLAN_VALUE="spp-evl";
		private const string COOKIE_KEY_PROMOPLAN_EXPDATE="spp-vexl";
		private const string COOKIE_EXPIRATION_DURATION="PROMOPLAN_COOKIE_EXPIRATION_MINS";
		
		#endregion

		# region private variables

		private int _intExpiryExtension;	// Number of minutes that cookie will live
		private string _strEncryptionKey;
		private Matchnet.Content.ValueObjects.BrandConfig.Brand _brand;		

		private Promo _promo;
		private bool _failedValidation;

		#endregion

		#region Constructor
		public PromoPlansHelper(string DESKey, Matchnet.Content.ValueObjects.BrandConfig.Brand brand)
		{	
			try
			{
				_strEncryptionKey=DESKey;						
				_intExpiryExtension=(Matchnet.Conversion.CInt(RuntimeSettings.GetSetting(COOKIE_EXPIRATION_DURATION,  brand.Site.Community.CommunityID, brand.Site.SiteID)));
				_brand=brand;
				_promo=null;
				_failedValidation=false;
				CapturePromoPlan();
			}
			catch
			{
			}

		}
		#endregion

		#region Public properties (readonly)		
		public bool FailedValidation
		{
			get{ return _failedValidation;}
		}
        public Promo Promo
        {
            get
            {
                return _promo;
            }
        }
		public Int32 PromoID
		{
			get
			{ 
				if(_promo!=null)
				{
					return  _promo.PromoID;
				}
				else 
				{
					return Constants.NULL_INT;
				}
			}
		}
		public int DefaultPlan
		{
			get
			{
				if(_promo!=null)
				{
					return _promo.DefaultPlanID;
				}
				else 
				{
					return Constants.NULL_INT;
				}
			}
		}
		public int BestValuePlan
		{
			get 
			{
				if(_promo!=null)
				{
					return _promo.BestValuePlanID;
				}
				else 
				{
					return Constants.NULL_INT;
				}
			}
		}
		public string PageTitle
		{
			get
			{
				string result=string.Empty;
				if(_promo!=null)
				{
					result=_promo.PageTitle;
				}
				return result;
			}
		}

		/// <summary>
		/// Returns list of validated promo plans extracted from promotional URL or cookie
		/// </summary>
		public PromoPlanCollection  GetPromoPlans
		{
			get {
                    PromoPlanCollection promoPlans = CapturePromoPlan();					
					
					if (promoPlans!=null){
						if (promoPlans.Count>0)
						{
							return promoPlans;
						}
					}
					return null;
				}
		}

		#endregion

		# region Private methods	
		/// <summary>
		///		Retrieves Promo plan string from cookie (if exits) or query string. It also populates member variables.
		/// </summary>
		/// <remarks>Each time the cookie is accessed or updated its epiration is extended by defined number of minutes. </remarks>
		/// <param name="context"></param>
		/// <returns></returns>
        private PromoPlanCollection CapturePromoPlan()
		{
			
			string strPromoPlanQ =Matchnet.Constants.NULL_STRING ; // extracted from query string
			bool blnCookieIvalid=false;
			HttpCookie PromoCookie=null;
            PromoPlanCollection ResultPromoPlans = null;
			try
			{
				// Get StrPromoPlan from query string	
				strPromoPlanQ = HttpContext.Current.Request.QueryString.Get("PromoPlan");

				if (strPromoPlanQ == Matchnet.Constants.NULL_STRING)
				{
					strPromoPlanQ ="";
				}
				// Get strPromoPlan from cookie
				PromoCookie=HttpContext.Current.Request.Cookies.Get(COOKIE_KEY_PROMOPLAN);

				// check if cookie exists
				if (PromoCookie!= null )
				{
					// compare query string strPromoPlan with cookie value                    
					if( !ContainsKey(PromoCookie.Values.AllKeys, COOKIE_KEY_PROMOPLAN_VALUE) ||
                        (PromoCookie.Values[COOKIE_KEY_PROMOPLAN_VALUE].ToLower()!=strPromoPlanQ.ToLower() && strPromoPlanQ.Length>0) )
					{
						ResultPromoPlans=GetPlansFromString(strPromoPlanQ);				
						if (!FailedValidation)						
						{
							System.Globalization.DateTimeFormatInfo dtfInfo = _brand.Site.CultureInfo.DateTimeFormat;
							// set the strPromo to query string copy
							// update the cookie with new strPromoPlan string 
							HttpContext.Current.Response.Cookies[COOKIE_KEY_PROMOPLAN][COOKIE_KEY_PROMOPLAN_VALUE]=strPromoPlanQ;
							// extend for next n minutes
							HttpContext.Current.Response.Cookies[COOKIE_KEY_PROMOPLAN][COOKIE_KEY_PROMOPLAN_EXPDATE]=DateTime.Now.AddMinutes(_intExpiryExtension).ToString(dtfInfo );
						}
						else  // pick the valid cookie in case of an invalid query string
						{
							ResultPromoPlans=GetPlansFromCookie(PromoCookie);							
							if(ResultPromoPlans==null)
							{
								blnCookieIvalid=true;
							}
						}
					}
					else
					{
						ResultPromoPlans=GetPlansFromCookie(PromoCookie);							
						if(ResultPromoPlans==null)
						{
							blnCookieIvalid=true;
						}
					}
				}
			}
			catch (Exception ex )
			{
				g.ProcessException(ex);
			}

			if(PromoCookie==null|| blnCookieIvalid)	
			{
				if (strPromoPlanQ.Length>0)
				{
					ResultPromoPlans=null;
					ResultPromoPlans=GetPlansFromString(strPromoPlanQ);				
					if (!FailedValidation)
					{
						System.Globalization.DateTimeFormatInfo dtfInfo = _brand.Site.CultureInfo.DateTimeFormat;
						//Add promoPlan string to cookies
						HttpCookie cookiePromoPlan = new HttpCookie(COOKIE_KEY_PROMOPLAN);				
						cookiePromoPlan.Values[COOKIE_KEY_PROMOPLAN_VALUE]=strPromoPlanQ;
						cookiePromoPlan.Values[COOKIE_KEY_PROMOPLAN_EXPDATE]=DateTime.Now.AddMinutes(_intExpiryExtension).ToString(dtfInfo);
						cookiePromoPlan.Expires=DateTime.Now.AddMinutes(_intExpiryExtension);
						HttpContext.Current.Response.Cookies.Add(cookiePromoPlan);
					}
				}
				

			}
			return ResultPromoPlans;
			
		}
        private PromoPlanCollection GetPlansFromCookie(HttpCookie PromoCookie)
		{
			string strPromoPlan="";
            PromoPlanCollection ResultPromoPlans = null;			
			System.Globalization.DateTimeFormatInfo dtfInfo = _brand.Site.CultureInfo.DateTimeFormat;
			DateTime dtExpiryDate =  Convert.ToDateTime(PromoCookie.Values[COOKIE_KEY_PROMOPLAN_EXPDATE],dtfInfo );
			// check if the cookie is expired
			if(DateTime.Compare(dtExpiryDate,DateTime.Now)>=0) // not expired
			{
				// set the strPromo to cookie version
				strPromoPlan=PromoCookie.Values[COOKIE_KEY_PROMOPLAN_VALUE];				
				ResultPromoPlans=GetPlansFromString(strPromoPlan);				
				if (!FailedValidation)
				{
					// Update cookie/extend for next n minutes
					HttpContext.Current.Response.Cookies[COOKIE_KEY_PROMOPLAN][COOKIE_KEY_PROMOPLAN_VALUE]=PromoCookie.Values[COOKIE_KEY_PROMOPLAN_VALUE];
					HttpContext.Current.Response.Cookies[COOKIE_KEY_PROMOPLAN][COOKIE_KEY_PROMOPLAN_EXPDATE]=DateTime.Now.AddMinutes(_intExpiryExtension).ToString(dtfInfo );
					HttpContext.Current.Response.Cookies[COOKIE_KEY_PROMOPLAN].Expires=DateTime.Now.AddMinutes(_intExpiryExtension);
				}
				else
				{
					ResultPromoPlans=null;
				}
			}
			else // expired
			{
				ResultPromoPlans=null;

			}
			
			return ResultPromoPlans;

		}
		/// <summary> Decrypts/Parses passed input string and validates the PromoID 
		/// Returns false if the Promo is expired or it's not valid.
		/// </summary>
		/// <remarks> Expected format for passed parameter (strPromoPlans) is PromoPlan=PromoID
		/// </remarks>
		/// <param name="strPromoPlans"></param>
		/// <returns></returns>
        private PromoPlanCollection GetPlansFromString(string strPromoPlans)
		{
            PromoPlanCollection ValidPrmoplans = null;
			if (strPromoPlans!=Matchnet.Constants.NULL_STRING)
			{
				try
				{
					_failedValidation=true;
					string strPromoID=Decrypt(strPromoPlans);
					if (strPromoID.Length>0)
					{
                        //09052008 TL Updated for ESP project to have promo engine return a promo with plans that contains member specific UI metadata
						_promo	= Matchnet.PromoEngine.ServiceAdapters.PromoEngineSA.Instance.GetPromoByID(Int32.Parse(strPromoID),_brand.BrandID, g.Member.MemberID);
                        if(_promo!=null)
						{
                            // Check to see if this promo was approved
                            if (_promo.PromoApprovalStatus == PromoApprovalStatus.Approved)
                            {
                                bool qualify = true;
                                // if this promo happens to be "Email with Targeting" promo, we DO NOT ignore the targeting
                                // conditions as usual
                                if (_promo.PromoType == PromoType.EmailLinkWithTargeting)
                                {
                                    qualify = PromoEngine.ServiceAdapters.PromoEngineSA.Instance.QualifyPromo(g.Member.MemberID,
                                        g.Brand, _promo);
                                }

                                if (qualify)
                                {
                                    if (_promo.PromoPlans != null)
                                    {
                                        if (_promo.PromoPlans.Count > 0)
                                        {
                                            ValidPrmoplans = _promo.PromoPlans;
                                            _failedValidation = false;
                                        }
                                    }
                                }
                            }
						}
					}					
				}
				catch (Exception ex )
				{
					g.ProcessException(ex);
				}
			}
			return ValidPrmoplans;
		}
		private string Encrypt(string unEncryptedvalue)
		{
			SymmetricalEncryption encryption = new SymmetricalEncryption(SymmetricalEncryption.Provider.DES);

			return encryption.Encrypt(unEncryptedvalue, _strEncryptionKey);
		}

		private string Decrypt(string encryptedValue)
		{
			SymmetricalEncryption encryption = new SymmetricalEncryption(SymmetricalEncryption.Provider.DES);

			return encryption.Decrypt(encryptedValue, _strEncryptionKey);
		}
        /// <summary>
        /// Method used to look through the keys to see if a certain key value exists.
        /// </summary>
        /// <param name="keys"></param>
        /// <param name="keyValue"></param>
        /// <returns></returns>
        private bool ContainsKey(string[] keys, string keyValue)
        {
            bool keyExists = false;

            for (int i = 0; i < keys.Length; i++)
            {
                if (keys[i] != null)
                {
                    if (keys[i].ToLower() == keyValue.ToLower())
                    {
                        keyExists = true;
                        break;
                    }
                }
            }

            return keyExists;
        }
		# endregion
	}
}
