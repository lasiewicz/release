﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="History20.ascx.cs"
    Inherits="Matchnet.Web.Applications.Subscription.History20" %>
<%@ Register TagPrefix="uc2" TagName="PaymentHistory" Src="Controls/PaymentHistory20.ascx" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="FE" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<div id="page-container">
    <uc2:PaymentHistory ID="PaymentHistory" runat="server">
    </uc2:PaymentHistory>
</div>
