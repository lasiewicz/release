using System;
using System.Collections.Specialized;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Matchnet.Web.Framework;
using Matchnet.Lib;
using Matchnet.Web.Applications.Subscription.Controls;

using Matchnet.Content.ValueObjects.PageConfig;
using Matchnet.Purchase.ServiceAdapters;
using Matchnet.Purchase.ValueObjects;
using Matchnet.Session.ValueObjects;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Configuration.ServiceAdapters;


namespace Matchnet.Web.Applications.Subscription
{
    public class SubscribeCommon : FrameworkControl
    {
        public const string SUBSCRIPTION_ENCRYPT_KEY = "m0nk$ies";
 
        public SubscribeCommon()
        {
        }

        public string EncryptNumber(int number)
        {
            Matchnet.Lib.Encryption.SymmetricalEncryption encryptMemberTranID = new Matchnet.Lib.Encryption.SymmetricalEncryption(Matchnet.Lib.Encryption.SymmetricalEncryption.Provider.DES);
            return encryptMemberTranID.Encrypt(number.ToString(), SUBSCRIPTION_ENCRYPT_KEY);
        }

        public string EncryptAndEncodeNumber(int number)
        {
            return Context.Server.UrlEncode(EncryptNumber(number));
        }

        public int GetDecryptedValue(string encryptedMemberTranID)
        {
            int memberTranID = 0;
            try
            {
                Matchnet.Lib.Encryption.SymmetricalEncryption decryptMemberTranID =
                    new Matchnet.Lib.Encryption.SymmetricalEncryption(Matchnet.Lib.Encryption.SymmetricalEncryption.Provider.DES);
                memberTranID = Matchnet.Conversion.CInt(decryptMemberTranID.Decrypt(encryptedMemberTranID,
                    SUBSCRIPTION_ENCRYPT_KEY));
            }
            catch
            {
                memberTranID = 0;
            }

            return memberTranID;
        }


        public string GetContinueLink(bool useJavaScript)
        {
            try
            {
                string href;
                
                if (g.GetDestinationURL() != null && g.GetDestinationURL().Length > 0)
                {
                    href = "/Applications/Subscription/Upsale.aspx?DestinationURL=" + System.Web.HttpUtility.UrlEncode(g.GetDestinationURL());
                }
                else
                {
                    href = "/Applications/Subscription/Upsale.aspx";
                }
                
                g.PersistLayoutTemplate = false;
                return FrameworkGlobals.LinkHref(href, false);
            }
            catch (Exception ex)
            {
                _g.ProcessException(ex);
                return "/default.aspx";
            }
        }
       
    }
}
