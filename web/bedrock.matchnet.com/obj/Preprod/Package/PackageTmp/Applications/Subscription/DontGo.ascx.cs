﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Session.ValueObjects;
using Matchnet.Web.Framework;
using Matchnet.Content.ServiceAdapters.Links;

namespace Matchnet.Web.Applications.Subscription
{
    public partial class DontGo : FrameworkControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            new CookieHelper().RemoveDontGoCookie();
            lnkSubscribe.NavigateUrl = FrameworkGlobals.GetSubscriptionLink(false, (int) PurchaseReasonType.DontGo);
            lnkCancel.NavigateUrl = "http://" + HttpContext.Current.Request.Url.Host + "/Applications/Home/Default.aspx";

            g.HeaderControl.ShowBlankHeader(Matchnet.Web.Framework.Ui.HeaderImageType.Default);
            g.SetTopAuxMenuVisibility(false, true);
            g.FooterControl20.HideFooterLinks();
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            if(g.Session[WebConstants.SESSION_PROPERTY_LAST_SUB_PROMOID] != null)
            {
                g.AnalyticsOmniture.Evar4 = g.Session[WebConstants.SESSION_PROPERTY_LAST_SUB_PROMOID];
            }
            if (g.Session[WebConstants.SESSION_PROPERTY_LAST_SUB_PRTID] != null)
            {
                g.AnalyticsOmniture.PRT_ID = g.Session[WebConstants.SESSION_PROPERTY_LAST_SUB_PRTID];
                
                //MPR-3423 - Capture Original PRTID to sub page.
                string prtidList = g.Session[WebConstants.SESSION_PROPERTY_ORIGINAL_SUB_PRTID];
                    g.Session.Add(WebConstants.SESSION_PROPERTY_ORIGINAL_SUB_PRTID,
                        (string.IsNullOrEmpty(prtidList)) ? g.Session[WebConstants.SESSION_PROPERTY_LAST_SUB_PRTID] : prtidList + "-" + g.Session[WebConstants.SESSION_PROPERTY_LAST_SUB_PRTID],
                                  SessionPropertyLifetime.Temporary);
            }
        }
    }
}