﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace Matchnet.Web.Applications.Subscription {
    
    
    public partial class DontGo {
        
        /// <summary>
        /// Txt7 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Matchnet.Web.Framework.Txt Txt7;
        
        /// <summary>
        /// Txt1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Matchnet.Web.Framework.Txt Txt1;
        
        /// <summary>
        /// Txt2 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Matchnet.Web.Framework.Txt Txt2;
        
        /// <summary>
        /// Txt3 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Matchnet.Web.Framework.Txt Txt3;
        
        /// <summary>
        /// lnkSubscribe control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HyperLink lnkSubscribe;
        
        /// <summary>
        /// Txt4 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Matchnet.Web.Framework.Txt Txt4;
        
        /// <summary>
        /// lnkCancel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HyperLink lnkCancel;
        
        /// <summary>
        /// Txt5 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Matchnet.Web.Framework.Txt Txt5;
        
        /// <summary>
        /// Txt6 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Matchnet.Web.Framework.Txt Txt6;
    }
}
