using System;
using System.Data;
using System.Collections.Specialized;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Matchnet.Lib;
using Matchnet.Lib.Util;
using Matchnet.Web.Framework;
using Matchnet.SharedLib.Utils;
using Matchnet.Session.ValueObjects;
using Matchnet.Purchase.ValueObjects;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Purchase.ServiceAdapters;
using Matchnet.Content.ValueObjects.Region;
using Matchnet.Web.Framework.Globalization;
using Matchnet.Member.ValueObjects.Privilege;
using Matchnet.Content.ValueObjects.PageConfig;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Spark.Common.PaymentProfileService;
using Matchnet.Web.Framework.Managers;

namespace Matchnet.Web.Applications.Subscription
{
    #region STRUCTURE
    public struct SelectedPlanInformation
    {
        private int _planID;
        public int PlanID
        {
            get { return this._planID; }
            set { this._planID = value; }
        }

        private PurchaseMode _purchaseMode;
        public PurchaseMode PurchaseMode
        {
            get { return this._purchaseMode; }
            set { this._purchaseMode = value; }
        }

        private decimal _creditAmount;
        public decimal CreditAmount
        {
            get { return this._creditAmount; }
            set { this._creditAmount = value; }
        }

        public SelectedPlanInformation(int planID, PurchaseMode purchaseMode, decimal creditAmount)
        {
            this._planID = planID;
            this._purchaseMode = purchaseMode;
            this._creditAmount = creditAmount;
        }
    }
    #endregion

    public enum SubscriptionPaymentInfo
    {
        CreditCard = 0,
        CreditCardIL = 1,
        Check = 2,
        OneclickCreditCard = 3,
        OneclickCheck = 4
    }



    public class SubscriptionHelper
    {
        public const string SUBSCRIPTION_ENCRYPT_KEY = "m0nk$ies";
        private SubscriptionHelper() { }

        /// <summary>
        /// This returns any pre-selected plan ID, implemented as part of two-step subscription process
        /// </summary>
        public static int PreSelectedPlanID
        {
            get
            {
                //check for pre-selected plan ID
                int selectedPlanID = Constants.NULL_INT;

                //first check from form post
                if (!String.IsNullOrEmpty(HttpContext.Current.Request.Form["plan"]))
                {
                    try
                    {
                        selectedPlanID = Convert.ToInt32(HttpContext.Current.Request.Form["plan"]);
                    }
                    catch
                    {
                        selectedPlanID = Constants.NULL_INT;
                    }
                }

                if (selectedPlanID == Constants.NULL_INT)
                {
                    //second check from url
                    try
                    {
                        if (!String.IsNullOrEmpty(HttpContext.Current.Request.QueryString[WebConstants.URL_PARAMETER_NAME_PLANID]))
                        {
                            selectedPlanID = SubscriptionHelper.GetDecryptedNumber(HttpContext.Current.Request.QueryString[WebConstants.URL_PARAMETER_NAME_PLANID]);
                        }
                    }
                    catch
                    {
                        selectedPlanID = Constants.NULL_INT;
                    }
                }

                return selectedPlanID;
            }
        }

        /// <summary>
        /// Determines if a string has only numeric values.
        /// </summary>
        /// <param name="data">string to check</param>
        public static bool IsNumeric(string data)
        {
            foreach (char c in data.ToCharArray())
            {
                if (!char.IsNumber(c))
                    return false;
            }
            return true;
        }

        public static NameValueCollection GetMemberPayment(int MemberID, Matchnet.Content.ValueObjects.BrandConfig.Brand brand)
        {
            NameValueCollection nvc = null;

            //MemberSub ms = PurchaseSA.Instance.GetSubscription(MemberID, brand.Site.SiteID);

            //if (ms != null)
            //{
            //    nvc = PurchaseSA.Instance.GetMemberPayment(MemberID,
            //        brand.Site.Community.CommunityID,
            //        ms.MemberPaymentID);
            //    nvc.Add("PaymentType", ms.PaymentType.ToString("d"));
            //}

            return nvc;
        }

        public static bool ValidateRoutingNumber(string val)
        {
            try
            {
                char[] chars = val.ToCharArray();
                int sum = 0;

                for (int i = 0; i < chars.Length; i += 3)
                {
                    sum += Matchnet.Conversion.CInt(System.Convert.ToString(chars[i])) * 3
                        + Matchnet.Conversion.CInt(System.Convert.ToString(chars[i + 1])) * 7
                        + Matchnet.Conversion.CInt(System.Convert.ToString(chars[i + 2]));
                }

                if (sum != 0 && sum % 10 == 0)
                    return true;
            }
            catch { }

            return false;
        }

        /// <summary>
        /// Gets the url links to pay by check page.
        /// </summary>
        /// <param name="g">Object contains the web contextual information</param>
        public static string GetPayByCheckURL(Matchnet.Web.Framework.ContextGlobal g)
        {
            try
            {
                return FrameworkGlobals.LinkHrefSSL(
                    "/Applications/Subscription/Subscribe.aspx?OverrideTransactionHistory=1&SPIS=" + SubscriptionPaymentInfo.Check.ToString("d") +
                    g.AppendDestinationURL());
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
                return "";
            }
        }

        /// <summary>
        /// Gets the url links to (What is CVC page).
        /// </summary>
        /// <param name="g">Object contains the web contextual information</param>
        /// <returns></returns>
        public static string GetWhatIsCVCURL(Matchnet.Web.Framework.ContextGlobal g)
        {
            try
            {
                return FrameworkGlobals.GetArticleLink("WHAT_IS_CVC_PAGE_CONTENT", LayoutTemplate.Popup, "", false);
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
                return "";
            }
        }

        /// <summary>
        /// Get the available credit card expiration years.
        /// </summary>
        /// <param name="g">Object contains the web contextual information</param>
        /// <param name="callerControl">The control where data will be displayed</param>
        public static DataTable GetCreditCardExpirationYears(Matchnet.Web.Framework.ContextGlobal g, System.Web.UI.Control callerControl)
        {
            DataTable dtCreditCardExprYear = new DataTable();
            dtCreditCardExprYear.Columns.Add("Content");
            dtCreditCardExprYear.Columns.Add("Value");
            int startYear = DateTime.Now.Year;
            dtCreditCardExprYear.Rows.Add(new object[] { g.GetResource("YEAR", callerControl), "" });
            for (int year = startYear; year < startYear + 9; year++)
            {
                dtCreditCardExprYear.Rows.Add(new object[] { year, year });
            }
            return dtCreditCardExprYear;
        }

        /// <summary>
        /// Get the available credit card expiration months.
        /// </summary>
        /// <param name="g">Object contains the web contextual information</param>
        /// <param name="callerControl">The control where data will be displayed</param>
        /// <returns></returns>
        public static DataTable GetCreditCardExpirationMonths(Matchnet.Web.Framework.ContextGlobal g, System.Web.UI.Control callerControl)
        {
            DataTable dtCreditCardExprMonth = new DataTable();
            dtCreditCardExprMonth.Columns.Add("Content");
            dtCreditCardExprMonth.Columns.Add("Value");
            dtCreditCardExprMonth.Rows.Add(new object[] { g.GetResource("MONTHABBR", callerControl), "" });
            for (int month = 1; month < 13; month++)
            {
                dtCreditCardExprMonth.Rows.Add(new object[] { month.ToString("0#"), month });
            }
            return dtCreditCardExprMonth;
        }

        /// <summary>
        /// Get the url links to pay by credit card page.
        /// </summary>
        /// <param name="g">Object contains the web contextual information</param>
        public static string GetPayByCreditCardURL(Matchnet.Web.Framework.ContextGlobal g)
        {
            try
            {
                //getting rid of extra ampersand at end
                return Matchnet.Content.ServiceAdapters.Links.LinkFactory.Instance.GetLink("/Applications/Subscription/Subscribe.aspx?OverrideTransactionHistory=1"
                    + g.AppendDestinationURL(), g.Brand, System.Web.HttpContext.Current.Request.Url.ToString());
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
                return "";
            }
        }

        public static string ConvertToRegex(string source, string regex)
        {
            StringBuilder buffer = new StringBuilder();
            Regex r = new Regex(regex);
            MatchCollection mc;
            string s = string.Empty;

            mc = r.Matches(source);

            for (int i = 0; i < mc.Count; i++)
                buffer.Append(mc[i].Value);

            if (buffer.Length > 0)
                s = buffer.ToString();

            return s;

        }


        public static void StoreSelectedPlan(Matchnet.Web.Framework.ContextGlobal g, int planID)
        {
            g.Session.Add("SelectedPlan", planID, Matchnet.Session.ValueObjects.SessionPropertyLifetime.Temporary);
        }

        public static void RemoveSelectedPlan(Matchnet.Web.Framework.ContextGlobal g)
        {
            if (g.Session["SelectedPlan"] != null)
                g.Session.Remove("SelectedPlan");
        }

        public static int GetSelectedPlan(Matchnet.Web.Framework.ContextGlobal g)
        {
            return g.Session.GetInt("SelectedPlan", int.MinValue);
        }

        /// <summary>
        /// method to expire payment profile information
        /// call when ever changes were made to payment profile
        /// </summary>
        /// <param name="g"></param>
        public static void RemovePaymentProfileCache(Matchnet.Web.Framework.ContextGlobal g)
        {
            g.Session.Remove("IsPaymentProfileEditable");
            g.Session.Remove("IsPaymentExpiring");
        }

        /// <summary>
        /// Helper method to verify if member has payment profile for edit
        /// </summary>
        /// <param name="g"></param>
        /// <returns></returns>
        public static bool IsPaymentProfileEditable(Matchnet.Web.Framework.ContextGlobal g)
        {
            bool result = false;
            try
            {

                #region check if exist in session and return session value
                string mySession = (string)g.Session.Get("IsPaymentProfileEditable");
                if (mySession != null)
                {
                    result = bool.Parse(mySession.Trim());
                    return result;
                }
                #endregion

                string strEnablePaymentProfile = RuntimeSettings.GetSetting("ENABLE_PAYMENTPROFILE",
                    g.TargetBrand.Site.Community.CommunityID,
                    g.TargetBrand.Site.SiteID);

                if (Convert.ToBoolean(strEnablePaymentProfile))
                {
                    Spark.Common.PaymentProfileService.PaymentProfileInfo paymentProfileInfo = Spark.Common.Adapter.PaymentProfileServiceWebAdapter.GetProxyInstance().GetMemberDefaultPaymentProfile(g.TargetMemberID, g.TargetBrand.Site.SiteID);
                    if (paymentProfileInfo != null && !String.IsNullOrEmpty(paymentProfileInfo.PaymentProfileID))
                    {
                        if (Convert.ToInt32(paymentProfileInfo.PaymentProfileType) == (int)PaymentType.CreditCard)
                        {
                            result = true;
                        }
                    }
                }

                //add to session so does not have to lookup on every hit
                g.Session.Add("IsPaymentProfileEditable"
                    , result.ToString()
                    , Matchnet.Session.ValueObjects.SessionPropertyLifetime.Temporary);
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
            return result;
        }

        /// <summary>
        /// Helper method to verify if a credit card exists and is about to expire
        /// </summary>
        /// <param name="g"></param>
        /// <returns></returns>
        public static bool IsPaymentExpiring(Matchnet.Web.Framework.ContextGlobal g)
        {
            bool paymentExpiring = false;

            try
            {
                #region check if exist in session and return session value
                string mySession = (string)g.Session.Get("IsPaymentExpiring");
                if (mySession != null)
                {
                    paymentExpiring = bool.Parse(mySession.Trim());
                    return paymentExpiring;
                }
                #endregion

                // check configiguration if this site enable Payment Profile
                string strEnablePaymentProfile = RuntimeSettings.GetSetting("ENABLE_PAYMENTPROFILE",
                    g.TargetBrand.Site.Community.CommunityID,
                    g.TargetBrand.Site.SiteID);
                if (Convert.ToBoolean(strEnablePaymentProfile))
                {
                    int days = 0;
                    try
                    {
                        string strConfigSetting = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("CREDIT_CARD_EXPIRATION_NOTIFY_DAYS", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID);
                        days = int.Parse(strConfigSetting);
                    }
                    catch { }//cant do anything here!


                    if (days > 0 && MemberPrivilegeAttr.IsCureentSubscribedMember(g))
                    {
                        Spark.Common.RenewalService.RenewalSubscription renewalSub = RenewalManager.Instance.GetRenewalSubscription(g.TargetMemberID, g.TargetBrand);

                        if (renewalSub != null && renewalSub.RenewalSubscriptionID > 0)
                        {
                            Spark.Common.PaymentProfileService.ObsfucatedPaymentProfileResponse paymentProfileResponse = Spark.Common.Adapter.PaymentProfileServiceWebAdapter.GetProxyInstanceForBedrock().GetObsfucatedPaymentProfileByMemberID(g.TargetMemberID, g.TargetBrand.Site.SiteID);
                            if (paymentProfileResponse != null && paymentProfileResponse.Code == "0" && paymentProfileResponse.PaymentProfile != null)
                            {
                                if (paymentProfileResponse.PaymentProfile is ObsfucatedCreditCardPaymentProfile
                                    || paymentProfileResponse.PaymentProfile is ObfuscatedCreditCardShortFormFullWebSitePaymentProfile
                                    || paymentProfileResponse.PaymentProfile is ObfuscatedCreditCardShortFormMobileSitePaymentProfile)
                                {
                                    ObsfucatedCreditCardPaymentProfile creditCardPaymentProfile = paymentProfileResponse.PaymentProfile as ObsfucatedCreditCardPaymentProfile; 
                                    ObfuscatedCreditCardShortFormFullWebSitePaymentProfile creditCardShortFormFullWebSitePaymentProfile = paymentProfileResponse.PaymentProfile as ObfuscatedCreditCardShortFormFullWebSitePaymentProfile;
                                    ObfuscatedCreditCardShortFormMobileSitePaymentProfile creditCardShortFormMobileSitePaymentProfile = paymentProfileResponse.PaymentProfile as ObfuscatedCreditCardShortFormMobileSitePaymentProfile;

                                    int Month = 0;
                                    int Year = 0;
                                    if (creditCardPaymentProfile != null)
                                    {
                                        Month = creditCardPaymentProfile.ExpMonth;
                                        Year = creditCardPaymentProfile.ExpYear;
                                    }
                                    else if (creditCardShortFormFullWebSitePaymentProfile != null)
                                    {
                                        Month = creditCardShortFormFullWebSitePaymentProfile.ExpMonth;
                                        Year = creditCardShortFormFullWebSitePaymentProfile.ExpYear;
                                    }
                                    else if (creditCardShortFormMobileSitePaymentProfile != null)
                                    {
                                        Month = creditCardShortFormMobileSitePaymentProfile.ExpMonth;
                                        Year = creditCardShortFormMobileSitePaymentProfile.ExpYear;
                                    }
                                    
                                    if (12 == Month)
                                    {
                                        Month = 1;
                                        Year++;
                                    }
                                    else
                                    {
                                        Month++;
                                    }

                                    DateTime expiration = new DateTime(Year, Month, 1);

                                    //days before renewal
                                    if (DateTime.Now.AddDays(days) >= renewalSub.RenewalDatePST)
                                    {
                                        if (expiration <= renewalSub.RenewalDatePST)
                                        {
                                            paymentExpiring = true;
                                        }
                                    }
                                }
                                else if (paymentProfileResponse.PaymentProfile is Spark.Common.PaymentProfileService.ObfuscatedDebitCardPaymentProfile)
                                {
                                    Spark.Common.PaymentProfileService.ObfuscatedDebitCardPaymentProfile debitCardPaymentProfile = (Spark.Common.PaymentProfileService.ObfuscatedDebitCardPaymentProfile)paymentProfileResponse.PaymentProfile;
                                    int Month = debitCardPaymentProfile.ExpMonth;
                                    int Year = debitCardPaymentProfile.ExpYear;

                                    if (12 == Month)
                                    {
                                        Month = 1;
                                        Year++;
                                    }
                                    else
                                    {
                                        Month++;
                                    }

                                    DateTime expiration = new DateTime(Year, Month, 1);

                                    //days before renewal
                                    if (DateTime.Now.AddDays(days) >= renewalSub.RenewalDatePST)
                                    {
                                        if (expiration <= renewalSub.RenewalDatePST)
                                        {
                                            paymentExpiring = true;
                                        }
                                    }
                                }
                            }
                        }                        
                    }
                }

                // add to session so does not have to look everytime
                g.Session.Add("IsPaymentExpiring"
                    , paymentExpiring.ToString()
                    , Matchnet.Session.ValueObjects.SessionPropertyLifetime.Temporary);
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
            return paymentExpiring;
        }

        /// <summary>
        /// helper method to crdit card collection type
        /// </summary>
        /// <param name="g"></param>
        /// <returns></returns>
        public static CreditCardCollection GetCreditCardTypes(Matchnet.Web.Framework.ContextGlobal g)
        {
            try
            {
                CreditCardCollection ccc = PurchaseSA.Instance.GetCreditCardTypes(g.TargetBrand.Site.SiteID);

                if (ccc != null)
                {
                    foreach (CreditCard cc in ccc)
                    {
                        cc.Content = g.GetResource(cc.ResourceConstant, null);
                    }
                }
                return ccc;
            }
            catch (Exception ex)
            {
                throw new Exception("SubscriptionHelper.GetCreditCardTypes()-" + ex.Message);
            }
        }

        /// <summary>
        /// helper method to build params for redirecting urls
        /// </summary>
        /// <param name="fc"></param>
        /// <param name="redirectURL"></param>
        public static void PmtProfileAdminRedirect(Matchnet.Web.Framework.FrameworkControl fc, string redirectURL)
        {
            if (fc.Request["LayoutTemplateID"] != null)
            {
                redirectURL = redirectURL + "&LayoutTemplateID=" + fc.Request["LayoutTemplateID"];
            }
            if (fc.Request[WebConstants.IMPERSONATE_MEMBERID] != null)
            {
                redirectURL = redirectURL + "&" + WebConstants.IMPERSONATE_MEMBERID + "=" + fc.Request[WebConstants.IMPERSONATE_MEMBERID];
            }
            if (fc.Request[WebConstants.IMPERSONATE_BRANDID] != null)
            {
                redirectURL = redirectURL + "&" + WebConstants.IMPERSONATE_BRANDID + "=" + fc.Request[WebConstants.IMPERSONATE_BRANDID];
            }
            redirectURL = FrameworkGlobals.LinkHrefSSL(redirectURL);
            fc.g.Transfer(redirectURL);
        }

        public static bool IsIsraeliBrand(int brandID)
        {
            if (brandID == (int)WebConstants.BRAND_ID.JDateCoIL ||
                brandID == (int)WebConstants.BRAND_ID.Cupid ||
                brandID == (int)WebConstants.BRAND_ID.Nana ||
                brandID == (int)WebConstants.BRAND_ID.NRGDating)
                return true;
            else
                return false;
        }

        public static bool ValidateIsraCardException(string CreditCardType, string CreditCardNumber)
        {
            const string YOTER_CLUB_ISRACARD = "72";
            const string ISRACARD_CAMPUS_CARD_STUDENT = "74";
            const string CREDIT_CARD_ISRACARD = "128";

            switch (CreditCardType)
            {
                case YOTER_CLUB_ISRACARD:
                    return (new Regex("\\d*").IsMatch(CreditCardNumber));
                //break;
                case ISRACARD_CAMPUS_CARD_STUDENT:
                    return (new Regex("\\d*").IsMatch(CreditCardNumber));
                //break;
                case CREDIT_CARD_ISRACARD:
                    return (new Regex("\\d*").IsMatch(CreditCardNumber));
                //break;				
            }

            return false;
        }

        public static string GetContinueLink(ContextGlobal g)
        {
            return GetContinueLink(true, g);
        }

        public static string GetContinueLink(bool useJavaScript, ContextGlobal g)
        {
            try
            {
                string href;
                bool sslCheck = false;

                bool tryToUpsale = false;

                if (HttpContext.Current.Request["upsale"] != null)
                {
                    if (HttpContext.Current.Request["upsale"] == "true")
                    {
                        tryToUpsale = true;
                    }                    
                }

                if (Convert.ToBoolean((RuntimeSettings.GetSetting("IS_UPS_UPSALE_ENABLED", g.Brand.Site.Community.CommunityID,
                    g.Brand.Site.SiteID, g.Brand.BrandID))) && tryToUpsale)
                {
                    if (g.GetDestinationURL() != null && g.GetDestinationURL().Length > 0)
                    {
                        href = "/Applications/Subscription/Upsale.aspx?DestinationURL=" + System.Web.HttpUtility.UrlEncode(g.GetDestinationURL());
                    }
                    else
                    {
                        href = "/Applications/Subscription/Upsale.aspx";
                    }
                }
                else
                {
                    if (g.GetDestinationURL() != null && g.GetDestinationURL().Length > 0)
                    {
                        if (g.DestinationPage == ContextGlobal.Pages.PAGE_INSTANT_MESSENGER_MAIN)
                        {
                            //Redirect to view profile -- we can't pop up an IM window because this would be blocked by popup blockers.
                            href = "/Applications/MemberProfile/ViewProfile.aspx?MemberID=" + g.RecipientMemberID;
                        }
                        else
                        {
                            href = g.GetDestinationURL(HttpContext.Current.Request[WebConstants.URL_PARAMETER_NAME_SPARKWS_CONTEXT] == "true");
                        }
                    }
                    else
                    {
                        href = "/Applications/Home/Default.aspx";
                        sslCheck = true;
                    }
                }

                g.PersistLayoutTemplate = false;
                return FrameworkGlobals.LinkHref(href, sslCheck);
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
                return "/default.aspx";
            }
        }

        public static string EncryptNumber(int number)
        {
            Matchnet.Lib.Encryption.SymmetricalEncryption encryptMemberTranID = new Matchnet.Lib.Encryption.SymmetricalEncryption(Matchnet.Lib.Encryption.SymmetricalEncryption.Provider.DES);
            return encryptMemberTranID.Encrypt(number.ToString(), SUBSCRIPTION_ENCRYPT_KEY);
        }

        public static string EncryptAndEncodeNumber(int number)
        {
            return System.Web.HttpContext.Current.Server.UrlEncode(EncryptNumber(number));
        }

        public static int GetDecryptedNumber(string encryptedValue)
        {
            int num = Constants.NULL_INT;
            try
            {
                Matchnet.Lib.Encryption.SymmetricalEncryption sEncryptor =
                    new Matchnet.Lib.Encryption.SymmetricalEncryption(Matchnet.Lib.Encryption.SymmetricalEncryption.Provider.DES);
                num = Matchnet.Conversion.CInt(sEncryptor.Decrypt(encryptedValue,
                    SUBSCRIPTION_ENCRYPT_KEY));
            }
            catch
            {
                num = Constants.NULL_INT;
            }

            return num;
        }
    }
}
