namespace Matchnet.Web.Applications.Subscription
{
	using System;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;
	using Matchnet.Lib;
	using Matchnet.Member.ServiceAdapters;
	using Matchnet.Member.ValueObjects;
	using Matchnet.Content.ServiceAdapters;
	using Matchnet.Content.ValueObjects;
	using Matchnet.Web.Framework;

	/// <summary>
	///		Summary description for CancellationPolicy.
	/// </summary>
	public class CancellationPolicyPage :  FrameworkControl
	{
		private const string RESOURCE_CONST_BASE="CANCELLATION_POLICY_HTML";
		private const string RESOURCE_CONST_FORMAT="CANCELLATION_POLICY_HTML_{0}";
		private const string TOKEN_CANCELLATION_DATE="CANCEL_DATE_BUSINESS_DAYS";
		private const string TOKEN_TRANSACTION_DATE="TRANSACTION_DATE";
		private const string TOKEN_MEMBER_NUMBER="MEMBER_NUMBER";
		private const string SETTING_TERMS_FLAG="TERMS_CONDITION_FLAG";
	

		
		protected Matchnet.Web.Framework.Txt txtPolicy;
		protected PlaceHolder plPolicy;

		
		private string memberState="";
		private string resourceConst=RESOURCE_CONST_BASE;
		private string nextBusinessDate="";
		private DateTime currentDate;

		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			string flag=Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting(SETTING_TERMS_FLAG,g.Brand.Site.SiteID,g.Brand.BrandID);

			
			if(g.Member ==null || !Conversion.CBool(flag,false))
			{
				plPolicy.Visible=false;
				return;

			}

			currentDate=DateTime.Now;
			MemberState=getMemberState();
			ResourceConst=getPolicyResource(MemberState);
			bool vis=PolicyVisibility();
			if(vis)
			{
				txtPolicy.ResourceConstant=ResourceConst;
				plPolicy.Visible=vis;
				g.AddExpansionToken(TOKEN_TRANSACTION_DATE,getCurrentDate());
				g.AddExpansionToken(TOKEN_CANCELLATION_DATE,getCancellationDeadline());
				g.AddExpansionToken(TOKEN_MEMBER_NUMBER, g.Member.MemberID.ToString());
				
			}
			


		}

		
		public string MemberState
		{
			get 
			{
				
				return memberState.ToUpper();}
			set
			{
				if (value != null)
					memberState=value.Trim().ToUpper();
			}
		}


		public string ResourceConst
		{
			get 
			{
				
				return resourceConst;
			}
			set
			{
				if (value != null)
					resourceConst=value;
			}
		}


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion

		private string getPolicyResource(string state)
		{
				string res=RESOURCE_CONST_BASE;
			string resource="";

			try
			{
				if(state==null  || state ==String.Empty)
					return RESOURCE_CONST_BASE;


				res= String.Format(RESOURCE_CONST_FORMAT,state.Trim().ToUpper());
				resource=g.GetResource(res,this);
				if (resource.ToUpper().IndexOf(RESOURCE_CONST_BASE) > -1 )
				{
					res=RESOURCE_CONST_BASE;
				}

				return res;

			}
			catch(Exception ex)
			{return res;}
		}

		private string getMemberState()
		{
			string state="";
			try
			{
				int regionID = 0;
				regionID = g.Member.GetAttributeInt(g.Brand, "RegionID", 0);
				if (regionID <= 0)
				{regionID = g.Brand.Site.DefaultRegionID;}
				Matchnet.Content.ValueObjects.Region.RegionLanguage lng=RegionSA.Instance.RetrievePopulatedHierarchy(regionID, g.TargetLanguageID);
				state=lng.StateAbbreviation;
				return state;
			}
			catch(Exception ex)
			{ return state;}
		}

		private bool PolicyVisibility()
		{
			bool visible=true;
			try
			{
				
				if(resourceConst == RESOURCE_CONST_BASE)
					return false;
				
				return visible;

			}
			catch(Exception ex)
			{return visible;}
		}


		private string getCancellationDeadline()
		{
			
			string date="";
			try
			{
				date=EventSA.Instance.GetNextBusinessDate(currentDate,3).ToShortDateString();
				return date;
			}
				
			catch(Exception ex)
			{ return date;}
		}
		
		private string getCurrentDate()
		{
			string format="{0}";
			string date="";
			try
			{
				string dt=currentDate.ToLongDateString();
				if(g.Brand.Site.LanguageID==(int)Language.Hebrew)
					dt=currentDate.ToShortDateString();

				date=dt;
				return date;
			}
				
			catch(Exception ex)
			{ return date;}
		}


	

	}
}
