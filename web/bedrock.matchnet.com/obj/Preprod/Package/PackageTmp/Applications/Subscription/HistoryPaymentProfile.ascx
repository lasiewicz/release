﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HistoryPaymentProfile.ascx.cs" Inherits="Matchnet.Web.Applications.Subscription.HistoryPaymentProfile" %>

<div id="AlignOverrideDiv" runat="server">
    <div id="rightNew">
        <h2>Transaction Payment Profile Information</h2>
        
        <table style="border: solid 1px #e8eef4;border-collapse: collapse;" cellpadding="5" cellpadding="0">
            <tr><td>Transaction ID:</td><td><asp:Literal ID="literalTranID" runat="server"></asp:Literal></td></tr>
            <tr><td>Payment type:</td><td><asp:Literal ID="literalPaymentType" runat="server"></asp:Literal></td></tr>
            <tr><td>Payment Profile ID:</td><td><asp:Literal ID="literalPaymentProfileID" runat="server"></asp:Literal></td></tr>
            <tr><td>Order ID:</td><td><asp:Literal ID="literalOrderID" runat="server"></asp:Literal></td></tr>
            <tr><td>Order Status Code:</td><td><asp:Literal ID="literalOrderCode" runat="server"></asp:Literal></td></tr>
            <tr><td>Order Status Desc:</td><td><asp:Literal ID="literalOrderStatusMessage" runat="server"></asp:Literal></td></tr>
            
            <asp:PlaceHolder ID="phCreditCardInfo" runat="server" Visible="false">
            <tr><td>Card Last Four:</td><td><asp:Literal ID="ccLastFour" runat="server"></asp:Literal></td></tr>
            <tr><td>Card Type:</td><td><asp:Literal ID="ccCardType" runat="server"></asp:Literal></td></tr>
            <tr><td>Exp Month:</td><td><asp:Literal ID="ccExpMonth" runat="server"></asp:Literal></td></tr>
            <tr><td>Exp Year:</td><td><asp:Literal ID="ccExpYear" runat="server"></asp:Literal></td></tr>
            <tr><td>First Name:</td><td><asp:Literal ID="ccFirstName" runat="server"></asp:Literal></td></tr>
            <tr><td>Last Name:</td><td><asp:Literal ID="ccLastName" runat="server"></asp:Literal></td></tr>
            <tr><td>Phone Number:</td><td><asp:Literal ID="ccPhone" runat="server"></asp:Literal></td></tr>
            <tr><td>Street Address:</td><td><asp:Literal ID="ccStreet" runat="server"></asp:Literal></td></tr>
            <tr><td>City:</td><td><asp:Literal ID="ccCity" runat="server"></asp:Literal></td></tr>
            <tr><td>State:</td><td><asp:Literal ID="ccState" runat="server"></asp:Literal></td></tr>
            <tr><td>Postal Code:</td><td><asp:Literal ID="ccPostal" runat="server"></asp:Literal></td></tr>
            <tr><td>Country:</td><td><asp:Literal ID="ccCountry" runat="server"></asp:Literal></td></tr>
            </asp:PlaceHolder>
            
            <asp:PlaceHolder ID="phCheckInfo" runat="server" Visible="false">
            <tr><td>Account Last Four:</td><td><asp:Literal ID="ckAccountLastFour" runat="server"></asp:Literal></td></tr>
            <tr><td>Routing Last Four:</td><td><asp:Literal ID="ckRoutingLastFour" runat="server"></asp:Literal></td></tr>
            <tr><td>Account Type:</td><td><asp:Literal ID="ckAccountType" runat="server"></asp:Literal></td></tr>
            <tr><td>First Name:</td><td><asp:Literal ID="ckFirstName" runat="server"></asp:Literal></td></tr>
            <tr><td>Last Name:</td><td><asp:Literal ID="ckLastName" runat="server"></asp:Literal></td></tr>
            <tr><td>Phone Number:</td><td><asp:Literal ID="ckPhone" runat="server"></asp:Literal></td></tr>
            <tr><td>Email:</td><td><asp:Literal ID="ckEmail" runat="server"></asp:Literal></td></tr>
            <tr><td>Bank Name:</td><td><asp:Literal ID="ckBankName" runat="server"></asp:Literal></td></tr>
            <tr><td>Bank State:</td><td><asp:Literal ID="ckBankState" runat="server"></asp:Literal></td></tr>
            <tr><td>Street Address:</td><td><asp:Literal ID="ckStreet" runat="server"></asp:Literal></td></tr>
            <tr><td>City:</td><td><asp:Literal ID="ckCity" runat="server"></asp:Literal></td></tr>
            <tr><td>State:</td><td><asp:Literal ID="ckState" runat="server"></asp:Literal></td></tr>
            <tr><td>Postal Code:</td><td><asp:Literal ID="ckPostal" runat="server"></asp:Literal></td></tr>
            </asp:PlaceHolder>
            
            <asp:PlaceHolder ID="phPaypalInfo" runat="server" Visible="false">
            <tr><td>First Name:</td><td><asp:Literal ID="paypalFirstName" runat="server"></asp:Literal></td></tr>
            <tr><td>Last Name:</td><td><asp:Literal ID="paypalLastName" runat="server"></asp:Literal></td></tr>
            <tr><td>Country:</td><td><asp:Literal ID="paypalCountry" runat="server"></asp:Literal></td></tr>
            <tr><td>Email:</td><td><asp:Literal ID="paypalEmail" runat="server"></asp:Literal></td></tr>
            <tr><td>Payer Status:</td><td><asp:Literal ID="paypalPayerStatus" runat="server"></asp:Literal></td></tr>
            <tr><td>Paypal Token:</td><td><asp:Literal ID="paypalToken" runat="server"></asp:Literal></td></tr>
            <tr><td>Paypal BillingAgreementID Last Four:</td><td><asp:Literal ID="paypalBillingAgreementID" runat="server"></asp:Literal></td></tr>
            </asp:PlaceHolder>

            <asp:PlaceHolder ID="phManualPaymentInfo" runat="server" Visible="false">
            <tr><td>First Name:</td><td><asp:Literal ID="manualPaymentFirstName" runat="server"></asp:Literal></td></tr>
            <tr><td>Last Name:</td><td><asp:Literal ID="manualPaymentLastName" runat="server"></asp:Literal></td></tr>
            </asp:PlaceHolder>
            
            <asp:PlaceHolder ID="phElectronicFundsTransferInfo" runat="server" Visible="false">
            <tr><td>Account Last Four:</td><td><asp:Literal ID="electronicFundsTransferAccountLastFour" runat="server"></asp:Literal></td></tr>
            <tr><td>Routing Last Four:</td><td><asp:Literal ID="electronicFundsTransferRoutingLastFour" runat="server"></asp:Literal></td></tr>
            <tr><td>Account Type:</td><td><asp:Literal ID="electronicFundsTransferAccountType" runat="server"></asp:Literal></td></tr>
            <tr><td>First Name:</td><td><asp:Literal ID="electronicFundsTransferFirstName" runat="server"></asp:Literal></td></tr>
            <tr><td>Last Name:</td><td><asp:Literal ID="electronicFundsTransferLastName" runat="server"></asp:Literal></td></tr>
            <tr><td>Phone Number:</td><td><asp:Literal ID="electronicFundsTransferPhone" runat="server"></asp:Literal></td></tr>
            <tr><td>Email:</td><td><asp:Literal ID="electronicFundsTransferEmail" runat="server"></asp:Literal></td></tr>
            <%--
            <tr><td>Bank Name:</td><td><asp:Literal ID="electronicFundsTransferBankName" runat="server"></asp:Literal></td></tr>
            <tr><td>Bank State:</td><td><asp:Literal ID="electronicFundsTransferBankState" runat="server"></asp:Literal></td></tr>
            --%>                
            <tr><td>Street Address:</td><td><asp:Literal ID="electronicFundsTransferStreet" runat="server"></asp:Literal></td></tr>
            <tr><td>City:</td><td><asp:Literal ID="electronicFundsTransferCity" runat="server"></asp:Literal></td></tr>
            <tr><td>State:</td><td><asp:Literal ID="electronicFundsTransferState" runat="server"></asp:Literal></td></tr>
            <tr><td>Postal Code:</td><td><asp:Literal ID="electronicFundsTransferPostal" runat="server"></asp:Literal></td></tr>
            </asp:PlaceHolder>
            
            <asp:PlaceHolder ID="phPaymentReceivedInfo" runat="server" Visible="false">
            <tr><td>First Name:</td><td><asp:Literal ID="paymentReceivedFirstName" runat="server"></asp:Literal></td></tr>
            <tr><td>Last Name:</td><td><asp:Literal ID="paymentReceivedLastName" runat="server"></asp:Literal></td></tr>
            </asp:PlaceHolder>            
            
        </table>
    </div>
</div>
