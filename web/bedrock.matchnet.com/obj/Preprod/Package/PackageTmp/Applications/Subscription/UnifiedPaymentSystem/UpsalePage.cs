﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using Matchnet.Lib;
using Matchnet.Web.Framework;
using Matchnet.Purchase.ServiceAdapters;
using Matchnet.Purchase.ValueObjects;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.Region;
using Matchnet.Configuration.ValueObjects;
using MPVO = Matchnet.Purchase.ValueObjects;

using Spark.Common.UPS;
using Matchnet.Configuration.ServiceAdapters;
using Spark.Common.Adapter;
using Spark.Common.OrderHistoryService;
using Spark.Common.PaymentProfileService;
using Matchnet.Web.Framework.Util;
using Newtonsoft.Json;
using Matchnet.Web.Framework.PagePixels;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Web.Framework.Managers;
using Spark.Common.RestConsumer.V2.Models.Subscription;
using Spark.Common.RestConsumer.V2.Models.OAuth2;


namespace Matchnet.Web.Applications.Subscription.UnifiedPaymentSystem
{
    public class UpsalePage : FrameworkControl, IPaymentUIJSON
    {
        private int version = Constants.NULL_INT;
        private int templateID = Constants.NULL_INT;
        private Matchnet.Purchase.ValueObjects.PaymentType paymentType;
        private int upsLegacyDataID = Constants.NULL_INT;
        private int UPSGlobalLogID = Constants.NULL_INT;
        private bool saveLegacyData = true;
        public Dictionary<String, String> OrderAttributes { get; set; }
        public bool IsSubscriptionConfirmationEmailEnabled { get; set; }
        private PremiumType _premiumType = PremiumType.None;
        OrderInfo orderInfo = null;

        #region IPaymentUIJSON Members
        PremiumType IPaymentUIJSON.PremiumType
        {
            get
            {
                return _premiumType;
            }
            set
            {
                _premiumType = value;
            }
        }

        bool IPaymentUIJSON.SaveLegacyData
        {
            get
            {
                return saveLegacyData;
            }
            set
            {
                saveLegacyData = true;
            }
        }

        int IPaymentUIJSON.Version
        {
            get
            {
                return version;
            }
            set
            {
                version = value;
            }
        }

        int IPaymentUIJSON.TemplateID
        {
            get
            {
                return templateID;
            }
            set
            {
                templateID = value;
            }
        }

        Matchnet.Purchase.ValueObjects.PaymentType IPaymentUIJSON.PaymentType
        {
            get
            {
                return paymentType;
            }
            set
            {
                paymentType = value;
            }
        }

        int IPaymentUIJSON.UPSLegacyDataID
        {
            get
            {
                return upsLegacyDataID;
            }
            set
            {
                upsLegacyDataID = value;
            }
        }

        int IPaymentUIJSON.UPSGlobalLogID
        {
            get
            {
                return UPSGlobalLogID;
            }
            set
            {
                UPSGlobalLogID = value;
            }
        }

        object IPaymentUIJSON.GetJSONObject()
        {
            SettingsManager settingsManager = new SettingsManager();

            if (settingsManager.GetSettingBool("USE_API_FOR_UPS_SUBSCRIPTION_PAYMENT_JSON", g.TargetCommunityID, g.Brand.Site.SiteID, g.TargetBrandID))
                return GetAPIPaymentJson();
            else
                return GetPaymentJson();
        }

        private PaymentUiPostData GetAPIPaymentJson()
        {
            saveLegacyData = false;

            if (version == Constants.NULL_INT)
                throw new Exception("Version cannot be null.");

            if (templateID == Constants.NULL_INT)
                throw new Exception("TemplateID cannot be null.");

            //set up the access token for the API. 
            PaymentUiPostData paymentData = null;
            LogonManager logonManager = new LogonManager(_g);
            OAuthTokens oAuthTokens = logonManager.GetTokensFromCookies();
            string apiToken = string.Empty;
            if (oAuthTokens != null)
            {
                apiToken = oAuthTokens.AccessToken;

                string cancelUrl = string.Empty;
                string destinationURL = string.Empty;
                
                if (Context.Request["gr"] != null)
                {
                    //this is a giftRedemption, so we want them to ultimately end up on the Manage Gifts page
                    cancelUrl = "http://" + g.Page.Request.Url.Host + "/Applications/MemberServices/ManageGifts.aspx";
                }
                else
                {
                    cancelUrl = "http://" + g.Page.Request.Url.Host + "/Applications/MemberServices/MemberServices.aspx";

                }
                  
                destinationURL = (Context.Request["DestinationURL"] != null) ? Context.Server.UrlEncode(Convert.ToString(Context.Request["DestinationURL"])) : string.Empty;
                if (destinationURL.IndexOf(g.Page.Request.Url.Host) < 0)
                {
                    destinationURL = "http://" + g.Page.Request.Url.Host + destinationURL;
                }

                int orderID = Constants.NULL_INT;
                if (Context.Request["oid"] != null)
                {
                    orderID = Convert.ToInt32(Context.Request["oid"]);
                }

                var request = new PaymentUiPostDataRequest
                {
                    PaymentUiPostDataType = "2",
                    CancelUrl = cancelUrl,
                    ConfirmationUrl = "https://" + g.Page.Request.Url.Host + "/Applications/Subscription/SubscriptionConfirmation.aspx",
                    DestinationUrl = destinationURL,
                    ReturnUrl = "http://" + g.Page.Request.Url.Host,
                    ClientIp = g.ClientIP,
                    PrtTitle = "",
                    SrId = (Context.Request["srid"] != null) ? Convert.ToString(Context.Request["srid"]).Replace(",", ";") : string.Empty,
                    PrtId = (Context.Request["prtid"] != null) ? Convert.ToString(Context.Request["prtid"]).Replace(",", ";") : string.Empty,
                    OmnitureVariables = new Dictionary<string, string>(),
                    MemberLevelTrackingLastApplication = "1",
                    MemberLevelTrackingIsMobileDevice = "false",
                    MemberLevelTrackingIsTablet = "false",
                    PromoType = "0",
                    OrderID = orderID.ToString(),
                    TemplateId = this.templateID.ToString(),
                    EID = g.Session[WebConstants.SESSION_PROPERTY_NAME_EID]
                };

                if (_premiumType != PremiumType.None)
                {
                    request.PremiumType = ((int)_premiumType).ToString();
                }

                #region Omniture Variables
                System.Diagnostics.Trace.WriteLine("GetJSONObject() - Start Omniture Variables");

                request.OmnitureVariables.Add("pageName", "Ala Carte - Select Product");
                request.OmnitureVariables.Add("prop17", OmnitureHelper.OmnitureHelper.GetProfileCompetionPercentage(_g.TargetMember, _g.TargetBrand));
                request.OmnitureVariables.Add("prop18", OmnitureHelper.OmnitureHelper.GetGender(_g.TargetMember.GetAttributeInt(_g.TargetBrand, Matchnet.Web.Framework.WebConstants.ATTRIBUTE_NAME_GENDERMASK)));
                request.OmnitureVariables.Add("prop19", OmnitureHelper.OmnitureHelper.GetAge(_g.TargetMember.GetAttributeDate(_g.TargetBrand, Matchnet.Web.Framework.WebConstants.ATTRIBUTE_NAME_BIRTHDATE)).ToString());
                if (_g.TargetBrand.Site.Community.CommunityID == (int)WebConstants.COMMUNITY_ID.JDate)
                {
                    request.OmnitureVariables.Add("prop20", MemberProfile.ProfileDisplayHelper.GetOptionValue(_g.TargetMember, _g, "JDateEthnicity", "JDateEthnicity"));
                }
                else
                {
                    request.OmnitureVariables.Add("prop20", MemberProfile.ProfileDisplayHelper.GetOptionValue(_g.TargetMember, _g, "Ethnicity", "Ethnicity"));
                }
                request.OmnitureVariables.Add("prop21", OmnitureHelper.OmnitureHelper.GetRegionString(_g.TargetMember, _g.TargetBrand, _g.TargetBrand.Site.LanguageID, false, true, false).Replace("\"", string.Empty));
                request.OmnitureVariables.Add("prop23", _g.TargetMember.MemberID.ToString());
                request.OmnitureVariables.Add("eVar44", string.Format(WebConstants.OMNITURE_EVAR44_MEMBERID, (_g.TargetMember.MemberID % 10).ToString()));
                request.OmnitureVariables.Add("prop29", g.AnalyticsOmniture.Prop29);
                
                request.OmnitureVariables.Add("purchaseID", (Context.Request["oid"] != null) ? Convert.ToString(Context.Request["oid"]) : string.Empty);
                // Subscription - Products
                if (orderInfo != null)
                {
                    Matchnet.Purchase.ValueObjects.Plan primaryPlan = UPSGetPrimaryPlan();
                    if (primaryPlan != null)
                    {
                        request.OmnitureVariables.Add("products", primaryPlan.PlanID.ToString() + ";" + primaryPlan.InitialDuration.ToString() + primaryPlan.InitialDurationType.ToString() + ";" + primaryPlan.RenewDuration
                            + ";" + orderInfo.TotalAmount);
                    }
                }
                else
                {
                    request.OmnitureVariables.Add("products", String.Empty);
                }
                request.OmnitureVariables.Add("evar11", g.Session[WebConstants.SESSION_PROPERTY_NAME_PROMOTIONID]);
                request.OmnitureVariables.Add("evar12", g.Session[WebConstants.SESSION_PROPERTY_NAME_EID]);
                request.OmnitureVariables.Add("evar13", g.Session[WebConstants.SESSION_PROPERTY_NAME_LUGGAGE]);

                #endregion

                APIPaymentUIJSON apiPaymentUIPSON = new APIPaymentUIJSON();
                paymentData = apiPaymentUIPSON.PostPayment(request, apiToken, g.Brand.BrandID.ToString());

                if (paymentData == null)
                    throw new Exception("GetAPIPaymentJson Unable to get payment data from Subcription. Upsale page");

            }
            else
                throw new Exception("UpsalePage...GetAPIPaymentJson...oAuthTokens was null");

            return paymentData;      
        }

        #region old payment information 
        private PaymentJson GetPaymentJson()
        {
            if (version == Constants.NULL_INT)
                throw new Exception("Version cannot be null.");

            if (templateID == Constants.NULL_INT)
                throw new Exception("TemplateID cannot be null.");

            if (upsLegacyDataID == Constants.NULL_INT)
                throw new Exception("UPS Legacy Data ID cannot be null.");

            SettingsManager settingsManager = new SettingsManager();
            Dictionary<string, string> colDataAttributes = new Dictionary<string, string>();

            try
            {
                if (g.TargetMember == null)
                    throw new Exception("Member cannot null.");

                TraceLogConnectorServiceWebAdapter.InsertEndpointTraceLog(UPSGlobalLogID, "PaymentUIConnector", "GetJSONObject", "ENTER PaymentUIConnector jump page", null, 1, Spark.Common.TraceLogConnectorService.UPSEndpointProcessingStatus.NONE, DateTime.Now);

                int memberID = g.TargetMember.MemberID;

                System.Diagnostics.Trace.WriteLine("Begin gathering JSON data in the upsale page");

                PaymentJson payment = new PaymentJson();
                string affiliateTrackingPixel = Constants.NULL_STRING;


                // Only show the confirmation section in the upsale page if going directly to the upsale
                // page without previously showing the confirmation page after a subscription purchase 
                if (Convert.ToBoolean((RuntimeSettings.GetSetting("UPS_DIRECT_TO_UPSALE_WITH_NO_CONFIRMATION_PAGE", g.TargetBrand.Site.Community.CommunityID,
                    g.TargetBrand.Site.SiteID, g.Brand.BrandID))))
                {
                    int orderID = Constants.NULL_INT;
                    ObsfucatedPaymentProfileResponse paymentProfileResponse = null;

                    if (Context.Request["oid"] != null)
                    {
                        orderID = Convert.ToInt32(Context.Request["oid"]);

                        System.Diagnostics.Trace.WriteLine("Get order information for Order ID [" + Convert.ToString(orderID) + "]");

                        // Get the affiliate tracking pixel for the subscription confirmation page 
                        PagePixelControl ucPixel = new PagePixelControl();
                        ucPixel.PageID = 3500;
                        affiliateTrackingPixel = ucPixel.WritePixel();

                        //get UPS order info
                        orderInfo = OrderHistoryServiceWebAdapter.GetProxyInstance().GetOrderInfo(orderID);
                        OrderHistoryServiceWebAdapter.CloseProxyInstance();

                        if (orderInfo != null)
                        {
                            System.Diagnostics.Trace.WriteLine("Successfully retrieved order information for Order ID [" + Convert.ToString(orderID) + "]");

                            //get payment profile info
                            paymentProfileResponse = PaymentProfileServiceWebAdapter.GetProxyInstance().GetObsfucatedPaymentProfileByOrderID(orderID, orderInfo.CustomerID, orderInfo.CallingSystemID);
                            PaymentProfileServiceWebAdapter.CloseProxyInstance();





                            string orderCustomerID = Constants.NULL_STRING;
                            string orderConfirmationNumber = Constants.NULL_STRING;
                            string orderCreditCardNumber = Constants.NULL_STRING;
                            string orderCreditCardType = Constants.NULL_STRING;
                            string orderTransactionDate = DateTime.Now.ToString();
                            string orderPaymentType = Constants.NULL_STRING;
                            string orderTotalAmount = Constants.NULL_STRING;

                            if (paymentProfileResponse != null && orderInfo != null)
                            {
                                System.Diagnostics.Trace.WriteLine("Successfully retrieved payment profile information for Order ID [" + Convert.ToString(orderID) + "]");

                                orderConfirmationNumber = orderID.ToString();
                                orderCustomerID = g.TargetMember.GetAttributeText(g.TargetBrand, "UserName");
                                orderPaymentType = paymentProfileResponse.PaymentProfile.PaymentType;
                                orderTotalAmount = orderInfo.TotalAmount.ToString();
                                orderTransactionDate = DateTime.Now.ToString();

                                if (paymentProfileResponse.PaymentProfile is ObsfucatedCreditCardPaymentProfile)
                                {
                                    ObsfucatedCreditCardPaymentProfile ccPaymentProfile = paymentProfileResponse.PaymentProfile as ObsfucatedCreditCardPaymentProfile;
                                    orderCreditCardType = ccPaymentProfile.CardType;
                                    orderCreditCardNumber = ccPaymentProfile.LastFourDigitsCreditCardNumber;
                                }
                                else if (paymentProfileResponse.PaymentProfile is ObfuscatedCreditCardShortFormFullWebSitePaymentProfile)
                                {
                                    ObfuscatedCreditCardShortFormFullWebSitePaymentProfile ccShortFormFullWebSitePaymentProfile = paymentProfileResponse.PaymentProfile as ObfuscatedCreditCardShortFormFullWebSitePaymentProfile;
                                    orderCreditCardType = ccShortFormFullWebSitePaymentProfile.CardType;
                                    orderCreditCardNumber = ccShortFormFullWebSitePaymentProfile.LastFourDigitsCreditCardNumber;
                                }
                                else if (paymentProfileResponse.PaymentProfile is ObfuscatedCreditCardShortFormMobileSitePaymentProfile)
                                {
                                    ObfuscatedCreditCardShortFormMobileSitePaymentProfile ccShortFormMobileSitePaymentProfile = paymentProfileResponse.PaymentProfile as ObfuscatedCreditCardShortFormMobileSitePaymentProfile;
                                    orderCreditCardType = ccShortFormMobileSitePaymentProfile.CardType;
                                    orderCreditCardNumber = ccShortFormMobileSitePaymentProfile.LastFourDigitsCreditCardNumber;
                                }
                                else if (paymentProfileResponse.PaymentProfile is ObfuscatedDebitCardPaymentProfile)
                                {
                                    ObfuscatedDebitCardPaymentProfile debitCardPaymentProfile = paymentProfileResponse.PaymentProfile as ObfuscatedDebitCardPaymentProfile;
                                    orderCreditCardType = debitCardPaymentProfile.CardType;
                                    orderCreditCardNumber = debitCardPaymentProfile.LastFourDigitsCreditCardNumber;
                                }

                                PaymentJsonArray orderConfirmation = new PaymentJsonArray();

                                orderConfirmation.Add("orderCustomerID", orderCustomerID);
                                orderConfirmation.Add("orderConfirmationNumber", orderConfirmationNumber);
                                orderConfirmation.Add("orderCreditCardNumber", orderCreditCardNumber);
                                orderConfirmation.Add("orderCreditCardType", orderCreditCardType);
                                orderConfirmation.Add("orderTransactionDate", orderTransactionDate);
                                orderConfirmation.Add("orderPaymentType", orderPaymentType);
                                orderConfirmation.Add("orderTotalAmount", orderTotalAmount);

                                payment.Data.OrderConfirmation.Add(orderConfirmation);
                            }
                            else
                            {
                                throw new Exception("Cannot get payment profile details for the order confirmation.");
                            }

                        }
                        else
                        {
                            throw new Exception("Cannot get order details for the order confirmation.");
                        }
                    }
                }

                // Get the most recent Plan ID that the member is on  
                Spark.Common.RenewalService.RenewalSubscription renewalSub = RenewalManager.Instance.GetRenewalSubscription(g.TargetMemberID, g.TargetBrand);
                if (renewalSub == null || renewalSub.RenewalSubscriptionID <= 0 || (renewalSub.RenewalDatePST < DateTime.Now))
                {
                    System.Diagnostics.Trace.WriteLine("Cannot find RenewalSubscription for Member ID [" + Convert.ToString(memberID) + "]");
                    System.Diagnostics.Trace.WriteLine("Redirect to the subscription page since the member does not have a basic subscription privilege");

                    // Basic subscription privilege is required
                    // If the member does not have basic subscription privilege, redirect the user to the subscription
                    // page to purchase a basic subscription 
                    Redirect.Subscription(g.TargetBrand
                        , (int)Matchnet.Content.ServiceAdapters.Links.PurchaseReasonType.AttemptToAccessUpsalePackagesWithNoSubscription
                        , Constants.NULL_INT
                        , false
                        , (Context.Request["DestinationURL"] != null) ? Context.Server.UrlEncode(Convert.ToString(Context.Request["DestinationURL"])) : string.Empty);
                    //g.Notification.AddErrorString("Member does not have a subscription. MemberID:" + g.TargetMember.MemberID);
                    //throw new Exception("Membersub cannot be null. MemberID:" + g.TargetMember.MemberID);
                }

                int activePlanID = renewalSub.PrimaryPackageID;
                //MPVO.Plan activePlan = PlanSA.Instance.GetPlan(activePlanID, g.TargetBrand.BrandID);

                // Get all the privileges that have not expired for this member for this site  
                Spark.Common.AccessService.AccessPrivilege[] accessPrivileges = Spark.Common.Adapter.AccessServiceWebAdapter.GetProxyInstance().GetCustomerPrivilegeListAllWithIgnoreCache(memberID);
                List<Spark.Common.AccessService.AccessPrivilege> activePrivileges = new List<Spark.Common.AccessService.AccessPrivilege>();
                foreach (Spark.Common.AccessService.AccessPrivilege checkAccessPrivilege in accessPrivileges)
                {
                    if (checkAccessPrivilege.CallingSystemID == g.TargetBrand.Site.SiteID)
                    {
                        if (checkAccessPrivilege.UnifiedPrivilegeType == Spark.Common.AccessService.PrivilegeType.BasicSubscription
                            || checkAccessPrivilege.UnifiedPrivilegeType == Spark.Common.AccessService.PrivilegeType.HighlightedProfile
                            || checkAccessPrivilege.UnifiedPrivilegeType == Spark.Common.AccessService.PrivilegeType.SpotlightMember
                            || checkAccessPrivilege.UnifiedPrivilegeType == Spark.Common.AccessService.PrivilegeType.JMeter
                            || checkAccessPrivilege.UnifiedPrivilegeType == Spark.Common.AccessService.PrivilegeType.AllAccess
                            || checkAccessPrivilege.UnifiedPrivilegeType == Spark.Common.AccessService.PrivilegeType.ReadReceipt)
                        {
                            if (checkAccessPrivilege.EndDateUTC > DateTime.Now)
                            {
                                activePrivileges.Add(checkAccessPrivilege);
                            }
                        }
                    }
                }

                // Get the details of the most recent plan that the member is on  
                Spark.Common.CatalogService.Package activeSubscriptionPackage = Spark.Common.Adapter.CatalogServiceWebAdapter.GetProxyInstance().GetPackageDetails(activePlanID);
                Spark.Common.Adapter.CatalogServiceWebAdapter.CloseProxyInstance();
                string upsaleFromType = "Standard";
                if (activeSubscriptionPackage.PackageType == Spark.Common.CatalogService.PackageType.Bundled)
                {
                    upsaleFromType = "BundledWithPremiumServices";
                }
                else if (activeSubscriptionPackage.PackageType == Spark.Common.CatalogService.PackageType.Basic)
                {
                    // Check to see if the member had purchased any a la carte premium service in addition to the
                    // standard plan that the member has most recently purchased
                    bool hasAtLeastOneActivePremiumService = false;

                    foreach (Spark.Common.AccessService.AccessPrivilege checkAccessPrivilege in activePrivileges)
                    {
                        if (checkAccessPrivilege.UnifiedPrivilegeType == Spark.Common.AccessService.PrivilegeType.HighlightedProfile
                            || checkAccessPrivilege.UnifiedPrivilegeType == Spark.Common.AccessService.PrivilegeType.SpotlightMember
                            || checkAccessPrivilege.UnifiedPrivilegeType == Spark.Common.AccessService.PrivilegeType.JMeter
                            || checkAccessPrivilege.UnifiedPrivilegeType == Spark.Common.AccessService.PrivilegeType.AllAccess
                            || checkAccessPrivilege.UnifiedPrivilegeType == Spark.Common.AccessService.PrivilegeType.ReadReceipt)
                        {
                            hasAtLeastOneActivePremiumService = true;
                        }
                    }

                    if (hasAtLeastOneActivePremiumService)
                    {
                        upsaleFromType = "StandardWithAlacarte";
                    }
                    else
                    {
                        upsaleFromType = "Standard";
                    }
                }

                // Get the packages allowed for upsale for this member 
                Spark.Common.CatalogService.Item activeSubscriptionPackageBasicItem = null;
                //Matchnet.Web.CatalogServiceReference.Item activeSubscriptionPackageBasicItem = null;
                foreach (Spark.Common.CatalogService.Item item in activeSubscriptionPackage.Items)
                {
                    List<Spark.Common.CatalogService.PrivilegeType> itemPrivileges = new List<Spark.Common.CatalogService.PrivilegeType>();
                    itemPrivileges.AddRange(item.PrivilegeType);
                    if (itemPrivileges.Contains(Spark.Common.CatalogService.PrivilegeType.BasicSubscription))
                    {
                        // Use the basic subscription item in the package to determine the duration and duration type of the package  
                        activeSubscriptionPackageBasicItem = item;
                        break;
                    }
                }
                int[] arrUpsalePackages = Spark.Common.Adapter.CatalogServiceWebAdapter.GetProxyInstance().GetUpsalePackages(activeSubscriptionPackageBasicItem.Duration, Enum.GetName(typeof(DurationType), activeSubscriptionPackageBasicItem.DurationType), g.TargetBrand.Site.SiteID, upsaleFromType, activeSubscriptionPackageBasicItem.RenewalDuration, Enum.GetName(typeof(DurationType), activeSubscriptionPackageBasicItem.RenewalDurationType));
                Spark.Common.Adapter.CatalogServiceWebAdapter.CloseProxyInstance();
                List<int> upsalePackages = new List<int>();
                upsalePackages.AddRange(arrUpsalePackages);

                List<MPVO.Plan> colPlansAvailableForUpsale = new List<MPVO.Plan>();
                foreach (int planID in upsalePackages)
                {
                    MPVO.Plan upsalePlan = PlanSA.Instance.GetPlan(planID, g.TargetBrand.BrandID);

                    if (upsalePlan != null)
                    {
                        colPlansAvailableForUpsale.Add(upsalePlan);
                    }
                }

                // Go through the upsale package list and remove the ones that are not allowed for purchase
                // since the member already has the maximum privilege time for upsale package  
                List<MPVO.Plan> allowedUpsalePlanCollection = new List<Purchase.ValueObjects.Plan>();

                // There is an upsale package that contains both all access and renewable all access emails 
                // This package is only available for upsale if the all access expiration date is before the 
                // basic subscription expiration date 
                // There is no need to check the count for the all access email part of the package 
                // If the user wants to purchase more all access emails, they can do that as a fixed pricing upgrade 
                // In the fixed pricing upgrade purchase, there will be a check to see what the limit
                // of the all access emails are 
                foreach (MPVO.Plan plan in colPlansAvailableForUpsale)
                {
                    if ((plan.PremiumTypeMask & PremiumType.HighlightedProfile) == PremiumType.HighlightedProfile)
                    {
                        if (IsAllowedToPurchaseUpsalePlan(activePrivileges, Spark.Common.AccessService.PrivilegeType.HighlightedProfile, renewalSub.RenewalDatePST))
                        {
                            allowedUpsalePlanCollection.Add(plan);
                        }
                    }
                    else if ((plan.PremiumTypeMask & PremiumType.SpotlightMember) == PremiumType.SpotlightMember)
                    {
                        if (IsAllowedToPurchaseUpsalePlan(activePrivileges, Spark.Common.AccessService.PrivilegeType.SpotlightMember, renewalSub.RenewalDatePST))
                        {
                            allowedUpsalePlanCollection.Add(plan);
                        }
                    }
                    else if ((plan.PremiumTypeMask & PremiumType.JMeter) == PremiumType.JMeter)
                    {
                        if (IsAllowedToPurchaseUpsalePlan(activePrivileges, Spark.Common.AccessService.PrivilegeType.JMeter, renewalSub.RenewalDatePST))
                        {
                            allowedUpsalePlanCollection.Add(plan);
                        }
                    }
                    else if ((plan.PremiumTypeMask & PremiumType.AllAccess) == PremiumType.AllAccess)
                    {
                        if (IsAllowedToPurchaseUpsalePlan(activePrivileges, Spark.Common.AccessService.PrivilegeType.AllAccess, renewalSub.RenewalDatePST))
                        {
                            allowedUpsalePlanCollection.Add(plan);
                        }
                    }
                    else if ((plan.PremiumTypeMask & PremiumType.ReadReceipt) == PremiumType.ReadReceipt)
                    {
                        if (IsAllowedToPurchaseUpsalePlan(activePrivileges, Spark.Common.AccessService.PrivilegeType.ReadReceipt, renewalSub.RenewalDatePST))
                        {
                            allowedUpsalePlanCollection.Add(plan);
                        }
                    }

                }

                payment.UPSLegacyDataID = upsLegacyDataID;
                payment.UPSGlobalLogID = UPSGlobalLogID;
                payment.GetCustomerPaymentProfile = 1;
                payment.Version = version;
                payment.CallingSystemID = g.TargetBrand.Site.SiteID;
                payment.TemplateID = templateID;
                payment.TimeStamp = DateTime.Now;
                payment.IsSubscriptionConfirmationEmailEnabled = settingsManager.GetSettingBool(SettingConstants.ENABLE_SUBSCRIPTION_CONFIRMATION_EMAIL, g.Brand);

                if (Context.Request["gr"] != null)
                {
                    //this is a giftRedemption, so we want them to ultimately end up on the Manage Gifts page
                    payment.Data.Navigation.CancelURL = "http://" + g.Page.Request.Url.Host + "/Applications/MemberServices/ManageGifts.aspx";
                }
                else
                {
                    payment.Data.Navigation.CancelURL = "http://" + g.Page.Request.Url.Host + "/Applications/MemberServices/MemberServices.aspx";

                }

                payment.Data.Navigation.ReturnURL = "http://" + g.Page.Request.Url.Host;
                payment.Data.Navigation.ConfirmationURL = "https://" + g.Page.Request.Url.Host + "/Applications/Subscription/SubscriptionConfirmation.aspx";
                payment.Data.Navigation.DestinationURL = (Context.Request["DestinationURL"] != null) ? Context.Server.UrlEncode(Convert.ToString(Context.Request["DestinationURL"])) : string.Empty;
                if (payment.Data.Navigation.DestinationURL.IndexOf(g.Page.Request.Url.Host) < 0)
                {
                    payment.Data.Navigation.DestinationURL = "http://" + g.Page.Request.Url.Host + payment.Data.Navigation.DestinationURL;
                }


                // Order Attributes
                OrderAttributes = new Dictionary<string, string>();

                OrderAttributes.Add("SRID", (Context.Request["srid"] != null) ? Convert.ToString(Context.Request["srid"]).Replace(",", ";") : string.Empty);


                if (Context.Request["prtid"] != null)
                {
                    OrderAttributes.Add("PRTID", (Context.Request["prtid"] != null) ? Convert.ToString(Context.Request["prtid"]).Replace(",", ";") : string.Empty);
                    OrderAttributes.Add("PricingGroupID", string.Empty);

                    OrderAttributes.Add("OriginalSubscriptionStatusBeforeUpsale", string.Empty);
                    OrderAttributes.Add("OriginalTemplateIDBeforeUpsale", string.Empty);
                }
                else if (orderInfo != null)
                {
                    // Get these values from the previous purchase instead of the upsale purchase 
                    OrderAttributes.Add("PRTID", (orderInfo.PurchaseReasonTypeID > 0) ? Convert.ToString(orderInfo.PurchaseReasonTypeID) : string.Empty);
                    OrderAttributes.Add("SecondaryPRTIDs", (!string.IsNullOrEmpty(orderInfo.SecondaryPurchaseReasonTypeIDs)) ? orderInfo.SecondaryPurchaseReasonTypeIDs : string.Empty);
                    OrderAttributes.Add("PricingGroupID", (orderInfo.PromoID > 0) ? Convert.ToString(orderInfo.PromoID) : string.Empty);

                    OrderAttributes.Add("OriginalSubscriptionStatusBeforeUpsale", orderInfo.OriginalSubscriptionStatusBeforeUpsale);
                    OrderAttributes.Add("OriginalTemplateIDBeforeUpsale", Convert.ToString(orderInfo.OriginalTemplateIDBeforeUpsale));
                }
                else
                {
                    OrderAttributes.Add("PRTID", string.Empty);
                    OrderAttributes.Add("PricingGroupID", string.Empty);

                    OrderAttributes.Add("OriginalSubscriptionStatusBeforeUpsale", string.Empty);
                    OrderAttributes.Add("OriginalTemplateIDBeforeUpsale", string.Empty);
                }

                foreach (string key in OrderAttributes.Keys)
                {
                    if (OrderAttributes[key] != string.Empty)
                        payment.Data.OrderAttributes += key + "=" + OrderAttributes[key] + ",";
                }

                if (payment.Data.OrderAttributes.Length > 0)
                    payment.Data.OrderAttributes = payment.Data.OrderAttributes.TrimEnd(',');

                // Member Info
                payment.Data.MemberInfo.CustomerID = g.TargetMemberID;
                payment.Data.MemberInfo.RegionID = g.TargetMember.GetAttributeInt(g.TargetBrand, "RegionID");
                if (payment.Data.MemberInfo.RegionID == Constants.NULL_INT
                    || payment.Data.MemberInfo.RegionID < 0)
                {
                    // There must be a valid RegionID to price the cart so get it from the database if the cache does not have a valid RegionID  
                    Member.ServiceAdapters.Member refreshedMember = MemberSA.Instance.GetMember(35, MemberLoadFlags.IngoreSACache);
                    payment.Data.MemberInfo.RegionID = refreshedMember.GetAttributeInt(g.TargetBrand, "RegionID");
                }
                payment.Data.MemberInfo.Language = ((Language)Enum.Parse(typeof(Language), g.TargetBrand.Site.LanguageID.ToString())).ToString();

                RegionLanguage regionLanguage = RegionSA.Instance.RetrievePopulatedHierarchy(payment.Data.MemberInfo.RegionID, (int)Language.English);
                if (regionLanguage.CountryRegionID == ConstantsTemp.REGIONID_USA)
                    payment.Data.MemberInfo.State = regionLanguage.StateAbbreviation.Trim().ToUpper();

                //LC: Adding extra fields needed for Kount RIS
                payment.Data.MemberInfo.EmailAddress = g.Member.GetAttributeText(g.Brand, "EmailAddress");
                payment.Data.MemberInfo.GenderMask = SubscriberManager.Instance.GetGender(g.Member.GetAttributeInt(g.Brand, "gendermask"));
                payment.Data.MemberInfo.BirthDate = g.Member.GetAttributeDate(g.Brand, "birthdate", DateTime.MinValue);
                payment.Data.MemberInfo.BrandInsertDate = g.Member.GetAttributeDate(g.Brand, "BrandInsertDate", DateTime.MinValue);

                #region UDFS
                //JS-1228 LC: Adding UDF fields needed for Kount
                payment.Data.MemberInfo.UserName = g.Member.GetUserName(g.Brand);
                payment.Data.MemberInfo.MaritalStatus = Option.GetDescription("MaritalStatus", g.Member.GetAttributeInt(g.Brand, "MaritalStatus"), g);
                payment.Data.MemberInfo.Occupation = g.Member.GetAttributeText(g.Brand, "OccupationDescription");
                payment.Data.MemberInfo.Education = Option.GetDescription("EducationLevel", g.Member.GetAttributeInt(g.Brand, "EducationLevel"), g);
                payment.Data.MemberInfo.PromotionID = g.Member.GetAttributeInt(g.Brand, "PromotionID").ToString();
                payment.Data.MemberInfo.Eyes = Option.GetDescription("EyeColor", g.Member.GetAttributeInt(g.Brand, "EyeColor"), g);
                payment.Data.MemberInfo.Hair = Option.GetDescription("HairColor", g.Member.GetAttributeInt(g.Brand, "HairColor"), g);
                payment.Data.MemberInfo.Height = g.Member.GetAttributeInt(g.Brand, "Height").ToString();

                if (g.Brand.Site.Community.CommunityID == (int)WebConstants.COMMUNITY_ID.JDate)
                {
                    payment.Data.MemberInfo.Ethnicity = Option.GetDescription("JDateEthnicity", g.Member.GetAttributeInt(g.Brand, "JDateEthnicity"), g);
                    payment.Data.MemberInfo.Religion = Option.GetDescription("JDateReligion", g.Member.GetAttributeInt(g.Brand, "JDateReligion"), g);
                }
                else
                {
                    payment.Data.MemberInfo.Ethnicity = Option.GetDescription("Ethnicity", g.Member.GetAttributeInt(g.Brand, "Ethnicity"), g);
                    payment.Data.MemberInfo.Religion = Option.GetDescription("Religion", g.Member.GetAttributeInt(g.Brand, "Religion"), g);
                }

                string aboutMe = g.Member.GetAttributeText(g.Brand, "AboutMe");
                payment.Data.MemberInfo.AboutMe = aboutMe.Length < 251 ? aboutMe : aboutMe.Substring(0, 250);

                #endregion UDF

                //// Omniture Variables
                PaymentJsonArray omnitureVariables = new PaymentJsonArray();

                omnitureVariables.Add("pageName", "Ala Carte - Select Product");
                omnitureVariables.Add("prop17", OmnitureHelper.OmnitureHelper.GetProfileCompetionPercentage(_g.TargetMember, _g.TargetBrand));
                omnitureVariables.Add("prop18", OmnitureHelper.OmnitureHelper.GetGender(_g.TargetMember.GetAttributeInt(_g.TargetBrand, Matchnet.Web.Framework.WebConstants.ATTRIBUTE_NAME_GENDERMASK)));
                omnitureVariables.Add("prop19", OmnitureHelper.OmnitureHelper.GetAge(_g.TargetMember.GetAttributeDate(_g.TargetBrand, Matchnet.Web.Framework.WebConstants.ATTRIBUTE_NAME_BIRTHDATE)).ToString());
                if (_g.TargetBrand.Site.Community.CommunityID == (int)WebConstants.COMMUNITY_ID.JDate)
                {
                    omnitureVariables.Add("prop20", MemberProfile.ProfileDisplayHelper.GetOptionValue(_g.TargetMember, _g, "JDateEthnicity", "JDateEthnicity"));
                }
                else
                {
                    omnitureVariables.Add("prop20", MemberProfile.ProfileDisplayHelper.GetOptionValue(_g.TargetMember, _g, "Ethnicity", "Ethnicity"));
                }
                omnitureVariables.Add("prop21", OmnitureHelper.OmnitureHelper.GetRegionString(_g.TargetMember, _g.TargetBrand, _g.TargetBrand.Site.LanguageID, false, true, false).Replace("\"", string.Empty));
                omnitureVariables.Add("prop23", _g.TargetMember.MemberID.ToString());
                if (payment.Data.OrderConfirmation.Count > 0)
                {
                    omnitureVariables.Add("events", "purchase,event48,event10");
                }
                else
                {
                    omnitureVariables.Add("events", "purchase,event48");
                }
                omnitureVariables.Add("eVar44", string.Format(WebConstants.OMNITURE_EVAR44_MEMBERID, (_g.TargetMember.MemberID % 10).ToString()));
                omnitureVariables.Add("prop29", g.AnalyticsOmniture.Prop29);
                omnitureVariables.Add("eVar4", OrderAttributes["PricingGroupID"]);
                omnitureVariables.Add("eVar6", (orderInfo != null && !string.IsNullOrEmpty(orderInfo.SecondaryPurchaseReasonTypeIDs) ?
                                                orderInfo.PurchaseReasonTypeID.ToString() + '-' + orderInfo.SecondaryPurchaseReasonTypeIDs.Replace('|', '-')
                                                : OrderAttributes["PRTID"]));

                //int memberSubscriptionStatus = FrameworkGlobals.GetMemberSubcriptionStatus(_g.TargetMember, _g.TargetBrand);
                //SubscriptionStatus enumSubscriptionStatus = (SubscriptionStatus)memberSubscriptionStatus;
                omnitureVariables.Add("eVar8", OrderAttributes["OriginalSubscriptionStatusBeforeUpsale"]);

                omnitureVariables.Add("purchaseID", (Context.Request["oid"] != null) ? Convert.ToString(Context.Request["oid"]) : string.Empty);
                // Subscription - Products
                if (orderInfo != null)
                {
                    Matchnet.Purchase.ValueObjects.Plan primaryPlan = UPSGetPrimaryPlan();
                    if (primaryPlan != null)
                    {
                        omnitureVariables.Add("products", primaryPlan.PlanID.ToString() + ";" + primaryPlan.InitialDuration.ToString() + primaryPlan.InitialDurationType.ToString() + ";" + primaryPlan.RenewDuration
                            + ";" + orderInfo.TotalAmount);
                    }
                }
                else
                {
                    omnitureVariables.Add("products", String.Empty);
                }
                omnitureVariables.Add("evar11", g.Session[WebConstants.SESSION_PROPERTY_NAME_PROMOTIONID]);
                omnitureVariables.Add("evar12", g.Session[WebConstants.SESSION_PROPERTY_NAME_EID]);
                omnitureVariables.Add("evar13", g.Session[WebConstants.SESSION_PROPERTY_NAME_LUGGAGE]);

                omnitureVariables.Add("eVar31", OrderAttributes["OriginalTemplateIDBeforeUpsale"]);

                payment.Data.OmnitureVariables.Add(omnitureVariables);

                if (affiliateTrackingPixel != Constants.NULL_STRING)
                {
                    payment.Data.AffiliateTrackingPixel = affiliateTrackingPixel;
                }

                // Legacy Data Info
                if (g.Member.MemberID != g.TargetMemberID)
                {
                    payment.Data.LegacyDataInfo.AdminMemberID = g.Member.MemberID;
                }
                payment.Data.LegacyDataInfo.Member = g.TargetMemberID;
                payment.Data.LegacyDataInfo.BrandID = g.TargetBrand.BrandID;
                payment.Data.LegacyDataInfo.SiteID = g.TargetBrand.Site.SiteID;

                System.Text.StringBuilder planIDList = new System.Text.StringBuilder();

                // A La Carte Plans
                // Should only be two. One of each premium service.
                foreach (MPVO.Plan plan in allowedUpsalePlanCollection)
                {
                    if (plan.EndDate != DateTime.MinValue)
                        continue;

                    PaymentJsonPackage package;
                    package = new PaymentJsonPackage();

                    package.ID = plan.PlanID;
                    package.Description = "";
                    package.SytemID = g.TargetBrand.Site.SiteID;
                    package.StartDate = renewalSub.RenewalDatePST;// memberSub.EndDate;
                    package.ExpiredDate = renewalSub.RenewalDatePST;// memberSub.EndDate;
                    package.CurrencyType = PaymentUIConnector.GetCurrencyAbbreviation(plan.CurrencyType);

                    package.Items.Add("InitialCostPerDuration", plan.InitialCost);
                    decimal proratedAmount = GetMemberProratedAmount(plan.InitialCost, renewalSub.RenewalDatePST, GetStartDateForUpsalePlan(plan, activePrivileges));
                    package.Items.Add("ProratedAmount", proratedAmount);
                    package.Items.Add("DurationType", "Day");
                    package.Items.Add("Duration", GetDuration(renewalSub.RenewalDatePST, GetStartDateForUpsalePlan(plan, activePrivileges)));
                    package.Items.Add("PlanID", plan.PlanID);
                    package.Items.Add("CurrencyType", PaymentUIConnector.GetCurrencyAbbreviation(plan.CurrencyType));
                    package.Items.Add("PaymentTypeMask", plan.PaymentTypeMask);
                    package.Items.Add("CreditAmount", (plan.CreditAmount < 0) ? 0 : plan.CreditAmount);
                    package.Items.Add("PurchaseMode", PurchaseMode.New.ToString());
                    package.Items.Add("PremiumType", plan.PremiumTypeMask.ToString());
                    package.Items.Add("RenewCost", plan.RenewCost);

                    if (proratedAmount > 0)
                    {
                        // If the package for upsale has amount of 0, do not display this package. 
                        payment.Data.Packages.Add(package);

                        if (package.ID > 0)
                        {
                            planIDList.Append(package.ID.ToString() + ",");
                        }
                    }
                };

                System.Diagnostics.Trace.WriteLine("JSON data to be sent: " + JsonConvert.SerializeObject(payment));

                colDataAttributes.Add("UPSLegacyDataID", Convert.ToString(upsLegacyDataID));
                colDataAttributes.Add("CustomerID", Convert.ToString(memberID));
                colDataAttributes.Add("CallingSystemID", Convert.ToString(g.TargetBrand.Site.SiteID));
                colDataAttributes.Add("TransactionType", "23");
                colDataAttributes.Add("PRTID", OrderAttributes["PRTID"]);
                colDataAttributes.Add("SRID", OrderAttributes["SRID"]);
                colDataAttributes.Add("PricingGroupID", OrderAttributes["PricingGroupID"]);
                colDataAttributes.Add("RegionID", Convert.ToString(payment.Data.MemberInfo.RegionID));
                colDataAttributes.Add("IPAddress", payment.Data.MemberInfo.IPAddress);
                colDataAttributes.Add("TemplateID", Convert.ToString(payment.TemplateID));
                colDataAttributes.Add("CancelURL", payment.Data.Navigation.CancelURL);
                colDataAttributes.Add("ConfirmationURL", payment.Data.Navigation.ConfirmationURL);
                colDataAttributes.Add("DestinationURL", payment.Data.Navigation.DestinationURL);
                colDataAttributes.Add("PlanIDList", planIDList.ToString().TrimEnd(','));
                colDataAttributes.Add("PaymentUIConnectorType", "UpsalePage");

                TraceLogConnectorServiceWebAdapter.InsertEndpointTraceLog(UPSGlobalLogID, "PaymentUIConnector", "GetJSONObject", "Save data attributes", colDataAttributes, Constants.NULL_INT, Spark.Common.TraceLogConnectorService.UPSEndpointProcessingStatus.NONE, DateTime.Now);
                TraceLogConnectorServiceWebAdapter.InsertEndpointTraceLog(UPSGlobalLogID, "PaymentUIConnector", "GetJSONObject", "NO ERROR", null, Constants.NULL_INT, Spark.Common.TraceLogConnectorService.UPSEndpointProcessingStatus.OK, DateTime.Now);

                return payment;

            }
            catch (ThreadAbortException tex)
            {
                //we don't care about thread abort exceptions, so don't log, just rethrow
                throw tex;
            }
            catch (Exception ex)
            {
                TraceLogConnectorServiceWebAdapter.InsertEndpointTraceLog(UPSGlobalLogID, "PaymentUIConnector", "GetJSONObject", "Save data attributes", colDataAttributes, Constants.NULL_INT, Spark.Common.TraceLogConnectorService.UPSEndpointProcessingStatus.NONE, DateTime.Now);
                TraceLogConnectorServiceWebAdapter.InsertEndpointTraceLog(UPSGlobalLogID, "PaymentUIConnector", "GetJSONObject", "ERROR DETECTED: " + ex.Message, null, Constants.NULL_INT, Spark.Common.TraceLogConnectorService.UPSEndpointProcessingStatus.ERROR, DateTime.Now);

                throw new Exception("Error in building JSON for PaymentUI. MemberID:" +
                    g.TargetMember.MemberID, ex);
            }
            finally
            {
                TraceLogConnectorServiceWebAdapter.InsertEndpointTraceLog(UPSGlobalLogID, "PaymentUIConnector", "GetJSONObject", "EXIT PaymentUIConnector jump page", null, 0, Spark.Common.TraceLogConnectorService.UPSEndpointProcessingStatus.NONE, DateTime.Now);
            }
        }
        #endregion





        #endregion

        private int GetExactMonthsUntilRenewal(DateTime startDate, DateTime endDate)
        {
            int monthDifference = System.Data.Linq.SqlClient.SqlMethods.DateDiffMonth(startDate, endDate);

            if (monthDifference > 0)
            {
                // Check to see if the number of months in between these two dates should be one less
                DateTime dteMonthDifferenceAdded = startDate.AddMonths(monthDifference);

                if (endDate < dteMonthDifferenceAdded)
                {
                    monthDifference = (monthDifference - 1);
                }
            }

            return monthDifference;
        }

        /// <summary>
        /// Copied from Purchase
        /// </summary>
        /// <param name="endDate"></param>
        /// <returns></returns>
        private int GetDuration(DateTime endDate, DateTime startDate)
        {
            // Get remaining days left on this plan
            // Whenever there is any remaining time in a day, give that full day of remaining credit to the 
            // member.  
            // Round 2.1 to 3 days
            // Round 2.9 to 3 days
            TimeSpan span = endDate.Subtract(startDate);
            Int32 remainingDays = Convert.ToInt32(Math.Ceiling(span.TotalDays));

            System.Diagnostics.Trace.WriteLine("RemainingDays:" + remainingDays);

            return remainingDays;
        }

        private bool IsAllowedToPurchaseUpsalePlan(List<Spark.Common.AccessService.AccessPrivilege> activePrivileges, Spark.Common.AccessService.PrivilegeType checkPrivilegeType, DateTime renewalDate)
        {
            bool isAllowedToPurchaseUpsalePlan = false;

            Spark.Common.AccessService.AccessPrivilege checkPrivilege = activePrivileges.Find(delegate(Spark.Common.AccessService.AccessPrivilege privilege) { return privilege.UnifiedPrivilegeType == checkPrivilegeType; });
            if (checkPrivilege == null)
            {
                // Member can purchase this privilege since he does not have this privilege yet
                isAllowedToPurchaseUpsalePlan = true;
            }
            else
            {
                if (checkPrivilege.EndDatePST == DateTime.MinValue
                    || checkPrivilege.EndDatePST < DateTime.Now)
                {
                    // Member can purchase this privilege since he had this privilege but it expired  
                    isAllowedToPurchaseUpsalePlan = true;
                }
                else
                {
                    // The start date of the premium service should be either earlier than the renewal date 
                    // or the same as the renewal date. If the start date of the premium service is past the
                    // renewal date, then set the start date of the premium service to the renewal date. 
                    if (checkPrivilege.EndDatePST > renewalDate)
                    {
                        isAllowedToPurchaseUpsalePlan = false;
                    }
                    else
                    {
                        System.TimeSpan dateDifference = checkPrivilege.EndDatePST.Subtract(renewalDate);
                        if (Math.Abs(dateDifference.TotalMinutes) > 20)
                        {
                            // There is still additional time that member can purchase for this privilege
                            isAllowedToPurchaseUpsalePlan = true;
                        }
                        else
                        {
                            // The end date of the privilege is the same as the end date of the basic subscription  
                            // so do not allow the member to purchase this privilege  
                            isAllowedToPurchaseUpsalePlan = false;
                        }
                    }
                }
            }

            return isAllowedToPurchaseUpsalePlan;
        }

        private DateTime GetStartDateForUpsalePlan(MPVO.Plan plan, List<Spark.Common.AccessService.AccessPrivilege> activePrivileges)
        {
            Spark.Common.AccessService.PrivilegeType checkPrivilegeType = Spark.Common.AccessService.PrivilegeType.None;

            if ((plan.PremiumTypeMask & PremiumType.HighlightedProfile) == PremiumType.HighlightedProfile)
            {
                checkPrivilegeType = Spark.Common.AccessService.PrivilegeType.HighlightedProfile;
            }
            else if ((plan.PremiumTypeMask & PremiumType.SpotlightMember) == PremiumType.SpotlightMember)
            {
                checkPrivilegeType = Spark.Common.AccessService.PrivilegeType.SpotlightMember;
            }
            else if ((plan.PremiumTypeMask & PremiumType.JMeter) == PremiumType.JMeter)
            {
                checkPrivilegeType = Spark.Common.AccessService.PrivilegeType.JMeter;
            }
            else if ((plan.PremiumTypeMask & PremiumType.AllAccess) == PremiumType.AllAccess)
            {
                checkPrivilegeType = Spark.Common.AccessService.PrivilegeType.AllAccess;
            }
            else if ((plan.PremiumTypeMask & PremiumType.ReadReceipt) == PremiumType.ReadReceipt)
            {
                checkPrivilegeType = Spark.Common.AccessService.PrivilegeType.ReadReceipt;
            }
            else
            {
                checkPrivilegeType = Spark.Common.AccessService.PrivilegeType.None;
            }

            Spark.Common.AccessService.AccessPrivilege checkPrivilege = activePrivileges.Find(delegate(Spark.Common.AccessService.AccessPrivilege privilege) { return privilege.UnifiedPrivilegeType == checkPrivilegeType; });

            if (checkPrivilege == null)
            {
                return DateTime.Now.AddMinutes(60);
            }
            else
            {
                if (checkPrivilege.EndDatePST <= DateTime.Now)
                {
                    return DateTime.Now.AddMinutes(60);
                }
                else
                {
                    return checkPrivilege.EndDatePST;
                }
            }
        }

        /// <summary>
        /// Copied from Purchase
        /// </summary>
        /// <param name="initialCost"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        private decimal GetMemberProratedAmount(decimal initialCost, DateTime endDate, DateTime startDate)
        {
            if (endDate < DateTime.Now)
            {
                g.Notification.AddErrorString("Subscription end date must be a future date. Enddate:" +
                                              endDate.ToString());

                throw new Exception("Subscription end date must be a future date. Enddate:" +
                                    endDate.ToString());
            }

            decimal amount = Constants.NULL_DECIMAL;
            decimal pricePerDay = Constants.NULL_DECIMAL;

            // The start date of the premium service should be either earlier than the renewal date 
            // or the same as the renewal date. If the start date of the premium service is past the
            // renewal date, then set the start date of the premium service to the renewal date. 
            if (startDate > endDate)
            {
                amount = 0.0m;
            }
            else
            {
                int monthsUntilRenewalDate = GetExactMonthsUntilRenewal(startDate, endDate);
                decimal subTotalFromMonths = (monthsUntilRenewalDate) * (initialCost);
                DateTime dteAfterMonthsAdded = startDate.AddMonths(monthsUntilRenewalDate);

                // Calculate cost per day
                DateTime dteEndDateOfRemainingDaysWithMonthAdded = dteAfterMonthsAdded.AddMonths(1);
                TimeSpan span = dteEndDateOfRemainingDaysWithMonthAdded.Subtract(dteAfterMonthsAdded);
                pricePerDay = (initialCost) / Convert.ToDecimal(span.Days);

                decimal subTotalFromRemainingDays = (pricePerDay) * (Convert.ToDecimal(GetDuration(endDate, dteAfterMonthsAdded)));

                amount = subTotalFromMonths + subTotalFromRemainingDays;
            }

            if (amount < 0)
                throw new Exception("Prorated Amount cannot be less then 0.");

            System.Diagnostics.Trace.WriteLine("InitialCost:" + initialCost +
                "EndDate:" + endDate + "PricePerDay:" + pricePerDay + "Amount:" + amount);

            return amount;
        }

        private Matchnet.Purchase.ValueObjects.Plan UPSGetPrimaryPlan()
        {
            int legacyPlanIDFound = Constants.NULL_INT;
            int alaCartePlanIDFound = Constants.NULL_INT;
            int discountPlanIDFound = Constants.NULL_INT;

            if (orderInfo.OrderDetail.Length == 1)
            {
                return Matchnet.Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(orderInfo.OrderDetail[0].PackageID, _g.Brand.BrandID);
            }
            else
            {
                foreach (OrderDetailInfo odi in orderInfo.OrderDetail)
                {
                    Matchnet.Purchase.ValueObjects.Plan plan = Matchnet.Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(odi.PackageID, _g.Brand.BrandID);

                    if (plan != null)
                    {
                        if ((plan.PlanTypeMask & Matchnet.Purchase.ValueObjects.PlanType.ALaCarte) == Matchnet.Purchase.ValueObjects.PlanType.ALaCarte)
                        {
                            // An ala carte item exists in the order
                            if (alaCartePlanIDFound == Constants.NULL_INT)
                            {
                                // Only set this once so the first PlanID found will be used as the
                                // primary PlanID
                                alaCartePlanIDFound = odi.PackageID;
                            }
                        }
                        else if ((plan.PlanTypeMask & Matchnet.Purchase.ValueObjects.PlanType.Discount) == Matchnet.Purchase.ValueObjects.PlanType.Discount)
                        {
                            // A discount item exists in the order
                            if (discountPlanIDFound == Constants.NULL_INT)
                            {
                                // Only set this once so the first PlanID found will be used as the
                                // primary PlanID
                                discountPlanIDFound = odi.PackageID;
                            }
                        }
                        else
                        {
                            // The primary PlanID will be the item that is not an ala carte 
                            // or discount package  
                            if (legacyPlanIDFound == Constants.NULL_INT)
                            {
                                // Only set this once so the first PlanID found will be used as the
                                // primary PlanID
                                legacyPlanIDFound = odi.PackageID;
                                break;
                            }
                        }
                    }
                }

                if (legacyPlanIDFound != Constants.NULL_INT)
                {
                    // Return any PlanID as the primary PlanID as long as this is not an ala carte 
                    // package or a discount package
                    return Matchnet.Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(legacyPlanIDFound, _g.Brand.BrandID);
                }
                else if (alaCartePlanIDFound != Constants.NULL_INT)
                {
                    // If the member only purchased an ala carte package and no base subscription packages,
                    // than return the first ala carte package found
                    return Matchnet.Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(alaCartePlanIDFound, _g.Brand.BrandID);
                }
                else
                {
                    // If the order did not have any base subscriptions or ala carte packages, than there
                    // no primary PlanID was found
                    // Do not return the discount PlanID since a discount cannot exists without a base 
                    // subscription or ala carte purchase  
                    return null;
                }
            }
        }
    }
}
