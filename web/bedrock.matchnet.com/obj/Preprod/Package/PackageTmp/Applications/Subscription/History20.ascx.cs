﻿using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using Matchnet.Lib;
using Matchnet.Web.Framework;
using Matchnet.Web.Applications.Subscription.Controls;
using Matchnet.Content.ValueObjects.PageConfig;

using Matchnet.Configuration.ServiceAdapters;

namespace Matchnet.Web.Applications.Subscription
{
    public partial class History20 : FrameworkControl
    {
        private void Page_Load(object sender, System.EventArgs e)
        {
            try
            {
                string breadcrumb = g.GetResource("NAV_HISTORY", this);
                g.BreadCrumbTrailHeader.SetTwoLinkCrumb(breadcrumb, string.Empty);
                g.BreadCrumbTrailFooter.SetTwoLinkCrumb(breadcrumb, string.Empty);

                //if (g.LayoutTemplate.Equals(LayoutTemplate.Popup))
                //{
                //    AlignOverrideDiv.Attributes.Add("class", "rightNewAlignOverride");
                //}

                PaymentHistory.Populate(g.TargetMemberID);
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            InitializeComponent();
            base.OnInit(e);
        }

        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }

        protected override void OnPreRender(EventArgs e)
        {
            try
            {
                // Omniture Analytics - Event Tracking
                if (Convert.ToBoolean(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ANALYTICS_OMNITURE", _g.Brand.Site.Community.CommunityID, _g.Brand.Site.SiteID)))
                {
                    _g.AnalyticsOmniture.Evar22 = (_g.TargetMember.IsPayingMember(_g.TargetBrand.Site.SiteID)) ? "Subscriber" : "Registered";
                    _g.AnalyticsOmniture.Prop22 = (_g.TargetMember.IsPayingMember(_g.TargetBrand.Site.SiteID)) ? "Subscriber" : "Registered";
                }

                base.OnPreRender(e);
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        #endregion
    }
}