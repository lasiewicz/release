﻿#region System References
using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
#endregion

#region Matchnet Web App References
using Matchnet.Web.Framework;
#endregion

namespace Matchnet.Web.Applications.ContactUs
{
    public partial class ContactUsConfirmation20 : FrameworkControl
    {
        private bool _videoComment = false;

        private void Page_Load(object sender, System.EventArgs e)
        {
            if (Request.QueryString["VC"] != null)
            {
                _videoComment = true;
                SetVideoCommentConditionalElements();
            }
        }

        private void SetVideoCommentConditionalElements()
        {
            txtConfirmation.ResourceConstant = "CONTACTUSCONFIRMATION_VIDEO_COMMENT";
            ArticleContent1.Visible = false;
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);

        }
        #endregion
    }
}