﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="ContactUs20.ascx.cs" Inherits="Matchnet.Web.Applications.ContactUs.ContactUs20" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="cc1" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<div id="page-container">
<script type="text/javascript" src="/javascript20/swfobject.js"></script>
<script type="text/javascript">
    var hasFlash = swfobject.hasFlashPlayerVersion("1.0.0");
    var version = "N/A";

    if (hasFlash) {
        var playerVersion = swfobject.getFlashPlayerVersion();
        version = playerVersion.major + "." + playerVersion.minor + "." + playerVersion.release;
    }

    $j.ajax({
        type: "POST",
        url: "/Applications/API/FlashVersionUpdate.asmx/SetFlashVersion",
        data: "{'version': '" + version + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json"
    });



    $j(document).ready(function () {

        if ($j(".ddlistContactUS").val() == '20')
            toggleUserNameReport(true);
        else
            toggleUserNameReport(false);

        $j(".ddlistContactUS").change(function () {
            if ($j(this).val() == "20")
                toggleUserNameReport(true);
            else
                toggleUserNameReport(false);
        });

        function toggleUserNameReport(showhide) {

            if (showhide) {
                $j(".lblReportUserNameContactUS").show();
                $j(".txtReportUsernameContactUs").show();
            }
            else {
                $j(".lblReportUserNameContactUS").hide();
                $j(".txtReportUsernameContactUs").hide();
            }
        }

    });





</script>
	<mn:Title runat="server" id="ttlContact" ResourceConstant="NAV_CONTACT_US"/>
	
	<asp:Panel cssclass="border-gen clearfix editorial cs-txt" runat="server" id="pnlContact">
		<mn:Image id="imgPhone" runat="server" CssClass="float-inside phone" filename="utl_contactUs_phone.gif" />
		<mn:txt id="txtBlockedIP" runat="server" resourceconstant="TXT_BLOCKED_IP" Visible="false" />
		<mn:txt id="txtCSNumber" runat="server" resourceconstant="TXT_CS" />
    </asp:Panel>
    <div class="clearfix margin-heavy">
        <dl class="dl-form">
	        <dt><label><mn:txt id="txtYourName" runat="server" resourceconstant="TXT_YOUR_NAME" /></label></dt>
	        <dd><asp:textbox id="txtUserName" runat="server" CssClass="form-element-narrow" />
	        <mn:MultiValidator id="nameValidator" tabIndex="-1" runat="server" MinimumLength="1" MaximumLength="50"
		        RequiredType="AlphaNumType" ControlToValidate="txtUserName" FieldNameResourceConstant="USERNAME_REQUIRED" IsRequired="True" Display="Dynamic" /></dd>
	        <dt><label><mn:txt id="txtYourEmailAddress" runat="server" resourceconstant="TXT_YOUR_EMAIL_ADDRESS" /></label></dt>
	        <dd><asp:textbox id="txtEmailAddress" runat="server" CssClass="form-element-narrow" dir="ltr" />
	            <asp:RequiredFieldValidator cssclass="error" ID="rfvEmailAddressValidator" EnableClientScript="False" Runat="server" ControlToValidate="txtEmailAddress" Display="Dynamic" />
	            <asp:RegularExpressionValidator id="revEmailAddressValidator" EnableClientScript="False" runat="server" ControlToValidate="txtEmailAddress" Display="Dynamic" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
            </dd>
            <dt><label><mn:txt id="txtPleaseSelectFollowing" runat="server" resourceconstant="PLEASE_SELECT_FROM_ONE_OF_THE_FOLLOWING" /></label></dt>
            <dd><asp:dropdownlist id="ddlList" runat="server" CssClass="form-element-narrow ddlistContactUS" />
                <mn:MultiValidator id="reasonValidator" tabIndex="-1" runat="server" MinimumLength="1" MaximumLength="50"
	            RequiredType="AlphaNumType" ControlToValidate="ddlList" FieldNameResourceConstant="REASON_REQUIRED" IsRequired="True" Display="Dynamic" />
            </dd>
            
            <dt>
                <label><mn:txt id="LabelReportUsername" runat="server" resourceconstant="TXT_REPORT_USERNAME" CssClass="lblReportUserNameContactUS" /></label>
            </dt>
            <dd>
                <asp:TextBox ID="txtReportUsername" runat="server" CssClass="form-element-narrow txtReportUsernameContactUs" dir="ltr"></asp:TextBox>
                <br /><br />
            </dd>
            
            <dt id="dtIGiveVideoPermission" runat="server" visible="false">&nbsp;</dt>
            <dd id="ddIGiveVideoPermission" runat="server" visible="false" class="dl-form-spacer"><asp:CheckBox ID="chkIGiveVideoPermission" runat="server" />&nbsp;<mn:Txt ID="txtIGiveVideoPermission" ResourceConstant="TXT_I_GIVE_VIDEO_PERMISSION" runat="server" /></dd>
            <dt><label><mn:txt id="txtYourCommentsQuestions" runat="server" resourceconstant="TXT_YOUR_COMMENTSQUESTIONS" /></label></dt>
		    <dd><asp:textbox id="txtComment" TextMode="MultiLine" CssClass="form-element-wide" Runat="server" Rows="7" />
		        <mn:MultiValidator id="commentValidator" tabIndex="-1" runat="server" MinimumLength="1" MaximumLength="3000"
			    RequiredType="StringType" ControlToValidate="txtComment" FieldNameResourceConstant="TXT_COMMENT" IsRequired="True"
			    Display="Dynamic" RequiredTypeResourceConstant="TXT_ALPHANUMERIC_VALIDATOR" />
			</dd>
        </dl>
    </div>	
	<div class="text-center">
		<cc1:frameworkbutton id="Go" runat="server" ResourceConstant="BTN_SEND" CssClass="btn primary" />
	</div>
</div>

<script type="text/javascript">
function ValidatorUpdateDisplay(val) {
    if (typeof(val.display) == "string") {
    if (val.display == "None") {
    return;
    }
    if (val.display == "Dynamic") {
    val.style.display = val.isvalid ? "none" : "block";
    return;
    }
    }
    if ((navigator.userAgent.indexOf("Mac") > -1) &&
    (navigator.userAgent.indexOf("MSIE") > -1)) {
    val.style.display = "inline";
    }
    val.style.visibility = val.isvalid ? "hidden" : "visible";
}
</script>