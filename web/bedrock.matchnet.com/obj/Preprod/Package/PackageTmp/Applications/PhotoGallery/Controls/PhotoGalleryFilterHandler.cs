﻿#region System References
using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
#endregion

#region Matchnet Web App References
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Util;
using Matchnet.Lib;
#endregion

#region Matchnet Service References
using Matchnet.Session.ValueObjects;
#endregion

namespace Matchnet.Web.Applications.PhotoGallery.Controls
{
    public class PhotoGalleryFilterHandler
    {

        IPhotoGalleryFilter _control = null;
        ContextGlobal g;


        public PhotoGalleryFilterHandler(ContextGlobal context, IPhotoGalleryFilter control)
        {
            _control = control;
            g = context;
        }


        public void LoadPage(bool isPostBack)
        {
            _control.SiteID = g.Brand.Site.SiteID;
            _control.BrandID = g.Brand.BrandID;
            _control.CommunityiID = g.Brand.Site.Community.CommunityID;

            int genderMask = 0;
            if (g.Member != null)
            {
                _control.MemberRegionID = g.Member.GetAttributeInt(_control.CommunityiID, _control.SiteID, _control.BrandID, WebConstants.ATTRIBUTE_NAME_REGIONID, int.MinValue);
                _control.MemberCountryRegionID = Matchnet.Content.ServiceAdapters.RegionSA.Instance.RetrievePopulatedHierarchy(_control.MemberRegionID, g.Brand.Site.LanguageID).CountryRegionID;

            }
            else
            {
                _control.MemberCountryRegionID = g.Brand.Site.DefaultRegionID;
            }
            if (isPostBack)
            { _control.Query = SetQuery(); }
            else
            {
                _control.Query = PhotoGalleryUtils.GetQueryFromSession(g.Brand, g.Member != null ? g.Member.MemberID : int.MinValue);
                
            }

            if (_control.Query == null || !PhotoSearch.ServiceAdapters.PhotoGallerySA.Instance.IsValidQuery(_control.Query))
            {
                _control.Query = PhotoSearch.ServiceAdapters.PhotoGallerySA.Instance.GetDefaultQuery(_control.BrandID);
                //					if(g.Member != null)
                //					{
                //						genderMask=g.Member.GetAttributeInt(_communityiID,_siteID,_brandID,WebConstants.ATTRIBUTE_NAME_GENDERMASK, 0);
                //						_query.Gender=GetSeekGender(genderMask);
                //						
                //					}
            }

            _control.SelGenderMask.DataSource = GetGenderOptions();
            _control.SelGenderMask.DataTextField = "Content";
            _control.SelGenderMask.DataValueField = "Value";
            _control.SelGenderMask.DataBind();

            _control.SelPostedDays.DataSource = GetPostedDaysOptions(GetPeriodOptions());
            _control.SelPostedDays.DataTextField = "Content";
            _control.SelPostedDays.DataValueField = "Value";
            _control.SelPostedDays.DataBind();

            _control.SelPostedDays.Items.FindByValue(_control.Query.InsertDays.ToString()).Selected = true;

            if (_control.Query.Gender != 0)
                _control.SelGenderMask.Items.FindByValue(_control.Query.Gender.ToString()).Selected = true;
            else
                _control.SelGenderMask.SelectedIndex = 0;

            _control.SelRegionID.DataSource = GetRegionOptions();
            _control.SelRegionID.DataTextField = "Content";
            _control.SelRegionID.DataValueField = "Value";
            _control.SelRegionID.DataBind();

            if (_control.Query.RegionType == PhotoSearch.ValueObjects.SearchRegionType.country)
                _control.QueryRegionID = _control.Query.CountryRegionID;
            else if (_control.Query.RegionType == PhotoSearch.ValueObjects.SearchRegionType.myregion)
                _control.QueryRegionID = _control.Query.RegionID;
            else
                _control.QueryRegionID = _control.Query.RegionID;

            if (_control.SelRegionID.Items.FindByValue(_control.QueryRegionID.ToString()) != null)
                _control.SelRegionID.Items.FindByValue(_control.QueryRegionID.ToString()).Selected = true;
            else
            {
                _control.SelRegionID.Items.FindByValue(Constants.NULL_INT.ToString()).Selected = true;

            }
            // min age
            string newValue;
            if (HttpContext.Current.Request["AgeMin"] != null)
            { _control.TxtAgeMin.Value = HttpContext.Current.Request["AgeMin"]; }
            else
            { _control.TxtAgeMin.Value = _control.Query.AgeMin.ToString(); }

            if (HttpContext.Current.Request["AgeMax"] != null)
            { _control.TxtAgeMax.Value = HttpContext.Current.Request["AgeMax"]; }
            else
            { _control.TxtAgeMax.Value = _control.Query.AgeMax.ToString(); }
            _control.Query.GuestFlag = g.Member == null;
            PhotoGalleryUtils.SaveQueryToSession(_control.Query, g.Brand, g.Member != null ? g.Member.MemberID : int.MinValue);
            //g.Session.Add(SESSION_PHOTO_GALLERY_QUERY,_query,SessionPropertyLifetime.Temporary);
            this.DoValidation();
        }


        public PhotoSearch.ValueObjects.PhotoGalleryQuery SetQuery()
        {
            try
            {
                
                PhotoSearch.ValueObjects.PhotoGalleryQuery query = new PhotoSearch.ValueObjects.PhotoGalleryQuery();

                query.Gender = Conversion.CInt(HttpContext.Current.Request[_control.SelGenderMask.UniqueID]);
                int ageMin = Conversion.CInt(HttpContext.Current.Request[_control.TxtAgeMin.UniqueID]);
                int ageMax = Conversion.CInt(HttpContext.Current.Request[_control.TxtAgeMax.UniqueID]);

                query.AgeMin = Math.Min(ageMin, ageMax);
                query.AgeMax = Math.Max(ageMin, ageMax);
                query.InsertDays = Conversion.CInt(HttpContext.Current.Request[_control.SelPostedDays.UniqueID]);
                query.CommunityID = _control.CommunityiID;
                query.SiteID = _control.SiteID;
                int regionid = Conversion.CInt(HttpContext.Current.Request[_control.SelRegionID.UniqueID]);
                if (regionid == Constants.NULL_INT)
                {
                    query.RegionType = PhotoSearch.ValueObjects.SearchRegionType.anywhere;

                }
                else if (regionid == _control.MemberRegionID)
                {
                    query.RegionType = PhotoSearch.ValueObjects.SearchRegionType.myregion;
                    query.Radius = g.Brand.DefaultSearchRadius;
                    query.CountryRegionID = _control.MemberCountryRegionID;
                }
                else
                {
                    query.RegionType = PhotoSearch.ValueObjects.SearchRegionType.country;
                    query.CountryRegionID = regionid;

                }

                query.RegionID = regionid;

                return query;
                
            }
            catch (Exception ex)
            {
                return null;
                g.ProcessException(ex);
            }
        }
        public void DoValidation()
        {
            // Add min and max age validation
            string minAgeError = g.GetResource("MIN_AGE_NOTICE", _control.ResourceControl);
            string maxAgeError = g.GetResource("MAX_AGE_NOTICE", _control.ResourceControl);
            string greaterThanError = g.GetResource("GREATER_THAN_NOTICE", _control.ResourceControl);

            _control.ValAgeMinRange.ErrorMessage = minAgeError;
            _control.ValAgeMinRange.MinimumValue = ConstantsTemp.MIN_AGE_RANGE.ToString();
            _control.ValAgeMinRange.MaximumValue = ConstantsTemp.MAX_AGE_RANGE.ToString();
            _control.ValAgeMaxRange.ErrorMessage = maxAgeError;
            _control.ValAgeMaxRange.MinimumValue = ConstantsTemp.MIN_AGE_RANGE.ToString();
            _control.ValAgeMaxRange.MaximumValue = ConstantsTemp.MAX_AGE_RANGE.ToString();
            _control.ValAgeMinReq.ErrorMessage = minAgeError;
            _control.ValAgeMaxReq.ErrorMessage = maxAgeError;
            //valCompare.ErrorMessage = greaterThanError;

            _control.ValAgeMinRange.Type = System.Web.UI.WebControls.ValidationDataType.Integer;
            _control.ValAgeMaxRange.Type = System.Web.UI.WebControls.ValidationDataType.Integer;

        }

        public int GetGenderMask(int gender)
        {

            int seekGender = 0;
            if (gender == ConstantsTemp.GENDERID_MALE)
            { seekGender = ConstantsTemp.GENDERID_FEMALE; }
            else
            { seekGender = ConstantsTemp.GENDERID_MALE; }
            return seekGender;
        }

        public int GetSeekGender(int gendermask)
        {

            int gender = 0;
            if (gendermask == ConstantsTemp.GENDERID_SEEKING_FEMALE)
            { gender = ConstantsTemp.GENDERID_FEMALE; }
            else
            { gender = ConstantsTemp.GENDERID_MALE; }
            return gender;
        }

        public DataTable GetGenderOptions()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("Content", typeof(System.String));
            dt.Columns.Add("Value", typeof(System.Int16));


            dt.Rows.Add(new object[] { g.GetResource("EVERYONE", _control.ResourceControl), 0 });
            dt.Rows.Add(new object[] { g.GetResource("WOMEN", _control.ResourceControl), ConstantsTemp.GENDERID_FEMALE });
            dt.Rows.Add(new object[] { g.GetResource("MEN", _control.ResourceControl), ConstantsTemp.GENDERID_MALE });


            return dt;
        }

        public DataTable GetPostedDaysOptions(PhotoSearch.ValueObjects.QueryOptionCollection coll)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("Content", typeof(System.String));
            dt.Columns.Add("Value", typeof(System.Int16));

            foreach (PhotoSearch.ValueObjects.QueryOption opt in coll)
            {
                string content = opt.OptionValue.ToString() + " " + g.GetResource("TXT_DAYS", _control.ResourceControl);
                dt.Rows.Add(new object[] { content, opt.OptionValue });

            }



            return dt;
        }

        public DataTable GetRegionOptions()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("Content", typeof(System.String));
            dt.Columns.Add("Value", typeof(System.Int32));

            PhotoSearch.ValueObjects.QueryOptionCollection coll = PhotoSearch.ServiceAdapters.PhotoGallerySA.Instance.GetOptionCollection(g.Brand.Site.SiteID, PhotoSearch.ValueObjects.QueryOptionType.region);

            dt.Rows.Add(new object[] { g.GetResource("ANYWHERE", _control.ResourceControl), Constants.NULL_INT });
            if (g.Member != null)
            {
                dt.Rows.Add(new object[] { FrameworkGlobals.GetUnicodeText(g.GetResource("YOUR_REGION", _control.ResourceControl)), g.Member.GetAttributeInt(g.Brand, WebConstants.ATTRIBUTE_NAME_REGIONID) });
            }
            if (coll == null)
                return dt;

            for (int i = 0; i < coll.Count; i++)
            {
                int regionid = coll[i].OptionValue;
                Content.ValueObjects.Region.Region region = Content.ServiceAdapters.RegionSA.Instance.RetrieveRegionByID(regionid, g.Brand.Site.LanguageID);
                dt.Rows.Add(new object[] { region.Description, regionid });

            }


            return dt;
        }

        public PhotoSearch.ValueObjects.QueryOptionCollection GetPeriodOptions()
        {

            PhotoSearch.ValueObjects.QueryOptionCollection coll = PhotoSearch.ServiceAdapters.PhotoGallerySA.Instance.GetOptionCollection(_control.SiteID, PhotoSearch.ValueObjects.QueryOptionType.insertdays);

            return coll;
        }
    }
}
