﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PhotoGallery30.ascx.cs"
    Inherits="Matchnet.Web.Applications.PhotoGallery.PhotoGallery30" %>
<%@ Import Namespace="Matchnet.Web.Framework.Managers" %>
<%@ Register TagPrefix="mp" TagName="PhotoGalleryFilter" Src="Controls/PhotoGalleryFilter30.ascx" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<div class="photo-gallery">
    <div class="header-option">
        <h1>
            <mn:Txt runat="server" ResourceConstant="TXT_PAGE_NAME" />
        </h1>
    </div>
    <mp:PhotoGalleryFilter ID="photogalleryfilter" runat="server"></mp:PhotoGalleryFilter>
    <div id="galleryContainer" class="photos clearfix">
    </div>
    <%--/#galleryContainer--%>
    <!--paging-->
    <div class="next-selector">
        <a href="#" onclick="galleryManager.onNextPageLoad(false); return false;" id="nextSelector">
            <mn:Txt ID="Txt5" runat="server" ResourceConstant="TXT_NEXT_PAGE" />
        </a>
    </div>
    <div class="back-to-top">
        <a href="#" title="Back to top" id="backtoTop">
            <mn:Txt runat="server" ResourceConstant="TXT_BACK_TO_TOP" />
        </a>
    </div>
    <div id="tempHolder" class="hide">
    </div>
    <div id="infscrLoader" class="hide">
        <div class="loading spinner-only">
            <blockquote>
                <mn:Txt runat="server" ResourceConstant="TXT_LOADING" />
            </blockquote>
        </div>
    </div>
    <!--modal-->
    <div class="hide">
        <div id="galleryModalContainer" class="modal-profile clearfix" style="display: none;">
            <div id="galleryModalAdHolder">
                <%--placeholder for the leader board ad--%>
            </div>
            <div class="profile-photo">
                <a id="galleryModalLnkImage" href="#" title="" target="_blank" rel="modal">
                    <img id="galleryModalImage" alt="" src="" />
                </a>
            </div>
            <div id="galleryModalCaptionContainer" class="profile-caption hide">
                <span id="galleryModalCaption">
                    <%-- caption goes here --%></span></div>
            <div class="profile-info">
                <div class="profile30-member-pulse">
                    <h2>
                        <a id="galleryModalLnkUsername" href="#" title="" target="_blank"><span id="galleryModalUsername">
                            <%-- username goes here --%></span></a>
                        <!--IM offline status-->
                        <span id="galleryModalOffLine" class="spr s-icon-status-offline-sm"><span>
                            <mn:Txt ID="Txt3" runat="server" ResourceConstant="TXT_OFFLINE" />
                        </span></span>
                        <!--IM online status-->
                        <a id="galleryModalLnkIMMe" href="#"><span id="galleryModalOnLine" class="spr s-icon-status-online-sm">
                            <span>
                                <mn:Txt ID="Txt4" runat="server" ResourceConstant="TXT_ONLINE" />
                            </span></span></a>
                        <!--new or updated status-->
                        <span id="galleryModalNewMember" class="spr s-icon-new-member"><span></span></span>
                        <span id="galleryModalUpdatedMember" class="spr s-icon-updated"><span></span></span>
                    </h2>
                </div>
                <ul class="interest">
                    <li><span id="galleryModalMaritalSeeking"></span>,</li>
                    <li><span id="galleryModalAgeLocation"></span></li>
                </ul>
                <a id="galleryModalLnkMorePhotos" href="#" target="_blank" class="more-photos">
                    <mn:Txt ID="Txt10" runat="server" ResourceConstant="TXT_MORE_PHOTOS" />
                </a>
            </div>
            <div class="profile-comm">
                <ul>
                    <li class="action-email"><a href="#" target="_blank" id="galleryModalEmailMe" class="link-primary">
                        <span>
                            <mn:Txt ID="Txt11" runat="server" ResourceConstant="TXT_EMAIL" />
                        </span></a></li>
                    <li class="profile"><a href="#" target="_blank" id="galleryModalViewProfile" class="link-tertiary" onclick="galleryManager.onViewProfileClick(this.href);return false">
                        <span>
                            <mn:Txt ID="Txt12" runat="server" ResourceConstant="TXT_VIEW_PROFILE" />
                        </span></a></li>
                    <li class="flirt"><a href="#" id="galleryModalFlirt" class="link-tertiary" onclick="galleryManager.onFlirtClick(); return false;">
                        <span class="spr s-icon-flirt-sent"></span><span>
                            <mn:Txt runat="server" ResourceConstant="TXT_FLIRT" />
                        </span></a></li>
                    <li class="favorite"><a href="#" id="galleryModalFavorites" class="link-tertiary"
                        onclick="galleryManager.onFavoritesClick(); return false;"><span class="spr s-icon-favorites-added">
                        </span><span>
                            <mn:Txt runat="server" ResourceConstant="TXT_FAVORITE" />
                        </span></a></li>
                </ul>
                <div class="notice">
                    <span id="galleryModalFavoritesAdded" class="hide message"><span class="spr s-icon-page-message">
                    </span>
                        <mn:Txt ID="Txt1" runat="server" ResourceConstant="TXT_FAVORITE_NOTICE_ADDED" />
                    </span><span id="galleryModalFavoritesRemoved" class="hide message"><span class="spr s-icon-page-message">
                    </span>
                        <mn:Txt ID="Txt2" runat="server" ResourceConstant="TXT_FAVORITE_NOTICE_REMOVED" />
                    </span><span id="galleryModalFavoritesError" class="hide message long"><span class="spr s-icon-page-message">
                    </span>
                        <mn:Txt ID="Txt6" runat="server" ResourceConstant="TXT_FAVORITE_NOTICE_ERROR" />
                    </span><span id="galleryModalFlirtSucceed" class="hide message"><span class="spr s-icon-page-message">
                    </span>
                        <mn:Txt ID="Txt7" runat="server" ResourceConstant="TXT_FLIRT_NOTICE_SUCCEED" />
                    </span><span id="galleryModalFlirtUnableFlirted" class="hide message"><span class="spr s-icon-page-message">
                    </span>
                        <mn:Txt ID="Txt8" runat="server" ResourceConstant="TXT_FLIRT_NOTICE_UNABLE_FLIRTED" />
                    </span><span id="galleryModalFlirtUnableLimit" class="hide message"><span class="spr s-icon-page-message">
                    </span>
                        <mn:Txt ID="Txt9" runat="server" ResourceConstant="TXT_FLIRT_NOTICE_UNABLE_LIMIT" />
                    </span><span id="galleryModalFlirtUnableNow" class="hide message"><span class="spr s-icon-page-message">
                    </span>
                        <mn:Txt ID="Txt13" runat="server" ResourceConstant="TXT_FLIRT_NOTICE_UNABLE_NOW" />
                    </span>
                </div>
            </div>
            <div id="galleryModalPrevious" class="nav previous">
                <a href="#" id="galleryModalPreviousLink" tabindex="0" onclick="galleryManager.onPreviousPhotoClick(); return false;">
                    Prev</a></div>
            <div id="galleryModalNext" class="nav next">
                <a href="#" id="galleryModalNextLink" tabindex="1" onclick="galleryManager.onNextPhotoClick(); return false;">
                    Next</a></div>
        </div>
        <%--/#galleryModalContainer--%>
    </div>
    <input type="hidden" id="hdnTeaseCategoryRC" value='<mn:txt  runat="server" resourceconstant="TXT_TEASE_CATEGORY" />' />
    <input type="hidden" id="hdnTeaseRC" value='<mn:txt  runat="server" resourceconstant="TXT_TEASE" />' />
    <script type="text/javascript">

        function spark_gallery_photo() {
            this.photoIndex = 0;
            this.photoCaption = '';
            this.photoCount = 0;
            this.largeImageURL = '';
            this.memberID = 0;
            this.username = '';
            this.profileURL = '';
            this.IMURL = '';
            this.EmailURL = '';
            this.ageDisplayText = '';
            this.postedDateDisplayText = '';
            this.online = false;
            this.newStatus = false;
            this.updatedStatus = false;
            this.ageLocationDisplayText = '';
            this.maritalSeekingDisplayText = '';
            this.morePhotoDisplayText = '';
            this.favorited = false;
            this.highlighted = false;
            this.flirted = false;
            this.location = '';
            this.locationShort = '';
        }

        function spark_gallery_manager() {
            this.photoList = [];
            this.instanceRef = null;
            this.updateInProgress = false;
            this.isVerbose = true;
            this.isBlocked = false;
            this.memberID = 0;
            this.startRow = 0;
            this.pageSize = 0;
            this.totalResultCount = 0;
            this.photoContainer = null;
            this.modalContainer = null;
            this.modalAd = null;
            this.currentModalPhotoIndex = 0;
            this.favoriteTitle = '';
            this.unFavoriteTitle = '';
            this.flirtActiveTitle = '';
            this.flirtInactiveTitle = '';
            this.sortBy = 1;
            this.brandId = null;
            this.clearPhotos = function () {
                this.photoList.length = 0;
                this.totalResultCount = 0;
                this.photoContainer.empty();
            }
            this.addPhoto = function (photoObj) {
                spark.util.addItem(this.photoList, photoObj);
            }
            this.toggleMemberFavorited = function (memberID, favorited) {
                for(var i=0; i< this.photoList.length; i++) {
                    if (this.photoList[i].memberID == memberID) {
                        this.photoList[i].favorited = favorited;
                    }
                }
            }
            this.createPhoto = function (photoGalleryMember){
                //photoGalleryMember is server side VO returned as JSON
                var photo1 = new spark_gallery_photo();
                photo1.photoIndex = this.photoList.length;
                photo1.largeImageURL = photoGalleryMember.LargeWebPath;
                photo1.photoCaption = photoGalleryMember.Caption;
                photo1.memberID = photoGalleryMember.MemberID;
                photo1.username = photoGalleryMember.UserName;
                photo1.profileURL = photoGalleryMember.ProfileURL;
                photo1.ageDisplayText = photoGalleryMember.Age;
                photo1.postedDateDisplayText = photoGalleryMember.PostedDateDisplayText;
                photo1.ageLocationDisplayText = photoGalleryMember.AgeLocationDisplayText;
                photo1.maritalSeekingDisplayText = photoGalleryMember.MaritalSeekingDisplayText;
                photo1.online = photoGalleryMember.IsOnline;
                photo1.newStatus = photoGalleryMember.IsNew;
                photo1.updatedStatus = photoGalleryMember.IsUpdated;
                photo1.IMURL = photoGalleryMember.IMURL;
                photo1.EmailURL = photoGalleryMember.EmailURL;
                photo1.photoCount = photoGalleryMember.PhotoCount;
                photo1.morePhotoDisplayText = photoGalleryMember.PhotoMoreDisplayText;
                photo1.favorited = photoGalleryMember.IsFavorited;
                photo1.highlighted = photoGalleryMember.IsHighlighted;
                photo1.location = photoGalleryMember.Location;
                photo1.locationShort = photoGalleryMember.LocationShort;
                return photo1;
            }
            this.getCurrentPageNumber = function(){
                var currentPage = 1;
                if (this.photoList.length > this.pageSize){
                    currentPage = Math.ceil(this.photoList.length / this.pageSize);
                }
                return currentPage;
            }
            this.onInitialLoad = function(){
                //this.startRow = 1;
                this.loadPhotoBatch(false);
            }
            this.onNextPageLoad = function(isTriggeredFromModalNext){
                //this.startRow = this.startRow + this.pageSize;
                this.loadPhotoBatch(isTriggeredFromModalNext);
            }
            this.onPreviousPhotoClick = function(){
                var selectedIndex = this.currentModalPhotoIndex - 1;
                this.onGalleryPhotoClick(selectedIndex);
            }
            this.onNextPhotoClick = function(){
                var selectedIndex = this.currentModalPhotoIndex + 1;
                if (selectedIndex < this.photoList.length){
                    this.onGalleryPhotoClick(selectedIndex);
                }
                else{
                    //load next batch
                    this.onNextPageLoad(true);
                }
            }
            this.onFavoritesClick = function(){
                var targetmemberID = $j('#galleryModalFavorites').data("targetmemberid");
                this.toggleFavorites(this.memberID, targetmemberID);
            }
            this.onFlirtClick = function () {
                var categoryKey = $j('#hdnTeaseCategoryRC').val();
                var teaseKey = $j('#hdnTeaseRC').val();
                var targetmemberID = $j('#galleryModalFavorites').data("targetmemberid");
                this.sendFlirt(this.memberID, targetmemberID, categoryKey, teaseKey);
            }
            this.onViewProfileClick = function (linkHref) {
                this.submitOmnitureProfileView();
                setTimeout(function() {
                    var redirectWindow = window.open(linkHref, '_blank');
                    redirectWindow.location;
                }, 50);
            }
            this.onGalleryPhotoClick = function (selectedIndex) {
                //load the modal
                if (selectedIndex >= 0 && selectedIndex < this.photoList.length){
                    this.currentModalPhotoIndex = selectedIndex;

                    var photo = this.photoList[selectedIndex],
                        currentPageIndex = this.getCurrentPageNumber(),
                        $modalHtml = this.modalContainer,
                        $modalAd = this.modalAd,
                        $modalLoadedContent = $j('#cboxLoadedContent'),
                        $galleryModalLnkUsername = $j('#galleryModalLnkUsername'),
                        $galleryModalLnkImage = $j('#galleryModalLnkImage'),
                        $galleryModalImage = $j('#galleryModalImage'),
                        $galleryModalCaption = $j('#galleryModalCaption'),
                        $galleryModalCaptionContainer = $j('#galleryModalCaptionContainer'),
                        $galleryModalUsername = $j('#galleryModalUsername'),
                        $galleryModalAgeLocation = $j('#galleryModalAgeLocation'),
//                        $galleryModalAgeLocation = $galleryModalAgeLocation.replace(" years old ",", "),
                        $galleryModalMaritalSeeking = $j('#galleryModalMaritalSeeking'),
                        $galleryModalLnkMorePhotos = $j('#galleryModalLnkMorePhotos'),
                        $galleryModalFavorites = $j('#galleryModalFavorites'),
                        $galleryModalEmailMe = $j('#galleryModalEmailMe'),
                        $galleryModalViewProfile = $j('#galleryModalViewProfile'),
                        $galleryModalOffLine = $j('#galleryModalOffLine'),
                        $galleryModalLnkIMMe = $j('#galleryModalLnkIMMe'),
                        $galleryModalUpdatedMember = $j('#galleryModalUpdatedMember'),
                        $galleryModalNewMember = $j('#galleryModalNewMember'),
                        $galleryModalFlirt = $j('#galleryModalFlirt'),
                        $galleryModalPrevious = $j('#galleryModalPrevious'),
                        $galleryModalNext = $j('#galleryModalNext');
                        $galleryModalAdHolder = $j('#galleryModalAdHolder');
                        $galleryModalAd = $j('#galleryModalAd');
                    
                    try{
                        console.log('Showing Photo Index: ' + selectedIndex);
                    } catch(e){}

                    $modalAd.css({display:'none'});
                    var $galleryModalAdDisplay = '<%=galleryModalAdDisplay%>';

                    $galleryModalLnkUsername.attr({href: photo.profileURL, title: photo.username});
                    $galleryModalLnkImage.attr({href: photo.profileURL, title: photo.username});
                    $galleryModalImage.attr({src: photo.largeImageURL, alt: photo.username});
                    if(photo.photoCaption){
                        $galleryModalCaption.text(photo.photoCaption);
                        $galleryModalCaptionContainer.show();
                    }else{
                        $galleryModalCaption.text('');
                        $galleryModalCaptionContainer.hide();
                    }
                    $galleryModalUsername.text(photo.username);
                    var ageLocationDisplayTextString = ' ' + photo.ageLocationDisplayText.replace(' years old ',', '); //
                    $galleryModalAgeLocation.text(ageLocationDisplayTextString);
                    $galleryModalMaritalSeeking.text(photo.maritalSeekingDisplayText);
                    $galleryModalLnkMorePhotos.text(photo.morePhotoDisplayText);
                    $galleryModalLnkMorePhotos.attr("href", photo.profileURL);
                    $galleryModalFavorites.data("targetmemberid", photo.memberID);
                    $galleryModalEmailMe.attr("href", photo.EmailURL);
                    $galleryModalViewProfile.attr("href", photo.profileURL);

                    if (photo.online){
                        $galleryModalOffLine.hide();
                        $galleryModalLnkIMMe.show().attr("href", photo.IMURL);
                    }else{
                        $galleryModalLnkIMMe.hide();
                        $galleryModalOffLine.show();
                    }

                    if (photo.newStatus){
                        $galleryModalUpdatedMember.hide();
                        $galleryModalNewMember.show();
                    }else if (photo.updatedStatus){
                        $galleryModalUpdatedMember.show();
                        $galleryModalNewMember.hide();
                    }else{
                        $galleryModalUpdatedMember.hide();
                        $galleryModalNewMember.hide();
                    }
                    
                    //favorite button
                    if(photo.favorited) {
                        $galleryModalFavorites.find('.s-icon-favorites-added').css({display:'inline-block'});
                        galleryManager.controlCommMessage();
                        $galleryModalFavorites.attr('title', galleryManager.unFavoriteTitle);
                    } else{
                        $galleryModalFavorites.find('.s-icon-favorites-added').css({display:'none'});
                        galleryManager.controlCommMessage();
                        $galleryModalFavorites.attr('title', galleryManager.favoriteTitle);
                    }

                    //flirt button reset
                    if (photo.flirted){
                        $galleryModalFlirt.find('.s-icon-flirt-sent').css({display:'inline-block'});
                    }else{
                        $galleryModalFlirt.find('.s-icon-flirt-sent').css({display:'none'});
                    }
                    $galleryModalFlirt.attr('title', galleryManager.flirtActiveTitle);

                    //previous button
                    if (selectedIndex > 0){
                        $galleryModalPrevious.show();
                    }else{
                        $galleryModalPrevious.hide();
                    }

                    //next button
                    if ((selectedIndex < (this.totalResultCount - 1)) || (selectedIndex < (this.photoList.length - 1))){
                        $galleryModalNext.show();
                    }else{
                        $galleryModalNext.hide();
                    }
                    
                    if(galleryManager.memberID <= 0) {
                        $j('#galleryModalFavorites').hide();
                    }

                    //update omniture
                    this.submitOmniturePageView(true);

                    //refresh the modal GAM adslot; spark.js
                    if ($galleryModalAdDisplay.toString() == 'block') {
                        updateAdFrameSR('iframeGAMPhotoGalleryModal', 'photogallery_modal_top_728x90', true);
                    }
                    
                    $modalHtml.show();

                    $j.colorbox({inline:true, href: $modalHtml, initialWidth:768, initialHeight:500, width:810, scrolling:false, 
                                onOpen:     function(){//Callback that fires right before ColorBox begins to open.
                                                $modalAd.css({display:'none'});
                                                
                                                if ($galleryModalAdDisplay.toString() == 'none'){
                                                    //reduce unneccesary vertical space when no AD
                                                    $galleryModalAdHolder.css({display:'none'});
                                                }
                                                //infinite scroll control
                                                galleryManager.controlInfiniteLoad(true);
                                                galleryManager.modalContainer.addClass('on');
                                            },
                                onLoad:     function(){//Callback that fires right before attempting to load the target content.
                                                //avoid visible collapsing when loading next photo batch
                                                if(selectedIndex % galleryManager.pageSize == 0){
                                                    $j('#cboxContent').prepend('<div id="cboxLoadingGraphicSolid"></div>');
                                                }
                                },
                                onComplete: function(){//Callback that fires right after loaded content is displayed.
                                                $modalAd.css({display:'block'});
                                                if ($galleryModalAdDisplay.toString() == 'block') {
                                                    $galleryModalAdHolder.css({display:'block'});
                                                    $galleryModalAd.css({display:'block'});
                                                }
                                                else {
                                                    $galleryModalAdHolder.css({display:'none'});
                                                    $galleryModalAd.css({display:'none'});
                                                }
                                                //avoid visible collapsing when loading next photo batch
                                                if(selectedIndex % galleryManager.pageSize == 0){
                                                    galleryManager.modalContainer.css({visibility:'hidden'});
                                                    $j.colorbox.resize();
                                                    $j('#cboxLoadingGraphicSolid').fadeOut(200, function(){
                                                        $j(this).remove();
                                                        galleryManager.modalContainer.css({visibility:'visible'});
                                                    });
                                                }
                                            },
                                onCleanup:  function(){//Callback that fires at the start of the close process.
                                                $modalAd.css({display: 'none'});
                                                //infinite scroll control
                                                galleryManager.controlInfiniteLoad(false);
                                                galleryManager.modalContainer.removeClass('on');
                                            }
                                });

                }
            }
            this.toggleFavorites = function(memberid, targetmemberid) {
                var galleryManagerRef = this.instanceRef,
                    $favorite = $j('#galleryModalFavorites'),
                    $favoriteIcon = $favorite.find('.s-icon-favorites-added'),
                    $favoriteMessageAdded = $j('#galleryModalFavoritesAdded'),
                    $favoriteMessageRemoved = $j('#galleryModalFavoritesRemoved'),
                    $favoriteMessageError = $j('#galleryModalFavoritesError');

                if (!this.updateInProgress) {
                    $j.ajax({
                            type: "POST",
                            url: "/Applications/API/Favorites.asmx/AddOrRemoveFavorite",
                            data: "{memberID:'" + memberid + "', favoriteMemberID: '" + targetmemberid + "'}",
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            beforeSend: function() {
                                galleryManagerRef.updateInProgress = true;
                            },
                            complete: function() {
                                galleryManagerRef.updateInProgress = false;
                            },
                            success: function(result) {
                                if(result.d == 'add') {
                                    //galleryModalFavoritesAdded
                                    //console.log("favorited!");
                                    $favoriteIcon.css({display:'inline-block'});
                                    galleryManager.controlCommMessage($favoriteMessageAdded);
                                    $favorite.attr('title', galleryManager.unFavoriteTitle);
                                    galleryManager.toggleMemberFavorited(targetmemberid, true);
                                    galleryManager.submitOmnitureFavorite(true);
                                }
                                else if(result.d == 'del') {
                                     //galleryModalFavoritesRemoved
                                     //console.log("UNfavorited!");
                                     $favoriteIcon.css({display:'none'});
                                     galleryManager.controlCommMessage($favoriteMessageRemoved);
                                     $favorite.attr('title', galleryManager.favoriteTitle);
                                    galleryManager.toggleMemberFavorited(targetmemberid, false);
                                }
                                else {
                                    //galleryModalFavoritesError
                                    galleryManager.controlCommMessage($favoriteMessageError);
                                }
                            },
                            error: function(jqXHR, textStatus, errorThrown) {
                                //galleryModalFavoritesError
                                console.log(errorThrown);
                            }
                            
                        });
                }

            }
            this.sendFlirt = function (sendingMemberId, targetMemberId, categoryKey, teaseKey) {
                var galleryManagerRef = this.instanceRef,
                    $flirt = $j('#galleryModalFlirt'),
                    $flirtIcon = $flirt.find('.s-icon-flirt-sent'),
                    $flirtMessageSucceed = $j('#galleryModalFlirtSucceed'),
                    $flirtMessageUnableFlirted = $j('#galleryModalFlirtUnableFlirted'),
                    $flirtMessageUnableLimit = $j('#galleryModalFlirtUnableLimit'),
                    $flirtMessageUnableNow = $j('#galleryModalFlirtUnableNow');

                if (!this.updateInProgress) {
                    $j.ajax({
                            type: "POST",
                            url: "/Applications/API/Flirts.asmx/SendFlirt",
                            data: "{sendingMemberId:'" + sendingMemberId + "', targetMemberId: '" + targetMemberId + "', categoryKey: '" + categoryKey + "', teaseKey: '" + teaseKey + "'}",
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            beforeSend: function() {
                                galleryManagerRef.updateInProgress = true;
                            },
                            complete: function() {
                                galleryManagerRef.updateInProgress = false;
                            },
                            success: function(result) {
                                if(result.d == '<%=FlirtOperationResult.FlirtSent.ToString("d") %>' || result.d == '<%=FlirtOperationResult.YouAreBlocked.ToString("d") %>') {
                                    //flirt sent successfully!
                                    $flirtIcon.css({display:'inline-block'});
                                    galleryManager.controlCommMessage($flirtMessageSucceed);
                                    $flirt.attr('title', galleryManager.flirtInactiveTitle);
                                    //galleryManager.photoList[galleryManager.currentModalPhotoIndex].flirted = true;
                                    for(var i=0; i< galleryManager.photoList.length; i++) {
                                        if (galleryManager.photoList[i].memberID == targetMemberId) {
                                            galleryManager.photoList[i].flirted = true;
                                        }
                                    }
                                    galleryManager.submitOmnitureFlirt();
                                }
                                else if (result.d == '<%=FlirtOperationResult.FlirtSentAlready.ToString("d") %>') {
                                    //you've already flirted with this member
                                    galleryManager.controlCommMessage($flirtMessageUnableFlirted);
                                    $flirt.attr('title', galleryManager.flirtInactiveTitle);
                                }
                                else if (result.d == '<%=FlirtOperationResult.TooManyFlirts.ToString("d") %>' || result.d == '<%=FlirtOperationResult.AlreadyUsedUpAllFreeTeases.ToString("d") %>') {
                                    //already hit limit for flirts today
                                    galleryManager.controlCommMessage($flirtMessageUnableLimit);
                                }
                                else if (result.d == '<%=FlirtOperationResult.SessionExpired.ToString("d") %>') {
                                    //session expired
                                    window.location.href = '/Applications/Logon/Logon.aspx?DestinationURL=' + encodeURIComponent(window.location.href.replace('http://'+window.location.host,''));
                                }
                                else {
                                     //sorry, problem with flirt!
                                     galleryManager.controlCommMessage($flirtMessageUnableNow);
                                }
                            },
                            error: function(jqXHR, textStatus, errorThrown) {
                                //sorry, problem with flirt!
                                console.log(errorThrown);
                            }
                            
                        });
                }
            }
            this.controlCommMessage = function($notice){
                var $messages = galleryManager.modalContainer.find('.profile-comm .message');
                    
                    $messages.hide();

                    if($notice){
                        $notice.fadeIn('slow', function(){
                            setTimeout(function(){
                                $notice.fadeOut("slow");
                                }, 2000);
                        });
                    }
            }
            this.updateFiltersAndLoadPhotoBatch = function (gender, minAge, maxAge, postedDayRange, regionId, sortBy) {
                var galleryManagerRef = this.instanceRef,
                    $divNoResults = $j("[id$='divNoResults']");
                if (!this.updateInProgress) {
                    this.startRow = 1;

                    this.sortBy = sortBy;
                    galleryManagerRef.ajaxBlock(galleryManagerRef.photoContainer);
                    $j.ajax({
                            type: "POST",
                            url: "/Applications/API/SearchResults.asmx/UpdateFiltersAndGetPhotoGalleryBatch",
                            data: "{gender:'" + gender + "', postedDayRange: '" + postedDayRange + "', regionId: '" + regionId + "', minAge: '" + minAge + "', maxAge: '" + maxAge +"', pageSize:'" + galleryManagerRef.pageSize + "', sortBy:'" + sortBy + "'}",
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            beforeSend: function() {
                                galleryManagerRef.updateInProgress = true;
                            },
                            complete: function() {
                                galleryManagerRef.updateInProgress = false;
                            },
                            success: function(result) {
                                var photoGalleryResult = (typeof result.d == 'undefined') ? result : result.d;
                                if (photoGalleryResult.Status != 2) {
                                   if(photoGalleryResult.StatusMessage == 'Filters Invalid') {
                                       resetFilterOptions();
                                       galleryManagerRef.ajaxUnBlock(galleryManagerRef.photoContainer);
                                   }
                                    
                                }
                                else {
                                    updateInitialFilterOptions();
                                    if (galleryManagerRef.isVerbose) {
                                        try {
                                            console.log('Total found: ' + photoGalleryResult.TotalResultCount);// + ', batch: ' + photoGalleryResult.PhotoGalleryMemberList);
                                        }
                                        catch (e) { }
                                    }
                                    galleryManagerRef.clearPhotos();
                                    ////load photos
                                    var newPhotoBatchList = [];
                                    galleryManagerRef.totalResultCount = photoGalleryResult.TotalResultCount;
                                    if (photoGalleryResult.PhotoGalleryMemberList.length > 0) {
                                        for (var i = 0; i < photoGalleryResult.PhotoGalleryMemberList.length; i++) {
                                            var photoGalleryMember = photoGalleryResult.PhotoGalleryMemberList[i];
                                            var photo1 = galleryManagerRef.createPhoto(photoGalleryMember);
                                            galleryManagerRef.addPhoto(photo1);
                                            spark.util.addItem(newPhotoBatchList, photo1);
                                        }

                                        galleryManagerRef.submitOmniturePageView(false);
                                        galleryManagerRef.loadPhotoBatchUI(newPhotoBatchList);
                                        try {
                                            console.log('Loaded page ' + galleryManagerRef.getCurrentPageNumber());
                                        }
                                        catch(e) {}
                                    }
                                    else {
                                        //no results or no more results
                                        if(galleryManager.photoContainer.find($divNoResults).length == 0){
                                            $divNoResults.clone().prependTo(galleryManagerRef.photoContainer).show();
                                        }
                                        galleryManagerRef.ajaxUnBlock(galleryManagerRef.photoContainer);
                                    }
                                }
                            },
                            error: function(jqXHR, textStatus, errorThrown) {
                                if (galleryManagerRef.isVerbose) {
                                    alert(textStatus);
                                }
                                galleryManagerRef.ajaxUnBlock(galleryManagerRef.photoContainer);
                            }
                        });
                }
            }
            this.controlInfiniteLoad = function(isModalOn){
                var $window = $j(window), $document = $j(document), bottomMargin = 250;
                
                if(isModalOn){
                    //disable infinite scroll when modal is on
                    $j(window).unbind('scroll.infinite');
                } else {
                    $window.bind('scroll.infinite', function(){
                        if ($window.scrollTop() >= $document.height() - $window.height() - bottomMargin) {
                            //add next photo batch at the end of the page
                            $window.unbind('scroll.infinite');
                            galleryManager.onNextPageLoad(false);
                        }    
                    });
                }

            }
            this.loadPhotoBatch = function (isTriggeredFromModalNext) {
                var galleryManagerRef = this.instanceRef;
                var $divNoResults = $j("[id$='divNoResults']");
                
                //retrieve data from server
                if (this.startRow <= 1 || this.photoList.length < this.totalResultCount){
                    if (!this.updateInProgress) {
                        var existingStartRow = this.startRow;
                        if (this.photoList.length <= 0){
                            this.startRow = 1;
                        }
                        else{
                            this.startRow = this.startRow + this.pageSize;
                        }

                        //ajax loader
                        if(this.startRow <= 1){
                            galleryManagerRef.ajaxBlock(galleryManagerRef.photoContainer);
                        } 
                        else if (isTriggeredFromModalNext){
                            //JAY-show ajax load for modal
                            //$j('#galleryModalContainer').css({minHeight:500});
                        }
                        else{
                            galleryManagerRef.infscrLoader('on');
                        }
                            
                        $j.ajax({
                            type: "POST",
                            url: "/Applications/API/SearchResults.asmx/GetPhotoGalleryBatch",
                            data: "{startRow:" + galleryManagerRef.startRow + ", pageSize:" + galleryManagerRef.pageSize + "}",
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            beforeSend: function () {
                                galleryManagerRef.updateInProgress = true;
                            },
                            complete: function () {
                                galleryManagerRef.updateInProgress = false;
                            },
                            success: function (result) {
                                var photoGalleryResult = (typeof result.d == 'undefined') ? result : result.d;
                                if (photoGalleryResult.Status != 2) {
                                    if (galleryManagerRef.isVerbose) {
                                        alert(photoGalleryResult.StatusMessage);
                                    }
                                }
                                else {

                                    if (galleryManagerRef.isVerbose) {
                                        try {
                                            console.log('Total found: ' + photoGalleryResult.TotalResultCount);// + ', batch: ' + photoGalleryResult.PhotoGalleryMemberList);
                                        }
                                        catch (e) { }
                                    }
                                    ////load photos
                                    var newPhotoBatchList = [];
                                    var existingTotalResultCount = galleryManagerRef.totalResultCount;
                                    galleryManagerRef.totalResultCount = photoGalleryResult.TotalResultCount;
                                    if (photoGalleryResult.PhotoGalleryMemberList.length > 0) {
                                        for (var i = 0; i < photoGalleryResult.PhotoGalleryMemberList.length; i++) {
                                            var photoGalleryMember = photoGalleryResult.PhotoGalleryMemberList[i];
                                            var photo1 = galleryManagerRef.createPhoto(photoGalleryMember);
                                            galleryManagerRef.addPhoto(photo1);
                                            spark.util.addItem(newPhotoBatchList, photo1);
                                        }
                                            
                                        if (isTriggeredFromModalNext){
                                            //load next photo in modal
                                            galleryManagerRef.onGalleryPhotoClick(galleryManagerRef.currentModalPhotoIndex + 1);
                                        }
                                        else if (galleryManagerRef.startRow > 1){
                                            //normal pagination via scrolling
                                            galleryManagerRef.submitOmniturePageView(false);
                                        }
                                        galleryManagerRef.loadPhotoBatchUI(newPhotoBatchList);

                                        try{
                                            console.log('Loaded page ' + galleryManagerRef.getCurrentPageNumber());
                                        }
                                        catch(e){}
                                    }
                                    else{
                                        //no results or no more results
                                        try{
                                            console.log('No more results found');
                                        }
                                        catch(e){}
                                        if(galleryManagerRef.startRow <= 1){
                                            $divNoResults.prependTo(galleryManagerRef.photoContainer).show();
                                        }
                                        else if (isTriggeredFromModalNext){
                                            if (galleryManagerRef.totalResultCount <= 0){
                                                //some error occurred, reset total to previous so user can retry
                                                galleryManagerRef.totalResultCount = existingTotalResultCount;
                                                try{
                                                    console.log('Error getting next batch, total found: ' + photoGalleryResult.TotalResultCount);
                                                }
                                                catch(e){}
                                            }
                                            //load current photo in modal (this will hide next button, should only happen if cache expired and new search resulted in less photos)
                                            galleryManagerRef.onGalleryPhotoClick(galleryManagerRef.currentModalPhotoIndex);
                                        }

                                        if (galleryManagerRef.photoList.length < galleryManagerRef.totalResultCount)
                                        {
                                            galleryManagerRef.startRow = existingStartRow; //reset start row to previous to try again, if there was an error
                                        }
                                        
                                        galleryManagerRef.ajaxUnBlock(galleryManagerRef.photoContainer);
                                        galleryManagerRef.infscrLoader('off');
                                        
                                    }

                                }
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                if (galleryManagerRef.isVerbose) {
                                    alert(textStatus);
                                }
                                try{
                                    console.log('Error getting next batch: ' + textStatus);
                                }
                                catch(e){}
                                galleryManagerRef.ajaxUnBlock(galleryManagerRef.photoContainer);
                                galleryManagerRef.infscrLoader('off');
                                galleryManagerRef.startRow = existingStartRow;
                                //JAY-hide ajax load for modal
                            }
                        });
                    }
                }
                else{
                    //no more results
                    try{
                        console.log('Already on last page');
                    }
                    catch(e){}
                }
            }
            this.loadPhotoBatchUI = function (newPhotoBatchList) {
                var data, template, html, $photoContainer = this.photoContainer,
                    $window = $j(window), $document = $j(document), $siteContainer = $j('#site-container'), 
                    $nextSelector = $j('#nextSelector');

                //generate html trough mustache js
                data = {photoList: newPhotoBatchList};
                //template = '{{#photoList}}<div class="photo"><div class="wrapper clearfix"><a href="#" onclick="galleryManager.onGalleryPhotoClick({{photoIndex}});return false;" title="{{username}}" rel="modal"><img src="{{largeImageURL}}" alt="{{userName}}" /></a><div class="info sliding"><h3><a href="#" title="{{username}}" onclick="galleryManager.onGalleryPhotoClick({{photoIndex}});return false;">{{username}}</a></h3><span>{{ageDisplayText}}</span><small class="timestamp">{{postedDateDisplayText}}</small></div></div></div>{{/photoList}}';
                template = '{{#photoList}}<div class="photo"><div class="wrapper clearfix"><a href="#" onclick="galleryManager.onGalleryPhotoClick({{photoIndex}});return false;" rel="modal"><img src="{{largeImageURL}}" alt="{{username}}" /></a><div class="info"><h3 class="user-name"><a href="#" title="{{username}}" onclick="galleryManager.onGalleryPhotoClick({{photoIndex}});return false;">{{username}}</a></h3><span title="{{location}}">{{ageDisplayText}}, {{locationShort}} </span><small class="timestamp">{{postedDateDisplayText}}</small></div></div></div>{{/photoList}}';
                html = Mustache.to_html(template, data);

                //trigger isotope
                if(this.startRow == 1){
                    //first time load or filter action
                    if($photoContainer.hasClass('isotope')){
                        $photoContainer.isotope('destroy');
                    }
                    $photoContainer.append(html);
                    galleryManager.isotopeInit();

                } else {
                    var $newItems = $j(html).css({ opacity: 0 });
                        $temp = $j('#tempHolder'),
                        $tempImgs = '',
                        tempImgsLength = 0,
                        loadedImgCount = 0;

                    $temp.append($newItems);
                    $tempImgs = $temp.find('img');
                    tempImgsLength = $tempImgs.length;

                    $tempImgs.each(function(){
                        var $this = $j(this),
                            new_img_src = $this.attr('src');

                        loadedImgCount++;

                        $this.load(function(){
                            if(loadedImgCount == tempImgsLength){
                                $loadedItems = $temp.find('.photo');
                                $photoContainer.isotope('insert', $loadedItems, function(){
                                    galleryManager.infscrLoader('off');
                                    $window.bind('scroll.infinite');
                                });
                            }
                        }).attr('src', new_img_src);
                    
                    });

//                    $temp.imagesLoaded(function(){
//                        $loadedItems = $temp.find('.photo');
//                        $photoContainer.isotope('insert', $loadedItems, function(){
//                            //console.log('INSERTED');
//                            galleryManager.infscrLoader('off');
//                            $window.bind('scroll.infinite');
//                        });
//                    });
                }

                //infinite scroll control
                galleryManager.modalContainer.hasClass('on') ? galleryManager.controlInfiniteLoad(true) : galleryManager.controlInfiniteLoad(false);
      
            }
            this.isotopeInit = function () {
                var $photoContainer = this.photoContainer,
                    $tempImgs = $photoContainer.find('img');
                    tempImgsLength = $tempImgs.length,
                    loadedImgCount = 0;

                $tempImgs.each(function(){
                    var $this = $j(this),
                        new_img_src = $this.attr('src');

                    loadedImgCount++;

                    $this.load(function(){
                        if(loadedImgCount == tempImgsLength){
                            $photoContainer.isotope({
                                itemSelector: '.photo',
                                transformsEnabled: false,
                                masonry: { columnWidth: $photoContainer.width() / 5 },
                                onLayout: function(){
                                    galleryManager.ajaxUnBlock(galleryManager.photoContainer);
                                }
                            });
                        }
                    }).attr('src', new_img_src);
                    
                });

                // trigger Isotope after images are loaded
//                $photoContainer.imagesLoaded(function () {
//                    $photoContainer.isotope({
//                        itemSelector: '.photo',
//                        transformsEnabled: false,
//                        masonry: { columnWidth: $photoContainer.width() / 5 },
//                        onLayout: function(){
//                            galleryManager.ajaxUnBlock(galleryManager.photoContainer);
//                        }
//                    });
//                });

                // sliding effect for IE9 only - chrome, ff will use css3 transition. (per performance issue - IE8 below use css hover only)
//                if($j('html').hasClass('ie9')){
//                    $photoContainer.on("mouseenter mouseleave", ".photo", function(e){
//                        if( e.type === 'mouseenter' ) {
//                            $j(".info", this).stop().animate({bottom: 0}, 200);
//                        } else if ( e.type === 'mouseleave' ){
//                            var captionHeight = $j(this).height();
//                            $j(".info", this).stop().animate({bottom:'-' + captionHeight}, 200);
//                        }
//                    });
//                }

            } // isotopeInit
            this.ajaxBlock = function (blockObj) {
                if (!this.isBlocked) {
                    this.isBlocked = true;
                    var blockParams = {
                        message: '<div class="loading spinner-only" /><blockquote><mn:txt runat="server" resourceconstant="TXT_LOADING" /></blockquote>',
                        centerY: false,
                        css: { backgroundColor: 'transparent', border: 'none', height: '85px', top: '30%' },
                        overlayCSS: { backgroundColor: '#fff', opacity: 0.8 }
                    };
                    blockObj.block(blockParams);
                }
            }
            this.ajaxUnBlock = function (blockObj) {
                if (this.isBlocked) {
                    blockObj.unblock();
                }
                this.isBlocked = false;
            }
            this.infscrLoader = function (cont){
                var $infscrLoader = $j('#infscrLoader');

                if (cont == 'on') {
                    this.isBlocked = true;
                    $infscrLoader.removeClass('hide');
                } else{
                    $infscrLoader.addClass('hide');
                    this.isBlocked = false;
                }
            }
            this.submitOmniturePageView = function (isModal){
                //update omniture
                if (s != null) {
                    PopulateS(true); //clear existing values in omniture "s" object

                    if (isModal){
                        s.pageName = "Photo Gallery - Modal";
                    }
                    else{
                        s.pageName = "Photo Gallery - PhotoGallery";
                    }
                    s.eVar25 = "" + this.getCurrentPageNumber() + "";
                    if (this.sortBy == 0){
                        s.eVar67 = 'Newest';
                    }
                    else{
                        s.eVar67 = 'Random';
                    }

                    if (this.brandId == 1004) { // IL site only
                        s.events = "event12";
                    }

                    s.t();
                }
            }
            this.submitOmnitureFavorite = function (isAdd){
                //currently, we only add omniture when adding to favorites
                if (isAdd){
                    //update omniture
                    if (s != null) {
                        PopulateS(true); //clear existing values in omniture "s" object
                        s.pageName = "Photo Gallery - Modal";
                        s.events = "event27";
                        s.eVar27 = "Hotlist (Photo Gallery - Modal)";
                        s.prop29 = s.pageName;
                        s.t();
                    }
                }
            }
            this.submitOmnitureFlirt = function (){
                //update omniture
                if (s != null) {
                    PopulateS(true); //clear existing values in omniture "s" object
                    s.pageName = "Photo Gallery - Modal";
                    s.events = "event11";
                    s.eVar26 = "Tease";
                    s.prop29 = s.pageName;
                    s.t();
                }
            }
            this.submitOmnitureProfileView = function () {
                if (this.brandId == 1004) { // IL site only
                    //update omniture
                    if (s != null) {
                        PopulateS(true); //clear existing values in omniture "s" object
                        s.pageName = "View Profile - Tab 1 - In My Own Words";
                        s.events = "event2,event12";
                        s.prop29 = s.pageName;
                        s.t();
                    }
                }
            }

        } //spark_gallery_manager

        //initialize objects
        var galleryManager = new spark_gallery_manager();

        $j(function () {
            var $window = $j(window),
                backtoTopVisible = false,
                $backtoTopWrapper = $j('#backtoTop').parent(),
                $adHtml = '<div class="ad" id="galleryModalAd"><iframe id="iframeGAMPhotoGalleryModal" name="iframeGAMPhotoGalleryModal" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" width="728" height="90"></iframe></div>';
            
            //place modal html inside of modal window
            $j('#cboxContent').prepend($adHtml);
        
            galleryManager.memberID = <%=_MemberID%>;
            galleryManager.brandId = <%=_g.Brand.BrandID.ToString()%>;
            galleryManager.pageSize = <%=_PageSize%>;
            galleryManager.instanceRef = galleryManager;
            galleryManager.photoContainer = $j('#galleryContainer');
            galleryManager.modalContainer = $j('#galleryModalContainer');
            galleryManager.modalAd = $j('#galleryModalAd');
            galleryManager.favoriteTitle = '<%=TextFavoriteHover%>'; //TXT_FAVORITE_HOVER
            galleryManager.unFavoriteTitle = '<%=TextUnfavoriteHover%>'; //TXT_UNFAVORITE_HOVER
            galleryManager.flirtActiveTitle = '<%=TextFlirtActiveHover%>'; //TXT_FLIRT_HOVER_ACTIVE
            galleryManager.flirtInactiveTitle = '<%=TextFlirtInactiveHover%>'; //TXT_FLIRT_HOVER_INACTIVE
            galleryManager.sortBy = <%=_SortBy%>;
            galleryManager.onInitialLoad();

            //bind arrow keyboard
            $j(document.documentElement).bind('keyup.navigation', function(e){
                // handle cursor keys
                if (e.keyCode == 37) {
                    // go previous
                    galleryManager.onPreviousPhotoClick();
                } else if (e.keyCode == 39) {
                    // go next
                    galleryManager.onNextPhotoClick();
                }
            });

            //control back to top button
            $j('#backtoTop').click(function(e) {
                e.preventDefault();
                $j('html, body').animate({ scrollTop: 0 }, 500);
            });

            $window.bind('scroll.backtotop', function(){
                if($window.scrollTop() < $window.height() / 2 && !backtoTopVisible){
                    $backtoTopWrapper.fadeOut();
                    backtoTopVisible = true;
                    //console.log('GONE');
                } else if ($window.scrollTop() > $window.height() / 2 && backtoTopVisible){
                    $backtoTopWrapper.fadeIn();
                    backtoTopVisible = false;
                    //console.log('BACK');
                }
            });
        
        });

    </script>
</div>
