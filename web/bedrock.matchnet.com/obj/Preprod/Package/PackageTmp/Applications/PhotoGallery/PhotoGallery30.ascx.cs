﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Diagnostics;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Managers;
using Matchnet.Web.Framework.Ui.Advertising;

namespace Matchnet.Web.Applications.PhotoGallery
{
    public partial class PhotoGallery30 : FrameworkControl
    {
        protected int _MemberID = 0;
        protected int _PageSize = 24;
        protected int _SortBy = 1;
        protected string TextFavoriteHover { get; set; }
        protected string TextUnfavoriteHover { get; set; }
        protected string TextFlirtActiveHover { get; set; }
        protected string TextFlirtInactiveHover { get; set; }
        protected string galleryModalAdDisplay { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (g.Member != null)
            {
                _MemberID = g.Member.MemberID;
            }

            _PageSize = PhotoGalleryManager.Instance.GetPageSize(g.Brand);
            PopulateTextProperties();

            try
            {
                bool galleryModalAdVisible = Conversion.CBool(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ENABLE_GALLERYMODAL_ADSLOT", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID));
                if (galleryModalAdVisible)
                {
                    galleryModalAdDisplay = "block";
                }
                else
                {
                    galleryModalAdDisplay = "none";
                }
            }
            catch (Exception ex)
            {
                galleryModalAdDisplay = "block";  //If error just set to display so that page doesn't crash
                string message = "GetSetting threw an exception: " + ex.Message;
                EventLog.WriteEntry("PhotoGallery30.ascx.cs", message, EventLogEntryType.Error);
            }
         
        }

        protected override void OnPreRender(EventArgs e)
        {
            if (Convert.ToBoolean(Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ANALYTICS_OMNITURE", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID)))
            {
                g.AnalyticsOmniture.Evar25 = "1"; //page 1 is default when gallery loads
            }

            if (photogalleryfilter.filterSettings == null)
            {
                photogalleryfilter.filterSettings = PhotoGalleryManager.Instance.GetFilterSettings(g.Brand, g.Member);
            }
            _SortBy = photogalleryfilter.filterSettings.SortBy;

            base.OnPreRender(e);
        }

        private void PopulateTextProperties()
        {
            TextFavoriteHover = ResourceManager.GetResource("TXT_FAVORITE_HOVER", this);
            TextUnfavoriteHover = ResourceManager.GetResource("TXT_UNFAVORITE_HOVER", this);
            TextFlirtActiveHover = ResourceManager.GetResource("TXT_FLIRT_HOVER_ACTIVE", this);
            TextFlirtInactiveHover = ResourceManager.GetResource("TXT_FLIRT_HOVER_INACTIVE", this);
        }
    }

}
