﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="PhotoGalleryFilter20.ascx.cs" Inherits="Matchnet.Web.Applications.PhotoGallery.Controls.PhotoGalleryFilter20" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="cc1" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<div class="filter">
<fieldset>
    <legend>Filter Photo Gallery results</legend>
	<label class="view">
		<mn:txt runat="server" id="txtShow" resourceconstant="TXT_SHOW" />:<br />
		<select id="GenderMask" runat="server"></select>
	</label>
	<label class="from">
		<mn:txt runat="server" id="txtPosted" resourceconstant="TXT_POSTED" />:<br />
		<select id="PostedDays" runat="server"></select>
	</label>
	<label class="from">
		<mn:txt runat="server" id="txtFrom" resourceconstant="TXT_FROM" />:<br />
		<select id="RegionID" runat="server"></select>
	</label>
	<div class="age-container">
        <label class="age-from">
		    <mn:txt runat="server" id="txtAges" resourceconstant="TXT_AGE" />:<br />
		    <input id="AgeMin" type="text" size="2" maxlength="2" runat="server" />
		  </label>
		  <label class="age-to">
		    &nbsp;<br /><mn:txt runat="server" id="txtTo" resourceconstant="TO_" />
		    <input id="AgeMax" type="text" size="2" maxlength="2" runat="server" />
		  </label>
	</div>	
	
	<label class="button">&nbsp;<br />
		<asp:Button runat="server" id="btnSearch" CssClass="btn secondary"  />
	</label>
	<br />
	<asp:rangevalidator runat="server" id="AgeMinRange" controltovalidate="AgeMin" display="None"></asp:rangevalidator>
	<br />
	<asp:rangevalidator runat="server" id="AgeMaxRange" controltovalidate="AgeMax" display="None"></asp:rangevalidator>
	<br />
	<asp:requiredfieldvalidator runat="server" id="AgeMinReq" controltovalidate="AgeMin" display="None"></asp:requiredfieldvalidator>
	<asp:requiredfieldvalidator runat="server" id="AgeMaxReq" controltovalidate="AgeMax" display="None"></asp:requiredfieldvalidator>
	<asp:validationsummary runat="server" id="valSummary" displaymode="BulletList"></asp:validationsummary>
</fieldset>	
</div>