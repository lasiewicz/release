﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PhotoGalleryProfile20.ascx.cs" Inherits="Matchnet.Web.Applications.PhotoGallery.Controls.PhotoGalleryProfile20" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mnl" Namespace="Matchnet.Web.Framework.Ui.BasicElements.Links" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="uc2" TagName="HighlightProfileInfoDisplay" Src="../../../Framework/Ui/BasicElements/HighlightInfoDisplay20.ascx" %>

<asp:Literal id="profileHoverUp" runat="server"/>
<div class="results photo-view" id="divPhotoGallery" runat="server">
    <div id="divPhotoGalleryProfile">
        <uc2:HighlightProfileInfoDisplay runat="server" id="ucHighlightProfileInfoDisplay" ShowImageNoText="true" />				
        <div class="picture">
        <mn:Image runat="server" id="imgThumb" border="0" ResourceConstant="" Width="79" Height="104" />
        </div>
        <h2><asp:HyperLink  id="lnkUserName" runat="server" /></h2>
        <p class="details"><asp:label id="txtAge" runat="server" class="normalText" /></p>
        <p class="timestamp"><asp:label id="txtPosted" runat="server" class="userdata" /></p>
    </div>			
</div>
