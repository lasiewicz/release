﻿using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Matchnet.Lib;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Ui;
using Matchnet.Web.Framework.Ui.BasicElements;

namespace Matchnet.Web.Applications.PhotoGallery
{
    public partial class PhotoGallery20 : FrameworkControl
    {
        protected ResultList20 PhotoGalleryList;
        protected Label lblListNavigationTop;
        protected System.Web.UI.HtmlControls.HtmlInputHidden StartRow;
        protected Label lblListNavigationBottom;

        private void Page_Load(object sender, System.EventArgs e)
        {

            // Put user code to initialize the page here
            //PhotoGalleryList.PageSize=12;
            //PhotoGalleryList.ChapterSize=4;
            string ctrlname = Request["Postback"];
            if (ctrlname != null && ctrlname != String.Empty)
            {
                PhotoGalleryList.StartRow = 1;

            }
            // Wire up the List Navigation.
            g.ListNavigationTop = lblListNavigationTop;
            g.ListNavigationBottom = lblListNavigationBottom;

            if (g.BreadCrumbTrailHeader != null)
            {
                g.BreadCrumbTrailHeader.SetTwoLinkCrumb(g.GetResource("TXT_PHOTO_GALLERY", this),
                                                        g.AppPage.App.DefaultPagePath);

            }
            if (g.BreadCrumbTrailFooter != null)
            {
                g.BreadCrumbTrailFooter.SetTwoLinkCrumb(g.GetResource("TXT_PHOTO_GALLERY", this),
                                                       g.AppPage.App.DefaultPagePath);
            }

        }
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);


        }


        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>

        #endregion
    }
}