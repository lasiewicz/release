﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet;
using Matchnet.Member.ServiceAdapters;
using Matchnet.PhotoSearch.ValueObjects;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Managers;

namespace Matchnet.Web.Applications.PhotoGallery
{
    public class PhotoGalleryPersistence
    {
        private int _memberID;
        private string _cookieName = "PG_{0}";
        private HttpCookie _persistCookie;
        private int _expirationDays;

        public int AgeMax { get; set; }
        public int AgeMin { get; set; }
        public int CommunityID { get; set; }
        public int CountryRegionID { get; set; }
        public int Gender { get; set; }
        public int GenderMask { get; set; }
        public bool GuestFlag { get; set; }
        public DateTime InsertDateMin { get; set; }
        public int InsertDays { get; set; }
        public int Page { get; set; }
        public int PageSize { get; set; }
        public double Radius { get; set; }
        public int RegionID { get; set; }
        public SearchRegionType RegionType { get; set; }
        public ReLoadFlags ReloadFlag { get; set; }
        public int SiteID { get; set; }
        public bool ShowHiddenFlag { get; set; }
        public int SortBy { get; set; }

        private const int AGE_RANGE_FOR_PHOTO_GALLERY_AS_HOMEPAGE = 7;

        public int MemberID
        {
            get
            {
                return _memberID;
            }
        }

        public PhotoGalleryPersistence(int memberID, int expirationDays, Brand brand)
        {
            _memberID = memberID;
            string _name = string.Format(_cookieName, memberID.ToString());

            _persistCookie = HttpContext.Current.Request.Cookies[_name];

            if (_persistCookie == null)
            {
                _persistCookie = new HttpCookie(_name, _name);
                InitializeNullValues(brand);
            }
            else
            {
                LoadValues();
            }

            _expirationDays = expirationDays;

            _persistCookie.Expires = DateTime.Now.AddDays(_expirationDays);
        }

        public void PersistValues()
        {
            _persistCookie["AgeMin"] = AgeMin.ToString();
            _persistCookie["AgeMax"] = AgeMax.ToString();

            _persistCookie["CommunityID"] = CommunityID.ToString();
            _persistCookie["CountryRegionID"] = CountryRegionID.ToString();
            _persistCookie["Gender"] = Gender.ToString();
            _persistCookie["GenderMask"] = GenderMask.ToString();
            _persistCookie["GuestFlag"] = GuestFlag.ToString();
            _persistCookie["InsertDateMin"] = InsertDateMin.ToString();
            _persistCookie["InsertDays"] = InsertDays.ToString();
            _persistCookie["Page"] = Page.ToString();
            _persistCookie["PageSize"] = PageSize.ToString();
            _persistCookie["Radius"] = Radius.ToString();
            _persistCookie["RegionID"] = RegionID.ToString();
            _persistCookie["RegionType"] = ((int)RegionType).ToString();
            _persistCookie["ReloadFlag"] = ReloadFlag.ToString();
            _persistCookie["SiteID"] = SiteID.ToString();
            _persistCookie["ShowHiddenFlag"] = ShowHiddenFlag.ToString();
            _persistCookie["SortBy"] = SortBy.ToString();
            _persistCookie.Expires = DateTime.Now.AddDays(_expirationDays);
            HttpContext.Current.Response.Cookies.Add(_persistCookie);

        }

        public void InitializeNullValues(Brand brand)
        {
            AgeMin = (int)brand.DefaultAgeMin;
            AgeMax = (int)brand.DefaultAgeMax;
            CommunityID = Constants.NULL_INT;
            CountryRegionID = Constants.NULL_INT;
            Gender = 0;
            GenderMask = Constants.NULL_INT;
            GuestFlag = false;
            InsertDateMin = new DateTime();
            InsertDays = 7;
            Page = Constants.NULL_INT;
            PageSize = Constants.NULL_INT;
            Radius = Constants.NULL_INT;
            RegionID = brand.Site.DefaultRegionID;
            if (Convert.ToBoolean(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("PHOTO_GALLERY_DEFAULT_COUNTRY_ENABLED", brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID)))
            {
                RegionType = SearchRegionType.country;
                CountryRegionID = brand.Site.DefaultRegionID;
            }
            else
            {
                RegionType = SearchRegionType.anywhere;
            }
            ReloadFlag = ReLoadFlags.none;
            SiteID = Constants.NULL_INT;
            ShowHiddenFlag = true;
            SortBy = (int)SortByType.random;

            Matchnet.Member.ServiceAdapters.Member member =
                Matchnet.Member.ServiceAdapters.MemberSA.Instance.GetMember(MemberID, MemberLoadFlags.None);

            if (PhotoGalleryManager.Instance.IsPhotoGalleryAsHomePageEnabled(member, brand))
            {
               int genderMask = member.GetAttributeInt(brand, WebConstants.ATTRIBUTE_NAME_GENDERMASK);
               Gender = (int)Spark.SAL.GenderUtils.GetGender(genderMask);
               GenderMask = genderMask;
               int memberAge =  Matchnet.Web.Framework.FrameworkGlobals.GetAge(member, brand);
               AgeMin = (memberAge - AGE_RANGE_FOR_PHOTO_GALLERY_AS_HOMEPAGE < 18) ? 18 : memberAge - AGE_RANGE_FOR_PHOTO_GALLERY_AS_HOMEPAGE;
               AgeMax = (memberAge + AGE_RANGE_FOR_PHOTO_GALLERY_AS_HOMEPAGE > 99) ? 99 : memberAge + AGE_RANGE_FOR_PHOTO_GALLERY_AS_HOMEPAGE;
            }
        }

        public string GetPersistedValue(string key)
        {
            return _persistCookie[key].ToString();
        }

        public void Clear()
        {
            if (_persistCookie != null)
                _persistCookie.Expires = DateTime.Now.AddYears(-30);
            HttpContext.Current.Response.Cookies.Add(_persistCookie);
        }

        private void LoadValues()
        {

            AgeMin = Convert.ToInt32(_persistCookie["AgeMin"]);
            AgeMax = Convert.ToInt32(_persistCookie["AgeMax"]);
            CommunityID = Convert.ToInt32(_persistCookie["CommunityID"]);
            CountryRegionID = Convert.ToInt32(_persistCookie["CountryRegionID"]);
            RegionID = Convert.ToInt32(_persistCookie["RegionID"]);
            Gender = Convert.ToInt32(_persistCookie["Gender"]);
            GenderMask = Convert.ToInt32(_persistCookie["GenderMask"]);
            GuestFlag = Convert.ToBoolean(_persistCookie["GuestFlag"]);
            InsertDateMin = Convert.ToDateTime(_persistCookie["InsertDateMin"]);
            InsertDays = Convert.ToInt32(_persistCookie["InsertDays"]);
            Page = Convert.ToInt32(_persistCookie["Page"]);
            PageSize = Convert.ToInt32(_persistCookie["PageSize"]);
            Radius = Convert.ToInt32(_persistCookie["Radius"]);
            RegionType = (SearchRegionType)Convert.ToInt32(_persistCookie["RegionType"]);
            ReloadFlag = (ReLoadFlags)Convert.ToInt32(_persistCookie["RegionType"]);
            SiteID = Convert.ToInt32(_persistCookie["SiteID"]);
            ShowHiddenFlag = Convert.ToBoolean(_persistCookie["ShowHiddenFlag"]);
            if (!String.IsNullOrEmpty(_persistCookie["SortBy"]))
            {
                SortBy = Convert.ToInt32(_persistCookie["SortBy"]);
            }

            if (SortBy < 0)
                SortBy = (int)SortByType.random;

        }


    }
}
