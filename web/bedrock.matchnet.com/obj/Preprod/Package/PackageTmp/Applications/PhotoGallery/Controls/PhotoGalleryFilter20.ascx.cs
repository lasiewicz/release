﻿#region System References
using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
#endregion

#region Matchnet Web App References
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Util;
using Matchnet.Lib;
#endregion

#region Matchnet Service References
using Matchnet.Session.ValueObjects;
#endregion

namespace Matchnet.Web.Applications.PhotoGallery.Controls
{
    public partial class PhotoGalleryFilter20 : FrameworkControl,IPhotoGalleryFilter
    {
        public const string SESSION_PHOTO_GALLERY_QUERY = "SESSION_PHOTO_GALLERY_QUERY";
        int _siteID;
        int _brandID;
        int _communityiID;
        int _regionID;
        int _memberRegionID = int.MinValue;
        int _memberCountryRegionID = int.MinValue;
        PhotoSearch.ValueObjects.PhotoGalleryQuery _query = null;


        #region IPhotoGalleryFilter implementation

        public System.Web.UI.HtmlControls.HtmlSelect SelGenderMask { get { return GenderMask; } }
        public System.Web.UI.HtmlControls.HtmlSelect SelRegionID { get { return RegionID; } }
        public System.Web.UI.HtmlControls.HtmlSelect SelPostedDays { get { return PostedDays; } }
        public System.Web.UI.HtmlControls.HtmlInputText TxtAgeMin { get { return AgeMin; } }
        public System.Web.UI.HtmlControls.HtmlInputText TxtAgeMax { get { return AgeMax; } }


        public System.Web.UI.WebControls.RangeValidator ValAgeMinRange { get { return AgeMinRange; } }
        public System.Web.UI.WebControls.RangeValidator ValAgeMaxRange { get { return AgeMaxRange; } }
        public System.Web.UI.WebControls.RequiredFieldValidator ValAgeMinReq { get { return AgeMinReq; } }
        public System.Web.UI.WebControls.RequiredFieldValidator ValAgeMaxReq { get { return AgeMaxReq; } }
        public System.Web.UI.WebControls.ValidationSummary ValSummary { get { return valSummary; } }
     
        public Matchnet.Web.Framework.Txt TxtShow { get { return txtShow; } }
        public Matchnet.Web.Framework.Txt TxtFrom { get { return txtFrom; } }
        public Matchnet.Web.Framework.Txt TxtAges { get { return txtAges; } }
        public Matchnet.Web.Framework.Txt TxtTo { get { return txtTo; } }
      



        public int SiteID { get { return _siteID; } set { _siteID = value; } }
        public int BrandID { get { return _brandID; } set { _brandID = value; } }
        public int CommunityiID { get { return _communityiID; } set { _communityiID = value; } }
        public int QueryRegionID { get { return _regionID; } set { _regionID = value; } }
        public int MemberRegionID { get { return _memberRegionID; } set { _memberRegionID = value; } }
        public int MemberCountryRegionID { get { return _memberCountryRegionID; } set { _memberCountryRegionID = value; } }
        public PhotoSearch.ValueObjects.PhotoGalleryQuery Query { get { return _query; } set { _query = value; } }

        public FrameworkControl ResourceControl { get { return this; } }

        #endregion

        PhotoGalleryFilterHandler _handler;

        //public int intRegionID
        //{
        //    get
        //    {
        //        //down and dirty fix for TT#16325
        //        if (g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.Cupid && g.Brand.Site.Name == "Cupid.co.il")
        //        {
        //            string molRegionID = g.Session["MembersOnlineRegion"].ToString();
        //            if (molRegionID != null && molRegionID != String.Empty)
        //            {
        //                this.RegionID.Value = molRegionID;
        //            }
        //        }
        //        return Matchnet.Conversion.CInt(RegionID.Value);
        //    }
        //}

        public int intAgeMin
        {
            get { return Matchnet.Conversion.CInt(AgeMin.Value); }
        }

        public int intAgeMax
        {
            get { return Matchnet.Conversion.CInt(AgeMax.Value); }
        }



        private void Page_Init(object sender, System.EventArgs e)
        {
            try
            {
                //PostedDays.on
                _handler = new PhotoGalleryFilterHandler(g, this);
            }

            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        private void Page_Load(object sender, System.EventArgs e)
        {
            try
            {

                btnSearch.Text = g.GetResource("SEARCH", this);
                _handler.LoadPage(IsPostBack);

            }
            catch (Exception ex)
            {
                string q = "";
                if (_query != null)
                { q = _query.ToString(); }

                System.Diagnostics.EventLog.WriteEntry("WWW", q, System.Diagnostics.EventLogEntryType.Warning);
                g.ProcessException(ex);
            }
        }



        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            this.Load += new System.EventHandler(this.Page_Load);
            this.Init += new System.EventHandler(this.Page_Init);

        }
        #endregion




        private void btnSearch_Click(object sender, System.EventArgs e)
        {
            try
            {

                string url = Request.RawUrl;

                if (url.IndexOf("StartRow=") > 0)
                {
                    int start = url.IndexOf("StartRow");
                    int end = url.IndexOf("&", start);
                    int len = url.Length;
                    url = url.Substring(0, start);

                }
                if (url.IndexOf("Postback") < 0)
                {
                    url += ((Request.RawUrl.IndexOf("?") >= 0) ? "&" : "?") + "Postback=1";
                }
                PhotoSearch.ValueObjects.PhotoGalleryQuery q = PhotoGalleryUtils.GetQueryFromSession(g.Brand, g.Member != null ? g.Member.MemberID : int.MinValue);

                q.ReloadFlag = PhotoSearch.ValueObjects.ReLoadFlags.none;
                PhotoGalleryUtils.SaveQueryToSession(q, g.Brand, g.Member != null ? g.Member.MemberID : int.MinValue);
                g.Transfer(url);
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }
    }
}