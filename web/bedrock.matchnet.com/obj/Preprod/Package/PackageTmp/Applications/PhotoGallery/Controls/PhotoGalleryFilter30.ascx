﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PhotoGalleryFilter30.ascx.cs" Inherits="Matchnet.Web.Applications.PhotoGallery.Controls.PhotoGalleryFilter30" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="cc1" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<div class="filter clearfix">
    <fieldset>
        <legend>Filter Photo Gallery results</legend>
	    <label class="view">
		    <mn:txt runat="server" id="txtShow" resourceconstant="TXT_SHOW" />:
		    <select id="GenderMask" runat="server"></select>
	    </label>
	    <label class="posted">
		    <mn:txt runat="server" id="txtPosted" resourceconstant="TXT_POSTED" />:
		    <select id="PostedDays" runat="server"></select>
	    </label>
	    <label class="from">
		    <mn:txt runat="server" id="txtFrom" resourceconstant="TXT_FROM" />:
		    <select id="RegionID" runat="server"></select>
	    </label>
	    <div class="age-container">
            <label class="age-from">
		        <mn:txt runat="server" id="txtAges" resourceconstant="TXT_AGE" />:
		        <input id="AgeMin" type="text" size="2" maxlength="2" runat="server" />
		      </label>
		      <label class="age-to">
		        &nbsp;<mn:txt runat="server" id="txtTo" resourceconstant="TO_" />
		        <input id="AgeMax" type="text" size="2" maxlength="2" runat="server" />
		      </label>
	    </div>
        <label class="sort">
            <mn:txt runat="server" ID="viewBy" ResourceConstant="TXT_BY" />
            <select id="SortBy" runat="server"></select>
        </label>
	
	    <label class="button">&nbsp;
		    <asp:Button runat="server" id="btnSearch" CssClass="btn secondary" onclientclick="updateFiltersAndLoadPhotoBatch(); return false;" />
	    </label>
    
        <asp:HiddenField runat="server" ID="hdnInitialGender"/>
        <asp:HiddenField runat="server" ID="hdnInitialPostedDays"/>
        <asp:HiddenField runat="server" ID="hdnInitialRegion"/>
        <asp:HiddenField runat="server" ID="hdnInitialMinAge"/>
        <asp:HiddenField runat="server" ID="hdnInitialMaxAge"/>
        <asp:HiddenField runat="server" ID="hdnInitialSortBy"/>
    </fieldset>	
</div>
<div id="divNoResults" runat="server" class="hide border-gen tips"><h2><mn:txt runat="server" id="txtNoResults" resourceconstant="NO_RESULTS" /></h2></div>

<script>
function updateFiltersAndLoadPhotoBatch() {
    var gender = $j('#<%=GenderMask.ClientID %>').val();
    var minAge = $j('#<%=AgeMin.ClientID %>').val();
    var maxAge = $j('#<%=AgeMax.ClientID %>').val();
    var postedDayRange = $j('#<%=PostedDays.ClientID %>').val();
    var regionId = $j('#<%=RegionID.ClientID %>').val();
    var sortBy = $j('#<%=SortBy.ClientID %>').val();
    //JAY: hide "no results" div
    galleryManager.updateFiltersAndLoadPhotoBatch(gender, minAge, maxAge, postedDayRange, regionId, sortBy);
}

function resetFilterOptions() {
    var gender = $j('#<%=hdnInitialGender.ClientID %>').val();
    var minAge = $j('#<%=hdnInitialMinAge.ClientID %>').val();
    var maxAge = $j('#<%=hdnInitialMaxAge.ClientID %>').val();
    var postedDayRange = $j('#<%=hdnInitialPostedDays.ClientID %>').val();
    var regionId = $j('#<%=hdnInitialRegion.ClientID %>').val();
    var sortBy = $j('#<%=hdnInitialSortBy.ClientID %>').val();

    $j('#<%=GenderMask.ClientID %>').val(gender);
    $j('#<%=AgeMin.ClientID %>').val(minAge);
    $j('#<%=AgeMax.ClientID %>').val(maxAge);
    $j('#<%=PostedDays.ClientID %>').val(postedDayRange);
    $j('#<%=RegionID.ClientID %>').val(regionId);
    $j('#<%=SortBy.ClientID %>').val(sortBy);
}

function updateInitialFilterOptions() {
    var gender = $j('#<%=GenderMask.ClientID %>').val();
    var minAge = $j('#<%=AgeMin.ClientID %>').val();
    var maxAge = $j('#<%=AgeMax.ClientID %>').val();
    var postedDayRange = $j('#<%=PostedDays.ClientID %>').val();
    var regionId = $j('#<%=RegionID.ClientID %>').val();
    var sortBy = $j('#<%=SortBy.ClientID %>').val();

    $j('#<%=hdnInitialGender.ClientID %>').val(gender);
    $j('#<%=hdnInitialMinAge.ClientID %>').val(minAge);
    $j('#<%=hdnInitialMaxAge.ClientID %>').val(maxAge);
    $j('#<%=hdnInitialPostedDays.ClientID %>').val(postedDayRange);
    $j('#<%=hdnInitialRegion.ClientID %>').val(regionId);
    $j('#<%=hdnInitialSortBy.ClientID %>').val(sortBy);
}

</script>