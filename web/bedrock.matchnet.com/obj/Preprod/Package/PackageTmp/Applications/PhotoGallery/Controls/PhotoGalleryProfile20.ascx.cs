﻿using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Web.Framework.Managers;
using Matchnet.Web.Framework.Ui;
#region Matchnet Web App References
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Util;
using Matchnet.Lib;
using Matchnet.PhotoSearch.ValueObjects;
using Matchnet.Member.ServiceAdapters;
#endregion

namespace Matchnet.Web.Applications.PhotoGallery.Controls
{
    public partial class PhotoGalleryProfile20 : FrameworkControl
    {
        const string STR_FORMAT_AGE = "{0} {1} {2}";
        const string STR_FORMAT_POSTED = "{0} {1} {2}";

        const string TOKEN_DIVID = "PG_ID";
        const string TOKEN_USERNAME = "PG_USERNAME";
        const string TOKEN_HASPHOTOS = "PG_HAS_PHOTOS";
        const string TOKEN_PHOTOSRC = "PG_PHOTO_SRC";

        //const string STR_DIV="<div id=profileLayer{0} style=position: absolute;></div>";
      

        int _age;
        string _ageStr = "";
        string _userName;
        string _photoThumbPath = "";
        string _photoPath = "";

        int _photoCount = 0;
        int _brandID;
        int _communityiID;
        int _siteID;

        int _ordinal = 0;

        int _days;
        bool _enableHover = false;
        public int row = 0;
        public int col = 0;
        Member.ServiceAdapters.Member _member = null;
        private PhotoResultItem _resultMember = null;
        private bool _isHighlighted = false;

        public Member.ServiceAdapters.Member GalleryMember
        {
            get { return _member; }
            set
            {
                _member = value;
            }
        }

        public PhotoResultItem ResultMember
        {
            get { return _resultMember; }
            set
            {
                _resultMember = value;

                if (FrameworkGlobals.memberHighlighted(this._resultMember.MemberID, g.Brand))
                {
                    this._isHighlighted = true;
                }
                else
                {
                    this._isHighlighted = false;
                }
            }
        }

        public bool IsHighlighted
        {
            get { return (_isHighlighted); }
            set { _isHighlighted = value; }
        }

        public int Ordinal
        {
            get { return (_ordinal); }
            set { _ordinal = value; }
        }
        private void Page_Load(object sender, System.EventArgs e)
        {
            try
            {

                if (_resultMember == null)
                    return;

                //Member.ServiceAdapters.Member member= MemberSA.Instance.GetMember(_resultMember.MemberID,Member.ServiceAdapters.MemberLoadFlags.None);

                _siteID = g.Brand.Site.SiteID;
                _brandID = g.Brand.BrandID;
                _communityiID = g.Brand.Site.Community.CommunityID;

                DateTime bod = _resultMember.GalleryMember.BirthDate;
                _userName = _resultMember.GalleryMember.UserName;
                _ageStr = FrameworkGlobals.GetAgeCaption(bod, g, this, "AGE_UNSPECIFIED");


                double hrs = DateTime.Now.Subtract(_resultMember.PhotoInsertDate).TotalHours;
                int hrs24 = (int)hrs / 24;
                int remhrs = (int)hrs % 24;

                //_days=DateTime.Now.Subtract(_resultMember.PhotoInsertDate ).Days;
                _days = hrs24 + (remhrs > 0 ? 1 : 0);
                if (hrs24 == 0 && (_resultMember.PhotoInsertDate.Date == DateTime.Today.Date && _resultMember.PhotoInsertDate.Month == DateTime.Today.Month && _resultMember.PhotoInsertDate.Year == DateTime.Today.Year))
                {
                    //leave like it - it's today
                }
                else
                {
                    hrs24 += 1;
                }
                _ageStr = String.Format(STR_FORMAT_AGE, FrameworkGlobals.GetUnicodeText(g.GetResource("AGE_STR_1", this)), _ageStr, FrameworkGlobals.GetUnicodeText(g.GetResource("AGE_STR", this)));

                string posted = "";
                if (hrs24 != 0)
                {
                    if (_days == 1 && g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.JDateFR)
                    {
                        posted = String.Format(STR_FORMAT_POSTED, FrameworkGlobals.GetUnicodeText(g.GetResource("POSTED_1", this)), _days, FrameworkGlobals.GetUnicodeText(g.GetResource("POSTED_2_ONE_DAY", this)));        
                    }
                    else
                    {
                        posted = String.Format(STR_FORMAT_POSTED, FrameworkGlobals.GetUnicodeText(g.GetResource("POSTED_1", this)), _days, FrameworkGlobals.GetUnicodeText(g.GetResource("POSTED_2", this)));        
                    }
                }
                    
                else
                    posted = String.Format(STR_FORMAT_POSTED, FrameworkGlobals.GetUnicodeText(g.GetResource("POSTED_TODAY", this)), "", "");


                lnkUserName.Text = FrameworkGlobals.Ellipsis(_userName, 12);
                if (this._isHighlighted)
                {
                    lnkUserName.NavigateUrl = BreadCrumbHelper.MakeViewProfileLink(BreadCrumbHelper.EntryPoint.PhotoGallery, _resultMember.MemberID, _ordinal, null, Constants.NULL_INT, false, (int)BreadCrumbHelper.PremiumEntryPoint.Highlight);
                }
                else
                {
                    lnkUserName.NavigateUrl = BreadCrumbHelper.MakeViewProfileLink(BreadCrumbHelper.EntryPoint.PhotoGallery, _resultMember.MemberID,_ordinal,null,Constants.NULL_INT);
                }
                txtPosted.Text = posted;
                txtAge.Text = _ageStr;


               
                _photoThumbPath = _resultMember.GalleryMember.ThumbWebPath;
                _photoPath = _resultMember.GalleryMember.FileWebPath;


                _photoCount = _resultMember.GalleryMember.PhotoCount;
                if (_photoThumbPath != null && _photoThumbPath != "")
                    imgThumb.ImageUrl = _photoThumbPath;
                else
                {
                    bool isMale = FrameworkGlobals.IsMaleGender(_resultMember.GalleryMember.MemberID, g);
                    //MemberPhotoHelper photoHelper = new MemberPhotoHelper(RuntimeSettings.Instance);
                    imgThumb.ImageUrl = MemberPhotoDisplayManager.Instance.GetNoPhotoFile(NoPhotoImageType.Thumb, isMale);
                }


                if (this._isHighlighted)
                {
                    imgThumb.NavigateUrl = BreadCrumbHelper.MakeViewProfileLink(BreadCrumbHelper.EntryPoint.PhotoGallery, _resultMember.MemberID, _ordinal, null, Constants.NULL_INT, false, (int)BreadCrumbHelper.PremiumEntryPoint.Highlight);
                }
                else
                {
                    imgThumb.NavigateUrl = BreadCrumbHelper.MakeViewProfileLink(BreadCrumbHelper.EntryPoint.PhotoGallery, _resultMember.MemberID, _ordinal, null, Constants.NULL_INT);
                }
                _enableHover = Boolean.Parse(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("PHOTOGALLERY_HOVER_ENABLE_FLAG", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID));


                if (_enableHover)
                {
                    int bannerscount = 0;
                    System.Diagnostics.Trace.Write("hover over enabled");

                    RenderDiv();
                    if (g.Brand.Site.LanguageID != (int)Language.Hebrew)
                        imgThumb.Attributes.Add("onMouseOver", "showProfileInfo(event, this, 'profileLayer_" + _resultMember.MemberPhotoID.ToString() + "'," + row.ToString() + "," + col.ToString() + "," + bannerscount + ");");
                    else
                        imgThumb.Attributes.Add("onMouseOver", "showProfileInfoRTL(event, this, 'profileLayer_" + _resultMember.MemberPhotoID.ToString() + "'," + row.ToString() + "," + col.ToString() + "," + bannerscount + ");");

                    imgThumb.Attributes.Add("onMouseOut", "hideProfileInfo('profileLayer_" + _resultMember.MemberPhotoID.ToString() + "');");
                }

                //if (FrameworkGlobals.memberHighlighted(_resultMember.MemberID, g.Brand))     
                if (this._isHighlighted)
                {
                    divPhotoGallery.Attributes.Remove("class");
                    divPhotoGallery.Attributes.Add("class", "results photo-view highlight");

                    ucHighlightProfileInfoDisplay.MemberHighlighted = true;
                    ucHighlightProfileInfoDisplay.ViewedMemberID = _resultMember.MemberID;
                }
                else
                {
                    ucHighlightProfileInfoDisplay.Visible = false;
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.Write("exception:" + ex.Message);
            }
        }


        private void RenderDiv()
        {
            string hasPhotos = "&nbsp;";


            if (_photoCount > 1)
            {
                string hasPhoto1 = g.GetResource("HAS_PHOTOS_1", this);
                string hasPhoto2 = g.GetResource("HAS_PHOTOS_2", this);
                string count = _photoCount.ToString();
                if (hasPhoto1 == "" && hasPhoto2 == "")
                { count = ""; }
                hasPhotos = String.Format("{0}&nbsp;{1}&nbsp;{2}", g.GetResource("HAS_PHOTOS_1", this), count, g.GetResource("HAS_PHOTOS_2", this));
            }
            g.AddExpansionToken(TOKEN_DIVID, _resultMember.MemberPhotoID.ToString());
            g.AddExpansionToken(TOKEN_USERNAME, _userName);
            g.AddExpansionToken(TOKEN_HASPHOTOS, hasPhotos);
            g.AddExpansionToken(TOKEN_PHOTOSRC, _photoPath);
            string divHTML = g.GetResource("DIV_HTML", this);
            profileHoverUp.Text = divHTML;
        }
        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }
        #endregion
    }
}