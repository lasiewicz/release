<%@ Control Language="c#" AutoEventWireup="false" Codebehind="Events.ascx.cs" Inherits="Matchnet.Web.Applications.Events.Events" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<%@ Register TagPrefix="uc1" TagName="EventTopNav" Src="EventTopNav.ascx" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<div id="rightNew">
	<mn:Txt id="adAllEventsPageTop" ResourceConstant="AD_ALL_EVENTS_PAGE_TOP" runat="server" ExpandImageTokens="true" />
	<mn:Txt id="txtEventsNav" ResourceConstant="TXT_EVENTS_NAV" runat="server" ExpandImageTokens="true" />
    
    <mn:Txt id="txtEventsIframeLink" ResourceConstant="TXT_EVENTS_IFRAME_LINK" runat="server" ExpandImageTokens="true" />
</div>

<script type="text/javascript"> 
 	function Launch(page) { 
		OpenWin = this.open(page, "CtrlWindow", "toolbar=no,menubar=no,location=no,scrollbars=no,resizable=no,width=360,height=350"); 
	} 
</script>