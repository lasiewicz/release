﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BBEPromo.ascx.cs" Inherits="Matchnet.Web.Applications.Events.BBEPromo" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>

<link rel="stylesheet" type="text/css" href="/css/bbe-print.css" media="print" />
<div class="bbe">
<%--	<h1 class="logo"><a href="#" title="JDate.com" class="spr s-bbe-logo-jd">JDate.com</a> <sub class="city spr s-bbe-city-la">Los Angeles</sub></h1>
        <blockquote class="catch-phrase">
		<big class="spr s-bbe-txt-catch-phrase">We review, you eat.</big>
		<small class="spr s-bbe-txt-tag-line">Places we love with insider specials</small>
	</blockquote>--%>
	<div class="intro">
		<h2>Thank you! We hope you enjoy our exclusive deals, handpicked for your palate.</h2>
		<p class="tag-line">Remember: to get the special below, all you need to do is request a passcode.</p>
	</div>
	<hr />
	<div class="header">
		<h5 class="spr s-bbe-txt-special">Today's Special</h5>
        <p class="emphasis-one">Brought to you by JDate and BlackboardEats<sup>&reg;</sup></p>
		<h2>Ago</h2>	
		<h3 class="tag-line"><big>30% off dinner</big> + Exclusive JDate perk: complimentary glass of wine or moscato per person (choice of any wine by the glass) or choice of any dessert per person.</h3>
	</div>
    <hr />
	<div class="details clearfix">
		<div class="main">
			
            <div class="cta">
				<button type="button" id="cta" class="spr s-bbe-btn-cta">Get your free passcode</button>
                <em class="emphasis-two">Hurry, get your free passcode by 1:00p.m. Saturday Dec. 8!</em>
			</div>

            <div class="passcode available" style="display:none;">
                <p>Don't worry; we'll also email this passcode to you!</p>
                <blockquote>
                    <big><span class="emphasis-one">Passcode:</span> <span class="passcode-value"></span></big>
                    <small>Special expires at close of business day on: <em class="emphasis-two">Mar. 6, 2013</em></small>
                </blockquote>
                <span class="spr s-bbe-icon-printer"></span><a href="#" title="Print Passcode" class="print" id="printPasscode">Print Passcode</a>
            </div>

            <div class="passcode unavailable" style="display:none;">
                <blockquote>
                    Sorry passcodes are no longer available for this deal.
                </blockquote>
            </div>

            <h3 class="store-name">Ago</h3>
            <ul class="store-info">
                <li><a href="https://maps.google.com/maps?ie=UTF-8&q=ago+west+hollywood&fb=1&gl=us&hq=ago&hnear=0x80c2bed921f033e3:0x50e44c2d0e8a26a7,West+Hollywood,+CA&cid=0,0,15109642331482253638&ei=02uqULSjCYPKiwKo3YGoCA&ved=0CKIBEPwSMAA" target="_blank" class="address">8478 Melrose Ave (between Clinton St. and N. Croft Ave.), <br />West Hollywood, CA 90069</a></li>
                <li>(323) 655-6333</li>
                <li><a href="http://agorestaurant.com" target="_blank" title="www.agorestaurant.com">www.agorestaurant.com</a></li>
                <li>Menu: <a href="http://agorestaurant.com/insalate/" target="_blank" title="www.agorestaurant.com/insalate/">www.agorestaurant.com/insalate/</a></li>
                <li style="font-size:13px;">Mon-Wed 12pm - 11pm; Thu-Fri 12pm - 11:30pm; Sat 6pm - 11:30pm; Sun 6pm - 10pm</li>
                <li>Price range: $24 (spinach and ricotta cheese ravioli) to $48 (Costata di Manzo)</li>
            </ul>

            <hr />

            <div class="review">
                <em class="author">By Valaer Murray</em>
                <img src="/img/Community/JDate/bbe-vendor-01.jpg" alt="Gusto" class="float-inside" />
                <p>As classic as a martini, Ago is the epitome of LA glamour, though it is and cozier than you'd think. Luxe leather banquettes and polished service remind you that you're in for a special night out while the food's simple execution with high-quality ingredients and a cheerful bar, reveal a neighborhood joint's charm-in that Old Hollywood way, of course. After all, the place is owned by Robert De Niro, who schools ballers in proper manners. The kitchen stays true to the classic trattoria experience, offering antipasto like burrata, which you've likely seen served every which way this year, but Ago's scoop of velvety creaminess with cold haricots vert and diced tomato has a sepia effect-this is how your Italian grandma would serve it. She would also discourage skimping on a traditional primo course, though the kitchen here will accommodate diners splitting pasta or risotto by serving separate portions. This is a good idea, too, because the mains are stellar, from the lightly charred rack of lamb served au jus alongside crisp rosemary potatoes to the whole fish, baked and filleted tableside with flourish and a flashlight-this is, after all, a romantic date spot.</p>
                
                <h4>menu musts</h4>
                <ul class="description">
                    <li>Polpette di carne</li>
                    <li>Burrata</li>
                    <li>Linguine vongole</li>
                    <li>Risotto al funghi</li>
                    <li>Agnello di latte al forno</li>
                    <li>Torta della Nonna</li>
                </ul>

                <h4>the crowd</h4>
                <p>Buttoned-up and well-heeled, but no flashiness required.</p>

                <h4>date Night Note</h4>
                <p>When asked "inside or outside?" know that outside means a twinkling, covered patio.</p>

                <h4>decibels</h4>
                <p>Medium</p>

            </div><%-- /.review --%>
        
        </div><%-- /.main --%>
		
        <div class="aside">
			<img src="/img/Community/JDate/bbe-vendor-02.jpg" alt="A bowl of pasta" />
            <img src="/img/Community/JDate/bbe-vendor-03.jpg" alt="Gusto menu" />
            <h3>The Fine Print</h3>
            <p>Valid from December 6th 2012 - March 6th 2013</p>
            <p>Deal Details: Not valid Dec 24, Dec 25, Dec 31, Jan 1, Feb 14; valid for dinner nightly; minimum purchase of one entree ("Paste e Risotti" or "Carne e Pesce") per person; valid for parties of 2; dine-in only; not valid on Chef Agostino's Wednesday night Tasting Menu; not valid with any other discount or promotion; reservations strongly suggested</p>
		</div>
		
	</div>
</div>

<script type="text/javascript">

    function BBEManager() {
        this.memberID = 0;
        this.isVerbose = false;
        this.updateInProgress = false;
        this.isExpired = false;
        this.ShowPasscode = function (passcode) {
            if (passcode != ''){
                $j('.due, div.cta', '.bbe').hide();
                $j('.passcode.available', '.bbe').find('.passcode-value').text(passcode).end().show();
            }
        }
        this.ShowExpired = function(){
            $j('div.cta', '.bbe').hide();
            $j('.passcode.unavailable', '.bbe').show();
        }
        this.ProcessBBEPasscodeRequest = function () {
            if (!bbeManager.updateInProgress) {                
                $j.ajax({
                    type: "POST",
                    url: "/Applications/API/Member.asmx/ProcessBBEPasscodeRequest",
                    data: "{memberID:" + this.memberID + "}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    beforeSend: function () {
                        bbeManager.updateInProgress = true;
                        //show ajax loader
                    },
                    complete: function () {
                        bbeManager.updateInProgress = false;
                        //remove ajax loader
                    },
                    success: function (result) {
                        var updateResult = (typeof result.d == 'undefined') ? result : result.d;
                        //console.log(updateResult);
                        if (updateResult.Status != 2) {
                            //error
                            if (bbeManager.isVerbose) {
                                alert(updateResult.StatusMessage);
                            }
                        }
                        else{
                            if (updateResult.BBEExpired){
                                //show expired message
                                bbeManager.ShowExpired();
                            }
                            else{
                                //show passcode
                                var passcode = updateResult.BBEPasscode;
                                bbeManager.ShowPasscode(passcode);
                            }
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        if (bbeManager.isVerbose) {
                            alert(textStatus);
                        }
                    }
                });
            }
        }
    }

    //initialize manager
    var bbeManager = new BBEManager();
    bbeManager.isVerbose = true;
    bbeManager.memberID = <%=ProfileMemberID%>;
    bbeManager.isExpired = <%=IsPromoExpired.ToString().ToLower()%>;

    // Temorary JS - Demonstration purpose
    $j('.s-bbe-btn-cta', '.bbe').on('click', function () {
        //bbeManager.ShowPasscode('Gusto111');
        //bbeManager.ShowExpired();
        bbeManager.ProcessBBEPasscodeRequest();
    });

    // Print passcode
    $j('#printPasscode').click(function (e) {
        e.preventDefault();
        window.print();
        return false;
    });

    if (bbeManager.isExpired){
        //show expired message
        bbeManager.ShowExpired();
    }
    else{
        // Show passcode if exists
        bbeManager.ShowPasscode('<%=Passcode%>');
    }

</script>
