using System;
using System.Web.UI.WebControls;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.Events;
using Matchnet.Web.Framework;

namespace Matchnet.Web.Applications.Events
{
	/// <summary>
	/// Summary description for EventView.
	/// </summary>
	public class EventView : FrameworkControl
	{
		protected Literal EventContent;

		private void Page_Load(object sender, System.EventArgs e)
		{
			try
			{
				int eventId = Convert.ToInt32(Request.QueryString["EventID"]);

				Event oEvent = EventSA.Instance.GetEvent(eventId);

				if (null == oEvent.Content || oEvent.Content.Length == 0)
				{
					EventContent.Text = "Not found";
				}
				else
				{
					EventContent.Text = oEvent.Content;
				}
			}
			catch(Exception ex)
			{
				g.ProcessException(ex);
			}		
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
