using System;

#region Matchnet Web App References
using Matchnet.Web.Framework;
#endregion

#region Matchnet Service References
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.Events;
#endregion

namespace Matchnet.Web.Applications.Events
{
    public class Events : FrameworkControl
    {
        protected System.Web.UI.WebControls.Repeater rptEvents;
        protected System.Web.UI.WebControls.Repeater rptEventPhotos;
        protected Matchnet.Web.Framework.Txt adAllEventsPageTop;

        const string TOP_BANNER_VISITOR = "AD_ALL_EVENTS_PAGE_TOP_VISITOR";
        const string TOP_BANNER_REGISTRANT = "AD_ALL_EVENTS_PAGE_TOP_REGISTRANT";
        const string TOP_BANNER_SUBSCRIBER = "AD_ALL_EVENTS_PAGE_TOP_SUBSCRIBER";

        private void Page_Load(object sender, System.EventArgs e)
        {
            try
            {
                string resourceConst = "";
                if (g.Member == null)
                { resourceConst = TOP_BANNER_VISITOR; }
                else
                {
                    //g.AddExpansionToken("MEMBERIDURLPARAM",_g.Member.MemberID.ToString());
                    if (g.Member.IsPayingMember(g.Brand.Site.SiteID))
                    { resourceConst = TOP_BANNER_SUBSCRIBER; }
                    else
                    { resourceConst = TOP_BANNER_REGISTRANT; }
                }

                adAllEventsPageTop.ResourceConstant = resourceConst;

                g.SetPageTitle(g.GetResource("TXT_PAGE_TITLE", this));

                if (g.BreadCrumbTrailHeader != null)
                {
                    g.BreadCrumbTrailHeader.SetTwoLinkCrumb(g.GetResource("TXT_EVENTS_NAV_BREAD_CRUMB", this),
                                                            g.AppPage.App.DefaultPagePath);

                }
                if (g.BreadCrumbTrailFooter != null)
                {
                    g.BreadCrumbTrailFooter.SetTwoLinkCrumb(g.GetResource("TXT_EVENTS_NAV_BREAD_CRUMB", this),
                                                           g.AppPage.App.DefaultPagePath);
                }
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        private void LoadEventList()
        {
            try
            {
                int eventTypeId = Matchnet.Conversion.CInt(Request.QueryString["EventTypeId"]);
                Matchnet.Content.ValueObjects.Events.EventCollection oEvents = new EventCollection();

                //Issue #14684: default to 1 (parties) since Photo Albums is being removed
                if (eventTypeId == Constants.NULL_INT)
                {
                    eventTypeId = 1;
                }

                if (eventTypeId != Matchnet.Constants.NULL_INT)
                {
                    oEvents = EventSA.Instance.GetEvents(g.Brand.Site.Community.CommunityID, (Matchnet.Content.ValueObjects.Events.Event.EVENT_TYPE)eventTypeId);
                    rptEvents.DataSource = oEvents;
                    rptEvents.DataBind();
                    rptEvents.Visible = true;
                    rptEventPhotos.Visible = false;
                }
                else
                {
                    oEvents = EventSA.Instance.GetAlbumEvents(g.Brand.Site.Community.CommunityID);
                    rptEventPhotos.DataSource = oEvents;
                    rptEventPhotos.DataBind();
                    rptEventPhotos.Visible = true;
                    rptEvents.Visible = false;
                }
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);

        }
        #endregion
    }
}
