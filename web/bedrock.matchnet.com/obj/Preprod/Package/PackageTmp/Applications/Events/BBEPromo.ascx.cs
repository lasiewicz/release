﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Managers;

namespace Matchnet.Web.Applications.Events
{
    public partial class BBEPromo : FrameworkControl
    {
        public int ProfileMemberID = 0;
        public string Passcode = "";
        public bool IsPromoExpired = false;
        public const string URL_BBE_SCENARIO = "bbescenario";

        public override void AppInit()
        {
            //Do not display sub now banner on this page
            g.HideSubNowBanner = true;
        }

        protected void Page_Load(object sender, EventArgs e)
        {           
            //registered members only and needs to be logged in
            if (g.Member == null)
            {
                g.LogonRedirect(HttpContext.Current.Request.RawUrl.ToString(), null);
            }
            else if (!BBEManager.Instance.IsBlackBoardEatsPromotionEnabled(g.Brand))
            {
                g.Transfer("/Default.aspx");
            }
            else
            {
                ProfileMemberID = g.Member.MemberID;
                bool autoPasscode = false;
                if (!String.IsNullOrEmpty(Request[URL_BBE_SCENARIO]))
                {
                    if (Request[URL_BBE_SCENARIO].Trim() == "2")
                    {
                        autoPasscode = true;
                    }
                }

                BBE bbe = BBEManager.Instance.GetMemberDataBBE(g.Member, g.Brand);
                DateTime bbeGustoOldPromoDate = new DateTime(2012, 11, 15);
                if (autoPasscode)
                {
                    //automated opt-in and passcode request scenario
                    if (bbe == null || string.IsNullOrEmpty(bbe.ViewedPromoDate)
                                    || string.IsNullOrEmpty(bbe.PasscodeRequestDate)
                                    || (Convert.ToDateTime(bbe.ViewedPromoDate) < bbeGustoOldPromoDate))
                    {
                        //record user has viewed bbe promo, opt-in and generate passcode
                        bbe = new BBE();
                        bbe = BBEManager.Instance.ProcessBBEPasscodeRequest(g.Member, g.Brand, g.Member.EmailAddress, bbe);
                    }
                    else if (string.IsNullOrEmpty(bbe.Passcode) && BBEManager.Instance.GetBBEPromotionExpirationDate(g.Brand) > DateTime.Now)
                    {
                        //this will happen if we decided to extend the promotion expiration date after user has viewed promo
                        //generate passcode, leave existing dates as is
                        bbe = BBEManager.Instance.ProcessBBEPasscodeRequest(g.Member, g.Brand, g.Member.EmailAddress, bbe);
                    }

                    if (string.IsNullOrEmpty(bbe.Passcode))
                    {
                        //promotion expired
                        Passcode = "";
                        IsPromoExpired = true;
                    }
                    else
                    {
                        //set passcode
                        Passcode = bbe.Passcode;
                    }
                }
                else
                {
                    //opt-in with separate passcode request button scenario
                    if (bbe == null || (Convert.ToDateTime(bbe.ViewedPromoDate) < bbeGustoOldPromoDate))
                    {
                        //record user has viewed bbe promo
                        bbe = new BBE();
                        bbe.ViewedPromoDate = DateTime.Now.ToString();
                        BBEManager.Instance.SaveMemberDataBBE(g.Member, g.Brand, bbe);
                    }
                    else
                    {
                        //set existing passcode
                        Passcode = bbe.Passcode;
                    }
                }
            }
        }
    }
}
