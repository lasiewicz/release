#region System References
using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
#endregion

#region Matchnet Web App References
using Matchnet.Web.Framework;
#endregion

namespace Matchnet.Web.Applications.Events
{

	public class EventTopNav : FrameworkControl
	{
		protected HtmlTableCell tdEventsParties;
		protected HtmlTableCell tdEventsPartiesOn;
		protected HtmlTableCell tdTravelAdventures;
		protected HtmlTableCell tdTravelAdventuresOn;
		protected HtmlTableCell tdEventsPhotoAlbums;
		protected HtmlTableCell tdEventsPhotoAlbumsOn ;
		protected HtmlTableCell tdEventsFAQ;
		protected HtmlTableCell tdEventsFAQOn;
		protected HtmlTableCell tdEventsQuestions;
		protected Matchnet.Web.Framework.Txt txtEventsParties;
		protected Matchnet.Web.Framework.Txt txtEventsPartiesOn;
		protected Matchnet.Web.Framework.Txt txtTravelAdventures;
		protected Matchnet.Web.Framework.Txt txtTravelAdventuresOn;
		protected Matchnet.Web.Framework.Txt txtEventsQuestions;
		protected Matchnet.Web.Framework.Txt txtEventsQuestionsOn;
		protected HtmlTableCell tdEventsQuestionsOn;

		private void Page_Load(object sender, System.EventArgs e)
		{
			try
			{
				//this could be a lot smarter but should work for now
				base.ViewState.Clear();
			
				if (Request.QueryString["EventTypeId"] != null)
				{
					int EventTypeId = Convert.ToInt32(Request.QueryString["EventTypeId"]);
					switch (EventTypeId)
					{
						case 1:
							tdEventsParties.Visible = false;
							tdEventsPartiesOn.Visible = true;
							break;
						case 2:
							tdTravelAdventures.Visible = false;
							tdTravelAdventuresOn.Visible = true;
							break;
					}
				}
				else
				{
					string sUrl = Page.Request.Url.ToString();
					if (sUrl.IndexOf("eventfaq") > 0)
					{
						tdEventsFAQ.Visible = false;
						tdEventsFAQOn.Visible = true;
					}
					if (sUrl.IndexOf("eventcontact") > 0)
					{
						tdEventsQuestions.Visible = false;
						tdEventsQuestionsOn.Visible = true;
					}
					if (sUrl.IndexOf("events") > 0)
					{
						tdEventsPhotoAlbums.Visible  = false;
						tdEventsPhotoAlbumsOn.Visible = true;
					}
				}

				if (g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.JDateCoIL) 
				{
					tdEventsFAQ.Visible = false;
					tdEventsFAQOn.Visible = false;
					tdEventsPhotoAlbums.Visible = false;
					tdEventsPhotoAlbumsOn.Visible = false;
				}

				//Issue #14684.  They want to remove the photo albums link completely.  I have a feeling that
				//it will be added back at some point, so I'm just making it invisible for now.
				tdEventsPhotoAlbums.Visible  = false;
				tdEventsPhotoAlbumsOn.Visible = false;
			}
			catch (Exception ex)
			{
				g.ProcessException(ex);
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
	}
}
