﻿using System;
using Spark.Common.AccessService;
using Matchnet.Web.Framework;

namespace Matchnet.Web.Applications.Omnidate
{
    public enum OmnidateUserClass
    {
        NonSubscriber = 1,
        Subscriber = 2,
        Premium = 3
    }

    public class OmnidateHelper
    {

        public static OmnidateUserClass GetOmnidateUserClass(ContextGlobal g)
        {
            var result = OmnidateUserClass.NonSubscriber;
            var privdate = g.Member.GetUnifiedAccessPrivilege(PrivilegeType.AllAccess, g.Brand.BrandID, g.Brand.Site.SiteID, g.Brand.Site.Community.CommunityID);
            if (privdate != null && privdate.EndDatePST > DateTime.Now)
            {
                result = OmnidateUserClass.Premium;
            }
            else
            {
                privdate = g.Member.GetUnifiedAccessPrivilege(PrivilegeType.BasicSubscription, g.Brand.BrandID, g.Brand.Site.SiteID, g.Brand.Site.Community.CommunityID);
                if (privdate != null && privdate.EndDatePST > DateTime.Now)
                {
                    result = OmnidateUserClass.Subscriber;
                }
            }

            return result;
        }
    }
}
