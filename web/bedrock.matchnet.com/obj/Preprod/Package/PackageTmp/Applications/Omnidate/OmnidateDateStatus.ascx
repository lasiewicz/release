﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OmnidateDateStatus.ascx.cs"
    Inherits="Matchnet.Web.Applications.Omnidate.OmnidateDateStatus" %>
<asp:PlaceHolder runat="server" ID="plcInvitationIssuedOmniture" Visible="false">

    <script type="text/javascript">
        var s_account = '<%=s_AccountName %>';
    </script>

    <script type="text/javascript" src="/Analytics/Javascript/Omniture.js"></script>

    <script type="text/javascript">
        var s = s_gi(s_account); s.linkTrackVars = 'events';
        s.linkTrackEvents = '<%=Event %>';
        s.events = '<%=Event %>';
        s.eVar27 = '<%=EVar27 %>';
        s.t(); 
    </script>

</asp:PlaceHolder>
