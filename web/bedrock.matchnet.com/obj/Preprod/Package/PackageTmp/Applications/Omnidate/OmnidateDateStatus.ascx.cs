﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Content.ValueObjects.Quotas;
using Matchnet.Email.ServiceAdapters;
using Matchnet.Email.ValueObjects;
using Matchnet.ExternalMail.ServiceAdapters;
using Matchnet.List.ServiceAdapters;
using Matchnet.List.ValueObjects;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Managers;

namespace Matchnet.Web.Applications.Omnidate
{
    public partial class OmnidateDateStatus : FrameworkControl
    {
        private const string PASSWORD = "ine7l4iicc";
        private const string INVITER_URL_PARAMETER = "userid1";
        private const string INVITEE_URL_PARAMETER = "userid2";
        private const string EVENT_URL_PARAMETER = "action";
        private const string PASSWORD_URL_PARAMETER = "password";

        private string _event;
        private string _evar27;
        private string _eventMessage;

        protected string Event
        {
            get { return _event; }
        }

        protected string EVar27
        {
            get { return _evar27; }
        }

        protected string EventMessage
        {
            get { return _eventMessage; }
        }

        protected string s_AccountName
        {
            get
            {
                return "spark" + _g.Brand.Site.Name.Replace(".", string.Empty);
            }
        }



        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(Request.QueryString[PASSWORD_URL_PARAMETER]))
                {
                    if (Request.QueryString[PASSWORD_URL_PARAMETER] == PASSWORD)
                    {
                        if (!string.IsNullOrEmpty(Request.QueryString[INVITER_URL_PARAMETER]) &&
                            !string.IsNullOrEmpty(Request.QueryString[INVITEE_URL_PARAMETER]) &&
                            !string.IsNullOrEmpty(Request.QueryString[EVENT_URL_PARAMETER]))
                        {
                            int inviterMemID;
                            int inviteeMemID;
                            int eventID;
                            if (int.TryParse(Request.QueryString[INVITER_URL_PARAMETER], out inviterMemID) &&
                               int.TryParse(Request.QueryString[INVITEE_URL_PARAMETER], out inviteeMemID) &&
                               int.TryParse(Request.QueryString[EVENT_URL_PARAMETER], out eventID))
                            {
                                Matchnet.Member.ServiceAdapters.Member inviter = MemberSA.Instance.GetMember(inviterMemID, MemberLoadFlags.None);
                                Matchnet.Member.ServiceAdapters.Member invitee = MemberSA.Instance.GetMember(inviteeMemID, MemberLoadFlags.None);

                                if (inviter != null && invitee != null)
                                {
                                    switch (eventID)
                                    {
                                        case 0:  //Invitation Issued
                                            plcInvitationIssuedOmniture.Visible = true;
                                            _event = "event55";
                                            if (FrameworkGlobals.SearchResultsM2MUpgradeEnabled(g))
                                            {
                                                string viewMode = "Gallery View)";
                                                if (!string.IsNullOrEmpty(Request.QueryString["ViewMode"]) &&
                                                    Request.QueryString["ViewMode"] == "list")
                                                {
                                                    viewMode = "List View)";
                                                }

                                                _evar27 = "Omnidate (Match_Results - ";
                                                if (g.AppPage.ControlName.ToLower() == "membersonline20" &&
                                                    MembersOnlineManager.Instance.IsMOLRedesign30Enabled(g.Member,
                                                        g.Brand))

                                                {
                                                    _evar27 = "Omnidate (MOL - ";
                                                }

                                                _evar27 += viewMode;
                                            }
                                            _eventMessage = "Click to Initiate OmniDate_55";
                                            Response.Write("Invitation Issued success");
                                            break;
                                        case 1: //Invitation Accepted
                                            plcInvitationIssuedOmniture.Visible = true;
                                            _event = "event56";
                                            _eventMessage = "Click to Accept OmniDate_56";
                                            UpdateHotLists(inviterMemID, inviteeMemID);
                                            Response.Write("Invitation Accepted success");
                                            break;
                                        case 4: //c
                                            SendMissedOmnidateMessage(inviter, invitee);
                                            Response.Write("//Invitation Expired success");
                                            break;
                                        default:
                                            break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
                g.ProcessException(ex);
            }


        }



        /// <summary>
        /// Updates the hot lists.
        /// </summary>
        /// <param name="inviterMemID">The inviter mem ID.</param>
        /// <param name="inviteeMemID">The invitee mem ID.</param>
        private void UpdateHotLists(int inviterMemID, int inviteeMemID)
        {
            ListSA.Instance.AddListMember(HotListCategory.WhoOmnidatedYou,
                       g.Brand.Site.Community.CommunityID,
                       g.Brand.Site.SiteID,
                       inviteeMemID,
                       inviterMemID,
                       null,
                       Constants.NULL_INT,
                       true,
                       true);

            ListSA.Instance.AddListMember(HotListCategory.MembersYouOmnidated,
                g.Brand.Site.Community.CommunityID,
                g.Brand.Site.SiteID,
                inviterMemID,
                inviteeMemID,
                null,
                Constants.NULL_INT,
                true,
                true);


        }

        /// <summary>
        /// Sends the missed omnidate message.
        /// </summary>
        /// <param name="inviter">The inviter.</param>
        /// <param name="invitee">The invitee.</param>
        private void SendMissedOmnidateMessage(Matchnet.Member.ServiceAdapters.Member inviter, Matchnet.Member.ServiceAdapters.Member invitee)
        {
            string subject = g.GetResource("OMNIDATE_MESSAGE_MISSED_SUBJECT", this);
            StringDictionary expansionTokens = new StringDictionary();
            expansionTokens.Add("USERNAME", inviter.GetUserName(g.Brand));
            string body = g.GetResource("OMNIDATE_MESSAGE_MISSED_BODY", this, expansionTokens);

            //string atQuotaErrorSubject = g.GetResource("AT_QUOTA_ERROR_SUBJECT", this);
            //string atQuotaErrorMessage = g.GetResource("AT_QUOTA_ERROR_MESSAGE", this, expansionTokens);

            MessageSave msg = new MessageSave(inviter.MemberID, invitee.MemberID, g.Brand.Site.Community.CommunityID, MailType.MissedOmnidate, subject,
                                              body);
            bool _saveInSent = false;
            AttributeOptionMailboxPreference mailboxMask = (AttributeOptionMailboxPreference)inviter.GetAttributeInt(g.Brand, WebConstants.ATTRIBUTE_NAME_MAILBOXPREFERENCE);
            if ((mailboxMask & AttributeOptionMailboxPreference.SaveCopiesOfSentMessages) == AttributeOptionMailboxPreference.SaveCopiesOfSentMessages)
            {
                _saveInSent = true;
            }

            if (QuotaSA.Instance.IsAtQuota(msg.GroupID, msg.FromMemberID, QuotaType.MissedOmnidate))
            {

                //msg = GetQuotaErrorMessage(invitee.Username, msg,
                //    expansionTokens,
                //    atQuotaErrorSubject,
                //    atQuotaErrorMessage);

            }
            else
            {
                QuotaSA.Instance.AddQuotaItem(msg.GroupID, msg.FromMemberID, QuotaType.MissedOmnidate);

                MessageSendResult messageSendResult = EmailMessageSA.Instance.SendMessage(msg, _saveInSent,
                                                                                     Constants.NULL_INT, false);
                if (messageSendResult.Status == MessageSendStatus.Success)
                {
                    ExternalMailSA.Instance.SendInternalMailNotification(messageSendResult.EmailMessage, g.Brand.BrandID,
                                                                         messageSendResult.RecipientUnreadCount);
                }
            }
        }

        private MessageSave GetQuotaErrorMessage(string p, MessageSave originalMessageSave, StringDictionary expansionTokens, string atQuotaErrorSubject, string atQuotaErrorMessage)
        {
            expansionTokens.Add("ORIGINALMESSAGE", originalMessageSave.MessageBody);

            return new MessageSave(originalMessageSave.FromMemberID,
                originalMessageSave.FromMemberID,
                originalMessageSave.GroupID,
                g.Brand.Site.SiteID,
                originalMessageSave.MailType,
                atQuotaErrorSubject,
                atQuotaErrorMessage);
        }
    }
}