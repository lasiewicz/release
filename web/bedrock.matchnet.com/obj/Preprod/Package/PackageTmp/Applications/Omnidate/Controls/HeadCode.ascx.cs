﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Web.Framework;
using Matchnet.Configuration.ServiceAdapters;

namespace Matchnet.Web.Applications.Omnidate.Controls
{
    public partial class HeadCode : FrameworkControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (RuntimeSettings.GetSetting("ENABLE_OMNIDATE", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID) == "true")
                {
                    plcOmnidate.Visible = true;

                    ltrHeadCode.Text = RuntimeSettings.GetSetting("OMNIDATE_HEAD_CODE",
                                                                  g.Brand.Site.Community.CommunityID,
                                                                  g.Brand.Site.SiteID);
                }
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }
    }
}