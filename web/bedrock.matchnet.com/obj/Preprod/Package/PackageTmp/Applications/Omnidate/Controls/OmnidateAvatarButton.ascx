﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OmnidateAvatarButton.ascx.cs"
    Inherits="Matchnet.Web.Applications.Omnidate.Controls.OmnidateAvatarButton" %>
<asp:PlaceHolder runat="server" ID="plcAvatarButton" Visible="false">

    <script type="text/javascript">
        Omni_Avatar('style', '<%=OmniStyle %>',
                'gender', '<%=Gender %>');
    </script>

</asp:PlaceHolder>
