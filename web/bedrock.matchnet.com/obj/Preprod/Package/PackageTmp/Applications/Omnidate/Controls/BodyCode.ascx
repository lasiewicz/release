﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BodyCode.ascx.cs" Inherits="Matchnet.Web.Applications.Omnidate.Controls.BodyCode" %>
<asp:PlaceHolder runat="server" ID="plcOmnidate" Visible="false">

    <script type="text/javascript">
        var MyText = new Object;
        MyText.online = "";
        MyText.inactive = "";
        MyText.dating = "";
        MyText.offline = "";
        MyText.info = "";

        function omnidateInit() {
            if (typeof (Omni_Initialize) != 'undefined') {
                Omni_Initialize(
                    'userid', '<%=MemberID%>',
                    'username', '<%=Username%>',
                    'gender', '<%=Gender%>',
                    'account', '<%=OmnidateAccountID%>',
                    'style', 'Spark01',
                    'profile', '<%=ViewProfileURL%>',
                    'alert', 'chime',
                    'userxml', '<%=UserXmlURL%>',
                    'userclass','<%=UserClass%>',
                    'registerurl','<%=RegisterUrl%>'
                    <%=OmnidateLanguage%>
                );
            }
        }

        $j(document).ready(function(){omnidateInit()});
    </script>

</asp:PlaceHolder>
