﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects.Photos;
using Matchnet.Web.Applications.MemberProfile;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Managers;
using Matchnet.Web.Framework.Util;

namespace Matchnet.Web.Applications.Omnidate
{
    public partial class OmnidateProfileInfo : FrameworkControl
    {
        private const string XML_ELEMENT = "<{0}>{1}</{0}>";
        private const string XML_CDATA_ELEMENT = "<{0}><![CDATA[{1}]]></{0}>";
        private const int MAXSIZE = 30;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["MemberID"]))
            {

                Int32 memberID = Conversion.CInt(Request["MemberID"]);
                Matchnet.Member.ServiceAdapters.Member member = MemberSA.Instance.GetMember(memberID, MemberLoadFlags.None);
                if (member != null)
                {
                    //Response.Write("<?xml version='1.0' encoding='iso-8859-1'?>");
                    Response.Write("<profile>");
                    Response.Write(string.Format(XML_ELEMENT, "userid", member.MemberID));
                    Response.Write(string.Format(XML_ELEMENT, "name", member.GetUserName(g.Brand)));
                    Response.Write(string.Format(XML_ELEMENT, "quote", string.Empty));
                    string seekingText = ProfileDisplayHelper.GetMaritalStatusSeekingGenderDisplay(member, this.g);
                    //FrameworkGlobals.GetGenderMaskString(member.GetAttributeInt(g.Brand, "GenderMask"));
                    Response.Write(string.Format(XML_ELEMENT, "gender", seekingText));
                    DateTime birthDate = member.GetAttributeDate(g.Brand, "BirthDate", DateTime.MinValue);
                    string age = FrameworkGlobals.GetAgeCaption(birthDate, g, this, "AGE_UNSPECIFIED");
                    Response.Write(string.Format(XML_ELEMENT, "age", age));
                    string location = string.Empty;
                    int regionID = member.GetAttributeInt(g.Brand, "RegionID");
                    if (regionID != Constants.NULL_INT)
                    {
                        location = FrameworkGlobals.Ellipsis(FrameworkGlobals.GetRegionString(regionID, g.Brand.Site.LanguageID, false, true, false), MAXSIZE);
                    }
                    Response.Write(string.Format(XML_ELEMENT, "location", location));
                    //string profession = member.GetAttributeText(g.Brand, "OccupationDescription");
                    string profession = string.Empty;
                    Response.Write(string.Format(XML_ELEMENT, "profession", profession));

                    bool isMale = FrameworkGlobals.IsMaleGender(member, g.Brand);

                    //MemberPhotoHelper photoHelper = new MemberPhotoHelper(RuntimeSettings.Instance);
                    string thumbnail = MemberPhotoDisplayManager.Instance.GetNoPhotoFile(NoPhotoImageType.Thumb, member, g.Brand);

                    if (member.HasApprovedPhoto(g.Brand.Site.Community.CommunityID))
                    {
                        Photo photo = MemberPhotoDisplayManager.Instance.GetDefaultPhoto(g.Member, member, g.Brand);
                        thumbnail = MemberPhotoDisplayManager.Instance.GetPhotoDisplayURL(g.Member, member, g.Brand, photo,
                                                                                PhotoType.Thumbnail,
                                                                                PrivatePhotoImageType.Thumb,
                                                                                NoPhotoImageType.Thumb, true);
                    }

                    Response.Write(string.Format(XML_ELEMENT, "photo", thumbnail));
                    //Response.Write(string.Format(XML_ELEMENT, "about", member.GetAttributeText(g.Brand, "AboutMe")));
                    Response.Write(string.Format(XML_ELEMENT, "about", ProfileDisplayHelper.GetFormattedEssayText(member, g, "AboutMe")));

                    Response.Write("</profile>");
                }
            }
            Response.End();
        }
    }
}