﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.List.ServiceAdapters;
using Matchnet.List.ValueObjects;
using Matchnet.Member.ValueObjects.Interfaces;
using Matchnet.Web.Framework;

namespace Matchnet.Web.Applications.Omnidate.Controls
{
    public partial class OmnidateInvitationButton : FrameworkControl
    {
        public enum DisplayContextEnum
        {
            MiniProfile,
            GalleryMiniProfile,
            FullProfile,
            FullProfile30,
            MiniProfile2,
            MiniProfile2Comm,
            MiniProfileListView,
            MiniProfileListViewComm
        }

        protected string OmnidateOnlineButtonURL { get; set; }
        protected string OmnidateDatingButtonURL { get; set; }
        protected string OmnidateButtonWidth { get; set; }
        protected string OmnidateButtonHeight { get; set; }
        protected string OmnidateButtonInactiveURL { get; set; }
        protected string OmnidateButtonOfflineURL { get; set; }

        public IMemberDTO TargetMember { get; set; }
        public DisplayContextEnum DisplayContext;

        public bool IsOmnidateDisplayed
        {
            get { return plcOmnidateButton.Visible; }
        }

        //protected void Page_Load(object sender, EventArgs e)
        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (RuntimeSettings.GetSetting("ENABLE_OMNIDATE", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID) == "true")
            {
                if (g.Member != null && TargetMember != null && g.Member.MemberID != TargetMember.MemberID)
                {
                    // Show the button only if the target member is online
                    if (Matchnet.MembersOnline.ServiceAdapters.MembersOnlineSA.Instance.IsMemberOnline(g.Brand.Site.Community.CommunityID, TargetMember.MemberID) && !Request.PathInfo.Contains("ProcessFlirtOperation"))
                    {

                        plcOmnidateButton.Visible = true;
                        onmnidateHover.Visible = true;

                        divButton.Attributes.Add("onmouseover", "javascript:document.getElementById('" + onmnidateHover.ClientID + "').style.display = 'block';");
                        divButton.Attributes.Add("onmouseout", "javascript:document.getElementById('" + onmnidateHover.ClientID + "').style.display = 'none';");

                        StringDictionary expansionTokens = new StringDictionary();
                        expansionTokens.Add("USERNAME", TargetMember.GetUserName(g.Brand));
                        lblHoverTitle.Text = g.GetResource("TXT_HOVER_TITLE", this, expansionTokens);

                        Matchnet.List.ServiceAdapters.List memberList = ListSA.Instance.GetList(TargetMember.MemberID, ListLoadFlags.IngoreSACache);
                        bool isBlocked = memberList.IsHotListed(HotListCategory.IgnoreList
                            , g.Brand.Site.Community.CommunityID
                            , g.Member.MemberID);

                        bool isOmnidateEnabledOnTargetMemberSite = getmnidateEnabledOnTargetMemberSite();

                        bool viewprofilefbtab = g.AppPage != null &&
                                                g.AppPage.ControlName.ToLower() == "viewprofilefbtab";

                        bool searchResultsM2MUpgradeEnabled = FrameworkGlobals.SearchResultsM2MUpgradeEnabled(g);
                        string M2MUpgrade = searchResultsM2MUpgradeEnabled ? "_M2M_UPGRADE" : string.Empty;
             
                        if (OmnidateHelper.GetOmnidateUserClass(g) != OmnidateUserClass.NonSubscriber && !isBlocked && isOmnidateEnabledOnTargetMemberSite && !viewprofilefbtab)
                        {
                            plcPayingMember.Visible = true;
                            plcOmnidateButtonObject.Visible = true;

                            string host = "http://" + Request.Url.Host;

                            switch (DisplayContext)
                            {
                                case DisplayContextEnum.MiniProfile:
                                    OmnidateOnlineButtonURL = host + g.GetResource("ONLINE_BUTTON_MINI_PROFILE", new string[0], true, this);
                                    OmnidateDatingButtonURL = host + g.GetResource("DATING_BUTTON_MINI_PROFILE", new string[0], true, this);
                                    OmnidateButtonInactiveURL = host + g.GetResource("INACTIVE_BUTTON_MINI_PROFILE", new string[0], true, this);
                                    OmnidateButtonOfflineURL = host + g.GetResource("OFFLINE_BUTTON_MINI_PROFILE", new string[0], true, this);
                                    OmnidateButtonWidth = g.GetResource("BUTTON_WIDTH_MINI_PROFILE", this);
                                    OmnidateButtonHeight = g.GetResource("BUTTON_HEIGHT_MINI_PROFILE", this);
                                    break;
                                case DisplayContextEnum.GalleryMiniProfile:
                                    OmnidateOnlineButtonURL = host + g.GetResource("ONLINE_BUTTON_GALLERY_MINI_PROFILE", new string[0], true, this);
                                    OmnidateDatingButtonURL = host + g.GetResource("DATING_BUTTON_GALLERY_MINI_PROFILE", new string[0], true, this);
                                    OmnidateButtonInactiveURL = host + g.GetResource("INACTIVE_BUTTON_GALLERY_MINI_PROFILE", new string[0], true, this);
                                    OmnidateButtonOfflineURL = host + g.GetResource("OFFLINE_BUTTON_GALLERY_MINI_PROFILE", new string[0], true, this);
                                    OmnidateButtonWidth = g.GetResource("BUTTON_WIDTH_GALLERY_MINI_PROFILE", this);
                                    OmnidateButtonHeight = g.GetResource("BUTTON_HEIGHT_GALLERY_MINI_PROFILE", this);
                                    break;
                                case DisplayContextEnum.FullProfile:
                                    OmnidateOnlineButtonURL = host + g.GetResource("ONLINE_BUTTON_FULL_PROFILE", new string[0], true, this);
                                    OmnidateDatingButtonURL = host + g.GetResource("DATING_BUTTON_FULL_PROFILE", new string[0], true, this);
                                    OmnidateButtonInactiveURL = host + g.GetResource("INACTIVE_BUTTON_FULL_PROFILE", new string[0], true, this);
                                    OmnidateButtonOfflineURL = host + g.GetResource("OFFLINE_BUTTON_FULL_PROFILE", new string[0], true, this);
                                    OmnidateButtonWidth = g.GetResource("BUTTON_WIDTH_FULL_PROFILE", this);
                                    OmnidateButtonHeight = g.GetResource("BUTTON_HEIGHT_FULL_PROFILE", this);
                                    txtOpenOmniBtnWrap.Visible = true;
                                    txtCloseOmniBtnWrap.Visible = true;
                                    txtOpenOmniBtnWrap.ResourceConstant = "LI_OPEN";
                                    txtCloseOmniBtnWrap.ResourceConstant = "LI_CLOSE";
                                    break;
                                case DisplayContextEnum.FullProfile30:
                                    OmnidateOnlineButtonURL = host + g.GetResource("ONLINE_BUTTON_FULL_PROFILE30", new string[0], true, this);
                                    OmnidateDatingButtonURL = host + g.GetResource("DATING_BUTTON_FULL_PROFILE30", new string[0], true, this);
                                    OmnidateButtonInactiveURL = host + g.GetResource("INACTIVE_BUTTON_FULL_PROFILE30", new string[0], true, this);
                                    OmnidateButtonOfflineURL = host + g.GetResource("OFFLINE_BUTTON_FULL_PROFILE30", new string[0], true, this);
                                    OmnidateButtonWidth = g.GetResource("BUTTON_WIDTH_FULL_PROFILE30", this);
                                    OmnidateButtonHeight = g.GetResource("BUTTON_HEIGHT_FULL_PROFILE30", this);
                                    txtOpenOmniBtnWrap.Visible = true;
                                    txtCloseOmniBtnWrap.Visible = true;
                                    txtOpenOmniBtnWrap.ResourceConstant = "DIV_OPEN";
                                    txtCloseOmniBtnWrap.ResourceConstant = "DIV_CLOSE";
                                    break;
                                case DisplayContextEnum.MiniProfile2:
                                    OmnidateOnlineButtonURL = host + g.GetResource("ONLINE_BUTTON_GALLERY_MINI_PROFILE_2" + M2MUpgrade, new string[0], true, this);
                                    OmnidateDatingButtonURL = host + g.GetResource("DATING_BUTTON_GALLERY_MINI_PROFILE_2" + M2MUpgrade, new string[0], true, this);
                                    OmnidateButtonInactiveURL = host + g.GetResource("INACTIVE_BUTTON_GALLERY_MINI_PROFILE_2" + M2MUpgrade, new string[0], true, this);
                                    OmnidateButtonOfflineURL = host + g.GetResource("OFFLINE_BUTTON_GALLERY_MINI_PROFILE_2" + M2MUpgrade, new string[0], true, this);
                                    OmnidateButtonWidth = g.GetResource("BUTTON_WIDTH_GALLERY_MINI_PROFILE_2", this);
                                    OmnidateButtonHeight = g.GetResource("BUTTON_HEIGHT_GALLERY_MINI_PROFILE_2", this);
                                    txtOpenOmniBtnWrap.Visible = true;
                                    txtCloseOmniBtnWrap.Visible = true;
                                    txtOpenOmniBtnWrap.ResourceConstant = "LI_OPEN";
                                    txtCloseOmniBtnWrap.ResourceConstant = "LI_CLOSE";
                                    break;
                                case DisplayContextEnum.MiniProfile2Comm:
                                    OmnidateOnlineButtonURL = host + g.GetResource("ONLINE_BUTTON_GALLERY_MINI_PROFILE_2_COMM", new string[0], true, this);
                                    OmnidateDatingButtonURL = host + g.GetResource("DATING_BUTTON_GALLERY_MINI_PROFILE_2_COMM", new string[0], true, this);
                                    OmnidateButtonInactiveURL = host + g.GetResource("INACTIVE_BUTTON_GALLERY_MINI_PROFILE_2_COMM", new string[0], true, this);
                                    OmnidateButtonOfflineURL = host + g.GetResource("OFFLINE_BUTTON_GALLERY_MINI_PROFILE_2_COMM", new string[0], true, this);
                                    OmnidateButtonWidth = g.GetResource("BUTTON_WIDTH_GALLERY_MINI_PROFILE_2_COMM", this);
                                    OmnidateButtonHeight = g.GetResource("BUTTON_HEIGHT_GALLERY_MINI_PROFILE_2_COMM", this);
                                    txtOpenOmniBtnWrap.Visible = true;
                                    txtCloseOmniBtnWrap.Visible = true;
                                    txtOpenOmniBtnWrap.ResourceConstant = "LI_OPEN";
                                    txtCloseOmniBtnWrap.ResourceConstant = "LI_CLOSE";
                                    break;
                                case DisplayContextEnum.MiniProfileListView:
                                    OmnidateOnlineButtonURL = host + g.GetResource("ONLINE_BUTTON_MINI_PROFILE_LIST_VIEW" + M2MUpgrade, new string[0], true, this);
                                    OmnidateDatingButtonURL = host + g.GetResource("DATING_BUTTON_MINI_PROFILE_LIST_VIEW" + M2MUpgrade, new string[0], true, this);
                                    OmnidateButtonInactiveURL = host + g.GetResource("INACTIVE_BUTTON_MINI_PROFILE_LIST_VIEW" + M2MUpgrade, new string[0], true, this);
                                    OmnidateButtonOfflineURL = host + g.GetResource("OFFLINE_BUTTON_MINI_PROFILE_LIST_VIEW" + M2MUpgrade, new string[0], true, this);
                                    OmnidateButtonWidth = g.GetResource("BUTTON_WIDTH_MINI_PROFILE_LIST_VIEW", this);
                                    OmnidateButtonHeight = g.GetResource("BUTTON_HEIGHT_MINI_PROFILE_LIST_VIEW", this);
                                    txtOpenOmniBtnWrap.Visible = true;
                                    txtCloseOmniBtnWrap.Visible = true;
                                    txtOpenOmniBtnWrap.ResourceConstant = "DIV_OPEN";
                                    txtCloseOmniBtnWrap.ResourceConstant = "DIV_CLOSE";
                                    break;
                                case DisplayContextEnum.MiniProfileListViewComm:
                                    OmnidateOnlineButtonURL = host + g.GetResource("ONLINE_BUTTON_MINI_PROFILE_LIST_VIEW_COMM", new string[0], true, this);
                                    OmnidateDatingButtonURL = host + g.GetResource("DATING_BUTTON_MINI_PROFILE_LIST_VIEW_COMM", new string[0], true, this);
                                    OmnidateButtonInactiveURL = host + g.GetResource("INACTIVE_MINI_PROFILE_LIST_VIEW_COMM", new string[0], true, this);
                                    OmnidateButtonOfflineURL = host + g.GetResource("OFFLINE_MINI_PROFILE_LIST_VIEW_COMM", new string[0], true, this);
                                    OmnidateButtonWidth = g.GetResource("BUTTON_WIDTH_MINI_PROFILE_LIST_VIEW_COMM", this);
                                    OmnidateButtonHeight = g.GetResource("BUTTON_HEIGHT_MINI_PROFILE_LIST_VIEW_COMM", this);
                                    txtOpenOmniBtnWrap.Visible = true;
                                    txtCloseOmniBtnWrap.Visible = true;
                                    txtOpenOmniBtnWrap.ResourceConstant = "LI_OPEN";
                                    txtCloseOmniBtnWrap.ResourceConstant = "LI_CLOSE";
                                    break;
                                default:
                                    plcOmnidateButton.Visible = false;
                                    plcOmnidateButtonObject.Visible = false;
                                    OmnidateOnlineButtonURL = host + g.GetResource("ONLINE_BUTTON_MINI_PROFILE", new string[0], true, this);
                                    OmnidateDatingButtonURL = host + g.GetResource("DATING_BUTTON_MINI_PROFILE", new string[0], true, this);
                                    OmnidateButtonInactiveURL = host + g.GetResource("INACTIVE_BUTTON_MINI_PROFILE", new string[0], true, this);
                                    OmnidateButtonOfflineURL = host + g.GetResource("OFFLINE_BUTTON_MINI_PROFILE", new string[0], true, this);
                                    break;
                            }
                        }
                        else
                        {
                            int prtid = 0;

                            plcNonPayingMember.Visible = true;

                            switch (DisplayContext)
                            {
                                case DisplayContextEnum.MiniProfile:
                                    imgInvitationButton.FileName = "btn-date-me.png";
                                    imgInvitationButtonMatchesBeta.FileName = "btn-date-me.png";
                                    prtid = 744;
                                    break;
                                case DisplayContextEnum.GalleryMiniProfile:
                                    imgInvitationButton.FileName = "icon-date-me.png";
                                    imgInvitationButtonMatchesBeta.FileName = "icon-date-me.png";
                                    prtid = 745;
                                    break;
                                case DisplayContextEnum.FullProfile:
                                    imgInvitationButton.FileName = "btn-date-me.png";
                                    imgInvitationButtonMatchesBeta.FileName = "btn-date-me.png";
                                    txtOpenOmniBtnWrap.Visible = true;
                                    txtCloseOmniBtnWrap.Visible = true;
                                    txtOpenOmniBtnWrap.ResourceConstant = "LI_OPEN";
                                    txtCloseOmniBtnWrap.ResourceConstant = "LI_CLOSE";
                                    prtid = 746;
                                    break;
                                case DisplayContextEnum.FullProfile30:
                                    imgInvitationButton.FileName = "btn-date-me-30.png";
                                    imgInvitationButtonMatchesBeta.FileName = "btn-date-me-30.png";
                                    txtOpenOmniBtnWrap.Visible = true;
                                    txtCloseOmniBtnWrap.Visible = true;
                                    txtOpenOmniBtnWrap.ResourceConstant = "DIV_OPEN";
                                    txtCloseOmniBtnWrap.ResourceConstant = "DIV_CLOSE";
                                    prtid = 746;
                                    break;
                                case DisplayContextEnum.MiniProfile2:
                                    imgInvitationButton.FileName = searchResultsM2MUpgradeEnabled ? "DateMe_v2.png" : "btn-date-me-30-s.png";
                                    imgInvitationButtonMatchesBeta.FileName =  searchResultsM2MUpgradeEnabled ? "DateMe_v2.png" : "btn-date-me-30-s.png";
                                    txtOpenOmniBtnWrap.Visible = true;
                                    txtCloseOmniBtnWrap.Visible = true;
                                    txtOpenOmniBtnWrap.ResourceConstant = "LI_OPEN";
                                    txtCloseOmniBtnWrap.ResourceConstant = "LI_CLOSE"; break;
                                case DisplayContextEnum.MiniProfile2Comm:
                                    imgInvitationButton.FileName = "btn-date-me-30-s.png";
                                    imgInvitationButtonMatchesBeta.FileName = "btn-date-me-30-s.png";
                                    plcNonPayingMember.Visible = false;
                                    plcNonPayingMemberMatchesBeta.Visible = true;
                                    txtOpenOmniBtnWrap.Visible = true;
                                    txtCloseOmniBtnWrap.Visible = true;
                                    txtOpenOmniBtnWrap.ResourceConstant = "LI_OPEN";
                                    txtCloseOmniBtnWrap.ResourceConstant = "LI_CLOSE";
                                    break;
                                case DisplayContextEnum.MiniProfileListView:
                                    imgInvitationButton.FileName = searchResultsM2MUpgradeEnabled ? "DateMe_v2.png" : "btn-date-me-30-s.png";
                                    imgInvitationButtonMatchesBeta.FileName = searchResultsM2MUpgradeEnabled ? "DateMe_v2.png" :  "btn-date-me-30-s.png";
                                    txtOpenOmniBtnWrap.Visible = true;
                                    txtCloseOmniBtnWrap.Visible = true;
                                    txtOpenOmniBtnWrap.ResourceConstant = "DIV_OPEN";
                                    txtCloseOmniBtnWrap.ResourceConstant = "DIV_CLOSE";
                                    break;
                                case DisplayContextEnum.MiniProfileListViewComm:
                                    imgInvitationButton.FileName = "btn-date-me-30-s.png";
                                    imgInvitationButtonMatchesBeta.FileName = "btn-date-me-30-s.png";
                                    plcNonPayingMember.Visible = false;
                                    plcNonPayingMemberMatchesBeta.Visible = true;
                                    txtOpenOmniBtnWrap.Visible = true;
                                    txtCloseOmniBtnWrap.Visible = true;
                                    txtOpenOmniBtnWrap.ResourceConstant = "LI_OPEN";
                                    txtCloseOmniBtnWrap.ResourceConstant = "LI_CLOSE";
                                    break;
                                default:
                                    plcOmnidateButton.Visible = false;
                                    break;
                            }

                            if (isBlocked)
                            {
                                string currentURL = Request.Url.AbsoluteUri.ToLower();
                                if (currentURL.IndexOf("rc") != -1)
                                {
                                    currentURL = g.RemoveParamFromURL(currentURL, "rc", false);
                                }

                                currentURL += currentURL.Contains('?') ? "&" : "?";
                                currentURL += "rc=OMNIDATE_BLOCKED_MEMBER";

                                lnkInvitationButton.NavigateUrl = currentURL;
                            }
                            else if (!isOmnidateEnabledOnTargetMemberSite)
                            {
                                /*
                                string currentURL = Request.Url.AbsoluteUri.ToLower();
                                if (currentURL.IndexOf("rc") != -1)
                                {
                                    currentURL = g.RemoveParamFromURL(currentURL, "rc", false);
                                }

                                currentURL += currentURL.Contains('?') ? "&" : "?";
                                currentURL += "rc=OMNIDATE_DISABLED_ON_TARGET_SITE";
                                */
                                switch (DisplayContext)
                                {
                                    case DisplayContextEnum.MiniProfile:
                                        imgInvitationButton.FileName = "btn-date-me-disabled.png";
                                        imgInvitationButtonMatchesBeta.FileName = "btn-date-me-disabled.png";
                                        break;
                                    case DisplayContextEnum.GalleryMiniProfile:
                                        imgInvitationButton.FileName = "icon-date-me-disabled.png";
                                        imgInvitationButtonMatchesBeta.FileName = "icon-date-me-disabled.png";
                                        break;
                                    case DisplayContextEnum.FullProfile:
                                        imgInvitationButton.FileName = "btn-date-me-disabled.png";
                                        imgInvitationButtonMatchesBeta.FileName = "btn-date-me-disabled.png";
                                        txtOpenOmniBtnWrap.Visible = true;
                                        txtCloseOmniBtnWrap.Visible = true;
                                        txtOpenOmniBtnWrap.ResourceConstant = "LI_OPEN";
                                        txtCloseOmniBtnWrap.ResourceConstant = "LI_CLOSE";
                                        break;
                                    case DisplayContextEnum.FullProfile30:
                                        imgInvitationButton.FileName = "btn-date-me-disabled-30.png";
                                        imgInvitationButtonMatchesBeta.FileName = "btn-date-me-disabled-30.png";
                                        txtOpenOmniBtnWrap.Visible = true;
                                        txtCloseOmniBtnWrap.Visible = true;
                                        txtOpenOmniBtnWrap.ResourceConstant = "DIV_OPEN";
                                        txtCloseOmniBtnWrap.ResourceConstant = "DIV_CLOSE";
                                        break;
                                    case DisplayContextEnum.MiniProfile2:
                                        imgInvitationButton.FileName = searchResultsM2MUpgradeEnabled ? "DateMe-greyed-out_v2.png" : "btn-date-me-disabled-30-s.png";
                                        imgInvitationButtonMatchesBeta.FileName = "btn-date-me-disabled-30-s.png";
                                        txtOpenOmniBtnWrap.Visible = true;
                                        txtCloseOmniBtnWrap.Visible = true;
                                        txtOpenOmniBtnWrap.ResourceConstant = "LI_OPEN";
                                        txtCloseOmniBtnWrap.ResourceConstant = "LI_CLOSE"; break;
                                    case DisplayContextEnum.MiniProfile2Comm:
                                        imgInvitationButton.FileName = "btn-date-me-disabled-30-s.png";
                                        imgInvitationButtonMatchesBeta.FileName = "btn-date-me-disabled-30-s.png";
                                        txtOpenOmniBtnWrap.Visible = true;
                                        txtCloseOmniBtnWrap.Visible = true;
                                        txtOpenOmniBtnWrap.ResourceConstant = "LI_OPEN";
                                        txtCloseOmniBtnWrap.ResourceConstant = "LI_CLOSE"; break;
                                    case DisplayContextEnum.MiniProfileListView:
                                        imgInvitationButton.FileName = searchResultsM2MUpgradeEnabled ? "DateMe-greyed-out_v2.png" : "btn-date-me-disabled-30-s.png";
                                        imgInvitationButtonMatchesBeta.FileName = "btn-date-me-disabled-30-s.png";
                                        txtOpenOmniBtnWrap.Visible = true;
                                        txtCloseOmniBtnWrap.Visible = true;
                                        txtOpenOmniBtnWrap.ResourceConstant = "DIV_OPEN";
                                        txtCloseOmniBtnWrap.ResourceConstant = "DIV_CLOSE";
                                        break;
                                    case DisplayContextEnum.MiniProfileListViewComm:
                                        imgInvitationButton.FileName = "btn-date-me-disabled-30-s.png";
                                        imgInvitationButtonMatchesBeta.FileName = "btn-date-me-disabled-30-s.png";
                                        txtOpenOmniBtnWrap.Visible = true;
                                        txtCloseOmniBtnWrap.Visible = true;
                                        txtOpenOmniBtnWrap.ResourceConstant = "LI_OPEN";
                                        txtCloseOmniBtnWrap.ResourceConstant = "LI_CLOSE";
                                        break;
                                    default:
                                        plcOmnidateButton.Visible = false;
                                        break;
                                }
                                lnkInvitationButton.NavigateUrl = string.Empty;
                                imgInvitationButton.AlternateText = g.GetResource("TTL_ONLINE_DATING_UNAVAILABLE", this);
                                imgInvitationButton.Attributes.Add("title", imgInvitationButton.AlternateText);
                                divButton.Attributes.Remove("onmouseover");
                                divButton.Attributes.Remove("onmouseout");
                            }
                            else // not a paying member
                            {
                                lnkInvitationButton.NavigateUrl = FrameworkGlobals.GetSubscriptionLink(false, prtid)
   + "&DestinationURL=" + HttpContext.Current.Server.UrlEncode("http://" + Request.Url.Host + "/Applications/MemberProfile/ViewProfile.aspx?MemberID=" + TargetMember.MemberID); ;

                            }

                            if (g.AppPage != null && g.AppPage.ControlName.ToLower() == "viewprofilefbtab")
                            {
                                string host = HttpContext.Current.Request.Url.Host;
                                lnkInvitationButton.NavigateUrl = "http://" + host +
                                            "/Applications/MemberProfile/ViewProfile.aspx?prm=80633&mailtypeid=1&MemberID=" +
                                            TargetMember.MemberID;
                            }

                            lnkInvitationButtonMatchesBeta.NavigateUrl = lnkInvitationButton.NavigateUrl;
                        }
                    }
                }
            }

        }

        private bool getmnidateEnabledOnTargetMemberSite()
        {
            bool result = false;
            string OmnidateAccountID;
            string TargetOmnidateAccountID;

            int targetBrandID = 0;
            TargetMember.GetLastLogonDate(g.Brand.Site.Community.CommunityID, out targetBrandID);

            Brand targetBrand = Matchnet.Content.ServiceAdapters.BrandConfigSA.Instance.GetBrandByID(targetBrandID);

            if (targetBrand != null)
            {
                if (RuntimeSettings.GetSetting("ENABLE_OMNIDATE", targetBrand.Site.Community.CommunityID, targetBrand.Site.SiteID) == "true")
                {
                    OmnidateAccountID = RuntimeSettings.GetSetting("OMNIDATE_ACCOUNT_ID", g.Brand.Site.Community.CommunityID,
                                                                      g.Brand.Site.SiteID);
                    TargetOmnidateAccountID = RuntimeSettings.GetSetting("OMNIDATE_ACCOUNT_ID", targetBrand.Site.Community.CommunityID,
                                                                      targetBrand.Site.SiteID);

                    if (OmnidateAccountID == TargetOmnidateAccountID)
                    {
                        result = true;
                    }

                }

            }

            return result;
        }


    }
}
