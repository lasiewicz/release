﻿using System;
using System.Web.UI.WebControls;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Web.Applications.Logon.Controls;
using Matchnet.Web.Framework;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Web.Framework.Managers;

namespace Matchnet.Web.Applications.Logon
{
    public partial class Logon20 : FrameworkControl
    {
        private string _DestinationUrl;
        public string DestinationUrl
        {
            get { return (_DestinationUrl); }
            set { _DestinationUrl = value; }
        }

        private void Page_Init(object sender, EventArgs e)
        {
            try
            {
                g.HeaderControl.ShowBlankHeader(Matchnet.Web.Framework.Ui.HeaderImageType.Login);
                g.SetTopAuxMenuVisibility(false, true);
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        private void Page_Load(object sender, EventArgs e)
        {
            try
            {
                var logonManager = new LogonManager(g);
                logonManager.AdminImpersonate();

                // this logic has to be in Page_Load, otherwise Footer has not processed in Init
                g.FooterControl20.HideFooterLinks();
                
                if (Request.QueryString.Get("SentPassword") == "true" && !IsPostBack)
                {
                    g.Notification.AddMessage("SUCCESSFULLY_SENT");
                }
                if (Request.QueryString.Get("logout") == "true" && !IsPostBack)
                {
                    g.Transfer("/Applications/Logon/Logoff.aspx");
                }

                if (AutoLoginHelper.IsSettingEnabled(_g))
                {
                    if (g.Session["HardLoginRequired"] == "true")
                    {
                        txtLoginUpsell.ResourceConstant = "TXT_AUTOLOGIN_HARDLOGIN_REASON";
                        g.Session.Remove("HardLoginRequired");
                    }
                }
                if (txtJoinToday != null)
                {
                    txtJoinToday.Href = FrameworkGlobals.GetRegistrationPageURL(g);
                }
                string url;
                url = FrameworkGlobals.GetRegistrationPageURL(g);
                g.ExpansionTokens.Add("REGISTRATION_URL", url);

                if (g.BreadCrumbTrailHeader != null)
                {
                    g.BreadCrumbTrailHeader.SetTwoLinkCrumb(g.GetResource("TXT_LOGIN", this),
                                                            g.AppPage.App.DefaultPagePath);

                }
                if (g.BreadCrumbTrailFooter != null)
                {
                    g.BreadCrumbTrailFooter.SetTwoLinkCrumb(g.GetResource("TXT_LOGIN", this),
                                                           g.AppPage.App.DefaultPagePath);
                }			
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }



        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new EventHandler(this.Page_Load);
            this.Init += new EventHandler(this.Page_Init);

        }
        #endregion
    }
}