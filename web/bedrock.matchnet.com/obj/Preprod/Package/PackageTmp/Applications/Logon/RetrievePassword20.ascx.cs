﻿using System;
using System.Web.UI.WebControls;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Ui.FormElements;

using Matchnet.ExternalMail.ServiceAdapters;

namespace Matchnet.Web.Applications.Logon
{
    /// <summary>
    ///		Summary description for RetrievePassword.
    /// </summary>
    public partial class RetrievePassword20 : FrameworkControl
    {
        //protected TextBox EmailAddress;
        //protected Title ttlForgotYourPasswordQuestion;
        //protected Txt txtEnterTheEmailAddress;
        //protected FrameworkHtmlButton btnGetLoginInfo;
        //protected Button btnRetrievePassword;
        //protected System.Web.UI.WebControls.RegularExpressionValidator revEmailAddressValidator;
        //protected System.Web.UI.WebControls.RequiredFieldValidator revEmailAddressRequiredValidator;

        private void Page_Load(object sender, EventArgs e)
        {
            // we are retiring this page
            g.Transfer("/Applications/MemberServices/ForgotPassword.aspx");
        }
        
        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new EventHandler(this.Page_Load);
        }
        #endregion

    }
}
