﻿#region

using System;
using System.Configuration;
using System.Threading;
using System.Web;
using System.Web.UI.WebControls;
using Matchnet.Caching.CacheBuster;
using Matchnet.Caching.CacheBuster.HTTPContextWrappers;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Managers;
using Spark.Authentication.OAuth;
using Spark.Authentication.OAuth.V2;

#endregion

namespace Matchnet.Web.Applications.Logon.Controls
{
    public partial class LogonControl20 : FrameworkControl
    {
        private static string _tokenCookieEnvironment;
        private SuaLogonManager _logonManager;

        private void Page_Load(object sender, System.EventArgs e)
        {
            try
            {
                // Before redirecting over to SUA check if there a live session, use it.
                if (_g.Member != null)
                {
                    _g.Transfer(FrameworkGlobals.LinkHref("/Applications/Home/Default.aspx", false));
                }

                _logonManager = new SuaLogonManager(_g.Brand.BrandID, new LogonManager(_g));
                _logonManager.ProcessSuaLogon();

                SetDefaultButton(Password, Login);
                SetDefaultButton(EmailAddress, Login);

                if (!IsPostBack)
                {
                    LoadControlState();
                }

                if (!AutoLoginHelper.IsSettingEnabled(_g))
                {
                    PlaceHolderAutoLogin.Visible = false;
                }

                if (!IsPostBack)
                {
                    CheckBoxAutoLogin.Checked =
                        Matchnet.Conversion.CBool(
                            RuntimeSettings.GetSetting("AUTOMATICLOGIN_CHECKBOX", g.Brand.Site.Community.CommunityID,
                                                       g.Brand.Site.SiteID), false);
                }
                var resourceManager = new ResourceManager(g.Brand);
                revEmailAddressRequiredValidator.ErrorMessage = resourceManager.GetResource("EMAIL_REQUIRED", this);
                revEmailAddressValidator.ErrorMessage = resourceManager.GetResource("EMAIL_REQUIRED", this);
                revPasswordRequiredValidator.ErrorMessage = resourceManager.GetResource("PASSWORD_REQUIRED", this);
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        private void LoadControlState()
        {
            // Pre-populate user email address based on cookie value (if present)
            var strEmail = g.Session["EmailAddress"];

            if (strEmail != String.Empty)
            {
                EmailAddress.Text = strEmail;
                this.fefControl.ElementClientId = this.Password.ClientID;
            }
            else
            {
                this.fefControl.ElementClientId = this.EmailAddress.ClientID;
            }
        }

        private void SetDefaultButton(TextBox txt, Matchnet.Web.Framework.Ui.FormElements.FrameworkButton button)
        {
            txt.Attributes.Add("onkeydown", "setAutoSubmitButton(" + button.ClientID + ",event)");
        }

        protected void Login_Click(object sender, System.EventArgs e)
        {
            try
            {
                Page.Validate();
                if (!Page.IsValid)
                {
                    return;
                }
                //todo : refactor ipaddress code
                var currentRequest = new CurrentRequest();
                var currentServer = new CurrentServer();
                var ipConverter = new IPConverter();
                var ipAddress = currentRequest.ClientIP;
                g.LoggingManager.LogInfoMessage("AutoLoginHelper",
                                              "Email : " + EmailAddress.Text + " IPAddress : " + ipAddress);
                // Removed Marketing related tracking vals #21001
                var authResult = MemberSA.Instance.Authenticate(g.Brand, EmailAddress.Text, Password.Text);

                // If this is SparkWS call, we are finding that sometimes the user logs in, is sent back to the
                // calling site (i.e. Connect), the calling Site makes a call to SparkWS.GetMember(), and SparkWS
                // is not having the most up-to-date Session.  Thus, we need to make this call synchronous.  NOTE:
                // this may still not solve this problem because there is a possibility that delay is not caused by
                // the asynchronous SaveSession call but instead by the asynchronous cache synchronization and replication.
                if (HttpContext.Current.Request[WebConstants.URL_PARAMETER_NAME_SPARKWS_CONTEXT] == "true")
                    g.SaveSessionAsync = false;

                System.Diagnostics.Trace.WriteLine("LogonControl login click: Authenticate status - " +
                                                   authResult.Status.ToString() + ", email: " + EmailAddress.Text);
                switch (authResult.Status)
                {
                    case AuthenticationStatus.Authenticated:
                        string encryptedPassword;
                        if (CheckBoxAutoLogin.Checked)
                        {
                            encryptedPassword = g.AutoLogin.CreateAutoLogin(EmailAddress.Text, Password.Text);
                        }
                        else
                        {
                            encryptedPassword = MemberSA.Instance.GetPasswordForAuth(EmailAddress.Text,
                                                                                     g.Brand.Site.Community.CommunityID,
                                                                                     Password.Text);
                            if (Matchnet.Web.Framework.AutoLoginHelper.IsSettingEnabled(g))
                            {
                                g.AutoLogin.Status = AutoLoginHelper.AutoLoginStatus.NormalLoggedIn;
                            }
                        }
                        g.StartLogonSession(authResult.MemberID, g.Brand.Site.Community.CommunityID, g.Brand.BrandID,
                                            this.EmailAddress.Text, authResult.UserName, false, Constants.NULL_INT);
                        var IMOAuthEnabled = Convert.ToBoolean(RuntimeSettings.GetSetting("SET_IM_OAUTH_COOKIES",
                                                                                          g.Brand.Site.Community.CommunityID,
                                                                                          g.Brand.Site.SiteID,
                                                                                          g.Brand.BrandID));
                        if (IMOAuthEnabled)
                        {
                            if (_tokenCookieEnvironment == null)
                            {
                                _tokenCookieEnvironment = SettingsManager.GetTokenEnvironmentForCookie();
                            }
                            LoginManager.Instance.FetchTokensAndSaveToCookies(EmailAddress.Text, encryptedPassword, true,
                                                                              CheckBoxAutoLogin.Checked,
                                                                              _tokenCookieEnvironment,
                                                                              g.Brand.Site.Name.ToLower(),
                                                                              g.Brand.BrandID);
                        }
                        _logonManager.PostLogonProcessor.ProcessSuccessfulLogon(authResult.MemberID, false);

                        break;
                    case AuthenticationStatus.InvalidEmailAddress:
                        ProcessFailedLogon("ERR_YOU_HAVE_PROVIDED_AN_INVALID_EMAIL_ADDRESS");
                        break;
                    case AuthenticationStatus.InvalidPassword:
                        ProcessFailedLogon("ERR_YOU_HAVE_PROVIDED_AN_INVALID_PASSWORD", false);
                        //additional custom error message for password field
                        phPasswordError.Visible = true;
                        litPasswordError.Text =
                            g.GetResource("ERR_YOU_HAVE_PROVIDED_AN_INVALID_PASSWORD_WITH_FORGOT_PASSWORD", null);
                        break;
                    case AuthenticationStatus.AdminSuspended:
                        ProcessFailedLogon("ADMIN_SUSPENDED_ACTION");
                        break;
                }
            }
            catch (ThreadAbortException)
            {
                // this is expected behavior due to a redirect after login
                //System.Diagnostics.Trace.WriteLine("LogonControl login click: Redirecting " + EmailAddress.Text);
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        private void ProcessFailedLogon(string ResourceError)
        {
            ProcessFailedLogon(ResourceError, true);
        }

        private void ProcessFailedLogon(string ResourceError, bool displayNotificationMessage)
        {
            if (g.AppPage.ControlName.ToLower() == "splash")
            {
                g.Transfer("/Applications/Logon/Logon.aspx?rc=" + ResourceError);
            }
            else
            {
                if (Request["rc"] != null && Request["rc"] != String.Empty)
                {
                    g.Transfer("/Applications/Logon/Logon.aspx?rc=" + ResourceError);
                }
                else
                {
                    if (displayNotificationMessage)
                    {
                        g.Notification.AddMessageString(g.GetResource(ResourceError, null));
                    }
                }
            }
        }

        #region Web Form Designer generated code

        protected override void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///     Required method for Designer support - do not modify
        ///     the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Login.Click += new System.EventHandler(this.Login_Click);
            this.Load += new System.EventHandler(this.Page_Load);
        }

        #endregion
    }
}