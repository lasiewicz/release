﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LogonControlSplash20.ascx.cs" Inherits="Matchnet.Web.Applications.Logon.Controls.LogonControlSplash20" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="dg" TagName="FirstElementFocus" Src="../../../Framework/JavaScript/FirstElementFocus.ascx" %>
<dg:FirstElementFocus id="fefControl" runat="server" />


<fieldset id="loginBox" class="rbox-style-high login-box member" border="0">

    <div class="field_sml">
        <asp:TextBox id="EmailAddress" Columns="30" runat="server" dir="ltr" CssClass="field_sml" />
        <!--<input name="username" type="text" class="field_sml" onfocus="username.className='field_sml_focus';" onblur="username.className='field_sml';" />-->
        <mn:txt runat="server" id="txtEmailAddress" ResourceConstant="TXT_EMAIL_ADDRESS" />
    </div>
    <div class="field_sml">
        <asp:TextBox id="Password" Columns="30" TextMode="Password" runat="server" dir="ltr" MaxLength="16" CssClass="field_sml" />
        <!--<input name="password" type="password" class="field_sml" onfocus="password.className='field_sml_focus';" onblur="password.className='field_sml';" />-->
        <mn:txt runat="server" id="txtPassword" ResourceConstant="TXT_PASSWORD" />
    </div>
    <div class="field_submit">
        <!--<a href="#" name="submit" class="btn">Member Login</a>-->
        <mn2:FrameworkButton id="Login" CssClass="btn primary wide" runat="server" ResourceConstant="BTN_LOGIN" />
        <asp:CheckBox ID="CheckBoxAutoLogin" Runat="server" Visible="false" />
    </div>

</fieldset>