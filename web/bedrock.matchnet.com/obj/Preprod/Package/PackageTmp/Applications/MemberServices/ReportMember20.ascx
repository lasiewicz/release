﻿<%@ Control Language="c#" AutoEventWireup="false" Codebehind="ReportMember20.ascx.cs" Inherits="Matchnet.Web.Applications.MemberServices.ReportMember20" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>

<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn" TagName="miniProfile" Src="/Framework/UI/BasicElements/MiniProfile20.ascx" %>

<script type="text/javascript">
<!--
function SetSelectedValue(val, complaintVisible) {
    jQuery(".selected-reason-holder").val(val);

    jQuery(".reason-text-isvisible").val(complaintVisible);
    ToggleComplaintVisible();

}

function ToggleComplaintVisible() {
    if (jQuery(".reason-text-isvisible").val() == 'true') {
        ToggleComplaintVisibility('visible', 'block');
    } else {
        ToggleComplaintVisibility('hidden', 'none');
    }
}

function ToggleComplaintVisibility(ComplaintVisibility, ComplaintDisplay) {
    var sComment = document.getElementById("Comment");
    sComment.style.visibility = ComplaintVisibility;
    sComment.style.display = ComplaintDisplay;
}

jQuery(document).ready(function () {
    // if we should bind the radio, do it
    if (jQuery(".selected-reason-holder").val() != '') {
        var selectedVal = jQuery(".selected-reason-holder").val();

        if (jQuery('input[name="rbReportReasons"]').is(':checked') == false) {
            var selectedOption = jQuery('input[name="rbReportReasons"]').filter('[value=' + selectedVal + ']');
            if (selectedOption) {
                selectedOption.attr('checked', true);
            } else {
                jQuery(".selected-reason-holder").val('');
                jQuery(".reason-text-isvisible").val('false');
            }
        }

        ToggleComplaintVisible();
    }
});

//-->
</script>
<div class="page-container">
<h1><mn:txt runat="server" id="txtReportMember" ResourceConstant="TXT_REPORT_A_MEMBER" /></h1>

	<div id="profile-mini-blocked"><mn:miniprofile runat="server" id="MemberMiniProfile" /></div>
	    
	<h3 class="border-btm-dotted"><mn:txt runat="server" id="txtToKeepOurSites" ResourceConstant="TXT_TO_KEEP_OUR_SITES" /></h3>
	
	<%--<asp:RequiredFieldValidator Runat="server" ID="ValidReason" ControlToValidate="rptReportReason" Display="Dynamic" CssClass="Error" />--%>
	<ul class="margin-medium form-radio-list">
	    <asp:Repeater runat="server" ID="rptReportReason" OnItemDataBound="rptReportReason_DataBind">
	        <ItemTemplate>
		    <!--<asp:RadioButtonList Runat="server" ID="rblReportReason" RepeatColumns="2" Width="500" RepeatDirection="Vertical" />-->
		    <li>
		    <asp:PlaceHolder ID="plcReportReason" runat="server">
		        <input type="radio" name="rbReportReasons" value="<%#DataBinder.Eval(Container.DataItem,"Value")%>"  onclick= "<asp:Literal ID="litReportReason" runat="server" />"  /> 
		        </asp:PlaceHolder><label for=""><%# DataBinder.Eval(Container, "DataItem.Content") %></label>
		    </li>
		    </ItemTemplate>
		</asp:Repeater>
		
	</ul>
	<input type="hidden" runat="server" id="hidSelectedReason" class="selected-reason-holder" />
    <input type="hidden" runat="server" id="hidReasonTxtVisible" class="reason-text-isvisible" />
	 <div id="Comment" class="margin-medium" style="display: none;" >
		<p><mn:txt runat="server" id="txtDescribe" ResourceConstant="TXT_PLEASE_DESCRIBE_COMPLAINT" /></p>
        <asp:TextBox Runat="server" ID="txtComplaint" Rows="8" CssClass="form-element-wide" TextMode="MultiLine" />
	</div>
	<div class="border-gen tips">
		<p><mn:txt runat="server" id="txtPleaseNote" ResourceConstant="TXT_PLEASE_NOTE" /></p>
	</div>

	 <div class="margin-medium">
	    <asp:CheckBox Runat="server" ID="chkBlock" /> 
	    <label for=""><mn:txt runat="server" id="txtBlock" ResourceConstant="TXT_ADD_MEMBER_TO_BLOCKED" /></label>
	 </div>
	
	
	<div class="text-center margin-medium">
		<mn2:FrameworkButton id="btnSend" runat="server" ResourceConstant="BTN_SUBMIT" CssClass="btn primary" />
	</div>
</div>
