﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="Suspend20.ascx.cs" Inherits="Matchnet.Web.Applications.MemberServices.Suspend20" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<div id="page-container" class="semantic-html"> 

    
    <asp:Panel ID="divRemoveProfile" runat="server" Visible="true">
	    <mn:Title runat="server" id="ttlHeader" ResourceConstant="TXT_SUSPEND_MEMBERSHIP"/>
	    <asp:Panel ID="divIAPurchase" runat="server" EnableViewState="false" Visible="false">
            <h3><mn:Txt id="txt1" runat="server" resourceconstant="IAP_PURCHASE" /></h3>
        </asp:Panel>
        <h3 class="border-btm-dotted"><mn:Txt id="txtWhySuspend" runat="server" resourceconstant="SUSPEND_INTRO" /></h3>
	    <div id="remove-profile-reasons-container" class="margin-medium"><asp:RadioButtonList id="lstReasons" runat="server"></asp:RadioButtonList></div>
	    <p class="border-top-dotted"><mn:Txt id="txtSuspendDisclaimer" runat="server" resourceconstant="SUSPEND_DISCLAIMER" /></p>
	    <div class="text-center"><mn2:FrameworkButton id="btnSuspend" runat="server" ResourceConstant="BTN_SUSPEND" CssClass="btn primary" />
	    <%--<asp:Button id="btnSuspend" runat="server" Text="Suspend" CssClass="activityButton"></asp:Button>--%>
	    </div>
     </asp:Panel>
</div>
