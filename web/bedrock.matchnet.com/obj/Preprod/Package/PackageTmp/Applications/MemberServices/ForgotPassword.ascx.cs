﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Managers;

namespace Matchnet.Web.Applications.MemberServices
{
    public partial class ForgotPassword : FrameworkControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
             
        }

        private void btnRestPassword_Click(object sender, System.EventArgs e)
        {
            if (!Validate())
                return;

            var success = MemberPasswordManager.Instance.ResetPassword(g.Brand.BrandID, tbxEmailAddress.Text.Trim());
            

            if (success)
            {
                pnlReset.Visible = false;
                pnlResetMsg.Visible = true;

                
            }
            else
            {
                g.Notification.AddMessageString(g.GetResource("RESET_PASSWORD_INSTRUCTIONS_ERROR"));
            }

            
        }

        private bool Validate()
        {
            if(string.IsNullOrEmpty(tbxEmailAddress.Text.Trim()))
            {
                
                g.Notification.AddMessage("ERR_YOU_HAVE_PROVIDED_AN_INVALID_EMAIL_ADDRESS");
                return false;
            }

            var regex = new Regex(@"^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$", RegexOptions.IgnoreCase);
            if(!regex.IsMatch(tbxEmailAddress.Text.Trim()))
            {
                g.LoggingManager.LogInfoMessage("ForgotPassword",
                                          string.Format("Email address validation failed for email:{0}",
                                                        tbxEmailAddress.Text.Trim()));

                g.Notification.AddMessage("ERR_YOU_HAVE_PROVIDED_AN_INVALID_EMAIL_ADDRESS");
                return false;
            }

            return true;
        }

        override protected void OnInit(EventArgs e)
        {
            InitializeComponent();
            base.OnInit(e);
        }
        
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
            btnResetPassword.Click += new System.EventHandler(this.btnRestPassword_Click);
        }
    }
}
