﻿using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;

using Matchnet.Web.Framework;

using Matchnet.ExternalMail.ServiceAdapters;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects;

namespace Matchnet.Web.Applications.MemberServices
{
    public partial class VerifyEmail20 : FrameworkControl
    {
        private void Page_Init(object sender, EventArgs e)
        {
            try
            {
                this.lblError.Visible = false;
                this.litErrorMessage.Text = "";
                btnSave.Click += new EventHandler(btnSave_Click);


                //if (Convert.ToBoolean(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ANALYTICS_OMNITURE", _g.Brand.Site.Community.CommunityID, _g.Brand.Site.SiteID)))
                //{

                //    Matchnet.Web.Analytics.Omniture omniture = (Matchnet.Web.Analytics.Omniture)Page.LoadControl("/Analytics/Omniture.ascx");


                //    PlaceHolder placeHolderAnalytics = new PlaceHolder();

                //    placeHolderAnalytics.Controls.Add(omniture);

                //    this.Controls.Add(placeHolderAnalytics);

                //    _g.AnalyticsOmniture = omniture;



                //}


            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        private void Page_Load(object sender, System.EventArgs e)
        {
            try
            {

                // this.OnPreRender += new EventHandler(OnPreRender);
                bool resendEmail = false;
                if (g.Member != null)
                {
                    int globalStatusMask = g.Member.GetAttributeInt(g.Brand, "GlobalStatusMask", 0);
                    g.AddExpansionToken("CURR_EMAIL_ADDR", _g.Member.EmailAddress);
                    if ((globalStatusMask & WebConstants.EMAIL_VERIFIED) == WebConstants.EMAIL_VERIFIED)
                    {
                        g.Notification.AddMessage("TXT_YOU_HAVE_ALREADY_ACTIVATED_YOUR_MEMBERSHIP");
                        return;
                    }

                    if (txtNewEmail.Text != String.Empty)
                    {
                        g.Member.EmailAddress = txtNewEmail.Text;
                        Member.ServiceAdapters.MemberSA.Instance.SaveMember(g.Member, g.Brand.Site.Community.CommunityID);
                        resendEmail = true;
                    }




                }
                else
                {
                    g.AddExpansionToken("CURR_EMAIL_ADDR", String.Empty);
                }



                if (Request["a"] != null)
                {
                    switch (Request["a"].ToLower())
                    {
                        case "sendemail":
                            if (g.Member != null)
                            {
                                g.Notification.AddMessage("EMAIL_VERIFICATION_LETTER_SENT_TO__S", new string[] { g.Member.EmailAddress });
                                bool res = ExternalMailSA.Instance.SendEmailVerification(g.Member.MemberID, g.Brand.BrandID);
                                if (res)
                                {
                                    g.Session.Add("OmnitureVerifyEmailResend", "true", Matchnet.Session.ValueObjects.SessionPropertyLifetime.Temporary);
                                    //g.AnalyticsOmniture.Events = "";
                                }
                            }
                            break;
                        case "verify":
                            setVerifiedEmail();
                            break;
                    }
                }
                if (resendEmail)
                {
                    if (g.Member != null)
                    {
                        g.Notification.AddMessage("EMAIL_VERIFICATION_LETTER_SENT_TO__S", new string[] { g.Member.EmailAddress });
                        ExternalMailSA.Instance.SendEmailVerification(g.Member.MemberID, g.Brand.BrandID);
                    }
                }
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }


        protected override void OnPreRender(EventArgs e)
        {
        }
        private void setVerifiedEmail()
        {
            try
            {
                string verifyCode = Request["VerifyCode"];
                WebConstants.ForcedBlockingMask blockingMask = WebConstants.ForcedBlockingMask.NotBlocked;
                if (verifyCode != null && verifyCode.Length > 32)
                {
                    string sMemberID = verifyCode.Substring(32);
                    int memberID = Convert.ToInt32(sMemberID);

                    Matchnet.Member.ServiceAdapters.Member member = MemberSA.Instance.GetMember(memberID, MemberLoadFlags.None);
                    if (member == null)
                        return;
                    blockingMask = g.EmailVerify.GetBlockingMaskAttr(member);
                    if (g.VerifyMemberHashCode(verifyCode, memberID, member.EmailAddress))
                    {
                        g.EmailVerify.SetVerifiedMemberAttributes(member);
                        MemberSA.Instance.SaveMember(member);

                        // notify of successful update
                        g.Notification.AddMessage("EMAIL_VERIFIED_", new string[] { g.Session.GetString("EmailAddress", "") });


                        if (!g.EmailVerify.EnableEmailVerification || ((blockingMask & WebConstants.ForcedBlockingMask.BlockedAfterEmailChange) == WebConstants.ForcedBlockingMask.BlockedAfterEmailChange))
                        {
                            g.Session.Add(Web.Applications.MemberProfile.EmailVerifyHelper.SESSION_EMAIL_VERIFY_NOTIFICATION_DISPLAY, "true", Matchnet.Session.ValueObjects.SessionPropertyLifetime.TemporaryDurable);
                            g.Transfer("/Applications/Home/Default.aspx");

                        }
                        else
                        {
                            g.Session.Add(Web.Applications.MemberProfile.EmailVerifyHelper.SESSION_EMAIL_VERIFY_JUST_VERIFIED, "true", Matchnet.Session.ValueObjects.SessionPropertyLifetime.Temporary);
                            g.Transfer("/Applications/MemberServices/VerifyEmailConfirmation.aspx");

                        }
                    }
                    else
                    {
                        // bad email address warning
                        g.Notification.AddMessage("ERR_INVALID_EMAIL_ADDRESS");
                    }
                }
                else
                {
                    // invalid code
                    g.Notification.AddMessage("PROBLEM_READING_VERIFY_CODE");
                }
            }
            catch (Exception ex)
            {
                g.Notification.AddMessage("PROBLEM_READING_VERIFY_CODE");
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
            this.Init += new System.EventHandler(this.Page_Init);
            this.lbutClickHere.Click += new EventHandler(lbutClickHere_Click);

        }
        #endregion

        private void lbutClickHere_Click(object sender, EventArgs e)
        {
            if (g.Member != null)
            {
                g.Transfer("/Applications/MemberServices/VerifyEmail.aspx?a=SendEmail");
            }
            else
            {
                g.Transfer(FrameworkGlobals.LinkHref("/Applications/Logon/Logon.aspx?DestinationURL=/Applications/MemberServices/VerifyEmail.aspx?a=SendEmail", true));
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                int memberID = Member.ServiceAdapters.MemberSA.Instance.GetMemberIDByEmail(txtNewEmail.Text.Trim());
                if (memberID != Constants.NULL_INT)
                {
                    if (memberID != g.Member.MemberID)
                    {
                        RegularExpressionValidatorNewEmailAddress.IsValid = false;
                        return;
                    }
                }

                bool resendEmail = false;
                if (txtNewEmail.Text != String.Empty)
                {
                    g.Member.EmailAddress = txtNewEmail.Text;
                    WebConstants.ForcedBlockingMask memberBlockingMask = g.EmailVerify.GetBlockingMaskAttr(g.Member);
                    WebConstants.EmailVerificationSteps stepMask = WebConstants.EmailVerificationSteps.AfterEmailChange;

                    if ((memberBlockingMask & WebConstants.ForcedBlockingMask.BlockedAfterEmailChange) == WebConstants.ForcedBlockingMask.BlockedAfterEmailChange)
                        stepMask = WebConstants.EmailVerificationSteps.AfterEmailChange;
                    else if ((memberBlockingMask & WebConstants.ForcedBlockingMask.BlockedAfterReg) == WebConstants.ForcedBlockingMask.BlockedAfterReg)
                        stepMask = WebConstants.EmailVerificationSteps.AfterRegistration;



                    if (g.EmailVerify.EnableEmailVerification)
                    {
                        if (g.EmailVerify.IfMustBlock(stepMask) || g.EmailVerify.IfMustHide(stepMask))
                        {
                            g.EmailVerify.SetNotVerifiedMemberAttributes(g.Member, stepMask);

                        }
                    }
                    MemberSaveResult result = Member.ServiceAdapters.MemberSA.Instance.SaveMember(g.Member, g.Brand.Site.Community.CommunityID);


                    if (result.SaveStatus == MemberSaveStatusType.Success)
                    {

                        g.Notification.AddMessage("EMAIL_VERIFICATION_LETTER_SENT_TO__S", new string[] { g.Member.EmailAddress });
                        ExternalMailSA.Instance.SendEmailVerification(g.Member.MemberID, g.Brand.BrandID);
                        g.Transfer("/Applications/Home/default.aspx");
                    }
                    else if (result.SaveStatus == MemberSaveStatusType.EmailAddressExists)
                    {
                        lblError.Text = g.GetResource("ERR_NEW_EMAIL_ADDRESS_EXISTS", this);
                        lblError.Visible = true;
                    }
                    else
                    {
                        // g.Notification.AddError("ERR_NEW_EMAIL_ADDRESS_IS_NOT_VALID");
                        lblError.Text = g.GetResource("ERR_NEW_EMAIL_ADDRESS_IS_NOT_VALID", this);
                        lblError.Visible = true;
                    }


                }
            }
            
        }
    }
}