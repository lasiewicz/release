﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AutoRenewalSettings20.ascx.cs"
    Inherits="Matchnet.Web.Applications.MemberServices.AutoRenewalSettings20" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<div id="page-container">
    <asp:PlaceHolder runat="server" ID="plcAutoRenewalSettings">
        <h1>
            <mn:Txt runat="server" ID="txtAutoRenewalSettings" ExpandImageTokens="True" ResourceConstant="TXT_AUTORENEWAL_SETTINGS" />
        </h1>
        <h3>
            <mn:Txt runat="server" ID="txtAutoRenewalInfo" ExpandImageTokens="True" ResourceConstant="TXT_AUTORENEWAL_INFO" />
        </h3>
        <asp:PlaceHolder ID="phlStandardSubscription" runat="server" Visible="true">
            <!--Base subscription (could be bundle)-->
            <div class="plan-subscription">
                <h3 class="strong">
                    <span>
                        <asp:CheckBox ID="chkEnableAutoRenewal" AutoPostBack="true" runat="server" /></span>
                    <mn:Txt runat="server" ID="txtSubscriptionPlan" ExpandImageTokens="True" ResourceConstant="TXT_SUBSCRIPTION_PLAN" />
                </h3>
            </div>
            <div class="float-outside">
                <h3 class="strong">
                    <mn:Txt runat="server" ID="txtRenewalRate" ExpandImageTokens="True" ResourceConstant="TXT_RENEWAL_RATE" />
                    &nbsp;
                    <mn:Txt runat="server" ID="txtRenewalAmountCurrency" ExpandImageTokens="True" ResourceConstant="TXT_CURRENCY" />
                    &nbsp;
                    <asp:Literal ID="litRenewalAmount" runat="server"></asp:Literal>
                    <span class="sub-copy">
                        <mn:Txt runat="server" ID="txtPerMonth" />
                    </span>
		            <asp:Label EnableViewState="False" Runat="server" ID="RenewalDuration" />
                </h3>
            </div>
            <div>
                <asp:PlaceHolder ID="phlPremiumServicesIncluded" runat="server">
                    <mn:Txt runat="server" ID="txtIncludes" ExpandImageTokens="True" ResourceConstant="TXT_INCLUDES" />
                    <asp:Literal ID="litPremiumServicesIncluded" runat="server"></asp:Literal>
                </asp:PlaceHolder>
                <asp:PlaceHolder ID="phlPremiumServicesIncludedEmpty" runat="server">&nbsp;</asp:PlaceHolder>
            </div>
            <!--A La Carte: Highlight-->
            <asp:PlaceHolder ID="phlALaCarteHighlight" runat="server" Visible="false">
                <div class="plan-subscription">
                    <h3 class="strong">
                        <span>
                            <asp:CheckBox ID="chkALaCarteHighlight" AutoPostBack="true" runat="server" /></span>
                        <mn:Txt runat="server" ID="txtALaCarteHighlightPlan" ExpandImageTokens="True" ResourceConstant="TXT_PROFILE_HIGHLIGHT" />
                    </h3>
                </div>
                <div class="float-outside">
                    <h3 class="strong">
                        <mn:Txt runat="server" ID="txt2" ExpandImageTokens="True" ResourceConstant="TXT_RENEWAL_RATE" />
                        &nbsp;
                        <mn:Txt runat="server" ID="txtALaCarteHighlightRenewalCurrency" ExpandImageTokens="True"
                            ResourceConstant="TXT_CURRENCY" />
                        &nbsp;
                        <asp:Literal ID="litALaCarteHighlightRenewalAmount" runat="server"></asp:Literal>
                        <span class="sub-copy">
                            <mn:Txt runat="server" ID="txt4" />
                        </span>
                    </h3>
                </div>
                <div>
                    <!--Any highlight only text goes here-->
                    &nbsp;
                </div>
            </asp:PlaceHolder>
            <!--A La Carte: Spotlight-->
            <asp:PlaceHolder ID="phlALaCarteSpotlight" runat="server" Visible="false">
                <div class="plan-subscription">
                    <h3 class="strong">
                        <span>
                            <asp:CheckBox ID="chkALaCarteSpotlight" AutoPostBack="true" runat="server" /></span>
                        <mn:Txt runat="server" ID="txtALaCarteSpotlightPlan" ExpandImageTokens="True" ResourceConstant="TXT_MEMBER_SPOTLIGHT" />
                    </h3>
                </div>
                <div class="float-outside">
                    <h3 class="strong">
                        <mn:Txt runat="server" ID="txt3" ExpandImageTokens="True" ResourceConstant="TXT_RENEWAL_RATE" />
                        &nbsp;
                        <mn:Txt runat="server" ID="txtALaCarteSpotlightRenewalCurrency" ExpandImageTokens="True"
                            ResourceConstant="TXT_CURRENCY" />
                        &nbsp;
                        <asp:Literal ID="litALaCarteSpotlightRenewalAmount" runat="server"></asp:Literal>
                        <span class="sub-copy">
                            <mn:Txt runat="server" ID="txt6" />
                        </span>
                    </h3>
                </div>
                <div>
                    <!--Any spotlight only text goes here-->
                    &nbsp;
                </div>
            </asp:PlaceHolder>
            <!--A La Carte: All Access-->
            <asp:PlaceHolder ID="phALaCarteAllAccess" runat="server" Visible="false">
                <div class="plan-subscription">
                    <h3 class="strong">
                        <span>
                            <asp:CheckBox ID="chkALaCarteAllAccess" AutoPostBack="true" runat="server" /></span>
                        <mn:Txt runat="server" ID="txt5" ExpandImageTokens="True" ResourceConstant="TXT_ALLACCESS" />
                    </h3>
                </div>
                <div class="float-outside">
                    <h3 class="strong">
                        <mn:Txt runat="server" ID="txt7" ExpandImageTokens="True" ResourceConstant="TXT_RENEWAL_RATE" />
                        &nbsp;
                        <mn:Txt runat="server" ID="txt8" ExpandImageTokens="True" ResourceConstant="TXT_CURRENCY" />
                        &nbsp;
                        <asp:Literal ID="litALaCarteAllAccessRenewalAmount" runat="server"></asp:Literal>
                        <span class="sub-copy">
                            <mn:Txt runat="server" ID="txt9" />
                        </span>
                    </h3>
                </div>
                <div>
                    <!--Any all access only text goes here-->
                    &nbsp;
                </div>
            </asp:PlaceHolder>
            <!--Renewal date and total-->
            <div class="border-top-dotted clear-both clearfix">
                <p class="float-inside">
                    <mn:Txt runat="server" ID="txtNextRenewalDate" ExpandImageTokens="True" ResourceConstant="TXT_NEXT_RENEWAL_DATE" />
                    <asp:Literal ID="litNextRenewalDate" runat="server"></asp:Literal>
                </p>
                <h3 class="float-outside strong">
                    <mn:Txt runat="server" ID="txtTotal" ExpandImageTokens="True" ResourceConstant="TXT_TOTAL" />
                    <mn:Txt runat="server" ID="txtTotalAmountCurrency" ExpandImageTokens="True" ResourceConstant="TXT_CURRENCY" />
                    &nbsp;<asp:Literal ID="litTotalAmount" runat="server"></asp:Literal>
                    <span class="sub-copy">
                        <mn:Txt runat="server" ID="txtPerMonthTotal" />
                    </span>
                </h3>
            </div>
        </asp:PlaceHolder>
        <asp:PlaceHolder ID="phlFreeTrial" runat="server" Visible="false">
            <div class="plan-subscription">
                <h3 class="strong">
                    <span>
                        <asp:CheckBox ID="chkFreeTrial" AutoPostBack="true" runat="server" /></span>
                    <mn:Txt runat="server" ID="txt10" ExpandImageTokens="True" ResourceConstant="TXT_FREETRIAL" />
                </h3>
            </div>
            <div>
                <!--Any all access only text goes here-->
                &nbsp;
            </div>
        </asp:PlaceHolder>
        <p class="border-top-dotted">
            <mn:Txt runat="server" ID="txtFooterBlurb" ExpandImageTokens="True" ResourceConstant="TXT_FOOTER_BLURB" />
        </p>
        <asp:PlaceHolder ID="phlFooterBlurbALaCarte" runat="server" Visible="false">
            <p>
                <mn:Txt runat="server" ID="txt1" ExpandImageTokens="True" ResourceConstant="TXT_FOOTER_BLURB_ALACARTE" />
            </p>
        </asp:PlaceHolder>
        <p class="text-center">
            <mn2:FrameworkButton ID="btnContinue" OnClick="btnContinue_Click" runat="server"
                ResourceConstant="BTN_CONTINUE" CssClass="btn primary" /></p>
    </asp:PlaceHolder>
    <asp:PlaceHolder runat="server" ID="plcAutoRenewalDisabledOn" Visible="False">
        <div class="auto-renewal-confirm">
            <p><mn:Txt runat="server" ID="txt11" ExpandImageTokens="True" ResourceConstant="TXT_RENEWAL_USER" /><%=g.Member.GetUserName(g.Brand) %>!</p>
            <p><mn:Txt runat="server" ID="txt12" ExpandImageTokens="True" ResourceConstant="TXT_RENEWAL_TEXT_TOP" /></p>
            <p><mn:Txt runat="server" ID="txt13" ExpandImageTokens="True" ResourceConstant="TXT_RENEWAL_PAYMENT" /><br />
                <mn:Txt runat="server" ID="txt14" ExpandImageTokens="True" ResourceConstant="TXT_RENEWAL_AMOUNT" /><%=litTotalAmount.Text%><mn:Txt runat="server" ID="txt19" ExpandImageTokens="True" ResourceConstant="TXT_RENEWAL_NIS" /><br />
                <mn:Txt runat="server" ID="txt15" ExpandImageTokens="True" ResourceConstant="TXT_RENEWAL_DATE" /><%=litNextRenewalDate.Text%>
            </p>
            <mn:Txt runat="server" ID="txt16" ExpandImageTokens="True" ResourceConstant="TXT_RENEWAL_TEXT_BOTTOM" />
        </div>
    </asp:PlaceHolder>
    <asp:PlaceHolder runat="server" ID="plcAutoRenewalDisabledOff" Visible="False">
        <div class="auto-renewal-confirm">
            <p><mn:Txt runat="server" ID="txt17" ExpandImageTokens="True" ResourceConstant="TXT_RENEWAL_USER" /><%=g.Member.GetUserName(g.Brand) %>!</p>
            <p><mn:Txt runat="server" ID="txt18" ExpandImageTokens="True" ResourceConstant="TXT_RENEWAL_OFF_TEXT_TOP" /></p>
            <p><mn:Txt runat="server" ID="txt21" ExpandImageTokens="True" ResourceConstant="TXT_RENEWAL_OFF_DATE" /><%=litNextRenewalDate.Text%></p>
            <mn:Txt runat="server" ID="txt22" ExpandImageTokens="True" ResourceConstant="TXT_RENEWAL_OFF_TEXT_BOTTOM" />
        </div>
    </asp:PlaceHolder>
</div>
