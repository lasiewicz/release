﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;

using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Util;
using Matchnet.Lib;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects;
using Matchnet.Purchase.ServiceAdapters;
using Matchnet.Purchase.ServiceAdapters.Admin;
using Matchnet.Purchase.ValueObjects;
using System.Collections.Generic;
using Spark.Common.Adapter;
using Spark.Common.AuthorizationService;
using Spark.Common.PaymentProfileService;
using Spark.Common.AccessService;
using Matchnet.PromoEngine.ServiceAdapters.TokenReplacement;
using Matchnet.PromoEngine.ValueObjects;
using Matchnet.Web.Framework.Managers;

namespace Matchnet.Web.Applications.MemberServices
{
    public partial class AutoRenewalSettings30 : FrameworkControl
    {
        Matchnet.Member.ServiceAdapters.Member _member = null;
        Matchnet.Content.ValueObjects.BrandConfig.Brand _brand = null;
        Plan _plan = null;
        bool _autoRenewAlreadyOn = false;

        //a la carte
        Plan _planHighlight = null;
        Plan _planSpotlight = null;
        Plan _planJMeter = null;
        Plan _planAllAccess = null;
        Plan _planReadReceipt = null;

        Spark.Common.RenewalService.RenewalSubscriptionDetail _rseHighlight = null;
        Spark.Common.RenewalService.RenewalSubscriptionDetail _rseSpotlight = null;
        Spark.Common.RenewalService.RenewalSubscriptionDetail _rseJMeter = null;
        Spark.Common.RenewalService.RenewalSubscriptionDetail _rseAllAccess = null;
        Spark.Common.RenewalService.RenewalSubscriptionDetail _rseReadReceipt = null;

        bool _hasActiveFreeTrialSubscription = false;

        protected void Page_Load(object sender, EventArgs e)
        {
            this.PopulateRenewalInformation();

            if (g.BreadCrumbTrailHeader != null)
            {
                g.BreadCrumbTrailHeader.SetTwoLinkCrumb(g.GetResource("TXT_AUTORENEWAL_SETTINGS", this),
                                                        g.AppPage.App.DefaultPagePath);

            }
            if (g.BreadCrumbTrailFooter != null)
            {
                g.BreadCrumbTrailFooter.SetTwoLinkCrumb(g.GetResource("TXT_AUTORENEWAL_SETTINGS", this),
                                                       g.AppPage.App.DefaultPagePath);
            }

            // autorenewal disabled
            if (phlStandardSubscription.Visible && SettingsManager.GetSettingBool("DISABLE_AUTO_RENEWAL_CONTROL_ON_SITE", g.Brand))
            {
                plcAutoRenewalSettings.Visible = false;
                litNextRenewalDate.Text = DateTime.Parse(litNextRenewalDate.Text).ToString("MM/dd/yyyy hh:mm",
                                                                                           CultureInfo.InvariantCulture);
                if (hSub.Value == "1")
                {
                    plcAutoRenewalDisabledOn.Visible = true;
                }
                else
                {
                    plcAutoRenewalDisabledOff.Visible = true;
                }
            }
        }

        private void PopulateRenewalInformation()
        {
            try
            {
                _member = g.Member;
                _brand = g.Brand;

                if (_member != null && _brand != null)
                {
                    Spark.Common.RenewalService.RenewalSubscription renewalSub = RenewalManager.Instance.GetRenewalSubscription(_member.MemberID, _brand);

                    if (renewalSub != null && renewalSub.RenewalSubscriptionID > 0)
                    {
                        litNextRenewalDate.Text = renewalSub.RenewalDatePST.ToString();
                        litNextRenewalDateHighlight.Text = renewalSub.RenewalDatePST.ToString();
                        litNextRenewalDateSpotlight.Text = renewalSub.RenewalDatePST.ToString();
                        litNextRenewalDateAllaccess.Text = renewalSub.RenewalDatePST.ToString();
                        litNextRenewalDateReadReceipt.Text = renewalSub.RenewalDatePST.ToString();

                        //base plan
                        _plan = PlanSA.Instance.GetPlan(renewalSub.PrimaryPackageID, _brand.BrandID);

                        if (renewalSub.IsRenewalEnabled)
                        {
                            _autoRenewAlreadyOn = true;
                            txtAutoRenewalSettings.Text = g.GetResource("TXT_AUTORENEWAL_SETTINGS_CANCEL", this);

                            //show premium services  panel . 
                            pnlShowPremiumservices.Visible = true;
                            txtShowPremiumservices.Visible = true;
                        }
                        else
                            txtAutoRenewalSettings.Text = g.GetResource("TXT_AUTORENEWAL_SETTINGS_RENEW_SUB", this);
                        
                        //a la carte plan(s)
                        if (renewalSub.RenewalSubscriptionDetails.Length > 1)
                        {
                            
                            Plan rsePlan = null;
                            foreach (Spark.Common.RenewalService.RenewalSubscriptionDetail rse in renewalSub.RenewalSubscriptionDetails)
                            {
                                if (!rse.IsPrimaryPackage)
                                {
                                    rsePlan = PlanSA.Instance.GetPlan(rse.PackageID, _brand.BrandID);
                                    if ((rsePlan.PremiumTypeMask & PremiumType.HighlightedProfile) == PremiumType.HighlightedProfile)
                                    {
                                        _planHighlight = rsePlan;
                                        _rseHighlight = rse;
                                        phlALaCarteHighlight.Visible = true;
                                        phlFooterBlurbALaCarte.Visible = true;
                                    }
                                    else if ((rsePlan.PremiumTypeMask & PremiumType.SpotlightMember) == PremiumType.SpotlightMember)
                                    {
                                        _planSpotlight = rsePlan;
                                        _rseSpotlight = rse;
                                        phlALaCarteSpotlight.Visible = true;
                                        phlFooterBlurbALaCarte.Visible = true;
                                    }
                                    else if ((rsePlan.PremiumTypeMask & PremiumType.JMeter) == PremiumType.JMeter)
                                    {
                                        //091310 TL
                                        //TL: Noticed that this member's auto-renewal page does not have JMeter option for user to turn off.
                                        //I will not add that to the UI for user since I didn't see it here originally
                                        //but the admin's page does so I added it here to support UPS Renewal changes so it does not disable JMeter
                                        _planJMeter = rsePlan;
                                        _rseJMeter = rse;
                                    }
                                    else if ((rsePlan.PremiumTypeMask & PremiumType.AllAccess) == PremiumType.AllAccess)
                                    {
                                        _planAllAccess = rsePlan;
                                        _rseAllAccess = rse;
                                        phALaCarteAllAccess.Visible = true;
                                        phlFooterBlurbALaCarte.Visible = true;
                                    }
                                    else if ((rsePlan.PremiumTypeMask & PremiumType.ReadReceipt) == PremiumType.ReadReceipt)
                                    {
                                        _planReadReceipt = rsePlan;
                                        _rseReadReceipt = rse;
                                        phALaCarteReadReceipt.Visible = true;
                                        phlFooterBlurbALaCarte.Visible = true;
                                    }
                                }
                            }
                        }
                        else
                        {
                            pnlShowPremiumservices.Visible = false;
                            txtShowPremiumservices.Visible = false;
                        }

                        //determine whether various subscriptions are selected
                        if (!Page.IsPostBack)
                        {
                            //base sub
                            if (renewalSub.IsRenewalEnabled)
                            {

                                btnSub.Text = g.GetResource("BTN_CANCEL_SUBSCIPTION", this);
                                hSub.Value = "1";
                            }
                            else
                            {
                                btnSub.Text = g.GetResource("BTN_REACTIVATE_SUBSCRIPTION", this);
                                hSub.Value = "0";
                            }

                            //a la carte subs
                            if (_rseHighlight != null && _rseHighlight.IsRenewalEnabled)
                            {
                                btnHighlight.Text = g.GetResource("BTN_CANCEL_SUBSCIPTION", this);
                                hHighlight.Value = "1";
                            }
                            else
                            {
                                btnHighlight.Text = g.GetResource("BTN_REACTIVATE_SUBSCRIPTION", this);
                                hHighlight.Value = "0";
                            }


                            if (_rseSpotlight != null && _rseSpotlight.IsRenewalEnabled)
                            {
                                btnSpotlight.Text = g.GetResource("BTN_CANCEL_SUBSCIPTION", this);
                                hSpotlight.Value = "1";
                            }
                            else
                            {
                                btnSpotlight.Text = g.GetResource("BTN_REACTIVATE_SUBSCRIPTION", this);
                                hSpotlight.Value = "0";
                            }


                            if (_rseAllAccess != null && _rseAllAccess.IsRenewalEnabled)
                            {

                                btnAllAccess.Text = g.GetResource("BTN_CANCEL_SUBSCIPTION", this);
                                hAllAccess.Value = "1";
                            }
                            else
                            {
                                btnAllAccess.Text = g.GetResource("BTN_REACTIVATE_SUBSCRIPTION", this);
                                hAllAccess.Value = "0";
                            }

                            if (_rseReadReceipt != null && _rseReadReceipt.IsRenewalEnabled)
                            {

                                btnReadReceipt.Text = g.GetResource("BTN_CANCEL_SUBSCIPTION", this);
                                hReadReceipt.Value = "1";
                            }
                            else
                            {
                                btnReadReceipt.Text = g.GetResource("BTN_REACTIVATE_SUBSCRIPTION", this);
                                hReadReceipt.Value = "0";
                            }                        
                        }
                        else
                        {
                            // Need to manually load the postback data for the checkbox control since
                            // viewstate is disabled  

                            string hiddenValue = String.Empty;


                            hiddenValue = Request.Form[hSub.UniqueID.ToString()];
                            if (hiddenValue != null)
                            {
                                hSub.Value = hiddenValue;
                            }

                            //a la carte subs
                            hiddenValue = Request.Form[hHighlight.UniqueID.ToString()];
                            if (hiddenValue != null)
                            {
                                hHighlight.Value = hiddenValue;
                                btnHighlight.Text = (hiddenValue == "1") ? g.GetResource("BTN_CANCEL_SUBSCIPTION", this) : g.GetResource("BTN_REACTIVATE_SUBSCRIPTION", this);

                            }


                            hiddenValue = Request.Form[hSpotlight.UniqueID.ToString()];
                            if (hiddenValue != null)
                            {
                                hSpotlight.Value = hiddenValue;
                                btnSpotlight.Text = (hiddenValue == "1") ? g.GetResource("BTN_CANCEL_SUBSCIPTION", this) : g.GetResource("BTN_REACTIVATE_SUBSCRIPTION", this);
                            }


                            hiddenValue = Request.Form[hAllAccess.UniqueID.ToString()];
                            if (hiddenValue != null)
                            {
                                hAllAccess.Value = hiddenValue;
                                btnAllAccess.Text = (hiddenValue == "1") ? g.GetResource("BTN_CANCEL_SUBSCIPTION", this) : g.GetResource("BTN_REACTIVATE_SUBSCRIPTION", this);
                            }

                            hiddenValue = Request.Form[hReadReceipt.UniqueID.ToString()];
                            if (hiddenValue != null)
                            {
                                hReadReceipt.Value = hiddenValue;
                                btnReadReceipt.Text = (hiddenValue == "1") ? g.GetResource("BTN_CANCEL_SUBSCIPTION", this) : g.GetResource("BTN_REACTIVATE_SUBSCRIPTION", this);
                            }
                        }

                        if (_plan != null)
                        {
                            int primaryPackagRenewalDurationInMonths = _plan.RenewDuration;

                            //display individual renewal amounts
                            litRenewalAmount.Text = (hSub.Value == "1") ? Convert.ToString(_plan.RenewCost).Trim() : "0.00";


                            decimal decALaCarteHighlightRenewalAmount = 0.0m;
                            if (hHighlight.Value == "1" && (_planHighlight != null))
                            {
                                decALaCarteHighlightRenewalAmount = _planHighlight.RenewCost * (primaryPackagRenewalDurationInMonths / _planHighlight.RenewDuration);
                            }
                            litALaCarteHighlightRenewalAmount.Text = Convert.ToString(decALaCarteHighlightRenewalAmount).Trim();


                            decimal decALaCarteSpotlightRenewalAmount = 0.0m;
                            if (hSpotlight.Value == "1" && (_planSpotlight != null))
                            {
                                decALaCarteSpotlightRenewalAmount = _planSpotlight.RenewCost * (primaryPackagRenewalDurationInMonths / _planSpotlight.RenewDuration);
                            }
                            litALaCarteSpotlightRenewalAmount.Text = Convert.ToString(decALaCarteSpotlightRenewalAmount).Trim();


                            decimal decALaCarteAllAccessRenewalAmount = 0.0m;
                            if (hAllAccess.Value == "1" && (_planAllAccess != null))
                            {
                                decALaCarteAllAccessRenewalAmount = _planAllAccess.RenewCost * (primaryPackagRenewalDurationInMonths / _planAllAccess.RenewDuration);
                            }
                            litALaCarteAllAccessRenewalAmount.Text = Convert.ToString(decALaCarteAllAccessRenewalAmount).Trim();

                            decimal decALaCarteReadReceiptRenewalAmount = 0.0m;
                            if (hReadReceipt.Value == "1" && (_planReadReceipt != null))
                            {
                                decALaCarteReadReceiptRenewalAmount = _planReadReceipt.RenewCost * (primaryPackagRenewalDurationInMonths / _planReadReceipt.RenewDuration);
                            }
                            litALaCarteReadReceiptRenewalAmount.Text = Convert.ToString(decALaCarteReadReceiptRenewalAmount).Trim();

                            //display bundled plan info
                            if (_plan.PremiumTypeMask != PremiumType.None)
                            {
                                phlPremiumServicesIncluded.Visible = true;
                                phlPremiumServicesIncludedEmpty.Visible = false;
                                bool anotherPremiumService = false;

                                StringBuilder sbPremiumServices = new StringBuilder(g.GetResource("TXT_WITH", this));
                                if ((_plan.PremiumTypeMask & PremiumType.HighlightedProfile) == PremiumType.HighlightedProfile)
                                {
                                    sbPremiumServices.Append(" " + g.GetResource("TXT_PROFILE_HIGHLIGHT", this));
                                    anotherPremiumService = true;
                                }
                                else
                                {
                                    anotherPremiumService = false;
                                }

                                if ((_plan.PremiumTypeMask & PremiumType.SpotlightMember) == PremiumType.SpotlightMember)
                                {
                                    if (anotherPremiumService)
                                    {
                                        sbPremiumServices.Append(g.GetResource("TXT_AND", this));
                                    }
                                    sbPremiumServices.Append(" " + g.GetResource("TXT_MEMBER_SPOTLIGHT", this));
                                    anotherPremiumService = true;
                                }
                                else
                                {
                                    anotherPremiumService = false;
                                }

                                if ((_plan.PremiumTypeMask & PremiumType.AllAccess) == PremiumType.AllAccess)
                                {
                                    if (anotherPremiumService)
                                    {
                                        sbPremiumServices.Append(g.GetResource("TXT_AND", this));
                                    }
                                    sbPremiumServices.Append(" " + g.GetResource("TXT_ALLACCESS", this));
                                    anotherPremiumService = true;
                                }
                                else
                                {
                                    anotherPremiumService = false;
                                }

                                if ((_plan.PremiumTypeMask & PremiumType.ReadReceipt) == PremiumType.ReadReceipt)
                                {
                                    if (anotherPremiumService)
                                    {
                                        sbPremiumServices.Append(g.GetResource("TXT_AND", this));
                                    }
                                    sbPremiumServices.Append(" " + g.GetResource("TXT_READRECEIPT", this));
                                    anotherPremiumService = true;
                                }
                                else
                                {
                                    anotherPremiumService = false;
                                }

                                litPremiumServicesIncluded.Text = sbPremiumServices.ToString();
                            }
                            else
                            {
                                phlPremiumServicesIncluded.Visible = false;
                                phlPremiumServicesIncludedEmpty.Visible = true;
                            }

                            // Display renewal duration
                            string resource = string.Empty;
                            string resourceConstantToUse = string.Empty;
                            if (primaryPackagRenewalDurationInMonths == 1)
                            {
                                resourceConstantToUse = "SITE_PLAN_MONTLY_RENEWAL_ADD_TEMPLATE_DEFAULT";
                                resource = g.GetResource(resourceConstantToUse, this);
                            }
                            else
                            {
                                resourceConstantToUse = "SITE_PLAN_MULTI_MONTH_RENEWAL_ADD_TEMPLATE_DEFAULT";
                                resource = String.Format(g.GetResource(resourceConstantToUse, this), primaryPackagRenewalDurationInMonths);
                            }

                            txtPerMonth.Text = resource;
                            txt4.Text = resource;
                            txt6.Text = resource;
                            txt9.Text = resource;
                            txtPerMonthTotal.Text = resource;
                        }
                        else
                        {
                            g.Notification.AddMessage("NO_PLAN");
                        }
                    }

                    // Check if this member has an active free trial subscription
                    try
                    {
                        this._hasActiveFreeTrialSubscription = AuthorizationServiceWebAdapter.GetProxyInstance().HasActiveFreeTrialSubscription(g.TargetMemberID, g.TargetBrand.Site.SiteID);

                        if (this._hasActiveFreeTrialSubscription)
                        {
                            phlFreeTrial.Visible = true;
                            phlStandardSubscription.Visible = false;
                            chkFreeTrial.Checked = true;
                            txtAutoRenewalSettings.Text = g.GetResource("TXT_AUTORENEWAL_SETTINGS_CANCEL", this);
                        }
                        else
                        {
                            phlFreeTrial.Visible = false;
                            //g.Notification.AddMessage("NO_SUBSCRIPTION");
                        }

                    }
                    catch (Exception ex)
                    {
                        System.Diagnostics.Trace.WriteLine("Error in checking to see if the member has an active free trial subscription, Error message: " + ex.Message);
                    }
                    finally
                    {
                        AuthorizationServiceWebAdapter.CloseProxyInstance();
                    }

                    
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.Write("Error in AutoRenewalSettings.PopulateRenewalInformation(), " + ex.ToString());

                g.ProcessException(ex);
            }
        }

        
        protected void btnSub_Click(object sender, EventArgs e)
        {
            if (_plan == null && !_hasActiveFreeTrialSubscription)
            {
                // Member is not subscribed but receives subscription time from administrative
                // adjustment  
                // Plan ID is 0 since the member has never subscribed before
                // If the member has subscribed before, then the Plan ID is taken from his
                // last subscription for any administrative adjustments. 
                g.Notification.Clear();
                g.Notification.AddError("NO_PLAN_TO_AUTO_RENEWAL");
            }
            else
            {

                if (hSub.Value == "0")
                {
                    bool displayRenewalsAlreadyActive = true;
                    bool displayRenewalSuccess = false;
                    bool displayRenewalError = false;

                    //Check Base Sub
                    bool reenableBaseSub = !_autoRenewAlreadyOn;



                    //Enable auto-renewal - update UPS
                    if (reenableBaseSub)
                    {
                        var secondaryPackageIDToEnable = new List<int>();

                        Spark.Common.RenewalService.RenewalResponse renewalResponse = RenewalServiceWebAdapter.GetProxyInstanceForBedrock().EnableAutoRenewal(secondaryPackageIDToEnable.ToArray(), g.Member.MemberID, g.Brand.Site.SiteID, 1, Constants.NULL_INT, "", Constants.NULL_INT);

                        if (renewalResponse.InternalResponse.responsecode != "0")
                        {
                            g.Notification.AddError("UNABLE_TO_UPDATE_RENEWAL");
                        }
                        else
                        {
                            litRenewalAmount.Text = Convert.ToString(_plan.RenewCost).Trim();
                            hSub.Value = "1";
                            g.Session.Add("AUTO_RENEW", "autorenon",
                                          Matchnet.Session.ValueObjects.SessionPropertyLifetime.Temporary);

                            // update the AutoRenewalStatus, so the Reactivate Link behaves correctly if it's enabled
                            // for the site.
                            g.Member.SetAttributeInt(_g.Brand, "AutoRenewalStatus", 1);
                            MemberSA.Instance.SaveMember(g.Member);

                            g.Notification.AddMessage("TXT_REACTIVATED_AUTORENEWAL");

                            pnlShowPremiumservices.Visible = true;
                            txtShowPremiumservices.Visible = true;
                        }

                    }
                }
                else
                {

                    //this will disable auto-renewal for base subscription and all a la carte
                    g.Session.Add("AUTO_RENEW", "autorenoff", Matchnet.Session.ValueObjects.SessionPropertyLifetime.Temporary);
                    g.Transfer("/Applications/Termination/TerminateReason.aspx");

                }
            }
        }

        /// <summary>
        ///  //Highlight (enable or disable)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnHighlight_Click(object sender, EventArgs e)
        {

            if (_plan != null && _rseHighlight != null)
            {
                //Check A la carte
                List<int> secondaryPackageIDs = new List<int>();
                secondaryPackageIDs.Add(_rseHighlight.PackageID);
                bool status = false;
                decimal decALaCarteHighlightRenewalAmount = 0.0m;

                if (hHighlight.Value == "0" && !_rseHighlight.IsRenewalEnabled)
                {
                    status = EnableAlacartRenewals(secondaryPackageIDs);

                    if (status)
                    {
                        btnHighlight.Text = g.GetResource("BTN_CANCEL_SUBSCIPTION", this);
                        g.Notification.AddMessage("TXT_REACTIVATED_PREMIUM_SERVICE_HIGHLIGHT");

                        hHighlight.Value = "1";

                        if (_planHighlight != null)
                        {
                            decALaCarteHighlightRenewalAmount = _planHighlight.RenewCost * (_plan.RenewDuration / _planHighlight.RenewDuration);
                            litALaCarteHighlightRenewalAmount.Text = Convert.ToString(decALaCarteHighlightRenewalAmount).Trim();
                        }

                    }
                    else
                    {
                        g.Notification.AddError("UNABLE_TO_UPDATE_RENEWAL");
                    }

                }
                else if (hHighlight.Value == "1" && _rseHighlight.IsRenewalEnabled)
                {
                    status = DisableAlacartRenewals(secondaryPackageIDs);

                    if (status)
                    {
                        btnHighlight.Text = g.GetResource("BTN_REACTIVATE_SUBSCRIPTION", this);
                        g.Notification.AddMessage("TXT_CANCEL_PREMIUM_SERVICE_HIGHLIGHT");
                        litALaCarteHighlightRenewalAmount.Text = Convert.ToString(decALaCarteHighlightRenewalAmount).Trim();
                        hHighlight.Value = "0";
                    }
                    else
                    {
                        g.Notification.AddError("UNABLE_TO_UPDATE_RENEWAL");
                    }
                }

            }
        }


        protected void btnSpotlight_Click(object sender, EventArgs e)
        {
            //Spotlight (enable or disable)
            if (_plan != null && _rseSpotlight != null)
            {
                List<int> secondaryPackageIDs = new List<int>();
                secondaryPackageIDs.Add(_rseSpotlight.PackageID);
                bool status = false;
                decimal decALaCarteSpotlightRenewalAmount = 0.0m;


                if (hSpotlight.Value == "0" && !_rseSpotlight.IsRenewalEnabled)
                {
                    status = EnableAlacartRenewals(secondaryPackageIDs);

                    if (status)
                    {
                        btnSpotlight.Text = g.GetResource("BTN_CANCEL_SUBSCIPTION", this);
                        g.Notification.AddMessage("TXT_REACTIVATED_PREMIUM_SERVICE_SPOTLIGHT");

                        hSpotlight.Value = "1";

                        if (_planSpotlight != null)
                        {
                            decALaCarteSpotlightRenewalAmount = _planSpotlight.RenewCost * (_plan.RenewDuration / _planSpotlight.RenewDuration);
                            litALaCarteSpotlightRenewalAmount.Text = Convert.ToString(decALaCarteSpotlightRenewalAmount).Trim();
                        }

                    }
                    else
                    {
                        g.Notification.AddError("UNABLE_TO_UPDATE_RENEWAL");
                    }

                }
                else if (hSpotlight.Value == "1" && _rseSpotlight.IsRenewalEnabled)
                {
                    status = DisableAlacartRenewals(secondaryPackageIDs);

                    if (status)
                    {
                        btnSpotlight.Text = g.GetResource("BTN_REACTIVATE_SUBSCRIPTION", this);
                        g.Notification.AddMessage("TXT_CANCEL_PREMIUM_SERVICE_SPOTLIGHT");
                        litALaCarteSpotlightRenewalAmount.Text = Convert.ToString(decALaCarteSpotlightRenewalAmount).Trim();
                        hSpotlight.Value = "0";
                    }
                    else
                    {
                        g.Notification.AddError("UNABLE_TO_UPDATE_RENEWAL");
                    }
                }

            }
        }

        protected void btnAA_Click(object sender, EventArgs e)
        {
            //All Access (enable or disable)
            if (_plan != null && _rseAllAccess != null)
            {
                List<int> secondaryPackageIDs = new List<int>();
                secondaryPackageIDs.Add(_rseAllAccess.PackageID);
                bool status = false;
                decimal decALaCarteAllAccessAmount = 0.0m;

                if (hAllAccess.Value == "0" && !_rseAllAccess.IsRenewalEnabled)
                {
                    status = EnableAlacartRenewals(secondaryPackageIDs);

                    if (status)
                    {
                        btnAllAccess.Text = g.GetResource("BTN_CANCEL_SUBSCIPTION", this);
                        g.Notification.AddMessage("TXT_REACTIVATED_PREMIUM_SERVICE_ALLACCESS");

                        hAllAccess.Value = "1";

                        if (_planAllAccess != null)
                        {
                            decALaCarteAllAccessAmount = _planAllAccess.RenewCost * (_plan.RenewDuration / _planAllAccess.RenewDuration);
                            litALaCarteAllAccessRenewalAmount.Text = Convert.ToString(decALaCarteAllAccessAmount).Trim();
                        }
                    }
                    else
                    {
                        g.Notification.AddError("UNABLE_TO_UPDATE_RENEWAL");
                    }
                }
                else if (hAllAccess.Value == "1" && _rseAllAccess.IsRenewalEnabled)
                {
                    status = DisableAlacartRenewals(secondaryPackageIDs);

                    if (status)
                    {
                        btnAllAccess.Text = g.GetResource("BTN_REACTIVATE_SUBSCRIPTION", this);
                        g.Notification.AddMessage("TXT_CANCEL_PREMIUM_SERVICE_ALLACCESS");
                        litALaCarteAllAccessRenewalAmount.Text = Convert.ToString(decALaCarteAllAccessAmount).Trim();
                        hAllAccess.Value = "0";
                    }
                    else
                    {
                        g.Notification.AddError("UNABLE_TO_UPDATE_RENEWAL");
                    }
                }

            }
        }

        protected void btnReadReceipt_Click(object sender, EventArgs e)
        {
            // Enable or disable Read Receipt renewal
            if (_plan != null && _rseReadReceipt != null)
            {
                List<int> secondaryPackageIDs = new List<int>();
                secondaryPackageIDs.Add(_rseReadReceipt.PackageID);
                bool status = false;
                decimal decALaCarteReadReceiptAmount = 0.0m;

                if (hReadReceipt.Value == "0" && !_rseReadReceipt.IsRenewalEnabled)
                {
                    status = EnableAlacartRenewals(secondaryPackageIDs);

                    if (status)
                    {
                        btnReadReceipt.Text = g.GetResource("BTN_CANCEL_SUBSCIPTION", this);
                        g.Notification.AddMessage("TXT_REACTIVATED_PREMIUM_SERVICE_READRECEIPT");

                        hReadReceipt.Value = "1";

                        if (_planReadReceipt != null)
                        {
                            decALaCarteReadReceiptAmount = _planReadReceipt.RenewCost * (_plan.RenewDuration / _planReadReceipt.RenewDuration);
                            litALaCarteReadReceiptRenewalAmount.Text = Convert.ToString(decALaCarteReadReceiptAmount).Trim();
                        }
                    }
                    else
                    {
                        g.Notification.AddError("UNABLE_TO_UPDATE_RENEWAL");
                    }
                }
                else if (hReadReceipt.Value == "1" && _rseReadReceipt.IsRenewalEnabled)
                {
                    status = DisableAlacartRenewals(secondaryPackageIDs);

                    if (status)
                    {
                        btnReadReceipt.Text = g.GetResource("BTN_REACTIVATE_SUBSCRIPTION", this);
                        g.Notification.AddMessage("TXT_CANCEL_PREMIUM_SERVICE_READRECEIPT");
                        litALaCarteReadReceiptRenewalAmount.Text = Convert.ToString(decALaCarteReadReceiptAmount).Trim();
                        hReadReceipt.Value = "0";
                    }
                    else
                    {
                        g.Notification.AddError("UNABLE_TO_UPDATE_RENEWAL");
                    }
                }
            }
        }

        #region helpers

        private bool EnableAlacartRenewals(List<int> ids)
        {
            bool status = false;

            if (ids.Count > 0)
            {

                try
                {
                    Spark.Common.RenewalService.RenewalResponse renewalResponse = RenewalServiceWebAdapter.GetProxyInstanceForBedrock().EnableALaCarteAutoRenewal(ids.ToArray(), g.Member.MemberID, g.Brand.Site.SiteID,
                                1, Constants.NULL_INT, "", Constants.NULL_INT);

                    status = (renewalResponse.InternalResponse.responsecode == "0");
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Trace.Write("Error in AutoRenewalSettings.enableAlacartRenewals(), " + ex.ToString());

                    g.ProcessException(ex);
                }


            }

            return status;
        }

        private bool DisableAlacartRenewals(List<int> ids)
        {
            bool status = false;

            if (ids.Count > 0)
            {
                try
                {
                    Spark.Common.RenewalService.RenewalResponse renewalResponse = RenewalServiceWebAdapter.GetProxyInstanceForBedrock().DisableALaCarteAutoRenewal(ids.ToArray(), g.Member.MemberID, g.Brand.Site.SiteID,
                        1, Constants.NULL_INT, "", Constants.NULL_INT);

                    status = (renewalResponse.InternalResponse.responsecode == "0");
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Trace.Write("Error in AutoRenewalSettings.disableAlacartRenewals(), " + ex.ToString());

                    g.ProcessException(ex);
                }
            }

            return status;
        }

        #endregion

        protected void chkFreeTrial_CheckedChanged(object sender, EventArgs e)
        {
            if (!chkFreeTrial.Checked)
            {
                // Get the active free trial subscription for this member 
                AuthorizationSubscription freeTrialSubscription = null;
                bool hasActiveFreeTrial = false;

                try
                {
                    PaymentProfileInfo defaultPaymentProfile = PaymentProfileServiceWebAdapter.GetProxyInstance().GetMemberDefaultPaymentProfile(g.TargetMemberID, g.Brand.Site.SiteID);

                    if (defaultPaymentProfile != null)
                    {
                        freeTrialSubscription = AuthorizationServiceWebAdapter.GetProxyInstance().GetActiveAuthorizationSubscription(defaultPaymentProfile.PaymentProfileID, g.TargetMemberID, g.Brand.Site.SiteID);
                    }
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Trace.WriteLine("Error in getting the free trial subscription details, Error message: " + ex.Message);
                }
                finally
                {
                    PaymentProfileServiceWebAdapter.CloseProxyInstance();
                    AuthorizationServiceWebAdapter.CloseProxyInstance();
                }

                if (freeTrialSubscription != null)
                {
                    if (freeTrialSubscription.CaptureDateUTC >= DateTime.UtcNow)
                    {
                        hasActiveFreeTrial = true;
                    }
                }

                if (hasActiveFreeTrial)
                {
                    AccessFreeTrial accessFreeTrial = new AccessFreeTrial();
                    accessFreeTrial.AuthorizationSubscriptionID = freeTrialSubscription.AuthorizationSubscriptionID;
                    accessFreeTrial.AuthorizationCode = freeTrialSubscription.AuthorizationCode;
                    accessFreeTrial.AuthorizationChargeID = freeTrialSubscription.AuthorizationChargeID;
                    accessFreeTrial.AuthorizationAmount = freeTrialSubscription.AuthorizationAmount;
                    accessFreeTrial.PaymentProfileID = freeTrialSubscription.PaymentProfileID;
                    accessFreeTrial.PaymentType = "CreditCard";
                    accessFreeTrial.CustomerID = freeTrialSubscription.CustomerID;
                    accessFreeTrial.CallingSystemID = freeTrialSubscription.CallingSystemID;
                    accessFreeTrial.CallingSystemTypeID = 2;
                    accessFreeTrial.CaptureDate = freeTrialSubscription.CaptureDateUTC;
                    accessFreeTrial.CaptureAttempt = freeTrialSubscription.CaptureAttempt;
                    accessFreeTrial.AuthorizationOnlyPackageID = freeTrialSubscription.PrimaryPackageID;
                    accessFreeTrial.SecondaryAuthorizationOnlyPackageID = freeTrialSubscription.AllSecondaryPackageID;
                    accessFreeTrial.AdminID = 0;
                    accessFreeTrial.AdminUserName = String.Empty;
                    accessFreeTrial.TransactionType = Spark.Common.AccessService.TransactionType.CancelFreeTrial;
                    accessFreeTrial.AuthorizationStatus = Spark.Common.AccessService.AuthorizationStatus.Successful;
                    accessFreeTrial.ConvertedOrderID = 0;
                    accessFreeTrial.AccessReasonID = 0;
                    accessFreeTrial.UnifiedActionTypeID = 2; // Remove privileges

                    AccessResponse accessResponse = AccessServiceWebAdapter.GetProxyInstance().AdjustAuthorizationOnlyPrivilege(accessFreeTrial);

                    if (accessResponse.InternalResponse.responsecode == "0")
                    {
                        g.Notification.AddMessage("FREETRIAL_UPDATED_SUCCESSFULLY");
                        chkFreeTrial.Checked = false;
                        phlFreeTrial.Visible = false;

                    }
                    else
                    {
                        g.Notification.AddError("FREETRIAL_UPDATE_ERROR");
                    }
                }
                else
                {
                    g.Notification.AddError("FREETRIAL_NO_LONGER_ACTIVE");
                }
                //g.Transfer("/Applications/Termination/TerminateReason.aspx");

            }
            
        }
    }
}