﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Managers;

namespace Matchnet.Web.Applications.MemberServices
{
    public partial class ForgotPasswordSetPassword : FrameworkControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // token exists?
            if(Request["resettoken"] == null)
            {
                g.Transfer("/Applications/Home/Default.aspx");
            }

            // if exists, check to see if it's still valid
            var tokenGuidString = Request["resettoken"].ToString();
            if(MemberPasswordManager.Instance.ValidateResetPasswordToken(HttpUtility.UrlDecode(tokenGuidString)) <= 0)
            {
                //g.Transfer("/Applications/Logon/Logon.aspx?rc=RESET_PASSWORD_TOKEN_EXIRED");

                pnlReset.Visible = false;
                pnlResetMsg.Visible = true;

            }
        }

        private void btnChangePassword_Click(object sender, System.EventArgs e)
        {

            if (!Validate())
                return;

            // if pass, change the member's password and send them to the logon page
            var tokenGuidString = Request["resettoken"].ToString();
            var memberId =
                MemberPasswordManager.Instance.ValidateResetPasswordToken(HttpUtility.UrlDecode(tokenGuidString));
            var member = MemberSA.Instance.GetMember(memberId, MemberLoadFlags.None);
            
            if(member == null)
                return;

            if(MemberPasswordManager.Instance.ChangePassword(member, g.Brand, tbxPassword.Text.Trim(),
                                                          g.GetResource("TXT_PASSWORD", this)))
            {
                g.Transfer("/Applications/Logon/Logon.aspx?rc=PASSWORD_HAS_BEEN_CHANGED");
            }
            else
            {
                g.Notification.AddMessage("PASSWORD_CHANGE_ERROR");
            }
        }

        private bool Validate()
        {
            if(string.IsNullOrEmpty(tbxPassword.Text.Trim()) || string.IsNullOrEmpty(tbxConfirmPassword.Text.Trim()))
            {
                g.Notification.AddMessage("BOTH_PASSWORDS_ARE_REQUIRED");
                return false;
            }

            // check to see if the password match
            if (tbxPassword.Text.Trim() != tbxConfirmPassword.Text.Trim())
            {
                g.Notification.AddMessage("PASSWORDS_DO_NOT_MATCH");
                return false;
            }

            // make sure password meets our current requirment for strength
            if (!MemberPasswordManager.Instance.ValidatePasswordStrength(tbxPassword.Text.Trim()))
            {
                g.Notification.AddMessage("PASSWORD_NOT_STRONG_ENOUGH");
                return false;
            }

            return true;
        }

        override protected void OnInit(EventArgs e)
        {
            InitializeComponent();
            base.OnInit(e);
        }

        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
            btnChangePassword.Click += new System.EventHandler(this.btnChangePassword_Click);
        }
    }
}
