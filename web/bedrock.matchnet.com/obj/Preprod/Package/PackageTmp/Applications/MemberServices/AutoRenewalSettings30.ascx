﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AutoRenewalSettings30.ascx.cs" Inherits="Matchnet.Web.Applications.MemberServices.AutoRenewalSettings30" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<div id="page-container">
    <script type="text/javascript">
        $j(document).ready(function () {

            $j("input[type='submit']").each(function () {
                if ($j(this).val() === "Reactivate Subscription") {
                    $j(this).addClass("reactivate");
                }
            });
            $j('.txtShowPremiumservices').click(function (event) {

                if ($j('.pnlShowPremiumservices').is(":visible")) {
                    $j('.pnlShowPremiumservices').hide();
                    $j('.txtShowPremiumservices').html('Edit my add-on features');
                } else {
                    $j('.pnlShowPremiumservices').show();
                    $j('.txtShowPremiumservices').html('Hide my add-on features');
                }

            });

        });
    </script>
     <style>
         
         #page-container 
         {
            padding: 0 30px;
         }
         
         .pnlShowPremiumservices {
             display: none;
             clear: both;
             
         }
         
         .txtShowPremiumservices {
             text-decoration: underline;
             color: blue;
             cursor: pointer;
             clear: both;
             float: left;
         }
         
         .plan-subscription {
             clear: both;
             width: 500px;
             margin-top: 50px;
            margin-bottom: 20px;
         }
         
         .arLabel {
             font-weight: bold;
         }
         .plan-alc {
             height: 50px;
             clear: both;
             width: 500px;
         }
         
        
        .dl-form dt
        {
            display: inline-block;
            float: left;
            width: 100px;
        }

        .dl-form dd
        {
            display: inline-block;
            float: left;
            width: 100px;
        }

       .dl-form {
           width: 300px;
          
           height: 200px;
           clear: both;
       }
      
        
        .divLeft {
            float: left;
            position: relative;
            margin-bottom: 20px;
            width: 80%;
            min-width: 300px;
        }
       h3 ul 
        {
            margin:5px;
            padding: 10px;
            list-style-type: disc;
        }
        
        h3 ul li 
        {
            margin-left: 30px;
        }
        
        
        .divRight {
            float: left;
            position: relative;
            width: 20%;
            min-width: 100px;
            
        }
        
        .divRight input[type="submit"]
        {
            width:220px;
        }
        
        input.reactivate, input.reactivate:hover 
        {
            background: #96b1cc;
            background: -webkit-gradient(linear, left bottom, left top, color-stop(0.28, #557DA8), color-stop(0.8, #96b1cc));
            background: -moz-linear-gradient(center bottom, #557DA8 28%, #96b1cc 80%);
        }
        
         
    </style> 
    

    <asp:PlaceHolder runat="server" ID="plcAutoRenewalSettings">
        <h1>
            <mn:Txt runat="server" ID="txtAutoRenewalSettings" ExpandImageTokens="True"  />
        </h1>
        <h3>
            <mn:Txt runat="server" ID="txtAutoRenewalInfo" ExpandImageTokens="True" ResourceConstant="TXT_AUTORENEWAL_INFO" />
        </h3>
        <asp:PlaceHolder ID="phlStandardSubscription" runat="server" Visible="true">
            <!--Base subscription (could be bundle)-->
             <div class="plan-subscription">
                    <div class="divLeft">
                        <ul>
                            <li>
                                <span class="arLabel"><mn:Txt runat="server" ID="txtSubscriptionPlan" ExpandImageTokens="True" ResourceConstant="TXT_SUBSCRIPTION_PLAN" /></span>
                                <asp:HiddenField runat="server" ID="hSub"/>
                            </li>
                            <li>
                                <mn:Txt runat="server" ID="txtRenewalRate" ExpandImageTokens="True" ResourceConstant="TXT_RENEWAL_RATE" />
                                <span style="padding-left:5px; padding-right: 5px;"><mn:Txt runat="server" ID="txtRenewalAmountCurrency" ExpandImageTokens="True" ResourceConstant="TXT_CURRENCY" /></span>
                                <asp:Literal ID="litRenewalAmount" runat="server"/><mn:Txt runat="server" ID="txtPerMonth" />
                            </li>
                            <li>
                                <mn:Txt runat="server" ID="txtNextRenewalDate" ExpandImageTokens="True" ResourceConstant="TXT_NEXT_RENEWAL_DATE" /> <asp:Literal ID="litNextRenewalDate" runat="server"></asp:Literal>
                            </li>
                            <li>
                                <asp:Label EnableViewState="False" Runat="server" ID="RenewalDuration" />
                            </li>
                         </ul>
                    </div>
                    <div class="divRight">
                        <mn2:FrameworkButton ID="btnSub" OnClick="btnSub_Click" runat="server" ResourceConstant="BTN_CANCEL_SUBSCIPTION" CssClass="btn primary" />   
                    </div>
            </div>
            
            
            <asp:Panel runat="server" ID="pnlShowPremiumservices" Visible="False" CssClass="pnlShowPremiumservices">
                    <div>
                        <asp:PlaceHolder ID="phlPremiumServicesIncluded" runat="server">
                            <mn:Txt runat="server" ID="txtIncludes" ExpandImageTokens="True" ResourceConstant="TXT_INCLUDES" />
                            <asp:Literal ID="litPremiumServicesIncluded" runat="server"></asp:Literal>
                        </asp:PlaceHolder>
                        <asp:PlaceHolder ID="phlPremiumServicesIncludedEmpty" runat="server">&nbsp;</asp:PlaceHolder>
                    </div>
                    <!--A La Carte: Highlight-->
                    <asp:PlaceHolder ID="phlALaCarteHighlight" runat="server" Visible="false">
                        <div class="plan-alc">
                            <div class="divLeft">
                                <ul>
                                    <li>
                                        <span class="arLabel"><mn:Txt runat="server" ID="txtALaCarteHighlightPlan" ExpandImageTokens="True" ResourceConstant="TXT_PROFILE_HIGHLIGHT" /></span>
                                        <asp:HiddenField runat="server" ID="hHighlight" EnableViewState="True" Value="0"/>
                                    </li>
                                    <li>
                                        <mn:Txt runat="server" ID="txt2" ExpandImageTokens="True" ResourceConstant="TXT_RENEWAL_RATE" />
                                        <span style="padding-left:5px; padding-right: 5px;"><mn:Txt runat="server" ID="txtALaCarteHighlightRenewalCurrency" ExpandImageTokens="True"  ResourceConstant="TXT_CURRENCY" /></span>
                                        <asp:Literal ID="litALaCarteHighlightRenewalAmount" runat="server"/><mn:Txt runat="server" ID="txt4" />
                                    </li>
                                    <li>
                                          <mn:Txt runat="server" ID="txt20" ExpandImageTokens="True" ResourceConstant="TXT_NEXT_RENEWAL_DATE" /> <asp:Literal ID="litNextRenewalDateHighlight" runat="server"></asp:Literal>            
                                    </li>
                                </ul>
                            </div>
                            <div class="divRight">
                                <mn2:FrameworkButton ID="btnHighlight" OnClick="btnHighlight_Click" runat="server" ResourceConstant="BTN_REACTIVATE_SUBSCRIPTION" EnableViewState="False" CssClass="btn primary" />
                            </div>
                        </div>
                    </asp:PlaceHolder>
                    <!--A La Carte: Spotlight-->
                    <asp:PlaceHolder ID="phlALaCarteSpotlight" runat="server" Visible="false">
                        <div class="plan-alc">
                            <div class="divLeft">
                                <ul>
                                    <li>
                                        <span class="arLabel"><mn:Txt runat="server" ID="txtALaCarteSpotlightPlan" ExpandImageTokens="True" ResourceConstant="TXT_MEMBER_SPOTLIGHT" /></span>
                                        <asp:HiddenField runat="server" ID="hSpotlight" EnableViewState="True" Value="0"/>
                                    </li>
                                    <li>
                                         <mn:Txt runat="server" ID="txt3" ExpandImageTokens="True" ResourceConstant="TXT_RENEWAL_RATE" />
                                         <span style="padding-left:5px; padding-right: 5px;"><mn:Txt runat="server" ID="txtALaCarteSpotlightRenewalCurrency" ExpandImageTokens="True" ResourceConstant="TXT_CURRENCY" /></span>
                                         <asp:Literal ID="litALaCarteSpotlightRenewalAmount" runat="server"/><mn:Txt runat="server" ID="txt6" />
                                    </li>
                                    <li>
                                        <mn:Txt runat="server" ID="txt23" ExpandImageTokens="True" ResourceConstant="TXT_NEXT_RENEWAL_DATE" /> <asp:Literal ID="litNextRenewalDateSpotlight" runat="server"></asp:Literal>            
                                    </li>
                                </ul>
                            </div>
                            <div class="divRight">
                                        <mn2:FrameworkButton ID="btnSpotlight" OnClick="btnSpotlight_Click" runat="server" ResourceConstant="BTN_REACTIVATE_SUBSCRIPTION" EnableViewState="False" CssClass="btn primary" />
                            </div>
                        </div>
                    </asp:PlaceHolder>
                    <!--A La Carte: All Access-->
                    <asp:PlaceHolder ID="phALaCarteAllAccess" runat="server" Visible="false">
                       <div class="plan-alc">
                                <div class="divLeft">
                                    <ul>
                                        <li>
                                            <span class="arLabel"><mn:Txt runat="server" ID="txt5" ExpandImageTokens="True" ResourceConstant="TXT_ALLACCESS" /></span>
                                            <asp:HiddenField runat="server" ID="hAllAccess" EnableViewState="True" Value="0"/>   
                                        </li>
                                        <li>
                                            <mn:Txt runat="server" ID="txt7" ExpandImageTokens="True" ResourceConstant="TXT_RENEWAL_RATE" />
                                            <span style="padding-left:5px; padding-right: 5px;"><mn:Txt runat="server" ID="txt8" ExpandImageTokens="True" ResourceConstant="TXT_CURRENCY" /></span>
                                            <asp:Literal ID="litALaCarteAllAccessRenewalAmount" runat="server"/><mn:Txt runat="server" ID="txt9" />
                                        </li>
                                        <li>
                                            <mn:Txt runat="server" ID="txt24" ExpandImageTokens="True" ResourceConstant="TXT_NEXT_RENEWAL_DATE" /> <asp:Literal ID="litNextRenewalDateAllaccess" runat="server"></asp:Literal>            
                                        </li>
                                    </ul>
                                </div>
                                 <div class="divRight">
                                        <mn2:FrameworkButton ID="btnAllAccess" OnClick="btnAA_Click" runat="server" ResourceConstant="BTN_REACTIVATE_SUBSCRIPTION" EnableViewState="False" CssClass="btn primary" />
                                 </div>
                        </div>
                    </asp:PlaceHolder>
                    <!--A La Carte: Read Receipt-->
                    <asp:PlaceHolder ID="phALaCarteReadReceipt" runat="server" Visible="false">
                       <div class="plan-alc">
                                <div class="divLeft">
                                    <ul>
                                        <li>
                                            <span class="arLabel"><mn:Txt runat="server" ID="txt25" ExpandImageTokens="True" ResourceConstant="TXT_READRECEIPT" /></span>
                                            <asp:HiddenField runat="server" ID="hReadReceipt" EnableViewState="True" Value="0"/>   
                                        </li>
                                        <li>
                                            <mn:Txt runat="server" ID="txt26" ExpandImageTokens="True" ResourceConstant="TXT_RENEWAL_RATE" />
                                            <span style="padding-left:5px; padding-right: 5px;"><mn:Txt runat="server" ID="txt27" ExpandImageTokens="True" ResourceConstant="TXT_CURRENCY" /></span>
                                            <asp:Literal ID="litALaCarteReadReceiptRenewalAmount" runat="server"/><mn:Txt runat="server" ID="txt28" />
                                        </li>
                                        <li>
                                            <mn:Txt runat="server" ID="txt29" ExpandImageTokens="True" ResourceConstant="TXT_NEXT_RENEWAL_DATE" /> <asp:Literal ID="litNextRenewalDateReadReceipt" runat="server"></asp:Literal>            
                                        </li>
                                    </ul>
                                </div>
                                 <div class="divRight">
                                        <mn2:FrameworkButton ID="btnReadReceipt" OnClick="btnReadReceipt_Click" runat="server" ResourceConstant="BTN_REACTIVATE_SUBSCRIPTION" EnableViewState="False" CssClass="btn primary" />
                                 </div>
                        </div>
                    </asp:PlaceHolder>

               </asp:Panel>
            <mn:Txt runat="server" ID="txtShowPremiumservices" CssClass="txtShowPremiumservices" ExpandImageTokens="True" Text="Edit my add-on features" Visible="False"  />
            
            <!--Renewal date and total-->
            <div class="border-top-dotted clear-both clearfix">
                
                <asp:Panel runat="server" Visible="False" ID="pnlTotal">
                <h3 class="float-outside strong">
                    <mn:Txt runat="server" ID="txtTotal" ExpandImageTokens="True" ResourceConstant="TXT_TOTAL" />
                    <mn:Txt runat="server" ID="txtTotalAmountCurrency" ExpandImageTokens="True" ResourceConstant="TXT_CURRENCY" />
                    &nbsp;<asp:Literal ID="litTotalAmount" runat="server"></asp:Literal>
                    <span class="sub-copy">
                        <mn:Txt runat="server" ID="txtPerMonthTotal" />
                    </span>
                </h3>
                </asp:Panel>
            </div>
        </asp:PlaceHolder>

        <asp:PlaceHolder ID="phlFreeTrial" runat="server" Visible="false">
            <div class="plan-subscription">
                <h3 class="strong">
                    <span>
                        <asp:CheckBox ID="chkFreeTrial" AutoPostBack="true" runat="server" OnCheckedChanged="chkFreeTrial_CheckedChanged" /></span>
                    <mn:Txt runat="server" ID="txt10" ExpandImageTokens="True" ResourceConstant="TXT_FREETRIAL" />
                </h3>
            </div>
            <div>
                <!--Any all access only text goes here-->
                &nbsp;
            </div>
        </asp:PlaceHolder>
        <p class="border-top-dotted">
            <mn:Txt runat="server" ID="txtFooterBlurb" ExpandImageTokens="True" ResourceConstant="TXT_FOOTER_BLURB" />
        </p>
        <asp:PlaceHolder ID="phlFooterBlurbALaCarte" runat="server" Visible="false">
            <p>
                <mn:Txt runat="server" ID="txt1" ExpandImageTokens="True" ResourceConstant="TXT_FOOTER_BLURB_ALACARTE" />
            </p>
        </asp:PlaceHolder>
    </asp:PlaceHolder>
    

    <asp:PlaceHolder runat="server" ID="plcAutoRenewalDisabledOn" Visible="False">
        <div class="auto-renewal-confirm">
            <p><mn:Txt runat="server" ID="txt11" ExpandImageTokens="True" ResourceConstant="TXT_RENEWAL_USER" /><%=g.Member.GetUserName(g.Brand) %>!</p>
            <p><mn:Txt runat="server" ID="txt12" ExpandImageTokens="True" ResourceConstant="TXT_RENEWAL_TEXT_TOP" /></p>
            <p><mn:Txt runat="server" ID="txt13" ExpandImageTokens="True" ResourceConstant="TXT_RENEWAL_PAYMENT" /><br />
                <mn:Txt runat="server" ID="txt14" ExpandImageTokens="True" ResourceConstant="TXT_RENEWAL_AMOUNT" /><%=litTotalAmount.Text%><mn:Txt runat="server" ID="txt19" ExpandImageTokens="True" ResourceConstant="TXT_RENEWAL_NIS" /><br />
                <mn:Txt runat="server" ID="txt15" ExpandImageTokens="True" ResourceConstant="TXT_RENEWAL_DATE" /><%=litNextRenewalDate.Text%>
            </p>
            <mn:Txt runat="server" ID="txt16" ExpandImageTokens="True" ResourceConstant="TXT_RENEWAL_TEXT_BOTTOM" />
        </div>
    </asp:PlaceHolder>
    <asp:PlaceHolder runat="server" ID="plcAutoRenewalDisabledOff" Visible="False">
        <div class="auto-renewal-confirm">
            <p><mn:Txt runat="server" ID="txt17" ExpandImageTokens="True" ResourceConstant="TXT_RENEWAL_USER" /><%=g.Member.GetUserName(g.Brand) %>!</p>
            <p><mn:Txt runat="server" ID="txt18" ExpandImageTokens="True" ResourceConstant="TXT_RENEWAL_OFF_TEXT_TOP" /></p>
            <p><mn:Txt runat="server" ID="txt21" ExpandImageTokens="True" ResourceConstant="TXT_RENEWAL_OFF_DATE" /><%=litNextRenewalDate.Text%></p>
            <mn:Txt runat="server" ID="txt22" ExpandImageTokens="True" ResourceConstant="TXT_RENEWAL_OFF_TEXT_BOTTOM" />
        </div>
    </asp:PlaceHolder>
</div>
