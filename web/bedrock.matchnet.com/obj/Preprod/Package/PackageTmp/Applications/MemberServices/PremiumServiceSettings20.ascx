﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="PremiumServiceSettings20.ascx.cs" Inherits="Matchnet.Web.Applications.MemberServices.PremiumServiceSettings20" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn" TagName="miniProfile" Src="/Framework/Ui/BasicElements/MiniProfile20.ascx" %>

<h1><mn:Txt runat="server" id="Txt1" ExpandImageTokens="True" ResourceConstant="TXT_PREMIUM_SERVICES_SETTINGS" /></h1>

<div id="premium-settings">
    <p class="editorial"><b><mn:Txt runat="server" id="Txt7" ExpandImageTokens="True" ResourceConstant="TXT_PREMIUM_SERVICES_SETTINGS_SUBHEADER" /></b></p>
    		
    <asp:Repeater ID="rptPremiumServiceContainer" runat="server">
        <ItemTemplate></ItemTemplate>
    </asp:Repeater>

    <p class="editorial clear-both"><mn:Txt runat="server" id="Txt5" ExpandImageTokens="True" ResourceConstant="TXT_PREMIUM_SERVICES_NOTE" /></p>

    <p class="text-center">
        <asp:LinkButton Runat="server" ID="btnSave" CssClass="link-primary"><mn:Txt runat="server" ID="txtUpdateSettings" ResourceConstant="TXT_UPDATE_SETTINGS" /></asp:LinkButton>
    </p>
</div>

<asp:PlaceHolder ID="plcSpotlightInfo" runat ="server" Visible="false">

    <asp:PlaceHolder ID="plcAdditionalInfo" runat="server" Visible="true">

    <div class="border-gen tips">
        <h2><mn:Txt runat="server" ID="txtGetMoreTitle" ResourceConstant="TXT_GET_MORE_TITLE" /></h2>
        <mn:Txt runat="server" ID="txtGetMore1" ResourceConstant="TXT_GET_MORE_1" />
        <mn:Txt runat="server" ID="txtGetMore2" ResourceConstant="TXT_GET_MORE_2" />
        <mn:Txt runat="server" ID="txtGetMore3" ResourceConstant="TXT_GET_MORE_3" />
    </div>
    </asp:PlaceHolder>

    
</asp:PlaceHolder>

