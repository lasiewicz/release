﻿using System;
using System.Data;

using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Util;

using Matchnet.Member.ServiceAdapters;
using Matchnet.Session.ValueObjects;
using Matchnet.MembersOnline.ServiceAdapters;
using Matchnet.Web.Framework.Managers;

namespace Matchnet.Web.Applications.MemberServices 
{
    public partial class Suspend20 : FrameworkControl
    {

        private void Page_Load(object sender, System.EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {

                }

                if (g.BreadCrumbTrailHeader != null)
                {
                    g.BreadCrumbTrailHeader.SetTwoLinkCrumb(g.GetResource("TXT_SUSPEND_MEMBERSHIP", this),
                                                            g.AppPage.App.DefaultPagePath);

                }
                if (g.BreadCrumbTrailFooter != null)
                {
                    g.BreadCrumbTrailFooter.SetTwoLinkCrumb(g.GetResource("TXT_SUSPEND_MEMBERSHIP", this),
                                                           g.AppPage.App.DefaultPagePath);
                }
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        private void Page_Init(object sender, System.EventArgs e)
        {

            

            if (SubscriberManager.Instance.IsIAPSubscriber(g.Member, g.Brand))
            {
                divIAPurchase.Visible = true;
            }

           
            try
            {
                BindDropDown();
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
               

            
        }

        private void BindDropDown()
        {
            DataTable cct = Option.GetOptions("SelfSuspendedReasonType", g);

            lstReasons.DataSource = cct;
            lstReasons.DataTextField = "Content";
            lstReasons.DataValueField = "Value";
            lstReasons.DataBind();
        }


        private void btnSuspend_Click(object sender, System.EventArgs e)
        {
            try
            {
                if (lstReasons.SelectedItem == null)
                {
                    g.Notification.AddError("REASON_IS_REQUIRED");
                }
                else
                {
                    string sReasonID = lstReasons.SelectedItem.Value;

                    g.Member.SetAttributeInt(g.Brand, "SelfSuspendedReasonType", Convert.ToInt32(sReasonID));
                    g.Member.SetAttributeInt(g.Brand, "SelfSuspendedFlag", 1);
                    MemberSA.Instance.SaveMember(g.Member);

                    g.Session.Add("SelfSuspendedFlag", "true", SessionPropertyLifetime.Temporary);

                    MembersOnlineSA.Instance.Remove(g.Brand.Site.Community.CommunityID, g.Member.MemberID);

                    g.Transfer("/Applications/MemberServices/MemberServices.aspx?rc=YOUR_MEMBERSHIP_HAS_BEEN_SUSPENDED");
                }
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSuspend.Click += new System.EventHandler(this.btnSuspend_Click);
            this.Load += new System.EventHandler(this.Page_Load);
            this.Init += new System.EventHandler(this.Page_Init);

        }
        #endregion
    }
}