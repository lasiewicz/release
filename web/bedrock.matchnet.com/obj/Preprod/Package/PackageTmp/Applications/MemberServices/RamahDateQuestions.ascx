﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RamahDateQuestions.ascx.cs" Inherits="Matchnet.Web.Applications.MemberServices.RamahDateQuestions" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<div id="page-container" class="semantic-html"> 

    <asp:Panel ID="pnlHideQuestions" runat="server" EnableViewState="false" Visible="false">
        You alreay have a badge enjoy !!
    </asp:Panel>

    <asp:Panel ID="pnlSuccess" runat="server" EnableViewState="false" Visible="false">
        <h2 class="component-header text-center" style="margin:2em;">Your Ramah alumni badge has been successfully added to your profile.</h2>
        <div class="text-center">
            <a href="/Applications/MemberProfile/ViewProfile.aspx" class="btn link-primary large">Go To My Profile</a>
        </div>
    </asp:Panel>

    <asp:Panel ID="pnlShowQuestions" runat="server" EnableViewState="false" Visible="false" CssClass="margin-heavy">
        <asp:Panel ID="pnlError" runat="server" EnableViewState="false" Visible="false">
            <ul class="error">
                <li>Please pick a valid camp or valid start year. Also please make sure the start year happens before the end year. </li>
            </ul>
        </asp:Panel>

        <h2 class="component-header">Which Ramah camp(s) did you attend?</h2>
        <asp:CheckBoxList ID="cblCamps" runat="server" RepeatLayout="UnorderedList" CssClass="clear-both column-count-three padding-heavy">
            <asp:ListItem Value="1">Berkshires</asp:ListItem>
            <asp:ListItem Value="2">California</asp:ListItem>
            <asp:ListItem Value="4">Canada</asp:ListItem>
            <asp:ListItem Value="8">Chicago-area Day Camp</asp:ListItem>
            <asp:ListItem Value="16">Connecticut (1953-1964)</asp:ListItem>
            <asp:ListItem Value="32">Darom</asp:ListItem>
            <asp:ListItem Value="64">Glen Spey (1967-1971)</asp:ListItem>
            <asp:ListItem Value="128">Jerusalem Day Camp</asp:ListItem>
            <asp:ListItem Value="256">Mador (1959-1980)</asp:ListItem>
            <asp:ListItem Value="512">Maine (1948-1949)</asp:ListItem>
            <asp:ListItem Value="1024">New England</asp:ListItem>
            <asp:ListItem Value="2048">Nyack</asp:ListItem>
            <asp:ListItem Value="4096">Philadelphia Day Camp</asp:ListItem>
            <asp:ListItem Value="8192">Poconos</asp:ListItem>
            <asp:ListItem Value="16384">Rockies</asp:ListItem>
            <asp:ListItem Value="32768">SEE (1974-2002)</asp:ListItem>
            <asp:ListItem Value="65536">Seminar</asp:ListItem>
            <asp:ListItem Value="131072">TRY/USY High</asp:ListItem>
            <asp:ListItem Value="262144">Wisconsin</asp:ListItem>
        </asp:CheckBoxList>

        <h2 class="component-header">What years did you attend camp?</h2>
        <div class="clear-both padding-heavy">
            <asp:DropDownList ID="ddStartDate" runat="server" /> - <asp:DropDownList ID="ddEndDate" runat="server" />
        </div>
        <asp:Panel ID="pnlTOS" runat="server" Visible="false">
             <span style="font-size:13px;color:red;">In order to receive the badge on your profile, please agree to the terms below.</span>
        </asp:Panel>
        
        <div style="display:inline-block; vertical-align:top;"><asp:CheckBox ID="cbxRamahTos" runat="server" /></div>
        <div style="display:inline-block;width:600px; padding-top:3px;font-size:13px;">
            By checking this box and submitting this information regarding your experiences at Camp Ramah, you agree that Spark Networks may share this information as well as your email information and other registration information with the National Ramah Commission. If you do not want us to share such information with Ramah, please do not complete this page.
        </div>
        <div class="text-center" style="padding-top:10px;">
            <asp:Button ID="btn" runat="server" Text="Add My Camp" OnClick="Btn_Click" CssClass="large" />
        </div>

    </asp:Panel>

</div>
