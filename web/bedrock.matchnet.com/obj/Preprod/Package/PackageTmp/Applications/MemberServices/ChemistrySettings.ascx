﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ChemistrySettings.ascx.cs" Inherits="Matchnet.Web.Applications.MemberServices.ChemistrySettings" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>

<div id="page-container">
	<mn:Title runat="server" id="ttlDisplaySettings" ResourceConstant="TXT_CHEMISTRY_SETTINGS" />
	
	<asp:PlaceHolder ID="phColorCodeSetting" runat="server" Visible="false">
	    <h3><mn:Txt runat="server" id="txtColorCodeSetting" ResourceConstant="TXT_COLOR_CODE_TEST_SETTING" /></h3>
	    <p class="border-top-dotted"><mn:Txt runat="server" id="txtColorCodeSettingDescription" ResourceConstant="TXT_COLOR_CODE_TEST_SETTING_DESCRIPTION" /></p>
        <blockquote class="editorial">
            <asp:RadioButton id="rbColorCodeShow" GroupName="ColorCodeShowHide" Runat="server" />
            <mn:Txt runat="server" id="txtColorCodeShow" ResourceConstant="TXT_SHOW" />
	        <asp:RadioButton id="rbColorCodeHide" GroupName="ColorCodeShowHide" Runat="server" />
	        <mn:Txt runat="server" id="txtColorCodeHide" ResourceConstant="TXT_HIDE" />
	    </blockquote>
	    
	    <p class="text-center editorial cc-button-primary">	
	        <asp:LinkButton ID="btnSubmit" runat="server" CssClass="link-primary" OnClick="btnSubmit_Click">Save Settings</asp:LinkButton>
        </p>
    </asp:PlaceHolder>
    
    <asp:PlaceHolder ID="phCodeCodeTakeTest" runat="server" Visible="false">
        <!--Link to take test-->
        <p class="text-center editorial cc-button-primary">	
	        <a href="/Applications/ColorCode/Landing.aspx?colortracking=membersvc" class="link-primary">Take the Color Code Test Now</a>
        </p>
    </asp:PlaceHolder>
    
    
</div>