﻿using System;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using Matchnet.Web.Framework;

using Matchnet.Member.ServiceAdapters;

namespace Matchnet.Web.Applications.MemberServices
{
    public partial class DisplaySettings20 : FrameworkControl
    {
        /// <summary>
        /// Adds two items to a provided radio button.
        /// </summary>
        /// <param name="rbList">Radio button list</param>
        /// <param name="name1">A string representing the resource text</param>
        /// <param name="value1">The value for the item.</param>
        /// <param name="name2">A string representing the resource text</param>
        /// <param name="value2">The value for the item.</param>
        private void AddTwoItems(ref RadioButtonList rbList, string name1, string value1, string name2, string value2)
        {
            ListItem item1 = new ListItem(name1 + "&nbsp;&nbsp;&nbsp;", value1);
            ListItem item2 = new ListItem(name2 + "&nbsp;&nbsp;&nbsp;", value2);

            rbList.Items.Add(item1);
            rbList.Items.Add(item2);
        }

        /// <summary>
        /// Binds control with data
        /// </summary>
        private void BindItems()
        {
            AddTwoItems(ref rblSearches, g.GetTargetResource("SHOW", this), "0", g.GetTargetResource("HIDE_SEARCHES", this), rblSearchesCheck.Value);
            AddTwoItems(ref rblHotList, g.GetTargetResource("SHOW", this), "0", g.GetTargetResource("HIDE", this), rblHotListCheck.Value);
            AddTwoItems(ref rblMembersOnline, g.GetTargetResource("SHOW", this), "0", g.GetTargetResource("HIDE_ONLINE", this), rblMembersOnlineCheck.Value);

            btnSaveSettings.Text = g.GetTargetResource("SAVE_SETTINGS", this);
        }

        /// <summary>
        /// Sets radio button items.
        /// </summary>
        private void SetRadioButtons()
        {
            Matchnet.Member.ServiceAdapters.Member oMember = MemberSA.Instance.GetMember(g.Session.GetInt(WebConstants.SESSION_PROPERTY_NAME_MEMBERID)
                , MemberLoadFlags.None);

            // get the display photos mask/value
            int DisplayPhotos = oMember.GetAttributeInt(g.Brand, "DisplayPhotosToGuests", 0);
            // if display photos is set, make all users selected
            if (DisplayPhotos == 1)
            {
                AllUsers.Checked = true;
            }
            else
            {
                MembersOnly.Checked = true;
            }

            // get the hidemask mask/value for the other three radio button lists.
            int HideMask = oMember.GetAttributeInt(g.Brand, "HideMask", 0);
            // run through each control on the page
            foreach (System.Web.UI.Control child in Controls)
            {
                // if the control is a radio button list
                if (child is RadioButtonList)
                {
                    // create an instance of that radio button list
                    RadioButtonList rblList = (RadioButtonList)child;
                    // for each radio button item in the list
                    foreach (ListItem btnItem in rblList.Items)
                    {
                        // find the hidden input with naming schema
                        System.Web.UI.HtmlControls.HtmlInputHidden hdnMask = (HtmlInputHidden)this.FindControl(rblList.ID + "Check");

                        // AND the masks together to get a value indicating status
                        int chkValue = Convert.ToInt16(hdnMask.Value) & HideMask;

                        // if the AND operation equals the value, select the radio button
                        if (chkValue.ToString() == btnItem.Value)
                        {
                            btnItem.Selected = true;
                        }
                    }
                }
            }
        }

        private void Page_Init(object sender, System.EventArgs e)
        {
            try
            {
                BindItems();
                if (!IsPostBack)
                {
                    SetRadioButtons();
                }
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        private void Page_Load(object sender, EventArgs e)
        {
            g.BreadCrumbTrailHeader.SetThreeLinkCrumb(g.GetResource("NAV_PROFILE_PHOTOS", this), "/Applications/MemberProfile/ViewProfile.aspx?EntryPoint=99999",
                g.GetResource("NAV_SUB_SHOW_HIDE_PROFILE", null), string.Empty);
            g.BreadCrumbTrailFooter.SetThreeLinkCrumb(g.GetResource("NAV_PROFILE_PHOTOS", this), "/Applications/MemberProfile/ViewProfile.aspx?EntryPoint=99999",
                g.GetResource("NAV_SUB_SHOW_HIDE_PROFILE", null), string.Empty);
        }

        public void btnSaveSettings_Click(object sender, System.EventArgs e)
        {
            try
            {
                int showPhotos = 0;
                int hideMask = 0;

                if (AllUsers.Checked)
                {
                    showPhotos = 1;
                }

                foreach (System.Web.UI.Control child in Controls)
                {
                    if (child is RadioButtonList)
                    {
                        RadioButtonList rblList = (RadioButtonList)child;
                        foreach (ListItem lstItem in rblList.Items)
                        {
                            if (lstItem.Selected)
                            {
                                hideMask += Convert.ToInt16(rblList.SelectedValue);
                            }
                        }
                    }
                }

                //remove from MOL if we need to
                if ((g.Member.GetAttributeInt(g.Brand, "HideMask") & (Int32)WebConstants.AttributeOptionHideMask.HideMembersOnline) != (Int32)WebConstants.AttributeOptionHideMask.HideMembersOnline &&
                    (hideMask & (Int32)WebConstants.AttributeOptionHideMask.HideMembersOnline) == (Int32)WebConstants.AttributeOptionHideMask.HideMembersOnline)
                {
                    Matchnet.MembersOnline.ServiceAdapters.MembersOnlineSA.Instance.Remove(g.Brand.Site.Community.CommunityID,
                        g.Member.MemberID);
                }

                // then save and send items/ and display a succes message
                g.Member.SetAttributeInt(g.Brand, "DisplayPhotosToGuests", showPhotos);
                g.Member.SetAttributeInt(g.Brand, "HideMask", hideMask);
                /*
                 * Martin Hristoforov / 05/15/06 Bug 18627
                 * this is not needed since member are complaining that when they change their display settings
                 * it shouldn't update their date.*/
                //g.Member.SetAttributeDate(g.Brand, "LastUpdated", DateTime.Now);

                MemberSA.Instance.SaveMember(g.Member);
                g.Notification.AddMessage("SUCCESSFULLY_SAVED_YOUR_PROFILE_DISPLAY_SETTINGS");
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSaveSettings.Click += new System.EventHandler(this.btnSaveSettings_Click);
            this.Init += new System.EventHandler(this.Page_Init);
            this.Load += new EventHandler(Page_Load);
        }
        #endregion
    }
}