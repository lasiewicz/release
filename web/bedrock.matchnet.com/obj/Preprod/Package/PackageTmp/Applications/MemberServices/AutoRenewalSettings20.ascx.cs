﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;

using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Util;
using Matchnet.Lib;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects;
using Matchnet.Purchase.ServiceAdapters;
using Matchnet.Purchase.ServiceAdapters.Admin;
using Matchnet.Purchase.ValueObjects;
using System.Collections.Generic;
using Spark.Common.Adapter;
using Spark.Common.AuthorizationService;
using Spark.Common.PaymentProfileService;
using Spark.Common.AccessService;
using Matchnet.PromoEngine.ServiceAdapters.TokenReplacement;
using Matchnet.PromoEngine.ValueObjects;
using Matchnet.Web.Framework.Managers;

namespace Matchnet.Web.Applications.MemberServices
{
    public partial class AutoRenewalSettings20 : FrameworkControl
    {
        Matchnet.Member.ServiceAdapters.Member _member = null;
        Matchnet.Content.ValueObjects.BrandConfig.Brand _brand = null;
        Plan _plan = null;
        bool _autoRenewAlreadyOn = false;

        //a la carte
        Plan _planHighlight = null;
        Plan _planSpotlight = null;
        Plan _planJMeter = null;
        Plan _planAllAccess = null;
        Spark.Common.RenewalService.RenewalSubscriptionDetail _rseHighlight = null;
        Spark.Common.RenewalService.RenewalSubscriptionDetail _rseSpotlight = null;
        Spark.Common.RenewalService.RenewalSubscriptionDetail _rseJMeter = null;
        Spark.Common.RenewalService.RenewalSubscriptionDetail _rseAllAccess = null;

        bool _hasActiveFreeTrialSubscription = false;

        protected void Page_Load(object sender, EventArgs e)
        {
            this.PopulateRenewalInformation();

            if (g.BreadCrumbTrailHeader != null)
            {
                g.BreadCrumbTrailHeader.SetTwoLinkCrumb(g.GetResource("TXT_AUTORENEWAL_SETTINGS", this),
                                                        g.AppPage.App.DefaultPagePath);

            }
            if (g.BreadCrumbTrailFooter != null)
            {
                g.BreadCrumbTrailFooter.SetTwoLinkCrumb(g.GetResource("TXT_AUTORENEWAL_SETTINGS", this),
                                                       g.AppPage.App.DefaultPagePath);
            }

            // autorenewal disabled
            if (phlStandardSubscription.Visible && SettingsManager.GetSettingBool("DISABLE_AUTO_RENEWAL_CONTROL_ON_SITE", g.Brand))
            {
                plcAutoRenewalSettings.Visible = false;
                litNextRenewalDate.Text = DateTime.Parse(litNextRenewalDate.Text).ToString("MM/dd/yyyy hh:mm",
                                                                                           CultureInfo.InvariantCulture);
                if (chkEnableAutoRenewal.Checked)
                {
                    plcAutoRenewalDisabledOn.Visible = true;

                }
                else
                {
                    plcAutoRenewalDisabledOff.Visible = true;
                }
            }
        }

        private void PopulateRenewalInformation()
        {
            try
            {
                _member = g.Member;
                _brand = g.Brand;

                if (_member != null && _brand != null)
                {
                    Spark.Common.RenewalService.RenewalSubscription renewalSub = RenewalManager.Instance.GetRenewalSubscription(_member.MemberID, _brand);

                    if (renewalSub != null && renewalSub.RenewalSubscriptionID > 0)
                    {
                        litNextRenewalDate.Text = renewalSub.RenewalDatePST.ToString();

                        //base plan
                        _plan = PlanSA.Instance.GetPlan(renewalSub.PrimaryPackageID, _brand.BrandID);

                        if (renewalSub.IsRenewalEnabled)
                        {
                            _autoRenewAlreadyOn = true;
                        }

                        //a la carte plan(s)
                        if (renewalSub.RenewalSubscriptionDetails.Length > 1)
                        {
                            Plan rsePlan = null;
                            foreach (Spark.Common.RenewalService.RenewalSubscriptionDetail rse in renewalSub.RenewalSubscriptionDetails)
                            {
                                if (!rse.IsPrimaryPackage)
                                {
                                    rsePlan = PlanSA.Instance.GetPlan(rse.PackageID, _brand.BrandID);
                                    if ((rsePlan.PremiumTypeMask & PremiumType.HighlightedProfile) == PremiumType.HighlightedProfile)
                                    {
                                        _planHighlight = rsePlan;
                                        _rseHighlight = rse;
                                        phlALaCarteHighlight.Visible = true;
                                        phlFooterBlurbALaCarte.Visible = true;
                                    }
                                    else if ((rsePlan.PremiumTypeMask & PremiumType.SpotlightMember) == PremiumType.SpotlightMember)
                                    {
                                        _planSpotlight = rsePlan;
                                        _rseSpotlight = rse;
                                        phlALaCarteSpotlight.Visible = true;
                                        phlFooterBlurbALaCarte.Visible = true;
                                    }
                                    else if ((rsePlan.PremiumTypeMask & PremiumType.JMeter) == PremiumType.JMeter)
                                    {
                                        //091310 TL
                                        //TL: Noticed that this member's auto-renewal page does not have JMeter option for user to turn off.
                                        //I will not add that to the UI for user since I didn't see it here originally
                                        //but the admin's page does so I added it here to support UPS Renewal changes so it does not disable JMeter
                                        _planJMeter = rsePlan;
                                        _rseJMeter = rse;
                                    }
                                    else if ((rsePlan.PremiumTypeMask & PremiumType.AllAccess) == PremiumType.AllAccess)
                                    {
                                        _planAllAccess = rsePlan;
                                        _rseAllAccess = rse;
                                        phALaCarteAllAccess.Visible = true;
                                        phlFooterBlurbALaCarte.Visible = true;
                                    }
                                }
                            }
                        }

                        //determine whether various subscriptions are selected
                        if (!Page.IsPostBack)
                        {
                            //base sub
                            if (renewalSub.IsRenewalEnabled)
                            {
                                chkEnableAutoRenewal.Checked = true;
                            }

                            //a la carte subs
                            if (_rseHighlight != null && _rseHighlight.IsRenewalEnabled)
                            {
                                chkALaCarteHighlight.Checked = true;
                            }

                            if (_rseSpotlight != null && _rseSpotlight.IsRenewalEnabled)
                            {
                                chkALaCarteSpotlight.Checked = true;
                            }

                            if (_rseAllAccess != null && _rseAllAccess.IsRenewalEnabled)
                            {
                                chkALaCarteAllAccess.Checked = true;
                            }

                        }
                        else
                        {
                            // Need to manually load the postback data for the checkbox control since
                            // viewstate is disabled  

                            //base sub
                            string checkBoxPostBackVal = Request.Form[chkEnableAutoRenewal.UniqueID.ToString()];
                            if (checkBoxPostBackVal != null)
                            {
                                chkEnableAutoRenewal.Checked = (checkBoxPostBackVal.ToLower() == "on" ? true : false);
                            }

                            //a la carte subs
                            checkBoxPostBackVal = Request.Form[chkALaCarteHighlight.UniqueID.ToString()];
                            if (checkBoxPostBackVal != null)
                            {
                                chkALaCarteHighlight.Checked = (checkBoxPostBackVal.ToLower() == "on" ? true : false);
                            }

                            checkBoxPostBackVal = Request.Form[chkALaCarteSpotlight.UniqueID.ToString()];
                            if (checkBoxPostBackVal != null)
                            {
                                chkALaCarteSpotlight.Checked = (checkBoxPostBackVal.ToLower() == "on" ? true : false);
                            }

                            checkBoxPostBackVal = Request.Form[chkALaCarteAllAccess.UniqueID.ToString()];
                            if (checkBoxPostBackVal != null)
                            {
                                chkALaCarteAllAccess.Checked = (checkBoxPostBackVal.ToLower() == "on" ? true : false);
                            }
                        }

                        if (_plan != null)
                        {
                            int primaryPackagRenewalDurationInMonths = _plan.RenewDuration;

                            //display individual renewal amounts
                            litRenewalAmount.Text = (chkEnableAutoRenewal.Checked ? Convert.ToString(_plan.RenewCost).Trim() : "0.00");

                            decimal decALaCarteHighlightRenewalAmount = 0.0m;
                            if (chkALaCarteHighlight.Checked && (_planHighlight != null))
                            {
                                decALaCarteHighlightRenewalAmount = _planHighlight.RenewCost * (primaryPackagRenewalDurationInMonths / _planHighlight.RenewDuration);
                            }
                            litALaCarteHighlightRenewalAmount.Text = Convert.ToString(decALaCarteHighlightRenewalAmount).Trim();

                            decimal decALaCarteSpotlightRenewalAmount = 0.0m;
                            if (chkALaCarteSpotlight.Checked && (_planSpotlight != null))
                            {
                                decALaCarteSpotlightRenewalAmount = _planSpotlight.RenewCost * (primaryPackagRenewalDurationInMonths / _planSpotlight.RenewDuration);
                            }
                            litALaCarteSpotlightRenewalAmount.Text = Convert.ToString(decALaCarteSpotlightRenewalAmount).Trim();

                            decimal decALaCarteAllAccessRenewalAmount = 0.0m;
                            if (chkALaCarteAllAccess.Checked && (_planAllAccess != null))
                            {
                                decALaCarteAllAccessRenewalAmount = _planAllAccess.RenewCost * (primaryPackagRenewalDurationInMonths / _planAllAccess.RenewDuration);
                            }
                            litALaCarteAllAccessRenewalAmount.Text = Convert.ToString(decALaCarteAllAccessRenewalAmount).Trim();

                            //calculate renewal total
                            decimal renewalTotal = 0m;

                            if (chkEnableAutoRenewal.Checked)
                            {
                                renewalTotal += _plan.RenewCost;
                            }
                            if (chkALaCarteHighlight.Checked)
                            {
                                renewalTotal += decALaCarteHighlightRenewalAmount;
                            }
                            if (chkALaCarteSpotlight.Checked)
                            {
                                renewalTotal += decALaCarteSpotlightRenewalAmount;
                            }
                            if (chkALaCarteAllAccess.Checked)
                            {
                                renewalTotal += decALaCarteAllAccessRenewalAmount;
                            }

                            litTotalAmount.Text = renewalTotal.ToString("N2");

                            //display bundled plan info
                            if (_plan.PremiumTypeMask != PremiumType.None)
                            {
                                phlPremiumServicesIncluded.Visible = true;
                                phlPremiumServicesIncludedEmpty.Visible = false;
                                bool anotherPremiumService = false;

                                StringBuilder sbPremiumServices = new StringBuilder(g.GetResource("TXT_WITH", this));
                                if ((_plan.PremiumTypeMask & PremiumType.HighlightedProfile) == PremiumType.HighlightedProfile)
                                {
                                    sbPremiumServices.Append(" " + g.GetResource("TXT_PROFILE_HIGHLIGHT", this));
                                    anotherPremiumService = true;
                                }
                                else
                                {
                                    anotherPremiumService = false;
                                }

                                if ((_plan.PremiumTypeMask & PremiumType.SpotlightMember) == PremiumType.SpotlightMember)
                                {
                                    if (anotherPremiumService)
                                    {
                                        sbPremiumServices.Append(g.GetResource("TXT_AND", this));
                                    }
                                    sbPremiumServices.Append(" " + g.GetResource("TXT_MEMBER_SPOTLIGHT", this));
                                    anotherPremiumService = true;
                                }
                                else
                                {
                                    anotherPremiumService = false;
                                }

                                if ((_plan.PremiumTypeMask & PremiumType.AllAccess) == PremiumType.AllAccess)
                                {
                                    if (anotherPremiumService)
                                    {
                                        sbPremiumServices.Append(g.GetResource("TXT_AND", this));
                                    }
                                    sbPremiumServices.Append(" " + g.GetResource("TXT_ALLACCESS", this));
                                    anotherPremiumService = true;
                                }
                                else
                                {
                                    anotherPremiumService = false;
                                }

                                litPremiumServicesIncluded.Text = sbPremiumServices.ToString();
                            }
                            else
                            {
                                phlPremiumServicesIncluded.Visible = false;
                                phlPremiumServicesIncludedEmpty.Visible = true;
                            }

                            // Display renewal duration
                            string resource = string.Empty;
                            string resourceConstantToUse = string.Empty;
                            if (primaryPackagRenewalDurationInMonths == 1)
                            {
                                resourceConstantToUse = "SITE_PLAN_MONTLY_RENEWAL_ADD_TEMPLATE_DEFAULT";
                                resource = g.GetResource(resourceConstantToUse, this);
                            }
                            else
                            {
                                resourceConstantToUse = "SITE_PLAN_MULTI_MONTH_RENEWAL_ADD_TEMPLATE_DEFAULT";
                                resource = String.Format(g.GetResource(resourceConstantToUse, this), primaryPackagRenewalDurationInMonths);
                            }

                            txtPerMonth.Text = resource;
                            txt4.Text = resource;
                            txt6.Text = resource;
                            txt9.Text = resource;
                            txtPerMonthTotal.Text = resource;
                        }
                        else
                        {
                            g.Notification.AddMessage("NO_PLAN");
                        }
                    }
                    else
                    {
                        // Check if this member has an active free trial subscription
                        try
                        {
                            this._hasActiveFreeTrialSubscription = AuthorizationServiceWebAdapter.GetProxyInstance().HasActiveFreeTrialSubscription(g.TargetMemberID, g.TargetBrand.Site.SiteID);
                        }
                        catch (Exception ex)
                        {
                            System.Diagnostics.Trace.WriteLine("Error in checking to see if the member has an active free trial subscription, Error message: " + ex.Message);
                        }
                        finally
                        {
                            AuthorizationServiceWebAdapter.CloseProxyInstance();
                        }

                        if (this._hasActiveFreeTrialSubscription)
                        {
                            phlFreeTrial.Visible = true;
                            phlStandardSubscription.Visible = false;
                            //phlFooterBlurbALaCarte.Visible = true;
                        }
                        else
                        {
                            phlFreeTrial.Visible = false;
                            g.Notification.AddMessage("NO_SUBSCRIPTION");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.Write("Error in AutoRenewalSettings.PopulateRenewalInformation(), " + ex.ToString());

                g.ProcessException(ex);
            }
        }

        //public string DurationTypeResourceConstant(int duration, Matchnet.DurationType durationType)
        //{
        //    string resourceConstant = Constants.NULL_STRING;

        //    if (duration > 1)
        //    {
        //        switch (durationType)
        //        {
        //            case DurationType.Minutes:
        //                resourceConstant = "MINUTE_S";
        //                break;
        //            case DurationType.Hour:
        //                resourceConstant = "HOUR_S";
        //                break;
        //            case DurationType.Day:
        //                resourceConstant = "DAY_S";
        //                break;
        //            case DurationType.Week:
        //                resourceConstant = "WEEK_S";
        //                break;
        //            case DurationType.Month:
        //                resourceConstant = "MONTH_S";
        //                break;
        //            case DurationType.Year:
        //                resourceConstant = "YEAR_S";
        //                break;
        //            default:
        //                resourceConstant = "MISSING";
        //                break;
        //        }
        //    }
        //    else
        //    {
        //        switch (durationType)
        //        {
        //            case DurationType.Minutes:
        //                resourceConstant = "MINUTE";
        //                break;
        //            case DurationType.Hour:
        //                resourceConstant = "HOUR";
        //                break;
        //            case DurationType.Day:
        //                resourceConstant = "DAY";
        //                break;
        //            case DurationType.Week:
        //                resourceConstant = "WEEK";
        //                break;
        //            case DurationType.Month:
        //                resourceConstant = "MONTH";
        //                break;
        //            case DurationType.Year:
        //                resourceConstant = "YEAR";
        //                break;
        //            default:
        //                resourceConstant = "MISSING";
        //                break;
        //        }
        //    }

        //    return resourceConstant;
        //}

        protected void btnContinue_Click(object sender, EventArgs e)
        {
            try
            {
                if (_plan == null
                    && !_hasActiveFreeTrialSubscription)
                {
                    // Member is not subscribed but receives subscription time from administrative
                    // adjustment  
                    // Plan ID is 0 since the member has never subscribed before
                    // If the member has subscribed before, then the Plan ID is taken from his
                    // last subscription for any administrative adjustments. 
                    g.Notification.Clear();
                    g.Notification.AddError("NO_PLAN_TO_AUTO_RENEWAL");
                }
                else
                {
                    if (chkEnableAutoRenewal.Checked)
                    {
                        bool displayRenewalsAlreadyActive = true;
                        bool displayRenewalSuccess = false;
                        bool displayRenewalError = false;
                        List<int> secondaryPackageIDList = new List<int>(); //for Spark.Renewal (UPS)

                        //Check Base Sub
                        bool reenableBaseSub = !_autoRenewAlreadyOn;

                        //Check A la carte
                        List<int> secondaryPackageIDToEnable = new List<int>();
                        List<int> secondaryPackageIDToDisable = new List<int>();

                        //Highlight (enable or disable)
                        if (_rseHighlight != null)
                        {
                            if (chkALaCarteHighlight.Checked && !_rseHighlight.IsRenewalEnabled)
                            {
                                secondaryPackageIDToEnable.Add(_rseHighlight.PackageID);
                            }
                            else if (!chkALaCarteHighlight.Checked && _rseHighlight.IsRenewalEnabled)
                            {
                                secondaryPackageIDToDisable.Add(_rseHighlight.PackageID);
                            }
                        }

                        //Spotlight (enable or disable)
                        if (_rseSpotlight != null)
                        {
                            if (chkALaCarteSpotlight.Checked && !_rseSpotlight.IsRenewalEnabled)
                            {
                                secondaryPackageIDToEnable.Add(_rseSpotlight.PackageID);
                            }
                            else if (!chkALaCarteSpotlight.Checked && _rseSpotlight.IsRenewalEnabled)
                            {
                                secondaryPackageIDToDisable.Add(_rseSpotlight.PackageID);
                            }
                        }

                        //All Access (enable or disable)
                        if (_rseAllAccess != null)
                        {
                            if (chkALaCarteAllAccess.Checked && !_rseAllAccess.IsRenewalEnabled)
                            {
                                secondaryPackageIDToEnable.Add(_rseAllAccess.PackageID);
                            }
                            else if (!chkALaCarteAllAccess.Checked && _rseAllAccess.IsRenewalEnabled)
                            {
                                secondaryPackageIDToDisable.Add(_rseAllAccess.PackageID);
                            }
                        }

                        //Enable auto-renewal - update UPS
                        if (reenableBaseSub)
                        {
                            //Enable renewal for base + a la carte
                            displayRenewalsAlreadyActive = false;
                            Spark.Common.RenewalService.RenewalResponse renewalResponse = RenewalServiceWebAdapter.GetProxyInstanceForBedrock().EnableAutoRenewal(secondaryPackageIDToEnable.ToArray(), g.Member.MemberID, g.Brand.Site.SiteID,
                                1, Constants.NULL_INT, "", Constants.NULL_INT);

                            if (renewalResponse.InternalResponse.responsecode != "0")
                                displayRenewalError = true;
                            else
                                displayRenewalSuccess = true;
                        }
                        else if (secondaryPackageIDToEnable.Count > 0)
                        {
                            //Enable renewal for a la carte only (base sub is already enabled)
                            displayRenewalsAlreadyActive = false;
                            Spark.Common.RenewalService.RenewalResponse renewalResponse = RenewalServiceWebAdapter.GetProxyInstanceForBedrock().EnableALaCarteAutoRenewal(secondaryPackageIDToEnable.ToArray(), g.Member.MemberID, g.Brand.Site.SiteID,
                                1, Constants.NULL_INT, "", Constants.NULL_INT);

                            if (renewalResponse.InternalResponse.responsecode != "0")
                                displayRenewalError = true;
                            else
                                displayRenewalSuccess = true;
                        }

                        //Disable A la carte - Update UPS
                        if (!displayRenewalError && secondaryPackageIDToDisable.Count > 0)
                        {
                            displayRenewalsAlreadyActive = false;
                            Spark.Common.RenewalService.RenewalResponse renewalResponse = RenewalServiceWebAdapter.GetProxyInstanceForBedrock().DisableALaCarteAutoRenewal(secondaryPackageIDToDisable.ToArray(), g.Member.MemberID, g.Brand.Site.SiteID,
                                1, Constants.NULL_INT, "", Constants.NULL_INT);

                            if (renewalResponse.InternalResponse.responsecode != "0")
                                displayRenewalError = true;
                            else
                                displayRenewalSuccess = true;
                        }

                        if (displayRenewalError)
                        {
                            g.Notification.AddError("UNABLE_TO_UPDATE_RENEWAL");
                        }
                        else if (displayRenewalSuccess)
                        {
                            g.Notification.AddMessage("RENEWAL_UPDATED_SUCCESSFULLY");
                            g.Session.Add("AUTO_RENEW", "autorenon", Matchnet.Session.ValueObjects.SessionPropertyLifetime.Temporary);

                            // this is used to display the autorenewal reactivate link in the top aux menu
                            g.Member.SetAttributeInt(_g.Brand, "AutoRenewalStatus", 1);
                            MemberSA.Instance.SaveMember(g.Member);
                        }
                        else if (displayRenewalsAlreadyActive)
                        {
                            g.Notification.AddError("RENEWAL_ALREADY_ACTIVE");
                        }

                    }
                    else if (chkFreeTrial.Checked)
                    {
                        // Get the active free trial subscription for this member 
                        AuthorizationSubscription freeTrialSubscription = null;
                        bool hasActiveFreeTrial = false;

                        try
                        {
                            PaymentProfileInfo defaultPaymentProfile = PaymentProfileServiceWebAdapter.GetProxyInstance().GetMemberDefaultPaymentProfile(g.TargetMemberID, g.Brand.Site.SiteID);

                            if (defaultPaymentProfile != null)
                            {
                                freeTrialSubscription = AuthorizationServiceWebAdapter.GetProxyInstance().GetActiveAuthorizationSubscription(defaultPaymentProfile.PaymentProfileID, g.TargetMemberID, g.Brand.Site.SiteID);
                            }
                        }
                        catch (Exception ex)
                        {
                            System.Diagnostics.Trace.WriteLine("Error in getting the free trial subscription details, Error message: " + ex.Message);
                        }
                        finally
                        {
                            PaymentProfileServiceWebAdapter.CloseProxyInstance();
                            AuthorizationServiceWebAdapter.CloseProxyInstance();
                        }

                        if (freeTrialSubscription != null)
                        {
                            if (freeTrialSubscription.CaptureDateUTC >= DateTime.UtcNow)
                            {
                                hasActiveFreeTrial = true;
                            }
                        }

                        if (hasActiveFreeTrial)
                        {
                            AccessFreeTrial accessFreeTrial = new AccessFreeTrial();
                            accessFreeTrial.AuthorizationSubscriptionID = freeTrialSubscription.AuthorizationSubscriptionID;
                            accessFreeTrial.AuthorizationCode = freeTrialSubscription.AuthorizationCode;
                            accessFreeTrial.AuthorizationChargeID = freeTrialSubscription.AuthorizationChargeID;
                            accessFreeTrial.AuthorizationAmount = freeTrialSubscription.AuthorizationAmount;
                            accessFreeTrial.PaymentProfileID = freeTrialSubscription.PaymentProfileID;
                            accessFreeTrial.PaymentType = "CreditCard";
                            accessFreeTrial.CustomerID = freeTrialSubscription.CustomerID;
                            accessFreeTrial.CallingSystemID = freeTrialSubscription.CallingSystemID;
                            accessFreeTrial.CallingSystemTypeID = 2;
                            accessFreeTrial.CaptureDate = freeTrialSubscription.CaptureDateUTC;
                            accessFreeTrial.CaptureAttempt = freeTrialSubscription.CaptureAttempt;
                            accessFreeTrial.AuthorizationOnlyPackageID = freeTrialSubscription.PrimaryPackageID;
                            accessFreeTrial.SecondaryAuthorizationOnlyPackageID = freeTrialSubscription.AllSecondaryPackageID;
                            accessFreeTrial.AdminID = 0;
                            accessFreeTrial.AdminUserName = String.Empty;
                            accessFreeTrial.TransactionType = Spark.Common.AccessService.TransactionType.CancelFreeTrial;
                            accessFreeTrial.AuthorizationStatus = Spark.Common.AccessService.AuthorizationStatus.Successful;
                            accessFreeTrial.ConvertedOrderID = 0;
                            accessFreeTrial.AccessReasonID = 0;
                            accessFreeTrial.UnifiedActionTypeID = 2; // Remove privileges

                            AccessResponse accessResponse = AccessServiceWebAdapter.GetProxyInstance().AdjustAuthorizationOnlyPrivilege(accessFreeTrial);

                            if (accessResponse.InternalResponse.responsecode == "0")
                            {
                                g.Notification.AddMessage("FREETRIAL_UPDATED_SUCCESSFULLY");
                            }
                            else
                            {
                                g.Notification.AddError("FREETRIAL_UPDATE_ERROR");
                            }
                        }
                        else
                        {
                            g.Notification.AddError("FREETRIAL_NO_LONGER_ACTIVE");
                        }
                    }
                    else
                    {
                        //ensure any available A La Cartes are also "unchecked"
                        if ((chkALaCarteSpotlight.Checked && _planSpotlight != null)
                            || (chkALaCarteHighlight.Checked && _planHighlight != null)
                            || (chkALaCarteAllAccess.Checked && _planAllAccess != null))
                        {
                            g.Notification.AddErrorString(g.GetResource("NEED_TO_UNCHECK_A_LA_CARTE_SERVICES", this));
                        }
                        else
                        {
                            //this will disable auto-renewal for base subscription and all a la carte
                            g.Session.Add("AUTO_RENEW", "autorenoff", Matchnet.Session.ValueObjects.SessionPropertyLifetime.Temporary);
                            g.Transfer("/Applications/Termination/TerminateReason.aspx");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.Write("Error in AutoRenewalSettings.btnContinue_Click(), " + ex.ToString());

                g.ProcessException(ex);
            }
        }
    }
}
