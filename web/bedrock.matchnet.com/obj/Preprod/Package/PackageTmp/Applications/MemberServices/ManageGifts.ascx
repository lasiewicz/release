﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ManageGifts.ascx.cs" Inherits="Matchnet.Web.Applications.MemberServices.ManageGifts" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>

<div id="page-container" class="bogo-management">
    <mn:Title runat="server" id="ttlHeader" ResourceConstant="TXT_TITLE"/>
    <asp:Panel ID="pnlManagement" runat="server"  CssClass="bogo-management-cont form-set">
        <asp:Panel ID="pnlSend" runat="server" Visible="false">
            <mn:txt id="txtInstructions" runat="server" resourceconstant="TXT_INSTRUCTIONS" />
            <asp:TextBox runat="server" id="txtEmailAddress" CssClass="xlarge"></asp:TextBox>&nbsp;
            <mn2:FrameworkButton ID="btnSave" runat="server" ResourceConstant="TXT_SEND" class="btn primary" OnClientClick="javascript: return Spark_validateEmail();"></mn2:FrameworkButton>
            <div id="bogoEmailErrorInvalid" class="form-error-inline" style="display:none;"><mn:txt runat="server" id="txtValidEmail" resourceconstant="TXT_ENTER_VALID_EMAIL" expandimagetokens="true" /></div>
            <asp:Label ID="lblInstructions2" runat="server"></asp:Label>
        </asp:Panel>
        <asp:Panel ID="pnlCantBeCurrentSub" runat="server" Visible="false">
            <mn:txt id="txtCantBeCurrentSub" runat="server" resourceconstant="TXT_CANT_BE_CURRENT_SUB" />
        </asp:Panel>
        <asp:Panel ID="pnlWillBeSent" runat="server" Visible="false">
            <asp:Label ID="lblWillBeSent" runat="server"></asp:Label>
        </asp:Panel>
        <asp:Panel ID="pnlWasSent" runat="server" Visible="false">
            <asp:Label ID="lblWasSent" runat="server"></asp:Label>
        </asp:Panel> 
        <asp:Panel ID="pnlRedeemed" runat="server" Visible="false">
            <asp:Label ID="lblRedeemed" runat="server"></asp:Label>
        </asp:Panel>
    </asp:Panel>
    <asp:Panel ID="pnlNoGift" runat="server" Visible="false" CssClass="bogo-management-cont">
         <mn:txt id="txtNoGift" runat="server" resourceconstant="TXT_NOGIFT" />
    </asp:Panel>

<script>
$j(function(){
    $j('#bogoTerms').click(function(){
        $j('#bogoTermsContainer').dialog({
            width: 590,
            modal: true,
            title:'',
            dialogClass: 'ui-dialog-rev ui-dialog-no-title',
            open:function(){
                var reversedClose = $j('<div class="ui-dialog-titlebar-close-rev link-style">Close <span class="spr s-icon-closethick-color"></span></div>').bind('click',function(){
                    $j('#bogoTermsContainer').dialog('close');
                });
                $j(this).parent().find('.ui-dialog-titlebar-close').replaceWith(reversedClose);
            }
        });

    });

});

function Spark_validateEmail(){
    //cache jquery objects
    var $invalidEmailError = $j('#bogoEmailErrorInvalid'),
        $input = $j('[id*=txtEmailAddress]');
            
    var v = $input.val();
    v =  v.replace(/^\s+|\s+$/g, "");//trim spaces

    if(v.match(/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i)){
        $invalidEmailError.hide();
        $input.addClass('email-good').removeClass('email-bad');
        return true;
    }
    else{
        $invalidEmailError.show();
        $input.addClass('email-bad').removeClass('email-good');
        return false;
    }
}
</script>

</div>