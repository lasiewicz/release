﻿using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using Matchnet.Web.Framework;
namespace Matchnet.Web.Applications.MemberServices
{
    public partial class ReportTooMany20 : FrameworkControl
    {
        int TargetMemberID = Constants.NULL_INT;
        private void Page_Load(object sender, System.EventArgs e)
        {
            try
            {
                // Make the miniprofile unclickable
                MemberMiniProfile.Transparency = true;

                // Set breadcrumb trail accordingly
                g.BreadCrumbTrailHeader.SetTwoLinkCrumb(g.GetResource("TXT_REPORT_A_MEMBER", this), String.Empty);
                g.BreadCrumbTrailFooter.SetTwoLinkCrumb(g.GetResource("TXT_REPORT_A_MEMBER", this), String.Empty);

                TargetMemberID = ReportAbuse.GetMemberID();
                // Make the miniprofile unclickable
                if (g.LayoutVersion == WebConstants.LayoutVersions.versionWide)
                {
                    MemberMiniProfile.Visible = false;
                    MemberMiniProfile20.Visible = true;
                    MemberMiniProfile20.Transparency = true;
                    ReportAbuse.LoadMiniProfile(MemberMiniProfile20, TargetMemberID);
                }
                else
                {
                    MemberMiniProfile20.Visible = false;
                    MemberMiniProfile.Visible = true;
                    MemberMiniProfile.Transparency = true;
                    ReportAbuse.LoadMiniProfile(MemberMiniProfile, TargetMemberID);
                }
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }
        #endregion
    }
}