﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="ReportTooMany20.ascx.cs" Inherits="Matchnet.Web.Applications.MemberServices.ReportTooMany20" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn" TagName="miniProfile" Src="/Framework/UI/BasicElements/MiniProfile.ascx" %>
<%@ Register TagPrefix="mn1" TagName="miniProfile20" Src="/Framework/UI/BasicElements/MiniProfile20.ascx" %>
<div class="page-container">
	<h1><mn:txt runat="server" id="txtReportMember" ResourceConstant="TXT_REPORT_A_MEMBER" /></h1>
	<mn:miniprofile runat="server" id="MemberMiniProfile" />
	<div id="profile-mini-blocked"><mn1:miniProfile20 runat="server" id="MemberMiniProfile20" /></div>
	<mn:txt runat="server" id="txtAlreadyReported" ResourceConstant="TXT_ALREADY_REPORTED" />
	<p class="editorial"><mn:txt runat="server" id="txtBack" resourceconstant="BACK" Href="javascript:history.go(-1);" /></p>
</div>