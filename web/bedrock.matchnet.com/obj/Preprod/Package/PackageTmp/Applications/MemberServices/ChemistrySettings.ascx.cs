﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Web.Framework;
using Matchnet.Web.Applications.ColorCode;

namespace Matchnet.Web.Applications.MemberServices
{
    public partial class ChemistrySettings : FrameworkControl
    {
        #region Event Handlers
        protected void Page_Init(object sender, System.EventArgs e)
        {
            //Determine if member has color code
            if (g.Member != null)
            {
                if (ColorCodeHelper.HasMemberCompletedQuiz(g.Member, g.Brand))
                {
                    phColorCodeSetting.Visible = true;
                    if (!Page.IsPostBack)
                    {
                        if (ColorCodeHelper.IsMemberColorCodeHidden(g.Member, g.Brand))
                        {
                            rbColorCodeHide.Checked = true;
                        }
                        else
                        {
                            rbColorCodeShow.Checked = true;
                        }
                    }
                }
                else
                {
                    phCodeCodeTakeTest.Visible = true;
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                bool hideColorCode = rbColorCodeHide.Checked;
                ColorCodeHelper.SetMemberColorCodePrivacy(g.Member, g.Brand, hideColorCode);
                g.Notification.AddMessage("SUCCESSFULLY_SAVED_YOUR_PROFILE_DISPLAY_SETTINGS");
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        #endregion
    }
}