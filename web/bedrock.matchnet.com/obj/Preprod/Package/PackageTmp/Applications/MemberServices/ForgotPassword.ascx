﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ForgotPassword.ascx.cs" Inherits="Matchnet.Web.Applications.MemberServices.ForgotPassword" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn1" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>


<style type="text/css">
    
  .resetPassword  li 
    {
        padding: 5px 5px 5px 5px;
        list-style-type:square;
    }
    
</style>

<div id="page-container">
<asp:Panel ID="pnlReset" runat="server">
    <h1><mn:Txt runat="server" id="txtForgotPassword" ResourceConstant="FORGOT_PASSWORD_HEADER" /></h1>
    <h3 class="tag-line"><mn:Txt runat="server" id="txt1" ResourceConstant="FORGOT_PASSWORD_DESC" /></h3>
    <div class="rbox-wrapper-member" style="margin-top:10px;">
        <fieldset id="loginBox" class="rbox-style-high login-box member">
            <dl class="dl-form" style="padding:20px 20px 20px 20px">
	                <dt><strong><mn:Txt runat="server" id="txt2" ResourceConstant="FORGOT_PASSWORD_EMAIL" /></strong></dt>
	                <dd><asp:TextBox runat="server" ID="tbxEmailAddress" Width="200"></asp:TextBox></dd>
                    <dt></dt>
                    <dd></dd>
	               <dt>&nbsp;</dt>
	                <dd class="cta" style="padding-top:10px;">
                        <mn1:frameworkbutton runat="server" id="btnResetPassword" ResourceConstant="BTN_RESET_PASSWORD"></mn1:frameworkbutton>
	                </dd>
            </dl>
        </fieldset>
    </div>
</asp:Panel>
<asp:Panel  ID="pnlResetMsg" runat="server" visible="false" CssClass="dl-form">
	<h1><mn:Txt runat="server" id="txt3" ResourceConstant="FORGOT_PASSWORD_HEADER_CONFIRM" /></h1>
    <div class="rbox-wrapper-member" style="margin-top:10px;">
        <fieldset id="Fieldset1" class="rbox-style-high login-box member">
            <dl class="dl-form" style="padding:20px 20px 20px 20px">
	                  <b><mn:Txt runat="server" id="txt9" ResourceConstant="FORGOT_PASSWORD_MAIN_DESC" /></b>
                      <ul class="resetPassword" style="padding: 10px 10px 10px 10px;">
                        <li> 
                            <mn:Txt runat="server" id="txt5" ResourceConstant="FORGOT_PASSWORD_MAIN_1" /> <a href="/Applications/Memberservices/ForgotPassword.aspx"><mn:Txt runat="server" id="txt4" ResourceConstant="FORGOT_PASSWORD_TRY_AGAIN" /></a>.
                        </li>
                        <li><mn:Txt runat="server" id="txt6" ResourceConstant="FORGOT_PASSWORD_MAIN_2" /></li>
                        <li><mn:Txt runat="server" id="txt7" ResourceConstant="FORGOT_PASSWORD_MAIN_3" /></li>
                        <li><mn:Txt runat="server" id="txt8" ResourceConstant="FORGOT_PASSWORD_MAIN_4" /> <a href="/applications/contactus/contactus.aspx"><mn:Txt runat="server" id="txt10" ResourceConstant="FORGOT_PASSWORD_CUSTOMER_CARE" /></a>.</li>
                      </ul>
            </dl>
        </fieldset>
    </div>
</asp:Panel>
</div>

 
