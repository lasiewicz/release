﻿using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using System.Collections;
using System.Collections.Generic;
using Matchnet.Web.Framework;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects;
using Matchnet.Member.ValueObjects.Privilege;
using Matchnet.Content.ServiceAdapters.Links;
using Matchnet.Web.Framework.Util;

using Matchnet.PremiumServices.ValueObjects;
using Matchnet.PremiumServices.ServiceAdapter;
using Matchnet.Web.Applications.PremiumServices;

namespace Matchnet.Web.Applications.MemberServices
{
    public partial class PremiumServiceSettings20 : PremiumServiceSettingBase
    {
        List<IPremiumServiceSettings> settings;
        Matchnet.Member.ServiceAdapters.Member _member = null;
        Matchnet.Content.ValueObjects.BrandConfig.Brand _brand = null;
        bool _adminMode = false;
        DateTime _highLightProfileExpirationDate = DateTime.MinValue;
        private void Page_Init(object sender, EventArgs e)
        {
            try
            {
                settings = new List<IPremiumServiceSettings>();
                //bindEnableDisableControl(rbgHighlightProfile);
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }


        private void Page_Load(object sender, System.EventArgs e)
        {
            try
            {
                if (Request.QueryString["impmid"] != null && Request.QueryString["imppid"] != null)
                {
                    // Opened by admin
                    if (!g.HasPrivilege(g.Member.MemberID, (int)PrivilegeType.PRIVILEGE_EDIT_MEMBER_ATTRIBUTES))
                    {
                        g.Transfer("/Default.aspx");
                    }

                    if (g.TargetMemberID != Constants.NULL_INT && g.TargetMember != null)
                    {
                        _member = g.TargetMember;
                        _brand = g.TargetBrand;
                        _adminMode = true;
                    }
                }
                else
                {
                    // Opened by member
                    _member = g.Member;
                    _brand = g.Brand;
                }

                if (_member != null && _brand != null)
                {
                    //ArrayList activePremiumServices = FrameworkGlobals.activePremiumServices(_member.MemberID, _brand);
                    Member = _member;
                    Brand = _brand;

                    // Only members with at least one active premium service subscriptions are allowed 
                    // to change settings  
                    if (MemberPremiumServices == null || MemberPremiumServices.Count == 0)
                    {
                        // Member has no active premium service
                        if (!_adminMode)
                        {
                            // Member will be redirected to the subscription page with AttemptToAccessPremiumServiceSettings as
                            // the purchase reason type
                            Redirect.Subscription(_brand, (Int32)PurchaseReasonType.AttemptToAccessPremiumServiceSettings, 0);
                        }
                        else
                        {
                            // Admin will receive status notification
                            g.Notification.AddMessage("NO_PREMIUM_SERVICE");
                        }
                        btnSave.Visible = false;
                    }
                    else
                    {

                        if (MemberPremiumServices.HasService((int)Matchnet.PremiumServices.ValueObjects.ServiceConstants.ServiceIDs.SpotlightMember))
                        {
                          
                            plcSpotlightInfo.Visible = true;
                            plcAdditionalInfo.Visible = !_adminMode;

                        }
                    }



                }
                rptPremiumServiceContainer.ItemDataBound += new RepeaterItemEventHandler(rptPremiumServiceContainer_ItemDataBound);
                rptPremiumServiceContainer.DataSource = MemberPremiumServices;
                rptPremiumServiceContainer.DataBind();
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.Write("Error in PremiumServiceSettings.Page_Load(), " + ex.ToString());

                g.ProcessException(ex);
            }
        }


        protected void rptPremiumServiceContainer_ItemDataBound(Object sender, RepeaterItemEventArgs e)
        {
            try
            {

                InstanceMember m = (InstanceMember)e.Item.DataItem;
                PremiumService s = Matchnet.PremiumServices.ServiceAdapter.ServiceSA.Instance.GetPremiumService(m.ServiceInstanceID);

                // Literal plcholder = ((Literal)e.Item.FindControl("plcPremiumServiceContainer"));
                string controlpath = "";
                switch (s.ServiceID)
                {
                    case (int)Matchnet.PremiumServices.ValueObjects.ServiceConstants.ServiceIDs.HighligtedProfile:
                        controlpath = "/Applications/PremiumServices/Controls/HighlightedMemberControl20.ascx";
                        break;

                    case (int)Matchnet.PremiumServices.ValueObjects.ServiceConstants.ServiceIDs.SpotlightMember:
                        controlpath = "/Applications/PremiumServices/Controls/SpotlightMemberControl20.ascx";
                        break;

                    default:
                        break;

                }

                if (!String.IsNullOrEmpty(controlpath))
                {

                    IPremiumServiceSettings control = (IPremiumServiceSettings)LoadControl(controlpath);
                    control.SetMember(_brand, _member, m);

                    ((FrameworkControl)control).AppInit();
                    e.Item.Controls.Add((FrameworkControl)control);
                    settings.Add(control);
                }


            }
            catch (Exception ex)
            {

            }

        }


        private void btnSave_Click(object sender, System.EventArgs e)
        { 
            try
            {
                if (Page.IsValid)
                {
                    for (int i = 0; i < settings.Count; i++)
                    {
                        IPremiumServiceSettings item = settings[i];
                        item.Save();
                    }

                    string msg = g.GetResource("TXT_PS_SAVED", this);
                    g.Notification.AddMessageString(msg);
                }
            }
            catch (Exception ex)
            {
                g.Notification.AddErrorString(g.GetResource("TXT_PS_SAVED_ERROR", this));
                g.ProcessException(ex);
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            this.Init += new System.EventHandler(this.Page_Init);
        }
        #endregion
    }
}