﻿using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;

using Matchnet.Web.Framework;
using Matchnet.Lib;

namespace Matchnet.Web.Applications.MemberServices
{
    public partial class VerifyEmailNag20 :FrameworkControl
    {
        private void Page_Init(object sender, EventArgs e)
        {
            try
            {
                if (g.EmailVerify.EnableEmailVerification)
                {
                    if (g.Member != null)
                    {
                        int globalStatusMask = g.Member.GetAttributeInt(g.Brand, "GlobalStatusMask", 0);
                        string emailAddress = g.Member.EmailAddress;

                        //this is a temporary hack that will check to see if the user is verified
                        //if so directo to another page and attempt to send a verification email
                        //but since the user is verified already they will instead get a message saying so
                        if ((globalStatusMask & WebConstants.EMAIL_VERIFIED) == WebConstants.EMAIL_VERIFIED)
                        {
                            g.Transfer("/Applications/MemberServices/VerifyEmail.aspx?a=SendEmail");
                        }
                        g.Transfer("/Applications/MemberServices/VerifyEmail.aspx");
                    }
                    else
                    {
                        g.Transfer("/Applications/MemberServices/VerifyEmail.aspx");
                    }
                }

            }
            catch (System.Threading.ThreadAbortException ex)
            { }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        private void Page_Load(object sender, System.EventArgs e)
        {

            if (g.BreadCrumbTrailHeader != null)
            {
                g.BreadCrumbTrailHeader.SetTwoLinkCrumb(g.GetResource("TXT_VERIFY_YOUR_EMAIL_ADDRESS", this),
                                                        g.AppPage.App.DefaultPagePath);

            }
            if (g.BreadCrumbTrailFooter != null)
            {
                g.BreadCrumbTrailFooter.SetTwoLinkCrumb(g.GetResource("TXT_VERIFY_YOUR_EMAIL_ADDRESS", this),
                                                       g.AppPage.App.DefaultPagePath);
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
            this.Init += new System.EventHandler(this.Page_Init);

        }
        #endregion
    }
}