﻿using Matchnet.Member.ServiceAdapters;
using Matchnet.Web.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Matchnet.Web.Applications.MemberServices
{
    public partial class RamahDateQuestions :  FrameworkControl
    {

        private const string RAMAH_ALUM = "RamahAlum";
        private const string RAMAH_CAMP = "RamahCamp";
        private const string RAMAH_START_YEAR = "RamahStartYear";
        private const string RAMAH_END_YEAR =  "RamahEndYear";
        private const string RAMAH_INSERT_DATE = "RamahInsertDate";
        private const string RAMAH_TOS = "RamahTOS";

        private const int FIRST_RAMAH_YEAR = 1947;

        protected void Page_Load(object sender, EventArgs e)
        {

            // check to see if the member already has RamahAlum if so show them a message
            if(Convert.ToBoolean(_g.Member.GetAttributeInt(_g.Brand,RAMAH_ALUM)))
            {
                //show already have message
                pnlHideQuestions.Visible = true;
            }
            else
            {
                //show questions 
                pnlShowQuestions.Visible = true;

                SetUpPageControls();
            }

        }

        protected void Btn_Click(object sender, EventArgs e)
        {

            if (cbxRamahTos.Checked)  
            {

                // lets do some server side validation
                List<ListItem> selected = cblCamps.Items.Cast<ListItem>().Where(li => li.Selected).ToList();


                if (!IsRamahValid(selected.Count))
                {
                    pnlError.Visible = true;
                }
                else
                {

                    int startYear = Convert.ToInt32(ddStartDate.SelectedValue);
                    int endYear = Convert.ToInt32(ddEndDate.SelectedValue);

                    try
                        {
                            Camps? mask = null;

                            foreach (ListItem li in selected)
                            {
                                foreach (int i in Enum.GetValues(typeof(Camps)))
                                {
                                    if (Convert.ToInt32(li.Value) == i)
                                    {
                                        if (mask == null)
                                            mask = (Camps)i;
                                        else
                                            mask = mask | (Camps)i;
                                    }

                                }
                            }


                            //lets add Ramah Date attributes for member. 
                            g.Member.SetAttributeDate(g.Brand, RAMAH_INSERT_DATE, DateTime.Now);

                            g.Member.SetAttributeInt(g.Brand, RAMAH_START_YEAR, Convert.ToInt32(ddStartDate.SelectedValue));

                            // Ramah Camp is a Mask

                            g.Member.SetAttributeInt(g.Brand, RAMAH_CAMP, (int)mask);

                            if (ddEndDate.SelectedValue != "")
                                g.Member.SetAttributeInt(g.Brand, RAMAH_END_YEAR, Convert.ToInt32(ddEndDate.SelectedValue));

                            g.Member.SetAttributeInt(g.Brand, RAMAH_ALUM, 1);

                            //set the RamahTOS
                            // g.Member.SetAttributeInt(g.Brand, RAMAH_TOS, 1);

                            //save the member after setting the attributes
                            MemberSA.Instance.SaveMember(g.Member);

                            //show success messages.
                            pnlError.Visible = false;
                            pnlShowQuestions.Visible = false;
                            pnlHideQuestions.Visible = false;

                            pnlSuccess.Visible = true;
                        }
                        catch (Exception ex)
                        {
                            g.ProcessException(ex);
                        }

                }

            }
            else
               pnlTOS.Visible = true;

        }

        //helpers
        private bool IsRamahValid(int count)
        {
            if (count == 0)
                return false;
            
            int startYear = Convert.ToInt32(ddStartDate.SelectedValue);
            int endYear = Convert.ToInt32(ddEndDate.SelectedValue);

            if (startYear > endYear)
                return false;

            return true;
        }



        private void SetUpPageControls()
        {

            pnlTOS.Visible = false;
            
            //start date
            ddStartDate.DataSource = GetCampYears(false);
            ddStartDate.DataBind();

            //end date
            ddEndDate.DataSource = GetCampYears(true);
            ddEndDate.DataBind();

        }
        
        private List<ListItem> GetCampYears(bool reverse)
        {
             int todayYear = DateTime.Now.Year;
             var years = new List<ListItem>();

             if (todayYear > FIRST_RAMAH_YEAR)
            {
                
                int numOfYears = todayYear - FIRST_RAMAH_YEAR;
                int currentYear = 0;

                for(int i=0; i <=numOfYears; i++ )
                {
                    currentYear = FIRST_RAMAH_YEAR + i;
                    years.Add(new ListItem(currentYear.ToString(), currentYear.ToString()));
                }
            }

             if (reverse)
                 years.Reverse();


            return years;
        }


        //RamahCamps Enum

        [Flags]
        public enum Camps
        {
            None = 0,
           Berkshires=1,
           California=2,
        Canada=4,
        Chicago=8,
        Connecticut=16,
        Darom=32,
        GlenSpey=64,
        Jerusalem=128,
        Mador=256,
        Maine=512,
        NewEngland=1024,
        Nyack=2048,
        Philadelphia=4096,
        Poconos=8192,
        Rockies=16384,
        SEE=32768,
        Seminar=65536,
        TRYUSYHigh=131072,
        Wisconsin=262144

        }

    }
}