﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="MemberServices20.ascx.cs" Inherits="Matchnet.Web.Applications.MemberServices.MemberServices20" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<div id="page-container">
    <mn:Title runat="server" id="ttlMemberServices" ResourceConstant="MEMBER_SERVICES_TITLE"/>
	<h2 class="content-headline"><mn:Txt runat="server" id="txtProfile" ResourceConstant="PROFILE_TITLE" /></h2>
	<ul>
        <li><mn:Txt runat="server" id="txtShowHideProfile" href="/Applications/MemberServices/DisplaySettings.aspx" ResourceConstant="SHOW_HIDE_PROFILE" /></li>
	    <li><mn:txt runat="server" id="txtChangeYourProfile" href="/Applications/MemberProfile/ViewProfile.aspx?EntryPoint=99999" ResourceConstant="TXT_CHANGE_YOUR_PROFILE" /></li>
        <li><mn:txt runat="server" id="txtChangeEmail" href="/Applications/MemberProfile/ChangeEmail.aspx" ResourceConstant="TTL_CHANGE_EMAIL_PASSWORD" /></li>
        <asp:PlaceHolder ID="phChemistrySettings" runat="server" Visible="false">
        <li><mn:Txt runat="server" ID="txtChemistry" ResourceConstant="TXT_CHEMISTRY_SETTINGS" Href="/Applications/MemberServices/ChemistrySettings.aspx" /></li>
    </asp:PlaceHolder>
	</ul>
	<h2 class="content-headline"><mn:Txt runat="server" id="txtMembershipManagement" ResourceConstant="MEMBERSHIP_MANAGEMENT_TITLE" /></h2>
    <ul>
	<asp:PlaceHolder ID="pnlSubInfo" Runat="server">
	    <li><mn:txt runat="server" id="txtSubscriptionPlans" href="/Applications/Subscription/Plan.aspx" ResourceConstant="SUBSCRIPTION_PLANS_AND_COSTS" /></li>
	<asp:PlaceHolder ID="pnlAutoRenewalSettings" Runat="server">
	    <li><mn:Txt id="txtCancelMembership" href="/Applications/MemberServices/AutoRenewalSettings.aspx" runat="server" /></li>
	</asp:PlaceHolder>
	    <li><mn:Txt id="txtSubscribeLink" runat="server" ResourceConstant="NAV_SUBSCRIBE" href="" /></li>
	</asp:PlaceHolder>
    <asp:PlaceHolder ID="PlaceHolderManageGifts" Runat="server" Visible="false">
        <li><mn:Txt id="TextManageGifts" runat="server" ResourceConstant="TXT_MANAGEGIFTS" href="/Applications/MemberServices/ManageGifts.aspx" /></li>    
    </asp:PlaceHolder>
	<asp:PlaceHolder ID="PlaceHolderPremiumServices" Runat="server" Visible="false">
		<li><mn:Txt id="TextPremiumServices" runat="server" ResourceConstant="TXT_PREMIUMSERVICES" href="/Applications/Subscription/PremiumServices.aspx?prtid=933" /></li>
	</asp:PlaceHolder>		
	<asp:PlaceHolder ID="pnlPremiumServiceSettings" Runat="server">
		<li><mn:Txt id="txtPremiumServiceSettings" runat="server" ResourceConstant="TXT_PREMIUMSERVICESETTINGS" href="/Applications/MemberServices/PremiumServiceSettings.aspx" /></li>
	</asp:PlaceHolder>	
	    <li><mn:txt runat="server" id="Txt2" href="/Applications/MemberServices/Suspend.aspx" ResourceConstant="NAV_SUSPEND_MEMBERSHIP" /></li>
	<asp:PlaceHolder ID="pnlAccountInfo" Runat="server">
		<li><mn:txt runat="server" href="/Applications/Subscription/History.aspx" id="txtPaymentRecord" ResourceConstant="TXT_ACCOUNT_INFORMATION" /></li>
	</asp:PlaceHolder>
	<asp:PlaceHolder ID="pnlPaymentProfile" Runat="server">
		<li><mn:txt runat="server" id="txtBillingInformation" ResourceConstant="TXT_BILLING_INFORMATION" /></li>
	</asp:PlaceHolder>
	    <li><mn:txt id="Txt1" runat="server" ResourceConstant="TXT_VERIFY_EMAIL" href="/Applications/MemberServices/VerifyEmailNag.aspx" /></li>
	</ul>
	
	<h2 class="content-headline"><mn:Txt runat="server" id="txtToolsAndSettings" ResourceConstant="TOOLS_AND_SETTINGS_TITLE" /></h2>
	<ul>
	<asp:PlaceHolder ID="pnlChatIMSettings" runat="server">
	    <li><mn:Txt runat="server" ID="txtChatIMSettings" ResourceConstant="TXT_CHAT_IM_SETTINGS" /></li>
	</asp:PlaceHolder>
	<li><mn:Txt runat="server" id="txtGeneralAlertsSettings" ResourceConstant="TXT_MESSAGE_ALERTS_SETTINGS" Href="/Applications/Email/MessageSettings.aspx" /></li>
    <asp:PlaceHolder ID="pnlContactBlock" runat="server">
        <li><mn:txt runat="server" id="txtBlock" ResourceConstant="NAV_SUB_IGNORED_BLOCKED" href="/Applications/HotList/View.aspx?CategoryID=-13" /></li>
    </asp:PlaceHolder>            
        <li><mn:Txt runat="server" id="txtAlertsSettings" ResourceConstant="OFF_SITE_SETTINGS" Href="/Applications/Email/MessageSettings.aspx#OffSite" /></li>
        <li><mn:txt runat="server" id="txtMembersWhoEmailed" ResourceConstant="TXT_MEMBERS_WHO_EMAILED_YOU" href="/Applications/HotList/View.aspx?CategoryID=-4" /></li>
        <li><mn:txt runat="server" id="txtNeverMiss" ResourceConstant="TXT_NEVER_MISS_EMAILS" href="/Applications/Article/ArticleView.aspx?CategoryID=1938&amp;ArticleID=6312&amp;RowNumber=1" /></li>
    </ul>

   <asp:PlaceHolder ID="pnlProfileBlock" runat="server">
   <h2 class="content-headline"><mn:Txt runat="server" id="txtBlockedMemberLists" ResourceConstant="BLOCKED_TITLE" /></h2>
   <ul>
	    <li><mn:txt runat="server" id="txtBlockedFromContact" href="/Applications/HotList/View.aspx?CategoryID=-13" ResourceConstant="TXT_BLOCKED_FROM_CONTACT" /></li>
	    <li><mn:txt runat="server" id="txtBlockedFromSearch" href="/Applications/HotList/View.aspx?CategoryID=-34" ResourceConstant="TXT_BLOCKED_FROM_SEARCH" /></li>
	    <li><mn:txt runat="server" id="txtBlockedFromViewing" href="/Applications/HotList/View.aspx?CategoryID=-35" ResourceConstant="TXT_BLOCKED_FROM_VIEWING" /></li>
   </ul>
   </asp:PlaceHolder>

	<h2 class="content-headline"><mn:Txt runat="server" id="txtHelp" ResourceConstant="HELP_TITLE" /></h2>
    <ul>
	    <li><mn:txt runat="server" id="txtFaq" href="/Applications/Article/FAQMain.aspx" ResourceConstant="FAQ" /></li>
	    <li><mn:txt runat="server" id="txtContactUs" href="/Applications/ContactUs/ContactUs.aspx" ResourceConstant="CONTACTUS" /></li>
    </ul>
    
	<h2 class="content-headline"><mn:Txt runat="server" id="txtLegal" ResourceConstant="LEGAL_TITLE" /></h2>
    <ul>
        <li><mn:Txt runat="server" href="/Applications/Article/ArticleView.aspx?CategoryID=1948&amp;ArticleID=6498&amp;HideNav=True#privacy" ResourceConstant="PRIVACY_STATEMENT" id="txtPrivacyStatement" /></li>
        <li><mn:Txt runat="server" id="txtTerms" ResourceConstant="TERMS_AND_CONDITIONS_OF_SERVICE"	href="/Applications/Article/ArticleView.aspx?CategoryID=1948&amp;ArticleID=6498&amp;HideNav=True#service" /></li>
        <li><mn:Txt runat="server" id="txtTermsOfPurchase" href="/Applications/Article/ArticleView.aspx?CategoryID=1948&amp;ArticleID=6498&amp;HideNav=True#purchase" ResourceConstant="TERMS_AND_CONDITIONS_OF_PURCHASE" /></li>
    </ul>
</div>
