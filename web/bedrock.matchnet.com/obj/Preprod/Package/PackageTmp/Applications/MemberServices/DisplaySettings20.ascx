﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="DisplaySettings20.ascx.cs" Inherits="Matchnet.Web.Applications.MemberServices.DisplaySettings20" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
	
<div id="page-container">
	<mn:Title runat="server" id="ttlDisplaySettings" ResourceConstant="TXT_PROFILE_SHOW_HIDE_SETTINGS"/>

	<h3><mn:Txt runat="server" id="txtMembersOnline" ResourceConstant="MEMBERSERVICES_MEMBERS_ONLINE" /></h3>
	<p class="border-top-dotted"><mn:Txt runat="server" id="txtMembersDescription" ResourceConstant="PROFILEDISPLAY___MEMBERSONLINE_DEFINITION_520039" /></p>
	<blockquote class="editorial"><asp:RadioButtonList Runat="server" ID="rblMembersOnline" RepeatColumns="2" RepeatDirection="Horizontal" RepeatLayout="Flow" /></blockquote>
	    <input type="hidden" value="4" id="rblMembersOnlineCheck" runat="server">
	
	<h3><mn:Txt runat="server" id="txtSearches" ResourceConstant="SEARCHES" /></h3>
	<p class="border-top-dotted"><mn:Txt runat="server" id="PROFILEDISPLAY___SEARCHES_DEFINITION_520040" ResourceConstant="PROFILEDISPLAY___SEARCHES_DEFINITION_520040" /></p>
	<blockquote class="editorial"><asp:RadioButtonList Runat="server" ID="rblSearches" RepeatColumns="2" RepeatDirection="Horizontal" RepeatLayout="Flow" /></blockquote>
		<input type="hidden" value="1" id="rblSearchesCheck" runat="server">
	
	<h3><mn:Txt runat="server" id="txtPhoto" ResourceConstant="MEMBERSERVICES_PHOTOS" /></h3>
	<p class="border-top-dotted"><mn:Txt runat="server" id="txtPhotoDescription" ResourceConstant="PROFILEDISPLAY___PHOTOS_DEFINITION_520041" /></p>
    <blockquote class="editorial"><asp:RadioButton id="AllUsers" GroupName="ShowPhoto" Runat="server" />
    <mn:Txt runat="server" id="txtShowPhotos" ResourceConstant="SHOW_TO_MEMBERS___NON_MEMBERS_520181" />
	<asp:RadioButton id="MembersOnly" GroupName="ShowPhoto" Runat="server" />
	<mn:Txt runat="server" id="txtMembersOnly" ResourceConstant="SHOW_TO_MEMBERS_ONLY" /></blockquote>
		
	<h3><mn:Txt runat="server" id="txtHotList" ResourceConstant="TXT_SHOW_HIDE_WHEN_YOU_VIEW_LIST_MEMBERS" /></h3>
	<p class="border-top-dotted"><mn:Txt runat="server" id="txtHotListDescription" ResourceConstant="TXT_SHOW_PROFILE_IN_HOTLISTS" /></p>
	<blockquote class="editorial"><asp:RadioButtonList Runat="server" ID="rblHotList" RepeatColumns="2" RepeatDirection="Horizontal" RepeatLayout="Flow" /></blockquote>
	    <input type="hidden" value="2" id="rblHotListCheck" runat="server">
	
	<p class="text-center"><asp:LinkButton cssclass="link-primary buttonALink" ID="btnSaveSettings" Runat="server" /></p>

	<p class="border-top-dotted"><mn:Txt runat="server" id="txtFooterText" ResourceConstant="TXT_SHOW_HIDE_NOTE" /></p>
</div>
