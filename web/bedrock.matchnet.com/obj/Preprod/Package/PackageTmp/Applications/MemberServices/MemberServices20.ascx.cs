﻿using System;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Web.Framework;

using Matchnet.Member.ServiceAdapters;
using Matchnet.Web.Applications.Subscription;
using Matchnet.PremiumServiceSearch11.ServiceAdapters;
using Matchnet.Web.Applications.ColorCode;
using Spark.Common.AuthorizationService;
using Spark.Common.PaymentProfileService;
using Spark.Common.Adapter;
using Matchnet.PremiumServices.ValueObjects;
using Matchnet.Web.Framework.Menus;
using Matchnet.Web.Framework.Managers;

namespace Matchnet.Web.Applications.MemberServices
{
    public partial class MemberServices20 : FrameworkControl
    {
        private void Page_Load(object sender, System.EventArgs e)
        {
            try
            {
                //corrects https bug introduced in V1.7
                txtSubscriptionPlans.Href = FrameworkGlobals.GetSubscriptionLink(false, (int)Constants.PRTID.MemberServicesSubscriptionPlansAndCosts);
                ///
                if (g.Brand.IsPaySite)
                {
                    pnlSubInfo.Visible = true;
                }
                else
                {
                    pnlSubInfo.Visible = false;
                }

                Matchnet.Member.ServiceAdapters.Member member = g.Member;

                DateTime subscriptionExpirationDate = member.GetAttributeDate(g.Brand, WebConstants.ATTRIBUTE_NAME_SUBSCRIPTIONEXPIRATIONDATE, DateTime.MinValue);
                //Need to detect whether the user ever had a subscription on the current Site.  This is true if and only if they have a SubscriptionExpirationDate attribute.
                //Note: MemberPrivilegeAttr.IsCureentSubscribedMember(g) will not work because this only returns true if the user has a current subscription.

                int iPrtID = (int)Constants.PRTID.MemberServicesSubscribe;
                if (subscriptionExpirationDate != DateTime.MinValue)
                {
                    txtSubscribeLink.Href = FrameworkGlobals.GetSubscriptionLink(iPrtID);
                    //txtPaymentRecord.Visible = true;
                    // need above in a panel so the other html code (<br /> etc...) gets hidden/shown correctly
                    pnlAccountInfo.Visible = true;
                }
                else
                {
                    txtSubscribeLink.Href = FrameworkGlobals.GetSubscriptionLink(iPrtID);
                    //txtPaymentRecord.Visible = false;
                    pnlAccountInfo.Visible = false;
                }

                // Show the auto renewal settings only if the member is a current subscriber

                PrepAutoRenewalLink();



                if ((Convert.ToBoolean((RuntimeSettings.GetSetting("IS_UPS_INTEGRATION_ENABLED", g.Brand.Site.Community.CommunityID,
   g.Brand.Site.SiteID, g.Brand.BrandID)))))
                {
                    if ((Convert.ToBoolean((RuntimeSettings.GetSetting("IS_UPS_ALC_ENABLED", g.Brand.Site.Community.CommunityID,
g.Brand.Site.SiteID, g.Brand.BrandID)))))
                    {
                        if (MemberPrivilegeAttr.IsCureentSubscribedMember(g))
                        {
                            ArrayList actives =
                                PremiumServiceSearch11.ServiceAdapters.PremiumServiceSA.Instance.
                                    GetMemberActivePremiumServices(
                                    member.MemberID, g.Brand);

                            if (actives.Count == 5)
                            {
                                PlaceHolderPremiumServices.Visible = false;
                            }
                            else
                            {
                                PlaceHolderPremiumServices.Visible = true;
                            }
                        }
                    }
                    else
                    {
                        PlaceHolderPremiumServices.Visible = false;
                    }
                }
                else
                {
                    PlaceHolderPremiumServices.Visible = false;
                }

                if (g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.Cupid)
                {
                    PlaceHolderPremiumServices.Visible = false;
                }

                if (Convert.ToBoolean(RuntimeSettings.GetSetting("ENABLE_BOGO", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID)))
                {
                    PlaceHolderManageGifts.Visible = true;
                }

                // Show the premium service settings
                pnlPremiumServiceSettings.Visible = MenuUtils.ShowPremiumServicesSettings(g);

                //disply link to pyament profile
                if (SubscriptionHelper.IsPaymentProfileEditable(g)) 
                {
                    pnlPaymentProfile.Visible = true;
                    if(SubscriberManager.Instance.IsIAPSubscriber(g.Member, g.Brand))
                    {

                        if (g.Brand.Site.SiteID == (int)Matchnet.Web.Framework.WebConstants.SITE_ID.JDateCoIL)
                            txtBillingInformation.Href = "http://static.jdate.com/Components/IAP/IL";
                        else if (g.Brand.Site.SiteID == (int)Matchnet.Web.Framework.WebConstants.SITE_ID.JDateFR)
                            txtBillingInformation.Href = "http://static.jdate.com/Components/IAP/FR/";
                        else
                            txtBillingInformation.Href = "http://static.jdate.com/Components/IAP/";

                        txtBillingInformation.Target = "_blank";
                    }
                    else
                        txtBillingInformation.Href = FrameworkGlobals.LinkHrefSSL("/Applications/BillingInformation/PaymentProfile.aspx");
                }
                else
                {
                    pnlPaymentProfile.Visible = false;
                }

                // Chat IM settings page link
                if (Convert.ToBoolean(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("CONNECT_CHAT_ENABLE", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID)))
                {
                    txtChatIMSettings.Href = FrameworkGlobals.BuildConnectFrameworkLink("chat/settings.html", g, true);
                }
                else
                {
                    pnlChatIMSettings.Visible = false;
                }

                // Self suspended? Redirect the user to unsuspend page
                int isSuspended = member.GetAttributeInt(g.Brand, "SelfSuspendedFlag");

                if (isSuspended == 1)
                {
                    g.Transfer("/Applications/MemberServices/Unsuspend.aspx?rc=" + Request["rc"]);
                }

                //color code
                phChemistrySettings.Visible = ColorCodeHelper.IsColorCodeEnabled(g.Brand);

                if (g.BreadCrumbTrailHeader != null)
                {
                    g.BreadCrumbTrailHeader.SetTwoLinkCrumb(g.GetResource("MEMBER_SERVICES_TITLE", this),
                                                            g.AppPage.App.DefaultPagePath);

                }
                if (g.BreadCrumbTrailFooter != null)
                {
                    g.BreadCrumbTrailFooter.SetTwoLinkCrumb(g.GetResource("MEMBER_SERVICES_TITLE", this),
                                                           g.AppPage.App.DefaultPagePath);
                }

                // visibility related to the profile blocking feature
                bool profileBlockUIEnabled = Convert.ToBoolean(RuntimeSettings.GetSetting("PROFILE_BLOCK_UI_ENABLED", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID));
                pnlContactBlock.Visible = !profileBlockUIEnabled;
                pnlProfileBlock.Visible = profileBlockUIEnabled;

            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        private void PrepAutoRenewalLink()
        {

            try
            {
                if (MemberPrivilegeAttr.IsCureentSubscribedMember(g))
                {

                    pnlAutoRenewalSettings.Visible = true;

                    if (SubscriberManager.Instance.IsIAPSubscriber(g.Member, g.Brand))
                    {
                        txtCancelMembership.Href = "http://static.jdate.com/Components/IAP/";
                        txtCancelMembership.Target = "_blank";
                        txtCancelMembership.Text = g.GetResource("TXT_CANCEL_MY_SUBSCRIPTION", this); // "Cancel My Subscription";
                    }
                    else
                    {
                        Spark.Common.RenewalService.RenewalSubscription renewalSub = RenewalManager.Instance.GetRenewalSubscription(g.TargetMemberID, g.TargetBrand);
                        if (renewalSub.IsRenewalEnabled)
                            txtCancelMembership.Text = g.GetResource("TXT_CANCEL_MY_SUBSCRIPTION", this); // "Cancel My Subscription";
                        else
                            txtCancelMembership.Text = g.GetResource("TXT_REACTIVATE_MY_SUB", this); //"Reactivate my Subscription";
                    }
                    
                }
                else
                {
                    pnlAutoRenewalSettings.Visible = false;
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.WriteLine("Error in checking to see if the member has an active free trial subscription, Error message: " + ex.Message);
            }


            // Show the auto renewal settings if the member has an active free trial subscription 
            try
            {
                bool hasActiveFreeTrialSubcription = false;

                hasActiveFreeTrialSubcription = AuthorizationServiceWebAdapter.GetProxyInstance().HasActiveFreeTrialSubscription(g.TargetMemberID, g.TargetBrand.Site.SiteID);

                if (hasActiveFreeTrialSubcription)
                {
                    pnlAutoRenewalSettings.Visible = true;
                    txtCancelMembership.Text = g.GetResource("TXT_CANCEL_MY_SUBSCRIPTION", this);
                }
                
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.WriteLine("Error in checking to see if the member has an active free trial subscription, Error message: " + ex.Message);
            }
            finally
            {
                AuthorizationServiceWebAdapter.CloseProxyInstance();
            }

            // for some reason, Cupid disables this 
            //if (g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.Cupid)
            //{
            //    pnlAutoRenewalSettings.Visible = false;
            //}

        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);

        }
        #endregion
    }
}
