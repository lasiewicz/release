﻿using System;
using Matchnet.Web.Framework;

using Matchnet.Member.ServiceAdapters;
using Matchnet.Session.ValueObjects;

namespace Matchnet.Web.Applications.MemberServices
{
    public partial class Unsuspend20 : FrameworkControl
    {
        private void Page_Load(object sender, System.EventArgs e)
        {
            if (g.Member != null)
            {
                int suspended = g.Member.GetAttributeInt(g.Brand, "SelfSuspendedFlag");
                if (suspended == 1 && g.AppPage.ControlName != "Logoff")
                {
                    g.HeaderControl.ShowBlankHeader(Matchnet.Web.Framework.Ui.HeaderImageType.Default);
                    g.SetTopAuxMenuVisibility(false, true);
                    g.FooterControl20.HideFooterLinks();
                }
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnUnsuspend.Click += new System.EventHandler(this.btnUnsuspend_Click);
            this.Load += new System.EventHandler(this.Page_Load);

        }
        #endregion

        private void btnUnsuspend_Click(object sender, System.EventArgs e)
        {
            try
            {
                g.Member.SetAttributeInt(g.Brand, "SelfSuspendedReasonType", 0);
                g.Member.SetAttributeInt(g.Brand, "SelfSuspendedFlag", 0);
                MemberSA.Instance.SaveMember(g.Member);

                g.Session.Add("SelfSuspendedFlag", "false", SessionPropertyLifetime.Temporary);
                g.Transfer("/Applications/MemberServices/MemberServices.aspx?rc=YOUR_MEMBERSHIP_HAS_BEEN_UNSUSPENDED__519711");
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }
    }
}