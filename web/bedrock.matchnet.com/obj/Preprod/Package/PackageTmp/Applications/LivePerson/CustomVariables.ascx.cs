﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Ui.Advertising;
using Matchnet.Web.Framework.Managers;

namespace Matchnet.Web.Applications.LivePerson
{
    public partial class CustomVariables : FrameworkControl
    {
        private const int GLOBALSTATUSMASK_BAD_EMAIL_SUSPEND = 2;
        private const int GLOBALSTATUSMASK_EMAIL_VERIFIED = 4;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                var attributesDic = new Dictionary<string, string>();
                if (g.Member != null)
                {
                    if (g.Member.MemberID > 0)
                    {
                        attributesDic.Add("MemberID", g.Member.MemberID.ToString()); // Member ID
                        attributesDic.Add("LastMemberID", (g.Member.MemberID % 10).ToString());
                        attributesDic.Add("MemberUsername", g.Member.GetUserName(_g.Brand));
                        attributesDic.Add("Gender",
                            OmnitureHelper.OmnitureHelper.GetGender(_g.Member.GetAttributeInt(_g.Brand,
                                Matchnet.Web.Framework.WebConstants.ATTRIBUTE_NAME_GENDERMASK)));
                        string location = OmnitureHelper.OmnitureHelper.GetRegionString(_g.Member, _g.Brand,
                                g.Brand.Site.LanguageID, true, true, false);

                        attributesDic.Add("Location", location);
                        attributesDic.Add("Age",
                            OmnitureHelper.OmnitureHelper.GetAge(_g.Member.GetAttributeDate(_g.Brand,
                                Matchnet.Web.Framework.WebConstants.ATTRIBUTE_NAME_BIRTHDATE)).ToString());

                        var gam = new GAM();
                        string subStatus = Convert.ToString(FrameworkGlobals.GetMemberSubcriptionStatus(g.Member, g.Brand));
                        attributesDic.Add("Sub status", gam.GetAddAttribute("SubscriptionStatus", subStatus).Value.ToString());

                        // Days since registration
                        DateTime regDate = Convert.ToDateTime(g.Member.GetAttributeDate(g.Brand, "BrandInsertDate"));
                        int days = 0;
                        if (regDate != DateTime.MinValue)
                        {
                            days = DateTime.Now.Subtract(regDate).Days;
                        }
                        attributesDic.Add("SinceReg", days.ToString());

                        // 0-3, 4-7, 8-15, 0-15, 16-30, 0-30, 31-60, 61-90, 91-180, 180+
                        if (days >= 0 && days <= 3)
                            attributesDic.Add("SinceReg_Range", "0-3");
                        else if (days >= 4 && days <= 7)
                            attributesDic.Add("SinceReg_Range", "4-7");
                        else if (days >= 8 && days <= 15)
                            attributesDic.Add("SinceReg_Range", "8-15");
                        else if (days >= 16 && days <= 30)
                            attributesDic.Add("SinceReg_Range", "16-30");
                        else if (days >= 31 && days <= 60)
                            attributesDic.Add("SinceReg_Range", "31-60");
                        else if (days >= 61 && days <= 90)
                            attributesDic.Add("SinceReg_Range", "61-90");
                        else if (days >= 91 && days <= 180)
                            attributesDic.Add("SinceReg_Range", "91-180");
                        else if (days >= 181)
                            attributesDic.Add("SinceReg_Range", "180+");

                        // Days since sub end (ex-subscribers)
                        int daysSinceSubEnd = 0;
                        DateTime subscriptionExpirationDate = DateTime.MinValue;
                        Spark.Common.AccessService.AccessPrivilege basicSubscriptionPrivilege =
                            g.Member.GetUnifiedAccessPrivilege(
                                Spark.Common.AccessService.PrivilegeType.BasicSubscription, g.Brand.BrandID,
                                g.Brand.Site.SiteID, g.Brand.Site.Community.CommunityID);
                        if (basicSubscriptionPrivilege != null)
                        {
                            subscriptionExpirationDate = basicSubscriptionPrivilege.EndDatePST;
                            if (subscriptionExpirationDate != DateTime.MinValue &&
                                subscriptionExpirationDate < DateTime.Now)
                            {
                                daysSinceSubEnd = (DateTime.Now - subscriptionExpirationDate).Days;
                            }
                        }

                        attributesDic.Add("SinceSubX", daysSinceSubEnd.ToString());

                        if (daysSinceSubEnd >= 0 && daysSinceSubEnd <= 3)
                            attributesDic.Add("SinceSubX_Range", "0-3");
                        else if (daysSinceSubEnd >= 4 && daysSinceSubEnd <= 7)
                            attributesDic.Add("SinceSubX_Range", "4-7");
                        else if (daysSinceSubEnd >= 8 && daysSinceSubEnd <= 15)
                            attributesDic.Add("SinceSubX_Range", "8-15");
                        else if (daysSinceSubEnd >= 16 && daysSinceSubEnd <= 30)
                            attributesDic.Add("SinceSubX_Range", "16-30");
                        else if (daysSinceSubEnd >= 31 && daysSinceSubEnd <= 60)
                            attributesDic.Add("SinceSubX_Range", "31-60");
                        else if (daysSinceSubEnd >= 61 && daysSinceSubEnd <= 90)
                            attributesDic.Add("SinceSubX_Range", "61-90");
                        else if (daysSinceSubEnd >= 91 && daysSinceSubEnd <= 180)
                            attributesDic.Add("SinceSubX_Range", "91-180");
                        else if (daysSinceSubEnd >= 181)
                            attributesDic.Add("SinceSubX_Range", "180+");

                        //auto renewal info
                        Spark.Common.RenewalService.RenewalSubscription renewalSub = RenewalManager.Instance.GetRenewalSubscription(g.Member.MemberID, g.Brand);
                        if (renewalSub != null && renewalSub.PrimaryPackageID > 0)
                        {
                            attributesDic.Add("Auto_Renew", renewalSub.IsRenewalEnabled ? "On" : "Off");
                            if (renewalSub.RenewalDatePST > DateTime.Now)
                            {
                                attributesDic.Add("Til_Renew", (renewalSub.RenewalDatePST - DateTime.Now.Date).Days.ToString());
                            }
                            else
                            {
                                attributesDic.Add("Til_Renew", "");
                            }
                        }
                        else
                        {
                            attributesDic.Add("Auto_Renew", "Off");
                            attributesDic.Add("Til_Renew", "");
                        }

                        // Date of birth
                        DateTime birthDate = g.Member.GetAttributeDate(_g.Brand, "Birthdate");
                        if (birthDate != DateTime.MinValue)
                        {
                            attributesDic.Add("Birthday_Days", (birthDate.DayOfYear - DateTime.Now.DayOfYear).ToString());
                        }

                        // Bad email / email verified
                        int globalStatusMask = g.Member.GetAttributeInt(_g.Brand, "GlobalStatusMask");
                        attributesDic.Add("Bad_Email", ((globalStatusMask & GLOBALSTATUSMASK_BAD_EMAIL_SUSPEND) == GLOBALSTATUSMASK_BAD_EMAIL_SUSPEND).ToString());
                        attributesDic.Add("Email_Verified", ((globalStatusMask & GLOBALSTATUSMASK_EMAIL_VERIFIED) == GLOBALSTATUSMASK_EMAIL_VERIFIED).ToString());
                    }
                    else
                    {
                        attributesDic.Add("Sub status", "Visitor");
                    }
                }
                else
                {
                    attributesDic.Add("Sub status", "Visitor");
                }

                System.Text.StringBuilder addAttributesOutput = new System.Text.StringBuilder();

                foreach (var attribute in attributesDic)
                {

                    addAttributesOutput.AppendLine("{" +
                                                   string.Format(" scope:'session', name:'{0}',  value: '{1}'",
                                                       attribute.Key, attribute.Value.Replace("'", "\'")) + " },");
                }

                Literal literal = new Literal();
                literal.Text = addAttributesOutput.ToString();
                plcLPAttributes.Controls.Add(literal);
            }
            catch (Exception ex)
            {

                g.ProcessException(ex);
            }
        }

        protected string GetUserStatus()
        {
            string status = string.Empty;
            if (g.Member != null)
            {
                if (g.Member.MemberID > 0)
                {
                    status = OmnitureHelper.OmnitureHelper.GetSubscriberStatus(_g.Member, _g.Brand.Site.SiteID);
                }
                else
                {
                    status = "Visitor";
                }
            }
            else
            {
                status = "Visitor";
            }


            return status;
        }

        protected string GetV15()
        {
            return g.AnalyticsOmniture.Evar15;
        }

        protected string GetC17()
        {
            return g.AnalyticsOmniture.Prop17;
        }

        protected string GetC18()
        {
            return g.AnalyticsOmniture.Prop18;
        }

        protected string GetC19()
        {
            return g.AnalyticsOmniture.Prop19;
        }

        protected string GetV36()
        {
            return g.AnalyticsOmniture.Evar36;
        }
    }
}
