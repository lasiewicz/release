﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="ProfileSetting20.ascx.cs"
    Inherits="Matchnet.Web.Applications.Termination.ProfileSetting20" %>
<%@ Register TagPrefix="cc1" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<div id="page-container">
<style>
    
    h1
    {
        margin-bottom: 0;
    }
    .oneMoreStep 
    {
        display: block;
        margin-bottom: 20px;
    }
    .termination-reasons 
    {
        margin: 20px 0;
    }
</style>
<script type="text/javascript">
    $j(document).ready(function () {
        $j("input[type='radio']").each(function () {
            if ($j(this).val() === "1") {
                $j(this).attr("checked", true);
            }
        });
    });
</script>
    <asp:PlaceHolder runat="server" ID="plcDefault">
        <mn:Title runat="server" ID="ttlTerminateMembership" ResourceConstant="TXT_TERMINATE_MEMBERSHIP" />
        <strong class="oneMoreStep">
            <mn:Txt ID="txtOneMoreStep" runat="server" ResourceConstant="ONE_MORE_STEP" />
        </strong>
        <p>
            <strong>
                <mn:Txt ID="txtIWantTo" runat="server" ResourceConstant="I_WANT_TO" />
            </strong>
        </p>
        <div class="termination-reasons">
            <mn:RequiredFieldValidator ID="valProfileSetting" runat="server" ControlToValidate="radProfileSetting"
                ErrorResourceConstant="CHOICE_IS_REQUIRED" Display="Dynamic" />
            <asp:RadioButtonList ID="radProfileSetting" runat="server" EnableViewState="False"
                CellPadding="1" CellSpacing="0" RepeatLayout="Flow" />
        </div>
        <p class="text-center">
            <cc1:FrameworkButton ID="btnCancelNow" runat="server" ResourceConstant="CANCEL_NOW"
                CssClass="btn primary" />
                
                 <span style="padding-left: 20px;"><cc1:FrameworkButton ID="btnCancelOut" ResourceConstant="BTN_NO_I_WANT_TO_KEEP" runat="server" CssClass="btn primary"/></span>
                
                </p>
        <input type="hidden" id="TransactionReasonID" runat="server" name="TransactionReasonID" />
    </asp:PlaceHolder>
    <asp:PlaceHolder runat="server" ID="plcNewFlow" Visible="false">
        <mn:Title runat="server" ResourceConstant="TXT_HEADER" />
        <mn:Txt ID="Txt1" runat="server" ResourceConstant="TXT_CONTENT" />
        <p class="text-center">
        <mn2:FrameworkButton ID="btnCancelNowNew" runat="server" CssClass="btn primary termination-btns" ResourceConstant="BTN_CANCEL"
            Enabled="true" />
        <mn2:FrameworkButton ID="btnBackToSite" runat="server" CssClass="btn primary termination-btns"
            ResourceConstant="BTN_BACK" Enabled="true" />
        </p>
    </asp:PlaceHolder>
</div>
