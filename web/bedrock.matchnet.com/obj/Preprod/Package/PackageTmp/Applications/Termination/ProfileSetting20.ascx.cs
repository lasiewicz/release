﻿using System;
using System.Web.UI.WebControls;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Web.Framework;

#region Matchnet Service References
using Matchnet.Member.ServiceAdapters;
using Matchnet.Session.ValueObjects;
using Matchnet.Purchase.ValueObjects;
#endregion

namespace Matchnet.Web.Applications.Termination
{
    public partial class ProfileSetting20 : FrameworkControl
    {
        private const string KEEP_PROFILE = "1";
        private const string REMOVE_PROFILE = "0";

        private void Page_Init(object sender, System.EventArgs e)
        {
            radProfileSetting.Items.Add(new ListItem(g.GetResource("KEEP_MY_PROFILE", this), KEEP_PROFILE));
            radProfileSetting.Items.Add(new ListItem(g.GetResource("REMOVE_MY_PROFILE", this), REMOVE_PROFILE));
        }

        private void Page_Load(object sender, System.EventArgs e)
        {
            try
            {
                if (Request.QueryString["TransactionReasonID"] != null)
                {
                    TransactionReasonID.Value = Request.QueryString["TransactionReasonID"];
                }

                if (SettingsManager.GetSettingBool(SettingConstants.ENABLE_NEW_AUTO_RENEWAL_TERMINATION_FLOW, g.Brand))
                {
                    plcDefault.Visible = false;
                    plcNewFlow.Visible = true;
                }

                //TODO: just show for jdate for now ..circle back and place in setting 
                btnCancelOut.Visible = (g.Brand.Site.SiteID == 103);
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        private void btnCancelNow_Click(object sender, EventArgs e)
        {
            try
            {
                int confirmationID = Constants.NULL_INT;
                Spark.Common.RenewalService.RenewalResponse renewalResponse = Termination.Terminate(g.Brand.Site.SiteID, g.Session.GetInt(WebConstants.SESSION_PROPERTY_NAME_MEMBERID), Matchnet.Conversion.CInt(TransactionReasonID.Value), out confirmationID);

                if (renewalResponse.InternalResponse.responsecode == "0")
                {
                    if (radProfileSetting.SelectedValue == REMOVE_PROFILE)
                    {
                        //need to self suspend yo' self
                        Matchnet.Member.ServiceAdapters.Member member = MemberSA.Instance.GetMember(g.Session.GetInt(WebConstants.SESSION_PROPERTY_NAME_MEMBERID), MemberLoadFlags.None);
                        member.SetAttributeInt(g.Brand, "SelfSuspendedFlag", 1);
                        MemberSA.Instance.SaveMember(member);

                        g.Session.Add("SelfSuspendedFlag", "1", SessionPropertyLifetime.Temporary);
                    }

                    g.Transfer("/Applications/Termination/TerminateFinal.aspx?ConfirmationID=" + confirmationID.ToString());
                }
                else
                {
                    throw new Exception("Failed to update with Unified Renewal to disable auto-renewal for MemberID: " + g.Session.GetInt(WebConstants.SESSION_PROPERTY_NAME_MEMBERID).ToString() + ", please try again or contact Software Engineering.");
                }
            }
            catch (Exception ex)
            {
                _g.ProcessException(ex);
            }
        }

        private void btnCancelNowNew_Click(object sender, EventArgs e)
        {
            try
            {
                int confirmationID = Constants.NULL_INT;
                Spark.Common.RenewalService.RenewalResponse renewalResponse = Termination.Terminate(g.Brand.Site.SiteID, g.Session.GetInt(WebConstants.SESSION_PROPERTY_NAME_MEMBERID), Matchnet.Conversion.CInt(TransactionReasonID.Value), out confirmationID);

                if (renewalResponse.InternalResponse.responsecode == "0")
                {
                    g.Transfer("/Applications/Termination/TerminateFinal.aspx?ConfirmationID=" + confirmationID.ToString());
                }
                else
                {
                    throw new Exception("Failed to update with Unified Renewal to disable auto-renewal for MemberID: " + g.Session.GetInt(WebConstants.SESSION_PROPERTY_NAME_MEMBERID).ToString() + ", please try again or contact Software Engineering.");
                }
            }
            catch (Exception ex)
            {
                _g.ProcessException(ex);
            }
        }

        private void btnBackToSite_Click(object sender, EventArgs e)
        {
            try
            {
                g.Transfer("http://" + g.Brand.Site.Name);
            }
            catch (Exception ex)
            {
                _g.ProcessException(ex);
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCancelNow.Click += new System.EventHandler(this.btnCancelNow_Click);
            this.btnCancelNowNew.Click += new System.EventHandler(this.btnCancelNowNew_Click);
            this.btnBackToSite.Click += new System.EventHandler(this.btnBackToSite_Click);
            this.btnCancelOut.Click += new System.EventHandler(this.btnBackToSite_Click);
            this.Load += new System.EventHandler(this.Page_Load);
            this.Init += new System.EventHandler(this.Page_Init);
        }
        #endregion
    }
}