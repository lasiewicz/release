﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Web.Framework;

namespace Matchnet.Web.Applications.Termination
{
    public partial class ContactCustomerSupport : FrameworkControl
    {
        private enum TerminationReasonEnum
        {
            TooExpensive = 9,
            DifficultyUsingTheSite = 13,
            NobodyContactsMe = 6,
            FoundMySoulMateOnTheSite = 2
        }

        private int GetTransactionReasonID()
        {
            int reasonID;
            if (!int.TryParse(Request.QueryString["TransactionReasonID"], out reasonID))
            {
                reasonID = 13;
            }
            return reasonID;
        }

        private TerminationReasonEnum GetTerminationReason()
        {
            return (TerminationReasonEnum)GetTransactionReasonID();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            hftransactionReasonID.Value = GetTransactionReasonID().ToString();
            lnkSkip.NavigateUrl = "/Applications/Termination/ProfileSetting.aspx?TransactionReasonID=" + GetTransactionReasonID().ToString();

            TerminationReasonEnum reason = GetTerminationReason();
            txtHeader.ResourceConstant = "TXT_HEADER_" + reason.ToString();
            txtTitle.ResourceConstant = "TXT_TITLE_" + reason.ToString();
            if (reason == TerminationReasonEnum.TooExpensive)
            {
                if (string.IsNullOrEmpty(Request.QueryString["fromsub"]))
                {
                    ShowOffer();
                }
                else
                {
                    g.Transfer("/Applications/Termination/ProfileSetting.aspx?TransactionReasonID=" + GetTransactionReasonID().ToString());
                }

            }
        }

        protected void OnPreRender(object sender, EventArgs e)
        {
            try
            {
                if (SettingsManager.GetSettingBool(SettingConstants.ENABLE_NEW_AUTO_RENEWAL_TERMINATION_FLOW, g.Brand))
                {
                    string terminationReasonText = string.Empty;
                    switch (GetTerminationReason())
                    {
                        case TerminationReasonEnum.TooExpensive:
                            terminationReasonText = " Too expensive";
                            break;
                        case TerminationReasonEnum.DifficultyUsingTheSite:
                            terminationReasonText = " Too difficult to use";
                            break;
                        case TerminationReasonEnum.NobodyContactsMe:
                            terminationReasonText = " Not enough members replied to my emails";
                            break;
                        case TerminationReasonEnum.FoundMySoulMateOnTheSite:
                            terminationReasonText = " I met someone on JDate";
                            break;
                        default:
                            terminationReasonText = string.Empty;
                            break;
                    }

                    g.AnalyticsOmniture.PageName = g.AnalyticsOmniture.PageName + " " + terminationReasonText;
                }
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        private void ShowOffer()
        {
            plcContactCS.Visible = false;
            plcOffer.Visible = true;
        }

        private void btnToPromo_Click(object sender, EventArgs e)
        {
            try
            {
                g.Transfer(SettingsManager.GetSettingString(SettingConstants.NEW_AUTO_RENEWAL_TERMINATION_FLOW_PROMO_LINK, g.Brand));
            }
            catch (Exception ex)
            {
                _g.ProcessException(ex);
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnToPromo.Click += new System.EventHandler(this.btnToPromo_Click);
            this.Load += new System.EventHandler(this.Page_Load);
            //this.Init += new System.EventHandler(this.Page_Init);
            this.PreRender += new System.EventHandler(this.OnPreRender);
        }
        #endregion
    }
}