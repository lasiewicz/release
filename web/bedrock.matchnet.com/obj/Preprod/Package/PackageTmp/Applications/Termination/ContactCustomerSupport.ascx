﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ContactCustomerSupport.ascx.cs"
    Inherits="Matchnet.Web.Applications.Termination.ContactCustomerSupport" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<script type="text/javascript" language="javascript">
    function IsNumeric(input) {
        var ValidChars = "0123456789-";
        var is_number = true;
        var Char;
        for (i = 0; i < input.length && is_number == true; i++) {
            Char = input.charAt(i);
            if (ValidChars.indexOf(Char) == -1) {
                is_number = false;
            }
        }
        return is_number;
    }
    jQuery(document).ready(function () {
        //btn_submit click event 
        jQuery("#btn_submit").click(function () {
            //vars
            var name = jQuery("#mem_name").val();
            var phone = jQuery("#mem_phone").val();
            var member_id = "<%=g.Member.MemberID %>";
            var calling_hour = jQuery("#mem_calling_time").val();
            var empty_phone_txt = '<%=g.GetResource("TXT_ERROR", this) %>';
            var numbers_only_txt = '<%=g.GetResource("TXT_NUMBER_ONLY_ERROR", this) %>';
            var check = true;
            //validate fields
            if (name == "") {
                jQuery("#name_error").css("display", "block");
                check = false;
            }
            if (phone == "") {
                jQuery("#phone_error").html(empty_phone_txt);
                jQuery("#phone_error").css("display", "block");
                check = false;
            }
            if (IsNumeric(phone) == false) {
                jQuery("#phone_error").html(numbers_only_txt);
                jQuery("#phone_error").css("display", "block");
                check = false;
            }
            //if validated
            if (check == true) {
                var reason_num = jQuery("#_ctl0__ctl5_hftransactionReasonID").val();
                //do ajax request
                jQuery.ajax({
                    type: "GET",
                    url: "http://memberslist.spark-networks.com/addmemberJSONP.ashx",
                    contentType: "application/x-www-form-urlencoded;charset=utf-8",
                    data: "name=" + encodeURIComponent(name) + "&phone=" + phone + "&email=" + member_id + "&address=" + encodeURIComponent(calling_hour) + "&category=14&site=4",
                    dataType: "jsonp",
                    success: function (ret_res) {
                        if (parseInt(ret_res.response) == 1) {
                            window.location = "ProfileSetting.aspx?rc=DATA_SENT_TO_CS&TransactionReasonID=" + reason_num; ;
                        } else {
                            window.location = "ContactCustomerSupport.aspx?TransactionReasonID=" + reason_num + "&rc=TXT_SAVING_ERROR";
                        }
                    }
                });
            }
        });
        jQuery("#mem_name").keypress(function (event) {
            if (jQuery("#mem_name").val() != '') {
                jQuery("#name_error").css("display", "none");
            }
        });
        jQuery("#mem_phone").keypress(function (event) {
            if (jQuery("#mem_phone").val() != '') {
                jQuery("#phone_error").css("display", "none");
            }
        });
    });
</script>
<div id="page-container">
    <mn:Title runat="server" ID="txtHeader" CssClass="termination-header" />
    <h3 class="termination-content">
        <mn:Txt runat="server" ID="txtTitle" />
    </h3>
    <asp:PlaceHolder runat="server" ID="plcContactCS">
        <ul id="termination-fieldset">
            <li>
                <label>
                    <mn:Txt runat="server" ResourceConstant="TXT_MEMBER_NUMBER" />
                </label>
                <input type="text" id="mem_number" name="mem_number" value="<%=g.Member.MemberID %>"
                    readonly="readonly" />
            </li>
            <li>
                <label>
                    <mn:Txt ID="Txt1" runat="server" ResourceConstant="TXT_MEMBER_NAME" />
                </label>
                <input type="text" id="mem_name" name="mem_name" />
                <div id="name_error" class="error" style="display:none;margin-right:93px;">
                    <mn:Txt ID="Txt7" runat="server" ResourceConstant="TXT_ERROR" />
                </div>
            </li>
            <li>
                <label>
                    <mn:Txt ID="Txt2" runat="server" ResourceConstant="TXT_MEMBER_PHONE" />
                </label>
                <input type="text" id="mem_phone" name="mem_phone" />
                <div id="phone_error" class="error" style="display:none;margin-right:93px;">
                </div>
            </li>
            <li>
                <label>
                    <mn:Txt ID="Txt3" runat="server" ResourceConstant="TXT_MEMBER_CALLING_TIME" />
                </label>
                <input type="text" id="mem_calling_time" name="mem_calling_time" />
            </li>
        </ul>
        <p>
            <mn:Txt ID="Txt4" runat="server" ResourceConstant="TXT_MANDATORY_FIELDS" />
        </p>
        <p class="text-center">
            <input type="button" id="btn_submit" class="btn primary" value="<%=g.GetResource("BTN_CONTINUE", this) %>" />
        </p>
        <p>
            <asp:HyperLink runat="server" ID="lnkSkip">
                <mn:Txt ID="Txt5" runat="server" ResourceConstant="TXT_SKIP_LINK" />
            </asp:HyperLink>
        </p>
        <asp:HiddenField ID="hftransactionReasonID" runat="server" />
    </asp:PlaceHolder>
    <asp:PlaceHolder runat="server" ID="plcOffer" Visible="false">
        <p class="text-center">
            <mn2:FrameworkButton ID="btnToPromo" runat="server" CssClass="btn primary" ResourceConstant="BTN_CONTINUE"
                Enabled="true" />
        </p>
    </asp:PlaceHolder>
</div>
