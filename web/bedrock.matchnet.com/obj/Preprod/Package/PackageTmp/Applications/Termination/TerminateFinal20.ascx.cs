﻿using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using Matchnet.Content;
using Matchnet.Web.Framework;


namespace Matchnet.Web.Applications.Termination
{
    public partial class TerminateFinal20 : FrameworkControl
    {
        private void Page_Load(object sender, System.EventArgs e)
        {
            try
            {
                int confirmationID = (Request["ConfirmationID"] != null ? Conversion.CInt(Request["ConfirmationID"]) : Constants.NULL_INT);

                g.ExpansionTokens.Add("CONFIRMATIONID", confirmationID.ToString());
                if (confirmationID != Constants.NULL_INT)
                {
                    g.Notification.AddMessage("YOUR_REQUEST", new string[1] { confirmationID.ToString() });
                }
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);

        }
        #endregion
    }
}