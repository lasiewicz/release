using System.Web;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Web.Framework;
using Matchnet.Purchase.ServiceAdapters;
using Matchnet.Purchase.ValueObjects;
using Spark.Common.Adapter;
using System;

namespace Matchnet.Web.Applications.Termination
{

	public class Termination
	{
		private int saveOfferID;
		private int saveOfferDiscountID;
		private int saveOfferPlanID;
		private string saveOfferResourceConstant;
		private int saveOfferTier;

		public Termination()
		{
			InitializeSaveOffer();
		}

		#region public properties

		public int SaveOfferID
		{
			get
			{
				return saveOfferID;
			}
			set
			{
				saveOfferID = value;
			}
		}

		public int SaveOfferDiscountID
		{
			get
			{
				return saveOfferDiscountID;
			}
			set
			{
				saveOfferDiscountID = value;
			}
		}

		public int SaveOfferPlanID
		{
			get
			{
				return saveOfferPlanID;
			}
			set
			{
				saveOfferPlanID = value;
			}
		}

		public string SaveOfferResourceConstant
		{
			get
			{
				return saveOfferResourceConstant;
			}
			set
			{
				saveOfferResourceConstant = value;
			}
		}

		public int SaveOfferTier		
		{
			get
			{
				return saveOfferTier;
			}
			set
			{
				saveOfferTier = value;
			}
		}

		#endregion

		public bool PopulateSaveOffer(int siteID, int memberID, int transactionReasonID)
		{
			return PopulateSaveOffer(siteID, memberID, transactionReasonID, 1);
		}
			
		public bool PopulateSaveOffer(int siteID, int memberID, int transactionReasonID, int tier)
		{
			InitializeSaveOffer();

            //These save offers are no longer utilized until we revisit discounts in UPS
            bool isEligibleForSaveOffer = false; //PurchaseSA.Instance.IsEligibleForSaveOffer(memberID, siteID)
			if (isEligibleForSaveOffer)
			{
                SaveOfferCollection soc = null;// PurchaseSA.Instance.GetSaveOffers(siteID);
				TransactionReasonSiteCollection trpc = PurchaseSA.Instance.GetTransactionReasonsSite(siteID);

				if ( soc != null )
				{
					foreach ( SaveOffer so in soc )
					{
						TransactionReason tr = trpc.FindByID( transactionReasonID );

						if ( tr != null )
						{
							if ( so.TransactionReasonType  == tr.TransactionReasonType && so.Tier == tier )
							{
								saveOfferID = so.SaveOfferID;
								saveOfferDiscountID = so.DiscountID;
								saveOfferPlanID = so.PlanID;
								saveOfferResourceConstant = so.ResourceConstant;
								saveOfferTier = so.Tier;
								return true;
							}
						
						}

					}
				}
			}

			return false;
		}

		private void InitializeSaveOffer()
		{
			saveOfferID = Constants.NULL_INT;
			saveOfferDiscountID = Constants.NULL_INT;
			saveOfferPlanID = Constants.NULL_INT;
			saveOfferResourceConstant = "RESOURCE_NOT_FOUND";
			saveOfferTier = Constants.NULL_INT;
		}


		#region static methods

		public static TransactionReasonSiteCollection GetTransactionReasons(int languageID, int privateLabelID)
		{
			ContextGlobal g = null;
			TransactionReasonSiteCollection trsc = null;
			HttpContext Context = HttpContext.Current;

			if ( Context != null )
			{
				if ( Context.Items["g"] != null )
				{
					g = (ContextGlobal) Context.Items["g"];
				}
			}
		
            
			
			if ( g != null )
			{
				trsc = PurchaseSA.Instance.GetTransactionReasonsSite(privateLabelID);

				if ( trsc != null )
				{
					foreach ( TransactionReason tr in trsc )
					{
						tr.Description = g.GetResource(tr.ResourceConstant, null);
                    }


                    if (g.Brand.BrandID == 1003)  //moving other to lower list for jdate
					{
                        TransactionReason transactionReasonOther = trsc[0];
                        
				        if (transactionReasonOther.Description == "Other")
				        {
                            trsc.RemoveAt(0);  // remove other from the first position
                            trsc.Add(transactionReasonOther); // and add it to the bottom of the list. 
				        }
                    }
				        
				}
            }

			return trsc;
		}

        public static Spark.Common.RenewalService.RenewalResponse Terminate(int privateLabelID, int memberID, int transactionReasonID, out int confirmationID)
		{
			return Terminate(privateLabelID, memberID, Constants.NULL_INT, transactionReasonID, out confirmationID);
		}


        public static Spark.Common.RenewalService.RenewalResponse Terminate(int privateLabelID, int memberID, int adminMemberID, int transactionReasonID, out int confirmationID)
		{
            //Update UPS Renewal
            Spark.Common.RenewalService.RenewalResponse renewalResponse = RenewalServiceWebAdapter.GetProxyInstanceForBedrock().DisableAutoRenewal(memberID,
                privateLabelID, 1, adminMemberID, "", transactionReasonID);

            confirmationID = renewalResponse.TransactionID;
            if (renewalResponse.InternalResponse.responsecode != "0")
                throw new Exception("Failed to update with Unified Renewal to disable auto-renewal for MemberID: " + memberID.ToString() + ", please try again or contact Software Engineering.");

            // this is used to display the autorenewal reactivate link in the top aux menu
            var member = MemberSA.Instance.GetMember(memberID, MemberLoadFlags.None);
            var brand = FrameworkGlobals.GetBrandForSite(privateLabelID);
            member.SetAttributeInt(brand, "AutoRenewalStatus", 0);
            MemberSA.Instance.SaveMember(member);

            return renewalResponse;

		}

		#endregion
	}
}
