﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Spark.SAL;
#region Matchnet Web App References
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Util;
using Matchnet.Lib;
using Matchnet.Member.ServiceAdapters;
#endregion

using Matchnet.PremiumServices.ValueObjects;
namespace Matchnet.Web.Applications.PremiumServices
{
    public class PremiumServiceSettingBase:FrameworkControl
    {

        private InstanceMemberCollection premiumServices = null;
        private Member.ServiceAdapters.Member member;
        private Content.ValueObjects.BrandConfig.Brand brand;

        public Member.ServiceAdapters.Member Member
        {
            get { return member; }
            set { member = value; }
        }

        public Content.ValueObjects.BrandConfig.Brand Brand
        {
            get { return brand; }
            set { brand = value; }
        }

        public InstanceMemberCollection MemberPremiumServices
        {
            get
            {
                if (premiumServices == null)
                {
                    if (brand != null && member != null)
                        premiumServices = Matchnet.PremiumServices.ServiceAdapter.ServiceMemberSA.Instance.GetMemberServices(brand.BrandID, member.MemberID);

                }
                return premiumServices;

            }


            set
            {
                if (value != null)
                    premiumServices = value;

            }
        }

    }
}
