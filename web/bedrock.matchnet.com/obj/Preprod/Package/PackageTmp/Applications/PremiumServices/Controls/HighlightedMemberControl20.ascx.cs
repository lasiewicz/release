﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

#region Matchnet Web App References
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Util;
using Matchnet.Lib;
#endregion

using Matchnet.PremiumServices.ValueObjects;

namespace Matchnet.Web.Applications.PremiumServices.Controls
{
    public partial class HighlightedMemberControl20 :  PremiumServiceSettingBase, IPremiumServiceSettings
    {
        HighlightedMember servicemember = null;
        Member.ServiceAdapters.Member siteMember = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                bindEnableDisableControl(rbgHighlightProfile);
                if (servicemember.EnableFlag)
                    rbgHighlightProfile.SelectedIndex = 0;
                else
                    rbgHighlightProfile.SelectedIndex = 1;
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }


        public void Save()
        {
            servicemember.EnableFlag = rbgHighlightProfile.Items[0].Selected;
            Matchnet.PremiumServices.ServiceAdapter.ServiceMemberSA.Instance.SaveMemberService(g.Brand, Member.MemberID, servicemember.ServiceMember);


        }


        public void SetMember(Content.ValueObjects.BrandConfig.Brand brand, Member.ServiceAdapters.Member member, InstanceMember premiummember)
        {
            servicemember = new HighlightedMember(premiummember);
            base.Member = member;
            base.Brand = brand;

        }



        protected void bindEnableDisableControl(RadioButtonList pButtonList)
        {
            if (pButtonList != null)
            {
                ListItem liEnable = new ListItem();
                liEnable.Value = "1";
                liEnable.Text = g.GetResource("TXT_ENABLE", this);
                pButtonList.Items.Add(liEnable);

                ListItem liDisable = new ListItem();
                liDisable.Value = "0";
                liDisable.Text = g.GetResource("TXT_DISABLE", this);
                pButtonList.Items.Add(liDisable);
            }
        }
    }
}