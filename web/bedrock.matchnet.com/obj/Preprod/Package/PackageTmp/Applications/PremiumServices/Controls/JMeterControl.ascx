﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="JMeterControl.ascx.cs"
    Inherits="Matchnet.Web.Applications.PremiumServices.Controls.JMeterControl" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<div id="highlightedMemberControl" class="roundedCorners_gradient clearFloats">
    <%--<mn:Image FileName="sub_profileHighlight_largeThumb.gif" Style="margin: 12px 12px 4px 4px;"
        CssClass="floatOutside" ID="image2" runat="server" TitleResourceConstant="TITLE_HIGHLIGHTED_PROFILE"
        ResourceConstant="ALT_HIGHLIGHTED_PROFILE" />--%>
    <h2>
        <mn:Txt runat="server" ID="Txt2" ExpandImageTokens="True" ResourceConstant="TXT_JMETER" />
        &nbsp;<a href="#" onmouseover="TogglePopupDiv('JMeter_help');return false;" onmouseout="TogglePopupDiv('JMeter_help');return false;"><mn:Image
            FileName="icon_help.gif" ID="Image1" runat="server" ResourceConstant="TITLE_ICON_HELP"
            align="top" /></a></h2>
    <div id="JMeter_help" class="helpLayerContainer">
        <div class="helpLayerMiddle">
            <div class="helpLayerInner lightest borderDark">
                <mn:Image FileName="sub_profileHighlight_largeThumb.gif" CssClass="floatOutside"
                    ID="Image6" runat="server" TitleResourceConstant="TITLE_HIGHLIGHTED_PROFILE"
                    ResourceConstant="ALT_HIGHLIGHTED_PROFILE" />
                <h5>
                    <mn:Txt runat="server" ID="txtRememberMeHelpLayerHeadline" ResourceConstant="TXT_HIGHLIGHTED_PROFILE_HELP_LAYER_HEADLINE" />
                </h5>
                <mn:Txt runat="server" ID="txtRememberMeHelpLayerBody" ResourceConstant="TXT_HIGHLIGHTED_PROFILE_HELP_LAYER_BODY" />
            </div>
        </div>
    </div>
    <!--end helpLayer -->
    <asp:RadioButtonList ID="rbgJMeter" runat="server">
    </asp:RadioButtonList>
    <mn:Txt runat="server" ID="txtInfo" ResourceConstant="TXT_INFO"></mn:Txt>
</div>
