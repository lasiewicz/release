﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SpotlightMemberControl20.ascx.cs" Inherits="Matchnet.Web.Applications.PremiumServices.Controls.SpotlightMemberControl20" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="anthem" Namespace="Anthem" Assembly="Anthem" %>
<%@ Register TagPrefix="mnfe" TagName="PickRegionNew" Src="/Framework/Ui/FormElements/PickRegionNew.ascx" %>
<%@ Register TagPrefix="mn" TagName="miniProfile" Src="/Framework/Ui/BasicElements/MiniProfile20.ascx" %>

<h2 id="member-spotlight-header"><mn:Txt runat="server" id="Txt3" ExpandImageTokens="True" ResourceConstant="TXT_MEMBER_SPOTLIGHT" /> <a id="member-spotlight-mini-profile-link" href="#member-spotlight-mini-profile"><mn:txt runat="server" id="Txt7" ResourceConstant="TXT_VIEW_MY_SPOTLIGHT_PROFILE" /></a></h2>
<a name="member-spotlight-mini-profile"></a>
    <div id="premium-settings-my-spotlight">
       <asp:PlaceHolder ID="phSpotlightProfileOld" runat="server" Visible="false">
        <mn:miniProfile runat="server" id="MemberMiniProfile"  runat="server" IsSpotlight="true" IsHighlighted="false" visible="true" OverrideSelfSpotlight="true" OnlineImageName="icon-status-online.gif" OfflineImageName="icon-status-offline.gif" />
       </asp:PlaceHolder>
       <asp:PlaceHolder ID="phSpotlightProfileNew" runat="server" Visible="false"></asp:PlaceHolder>
    </div>
<p class="editorial" id="txt-explain-spotlight">
    <mn:Image FileName="ui-settings-spotlight.png" CssClass="float-outside margin-light" id="Image6" runat="server" titleResourceConstant="TITLE_SPOTLIGHTED_PROFILE" ResourceConstant="ALT_SPOTLIGHTED_PROFILE" />
    <mn:txt runat="server" id="Txt6" ResourceConstant="TXT_MEMBER_SPOTLIGHT_HELP_LAYER_BODY" />
</p>

<div id="spotlight-form-container">
    <div class="enable">
        <div class="item"><asp:RadioButton id="rdbEnable" runat="server" GroupName="EnableGroup" CssClass="radio-button-shifter"></asp:RadioButton><mn:Txt runat="server" id="Txt1" ExpandImageTokens="True" ResourceConstant="TXT_ENABLE" /></div>
        <h3><mn:txt runat="server" id="Txt5" ResourceConstant="TXT_SPOTLIGHT_ENABLE_HEADING" /></h3>
        <div class="item">
            <strong><asp:DropDownList ID="ddlGender" Runat="server" /> <mn:txt id="txtSeekingGender" runat="server" ResourceConstant="TXT_SEEKING_A" /> <asp:DropDownList ID="ddlSeekingGender" Runat="server" /></strong>
            <strong><mn:txt runat="server" id="txtAges" resourceconstant="TXT_AGE" /></strong> <input id="AgeMin" type="text" style="WIDTH:20px" size="2" maxlength="2" runat="server" /> <strong><mn:txt runat="server" id="txtTo" resourceconstant="TO_" /></strong> <input id="AgeMax" type="text" style="WIDTH:20px" size="2" maxlength="2" runat="server" />
            <asp:rangevalidator runat="server" id="AgeMinRange" controltovalidate="AgeMin" display="None"></asp:rangevalidator>
            <asp:rangevalidator runat="server" id="AgeMaxRange" controltovalidate="AgeMax" display="None"></asp:rangevalidator>
            <asp:requiredfieldvalidator runat="server" id="AgeMinReq" controltovalidate="AgeMin" display="None"></asp:requiredfieldvalidator>
            <asp:requiredfieldvalidator runat="server" id="AgeMaxReq" controltovalidate="AgeMax" display="None"></asp:requiredfieldvalidator>
            <asp:validationsummary runat="server" id="valSummary" displaymode="BulletList"></asp:validationsummary>
        </div>
        <div class="item">
            <anthem:PlaceHolder ID="plcDistance" Runat="server">
            <strong><mn:txt id="txtWithin" ResourceConstant="TXT_LOCATED_WITHIN" runat="server" />&nbsp;
            <asp:DropDownList id="ddlDistance" Runat="server"></asp:DropDownList>&nbsp;<mn:txt id="txtOf" ResourceConstant="TXT_OF" runat="server"></mn:txt></strong>
            </anthem:PlaceHolder><anthem:HyperLink ID="lnkLocation" Runat="server" CssClass="regionPickerLink" />
            <mnfe:PickRegionNew id="prSearchRegion" Runat="server" />
        </div>
    </div>

    <div class="disable">
        <div class="item">
            <asp:RadioButton id="rdbDisable" runat="server" CssClass="radio-button-shifter" GroupName="EnableGroup"></asp:RadioButton><mn:Txt runat="server" id="Txt2" ExpandImageTokens="True" ResourceConstant="TXT_DISABLE" />
        </div>
    </div>

</div>
<hr class="hr-thin" />