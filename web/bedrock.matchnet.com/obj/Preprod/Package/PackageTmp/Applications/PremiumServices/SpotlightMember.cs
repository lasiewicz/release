﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Matchnet.PremiumServices.ValueObjects;


namespace Matchnet.Web.Applications.PremiumServices
{
    public class SpotlightMember
    {
        private InstanceMember member;

        public SpotlightMember(InstanceMember m)
        {
            if (m != null)
                member = m;
            else
                member = new Matchnet.PremiumServices.ValueObjects.InstanceMember();
        }

        public InstanceMember ServiceMember
        {
            get { return member; }
            set { member = value; }
        }
        public bool EnableFlag
        {
            get { return member.Attributes.Get<bool>((int)Matchnet.PremiumServices.ValueObjects.ServiceConstants.Attributes.EnableFlag);}
            set { member.Attributes.Add<bool>((int)Matchnet.PremiumServices.ValueObjects.ServiceConstants.Attributes.EnableFlag, value); }
        }


        public DateTime ExpirationDate
        {
            get { return member.Attributes.Get<DateTime>((int)Matchnet.PremiumServices.ValueObjects.ServiceConstants.Attributes.ExpirationDate); }
            set { member.Attributes.Add<DateTime>((int)Matchnet.PremiumServices.ValueObjects.ServiceConstants.Attributes.ExpirationDate, value); }
        }

        public int AgeMin
        {
            get { return member.Attributes.Get<int>((int)Matchnet.PremiumServices.ValueObjects.ServiceConstants.Attributes.AgeRange, true); }
            set { member.Attributes.Add<int>((int)Matchnet.PremiumServices.ValueObjects.ServiceConstants.Attributes.AgeRange, value, default(int)); }
        }

        public int AgeMax
        {
            get { return member.Attributes.Get<int>((int)Matchnet.PremiumServices.ValueObjects.ServiceConstants.Attributes.AgeRange, false); }
            set { member.Attributes.Add<int>((int)Matchnet.PremiumServices.ValueObjects.ServiceConstants.Attributes.AgeRange, default(int), value); }
        }


        public int Distance
        {
            get { return member.Attributes.Get<int>((int)Matchnet.PremiumServices.ValueObjects.ServiceConstants.Attributes.Distance); }
            set { member.Attributes.Add<int>((int)Matchnet.PremiumServices.ValueObjects.ServiceConstants.Attributes.Distance, value); }
        }

        public int GenderMask
        {
            get { return member.Attributes.Get<int>((int)Matchnet.PremiumServices.ValueObjects.ServiceConstants.Attributes.GenderMask); }
            set { member.Attributes.Add<int>((int)Matchnet.PremiumServices.ValueObjects.ServiceConstants.Attributes.GenderMask, value); }
        }


        public int RegionID
        {
            get { return member.Attributes.Get<int>((int)Matchnet.PremiumServices.ValueObjects.ServiceConstants.Attributes.RegionID); }
            set { member.Attributes.Add<int>((int)Matchnet.PremiumServices.ValueObjects.ServiceConstants.Attributes.RegionID, value); }
        }

        public int Depth1RegionID
        {
            get { return member.Attributes.Get<int>((int)Matchnet.PremiumServices.ValueObjects.ServiceConstants.Attributes.Depth1RegionID); }
            set { member.Attributes.Add<int>((int)Matchnet.PremiumServices.ValueObjects.ServiceConstants.Attributes.Depth1RegionID, value); }
        }

        public int Depth2RegionID
        {
            get { return member.Attributes.Get<int>((int)Matchnet.PremiumServices.ValueObjects.ServiceConstants.Attributes.Depth2RegionID); }
            set { member.Attributes.Add<int>((int)Matchnet.PremiumServices.ValueObjects.ServiceConstants.Attributes.Depth2RegionID, value); }
        }

        public int Depth3RegionID
        {
            get { return member.Attributes.Get<int>((int)Matchnet.PremiumServices.ValueObjects.ServiceConstants.Attributes.Depth3RegionID); }
            set { member.Attributes.Add<int>((int)Matchnet.PremiumServices.ValueObjects.ServiceConstants.Attributes.Depth3RegionID, value); }
        }
    }
}
