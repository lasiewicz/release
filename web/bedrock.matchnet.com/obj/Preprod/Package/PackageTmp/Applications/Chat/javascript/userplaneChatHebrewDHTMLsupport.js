﻿
/********************************************************************************
Name: userplaneIMHebrewDHTMLsupport.js
Description: This javascript file contains the code necessary to communicate
with the Hebrew Userplane flash Instant Messenger version
in order to create two DHTML "overlays" on top of flash
so that we can support Hebrew on Windows 98! 
(Otherwise, apparently, we'd use Userplanes Flash IM alone without DHTML overlay, according to history conveyed by Yovav)
File Replaces former flashdiv.js file. (notes by Adam 8/23/05)
*********************************************************************************/

/*	Name: aKeyPressd
Description: Wires the Enter key to be equivalent of Send button in Flash movie.
Fired onKeyUp event in the HTML INPUT box that overlays the Flash
movie. Flash function TCallLabel is called to tell flash to call the getText command.*/
function aKeyPressd() {
    if (window.event.keyCode == 13) {
        flashDiv.TCallLabel("_root", "EnterPressed")
    }
}

function getCHObject() {
    if (document.all) {
        return document.all["ch"];
    }
    else if (document.layers) {
        return document.ch;
    }
    else if (document.getElementById) {
        return document.getElementById("ch");
    }

    return null;
}

var IsHebrewOnWin32IE = true; //if this file is included, then this is true. Used to check for focus in common userplane.js calls. bug 15801
var InternetExplorer = navigator.appName.indexOf("Microsoft") != -1;
/*	Name: ic_DoFSCommand
Description: Handle all the the FSCommand messages in a Flash movie */
function ch_DoFSCommand(command, args) {
    alert('hey');
    //var icObj = InternetExplorer ? ic : document.ic;
    var icObj = getCHObject();
    if (icObj == null) { return; }

    flashDiv = icObj;
    window.onresize = resizeFunction
    switch (command) {
        case "createLayer":
            LayerArgs = args
            createLayer()
            break;
        case "updateLoc":
            UpdateArgs = args
            updateLoc()
            break;
        case "setText":
            TextArgs = args
            setText()
            break;
        case "setVal":
            TextArgs = args
            setVal()
            break;
        case "addText":
            TextArgs = args
            addText()
            break;
        case "getText":
            divName = args
            getText()
            break;
        case "removeDiv":
            divName = args
            removeDiv()
            break;
        /*
        Don't know if these are in use??
        case "print":
        openIMPrintWindow( args );	
        break;
        case "testFS":
        var icObject = document.getElementById("ic")
        //alert( icObject );
        icObject.setVariable( "fsCommandIsAvailable", "true" );
        break;
        */ 
    }

}

// Hook for Internet Explorer 
/*
duplicated VBScript code... see upInstantCommunicator.ascx
if (navigator.appName && navigator.appName.indexOf("Microsoft") != -1 && 
navigator.userAgent.indexOf("Windows") != -1 && navigator.userAgent.indexOf("Windows 3.1") == -1) {
document.write('<' + 'SCRIPT LANGUAGE=VBScript\> \n');
document.write('on error resume next \n');
document.write('Sub ic_FSCommand(ByVal command, ByVal args)\n');
document.write('  call ic_DoFSCommand(command, args)\n');
document.write('end sub\n');
document.write('<'+'/SCRIPT\> \n');
}
*/

/*	Name: createLayer()
Description: This function is called from within the Flash movie. It is currently called twice: 
once for the chattxt layer and once for the messagetxt layer. Flash passes in the 
LayerArgs argument that consists of an object definition containing size and position
properties for the layer overlays. The arg passed in looks like this:
chattxt:
{x:2,y:120.35,height:161.6,width:353,padding:0,myType:'TextField',myName:'chattxt',myTarget:'/chattxt',myFont:'Arial',myAlign:'',dir:'RTL',mySize:'12px',scrollFlag:true,myColor:'#000000',myText:''}
messagetxt:
{x:13.4,y:292,height:18.7,width:333,padding:0,myType:'InputText',myName:'messagetxt',myTarget:'/messagetxt',myFont:'Arial',myAlign:'',dir:'RTL',mySize:'12px',scrollFlag:false,myColor:'#000000',myText:'<input type=text name=messagetxtInputText dir=rtl value=\"\" style=width:100%;height:100% onKeyUp=aKeyPressd()>'} */
function createLayer() {
    // create a div Property refernce object
    eval("divProp=" + LayerArgs)
    // check that this layer does not exits
    createFlag = true
    for (n in dynlayer.childNodes) {
        if (divProp.myName == dynlayer.childNodes[n].id) {
            createFlag = false
        }
    }
    // create the new layer

    if (createFlag) {

        flashWidth = flashDiv.getVariable("stage.width")
        flashHeight = flashDiv.getVariable("stage.height")
        xref = document.body.clientWidth / 100
        yref = document.body.clientHeight / 100
        dynlayer.innerHTML = dynlayer.innerHTML + '<Div id="' + divProp.myName + '" style="border-style:solid;padding:5; border-width:0; position: absolute; z-index: 1;"></div>'
        eval(divProp.myName).myX = divProp.x
        eval(divProp.myName).myY = divProp.y
        setStyle()
        TextArgs = LayerArgs
        setText()
        //not sure of the purpose of this call to flash?
        flashDiv.TCallLabel(divProp.myTarget, "callback")
        //set focus on message sending area so that when IM opens you can just start typing
        if (divProp.myName == "messagetxt") {
            document.all["messagetxtInputText"].focus();
        }
    } else {
        setStyle()
        TextArgs = LayerArgs
        setText()
        flashDiv.TCallLabel(divProp.myTarget, "callback")
    }

}

/*	Name: updateLoc 
Description: This fires when show video button is clicked. This is called some
40 or so times during the "tweening" of the transition 
and thereby creates a smooth effect when opening the A/V (audio/video) pane */
function updateLoc() {
    eval("divProp=" + UpdateArgs);
    eval(divProp.myName).style.left = divProp.x + "px";
    //50 is the height of the image header.
    eval(divProp.myName).style.top = (divProp.y + 50) + "px";
    eval(divProp.myName).style.width = divProp.width + "px";
    eval(divProp.myName).style.height = divProp.height + "px";
    eval(divProp.myName).scrollTop = eval(divProp.myName).scrollHeight;
}

function updatefontSizes() {
    //flashWidth=flashDiv.TgetProperty("_root",8)
    //for (n=0;n<dynlayer.childNodes.length;n++){
    //originalSize=flashDiv.getVariable(dynlayer.childNodes[n].target + ".mySize")
    //dynlayer.childNodes[n].style.fontSize=(originalSize*document.body.clientWidth/flashWidth)
    //}
}

//for unknown reasons, replaces quotes with &quot;
function takeQouteOut(theString) {
    rExp = /"/gi;
    mystring = new String(theString);
    replaceString = new String("&quot;");
    results = mystring.replace(rExp, replaceString);
    return results;
}

//for unknown reasons, replaces single quotes with char(180) which seems to be a right curly apostrophe
function takeSingleQouteOut(theString) {
    rExp = /'/gi;
    replaceString = new String(String.fromCharCode(180));
    mytextstart = theString.indexOf("myText:") + 8;
    mytextend = theString.indexOf("'}", mytextstart) - 1;
    mystring = new String(theString);
    mystring = theString.substring(mytextstart, mytextend)
    mytemptext = mystring.replace(rExp, replaceString);
    results = theString.substring(0, mytextstart) + mytemptext + results.substring(mytextend, results.length);
    return results;
}

//TODO: returns the wrong type of quote! Doesn't seem to have noticeable effects but HTML is non-standard
function returnQuote(theString) {
    mystring = new String(theString)
    replaceString = new String('"')
    rExp = /&quot;/gi;
    results = mystring.replace(rExp, replaceString)
    rExp = /`/gi;
    replaceString = new String("'");
    results = results.replace(rExp, replaceString);
    return results
}


function setText() {
    eval("textProp=" + takeQouteOut(TextArgs))
    myName = textProp.myName
    myText = textProp.myText
    myType = textProp.myType
    eval(myName).innerHTML = returnQuote(myText)
    eval(myName).scrollTop = eval(myName).scrollHeight

}
function setVal() {
    eval("textProp=" + takeQouteOut(TextArgs))
    myName = textProp.myName
    myText = textProp.myText
    myType = textProp.myType
    if (myType == "InputText") {
        eval(myName + "InputText").value = returnQuote(myText)
    }

}

/*	Name: addText()
Description: This function adds the text both sent to and received from another IM user 
during an IM conversation. The function adds to the "chat" text window where the conversation appears.
The TextArgs parameter receives an object that contains a snippet of formatted HTML that looks like this:
{myName:'chattxt',myText:'<table style='margin: 0px; padding: 0px; border: none;'><tr><td style='padding-left: 3px'><font dir='rtl' Align='right' Face='Arial' color='3A7508'><b>100021231</b></font>:</td><td><font dir='rtl' Align='right' Face='Arial'><FONT FACE=\"Arial\" SIZE=\"12\" COLOR=\"#000000\">this is not OK text! Too big</font></td></tr></table>'}*/
function addText() {
    eval("textProp=" + takeSingleQouteOut(takeQouteOut(TextArgs)))
    myName = textProp.myName
    myText = textProp.myText
    eval(myName).innerHTML = eval(myName).innerHTML + returnQuote(myText)
    eval(myName).scrollTop = eval(myName).scrollHeight
}

/*	Name: getText()
Description: This function gets the message typed by the user, after clicking enter, or send
and sends that text to a Flash internal variable. */
function getText() {
    if (eval(divName).myType == "InputText") {
        flashDiv.setVariable("_root.divText", eval(divName + "InputText").value);
    } else {
        flashDiv.setVariable("_root.divText", eval(divName).innerHTML);
    }
}

function removeDiv() {
    eval(divName).style.visibility = "hidden"
}
function setStyle() {
    // get flash object location on screen
    refObj = getAbsolutePos(flashDiv)
    flashWidth = flashDiv.getVariable("stage.width")
    flashHeight = flashDiv.getVariable("stage.height")
    eval(divProp.myName).style.visibility = "visible"
    flashXscale = (flashDiv.offsetWidth / flashWidth)
    flashYscale = (flashDiv.offsetHeight / flashHeight)
    eval(divProp.myName).style.left = (divProp.x * flashDiv.offsetWidth) / flashWidth + refObj.x
    eval(divProp.myName).style.top = (divProp.y * flashDiv.offsetHeight) / flashHeight + refObj.y
    if (flashDiv.width.substr(flashDiv.width.length - 1, 1) == "%") {
        myFontSize = Number(divProp.mySize.substr(divProp.mySize.length - 4, divProp.mySize.length - 2))
        eval(divProp.myName).style.width = 100 * (flashXscale * divProp.width / document.body.clientWidth) + "%"
        eval(divProp.myName).style.height = 100 * (flashYscale * divProp.height / document.body.clientHeight) + "%"
    } else {
        eval(divProp.myName).style.width = divProp.width
        eval(divProp.myName).style.height = divProp.height
        eval(divProp.myName).style.fontSize = divProp.mySize;
    }
    eval(divProp.myName).style.padding = divProp.padding
    //eval(divProp.myName).style.width=divProp.width-2*divProp.padding
    //eval(divProp.myName).style.height=divProp.height-2*divProp.padding
    //eval(divProp.myName).style.fontSize=divProp.mySize;
    eval(divProp.myName).style.fontFamily = divProp.myFont
    eval(divProp.myName).style.color = divProp.myColor
    //eval(divProp.myName).style.border="solid 1px"	
    eval(divProp.myName).target = divProp.myTarget
    eval(divProp.myName).myType = divProp.myType
    eval(divProp.myName).dir = divProp.dir
    eval(divProp.myName).Align = divProp.myAlign
    if (divProp.scrollFlag) {
        eval(divProp.myName).style.overflow = 'auto'
    }
}

function getAbsolutePos(obj) {
    totalX = obj.offsetLeft
    totalY = obj.offsetTop
    while (obj.tagName != "BODY") {
        obj = obj.offsetParent
        totalX += obj.offsetLeft
        totalY += obj.offsetTop

    }

    return ({ x: totalX, y: totalY })

}

function resizeFunction() {
    //alert("resize")
    refObj = getAbsolutePos(flashDiv)
    flashWidth = flashDiv.getVariable("stage.width")
    flashHeight = flashDiv.getVariable("stage.height")
    flashXscale = (flashDiv.offsetWidth / flashWidth)
    flashYscale = (flashDiv.offsetHeight / flashHeight)
    for (var i in dynlayer.childNodes) {
        if (dynlayer.childNodes[i].tagName == "DIV") {
            dynlayer.childNodes[i].style.left = (dynlayer.childNodes[i].myX * flashDiv.offsetWidth) / flashWidth + refObj.x
            dynlayer.childNodes[i].style.top = (dynlayer.childNodes[i].myY * flashDiv.offsetHeight) / flashHeight + refObj.y
            if (flashDiv.width.substr(flashDiv.width.length - 1, 1) == "%") {
                dynlayer.childNodes[i].style.width = 100 * (flashXscale * dynlayer.childNodes[i].myWidth / document.body.clientWidth) + "%"
                dynlayer.childNodes[i].style.height = 100 * (flashYscale * dynlayer.childNodes[i].myHeight / document.body.clientHeight) + "%"
            }
        }
    }

    //updatefontSizes()
}