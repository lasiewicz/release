using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Matchnet.List.ValueObjects;

namespace Matchnet.Web.Applications.Chat.XMLResponses
{
	/// <summary>
	///	Sets whether or not userID wants blockedUserID blocked (added to their blocked list)
	///	Parameters
	///		domainID:		The identifier for the domain the user is on (not in used by us. We use URL to identify brand)
	///		userID:			The user�s Identifier (handled by base.MemberID);
	///		blockedUserID:		The user to block (or unblock)
	///		blocked:			A true or false value that specifies whether or not to block the user
	/// </summary>
	public class setBlockedStatus : BaseXMLResponse
	{

		private const string PARAM_TARGETUSERID = "blockedUserID";
		private const string PARAM_BLOCKED = "blocked";
		private const HotListCategory _hotListCategory = HotListCategory.IgnoreList; //modify the block list


		private void Page_Load(object sender, System.EventArgs e)
		{
			//get required parameters
			base.GetTargetUserID(PARAM_TARGETUSERID);
			bool AddToBlockList = base.GetParameter(PARAM_BLOCKED);

			// decide whether to add friend or
			switch (AddToBlockList) 
			{
				case true:
					base.AddListMember(_hotListCategory);
					break;
				case false:
					base.RemoveListMember(_hotListCategory);
					break;
			}

			// send nothing in response except root element
			startResponse();
			endResponse();
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
