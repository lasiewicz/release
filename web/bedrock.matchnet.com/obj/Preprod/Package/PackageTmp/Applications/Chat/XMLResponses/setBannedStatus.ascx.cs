using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace Matchnet.Web.Applications.Chat.XMLResponses
{

	/// <summary>
	///		Sets whether or not userID was banned by an admin.  Once banned, the user will be disconnected, it is recommended that any future getUser calls return INVALID for the user so they cannot reconnect
	///		
	///		Parameters:
	///			domainID: the identifier for the domain the user is on (ignored)
	///			userID:	The user�s Identifier (handled by base MemberID)
	///			banned:	A true or false value that specifies whether or not to ban the user
	/// </summary>
	public class setBannedStatus : BaseXMLResponse
	{

		private const string PARAM_BANNED = "banned";

		private void Page_Load(object sender, System.EventArgs e)
		{
			BanMember();
			base.startResponse();
			base.endResponse();
		}

		/// <summary>
		/// Bans a member based on the querystring parameters
		/// </summary>
		private void BanMember()
		{
			if(Request[PARAM_BANNED] != null)
			{
				if(Request[PARAM_BANNED].ToLower() == "true")
				{
					// ban the member with int value of 1 (Value of 1 = Banned from within UP Chat app)
					this.Member.SetAttributeInt(g.Brand, "ChatBanned", 1);
				}
				else
				{
					// unban the member with int value of 0 (Value of 0 = Not banned from UP Chat app)
					this.Member.SetAttributeInt(g.Brand, "ChatBanned", 0);
				}

				Matchnet.Member.ServiceAdapters.MemberSA.Instance.SaveMember(this.Member);
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
