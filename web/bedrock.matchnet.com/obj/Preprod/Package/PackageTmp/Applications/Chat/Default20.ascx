﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="Default20.ascx.cs" Inherits="Matchnet.Web.Applications.Chat.Default20" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>

<asp:PlaceHolder Runat="server" ID="phChat">
<!-- <h1><mn:Txt id="Txt1" runat="server" ResourceConstant="NAV_CHAT" /></h1> -->
<div id="enter-chat">
    <div class="enter-chat-wrapper">
        <div class="enter-chat-info">
        <mn:Txt id="Txt2" runat="server" ResourceConstant="CHAT_DISCLAIMER" />
	    <mn:image id="imgChatButton" runat="server" filename="btn-chat.png" NavigateURL="javascript:launchWindow('/Applications/Chat/upChat.aspx', 'ChatWindow', 750, 550, 'scrollbars=no,resizable=yes,menubar=no,location=no')" 
	    ResourceConstant="TXT_CHAT_INTRO" class="enter-chat-button" />
	    </div>
	    <!-- <div class="enter-chat-image">
	    <mn:image id="Image1" runat="server" filename="cht-enter-cropped.jpg"   ResourceConstant="TXT_CHAT_INTRO"/>
	    </div> -->
	</div>
</div>
</asp:PlaceHolder>

<asp:PlaceHolder runat="server" ID="phBanned" Visible="False">
	<mn:image id="imgChat" runat="server" filename="icon_grn_chat.gif" />
	&nbsp;<mn:txt runat="server" id="txtBanned" ResourceConstant="CHT_BANNED" />
</asp:PlaceHolder>