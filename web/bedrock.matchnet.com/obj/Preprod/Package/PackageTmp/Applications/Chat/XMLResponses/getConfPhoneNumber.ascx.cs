﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Matchnet.Web.Applications.Chat.XMLResponses
{
    public partial class getConfPhoneNumber : BaseXMLResponse
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            startResponse();
            writeElement("confPhoneNumber", "<![CDATA[]]>");
            endResponse();
        }
    }
}