using System;
using System.Web;
using System.Web.UI.WebControls;
using Matchnet.Web.Framework;
using Matchnet.Member.ServiceAdapters;
using Matchnet.List.ValueObjects;
using Matchnet.List.ServiceAdapters;

namespace Matchnet.Web.Applications.Chat
{
	/// <summary>
	///		Summary description for ChatXML.
	/// </summary>
	public class ChatXML : FrameworkControl
	{
		protected System.Web.UI.WebControls.PlaceHolder phResponseControl;
		protected int _MemberID = Constants.NULL_INT;
		protected Matchnet.Member.ServiceAdapters.Member _Member;

		/// <summary>
		/// Requests the QS paramater 'function' and loads the appropriate control or performs the called for action.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void Page_Load(object sender, System.EventArgs e)
		{
			try
			{
				g.SaveSession = false;

				// examine the request to determine the correct response control.
				string function = Request.QueryString["function"];
		
				if (function == null)
				{	// this will result in a valid respose to this unknown request.
					function = string.Empty;
				}

				switch(function.ToLower())
				{
					//load the XML response control accordingly
					case "getdomainpreferences":
					case "getuser":
					case "setbannedstatus": // whether to ban a member
					case "setblockedstatus": // whether to add or remove a member from blocked list
					case "setfriendstatus": // whether to add or remove a member from hotlist
                    case "getconfphonenumber": // Returns the confPhoneNumber  
						phResponseControl.Controls.Add(LoadControl("/Applications/Chat/XMLResponses/" + function.ToLower() + ".ascx"));
						break;
					// We have no implementation for the following methods, but they still require a blank XML response
					case "onroomstatuschange":
					case "onuserconnectionchange":
					case "onuserroomchange":
						SendBlankXMLReply();						
						break;
					default:
						Response.Write(string.Format("Querystring parameter {0} must contain a valid operation name.",function));
						break;
				}
			}
			catch(Exception ex)
			{
				g.ProcessException(ex);
			}
		}

		/// <summary>
		/// Sends a blank XML reply to userplane. Certain "functions" called by Userplane
		/// have no return values, but require at minimum the root XML element, even if not implemented.
		/// </summary>
		private void SendBlankXMLReply() 
		{
			phResponseControl.Controls.Add(LoadControl("/Applications/Chat/XMLResponses/NoResponse.ascx"));
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
