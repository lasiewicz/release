function csEvent(strEvent, strParameter1, strParameter2)
{
	// if the event fired is a viewprofile click
	if(strEvent == "User.ViewProfile")
	{	
		// the script to launchWindow is created by .Net with a -1 in place of member ID
		var completeProfileLink = destinationProfileLink.replace("-1", strParameter1);
	
		// if the click to view a profile is the same as the current user, show a view profile for the current user
		// in opener window, if possible, else pop-open new window.
		if( strParameter1 == ownMemberID )
		{	
			//checking for window.opener. Careful to check for parent (using frames). Putting inside try catch block causes problems when trying to access closed window: gives you security alert warning!
			var mywin = window.parent.opener;
			if (mywin != null && !mywin.closed) 
			{
				mywin.location.href = ownProfileLink;
				mywin.focus();
			}
			else
			{
				launchWindow(ownProfileLink, 'ProfileFromIM', 810, 600, "scrollbars=yes,resizable=yes,menubar=yes,location=yes,directories=yes,toolbar=yes");
			}
		}
		else
		{
			//open in new window
			eval(completeProfileLink);
			//javascript:launchWindow('http://dv-barney.jdate.com/Applications/MemberProfile/ViewProfile.aspx?MemberID=-1&Ordinal=1&PersistLayoutTemplate=1&LayoutTemplateID=11', '-1', 622, 550, 'scrollbars=yes,resizable=no')
		}
	}
	
	if(strEvent == "InstantCommunicator.StartConversation")
	{
		var bServer = strParameter2; //whether or not this is a server initiated IM conversation invitation (
		// only open up a WebMessenger window if bServer is false, because non-user initiated pop-up windows will be blocked by most pop-up blockers 16749
		if (!bServer) 
		{
			LaunchNewIMConversation(strParameter1); //strParameter1 is the userID of the initiating user who sent conversation invitation
		}
	}
	
	if (strEvent == "Chat.Help" || strEvent == "Webchat.Help") {
		//open a help window
		var helpLink = "/Applications/Article/ArticleView.aspx?CategoryID=1000339&ArticleID=100000120";

			//checking for window.opener. Careful to check for parent (using frames). Putting inside try catch block causes problems when trying to access closed window: gives you security alert warning!
			var mywin = window.parent.opener;
			if (mywin != null && !mywin.closed)
			{
				mywin.location.href = helpLink;
				mywin.focus();
			}
			else 
			{
				launchWindow(helpLink, 'Webchat.Help', 810, 600, "scrollbars=yes,resizable=yes,menubar=yes,location=yes,directories=yes,toolbar=yes");
			}
	}
}