﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MobileLanding.ascx.cs" Inherits="Matchnet.Web.Applications.Mobile.MobileLanding" %>

<!--main page-->

<asp:PlaceHolder ID="pnlMain" runat="server" Visible="false" EnableViewState="false">
    <link id="" rel="stylesheet" type="text/css" href="/css/Mobile/main.css" />
    <link id="" rel="stylesheet" type="text/css" href="/css/Mobile/style.css" />
    <script>
        $j(function () {
            var GET = {},
                query = window.location.search.substring(1).split("&");

            for (var i = 0, max = query.length; i < max; i++) {
                if (query[i] === "") // check for trailing & with no param
                    continue;
                var param = query[i].split("=");
                GET[decodeURIComponent(param[0])] = decodeURIComponent(param[1] || "");
            }
            if (query[0] !== "" && GET.App) {
                var app = $j('a.features' + '.' + GET.App).attr('href');
                eval(app);
            } else {
                $j('#blockUIEl').remove();
            }
        });
    </script>
    <div id="mainDiv" style="position: relative;">
        <div id="blockUIEl">
            <div class="blockUI blockOverlay" style="z-index: 1000; border: none; margin: 0px; padding: 0px; width: 100%; height: 100%; top: 0px; left: 0px; opacity: 1; cursor: default; position: absolute; background-color: rgb(255, 255, 255);"></div>
            <div class="blockUI blockMsg blockElement" style="z-index: 1011; position: absolute; padding: 0px; margin: 0px; width: 30%; top: 30%; left: 348px; text-align: center; color: rgb(0, 0, 0); border: none; cursor: default; height: 85px; background-color: transparent;"><div class="loading spinner-only"></div><blockquote>Loading...</blockquote></div>
        </div>
        <article class="clearfix gradient">
            <header>
                <hgroup>
                    <h1 class="ir">Find a Date (or Love) Anywhere You Go.</h1>
                    <h2 style="margin: 5px 0 25px; font: normal normal normal 1.563em museo-sans,arial,'helvetica neue',helvetica,sans-serif; color: #525252; display: block;">JDate just became your new wingman.</h2>
                </hgroup>
            </header>
            <img src="../../img/Community/JDate/Mobile/jd-iphone.png" alt="" width="238" height="518" border="0" class="eye-catcher" />
            <section>
                <asp:LinkButton runat="server" OnClick="lbISeeFeatures_Click" CssClass="app-icon"><span id="jdapp-icon"></span></asp:LinkButton>
                <h1>
                    <asp:LinkButton runat="server" OnClick="lbISeeFeatures_Click">JDate iOS<br>App</asp:LinkButton>
                </h1>
                <p>Our full-featured iPhone app is the easiest new way to connect with JDaters!
                This robust app allows you to quickly search, browse, and chat from anywhere.
                </p>
                <asp:LinkButton Text="SEE FEATURES" runat="server" OnClick="lbISeeFeatures_Click" CssClass="features ios" /><a class="arrow"><span></span></a>
                <div class="stores">
                    <a href="https://itunes.apple.com/us/app/jdate/id484715536?ls=1&mt=8" id="app-store" class="ir" target="_blank">Download on the App Store</a>
                </div>
            </section>
            <section>
                <asp:LinkButton runat="server" OnClick="lbJSeeFeatures_Click" CssClass="app-icon"><span id="qv-icon"></span></asp:LinkButton>
                <h1>
                    <asp:LinkButton runat="server" OnClick="lbJSeeFeatures_Click">JPix by JDate<br />Mobile App</asp:LinkButton>
                </h1>
                <p>A fun complement to our mobile site, JPix was created to give you an easy way to connect on JDate. Browse profiles, send Flirts, and Favorite your top picks in minutes!</p>
                <asp:LinkButton Text="SEE FEATURES" runat="server" OnClick="lbJSeeFeatures_Click" CssClass="features android" /><a class="arrow"><span></span></a>
                <div class="stores">
                    <a href="https://play.google.com/store/apps/details?id=com.spark.android.jpix" id="play-store" class="ir" target="_blank">Android App on Google Play</a>
                </div>
            </section>
            <section>
                <asp:LinkButton runat="server" OnClick="lbMSeeFeatures_Click" CssClass="app-icon"><span id="mos-icon"></span></asp:LinkButton>
                <h1>
                    <asp:LinkButton runat="server" OnClick="lbMSeeFeatures_Click">JDate Mobile<br />Optimized Site<span></span></asp:LinkButton>
                </h1>
                <p>With our full-featured mobile-optimized site, you can browse profiles, search through your matches, send emails and IM &#8212; all from your smartphone.</p>
                <asp:LinkButton Text="SEE FEATURES" ID="lbSeeFeatures" runat="server" OnClick="lbMSeeFeatures_Click" CssClass="features mos" /><a class="arrow"><span></span></a>
            </section>
        </article>
    </div>
</asp:PlaceHolder>

<asp:PlaceHolder ID="pnlIOS" runat="server" EnableViewState="false" Visible="false">
    <link id="" rel="stylesheet" type="text/css" href="/css/Mobile/main.css" />
    <link id="" rel="stylesheet" type="text/css" href="/css/Mobile/style-sub.css" />
    <link id="" rel="stylesheet" type="text/css" href="/css/Mobile/istyle.css" />
    <div id="mainDiv" class="ios">
        <article class="clearfix gradient">
            <nav>
                <ul>
                    <li>
                        <asp:LinkButton runat="server" OnClick="lbSeeFeatures_Click">Mobile Overview</asp:LinkButton></li>
                    <li class="current">
                        <asp:LinkButton runat="server" OnClick="lbISeeFeatures_Click">JD iOS App</asp:LinkButton></li>
                    <li>
                        <asp:LinkButton runat="server" OnClick="lbMSeeFeatures_Click">Mobile-Optimized Site</asp:LinkButton></li>
                    <li>
                        <asp:LinkButton runat="server" OnClick="lbJSeeFeatures_Click">JPix by JDate</asp:LinkButton></li>
                </ul>
            </nav>
            <header>
                <hgroup>
                    <h1 class="ir">A Site Built for You</h1>
                    <h2>Discover the easiest new way to #GetChosen!</h2>
                </hgroup>
            </header>
            <p>
                Our brand-new JDate app is the fun way to connect with likeminded Jewish singles near you. Browse profiles, chat in real-time, and send a message to someone who shares your love of bagels and lox.
            </p>
            <div class="stores">
                <a href="https://itunes.apple.com/us/app/jdate/id484715536?ls=1&mt=8" id="app-store" class="ir" target="_blank">Download on the App Store</a>
            </div>
            <img src="../../img/Community/JDate/Mobile/ios/iphone.png" alt="" width="145" height="360" border="0" id="iphone" />
        </article>
        <article id="content">
			<nav>
				<ul>
					<li class="current" data-tag="the-features">Features<span></span></li>
					<li data-tag="faq">FAQ<span></span></li>
					<li data-tag="feedback">Feedback<span></span></li>
				</ul>
			</nav>
			<section id="the-features" class="current-tab">
				<header>
					<div id="feature-nav">
						<div id="nav-up" class="disabled"></div>
						<div class="feature-nav">
							<ul>
								<li class="selected"><a href="#" class="view-profile ir" title="View Profile">View Profile</a></li>
								<li><a href="#" class="secret-admirer ir" title="Secret Admirer">Secret Admirer</a></li>
								<li><a href="#" class="explore ir" title="Explore">Explore</a></li>
								<li><a href="#" class="chat ir" title="Chat">Chat</a></li>
								<li><a href="#" class="search ir" title="Search">Search</a></li>
								<li><a href="#" class="mol ir" title="Members Online">Members Online</a></li>
								<li><a href="#" class="match ir" title="Matches">Matches</a></li>
								<li><a href="#" class="inbox ir" title="Inbox">Inbox</a></li>
								<li><a href="#" class="upload-photo ir" title="Upload Photo">Upload Photo</a></li>
								<li><a href="#" class="viewed-you ir" title="Viewed You">Viewed You</a></li>
								<li><a href="#" class="subscribe ir" title="Subscribe">Subscribe</a></li>
							</ul>
						</div>
						<div id="nav-down"></div>
					</div>
				</header>
				<div id="view-profile" class="section-img enabled"></div>
				<div id="secret-admirer" class="section-img"></div>
				<div id="explore" class="section-img"></div>
				<div id="chat" class="section-img"></div>
				<div id="search" class="section-img"></div>
				<div id="mol" class="section-img"></div>
				<div id="match" class="section-img"></div>
				<div id="inbox" class="section-img"></div>
				<div id="upload-photo" class="section-img"></div>
				<div id="viewed-you" class="section-img"></div>
				<div id="subscribe" class="section-img"></div>
				<div id="view-profile-content" class="featured-content enabled">
					<div class="feature-icon"></div>
					<h1>View Profile</h1>
					<p>
						Check your JDate profile from anywhere with the &ldquo;View Profile&rdquo;
						feature. Remember, your profile is the first impression you make, so put
						your best self forward!
					</p>
				</div>
				<div id="secret-admirer-content" class="featured-content">
					<div class="feature-icon"></div>
					<h1>Secret Admirer</h1>
					<p>
						A member favorite, the Secret Admirer game is a fun, anonymous way to be
						matched with people you like. Simply tell us when someone catches your
						eye and we&rsquo;ll secretly find out if the feeling is mutual. If it&rsquo;s
						a match, then we&rsquo;ll let you both know!
					</p>
				</div>
				<div id="explore-content" class="featured-content">
					<div class="feature-icon"></div>
					<h1>Explore</h1>
					<p>
						View all your profile activity at a glance! From seeing when someone
						&ldquo;Viewed You&rdquo; to getting instant chat requests, the JDate
						app makes it easy for you to explore, connect, and (most importantly)
						meet new people! 
					</p>
				</div>
				<div id="chat-content" class="featured-content">
					<div class="feature-icon"></div>
					<h1>Chat</h1>
					<p>
						Did someone&rsquo;s profile grab your attention? If they&rsquo;re
						online, start an IM chat and see if sparks fly. This fun feature allows
						you to connect in real-time with any JDater, whether they are online
						through our full website OR within the app!
					</p>
				</div>
				<div id="search-content" class="featured-content">
					<div class="feature-icon"></div>
					<h1>Search</h1>
					<p>
						Do a quick search and find all the Jewish singles who match your
						specific preferences. Just enter the characteristics and desired
						location of the person you want to meet and get ready to be wowed! 
					</p>
				</div>
				<div id="mol-content" class="featured-content">
					<div class="feature-icon"></div>
					<h1>Members Online</h1>
					<p>
						Simply tap on the &ldquo;Members Online&rdquo; button to scroll
						through all the members online right this very instant. See someone
						you like? Start chatting in real-time!
					</p>
				</div>
				<div id="match-content" class="featured-content">
					<div class="feature-icon"></div>
					<h1>Matches</h1>
					<p>
						Whether you&rsquo;re stuck in a waiting room, on the subway, or standing in line, you
						can now edit your profile from anywhere! Update your profile picture, change
						your search preferences, or add an essay &#8212; all easy to do and all increase
						your odds of meeting someone new! 
					</p>
				</div>
				<div id="inbox-content" class="featured-content">
					<div class="feature-icon"></div>
					<h1>Inbox</h1>
					<p>
						We hear it from our Success Stories all the time: their love story started
						with a simple email on JDate! Now you can read your messages, view sent messages,
						and even send an email using our iPhone app. No more waiting until you&rsquo;re
						in front of a computer to email that someone special.
					</p>
				</div>
				<div id="upload-photo-content" class="featured-content">
					<div class="feature-icon"></div>
					<h1>Upload Photo</h1>
					<p>
						A great photo is all it takes to grab the attention of your dream date! Our app
						makes it easy to not only upload a photo from your Apple device, Facebook, or
						Instagram, but you can also take and upload a photo right from within the app.
						Smile pretty &#8212; your Beshert could be watching!
					</p>
				</div>
				<div id="viewed-you-content" class="featured-content">
					<div class="feature-icon"></div>
					<h1>Viewed You</h1>
					<p>
						Want to see who&rsquo;s been checking you out? Simply tap on &ldquo;Viewed You&rdquo;
						and scroll through the long list of potential matches who have been eyeing your
						profile! It&rsquo;s one of the quickest and easiest ways to strike up a conversation
						with someone new.
					</p>
				</div>
				<div id="subscribe-content" class="featured-content">
					<div class="feature-icon"></div>
					<h1>Subscribe</h1>
					<p>
						Become a subscriber from the JDate app at any time. See a member you want to
						email? Simply tap &ldquo;Subscribe&rdquo; and upgrade immediately so you can
						connect with that person &#8212; and any other JDater who catches your attention!
					</p>
				</div>
			</section>
			<section id="faq">
				<h1>FAQ</h1>
				<dl>
					<dt>What is the JDate iPhone app?</dt>
					<dd>
						The JDate iPhone app is the easiest new way to connect with Jewish singles near you. Whether you&rsquo;re at home, on the
						train, or in line at the bagel shop, this app allows you to browse JDate using your Apple device! Not only is this fun
						app packed with features, but it&rsquo;s also the best thing since sliced challah (chai five, ya&rsquo;ll)! Download it
						right now and start connecting with hundreds of thousands of active JDaters today!
					</dd>
					<dt>What features are available on the JDate iPhone app?</dt>
					<dd>With the JDate app, you can:
						<ol>
							<li>Browse member profiles with ease</li>
							<li>Chat instantly with members on the site OR in the app</li>
							<li>Play Secret Admirer anonymously and be matched with interested members</li>
							<li>Access your inbox, sent mail and deleted messages</li>
							<li>View all profile activity at a glance</li>
							<li>Upload photos from your device, Facebook or Instagram</li>
							<li>Or take and upload a selfie on the spot</li>
							<li>See who&rsquo;s viewed you</li>
							<li>See who&rsquo;s Favorited you</li>
							<li>Access your Favorites list</li>
							<li>View Your Matches and Members Online</li>
							<li>Create, edit and view your profile</li>
							<li style='list-style-type: none;'>And more&hellip;</li>
						</ol>
					</dd>
					<dt>Can I become a JDate subscriber via the iPhone app?</dt>
					<dd>
						Yes, you can subscribe via Apple&rsquo;s in-app purchase.
					</dd>
					<dt>What type of communication can I send in the JDate app?</dt>
					<dd>
						All subscribers can chat instantly with other members online, email messages and send Flirts.
					</dd>
					<dt>Why should I download the app?</dt>
					<dd>
						The JDate app is the quicker, easier, superior way to meet likeminded Jewish singles. Download it today and find more Jews
						than the buffet line at a Chinese restaurant on Christmas.
					</dd>
				</dl>
			</section>
			<section id="feedback">
				<h1>Feedback</h1>
				<p>
					<strong>Do you have a question or comment?</strong><br />
					Give us a call at <a href="tel:+18774533861">1-877-453-3861</a>, or use the form below to send us an email. Our customer care staff is here to help you find that<br />special someone.
				</p>
				<p>
					If you prefer, you can send us mail at the following address. Please be sure to include your full
					name, username, and email address.
				</p>
				<div class="vcard">
					<p class="org">JDate.com<p>
					<p class="adr">
						<span class="street-address">P.O. Box 540</span><br />
						<span class="locality">Lehi</span>, <span class="region">UT</span> <span class="postal-code">84043</span> <span class="country-name">U.S.A.</span>
					</p>
				</div>
			</section>
		</article>
    </div>
    <script>
        var CMMJS = {

            changeEl: function () {

                $j('#feature-nav a').click(function (e) {
                    e.preventDefault();
                    var classArr = $j(this).attr('class'),
                        theClass, theLink;
                    classArr = classArr.split(' ');
                    for (x in classArr) {
                        if (classArr[x] !== "ir") {
                            theClass = classArr[x];
                        }
                    }
                    $j('.section-img.enabled').removeClass('enabled');
                    $j("#" + theClass).addClass('enabled');
                    theLink = $j("." + theClass);
                    $j('#feature-nav li.selected').removeClass('selected');
                    theLink.parent().addClass('selected');
                    theClass = theClass + "-content";
                    theClass = $j("#" + theClass);
                    $j('.featured-content.enabled').removeClass('enabled');
                    theClass.addClass('enabled');
                })
            },
            changeTab: function () {
                $j('#content nav li').click(function (e) {
                    e.preventDefault();
                    var tagName = $j(this).attr('data-tag');
                    $j('.current-tab').removeClass('current-tab');
                    $j('#' + tagName).addClass('current-tab');
                    $j('#content nav li.current').removeClass('current');
                    $j(this).addClass('current');
                })

            },
            navUp: function () {
                var pos = parseInt($j('.feature-nav ul').css("margin-top"));
                var new_pos = pos + 72;
                if (new_pos <= 0) {
                    $j('.feature-nav ul').animate({ marginTop: new_pos + "px" }, {
                        //easing: CMMJS.offUpArrow(),
                        duration: 200,
                        complete: CMMJS.onUpArrow()
                    });
                    $j('#nav-down').toggleClass("disabled", false);
                }
                if (new_pos == 0) {
                    $j('#nav-up').toggleClass("disabled", true);
                }
            },
            navDown: function () {
                var pos = parseInt($j('.feature-nav ul').css("margin-top"));
                var new_pos = pos - 72;
                if (new_pos >= -504) {//360){
                    $j('.feature-nav ul').animate({ marginTop: new_pos + "px" }, {
                        //easing: CMMJS.offDownArrow(),
                        duration: 200,
                        complete: CMMJS.onDownArrow()
                    });
                    $j('#nav-up').toggleClass("disabled", false);
                }
                if (new_pos == -504) {//360){
                    $j('#nav-down').toggleClass("disabled", true);
                }
            },
            offDownArrow: function () {
                $j('#nav-down').hide();
            },
            onDownArrow: function () {
                setTimeout(
                  function () {
                      $j('#nav-down').show();
                  }, 700);
            },
            offUpArrow: function () {
                $j('#nav-up').hide();
            },
            onUpArrow: function () {
                setTimeout(
                  function () {
                      $j('#nav-up').show();
                  }, 700);
            }

        }

        $j(document).ready(function () {

            CMMJS.changeEl();
            CMMJS.changeTab();


            $j('#nav-up').click(function () {
                CMMJS.navUp();
            });

            $j('#nav-down').click(function () {
                CMMJS.navDown();
            });
        });

    </script>
</asp:PlaceHolder>

<asp:PlaceHolder ID="pnlMobile" runat="server" EnableViewState="false" Visible="false">
    <link id="" rel="stylesheet" type="text/css" href="/css/Mobile/main.css" />
    <link id="" rel="stylesheet" type="text/css" href="/css/Mobile/style-sub.css" />
    <link id="" rel="stylesheet" type="text/css" href="/css/Mobile/mstyle.css" />
    <div id="mainDiv">
        <article class="clearfix gradient">
            <nav>
                <ul>
                    <li>
                        <asp:LinkButton runat="server" OnClick="lbSeeFeatures_Click">Mobile Overview</asp:LinkButton></li>
                    <li>
                        <asp:LinkButton runat="server" OnClick="lbISeeFeatures_Click">JD iOS App</asp:LinkButton></li>
                    <li class="current">
                        <asp:LinkButton runat="server" OnClick="lbMSeeFeatures_Click">Mobile-Optimized Site</asp:LinkButton></li>
                    <li>
                        <asp:LinkButton runat="server" OnClick="lbJSeeFeatures_Click">JPix by JDate</asp:LinkButton></li>
                </ul>
            </nav>
            <header>
                <hgroup>
                    <h1 class="ir">A Site Built for You</h1>
                    <h2>Busy schedule? It's no longer an excuse!</h2>
                </hgroup>
            </header>
            <p>
                Our mobile-optimized site makes it even easier to connect with JDaters near you! Showcasing
					the best features of JDate.com, you can browse profiles and photos, send emails, start an
					IM conversation, edit your profile and even upload a photo &#8212; from anywhere, at any time.
            </p>
            <img src="../../img/Community/JDate/Mobile/mos/iphone.png" alt="" width="145" height="360" border="0" id="iphone" />
            <img src="../../img/Community/JDate/Mobile/mos/android.png" alt="" width="170" height="358" border="0" id="android" />
        </article>
        <article id="content">
            <nav>
                <ul>
                    <li class="current" data-tag="the-features">Features<span></span></li>
                    <li data-tag="faq">FAQ<span></span></li>
                    <li data-tag="feedback">Feedback<span></span></li>
                </ul>
            </nav>
            <section id="the-features" class="current-tab">
                <header>
                    <div id="feature-nav">
                        <div id="nav-up" class="disabled"></div>
                        <div class="feature-nav">
                            <ul>
                                <li class="selected"><a href="#" class="search ir" title="Search">Search</a></li>
                                <li><a href="#" class="inbox ir" title="Inbox">Inbox</a></li>
                                <li><a href="#" class="viewed-you ir" title="Viewed You">Viewed You</a></li>
                                <li><a href="#" class="favorited-you ir" title="Favorited You">Favorited You</a></li>
                                <li><a href="#" class="your-favorites ir" title="Your Favorites">Your Favorites</a></li>
                                <li><a href="#" class="view-profile ir" title="View Profile">View Profile</a></li>
                                <li><a href="#" class="edit-profile ir" title="Edit Profile">Edit Profile</a></li>
                                <li><a href="#" class="photo-upload ir" title="Photo Upload">Photo Upload</a></li>
                                <li><a href="#" class="members-online ir" title="Members Online">Members Online</a></li>
                                <li><a href="#" class="im ir" title="Instant Message">Instant Message</a></li>
                                <li><a href="#" class="subscribe ir" title="Subscribe">Subscribe</a></li>
                            </ul>
                        </div>
                        <div id="nav-down"></div>
                    </div>
                </header>
                <div id="search" class="section-img enabled"></div>
                <div id="inbox" class="section-img"></div>
                <div id="viewed-you" class="section-img"></div>
                <div id="favorited-you" class="section-img"></div>
                <div id="your-favorites" class="section-img"></div>
                <div id="view-profile" class="section-img"></div>
                <div id="edit-profile" class="section-img"></div>
                <div id="photo-upload" class="section-img"></div>
                <div id="members-online" class="section-img"></div>
                <div id="im" class="section-img"></div>
                <div id="subscribe" class="section-img"></div>
                <div id="search-content" class="featured-content enabled">
                    <div class="feature-icon"></div>
                    <h1>Search</h1>
                    <p style="width: 370px;">
                        Do a quick search and find all the Jewish singles who match your preferences.
							Just set the criteria you want and click &ldquo;Search.&rdquo; With thousands of JDaters<sup>&reg;</sup>
                        near you, there&rsquo;s a good chance you may just find your Beshert!
                    </p>
                </div>
                <div id="inbox-content" class="featured-content">
                    <div class="feature-icon"></div>
                    <h1>Inbox</h1>
                    <p style="width: 370px;">
                        Ready to send a message to one of your matches? Or, maybe you want to read the
							new message you received a few minutes ago? Simply head to your inbox, read
							your message, and respond right away. No more waiting until you're on your
							computer to email someone special.
                    </p>
                </div>
                <div id="viewed-you-content" class="featured-content">
                    <div class="feature-icon"></div>
                    <h1>Viewed You</h1>
                    <p style="width: 370px;">
                        Want to see who&rsquo;s checking you out? Simply tap on the &ldquo;Viewed You&rdquo; button and
							scroll through the long list of potential matches who have been eyeing your profile!
                    </p>
                </div>
                <div id="favorited-you-content" class="featured-content">
                    <div class="feature-icon"></div>
                    <h1>Matches</h1>
                    <p style="width: 370px;">
                        Get ready to find your best, most relevant matches! Our Mutual Match system compares your
							preferences to calculate how well you and each of your matches fit. Try it now to see if
							you find your perfect match!
                    </p>
                </div>
                <div id="your-favorites-content" class="featured-content">
                    <div class="feature-icon"></div>
                    <h1>Secret Admirer</h1>
                    <p style="width: 370px;">
                        Play the Secret Admirer game! You choose the matches you like and we&rsquo;ll secretly find
							out if they&rsquo;re interested in you&hellip; who said finding love was hard?! 
                    </p>
                </div>
                <div id="view-profile-content" class="featured-content">
                    <div class="feature-icon"></div>
                    <h1>View Profile</h1>
                    <p style="width: 370px;">
                        Check your JDate profile from anywhere with the &ldquo;View Profile&rdquo; feature. Remember,
							your profile is the first impression you make, so put your best foot forward!
                    </p>
                </div>
                <div id="edit-profile-content" class="featured-content">
                    <div class="feature-icon"></div>
                    <h1>Edit Profile</h1>
                    <p style="width: 370px;">
                        Whether you&rsquo;re stuck in a waiting room, on the subway, or standing in line, you
							can now edit your profile from anywhere! Update your profile picture, change
							your search preferences, or add an essay &#8212; all easy to do and all increase
							your odds of meeting someone new! 
                    </p>
                </div>
                <div id="photo-upload-content" class="featured-content">
                    <div class="feature-icon"></div>
                    <h1>Photo Upload</h1>
                    <p style="width: 370px;">
                        When it comes to online dating, a photo is worth more than a thousand words! With JDate Mobile,
							you can add your own photos to your profile &#8212; right from your smartphone! If you&rsquo;ve
							got a great picture in your phone&rsquo;s gallery, simply upload it and let other members see
							the real you.
                    </p>
                </div>
                <div id="members-online-content" class="featured-content">
                    <div class="feature-icon"></div>
                    <h1>Members Online</h1>
                    <p style="width: 370px;">
                        See photos and profiles of everyone who&rsquo;s online when you are. Just tap the
							&ldquo;Members Online&rdquo; button in the top-right corner and take a look!
                    </p>
                </div>
                <div id="im-content" class="featured-content">
                    <div class="feature-icon"></div>
                    <h1>Instant Message</h1>
                    <p style="width: 370px;">
                        Did someone&rsquo;s profile grab your attention? If they&rsquo;re online, start an IM conversation
							and see if sparks fly. This fun feature allows you to have a quick chat with any JDater<sup>&reg;</sup>
                        right from your smartphone.
                    </p>
                </div>
                <div id="subscribe-content" class="featured-content">
                    <div class="feature-icon"></div>
                    <h1>Subscribe</h1>
                    <p style="width: 370px;">
                        You can become a subscriber from the JDate Mobile site at any time. If you see a
							member you want to email, simply tap &ldquo;Subscribe&rdquo; and upgrade immediately so you
							can connect with JDate singles just like you!
                    </p>
                </div>
            </section>
            <section id="faq">
                <h1>FAQ</h1>
                <dl>
                    <dt>What is the JDate Mobile-Optimized Site?</dt>
                    <dd>You can become a JDate subscriber at any time using your smartphone. If you see a
							member you want to email, simply click &ldquo;Subscribe&rdquo; and upgrade immediately so you
							can connect with Jewish singles near you!
                    </dd>
                    <dt>What features are available on the JDate Mobile-Optimized Site?</dt>
                    <dd>The following features are available on JDate Mobile:
							<ol>
                                <li>Search for and view member profiles</li>
                                <li>Send Instant Messages, email messages, and Flirts</li>
                                <li>View all members that are currently online</li>
                                <li>Check your inbox</li>
                                <li>View your Matches</li>
                                <li>View your Profile Activity</li>
                                <li>Register as a new member</li>
                                <li>Subscribe for access to all JDate features</li>
                                <li>Play the Secret Admirer game</li>
                                <li>Upload photos</li>
                                <li>And more&hellip;</li>
                            </ol>
                    </dd>
                    <dt>Is the JDate Mobile-Optimized Site an App?</dt>
                    <dd>No. The JDate Mobile Site is not an application, it is a website optimized for mobile devices. However, you can use
							JDate Mobile exactly like an app by visiting m.JDate.com on your mobile device, logging in, and
							&ldquo;adding the site to your home screen.&rdquo; This will put an icon for JDate Mobile on your phone, and tapping
							it will function just like an app.
                    </dd>
                    <dt>What is the web address of the JDate Mobile-Optimized Site?</dt>
                    <dd>The web address for JDate Mobile is: m.JDate.com. No &ldquo;http&rdquo; or &ldquo;www&rdquo; necessary! If accessing
							JDate Mobile from a mobile device, you can just type in JDate.com and you'll be automatically
							directed to JDate Mobile.
                    </dd>
                    <dt>I would like to become a subscriber via my mobile device.  What forms of payment may I use?
                    </dt>
                    <dd>You can use any form of payment that we accept on the full JDate site, including: American Express<sup>&reg</sup>,
							VISA<sup>&reg;</sup>, Mastercard<sup>&reg;</sup> and Discover<sup>&reg;</sup>.
                    </dd>
                    <dt>Is there an additional charge for the JDate Mobile-Optimized Site?</dt>
                    <dd>No, the JDate Mobile Site is free and both subscribers and non-subscribers are able to access the site. However, just
							as is the case on the full JDate website, non-subscribers on the mobile site are limited in their access to
							all the great communication features.
                    </dd>
                    <dt>What type of communication can I send on the JDate Mobile-Optimized Site?</dt>
                    <dd>All subscribers can send Instant Messages, email messages and Flirts. There is also a button on the Home page to view
							all members currently online, so you can see who is available to chat.
                    </dd>
                </dl>
            </section>
            <section id="feedback">
                <h1>Feedback</h1>
                <p>
                    <strong>Do you have a question or comment?</strong><br />
                    Give us a call at <a href="tel:+18774533861">1-877-453-3861</a>, or use the form below to send us an email. Our customer care staff is here to help you find that<br />
                    special someone.
                </p>

                <p>
                    If you prefer, you can send us mail at the following address. Please be sure to include your full
						name, username, and email address.
                </p>
                <div class="vcard">
                    <p class="org">
                        JDate.com<p>
                    <p class="adr">
                        <span class="street-address">P.O. Box 540</span><br />
                        <span class="locality">Lehi</span>, <span class="region">UT</span> <span class="postal-code">84043</span> <span class="country-name">U.S.A.</span>
                    </p>
                </div>
            </section>
        </article>
    </div>
    <script>
        var CMMJS = {

            changeEl: function () {

                $j('#feature-nav a').click(function (e) {
                    e.preventDefault();
                    var classArr = $j(this).attr('class'),
                        theClass, theLink;
                    classArr = classArr.split(' ');
                    for (x in classArr) {
                        if (classArr[x] !== "ir") {
                            theClass = classArr[x];
                        }
                    }
                    $j('.section-img.enabled').removeClass('enabled');
                    $j("#" + theClass).addClass('enabled');
                    theLink = $j("." + theClass);
                    $j('#feature-nav li.selected').removeClass('selected');
                    theLink.parent().addClass('selected');
                    theClass = theClass + "-content";
                    theClass = $j("#" + theClass);
                    $j('.featured-content.enabled').removeClass('enabled');
                    theClass.addClass('enabled');
                })
            },
            changeTab: function () {
                $j('#content nav li').click(function (e) {
                    e.preventDefault();
                    var tagName = $j(this).attr('data-tag');
                    $j('.current-tab').removeClass('current-tab');
                    $j('#' + tagName).addClass('current-tab');
                    $j('#content nav li.current').removeClass('current');
                    $j(this).addClass('current');
                })

            },
            navUp: function () {
                var pos = parseInt($j('.feature-nav ul').css("margin-top"));
                var new_pos = pos + 72;
                if (new_pos <= 0) {
                    $j('.feature-nav ul').animate({ marginTop: new_pos + "px" }, {
                        //easing: CMMJS.offUpArrow(),
                        duration: 200,
                        complete: CMMJS.onUpArrow()
                    });
                    $j('#nav-down').toggleClass("disabled", false);
                }
                if (new_pos == 0) {
                    $j('#nav-up').toggleClass("disabled", true);
                }
            },
            navDown: function () {
                var pos = parseInt($j('.feature-nav ul').css("margin-top"));
                var new_pos = pos - 72;
                if (new_pos >= -504) {//360){
                    $j('.feature-nav ul').animate({ marginTop: new_pos + "px" }, {
                        //easing: CMMJS.offDownArrow(),
                        duration: 200,
                        complete: CMMJS.onDownArrow()
                    });
                    $j('#nav-up').toggleClass("disabled", false);
                }
                if (new_pos == -504) {//360){
                    $j('#nav-down').toggleClass("disabled", true);
                }
            },
            offDownArrow: function () {
                $j('#nav-down').hide();
            },
            onDownArrow: function () {
                setTimeout(
                  function () {
                      $j('#nav-down').show();
                  }, 700);
            },
            offUpArrow: function () {
                $j('#nav-up').hide();
            },
            onUpArrow: function () {
                setTimeout(
                  function () {
                      $j('#nav-up').show();
                  }, 700);
            }

        }

        $j(document).ready(function () {

            CMMJS.changeEl();
            CMMJS.changeTab();


            $j('#nav-up').click(function () {
                CMMJS.navUp();
            });

            $j('#nav-down').click(function () {
                CMMJS.navDown();
            });
        });

    </script>
</asp:PlaceHolder>

<asp:PlaceHolder ID="pnlJpix" runat="server" EnableViewState="false" Visible="false">
    <%--<link id="" rel="stylesheet" type="text/css" href="/css/Mobile/normalize.css" />--%>
    <link id="" rel="stylesheet" type="text/css" href="/css/Mobile/main.css" />
    <link id="" rel="stylesheet" type="text/css" href="/css/Mobile/style-sub.css" />
    <link id="" rel="stylesheet" type="text/css" href="/css/Mobile/jstyle.css" />
    <div id="mainDiv">
        <article class="clearfix gradient">
            <nav>
                <ul>
                    <li>
                        <asp:LinkButton runat="server" OnClick="lbSeeFeatures_Click">Mobile Overview</asp:LinkButton></li>
                    <li>
                        <asp:LinkButton runat="server" OnClick="lbISeeFeatures_Click">JD iOS App</asp:LinkButton></li>
                    <li>
                        <asp:LinkButton runat="server" OnClick="lbMSeeFeatures_Click">Mobile-Optimized Site</asp:LinkButton></li>
                    <li class="current">
                        <asp:LinkButton runat="server" OnClick="lbJSeeFeatures_Click">JPix by JDate</asp:LinkButton></li>
                </ul>
            </nav>
            <header>
                <hgroup>
                    <h1 class="ir">JPix Mobile App</h1>
                    <h2>On the train commuting to work? Browse hundreds of member photos.</h2>
                </hgroup>
            </header>
            <p>
                The JPix Mobile App is a stand-alone application designed to focus on our member photos, with
					profile information available on demand.
            </p>
            <div class="stores">
                <a href="https://play.google.com/store/apps/details?id=com.spark.android.jpix" id="play-store" class="ir" target="_blank">Android App on Google Play</a>
            </div>
            <img src="../../img/Community/JDate/Mobile/jpix/android.png" alt="" width="170" height="358" border="0" id="android" />
        </article>
        <article id="content">
            <nav>
                <ul>
                    <li class="current" data-tag="the-features">Features<span></span></li>
                    <li data-tag="faq">FAQ<span></span></li>
                    <li data-tag="feedback">Feedback<span></span></li>
                </ul>
            </nav>
            <section id="the-features" class="current-tab">
                <header>
                    <div id="feature-nav">
                        <div id="nav-up" class="disabled"></div>
                        <div class="feature-nav">
                            <ul>
                                <li class="selected"><a href="#" class="member-photos ir" title="Member Photos">Member Photos</a></li>
                                <li><a href="#" class="member-profile ir" title="Member Profile">Member Profile</a></li>
                                <li><a href="#" class="smiles-favorites ir" title="Smiles and Favorites">Smiles and Favorites</a></li>
                                <li><a href="#" class="instructions ir" title="Instructions">Instructions</a></li>
                            </ul>
                        </div>
                        <div id="nav-down" class="disabled"></div>
                    </div>
                </header>
                <div id="member-photos" class="section-img enabled"></div>
                <div id="member-profile" class="section-img"></div>
                <div id="smiles-favorites" class="section-img"></div>
                <div id="instructions" class="section-img"></div>
                <div id="member-photos-content" class="featured-content enabled">
                    <div class="feature-icon"></div>
                    <h1>Member Photos</h1>
                    <p>
                        Quickly swipe through member photos with ease. You can browse
							hundreds of photos in just minutes. Want to see another member?
							Swipe horizontally. Want to see more of the current member?
							Swipe vertically.  We have hundreds of thousands of members and
							millions of photos &#8212; find love with a swipe!
                    </p>
                </div>
                <div id="member-profile-content" class="featured-content">
                    <div class="feature-icon"></div>
                    <h1>Member Profile</h1>
                    <p>
                        Just found someone you like and want to know more? Double
							click the photo (or long-tap on android) and see all profile
							information. Our members are looking for someone who shares their values,
							so take a look at their profile information and see
							if the two of you match up!
                    </p>
                </div>
                <div id="smiles-favorites-content" class="featured-content">
                    <div class="feature-icon"></div>
                    <h1>Flirt and Favorites</h1>
                    <p>
                        Make a connection by sending a Flirt or adding a member
							to your Favorites list. Flirts are fun one-liners you can send
							to let someone know you&rsquo;re interested. The Favorites list is an
							easy place for you to add members you&rsquo;d like to revisit later!
                    </p>
                </div>
                <div id="instructions-content" class="featured-content">
                    <div class="feature-icon"></div>
                    <h1>Instructions</h1>
                    <p>
                        Not sure how to view more photos or look at other members? Our
							helpful guidelines make it easy! Take a moment to read through
							our easy directions and get to know all the ways you can interact
							with Jewish singles near you by using the JPix Mobile app.
                    </p>
                </div>
            </section>
            <section id="faq">
                <h1>FAQ</h1>
                <dl>
                    <dt>What is JPix?</dt>
                    <dd>JPix by JDate is an app available for Apple and Android devices that allows you
							to browse other members&rsquo; photos and profiles in a fun, quick and easy way. Designed to be a
							fun complement to the JDate Mobile Site, JPix is meant to be a way for you to interact
							with JDate when you only have a few moments to spare.
                    </dd>
                    <dt>How does it work?</dt>
                    <dd>After logging in, you are presented with a full screen slideshow of members that fall within
							your search preferences.  From there, you can swipe horizontally to view other members, swipe
							vertically to view more of that member&rsquo;s photos, or double-tap (for Apple users) and long-tap
							(for Android users) to &ldquo;turn the card over&rdquo; and view that member&rsquo;s full profile information.
							At any time, you can send a one-tap Flirt to the member or add them to your Favorites.
                    </dd>
                    <dt>Why does JPix allow me to send Flirts and add to Favorites?</dt>
                    <dd>Because JPix was designed for members to use while on-the-go, when
							they only have a limited amount of time to spend on the app, we wanted to make communication
							as effortless and quick as possible.  This is why we provide you with one-tap Flirts.
							If you find a member you&rsquo;d like to message, you can simply tap &ldquo;Favorites&rdquo; and the member
							will be bookmarked in your Favorites list so you can interact with them later, when you have
							more time.  You can send Instant Messages, email messages and Flirts from the JDate
							Mobile Site, which can be accessed by going to m.JDate.com on your mobile device&rsquo;s web browser.
                    </dd>
                    <dt>Can I send messages to another JDate member using my phone?</dt>
                    <dd>Yes, go to m.JDate.com on your mobile phone&rsquo;s browser and login. Here you can send an
							Instant Messages, email messages and Flirts to other JDate members.
                    </dd>
                    <dt>Is JPix by JDate free?</dt>
                    <dd>Yes JPix is free to all members and can be downloaded in the <a href="https://itunes.apple.com/us/app/jpix/id660948609">Apple App
							Store</a> and <a href="https://play.google.com/store/apps/details?id=com.spark.android.jpix">Google Play Store</a>.
                    </dd>
                    <dt>Where can I send feedback on JPix?</dt>
                    <dd>We'd love to hear your feedback! You can email us at <a href="mailto:Apps@JDate.com">Apps@JDate.com</a>.
                    </dd>
            </section>
            <section id="feedback">
                <h1>Feedback</h1>
                <p>
                    <strong>Do you have a question or comment?</strong><br />
                    Give us a call at <a href="tel:+18774533861">1-877-453-3861</a>. Our customer care staff is here to help you find that<br />
                    special someone.
                </p>

                <p>
                    If you prefer, you can send us mail at the following address. Please be sure to include your full
						name, username, and email address.
                </p>
                <div class="vcard">
                    <p class="org">
                        JDate.com<p>
                    <p class="adr">
                        <span class="street-address">P.O. Box 540</span><br />
                        <span class="locality">Lehi</span>, <span class="region">UT</span> <span class="postal-code">84043</span> <span class="country-name">U.S.A.</span>
                    </p>
                </div>
            </section>
        </article>
    </div>
    <script>
        var CMMJS = {

            changeEl: function () {

                $j('#feature-nav a').click(function (e) {
                    e.preventDefault();
                    var classArr = $j(this).attr('class'),
                        theClass, theLink;
                    classArr = classArr.split(' ');
                    for (x in classArr) {
                        if (classArr[x] !== "ir") {
                            theClass = classArr[x];
                        }
                    }
                    $j('.section-img.enabled').removeClass('enabled');
                    $j("#" + theClass).addClass('enabled');
                    theLink = $j("." + theClass);
                    $j('#feature-nav li.selected').removeClass('selected');
                    theLink.parent().addClass('selected');
                    theClass = theClass + "-content";
                    theClass = $j("#" + theClass);
                    $j('.featured-content.enabled').removeClass('enabled');
                    theClass.addClass('enabled');
                })
            },
            changeTab: function () {
                $j('#content nav li').click(function (e) {
                    e.preventDefault();
                    var tagName = $j(this).attr('data-tag');
                    $j('.current-tab').removeClass('current-tab');
                    $j('#' + tagName).addClass('current-tab');
                    $j('#content nav li.current').removeClass('current');
                    $j(this).addClass('current');
                })

            },
            navUp: function () {
                var pos = parseInt($j('.feature-nav ul').css("margin-top"));
                var new_pos = pos + 72;
                if (new_pos <= 0) {
                    $j('.feature-nav ul').animate({ marginTop: new_pos + "px" }, 200);
                    $j('#nav-down').toggleClass("disabled", false);
                }
                if (new_pos == 0) {
                    $j('#nav-up').toggleClass("disabled", true);
                }
            },
            navDown: function () {
                var pos = parseInt($j('.feature-nav ul').css("margin-top"));
                var new_pos = pos - 72;
                if (new_pos >= -504) {//360){
                    $j('.feature-nav ul').animate({ marginTop: new_pos + "px" }, 200);
                    $j('#nav-up').toggleClass("disabled", false);
                }
                if (new_pos == -504) {//360){
                    $j('#nav-down').toggleClass("disabled", true);
                }
            }

        }

        $j(document).ready(function () {

            CMMJS.changeEl();
            CMMJS.changeTab();
        });

    </script>
</asp:PlaceHolder>

<asp:PlaceHolder ID="navScript" runat="server" EnableViewState="false" Visible="false">
    <script>
        $j(function () {
            $j('#nav').hide();

        });
    </script>
</asp:PlaceHolder>


<script>
    $j(function () {
        $j('#iconListLink').hide();
        $j('#topSubscriptionBanner').hide();

        var method;
        var noop = function () { };
        var methods = [
            'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
            'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
            'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
            'timeStamp', 'trace', 'warn'
        ];
        var length = methods.length;
        var console = (window.console = window.console || {});

        while (length--) {
            method = methods[length];

            // Only stub undefined methods.
            if (!console[method]) {
                console[method] = noop;
            }
        }

    });
</script>

