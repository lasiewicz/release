﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="Group.ascx.cs" Inherits="Matchnet.Web.Applications.AffiliationGroups.Group" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="cc1" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="uc1" TagName="MicroProfileStrip" Src="/Framework/Ui/BasicElements/MicroProfileStrip.ascx" %>
<script type="text/javascript">
    function showWarning()
    {
        if (confirm("Joining this group will remove your association with other groups in this category if you have any"))
            return true;
        else
            return false;
    }

</script>
<div class="group">
    <div class="group-photo"><asp:Image ID="imgLogo" runat="server" Width="176" /></div>
    <div class="group-info">
        <h1><asp:Literal ID="litGroupName" runat="server" /> <cc1:FrameworkButton ID="btnJoin" CssClass="group-join btn secondary" runat="server" ResourceConstant="BTN_JOIN" OnClientClick="return showWarning()"/></h1>
        <asp:Literal ID="litGroupMember" runat="server"  Visible="true"/>
        <div class="group-creator"><mn:Txt ID="lblContact" runat="server" ResourceConstant="TXT_CONTACT" />&nbsp;<asp:Literal ID="litContact" runat ="server" /></div>
        <div class="group-location"><mn:Txt ID="lblLocation" runat="server" ResourceConstant="TXT_LOCATION" />&nbsp;<asp:Literal ID="litLocation" runat ="server" /></div>
        <div class="group-membership"><mn:Txt ID="lblMembership" runat="server" ResourceConstant="TXT_MEMBERSHIP" />&nbsp;<asp:Literal ID="litMembership" runat ="server" /></div>
        <div class="group-subgroup"><mn:Txt ID="lblOption" runat="server"  />&nbsp;<asp:Literal ID="litOption" runat ="server" /></div>
        <div class="group-category"><mn:Txt ID="lblCategory" runat="server" ResourceConstant="TXT_CATEGORY" />&nbsp;<asp:HyperLink runat="server" ID="lnkCategory"><asp:Literal ID="litCategory" runat ="server" /></asp:HyperLink></div>
        <div class="group-contact">
            <strong><mn:Txt ID="lblContactInfo" runat="server" ResourceConstant="TXT_CONTACT_INFO" />&nbsp;<asp:Literal ID="litContactInfo" runat ="server" /></strong>
            <div class="group-address"><mn:Txt ID="txtAddress" runat="server" ResourceConstant="TXT_ADDRESS" />&nbsp;<asp:Literal ID="litAddress" runat ="server" /></div>
            <div class="group-phone"><mn:Txt ID="txtPhone" runat="server" ResourceConstant="TXT_PHONE" />&nbsp;<asp:Literal ID="litPhone" runat ="server" /></div>
            <div class="group-email"><mn:Txt ID="txtEmail" runat="server" ResourceConstant="TXT_EMAIL" />&nbsp;<asp:Literal ID="litEmail" runat ="server" /></div>
            <div class="group-website"><mn:Txt ID="txtWebsite" runat="server" ResourceConstant="TXT_WEBSITE" />&nbsp;<asp:Literal ID="litWebSite" runat ="server" /></div>
        </div>
    </div>
    <div class="group-summary">
        <h3><mn:Txt ID="lblAbout" runat="server" ResourceConstant="TXT_ABOUT" /></h3>
        <asp:Literal ID="litAbout" runat ="server" />
    </div>
</div>

<div id="group-postlogin-cta" class="clearfix">
    <h2><mn:Txt ID="txtPromoTitle" runat="server" ResourceConstant="TXT_FIND_FUN" /></h2>
    <p><mn:Txt ID="txtPromo" runat="server" ResourceConstant="TXT_FIND_FUN_TEXT" /></p>
</div>

<div id="group-members">
    <h2><asp:Literal ID="litMembers" runat="server" /></h2>
    <div class="micro-profile-strip clearfix">
        <uc1:MicroProfileStrip ID="groupMember1" runat="server" />
    </div>
    <div class="micro-profile-strip clearfix">
        <uc1:MicroProfileStrip ID="groupMember2" runat="server" />
    </div>
    <asp:HyperLink ID="lnkMore" runat="server"><mn:Txt runat="server" ID=txtMore  ResourceConstant="TXT_MORE" /></asp:HyperLink>
</div>
