﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using System.Collections.Specialized;
using System.Text;

using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Ui.BasicElements;

using Matchnet.Lib;
using Matchnet.Lib.Util;

using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects;
using Matchnet.Member.ValueObjects.Privilege;

using Matchnet.Content.ValueObjects.BrandConfig;

using Matchnet.AffiliationGroups.ServiceAdapters;
using Matchnet.AffiliationGroups.ValueObjects;
using Matchnet.Web.Applications.Home;
namespace Matchnet.Web.Applications.AffiliationGroups
{
    public partial class Group : FrameworkControl
    {

        int _groupid;
        AffiliationGroup _group=null;
        string locationformat = "{0}, {1}";
        string addressformat = "{0} {1},{2},{3} {4}";
        bool joined = false;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                _groupid = Conversion.CInt(Request["groupid"]);
             
               _group = Matchnet.AffiliationGroups.ServiceAdapters.AffiliationGroupsSA.Instance.GetAffiliationGroup(_groupid);
               if (_group == null)
               {
                   g.Transfer("/Applications/AffiliationGroups/GroupsList.aspx");
               }
                AffiliationCategorySite category = Matchnet.AffiliationGroups.ServiceAdapters.MetaDataSA.Instance.GetAffiliationCategory(_group.CategorySiteID);
               // List<AffiliationGroupMember> membergroups = AffiliationGroupMemberSA.Instance.GetMemberGroups(g.Brand.Site.SiteID, _group.CategorySiteID, g.Member.MemberID);
                if (g.Member != null)
                {
                    AffiliationMemberGroupsList membergroups = Matchnet.AffiliationGroups.ServiceAdapters.AffiliationGroupMemberSA.Instance.GetMemberGroups(g.Brand.Site.SiteID,  g.Member.MemberID);
                    if (membergroups.IfGroupMember(_group.CategorySiteID,_groupid))
                    {
                        btnJoin.Visible = false;
                        litGroupMember.Text = g.GetResource("TXT_GROUP_MEMBER", this, new string[] { _group.Name });
                    }
                }
                else
                {
                    btnJoin.Visible = false;
                }
                litGroupName.Text = _group.Name;
               
                if(!String.IsNullOrEmpty(_group.Image))
                    imgLogo.ImageUrl = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("IMG_URL") + "/" + category.ImagePath + "/" + _group.Image;
                else
                    imgLogo.ImageUrl = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("IMG_URL") + "/" + category.ImagePath + "/" + category.DefaultLogo;


                litContact.Text = _group.Head;
                litAbout.Text = _group.About;
                litLocation.Text = string.Format(locationformat, _group.City, _group.State);
                litAddress.Text = String.Format(addressformat, _group.Address1, _group.Address2, _group.City, _group.State, _group.PostalCode);
                litPhone.Text = _group.Phone;
                litEmail.Text = _group.Email;
                litWebSite.Text = _group.URL;
                litCategory.Text = category.Name;
                if (_group.MembershipCount <= 0)
                    litMembership.Text = "";
                else
                    litMembership.Text=_group.MembershipCount.ToString("N0");

                AffiliationCategoryOption option = category.Options.GetOption("Denomination");
                int valueid=_group.Options[0].AffiliationCategoryOptionValueID;
                lblOption.Text = option.Description;
                litOption.Text=  option.GetOptionValue(valueid).Description;
                lnkCategory.NavigateUrl = "/Applications/AffiliationGroups/GroupsList.aspx?categoryid=" + category.CategorySiteID.ToString();
            }
            catch (Exception ex)
            { g.ProcessException(ex); }
        }

        private void btnJoin_Click(object sender, System.EventArgs e)
        {
            try
            {
                if (g.Member != null)
                    Matchnet.AffiliationGroups.ServiceAdapters.AffiliationGroupMemberSA.Instance.AddGroupMember(g.Brand.BrandID, _group.CategorySiteID, _groupid, g.Member.MemberID);

                g.Notification.AddMessageString(g.GetResource("TXT_JOIN_SUCCESS"));
                joined = true;
                if (g.Member.GetAttributeInt(g.Brand, "HasAffiliationGroup") != 1)
                {
                    g.Member.SetAttributeInt(g.Brand, "HasAffiliationGroup", 1);
                    Member.ServiceAdapters.MemberSA.Instance.SaveMember(g.Member);
                }
            }
            catch (Exception ex)
            { g.Notification.AddErrorString(g.GetResource("TXT_JOIN_ERR"));
                g.ProcessException(ex); 
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            if (joined)
                btnJoin.Visible = false;
            List<int> groupMembers = AffiliationGroupMemberSA.Instance.GetGroupMembers(_groupid);

            
            int shift = 0;
            if (groupMembers != null && groupMembers.Count > 0)
            {
                litMembers.Visible = true;
                litMembers.Text = g.GetResource("TXT_MEET_MEMBERS", this, new string[] { _group.Name });
            
            }
          
            
          
            List<ProfileHolder> profiles = new List<ProfileHolder>();
            int start = 0;
            if (joined)
            {
                  ProfileHolder p = new ProfileHolder();
                  p.Member = g.Member;
                  profiles.Add(p);
                 
            }
            int memberid=0;

            if (g.Member != null)
                memberid = g.Member.MemberID;
           
            int finish = groupMembers.Count > 3 ? 3 : groupMembers.Count;
            for (int i = start; i < finish; i++)
            {
                ProfileHolder p = new ProfileHolder();
                if (groupMembers[i] == memberid && joined)
                {
                    finish = groupMembers.Count > 4 ? 4 : groupMembers.Count;
                    shift = 1;
                    continue;
                }
                p.Member = Member.ServiceAdapters.MemberSA.Instance.GetMember(groupMembers[i], MemberLoadFlags.None);
                profiles.Add(p);
            }

            groupMember1.MemberList = profiles;
            List<ProfileHolder> profiles2 = new List<ProfileHolder>();
            if (groupMembers.Count > 3 + shift)
            {
                if(joined)
                { start = 2; }
                else
                {  start=3; }
                start+= shift;
                finish = groupMembers.Count > 6 ? 6: groupMembers.Count;
                for (int i = start; i < finish; i++)
                {
                    if (groupMembers[i] == memberid && joined)
                    {
                        finish = groupMembers.Count > 6 + shift ? 6 + shift : groupMembers.Count;
                        continue;
                    }
                    ProfileHolder p = new ProfileHolder();
                    p.Member = Member.ServiceAdapters.MemberSA.Instance.GetMember(groupMembers[i], MemberLoadFlags.None);
                    profiles2.Add(p);
                }
            }
            groupMember2.MemberList = profiles2;
            if (groupMembers.Count > 6)
            {
                lnkMore.Visible = true;
                lnkMore.NavigateUrl = "/Applications/AffiliationGroups/GroupMembers.aspx?groupid=" + _groupid.ToString();

            }
            else
            {
                lnkMore.Visible = false;
            }
        }


        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnJoin.Click += new System.EventHandler(this.btnJoin_Click);
            this.Load += new System.EventHandler(this.Page_Load);
           // this.Init += new System.EventHandler(this.Page_Init);

        }
        #endregion
    }
}
