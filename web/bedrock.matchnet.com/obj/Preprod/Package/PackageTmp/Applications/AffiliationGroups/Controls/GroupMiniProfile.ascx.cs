﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;

using Matchnet.Web.Framework;

using Matchnet.Lib;
using Matchnet.Lib.Util;

using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects;
using Matchnet.Member.ValueObjects.Privilege;

using Matchnet.Content.ValueObjects.BrandConfig;

using Matchnet.AffiliationGroups.ServiceAdapters;
using Matchnet.AffiliationGroups.ValueObjects;

namespace Matchnet.Web.Applications.AffiliationGroups.Controls
{
    public partial class GroupMiniProfile : FrameworkControl
    {
        AffiliationGroup _group;
        int _groupid;

        public AffiliationGroup Group
        {
            get;
            set;
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
              //  G = Matchnet.AffiliationGroups.ServiceAdapters.AffiliationGroupsSA.Instance.GetAffiliationGroup(GroupID);
                AffiliationCategorySite category = Matchnet.AffiliationGroups.ServiceAdapters.MetaDataSA.Instance.GetAffiliationCategory(Group.CategorySiteID);
                lnkGroupName.Text = Group.Name;
                lnkGroupName.NavigateUrl = "/Applications/AffiliationGroups/Group.aspx?groupid=" + Group.AffiliationGroupID;
                imgLogo.ImageUrl = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("IMG_URL") + "/" + category.ImagePath + "/" + Group.Image;
               
                if(!String.IsNullOrEmpty(Group.Image))
                    imgLogo.ImageUrl = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("IMG_URL") + "/" + category.ImagePath + "/" + Group.Image;
                else
                    imgLogo.ImageUrl = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("IMG_URL") + "/" + category.ImagePath + "/" + category.DefaultLogo;

                litGroupHead.Text = Group.Head;
                litAbout.Text = Group.About;
            }
            catch (Exception ex)
            { g.ProcessException(ex); }
        }
    }
}