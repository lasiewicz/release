﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GroupsList.ascx.cs" Inherits="Matchnet.Web.Applications.AffiliationGroups.GroupsList" %>
<%@ Register TagPrefix="uc1" TagName="GroupMiniProfile" Src="/Applications/AffiliationGroups/Controls/GroupMiniProfile.ascx" %>
<%@ Register TagPrefix="uc2" TagName="TabBar" Src="/Framework/Ui/BasicElements/TabBar.ascx" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="cc1" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<input id="hidStartRow" type="hidden" name="hidStartRow" runat="server" />
<asp:PlaceHolder ID="phTopFeature" runat="server" Visible="true" >
<div class="featured-group group">
    <h1><mn:Txt ID="txtFeatureGroup" runat="server" ResourceConstant="TXT_FEATURE" /></h1>
    <div class="group-photo"><asp:Image ID="imgFeature" runat="server" /></div>
    <div class="group-info">
        <h2><asp:HyperLink ID="lnkFeaturedGroups"  runat="server"><asp:Literal ID="litGroupName" runat="server" /></asp:HyperLink></h2>
        <div class="group-summary">
            <asp:Literal ID="litAbout" runat ="server" />&nbsp;&nbsp;
            <asp:HyperLink ID="lnkMore" runat="server"><mn:Txt runat="server" ID=txtMore  ResourceConstant="TXT_MORE" /></asp:HyperLink>
        </div>
    </div>
</div>


</asp:PlaceHolder>
<asp:PlaceHolder ID="phPromo" runat="server">
<div id="group-prelogin-cta" class="clearfix">
    <asp:HyperLink ID="lnkCreateGroup" runat="server"><mn:Txt ID="txtCreate" runat="server" ResourceConstant="TXT_CREATE_GROUP" /></asp:HyperLink>  
    <h2><mn:Txt ID="txtPromoTitle" runat="server" ResourceConstant="TXT_FIND_FUN" /></h2>
    <p><mn:Txt ID="txtPromo" runat="server" ResourceConstant="TXT_FIND_FUN_TEXT" /></p>
</div>
</asp:PlaceHolder>
<div id="group-tabs">
    <div class="pagination">
        <asp:label id="lblListNavigationTop" Runat="server" />
    </div>

    <uc2:TabBar id="tabBar" runat="server" />
    <ul id="group-list">
        <asp:Repeater ID="rptGroups" runat="server" OnItemDataBound="BindGroups">
            <ItemTemplate>
             <uc1:GroupMiniProfile id="groupProfile" runat="server" />
            </ItemTemplate>
        </asp:Repeater>
    </ul>
    <div class="text-outside"><asp:HyperLink runat="server" ID="lnkSeeMore"><mn:Txt runat="server" ResourceConstant="TXT_SEE_MORE" /></asp:HyperLink></div>
    <div class="pagination" >
        <asp:label id="lblListNavigationBottom" Runat="server"></asp:label>
    </div>
</div>
