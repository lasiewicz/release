﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;

using Matchnet.Web.Framework;

using Matchnet.Lib;
using Matchnet.Lib.Util;

using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects;
using Matchnet.Member.ValueObjects.Privilege;

using Matchnet.Content.ValueObjects.BrandConfig;

using Matchnet.AffiliationGroups.ServiceAdapters;
using Matchnet.AffiliationGroups.ValueObjects;

namespace Matchnet.Web.Applications.AffiliationGroups.Controls
{
    public partial class GroupMicroProfile : FrameworkControl
    {
        AffiliationGroup _group;
        int _groupid;
        string locationformat = "{0}, {1}";
        public AffiliationGroup Group
        {
            get { return _group; }
            set { _group = value; }
        }

        public int GroupID
        {
            get{return _groupid;}
            set { _groupid = value; }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                  Group = Matchnet.AffiliationGroups.ServiceAdapters.AffiliationGroupsSA.Instance.GetAffiliationGroup(GroupID);
                AffiliationCategorySite category = Matchnet.AffiliationGroups.ServiceAdapters.MetaDataSA.Instance.GetAffiliationCategory(Group.CategorySiteID);
                if (!String.IsNullOrEmpty(Group.Image))
                    imgLogo.ImageUrl = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("IMG_URL") + "/" + category.ImagePath + "/" + Group.Image;
                else
                    imgLogo.ImageUrl = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("IMG_URL") + "/" + category.ImagePath + "/" + category.DefaultLogo;
                litLocation.Text = string.Format(locationformat, _group.City, _group.State);
                litCategory.Text = category.Name;
                lnkCategory.NavigateUrl = "/Applications/AffiliationGroups/GroupsList.aspx?categoryid=" + category.CategorySiteID.ToString();
                lnkGroupName.NavigateUrl = "/Applications/AffiliationGroups/Group.aspx?groupid=" + Group.AffiliationGroupID;
                litGroupName.Text = Group.Name;
            }
            catch (Exception ex)
            { g.ProcessException(ex); }
        }
    }
}