﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GroupMembers.ascx.cs" Inherits="Matchnet.Web.Applications.AffiliationGroups.GroupMembers" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="cc1" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="result" TagName="ResultList" Src="../../Framework/Ui/BasicElements/ResultList20.ascx" %>
<%@ Register TagPrefix="mn1" TagName="ResultsViewType" Src="/Framework/Ui/BasicElements/ResultsViewType.ascx" %>

<div id="group-member-list" class="group header-options clearfix">
	<div class="pagination">
		<asp:label id="lblListNavigationTop" Runat="server" />
	</div>
    <div class="group-tmb"><asp:Image ID="imgLogo" runat="server" Width="80" /></div>
    <div class="group-info">
        <h1><asp:Literal ID="litGroupName" runat="server" /> <cc1:FrameworkButton ID="btnJoin" runat="server" ResourceConstant="BTN_JOIN" OnClientClick="return showWarning()" /></h1>
        <div class="group-is-member><asp:Literal ID="litGroupMember" runat="server"  Visible="true"/></div>
        <div class="group-creator"><mn:Txt ID="lblContact" runat="server" ResourceConstant="TXT_CONTACT" />&nbsp;<asp:Literal ID="litContact" runat ="server" /></div>
        <div class="group-location"><mn:Txt ID="lblLocation" runat="server" ResourceConstant="TXT_LOCATION" />&nbsp;<asp:Literal ID="litLocation" runat ="server" /></div>
        <div class="group-membership"> <mn:Txt ID="lblMembership" runat="server" ResourceConstant="TXT_MEMBERSHIP" />&nbsp;<asp:Literal ID="litMembership" runat ="server" /></div>
        <div class="group-category"><mn:Txt ID="lblCategory" runat="server" ResourceConstant="TXT_CATEGORY" />&nbsp;<asp:Literal ID="litCategory" runat ="server" /></div>
    </div>
</div>    

<div class="clearfix clear-both" id="results-container">
    <ul class="nav-rounded-tabs no-click clearfix">
	    <li class="no-tab view-type">
            <mn1:ResultsViewType id="idResultsViewType" runat="server" />   
        </li>
    </ul>
    
    <asp:placeholder id="Results" runat="server" visible="true">
        <result:resultlist id="SearchResultList" runat="server" resultlistcontexttype="SearchResult"></result:resultlist>
    </asp:placeholder>

    <div class="pagination" ><asp:label id="lblListNavigationBottom" Runat="server" /></div>
</div>