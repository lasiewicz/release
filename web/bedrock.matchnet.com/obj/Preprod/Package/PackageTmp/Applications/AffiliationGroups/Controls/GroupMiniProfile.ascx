﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GroupMiniProfile.ascx.cs" Inherits="Matchnet.Web.Applications.AffiliationGroups.Controls.GroupMiniProfile" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>

<li class="clearfix">
  <div class="group-tmb">
      <asp:Image ID="imgLogo" runat="server" Width="80" />
  </div>
  <div class="group-info">
    <h2><asp:HyperLink ID="lnkGroupName" runat="server"/></h2>
    <h3 class="group-creator"><mn:Txt ID="lblContact" runat="server" ResourceConstant="TXT_CONTACT" /> <asp:Literal ID="litGroupHead" runat="server" /></h3>
	<p class="group-summary"><asp:Literal ID="litAbout" runat="server" /></p>
   </div>
</li>