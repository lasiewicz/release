﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SubmitQuestionClientModule.ascx.cs" Inherits="Matchnet.Web.Applications.QuestionAnswer.Controls.SubmitQuestionClientModule" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<% if(IsEnabled){ %>
    <script type="text/javascript">
        /* <![CDATA[ */
        $j(document).ready(function() {
            var dialogId="submitQuestion<%=Ordinal %>";
            var dialogLinkId = "submitQuestionLink<%=Ordinal %>";
            $j("#"+dialogLinkId).click(function() {
                $j("#"+dialogLinkId).attr("disabled", "disabled");
                $j.get('/Applications/QuestionAnswer/SubmitQuestionPopup.aspx?eid='+dialogId+'&EntryPoint=<%=MyEntryPoint %>', function(response) {
                    $j("#"+dialogId).html(response);
                    
                    // moved dialog event inside .get to ensure response is available
                    $j("#"+dialogId).dialog({
                        width: 730,
                        height: 440,
                        modal: true,
                        title: "<%=GetResourceForJS("TXT_SEND_QUESTION") %>",
                        dialogClass: 'ui-dialog-override send-question-overlay',
                        close: function(type, data) {
                            $j("#"+dialogLinkId).removeAttr("disabled");
                        }
                    });
                });

//                spark.tracking.addEvent("event2", true);
//                spark.tracking.addPageName("Q&A overlay", true);
//                spark.tracking.addProp(10, "<%=CurrentUrl %>", true);
//                spark.tracking.track();
            });
        });

        /* ]]> */
    </script>
    <div class="send-your-question">
        <asp:Literal ID="literalSendQuestion" runat="server" />
    </div>

    <div id="submitQuestion<%=Ordinal%>" class="hide">
        <mn:image id="mnimage5012" runat="server" CssClass="loading-gif" filename="ajax-loader.gif" />
    </div>
    <%--end send question--%>        
<% } %>