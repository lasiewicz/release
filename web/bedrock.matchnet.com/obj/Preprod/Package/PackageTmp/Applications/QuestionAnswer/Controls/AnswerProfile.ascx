﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AnswerProfile.ascx.cs" Inherits="Matchnet.Web.Applications.QuestionAnswer.Controls.AnswerProfile" %>
<%@ Register TagPrefix="uc1" TagName="NoPhoto" Src="~/Framework/Ui/PageElements/NoPhoto20.ascx" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="uc1" TagName="Add2List" Src="/Framework/Ui/BasicElements/Add2List.ascx" %>
<%@ Register TagPrefix="uc2" Src="~/Framework/Ui/BasicElements/LastTime.ascx" TagName="LastTime" %>
<%@ Register TagPrefix="qa" TagName="MiniQuestionClientModule" Src="MiniQuestionClientModule.ascx" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<div class="answer-profile clearfix">
     <asp:PlaceHolder ID="phQuestion" runat="server" Visible="false">
        <div class="question-container clearfix">
            <input type="button" runat="server" ID="btnSubmitAnswer" Visible="true" class="btn primary" />

            <span class="question"><mn:Txt ID="txtQ" runat="server" ResourceConstant="TXT_Q" /><mn:Txt ID="litQuestion" runat="server"></mn:Txt></span>
            <div id="divQuestionModule"  runat="server" class="question-dialog hide">
                <div class="qanda">
            <%-- <h2><mn:txt id="Txt1" runat="server" resourceconstant="TXT_ANSWER_HEADER" expandimagetokens="true" /></h2>
            <span class="answer-popup-close"><mn:txt id="Txt2" runat="server" resourceconstant="TXT_COMMENT_CLOSE_BOX" expandimagetokens="true" /></span>--%>
                <qa:MiniQuestionClientModule ID="ucMiniQuestionClientModule" runat="server" /></div>
            </div>
        </div>
    </asp:PlaceHolder>
	<div class="member-pic">
	    <mn:Image runat="server" ID="imgThumb" Width="54" Height="70" class="profileImageHover" Border="0" ResourceConstant="ALT_PROFILE_PICTURE" TitleResourceConstant="TTL_VIEW_MY_PROFILE" />
        <uc1:NoPhoto runat="server" ID="noPhoto" class="no-photo" Visible="False" />
        <asp:PlaceHolder ID="phColorCode" runat="server" Visible="false">
            <div class="cc-pic-tag cc-pic-tag-sm cc-pic-<%=_ColorText.ToLower() %>">
                <span>
                    <%=_ColorText %></span></div>
        </asp:PlaceHolder>
	</div>
	<div class="actions">
	    <%--online--%>
	    <asp:hyperlink id="lnkIMMe" Runat="server">
			<mn:Image id="imgChat" runat="server" filename="icon-status-online.gif" resourceconstant="ALT_ONLINE" />
		</asp:hyperlink>
		<%--offline, no link--%>
		<asp:PlaceHolder ID="phOnlineStatusOffline" runat="server" Visible="false">
			<mn:Image id="imgChatOffline" runat="server" filename="icon-status-offline.gif" resourceconstant="ALT_IM_OFFLINE" />
		</asp:PlaceHolder>
		<%--flirt--%>
	    <a id="lnkFlirtMe" Runat="server">
			<mn:image id="ImageTeaseIcon" runat="server" filename="icon-flirt.gif" ResourceConstant="ALT_TEASE_ME"></mn:image>
		</a>
		<%--ecard--%>
		<asp:hyperlink id="lnkEcardMe" Runat="server">
			<mn:image id="ImageECardIcon" runat="server" ResourceConstant="ALT_ECARDS" filename="icon-ecard.gif"></mn:image>
		</asp:hyperlink>
		
		<%--favorites--%>    
		<uc1:Add2List id="add2List" runat="server" ClearIconInlineAlignments="true" Orientation="IconOnly" IncludeSpanAroundDescriptionText="true" DisplayLargeIcon="false" IconHotList="icon-hotlist-add.gif" IconHotListRemove="icon-hotlist-remove.gif" ></uc1:Add2List>

    </div>
     

    
    <div class="answer-container">
  
	        <div class="answer-body">
	   
		    <div class="member-answer"><asp:Literal ID="literalMemberAnswer" runat="server"></asp:Literal></div>
	    </div>
	    <div class="member-info">
	            <span class="member-info-cont"><span class="member-name"><asp:HyperLink runat="server" ID="lnkUserName1" /></span><span class="member-values">,
	            <asp:Literal ID="literalAge" runat="server"></asp:Literal>,
	            <asp:Literal ID="literalSeeking" runat="server"></asp:Literal>,
	            <asp:Literal ID="literalLocation" runat="server"></asp:Literal> </span>
	             &#9642; <span class="timestamp"> <uc2:LastTime ID="lastTimeAction" runat="server" /></span>
	        </span>
	        <span class="answer-info"> 
	            <% if (IsCommentsEnabled && IsLoggedIn){ %><label class="link-label" for="comment-<%=AnswerId%>-<%=MemberId%>"><mn:txt id="mntxt2513" runat="server" resourceconstant="TXT_COMMENT_LABEL" expandimagetokens="true" /></label><% } %> 
	            <% if (IsMemberLikeEnabled && IsLoggedIn) { %> <span onclick="spark.qanda.LikeAnswer(this,<%=MemberLikeId%>,<%=AnswerId%>,{QuestionId:<%=QuestionId %>,Event:'event51,event52',Overwrite:true,Props:[{Num:38,Value:'<%=Location %>'}]})"> &#9642;
	                <asp:Literal ID="likeTxt" visible="false" runat="server"/>
	            </span>
	            <% } %>
	        </span>
	        
	    </div>
	    <% if (IsCommentsEnabled && IsLoggedIn) { %>
	    <div class="member-comment hide">
	        <h2><mn:txt id="mntxt9377" runat="server" resourceconstant="TXT_ANSWER_COMMENT_HEADER" expandimagetokens="true" /></h2>
	        <span class="close"><mn:txt id="mntxt5239" runat="server" resourceconstant="TXT_COMMENT_CLOSE_BOX" expandimagetokens="true" /></span>
	        <textarea id="comment-<%=AnswerId%>-<%=MemberId%>" rows="2" cols="70" class="watermark"></textarea>
	        <input type="submit" id="<%=AnswerId%>-<%=MemberId%>" name="sendComment" class="btn secondary commentSubmit" value="<%=g.GetResource("TXT_COMMENT_LABEL",this)%>" />
	    </div>
	    <% } %>
	    <a id="lnkEmailMe" Runat="server" class="email" style="display:none">
		    <mn:image id="ImageEmailMe" runat="server" resourceconstant="ALT_EMAIL" TitleResourceConstant="ALT_EMAIL" filename="btn-email-sm.png"></mn:image>
	    </a>
    </div>
</div>
