﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="QuestionsList.ascx.cs" Inherits="Matchnet.Web.Applications.QuestionAnswer.Controls.QuestionsList" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mnbe" Namespace="Matchnet.Web.Framework.Ui.BasicElements"%>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>

<div class="sort-display clearfix">
	<div class="sort-options">
        <mn:Txt ID="txtSort" runat="server"  ResourceConstant="TXT_SORT" />
        <span class="link <asp:Literal id=litClassSortRecent runat=server />"><asp:HyperLink ID="lnkSortRecent" runat="server" NavigateUrl="/Applications/QuestionAnswer/Home.aspx?TabId=3&sort=recent"><mn:Txt runat="server" ID="txtSortRecent" ResourceConstant="TXT_SORT_RECENT" /></asp:HyperLink></span> <span class="divider">|</span>
        <span class="link <asp:Literal id=litClassSortAZ runat=server />"><asp:HyperLink ID="lnkSortAZ" runat="server"  NavigateUrl="/Applications/QuestionAnswer/Home.aspx?TabId=3&sort=az"><mn:Txt runat="server" ID="txtSortAZ" ResourceConstant="TXT_SORT_AZ" /></asp:HyperLink></span>
    </div>
   <div class="paging"><asp:label id="lblListNavigationTop" Runat="server" /></div>
</div>

<ul class="q-all">
   <asp:Repeater ID="rptQuestions" runat ="server">
     <ItemTemplate>
     <li><mn:Txt ID="txtQ" runat="server" ResourceConstant="TXT_Q" /> 
     <asp:HyperLink ID="lnkQuestionTab" runat="server"><mn:Txt ID="txtQuestion" runat="server" /></asp:HyperLink>
     </li>
     </ItemTemplate>
     </asp:Repeater>
</ul>
     
<div class="sort-display lower clearfix">
	<div class="sort-options">
        <mn:Txt ID="txt3" runat="server"  ResourceConstant="TXT_SORT" />
        <span class="link <asp:Literal id=litClassSortRecent1 runat=server />"><asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="/Applications/QuestionAnswer/Home.aspx?TabId=2&sort=recent"><mn:Txt runat="server" ID="txt1" ResourceConstant="TXT_SORT_RECENT" /></asp:HyperLink></span> <span class="divider">|</span> 
        <span class="link <asp:Literal id=litClassSortAZ1 runat=server />"><asp:HyperLink ID="HyperLink2" runat="server"  NavigateUrl="/Applications/QuestionAnswer/Home.aspx?TabId=2&sort=az"><mn:Txt runat="server" ID="txt2" ResourceConstant="TXT_SORT_AZ" /></asp:HyperLink></span>
    </div>
    <div class="pagination"><asp:label id="lblListNavigationBottom" Runat="server" /></div>
</div>