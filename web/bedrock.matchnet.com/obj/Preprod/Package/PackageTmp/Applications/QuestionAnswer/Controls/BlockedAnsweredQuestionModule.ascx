﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BlockedAnsweredQuestionModule.ascx.cs" Inherits="Matchnet.Web.Applications.QuestionAnswer.Controls.BlockedAnsweredQuestionModule" %>
<%@ Register TagPrefix="uc1" TagName="NoPhoto" Src="~/Framework/Ui/PageElements/NoPhoto20.ascx" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="uc2" Src="~/Framework/Ui/BasicElements/LastTime.ascx" TagName="LastTime" %>
<%@ Register TagPrefix="qa" TagName="WatermarkTextbox" Src="~/Framework/Ui/FormElements/WatermarkTextbox.ascx" %>

<script type="text/javascript">
    //<![CDATA[
    (function() {
        __addToNamespace__('spark.qanda', {
            clickOnce:{},
            txtCommentAnswerHeading:'<%=GetResourceForJS("TXT_ANSWER_COMMENT_HEADER")%>',
            txtQUESTIONANSWEREDAPPROVAL:'<%=GetResourceForJS("TXT_QUESTIONANSWEREDAPPROVAL")%>',
            txtEDIT:'<%=GetResourceForJS("TXT_REMOVE")%>',
            txtQ:'<%=GetResourceForJS("TXT_Q")%>',
            ProfileMemberId:<%=ProfileMemberId %>,
            LoggedInMemberId:<%=LoggedInMemberId %>,
            EntryPoint:'<%=EntryPoint%>',
            TrackingParam:'<%=TrackingParam%>',
            IsSite20Enabled:<%=g.IsSite20Enabled.ToString().ToLower() %>,
            ValidateQAnswer: function(getVal, errorId) {
                var error = false;
                var answerText = getVal();
                
                if (!answerText || answerText == '<%=_WatermarkText%>' || answerText == '') {
                    error = true;
                }

                if (error == true) {
                    $j('#'+errorId).css('display', 'block');
                    return false;
                }
                else {
                    spark.tracking.addEvent("event51", true);
                    spark.tracking.addProp(38, "Answer-Another Member Profile", true);
                    spark.tracking.track();
                    var elementId=$j('#'+errorId).parents(".double-q-answer").attr("id");
                    var myFunc=function() {                        
                        var result = spark.qanda.AnswerBlockedQuestion(elementId, answerText, spark.qanda.ProfileMemberId,spark.qanda.LoggedInMemberId,'block-answer-list');
                        if(result) {
                            try
                            {
                            <%if (!String.IsNullOrEmpty(RunJSOnAnswerPostSuccess)){ %>
                                <%=RunJSOnAnswerPostSuccess%>
                            <%}else { %>
                                //refresh ads on successful submit
                                var obj = (tabs && tabs.getDisplayedSlide) ? tabs.getDisplayedSlide():{}; 
                                try {
                                    if (obj.topAdSrc && obj.topAdSrc.length > 0 && updateTop728by90SR && topAd728x90IFrameID) {
                                            updateTop728by90SR(topAd728x90IFrameID, obj.topAdSrc, obj.isGAM);
                                    }
                                } catch (e) {}
                                try {
                                    if (obj.rightAdSrc && obj.rightAdSrc.length > 0 && updateRight300by250SR && rightAd300x250IFrameID) {
                                            updateRight300by250SR(rightAd300x250IFrameID, obj.rightAdSrc, obj.isGAM)
                                    }
                                } catch (e) { }
                            <%} %>
                            }
                            catch (e) {}
                        }
                        return result;
                    }
                    var success=spark.qanda.ajaxCallWithBlock(elementId,{ 
                          message: '<%=GetResourceForJS("HTML_BLOCK_ELEMENT_MSG") %>',
                          centerY: false,
                          css: {backgroundColor: 'transparent', border: 'none', top:'10%'},
                          overlayCSS: {backgroundColor: '#fff', opacity: 0.8 }
                    },myFunc,'/Applications/API/QuestionAnswerService.asmx/AnswerBlockedQuestion');                                
                    var display = ((success+''=='true')?'none':'block');
                    $j('#'+errorId).css('display', display);
                    return false;
                }
            },            
            __init__: function() { 
            }        
       });
    })();
    
    $j(document).ready(function() {
        $j(".answer-yours textarea, .answer-yours input[type=radio]").click(function(evt) {
            var key=$j(this).attr("id");
            if (!spark.qanda.clickOnce[key]) {
                spark.tracking.addEvent("event50", true);
                spark.tracking.addProp(38, "Answer-Another Member Profile", true);
                spark.tracking.track();
                spark.qanda.clickOnce[key] = true;
            }
        });
    });   
    //]]>    
</script>
<asp:PlaceHolder ID="phClientPagintation" runat="server" Visible="false">
    <script type="text/javascript">
        //<![CDATA[
        $j(document).ready(function() {
            if($j(".double-q-answer").size() > <%=PageSize %>) {        
                __addToNamespace__('spark.paging', {
                    blockedAnswerPagingObj : new spark.paging.Pages($j(".double-q-answer"), <%=PageSize %>, <%=ChapterSize %>, {next: '<%=GetResourceForJS("TXT_NEXT", null)%>',prev: '<%=GetResourceForJS("TXT_PREVIOUS", null)%>'}, {location:'profile answer only'}),
                    __init__:function() {
                        spark.paging.blockedAnswerPagingObj.getPagingText({ name: 'spark.paging.blockedAnswerPagingObj', elemIds: ['topPaging2'] });
                        $j("#blockedAnswerHeader").addClass("with-paging");
                    }
                });                
            }else {
                $j("#blockedAnswerHeader").removeClass("with-paging");
            }
        });   
        //]]>    
    </script>
</asp:PlaceHolder>

<asp:PlaceHolder ID="phHeader" runat="server" Visible="false">
    <div id="blockedAnswerHeader" class="double-q-header clearfix">
        <h2><asp:Literal ID="numMatchedQuestions" runat="server"/></h2><div id="topPaging2" class="paging"></div>
        <p><asp:Literal ID="instructions" runat="server"/></p>
    </div>        
</asp:PlaceHolder>
<span id="block-answer-list">
    <asp:Repeater ID="repeaterDoubleQandAOneAnswered" runat="server" OnItemDataBound="repeaterDoubleQandAOneAnswered_ItemDataBound" Visible="false">
        <ItemTemplate>
        <div id="qid-<asp:Literal id='questionId' runat='server'/>" class="double-q-answer clearfix">
            <h2><mn2:FrameworkLiteral ID="literalQ2" runat="server" ResourceConstant="TXT_Q"></mn2:FrameworkLiteral>
                <asp:HyperLink ID="lnkQuestion2" NavigateUrl="/Applications/QuestionAnswer/Question.aspx" runat="server">
                    <asp:Literal ID="literalQuestion2" runat="server"></asp:Literal>
                </asp:HyperLink>
            </h2>
            <div class="answer-profile clearfix">
	            <div class="member-pic">
	                <mn:Image runat="server" ID="imgThumb" Width="54" Height="70" class="profileImageHover" Border="0" ResourceConstant="ALT_PROFILE_PICTURE" TitleResourceConstant="TTL_VIEW_MY_PROFILE" />
                    <uc1:NoPhoto runat="server" ID="noPhoto" class="no-photo" Visible="False" />
                    <asp:PlaceHolder ID="phColorCode" runat="server" Visible="false">
                        <div class="cc-pic-tag cc-pic-tag-sm cc-pic-<%=_ColorText.ToLower() %>"><span><%=_ColorText %></span></div>
                    </asp:PlaceHolder>
	            </div>            
                <div class="answer-container">
		            <div class="answer-body">
		                <div class="member-answer messaging"><mn2:FrameworkLiteral ID="literalBlockedQuestion" runat="server" ResourceConstant="TXT_BLOCKED_ANSWER"></mn2:FrameworkLiteral></div>
	                </div>
	            </div>
                <div class="answer-yours clearfix">
		                <qa:WatermarkTextbox ID="txtAnswer1" runat="server" TextMode="MultiLine" CssClass="filled" WatermarkCssClass="watermark" MaxLength="3000" WatermarkTextResourceConstant="TXT_QUESTIONANSWER_WATERMARK" Visible="false"></qa:WatermarkTextbox>
	                    <ul id="rblStockAnswers" class="radio-list clearfix answer-body" runat="server" Visible="false" />
	                    <div class="answer-links"><mn2:FrameworkButton ID="btnSubmitQuestion" runat="server" CssClass="btn secondary wide" ResourceConstant="TXT_ANSWER_QUESTION" /></div>
		                <div id="divAnswerError<%# DataBinder.Eval(Container.DataItem, "QuestionID")%>" style="display:none;" class="notification error"><mn:image ID="imgAnswerError" filename="page_error.gif" runat="server" /> <mn2:FrameworkLiteral ID="literalAnswerError" runat="server" ResourceConstant="TXT_EMPTY_ANSWER_ERROR"></mn2:FrameworkLiteral></div>
	            </div>
	        </div>
        </div><!-- end double-q-answer -->
        </ItemTemplate>
    </asp:Repeater>
</span>