﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MiniQuestionModule.ascx.cs" Inherits="Matchnet.Web.Applications.QuestionAnswer.Controls.MiniQuestionModule" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mnl" Namespace="Matchnet.Web.Framework.Ui.BasicElements.Links"
    Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mnbe" Namespace="Matchnet.Web.Framework.Ui.BasicElements"
    Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="qa" TagName="WatermarkTextbox" Src="~/Framework/Ui/FormElements/WatermarkTextbox.ascx" %>
<%@ Register TagPrefix="ml" TagName="MemberLikeAnswerClientModule" Src="~/Applications/MemberLike/MemberLikeAnswerClientModule.ascx" %>

<script type="text/javascript">
    //<![CDATA[
    (function() {
        __addToNamespace__('spark.qanda', {
            BrandId:<%=g.Brand.BrandID %>,
            txtViewMyProfile: '<%=GetResourceForJS("TXT_VIEW_MY_PROFILE") %>',
            IsSite20Enabled:<%=g.IsSite20Enabled.ToString().ToLower() %>,
            txtOneAnswerNumberText:'<%=GetResourceForJS("TXT_ONE_ANSWER") %>',
            txtSendToFriend:'<%=GetResourceForJS("TXT_SENDTOFRIEND") %>',
            txtNextQuestion:'<%=GetResourceForJS("TXT_NEXT_QUESTION") %>',
            txtAnswerNumberText:'<%=GetResourceForJS("TXT_ANSWERS") %>',
            txtAnswerError:'<%=GetResourceForJS("TXT_EMPTY_ANSWER_ERROR") %>',
            txtNoAnswers:'<%=GetResourceForJS("TXT_NO_ANSWERS") %>',
            txtQuestionHeaderText:'<%=GetResourceForJS("TXT_Q") %>',
            txtAnswerHeaderText:'<%=GetResourceForJS("TXT_A") %>',
            txtAnswerButtonText:'<%=GetResourceForJS("TXT_ANSWER_QUESTION") %>',
            txtAnswerErrorImageUrl:'<%=imgAnswerError.ImageUrl %>',
            txtWaterMarkText:'<%=_WatermarkText%>',            
            AddWaterMark:function(elemId, text) {
                 $j("#"+elemId).watermark(text , { useNative: false });
            },
            ValidateQAnswer:function (elemId) {
                var error = false;
                var answerText = jQuery.trim($j("#"+elemId).val());
                
                if (answerText == spark.qanda.txtWaterMarkText || answerText == '') {
                    error = true;
                }

                if (error == true) {
                    $j('#divAnswerError').css('display', 'block');
                    return false;
                }
                else {
                    spark.tracking.addEvent("event51", true);
                    spark.tracking.addProp(38, "Answer-Home Page", true);
                    spark.tracking.track();
                    return true;
                }
            },
            __init__: function() {}
       });
    })();
    //]]>
</script>

<% if (IsQandaNextQuestionEnabled()){ %>
<script type="text/javascript" src="/Applications/QuestionAnswer/Controls/qanda.js"></script>
<script type="text/javascript">
    //<![CDATA[
    (function() {
        __addToNamespace__('spark.qanda', {
            __init__: function() {
                if (!$j.render) {
                    $j("head").append("<script type=\"text/javascript\" src=\"/javascript20/library/jquery.tmpl.js\"><"+ "/script>");
                }
                if (!spark || !spark.qanda || !spark.qanda.GetQuestion) {
                    $j("head").append("<script type=\"text/javascript\" src=\"/Applications/QuestionAnswer/Controls/qanda.js\"><"+ "/script>");
                }
            }
       });
    })();

    $j(document).ready(function() {
        $j("a.next").live('click', function(evt) {
            var myFunc = function() {
                var sortVals=(null != $j("#miniAnswerSort").val()) ? $j("#miniAnswerSort").val().split("|") : ["gender","false"];                
                spark.qanda.GetNextQuestion({hiddenElemId:'<%=hidQuestionID.ClientID %>',questionElemId:'mini-q-module',answerListElemId:'answers-list',answerModuleElemId:'mini-a-module',answerNumElemId:'answerNum',answerNumTxtElemId:'answerNumTxt',memberId:<%=MemberId%>,pageSize:<%=PageSize%>,startIdx:1,sortBy:sortVals[0],reverse:sortVals[1],outtaQuestionRedirect:true});
                //refresh ads
                refreshTop728by90SR();
                refreshRight300by250SR()
            };
            spark.qanda.ajaxCallWithBlock('mini-q-module',{ 
                  message: '<%=GetResourceForJS("HTML_BLOCK_ELEMENT_MSG") %>',
                  centerY: false,
                  css: {backgroundColor: 'transparent', border: 'none', top:'10%'},
                  overlayCSS: {backgroundColor: '#fff', opacity: 0.8 }
            },myFunc,'/Applications/API/QuestionAnswerService.asmx/GetQuestion');
            evt.stopPropagation();
            evt.preventDefault();
            return false;
        });
    });
    //]]>
</script>
<% } %>    

<dl id="mini-q-module" class="mini-q-module clearfix">

    <asp:PlaceHolder ID="phConfirmationForNewQuestion" runat="server" Visible="false">
        <div class="notification">
            <%--This feature is not available yet, but this will be a placeholder for it. ER-385. Also, while this copy should be in a resource, that link needs to be dynamic to link to the appropriate question page. Maybe break it up into two resources, though that takes away from flexibility in the copy. --%>
            Thanks for answering! While your post is being reviewed, <a href="#">see what other people are saying.</a>
        </div>
    </asp:PlaceHolder>

	<dt class="question">
		<mn2:FrameworkLiteral ID="literalA" runat="server" ResourceConstant="TXT_Q"></mn2:FrameworkLiteral> <asp:HyperLink ID="lnkQuestion" runat="server"><asp:Literal ID="literalQuestionText" runat="server"></asp:Literal></asp:HyperLink>
	</dt>

	<asp:PlaceHolder ID="phNotAnswered" runat="server">
	    <!--member did not answer-->
	    <dd class="answer">
		    <mn2:FrameworkLiteral ID="FrameworkLiteral1" runat="server" ResourceConstant="TXT_A"></mn2:FrameworkLiteral>
		    <div id="answer-input" class="answer-input">
		        <qa:WatermarkTextbox ID="txtAnswer1" runat="server" TextMode="MultiLine" CssClass="filled" WatermarkCssClass="watermark" MaxLength="3000" WatermarkTextResourceConstant="TXT_QUESTIONANSWER_WATERMARK" Visible="false"></qa:WatermarkTextbox>
	            <ul id="rblStockAnswers" class="radio-list clearfix answer-body" runat="server" Visible="false" />
	            <div class="answer-links">
			        <asp:HyperLink ID="lnkSendToFriend" runat="server" NavigateUrl="/Applications/SendToFriend/SendToFriendQandA.aspx" CssClass="stf"><mn2:FrameworkLiteral ID="literalSendToFriend" runat="server" ResourceConstant="TXT_SENDTOFRIEND"></mn2:FrameworkLiteral></asp:HyperLink> 
			        <asp:HyperLink ID="lnkNextQuestion" runat="server" NavigateUrl="/Applications/Home/default.aspx" CssClass="next"><mn2:FrameworkLiteral ID="FrameworkLiteral4" runat="server" ResourceConstant="TXT_NEXT_QUESTION"></mn2:FrameworkLiteral></asp:HyperLink>
	                <mn2:FrameworkButton ID="btnSubmitQuestion" runat="server" CssClass="btn secondary wide" ResourceConstant="TXT_ANSWER_QUESTION" />
	            </div>
		        <div id="divAnswerError" style="display:none;" class="notification error"><mn:image ID="imgAnswerError" filename="page_error.gif" runat="server" /> <mn2:FrameworkLiteral ID="literalAnswerError" runat="server" ResourceConstant="TXT_EMPTY_ANSWER_ERROR"></mn2:FrameworkLiteral></div>
			</div>
	    </dd>
	</asp:PlaceHolder>
	
	<asp:PlaceHolder ID="phAnswered" runat="server">
	    <!--member already answered-->
	    <dd class="answer">
			<span class="a answered"><mn2:FrameworkLiteral ID="FrameworkLiteral2" runat="server" ResourceConstant="TXT_A"></mn2:FrameworkLiteral></span>
			<div class="member-answer clearfix">
			    <asp:Literal ID="literalMemberAnswer" runat="server" Visible="false"></asp:Literal>
			    <ul id="rblMemberAnswers" class="radio-list disabled clearfix answer-body" runat="server" Visible="false" />
			</div>
            <asp:PlaceHolder ID="phApprovalNotification" runat="server" Visible="false">
                <div class="notification">
                    <mn:Image ID="imgAnsweredNotification" runat="server" FileName="icon-page-message.gif" />
                    <mn2:FrameworkLiteral ID="literalQuestionAnsweredApproval" runat="server" ResourceConstant="TXT_QUESTIONANSWEREDAPPROVAL"></mn2:FrameworkLiteral>
                </div>
            </asp:PlaceHolder>            
            <div class="answer-links clearfix">
		        <asp:HyperLink ID="lnkSendToFriend2" runat="server" NavigateUrl="/Applications/SendToFriend/SendToFriendQandA.aspx" CssClass="stf"><mn2:FrameworkLiteral ID="literalSendToFriend2" runat="server" ResourceConstant="TXT_SENDTOFRIEND"></mn2:FrameworkLiteral></asp:HyperLink> 
                <asp:HyperLink ID="lnkNextQuestion2" runat="server" NavigateUrl="/Applications/QuestionAnswer/Question.aspx" CssClass="next"><mn2:FrameworkLiteral ID="FrameworkLiteral3" runat="server" ResourceConstant="TXT_NEXT_QUESTION"></mn2:FrameworkLiteral></asp:HyperLink> 
                <ml:MemberLikeAnswerClientModule ID="memberLikeClientModule" runat="server" Enabled="true"/>
		    </div>            
		</dd>
<% if(!rblMemberAnswers.Visible) { %>
<script type="text/javascript">
  $j("#mini-q-module .member-answer", $j("#qanda")).expander({
    slicePoint: 190,
    expandPrefix: ' ',
    expandText: '[&hellip;]',
    userCollapseText: '[^]'
  });
</script>
<% } %>
	</asp:PlaceHolder>
	
	<asp:HiddenField ID="hidQuestionID" runat="server" />
</dl>