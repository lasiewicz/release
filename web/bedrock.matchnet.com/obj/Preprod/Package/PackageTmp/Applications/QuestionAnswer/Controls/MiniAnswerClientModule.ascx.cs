﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Ui;
using Matchnet.MemberLike.ValueObjects;
using Matchnet.Web.Applications.MemberLike;

namespace Matchnet.Web.Applications.QuestionAnswer.Controls
{
    public partial class MiniAnswerClientModule : FrameworkControl
    {
        protected string _WatermarkText = "";
        private int _questionId = 0;
        private BreadCrumbHelper.EntryPoint _entryPoint = BreadCrumbHelper.EntryPoint.HomePage;
        private string _lastSort = null;

        public bool IsCommentsEnabled
        {
            get { return QuestionAnswerHelper.IsQuestionAnswerCommentEnabled(g.Brand); }
        }

        public bool IsSortEnabled
        {
            get { return QuestionAnswerHelper.IsQuestionAnswerSortEnabled(g.Brand); }
        }

        public bool IsLoggedIn
        {
            get { return null != g.Member && g.Member.MemberID > 0; }
        }

        public bool IsMemberLikeEnabled
        {
            get { return MemberLikeHelper.Instance.IsMemberLikeEnabled(g.Brand); }
        }

        public string ClickableElementId { get; set; }
        public string ElementId { get; set; }
        public string TrackingParam { get; set; }
        public int PageSize { get; set; }
        public int TotalAnswersNum { get; set; }
        public BreadCrumbHelper.EntryPoint EntryPoint {
            get { return _entryPoint; }
            set { _entryPoint = value; }
        }
        
        public int QuestionId {
            get { return _questionId; }
            set { _questionId = value; }
        }

        public int MemberId
        {
            get
            {
                if (g.Member != null && g.Member.MemberID > 0)
                {
                    return g.Member.MemberID;
                }
                return 0;
            }
        }

        public string MemberUserName
        {
            get
            {
                if (g.Member != null && g.Member.MemberID > 0)
                {
                    return g.Member.GetUserName(g.Brand);
                }
                return string.Empty;
            }
        }

        public bool IsPayingMember
        {
            get
            {
                if (g.Member != null && g.Member.MemberID > 0)
                {
                    return g.Member.IsPayingMember(g.Brand.Site.SiteID);
                }
                return false;
            }
        }

        public string GetResourceForJS(string resConst)
        {
            return FrameworkGlobals.JavaScriptEncode(g.GetResource(resConst,new string [0],true,this));
        }

        public string GetRedirectUrl(int prtId)
        {
            string redirectUrl = string.Empty;
            if (g.Member != null && g.Member.MemberID > 0)
            {
                if (!g.Member.IsPayingMember(g.Brand.Site.SiteID))
                {
                    Matchnet.Content.ServiceAdapters.Links.ParameterCollection parameterCollection = new Matchnet.Content.ServiceAdapters.Links.ParameterCollection();
                    if (_g.ImpersonateContext)
                    {
                        parameterCollection.Add(WebConstants.IMPERSONATE_MEMBERID, _g.TargetMemberID.ToString());
                        parameterCollection.Add(WebConstants.IMPERSONATE_BRANDID, _g.TargetBrandID.ToString());
                    }
                    redirectUrl = Framework.Util.Redirect.GetSubscriptionUrl(_g.Brand, prtId, g.Member.MemberID, false, HttpContext.Current.Server.UrlEncode(HttpContext.Current.Items["FullURL"].ToString()), parameterCollection);
                }
            }
            return redirectUrl;
        }

        public string LastSort
        {
            get
            {
                if (string.IsNullOrEmpty(_lastSort))
                {
                    try
                    {
                        _lastSort = QuestionAnswerHelper.GetLastSortForMember(g.Brand, g.Member);
                    }
                    catch (Exception ex)
                    {
                        g.ProcessException(ex);
                    }
                }
                return _lastSort;
            }
        }        

        protected void Page_Load(object sender, EventArgs e)
        {
            //Control c =this.LoadControl("~/Framework/Ui/FormElements/WatermarkTextbox.ascx");
            //_WatermarkText = g.GetResource("TXT_QUESTIONANSWER_WATERMARK", c).Trim();
            lnkQuestion.NavigateUrl = "/Applications/QuestionAnswer/Question.aspx?" + WebConstants.URL_PARAMETER_NAME_QUESTION_ID + "=" + QuestionId;
        }
    }
}
