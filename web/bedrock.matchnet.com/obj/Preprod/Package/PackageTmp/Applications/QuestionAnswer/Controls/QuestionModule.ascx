﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="QuestionModule.ascx.cs" Inherits="Matchnet.Web.Applications.QuestionAnswer.Controls.QuestionModule" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mnl" Namespace="Matchnet.Web.Framework.Ui.BasicElements.Links"
    Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mnbe" Namespace="Matchnet.Web.Framework.Ui.BasicElements"
    Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="qa" TagName="WatermarkTextbox" Src="~/Framework/Ui/FormElements/WatermarkTextbox.ascx" %>
<%@ Register TagPrefix="ml" TagName="MemberLikeAnswerClientModule" Src="~/Applications/MemberLike/MemberLikeAnswerClientModule.ascx" %>

<dl id="q-module" class="q-module clearfix">

    <asp:PlaceHolder ID="phConfirmationForNewQuestion" runat="server" Visible="false">
        <div class="notification">
            <%--This feature is not available yet, but this will be a placeholder for it. ER-385. Also, while this copy should be in a resource, that link needs to be dynamic to link to the appropriate question page. Maybe break it up into two resources, though that takes away from flexibility in the copy. --%>
            Thanks for answering! While your post is being reviewed, <a href="#">see what other people are saying.</a>
        </div>
    </asp:PlaceHolder>
    
	<dt class="question">
		<mn2:FrameworkLiteral ID="FrameworkLiteral2" runat="server" ResourceConstant="TXT_Q"></mn2:FrameworkLiteral> <asp:HyperLink ID="lnkQuestion" runat="server"><asp:Literal ID="literalQuestionText" runat="server"></asp:Literal></asp:HyperLink>
	</dt>
	
	<asp:PlaceHolder ID="phNotAnswered" visible="false" runat="server">
	    <!--member did not answer-->
	    <dd class="answer">
		    <mn2:FrameworkLiteral ID="literalA" runat="server" ResourceConstant="TXT_A"></mn2:FrameworkLiteral>
		    <div id="answer-input" class="answer-input">
		        <div id="divAnswerError" style="display:none;" class="notification error"><mn:image ID="imgAnswerError" filename="page_error.gif" runat="server" /> <mn2:FrameworkLiteral ID="literalAnswerError" runat="server" ResourceConstant="TXT_EMPTY_ANSWER_ERROR"></mn2:FrameworkLiteral></div>
	            <qa:WatermarkTextbox ID="txtAnswer1" runat="server" TextMode="MultiLine" CssClass="filled" WatermarkCssClass="watermark" MaxLength="3000" WatermarkTextResourceConstant="TXT_QUESTIONANSWER_WATERMARK" Visible="false"></qa:WatermarkTextbox>
	            <ul id="rblStockAnswers" class="radio-list clearfix answer-body" runat="server" Visible="false" />
			    <div class="answer-links clearfix">
			        <asp:HyperLink ID="lnkSendToFriend" runat="server" NavigateUrl="/Applications/SendToFriend/SendToFriendQandA.aspx" CssClass="stf"><mn2:FrameworkLiteral ID="literalSendToFriend" runat="server" ResourceConstant="TXT_SENDTOFRIEND"></mn2:FrameworkLiteral></asp:HyperLink> 
			        <asp:HyperLink ID="lnkNextQuestion" runat="server" NavigateUrl="/Applications/QuestionAnswer/Question.aspx" CssClass="next"><mn2:FrameworkLiteral ID="FrameworkLiteral4" runat="server" ResourceConstant="TXT_NEXT_QUESTION"></mn2:FrameworkLiteral></asp:HyperLink> 
			        <mn2:FrameworkButton ID="btnSubmitQuestion" runat="server" CssClass="btn secondary wide" ResourceConstant="TXT_ANSWER_QUESTION" OnClientClick="return ValidateQAnswer();"/>
			    </div>
			</div>
	    </dd>
	</asp:PlaceHolder>
	
	<asp:PlaceHolder ID="phAnswered" runat="server">
	    <!--member already answered-->
	    <dd class="answer">
            <!-- note that span.a gets an additional class once the question has been answered. -->
			<span class="a answered"><mn2:FrameworkLiteral ID="FrameworkLiteral1" runat="server" ResourceConstant="TXT_A"></mn2:FrameworkLiteral></span>
			<div class="member-answer clearfix">
			    <asp:Literal ID="literalMemberAnswer" runat="server" Visible="false"></asp:Literal>
			    <ul id="rblMemberAnswers" class="radio-list disabled clearfix answer-body" runat="server" Visible="false" />
			    <asp:HyperLink CssClass="edit" ID="lnkEditQuestion" runat="server"><mn2:FrameworkLiteral ID="literalEditQuestion" runat="server" ResourceConstant="TXT_EDIT_ANSWER"></mn2:FrameworkLiteral></asp:HyperLink>
			</div>
            <asp:PlaceHolder ID="phApprovalNotification" runat="server" Visible="false">
                <div class="notification">
                    <mn:Image ID="imgAnsweredNotification" runat="server" FileName="icon-page-message.gif" />
                    <mn2:FrameworkLiteral ID="literalQuestionAnsweredApproval" runat="server" ResourceConstant="TXT_QUESTIONANSWEREDAPPROVAL"></mn2:FrameworkLiteral>
                </div>
            </asp:PlaceHolder>
            <div class="answer-links clearfix">
		        <asp:HyperLink ID="lnkSendToFriend2" runat="server" NavigateUrl="/Applications/SendToFriend/SendToFriendQandA.aspx" CssClass="stf"><mn2:FrameworkLiteral ID="literalSendToFriend2" runat="server" ResourceConstant="TXT_SENDTOFRIEND"></mn2:FrameworkLiteral></asp:HyperLink> 
                <asp:HyperLink ID="lnkNextQuestion2" runat="server" NavigateUrl="/Applications/QuestionAnswer/Question.aspx" CssClass="next"><mn2:FrameworkLiteral ID="FrameworkLiteral3" runat="server" ResourceConstant="TXT_NEXT_QUESTION"></mn2:FrameworkLiteral></asp:HyperLink> 
                <ml:MemberLikeAnswerClientModule ID="memberLikeClientModule" runat="server" Enabled="true"/>
		    </div>
		</dd>
	</asp:PlaceHolder>
	<asp:HiddenField ID="hidQuestionID" runat="server" />
</dl>
<script type="text/javascript">
    //<![CDATA[
    function ValidateQAnswer() {
        var error = false;
        var answerText = jQuery.trim(<%=AnswerElement%>);
        if (answerText == '<%=_WatermarkText%>' || answerText == '') {
            error = true;
        }

        if (error == true) {
            $j('#divAnswerError').css('display', 'block');
            return false;
        }
        else {
            spark.tracking.addEvent("event51", true);
            spark.tracking.addProp(38, "Answer-<%=Location%>", true);
            spark.tracking.track();
            return true;
        }
    }
    //]]>
</script>