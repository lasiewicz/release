﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MiniAnswerModule.ascx.cs" Inherits="Matchnet.Web.Applications.QuestionAnswer.Controls.MiniAnswerModule" %>
<%@ Register TagPrefix="qa" TagName="MiniAnswerProfile" Src="MiniAnswerProfile.ascx" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="qa2" TagName="MiniAnswerClientModule" Src="MiniAnswerClientModule.ascx" %>

<script type="text/javascript">
    //<![CDATA[
    (function() {
        __addToNamespace__('spark.qanda', {
            txtNoYourMatchesAnswers:'<%=GetResourceForJS("TXT_NO_YOUR_MATCHES_ANSWERS") %>'
        });
    })();
        
    $j(document).ready(function() {
        if (spark && spark.qanda && spark.qanda.QuestionInputElement) {
            $j("#" + spark.qanda.QuestionInputElement).click(function(evt) {
                if (!spark.tracking.clickOnce) {
                    spark.tracking.addEvent("event50", true);
                    spark.tracking.addProp(38, "Answer-Home Page", true);
                    spark.tracking.track();
                    spark.tracking.clickOnce = true;
                }
            });
        }
    });
    //]]>
</script>

<asp:PlaceHolder ID="phMiniAnswerProfile" runat="server" Visible="false">
    <div id="mini-a-module" class="mini-a-module clearfix">
        <qa2:MiniAnswerClientModule ID="MiniAnswerClientModule" ElementId="answers-list" runat="server" />    
        <div id="answers-list">
        <mn:Txt ID="txtNoYourMatchesAnswers" ResourceConstant="TXT_NO_YOUR_MATCHES_ANSWERS" runat="server" Visible="false" />
        <!--mini answer profiles-->
        <asp:Repeater ID="repeaterAnswers" runat="server" OnItemDataBound="repeaterAnswers_ItemDataBound">
            <ItemTemplate>
                <qa:MiniAnswerProfile ID="MiniAnswerProfile1" runat="server"></qa:MiniAnswerProfile>
            </ItemTemplate>
        </asp:Repeater>
        </div>
        <div class="text-outside">
            <asp:HyperLink ID="lnkViewAllAnswers" runat="server">
                <mn2:FrameworkLiteral ID="literalViewAllAnswers" runat="server" ResourceConstant="TXT_VIEW_ALL_ANSWERS"></mn2:FrameworkLiteral>
            </asp:HyperLink>
        </div> 
    </div>

    <script type="text/javascript">
     $j("#mini-a-module .member-answer", $j("#qanda")[0]).expander({
        slicePoint: 200,
        expandPrefix: ' ',
        expandText: '[&hellip;]',
        userCollapseText: '[^]'
      });
    </script>

</asp:PlaceHolder>

<asp:PlaceHolder ID="phNoAnswers" runat="server" Visible="false">
        <div id="mini-a-module" class="mini-a-module clearfix">
            <qa2:MiniAnswerClientModule ID="MiniAnswerClientModule1" ElementId="answers-list" runat="server" />    
            <div id="answers-list"></div>
        </div>
        <script type="text/javascript">
            $j(".header-options").hide();
        </script>        
</asp:PlaceHolder>

