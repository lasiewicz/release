﻿__addToNamespace__('spark.qanda', {
    LikeAnswer: function (element, memberId, answerId, omnitureParams) {
        var myFunc = function () {
            $j(element).addClass('ui-ajax-loader-sm');
            $j.ajax({
                type: "POST",
                url: "/Applications/API/MemberLikeService.asmx/ProcessMemberLike",
                data: "{brandId:" + spark.qanda.BrandId + "," +
                    "memberId:" + memberId + "," +
                    "likeObjectId:" + answerId + "," +
                    "likeObjectTypeId:1}",
                contentType: "application/json; charset=ISO-8859-1",
                dataType: "json",
                success: function (msg, status) {
                    try {
                        if (msg && msg.d && msg.d.Operation) {
                            var likeTxt = (msg.d.Operation == 'add') ? spark.qanda.txtUnLike : spark.qanda.txtLike;
                            var likeProp = (msg.d.Operation == 'add') ? "Like-" : "UnLike-";
                            var obj = $j(element);
                            var likeNum = (msg.d.TotalLikes && parseInt(msg.d.TotalLikes) > 0) ? spark.qanda.txtLikeNum.replace('{0}', msg.d.TotalLikes) : '';
                            obj.html(likeTxt.replace('{0}', likeNum));
                            if (omnitureParams) {
                                var overwrite = omnitureParams.Overwrite;
                                spark.tracking.addEvent(omnitureParams.Event, overwrite);
                                $j.each(omnitureParams.Props, function (i, prop) {
                                    spark.tracking.addProp(prop.Num, likeProp + prop.Value, overwrite);
                                });
                                spark.tracking.track();
                            }
                            if (msg.d.Operation == 'add') {
                                $j.ajax({
                                    type: "POST",
                                    url: "/Applications/API/MemberLikeService.asmx/ProcessLikeNotification",
                                    data: "{brandId:" + spark.qanda.BrandId + "," +
                                        "memberId:" + memberId + "," +
                                        "questionId:" + omnitureParams.QuestionId + "," +
                                        "answerId:" + answerId + "}",
                                    contentType: "application/json; charset=ISO-8859-1",
                                    dataType: "json"
                                });
                            }
                        }
                    } catch (e) { }
                    $j(element).removeClass('ui-ajax-loader-sm');
                },
                error: function (msg, status) { $j(element).removeClass('ui-ajax-loader-sm'); }
            });
        };
        spark.qanda.ajaxCallWithBlock(element, {
            message: '',
            centerY: false,
            css: { backgroundColor: 'transparent', border: 'none', top: '10%' },
            overlayCSS: { backgroundColor: '#fff', opacity: 0.8 }
        },
        myFunc, '/Applications/API/MemberLikeService.asmx/ProcessMemberLike');
    },
    LikeAnswerFreeText: function (element, memberId, profileOwnerID, objectId, sectionAttribute, omnitureParams) {
        var myFunc = function () {
            $j(element).addClass('ui-ajax-loader-sm');
            $j.ajax({
                type: "POST",
                url: "/Applications/API/MemberLikeService.asmx/ProcessMemberLike",
                data: "{brandId:" + spark.qanda.BrandId + "," +
                    "memberId:" + memberId + "," +
                    "likeObjectId:" + objectId + "," +
                    "likeObjectTypeId:2}",
                contentType: "application/json; charset=ISO-8859-1",
                dataType: "json",
                success: function (msg, status) {
                    try {
                        if (msg && msg.d && msg.d.Operation) {
                            var likeTxt = (msg.d.Operation == 'add') ? spark.qanda.txtUnLike : spark.qanda.txtLike;
                            var likeProp = (msg.d.Operation == 'add') ? "Like-" : "UnLike-";
                            var obj = $j(element);
                            var likeNum = (msg.d.TotalLikes && parseInt(msg.d.TotalLikes) > 0) ? spark.qanda.txtLikeNum.replace('{0}', msg.d.TotalLikes) : '';
                            obj.html(likeTxt.replace('{0}', likeNum));
                            if (omnitureParams) {
                                var overwrite = omnitureParams.Overwrite;
                                spark.tracking.addEvent(omnitureParams.Event, overwrite);
                                $j.each(omnitureParams.Props, function (i, prop) {
                                    spark.tracking.addProp(prop.Num, likeProp + prop.Value, overwrite);
                                });
                                spark.tracking.track();
                            }
                            if (msg.d.Operation == 'add') {
                                $j.ajax({
                                    type: "POST",
                                    url: "/Applications/API/MemberLikeService.asmx/ProcessFreeTextLikeNotification",
                                    data: "{brandId:" + spark.qanda.BrandId + "," +
                                        "memberId:" + memberId + "," +
                                        "questionId:" + omnitureParams.QuestionId + "," +
                                        "objectId:" + objectId + "," +
                                        "profileOwnerId:" + profileOwnerID + "," +
                                        "sectionAttribute:\"" + sectionAttribute + "\"}",
                                    contentType: "application/json; charset=ISO-8859-1",
                                    dataType: "json"
                                });
                            }
                        }
                    } catch (e) { }
                    $j(element).removeClass('ui-ajax-loader-sm');
                },
                error: function (msg, status) { $j(element).removeClass('ui-ajax-loader-sm'); }
            });
        };
        spark.qanda.ajaxCallWithBlock(element, {
            message: '',
            centerY: false,
            css: { backgroundColor: 'transparent', border: 'none', top: '10%' },
            overlayCSS: { backgroundColor: '#fff', opacity: 0.8 }
        },
        myFunc, '/Applications/API/MemberLikeService.asmx/ProcessMemberLike');
    },
    SendMail: function (elementId, questionId, fromMemberId, fromUserName, omnitureParams) {
        var answerId = (elementId && elementId.indexOf('-') > -1) ? elementId.split('-')[1] : 0;
        var toMemberId = (elementId && elementId.indexOf('-') > -1) ? elementId.split('-')[2] : 0;
        var obj = $j("#" + elementId).siblings(".error").remove();
        var mailMsg = $j.trim($j("#" + elementId).val() + '');
        if (mailMsg == '' || mailMsg == spark.qanda.txtCommentWatermarkCopy) {
            var obj = $j("#" + elementId).parents(".member-comment");
            obj.append("<div style=\"display:block;\" class=\"notification error\">" + spark.qanda.txtEmptyCommentErrorCopy + "</div>");
            return false;
        }
        var mailMessage = spark.util.uniescape($j("#" + elementId).val());
        var mailSubject = spark.util.uniescape(spark.qanda.txtCommentMailSubject);
        $j.ajax({
            type: "POST",
            url: "/Applications/API/IMailService.asmx/SendComment",
            data: "{brandId:" + spark.qanda.BrandId + "," +
                "elementId:'" + elementId + "'," +
                "questionId:" + questionId + "," +
                "answerId:" + answerId + "," +
                "toMemberId:" + toMemberId + "," +
                "fromMemberId:" + fromMemberId + "," +
                "fromUserName:\"" + spark.util.uniescape(fromUserName) + "\"," +
                "mailSubject:\"" + mailSubject + "\"," +
                "mailMessage:\"" + mailMessage + "\"}",
            contentType: "application/json; charset=ISO-8859-1",
            dataType: "json",
            success: function (msg, status) {
                if (msg && msg.d && msg.d.Status) {
                    var obj = $j("#" + msg.d.ElementId).parents(".member-comment");
                    obj.children('textarea,input,h2').remove();
                    obj.append(spark.qanda.txtCommentConfirmationCopy);
                    if (omnitureParams) {
                        var overwrite = omnitureParams.Overwrite;
                        spark.tracking.addEvent(omnitureParams.Event, overwrite);
                        $j.each(omnitureParams.Props, function (i, prop) {
                            spark.tracking.addProp(prop.Num, prop.Value, overwrite);
                        });
                        spark.tracking.track();
                    }
                }
                else if (msg && msg.d && (msg.d.StatusID == 5)) {
                    //session expired
                    window.location.href = '/Applications/Logon/Logon.aspx?DestinationURL=' + encodeURIComponent(window.location.href.replace('http://' + window.location.host, ''));
                }
                else {
                    var obj = $j("#" + msg.d.ElementId).parents(".member-comment");
                    obj.children('textarea,input,h2').remove();
                    obj.append(spark.qanda.txtCommentErrorCopy);
                }
            },
            error: function (msg, status) {
                var obj = $j("#" + msg.d.ElementId).parents(".member-comment");
                obj.children('textarea,input,h2').remove();
                obj.append(spark.qanda.txtCommentErrorCopy);
            }
        });
    },
    GetNextQuestion: function (params) {
        var startIndex = (params.startIdx && params.startIdx > 0) ? params.startIdx : 1;
        var questionId = (!params.questId) ? 0 : params.questId;
        $j.ajax({
            type: "POST",
            url: "/Applications/API/QuestionAnswerService.asmx/GetQuestion",
            data: "{questionID:" + questionId + "," +
                "memberID:" + params.memberId + "," +
                "startIndex:" + startIndex + "," +
                "pageSize:" + params.pageSize + "," +
                "sortBy:'" + params.sortBy + "'," +
                "reverse:" + params.reverse + "," +
                "entryPoint:null," +
                "trackingParam:null," +
                "isSite20Enabled:" + spark.qanda.IsSite20Enabled + "," +
                "includeAnsweredQuestions:false}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (msg, status) {
                if (msg && msg.d && msg.d.QuestionId > 0) {
                    var type = msg.d.QuestionTypeId;
                    if (!spark.util.jtemplates.qandaQuestions) spark.util.jtemplates.qandaQuestions = [];
                    if (!spark.util.jtemplates.qandaQuestions[type]) {
                        $j.ajax({ url: '/WebServices/templates/question.' + type + '.tmpl.htm',
                            success: function (template) {
                                spark.util.jtemplates.qandaQuestions[type] = template;
                                $j("#" + params.questionElemId).empty();
                                $j(spark.util.jtemplates.qandaQuestions[type]).render(msg.d).appendTo("#" + params.questionElemId);

                                if (type == "1") {
                                    spark.qanda.AddWaterMark($j("#" + params.questionElemId + " textarea").attr("id"), spark.qanda.txtWaterMarkText);
                                }
                            },
                            cache: false
                        });
                    } else {
                        $j("#" + params.questionElemId).empty();
                        $j(spark.util.jtemplates.qandaQuestions[type]).render(msg.d).appendTo("#" + params.questionElemId);

                        if (type == "1") {
                            spark.qanda.AddWaterMark($j("#" + params.questionElemId + " textarea").attr("id"), spark.qanda.txtWaterMarkText);
                        }
                    }


                    if (msg.d.Answers && msg.d.Answers.length > 0) {
                        var answers = msg.d.Answers;
                        // if not cached, asynchronously add our template content.
                        if (!spark.util.jtemplates.qandAnswers) {
                            $j.ajax({ url: '/WebServices/templates/answers.tmpl.htm',
                                success: function (template) {
                                    spark.util.jtemplates.qandAnswers = template;
                                    $j("#" + params.answerListElemId).empty();
                                    $j(spark.util.jtemplates.qandAnswers).render(answers).appendTo("#" + params.answerListElemId);
                                },
                                cache: false
                            });
                        } else {
                            $j("#" + params.answerListElemId).empty();
                            $j(spark.util.jtemplates.qandAnswers).render(answers).appendTo("#" + params.answerListElemId);
                        }
                        $j("#" + params.answerNumElemId).text(msg.d.TotalAnswerNum);
                        var answerTxt = (answers.length == 1) ? spark.qanda.txtOneAnswerNumberText : spark.qanda.txtAnswerNumberText;
                        $j("#" + params.answerNumTxtElemId).text(answerTxt);
                        $j("#" + params.answerModuleElemId + " .member-answer", $j("#qanda")).expander({
                            slicePoint: 200,
                            expandPrefix: ' ',
                            expandText: '[&hellip;]',
                            userCollapseText: '[^]'
                        });
                        //if there are answers show header and sorting
                        $j(".header-options").show();
                        var href = $j("#answer-count > a").attr("href");
                        $j("#answer-count > a").attr("href", href.split("?")[0] + "?questid=" + msg.d.QuestionId);

                        if (spark && spark.qanda) {
                            spark.qanda.QuestionId = msg.d.QuestionId;
                            if (spark.qanda.likeOmniParams) {
                                spark.qanda.likeOmniParams.QuestionId = msg.d.QuestionId;
                            }
                        }

                        //add watermark for comment boxes
                        $j('.member-comment > textarea').each(function () {
                            spark.qanda.AddWaterMark($j(this).attr("id"), spark.qanda.txtCommentWatermarkCopy);
                        });

                        if (msg.d.HasYourMatchesAnswers) {
                            if (msg.d.HasYourMatchesAnswers == 'false') {
                                $j("#" + params.answerListElemId).prepend(spark.qanda.txtNoYourMatchesAnswers);
                            }
                        }
                    } else {
                        $j("#" + params.answerListElemId).empty().append(spark.qanda.txtNoAnswers);
                        $j(".header-options").hide();
                    }

                    return true;
                }
                else {
                    //no available questions
                    if (params.outtaQuestionRedirect) {
                        window.location = "/Applications/QuestionAnswer/Home.aspx?tabid=1";
                    }
                }
            }
        });
        return true;
    },
    GetQuestion: function (evt, elementId, moduleId, answerNumId, answerNumTextId, questId, memberId, pageSize, startIdx, sortBy, reverse) {
        var startIndex = (startIdx && startIdx > 0) ? startIdx : 1;
        var questionId = (!questId) ? 0 : questId;
        $j.ajax({
            type: "POST",
            url: "/Applications/API/QuestionAnswerService.asmx/GetQuestion",
            data: "{questionID:" + questionId + "," +
                "memberID:" + memberId + "," +
                "startIndex:" + startIndex + "," +
                "pageSize:" + pageSize + "," +
                "sortBy:'" + sortBy + "'," +
                "reverse:" + reverse + "," +
                "entryPoint:'" + spark.qanda.EntryPoint + "'," +
                "trackingParam:'" + spark.qanda.TrackingParam + "'," +
                "isSite20Enabled:" + spark.qanda.IsSite20Enabled + "," +
                "includeAnsweredQuestions:true}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (msg, status) {
                if (msg && msg.d && msg.d.QuestionId > 0) {
                    var answers = msg.d.Answers;
                    // if not cached, asynchronously add our template content.
                    if (!spark.util.jtemplates.qandAnswers) {
                        $j.ajax({ url: '/WebServices/templates/answers.tmpl.htm',
                            success: function (template) {
                                spark.util.jtemplates.qandAnswers = template;
                                $j("#" + elementId).empty();
                                $j(spark.util.jtemplates.qandAnswers).render(answers).appendTo("#" + elementId);
                            },
                            cache: false
                        });
                    } else {
                        $j("#" + elementId).empty();
                        $j(spark.util.jtemplates.qandAnswers).render(answers).appendTo("#" + elementId);
                    }

                    $j("#" + answerNumId).text(msg.d.TotalAnswerNum);
                    var answerTxt = (answers.length == 1) ? spark.qanda.txtOneAnswerNumberText : spark.qanda.txtAnswerNumberText;
                    $j("#" + answerNumTextId).text(answerTxt);
                    $j("#" + moduleId + " .member-answer", $j("#qanda")).expander({
                        slicePoint: 200,
                        expandPrefix: ' ',
                        expandText: '[&hellip;]',
                        userCollapseText: '[^]'
                    });
                    $j("#" + moduleId + " textarea").watermark(spark.qanda.txtCommentWatermarkCopy);
                    if (null != answers && answers.length > 0) {
                        if (msg.d.HasYourMatchesAnswers) {
                            if (msg.d.HasYourMatchesAnswers == 'false') {
                                $j("#" + elementId).prepend(spark.qanda.txtNoYourMatchesAnswers);
                            }
                        }
                    }
                }
            }
        });
    },
    AnswerBlockedQuestion: function (elementId, answerValue, memberId, answerMemberId, answerListId) {
        var questionId = (elementId && elementId.indexOf('-') > -1) ? elementId.split('-')[1] : 0;
        var answerText = spark.util.uniescape(answerValue);
        var applyTemplate = function (listId, content) {
            $j("#" + listId).empty();
            $j(spark.util.jtemplates.dblQandAnswers).render(content).appendTo("#" + listId);
            $j("#" + listId).attr("id", listId).attr("class", "double-q clearfix");
            //ellipsize long answers
            $j("#" + listId + " .member-answer").expander({
                slicePoint: 200,
                expandPrefix: ' ',
                expandText: '[&hellip;]',
                userCollapseText: '[^]'
            });
        };
        $j.ajax({
            type: "POST",
            url: "/Applications/API/QuestionAnswerService.asmx/AnswerBlockedQuestion",
            data: "{questionID:" + questionId + "," +
                "memberID:" + memberId + "," +
                "answerMemberID:" + answerMemberId + "," +
                "answerText:\"" + answerText + "\"," +
                "entryPoint:'" + spark.qanda.EntryPoint + "'," +
                "trackingParam:'" + spark.qanda.TrackingParam + "'," +
                "isSite20Enabled:" + spark.qanda.IsSite20Enabled + "}",
            contentType: "application/json; charset=ISO-8859-1",
            dataType: "json",
            success: function (msg, status) {
                if (msg && msg.d && msg.d.QuestionId > 0) {
                    // if not cached, asynchronously add our template content.
                    if (!spark.util.jtemplates.dblQandAnswers) {
                        $j.ajax({ url: '/WebServices/templates/doubleanswer.tmpl.htm',
                            success: function (template) {
                                spark.util.jtemplates.dblQandAnswers = template;
                                applyTemplate(elementId, msg.d);
                            },
                            cache: false
                        });
                    } else {
                        applyTemplate(elementId, msg.d);
                    }
                    //cleanup the numbers in the headers
                    var numMatched = parseInt($j("#numMatched").text());
                    var numBlocked = parseInt($j("#numBlocked").text());
                    $j("#numMatched").text(numMatched + 1);
                    $j("#bothAnsweredHeader").css("visibility", "").css("display", "block");
                    $j("#numBlocked").text(numBlocked - 1);
                    //remove the blocked header if no more questions are blocked
                    if (0 == numBlocked - 1) {
                        $j("#blockedAnswerHeader").css("display", "none");
                    }
                    return true;
                }
                return false;
            },
            error: function (msg) { return false; }
        });
        return true;
    },
    ajaxCallWithBlock: spark.util.ajaxCallWithBlock,
    __init__: function () {
        if (!$j.render) {
            $j("head").append("<script type=\"text/javascript\" src=\"/javascript20/library/jquery.tmpl.js\"><" + "/script>");
        }
    },
    FreeTextSendMail: function (elementId, attribute, fromMemberId, fromUserName, omnitureParams) {
        //var answerId = (elementId && elementId.indexOf('-') > -1) ? elementId.split('-')[1] : 0;
        var toMemberId = (elementId && elementId.indexOf('-') > -1) ? elementId.split('-')[2] : 0;
        var obj = $j("#" + elementId).siblings(".error").remove();
        var mailMsg = $j.trim($j("#" + elementId).val() + '');
        if (mailMsg == '' || mailMsg == spark.qanda.txtCommentWatermarkCopy) {
            var obj = $j("#" + elementId).parents(".comment-block");
            obj.append("<div style=\"display:block;\" class=\"notification error\">" + spark.qanda.txtEmptyCommentErrorCopy + "</div>");
            return false;
        }
        var mailMessage = spark.util.uniescape($j("#" + elementId).val());
        var mailSubject = spark.util.uniescape(spark.qanda.txtFreeTextCommentMailSubject);
        var freeTextSection = spark.util.uniescape(attribute);
        $j.ajax({
            type: "POST",
            url: "/Applications/API/IMailService.asmx/FreeTextSendComment",
            data: "{brandId:" + spark.qanda.BrandId + "," +
                "elementId:'" + elementId + "'," +
                "toMemberId:" + toMemberId + "," +
                "fromMemberId:" + fromMemberId + "," +
                "fromUserName:\"" + spark.util.uniescape(fromUserName) + "\"," +
                "mailSubject:\"" + mailSubject + "\"," +
                "sectionAttribute:\"" + freeTextSection + "\"," +
                "mailMessage:\"" + mailMessage + "\"}",
            contentType: "application/json; charset=ISO-8859-1",
            dataType: "json",
            success: function (msg, status) {
                if (msg && msg.d && msg.d.Status) {
                    var obj = $j("#" + msg.d.ElementId).parents(".comment-block");
                    obj.find('textarea,.text-outside,h2').remove();
                    obj.append(spark.qanda.txtCommentConfirmationCopy);
                    if (omnitureParams) {
                        var overwrite = omnitureParams.Overwrite;
                        spark.tracking.addEvent(omnitureParams.Event, overwrite);
                        $j.each(omnitureParams.Props, function (i, prop) {
                            spark.tracking.addProp(prop.Num, prop.Value, overwrite);
                        });
                        spark.tracking.track();
                    }
                }
                else if (msg && msg.d && (msg.d.StatusID == 5)) {
                    //session expired
                    window.location.href = '/Applications/Logon/Logon.aspx?DestinationURL=' + encodeURIComponent(window.location.href.replace('http://' + window.location.host, ''));
                }
                else {
                    var obj = $j("#" + msg.d.ElementId).parents(".comment-block");
                    obj.find('textarea,.text-outside,h2').remove();
                    obj.append(spark.qanda.txtCommentErrorCopy);
                }
            },
            error: function (msg, status) {
                var obj = $j("#" + msg.d.ElementId).parents(".comment-block");
                obj.find('textarea,.text-outside,h2').remove();
                obj.append(spark.qanda.txtCommentErrorCopy);
            }
        });
    },
    LikePhoto: function (element, photoObj, memberId, profileOwnerID, objectId, omnitureParams) {
        var myFunc = function () {
            $j(element).addClass('ui-ajax-loader-sm');
            $j.ajax({
                type: "POST",
                url: "/Applications/API/MemberLikeService.asmx/ProcessMemberLike",
                data: "{brandId:" + spark.qanda.BrandId + "," +
                    "memberId:" + memberId + "," +
                    "likeObjectId:" + objectId + "," +
                    "likeObjectTypeId:3}",
                contentType: "application/json; charset=ISO-8859-1",
                dataType: "json",
                success: function (msg, status) {
                    try {
                        if (msg && msg.d && msg.d.Operation) {
                            var likeTxt = (msg.d.Operation == 'add') ? spark.qanda.txtUnLike : spark.qanda.txtLike;
                            var likeProp = (msg.d.Operation == 'add') ? "Like-" : "UnLike-";
                            var obj = $j(element);
                            var likeNum = (msg.d.TotalLikes && parseInt(msg.d.TotalLikes) > 0) ? spark.qanda.txtLikeNum.replace('{0}', msg.d.TotalLikes) : '';
                            obj.html(likeTxt.replace('{0}', likeNum));
                            $j("#" + photoObj.commentAndLikeDivID).find(".like").html(likeTxt.replace('{0}', likeNum));
                            if (omnitureParams) {
                                var overwrite = omnitureParams.Overwrite;
                                spark.tracking.addEvent(omnitureParams.Event, overwrite);
                                $j.each(omnitureParams.Props, function (i, prop) {
                                    spark.tracking.addProp(prop.Num, likeProp + prop.Value, overwrite);
                                });
                                spark.tracking.track();
                            }
                            if (msg.d.Operation == 'add') {
                                $j.ajax({
                                    type: "POST",
                                    url: "/Applications/API/MemberLikeService.asmx/ProcessPhotoLikeNotification",
                                    data: "{brandId:" + spark.qanda.BrandId + "," +
                                        "memberId:" + memberId + "," +
                                        "questionId:" + omnitureParams.QuestionId + "," +
                                        "objectId:" + objectId + "," +
                                        "profileOwnerId:" + profileOwnerID + "}",
                                    contentType: "application/json; charset=ISO-8859-1",
                                    dataType: "json"
                                });
                            }
                        }
                    } catch (e) { }
                    $j(element).removeClass('ui-ajax-loader-sm');
                },
                error: function (msg, status) { $j(element).removeClass('ui-ajax-loader-sm'); }
            });
        };
        spark.qanda.ajaxCallWithBlock(element, {
            message: '',
            centerY: false,
            css: { backgroundColor: 'transparent', border: 'none', top: '10%' },
            overlayCSS: { backgroundColor: '#fff', opacity: 0.8 }
        },
        myFunc, '/Applications/API/MemberLikeService.asmx/ProcessMemberLike');
    },
    PhotoCommentSendMail: function (elementId, photoID, fromMemberId, fromUserName, omnitureParams) {
        //var answerId = (elementId && elementId.indexOf('-') > -1) ? elementId.split('-')[1] : 0;
        var toMemberId = (elementId && elementId.indexOf('-') > -1) ? elementId.split('-')[2] : 0;
        var obj = $j("#" + elementId).siblings(".error").remove();
        var mailMsg = $j.trim($j("#" + elementId).val() + '');
        if (mailMsg == '' || mailMsg == spark.qanda.txtCommentWatermarkCopy) {
            var obj = $j("#" + elementId).parents(".comment-block");
            obj.append("<div style=\"display:block;\" class=\"notification error\">" + spark.qanda.txtEmptyCommentErrorCopy + "</div>");
            return false;
        }
        var mailMessage = spark.util.uniescape($j("#" + elementId).val());
        var mailSubject = spark.util.uniescape(spark.qanda.txtPhotoCommentMailSubject);
        $j.ajax({
            type: "POST",
            url: "/Applications/API/IMailService.asmx/PhotoSendComment",
            data: "{brandId:" + spark.qanda.BrandId + "," +
                "elementId:'" + elementId + "'," +
                "toMemberId:" + toMemberId + "," +
                "fromMemberId:" + fromMemberId + "," +
                "fromUserName:\"" + spark.util.uniescape(fromUserName) + "\"," +
                "mailSubject:\"" + mailSubject + "\"," +
                "photoID:\"" + photoID + "\"," +
                "mailMessage:\"" + mailMessage + "\"}",
            contentType: "application/json; charset=ISO-8859-1",
            dataType: "json",
            success: function (msg, status) {
                if (msg && msg.d && msg.d.Status) {
                    var obj = $j("#" + msg.d.ElementId).parents(".comment-block");
                    obj.find('textarea,.text-outside,h2').remove();
                    obj.append(spark.qanda.txtCommentConfirmationCopy);
                    if (omnitureParams) {
                        var overwrite = omnitureParams.Overwrite;
                        spark.tracking.addEvent(omnitureParams.Event, overwrite);
                        $j.each(omnitureParams.Props, function (i, prop) {
                            spark.tracking.addProp(prop.Num, prop.Value, overwrite);
                        });
                        spark.tracking.track();
                    }
                }
                else if (msg && msg.d && (msg.d.StatusID == 5)) {
                    //session expired
                    window.location.href = '/Applications/Logon/Logon.aspx?DestinationURL=' + encodeURIComponent(window.location.href.replace('http://' + window.location.host, ''));
                }
                else {
                    var obj = $j("#" + msg.d.ElementId).parents(".comment-block");
                    obj.find('textarea,.text-outside,h2').remove();
                    obj.append(spark.qanda.txtCommentErrorCopy);
                }
            },
            error: function (msg, status) {
                var obj = $j("#" + msg.d.ElementId).parents(".comment-block");
                obj.find('textarea,.text-outside,h2').remove();
                obj.append(spark.qanda.txtCommentErrorCopy);
            }
        });
    }
});
