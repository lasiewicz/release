﻿using System;
using System.Collections.Generic;
using Matchnet.Web.Framework;
using Matchnet.PremiumServiceSearch.ValueObjects.QuestionAnswer;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Web.Framework.Managers;
using Matchnet.Web.Framework.Ui;
using Matchnet.Web.Applications.ColorCode;
using Matchnet.Content.ServiceAdapters.Links;
using Matchnet.Web.Applications.MemberProfile;
using Matchnet.MemberLike.ValueObjects;
using Matchnet.Web.Applications.MemberLike;
using Matchnet.PremiumServiceSearch.ServiceAdapters;
using QnA = Matchnet.PremiumServiceSearch.ValueObjects.QuestionAnswer;

namespace Matchnet.Web.Applications.QuestionAnswer.Controls
{
    public partial class AnswerProfile : FrameworkControl
    {
        private Answer _Answer;
        private Matchnet.Member.ServiceAdapters.Member _Member;
        private int _ordinal;
        private BreadCrumbHelper.EntryPoint _myEntryPoint = BreadCrumbHelper.EntryPoint.QuestionPage;
        private string _viewProfileUrl;
        private bool _isHighlighted = false;
        protected Color _Color = Color.none;
        protected string _ColorText = "";
        private string _location = "Question Page";
        public string Location
        {
            get { return _location; }
            set { _location = value; }
        }

        public bool IsCommentsEnabled
        {
            get { return QuestionAnswerHelper.IsQuestionAnswerCommentEnabled(g.Brand); }
        }

        public bool IsMemberLikeEnabled
        {
            get { return MemberLikeHelper.Instance.IsMemberLikeEnabled(g.Brand); }
        }

        public bool IsLoggedIn
        {
            get { return null != g.Member && g.Member.MemberID > 0; }
        }

        public int Ordinal
        {
            get { return (_ordinal); }
            set { _ordinal = value; }
        }


        public BreadCrumbHelper.EntryPoint MyEntryPoint
        {
            get { return (_myEntryPoint); }
            set { _myEntryPoint = value; }
        }

        public bool IsHighlighted
        {
            get { return (this._isHighlighted); }
            set { this._isHighlighted = value; }
        }

        public string TrackingParam { get; set; }

        public int AnswerId
        {
            get { return _Answer.AnswerID; }
        }

        public int QuestionId
        {
            get { return _Answer.QuestionID; }
        }

        public int MemberId
        {
            get
            {
                if (_Member != null && _Member.MemberID > 0)
                {
                    return _Member.MemberID;
                }
                return 0;
            }
        }

        public int MemberLikeId
        {
            get
            {
                if (g.Member != null && g.Member.MemberID > 0)
                {
                    return g.Member.MemberID;
                }
                return 0;
            }
        }
        public DisplayAnswersMode DisplayMode { get; set; }
        public string QuestionModuleClientID { get { return divQuestionModule.ClientID; } }

        #region Event Handlers
        protected void Page_Load(object sender, EventArgs e)
        {
            btnSubmitAnswer.Value = g.GetResource("TXT_ANSQ_LABEL", this);
        }
        #endregion

        #region Public methods
        public string GetResourceForJS(string resConst)
        {
            return FrameworkGlobals.JavaScriptEncode(g.GetResource(resConst, new string[0], true, this));
        }

        public void LoadAnswerProfile(Answer answer)
        {
            try
            {

                _Answer = answer;

                _Member = MemberSA.Instance.GetMember(answer.MemberID, MemberLoadFlags.None);

                //member answer
                literalMemberAnswer.Text = _Answer.AnswerValue.Replace(Environment.NewLine, "<br />");
                if (IsMemberLikeEnabled)
                {
                    MemberLikeParams likeParams = new MemberLikeParams();
                    likeParams.SiteId = g.Brand.Site.SiteID;
                    likeParams.MemberId = g.Member.MemberID;
                    List<MemberLikeObject> likes = MemberLikeHelper.Instance.GetLikesByObjectIdAndType(likeParams, answer.AnswerID, Convert.ToInt32(LikeObjectTypes.Answer));
                    int likeCount = 0;
                    bool hasLike = false;
                    if (null != likes)
                    {
                        likes = likes.FindAll(delegate(MemberLikeObject likeObject)
                        {
                            Matchnet.Member.ServiceAdapters.Member aMember = MemberSA.Instance.GetMember(likeObject.MemberId, MemberLoadFlags.None);
                            return !MemberLikeHelper.Instance.IsMemberSuppressed(aMember, g);
                        });
                        MemberLikeObject match = likes.Find(delegate(MemberLikeObject obj)
                        {
                            bool b = (obj.MemberId == g.Member.MemberID);
                            return b;
                        });
                        hasLike = (null != match);
                        likeCount = likes.Count;
                    }

                    string likeNumber = string.Empty;
                    if (likeCount > 0)
                    {
                        likeNumber = string.Format(g.GetResource("TXT_LIKE_NUM", this), likeCount);
                    }

                    string likeResKey = (hasLike) ? "TXT_ANSWER_UNLIKE" : "TXT_ANSWER_LIKE";
                    likeTxt.Text = string.Format(g.GetResource(likeResKey, this), likeNumber);
                    likeTxt.Visible = true;
                }
                //load profile info
                LoadProfileInformation();
                if (DisplayMode == DisplayAnswersMode.recent)
                {
                    QnA.Question q = QuestionAnswerSA.Instance.GetQuestion(answer.QuestionID);
                    if (q == null)
                        return;
                    phQuestion.Visible = true;
                    //litQuestion.Text = g.GetResource("TXT_Q", this, new string[] { q.Text });
                    litQuestion.Text = q.Text;
                    litQuestion.Href = "/Applications/QuestionAnswer/Home.aspx?TabId=1&questid=" + q.QuestionID.ToString();
                    if (g.Member != null)
                    {
                        Answer memberanswer = QuestionAnswerHelper.GetMemberAnswer(g.Member.MemberID, q);
                        /*
                      
                          //if (memberanswer == null)
                          if (memberanswer == null || ((memberanswer.AnswerStatus == QuestionAnswerEnums.AnswerStatusType.None || memberanswer.AnswerStatus == QuestionAnswerEnums.AnswerStatusType.Pending)))
                          {
                            //  btnSubmitAnswer.Visible = true;
                              ucMiniQuestionClientModule.LoadQuestion(q);

                          }
                          else
                          {
  //                            btnSubmitAnswer.Visible = false;
                          }
                         * */


                        if (memberanswer != null && (memberanswer.AnswerStatus == QuestionAnswerEnums.AnswerStatusType.None || memberanswer.AnswerStatus == QuestionAnswerEnums.AnswerStatusType.Pending))
                        {
                            btnSubmitAnswer.Visible = false;
                        }
                        else
                        {
                            //  btnSubmitAnswer.Visible = true;
                            ucMiniQuestionClientModule.LoadQuestion(q);
                        }
                    }
                    // divQuestionModule.Style.Add("display", "none");
                }
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        #endregion

        #region Private methods
        private void LoadProfileInformation()
        {
            //member profile info
            SearchResultProfile searchResult = new SearchResultProfile(_Member, Ordinal);
            lnkUserName1.Text = FrameworkGlobals.Ellipsis(_Member.GetUserName(_g.Brand), 12);
            this.literalSeeking.Text = Matchnet.Web.Applications.MemberProfile.ProfileDisplayHelper.GetMaritalStatusSeekingGenderDisplay(_Member, this.g);
            this.literalAge.Text = FrameworkGlobals.GetAge(_Member, g.Brand).ToString();
            this.literalLocation.Text = ProfileDisplayHelper.GetRegionDisplay(_Member, g);

            //set send e-card link (logic taken from ProfileHeaderView.ascx)
            if (g.EcardsEnabled)
            {
                // Generate mingle ecard page url. (to parameter is the username of the reciever)
                string destPageUrl = FrameworkGlobals.BuildConnectFrameworkLink("cards/categories.html?to=" + _Member.GetUserName(_g.Brand) + "&MemberID=" + _Member.MemberID.ToString() + "&return=1", g, true);

                // Assign url to ECard link.
                this.lnkEcardMe.NavigateUrl = destPageUrl;

                //Adjust left nav for popup
                if (!(g.GetEntryPoint(Request) == BreadCrumbHelper.EntryPoint.LookUpMember))
                {
                    this.lnkEcardMe.NavigateUrl += "&HideNav=1";
                }

            }
            else
                this.lnkEcardMe.Visible = false;

            //set IM/online link
            bool isOnline;
            OnlineLinkHelper.SetOnlineLink(_Member, g.Member, imgChat, this.lnkIMMe, LinkParent.QuestionPageAnswerProfile, out isOnline);
            if (!isOnline)
            {
                this.phOnlineStatusOffline.Visible = true;
                this.lnkIMMe.Visible = false;
                imgChatOffline.FileName = "icon-status-offline.gif";
            }
            else
            {
                this.lnkIMMe.Visible = true;

                this.phOnlineStatusOffline.Visible = false;
                imgChat.FileName = "icon-status-online.gif";
            }

            //set email and flirt/tease link
            bool returnToMember = (g.GetEntryPoint(Request) == BreadCrumbHelper.EntryPoint.LookUpMember);
            Matchnet.Web.Applications.MemberProfile.ProfileDisplayHelper.SetEmailTeaseLinks(g, _Member.MemberID, ref lnkEmailMe, ref lnkFlirtMe, returnToMember);
            ProfileDisplayHelper.AddFlirtOnClick(g, ref lnkFlirtMe);

            lnkEmailMe.HRef = lnkEmailMe.HRef + "&LinkParent=" + LinkParent.QuestionAnswerEmailButton.ToString("d");

            //set add to hot list link
            this.add2List.MemberID = _Member.MemberID;

            if (this.IsHighlighted)
            {
                _viewProfileUrl = BreadCrumbHelper.MakeViewProfileLink(this.MyEntryPoint, _Member.MemberID, this.Ordinal, null, Constants.NULL_INT, false, (int)BreadCrumbHelper.PremiumEntryPoint.Highlight);
            }
            else
            {
                _viewProfileUrl = BreadCrumbHelper.MakeViewProfileLink(this.MyEntryPoint, _Member.MemberID, this.Ordinal, null, Constants.NULL_INT, false, (int)BreadCrumbHelper.PremiumEntryPoint.NoPremium);
            }

            if (!String.IsNullOrEmpty(TrackingParam))
            {
                _viewProfileUrl = BreadCrumbHelper.AppendParamToProfileLink(_viewProfileUrl, TrackingParam);
            }

            lnkUserName1.NavigateUrl = _viewProfileUrl;
            //MemberPhotoHelper photoHelper = new MemberPhotoHelper(RuntimeSettings.Instance);
            noPhoto.NoPhotoFileName = MemberPhotoDisplayManager.Instance.GetNoPhotoFile(NoPhotoImageType.TinyThumbV2, true);
            searchResult.SetThumb(imgThumb, noPhoto, _viewProfileUrl);

            //color code
            if (ColorCodeHelper.IsColorCodeEnabled(g.Brand) && ColorCodeHelper.HasMemberCompletedQuiz(_Member, g.Brand) && !ColorCodeHelper.IsMemberColorCodeHidden(_Member, g.Brand))
            {
                phColorCode.Visible = true;
                _Color = ColorCodeHelper.GetPrimaryColor(_Member, g.Brand);
                _ColorText = ColorCodeHelper.GetFormattedColorText(_Color);
            }

            lastTimeAction.LoadLastTime(_Answer.UpdateDate);
        }
        #endregion
    }
}
