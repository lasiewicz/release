﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Caching;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Member.ValueObjects.Interfaces;
using Matchnet.Web.Framework;
using Matchnet.PremiumServiceSearch.ValueObjects.QuestionAnswer;

namespace Matchnet.Web.Applications.QuestionAnswer.Controls
{
    public partial class BothAnsweredQuestionModule : FrameworkControl
    {
        private Hashtable myQuestions;
        private List<Matchnet.PremiumServiceSearch.ValueObjects.QuestionAnswer.Question> _matchingQuestions;
        protected IMemberDTO _MemberProfile;
        private int _PageSize = 6;
        private int _ChapterSize = 3;

        public int PageSize
        {
            get { return _PageSize; }
            set { _PageSize = value; }
        }

        public int ChapterSize
        {
            get { return _ChapterSize; }
            set { _ChapterSize = value; }
        }

        public IMemberDTO MemberProfile
        {
            get { return _MemberProfile; }
            set { _MemberProfile = value; }
        }

        public List<Matchnet.PremiumServiceSearch.ValueObjects.QuestionAnswer.Question> MatchingQuestions
        {
            set { this._matchingQuestions = value; }
            get { return this._matchingQuestions; }
        }

        public string GetResourceForJS(string resConst, object resourceControl)
        {
            if (null == resourceControl)
            {
                return FrameworkGlobals.JavaScriptEncode(g.GetResource(resConst));
            }
            return FrameworkGlobals.JavaScriptEncode(g.GetResource(resConst, new string[0], true, resourceControl));
        }
        
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public void LoadModule(IMemberDTO member, List<PremiumServiceSearch.ValueObjects.QuestionAnswer.Question> questions)
        {
            this.MemberProfile = member;

            if (null != questions && questions.Count > 0)
            {
                MatchingQuestions = questions;
                headerStyle.Text = "display:block";
                phClientPagintation.Visible = QuestionAnswerHelper.IsQuestionAnswerClientPaginationEnabled(g.Brand);

                //get question and answers for member                
                myQuestions = QuestionAnswerHelper.GetLoggedInMemberQuestions(g);

                repeaterDoubleQandABothAnswered.DataSource = MatchingQuestions;
                repeaterDoubleQandABothAnswered.DataBind();
                repeaterDoubleQandABothAnswered.Visible = true;
            }
            else
            {
                headerStyle.Text = "display:none; visibility:hidden";
                phClientPagintation.Visible = false;
            }
            string reskey = (null != MatchingQuestions && MatchingQuestions.Count > 1) ? "TXT_NUM_MATCHED" : "TXT_NUM_MATCHED_ONE";
            numMatchedQuestions.Text = g.GetResource(reskey, this, new string[] { ((null != MatchingQuestions) ? MatchingQuestions.Count.ToString() : "0") });
        }

        protected void repeaterDoubleQandABothAnswered_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Matchnet.PremiumServiceSearch.ValueObjects.QuestionAnswer.Question question = e.Item.DataItem as Matchnet.PremiumServiceSearch.ValueObjects.QuestionAnswer.Question;
                if (null != myQuestions && myQuestions.ContainsKey(question.QuestionID))
                {
                    Literal literalQuestion = (Literal)e.Item.FindControl("literalQuestion");
                    HyperLink linkQuestion = (HyperLink)e.Item.FindControl("lnkQuestion");
                    Literal questionId = (Literal)e.Item.FindControl("questionId");
                    literalQuestion.Text = question.Text;
                    linkQuestion.NavigateUrl += "?" + WebConstants.URL_PARAMETER_NAME_QUESTION_ID + "=" + question.QuestionID;
                    BothAnsweredProfile miniAnswerProfile = e.Item.FindControl("MiniAnswerProfile1") as BothAnsweredProfile;
                    miniAnswerProfile.LoadAnswerProfile(MemberProfile, question.Answers[0],myQuestions[question.QuestionID] as Answer);
                    questionId.Text = question.QuestionID.ToString();
                }
            }
        }

    }
}