﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Web.Framework;
using Matchnet.Web.Applications.MemberLike;
using Matchnet.PremiumServiceSearch.ValueObjects.QuestionAnswer;

namespace Matchnet.Web.Applications.QuestionAnswer.Controls
{
    public partial class MiniAnswerModule : FrameworkControl
    {
        private Matchnet.PremiumServiceSearch.ValueObjects.QuestionAnswer.Question _Question;
        private const int DEFAULT_PAGE_SIZE = 5;
        private int _PageSize = Constants.NULL_INT;
        private string _lastSort = null;

        #region Properties
        public int PageSize
        {
            get {
                if (_PageSize == Constants.NULL_INT)
                {
                    _PageSize = DEFAULT_PAGE_SIZE;
                    int settingPageSize = SettingsManager.GetSettingInt(SettingConstants.QANDA_ANSWER_PAGE_SIZE, g.Brand);
                    if (settingPageSize != Constants.NULL_INT)
                    {
                        _PageSize = settingPageSize;
                    }
                }
                return _PageSize; 
            }
            set { _PageSize = value; }
        }

        public string TrackingParam { get; set; }

        public string LastSort
        {
            get
            {
                if (string.IsNullOrEmpty(_lastSort))
                {
                    try
                    {
                        _lastSort = QuestionAnswerHelper.GetLastSortForMember(g.Brand, g.Member);
                    }
                    catch (Exception ex)
                    {
                        g.ProcessException(ex);
                    }
                }
                return _lastSort;
            }
        }

        public bool IsMemberLikeEnabled
        {
            get { return MemberLikeHelper.Instance.IsMemberLikeEnabled(g.Brand); }
        }
        #endregion

        public string GetResourceForJS(string resConst)
        {
            return FrameworkGlobals.JavaScriptEncode(g.GetResource(resConst, new string[0], true, this));
        }

        #region Event Handlers
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        protected void repeaterAnswers_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Answer answer = e.Item.DataItem as Answer;

                MiniAnswerProfile miniAnswerProfile = e.Item.FindControl("MiniAnswerProfile1") as MiniAnswerProfile;
                miniAnswerProfile.TrackingParam = TrackingParam;
                miniAnswerProfile.LoadAnswerProfile(answer);
            }
        }

        #endregion

        #region Public methods
        public void LoadAnswers(Matchnet.PremiumServiceSearch.ValueObjects.QuestionAnswer.Question question)
        {
            _Question = question;
            MiniAnswerClientModule.QuestionId = _Question.QuestionID;
            MiniAnswerClientModule.TrackingParam = TrackingParam;
            MiniAnswerClientModule.PageSize = PageSize;
            lnkViewAllAnswers.Visible = false;

            //load answers
            phMiniAnswerProfile.Visible = false;
            IComparer<Answer> comparer = GetComparer(Request);
            Answer[] approvedAnswers = QuestionAnswerHelper.GetApprovedAnswers(question, g.Member.MemberID, g, comparer);
            if (comparer is YourMatchesAnswerComparer)
            {
                Txt txtNoYourMatchesAnswers = this.FindControl("txtNoYourMatchesAnswers") as Txt;
                txtNoYourMatchesAnswers.Visible = !((YourMatchesAnswerComparer)comparer).HasYourMatches;
            }

            MiniAnswerClientModule.TotalAnswersNum = approvedAnswers.Length;
            if (approvedAnswers.Length > 0)
            {
                if (approvedAnswers.Length > PageSize)
                {
                    lnkViewAllAnswers.NavigateUrl = "/Applications/QuestionAnswer/Question.aspx?" + WebConstants.URL_PARAMETER_NAME_QUESTION_ID + "=" + question.QuestionID.ToString();
                    lnkViewAllAnswers.Visible = true;
                }

                phMiniAnswerProfile.Visible = true;

                //load answer profiles
                repeaterAnswers.DataSource = approvedAnswers.Take(PageSize).ToArray();
                repeaterAnswers.DataBind();

            }
            else
            {
                MiniAnswerClientModule1.QuestionId = _Question.QuestionID;
                MiniAnswerClientModule1.TrackingParam = TrackingParam;
                MiniAnswerClientModule1.PageSize = PageSize;
                MiniAnswerClientModule1.TotalAnswersNum = 0;
                Answer memberAnswer = QuestionAnswerHelper.GetMemberAnswer(g.Member.MemberID, question);
                if (memberAnswer == null)
                {
                    phNoAnswers.Visible = true;
                }
            }
            
        }

        #endregion

        private IComparer<Answer> GetComparer(HttpRequest request)
        {
            string sortBy = request["sort"];
            bool reverseSort = (!string.IsNullOrEmpty(request["sortd"]) && Boolean.TrueString.ToLower().Equals(request["sortd"].ToString().ToLower()));
            string sortCriteria = (string.IsNullOrEmpty(sortBy)) ? null : sortBy + "|" + reverseSort.ToString().ToLower();
            return QuestionAnswerHelper.GetAnswerComparerFromCriteria(sortCriteria, LastSort, g);
        }
    }
}