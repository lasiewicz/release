﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Web.Framework;
using Matchnet.PremiumServiceSearch.ServiceAdapters;

namespace Matchnet.Web.Applications.QuestionAnswer
{
    public partial class Question : FrameworkControl, iTab
    {
        public bool TabbedDisplay { get; set; }
        private Matchnet.PremiumServiceSearch.ValueObjects.QuestionAnswer.Question _Question;
        private bool _AnswerSuccessfullySubmitted = false;
        bool _autoLoad = true;
        private string _location = "Question Page";
        public string Location
        {
            get { return _location; }
            set { _location = value; }
        }

        public bool AutoLoad { get { return _autoLoad; } set { _autoLoad = value; } }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (AutoLoad)
            {
                Load();
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            //omniture
            if (_AnswerSuccessfullySubmitted)
            {
                if (_Question != null)
                {
                    g.AnalyticsOmniture.Evar15 = _Question.QuestionID.ToString(); //capture questionID for successful answer
                    g.AnalyticsOmniture.AddEvent("event42"); //capture successful answer submitted
                }
            }

            base.OnPreRender(e);
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            QuestionModule1.AnswerSubmitted += new Matchnet.Web.Applications.QuestionAnswer.Controls.QuestionModule.AnswerSubmittedEventHandler(QuestionModule1_AnswerSubmitted);
        }

        protected void QuestionModule1_AnswerSubmitted(bool isSuccess)
        {
            _AnswerSuccessfullySubmitted = isSuccess;
        }

        public void Load()
        {
            try
            {
                if (g.BreadCrumbTrailHeader != null)
                {
                    g.BreadCrumbTrailHeader.SetTwoLinkCrumb(g.GetResource("NAV_QUESTION", this), g.AppPage.App.DefaultPagePath);

                }
                if (QuestionAnswerHelper.IsQuestionAnswerEnabled(g.Brand))
                {
                    if (g.Member != null)
                    {
                        //question and answers
                        int questionID = Constants.NULL_INT;
                        if (Page.IsPostBack)
                        {
                            int.TryParse(Request.Form[hidQuestionID.UniqueID], out questionID);
                        }
                        else
                        {
                            if (!String.IsNullOrEmpty(Request.QueryString["err"]))
                            {
                                g.Notification.AddErrorString(g.GetResource(Request.QueryString["err"], this));
                            }

                            if (!String.IsNullOrEmpty(Request.QueryString["answerSubmitted"]))
                            {
                                if (Request.QueryString["answerSubmitted"] == "true")
                                {
                                    _AnswerSuccessfullySubmitted = true;
                                }
                            }
                        }

                        if (questionID <= 0 && !String.IsNullOrEmpty(Request.QueryString[WebConstants.URL_PARAMETER_NAME_QUESTION_ID]))
                        {
                            int.TryParse(Request.QueryString[WebConstants.URL_PARAMETER_NAME_QUESTION_ID], out questionID);
                        }
                        this.LoadQuestionAnswer(questionID);
                    }
                    else
                    {
                        g.Transfer("/Applications/Logon/Logon.aspx?DestinationURL=" + HttpUtility.UrlEncode(Request.RawUrl));
                    }
                }
                else
                {
                    g.Transfer("/Applications/Home/default.aspx");
                }
                QuestionModule1.TabbedDisplay = TabbedDisplay;
            }
            catch (Exception ex)
            { g.ProcessException(ex); }
        }

        #region Private Methods
        private void LoadQuestionAnswer(int questionID)
        {
            if (questionID > 0)
            {
                _Question = QuestionAnswerSA.Instance.GetQuestion(questionID);
            }
            
            if (_Question == null)
            {
                //get default random active question
                _Question = QuestionAnswer.QuestionAnswerHelper.GetActiveQuestion(g.Member, g, true);
            }

            if (_Question != null)
            {
                hidQuestionID.Value = _Question.QuestionID.ToString();
                QuestionModule1.Location = Location;                
                QuestionModule1.LoadQuestion(_Question);
                AnswerModule1.Location = Location;
                AnswerModule1.TabbedDisplay = TabbedDisplay;
                AnswerModule1.LoadAnswers(_Question);
            }


        }

        #endregion
    }
}