﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SubmitQuestionPopup.ascx.cs" Inherits="Matchnet.Web.Applications.QuestionAnswer.SubmitQuestionPopup" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>

<form id="Main" method="post" runat="server">
    <div id="questionForm">
        <div id="questionIntro"><mn:Txt ID="Txt1" ResourceConstant="TXT_QUESTION_INTRO" runat="server" /></div>
        <div id="questionConfirmation" style="display:none;" class="notification">
            <mn:Txt ID="questionSubmitConfirmation" ResourceConstant="TXT_QUESTION_CONFIRMATION" runat="server" />
        </div>
        <dl class="clearfix">
            <dt><mn2:FrameworkLabel ID="FrameworkLabel2" AssociatedControlID="questionText" ResourceConstant="TXT_QUESTION" runat="server"/></dt>
            <dd><asp:TextBox ID="questionText" TextMode="MultiLine" runat="server" /> <span id="qterr" style="display:none;" class="notification error"><mn:Txt ID="questionTextError" ResourceConstant="TXT_QUESTION_TEXT_ERROR" runat="server" /></span></dd>
        </dl>
        <div id="send-question-anonymous"><asp:CheckBox ID="cbAnonymous" runat="server" Checked="true" /><mn2:FrameworkLabel ID="FrameworkLabel3" AssociatedControlID="cbAnonymous" ResourceConstant="TXT_ANONYMOUS_CHECK" runat="server" /></div>
        <mn2:FrameworkButton ID="btnSubmitQuestion" ResourceConstant="TXT_SUBMIT_QUESTION" runat="server" CssClass="btn primary large wide" />
    </div>
</form>
<script type="text/javascript">
    __addToNamespace__('spark.qanda', {
        SubmitQuestionText: function(qtId, cbaId, omnitureParams) {
            var isValid = true;
            var questionText = $j.trim($j("#" + qtId).val());
            if(questionText == '') {
                isValid=false;
                $j("#qterr").show();
            } else {
                $j("#qterr").hide();
            }
            var isAnonymous = !$j("#" + cbaId).is(":checked");
            if(isValid) {
                $j.ajax({
                    type: "POST",
                    url: "/Applications/API/QuestionAnswerService.asmx/SubmitQuestionText",
                    data: "{brandID:" +<%=BrandId %>+ "," +
                    "questionText:\"" +spark.util.uniescape(questionText)+ "\"," +
                    "memberID:"+<%=MemberId %> +","+
                    "isAnonymous:"+isAnonymous+"}",                    
                    contentType: "application/json; charset=ISO-8859-1",
                    dataType: "json",
                    success: function(msg, status) {
                        if (msg && msg.d) {                            
                            if (omnitureParams) {
                                var overwrite = omnitureParams.Overwrite;
                                if (omnitureParams.Event) {
                                    spark.tracking.addEvent(omnitureParams.Event, overwrite);
                                }
                                if (omnitureParams.PageName) {
                                    spark.tracking.addPageName(omnitureParams.PageName, true);
                                }
                                $j.each(omnitureParams.Props, function(i, prop) {
                                    spark.tracking.addProp(prop.Num, prop.Value, overwrite);
                                });
                                spark.tracking.track();
                            }
                            $j("#questionIntro").hide();
                            $j("#questionConfirmation").show();
                            $j("textarea", "#questionForm").val('');
                        } else {
                        }
                    },
                    error: function(msg, status) {}
                });
            }
            return isValid; 
        },
        __init__: function() {
        }
    });

    $j(document).ready(function() {
        $j("#<%=this.btnSubmitQuestion.ClientID %>").click(function(evt) {
            var myFunc=function() {
                return spark.qanda.SubmitQuestionText("<%=this.questionText.ClientID %>","<%=this.cbAnonymous.ClientID %>",null);
            };
            spark.util.ajaxCallWithBlock('questionForm',{ 
                  message: '<%=GetResourceForJS("HTML_BLOCK_ELEMENT_MSG") %>',
                  centerY: false,
                  css: {backgroundColor: 'transparent', border: 'none', top:'10%'},
                  overlayCSS: {backgroundColor: '#fff', opacity: 0.8 }
            },myFunc,'/Applications/API/QuestionAnswerService.asmx/SubmitQuestionText');            
            return false;
        });
    });
</script>
