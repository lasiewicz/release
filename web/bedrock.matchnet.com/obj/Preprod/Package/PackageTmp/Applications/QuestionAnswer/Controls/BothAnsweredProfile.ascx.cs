﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Member.ValueObjects.Interfaces;
using Matchnet.Member.ValueObjects.MemberDTO;
using Matchnet.Web.Applications.MemberProfile;
using Matchnet.Web.Framework;
using Matchnet.PremiumServiceSearch.ValueObjects.QuestionAnswer;
using Matchnet.Web.Applications.ColorCode;
using Matchnet.Web.Framework.Managers;
using Matchnet.Web.Framework.Ui;
using Matchnet.Member.ServiceAdapters;
using Matchnet.MemberLike.ValueObjects;
using Matchnet.Web.Applications.MemberLike;

namespace Matchnet.Web.Applications.QuestionAnswer.Controls
{
    public partial class BothAnsweredProfile : FrameworkControl
    {
        protected IMemberDTO _MemberProfile;
        private int _ordinal;
        private Answer _Answer;
        private Answer _MemberAnswer;
        private BreadCrumbHelper.EntryPoint _myEntryPoint = BreadCrumbHelper.EntryPoint.QuestionPage;
        private string _viewProfileUrl;
        private bool _isHighlighted = false;
        protected Color _Color = Color.none;
        protected string _ColorText = "";
        protected Color _Color2 = Color.none;
        protected string _ColorText2 = "";

        public bool IsCommentsEnabled
        {
            get { return QuestionAnswerHelper.IsQuestionAnswerCommentEnabled(g.Brand); }
        }

        public bool IsLoggedIn
        {
            get { return null != g.Member && g.Member.MemberID > 0; }
        }

        public bool IsMemberLikeEnabled
        {
            get { return MemberLikeHelper.Instance.IsMemberLikeEnabled(g.Brand); }
        }

        public int Ordinal
        {
            get { return (_ordinal); }
            set { _ordinal = value; }
        }

        public IMemberDTO MemberProfile
        {
            get { return _MemberProfile; }
            set { _MemberProfile = value; }
        }

        public int MemberLikeId
        {
            get
            {
                if (g.Member != null && g.Member.MemberID > 0)
                {
                    return g.Member.MemberID;
                }
                return 0;
            }
        }

        public int AnswerId
        {
            get { return _Answer.AnswerID; }
        }

        public int QuestionId
        {
            get { return _Answer.QuestionID; }
        }

        public int MemberId
        {
            get
            {
                if (MemberProfile != null && MemberProfile.MemberID > 0)
                {
                    return MemberProfile.MemberID;
                }
                return 0;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        #region Public methods
        public void LoadAnswerProfile(IMemberDTO _member, Answer answer, Answer loggedInMemberAnswer)
        {
            try
            {
                _Answer = answer;
                _MemberAnswer = loggedInMemberAnswer;
                MemberProfile = _member;

                lnkEditQuestion.NavigateUrl = "/Applications/MemberProfile/ViewProfile.aspx?EntryPoint=99999&" +
                                              WebConstants.URL_PARAMETER_NAME_PROFILETAB + "=" +
                                              ProfileTabEnum.Relationship.ToString("d");

                //member answer
                literalMemberAnswer.Text = _Answer.AnswerValue.Replace(Environment.NewLine, "<br />");
                literalMemberAnswer2.Text = _MemberAnswer.AnswerValue.Replace(Environment.NewLine, "<br />");
                if (_MemberAnswer.AnswerStatus == QuestionAnswerEnums.AnswerStatusType.Pending)
                {
                    phPendingAnswer.Visible = true;
                }

                Literal likeQuestionId = this.FindControl("likeQuestionId") as Literal;
                likeQuestionId.Text = _Answer.QuestionID.ToString();

                //member profile info
                //MemberPhotoHelper photoHelper = new MemberPhotoHelper(RuntimeSettings.Instance);
                SearchResultProfile searchResult = new SearchResultProfile(MemberProfile, Ordinal);
                noPhoto.NoPhotoFileName = MemberPhotoDisplayManager.Instance.GetNoPhotoFile(NoPhotoImageType.TinyThumbV2, true);
                searchResult.SetThumb(imgThumb, noPhoto, string.Empty);
                SearchResultProfile searchResult2 = new SearchResultProfile(g.Member, Ordinal);
                noPhoto2.NoPhotoFileName = MemberPhotoDisplayManager.Instance.GetNoPhotoFile(NoPhotoImageType.TinyThumbV2, true);
                searchResult2.SetThumb(imgThumb2, noPhoto2, string.Empty);

                //color code
                if (ColorCodeHelper.IsColorCodeEnabled(g.Brand) && ColorCodeHelper.HasMemberCompletedQuiz(MemberProfile, g.Brand) && !ColorCodeHelper.IsMemberColorCodeHidden(MemberProfile, g.Brand))
                {
                    phColorCode.Visible = true;
                    _Color = ColorCodeHelper.GetPrimaryColor(MemberProfile, g.Brand);
                    _ColorText = ColorCodeHelper.GetFormattedColorText(_Color);
                }

                if (ColorCodeHelper.IsColorCodeEnabled(g.Brand) && ColorCodeHelper.HasMemberCompletedQuiz(g.Member, g.Brand) && !ColorCodeHelper.IsMemberColorCodeHidden(g.Member, g.Brand))
                {
                    phColorCode2.Visible = true;
                    _Color2 = ColorCodeHelper.GetPrimaryColor(g.Member, g.Brand);
                    _ColorText2 = ColorCodeHelper.GetFormattedColorText(_Color2);
                }

                lastTimeAction.LoadLastTime(_Answer.UpdateDate);
                lastTimeAction2.LoadLastTime(_MemberAnswer.UpdateDate);

                if (IsCommentsEnabled)
                {
                    buttonAnswerId.ID = string.Format("|{0}|{1}|{2}", AnswerId, MemberProfile.MemberID, _Answer.QuestionID);
                }

                if (IsMemberLikeEnabled)
                {
                    MemberLikeParams likeParams = new MemberLikeParams();
                    likeParams.SiteId = g.Brand.Site.SiteID;
                    likeParams.MemberId = MemberProfile.MemberID;
                    List<MemberLikeObject> likes = MemberLikeHelper.Instance.GetLikesByObjectIdAndType(likeParams, answer.AnswerID, Convert.ToInt32(LikeObjectTypes.Answer));
                    int likeCount = 0;
                    bool hasLike = false;
                    if (null != likes)
                    {
                        likes = likes.FindAll(delegate(MemberLikeObject likeObject)
                        {
                            //TODO: Create MemberLike DTO
                            IMemberDTO aMember = MemberDTOManager.Instance.GetIMemberDTO(likeObject.MemberId, g, MemberType.Search, MemberLoadFlags.None);
                            bool b = !MemberLikeHelper.Instance.IsMemberSuppressed(aMember, g);
                            return b;
                        });
                        MemberLikeObject match = likes.Find(delegate(MemberLikeObject obj)
                        {
                            bool b = (obj.MemberId == g.Member.MemberID);
                            return b;
                        });
                        hasLike = (null != match);
                        likeCount = likes.Count;
                    }

                    string likeNumber = string.Empty;
                    if (likeCount > 0)
                    {
                        likeNumber = string.Format(g.GetResource("TXT_LIKE_NUM", this), likeCount);
                    }

                    string likeResKey = (hasLike) ? "TXT_ANSWER_UNLIKE" : "TXT_ANSWER_LIKE";
                    likeTxt.Text = string.Format(g.GetResource(likeResKey, this), likeNumber);
                    likeTxt.Visible = true;
                }
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }

        }


        #endregion
    }

}