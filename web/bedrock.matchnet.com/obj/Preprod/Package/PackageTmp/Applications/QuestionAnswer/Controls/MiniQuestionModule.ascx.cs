﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Web.Framework;
using Matchnet.PremiumServiceSearch.ValueObjects.QuestionAnswer;
using Matchnet.PremiumServiceSearch.ServiceAdapters;
using Matchnet.UserNotifications.ServiceAdapters;
using Matchnet.UserNotifications.ValueObjects;
using Matchnet.Web.Applications.UserNotifications;
using Matchnet.List.ServiceAdapters;
using Matchnet.List.ValueObjects;
using Matchnet.Web.Applications.MemberLike;

namespace Matchnet.Web.Applications.QuestionAnswer.Controls
{
    public partial class MiniQuestionModule : FrameworkControl
    {
        private Matchnet.PremiumServiceSearch.ValueObjects.QuestionAnswer.Question _Question;
        protected string _WatermarkText = "";
        private QuestionHandler _questionHandler = new QuestionHandler();
        private const int DEFAULT_PAGE_SIZE = 5;
        private int _PageSize = Constants.NULL_INT;

        public bool MemberAnswerFlag { get; set; }
        public int PageSize
        {
            get
            {
                if (_PageSize == Constants.NULL_INT)
                {
                    _PageSize = DEFAULT_PAGE_SIZE;
                    int settingPageSize = SettingsManager.GetSettingInt(SettingConstants.QANDA_ANSWER_PAGE_SIZE, g.Brand);
                    if (settingPageSize != Constants.NULL_INT)
                    {
                        _PageSize = settingPageSize;
                    }
                }
                return _PageSize;
            }
            set { _PageSize = value; }
        }

        public bool IsLoggedIn
        {
            get { return null != g.Member && g.Member.MemberID > 0; }
        }
        
        public string AnswerElement
        {
            get
            {
                return _questionHandler.AnswerElement;
            }
        }

        public int MemberId
        {
            get
            {
                if (g.Member != null && g.Member.MemberID > 0)
                {
                    return g.Member.MemberID;
                }
                return 0;
            }
        }

        public bool IsQandaNextQuestionEnabled()
        {
            return QuestionAnswerHelper.IsQuestionAnswerNextQuestionEnabled(g.Brand) && IsLoggedIn;
        }

        public string GetResourceForJS(string resConst)
        {
            return FrameworkGlobals.JavaScriptEncode(g.GetResource(resConst, new string[0], true, this));
        }

        #region Event Handlers
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            this.btnSubmitQuestion.Click += new EventHandler(btnSubmitQuestion_Click);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            _WatermarkText = g.GetResource("TXT_QUESTIONANSWER_WATERMARK", txtAnswer1).Trim();

            //Temp Fix to support posting answers from UI Templating, templates are question.1.tmpl.htm and question2.tmpl.htm
            if (Page.IsPostBack && Request.Form["hidQuestionUITemplate"] != null && Request.Form["_ctl0:_ctl6:MiniQuestionModule1:btnSubmitQuestion"] != null)
            {
                int questionID = 0;
                int.TryParse(Request.Form["_ctl0:_ctl6:MiniQuestionModule1:hidQuestionID"], out questionID);
                _Question = QuestionAnswerSA.Instance.GetQuestion(questionID);
                _questionHandler.Init(g, _Question, this, this);
                string answerText = _questionHandler.GetAnswerTextUITemplate(Request);

                ProcessSubmitQuestion(questionID, answerText);
            }
        }

        protected void btnSubmitQuestion_Click(object sender, EventArgs e)
        {
            try
            {
                //save answer
                int questionID = Constants.NULL_INT;
                questionID = Convert.ToInt32(Request.Form[hidQuestionID.UniqueID]);
                _Question = QuestionAnswerSA.Instance.GetQuestion(questionID);
                _questionHandler.Init(g, _Question, this, this);
                string answerText = _questionHandler.GetAnswerText(Request);

                ProcessSubmitQuestion(questionID, answerText);
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }

        }


        #endregion

        #region Public methods
        public void LoadQuestion(Matchnet.PremiumServiceSearch.ValueObjects.QuestionAnswer.Question question)
        {
            try
            {
                _Question = question;
                _questionHandler.Init(g, question, this, this);

                //question text
                literalQuestionText.Text = _Question.Text;
                hidQuestionID.Value = _Question.QuestionID.ToString();
                lnkQuestion.NavigateUrl = "/Applications/QuestionAnswer/Question.aspx?" + WebConstants.URL_PARAMETER_NAME_QUESTION_ID + "=" + question.QuestionID.ToString();
                
                lnkSendToFriend.NavigateUrl += "?" + WebConstants.URL_PARAMETER_NAME_QUESTION_ID + "=" + question.QuestionID.ToString();
                lnkSendToFriend2.NavigateUrl += "?" + WebConstants.URL_PARAMETER_NAME_QUESTION_ID + "=" + question.QuestionID.ToString();

                lnkNextQuestion.Visible = IsQandaNextQuestionEnabled();
                FrameworkLiteral4.Visible = IsQandaNextQuestionEnabled();
                lnkNextQuestion2.Visible = IsQandaNextQuestionEnabled();
                FrameworkLiteral3.Visible = IsQandaNextQuestionEnabled();

                //determine if user has already answered the question
                Answer memberAnswer = QuestionAnswerHelper.GetMemberAnswer(g.Member.MemberID, question);
                if (memberAnswer == null)
                {
                    _questionHandler.DisplayUnAnsweredQuestion();
                    phNotAnswered.Visible = true;
                    phAnswered.Visible = false;
                    this.btnSubmitQuestion.OnClientClick = "return spark.qanda.ValidateQAnswer('" + _questionHandler.AnswerElementId + "');";
                }
                else
                {
                    _questionHandler.DisplayAnsweredQuestion(memberAnswer);
                    phNotAnswered.Visible = false;
                    phAnswered.Visible = true;

                    if (memberAnswer.AnswerStatus != QuestionAnswerEnums.AnswerStatusType.Pending)
                    {
                        MemberLikeAnswerClientModule memberLikeClientModule = this.FindControl("memberLikeClientModule") as MemberLikeAnswerClientModule;
                        if (null != memberLikeClientModule)
                        {
                            memberLikeClientModule.MyEntryPoint = Matchnet.Web.Framework.Ui.BreadCrumbHelper.EntryPoint.MemberLikeLink;
                            memberLikeClientModule.LoadMemberLikeClentModule(memberAnswer);
                        }
                    }

                    MemberAnswerFlag = true;
                }
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        #endregion

        #region Private Methods
        private void ProcessSubmitQuestion(int questionID, string answerText)
        {
            try
            {
                //save answer
                if (!string.IsNullOrEmpty(answerText.Trim()) && !answerText.Trim().Equals(g.GetResource("TXT_QUESTIONANSWER_WATERMARK", txtAnswer1).Trim()))
                {
                    QuestionAnswerSA.Instance.AddAnswer(g.Member.MemberID, g.Brand.Site.SiteID, questionID, answerText);
                    //clear logged in member's qandas from cache
                    QuestionAnswerHelper.ResetLoggedInMemberQuestions(g);

                    if (UserNotificationFactory.IsUserNotificationsEnabled(g))
                    {
                        Matchnet.List.ServiceAdapters.List list = ListSA.Instance.GetList(g.Member.MemberID);
                        int totalFavorites = list.GetCount(HotListCategory.WhoAddedYouToTheirFavorites, g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID);
                        Int32 temp = 0;
                        System.Collections.ArrayList whoAddedYou = list.GetListMembers(HotListCategory.WhoAddedYouToTheirFavorites, g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, 1, 500, out temp);

                        foreach (Int32 whoAddedYouId in whoAddedYou)
                        {
                            UserNotificationParams unParams = UserNotificationParams.GetParamsObject(whoAddedYouId, g.Brand.Site.SiteID, g.Brand.Site.Community.CommunityID);
                            UserNotification notification = UserNotificationFactory.Instance.GetUserNotification(new FavoriteAnsweredQandAUserNotification(), whoAddedYouId, g.Member.MemberID, g);
                            if (notification.IsActivated())
                            {
                                UserNotificationsServiceSA.Instance.AddUserNotification(unParams, notification.CreateViewObject());
                            }
                        }
                    }

                    //redirect to question detail page
                    g.Transfer("/Applications/QuestionAnswer/Question.aspx?" + WebConstants.URL_PARAMETER_NAME_QUESTION_ID + "=" + questionID.ToString() + "&answerSubmitted=true");

                }
                else
                {
                    g.Transfer("/Applications/QuestionAnswer/Question.aspx?" + WebConstants.URL_PARAMETER_NAME_QUESTION_ID + "=" + questionID.ToString() + "&err=TXT_EMPTY_ANSWER_ERROR");
                }
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }
        #endregion
    }
}