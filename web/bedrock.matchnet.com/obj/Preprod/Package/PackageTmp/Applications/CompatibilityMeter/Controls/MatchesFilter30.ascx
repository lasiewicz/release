﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MatchesFilter30.ascx.cs"
    Inherits="Matchnet.Web.Applications.CompatibilityMeter.Controls.MatchesFilter30" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="cc1" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<div id="sort-online-now" class="clear-floats matches-30">
    <div class="floatInside show-gender" id="_wrapper_GenderMask" style="padding: 5px;" runat="server">
        <mn:Txt runat="server" ID="txtShow" ResourceConstant="TXT_SHOW" />
        <select name="GenderMask" id="GenderMask" runat="server">
        </select>
        <span class="JMselect">
            <mn:Txt runat="server" ID="txtJMselectShow" ResourceConstant="TXT_JMSELECT_SHOW" />
        </span>
    </div>
    <div class="floatInside from-region" id="_wrapper_RegionID" style="padding: 5px;" runat="server">
        <mn:Txt runat="server" ID="txtFrom" ResourceConstant="TXT_FROM" />
        <select name="RegionID" id="RegionID" runat="server">
        </select>
        <span class="JMselect">
            <mn:Txt runat="server" ID="txtJMselectFrom" ResourceConstant="TXT_JMSELECT_FROM" />
        </span>
    </div>
    <div class="floatInside JMinput" style="padding: 5px;">
        <mn:Txt runat="server" ID="txtAges" ResourceConstant="TXT_AGE" />
        <input name="AgeMin" id="AgeMin" type="text" style="width: 20px" size="2" maxlength="2"
            runat="server">
        <mn:Txt runat="server" ID="txtTo" ResourceConstant="TO_" />
        <input name="Application:AgeMax" id="AgeMax" type="text" style="width: 20px" size="2"
            maxlength="2" runat="server">
    </div>
    <div class="floatInside JMsubmit">
        <cc1:FrameworkButton runat="server" ID="btnSearch" CssClass="activityButton" ResourceConstant="SEARCH" />
    </div>
    <span class="JMphotos">
        <input type="checkbox" id="chkPhotoRequired" runat="server" />
        <mn:Txt runat="server" ID="txtJmeterHasPhotos" ResourceConstant="HAS_PHOTOS" />
    </span>
    <div class="floatInside" style="padding: 5px; margin-top: 10px; display: none;">
        <asp:RangeValidator runat="server" ID="AgeMinRange" ControlToValidate="AgeMin" Display="None"></asp:RangeValidator>
        <asp:RangeValidator runat="server" ID="AgeMaxRange" ControlToValidate="AgeMax" Display="None"></asp:RangeValidator>
        <asp:RequiredFieldValidator runat="server" ID="AgeMinReq" ControlToValidate="AgeMin"
            Display="None"></asp:RequiredFieldValidator>
        <asp:RequiredFieldValidator runat="server" ID="AgeMaxReq" ControlToValidate="AgeMax"
            Display="None"></asp:RequiredFieldValidator>
        <asp:ValidationSummary runat="server" ID="valSummary" DisplayMode="BulletList"></asp:ValidationSummary>
    </div>
    <%-- <asp:CompareValidator runat="server" ID="valCompare" ControlToValidate="AgeMin" ControlToCompare="AgeMax"
        Operator="LessThanEqual" Display="None"></asp:CompareValidator>--%>
</div>
