﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Web.Framework;

namespace Matchnet.Web.Applications.CompatibilityMeter.Controls
{
    public partial class JMeterSidebarMatches : FrameworkControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            CompatibilityMeterHandler cmh = new CompatibilityMeterHandler(g);
            if (!cmh.IsJmeterPremium())
            {
                lnkJMeterMatches.NavigateUrl = FrameworkGlobals.GetSubscriptionLink(false, 1410);
            }
        }
    }
}