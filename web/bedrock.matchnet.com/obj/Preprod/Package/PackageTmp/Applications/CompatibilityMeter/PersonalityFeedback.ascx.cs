﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Ui.FormElements;
using Matchnet.Web.Framework.Util;
using Matchnet.Web.Framework.Ui.BasicElements;

using Matchnet.MatchTest.ServiceAdapters;
using Matchnet.MatchTest.ValueObjects.MatchMeter;

namespace Matchnet.Web.Applications.CompatibilityMeter
{
    public partial class PersonalityFeedback : FrameworkControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    var pfb = MatchMeterSA.Instance.GetPersonalFeedback(g.Member.MemberID, g.Brand, MemberPrivilegeAttr.IsCureentSubscribedMember(g));
                    rptFeedbackCategories.DataSource = pfb.FeedbackCategories;
                    rptFeedbackCategories.DataBind();
                }
                catch (Exception ex)
                {
                    g.ProcessException(ex);
                }
            }
        }
    }
}