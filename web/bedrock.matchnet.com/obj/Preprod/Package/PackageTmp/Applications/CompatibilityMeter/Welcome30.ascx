﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Welcome30.ascx.cs" Inherits="Matchnet.Web.Applications.CompatibilityMeter.Welcome30" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="uc1" TagName="SendToAFriend" Src="Controls/SendToAFriend.ascx" %>
<div class="jmeter-welcome">
    <h1>
        <mn:Title runat="server" ID="ttlHeader" ResourceConstant="TTL_HEADER" />
    </h1>
    <div>
        <asp:LinkButton runat="server" OnClick="btnStartTest_Click" ID="btnStartTest">
            <mn:Image ID="Image1" runat="server" FileName="jmeter-welcome-button.png" CssClass="jmeter-welcome-submit"
                AlternateText="J-meter" />
        </asp:LinkButton>
    </div>
    <div>
        <mn:Title runat="server" ID="Title1" ResourceConstant="JMETER_WELCOME_TEXT" />
    </div>
    <p>
        <asp:CheckBox runat="server" ID="cbTerms" Checked="true" />
        <strong>
            <asp:Label runat="server" ID="lblTargetedConfirmTerms"></asp:Label>
        </strong>
        <mn:MultiValidator ID="cbTermsAndConditionsValidator" TabIndex="-1" runat="server"
            RequiredType="CheckBoxType" EnableClientScript="True" ControlToValidate="cbTerms"
            FieldNameResourceConstant="ACCEPTING_TERMS_IS_REQUIRED" IsRequired="True" Display="Dynamic" />
    </p>
    <asp:LinkButton runat="server" OnClick="btnStartTest_Click" ID="btnStartTest2">
        <mn:Image runat="server" FileName="jmeter-welcome-button.png" CssClass="jmeter-welcome-submit" AlternateText="J-meter"/>
    </asp:LinkButton>
</div>
