﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Ui.FormElements;
using Matchnet.Web.Framework.Util;

using Matchnet.MatchTest.ServiceAdapters;
using Matchnet.MatchTest.ValueObjects.MatchMeter;

using Spark.SAL;


namespace Matchnet.Web.Applications.CompatibilityMeter
{
    public partial class Welcome30 : FrameworkControl
    {
        //protected Matchnet.Web.Framework.Title ttlHeader;
        protected void Page_Load(object sender, EventArgs e)
        {
            MatchTestStatus mts = MatchTestStatus.NotTaken;
            try
            {
                //pfb = MatchMeterSA.Instance.GetPersonalFeedback(g.Member.MemberID, g.Brand.Site.LanguageID, MemberPrivilegeAttr.IsCureentSubscribedMember(g));
                mts = MatchMeterSA.Instance.GetMatchTestStatus(g.Member.MemberID, MemberPrivilegeAttr.IsCureentSubscribedMember(g), g.Brand);


                try
                {
                    if (mts == Matchnet.MatchTest.ValueObjects.MatchMeter.MatchTestStatus.NotTaken)
                    {
                        string genderPref = "MAN";
                        int genderMask = g.Member.GetAttributeInt(g.Brand, WebConstants.ATTRIBUTE_NAME_GENDERMASK);
                        if ((genderMask & Matchnet.Lib.ConstantsTemp.GENDERID_FEMALE) == Matchnet.Lib.ConstantsTemp.GENDERID_FEMALE)
                        {
                            genderPref = "WOMAN";
                        }

                        lblTargetedConfirmTerms.Text = g.GetResource(genderPref + "_TXT_CONFIRM_TERMS", this);


                    }
                    else
                    {
                        if (mts == Matchnet.MatchTest.ValueObjects.MatchMeter.MatchTestStatus.NotCompleted)
                        {
                            g.Transfer("/Applications/CompatibilityMeter/PersonalityTest.aspx" + getMemberId());
                        }
                        else // Member have completed the minimum amount of questions 
                        {
                            g.Transfer("/Applications/CompatibilityMeter/ReturningUser.aspx" + getMemberId());
                        }
                    }
                }
                catch (Exception ex)
                {
                    g.ProcessException(ex);
                }
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);

                if (!g.IsDevMode && !(ex is System.Threading.ThreadAbortException))
                {
                    g.Transfer("/Applications/CompatibilityMeter/Error.aspx");
                }
            }

        }

        protected override void OnPreRender(EventArgs e)
        {
            try
            {
                _g.AnalyticsOmniture.PageName = "JMeter Welcome";
                _g.AnalyticsOmniture.Evar31 = "N";
                if (Request.Params.Get("FromInvite") != null)
                {
                    _g.AnalyticsOmniture.Evar31 = "Y";
                }
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }
        private string getMemberId()
        {
            try
            {
                if (Request.Params.Get("MemberID") != null)
                {
                    return "&MemberID=" + Request.Params.Get("MemberID") ;
                }
            }
            catch (FormatException)
            {
                return string.Empty;
            }
            return string.Empty;
        }

        protected void btnStartTest_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                g.Transfer("/Applications/CompatibilityMeter/PersonalityTest.aspx" + "?fromWelcome=1" + getMemberId());
            }
        }
    }
}