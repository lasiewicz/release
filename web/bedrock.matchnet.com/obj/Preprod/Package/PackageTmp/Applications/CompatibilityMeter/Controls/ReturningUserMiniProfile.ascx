﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ReturningUserMiniProfile.ascx.cs"
    Inherits="Matchnet.Web.Applications.CompatibilityMeter.Controls.ReturningUserMiniProfile" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="uc1" TagName="NoPhoto" Src="~/Framework/Ui/PageElements/NoPhoto20.ascx" %>
<li>
    <div class="photo">
        <mn:Image runat="server" ID="imgThumb" ResourceConstant="" />
        <uc1:NoPhoto runat="server" ID="noPhoto" DivCSSClass="ask" Mode="NoPhoto" Visible="False" />
    </div>
    <div class="info">
        <div class="title">
            <asp:HyperLink runat="server" ID="lnkToProfile">
                <asp:Literal runat="server" ID="litUsername"></asp:Literal>
            </asp:HyperLink>
        </div>
        <div class="ynm">

            
            <mn:Image ID="ImageYes" FileName="icon-click-y-off.gif" runat="server" Hspace="3"
                ResourceConstant="YNM_Y_IMG_ALT_TEXT" align="absmiddle" />
            <mn:Txt ID="TxtYes" runat="server" ResourceConstant="YNM_YES" />
            &nbsp;
            <mn:Image ID="ImageMaybe" FileName="icon-click-m-off.gif" runat="server" Hspace="3"
                ResourceConstant="YNM_M_IMG_ALT_TEXT" align="absmiddle" />
            <mn:Txt ID="TxtMaybe" runat="server" ResourceConstant="YNM_MAYBE" />
            &nbsp;
            <mn:Image ID="ImageNo" FileName="icon-click-n-off.gif" runat="server" Hspace="3"
                ResourceConstant="YNM_N_IMG_ALT_TEXT" align="absmiddle" />
            <mn:Txt ID="TxtNo" runat="server" ResourceConstant="YNM_NO" />
            &nbsp;
            <mn:Txt runat="server" ID="TxtThinkYoud" ResourceConstant="PRO_THINK_YOUD" />
            &nbsp;<a id="lnkClick" runat="server"><strong><mn:Txt runat="server" ID="txtClick"
                ResourceConstant="TXT_CLICK_TRADE" />
            </strong></a>
            <mn:Txt runat="server" ID="Txt1" ResourceConstant="PRO_THINK_YOUD_AFTER_CLICK_LINK" />

            <input type="hidden" id="YNMVoteStatus" runat="server" name="YNMVoteStatus" />
        </div>
        <div class="text">
            <asp:Label ID="txtAge" runat="server" />,
            <asp:Literal ID="literalMaritalAndGender" runat="server" />
            <br />
            <asp:Label ID="txtLocation" runat="server" />
        </div>
    </div>
    <div class="right">
        <asp:HyperLink ID="lnkToOneOnOne" class="jmeter-name-title" runat="server">
            J<span style="heart">♥</span>meter 
        </asp:HyperLink>
        <div class="jmeter-clock mini">
            <mn:Image class="title" ID="imgClockMatch" runat="server" FileName="clock/match.png" />
            <asp:HyperLink ID="lnkToOneOnOne2" class="jmeter-name-title" runat="server">
                <mn:Image class="bar" titleResourceConstant="ALT_CLOCK" ID="imgClockBar" runat="server" FileName="clock/5.png" />
            </asp:HyperLink>
            <mn:Image class="pan" ID="imgClockPan" runat="server" FileName="clock/pan.png" />
            <div class="loader">
                <mn:Image ID="clockLoader" runat="server" FileName="ajax-loader.gif" />
            </div>
            <asp:HiddenField runat="server" ID="hfMatchScore" />
        </div>
        <asp:HyperLink runat="server" ID="lnkToProfile2" class="next-button">
            <mn:Image ID="JmeterReturningMatchbuttImage" runat="server" FileName="jmeter-returning-match-butt.png" AlternateText="View Full Profile"/>
        </asp:HyperLink>
    </div>
</li>
