﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BetaPopup.ascx.cs" Inherits="Matchnet.Web.Applications.CompatibilityMeter.BetaPopup" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<div id="pnlMatchMeterInfo" runat="server" class="highlightProfileInfo">
    <div class="highlightProfileInfoClose">
        <asp:HyperLink ID="lnkMatchMeterInfoCloseText" runat="server" Text="close"></asp:HyperLink>
        <asp:HyperLink ID="lnkMatchMeterCloseImage" runat="server" Text="imageClose"></asp:HyperLink>
    </div>
    <p>
        <b>
            <mn:txt id="txtP1" runat="server" resourceconstant="TXT_P_1" expandimagetokens="True" />
        </b>
    </p>
    <p>
        <mn:txt id="txtP2" runat="server" resourceconstant="TXT_P_2" expandimagetokens="True" />
    </p>
    <p>
        <b>
            <mn:txt id="txtP3" runat="server" resourceconstant="TXT_P_3" expandimagetokens="True" />
        </b>
    </p>
    <p>
        <mn:txt id="txtP4" runat="server" resourceconstant="TXT_P_4" expandimagetokens="True" />
    </p>
</div>
