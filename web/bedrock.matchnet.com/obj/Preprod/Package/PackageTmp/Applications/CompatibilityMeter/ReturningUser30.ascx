﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ReturningUser30.ascx.cs"
    Inherits="Matchnet.Web.Applications.CompatibilityMeter.ReturningUser30" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="uc1" TagName="LookupByUsername" Src="Controls/LookupByUsername.ascx" %>
<%@ Register TagPrefix="uc1" TagName="SendToAFriend" Src="Controls/SendToAFriend.ascx" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn" TagName="microProfile" Src="/Framework/Ui/BasicElements/MicroProfile.ascx" %>
<%@ Register TagPrefix="uc2" TagName="MicroProfile20" Src="/Framework/Ui/BasicElements/MicroProfile20.ascx" %>
<%@ Register TagPrefix="uc2" TagName="ReturningUserMiniProfile" Src="~/Applications/CompatibilityMeter/Controls/ReturningUserMiniProfile.ascx" %>
<%@ Register TagPrefix="uc1" TagName="MatchesFilter" Src="Controls/MatchesFilter30.ascx" %>
<uc1:MatchesFilter ID="mf" runat="server" Visible="false" />
<div class="jmeter-returning">
    <h1>
        <mn:Txt ID="txtHeader" runat="server" ResourceConstant="TTL_HEADER" ExpandImageTokens="True" />
    </h1>
    <div class="sub-header">
        <mn:Txt ID="txtSubHeader" runat="server" ResourceConstant="TTL_SUB_HEADER" ExpandImageTokens="True" />
    </div>
    <mn:Txt ID="txtText" runat="server" ResourceConstant="TTL_TEXT" ExpandImageTokens="True" />
    <ul class="small-matches">
        <asp:Repeater ID="rptrYourMatchesList" runat="server" OnItemDataBound="rptrYourMatchesList_ItemDataBound">
            <ItemTemplate>
                <uc2:ReturningUserMiniProfile runat="server" ID="returningUserMiniProfile" Member="<%# Container.DataItem %>">
                </uc2:ReturningUserMiniProfile>
            </ItemTemplate>
        </asp:Repeater>
    </ul>
    <div class="buttons">
        <a href="/Applications/CompatibilityMeter/Completion.aspx?fromreturning=1" class="feedback"></a>
        <asp:HyperLink runat="server" NavigateUrl="/Applications/CompatibilityMeter/Matches.aspx?fromreturning=1" ID="lnkMatches" CssClass="matches"></asp:HyperLink>
    </div>
</div>
<!--
<!-- end Right Div area -->
<iframe tabindex="-1" name="FrameYNMVote" frameborder="0" width="1" scrolling="no"
    height="1">
    <layer name="FrameYNMVote" frameborder="0" scrolling="no" width="1" height="1"></layer>
</iframe>
