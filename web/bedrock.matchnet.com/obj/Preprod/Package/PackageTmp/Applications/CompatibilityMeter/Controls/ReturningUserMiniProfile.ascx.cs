﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Matchnet.List.ValueObjects;
using Matchnet.Member.ServiceAdapters.Interfaces;
using Matchnet.Member.ValueObjects.Interfaces;
using Matchnet.Web.Applications.MemberProfile;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Managers;
using Matchnet.Web.Framework.Ui;
using Matchnet.Web.Framework.Ui.BasicElements;

namespace Matchnet.Web.Applications.CompatibilityMeter.Controls
{
    public partial class ReturningUserMiniProfile : FrameworkControl, IMiniProfile
    {
        MiniProfileHandler _handler = null;

        public IMemberDTO Member { get; set; }
        public decimal MatchScore { get; set; }
        protected string _viewProfileUrl = "";
        protected HotListCategory _hotListCategory = HotListCategory.Default;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                _handler = new MiniProfileHandler(this);

                _handler.SetYNMControls(g);
                hfMatchScore.Value = MatchScore.ToString();

                DateTime birthDate = Member.GetAttributeDate(g.Brand, "BirthDate", DateTime.MinValue);
                txtAge.Text = FrameworkGlobals.GetAge(birthDate).ToString();
                string location = ProfileDisplayHelper.GetRegionDisplay(Member, g);
                txtLocation.Text = FrameworkGlobals.Ellipsis(location, 22, "&hellip;");
                txtLocation.ToolTip = location;
                literalMaritalAndGender.Text = ProfileDisplayHelper.GetMaritalStatusSeekingGenderDisplay(Member, this.g);
                litUsername.Text = Member.GetUserName(g.Brand);
                _viewProfileUrl = "/Applications/MemberProfile/ViewProfile.aspx?MemberID=" + Member.MemberID;
                DisplayMemberPhoto();

                lnkToProfile.NavigateUrl = _viewProfileUrl;
                lnkToProfile2.NavigateUrl = _viewProfileUrl;
                lnkToOneOnOne.NavigateUrl = "/Applications/CompatibilityMeter/OneOnOne.aspx?MemberID=" + Member.MemberID;
                lnkToOneOnOne2.NavigateUrl = "/Applications/CompatibilityMeter/OneOnOne.aspx?MemberID=" + Member.MemberID;
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        private void DisplayMemberPhoto()
        {
            imgThumb.NavigateUrl = _viewProfileUrl;

            bool isMale = FrameworkGlobals.IsMaleGender(Member, g.Brand);
            imgThumb.ImageUrl = MemberPhotoDisplayManager.Instance.GetNoPhotoFile(NoPhotoImageType.Thumb, isMale);

            var photo = MemberPhotoDisplayManager.Instance.GetDefaultPhoto(g.Member, Member, g.Brand);
            if (photo == null)
            {
                imgThumb.Visible = false;
                noPhoto.Visible = true;
                noPhoto.MemberID = Member.MemberID;
                noPhoto.Member = Member;
                noPhoto.DestURL = _viewProfileUrl;

                noPhoto.NoPhotoFileName = MemberPhotoDisplayManager.Instance.GetNoPhotoFile(NoPhotoImageType.BackgroundThumb, true);
                noPhoto.PrivatePhotoFileName = MemberPhotoDisplayManager.Instance.GetNoPhotoFile(NoPhotoImageType.BackgroundThumb, true);
            }
            else
            {
                imgThumb.ImageUrl = MemberPhotoDisplayManager.Instance.GetPhotoDisplayURL(g.Member, Member, g.Brand,
                                                                       photo, PhotoType.Thumbnail,
                                                                       PrivatePhotoImageType.Thumb,
                                                                        NoPhotoImageType.Thumb);
           
            }
        }

        public Matchnet.Web.Framework.Image IMGYes { get { return ImageYes; } }
        public Matchnet.Web.Framework.Image IMGMaybe { get { return ImageMaybe; } }
        public Matchnet.Web.Framework.Image IMGNo { get { return ImageNo; } }
        //public Matchnet.Web.Framework.Image IMGBothSaidYes { get { return imgBothSaidYes; } }
        public Matchnet.Web.Framework.Txt TXTYes { get { return TxtYes; } }
        public Matchnet.Web.Framework.Txt TXTMaybe { get { return TxtMaybe; } }
        public Matchnet.Web.Framework.Txt TXTNo { get { return TxtNo; } }
        //  public HtmlInputHidden YNMVOTEStatus { get { return YNMVoteStatus; } }
        //        public HtmlTableCell YNMBackgroundAnimation { get { return tdAnimBG; } }

        public SearchResultProfile SearchResult
        {
            get { throw new NotImplementedException(); }
        }

        public FrameworkControl ThisControl
        {
            get { throw new NotImplementedException(); }
        }

        public Literal TXTGender
        {
            get { throw new NotImplementedException(); }
        }

        public Literal TXTAge
        {
            get { throw new NotImplementedException(); }
        }

        public Literal TXTRegion
        {
            get { throw new NotImplementedException(); }
        }

        public Literal TXTHeadline
        {
            get { throw new NotImplementedException(); }
        }

        public HyperLink LNKUserName
        {
            get { throw new NotImplementedException(); }
        }

        public Framework.Image IMGIsNew
        {
            get { throw new NotImplementedException(); }
        }

        public Framework.Image IMGIsUpdate
        {
            get { throw new NotImplementedException(); }
        }

        public Framework.Image IMGThumb
        {
            get { throw new NotImplementedException(); }
        }

        public Literal LITContainer
        {
            get { throw new NotImplementedException(); }
        }

        public Framework.Image IMGPremiumAuthenticated
        {
            get { return new Framework.Image(); }
        }

        public HighlightProfileInfoDisplay UCHighlightProfileInfoDisplay
        {
            get { throw new NotImplementedException(); }
        }

        public LastLoginDate LastLoginDate
        {
            get { throw new NotImplementedException(); }
        }

        public Panel PNLComments
        {
            get { throw new NotImplementedException(); }
        }

        public PlaceHolder PLCBreaks
        {
            get { throw new NotImplementedException(); }
        }

        public System.Web.UI.HtmlControls.HtmlInputText TXTFavoriteNote
        {
            get { throw new NotImplementedException(); }
        }

        public Framework.Ui.FormElements.FrameworkButton BTNSave
        {
            get { throw new NotImplementedException(); }
        }

        public LinkButton LBRemoveProfile
        {
            get { throw new NotImplementedException(); }
        }

        public Txt TXTMore
        {
            get { throw new NotImplementedException(); }
        }

        public Framework.Image IMGChat
        {
            get { throw new NotImplementedException(); }
        }

        public HyperLink LNKOnline
        {
            get { throw new NotImplementedException(); }
        }

        public Txt TXTOnline
        {
            get { throw new NotImplementedException(); }
        }

        public Framework.Image IMGBothSaidYes
        {
            get { throw new NotImplementedException(); }
        }

        public System.Web.UI.HtmlControls.HtmlInputHidden YNMVOTEStatus
        {
            get { return YNMVoteStatus; }
        }

        public System.Web.UI.HtmlControls.HtmlTableCell YNMBackgroundAnimation
        {
            get { return new HtmlTableCell(); }
        }

        public Framework.Image IMGGame
        {
            get { throw new NotImplementedException(); }
        }

        public HyperLink LNKGame
        {
            get { throw new NotImplementedException(); }
        }

        public Txt TXTGame
        {
            get { throw new NotImplementedException(); }
        }

        public ResultContextType DisplayContext
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public string DefaultNote
        {
            get { throw new NotImplementedException(); }
        }

        public bool IsHotListFriend
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public bool Transparency
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public string CategoryID
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public int Counter
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public int MemberID
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public int Ordinal
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public Framework.Ui.BreadCrumbHelper.EntryPoint MyEntryPoint
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public Framework.Ui.MOCollection MyMOCollection
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public List.ValueObjects.HotListCategory HotListCategory
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public bool EnableSingleSelect
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public string Note
        {
            get { throw new NotImplementedException(); }
        }

        public bool CurrentlyOnline
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public bool IsHighlighted
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public bool IsSpotlight
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public bool OverrideSelfSpotlight
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public bool IsPromotionalMember
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public bool IsMatchMeterEnabled
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public string ViewProfileURL
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public string YesFileNameSelected
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public string NoFileNameSelected
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public string MaybeFileNameSelected
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }
    }
}