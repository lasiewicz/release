﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="JMeterSidebar.ascx.cs"
    Inherits="Matchnet.Web.Applications.CompatibilityMeter.Controls.JMeterSidebar" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="uc1" TagName="SendToAFriend" Src="~/Applications/CompatibilityMeter/Controls/SendToAFriend.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Matches" Src="~/Applications/CompatibilityMeter/Controls/JMeterSidebarMatches.ascx" %>
<%@ Register TagPrefix="uc1" TagName="MOL" Src="~/Applications/CompatibilityMeter/Controls/JMeterSidebarMOL.ascx" %>
<%@ Register TagPrefix="mn" TagName="AdUnit" Src="/Framework/UI/Advertising/AdUnit.ascx" %>
<div id="jmeter-side">
    <div class="jmater-side-content">
        <asp:PlaceHolder runat="server" ID="plcJMeterLogo">
            <!--<img src="/img/site/jdate-co-il/jmeter-logo.png" />-->
            <mn:Image runat="server" ID="imgJmeterLogo" FileName="jmeter-logo.png" />
        </asp:PlaceHolder>
        <asp:PlaceHolder runat="server" ID="plcBanner" Visible="false">
            <%--<mn:Image runat="server" ID="imgReturnUpgrade" FileName="returning-banner.png" />
            <mn:Image runat="server" ID="imgReturnPremium" FileName="returning-banner-prm.png" />--%>
            <asp:HyperLink runat="server" ID="lnkBanner">
                <mn:Image runat="server" ID="imgBanner" />
            </asp:HyperLink>
        </asp:PlaceHolder>
        <asp:PlaceHolder runat="server" ID="plcJMeterMatches">
            <uc1:Matches runat="server" ID="matches" />
        </asp:PlaceHolder>
        <asp:PlaceHolder runat="server" ID="plcAdUnitTop" Visible="False">
            <mn:AdUnit GAMAdSlot="sidebar_300x250" runat="server" GAMPageMode="None" ID="adUnitOverlayTop">
            </mn:AdUnit>
        </asp:PlaceHolder>
        <asp:PlaceHolder runat="server" ID="plcMOL">
            <uc1:MOL runat="server" ID="mol" />
        </asp:PlaceHolder>
        <asp:PlaceHolder runat="server" ID="plcRaananPhoto">
            <!--<img src="/img/site/jdate-co-il/raanan.jpg" />-->
            <mn:Image runat="server" ID="imgRaananHess" FileName="raanan.jpg" CssClass="jmeter-raanan-photo" />
        </asp:PlaceHolder>
        <asp:PlaceHolder runat="server" ID="plcJMeterInvite">
            <div class="jmeter-invite">
                <uc1:SendToAFriend ID="SendToAFriend" runat="server"></uc1:SendToAFriend>
            </div>
        </asp:PlaceHolder>
        <asp:PlaceHolder runat="server" ID="plcPrintFeedback">
            <div class="jmeter-print" onclick="printdiv('div_print');">
                <!--<img src="/img/site/jdate-co-il/jmeter-print-icon.png" />-->
                <mn:Image runat="server" ID="imgPrintIcon" FileName="jmeter-print-icon.png" />
                <span>
                    <mn:Txt runat="server" ID="txtP2" ResourceConstant="TTL_JMETER_PRINT" />
                </span>
            </div>
        </asp:PlaceHolder>
        <asp:PlaceHolder runat="server" ID="plcAdUnit">
            <mn:AdUnit GAMAdSlot="sidebar_300x250" runat="server" GAMPageMode="None" ID="adUnitOverlay">
            </mn:AdUnit>
        </asp:PlaceHolder>
    </div>
    <div style="clear: both;">
    </div>
</div>
