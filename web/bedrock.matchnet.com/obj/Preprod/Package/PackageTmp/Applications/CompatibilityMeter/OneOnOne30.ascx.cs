﻿using System;
using System.IO;
using System.Linq;
using System.Web;
using Matchnet.Web.Applications.CompatibilityMeter.Controls;
using Matchnet.Web.Framework;
using Matchnet.Member.ServiceAdapters;
using Matchnet.MatchTest.ServiceAdapters;
using Matchnet.MatchTest.ValueObjects.MatchMeter;
using Matchnet.List.ServiceAdapters;
using Matchnet.List.ValueObjects;
using Matchnet.Content.ValueObjects.PageConfig;

namespace Matchnet.Web.Applications.CompatibilityMeter
{
    public partial class OneOnOne30 : FrameworkControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                /*
                // Checking if a subscription is requiered to view this page.
                int mmsett = Int32.Parse(RuntimeSettings.GetSetting("MatchTest_App_Settings", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID));
                int mmenumval = (Int32)WebConstants.MatchMeterFlags.PayFor1On1;
                if ((mmsett & mmenumval) == mmenumval && !MemberPrivilegeAttr.IsCureentSubscribedMember(g))
                {
                    // If a sub is needed, redirect to sub page.
                    g.Transfer("/Applications/Subscription/Subscribe.aspx");
                    //Redirect.Subscription(g.Brand, prtid, destinationMemberID, false, Server.UrlEncode(viewProfileLink));
                }*/

                CompatibilityMeterHandler cmh = new CompatibilityMeterHandler(g);
                cmh.RedirectIfNotAllowed(CompatibilityMeterHandler.JmeterPage.OneOnOne);
                /* else
                 {*/
                int memberID = getMemberId();
                if (memberID > 0)
                {
                    Member.ServiceAdapters.Member aMember = MemberSA.Instance.GetMember(memberID, MemberLoadFlags.None);
                    //   microProfile.Member = aMember;




                    var ooofb = MatchMeterSA.Instance.GetOneOnOne(g.Member.MemberID, MemberPrivilegeAttr.IsCureentSubscribedMember(g), memberID, aMember.IsPayingMember(g.Brand.Site.SiteID), g.Brand);

                    oneOnOneTop.OOOFB = ooofb;
                    oneOnOneTop.ViewdMember = aMember;

                    // Check if current member finished the test
                    if (ooofb.MatchStatus != OneOnOneStatus.RequestUserNotCompleted && ooofb.MatchStatus != OneOnOneStatus.RequestUserNotTaken)
                    {
                        // Check if the targat member finished the test
                        if (ooofb.MatchStatus != OneOnOneStatus.MatchMemberNotTaken && ooofb.MatchStatus != OneOnOneStatus.MatchMemberNotCompleted)
                        {
                            if (!((g.Member.GetAttributeInt(g.TargetBrand, "HideMask", 0) & (Int32)WebConstants.AttributeOptionHideMask.HideHotLists) == (Int32)WebConstants.AttributeOptionHideMask.HideHotLists))
                            {
                                ListSA.Instance.AddListMember(HotListCategory.WhoJmeteredYou,
                                g.Brand.Site.Community.CommunityID,
                                g.Brand.Site.SiteID,
                                memberID,
                                g.Member.MemberID,
                                null,
                                Constants.NULL_INT,
                                true,
                                true);
                            }

                            try
                            {
                                ListSA.Instance.AddListMember(HotListCategory.MemberYouJmetered,
                                g.Brand.Site.Community.CommunityID,
                                g.Brand.Site.SiteID,
                                g.Member.MemberID,
                                memberID,
                                null,
                                Constants.NULL_INT,
                                true,
                                true);
                            }
                            catch (Exception ex1)
                            {
                                g.ProcessException(ex1);
                            }



                            rptFeedbackCategories.DataSource = ooofb.OneOnOneCategories;
                            rptFeedbackCategories.DataBind();

                            // Next prev jmeter matches
                            try
                            {
                                int ordinal;
                                if (int.TryParse(Request.QueryString["Ordinal"], out ordinal) && cmh.IsJMeterMatchesEnabled())
                                {
                                    // Get JMeter matches preferences
                                    System.Web.UI.Page page = new System.Web.UI.Page();
                                    MatchesFilter30 mf = (MatchesFilter30)page.LoadControl("/Applications/CompatibilityMeter/Controls/MatchesFilter30.ascx");
                                    System.Web.UI.HtmlControls.HtmlForm Form1 = new System.Web.UI.HtmlControls.HtmlForm();
                                    Form1.Controls.Add(mf);
                                    page.Controls.Add(Form1);
                                    StringWriter sw = new StringWriter();
                                    HttpContext.Current.Server.Execute(page, sw, false);

                                    // Get Jmeter matches from API


                                    if (ordinal > 1)
                                    {
                                        // Show next + Previous
                                        var bmresults = MatchMeterSA.Instance.GetBestMatches(_g.Member.MemberID, _g.Brand, mf.intRegionID, mf.intAgeMin, mf.intAgeMax, mf.GetGenderMask(), MemberPrivilegeAttr.IsCureentSubscribedMember(_g), ordinal - 1, 3, mf.PhotoRequired);

                                        if (bmresults != null && bmresults.MatchesCount > 0)
                                        {
                                            lnkNextProfile.NavigateUrl = "/Applications/CompatibilityMeter/OneOnOne.aspx?MemberID=" + bmresults.MatchResultsDic.Keys.ElementAt(2).MemberID + "&Ordinal=" + (ordinal + 1);
                                            lnkPrevProfile.NavigateUrl = "/Applications/CompatibilityMeter/OneOnOne.aspx?MemberID=" + bmresults.MatchResultsDic.Keys.First().MemberID + "&Ordinal=" + (ordinal - 1);
                                        }
                                    }
                                    else
                                    {
                                        // Show next 
                                        var bmresults = MatchMeterSA.Instance.GetBestMatches(_g.Member.MemberID, _g.Brand, mf.intRegionID, mf.intAgeMin, mf.intAgeMax, mf.GetGenderMask(), MemberPrivilegeAttr.IsCureentSubscribedMember(_g), ordinal + 1, 3, mf.PhotoRequired);

                                        if (bmresults != null && bmresults.MatchesCount > 0)
                                        {
                                            lnkNextProfile.NavigateUrl = "/Applications/CompatibilityMeter/OneOnOne.aspx?MemberID=" + bmresults.MatchResultsDic.Keys.First().MemberID + "&Ordinal=" + (ordinal + 1);
                                        }
                                        plcPrevProfile.Visible = false;
                                    }
                                }
                                else
                                {
                                    plcNextProfile.Visible = false;
                                    plcPrevProfile.Visible = false;
                                }
                            }
                            catch (Exception ex1)
                            {
                                plcNextProfile.Visible = false;
                                plcPrevProfile.Visible = false;
                            }
                        }
                        else
                        {
                            // redirect to the send invitation page

                            if (CompatibilityMeterHandler.IsJMeterV3(g.Brand))
                            {
                                g.Transfer("/Applications/MemberProfile/ViewProfile.aspx?ProfileTab=JMeter&MemberID=" +
                                           memberID.ToString());
                            }
                            else
                            {
                                g.Transfer("/Applications/CompatibilityMeter/SendInvitation.aspx?MemberID=" + memberID.ToString());    
                            }
                        }
                    }
                    else
                    {
                        // Current member hasn't finished the test
                        g.Transfer("/Applications/CompatibilityMeter/Welcome.aspx?MemberID=" + memberID.ToString());
                    }
                }
                string matchesPageURL = FrameworkGlobals.LinkHref("/Applications/CompatibilityMeter/Matches.aspx", false);
                //   lnkYourMatches1.NavigateUrl = matchesPageURL;
                // If we're in a popup, open the matches page in a new window
                if (g.LayoutTemplate == LayoutTemplate.Popup || g.LayoutTemplate == LayoutTemplate.PopupProfile)
                {
                    //   lnkYourMatches1.Attributes.Add("onclick", string.Format("javascript:window.open('{0}', 'matches_window', 'height=664,width=780,status=yes,toolbar=yes,menubar=yes,location=yes, resizable=yes, scrollbars=1');", matchesPageURL));
                    //   lnkYourMatches1.NavigateUrl = string.Empty;
                }

                //    lnkCompletion.NavigateUrl = "/Applications/CompatibilityMeter/Completion.aspx";

                if (g.BreadCrumbTrailHeader != null)
                {
                    g.BreadCrumbTrailHeader.SetTwoLinkCrumb(g.GetResource("TXT_HOW_DO_WE_MATCH", this),
                                                            g.AppPage.App.DefaultPagePath);

                }
                if (g.BreadCrumbTrailFooter != null)
                {
                    g.BreadCrumbTrailFooter.SetTwoLinkCrumb(g.GetResource("TXT_HOW_DO_WE_MATCH", this),
                                                           g.AppPage.App.DefaultPagePath);
                }
            }
            /* }*/
            catch (Exception ex)
            {
                g.ProcessException(ex);

                if (!g.IsDevMode && !(ex is System.Threading.ThreadAbortException))
                {
                    g.Transfer("/Applications/CompatibilityMeter/Error.aspx");
                }
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            try
            {
                _g.AnalyticsOmniture.PageName = "JMeter 1on1";

                string HowDoWeMatchResourceName = "TXT_HOW_DO_WE_MATCH";
                //if (MatchMeterScoreBar.MatchSteps == 1) HowDoWeMatchResourceName = "TXT_HOW_DO_WE_MATCH_1";
                //if (MatchMeterScoreBar.MatchSteps == 1.5) HowDoWeMatchResourceName = "TXT_HOW_DO_WE_MATCH_15";
                //if (MatchMeterScoreBar.MatchSteps == 2) HowDoWeMatchResourceName = "TXT_HOW_DO_WE_MATCH_2";
                //if (MatchMeterScoreBar.MatchSteps == 2.5) HowDoWeMatchResourceName = "TXT_HOW_DO_WE_MATCH_25";
                //if (MatchMeterScoreBar.MatchSteps == 3) HowDoWeMatchResourceName = "TXT_HOW_DO_WE_MATCH_3";
                //if (MatchMeterScoreBar.MatchSteps == 3.5) HowDoWeMatchResourceName = "TXT_HOW_DO_WE_MATCH_35";
                //if (MatchMeterScoreBar.MatchSteps == 4) HowDoWeMatchResourceName = "TXT_HOW_DO_WE_MATCH_4";
                //if (MatchMeterScoreBar.MatchSteps == 4.5) HowDoWeMatchResourceName = "TXT_HOW_DO_WE_MATCH_45";
                //if (MatchMeterScoreBar.MatchSteps == 5) HowDoWeMatchResourceName = "TXT_HOW_DO_WE_MATCH_5";

                //  txtHowDoWeMatch.ResourceConstant = HowDoWeMatchResourceName;

            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        private int getMemberId()
        {
            try
            {
                if (Request.Params.Get("MemberID") != null)
                {
                    return int.Parse(Request.Params.Get("MemberID"));
                }
            }
            catch (FormatException)
            {
                return 0;
            }
            return 0;
        }
    }
}