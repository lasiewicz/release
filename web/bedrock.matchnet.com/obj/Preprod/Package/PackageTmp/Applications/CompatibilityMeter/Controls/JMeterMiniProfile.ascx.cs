﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Matchnet.Content.ServiceAdapters.Links;
using Matchnet.List.ServiceAdapters;
using Matchnet.List.ValueObjects;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ServiceAdapters.Interfaces;
using Matchnet.Member.ValueObjects.Interfaces;
using Matchnet.Member.ValueObjects.Photos;
using Matchnet.Web.Applications.MemberProfile;
using Matchnet.Configuration.ServiceAdapters.Analitics;
using Matchnet.Web.Applications.ColorCode;
using System.Text;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Managers;
using Matchnet.Web.Framework.Ui;
using Matchnet.Web.Framework.Ui.BasicElements;
using Image = Matchnet.Web.Framework.Image;

namespace Matchnet.Web.Applications.CompatibilityMeter.Controls
{
    public partial class JMeterMiniProfile : FrameworkControl, IMiniProfile
    {
        public delegate void SaveNoteEventHandler(object sender, SaveNoteEventArgs e);
        public event SaveNoteEventHandler SaveNote;
        protected string strDefaultNote = String.Empty;
        const int MAX_SPOTLIGHT_PHOTO = 4;
        protected Color _Color = Color.none;
        protected string _ColorText = "";

        #region Members

        private IMemberDTO _Member;
        private ResultContextType _DisplayContext = ResultContextType.SearchResult;
        private string _categoryID = String.Empty;
        private int _memberID;
        private HotListCategory _hotListCategory;
        private bool _isHighlighted;
        private bool _isSpotlight;
        private bool _overrideSelfSpotlight;
        const int MAX_STRING_LENGTH = 31;
        bool promotionMemberFlag;
        protected MiniProfileHotListBar HotListBar;

        private decimal _MatchScore;
        protected SearchResultProfile searchResult;
        protected bool _transparency;
        MiniProfileHandler _handler;
        protected Image ImageList;
        private List<Photo> _approvedPhotos;

        protected System.Web.UI.WebControls.HyperLink lnkGame;
        protected Txt txtGame;
        protected Matchnet.Web.Framework.Image gamesIcon;

        #region deleted for site redesign

        #endregion

        #endregion

        private string offlineImageName;
        private string onlineImageName;
        private int _approvedPhotosCount = -1;

        public JMeterMiniProfile()
        {
            MaybeFileNameSelected = "icon-click-m-on.gif";
            NoFileNameSelected = "icon-click-n-on.gif";
            YesFileNameSelected = "icon-click-y-on.gif";
        }

        public string OfflineImageName
        {
            get { return offlineImageName; }
            set { offlineImageName = value; }
        }

        public string OnlineImageName
        {
            get { return onlineImageName; }
            set { onlineImageName = value; }
        }
        #region IMiniProfile implementation

        public FrameworkControl ThisControl
        {
            get { return this; }
        }

        public Literal TXTGender
        {
            get { return TextGender; }
        }
        public Literal TXTAge
        {
            get { return TextAge; }
        }
        public Literal TXTRegion
        {
            get { return TextRegion; }
        }
        public Literal TXTHeadline
        {
            get { return TextHeadline; }
        }

        public SearchResultProfile SearchResult
        {
            get { return searchResult; }
        }

        public Matchnet.Web.Framework.Image IMGGame
        {
            get
            {
                return gamesIcon;
            }
        }
        public System.Web.UI.WebControls.HyperLink LNKGame
        {
            get
            {
                return lnkGame;
            }
        }
        public Matchnet.Web.Framework.Txt TXTGame
        {
            get
            {
                return txtGame;
            }
        }

        public int ApprovedPhotosCount
        {
            get
            {
                try
                {
                    if (_approvedPhotosCount == -1)
                    {
                        _approvedPhotosCount = MemberPhotoDisplayManager.Instance.GetApprovedPhotosCount(g.Member, Member, g.Brand);
                    }

                    return _approvedPhotosCount;

                }
                catch (Exception ex)
                { return 0; }

            }

        }
        public decimal MatchesScore
        {
            set { _MatchScore = value; }
        }

        public HyperLink LNKUserName { get { return LinkUserName; } }
        public Image IMGIsNew { get { return ImageIsNew; } }
        public Image IMGThumb { get { return ImageThumb; } }
        public Image IMGIsUpdate { get { return ImageIsUpdate; } }
        public Literal LITContainer { get { return litContainer; } }
        public Image IMGPremiumAuthenticated { get { return imgPremiumAuthenticated; } }
        public HighlightProfileInfoDisplay UCHighlightProfileInfoDisplay { get { return ucHighlightProfileInfoDisplay; } }
        public LastLoginDate LastLoginDate { get { return lastLoginDate; } }
        public Panel PNLComments { get { return pnlComments; } }
        public PlaceHolder PLCBreaks { get { return plcBreaks; } }
        public HtmlInputText TXTFavoriteNote { get { return txtFavoriteNote; } }
        public Matchnet.Web.Framework.Ui.FormElements.FrameworkButton BTNSave { get { return btnSave; } }
        public LinkButton LBRemoveProfile { get { return lbRemoveProfile; } }
        public Txt TXTMore { get { return txtMore; } }
        public Txt TXTOnline { get { return txtOnline; } }
        public Image IMGChat { get { return imgChat; } }
        public HyperLink LNKOnline { get { return lnkOnline; } }
        public Image IMGYes { get { return ImageYes; } }
        public Image IMGMaybe { get { return ImageMaybe; } }
        public Image IMGNo { get { return ImageNo; } }
        public Image IMGBothSaidYes { get { return imgBothSaidYes; } }
        public Txt TXTYes { get { return TxtYes; } }
        public Txt TXTMaybe { get { return TxtMaybe; } }
        public Txt TXTNo { get { return TxtNo; } }
        public HtmlInputHidden YNMVOTEStatus { get { return YNMVoteStatus; } }
        public HtmlTableCell YNMBackgroundAnimation { get { return null; } }

        #endregion


        #region Properties

        public string YesFileNameSelected { get; set; }

        public string NoFileNameSelected { get; set; }

        public string MaybeFileNameSelected { get; set; }

        public bool IsHotListFriend { get; set; }

        public string DefaultNote
        {
            get
            {
                return strDefaultNote;
            }
        }

        public bool Transparency
        {
            get { return (_transparency); }
            set { _transparency = value; }
        }

        public string CategoryID
        {
            get { return (_categoryID); }
            set { _categoryID = value; }
        }

        public int Counter { get; set; }

        public IMemberDTO Member
        {
            get { return _Member; }
            set
            {
                _Member = value;
                _memberID = _Member.MemberID;
                if (add2List != null)
                {
                    add2List.MemberID = _Member.MemberID;
                }

                //if (FrameworkGlobals.memberHighlighted(this._memberID, g.Brand))
                if (FrameworkGlobals.memberHighlighted(this._Member, g.Brand))
                {
                    this._isHighlighted = true;
                }
                else
                {
                    this._isHighlighted = false;
                }
            }
        }

        public int MemberID
        {
            get { return (_memberID); }
            set
            {
                _memberID = value;
                if (value != 0)
                {
                    _Member = MemberSA.Instance.GetMember(_memberID,
                                MemberLoadFlags.None);
                    if (add2List != null)
                    {
                        add2List.MemberID = _Member.MemberID;
                    }
                }
                else
                {
                    _Member = null;
                }
            }
        }

        public int Ordinal { get; set; }

        public BreadCrumbHelper.EntryPoint MyEntryPoint { get; set; }

        public MOCollection MyMOCollection { get; set; }

        public HotListCategory HotListCategory
        {
            get { return (_hotListCategory); }
            set { _hotListCategory = value; }
        }

        public bool EnableSingleSelect { get; set; }

        public string Note
        {
            get
            {
                return txtFavoriteNote.Value;
            }
        }

        public bool CurrentlyOnline { get; set; }

        public bool IsHighlighted
        {
            get { return (this._isHighlighted); }
            set { this._isHighlighted = value; }
        }

        public bool IsSpotlight
        {
            get { return (this._isSpotlight); }
            set { this._isSpotlight = value; }
        }

        public bool OverrideSelfSpotlight
        {
            get { return (this._overrideSelfSpotlight); }
            set { this._overrideSelfSpotlight = value; }
        }
        public int JmeterMatchScore
        {
            get
            {
                if (ViewState["JmeterMatchScore"] == null)
                {
                    ViewState["JmeterMatchScore"] = -1;
                }
                return (int)ViewState["JmeterMatchScore"];
            }
            set { ViewState["JmeterMatchScore"] = value; }
        }

        public bool IsPromotionalMember
        {
            get { return (this.promotionMemberFlag); }
            set { this.promotionMemberFlag = value; }
        }

        public bool IsMatchMeterEnabled { get; set; }

        public String ViewProfileURL { get; set; }

        #endregion

        private void Page_Load(object sender, System.EventArgs e)
        {
            int brandId = 0;
            _handler = new MiniProfileHandler(this);

            if (_Member == null)
            {
                //plcHotListBar.Controls.Remove(HotListBar);
                return;
            }
            try
            {

                strDefaultNote = g.GetResource("HOT_MAKE_A_NOTE", this);

                if (LinkUserName.Text == null || LinkUserName.Text.Trim().Length == 0)
                {
                    plcTransparent.Visible = Transparency;

                    searchResult = new SearchResultProfile(_Member, Ordinal);

                    if (_Member == null)
                    {
                        ThrowNullMemberError();
                        return;
                    }

                    // Configure HotListBar
                    //HotListBar.Member = _Member;
                    // only add the HotListBar if someone's logged in
                    if (_g.Member != null)
                    {
                        if (_g.Member.MemberID != _Member.MemberID)
                        {
                            //HotListBar = (MiniProfileHotListBar)LoadControl("/Framework/Ui/BasicElements/MiniProfileHotListBar.ascx");
                            HotListBar.Member = _Member;

                            //plcHotListBar.Controls.Add( HotListBar );
                        }
                    }

                    //photo (this also sets the ViewProfileURL property)
                    _handler.SetMemberPhotoAndName(g);

                    //online link
                    if (!String.IsNullOrEmpty(onlineImageName))
                        _handler.SetOnlineLink(onlineImageName, offlineImageName);
                    else
                        _handler.SetOnlineLink();
                    _handler.SetLinks();

                    _handler.SetMemberDetails(g, MAX_STRING_LENGTH);
                    _handler.SetThumb(noPhoto, g.Brand);
                    //member subclass-specific stuff
                    _handler.ToggleDisplayType(g);

                    //	Set up contact links (email & ecard & tease)
                    SetContactLinks();

                    // Set up Jmeter controls
                    SetJmeter();
                    SetEmailButtonABTest();
                    SetQuickMessage();
                }

                // TT15768 - prep href for back link in article page
                PrepClickLink();
                insertPixel();
                SetSpotlightDetails();

                //color code
                if (ColorCodeHelper.IsColorCodeEnabled(g.Brand) && ColorCodeHelper.HasMemberCompletedQuiz(_Member, g.Brand) && !ColorCodeHelper.IsMemberColorCodeHidden(_Member, g.Brand))
                {
                    phColorCode.Visible = true;
                    _Color = ColorCodeHelper.GetPrimaryColor(_Member, g.Brand);
                    _ColorText = ColorCodeHelper.GetFormattedColorText(_Color);
                }

                btnOmnidate.TargetMember = Member;

                plcClick.Visible = g.IsYNMEnabled;

                if (Boolean.Parse(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("MINGLE_SEARCH", _g.Brand.Site.Community.CommunityID, _g.Brand.Site.SiteID)))
                {
                    PlaceHolderMingleSearch.Visible = false;
                }
            }
            catch (Exception ex)
            {
                _g.ProcessException(ex);
            }
        }

        private void SetQuickMessage()
        {
            if (Boolean.Parse(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ENABLE_SEARCH_RESULTS_QUICK_MESSAGE", _g.Brand.Site.Community.CommunityID, _g.Brand.Site.SiteID)) &&
               (DisplayContext == ResultContextType.SearchResult ||
               DisplayContext == ResultContextType.MembersOnline ||
               DisplayContext == ResultContextType.QuickSearchResult ||
               DisplayContext == ResultContextType.HotList ||
               DisplayContext == ResultContextType.ReverseSearchResult)
               )
            {
                PlaceHolderEmailMe.Visible = false;
                imgBtnQuickMessage.Visible = true;
                imgBtnQuickMessage.Attributes.Add("MemberID", MemberID.ToString());
                plcQuickMessage.Visible = true;
                txtQuickMessageSubject.Text = g.GetResource("MESSAGE_SUBJECT", this);
                txtQuickMessageBody.Text = g.GetResource("MESSAGE_BODY", this);

                StringBuilder validator = new StringBuilder();
                validator.AppendLine("var quickMessageValidator = { \"quick-message-subject\":[");
                validator.AppendLine("{ \"errorKey\": \"required\", \"message\": \"" + g.GetResource("TXT_CHANGE_SUBJECT", this) + "\" },");
                validator.AppendLine("{ \"errorKey\": \"noChange\", \"message\": \"" + g.GetResource("TXT_CHANGE_SUBJECT", this) + "\" }],");
                validator.AppendLine("\"quick-message-body\":[");
                validator.AppendLine("{ \"errorKey\": \"required\", \"message\": \"" + g.GetResource("TXT_MESSAGE_BODY_BLANK") + "\" },");
                validator.AppendLine("{ \"errorKey\": \"noChange\", \"message\": \"" + g.GetResource("TXT_CHANGE_BODY", this) + "\" }]};");
                validator.AppendLine("var quickMessageGeneralError =\"" + g.GetResource("TXT_QUICK_MESSAGE_GENERAL_ERROR") + "\";");
                validator.AppendLine("var quickMessageProfileType=\"miniprofile\";");

                this.Page.ClientScript.RegisterStartupScript(this.GetType(), "QuickMessageValidator", validator.ToString(), true);
            }
        }

        protected string GetMemberIDAttr()
        {
            return "MemberID=\"" + MemberID + "\"";
        }

        private void SetEmailButtonABTest()
        {
            Scenario scenario = g.GetABScenario("MINIPROFILE_BUTTON");
            if (scenario == Scenario.B)
            {
                ImageEmailMe.FileName = "btn-view-member.gif";
                ImageEmailMe.ResourceConstant = "VIEW_MEMBER";
                txtMore.Visible = false;
            }
        }

        private void SetJmeter()
        {
            if (IsMatchMeterEnabled)
            {
                // ucMatchMeterInfoDisplay.IsMatchMeterInfoDisaplay = IsMatchMeterInfoDisaplay;
                ucMatchMeterInfoDisplay.Visible = true;
                ucMatchMeterInfoDisplay.IsMatchMeterEnabled = true;
                ucMatchMeterInfoDisplay.IsMemberHighlighted = IsHighlighted;
                ucMatchMeterInfoDisplay.MemberID = MemberID;
                if (_DisplayContext == ResultContextType.MatchMeterMatches)
                {
                    ucMatchMeterInfoDisplay.IsMatchesPage = true;
                    ucMatchMeterInfoDisplay.MatchScore = _MatchScore;
                    ucMatchMeterInfoDisplay.Ordinal = Ordinal;
                }
            }
            else
            {
                ucMatchMeterInfoDisplay.IsMatchMeterEnabled = false;
                ucMatchMeterInfoDisplay.Visible = false;
            }

            if (_DisplayContext == ResultContextType.MatchMeterMatches)
            {
                ImageEmailMe.FileName = "jmeter-btn-email.gif";
            }
        }

        private void PrepClickLink()
        {
            string refUrl = string.Empty;
            if (Request.Url.AbsoluteUri != null)
            {
                refUrl = Request.Url.AbsoluteUri;
            }

            if (refUrl.IndexOf("StartRow") != -1)
            {
                refUrl = g.RemoveParamFromURL(refUrl, "StartRow", false);
            }

            string qm = "?";
            if ((refUrl.IndexOf("?") != -1))
            {
                qm = "&";
            }

            refUrl = refUrl + qm + "StartRow=" + GetStartRow();
            refUrl = Server.UrlEncode(refUrl);

            //lnkClick.HRef = FrameworkGlobals.LinkHref("/Applications/Article/ArticleView.aspx?CategoryID=1980&ArticleID=6296&refUrl=" + refUrl, true);

            if (g.Session["ArticleRefUrl"] != null && !g.Session["ArticleRefUrl"].Equals(String.Empty))
            {
                // remove existing value
                g.Session.Remove("ArticleRefUrl");
            }
        }


        private void hideYNMComponents()
        {
            txtDoWeClick.Visible = false;
            TxtYes.Visible = false;
            ImageYes.Visible = false;
            TxtNo.Visible = false;
            ImageNo.Visible = false;
            TxtMaybe.Visible = false;
            ImageMaybe.Visible = false;
            //txtClick.Visible = false;
            //TxtThinkYoud.Visible = false;
        }

        private void SetContactLinks()
        {
            lnkEmailMe.HRef = FrameworkGlobals.LinkHref(string.Format("/Applications/Email/Compose.aspx?MemberID={0}&LinkParent={1}&StartRow={2}&", _Member.MemberID, (int)LinkParent.MiniProfile, GetStartRow(), WebConstants.PageIDs.Compose), true);
            lnkViewProfile.HRef = ViewProfileURL;

            if (_g.Member != null)
            {
                //set flirt/tease link
                lnkTeaseMe.HRef = FrameworkGlobals.LinkHref(ProfileDisplayHelper.GetFlirtLink(_g, _Member.MemberID, GetStartRow()) + "&" + WebConstants.ACTION_CALL + "=" + WebConstants.PageIDs.Tease.ToString(), true);
                ProfileDisplayHelper.AddFlirtOnClick(_g, ref lnkTeaseMe);
            }
            else
            {
                lnkTeaseMe.HRef = g.GetLogonRedirect(ViewProfileURL, null);
            }


            if (Boolean.Parse(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("MINGLE_SEARCH", _g.Brand.Site.Community.CommunityID, _g.Brand.Site.SiteID)))
            {
                PlaceHolderViewProfile.Visible = true;
                PlaceHolderEmailMe.Visible = false;
            }
            else
            {
                PlaceHolderViewProfile.Visible = false;
                PlaceHolderEmailMe.Visible = true;
            }

            #region ECARD IMPLEMENTATION	5-22-06: Steve C.

            // Show hide send ECard link based on setting from database.  (ON or OFF)
            if (g.EcardsEnabled)
            {
                // Generate mingle ecard page url. (to parameter is the username of the reciever)
                string destPageUrl = FrameworkGlobals.BuildConnectFrameworkLink("cards/categories.html?to=" + _Member.GetUserName(_g.Brand) + "&return=1", g, true);

                // Assign url to ECard link.
                lnkECard.HRef = destPageUrl;
            }
            else
            {
                TableCellECard.Visible = false;
            }

            #endregion
        }




        /// <summary>
        /// Get startRow value for persisting result index
        /// </summary>
        /// <returns></returns>
        private int GetStartRow()
        {
            // Recurse through the parent controls.  If one of the controls is "ResultList" retrieve the StartRow value.
            Control parentCtrl = (this).Parent;
            while (parentCtrl != null)
            {
                if (parentCtrl.GetType().Name == "framework_ui_basicelements_resultlist20_ascx")
                {
                    return ((ResultList20)parentCtrl).StartRow;
                }

                parentCtrl = parentCtrl.Parent;
            }

            return Constants.NULL_INT;
        }


        private void SetYNMControls()
        {
            ClickMask clickMask = g.List.GetClickMask(g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, _Member.MemberID);

            YNMVoteStatus.Value = ((int)clickMask).ToString();

            if ((clickMask & ClickMask.MemberYes) == ClickMask.MemberYes)
            {
                ImageYes.FileName = "icon-click-y-on.gif";
            }
            else if ((clickMask & ClickMask.MemberNo) == ClickMask.MemberNo)
            {
                ImageNo.FileName = "icon-click-n-on.gif";
            }
            else if ((clickMask & ClickMask.MemberMaybe) == ClickMask.MemberMaybe)
            {
                ImageMaybe.FileName = "icon-click-m-on.gif";
            }

            if ((clickMask & ClickMask.MemberYes) == ClickMask.MemberYes && (clickMask & ClickMask.TargetMemberYes) == ClickMask.TargetMemberYes)
            {
                imgBothSaidYes.Visible = true;
            }

            YNMVoteParameters parameters = new YNMVoteParameters();

            parameters.SiteID = g.Brand.Site.SiteID;
            parameters.CommunityID = g.Brand.Site.Community.CommunityID;
            parameters.FromMemberID = g.Session.GetInt(WebConstants.SESSION_PROPERTY_NAME_MEMBERID);
            parameters.ToMemberID = _Member.MemberID;

            string encodedParameters = parameters.EncodeParams();

            ImageYes.NavigateUrl = string.Format("javascript:YNMVote('{0}','1','{1}');",
                YNMVoteStatus.Parent.ClientID, encodedParameters);
            ImageYes.Attributes.Add("onMouseOver", string.Format("YNMMouseOver(this, '{0}', '1')", YNMVoteStatus.Parent.ClientID));
            ImageYes.Attributes.Add("onMouseOut", string.Format("YNMMouseOut(this, '{0}', '1')", YNMVoteStatus.Parent.ClientID));

            ImageNo.NavigateUrl = string.Format("javascript:YNMVote('{0}','2','{1}');",
                YNMVoteStatus.Parent.ClientID, encodedParameters);
            ImageNo.Attributes.Add("onMouseOver", string.Format("YNMMouseOver(this, '{0}', '2')", YNMVoteStatus.Parent.ClientID));
            ImageNo.Attributes.Add("onMouseOut", string.Format("YNMMouseOut(this, '{0}', '2')", YNMVoteStatus.Parent.ClientID));

            ImageMaybe.NavigateUrl = string.Format("javascript:YNMVote('{0}','3','{1}');",
                YNMVoteStatus.Parent.ClientID, encodedParameters);
            ImageMaybe.Attributes.Add("onMouseOver", string.Format("YNMMouseOver(this, '{0}', '4')", YNMVoteStatus.Parent.ClientID));
            ImageMaybe.Attributes.Add("onMouseOut", string.Format("YNMMouseOut(this, '{0}', '4')", YNMVoteStatus.Parent.ClientID));

            TxtYes.Href = ImageYes.NavigateUrl;
            TxtYes.Attributes.Add("onmouseover", string.Format("document." + WebConstants.HTML_FORM_ID + ".{0}.onmouseover();", ImageYes.ClientID));
            TxtYes.Attributes.Add("onmouseout", string.Format("document." + WebConstants.HTML_FORM_ID + ".{0}.onmouseout();", ImageYes.ClientID));

            TxtNo.Href = ImageNo.NavigateUrl;
            TxtNo.Attributes.Add("onmouseover", string.Format("document." + WebConstants.HTML_FORM_ID + ".{0}.onmouseover();", ImageNo.ClientID));
            TxtNo.Attributes.Add("onmouseout", string.Format("document." + WebConstants.HTML_FORM_ID + ".{0}.onmouseout();", ImageNo.ClientID));

            TxtMaybe.Href = ImageMaybe.NavigateUrl;
            TxtMaybe.Attributes.Add("onmouseover", string.Format("document." + WebConstants.HTML_FORM_ID + ".{0}.onmouseover();", ImageMaybe.ClientID));
            TxtMaybe.Attributes.Add("onmouseout", string.Format("document." + WebConstants.HTML_FORM_ID + ".{0}.onmouseout();", ImageMaybe.ClientID));

            //tdAnimBG.Attributes.Add("onmouseover", "this.style.backgroundImage='URL(" + Framework.Image.GetURLFromFilename("bknd_vote_anim.gif") + ")';");
            //tdAnimBG.Attributes.Add("onmouseout", "this.style.backgroundImage='URL(" + Framework.Image.GetURLFromFilename("bknd_vote.gif") + ")';");

            if (g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.JDateCoIL)
            {
                imgPremiumAuthenticated.Visible = FrameworkGlobals.IsMemberPremiumAuthenticated(_Member.MemberID, g.Brand);
            }

            ////if (FrameworkGlobals.memberHighlighted(_Member.MemberID, g.Brand))     
            //if (this._isHighlighted)     
            //{
            //    tdAnimBG.Attributes.Remove("onmouseover");                
            //    tdAnimBG.Attributes.Remove("onmouseout");                                    

            //    tdAnimBG.Attributes.Add("onmouseover", "this.style.backgroundImage='URL(" + Framework.Image.GetURLFromFilename("bknd_vote_anim_highlighted.gif") + ")';");
            //    tdAnimBG.Attributes.Add("onmouseout", "this.style.backgroundImage='URL(" + Framework.Image.GetURLFromFilename("bknd_vote_highlighted.gif") + ")';");                
            //}
        }

        private void ThrowNullMemberError()
        {
            g.Notification.AddError("PAGE_MEMBER_PROFILE_TEMPORARILY_UNAVAILABLE");
            TextGender.Text = g.GetResource("MEMBER_PROFILE_TEMPORARILY_UNAVAILABLE", this);
            ImageEmailMe.Visible = false;
            ImageList.Visible = false;
            ImageThumb.ImageUrl = "/img/trans.gif";
        }

        public ResultContextType DisplayContext
        {
            set
            {
                _DisplayContext = value;
            }

            get
            {
                return _DisplayContext;
            }
        }

        private void lbRemoveProfile_Click(object sender, System.EventArgs e)
        {
            try
            {
                if ((_hotListCategory != HotListCategory.Default || _hotListCategory > 0) ||
                    (_hotListCategory != HotListCategory.MutualYes && _hotListCategory != HotListCategory.YourMatches))
                {
                    ListSA.Instance.RemoveListMember(_hotListCategory, g.Brand.Site.Community.CommunityID, g.Member.MemberID, this.Member.MemberID);
                }

                int intval = Convert.ToInt32(_hotListCategory);
                g.Transfer("/Applications/HotList/View.aspx?CategoryID=" + intval.ToString() + "&StartRow=" + this.GetStartRow() + "&rc=YOUR_LIST_HAS_BEEN_UPDATED");
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            try
            {
                base.OnPreRender(e);

                if (g.AnalyticsOmniture != null)
                {
                    string additionalSource = "";
                    //This line was removed to make miniprofile work since it no longer implements IPostBackDataHandler:
                    //Page.RegisterRequiresPostBack(this);

                    if (_DisplayContext == ResultContextType.HotList)
                    {
                        _handler.ShowHotListMember(g);
                        if (_isSpotlight)
                            additionalSource = "List";
                    }
                    else if (_DisplayContext == ResultContextType.MembersOnline)
                    {
                        additionalSource = "List";
                    }

                    //lnkEmailMe.HRef = g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.Compose, WebConstants.Action.Email, lnkEmailMe.HRef, additionalSource);
                    lnkTeaseMe.HRef = g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.Tease, WebConstants.Action.Flirt, lnkTeaseMe.HRef, additionalSource);
                    LNKUserName.NavigateUrl = g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.ViewProfile, WebConstants.Action.ProfileName, LNKUserName.NavigateUrl, additionalSource);
                    IMGThumb.NavigateUrl = g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.ViewProfile, WebConstants.Action.ProfilePhoto, IMGThumb.NavigateUrl, additionalSource);
                    noPhoto.DestURL = IMGThumb.NavigateUrl;
                    //txtMore.Href = g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.ViewProfile, WebConstants.Action.ViewProfile, txtMore.Href, additionalSource);
                    //lnkOnline.NavigateUrl = g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.InstantCommunicator, WebConstants.Action.IM, lnkOnline.NavigateUrl, additionalSource);
                    //add2List.ExtraJavaScriptToRun_HotlistLoggedInOnClick = g.AnalyticsOmniture.GetOnClickCustomLinkTracking(WebConstants.Action.HotList, g.AnalyticsOmniture.PageName, "", false, false);
                    IMGYes.Attributes.Add("onclick", g.AnalyticsOmniture.GetOnClickCustomLinkTracking(WebConstants.Action.ClickY, g.AnalyticsOmniture.PageName, "", false, false));
                    IMGNo.Attributes.Add("onclick", g.AnalyticsOmniture.GetOnClickCustomLinkTracking(WebConstants.Action.ClickN, g.AnalyticsOmniture.PageName, "", false, false));
                    IMGMaybe.Attributes.Add("onclick", g.AnalyticsOmniture.GetOnClickCustomLinkTracking(WebConstants.Action.ClickM, g.AnalyticsOmniture.PageName, "", false, false));
                    lnkOnline.Attributes.Add("onclick", g.AnalyticsOmniture.GetOnClickCustomLinkTracking(WebConstants.Action.IM, g.AnalyticsOmniture.PageName, "", false, false));
                    lnkECard.HRef = g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.ECard, WebConstants.Action.ECard, lnkECard.HRef, additionalSource);
                    add2List.ActionCallPage = g.AnalyticsOmniture.PageName;
                    add2List.ActionCallPageDetail = "";

                    Scenario scenario = g.GetABScenario("MINIPROFILE_BUTTON");
                    if (scenario == Scenario.B)
                    {
                        lnkEmailMe.HRef = g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.ViewProfile, WebConstants.Action.ViewMemberButton, txtMore.Href, additionalSource);
                    }
                    else
                    {
                        lnkEmailMe.HRef = g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.Compose, WebConstants.Action.EmailMeNowButton, lnkEmailMe.HRef, additionalSource);

                        // txtMore.Href = g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.ViewProfile, WebConstants.Action.ViewMemberButton, txtMore.Href, additionalSource);

                        txtMore.Text = FrameworkGlobals.GetMorePhotoLink(ApprovedPhotosCount, g, this);
                        if (ApprovedPhotosCount > 0)
                        {
                            txtMore.Href = g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.ViewProfile, WebConstants.Action.ViewMorePhotos, txtMore.Href, additionalSource);
                        }
                        else
                        {
                            txtMore.Href = g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.ViewProfile, WebConstants.Action.ViewMemberLink, txtMore.Href, additionalSource);
                        }
                    }
                }

                if (lbRemoveProfile.Visible == false)
                {
                    divMatchMeterInfoDisplay.Attributes["class"] = "jmeterLinkMiniProfileReg";
                }

            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }


        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbRemoveProfile.Click += new System.EventHandler(this.lbRemoveProfile_Click);
            this.btnSave.Click += new System.EventHandler(this.btnSave_ServerClick);
            this.Load += new System.EventHandler(this.Page_Load);
            rptPhotos.ItemDataBound += new RepeaterItemEventHandler(rptPhotos_ItemDataBound);
            this.PreRender += new EventHandler(MiniProfile_PreRender);
            if (add2List != null)
            {
                add2List.MemberID = this.MemberID;
            }
        }
        #endregion

        private void btnSave_ServerClick(object sender, System.EventArgs e)
        {
            SaveNoteEventArgs args = new SaveNoteEventArgs();
            args.Member = _Member;
            args.Note = txtFavoriteNote.Value;
            SaveNote(sender, args);

            g.Notification.AddMessage("NOT_YOUR_HOT_LIST_HAS_BEEN_UPDATED");
        }

        private void MiniProfile_PreRender(object sender, EventArgs e)
        {
            if (g.Member != null && _Member != null && g.Member.MemberID != _Member.MemberID)
            {
                //TableYNM.Visible = true;
                _handler.SetYNMControls(g);
                SetHighlightDetail();
            }
            else
            {
                hideYNMComponents();
            }
        }
        private void insertPixel()
        {
            try
            {
                if (g.PromotionalProfileEnabled() && g.PromotionalProfileMember() == _Member.MemberID)
                {
                    string pixelResx = g.GetResource("PROMOTION_MINIPROFILE_PIXEL");
                    if (!string.IsNullOrEmpty(pixelResx))
                    { plcPixel.Text = pixelResx; }
                }
            }
            catch (Exception ex) { }
        }


        private void SetSpotlightDetails()
        {

            if (g.Member == null)
                return;
            bool selfSpotlight = (g.Member.MemberID == _Member.MemberID);
            //headline
            if (!_isSpotlight)
            { return; }

            plcSpotlightSettings.Visible = selfSpotlight && !_overrideSelfSpotlight;
            string matches = ProfileDisplayHelper.GetSpotlightMatchString(g, _Member);
            if (!String.IsNullOrEmpty(matches) && !selfSpotlight)
            {
                txtBothLike.Visible = true;
                searchResult.SetHeadline(TextHeadline, matches, _memberID, 80, 20);
            }
            else
            {
                searchResult.SetHeadline(TextHeadline, g.Session.GetInt(WebConstants.SESSION_PROPERTY_NAME_MEMBERID), 60, 20);
            }

            txtSpotlight.Visible = (!promotionMemberFlag && (!selfSpotlight || _overrideSelfSpotlight));
            txtSpotlightPromo.Visible = (promotionMemberFlag && !selfSpotlight);
            txtSelfSpotlight.Visible = selfSpotlight && !_overrideSelfSpotlight;


            litContainer.Text = "<div class=\"results list-view spotlighted\" style=\"z-index: " + Convert.ToString(100 - this.Counter) + ";\">";
            plcSpotlight.Visible = true;
            plcNormalView.Visible = false;
            DateTime birthDate = _Member.GetAttributeDate(g.Brand, "BirthDate", DateTime.MinValue);
            string age = FrameworkGlobals.GetAgeCaption(birthDate, g, this, "AGE_UNSPECIFIED");
            string regionInfo = String.Empty;
            int regionid = _Member.GetAttributeInt(g.Brand, "RegionID");


            regionInfo = FrameworkGlobals.GetRegionString(regionid, g.Brand.Site.LanguageID);


            TextInfo.Text = String.Format("{0}, {1}", age, regionInfo);
            plcNormalView.Visible = false;
            plcSpotlightView.Visible = true;
            var photos = MemberPhotoDisplayManager.Instance.GetApprovedPhotosExcludingMain(g.Member, _Member, g.Brand, false);
            // the code that used to be here had 0 comments, so i can only guess what it was trying to do.
            // i am assuming for this repeater bind, it doesn't want the main/default photo.  it wants extra approved photos aside from
            // the main/default photo.  i am going to rewrite this code based on that assumption
            if (photos != null && photos.Count > 0)
            {
                if (photos.Count > MAX_SPOTLIGHT_PHOTO)
                {
                    // let's trim the ones we don't want
                    photos.RemoveRange(MAX_SPOTLIGHT_PHOTO, MAX_SPOTLIGHT_PHOTO - photos.Count);
                }

                rptPhotos.DataSource = photos;
                rptPhotos.DataBind();
            }

            if (!selfSpotlight || _overrideSelfSpotlight)
            {
                ucSpotlightInfoDisplay.Spotlight = true;
                ucSpotlightInfoDisplay.Visible = true;
            }
            bool showEnlarge = Conversion.CBool(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("SPOTLIGHT_ENLARGE_PROFILE", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID));
            if (showEnlarge && _Member.HasApprovedPhoto(g.Brand.Site.Community.CommunityID))
            {
                ucProfileEnlarge.Member = _Member;
                ucProfileEnlarge.Visible = true;
            }
            //memberSpotlight_curveBottom.Visible = true;
        }

        private void SetHighlightDetail()
        {
            if (this._isHighlighted)
            {
            }
        }

        protected void rptPhotos_ItemDataBound(Object sender, RepeaterItemEventArgs e)
        {
            try
            {
                //MemberPhotoHelper photoHelper = new MemberPhotoHelper(RuntimeSettings.Instance);
                Photo photo = (Photo)e.Item.DataItem;
                Image img = ((Image)e.Item.FindControl("imgTinyThumb"));
                if (MemberPhotoDisplayManager.Instance.IsPrivatePhoto(photo, g.Brand))
                    return;
                if (!MemberPhotoDisplayManager.Instance.PhotoIsEmpty(photo, PhotoType.Thumbnail, g.Brand))
                {
                    img.ImageUrl = MemberPhotoDisplayManager.Instance.GetPhotoDisplayURL(g.Member, _Member, g.Brand, photo, PhotoType.Thumbnail,
                                                         PrivatePhotoImageType.Thumb, NoPhotoImageType.Thumb);
                }
                else
                {
                    img.ImageUrl = MemberPhotoDisplayManager.Instance.GetPhotoDisplayURL(g.Member, _Member, g.Brand, photo, PhotoType.Full,
                                                         PrivatePhotoImageType.Full, NoPhotoImageType.None);
                }

            }
            catch (Exception ex)
            {

            }

        }

    }

}
