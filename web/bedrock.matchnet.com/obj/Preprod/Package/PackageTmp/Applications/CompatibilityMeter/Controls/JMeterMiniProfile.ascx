﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="JMeterMiniProfile.ascx.cs"
    Inherits="Matchnet.Web.Applications.CompatibilityMeter.Controls.JMeterMiniProfile" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="uc1" TagName="LastLoginDate" Src="~/Framework/Ui/BasicElements/LastLoginDate.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Add2List" Src="~/Framework/Ui/BasicElements/Add2List.ascx" %>
<%@ Register TagPrefix="uc1" TagName="NoPhoto" Src="~/Framework/Ui/PageElements/NoPhoto20.ascx" %>
<%@ Register TagPrefix="uc2" TagName="MiniProfileHotListBar" Src="~/Framework/Ui/BasicElements/MiniProfileHotListBar.ascx" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="uc2" TagName="HighlightProfileInfoDisplay" Src="~/Framework/Ui/BasicElements/HighlightProfileInfoDisplay.ascx" %>
<%@ Register TagPrefix="uc3" TagName="SpotlightInfoDisplay" Src="~/Framework/Ui/BasicElements/SpotlightInfoDisplay.ascx" %>
<%@ Register TagPrefix="uc4" TagName="ProfileEnlarge" Src="~/Framework/Ui/BasicElements/ProfileEnlarge.ascx" %>
<%@ Register TagPrefix="uc2" TagName="MatchMeterInfoDisplay" Src="~/Framework/Ui/BasicElements/MatchMeterInfoDisplay.ascx" %>
<%@ Register TagPrefix="uc5" TagName="OmnidateInvitationButton" Src="~/Applications/Omnidate/Controls/OmnidateInvitationButton.ascx" %>
<asp:PlaceHolder ID="plcSpotlight" runat="server" Visible="false">
    <div class="results spotlight-header clearfix">
        <strong>
            <mn:Txt ID="txtSpotlight" runat="server" ResourceConstant="SPOTLIGHT_TITLE" Visible="false" />
            <mn:Txt ID="txtSpotlightPromo" runat="server" ResourceConstant="SPOTLIGHT_PROMO_TITLE"
                Visible="false" />
            <mn:Txt ID="txtSelfSpotlight" runat="server" ResourceConstant="SPOTLIGHT_SELF_TITLE"
                Visible="false" />
        </strong>
    </div>
</asp:PlaceHolder>
<asp:Literal ID="plcPixel" runat="server"></asp:Literal>
<asp:Literal ID="litContainer" runat="server"></asp:Literal>
<div class="picture">
    <uc1:NoPhoto runat="server" ID="noPhoto" class="noPhoto" Visible="False" />
    <mn:Image ID="ImageThumb" runat="server" Width="80" Height="104" />
    <asp:PlaceHolder ID="phColorCode" runat="server" Visible="false">
        <div class="cc-pic-tag cc-pic-tag-sm cc-pic-<%=_ColorText.ToLower() %>">
            <span>
                <%=_ColorText %></span></div>
    </asp:PlaceHolder>
</div>
<div class="liquid">
    <div class="header clearfix">
        <h2>
            <asp:HyperLink ID="LinkUserName" runat="server" /><mn:Image ID="ImageIsNew" runat="server"
                FileName="icon-new-member.gif" Visible="false" TitleResourceConstant="ALT_NEW_MEMBER" />
            <mn:Image ID="ImageIsUpdate" runat="server" FileName="icon-updated.gif" Visible="false"
                TitleResourceConstant="ALT_UPDATED_MEMBER" /></h2>
        <div id="divMatchMeterInfoDisplay" runat="server" class="jmeterLinkMiniProfile">
            <uc2:MatchMeterInfoDisplay runat="server" ID="ucMatchMeterInfoDisplay" DispalyType="1" />
        </div>
        <div class="float-outside">
            <uc2:HighlightProfileInfoDisplay runat="server" ID="ucHighlightProfileInfoDisplay"
                ShowImageNoText="false" />
            <asp:LinkButton Visible="False" runat="server" ID="lbRemoveProfile" />
            <uc3:SpotlightInfoDisplay runat="server" ID="ucSpotlightInfoDisplay" ShowImageNoText="false"
                Visible="false" />
            <asp:PlaceHolder ID="plcSpotlightSettings" runat="server" Visible="false"><a href="/Applications/MemberServices/PremiumServiceSettings.aspx">
                <strong>
                    <mn:Txt ID="txtlnkSpotlightSettings" runat="server" ResourceConstant="SPOTLIGHT_SETTINGS" />
                </strong></a></asp:PlaceHolder>
        </div>
    </div>
    <div class="details">
        <uc4:ProfileEnlarge runat="server" ID="ucProfileEnlarge" Visible="false" />
        <p>
            <mn:Txt runat="server" ID="txtBothLike" ResourceConstant="TXT_BOTH_LIKE" Visible="false"
                ExpandImageTokens="true" />
            <asp:Literal ID="TextHeadline" runat="server" /><br />
            <mn:Txt runat="server" ID="txtMore" />
        </p>
    </div>
    <asp:PlaceHolder runat="server" ID="plcQuickMessage" Visible="false">
        <div class="quick-message" <%=GetMemberIDAttr() %>>
            <div class="subject">
                <asp:TextBox runat="server" ID="txtQuickMessageSubject" Columns="37" MaxLength="50" CssClass="quick-message-subject" />
            </div>
            <div class="body">
                <asp:TextBox runat="server" ID="txtQuickMessageBody" Rows="3" TextMode="MultiLine"
                    CssClass="quick-message-body" Columns="27" />
            </div>
            <div class="close">
                <mn:Txt runat="server" ResourceConstant="CLOSE_BTN_TXT" />
            </div>
            <div class="send-message">
                <mn:Txt runat="server" ResourceConstant="SEND_MESSAGE_TXT" />
            </div>
        </div>
        <div class="send-message-loading" <%=GetMemberIDAttr() %>>
            <mn:Image ID="Image2" runat="server" FileName="ajax-loader-2.gif" />
        </div>
    </asp:PlaceHolder>
    <div class="info">
        <div class="history">
            <div class="history-icons">
                <mn:Image ID="imgBothSaidYes" runat="server" TitleResourceConstant="TTL_CLICK_YOU_BOTH_SAID_YES_EXCLAMATION"
                    FileName="icon-click-yy.gif" Visible="false" />
                <mn:Image ID="ImageOnline" runat="server" FileName="icon-IM.gif" Visible="false" />
                <mn:Image ID="imgPremiumAuthenticated" runat="server" FileName="icon_premiumAuthenticated_l.gif"
                    Visible="false" TitleResourceConstant="ALT_PREMIUM_AUTHENTICATED" ResourceConstant="ALT_PREMIUM_AUTHENTICATED"
                    class="verifiedIcon" />
                <asp:PlaceHolder ID="plcHotListBar" runat="server">
                    <uc2:MiniProfileHotListBar ID="HotListBar" runat="server" FriendIconFileName="icon-members-hotlisted-{0}.gif"
                        ContactedIconFileName="icon-members-emailed-{0}.gif" TeaseIconImageFileName="icon-members-flirted-{0}.gif"
                        IMIconFileName="icon-members-IM-{0}.gif" ViewProfileIconFileName="icon-members-viewed-{0}.gif"
                        OmnidateIconFileName="icon-members-omnidated-{0}.gif" />
                </asp:PlaceHolder>
            </div>
            <div class="actions-email">
                <asp:PlaceHolder runat="server" ID="PlaceHolderEmailMe"><a id="lnkEmailMe" runat="server">
                    <mn:Image ID="ImageEmailMe" runat="server" FileName="btn-email.gif" ResourceConstant="ALT_VIEW_MEMBER" />
                </a></asp:PlaceHolder>
                <mn:Image ID="imgBtnQuickMessage" Visible="false" runat="server" FileName="btn-email.gif"
                    ResourceConstant="ALT_VIEW_MEMBER" CssClass="open-quick-message" />
                <asp:PlaceHolder runat="server" ID="PlaceHolderViewProfile"><a id="lnkViewProfile"
                    runat="server">
                    <mn:Image ID="ImageViewProfile" runat="server" FileName="btn-profile.gif" ResourceConstant="ALT_VIEW_MEMBER" />
                </a></asp:PlaceHolder>
            </div>
        </div>
        <div class="overview">
            <p>
                <asp:PlaceHolder ID="plcNormalView" runat="server">
                    <asp:Literal ID="TextAge" runat="server" />
                    <mn:Txt ID="ageTxt" runat="server" ResourceConstant="ageTxt" />
                    <asp:Literal ID="TextGender" runat="server" /><br />
                    <asp:Literal ID="TextRegion" runat="server" /><br />
                    <span class="timestamp">
                        <uc1:LastLoginDate ID="lastLoginDate" runat="server" />
                    </span></asp:PlaceHolder>
                <asp:PlaceHolder ID="plcSpotlightView" runat="server" Visible="false">
                    <asp:Literal ID="TextInfo" runat="server" /><br />
                    <asp:Repeater ID="rptPhotos" runat="server">
                        <ItemTemplate>
                            <mn:Image ID="imgTinyThumb" runat="server" Width="35" Height="43" />
                        </ItemTemplate>
                    </asp:Repeater>
                </asp:PlaceHolder>
            </p>
        </div>
    </div>
</div>
<asp:PlaceHolder runat="server" ID="PlaceHolderMingleSearch">
    <div class="communications clearfix">
        <div class="wrapper">
            <div class="actions image-text-pair clearfix">
                <div class="item">
                    <asp:HyperLink runat="server" ID="lnkOnline">
                        <mn:Image ID="imgChat" runat="server" FileName="icon-chat.gif" ResourceConstant="ALT_IM_ONLINE" />&nbsp;
                        <span>
                            <mn:Txt runat="server" ResourceConstant="PRO_ONLINE" ID="txtOnline" />
                        </span>
                    </asp:HyperLink>
                </div>
                <uc5:OmnidateInvitationButton runat="server" ID="btnOmnidate" DisplayContext="MiniProfile" />
                <div class="item">
                    <%--<a id="lnkTeaseMe" runat="server">--%>
                    <a id="lnkTeaseMe" runat="server">
                        <mn:Image ID="icon_tease" runat="server" FileName="icon-flirt.gif" ResourceConstant="PRO_TEASE"
                            TitleResourceConstant="ALT_TEASE_ME" />&nbsp; <span>
                                <mn:Txt runat="server" ID="txtTease" ResourceConstant="PRO_TEASE" />
                            </span></a>
                </div>
                <div class="item">
                    <asp:PlaceHolder ID="TableCellECard" runat="server"><a id="lnkECard" runat="server">
                        <mn:Image ID="icon_ecard" runat="server" FileName="icon-ecard.gif" ResourceConstant="PRO_ECARD"
                            TitleResourceConstant="ALT_ECARD" />&nbsp; <span>
                                <mn:Txt runat="server" ID="txtECard" ResourceConstant="PRO_ECARD" />
                            </span></a></asp:PlaceHolder>
                </div>
                <div class="item">
                    <uc1:Add2List ID="add2List" runat="server" IconHotList="icon-hotlist-add.gif" IconHotListRemove="icon-hotlist-remove.gif" IsJMeterPage="True" />
                </div>
            </div>
        </div>
        <asp:PlaceHolder runat="server" ID="plcClick">
            <div class="click">
                <span class="do-we-click">
                    <mn:Txt runat="server" ID="txtDoWeClick" ResourceConstant="TXT_DOWECLICK" />
                </span>
                <%--<mn:Txt runat="server" ID="TxtThinkYoud" ResourceConstant="PRO_THINK_YOUD" />
            &nbsp;<strong><mn:Txt runat="server" ID="txtClick" ResourceConstant="TXT_CLICK_TRADE" /></strong>--%>
                <span class="no-wrap-class">
                    <mn:Txt runat="server" ID="Txt1" ResourceConstant="PRO_THINK_YOUD_AFTER_CLICK_LINK" />
                    <mn:Image ID="ImageYes" FileName="icon-click-y-off.gif" runat="server" ResourceConstant="YNM_Y_IMG_ALT_TEXT"
                        align="absmiddle" />
                        <mn:Txt runat="server" ID="Txt2" ResourceConstant="YNM_YES" />
                    <mn:Txt ID="TxtYes" runat="server" ResourceConstant="YNM_YES" CssClass="narrow-hide" />
                    <mn:Image ID="ImageNo" FileName="icon-click-n-off.gif" runat="server" ResourceConstant="YNM_N_IMG_ALT_TEXT"
                        align="absmiddle" />
                        <mn:Txt runat="server" ID="Txt3" ResourceConstant="YNM_NO" />
                    <mn:Txt ID="TxtNo" runat="server" ResourceConstant="YNM_NO" CssClass="narrow-hide" />
                    <mn:Image ID="ImageMaybe" FileName="icon-click-m-off.gif" runat="server" ResourceConstant="YNM_M_IMG_ALT_TEXT"
                        align="absmiddle" />
                        <mn:Txt runat="server" ID="Txt4" ResourceConstant="YNM_MAYBE" />
                    <mn:Txt ID="TxtMaybe" runat="server" ResourceConstant="YNM_MAYBE" CssClass="narrow-hide" />
                    <input type="hidden" id="YNMVoteStatus" runat="server" name="YNMVoteStatus" />
                </span>
            </div>
        </asp:PlaceHolder>
    </div>
</asp:PlaceHolder>
<asp:Panel runat="server" ID="pnlComments" Visible="False" CssClass="note">
    <input id="txtFavoriteNote" type="text" maxlength="55" name="<%= Member.MemberID %>"
        runat="server" />
    <mn2:FrameworkButton ID="btnSave" runat="server" ResourceConstant="SAVE" name="Save"
        type="submit"></mn2:FrameworkButton>&nbsp;
</asp:Panel>
<asp:PlaceHolder ID="litContainerClose" runat="server"></div></asp:PlaceHolder>
<asp:PlaceHolder runat="server" ID="plcTransparent" Visible="False"></asp:PlaceHolder>
<asp:PlaceHolder ID="plcBreaks" runat="server" />
