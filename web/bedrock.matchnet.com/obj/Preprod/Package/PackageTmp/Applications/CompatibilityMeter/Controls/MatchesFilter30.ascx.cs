﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Util;
using Matchnet.Lib;

using Matchnet.Session.ValueObjects;
using Spark.SAL;


namespace Matchnet.Web.Applications.CompatibilityMeter.Controls
{
    public partial class MatchesFilter30 : FrameworkControl
    {
        private MemberSearch _oMemberSearch;
        private MemberSearch OMemberSearch
        {
            get
            {
                if (_oMemberSearch == null)
                {
                    _oMemberSearch = getMemberSearch();
                }
                return _oMemberSearch;
            }
        }

        #region Properties (3)

        public int intAgeMax
        {
            get { return Matchnet.Conversion.CInt(AgeMax.Value); }
        }

        public int intAgeMin
        {
            get { return Matchnet.Conversion.CInt(AgeMin.Value); }
        }

        public int intRegionID
        {
            get
            {
                //down and dirty fix for TT#16325
                if (g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.Cupid && g.Brand.Site.Name == "Cupid.co.il")
                {
                    string molRegionID = g.Session["MatchMeterRegion"].ToString();
                    if (molRegionID != null && molRegionID != String.Empty)
                    {
                        this.RegionID.Value = molRegionID;
                    }
                }

                if (g.Session["MatchMeterRegion"].ToString() == "-1")
                    return g.Member.GetAttributeInt(g.Brand, WebConstants.ATTRIBUTE_NAME_REGIONID);
                   // return int.Parse(g.Session[WebConstants.ATTRIBUTE_NAME_MOLREGIONID]);

                return Matchnet.Conversion.CInt(RegionID.Value);
            }
        }

        public bool PhotoRequired
        {
            get { return chkPhotoRequired.Checked; }
        }

        #endregion Properties

        #region Methods (7)


        // Public Methods (1) 

        public int GetGenderMask()
        {
            //fix for outstanding cupid.co.il bug #16325. Quick and dirty no need to spend 2 days finding
            //where value is lost for this site.  If this gets executed on any other site is will do no harm.
            //seems should jsut use this line of code without the if statment for all sites anyway.
            if (g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.Cupid && g.Brand.Site.Name == "Cupid.co.il")
            {
                string newValue = g.Session["MatchMeterGender"];
                if (newValue != null && newValue != string.Empty)
                {
                    this.GenderMask.Value = newValue;
                }
                else
                {
                    newValue = OMemberSearch.Gender.ToString();
                }
            }
            switch (Convert.ToInt32(GenderMask.Value))
            {
                case 0:
                    return (ConstantsTemp.GENDERID_FEMALE | ConstantsTemp.GENDERID_SEEKING_MALE);

                case 1:
                    return (ConstantsTemp.GENDERID_MALE | ConstantsTemp.GENDERID_SEEKING_FEMALE);

                case 2:
                    return (ConstantsTemp.GENDERID_FEMALE | ConstantsTemp.GENDERID_SEEKING_FEMALE);

                case 3:
                    return (ConstantsTemp.GENDERID_MALE | ConstantsTemp.GENDERID_SEEKING_MALE);

                default:
                    return Constants.NULL_INT;
            }
        }



        // Protected Methods (1) 

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                this.DoValidation();
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }



        // Private Methods (5) 

        private void btnSearch_Click(object sender, System.EventArgs e)
        {
            try
            {
                if (int.Parse(AgeMin.Value) > int.Parse(AgeMax.Value))
                {
                    string temp = AgeMin.Value;
                    AgeMin.Value = AgeMax.Value;
                    AgeMax.Value = temp;
                }
                int expirationDays = Conversion.CInt(Configuration.ServiceAdapters.RuntimeSettings.GetSetting("MATCH_METER_CRITERIA_EXPIRATION", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID), 365);
                g.Session.Add("MatchMeterGender", this.GenderMask.Value, expirationDays, false);
                g.Session.Add("MatchMeterRegion", this.RegionID.Value, expirationDays, false);
                g.Session.Add("MatchMeterAgeMin", this.AgeMin.Value, expirationDays, false);
                g.Session.Add("MatchMeterAgeMax", this.AgeMax.Value, expirationDays, false);
                g.Session.Add("MatchMeterPhotoRequired", this.chkPhotoRequired.Checked.ToString(), expirationDays, false);
                g.Session.Add(WebConstants.ATTRIBUTE_NAME_MOLREGIONID, this.RegionID.Value, expirationDays, false);

                string url = Request.RawUrl;
                url = g.RemoveParamFromURL(url, "StartRow", false);
                url += ((url.IndexOf("?") >= 0) ? "&" : "?") + "StartRow=1";

                if (url.IndexOf("Postback") < 0)
                {
                    url += ((url.IndexOf("?") >= 0) ? "&" : "?") + "Postback=1";
                }

                g.Transfer(url);
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        private void DoValidation()
        {
            // Add min and max age validation
            string minAgeError = g.GetResource("MIN_AGE_NOTICE", this);
            string maxAgeError = g.GetResource("MAX_AGE_NOTICE", this);
            string greaterThanError = g.GetResource("GREATER_THAN_NOTICE", this);

            AgeMinRange.ErrorMessage = minAgeError;
            AgeMinRange.MinimumValue = ConstantsTemp.MIN_AGE_RANGE.ToString();
            AgeMinRange.MaximumValue = ConstantsTemp.MAX_AGE_RANGE.ToString();
            AgeMaxRange.ErrorMessage = maxAgeError;
            AgeMaxRange.MinimumValue = ConstantsTemp.MIN_AGE_RANGE.ToString();
            AgeMaxRange.MaximumValue = ConstantsTemp.MAX_AGE_RANGE.ToString();
            AgeMinReq.ErrorMessage = minAgeError;
            AgeMaxReq.ErrorMessage = maxAgeError;
            //valCompare.ErrorMessage = greaterThanError;

            AgeMinRange.Type = System.Web.UI.WebControls.ValidationDataType.Integer;
            AgeMaxRange.Type = System.Web.UI.WebControls.ValidationDataType.Integer;
            //valCompare.Type = System.Web.UI.WebControls.ValidationDataType.Integer;
        }

        private DataTable GetGenderOptions()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("Content", typeof(System.String));
            dt.Columns.Add("Value", typeof(System.Int16));

            switch (g.Brand.Site.Community.CommunityID)
            {
                case (int)WebConstants.COMMUNITY_ID.JDate:
                    dt.Rows.Add(new object[] { g.GetResource("EVERYONE", this), -1 });
                    dt.Rows.Add(new object[] { g.GetResource("WOMEN", this), 0 });
                    dt.Rows.Add(new object[] { g.GetResource("MEN", this), 1 });
                    break;

                case (int)WebConstants.COMMUNITY_ID.Glimpse:
                    dt.Rows.Add(new object[] { g.GetResource("EVERYONE", this), -1 });
                    dt.Rows.Add(new object[] { g.GetResource("WOMEN", this), 2 });
                    dt.Rows.Add(new object[] { g.GetResource("MEN", this), 3 });
                    break;

                default:
                    dt.Rows.Add(new object[] { g.GetResource("EVERYONE", this), -1 });
                    dt.Rows.Add(new object[] { g.GetResource("WOMEN_SEEKING_MEN", this), 0 });
                    dt.Rows.Add(new object[] { g.GetResource("MEN_SEEKING_WOMEN", this), 1 });
                    dt.Rows.Add(new object[] { g.GetResource("WOMEN_SEEKING_WOMEN", this), 2 });
                    dt.Rows.Add(new object[] { g.GetResource("MEN_SEEKING_MEN", this), 3 });
                    break;
            }

            return dt;
        }

        private DataTable GetRegionOptions()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("Content", typeof(System.String));
            dt.Columns.Add("Value", typeof(System.Int32));


            if (g.Brand.Site.Community.CommunityID == (int)WebConstants.COMMUNITY_ID.Cupid)
            {
                if (g.Member != null)
                {
                    string SessionRegionID = g.Session[WebConstants.ATTRIBUTE_NAME_MOLREGIONID].ToString();
                    if (SessionRegionID != String.Empty)
                    {
                        dt.Rows.Add(new object[] { FrameworkGlobals.GetUnicodeText(g.GetResource("YOUR_REGION", this)), SessionRegionID });
                    }
                    else
                    {
                        dt.Rows.Add(new object[] { FrameworkGlobals.GetUnicodeText(g.GetResource("YOUR_REGION", this)), g.Member.GetAttributeInt(g.Brand, WebConstants.ATTRIBUTE_NAME_REGIONID) });
                        g.Session.Add(WebConstants.ATTRIBUTE_NAME_MOLREGIONID, g.Member.GetAttributeInt(g.Brand, WebConstants.ATTRIBUTE_NAME_REGIONID).ToString(), SessionPropertyLifetime.Temporary);
                    }
                }

                dt.Rows.Add(new object[] { FrameworkGlobals.GetUnicodeText(g.GetResource("ALL_OF_ISRAEL", this)), ConstantsTemp.REGIONID_ISRAEL });
                dt.Rows.Add(new object[] { FrameworkGlobals.GetUnicodeText(g.GetResource("ANYWHERE_CUPID", this)), Constants.NULL_INT });
            }
            else
            {

                if (g.Member != null)
                {
                    if (g.Session[WebConstants.ATTRIBUTE_NAME_MOLREGIONID] != String.Empty && g.Session[WebConstants.ATTRIBUTE_NAME_MOLREGIONID] != Constants.NULL_INT.ToString())
                    {
                        dt.Rows.Add(new object[] { g.GetResource("YOUR_REGION", this), "-1" });
                    }
                    else
                    {
                        dt.Rows.Add(new object[] { FrameworkGlobals.GetUnicodeText(g.GetResource("YOUR_REGION", this)), g.Member.GetAttributeInt(g.Brand, WebConstants.ATTRIBUTE_NAME_REGIONID) });
                        g.Session.Add(WebConstants.ATTRIBUTE_NAME_MOLREGIONID, g.Member.GetAttributeInt(g.Brand, WebConstants.ATTRIBUTE_NAME_REGIONID).ToString(), SessionPropertyLifetime.Temporary);
                    }
                }

                if (g.Brand.Site.Community.CommunityID != (Int32)WebConstants.COMMUNITY_ID.JDate)
                {
                    dt.Rows.Add(new object[] { g.GetResource("AUSTRALIA", this), ConstantsTemp.REGIONID_AUSTRALIA });
                }
                if (g.Brand.Site.SiteID == (Int32)WebConstants.SITE_ID.JDateFR)
                {
                    dt.Rows.Add(new object[] { g.GetResource("BELGIUM", this), ConstantsTemp.REGIONID_BELGIUM });
                }
                //dt.Rows.Add(new object[] { g.GetResource("CANADA", this), ConstantsTemp.REGIONID_CANADA });
                if (g.Brand.Site.SiteID == (Int32)WebConstants.SITE_ID.JDateFR)
                {
                    dt.Rows.Add(new object[] { g.GetResource("FRANCE", this), ConstantsTemp.REGIONID_FRANCE });
                }
                if (g.Brand.Site.Community.CommunityID != (Int32)WebConstants.COMMUNITY_ID.JDate)
                {
                    dt.Rows.Add(new object[] { g.GetResource("GERMANY", this), ConstantsTemp.REGIONID_GERMANY });
                }
                dt.Rows.Add(new object[] { g.GetResource("ISRAEL", this), ConstantsTemp.REGIONID_ISRAEL });
                if (g.Brand.Site.SiteID == (Int32)WebConstants.SITE_ID.JDateFR)
                {
                    dt.Rows.Add(new object[] { g.GetResource("LUXEMBURG", this), ConstantsTemp.REGIONID_LUXEMBURG });
                }
                //dt.Rows.Add(new object[] { g.GetResource("UNITED_KINGDOM", this), ConstantsTemp.REGIONID_UNITED_KINGDOM });
                //dt.Rows.Add(new object[] { g.GetResource("UNITED_STATES", this), ConstantsTemp.REGIONID_USA });
                if (g.Brand.Site.SiteID == (Int32)WebConstants.SITE_ID.JDateFR)
                {
                    dt.Rows.Add(new object[] { g.GetResource("SWITZERLAND", this), ConstantsTemp.REGIONID_SWITZERLAND });
                }

                dt.Rows.Add(new object[] { g.GetResource("ANYWHERE", this), Constants.NULL_INT });

            }

            return dt;
        }

        private void Page_Init(object sender, System.EventArgs e)
        {
            try
            {
                GenderMask.DataSource = GetGenderOptions();
                GenderMask.DataTextField = "Content";
                GenderMask.DataValueField = "Value";
                GenderMask.DataBind();

                RegionID.DataSource = GetRegionOptions();
                RegionID.DataTextField = "Content";
                RegionID.DataValueField = "Value";
                RegionID.DataBind();

                string mmRegionID = g.Session["MatchMeterRegion"].ToString();
                if (g.Session["MatchMeterRegion"].ToString() == "-1")
                {
                    mmRegionID = g.Session[WebConstants.ATTRIBUTE_NAME_MOLREGIONID];
                }
                if (_g.Member != null && !string.IsNullOrEmpty(mmRegionID))
                {
                    RegionID.SelectedIndex = RegionID.Items.IndexOf(RegionID.Items.FindByValue(mmRegionID));
                }
                else
                {
                    if (OMemberSearch != null)
                    {

                        ListItem li = RegionID.Items.FindByValue(OMemberSearch.RegionID.ToString());
                        if (li != null)
                        {
                            RegionID.SelectedIndex = RegionID.Items.IndexOf(li);
                        }
                    }
                    else if (_g.Brand.Site.DefaultRegionID != Constants.NULL_INT)
                    {

                        if (g.Brand.Site.Community.CommunityID == (int)WebConstants.COMMUNITY_ID.Cupid)
                        {
                            //Cupid defaults to "Anywhere"
                            RegionID.SelectedIndex =
                                RegionID.Items.IndexOf(RegionID.Items.FindByValue(Constants.NULL_INT.ToString()));
                        }
                        else
                        {
                            RegionID.SelectedIndex =
                                RegionID.Items.IndexOf(
                                    RegionID.Items.FindByValue(_g.Brand.Site.DefaultRegionID.ToString()));
                        }

                    }
                }
                if (mmRegionID != null && mmRegionID != string.Empty)
                {
                    RegionID.Value = mmRegionID;
                }

                // min age
                string newValue;
                if (Request["AgeMin"] != null)
                {
                    AgeMin.Value = Request["AgeMin"];
                }
                else
                {
                    newValue = g.Session["MatchMeterAgeMin"];
                    if (newValue != null && newValue != string.Empty)
                    {
                        this.AgeMin.Value = newValue;
                    }
                    else
                    {
                        if (OMemberSearch != null)
                        {
                            this.AgeMin.Value = OMemberSearch.Age.MinValue.ToString();
                        }
                        else
                        {
                            this.AgeMin.Value = g.Brand.DefaultAgeMin.ToString();
                        }

                    }
                }

                if (Request["AgeMax"] != null)
                {
                    AgeMax.Value = Request["AgeMax"];
                }
                else
                {
                    newValue = g.Session["MatchMeterAgeMax"];
                    if (newValue != null && newValue != string.Empty)
                    {
                        this.AgeMax.Value = newValue;
                    }
                    else
                    {
                        if (OMemberSearch != null)
                        {
                            this.AgeMax.Value = OMemberSearch.Age.MaxValue.ToString();
                        }
                        else
                        {
                            this.AgeMax.Value = g.Brand.DefaultAgeMax.ToString();
                        }

                    }
                }

                newValue = g.Session["MatchMeterGender"];
                if (newValue != null && newValue != string.Empty)
                    this.GenderMask.Value = newValue;
                else
                {
                    if (OMemberSearch != null)
                    {
                        this.GenderMask.Value = OMemberSearch.SeekingGender == Gender.Male ? "1" : "0";
                    }
                }

                newValue = g.Session["MatchMeterPhotoRequired"];
                if (!string.IsNullOrEmpty(newValue))
                {
                    bool val;
                    if (bool.TryParse(newValue, out val))
                    {
                        chkPhotoRequired.Checked = val;
                    }
                    else
                    {
                        chkPhotoRequired.Checked = true;
                    }
                }
                else
                {
                    chkPhotoRequired.Checked = true;
                }



                if (Request["Postback"] == null)
                {
                    if (!IsPostBack)
                    {
                        if ((int)WebConstants.COMMUNITY_ID.Cupid == g.Brand.Site.Community.CommunityID)
                        {	// Default the filter criteria differently for cupid.co.il
                            GenderMask.SelectedIndex = 0;	// Anyone

                            // Anywhere
                            if (this.RegionID.Value != null && this.RegionID.Value != string.Empty)
                            {
                                RegionID.SelectedIndex = -1;
                                RegionID.Items.FindByValue(Constants.NULL_INT.ToString()).Selected = true;
                            }
                        }
                    }
                }
            }

            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        private MemberSearch getMemberSearch()
        {
            MemberSearch search = null;
            try
            {
                if (SearchFoundInSession())
                {
                    search = MemberSearchCollection.Load(Constants.NULL_INT, g.Brand).PrimarySearch;

                }
                else
                {
                    if (g.Member != null)
                    {
                        search = MemberSearchCollection.Load(g.Member.MemberID, g.Brand).PrimarySearch;
                        /*  if (search != null)
                          {
                              foundInSession = UpdateSearchFromSession(search);
                          }*/
                        search.MemberID = Constants.NULL_INT;
                    }
                    else
                    {
                        search = MemberSearchCollection.Load(Constants.NULL_INT, g.Brand).PrimarySearch;
                    }
                }

                return search;
            }
            catch (Exception ex)
            {
                return search;
            }

        }

        public bool SearchFoundInSession()
        {
            bool found;
            if (GetPreferenceSessionValue("GenderMask") == null || GetPreferenceSessionValue("RegionID") == null
                    || GetPreferenceSessionValue("MinAge") == null || GetPreferenceSessionValue("MaxAge") == null || GetPreferenceSessionValue("Distance") == null)
                found = false;
            else
                found = true;

            return found;

        }

        public string GetPreferenceSessionValue(string prefName)
        {
            string value = null;
            try
            {
                string keyFormat = "QS_" + "{0}";
                string key = String.Format(keyFormat, prefName.ToUpper());
                value = g.Session.GetString(key, null);
                return value;
            }
            catch (Exception ex)
            {
                return null;
            }
        }



        #endregion Methods


        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            this.Load += new System.EventHandler(this.Page_Load);
            this.Init += new System.EventHandler(this.Page_Init);

        }
        #endregion
    }
}