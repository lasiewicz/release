﻿using System;
using System.Linq;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Web.Applications.Email;
using Matchnet.Web.Framework;
using Matchnet.Email.ValueObjects;
using Matchnet.Email.ServiceAdapters;

using Matchnet.MatchTest.ServiceAdapters;
using Matchnet.MatchTest.ValueObjects.MatchMeter;
using System.Text.RegularExpressions;
using Matchnet.List.ServiceAdapters;
using Matchnet.List.ValueObjects;

using Matchnet.Content.ValueObjects.Quotas;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ServiceAdapters.Links;
using Matchnet.Lib;
using Matchnet.Content.ValueObjects.PageConfig;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Web.Framework.Managers;

namespace Matchnet.Web.Applications.CompatibilityMeter
{
    public partial class SendInvitation20 : FrameworkControl
    {
        #region Fields (1)

        private const int MESSAGE_BODY_MAX_CONCURRENT = 30;

        private bool IsPopup
        {
            get { return (!string.IsNullOrEmpty(Request["opener"])); }
        }

        #endregion Fields

        #region Methods (10)


        // Protected Methods (4) 

        protected void btnMatches_Click(object sender, EventArgs e)
        {

        }

        protected void lnkSendInvitation_Click(object sender, EventArgs e)
        {
            if (compileSendMessage())
            {
                // Update sending member quota
                QuotaSA.Instance.AddQuotaItem(g.Brand.Site.Community.CommunityID, g.Member.MemberID, QuotaType.SendJmeterInvitation);
                // Upadte source member quota 
                //QuotaSA.Instance.AddQuotaItem(g.Brand.Site.Community.CommunityID, getMemberId(), QuotaType.ReceiveJmeterInvite);

                // save the to member in the current member's hotlist
                /* ListSA.Instance.AddListMember(HotListCategory.MembersYouEmailed,
                      g.Brand.Site.Community.CommunityID,
                      g.Brand.Site.SiteID,
                      g.Session.GetInt(WebConstants.SESSION_PROPERTY_NAME_MEMBERID),
                      messageSave.ToMemberID,
                      null,
                      Constants.NULL_INT,
                      true,
                      ignoreQuota);*/


                //Save the member in the member you sent jmeter invitation to
                ListSA.Instance.AddListMember(HotListCategory.MemberYouSentJmeterInvitationTo,
                    g.Brand.Site.Community.CommunityID,
                    g.Brand.Site.SiteID,
                    g.Member.MemberID,
                    getMemberId(),
                    string.Empty,
                    0,
                    false,
                    true);

                ShowNotification("JMETER_INVITATION_SUCCESSFULLY_SENT");
            }
            else
            {
                //g.Notification.AddError("TXT_MESSAGE_SENT_FAILURE");
                ShowNotification("TXT_MESSAGE_SENT_FAILURE");
            }

        }

        protected override void OnPreRender(EventArgs e)
        {
            try
            {
                _g.AnalyticsOmniture.PageName = "JMeter Invite";
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                if (!string.IsNullOrEmpty(Request["opener"]))
                {
                    hfOpenerURL.Value = Request["opener"];
                }
                hfRefURL.Value = g.GetReferrerUrl();

                // set placeholders
                plcStandardOpen.Visible = !IsPopup;
                plcStandardClose.Visible = !IsPopup;
                plcPopup.Visible = IsPopup;

                int memberID = getMemberId();

                Member.ServiceAdapters.Member aMember = MemberSA.Instance.GetMember(memberID, MemberLoadFlags.None);

                microProfile.Member = aMember;
                microProfile.Transparency = true;

                txtMessage.Text = g.GetResource("TEXTBOX_CONTENT", this).Replace("[USERNAME]", g.Member.GetUserName(_g.Brand));
                if (MemberPrivilegeAttr.IsCureentSubscribedMember(g))
                {
                    txtMessage.Enabled = true;
                }

                string matchesPageURL = FrameworkGlobals.LinkHref("/Applications/CompatibilityMeter/Matches.aspx", false);
                lnkYourMatches1.NavigateUrl = matchesPageURL;
                // If we're in a popup, open the matches page in a new window
                if (g.LayoutTemplate == LayoutTemplate.Popup || g.LayoutTemplate == LayoutTemplate.PopupProfile)
                {
                    lnkYourMatches1.Attributes.Add("onclick", string.Format("javascript:window.open('{0}', 'matches_window', 'height=664,width=780,status=yes,toolbar=yes,menubar=yes,location=yes, resizable=yes, scrollbars=1');", matchesPageURL));
                    lnkYourMatches1.NavigateUrl = string.Empty;
                }

                if (memberID > 0)
                {
                    CompatibilityMeterHandler cmh = new CompatibilityMeterHandler(g);
                    string notification = cmh.CheckSendInvitationNotification(aMember);
                    if (string.IsNullOrEmpty(notification))
                    {


                        // Check if current member finished the test
                        if (IsMemeberHasFeedback(g.Member.MemberID))
                        {
                            lnkBackToProfile.NavigateUrl = FrameworkGlobals.LinkHref("/Applications/MemberProfile/ViewProfile.aspx?MemberID=" + memberID.ToString(), false);
                            lnkSendAMessage.NavigateUrl = FrameworkGlobals.LinkHref(string.Format("/Applications/Email/Compose.aspx?MemberID={0}&LinkParent={1}", memberID.ToString(), (int)LinkParent.MatchMeterSendInvitation), false);

                            int memGenderMask = g.Member.GetAttributeInt(g.Brand, WebConstants.ATTRIBUTE_NAME_GENDERMASK);
                            int targetMemGenderMask = aMember.GetAttributeInt(g.Brand, WebConstants.ATTRIBUTE_NAME_GENDERMASK);
                            string currentMemberSex = ((memGenderMask & ConstantsTemp.GENDERID_MALE) == ConstantsTemp.GENDERID_MALE) ? "M" : "F";
                            string targetMemberSex = ((targetMemGenderMask & ConstantsTemp.GENDERID_MALE) == ConstantsTemp.GENDERID_MALE) ? "M" : "F";

                            string txt = g.GetResource("TXT_TEXT_MALE", this);
                            if (targetMemberSex == "F")
                            {
                                txt = g.GetResource("TXT_TEXT_FEMALE", this);
                            }

                            lblText.Text = txt.Replace("[USERNAME]", aMember.GetUserName(_g.Brand));
                            string txt2 = g.GetResource("TXT_TEXT2_MALE2FEMALE", this); // M sends to F
                            imgSendInvitation.FileName = "jmeter_btn_invite_mf.gif";
                            imgSendInvitation.TitleResourceConstant = "TTL_INVITE_MF";
                            if (currentMemberSex == "M" && targetMemberSex == "M") // M sends to M
                            {
                                txt2 = g.GetResource("TXT_TEXT2_MALE2MALE", this);
                                imgSendInvitation.FileName = "jmeter_btn_invite_mm.gif";
                                imgSendInvitation.TitleResourceConstant = "TTL_INVITE_MM";
                            }
                            else
                            {
                                if (currentMemberSex == "F" && targetMemberSex == "M") // F sends to M
                                {
                                    txt2 = g.GetResource("TXT_TEXT2_FEMALE2MALE", this);
                                    imgSendInvitation.FileName = "jmeter_btn_invite_fm.gif";
                                    imgSendInvitation.TitleResourceConstant = "TTL_INVITE_FM";
                                }
                                else
                                {
                                    if (currentMemberSex == "F" && targetMemberSex == "F") // F sends to F
                                    {
                                        txt2 = g.GetResource("TXT_TEXT2_FEMALE2FEMALE", this);
                                        imgSendInvitation.FileName = "jmeter_btn_invite_ww.gif";
                                        imgSendInvitation.TitleResourceConstant = "TTL_INVITE_FF";
                                    }
                                }
                            }
                            lblText2.Text = txt2;
                        }
                        else
                        {
                            // Current member hasn't finished the test
                            g.Transfer("/Applications/CompatibilityMeter/Welcome.aspx");
                        }
                    }
                    else
                    {
                        ShowNotification(notification);
                    }
                }
                else
                {
                    // no mem ID in the URL.
                    microProfile.Visible = false;
                }
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);

                if (!g.IsDevMode && !(ex is System.Threading.ThreadAbortException))
                {
                    g.Transfer("/Applications/CompatibilityMeter/Error.aspx");
                }
            }
        }


        // Private Methods (6) 

        // compiles and attempts to send/save the message
        private bool compileSendMessage()
        {
            string messageSubject;
            string messageBody;

            try
            {
                messageSubject = g.GetResource("INVITATION_SUBJECT", this).Trim();
                messageBody = txtMessage.Text.Trim();

                if (messageSubject.Length <= 0)
                {
                    g.Notification.AddError("TXT_MESSAGE_SUBJECT_BLANK");
                    return false;
                }

                if (messageBody.Length <= 0)
                {
                    g.Notification.AddError("TXT_MESSAGE_BODY_BLANK");
                    return false;
                }

                Int32 toMemberID;

                toMemberID = getMemberId();

                // parse out all html tags
                messageSubject = Regex.Replace(messageSubject, "<[^>]*>", "");
                messageBody = Regex.Replace(formatMessageBody(messageBody), "<[^>]*>", "");

                MailType mailType = MailType.CompatibilityMeterEmail;

                MessageSave messageSave = new MessageSave(g.Member.MemberID,
                    toMemberID,
                    g.Brand.Site.Community.CommunityID,
                    g.Brand.Site.SiteID,
                    mailType,
                    messageSubject,
                    messageBody);

                messageSave.MessageListID = 0;// getMessageId();

                // If the sender is blocked by the receiver, send a blocked message to the sender and halt sending to the receiver
                bool blocked = false;
                try
                {
                    Member.ServiceAdapters.Member toMember = MemberSA.Instance.GetMember(messageSave.ToMemberID, MemberLoadFlags.None);
                    MessageSendResult sendResult = EmailMessageSA.Instance.CheckAndSendMessageForBlockedSenderInRecipientList(messageSave.GroupID, string.Format(_g.GetResource("TXT_IGNORE_SUBJECT", this), messageSave.MessageHeader), string.Format(_g.GetResource("TXT_IGNORE_CONTENT", this), toMember.GetUserName(_g.Brand)),
                                                                                                                              toMember.MemberID, _g.Member.MemberID, _g.Brand.Site.SiteID,
                                                                                                                              _g.Brand.Site.Community.CommunityID);
                    blocked = (null != sendResult && sendResult.Status == MessageSendStatus.Success);
                    ComposeHandler.LogIgnoreEMail(_g, _g.Member.MemberID.ToString(), messageSave.ToMemberID.ToString());
                }
                catch (Exception e)
                {
                    g.LoggingManager.LogException(e, _g.Member, _g.Brand, "SendInvitation20");
                }
                if (blocked) return true;

                // Try to send email message

                MessageSendResult messageSendResult = EmailMessageSA.Instance.SendMessage(messageSave, true, 0, true);

                switch (messageSendResult.Status)
                {
                    case MessageSendStatus.Success:                        // Check to see if we need to send a "Recipient on Vacation" email.
                        if (IsMemberOnVacation(messageSave.ToMemberID))
                        {
                            Member.ServiceAdapters.Member toMember = MemberSA.Instance.GetMember(messageSave.ToMemberID, MemberLoadFlags.None);

                            MessageSave vacationMessage = new MessageSave(messageSave.ToMemberID,
                                g.Member.MemberID,
                                g.Brand.Site.Community.CommunityID,
                                g.Brand.Site.SiteID,
                                MailType.Email,
                                String.Format(g.GetResource("TXT_VACATION_SUBJECT", this), messageSave.MessageHeader),
                                String.Format(g.GetResource("TXT_VACATION_CONTENT", this), toMember.GetUserName(_g.Brand)));

                            EmailMessageSA.Instance.SendMessage(vacationMessage, false, Constants.NULL_INT, false);
                        }

                        // send an external mail to let recipient know that he/she has a message

                        return true;

                    case MessageSendStatus.Failed:
                        // one cause of failure is the per day send limit
                        if (messageSendResult.Error == MessageSendError.ComposeLimitReached)
                        {

                            /*   g.Transfer("/Applications/Email/TeaseTooMany.aspx?" + TeaseTooMany.QSPARAM_ERROR_OCCURRED + "=true&" + TeaseTooMany.QSPARAM_ERROR_TYPE + "=email");*/
                        }
                        // everything else is unknown
                        else
                        {
                            throw new Exception("Unknown exception occured in compileSendMessage().");
                        }
                        return false;

                    default:
                        return false;
                }
            }
            catch (Exception anException)
            {
                g.ProcessException(anException);
            }
            return false;
        }

        // buffer out the message body with spaces for every 30 characters encountered.
        private string formatMessageBody(string messageBody)
        {
            int currentCount = 0;

            for (int i = 0; i < messageBody.Length; i++)
            {
                if (messageBody[i].Equals(' ') || messageBody[i].Equals('\n'))
                {
                    currentCount = 0;
                }
                else
                {
                    currentCount++;
                    if (currentCount == MESSAGE_BODY_MAX_CONCURRENT)
                    {
                        messageBody = messageBody.Insert(i, " ");
                        currentCount = 0;
                    }
                }
            }
            return messageBody;
        }

        private int getMemberId()
        {
            try
            {
                if (Request.Params.Get("MemberID") != null)
                {
                    return int.Parse(Request.Params.Get("MemberID"));
                }
            }
            catch (FormatException)
            {
                return 0;
            }
            return 0;
        }

        private Boolean IsMemberOnVacation(Int32 toMemberID)
        {
            // Retrieve the ToMember's VacationStartDate.
            Member.ServiceAdapters.Member toMember = MemberSA.Instance.GetMember(toMemberID, MemberLoadFlags.None);

            DateTime vacationStartDate = toMember.GetAttributeDate(g.Brand, "VacationStartDate");

            if (vacationStartDate == DateTime.MinValue)
                return false;
            else
                return true;
        }

        private bool IsMemeberHasFeedback(int memberID)
        {
            //var pfb = MatchMeterSA.Instance.GetPersonalFeedback(memberID, g.Brand.Site.LanguageID, MemberPrivilegeAttr.IsCureentSubscribedMember(g));
            MatchTestStatus mts = MatchMeterSA.Instance.GetMatchTestStatus(memberID, MemberPrivilegeAttr.IsCureentSubscribedMember(g), g.Brand);
            return (mts == Matchnet.MatchTest.ValueObjects.MatchMeter.MatchTestStatus.MinimumCompleted || mts == Matchnet.MatchTest.ValueObjects.MatchMeter.MatchTestStatus.Completed);
        }

        private void ShowNotification(string notificationResource)
        {
            string destURL = IsPopup ? hfOpenerURL.Value : hfRefURL.Value;
            if (!string.IsNullOrEmpty(destURL))
            {
                if (destURL.IndexOf("rc") != -1)
                {
                    destURL = g.RemoveParamFromURL(destURL, "rc", false);
                }

                destURL += destURL.Contains('?') ? "&" : "?";
                destURL += "rc=" + notificationResource;
                if (IsPopup)
                {
                    this.Page.ClientScript.RegisterStartupScript(this.GetType(), "parentredirect", "window.opener.location.href='" + destURL + "';\nwindow.close();", true);
                }
                else
                {
                    g.Transfer(destURL);
                }
            }
            else
            {
                g.Notification.AddMessage(notificationResource);
            }
        }
        #endregion Methods
    }
}
