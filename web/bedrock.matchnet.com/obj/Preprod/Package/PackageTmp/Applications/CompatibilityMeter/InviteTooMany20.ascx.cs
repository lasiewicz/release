﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Ui.FormElements;
using Matchnet.Web.Framework.Util;
using Matchnet.Web.Framework.Ui.BasicElements;

namespace Matchnet.Web.Applications.CompatibilityMeter
{
    public partial class InviteTooMany20 : FrameworkControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(g.GetReferrerUrl()))
            {
                hlBack.NavigateUrl = g.GetReferrerUrl();
            }
        }
    }
}