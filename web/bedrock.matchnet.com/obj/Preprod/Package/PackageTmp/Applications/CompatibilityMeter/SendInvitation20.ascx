﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SendInvitation20.ascx.cs" Inherits="Matchnet.Web.Applications.CompatibilityMeter.SendInvitation20" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn" TagName="microProfile" Src="../../Framework/Ui/BasicElements/GalleryMiniProfile.ascx" %>
<%@ Register TagPrefix="uc1" TagName="LookupByUsername" Src="Controls/LookupByUsername.ascx" %>
<%@ Register TagPrefix="uc1" TagName="SendToAFriend" Src="Controls/SendToAFriend.ascx" %>
<asp:PlaceHolder runat="server" ID="plcStandardOpen">
    <div id="rightNew">
        <div id="jmeterCenterWrapper">
            <mn:Image runat="server" ID="JMeterTopCap" FileName="jmeter_containerTopCap.gif"
                Style="display: block" alt="" />
            <div class="jmeterCenterContainer clear-floats">
                <div id="jmeterLeft">
                    <h2>
                        <mn:Image runat="server" ID="JMeterLogo" FileName="jmeter_logo.gif" ResourceConstant="ALT_JMETER_LOGO"
                            TitleResourceConstant="TTL_JMETER_LOGO" />
                    </h2>
                    <p>
                        <asp:HyperLink runat="server" ID="lnkYourMatches1">
                            <mn:Image runat="server" ID="Image2" FileName="jmeter_btnYourMatches.gif" ResourceConstant="ALT_JMETER_YOUR_MATCHES"
                                TitleResourceConstant="TTL_JMETER_YOUR_MATCHES" CssClass="cursor" />
                        </asp:HyperLink></p>
                </div>
                <!-- jmeterLeft end -->
                <div id="jmeterRight">
                    <%--<mn:Title runat="server" ID="ttlSendInvitation" ResourceConstant="TTL_SEND_INVITATION" />--%>
</asp:PlaceHolder>
<asp:PlaceHolder runat="server" ID="plcPopup">
    <div class="padding-right-5px">
        <%--<span class="close-button-text close-button-text-cancel">--%>
        <div class="floatOutside">
        <span><a class="ppLink" href="#"
            onclick="window.close();">
            <mn:Txt ID="txtClose" CssClass="close-color" runat="server" ResourceConstant="CLOSE"></mn:Txt>
        </a><a href="#" onclick="window.close();">
            <mn:Image ID="Image1" runat="server" FileName="icon_close1.gif" align="absmiddle"
                Hspace="4"></mn:Image>
        </a>
        </span>
        <%--<span><mn:Txt runat="server" ID="txtCancelTitle" ResourceConstant="TXT_CANCEL_TITLE" /></span>--%>
        </div>
</asp:PlaceHolder>
<p>
    <b>
        <mn:Txt runat="server" ID="txtHowDoWeMatch" ResourceConstant="TXT_HOW_DO_WE_MATCH" />
    </b>
</p>
<p>
    <asp:Label runat="server" ID="lblText"></asp:Label>
</p>
<p>
    <asp:Label runat="server" ID="lblText2"></asp:Label>
</p>
<p>
    <asp:TextBox runat="server" ID="txtMessage" Enabled="false" TextMode="MultiLine"
        Width="200px" Rows="5"></asp:TextBox></p>
<p>
    <asp:LinkButton runat="server" ID="lnkSendInvitation" OnClick="lnkSendInvitation_Click">
        <mn:Image runat="server" ID="imgSendInvitation" FileName="jmeter_btn_invite_male.gif"
            ResourceConstant="TXT_SEND_INVITATION" />
    </asp:LinkButton>
</p>
<!-- jmeterRightend -->
</div>
<asp:PlaceHolder runat="server" ID="plcStandardClose">
    <mn:microProfile runat="server" ID="microProfile" />
    <asp:HyperLink runat="server" ID="lnkBackToProfile">
        <mn:Txt runat="server" ID="txtBackToProfile" ResourceConstant="TXT_BACK_TO_PROFILE" />
    </asp:HyperLink>
    <br />
    <asp:HyperLink runat="server" ID="lnkSendAMessage">
        <mn:Image runat="server" ID="imgSendMessage" FileName="jmeter_btn_email.gif" ResourceConstant="TXT_SEND_MESSAGE"
            TitleResourceConstant="TXT_SEND_MESSAGE" />
    </asp:HyperLink>
    <!-- jmeterCenterContainer end -->
    </div>
    <!-- jmeterCenterWrapper end -->
    </div>
    <!-- end Right Div area -->
</asp:PlaceHolder>
<asp:HiddenField runat="server" ID="hfRefURL" />
<asp:HiddenField runat="server" ID="hfOpenerURL" />
