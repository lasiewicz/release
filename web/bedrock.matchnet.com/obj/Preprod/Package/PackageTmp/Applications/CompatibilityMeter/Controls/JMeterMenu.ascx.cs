﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.MatchTest.ServiceAdapters;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Ui;

namespace Matchnet.Web.Applications.CompatibilityMeter.Controls
{
    public partial class JMeterMenu : FrameworkControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var cmh = new CompatibilityMeterHandler(g);

            plcLICompletion.Visible = cmh.IsMemeberHasFeedback();
            plcLIPersonalityTest.Visible = !plcLICompletion.Visible;
            plcLIMatches.Visible = cmh.IsJMeterMatchesEnabled();
        }

        public string GetClassActive(string pageName)
        {
            if (g.AppPage.ControlName == pageName)
            {
                return "class=\"active\"";
            }
            return string.Empty;
        }

        public string GetClassNoMatches()
        {
            var cmh = new CompatibilityMeterHandler(g);
            return cmh.IsJMeterMatchesEnabled() ? string.Empty : "no-matches";
        }
    }
}