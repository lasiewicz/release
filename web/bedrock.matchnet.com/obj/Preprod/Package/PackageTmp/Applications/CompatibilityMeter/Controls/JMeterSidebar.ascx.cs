﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Web.Framework;

namespace Matchnet.Web.Applications.CompatibilityMeter.Controls
{
    public partial class JMeterSidebar : FrameworkControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            CompatibilityMeterHandler compatibilityMeterHandler = new CompatibilityMeterHandler(g);

            //plcPrintFeedback.Visible = (g.AppPage.ControlName == "Completion");
            plcPrintFeedback.Visible = (compatibilityMeterHandler.GetCurrentPage() == CompatibilityMeterHandler.JmeterPage.Completion) ||
                                        (compatibilityMeterHandler.GetCurrentPage() == CompatibilityMeterHandler.JmeterPage.OneOnOne);

            plcRaananPhoto.Visible = compatibilityMeterHandler.GetCurrentPage() ==
                                     CompatibilityMeterHandler.JmeterPage.Welcome;

            if ((compatibilityMeterHandler.GetCurrentPage() == CompatibilityMeterHandler.JmeterPage.Welcome) ||
                (compatibilityMeterHandler.GetCurrentPage() == CompatibilityMeterHandler.JmeterPage.Error) ||
                (compatibilityMeterHandler.GetCurrentPage() == CompatibilityMeterHandler.JmeterPage.PersonalityTest) ||
                (compatibilityMeterHandler.GetCurrentPage() == CompatibilityMeterHandler.JmeterPage.Matches) ||
                !compatibilityMeterHandler.IsJMeterMatchesEnabled())
            {
                plcJMeterMatches.Visible = false;
            }

            if ((compatibilityMeterHandler.GetCurrentPage() == CompatibilityMeterHandler.JmeterPage.Welcome) ||
                (compatibilityMeterHandler.GetCurrentPage() == CompatibilityMeterHandler.JmeterPage.Error) ||
                (compatibilityMeterHandler.GetCurrentPage() == CompatibilityMeterHandler.JmeterPage.PersonalityTest))
            {
                plcMOL.Visible = false;
            }

            if (compatibilityMeterHandler.GetCurrentPage() == CompatibilityMeterHandler.JmeterPage.PersonalityTest)
            {
                plcAdUnit.Visible = false;
                plcJMeterLogo.Visible = false;
            }

            if (compatibilityMeterHandler.GetCurrentPage() == CompatibilityMeterHandler.JmeterPage.PersonalityTest ||
                (compatibilityMeterHandler.GetCurrentPage() == CompatibilityMeterHandler.JmeterPage.Error))
            {
                plcJMeterInvite.Visible = false;
            }

            if (compatibilityMeterHandler.GetCurrentPage() == CompatibilityMeterHandler.JmeterPage.ReturningUser)
            {
                SetBanner();
                plcAdUnit.Visible = false;
                plcAdUnitTop.Visible = true;
            }
        }

        private void SetBanner()
        {
            plcBanner.Visible = true;

            CompatibilityMeterHandler cmh = new CompatibilityMeterHandler(g);
            if (cmh.IsJMeterMatchesEnabled())
            {
                if (MemberPrivilegeAttr.IsCureentSubscribedMember(g))
                {

                    if (cmh.IsJmeterPremium())
                    {
                        if (cmh.IsJMeterMatchesEnabled())
                        {
                            lnkBanner.NavigateUrl = "/Applications/CompatibilityMeter/Matches.aspx";
                            imgBanner.FileName = "returning-banner-prm.png";
                        }
                        else
                        {
                            lnkBanner.Visible = false;
                            imgBanner.Visible = false;
                        }
                    }
                    else
                    {
                       lnkBanner.NavigateUrl = "/Applications/Subscription/PremiumServices.aspx?prtid=664&PackagesToDisplay=JM";
                       imgBanner.FileName = "returning-banner.png";
                    }
                }
                else
                {
                    lnkBanner.NavigateUrl = FrameworkGlobals.GetSubscriptionLink(false, 663) + "&DestinationURL=" + HttpContext.Current.Server.UrlEncode(HttpContext.Current.Items["FullURL"].ToString());
                    imgBanner.FileName = "jmeter_returning_subscribe.jpg";
                }
            }
            else
            {
                lnkBanner.Visible = false;
                imgBanner.Visible = false;
            }
        }
    }
}