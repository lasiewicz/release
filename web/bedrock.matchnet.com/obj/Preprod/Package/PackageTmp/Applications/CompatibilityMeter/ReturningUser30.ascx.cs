﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.UI.WebControls;
using Matchnet.MatchTest.ServiceAdapters;
using Matchnet.Member.ServiceAdapters.Interfaces;
using Matchnet.Member.ValueObjects.Interfaces;
using Matchnet.Web.Applications.CompatibilityMeter.Controls;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Ui.BasicElements;

namespace Matchnet.Web.Applications.CompatibilityMeter
{
    public partial class ReturningUser30 : FrameworkControl
    {
        private Dictionary<IMemberDTO, decimal> MatchResultsMemberScoreDictionary;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                LoadMicroProfiles();
                lnkMatches.Visible = new CompatibilityMeterHandler(g).IsJMeterMatchesEnabled();
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        private void LoadMicroProfiles()
        {
            var bmresults = MatchMeterSA.Instance.GetBestMatches(g.Member.MemberID, g.Brand, mf.intRegionID, mf.intAgeMin, mf.intAgeMax, mf.GetGenderMask(), MemberPrivilegeAttr.IsCureentSubscribedMember(g), 0, 2, mf.PhotoRequired);
            if (bmresults != null && bmresults.MatchesCount > 0)
            {
                MatchResultsMemberScoreDictionary = bmresults.MatchResultsDic;
                ArrayList members = new ArrayList(MatchResultsMemberScoreDictionary.Keys);
                rptrYourMatchesList.DataSource = members;
                rptrYourMatchesList.DataBind();
            }
        }

        protected void rptrYourMatchesList_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            ReturningUserMiniProfile returningUserMiniProfile = (ReturningUserMiniProfile)e.Item.FindControl("returningUserMiniProfile");
            returningUserMiniProfile.MatchScore = MatchResultsMemberScoreDictionary[returningUserMiniProfile.Member];
        }

        
        protected override void OnPreRender(EventArgs e)
        {
            try
            {
                _g.AnalyticsOmniture.PageName = "JMeter Returning";
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }
    }
}