﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OneOnOne30.ascx.cs"
    Inherits="Matchnet.Web.Applications.CompatibilityMeter.OneOnOne30" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="uc1" TagName="LookupByUsername" Src="Controls/LookupByUsername.ascx" %>
<%@ Register TagPrefix="mn" TagName="microProfile" Src="../../Framework/Ui/BasicElements/GalleryMiniProfile.ascx" %>
<%@ Register TagPrefix="uc1" TagName="MatchMeterScoreBar" Src="~/Applications/CompatibilityMeter/Controls/MatchMeterScoreBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="OneOnOneCategoryItem" Src="Controls/OneOnOneCategoryItem30.ascx" %>
<%@ Register TagPrefix="uc1" TagName="OneOnOneTop" Src="Controls/OneOnOneTop.ascx" %>
<script type="text/javascript">
    function printdiv(printpage) {
        var headstr = "<html><head><style>.review-details {display:block;}.review-title {width:670px;}.jmeter-category-title > .category-button{display:none}..jmeter-category-title > .category-name{padding-top:15px;}</style><title></title></head><body><div style=\"width:400px; text-align:center\">";
        var footstr = "</div></body>";
        var newstr = document.getElementById(printpage).innerHTML;
        var oldstr = document.body.innerHTML;
        document.body.innerHTML = headstr + newstr + footstr;
        window.print();
        document.body.innerHTML = oldstr;
        return false;
    }
</script>
<!-- jmeterLeft end -->
<div id="div_print">
    <div class="one-on-one-titles">
        <mn:Title runat="server" ID="ttlOneOnOne" ResourceConstant="TTL_ONE_ON_ONE" />
        <h2>
            <mn:Txt runat="server" ID="ttlSubOneOnOne" ResourceConstant="TTL_SUB_ONE_ON_ONE" />
        </h2>
        <div class="title-buttons">
            <asp:PlaceHolder runat="server" ID="plcNextProfile">
                <div class="match-button next-match">
                    <div class="arrow">
                    </div>
                    <asp:HyperLink ID="lnkNextProfile" runat="server">
                        <mn:Txt runat="server" ID="btnMatchNext" ResourceConstant="BTN_MATCH_NEXT" />
                    </asp:HyperLink>
                </div>
            </asp:PlaceHolder>
            <asp:PlaceHolder runat="server" ID="plcPrevProfile">
                <div class="match-button prev-match">
                    <asp:HyperLink ID="lnkPrevProfile" runat="server">
                        <mn:Txt runat="server" ID="btnMatchPrev" ResourceConstant="BTN_MATCH_PREV" />
                    </asp:HyperLink>
                    <div class="arrow">
                    </div>
                </div>
            </asp:PlaceHolder>
            <a class="back-button" onclick="history.go(-1)">
                <mn:Txt runat="server" ID="Txt2" ResourceConstant="TTL_ONE_ON_ONE_BACK" />
            </a>
        </div>
    </div>
    <uc1:OneOnOneTop runat="server" ID="oneOnOneTop"></uc1:OneOnOneTop>
    <div class="jmeterButtonSet">
    </div>
    <asp:Repeater runat="server" ID="rptFeedbackCategories">
        <ItemTemplate>
            <uc1:OneOnOneCategoryItem ID="OneOnOneCategoryItem1" runat="server" OOOCItem='<%# Container.DataItem%>' />
        </ItemTemplate>
    </asp:Repeater>
</div>
<!-- jmeterRightend -->
<!-- end Right Div area -->
<iframe tabindex="-1" name="FrameYNMVote" frameborder="0" width="1" scrolling="no"
    height="1">
    <layer name="FrameYNMVote" frameborder="0" scrolling="no" width="1" height="1"></layer>
</iframe>
