﻿<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<%@ Control Language="c#" AutoEventWireup="false" CodeBehind="LookupByUsername.ascx.cs"
    Inherits="Matchnet.Web.Applications.CompatibilityMeter.Controls.LookupByUsername"
    TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
    <div class="jmeterLookupByUsername">
        <strong>
            <mn:Txt ID="Txt1" runat="server" ResourceConstant="TXT_BY_USERNAME" ExpandImageTokens="True" />
        </strong>
        <br />
        <input type="text" id="LookupUserName" runat="server" name="LookupUserName">
    <mn2:FrameworkButton ID="btnLookupByUserName" runat="server" CssClass="activityButton"
        ResourceConstant="BTN_LOOKUP" Enabled="true" OnClick="btnLookupByUserName_Click" />
    <mn:MultiValidator ID="LookupUserNameValidator" TabIndex="-1" runat="server" MinimumLength="1"
            MaximumLength="25" RequiredType="AlphaNumType" ControlToValidate="LookupUserName"
        FieldNameResourceConstant="USERNAME" IsRequired="True" Display="Dynamic" />
</div>
