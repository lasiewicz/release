﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Error30.ascx.cs" Inherits="Matchnet.Web.Applications.CompatibilityMeter.Error30" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<div id="jmeter-error">
    <img alt="JMeter" src="/img/site/jdate-co-il/jmeter-error-mark.png" />
    <h1>
        <mn:Txt runat="server" ID="txtP1" ResourceConstant="TXT_P_1" />
    </h1>
    <p>
        <mn:Txt runat="server" ID="txtP2" ResourceConstant="TXT_P_2" />
    </p>
    <p class="jmeter-error-button mol">
        <asp:HyperLink runat="server" ID="lnkMOL">
            <%--    <img src="img_jmeter/jmeter_btnMembersOnline.gif" border="0" alt="" title="" />--%>
            <!--<img alt="JMeter" src="/img/site/jdate-co-il/jmeter-error-mol.png" />-->
            <mn:Image ID="btnErrorMol" runat="server" FileName="jmeter-error-mol.png" />
        </asp:HyperLink>
    </p>
    <p class="jmeter-error-button">
        <asp:HyperLink runat="server" ID="lnkSearch">
            <%--    <img src="img_jmeter/jmeter_btnSearchResults.gif" border="0" alt="" title="" />--%>
            <!--<img alt="JMeter" src="/img/site/jdate-co-il/jmeter-error-matches.png" />-->
            <mn:Image ID="btnErrorMatches" runat="server" FileName="jmeter-error-matches.png" />
        </asp:HyperLink>
    </p>
</div>
<!-- end Right Div area -->
