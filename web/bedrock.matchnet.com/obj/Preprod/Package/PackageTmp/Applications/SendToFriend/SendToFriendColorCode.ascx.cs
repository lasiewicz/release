﻿using System;
using Matchnet.Web.Framework;
using Matchnet.ExternalMail.ServiceAdapters;
using Matchnet.Member.ServiceAdapters;
using System.Text.RegularExpressions;
using Matchnet.Web.Applications.ColorCode;

namespace Matchnet.Web.Applications.SendToFriend
{
    public partial class SendToFriendColorCode : FrameworkControl
    {
        #region Event Handlers
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (g.Member != null && ColorCodeHelper.IsColorCodeEnabled(g.Brand) && ColorCodeHelper.HasMemberCompletedQuiz(g.Member, g.Brand))
                {
                    if (g.Session.GetInt(WebConstants.SESSION_PROPERTY_NAME_MEMBERID) > 0)
                    {
                        if (!Page.IsPostBack)
                        {
                            Matchnet.Member.ServiceAdapters.Member member = MemberSA.Instance.GetMember(g.Session.GetInt(WebConstants.SESSION_PROPERTY_NAME_MEMBERID),
                                MemberLoadFlags.None);

                            this.txtYourName.Text = member.GetUserName(g.Brand);
                            this.txtYourEmail.Text = member.EmailAddress;
                        }
                    }

                    if (!Page.IsPostBack)
                    {
                        this.txtSubject.Text = g.GetTargetResource("AN_INTERESTING_PAGE", this).Replace("%s", String.IsNullOrEmpty(g.Member.GetUserName(_g.Brand)) ? g.Member.MemberID.ToString() : g.Member.GetUserName(_g.Brand));
                        this.txtMessage.Text = g.GetTargetResource("HEY_CHECK_OUT_THIS_LINK__518575", this);
                        this.instructionsLiteral.Text = g.GetTargetResource("SEND_INSTRUCTIONS", this).Replace("%s", g.Brand.Uri);
                    }

                    this.fefControl.ElementClientId = this.txtFriendName.ClientID;

                    string reminder = g.GetResource(TxtReminder.ResourceConstant, this);
                    if (String.IsNullOrEmpty(reminder) || reminder.ToLower().IndexOf("_resources") >= 0)
                    {
                        TxtReminder.Visible = false;
                    }
                }
                else
                {
                    g.Transfer("/Applications/ColorCode/Landing.aspx");
                }
            }
            catch (Exception ex) { g.ProcessException(ex); }
            
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            this.btnSendToFriend.Click += new System.EventHandler(this.btnSendToFriend_Click);
        }

        private void btnSendToFriend_Click(object sender, System.EventArgs e)
        {
            string redirectURL = "/Applications/SendToFriend/MessageSent.aspx?colorcodeSTF=true";

            try
            {

                bool isValidRequest = this.validateRequest();

                if (!isValidRequest)
                {
                    return;
                }

                string friendName = txtFriendName.Text;
                string friendEmail = txtFriendEmail.Text;
                string yourEmail = txtYourEmail.Text;
                string yourName = txtYourName.Text;
                string subject = txtSubject.Text;
                string theMessage = txtMessage.Text;
                int memberID = g.Member.MemberID;

                ExternalMailSA.Instance.SendColorCodeToFriend(friendName, friendEmail, yourEmail, yourName, memberID, subject, theMessage, g.Brand.BrandID);
            }
            catch (Exception ex)
            {
                redirectURL += "&Error=true";
                g.ProcessException(ex);
            }

            g.Transfer(redirectURL);
        }

        #endregion

        #region Private methods
        private bool validateRequest()
        {
            bool isValidRequest = true;

            if (Request[this.txtFriendName.UniqueID] == "")
            {
                g.Notification.AddError("FRIENDS_NAME_IS_REQUIRED");
                isValidRequest = false;
            }
            else if (Request[this.txtFriendEmail.UniqueID] == "")
            {
                g.Notification.AddError("FRIENDS_EMAIL_IS_REQUIRED");
                isValidRequest = false;
            }
            else if (!this.isValidEmail(Request[this.txtFriendEmail.UniqueID]))
            {
                g.Notification.AddError("FRIENDS_EMAIL_IS_NOT_A_VALID_EMAIL_ADDRESS");
                isValidRequest = false;
            }
            else if (Request[this.txtYourName.UniqueID] == "")
            {
                g.Notification.AddError("YOUR_NAME_IS_REQUIRED");
                isValidRequest = false;
            }
            else if (Request[this.txtYourEmail.UniqueID] == "")
            {
                g.Notification.AddError("YOUR_EMAIL_ADDRESS_IS_REQUIRED");
                isValidRequest = false;
            }
            else if (!this.isValidEmail(Request[this.txtYourEmail.UniqueID]))
            {
                g.Notification.AddError("YOUR_EMAIL_ADDRESS_IS_NOT_A_VALID_EMAIL_ADDRESS");
                isValidRequest = false;
            }
            else if (Request[this.txtSubject.UniqueID] == "")
            {
                g.Notification.AddError("SUBJECT_IS_REQUIRED");
                isValidRequest = false;
            }
            else if (Request[this.txtMessage.UniqueID] == "")
            {
                g.Notification.AddError("MESSAGE_IS_REQUIRED");
                isValidRequest = false;
            }
            else if (Request[this.txtMessage.UniqueID].Length > 200)
            {
                g.Notification.AddErrorString(g.GetResource("MESSAGE_OVER_MAX_LENGTH", this));
                isValidRequest = false;
            }

            return isValidRequest;
        }

        private bool isValidEmail(string anEmailAddress)
        {
            bool isValidEmail = true;
            Regex emailPattern = new Regex("^\\w+((-\\w+)|(\\.\\w+))*\\@[A-Za-z0-9]+((\\.|-)[A-Za-z0-9]+)*\\.[A-Za-z0-9]+$");

            if (!emailPattern.IsMatch(anEmailAddress))
            {
                isValidEmail = false;
            }

            return isValidEmail;
        }

        #endregion
    }
}