﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SendToFriendColorCode.ascx.cs" Inherits="Matchnet.Web.Applications.SendToFriend.SendToFriendColorCode" %>
<%@ Register TagPrefix="uc1" TagName="MiniProfile" Src="/Framework/Ui/BasicElements/MiniProfile20.ascx" %>
<%@ Register TagPrefix="dg" TagName="FirstElementFocus" Src="/Framework/JavaScript/FirstElementFocus.ascx" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="cc1" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>

<div id="page-container" class="cc-content-main page-colorcode">
<h2 class="tagline"><mn:txt id="mntxt5614" runat="server" resourceconstant="TXT_COLOR_CODE_HEADER" expandimagetokens="true" /></h2>

<div id="cc-content" class="cc-content-full">
<mn:Title runat="server" id="ttlHeader" ResourceConstant="TXT_SEND_TO_A_FRIEND" ImageName="hdr_send_to_friend.gif" CommunitiesForImage="CI;" />
<div id="send-to-friend">
<dg:FirstElementFocus id="fefControl" runat="server" />

<div class="email-form-wrapper">
    <fieldset id="friend">
       <label for="txtFriendName">
            <mn:txt runat="server" id="txtFriendsName" ResourceConstant="FRIENDSNAME" />:
       </label>
        <asp:textbox id="txtFriendName" runat="server"></asp:textbox><br />
         <label for="txtFriendEmail">
            <mn:txt runat="server" id="txtFriendsEmail" ResourceConstant="FRIENDSEMAIL" />:
         </label>
         <asp:textbox id="txtFriendEmail" runat="server" dir="ltr"></asp:textbox><br />
    </fieldset>
     <fieldset id="you">
        <label for="txtYourName">
            <mn:txt runat="server" id="txtYourName1" ResourceConstant="YOURNAME" />:
        </label>
          <asp:textbox id="txtYourName" runat="server"></asp:textbox><br/>
          <label for="txtYourEmail"> 
            <mn:txt runat="server" id="txt1" ResourceConstant="TXT_YOUR_EMAIL" />:
           </label>
           <asp:textbox id="txtYourEmail" runat="server" dir="ltr"></asp:textbox><br/>
    </fieldset>
    <br class="clear-both" />
    <label for="txtSubject1">
        <mn:txt runat="server" id="txtSubject1" ResourceConstant="EMAILSORT__SUBJECT_520331" />:
    </label>
    <asp:textbox id="txtSubject" runat="server" Columns="50" MaxLength="200"></asp:textbox><br />
    
     <label for="txtMessage">
        <mn:txt runat="server" id="txtMessage1" ResourceConstant="MESSAGE__518570"  />:
     </label>
     <asp:textbox id="txtMessage" runat="server" TextMode="MultiLine" Columns="50" Rows="7" MaxLength="200"></asp:textbox> <br />
    </div>
    <hr class="hr-thin" />
    <div class="text-center">
        <cc1:FrameworkButton class="btn primary" id="btnSendToFriend" runat="server" ResourceConstant="BTN_SEND"></cc1:FrameworkButton>
    </div>

    <div class="border-gen tips">
	    <p><mn:txt runat="server" id="TxtReminder" ResourceConstant="REMINDER" /></p>
	    <p><asp:Literal Runat="server" ID="instructionsLiteral"></asp:Literal></p>
     </div>
</div>
</div>

</div>