using System;
using System.Text.RegularExpressions;
using Matchnet.Web.Framework;
using Matchnet.ExternalMail.ServiceAdapters;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Web.Applications.MemberProfile;
using Matchnet.PremiumServiceSearch.ServiceAdapters;
using Matchnet.ActivityRecording.ServiceAdapters;

namespace Matchnet.Web.Applications.SendToFriend
{
	/// <summary>
	///		Summary description for SendToFriend.
	/// </summary>
	public class SendToFriendQandA : FrameworkControl
	{
		protected System.Web.UI.WebControls.TextBox txtFriendName;
		protected System.Web.UI.WebControls.TextBox txtYourName;
		protected System.Web.UI.WebControls.TextBox txtFriendEmail;
		protected System.Web.UI.WebControls.TextBox txtYourEmail;
		protected System.Web.UI.WebControls.TextBox txtSubject;
		protected System.Web.UI.WebControls.TextBox txtMessage;
		protected System.Web.UI.WebControls.Button btnSendToFriend;
		protected System.Web.UI.WebControls.PlaceHolder miniProfile;
		protected System.Web.UI.WebControls.Literal instructionsLiteral;
		protected Matchnet.Web.Framework.JavaScript.FirstElementFocus fefControl;
		protected Matchnet.Web.Framework.Txt TxtReminder;

		private void Page_Load(object sender, System.EventArgs e)
		{
			try
			{
                if (g.Member != null)
                {
                    if (g.Session.GetInt(WebConstants.SESSION_PROPERTY_NAME_MEMBERID) > 0)
                    {
                        if (!Page.IsPostBack)
                        {
                            Matchnet.Member.ServiceAdapters.Member member = MemberSA.Instance.GetMember(g.Session.GetInt(WebConstants.SESSION_PROPERTY_NAME_MEMBERID), MemberLoadFlags.None);

                            this.txtYourName.Text = member.GetUserName(_g.Brand);
                            this.txtYourEmail.Text = member.EmailAddress;
                        }
                    }

                    if (!Page.IsPostBack)
                    {
                        this.txtSubject.Text = g.GetTargetResource("AN_INTERESTING_PAGE", this).Replace("%s2", g.Brand.Uri).Replace("%s1", this.txtYourName.Text);
                        this.txtMessage.Text = g.GetTargetResource("HEY_CHECK_OUT_THIS_LINK__518575", this).Replace("%s", g.Brand.Uri);
                        this.instructionsLiteral.Text = g.GetTargetResource("SEND_INSTRUCTIONS", this).Replace("%s", g.Brand.Uri);
                    }
                    this.fefControl.ElementClientId = this.txtFriendName.ClientID;
                }
                else
                {
                    g.Transfer("/Default.aspx");
                }
			}
			catch (Exception ex) { g.ProcessException(ex); }

            string reminder = g.GetResource(TxtReminder.ResourceConstant, this);
			if(String.IsNullOrEmpty(reminder) || reminder.ToLower().IndexOf("_resources") >= 0)
			{
				TxtReminder.Visible = false;
			}
		}

		private bool validateRequest()
		{
			bool isValidRequest = true;

			if (Request[this.txtFriendName.UniqueID] == "")
			{
				g.Notification.AddError("FRIENDS_NAME_IS_REQUIRED");
				isValidRequest = false;
			}
			else if (Request[this.txtFriendEmail.UniqueID] == "")
			{
				g.Notification.AddError("FRIENDS_EMAIL_IS_REQUIRED");
				isValidRequest = false;
			}
			else if (!this.isValidEmail(Request[this.txtFriendEmail.UniqueID]))
			{
				g.Notification.AddError("FRIENDS_EMAIL_IS_NOT_A_VALID_EMAIL_ADDRESS");
				isValidRequest = false;
			}
			else if (Request[this.txtYourName.UniqueID] == "")
			{
				g.Notification.AddError("YOUR_NAME_IS_REQUIRED");
				isValidRequest = false;
			}
			else if (Request[this.txtYourEmail.UniqueID] == "")
			{
				g.Notification.AddError("YOUR_EMAIL_ADDRESS_IS_REQUIRED");
				isValidRequest = false;
			}
			else if (!this.isValidEmail(Request[this.txtYourEmail.UniqueID]))
			{
				g.Notification.AddError("YOUR_EMAIL_ADDRESS_IS_NOT_A_VALID_EMAIL_ADDRESS");
				isValidRequest = false;
			}
			else if (Request[this.txtSubject.UniqueID] == "")
			{
				g.Notification.AddError("SUBJECT_IS_REQUIRED");
				isValidRequest = false;
			}
			else if (Request[this.txtMessage.UniqueID] == "")
			{
				g.Notification.AddError("MESSAGE_IS_REQUIRED");
				isValidRequest = false;
            }
            else if (txtSubject.Text.Length > 200)
            {
                g.Notification.AddErrorString(g.GetResource("STRING_OVER_MAX_LENGTH", this).Replace("%s", "subject"));
                isValidRequest = false;
            }
            else if (txtMessage.Text.Length > 200)
            {
                g.Notification.AddErrorString(g.GetResource("STRING_OVER_MAX_LENGTH", this).Replace("%s","message"));
                isValidRequest = false;
            }

			return isValidRequest;
		}
		
		private bool isValidEmail(string anEmailAddress)
		{
			bool isValidEmail = true;
			Regex emailPattern = new Regex("^\\w+((-\\w+)|(\\.\\w+))*\\@[A-Za-z0-9]+((\\.|-)[A-Za-z0-9]+)*\\.[A-Za-z0-9]+$");

			if (!emailPattern.IsMatch(anEmailAddress))
			{
				isValidEmail = false;
			}

			return isValidEmail;
		}

		private void btnSendToFriend_Click(object sender, System.EventArgs e)
		{
            Matchnet.Member.ServiceAdapters.Member member = MemberSA.Instance.GetMember(g.Session.GetInt(WebConstants.SESSION_PROPERTY_NAME_MEMBERID), MemberLoadFlags.None);
            int memberID = member.MemberID;
            string questionIdStr = (String.IsNullOrEmpty(Request["questid"]))?String.Empty:Request["questid"];
            string redirectURL = "/Applications/SendToFriend/MessageSent.aspx?qandaSTF=true&MemberID=" + memberID;
            if (!String.IsNullOrEmpty(questionIdStr))
            {
                redirectURL+="&questid="+questionIdStr;
            }

            //adding on additional URL params needed for tabbed profile pages to generate Next Profile link
            redirectURL += ProfileDisplayHelper.GetURLParamsNeededByNextProfile();

			try
			{

				bool isValidRequest = this.validateRequest();

				if (!isValidRequest)
				{
					return;
				}

				string friendName = txtFriendName.Text;
				string friendEmail = txtFriendEmail.Text;
                string yourName = txtYourName.Text;
				string yourEmail = txtYourEmail.Text;
				string subject = txtSubject.Text;
				string theMessage = txtMessage.Text;
                int questionId=(String.IsNullOrEmpty(questionIdStr))?-999:Int32.Parse(questionIdStr);
                Matchnet.PremiumServiceSearch.ValueObjects.QuestionAnswer.Question question = QuestionAnswerSA.Instance.GetQuestion(questionId);
                string truncatedQuestionText = (null==question)?String.Empty:SendToFriendHelper.TruncateAndPreserveWords(question.Text, 200);
				ExternalMailSA.Instance.SendMemberQuestion(friendName, friendEmail, yourEmail, yourName, memberID, subject, theMessage, g.Brand.BrandID, questionId, truncatedQuestionText);
            } 
			catch (Exception ex) { 
				redirectURL += "&Error=true";
				g.ProcessException(ex); 
			}

            try
            {
                //activity recording
                ActivityRecordingSA.Instance.RecordActivity(
                    memberID,
                    Constants.NULL_INT,
                    g.Brand.Site.SiteID,
                    ActivityRecording.ValueObjects.Types.ActionType.MailQuestion,
                    ActivityRecording.ValueObjects.Types.CallingSystem.QuestionAnswer,
                    string.Format("<QuestionID>{0}</QuestionID><RecipientEmail>{1}</RecipientEmail>", questionIdStr, txtFriendEmail.Text));
            }
            catch (Exception logEx)
            {
                string message = "ActivityRecording threw an exception: " + logEx.Message;
                System.Diagnostics.EventLog.WriteEntry(Matchnet.PremiumServiceSearch.ValueObjects.ServiceDefinitions.ServiceConstants.SERVICE_NAME_QUESTION, message, System.Diagnostics.EventLogEntryType.Error);
            }

            g.Transfer(redirectURL);
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.btnSendToFriend.Click += new System.EventHandler(this.btnSendToFriend_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

	}
}
