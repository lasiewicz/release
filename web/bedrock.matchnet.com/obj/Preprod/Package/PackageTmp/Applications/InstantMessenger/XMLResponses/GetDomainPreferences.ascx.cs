using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Web.Framework;

namespace Matchnet.Web.Applications.InstantMessenger.XMLResponses
{

	/// <summary>
	///		Summary description for GetDomainPreferences.
	///		This control handles the XML response for a getDomainPreferences call from the Userplane system.
	///		
	///		Parameters:
	///		- domainID
	///	
	/// </summary>
	public class GetDomainPreferences : BaseXMLResponse
	{

		private void Page_Load(object sender, System.EventArgs e)
		{
			startResponse();

			writeElement("characterLimit", g.GetResource("characterLimit", this));
			
			writeElement("forbiddenWordsList", string.Empty);
			writeElement("smileys", string.Empty);
			writeElement("maxVideoBandwidth", g.GetResource("maxVideoBandwidth", this));
            string domainLogoResx = g.GetResource("domainLogoLarge", this);
            writeElement("domainLogoLarge", Framework.Image.GetURLFromFilename(domainLogoResx));
			
			writeElement("line1", g.GetResource("line1", this));
			writeElement("line2", g.GetResource("line2", this));
			writeElement("line3", g.GetResource("line3", this));
			writeElement("line4", g.GetResource("line4", this));
			writeElement("avEnabled", g.GetResource("avEnabled", this));
			writeElement("gameButton", g.GetResource("gameButton", this));
			writeElement("conferenceCallEnabled", g.GetResource("conferenceCallEnabled", this));
			writeElement("clickableUserName", g.GetResource("clickableUserName", this));
			writeElement("printButton", g.GetResource("printButton", this));
			writeElement("buddyListButton", "false"); //bug 15800: don't want to view hotlist g.GetResource("buddyListButton", this)
			writeElement("preferencesButton", g.GetResource("preferencesButton", this));
			writeElement("smileyButton", g.GetResource("smileyButton", this));
			writeElement("addBuddyEnabled", g.GetResource("addBuddyEnabled", this));  
			writeElement("connectionTimeout", g.GetResource("connectionTimeoutMinutes", this)); // this governs how long you conversation window stays active, and should be a high number so you don't lose conversations when getting coffee (other IM clients like MSN, AIM, Yahoo etc. don't have a client timeout that I know of!) according to Userplane, there is no significant load on their servers to having open IM conversations.
			writeElement("sendTextToImages", g.GetResource("sendTextToImages", this));

			// Since jdate.com and jdate.co.il share the DomainID, all calls from Userplane
			// will look like jdate.com when examined from the g object.  Special tags we need for
			// jdate community and cupid.
            //07/21/09 SK - changed to convert to sitesettings
            bool allowConfCalls = SettingsManager.GetSettingBool(SettingConstants.USER_PLANE_ALLOW_PHONE_CALLS, g.Brand);
			if(allowConfCalls)
			{
				// getconfphonenumber call to us from Userplane is disabled by default.  We must write allowCalls tag to enable this.
				// Let's do this only for jdate community and cupid.
				writeElement("allowCalls", g.GetResource("allowCalls", this));

				// We are override the conference phone icon.  We have some sort of hack with Userplane that tricks the
				// Flash control to think what it's rendering is a print button.  By writing the URL of the image to
				// this "print" tag, we are replacing the icon image for the conference phone number button.
				Response.Write("<buttonIcons>");
                string phoneIconResx = g.GetResource("phoneIcon", this);
                writeElement("print", Framework.Image.GetURLFromFilename(phoneIconResx));
				Response.Write("</buttonIcons>");
			}

			Response.Write("<systemMessages>");
			writeElement("waiting", g.GetResource("displayWaitingMessage", this));
			writeElement("open", g.GetResource("displayOpenMessage", this));
			writeElement("closed", g.GetResource("displayClosedMessage", this));
			writeElement("blocked", g.GetResource("displayBlockedMessage", this));
			writeElement("away", g.GetResource("displayAwayMessage", this));
			writeElement("nonDeliveryTimeout", g.GetResource("nonDeliveryTimeout", this));

			writeElement("conferenceCallInvitation", g.GetResource("conferenceCallInvitation", this));
			writeElement("conferenceCallReminder", g.GetResource("conferenceCallReminder", this));
			writeElement("conferenceCallRetrievingNumber", g.GetResource("conferenceCallRetrievingNumber", this));

			//writeElement("nonDeliveryTimeout", "60");//60 seconds
			//Response.Write("<nonDeliveryMessage sendOnClose='false' promptUser='true'>[[DISPLAYNAME]] seems not to be responding. We can deliver these messages by email so they can get it later. Would you like that?</nonDeliveryMessage>");
			//writeElement("nonDeliveryMessage", "[DISPLAYNAME]] seems not to be responding. We can deliver these messages by email so they can get it later. Would you like that?");
			//writeElement("nonDeliverySendOnClose", "false");//these don't work but they should! Nate!!! App inspector uses these!
			//writeElement("nonDeliveryPromptUser", "true");//these don't work but they should! Nate!!! App inspector uses these!
			//Response.Write("<nonDeliveryMessage timeout='30' sendOnClose='false' promptUser='true'></nonDeliveryMessage>" );
			//in attempt to fix 16298 - commenting out nonDeliveryMessage in hopes that default will come from locale file that Userplane has (and thus will show Hebrew)
			//writeElement("nonDeliveryMessage", g.GetResource("nonDeliveryMessage", this));

			Response.Write("</systemMessages>");

			endResponse();
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
