using System;
using System.Web;
using System.Data;
using System.Web.UI.WebControls;

using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Managers;
using Matchnet.Web.Framework.Ui;
using Matchnet.Web.Framework.Ui.BasicElements;
using Matchnet.Lib;
using Matchnet.Lib.Util;

using Matchnet.List.ValueObjects;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects.Photos;
using Matchnet.InstantMessenger.ServiceAdapters;
using Matchnet.InstantMessenger.ValueObjects;
using Matchnet.MembersOnline.ServiceAdapters;
using Matchnet.Content.ServiceAdapters;
using System.Collections;
using Matchnet.Content.ServiceAdapters.Links;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Games.ServiceAdapters;
using Matchnet.Games.ValueObjects;
using Matchnet.Web.Interfaces;

namespace Matchnet.Web.Applications.InstantMessenger
{

    /// <summary>
    ///		Return snippets of HTML (DHTML alerts) and javascript array of incoming IM invitations.
    ///		This is placed inside an IFRAME in the Left.ascx control.
    ///		When it loads, it calls a javascript in it's parent window and passes it the HTML in a javascript array of alert objects.
    /// </summary>
    public class CheckIM : FrameworkControl
    {
        protected Repeater repIMAlerts;
        protected PlaceHolder phHebScript;
        protected PlaceHolder plcIMSound;
        protected PlaceHolder phHebScript2;
        protected PlaceHolder phAjax;
        protected PlaceHolder phHTMLBottom;
        protected PlaceHolder phHTMLTop;
        protected Literal litIMAlertsArray;
        protected Repeater repeaterAjaxIMAlerts;

        //Define constants for columns used for IM notifications DataTable
        protected const string ALERTID = "alertId";
        protected const string ONLINEIMAGESRC = "onlineImageSRC";
        protected const string ONLINEIMAGETITLE = "onlineImageTitle";
        protected const string ONLINEIMAGEALT = "onlineImageALT";
        protected const string USERNAMELINK = "userNameLink";
        protected const string USERNAME = "userName";
        protected const string PROFILELINK = "profileLink";
        protected const string PROFILETHUMBSRC = "profileThumbSRC";
        protected const string PROFILETHUMBTITLE = "profileThumbTitle";
        protected const string PROFILETHUMBALT = "profileThumbALT";
        protected const string ALERTMESSAGE = "alertMessage";
        protected const string TXTACCEPT = "TXTAccept";
        protected const string YESLINK = "yesLink";
        protected const string YESTEXT = "yesText";
        protected const string NOLINK = "noLink";
        protected const string NOTEXT = "noText";
        protected const string IMGMUTUALYESSRC = "imgMutualYesSRC";
        protected const string IMGMUTUALYESTITLE = "imgMutualYesTitle";
        protected const string IMGMUTUALYESALT = "imgMutualYesALT";
        protected const string UPGRADETEXT = "upgradeText";
        protected const string UPGRADELINK = "upgradeLink";
        protected const string AGETEXT = "ageText";
        protected const string LOCATIONTEXT = "locationText";
        protected const string GENDERTEXT = "genderText";
        protected const string CLOSELINK = "closeLink";
        protected const string CLOSELINKTEXT = "closeLinkText";
        protected const string ISIM = "isIM";
        public const string Subscribe = "/Applications/Subscription/Subscribe.aspx";
        protected bool hasUnReadConversationInvitations = false;//determines whether or not to play a sound - we only want the sound to play once per
        protected bool isAjaxMode = false;
        protected ArrayList IMAlertIDList = new ArrayList();
        protected bool isRoadblocked = false;
        protected bool played = false;

        private void Page_Load(object sender, System.EventArgs e)
        {
            g.SaveSession = false;
            //defeat caching for good measure
            Response.Cache.SetCacheability(HttpCacheability.NoCache);

            //set ajax mode
            if (Request.QueryString[WebConstants.URL_PARAMETER_NAME_AJAX] != null && Request.QueryString[WebConstants.URL_PARAMETER_NAME_AJAX].ToLower() == "true")
            {
                isAjaxMode = true;
            }

            //show Instant Messenger invitations and add them to javascript array to pass to parent window
            try
            {
                GetCurrentInvitations();
            }
            catch (Exception ex)
            {
                _g.ProcessException(new ApplicationException("Error trying to check for IM invitations", ex));
            }
            //Hebrew needs different constants to set direction of movement of DHTML notifier, so
            //include different javascript variables if this is a Hebrew site.
            if (g.Brand.Site.LanguageID == (int)Matchnet.Language.Hebrew)
            {
                phHebScript.Visible = true;
                phHebScript2.Visible = true;
            }

        }

        protected override void OnPreRender(EventArgs e)
        {
            if (isAjaxMode)
            {
                phAjax.Visible = true;
                phHTMLBottom.Visible = false;
                phHTMLTop.Visible = false;

                //generate JS Script to load IM Alert Array
                if (this.IMAlertIDList.Count > 0)
                {
                    this.repeaterAjaxIMAlerts.DataSource = this.IMAlertIDList;
                    this.repeaterAjaxIMAlerts.DataBind();
                }

                this.plcIMSound.Visible = hasUnReadConversationInvitations;
            }

            base.OnPreRender(e);
        }


        /// <summary>
        /// Gets list of current Instant Messenger invitations, adds them to an in Memory DataTable, then binds
        /// that table to a Repeater. The Repeater contains javascript that adds each invitation to an array
        /// that it passes to the parent window for handling.
        /// </summary>
        private void GetCurrentInvitations()
        {
            if (g.Member == null)
            {
                //if users session is expired, do nothing
                return;
            }

            DataTable dtIMAlerts = new DataTable();

            dtIMAlerts.Columns.Add(ALERTID);
            dtIMAlerts.Columns.Add(ONLINEIMAGESRC);
            dtIMAlerts.Columns.Add(ONLINEIMAGETITLE);
            dtIMAlerts.Columns.Add(ONLINEIMAGEALT);
            dtIMAlerts.Columns.Add(USERNAMELINK);
            dtIMAlerts.Columns.Add(USERNAME);
            dtIMAlerts.Columns.Add(PROFILELINK);
            dtIMAlerts.Columns.Add(PROFILETHUMBSRC);
            dtIMAlerts.Columns.Add(PROFILETHUMBTITLE);
            dtIMAlerts.Columns.Add(PROFILETHUMBALT);
            dtIMAlerts.Columns.Add(ALERTMESSAGE);
            dtIMAlerts.Columns.Add(TXTACCEPT);
            dtIMAlerts.Columns.Add(YESLINK);
            dtIMAlerts.Columns.Add(YESTEXT);
            dtIMAlerts.Columns.Add(NOLINK);
            dtIMAlerts.Columns.Add(NOTEXT);
            dtIMAlerts.Columns.Add(IMGMUTUALYESSRC);
            dtIMAlerts.Columns.Add(IMGMUTUALYESTITLE);
            dtIMAlerts.Columns.Add(IMGMUTUALYESALT);
            dtIMAlerts.Columns.Add(UPGRADETEXT);
            dtIMAlerts.Columns.Add(UPGRADELINK);
            dtIMAlerts.Columns.Add(AGETEXT);
            dtIMAlerts.Columns.Add(LOCATIONTEXT);
            dtIMAlerts.Columns.Add(GENDERTEXT);
            dtIMAlerts.Columns.Add(CLOSELINK);
            dtIMAlerts.Columns.Add(CLOSELINKTEXT);
            dtIMAlerts.Columns.Add(ISIM);

            //get a list of current Instant Messenger invitations
            ConversationInvites invites = InstantMessengerSA.Instance.GetInvites(
                g.Member.MemberID
                , g.Brand.Site.Community.CommunityID);

            if ((invites != null) && (invites.Count > 0))
            {
                foreach (ConversationInvite invite in invites)
                {
                    //check if this invite has never been rendered before
                    if (invite.IsRead == false)
                    {
                        //mark invite as being Read
                        invite.IsRead = true;
                        InstantMessengerSA.Instance.Save(invite);
                        //record if there are any new unread invites
                        if (hasUnReadConversationInvitations == false)
                        {
                            hasUnReadConversationInvitations = true;
                        }
                    }

                    int senderMemberID = invite.Message.Sender.MemberID;
                    

                    // dont allow a popup to display for a member who is currently being ignored
                    if (!g.List.IsHotListed(HotListCategory.IgnoreList, g.Brand.Site.Community.CommunityID, senderMemberID))
                    {
                        DataRow row = dtIMAlerts.NewRow();

                        row[ISIM] = "True";

                        row[ALERTID] = "alertId_" + senderMemberID.ToString();
                        this.IMAlertIDList.Add("alertId_" + senderMemberID.ToString()); //keep track of alertID for ajax mode

                        row[ONLINEIMAGESRC] = Framework.Image.GetURLFromFilename("icon_chat.gif");
                        row[ONLINEIMAGETITLE] = g.GetResource("ONLINE", this);
                        row[ONLINEIMAGEALT] = g.GetResource("ONLINE", this);

                        //Get Sending Member Username and ThumbImage from Cache
                        Matchnet.Member.ServiceAdapters.Member sender = MemberSA.Instance.GetMember(senderMemberID
                            , MemberLoadFlags.None);

                        string senderMemberName = sender.GetUserName(_g.Brand);
                        string senderAge = FrameworkGlobals.GetAge(sender, g.Brand).ToString();
                        string senderGender = FrameworkGlobals.GetGenderString(sender.GetAttributeInt(g.Brand, "GenderMask"));
                        string senderLocation = FrameworkGlobals.GetRegionString(sender.GetAttributeInt(g.Brand, "RegionID"), g.Brand.Site.LanguageID);

                        // Set the UI to render in the appropriate mode
                        isRoadblocked = !IMHelper.CanMemberReplyIM(g.Member, sender, g.Brand,g);

                        //pop-up profile in new window - not in same window - esp. if in chat window 16758 
                        //IMAlertColumns[4] = "/Applications/MemberProfile/ViewProfile.aspx?"
                        //	+ "MemberID=" + senderMemberID.ToString();
                        row[USERNAMELINK] = BreadCrumbHelper.MakeViewProfileLink(BreadCrumbHelper.EntryPoint.NoArgs, senderMemberID);
                        row[USERNAME] = senderMemberName;
                        row[PROFILELINK] = row[USERNAMELINK];
                        row[PROFILETHUMBSRC] = getMemberPhotoURL(sender);
                        row[PROFILETHUMBTITLE] = g.GetResource("TTL_VIEW_MY_PROFILE", this);
                        row[PROFILETHUMBALT] = row[PROFILETHUMBTITLE];
                        row[ALERTMESSAGE] = g.GetResource("DO_YOU_WANT_TO_CHAT", this);
                        row[TXTACCEPT] = g.GetResource("ACCEPT", this);
                        row[YESTEXT] = g.GetResource("ALT_YES", this);
                        row[NOTEXT] = g.GetResource("ALT_NO", this);
                        row[AGETEXT] = senderAge;
                        row[GENDERTEXT] = senderGender;
                        row[LOCATIONTEXT] = senderLocation;
                        row[UPGRADELINK] = Subscribe + "?prtid=" + ((int)Content.ServiceAdapters.Links.PurchaseReasonType.ImUpgradeToChatLink).ToString() + "&smid=" + senderMemberID;
                        row[UPGRADETEXT] = g.GetResource("UPGRADE_TO_CHAT", this);
                        row[CLOSELINKTEXT] = g.GetResource("CLOSE_LINK_TEXT", this);
                        // TODO: Either remove or make it setting base
                        if (Conversion.CBool(System.Configuration.ConfigurationSettings.AppSettings.Get("DebugTrace")))
                        {
                            System.Diagnostics.Trace.WriteLine(string.Format("ConnectIMEnabled: {0}  IMID: {1} SenderMemberName: {2} SenderMemberID: {3} VO_Version: {4}",
                                g.IsConnectIMEnabled.ToString(), invite.IMID.ToString(), senderMemberName, senderMemberID.ToString(), invite.Version.ToString()));
                        }
                        // Yes, No, Close Links
                        if (g.IsConnectIMEnabled && invite.IMID != 0)
                        {
                            //function ConnectIMYes(strIMID, strSendingUsername, strDestinationMemberId, conversationInvitationKey, strSpwscypher) {
                            //    function ConnectIMNo(strIMID, strDestinationMemberId, conversationInvitationKey, strSpwscypher) {
                            //        function ConnectIMClose(strIMID, strDestinationMemberId, conversationInvitationKey, strSpwscypher) {
                            row[YESLINK] = string.Format("javascript:ConnectIMYes('{0}', '{1}', '{2}', '{3}', '{4}');", invite.IMID, senderMemberName, senderMemberID, invite.ConversationKey.GUID, g.GetEncryptedSessionID(true));
                            row[NOLINK] = string.Format("javascript:ConnectIMNo('{0}', '{1}', '{2}', '{3}');", invite.IMID, senderMemberID, invite.ConversationKey.GUID, g.GetEncryptedSessionID(true));
                            row[CLOSELINK] = string.Format("javascript:ConnectIMClose('{0}', '{1}', '{2}', '{3}');", invite.IMID, senderMemberID, invite.ConversationKey.GUID, g.GetEncryptedSessionID(true));
                        }
                        else if (FrameworkGlobals.GetChatProvider(g.Brand) == Spark.Common.Chat.ChatProvider.Ejabberd)
                        {
                            var conversationTokenId = string.Empty;
                            var conversationToken = InstantMessengerSA.Instance.ReadConversationToken(senderMemberID,
                                                g.Member.MemberID,
                                                g.Brand.Site
                                                 .Community
                                                 .CommunityID);
                            if ((conversationToken != null) && !string.IsNullOrEmpty(conversationToken.ConversationTokenId))
                            {
                                conversationTokenId = conversationToken.ConversationTokenId;
                                g.LoggingManager.LogInfoMessage("CheckIM", string.Format("ConversationToken found. Id:{0}", conversationTokenId));
                            } 
                            row[YESLINK] = string.Format("javascript:EjabIMYes('{0}', '{1}', '{2}', '{3}');", senderMemberID, invite.ConversationKey.GUID, RuntimeSettings.GetSetting("WEBCHAT_URL",
                                                                      g.Brand.Site.Community.CommunityID,
                                                                      g.Brand.Site.SiteID, g.Brand.BrandID), conversationTokenId);
                            row[NOLINK] = string.Format("javascript:EjabIMNo('{0}', '{1}', '{2}');", senderMemberID, invite.ConversationKey.GUID, conversationTokenId);
                            row[CLOSELINK] = string.Format("javascript:EjabIMClose('{0}', '{1}', '{2}');", senderMemberID, invite.ConversationKey.GUID, conversationTokenId);

                        }
                        else
                        {
                            row[YESLINK] = string.Format("javascript:IMYes('{0}', '{1}');", senderMemberID, invite.ConversationKey.GUID);
                            row[NOLINK] = string.Format("javascript:IMNo('{0}', '{1}');", senderMemberID, invite.ConversationKey.GUID);
                            row[CLOSELINK] = string.Format("javascript:IMClose('{0}', '{1}');", senderMemberID, invite.ConversationKey.GUID);
                        }
                        // the mutual yes icon.
                        // get click mask
                        ClickMask clickMask = g.List.GetClickMask(g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, senderMemberID);
                        // check for match
                        if ((clickMask & ClickMask.MemberYes) == ClickMask.MemberYes
                            && (clickMask & ClickMask.TargetMemberYes) == ClickMask.TargetMemberYes)
                        {
                            row[IMGMUTUALYESSRC] = Framework.Image.GetURLFromFilename("icon_click_YY.gif"); //"icon_click_YY.gif";
                            row[IMGMUTUALYESTITLE] = "Mutual Yes";
                            row[IMGMUTUALYESALT] = "Mutual Yes";
                        }
                        else // need to make these blank so the javascript knows not to try to display the icon
                        {
                            row[IMGMUTUALYESSRC] = "";
                            row[IMGMUTUALYESTITLE] = "";
                            row[IMGMUTUALYESALT] = "";
                        }

                        dtIMAlerts.Rows.Add(row);
                    }
                }
            }

            if (dtIMAlerts.Rows.Count > 0)
            {
                //play sound once if user has at least one new IM invitation (i.e., one that is IsRead = false)
                this.plcIMSound.Visible = hasUnReadConversationInvitations;

                //Bind the DataTable to the Repeater in the aspx code in-front
                this.repIMAlerts.DataSource = dtIMAlerts;
                this.repIMAlerts.DataBind();
            }
        }

        // Returns the URL for the correct Member photo (or stock photo).
        private string getMemberPhotoURL(Matchnet.Member.ServiceAdapters.Member member)
        {
            string url = string.Empty;
            try
            {
                //MemberPhotoHelper photoHelper = new MemberPhotoHelper(RuntimeSettings.Instance);
                Photo photo = MemberPhotoDisplayManager.Instance.GetDefaultPhoto(g.Member, member, g.Brand);
                url = MemberPhotoDisplayManager.Instance.GetPhotoDisplayURL(g.Member, member, g.Brand, photo,
                                                           PhotoType.Thumbnail,
                                                           PrivatePhotoImageType.TinyThumb,
                                                           NoPhotoImageType.BackgroundTinyThumb);
            }
            catch (Exception ex)
            {
                g.ProcessException(new ApplicationException("Error getting photo for member " + member.MemberID, ex));
            }

            return url;
        }

        protected void repIMAlerts_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.FindControl("imgIcon") != null)
            {
                System.Web.UI.HtmlControls.HtmlImage imgIcon =
                    e.Item.FindControl("imgIcon") as System.Web.UI.HtmlControls.HtmlImage;
                imgIcon.Attributes.Add("Title", ((DataRowView)e.Item.DataItem)[ONLINEIMAGETITLE].ToString());
                imgIcon.Alt = ((DataRowView)e.Item.DataItem)[ONLINEIMAGEALT].ToString();
                imgIcon.Src = ((DataRowView)e.Item.DataItem)[ONLINEIMAGESRC].ToString();
                if (((DataRowView)e.Item.DataItem)[ISIM].ToString() == "True")
                {
                    imgIcon.Height = 19;
                    imgIcon.Width = 29;
                }
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
            this.repIMAlerts.ItemDataBound += new RepeaterItemEventHandler(repIMAlerts_ItemDataBound);
        }
        #endregion
    }
}
