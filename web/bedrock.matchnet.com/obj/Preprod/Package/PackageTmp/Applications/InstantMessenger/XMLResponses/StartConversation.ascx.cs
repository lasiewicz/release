using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using Matchnet.InstantMessenger.ServiceAdapters;
using Matchnet.InstantMessenger.ValueObjects;
using Matchnet.List.ServiceAdapters;
using Matchnet.List.ValueObjects;
using Matchnet.Web.Framework;
using Matchnet.UserNotifications.ServiceAdapters;
using Matchnet.UserNotifications.ValueObjects;
using Matchnet.Web.Applications.UserNotifications;

namespace Matchnet.Web.Applications.InstantMessenger.XMLResponses
{

	/// <summary>
	///		Summary description for StartConversation.
	///		
	///		Handles the XML Response for startConversation calls from
	///		the Userplane IM system.
	///		
	///		Parameters:
	///		- domainID
	///		- memberID
	///		- targetMemberID
	///		
	/// </summary>
	public class StartConversation : BaseXMLResponse
	{
        private const int NonPermitMember = 0;
        private const int PermitMember = 1; // 1 bit
        private const int SubscribedMember = 1023; //10 bits 111111111, subscribe member should have privilege to everything
		private void Page_Load(object sender, System.EventArgs e)
		{
			try
			{
				base.startResponse();

                ConversationKey key;
                // TODO: Either remove or make it setting base                
                if (Conversion.CBool(System.Configuration.ConfigurationSettings.AppSettings.Get("DebugTrace")))
                {
                    System.Diagnostics.Trace.WriteLine(string.Format("StartConversation MemberID: {0} TargetMemberID: {1} CommunityID: {2} SiteID: {3} IMID: {4}",
                        MemberID.ToString(), TargetMemberID.ToString(), g.Brand.Site.Community.CommunityID.ToString(), g.Brand.Site.SiteID.ToString(), IMID.ToString()));
                }

                bool startconversation = true;
                if (g.Brand.BrandID != (int)WebConstants.BRAND_ID.JDateCoIL)
                {
                    try
                    {
                        Matchnet.Member.ServiceAdapters.Member member = Matchnet.Member.ServiceAdapters.MemberSA.Instance.GetMember(MemberID, Matchnet.Member.ServiceAdapters.MemberLoadFlags.None);
                        startconversation = IsPayingMember(member, g);
                        if (!startconversation)
                            return;
                    }
                    catch (Exception ex)
                    { }
                }
                if (IMID == Constants.NULL_INT)
                {

                    //New code to pass TransactionID to the Conversation object. Will pass Constants.NULL_STRING
                    //if not transactionID is passed.
                    //In Service layer, it will check if a transactionID is passed and if server settings should allow use
                    //of transaction IDs (signifying that we have tested transactionID functionality and are ready
                    //to make it go live...)
                    key = InstantMessengerSA.Instance.InitiateConversation(MemberID
                        , TargetMemberID
                        , g.Brand.Site.Community.CommunityID
                        , TransactionID);
                }
                else
                {
                    key = InstantMessengerSA.Instance.InitiateConversation(MemberID
                        , TargetMemberID
                        , g.Brand.Site.Community.CommunityID
                        , IMID);
                }

				if (key != null)
				{	// Update the MembersYouIMed Hot List for this conversation.
					ListSA.Instance.AddListMember(HotListCategory.MembersYouIMed,
						g.Brand.Site.Community.CommunityID,
						g.Brand.Site.SiteID,
						MemberID,
						TargetMemberID,
						string.Empty,
						Constants.NULL_INT,
						true,
						false);

                    if (UserNotificationFactory.IsUserNotificationsEnabled(g))
                    {
                        UserNotificationParams unParams = UserNotificationParams.GetParamsObject(TargetMemberID, g.Brand.Site.SiteID, g.Brand.Site.Community.CommunityID);
                        UserNotification notification = UserNotificationFactory.Instance.GetUserNotification(new IMedYouUserNotification(), TargetMemberID, MemberID, g);
                        if (notification.IsActivated())
                        {
                            UserNotificationsServiceSA.Instance.AddUserNotification(unParams, notification.CreateViewObject());
                        }
                    }
				}
			}
			catch (Exception ex)
			{
				g.ProcessException(ex);
			}
			finally
			{
				base.endResponse();
			}		
		}


        public bool IsPayingMember(Member.ServiceAdapters.Member member, ContextGlobal g)
        {
            int memberPrivilege = NonPermitMember;



            if (member != null)
            {
                //check if a member currently has a subscription 
                if (member.IsPayingMember(g.Brand.Site.SiteID))
                {
                    memberPrivilege = memberPrivilege | SubscribedMember;
                }
            }

            if (PermitMember == (PermitMember & memberPrivilege))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
