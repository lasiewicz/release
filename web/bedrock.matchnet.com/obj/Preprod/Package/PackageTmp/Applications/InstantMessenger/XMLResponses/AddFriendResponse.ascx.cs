using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using Matchnet.List.ServiceAdapters;
using Matchnet.List.ValueObjects;
using Matchnet.UserNotifications.ServiceAdapters;
using Matchnet.UserNotifications.ValueObjects;
using Matchnet.Web.Applications.UserNotifications;

namespace Matchnet.Web.Applications.InstantMessenger.XMLResponses
{

	/// <summary>
	///		AddFriendResponse is the XML response handler for AddFriend calls from the Userplane system.
	///		
	///		Expected parameters:
	///		- domainID
	///		- memberID
	///		- targetMemberID
	///		
	/// </summary>
	public class AddFriendResponse : BaseXMLResponse
	{

		private void Page_Load(object sender, System.EventArgs e)
		{
			base.startResponse();

			try
			{
				// The AddFriend call translates to a Favorites Hot List entry.
				if (MemberID > 0 && TargetMemberID > 0)
				{
					ListSA.Instance.AddListMember(HotListCategory.Default,
						g.Brand.Site.Community.CommunityID,
						Constants.NULL_INT,
						MemberID,
						TargetMemberID,
						null,
						Constants.NULL_INT,
						true,
						true);

                    if (UserNotificationFactory.IsUserNotificationsEnabled(g))
                    {
                        UserNotificationParams unParams = UserNotificationParams.GetParamsObject(TargetMemberID, g.Brand.Site.SiteID, g.Brand.Site.Community.CommunityID);
                        UserNotification notification = UserNotificationFactory.Instance.GetUserNotification(new AddedYouAsFavoriteUserNotification(), TargetMemberID, MemberID, g);
                        if (notification.IsActivated())
                        {
                            UserNotificationsServiceSA.Instance.AddUserNotification(unParams, notification.CreateViewObject());
                        }
                    }
				}
			}
			finally
			{
				base.endResponse();
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
