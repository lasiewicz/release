using System;
using System.Data;
using System.Drawing;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Threading;
using System.Globalization;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.InstantMessenger.ServiceAdapters;
using Matchnet.InstantMessenger.ValueObjects;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects.Photos;
using Matchnet.Web.Framework;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Web.Framework.Managers;

namespace Matchnet.Web.Applications.InstantMessenger.XMLResponses
{

	/// <summary>
	///		Summary description for StartIC.
	///		
	///		The control handles the XML Response for Userplane calls (action=startIC)
	///		This action will be called every time an InstantCommunicator window
	///		(upInstantCommunicator.aspx) is opened. 
	///		Parameters:
	///		- domainID
	///		- memberID
	///		- targetMemberID
	///		
	/// </summary>
	public class StartIC : BaseXMLResponse
	{

		private void Page_Load(object sender, System.EventArgs e)
		{
			try
			{
				//16402 Make sure resources are in Hebrew if this is a JDIL member - base brand on currently viewing Member (not TargetMember)
				// (reason: startIC is called each time a IM window is opened (i.e., twice: once for initiator and once for receiver). 
				// When receiving member initiates startIC, the order of memberA and memberB is reversed. We want to always show 
				// English to an english member and Hebrew to a Hebrew member. So base brand off Member, not TargetMember
				SetBrandIfJDIL(Member);

				base.startResponse();

				// Need to validate that we are working with a valid member

				if (Member != null)
				{
					// Write the Member node.
					Response.Write("<member>");
					writeElement("displayName", Member.GetUserName(base.g.Brand), true);
					writeElement("imagePath", GetPhotoURL(Member));
					writeElement("avEnabled", "true");
					writeElement("gameButton", "true");
					writeElement("conferenceCallEnabled", "true");
					writeElement("kissSmackEnabled", "false");
					writeElement("showErrors", "");
					writeElement("sound", "true");

					// Focus will draw the users attention to the IC window when messages arrive.
					writeElement("focus", "true"); //focusing on Flash causes problem for Hebrew DHTML IM version - so we handle that case in javascript focus command 15855 and 15801
					writeElement("autoOpenAV", "false");
					writeElement("autoStartAudio", "false");
					writeElement("autoStartVideo", "false");
					Response.Write("</member>");
				}

				if (TargetMember != null)
				{
					// Write the TargetMember node.
					Response.Write("<targetMember>");
					writeElement("displayName", TargetMember.GetUserName(base.g.Brand), true);
					writeElement("line1", getAgeGenderDisplay(_TargetMember), true);
					writeElement("line2", getRegionDisplay(_TargetMember), true);
					writeElement("line3", getHeadline(_TargetMember), true);
					writeElement("line4", "", true);
					writeElement("imagePath", GetPhotoURL(TargetMember));
					writeElement("avEnabled", "true");
					writeElement("gameButton", "true");
					writeElement("conferenceCallEnabled", "true");
					writeElement("block", "false");
					Response.Write("</targetMember>");
				}

				if (Member != null && TargetMember != null)
				{
					// Join/Acknowledge any pending IM Conversation Invites for this pair of members.
					InstantMessengerSA.Instance.JoinConversation(Member.MemberID
						, TargetMember.MemberID
						, g.Brand.Site.Community.CommunityID);
				}
			}
			catch(Exception ex){
				g.ProcessException(ex);
				Response.Write(ex.ToString());
			}
			finally
			{
				base.endResponse();
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion

		private string getAgeGenderDisplay(Matchnet.Member.ServiceAdapters.Member pMember)
		{
			StringBuilder display = new StringBuilder(30);

			// Age
			DateTime birthDate = pMember.GetAttributeDate(g.Brand, "BirthDate",DateTime.MinValue);
			display.Append(FrameworkGlobals.GetAgeCaption(birthDate,g,this,"AGE_UNSPECIFIED"));

			display.Append(", ");

			display.Append(FrameworkGlobals.GetGenderMaskString(pMember.GetAttributeInt(g.Brand, "GenderMask")));
			return display.ToString();
		}


		private string getHeadline(Matchnet.Member.ServiceAdapters.Member pMember)
		{
			return FrameworkGlobals.Ellipsis(pMember.GetAttributeTextApproved(g.Brand, "AboutMe", g.GetResource("FREETEXT_NOT_APPROVED", null)), 20);//GetAttributeText(g.Brand, "AboutMe"), 50);
		}

		private string getRegionDisplay(Matchnet.Member.ServiceAdapters.Member pMember)
		{
			int regionID = pMember.GetAttributeInt(g.Brand, "RegionID");
			if (regionID != Constants.NULL_INT)
			{
				return FrameworkGlobals.Ellipsis(FrameworkGlobals.GetRegionString(regionID, g.Brand.Site.LanguageID), 100);
			}
			return string.Empty;
		}


		/// <summary>
		/// Returns full absolute URL of the thumbnail photo for given member, or else returns the noPhoto.jpg url.
		/// </summary>
		/// <param name="member"></param>
		/// <returns></returns>
		private string GetPhotoURL(Matchnet.Member.ServiceAdapters.Member member)
		{
			string url = string.Empty;
			try 
			{
                //MemberPhotoHelper photoHelper = new MemberPhotoHelper(RuntimeSettings.Instance);
                Photo photo = MemberPhotoDisplayManager.Instance.GetDefaultPhoto(member, member, g.Brand);
                url = MemberPhotoDisplayManager.Instance.GetPhotoDisplayURL(member, member, g.Brand, photo, PhotoType.Full, PrivatePhotoImageType.Thumb, NoPhotoImageType.Thumb);
                
                string subDomainPrefix = base.g.Brand.Site.DefaultHost;
                if (HttpContext.Current.Request.Url.Host.ToLower().IndexOf("stage.") > -1)
                {
                    subDomainPrefix = "stage";
                }

                if (!url.StartsWith("http://"))
                {
                    url = "http://" + subDomainPrefix + "." + base.g.Brand.Uri + url;
                }
			}
			catch (Exception ex) 
			{
				g.ProcessException(new ApplicationException("Error getting photo for member " + this._MemberID, ex));
			}
			return url;
		}

	}
}
