<%@ Control Language="c#" AutoEventWireup="false" Codebehind="CheckIM.ascx.cs" Inherits="Matchnet.Web.Applications.InstantMessenger.CheckIM" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<asp:PlaceHolder ID="phHTMLTop" runat="server" Visible="true">
<html>
  <head>
    <meta http-equiv="Expires" content="Tue, 01 Dec 1990 06:30:00 GMT">
    <meta http-equiv="Pragma" content="no-cache">
    <script type="text/javascript">
    <!--
		var xIMAlerts = null; // defined below - an array of custom objects that contain the HTML of the Instant Messenger invitation requests.

		function _onload()
		{
						
			if (xIMAlerts) 
			{ 
				//pass the array of alerts to the parent function in main window
				parent.dg_IMFromChild(xIMAlerts); 
				//alert("Error trying to call parent.dg_IMFromChild\(xIMAlerts\) because parent window isn't available");
			}
			else 
			{
				//there are no alerts, remove any that might still be on screen
				parent.dg_dhtmlNotifier_removeOldIMAlerts();
				//alert("Error trying to call parent.dg_dhtmlNotifier_removeOldIMAlerts\(\) because parent window isn't available");
			
			}
		}

			<asp:PlaceHolder id="phHebScript" runat="server" visible="false">
			if (document.all)
			{
				parent.dg_dhtmlNotifier_CONST_VISIBLE_LEFT 
					= -parent.dg_dhtmlNotifier_CONST_ALERT_WIDTH+15;
				parent.dg_dhtmlNotifier_CONST_INVISIBLE_LEFT
					= 0;
				parent.dg_dhtmlNotifier_CONST_MINOR_STEP_PIXELS = -1;
				parent.dg_dhtmlNotifier_CONST_MAJOR_STEP_PIXELS = -6;
			}
			else
			{
				parent.dg_dhtmlNotifier_CONST_VISIBLE_LEFT 
					= parent.window.innerWidth - parent.dg_dhtmlNotifier_CONST_ALERT_WIDTH - 5;
				parent.dg_dhtmlNotifier_CONST_INVISIBLE_LEFT
					= parent.window.innerWidth;
				parent.dg_dhtmlNotifier_CONST_MINOR_STEP_PIXELS = -1;
				parent.dg_dhtmlNotifier_CONST_MAJOR_STEP_PIXELS = -6;
			}
			</asp:PlaceHolder>

		//-->
    </script>
	    
  </head>
  <body bgcolor="white"  onload="_onload()">
    <%= DateTime.Now%>
</asp:PlaceHolder> 
<div id='xIMAlertTemplate'>
		<asp:Repeater ID="repIMAlerts" Runat="server" OnItemDataBound="repIMAlerts_ItemDataBound">
			<ItemTemplate>
<div id='<%# DataBinder.Eval(Container.DataItem, "alertId") %>' class="IMAlert">
            
		    <object width="1" height="1" id="Object1"
	            classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"
	            codebase="https://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0" VIEWASTEXT>
	            <param name="movie" value="/Applications/InstantMessenger/sounds/IM_sound.swf" />
	            <param name="quality" value="high" />
	            <param name="bgcolor" value="#FFFFFF" />
	            <embed src="/Applications/InstantMessenger/sounds/IM_sound.swf"
		            name="IM_sound" width="1" height="1"
		            quality="high" bgcolor="#FFFFFF"
		            type="application/x-shockwave-flash"
		            pluginspage="http://www.macromedia.com/go/getflashplayer">
	            </embed>
            </object>
       
	<table class="IMAlertHeader" cellspacing="0" cellpadding="0" border="0" width="100%">
	<tbody><tr>
		<td width="29">
			    <img runat="server" id="imgIcon" class="onlineGif" align="middle" /> 
		</td>
		<td width="124">
		<span class="ImTitle"><%# DataBinder.Eval(Container.DataItem, "alertMessage")%></span>
		</td>
		<% if (isRoadblocked)
     { %>
		<td width="68">
		    <a href="<%# DataBinder.Eval(Container.DataItem, "closeLink") %>"><strong><small><%# DataBinder.Eval(Container.DataItem, "closeLinkText")%></small></strong></a>
		</td>
		<%} %>
	</tr>
	</tbody>
	</table>
	<table class="AlertTable" cellSpacing="1" cellPadding="2" width="100%" border="0">
	<tbody><tr>
		<td valign="top" align="left" class="alertTableProfileImage">
			<a href="<%# DataBinder.Eval(Container.DataItem, "profileLink") %>">
				<img class=profileImageHover width=43 border=0 height=56 
					src="<%# DataBinder.Eval(Container.DataItem, "profileThumbSRC") %>" 
					title="<%# DataBinder.Eval(Container.DataItem, "profileThumbTitle") %>"
					alt="<%# DataBinder.Eval(Container.DataItem, "profileThumbALT") %>" />
			</a> 
		</td>
		<td align="left" class="alertTableProfileInfo">
			<img style="display: none" class="IM-mutual-yes"
				title="<%# DataBinder.Eval(Container.DataItem, "imgMutualYesTitle") %>" 
				alt="<%# DataBinder.Eval(Container.DataItem, "imgMutualYesALT") %>" 
				src="<%# DataBinder.Eval(Container.DataItem, "imgMutualYesSRC") %>" />
			<a href="<%# DataBinder.Eval(Container.DataItem, "userNameLink") %>">
				<strong class="alertTabProfileUsername"><%# DataBinder.Eval(Container.DataItem, "userName")%></strong>
			</a>
			<br />
			<%# DataBinder.Eval(Container.DataItem, "ageText")%> 
			<%# DataBinder.Eval(Container.DataItem, "genderText")%>
			<br />
			<%# DataBinder.Eval(Container.DataItem, "locationText")%>
			<br />
			<% if (!isRoadblocked)
      { %>
			<p style="margin: .4em 0;"><strong class="alertTabProfileApprove"><%# DataBinder.Eval(Container.DataItem, "TXTAccept")%></strong></p>
			<div class="IM-cta">
			<a href="<%# DataBinder.Eval(Container.DataItem, "yesLink") %>" class="activityButton link-primary ImButton">
				<span><%# DataBinder.Eval(Container.DataItem, "yesText")%></span>
			</a>
			&nbsp;
			<a href="<%# DataBinder.Eval(Container.DataItem, "noLink") %>" class="activityButton link-primary ImButton">
				<span><%# DataBinder.Eval(Container.DataItem, "noText")%></span>
			</a></div>
			<% }
      else
      {	     %>
            <div class="IM-cta">
            <a href="<%# DataBinder.Eval(Container.DataItem, "upgradeLink") %>" class="activityButton link-primary">
				<span><%# DataBinder.Eval(Container.DataItem, "upgradeText")%></span>
			</a>
			</div>
      <%} %>
		</td>
	</tr></tbody>
	</table>
</div>
    <script type="text/javascript">
    <!--
		if (!xIMAlerts) xIMAlerts = new Array();
		var xna = new Object();
		xna.visible = false;
		xna.alertId = '<%# DataBinder.Eval(Container.DataItem, "alertId") %>';
		var domNode = document.getElementById(xna.alertId);
		if (domNode)
		{
			xna.html = "<div class='IMAlert' id='" + xna.alertId + "'>" + domNode.innerHTML + "</div>";
		}
		else
		{
			xna.html = "<div id='" + xna.alertId + "'>Alert: " + xna.alertId + "</div>";
		}
		xIMAlerts.push(xna);
	//-->
    </script>
			</ItemTemplate>
		</asp:Repeater>
</div>
<asp:PlaceHolder Runat="server" ID="plcIMSound" Visible="False">
	<object width="1" height="1" id="IM_sound"
		classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"
		codebase="https://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0" VIEWASTEXT>
		<param name="movie" value="/Applications/InstantMessenger/sounds/IM_sound.swf" />
		<param name="quality" value="high" />
		<param name="bgcolor" value="#FFFFFF" />
		<embed src="/Applications/InstantMessenger/sounds/IM_sound.swf"
			name="IM_sound" width="1" height="1"
			quality="high" bgcolor="#FFFFFF"
			type="application/x-shockwave-flash"
			pluginspage="http://www.macromedia.com/go/getflashplayer">
		</embed>
	</object>
</asp:PlaceHolder>

<asp:PlaceHolder ID="phHTMLBottom" runat="server" Visible="true">
</body>
</html>

</asp:PlaceHolder>
<asp:PlaceHolder runat="server" ID="phAjax" Visible="false">
    <!--this div contains JS to be evaluated from an ajax call-->
    <div id="divAjScript">
        <asp:PlaceHolder ID="phHebScript2" runat="server" Visible="false">if (document.all)
            { dg_dhtmlNotifier_CONST_VISIBLE_LEFT = -dg_dhtmlNotifier_CONST_ALERT_WIDTH+15;
            dg_dhtmlNotifier_CONST_INVISIBLE_LEFT= 0; dg_dhtmlNotifier_CONST_MINOR_STEP_PIXELS
            = -1; dg_dhtmlNotifier_CONST_MAJOR_STEP_PIXELS = -6; } else { dg_dhtmlNotifier_CONST_VISIBLE_LEFT
            = window.innerWidth - dg_dhtmlNotifier_CONST_ALERT_WIDTH - 5; dg_dhtmlNotifier_CONST_INVISIBLE_LEFT=
            window.innerWidth; dg_dhtmlNotifier_CONST_MINOR_STEP_PIXELS = -1; dg_dhtmlNotifier_CONST_MAJOR_STEP_PIXELS
            = -6; } </asp:PlaceHolder>
        <asp:Repeater ID="repeaterAjaxIMAlerts" runat="server">
            <ItemTemplate>
                if (!xIMAlerts) xIMAlerts = new Array(); var xna = new Object(); xna.visible = false;
                xna.alertId = '<%# Container.DataItem.ToString()%>'; xIMAlerts.push(xna);
            </ItemTemplate>
        </asp:Repeater>
    </div>
</asp:PlaceHolder>
