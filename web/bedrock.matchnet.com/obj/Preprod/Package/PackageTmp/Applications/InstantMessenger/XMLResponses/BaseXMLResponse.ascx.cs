using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects;
using Matchnet.Member.ValueObjects.Photos;
using Matchnet.Web.Framework;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Content.ServiceAdapters;


namespace Matchnet.Web.Applications.InstantMessenger.XMLResponses
{

	/// <summary>
	///		Summary description for BaseXMLResponse.
	/// </summary>
	public class BaseXMLResponse : FrameworkControl
	{
		private const string XML_ELEMENT		= "<{0}>{1}</{0}>";
		private const string XML_CDATA_ELEMENT	= "<{0}><![CDATA[{1}]]></{0}>";
        protected int _IMID = Constants.NULL_INT;
        protected int _MemberID = Constants.NULL_INT;
		protected int _TargetMemberID = Constants.NULL_INT;
		protected Matchnet.Member.ServiceAdapters.Member _Member;
		protected Matchnet.Member.ServiceAdapters.Member _TargetMember;
		protected const string PARAM_MEMBERID = "memberID";
		protected const string PARAM_TARGETMEMBERID = "targetMemberID";
		protected const string PARAM_TRANSACTIONID = "callID";//new feature: Userplane passing transaction ID with requests.
        protected const string PARAM_IMID = "imid";
		protected string _transactionID = Constants.NULL_STRING;

		private void Page_Load(object sender, System.EventArgs e)
		{
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion

		protected void startResponse()
		{
			Response.Write("<?xml version='1.0' encoding='iso-8859-1'?>");
			Response.Write("<icappserverxml>");
		}

		protected void endResponse()
		{
			Response.Write("</icappserverxml>");
		}

		protected void writeElement(string pElementName, string pElementValue)
		{
			writeElement(pElementName, pElementValue, false);
		}

		protected void writeElement(string pElementName, string pElementValue, bool pUseCDATA)
		{
			if (pUseCDATA)
			{
				Response.Write(string.Format(XML_CDATA_ELEMENT, pElementName, pElementValue));
			}
			else
			{
				Response.Write(string.Format(XML_ELEMENT, pElementName, pElementValue));
			}
		}

		protected string InviteKey
		{
			get
			{
				return Request["key"];
			}
		}

		protected Matchnet.Member.ServiceAdapters.Member Member
		{
			get
			{
				if (_Member == null)
				{
					if (MemberID != Constants.NULL_INT)
					{
						_Member = MemberSA.Instance.GetMember(MemberID, MemberLoadFlags.None);
					}
				}
				return _Member;
			}
		}

        /// <summary>
        /// IM ID passed by Provo.  Userplane does not make use of this.
        /// </summary>
        protected int IMID
        {
            get
            {
                if (_IMID == Constants.NULL_INT)
                {
                    if (Request.QueryString[PARAM_IMID] != null)
                    {
                        if (!int.TryParse(Request.QueryString[PARAM_IMID], out _IMID))
                        {
                            Exception ex = new Exception("Parameter: " + PARAM_IMID + " not an integer. ");
                            g.ProcessException(ex);
                            Response.Write(ex.ToString()); //write to the output so that userplane will get invalid XML response
                        }
                    }                    
                }
                return _IMID;
            }
        }

		/// <summary>
		/// Required parameter. Returns the member id of user "A" in the Instant Message XML request.
		/// MemberID is  used in several XML response types. User "A" can either be the intiator or 
		/// the receiver, depending on the context.
		/// </summary>
		protected int MemberID
		{
			get
			{
				if (_MemberID == Constants.NULL_INT)
				{
					if(Request.QueryString[PARAM_MEMBERID] != null)
					{
						try
						{
							_MemberID = Convert.ToInt32(Request.QueryString[PARAM_MEMBERID]); 
						}
						catch (System.FormatException ex1)
						{
							Exception ex = new Exception("Parameter: " + PARAM_MEMBERID + " not an integer. " , ex1);
							g.ProcessException(ex);
							Response.Write(ex.ToString()); //write to the output so that userplane will get invalid XML response
						}
					}
					else
					{
						Exception ex = new Exception("Required parameter: " + PARAM_MEMBERID + " not found in querystring.");
						g.ProcessException(ex);
						Response.Write(ex.ToString()); //write to the output so that userplane will get invalid XML response
					}
				}
				return _MemberID;
			}
		}

		protected Matchnet.Member.ServiceAdapters.Member TargetMember
		{
			get
			{
				if (_TargetMember == null)
				{
					if (TargetMemberID != Constants.NULL_INT)
					{
						_TargetMember = MemberSA.Instance.GetMember(TargetMemberID, MemberLoadFlags.None);
					}
				}

				return _TargetMember;
			}
		}

		/// <summary>
		/// Required parameter. Returns the member id of user "B" in the Instant Message XML request.
		/// TargetMemberID is  used in several XML response types. User "B" can either be the intiator or 
		/// the receiver, depending on the context.
		/// </summary>
		protected int TargetMemberID
		{
			get
			{
				if (_TargetMemberID == Constants.NULL_INT)
				{
					if (Request.QueryString[PARAM_TARGETMEMBERID] != null) 
					{
						try 
						{
							_TargetMemberID = Convert.ToInt32(Request["targetMemberID"]);
						}
						catch (System.FormatException ex1) 
						{
							Exception ex = new Exception("Parameter: " + PARAM_TARGETMEMBERID + " not an integer." , ex1);
							g.ProcessException(ex);
							Response.Write(ex.ToString()); //write to the output so that userplane will get invalid XML response
						}
					}
					else 
					{
						Exception ex = new Exception("Required parameter: " + PARAM_TARGETMEMBERID + " not found in querystring. ");
						g.ProcessException(ex);
						Response.Write(ex.ToString()); //write to the output so that userplane will get invalid XML response
					}
				}
				return _TargetMemberID;
			}
		}

		/// <summary>
		/// This parameter is sent by Userplane in order to uniquely identify each XML request 
		/// and avoid processing of duplicate transactions. It is based on current time
		/// "callID=" + escape(Number( new Date() ) + "_" + this.iID) So it is a numeric format for the date 
		/// (milliseconds), then an underscore, then a number that is unique to that call in that instance.
		/// http://yourdomain.com/ICXML.aspx?action=getDomainPreferences&domainID=yourDomain.com&callID=1130450215254_342
		/// </summary>
		protected string TransactionID 
		{
			get 
			{
				if (this._transactionID == Constants.NULL_STRING) 
				{
				
					//TODO: when this feature goes live, it will be a required parameter: uncomment these lines
					//if (Request.QueryString[PARAM_TRANSACTIONID] != null && Request.QueryString[PARAM_TRANSACTIONID] != string.Empty) 
					//{
						this._transactionID = Request.QueryString[PARAM_TRANSACTIONID];
					//}
					//else 
					//{
					//	Exception ex = new Exception("Required parameter: " + PARAM_TRANSACTIONID + " not found in querystring. ");
					//	g.ProcessException(ex);
					//	Response.Write(ex.ToString()); //write to the output so that userplane will get invalid XML response
					//}
				}
				return this._transactionID;
			}

		}

		/// <summary>
		/// Returns Members last logged in brand (important to determine whether to show JD or JDIL images in IM window)
		/// </summary>
		public Brand GetLastBrandJDate (Matchnet.Member.ServiceAdapters.Member member)
		{
			// need LastBrandID because BrandInsertDate and BrandLastLogonDate are at the Brand scope.
			int lastBrandID = -1;
			//	member.GetAttributeInt(Constants.NULL_INT, siteID, Constants.NULL_INT, "LastBrandID", Constants.NULL_INT );
			//(int)WebConstants.SITE_ID.JDate;
			try 
			{
				member.GetLastLogonDate((int)WebConstants.COMMUNITY_ID.JDate, out lastBrandID);
			}
			catch (Exception ex)
			{
				//null object reference sometimes thrown here... Perhaps after new service release
				g.ProcessException(new ApplicationException("Error trying to get the last logged in brand of user" + member.MemberID,ex));
			}

			Brand lastBrand;
			if (lastBrandID > 0) 
			{
				lastBrand = BrandConfigSA.Instance.GetBrandByID(lastBrandID);
			}
			else 
			{
				lastBrand = BrandConfigSA.Instance.GetBrandByID((int)WebConstants.BRAND_ID.JDate);
			}
			return lastBrand;
		}

		/// <summary>
		/// Set the current Brand to Jdate.co.il if last logged in site for given Member was JDIL.
		/// This is important for bugs: 16402, 16392, 16298, 15972, 14572.
		/// We must determine if JDIL Hebrew resources should be served not relying on URL 
		/// (jdate.com is called by userplane for both - to support JD to JDIL communication)
		/// </summary>
		/// <param name="member">Either the Member or the Target Member. Choose carefully depending
		/// on context, which member should determine the branding!</param>
		protected void SetBrandIfJDIL(Matchnet.Member.ServiceAdapters.Member member) 
		{
			Brand brand = g.Brand; //default
			if ((int)WebConstants.COMMUNITY_ID.JDate == g.Brand.Site.Community.CommunityID) 
			{
				//get the last logged in brand for member (use TargetMember when sending email
				//to them, use Member when showing thumbnails etc.)
				brand = GetLastBrandJDate(member);
				//change brand to JDIL if last logged in brand of member was JDIL
                if (brand.BrandID == (int)WebConstants.BRAND_ID.JDateCoIL) 
                {			
                    //HACK alert! Change current brand to JDIL
                    g.Brand = brand;
                }
                else if (brand.BrandID == (int) WebConstants.BRAND_ID.JDateFR)
                {
                    g.Brand = brand;
                }
			}
		}
	}
}
