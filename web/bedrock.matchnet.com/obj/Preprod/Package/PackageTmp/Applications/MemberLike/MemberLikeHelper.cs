﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.Member.ValueObjects.Interfaces;
using Matchnet.Web.Framework;
using Matchnet.MemberLike.ValueObjects;
using Matchnet.MemberLike.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.PremiumServiceSearch.ServiceAdapters;
using Matchnet.PremiumServiceSearch.ValueObjects.QuestionAnswer;
using Matchnet.UserNotifications.ServiceAdapters;
using Matchnet.UserNotifications.ValueObjects;
using Matchnet.Web.Applications.UserNotifications;

namespace Matchnet.Web.Applications.MemberLike
{
    public class MemberLikeHelper
    {
        public static readonly MemberLikeHelper Instance = new MemberLikeHelper();

        private MemberLikeHelper() { }

        public bool IsMemberLikeEnabled(Brand brand)
        {
            string isEnabled = "false";
            try
            {
                isEnabled = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ENABLE_MEMBERLIKE", brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID);
            }
            catch (Exception ex)
            {
                //setting missing
                isEnabled = "false";
            }

            if (isEnabled.ToLower().Trim() == "true")
                return true;
            else
                return false;
        }

        public bool IsFreeTextMemberLikeEnabled(Brand brand)
        {
            string isEnabled = "false";
            try
            {
                isEnabled = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ENABLE_FREETEXT_MEMBERLIKE", brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID);
            }
            catch (Exception ex)
            {
                //setting missing
                isEnabled = "false";
            }

            if (isEnabled.ToLower().Trim() == "true")
                return true;
            else
                return false;
        }

        public bool IsMemberPhotoLikeEnabled(Brand brand)
        {
            string isEnabled = "false";
            try
            {
                isEnabled = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ENABLE_FREETEXT_MEMBERLIKE", brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID);
            }
            catch (Exception ex)
            {
                //setting missing
                isEnabled = "false";
            }

            if (isEnabled.ToLower().Trim() == "true")
                return true;
            else
                return false;
        }

        private static void LogException(Exception ex, string functionName)
        {
            System.Diagnostics.EventLog.WriteEntry("WWW", functionName + " error: " + ex.Message, System.Diagnostics.EventLogEntryType.Error);
        }

        public bool IsMemberSuppressed(IMemberDTO member, ContextGlobal g)
        {
            bool suppress = false;
            try
            {
                int globalStatusMask = member.GetAttributeInt(g.Brand, WebConstants.ATTRIBUTE_NAME_GLOBALSTATUSMASK);
                bool adminSuspend = Matchnet.Conversion.CInt(globalStatusMask & Matchnet.Lib.ConstantsTemp.ADMIN_SUSPEND) == Matchnet.Lib.ConstantsTemp.ADMIN_SUSPEND;
                bool selfSuspend = (member.GetAttributeInt(g.TargetBrand, "SelfSuspendedFlag", 0) == 1);
                suppress = adminSuspend || selfSuspend;
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
                suppress = false;
            }
            return suppress;
        }

        public int GetCountOfLikesByObjectIdandType(MemberLikeParams likeParams, int likeObjectId, int likeObjectTypeId)
        {
            try
            {
                return MemberLikeServiceSA.Instance.GetCountOfLikeListByObjectIdAndType(likeParams, likeObjectId, likeObjectTypeId);
            }
            catch (Exception ex)
            {
                LogException(ex, "MemberLikeHelper.GetCountOfLikesByObjectIdandType()");
                return 0;
            }
        }

        public List<MemberLikeObject> GetLikesByObjectIdAndType(MemberLikeParams likeParams, int likeObjectId, int likeObjectTypeId)
        {
            try
            {
                return MemberLikeServiceSA.Instance.GetLikeListByObjectIdAndType(likeParams, likeObjectId, likeObjectTypeId).TypeLikes;
            }
            catch (Exception ex)
            {
                LogException(ex, "MemberLikeHelper.GetLikesByObjectIdAndType()");
                return null;
            }
        }

        public List<MemberLikeObject> GetLikesByMember(MemberLikeParams likeParams)
        {
            try
            {
                return MemberLikeServiceSA.Instance.GetLikeListByMember(likeParams).MemberLikes;
            }
            catch (Exception ex)
            {
                LogException(ex, "MemberLikeHelper.GetLikesByMember()");
                return null;
            }
        }
        
        public List<MemberLikeObject> GetLikesByMemberAndType(MemberLikeParams likeParams, int likeObjectTypeId)
        {
            try
            {
                return MemberLikeServiceSA.Instance.GetLikeListByMemberAndType(likeParams, likeObjectTypeId).MemberLikes;
            }
            catch (Exception ex)
            {
                LogException(ex, "MemberLikeHelper.GetLikesByMemberAndType()");
                return null;
            }
        }
       
        
        public bool HasMemberLike(List<MemberLikeObject> memberLikes, Predicate<MemberLikeObject> predicate)
        {
            if (null == memberLikes) return false;
            MemberLikeObject match = memberLikes.Find(predicate);
            return null != match;
        }

        public void ProcessUserNotification(MemberLikeParams likeParams, int likeObjectId, int likeObjectTypeId, ContextGlobal g, Hashtable additionalParams)
        {
            try
            {
                if (!UserNotificationFactory.IsUserNotificationsEnabled(g)) return;
                UserNotificationParams unParams = null;
                UserNotification userNotification = null;
                int questionId = 0;
                int answerId = 0;
                Question question = null;
                int answerMemberId = 0;

                switch (likeObjectTypeId)
                {
                    case ((int)LikeObjectTypes.Answer):
                        questionId = (int)additionalParams["questionId"];
                        answerId = (int)additionalParams["answerId"];
                        question = QuestionAnswerSA.Instance.GetQuestion(questionId);
                        answerMemberId = Constants.NULL_INT;
                        foreach(Answer answer in question.Answers) {
                            if (answer.AnswerID == answerId)
                            {
                                answerMemberId = answer.MemberID;
                            }
                        }
                        additionalParams["answerMemberId"] = answerMemberId;
                        userNotification = UserNotificationFactory.Instance.GetUserNotification(new LikedAnswerUserNotification(), answerMemberId, likeParams.MemberId, g, additionalParams);
                        unParams = UserNotificationParams.GetParamsObject(answerMemberId, g.Brand.Site.SiteID, g.Brand.Site.Community.CommunityID);
                        break;

                    case ((int)LikeObjectTypes.ProfileFreeText):
                        questionId = (int)additionalParams["questionId"];
                        answerId = (int)additionalParams["objectId"];
                        question = QuestionAnswerSA.Instance.GetQuestion(questionId);
                        answerMemberId = (int)additionalParams["profileOwnerId"];
                        string sectionAttribute = (string)additionalParams["sectionAttribute"];
                        //foreach (Answer answer in question.Answers)
                        //{
                        //    if (answer.AnswerID == answerId)
                        //    {
                        //        answerMemberId = answer.MemberID;
                        //    }
                        //}
                        additionalParams["answerMemberId"] = answerMemberId;
                        userNotification = UserNotificationFactory.Instance.GetUserNotification(new LikedFreeTextUserNotification().SectionAttribute(sectionAttribute), answerMemberId, likeParams.MemberId, g, additionalParams);
                        unParams = UserNotificationParams.GetParamsObject(answerMemberId, g.Brand.Site.SiteID, g.Brand.Site.Community.CommunityID);
                        break;

                    case ((int)LikeObjectTypes.Photo):
                        questionId = (int)additionalParams["questionId"];
                        answerId = (int)additionalParams["objectId"];
                        question = QuestionAnswerSA.Instance.GetQuestion(questionId);
                        answerMemberId = (int)additionalParams["profileOwnerId"];

                        additionalParams["answerMemberId"] = answerMemberId;
                        userNotification = UserNotificationFactory.Instance.GetUserNotification(new LikedPhotoUserNotification(), answerMemberId, likeParams.MemberId, g, additionalParams);
                        unParams = UserNotificationParams.GetParamsObject(answerMemberId, g.Brand.Site.SiteID, g.Brand.Site.Community.CommunityID);
                        break;
                }

                if (null != userNotification && null != unParams)
                {
                    UserNotificationsServiceSA.Instance.AddUserNotification(unParams, userNotification.CreateViewObject());
                }                
            }
            catch (Exception e)
            {
                g.ProcessException(e);
            }
        }

        public PayLoad ProcessMemberLike(MemberLikeParams likeParams, int likeObjectId, int likeObjectTypeId, Brand brand)
        {
            PayLoad payLoad = new PayLoad();
            if (IsMemberLikeEnabled(brand))
            {
                try
                {
                    List<MemberLikeObject> memberLikes = MemberLikeServiceSA.Instance.GetLikeListByMemberAndType(likeParams, likeObjectTypeId).MemberLikes;
                    MemberLikeObject match = memberLikes.Find(delegate(MemberLikeObject obj)
                    {
                        bool b = (obj.SiteId == brand.Site.SiteID &&
                            obj.MemberId == likeParams.MemberId &&
                            obj.LikeObjectId == likeObjectId &&
                            obj.LikeObjectTypeId == likeObjectTypeId);
                        return b;
                    });
                    if (null == match)
                    {
                        payLoad.Operation = "add";
                        payLoad.TotalLikes = MemberLikeServiceSA.Instance.AddMemberLike(likeParams, likeObjectId, likeObjectTypeId);
                        
                    }
                    else
                    {
                        payLoad.Operation = "del";
                        payLoad.TotalLikes = MemberLikeServiceSA.Instance.RemoveMemberLike(likeParams, likeObjectId, likeObjectTypeId);
                    }
                }
                catch (Exception ex)
                {
                    LogException(ex, "MemberLikeHelper.ProcessMemberLike()");
                }
            }
            return payLoad;
        }
    }

    [Serializable]
    public class PayLoad
    {
        public int TotalLikes { get; set; }
        public string Operation { get; set; }
        public PayLoad() { }
    }

}
