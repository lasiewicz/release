﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MemberLikePopup.ascx.cs" Inherits="Matchnet.Web.Applications.MemberLike.MemberLikePopup" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="uc1" TagName="NoPhoto" Src="~/Framework/Ui/PageElements/NoPhoto20.ascx" %>
<%@ Register TagPrefix="uc2" Src="~/Framework/Ui/BasicElements/LastTime.ascx" TagName="LastTime" %>
<form id="Main" method="post" runat="server">
<%--<div class="like-answer-sort">
<label>Sort:
    <select id="miniAnscwerSort" name="miniAnswefrSort">
	    <option selected="selected" value="date|false">Date: newest first</option>
	    <option value="date|true">Date: oldest first</option>
	    <option value="gender|true">Gender: men first</option>
	    <option value="gender|false">Gender: women first</option>
    </select>
</label>
</div>--%>
<div class="ui-pagination"><asp:label id="lblListNavigationTop" Runat="server" /></div>
<div id="like-answer-container" class="results">
     <asp:Repeater ID="repeaterMemberLikes" runat="server" OnItemDataBound="repeaterMemberLikes_ItemDataBound">
        <ItemTemplate>
<div class="like-answer-profile clearfix">
    <div class="member-pic picture">
	            <mn:Image runat="server" ID="imgThumb" Width="54" Height="70" class="profileImageHover" Border="0" ResourceConstant="ALT_PROFILE_PICTURE" TitleResourceConstant="TTL_VIEW_MY_PROFILE" />
                <uc1:NoPhoto runat="server" ID="noPhoto" class="no-photo" Visible="False" />            
    </div>
    <div class="member-info">
        <p><asp:HyperLink ID="lnkUserName1" runat="server" /><br />
        <span class="member-details">
        <asp:Literal ID="literalAge" runat="server"></asp:Literal>,
        <asp:Literal ID="literalSeeking" runat="server"></asp:Literal>,<br />
        <asp:Literal ID="literalLocation" runat="server"></asp:Literal> </span> 
        <span class="timestamp"><uc2:LastTime ID="lastTimeAction" runat="server" /></span></p>
    </div>
    <div class="action-items">    
        <a id="lnkEmailMe" Runat="server" class="email link-primary"><mn:txt id="mntxt6958" runat="server" resourceconstant="TXT_EMAIL_MEMBER_BUTTON" expandimagetokens="true" /></a>			            
        <a id="lnkFlirtMe" Runat="server" class="tease link-primary"><mn:txt id="mntxt6959" runat="server" resourceconstant="TXT_FREE_FLIRT_BUTTON" expandimagetokens="true" /></a><br />			            
        <%--favorites--%>    
        <a href="javascript:void(0);" onclick='spark.list.ProcessFavorite(this,<asp:Literal ID="favoriteMemberId" runat="server"/>, {Event:"event23",PageName:"Q&A overlay",Overwrite:true,Props:[{Num:10,Value:"<%=CurrentUrl %>"}]});'><asp:Literal ID="favoriteText" runat="server" /></a>
    </div>
</div>
        </ItemTemplate>    
    </asp:Repeater>
</div><%--end #like-answer-container--%>
<div class="ui-pagination" style="display:none;"><asp:label id="lblListNavigationBottom" Runat="server" /></div>
</form>
<script type="text/javascript">
    __addToNamespace__('spark.list', {
        txtRemove: '<%=GetResourceForJS("TXT_REMOVE")%>',
        txtAdd: '<%=GetResourceForJS("TXT_ADD")%>',
        ProcessFavorite: function(element, favoriteMemberId, omnitureParams) {
            $j(element).attr("disabled", "disabled");
            $j.ajax({
                type: "POST",
                url: "/Applications/API/Favorites.asmx/ProcessFavoriteOperation",
                data: "{favoriteMemberID:" + favoriteMemberId + "}",
                contentType: "application/json; charset=ISO-8859-1",
                dataType: "json",
                success: function(msg, status) {
                    if (msg && msg.d) {
                        var listTxt = (msg.d == 'add') ? spark.list.txtRemove : spark.list.txtAdd;
                        var listProp = (msg.d == 'add') ? "Add-" : "Remove-";
                        $j(element).text(listTxt);
                        if (omnitureParams) {
                            var overwrite = omnitureParams.Overwrite;
                            if (omnitureParams.Event) {
                                spark.tracking.addEvent(omnitureParams.Event, overwrite);
                            }
                            if (omnitureParams.PageName) {
                                spark.tracking.addPageName(omnitureParams.PageName, true);
                            }
                            $j.each(omnitureParams.Props, function(i, prop) {
                                var propVal = (prop.Num != 10) ? listProp + prop.Value : prop.Value;
                                spark.tracking.addProp(prop.Num, propVal, overwrite);
                            });
                            spark.tracking.track();
                        }
                    } else {
                    }
                    $j(element).removeAttr("disabled");
                },
                error: function(msg, status) {
                    $j(element).removeAttr("disabled");
                }
            });
        },
        __init__: function() {
        }
    });

    <% if(!string.IsNullOrEmpty(ElementId)) { %>
    $j(document).ready(function() {
        $j("#<%=lblListNavigationBottom.ClientID %>, #<%=lblListNavigationTop.ClientID %>").click(function(evt) {
            var $target = $j(evt.target);
            var href = $target.attr("href");
            var paginationResponses=$j('#<%=ElementId %>').data("paginationResponses");
            if(href && href != '') {
                if(paginationResponses && paginationResponses[href]) {
                    if($j('#<%=ElementId %>').html() != paginationResponses[href]) {
                        $j('#<%=ElementId %>').html(paginationResponses[href]);
                    }
                } else {
                    $j.get(href, function(response) {
                        paginationResponses[href] = response;
                        $j('#<%=ElementId %>').html(response);
                        $j('#<%=ElementId %>').data("paginationResponses",paginationResponses);
                    });
                }
            }
            evt.preventDefault();
        });
    });
    <% } %>
</script>
