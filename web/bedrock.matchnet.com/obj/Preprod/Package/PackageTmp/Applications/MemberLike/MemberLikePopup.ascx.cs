﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Web.Framework;
using Matchnet.MemberLike.ValueObjects;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Web.Framework.Managers;
using Matchnet.Web.Framework.Ui;
using Matchnet.Web.Framework.Ui.PageElements;
using Matchnet.Web.Framework.Ui.BasicElements;
using Matchnet.Web.Applications.MemberProfile;

namespace Matchnet.Web.Applications.MemberLike
{
    public partial class MemberLikePopup : FrameworkControl
    {
        private List<MemberLikeObject> memberLikes = null;
        private int likeObjectId = Constants.NULL_INT;
        private int likeObjectTypeId = Constants.NULL_INT;
        private int _StartRow = 0;
        private int _ChapterSize = 3;
        private int _TotalMemberLikes = 0;
        private string _currentUrl = string.Empty;
        private static readonly string MEMBER_LIKE_POPUP_URL_BASE = "/Applications/MemberLike/MemberLikePopup.aspx?loid={0}&lotid={1}&";

        public BreadCrumbHelper.EntryPoint MyEntryPoint { get; set; }
        public string TrackingParam { get; set; }
        public string CurrentUrl
        {
            get
            {
                return _currentUrl;
            }
        }

        public int PageSize
        {
            get
            {
                int _PageSize = 8;

                int settingPageSize = SettingsManager.GetSettingInt(SettingConstants.MEMBER_LIKE_POPUP_PAGE_SIZE, g.Brand);
                if (settingPageSize != Constants.NULL_INT)
                {
                    _PageSize = settingPageSize;
                }
                
                return _PageSize;
            }
        }

        public string ElementId { get; set; }

        public string GetResourceForJS(string resConst)
        {
            return FrameworkGlobals.JavaScriptEncode(g.GetResource(resConst, new string[0], true, this));
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (MemberLikeHelper.Instance.IsMemberLikeEnabled(g.Brand))
            {
                if (!string.IsNullOrEmpty(Request["loid"]))
                {
                    likeObjectId = Convert.ToInt32(Request["loid"]);
                }
                if (!string.IsNullOrEmpty(Request["lotid"]))
                {
                    likeObjectTypeId = Convert.ToInt32(Request["lotid"]);
                }
                if (!string.IsNullOrEmpty(Request["eid"]))
                {
                    ElementId = Request["eid"];
                }

                if (!string.IsNullOrEmpty(Request[WebConstants.URL_PARAMETER_NAME_ENTRYPOINT]))
                {
                    try
                    {
                        MyEntryPoint = (BreadCrumbHelper.EntryPoint)Enum.Parse(typeof(BreadCrumbHelper.EntryPoint), Request[WebConstants.URL_PARAMETER_NAME_ENTRYPOINT]);
                    }
                    catch (Exception ignore){}
                }

                if (Constants.NULL_INT != likeObjectId && Constants.NULL_INT != likeObjectTypeId)
                {
                    memberLikes = MemberLikeHelper.Instance.GetLikesByObjectIdAndType(MemberLikeParams.GetParamsObject(g.Member.MemberID, g.Brand.Site.SiteID), likeObjectId, likeObjectTypeId);
                    if (null != memberLikes)
                    {
                        memberLikes = memberLikes.FindAll(delegate(MemberLikeObject likeObject)
                        {
                            Matchnet.Member.ServiceAdapters.Member _Member = MemberSA.Instance.GetMember(likeObject.MemberId, MemberLoadFlags.None);
                            return !MemberLikeHelper.Instance.IsMemberSuppressed(_Member, g);
                        });
                    }
                    _TotalMemberLikes = memberLikes.Count;
                    //Get StartRow (if user clicked on paging)
                    if (!String.IsNullOrEmpty(Request.QueryString["StartRow"]))
                    {
                        int.TryParse(Request.QueryString["StartRow"], out _StartRow);
                        if (_StartRow < 0)
                            _StartRow = 0;
                    }

                    // if the start row is greater than the totalrows, set the startrow to the nearest round page_size less than totalrows
                    int _PageSize = PageSize;
                    if (_StartRow > _TotalMemberLikes)
                    {
                        _StartRow = (_TotalMemberLikes / _PageSize) * _PageSize + 1;
                    }

                    if (memberLikes.Count > _PageSize)
                    {
                        
                        //load paging
                        PagingHelper.LoadPageNavigation(lblListNavigationTop, lblListNavigationBottom, string.Format(MEMBER_LIKE_POPUP_URL_BASE,likeObjectId,likeObjectTypeId), memberLikes.Count, _ChapterSize, _PageSize, _StartRow, g.GetResource("TXT_NEXT"), g.GetResource("TXT_PREVIOUS"));
                    }

                }

                if (null != memberLikes && memberLikes.Count > 0)
                {
                    _currentUrl = Request.Url.AbsoluteUri; 
                    repeaterMemberLikes.DataSource = memberLikes.Skip(_StartRow - 1).Take(PageSize);
                    repeaterMemberLikes.DataBind();
                }
            }
        }

        protected void repeaterMemberLikes_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                MemberLikeObject likeObject = e.Item.DataItem as MemberLikeObject;
                int ordinal = e.Item.ItemIndex;
                Matchnet.Member.ServiceAdapters.Member _Member = MemberSA.Instance.GetMember(likeObject.MemberId, MemberLoadFlags.None);
                if (!MemberLikeHelper.Instance.IsMemberSuppressed(_Member, g))
                {
                    Matchnet.Web.Framework.Image imgThumb = e.Item.FindControl("imgThumb") as Matchnet.Web.Framework.Image;
                    Matchnet.Web.Framework.Image ImageEmailMe = e.Item.FindControl("ImageEmailMe") as Matchnet.Web.Framework.Image;
                    NoPhoto20 noPhoto = e.Item.FindControl("noPhoto") as NoPhoto20;
                    HyperLink lnkUserName1 = e.Item.FindControl("lnkUserName1") as HyperLink;
                    Literal literalSeeking = e.Item.FindControl("literalSeeking") as Literal;
                    Literal literalAge = e.Item.FindControl("literalAge") as Literal;
                    Literal literalLocation = e.Item.FindControl("literalLocation") as Literal;
                    HtmlAnchor lnkEmailMe = e.Item.FindControl("lnkEmailMe") as HtmlAnchor;
                    Txt imgEmailMe = e.Item.FindControl("mntxt6958") as Txt;                    
                    HtmlAnchor lnkFlirtMe = e.Item.FindControl("lnkFlirtMe") as HtmlAnchor;
                    Txt imgFlirtMe = e.Item.FindControl("mntxt6959") as Txt;

                    Literal literalFavMemberId = e.Item.FindControl("favoriteMemberId") as Literal;
                    Literal literalFavText = e.Item.FindControl("favoriteText") as Literal;
                    SearchResultProfile searchResult = new SearchResultProfile(_Member, ordinal);
                    string _viewProfileUrl = BreadCrumbHelper.MakeViewProfileLink(this.MyEntryPoint, _Member.MemberID, ordinal, null, Constants.NULL_INT, false, (int)BreadCrumbHelper.PremiumEntryPoint.NoTracking);
                    if (!String.IsNullOrEmpty(TrackingParam))
                    {
                        _viewProfileUrl = BreadCrumbHelper.AppendParamToProfileLink(_viewProfileUrl, TrackingParam);
                    }

                    lnkUserName1.NavigateUrl = _viewProfileUrl;
                    //MemberPhotoHelper photoHelper = new MemberPhotoHelper(RuntimeSettings.Instance);
                    noPhoto.NoPhotoFileName = MemberPhotoDisplayManager.Instance.GetNoPhotoFile(NoPhotoImageType.ThumbV2, true); 
                    searchResult.SetThumb(imgThumb, noPhoto, _viewProfileUrl);
                    lnkUserName1.Text = FrameworkGlobals.Ellipsis(_Member.GetUserName(_g.Brand), 12);
                    literalSeeking.Text = Matchnet.Web.Applications.MemberProfile.ProfileDisplayHelper.GetMaritalStatusSeekingGenderDisplay(_Member, this.g);
                    literalAge.Text = FrameworkGlobals.GetAge(_Member, g.Brand).ToString();
                    literalLocation.Text = ProfileDisplayHelper.GetRegionDisplay(_Member, g);

                    //set email and flirt/tease link
                    bool returnToMember = (g.GetEntryPoint(Request) == BreadCrumbHelper.EntryPoint.LookUpMember);
                    ProfileDisplayHelper.SetEmailTeaseLinks(g, _Member.MemberID, ref lnkEmailMe, ref lnkFlirtMe, returnToMember);
                    ProfileDisplayHelper.AddFlirtOnClick(g, ref lnkFlirtMe);

                    if (g.Member.IsPayingMember(g.Brand.Site.SiteID))
                    {
                        lnkEmailMe.Visible = true;
                        imgEmailMe.Visible = true;
                        lnkFlirtMe.Visible = false;
                        imgFlirtMe.Visible = false;
                    }
                    else
                    {
                        lnkEmailMe.Visible = false;
                        imgEmailMe.Visible = false;
                        lnkFlirtMe.Visible = true;
                        imgFlirtMe.Visible = true;
                    }


                    //set add to hot list link
                    bool isFavorite = g.List.IsHotListed(Matchnet.List.ValueObjects.HotListCategory.Default, g.Brand.Site.Community.CommunityID, _Member.MemberID);
                    literalFavText.Text = (isFavorite) ? g.GetResource("TXT_REMOVE", this) : g.GetResource("TXT_ADD", this);
                    literalFavMemberId.Text = _Member.MemberID.ToString();
                }
            }
        }
    }
}
