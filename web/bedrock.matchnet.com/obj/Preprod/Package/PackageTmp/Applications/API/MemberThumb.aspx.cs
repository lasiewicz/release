using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using System.IO;
using System.Xml;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects;
using Matchnet.Member.ValueObjects.Photos;
using Matchnet.Web.Framework;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects;
using Matchnet.Web.Framework.Managers;

namespace Matchnet.Web.Applications.API
{
	/// <summary>
	/// Summary description for MemberThumb.
	/// </summary>
	public class MemberThumb : System.Web.UI.Page
	{
		Matchnet.Member.ServiceAdapters.Member member;
		Content.ValueObjects.BrandConfig.Brand brand;

		private void Page_Load(object sender, System.EventArgs e)
		{
			try
			{
				Int32 memberID = Conversion.CInt(Request["MemberID"]);
				Int32 communityID = Conversion.CInt(Request["CommunityID"]);
				bool showAll = Conversion.CInt(Request["All"]) == 1;

				if(communityID == Constants.NULL_INT)
				{
					communityID = 1;
				}

				Int32 brandID = Constants.NULL_INT;

				member = MemberSA.Instance.GetMember(memberID, MemberLoadFlags.None);
				member.GetLastLogonDate(communityID, out brandID);

				brand = BrandConfigSA.Instance.GetBrandByID(brandID);
                //MemberPhotoHelper photoHelper = new MemberPhotoHelper(RuntimeSettings.Instance);

				if (!member.HasApprovedPhoto(communityID))
				{
                    Response.Redirect(MemberPhotoDisplayManager.Instance.GetNoPhotoFile(NoPhotoImageType.BackgroundThumb, member, brand));  
				}
				else if (!showAll)
				{
                    Photo photo = MemberPhotoDisplayManager.Instance.GetDefaultPhoto(member, member, brand);
                    Response.Redirect(MemberPhotoDisplayManager.Instance.GetPhotoDisplayURL(member, member, brand, photo, PhotoType.Thumbnail,
				                                                           PrivatePhotoImageType.BackgroundThumb,
				                                                           NoPhotoImageType.BackgroundThumb, true));
				}
				else
				{
					StringBuilder sbOutput = new StringBuilder();
					sbOutput.Append("sp_MemberThumb(" + member.MemberID);
					sbOutput.Append(", \"" + member.GetUserName(brand) + "\"");
					sbOutput.Append(", new Array(");

					foreach (Photo photo in member.GetPhotos(communityID))
					{
						//sbOutput.Append("\"" + photo.FileWebPath + "\", ");
                        string photoPath = MemberPhotoDisplayManager.Instance.GetPhotoDisplayURL(member, member, brand, photo, PhotoType.Full,
					                                                            PrivatePhotoImageType.Full,
					                                                            false);
                        sbOutput.Append("\"" + photoPath + "\", ");
					}
					sbOutput.Append("\"\")");
					sbOutput.Append(", new Array(");

					foreach (Photo photo in member.GetPhotos(communityID))
					{
						sbOutput.Append(photo.IsPrivate ? "\"private\", " : "\"public\", ");
					}
					sbOutput.Append("\"\"));");
					sbOutput.Append(Environment.NewLine);

					Response.Write(sbOutput.ToString());
				}
			}
			catch (Exception ex)
			{
				System.Diagnostics.EventLog.WriteEntry("WWW", ex.ToString(), System.Diagnostics.EventLogEntryType.Error);
			}
		}


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
