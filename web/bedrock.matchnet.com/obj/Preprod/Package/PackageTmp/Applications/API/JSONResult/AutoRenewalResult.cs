﻿using System.Collections.Generic;
using Matchnet.Purchase.ValueObjects;
using Matchnet.Web.Applications.API.JSONResult;

public class AutoRenewalResult : ResultBase
{
    //list of properties for all possible values returned that may be needed to update other UI as part of this Ajax update
    public string NextRenewalDate { get; set; }
    public string RenewalAmount { get; set; }
    public string TotalAmount { get; set; }
    public List<PremiumService> PremiumServicesList { get; set; }
    
    

    public class PremiumService
    {
        public string ServiceName;
        public string RenewalAmount;
        public decimal RenewalAmountDecimal;
    }
}