﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.Web.Script.Serialization;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.Region;
using Matchnet.Lib;
using Matchnet.Web.Framework;
using Matchnet.Web.Applications.API.JSONResult;
using System.Text;

namespace Matchnet.Web.Applications.API
{
    /// <summary>
    /// Summary description for RegionService
    /// </summary>
    [WebService(Namespace = "http://spark.net/API")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [ScriptService]
    public class RegionService : WebService
    {

        private ContextGlobal _g;

        public RegionService()
		{
            if (null == _g)
            {
                if (Context != null)
                {
                    if (Context.Items["g"] != null)
                    {
                        _g = (ContextGlobal)Context.Items["g"];
                    }
                    else
                    {
                        _g = new ContextGlobal(Context);
                    }
                }
                //System.Web.UI.Page page = new System.Web.UI.Page();
                //Matchnet.Web.Analytics.Omniture omniture = (Matchnet.Web.Analytics.Omniture)page.LoadControl("/Analytics/Omniture.ascx");
                //_g.AnalyticsOmniture = omniture;
                _g.SaveSession = false;
            }
        }

		[WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string GetRegionId(string zipCode, string cityName,int stateRegionId, int countryRegionId, int languageId)
        {
            RegionID regionId = null;
            if (countryRegionId > 0 && !string.IsNullOrEmpty(zipCode))
            {
                regionId = RegionSA.Instance.FindRegionIdByPostalCode(countryRegionId, zipCode);
            }
            else if (stateRegionId > 0 && languageId > 0 && !string.IsNullOrEmpty(cityName))
            {
                regionId = RegionSA.Instance.FindRegionIdByCity(stateRegionId, cityName, languageId);
            }
            else if (countryRegionId > 0 && languageId > 0 && !string.IsNullOrEmpty(cityName))
            {
                regionId = RegionSA.Instance.FindRegionIdByCity(countryRegionId, cityName, languageId);
            }

            Hashtable h = new Hashtable();
            if (null != regionId && regionId.ID > 0)
            {
                h.Add("regionId", regionId);
            }
            else
            {
                string errMsg=string.Empty;
                if (countryRegionId == ConstantsTemp.REGION_US || countryRegionId == ConstantsTemp.REGIONID_CANADA)
                {
                    errMsg=_g.GetResource("INVALID_POSTAL_CODE");
                }
                else
                {
                    errMsg=_g.GetResource("ERR_CITY_IS_INVALID");
                }
                h.Add("error", errMsg);
            }
            return new JavaScriptSerializer().Serialize(h);
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string GetCities(string zipCode)
        {
            RegionCollection colCities = RegionSA.Instance.RetrieveCitiesByPostalCode(zipCode);
            return new JavaScriptSerializer().Serialize(colCities);
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string GetStates(int countryRegionId, int languageId)
        {
            Hashtable h = new Hashtable();
            if (languageId <= 0)
            {
                languageId = _g.Brand.Site.LanguageID;
            }

            Region regionInfo = RegionSA.Instance.RetrieveRegionByID(countryRegionId, languageId);
            if (regionInfo.ChildrenDepth != 3)
            {
                RegionCollection myRegions;
                if (languageId == (int)Matchnet.Language.Hebrew)
                {
                    myRegions = RegionSA.Instance.RetrieveChildRegions(countryRegionId, languageId, WebConstants.TEMP_TRANSLATION_ID, true);
                }
                else
                {
                    myRegions = RegionSA.Instance.RetrieveChildRegions(countryRegionId, languageId, WebConstants.TEMP_TRANSLATION_ID);
                }             
   
                try
                {	// Determine how the State/Region label should render.
                    int regionMask = myRegions[0].Mask;
                    if (regionMask != 8)
                    {
                        regionMask -= 2;
                    }
                    //h.Add("stateLbl",_g.GetResource(FrameworkGlobals.GetRegionResourceConstant(regionMask)));
                    
                }
                catch { }
                h.Add("states", myRegions);
            }
            h.Add("isUSAAndCanada", (countryRegionId == ConstantsTemp.REGION_US || countryRegionId == ConstantsTemp.REGIONID_CANADA));
            h.Add("isUSA", countryRegionId == ConstantsTemp.REGION_US);
            

            return new JavaScriptSerializer().Serialize(h);
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public RegionResult GetRegionCities(int parentRegionID, string description)
        {
            RegionResult result = new RegionResult();

            try
            {
                int languageId = _g.Brand.Site.LanguageID;

                RegionCollection myRegions;
                if (string.IsNullOrEmpty(description))
                    myRegions = RegionSA.Instance.RetrieveChildRegions(parentRegionID, languageId);
                else
                    myRegions = RegionSA.Instance.RetrieveChildRegions(parentRegionID, languageId, description + "%");

                if (myRegions != null)
                {
                    //add list of cities
                    foreach (Region r in myRegions)
                    {
                        result.CityList.Add(r.Description);
                    }
                }

                result.Status = (int)JSONStatus.Success;
                result.StatusMessage = "Success";
            }
            catch (Exception ex)
            {
                result.Status = (int)JSONStatus.Failed;
                result.StatusMessage = ex.Message;
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public RegionResult GetRegionStates(int countryRegionId)
        {
            RegionResult result = new RegionResult();

            try
            {
                int languageId = _g.Brand.Site.LanguageID;

                Region regionInfo = RegionSA.Instance.RetrieveRegionByID(countryRegionId, languageId);

                //country has states
                if (regionInfo.ChildrenDepth == 2)
                {
                    RegionCollection myRegions;
                    myRegions = RegionSA.Instance.RetrieveChildRegions(countryRegionId, languageId);

                    if (myRegions != null)
                    {
                        try
                        {	// Determine how the State/Region label should render.
                            int regionMask = myRegions[0].Mask;
                            if (regionMask != 8)
                            {
                                regionMask -= 2;
                            }                            
                        }
                        catch { }

                        //add list of states
                        foreach (Region r in myRegions)
                        {
                            result.StatesList.Add(r.RegionID.ToString(), r.Description);
                        }
                        result.hasStates = true;
                    }

                }
                result.isCanada = countryRegionId == ConstantsTemp.REGIONID_CANADA;
                result.isUSA = countryRegionId == ConstantsTemp.REGION_US;

                result.Status = (int)JSONStatus.Success;
                result.StatusMessage = "Success";
            }
            catch (Exception ex)
            {
                result.Status = (int)JSONStatus.Failed;
                result.StatusMessage = ex.Message;
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public RegionResult GetRegionInfoByZipCode(string zipCode, int countryRegionId)
        {
            RegionResult result = new RegionResult();

            RegionID regionId = null;
            int languageId = _g.Brand.Site.LanguageID;
            if (countryRegionId > 0 && !string.IsNullOrEmpty(zipCode))
            {
                //get region by zip
                regionId = RegionSA.Instance.FindRegionIdByPostalCode(countryRegionId, zipCode);
            }

            result.RegionID = (regionId != null) ? regionId.ID : 0;
            if (result.RegionID > 0)
            {
                result.LocationDisplayText = FrameworkGlobals.GetRegionString(regionId.ID, _g.Brand.Site.LanguageID);

                result.Status = (int)JSONStatus.Success;
                result.StatusMessage = "Success";
            }
            else
            {
                result.Status = (int)JSONStatus.Failed;
                result.StatusMessage = _g.GetResource("TXT_INVALID_POSTAL_CODE");
                
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public RegionResult GetRegionInfoByCity(string cityName, int stateRegionId, int countryRegionId)
        {
            RegionResult result = new RegionResult();

            RegionID regionId = null;
            int languageId = _g.Brand.Site.LanguageID;

            //verify if country requires state/region
             bool isStatesRequired = RegionSA.Instance.RetrieveRegionByID(countryRegionId, _g.Brand.Site.LanguageID).ChildrenDepth == 2;

            //get region by city based on country/state or just country
            if (stateRegionId > 0 && languageId > 0 && !string.IsNullOrEmpty(cityName))
            {
                regionId = RegionSA.Instance.FindRegionIdByCity(stateRegionId, cityName, languageId);
            }
            else if (countryRegionId > 0 && languageId > 0 && !string.IsNullOrEmpty(cityName))
            {
                regionId = RegionSA.Instance.FindRegionIdByCity(countryRegionId, cityName, languageId);
            }

            result.RegionID = (regionId != null) ? regionId.ID : 0;
            if (result.RegionID > 0)
            {
                result.LocationDisplayText = FrameworkGlobals.GetRegionString(regionId.ID, _g.Brand.Site.LanguageID);

                result.Status = (int)JSONStatus.Success;
                result.StatusMessage = "Success";
            }
            else
            {
                result.Status = (int)JSONStatus.Failed;
                if (isStatesRequired && stateRegionId <= 0)
                    result.StatusMessage = _g.GetResource("TXT_INVALID_STATE");
                else
                    result.StatusMessage = _g.GetResource("TXT_INVALID_CITY");

            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public RegionResult GetAreaCodeInfo(string areaCodes, int countryRegionId)
        {
            RegionResult result = new RegionResult();

            StringBuilder sbAreaCodes = new StringBuilder();

            if (!String.IsNullOrEmpty(areaCodes))
            {
                string[] arrAreaCodes = areaCodes.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                foreach (string areaCode in arrAreaCodes)
                {
                    //validate area code, only add valid ones
                    Int32 intAreaCode = Conversion.CInt(areaCode);
                    if (intAreaCode > 0 && RegionSA.Instance.IsValidAreaCode(intAreaCode, countryRegionId))
                    {
                        sbAreaCodes.Append(areaCode + ",");
                    }
                }
            }

            result.ValidAreaCodes = sbAreaCodes.Length > 0 ? sbAreaCodes.ToString().Substring(0, sbAreaCodes.Length - 1) : "";
            
            if (result.ValidAreaCodes.Length > 0)
            {
                result.LocationDisplayText = result.ValidAreaCodes.Replace(",", ", ");

                result.Status = (int)JSONStatus.Success;
                result.StatusMessage = "Success";
            }
            else
            {
                result.Status = (int)JSONStatus.Failed;
                result.StatusMessage = _g.GetResource("TXT_INVALID_AREA_CODE");
            }

            return result;
        }

    }
}
