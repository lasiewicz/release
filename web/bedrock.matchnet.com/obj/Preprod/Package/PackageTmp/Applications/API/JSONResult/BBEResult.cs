﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Matchnet.Web.Applications.API.JSONResult
{
    /// <summary>
    /// Class to hold various Black Board eat results for ajax calls
    /// </summary>
    [Serializable]
    public class BBEResult : ResultBase
    {
        public bool BBEExpired { get; set; }
        public string BBEPasscode { get; set; }
    }
}