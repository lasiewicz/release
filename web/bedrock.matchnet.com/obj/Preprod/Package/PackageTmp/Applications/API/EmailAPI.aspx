<%@ Page language="c#" Codebehind="EmailAPI.aspx.cs" AutoEventWireup="false" Inherits="Matchnet.Web.Applications.API.EmailAPI" %>

<asp:Repeater ID="xmlRepeater" Runat="server">
	<HeaderTemplate>
		<messages>
	</HeaderTemplate>
	<ItemTemplate>
		<message>
			<from><asp:Literal ID="litFrom" Runat="server" /></from>
			<subject><asp:Literal ID="litSubject" Runat="server" /></subject>
		</message>
	</ItemTemplate>
	<FooterTemplate>
		</messages>
	</FooterTemplate>
</asp:Repeater>
