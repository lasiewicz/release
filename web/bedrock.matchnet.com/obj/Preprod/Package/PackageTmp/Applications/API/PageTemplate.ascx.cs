using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using Matchnet.Web.Framework;

namespace Matchnet.Web.Applications.API
{
	public class PageTemplate : FrameworkControl
	{

		private void Page_Load(object sender, System.EventArgs e)
		{
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			InitializeComponent();
			base.OnInit(e);
		}
		
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
			this.PreRender += new EventHandler(PageTemplate_PreRender);
		}
		#endregion

		private void PageTemplate_PreRender(object sender, EventArgs e)
		{

		}
	}
}
