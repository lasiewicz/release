﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Purchase.ServiceAdapters;
using Matchnet.Purchase.ValueObjects;
using Matchnet.Web.Applications.API.JSONResult;
using Matchnet.Web.Framework;
using Spark.Common.Adapter;
using System.ComponentModel;
using Matchnet.Web.Framework.Managers;
using Spark.Common.PurchaseService;

namespace Matchnet.Web.Applications.API
{
    /// <summary>
    /// Summary description for AutoRenewal
    /// </summary>
    [WebService(Namespace = "http://spark.net/API")]
    [System.Web.Script.Services.ScriptService]
    public class OneClickPurchase : System.Web.Services.WebService
    {
        private ContextGlobal _g;

        private object ResourceControl
        {
            get
            {
                System.Web.UI.Page page = new System.Web.UI.Page();
                return page.LoadControl("/Applications/MemberServices/AutoRenewalSettings20.ascx");
            }
        }

        public OneClickPurchase()
        {
            if (null == _g)
            {
                if (Context != null)
                {
                    if (Context.Items["g"] != null)
                    {
                        _g = (ContextGlobal)Context.Items["g"];
                    }
                    else
                    {
                        _g = new ContextGlobal(Context);
                    }
                    if (_g != null)
                        _g.SaveSession = false;
                }
            }

            InitializeComponent();
        }

        #region Component Designer generated code

        private IContainer components = null;

        private void InitializeComponent()
        {
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && components != null)
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #endregion

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public OneClickPurchaseResult PurchaseAllAccess(int memberID)
        {
            var result = new OneClickPurchaseResult();

            try
            {
                if (_g != null)
                {
                    //result = CreateMockData(_g.Member.GetUserName(_g.Brand));
                    //return result;
                    var oneClickPurchaseManager = new OneClickPurchaseManager();

                    // Check if one click payment profile exists
                    if (!oneClickPurchaseManager.MemberHaveValidOneClickPaymentProfile(_g.Member.MemberID, _g.Brand.Site.SiteID))
                    {
                        result.Status = (int)JSONStatus.Failed;
                        result.StatusMessage = "No valid one click payment profile";
                        return result;
                    }

                    // Purchase all access
                    var package = oneClickPurchaseManager.GetUpsalePackage(_g, PremiumType.AllAccess);
                    string oneClickPackages = oneClickPurchaseManager.SerializeOneClickPackage(package);

                    Spark.Common.PurchaseService.purchaseResponse purchaseResponse = oneClickPurchaseManager.DoOneClickPurchase(_g, 4148, oneClickPackages);

                    var orderInfo = OrderHistoryServiceWebAdapter.GetProxyInstance().GetOrderInfo(int.Parse(purchaseResponse.orderID));
                    result.Username = _g.Member.GetUserName(_g.Brand);
                    result.OrderID = orderInfo.OrderID.ToString();
                    result.CreditCardType = oneClickPurchaseManager.GetCreditCardTypeText(orderInfo.OrderID,
                        orderInfo.CustomerID, orderInfo.CallingSystemID, _g);
                    result.LastFourDigitsCreditCardNumber = orderInfo.LastFourAccountNumber;
                    result.TransactionDate = DateTime.Now.ToShortDateString();
                    result.StatusMessage = "Success";
                    result.Status = (int)JSONStatus.Success;
                }
            }
            catch (Exception ex)
            {
                _g.ProcessException(ex);
                result.Status = (int)JSONStatus.Failed;
                //result.StatusMessage = ex.Message;
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public OneClickPurchaseResult AddAllAccess(int memberID)
        {
            var result = new OneClickPurchaseResult();

            try
            {
                if (_g != null)
                {
                    //result = CreateMockData(_g.Member.GetUserName(_g.Brand));
                    //return result;
                    var oneClickPurchaseManager = new OneClickPurchaseManager();

                    // Check if one click payment profile exists
                    if (!oneClickPurchaseManager.MemberHaveValidOneClickPaymentProfile(_g.Member.MemberID, _g.Brand.Site.SiteID))
                    {
                        result.Status = (int)JSONStatus.Failed;
                        result.StatusMessage = "No valid one click payment profile";
                        return result;
                    }

                    // Purchase all access emails
                    var package = oneClickPurchaseManager.GetUpsalePackage(_g, PremiumType.AllAccessEmail);
                    string oneClickPackages = oneClickPurchaseManager.SerializeOneClickPackage(package);

                    Spark.Common.PurchaseService.purchaseResponse purchaseResponse = oneClickPurchaseManager.DoOneClickPurchase(_g, 4149, oneClickPackages);

                    var orderInfo = OrderHistoryServiceWebAdapter.GetProxyInstance().GetOrderInfo(int.Parse(purchaseResponse.orderID));
                    result.Username = _g.Member.GetUserName(_g.Brand);
                    result.OrderID = orderInfo.OrderID.ToString();
                    result.CreditCardType = oneClickPurchaseManager.GetCreditCardTypeText(orderInfo.OrderID,
                    orderInfo.CustomerID, orderInfo.CallingSystemID, _g);
                    result.LastFourDigitsCreditCardNumber = orderInfo.LastFourAccountNumber;
                    result.TransactionDate = DateTime.Now.ToShortDateString();
                    result.StatusMessage = "Success";
                    result.Status = (int)JSONStatus.Success;
                }
            }
            catch (Exception ex)
            {
                _g.ProcessException(ex);
                result.Status = (int)JSONStatus.Failed;
                //result.StatusMessage = ex.Message;
            }

            return result;
        }

        private OneClickPurchaseResult CreateMockData(string username)
        {
            return new OneClickPurchaseResult()
            {
                Username = username,
                OrderID = "9999999999",
                CreditCardType = "ויזה",
                LastFourDigitsCreditCardNumber = "1234",
                Status = (int)JSONStatus.Success,
                StatusMessage = "Success!",
                TransactionDate = DateTime.Now.ToShortDateString()
            };
        }
    }
}
