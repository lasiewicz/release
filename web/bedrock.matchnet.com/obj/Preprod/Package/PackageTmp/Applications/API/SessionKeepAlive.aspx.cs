using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

//using Matchnet.Content.ServiceAdapters;
//using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Session.ServiceAdapters;
using Matchnet.Session.ValueObjects;

namespace Matchnet.Web.Applications.API
{
	/// <summary>
	/// To keep session alive... to be called just under every 20 minutes 
	/// </summary>
	public class SessionKeepAlive : SessionDependentAPIBase
	{
		private void Page_Load(object sender, System.EventArgs e)
		{
            // Do nothing! Keep session alive by simply calling session in base

            //must save session so it doesn't expire in middle tier (web server will keep it)
            SaveSession();
			
			if (! (MemberID > 0) ) 
			{
				Response.Write("notLoggedIn()");
			}
		}



		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
