﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using Matchnet.Session.ValueObjects;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Util;

namespace Matchnet.Web.Applications.API
{
    /// <summary>
    /// Summary description for Captcha
    /// </summary>
    [WebService(Namespace = "http://spark.net/API")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class Captcha : System.Web.Services.WebService
    {
        private ContextGlobal _g;

        public Captcha()
        {
            if (null == _g)
            {
                if (Context != null)
                {
                    if (Context.Items["g"] != null)
                    {
                        _g = (ContextGlobal)Context.Items["g"];
                    }
                    else
                    {
                        _g = new ContextGlobal(Context);
                    }
                    if (_g != null)
                        _g.SaveSession = false;
                }
            }
        }

        [WebMethod]
        public string RefreshCaptcha()
        {
            string result = "success";
            try
            {
                string generatedText = RandomTextGenerator.Generate(4, 4);
                _g.Session.Add("CAPTCHAText", generatedText, SessionPropertyLifetime.Temporary);
            }
            catch (Exception ex)
            {
                result = "error";
            }

            return result;
        }
    }
}
