﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Purchase.ServiceAdapters;
using Matchnet.Purchase.ValueObjects;
using Matchnet.Web.Applications.API.JSONResult;
using Matchnet.Web.Framework;
using Spark.Common.Adapter;
using System.ComponentModel;
using Matchnet.Web.Framework.Managers;
using Spark.Common.PurchaseService;

namespace Matchnet.Web.Applications.API
{
    /// <summary>
    /// Summary description for AutoRenewal
    /// </summary>
    [WebService(Namespace = "http://spark.net/API")]
    [System.Web.Script.Services.ScriptService]
    public class AutoRenewal : System.Web.Services.WebService
    {
        private ContextGlobal _g;

        private object ResourceControl
        {
            get
            {
                System.Web.UI.Page page = new System.Web.UI.Page();
                return page.LoadControl("/Applications/MemberServices/AutoRenewalSettings20.ascx");
            }
        }

        public AutoRenewal()
        {
            if (null == _g)
            {
                if (Context != null)
                {
                    if (Context.Items["g"] != null)
                    {
                        _g = (ContextGlobal)Context.Items["g"];
                    }
                    else
                    {
                        _g = new ContextGlobal(Context);
                    }
                    if (_g != null)
                        _g.SaveSession = false;
                }
            }

            InitializeComponent();
        }

        #region Component Designer generated code

        private IContainer components = null;

        private void InitializeComponent()
        {
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && components != null)
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #endregion

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public AutoRenewalResult EnableAutoRenewal(Int32 memberID)
        {
            AutoRenewalResult autoRenewalResult = new AutoRenewalResult() { Status = (int)JSONResult.JSONStatus.Failed };
            var premiumServicesDic = new Dictionary<PremiumType, AutoRenewalResult.PremiumService>();

            try
            {
                Matchnet.Member.ServiceAdapters.Member member = MemberSA.Instance.GetMember(memberID,
                                                                                       MemberLoadFlags.None);
                List<int> secondaryPackageIDToEnable = new List<int>();

                Spark.Common.RenewalService.RenewalSubscription renewalSub = RenewalManager.Instance.GetRenewalSubscription(member.MemberID, _g.Brand);
                if (renewalSub != null && renewalSub.RenewalSubscriptionID > 0)
                {
                    //Enable renewal for base + a la carte
                    if (!renewalSub.IsRenewalEnabled)
                    {
                        decimal renewalTotal = 0m;

                        autoRenewalResult.NextRenewalDate = renewalSub.RenewalDatePST.ToString();

                        //base plan
                        Plan _plan = PlanSA.Instance.GetPlan(renewalSub.PrimaryPackageID, _g.Brand.BrandID);

                        if (_plan != null)
                        {
                            autoRenewalResult.RenewalAmount = Convert.ToString(_plan.RenewCost).Trim();
                            renewalTotal = _plan.RenewCost;
                        }

                        //a la carte plan(s)
                        if (renewalSub.RenewalSubscriptionDetails.Length > 1)
                        {
                            premiumServicesDic = new Dictionary<PremiumType, AutoRenewalResult.PremiumService>();

                            Plan rsePlan = null;
                            foreach (Spark.Common.RenewalService.RenewalSubscriptionDetail rse in renewalSub.RenewalSubscriptionDetails)
                            {
                                if (!rse.IsPrimaryPackage)
                                {
                                    rsePlan = PlanSA.Instance.GetPlan(rse.PackageID, _g.Brand.BrandID);

                                    foreach (PremiumType premiumType in Enum.GetValues(typeof(PremiumType)))
                                    {
                                        if ((rsePlan.PremiumTypeMask & premiumType) == premiumType && IsPremiumTypeRenewable(premiumType))
                                        {
                                            premiumServicesDic[premiumType] = new AutoRenewalResult.PremiumService()
                                            {
                                                RenewalAmount = Convert.ToString(rsePlan.RenewCost).Trim(),
                                                RenewalAmountDecimal = rsePlan.RenewCost,
                                                ServiceName = premiumType.ToString()
                                            };

                                            secondaryPackageIDToEnable.Add(rse.PackageID);
                                        }
                                    }
                                }
                            }
                            autoRenewalResult.PremiumServicesList = new List<AutoRenewalResult.PremiumService>();
                            foreach (PremiumType premiumType in premiumServicesDic.Keys)
                            {
                                renewalTotal += premiumServicesDic[premiumType].RenewalAmountDecimal;
                                autoRenewalResult.PremiumServicesList.Add(new AutoRenewalResult.PremiumService()
                                                                               {
                                                                                   RenewalAmount = premiumServicesDic[premiumType].RenewalAmount,
                                                                                   //ServiceName = premiumServicesDic[premiumType].ServiceName
                                                                                   ServiceName = _g.GetResource("TXT_" + premiumServicesDic[premiumType].ServiceName.Replace(" ", string.Empty).ToUpper(), ResourceControl)
                                                                               });
                            }
                        }


                        autoRenewalResult.TotalAmount = renewalTotal.ToString("N2");

                        Spark.Common.RenewalService.RenewalResponse renewalResponse =
                          RenewalServiceWebAdapter.GetProxyInstanceForBedrock().EnableAutoRenewal(
                              secondaryPackageIDToEnable.ToArray(), member.MemberID, _g.Brand.Site.SiteID,
                              1, Constants.NULL_INT, "", Constants.NULL_INT);

                        if (renewalResponse.InternalResponse.responsecode != "0")
                        {
                            // Error
                            autoRenewalResult.StatusMessage = "EnableAutoRenewal Error";
                        }
                        else
                        {
                            // Success
                            autoRenewalResult.Status = (int)JSONResult.JSONStatus.Success;
                        }

                    }
                    else
                    {
                        autoRenewalResult.StatusMessage = "IsRenewalEnabled";
                    }

                }
                else
                {
                    autoRenewalResult.StatusMessage = "No renewalSub";
                }
            }
            catch (Exception ex)
            {
                if (_g != null)
                {
                    _g.ProcessException(ex);
                    autoRenewalResult.StatusMessage = "Exception Error";
                }
            }

            return autoRenewalResult;
        }

        private bool IsPremiumTypeRenewable(PremiumType premiumType)
        {
            if (premiumType == PremiumType.HighlightedProfile ||
                premiumType == PremiumType.SpotlightMember ||
                premiumType == PremiumType.JMeter ||
                premiumType == PremiumType.AllAccess ||
                premiumType == PremiumType.ReadReceipt
                )
                return true;
            return false;

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public OneClickPurchaseResult PurchaseAllAccess(int memberID)
        {
            var result = new OneClickPurchaseResult();

            try
            {
                if (_g != null)
                {
                    //result = CreateMockData(_g.Member.GetUserName(_g.Brand));
                    //return result;
                    var oneClickPurchaseManager = new OneClickPurchaseManager();

                    // Check if one click payment profile exists
                    if (!oneClickPurchaseManager.MemberHaveValidOneClickPaymentProfile(_g.Member.MemberID, _g.Brand.Site.SiteID))
                    {
                        result.Status = (int)JSONStatus.Failed;
                        result.StatusMessage = "No valid one click payment profile";
                        return result;
                    }

                    // Purchase all access
                    var package = oneClickPurchaseManager.GetUpsalePackage(_g, PremiumType.AllAccess);
                    string oneClickPackages = oneClickPurchaseManager.SerializeOneClickPackage(package);

                    Spark.Common.PurchaseService.purchaseResponse purchaseResponse = oneClickPurchaseManager.DoOneClickPurchase(_g, 4148, oneClickPackages);

                    var orderInfo = OrderHistoryServiceWebAdapter.GetProxyInstance().GetOrderInfo(int.Parse(purchaseResponse.orderID));
                    result.Username = _g.Member.GetUserName(_g.Brand);
                    result.OrderID = orderInfo.OrderID.ToString();
                    result.CreditCardType = oneClickPurchaseManager.GetCreditCardTypeText(orderInfo.OrderID,
                        orderInfo.CustomerID, orderInfo.CallingSystemID, _g);
                    result.LastFourDigitsCreditCardNumber = orderInfo.LastFourAccountNumber;
                    result.TransactionDate = DateTime.Now.ToShortDateString();
                    result.StatusMessage = "Success";
                    result.Status = (int)JSONStatus.Success;
                }
            }
            catch (Exception ex)
            {
                _g.ProcessException(ex);
                result.Status = (int)JSONStatus.Failed;
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public OneClickPurchaseResult AddAllAccess(int memberID)
        {
            var result = new OneClickPurchaseResult();

            try
            {
                if (_g != null)
                {
                    //result = CreateMockData(_g.Member.GetUserName(_g.Brand));
                    //return result;
                    var oneClickPurchaseManager = new OneClickPurchaseManager();

                    // Check if one click payment profile exists
                    if (!oneClickPurchaseManager.MemberHaveValidOneClickPaymentProfile(_g.Member.MemberID, _g.Brand.Site.SiteID))
                    {
                        result.Status = (int)JSONStatus.Failed;
                        result.StatusMessage = "No valid one click payment profile";
                        return result;
                    }

                    // Purchase all access emails
                    var package = oneClickPurchaseManager.GetFixedPricingUpslaePackage(_g);
                    string oneClickPackages = oneClickPurchaseManager.SerializeOneClickPackage(package);

                    Spark.Common.PurchaseService.purchaseResponse purchaseResponse = oneClickPurchaseManager.DoOneClickPurchase(_g, 4149, oneClickPackages);

                    var orderInfo = OrderHistoryServiceWebAdapter.GetProxyInstance().GetOrderInfo(int.Parse(purchaseResponse.orderID));
                    result.Username = _g.Member.GetUserName(_g.Brand);
                    result.OrderID = orderInfo.OrderID.ToString();
                    result.CreditCardType = oneClickPurchaseManager.GetCreditCardTypeText(orderInfo.OrderID,
                    orderInfo.CustomerID, orderInfo.CallingSystemID, _g);
                    result.LastFourDigitsCreditCardNumber = orderInfo.LastFourAccountNumber;
                    result.TransactionDate = DateTime.Now.ToShortDateString();
                    result.StatusMessage = "Success";
                    result.Status = (int)JSONStatus.Success;
                }
            }
            catch (Exception ex)
            {
                _g.ProcessException(ex);
                result.Status = (int)JSONStatus.Failed;
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public PackagePricingDetails GetPackagePricingDetails(string package)
        {
            var result = new PackagePricingDetails();

            try
            {
                if (_g != null)
                {
                    var oneClickPurchaseManager = new OneClickPurchaseManager();

                    PremiumType pt;
                    if (!PremiumType.TryParse(package, true, out pt))
                    {
                        result.Status = (int)JSONStatus.Failed;
                        result.StatusMessage = "Error parsing package name";
                        return result;
                    }

                   
                    OneClickPurchaseManager.OneClickPackage upsalePackage;
                    if (pt == PremiumType.AllAccess)
                    {
                        upsalePackage = oneClickPurchaseManager.GetUpsalePackage(_g, pt);
                    }
                    else
                    {
                        upsalePackage = oneClickPurchaseManager.GetFixedPricingUpslaePackage(_g);
                    }
                    result.PackageOverrideTotalAmount = upsalePackage.PackageOverrideTotalAmount.ToString("0.00");
                    result.StatusMessage = "Success";
                    result.Status = (int)JSONStatus.Success;
                }
            }
            catch (Exception ex)
            {
                _g.ProcessException(ex);
                result.Status = (int)JSONStatus.Failed;
            }

            return result;
        }

    }
}
