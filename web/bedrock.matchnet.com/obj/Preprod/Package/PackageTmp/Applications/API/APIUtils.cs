﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Matchnet.Web.Applications.API
{
    public class APIUtils
    {
        public static Matchnet.Content.ValueObjects.BrandConfig.Brand GetAnyBrandForCommunity(int communityId)
        {
            Matchnet.Content.ValueObjects.BrandConfig.Brands brands = Matchnet.Content.ServiceAdapters.BrandConfigSA.Instance.GetBrandsByCommunity(communityId);
            Matchnet.Content.ValueObjects.BrandConfig.Brand theBrand = null;

            foreach(Matchnet.Content.ValueObjects.BrandConfig.Brand brand in brands)
            {
                theBrand = brand;
                break;
            }
            
            return theBrand;
        }
    }
}
