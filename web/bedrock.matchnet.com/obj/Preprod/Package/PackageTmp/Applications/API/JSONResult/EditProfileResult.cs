﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Matchnet.Web.Applications.API.JSONResult
{
    /// <summary>
    /// Result class to encapsulate data returned from updating edit profile
    /// </summary>
    [Serializable]
    public class EditProfileResult : ResultBase
    {
        public int MemberID { get; set; }
        public string ItemHTML { get; set; }
        public List<SectionHTML> SectionHTMLs { get; set; }
        public List<EditProfileValidationError> ValidationErrors { get; set; }

        //this contains a list of profile attributes that changed which requires email notification sent
        //to help prevent fraud and hacked accounts.  As of now, it is only used on the server side.
        [NonSerialized]
        public List<string> ProfileChangedAttributesForEmailConfirmation;

        public EditProfileResult()
        {
            ValidationErrors = new List<EditProfileValidationError>();
            ProfileChangedAttributesForEmailConfirmation = new List<string>();
        }
    }

    [Serializable]
    public class EditProfileValidationError
    {
        public string AttributeName { get; set; }
        public string ValidationMessage { get; set; }
    }

    [Serializable]
    public class SectionHTML
    {
        public int GroupTypeID { get; set; }
        public string SectionName { get; set; }
        public string HTML { get; set; }
    }
}
