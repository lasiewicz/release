﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Matchnet.Web.Applications.API.JSONResult
{
    [Serializable]
    public class IMHistoryMemberListResult : ResultBase
    {
        public int MemberID { get; set; }
        public int BatchResultCount { get; set; }
        public int TotalResultCount { get; set; }
        public List<IMHistoryMember> IMHistoryMemberList { get; set; }
    }

    [Serializable]
    public class IMHistoryMember
    {
        public int MemberID { get; set; }
        public string AgeLocationDisplayText { get; set; }
        public string ThumbWebPath { get; set; }
        public string UserName { get; set; }
        public string LastMessageDateString { get; set; }
    }
}