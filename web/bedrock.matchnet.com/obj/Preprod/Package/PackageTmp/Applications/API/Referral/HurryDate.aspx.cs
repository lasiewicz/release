﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Content.ServiceAdapters;

namespace Matchnet.Web.Applications.API.Referral
{
    /// <summary>
    /// A jump page used as the URL in GAM for HurryDate ads.  The page retrieves member information, and passes it along
    /// to HurryDate's ad landing page via redirect.
    /// </summary>
    public partial class HurryDate : SessionDependentAPIBase
    {
        

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //get hurrydate promoID, should have been provided during URL input in GAM
                string hurrydatePromoID = Request["promotionid"];

                Matchnet.Member.ServiceAdapters.Member member = null;
                if (this.MemberID > 0 && !String.IsNullOrEmpty(hurrydatePromoID))
                {
                    //get member object
                    member = MemberSA.Instance.GetMember(this.MemberID, MemberLoadFlags.None);

                    //get zipcode
                    //int regionID = member.GetAttributeInt(this.Brand, "RegionID", 0);
                    //Matchnet.Content.ValueObjects.Region.RegionLanguage regionLang = RegionSA.Instance.RetrievePopulatedHierarchy(regionID, Brand.Site.LanguageID);
                    //string zipcode = (regionLang != null && regionLang.PostalCode != null) ? regionLang.PostalCode : "";

                    string landing = "jdate";
                    switch (Brand.Site.SiteID)
                    {
                        case 101:
                            landing = "as";
                            break;
                    }
                    
                    if (member != null)
                    {
                        //direct to hurrydate ad landing page
                        Response.Redirect(String.Format("http://www.hurrydate.com/index.cfm?fuseaction=landing.{0}&promotionid={1}&siteid={2}&memberid={3}", new object[] { landing, hurrydatePromoID, this.Brand.Site.SiteID, this.MemberID }),false);
                        //Response.Write(String.Format("http://www.hurrydate.com/index.cfm?fuseaction=landing.cm&promotionid={0}&siteid={1}&memberid={2}&zipcode={3}", new object[] { hurrydatePromoID, this.Brand.Site.SiteID, this.MemberID, zipcode }));
                    }
                }

                if (String.IsNullOrEmpty(hurrydatePromoID))
                {
                    Exception ex = new Exception("No HurryDate promotion id for GAM Ad clickthrough");
                }
                else if (member == null)
                {
                    Exception ex = new Exception("Member object not found for HurryDate GAM Ad clickthrough, memberID = " + this.MemberID.ToString());
                }
            }
            catch (Exception ex)
            {
                //write to event log
                Matchnet.Web.Framework.Diagnostics.ContextExceptions.WriteExceptionToEventLogNoG(ex);

                //redirect back to home
                
            }
            
        }

    }
}
