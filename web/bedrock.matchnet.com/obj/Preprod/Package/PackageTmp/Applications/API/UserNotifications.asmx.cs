﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.Web.Script.Serialization;
using Matchnet.Web.Framework;

using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Web.Applications.UserNotifications.Controls;
using Matchnet.UserNotifications.ValueObjects;

namespace Matchnet.Web.Applications.API
{
    /// <summary>
    /// Summary description for Notifications
    /// </summary>
    [WebService(Namespace = "http://spark.net/API")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    [ScriptService]
    public class UserNotificationsService : WebService
    {

        private Int32 _communityID;
        private Brand _brand;
        private ContextGlobal _g;

        public UserNotificationsService()
		{
            if (null == _g)
            {
                if (Context != null)
                {
                    if (Context.Items["g"] != null)
                    {
                        _g = (ContextGlobal)Context.Items["g"];
                    }
                    else
                    {
                        _g = new ContextGlobal(Context);
                    }
                    _g.SaveSession = false;
                }
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string GetUserNotifications(Int32 brandID, Int32 memberID, Int32 startIdx, Int32 pageSize, bool setAsViewed)
        {
            _brand = BrandConfigSA.Instance.GetBrandByID(brandID);
            _communityID = _brand.Site.Community.CommunityID;
            Matchnet.Member.ServiceAdapters.Member member = MemberSA.Instance.GetMember(memberID, MemberLoadFlags.None);
            System.Web.UI.Page page = new System.Web.UI.Page();
            UserNotificationsControl ctrl = page.LoadControl("/Applications/UserNotifications/Controls/UserNotificationsControl.ascx") as UserNotificationsControl;
            int totalNotifications = ctrl.GetTotalUserNotificationViewObjectsNumber(memberID);
            List<UserNotificationViewObject> notifications = ctrl.GetUserNotficationViewObjects(memberID, startIdx, pageSize, setAsViewed);
            JavaScriptSerializer serializer= new JavaScriptSerializer();
            return serializer.Serialize(new object[]{notifications, totalNotifications});            
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string GetNewNotificationsNumber(Int32 brandID, Int32 memberID)
        {
            _brand = BrandConfigSA.Instance.GetBrandByID(brandID);
            _communityID = _brand.Site.Community.CommunityID;
            Matchnet.Member.ServiceAdapters.Member member = MemberSA.Instance.GetMember(memberID, MemberLoadFlags.None);
            System.Web.UI.Page page = new System.Web.UI.Page();
            UserNotificationsControl ctrl = page.LoadControl("/Applications/UserNotifications/Controls/UserNotificationsControl.ascx") as UserNotificationsControl;
            Hashtable h = new Hashtable();
            int num = ctrl.GetCountOfNewUserNotficationViewObjects(memberID);           
            h.Add("newNotifications", ((num > 99)?"99+":num+""));
            return new JavaScriptSerializer().Serialize(h);
        }

        private Random getTotallyRandom(int memberID)
        {
            Random r = new Random(memberID);
            int d = Math.Abs((memberID * r.Next()) / (int)DateTime.Now.Ticks);
            return new Random(d);
        }
    }

}
