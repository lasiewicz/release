﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.Web.Script.Serialization;
using Matchnet.Web.Framework;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.MemberLike.ValueObjects;
using Matchnet.Web.Applications.MemberLike;

namespace Matchnet.Web.Applications.API
{
    /// <summary>
    /// Summary description for MemberLikeService
    /// </summary>
    [WebService(Namespace = "http://spark.net/API")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class MemberLikeService : System.Web.Services.WebService
    {
        private ContextGlobal _g;

        public MemberLikeService()
        {
            if (null == _g)
            {
                if (Context != null)
                {
                    if (Context.Items["g"] != null)
                    {
                        _g = (ContextGlobal)Context.Items["g"];
                    }
                    else
                    {
                        _g = new ContextGlobal(Context);
                    }
                    if (_g != null)
                        _g.SaveSession = false;
                }
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public PayLoad ProcessMemberLike(int brandId, int memberId, int likeObjectId, int likeObjectTypeId)
        {
            Brand brand = BrandConfigSA.Instance.GetBrandByID(brandId);
            MemberLikeParams likeParams=new MemberLikeParams();
            likeParams.SiteId=brand.Site.SiteID;
            likeParams.MemberId=memberId;
            PayLoad payLoad =null;
            if (null != _g.Member && _g.Member.MemberID == memberId)
            {
                payLoad = MemberLikeHelper.Instance.ProcessMemberLike(likeParams, likeObjectId, likeObjectTypeId, brand);
            }
            else
            {
                payLoad = new PayLoad();
            }
            return payLoad;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public List<MemberLikeObject> GetMemberLikesByObjectAndType(int brandId, int memberId, int likeObjectId, int likeObjectTypeId)
        {
            Brand brand = BrandConfigSA.Instance.GetBrandByID(brandId);
            MemberLikeParams likeParams = new MemberLikeParams();
            likeParams.SiteId = brand.Site.SiteID;
            likeParams.MemberId = memberId;
            List<MemberLikeObject> likeObjects = MemberLikeHelper.Instance.GetLikesByObjectIdAndType(likeParams, likeObjectId, likeObjectTypeId);
            return likeObjects;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public List<MemberLikeObject> GetMemberLikesByMember(int brandId, int memberId, int likeObjectId, int likeObjectTypeId)
        {
            Brand brand = BrandConfigSA.Instance.GetBrandByID(brandId);
            MemberLikeParams likeParams = new MemberLikeParams();
            likeParams.SiteId = brand.Site.SiteID;
            likeParams.MemberId = memberId;
            List<MemberLikeObject> likeObjects = MemberLikeHelper.Instance.GetLikesByMember(likeParams);
            return likeObjects;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void ProcessLikeNotification(int brandId, int memberId, int questionId, int answerId) 
        {
            Brand brand = BrandConfigSA.Instance.GetBrandByID(brandId);
            MemberLikeParams likeParams = new MemberLikeParams();
            likeParams.SiteId = brand.Site.SiteID;
            likeParams.MemberId = memberId;
            Hashtable additionalParams = new Hashtable();
            additionalParams["questionId"] = questionId;
            additionalParams["answerId"] = answerId;
            if (null != _g.Member && _g.Member.MemberID == memberId)
            {
                MemberLikeHelper.Instance.ProcessUserNotification(likeParams, answerId, (int)LikeObjectTypes.Answer, _g, additionalParams);
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void ProcessFreeTextLikeNotification(int brandId, int memberId, int questionId, int objectId, int profileOwnerId, string sectionAttribute)
        {
            Brand brand = BrandConfigSA.Instance.GetBrandByID(brandId);
            MemberLikeParams likeParams = new MemberLikeParams();
            likeParams.SiteId = brand.Site.SiteID;
            likeParams.MemberId = memberId;
            Hashtable additionalParams = new Hashtable();
            additionalParams["questionId"] = questionId;
            additionalParams["objectId"] = objectId;
            additionalParams["profileOwnerId"] = profileOwnerId;
            additionalParams["sectionAttribute"] = sectionAttribute;

            if (null != _g.Member && _g.Member.MemberID == memberId)
            {
                MemberLikeHelper.Instance.ProcessUserNotification(likeParams, objectId, (int)LikeObjectTypes.ProfileFreeText, _g, additionalParams);
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void ProcessPhotoLikeNotification(int brandId, int memberId, int questionId, int objectId, int profileOwnerId)
        {
            Brand brand = BrandConfigSA.Instance.GetBrandByID(brandId);
            MemberLikeParams likeParams = new MemberLikeParams();
            likeParams.SiteId = brand.Site.SiteID;
            likeParams.MemberId = memberId;
            Hashtable additionalParams = new Hashtable();
            additionalParams["questionId"] = questionId;
            additionalParams["objectId"] = objectId;
            additionalParams["profileOwnerId"] = profileOwnerId;

            if (null != _g.Member && _g.Member.MemberID == memberId)
            {
                MemberLikeHelper.Instance.ProcessUserNotification(likeParams, objectId, (int)LikeObjectTypes.Photo, _g, additionalParams);
            }
        }
    }

}
