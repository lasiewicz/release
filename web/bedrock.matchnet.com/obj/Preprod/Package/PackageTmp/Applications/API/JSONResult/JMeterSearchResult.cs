﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Matchnet.Web.Applications.API.JSONResult
{
    public class JMeterSearchResult : ResultBase
    {
        public List<Profile> ProfilesList;
    }

    public class Profile
    {
        public string ThumbnailURL { get; set; }
        public string Link { get; set; }
    }
}