﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.List.ServiceAdapters;
using Matchnet.List.ValueObjects;
using Matchnet.UserNotifications.ServiceAdapters;
using Matchnet.UserNotifications.ValueObjects;
using Matchnet.Web.Applications.UserNotifications;
using Matchnet.Web.Framework;

namespace Matchnet.Web.Applications.API
{
    public partial class ProfileBlocking : SessionDependentAPIBase
    {
        [Flags]
        public enum BlockType
        {
            Contact = 1,
            BlockFromSearch = 2,
            RemoveFromSearch = 4
        }

        private ContextGlobal _g;

        protected void Page_Load(object sender, EventArgs e)
        {

            try
            {
                _g = new ContextGlobal(Context);
                _g.IsAPI = true;
            }
            catch (Exception ex)
            { }
            
            // Parameters coming in blockMaskValue, tmemberID
            // Contact : 1, BlockSearch : 2, RemoveProfile: 4
            // Depending on the mask value coming in, make sure the update the appropriate lists (not just add but delete too)

            int blockMaskValue = Convert.ToInt16(Request["blockMaskValue"]);
            int targetMemberID = Convert.ToInt32(Request["tmemberID"]);

            int communityID = Brand.Site.Community.CommunityID;
            int siteID = Brand.Site.SiteID;

            AjaxResult result = new AjaxResult();
            result.ErrorMask = 0;
            result.MaskValue = 0;
            result.SuccessMessage = _g.GetResource("PROFILE_BLOCKING_ACTION_SUCCESS");
            result.ErrorMessages.Add(_g.GetResource("IGNORE_LIST_UPDATE_ERROR"));
            result.ErrorMessages.Add(_g.GetResource("BLOCK_FROM_SEARCH_LIST_LIMIT_EXCEEDED"));
            result.ErrorMessages.Add(_g.GetResource("REMOVE_FROM_SEARCH_LIST_LIMIT_EXCEEDED"));

            // **Block from Contact
            if ((blockMaskValue & (int)BlockType.Contact) == (int)BlockType.Contact)
            {
                // make sure the target member is on the member's ignorelist
                if (!ListSA.Instance.GetList(MemberID).IsHotListed(HotListCategory.IgnoreList, communityID, targetMemberID))
                {
                    ListSaveResult saveResult = ListSA.Instance.AddListMember(HotListCategory.IgnoreList, communityID, siteID, MemberID, targetMemberID,
                        string.Empty, Constants.NULL_INT, false, false);
                }

                result.MaskValue = result.MaskValue | (int)BlockType.Contact;
            }
            else
            {
                // make sure the target member is removed from the member's ignorelist
                ListSA.Instance.RemoveListMember(HotListCategory.IgnoreList, communityID, MemberID, targetMemberID);
            }

            // **Block from Search (searching me)
            if ((blockMaskValue & (int)BlockType.BlockFromSearch) == (int)BlockType.BlockFromSearch)
            {
                // make sure member's ExcludeFromList has target member on it
                if (ListSA.Instance.GetList(MemberID).IsHotListed(HotListCategory.ExcludeFromList, communityID, targetMemberID))
                {
                    result.MaskValue = result.MaskValue | (int)BlockType.BlockFromSearch;
                }
                else
                {
                    ListSaveResult saveResult = ListSA.Instance.AddListMember(HotListCategory.ExcludeFromList, communityID, siteID, MemberID, targetMemberID,
                        string.Empty, Constants.NULL_INT, false, false);

                    ///remove the "viewed" entry from the other member's hot list
                    ListSA.Instance.RemoveListMember(HotListCategory.WhoViewedYourProfile, communityID, targetMemberID, MemberID);

                    if (UserNotificationFactory.IsUserNotificationsEnabled(_g))
                    {
                        UserNotificationParams unParams = UserNotificationParams.GetParamsObject(targetMemberID, Brand.Site.SiteID, Brand.Site.Community.CommunityID);
                        UserNotification notification = UserNotificationFactory.Instance.GetUserNotification(new ViewedYourProfileUserNotification(), targetMemberID, MemberID, _g);
                        if (notification.IsActivated())
                        {
                            UserNotificationsServiceSA.Instance.RemoveUserNotifications(unParams, notification.CreateViewObject());
                        }
                    }


                    // check for exceeded max limit
                    if (saveResult.Status == ListActionStatus.ExceededMaxAllowed)
                    {
                        result.ErrorMask = result.ErrorMask | (int)BlockType.BlockFromSearch;
                    }
                    else
                    {
                        result.MaskValue = result.MaskValue | (int)BlockType.BlockFromSearch;
                    }
                }
            }
            else
            {
                // make sure target member is removed from member's ExcludeFromList
                ListSA.Instance.RemoveListMember(HotListCategory.ExcludeFromList, communityID, MemberID, targetMemberID);

                #region logic moved to ListSA
                //// check to see if the member is on the target member's ExcludeList
                //// if yes: target member does not want to see the member anyways. do nothing.
                //// if no: the member originally triggered the ExcludeListInternal add, so delete the member from the target member's ExcludeListInternal
                //if(!ListSA.Instance.GetList(targetMemberID).IsHotListed(HotListCategory.ExcludeList, communityID, MemberID))
                //{
                //    // make sure member is removed from target member's ExcludeListInternal
                //    ListSA.Instance.RemoveListMember(HotListCategory.ExcludeListInternal, communityID, targetMemberID, MemberID);
                //}
                #endregion
            }

            // **Remove from Search (from my search)
            if ((blockMaskValue & (int)BlockType.RemoveFromSearch) == (int)BlockType.RemoveFromSearch)
            {
                // make sure the target member is on the member's ExcludeList
                if (ListSA.Instance.GetList(MemberID).IsHotListed(HotListCategory.ExcludeList, communityID, targetMemberID))
                {
                    result.MaskValue = result.MaskValue | (int)BlockType.RemoveFromSearch;
                }
                else
                {
                    ListSaveResult saveResult = ListSA.Instance.AddListMember(HotListCategory.ExcludeList, communityID, siteID,
                        MemberID, targetMemberID, string.Empty, Constants.NULL_INT, false, false);

                    // check for exceeded max limit
                    if (saveResult.Status == ListActionStatus.ExceededMaxAllowed)
                    {
                        result.ErrorMask = result.ErrorMask | (int)BlockType.RemoveFromSearch;
                    }
                    else
                    {
                        result.MaskValue = result.MaskValue | (int)BlockType.RemoveFromSearch;
                    }
                }
            }
            else
            {
                // make sure the target member is removed from the member's ExcludeList
                ListSA.Instance.RemoveListMember(HotListCategory.ExcludeList, communityID, MemberID, targetMemberID);

                #region logic moved to ListSA
                //// check to see if the target member's ExcludeFromList contains the member
                //// if yes: target member does not want the member to see the target member, do nothing
                //// if no: member added the target member to his ExcludeListInternal, so remove
                //if (!ListSA.Instance.GetList(targetMemberID).IsHotListed(HotListCategory.ExcludeFromList, communityID, MemberID))
                //{
                //    // make sure the target member is removed from the member's ExcludeListInternal
                //    ListSA.Instance.RemoveListMember(HotListCategory.ExcludeListInternal, communityID, MemberID, targetMemberID);
                //}
                #endregion
            }

            Response.Write(Newtonsoft.Json.JsonConvert.SerializeObject(result));
        }
    }

    public class AjaxResult
    {
        private ArrayList _message = null;

        public int ErrorMask { get; set; }
        public int MaskValue { get; set; }
        public string SuccessMessage { get; set; }
        public List<string> ErrorMessages { get; set; }

        public AjaxResult()
        {
            SuccessMessage = "";
            ErrorMessages = new List<string>();
        }
    }
}
