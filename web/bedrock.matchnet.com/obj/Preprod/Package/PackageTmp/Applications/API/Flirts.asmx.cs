﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.Web.Script.Serialization;
using Matchnet.Email.ServiceAdapters;
using Matchnet.Email.ValueObjects;
using Matchnet.Web.Framework;

using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Web.Applications.Email;
using Matchnet.Web.Framework.Managers;

using System.Text;
using System.IO;
using System.Web.UI;
using Matchnet.Web.Applications.API.JSONResult;


namespace Matchnet.Web.Applications.API
{
    /// <summary>
    /// Summary description for Notifications
    /// </summary>
    [WebService(Namespace = "http://spark.net/API")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    [ScriptService]
    public class FlirtsService : WebService
    {

        private Int32 _communityID;
        private Brand _brand;
        private ContextGlobal _g;

        public FlirtsService()
		{
            if (null == _g)
            {
                if (Context != null)
                {
                    if (Context.Items["g"] != null)
                    {
                        _g = (ContextGlobal)Context.Items["g"];

                    }
                    else
                    {
                        _g = new ContextGlobal(Context);
                    }
                    _g.SaveSession = false;
                }
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public int SendFlirt(int sendingMemberId, int targetMemberId, string categoryKey, string teaseKey)
        {
            if (_g != null && _g.Member != null)
            {
                var teaseCategories = TeaseSA.Instance.GetTeaseCategoryCollection(_g.Brand.Site.SiteID);
                var category = teaseCategories.Cast<TeaseCategory>().FirstOrDefault(c => c.ResourceKey == categoryKey);

                if (category == null)
                {
                    return Convert.ToInt32(FlirtOperationResult.OperationFailed);
                }

                var teases = TeaseSA.Instance.GetTeaseCollection(category.TeaseCategoryID, _g.Brand.Site.SiteID);

                var tease = teases.Cast<Matchnet.Email.ValueObjects.Tease>().FirstOrDefault(t => t.ResourceKey == teaseKey);

                if (tease == null)
                {
                    return Convert.ToInt32(FlirtOperationResult.OperationFailed);
                }

                var flirtManager = new FlirtManager(_g);
                int canSendFlirt = flirtManager.CanFlirtBeSent(sendingMemberId, targetMemberId, _g.Brand);
                if (canSendFlirt != 1)
                {
                    return canSendFlirt;
                }

                var resourceManager = new ResourceManager(_g.Brand);
                var teaseContentText = resourceManager.GetResource(_g.Brand, tease.ResourceKey);
                var teaseHeaderText = resourceManager.GetResource(_g.Brand, "TXT_TEASE");
                var teasesLeft = 0;

                var flirtResult = flirtManager.SendFlirt(category.TeaseCategoryID, tease.TeaseID, teaseHeaderText, teaseContentText,
                                       targetMemberId, out teasesLeft);

                return Convert.ToInt32(flirtResult);
            }
            else
            {
                return Convert.ToInt32(FlirtOperationResult.SessionExpired);
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string ProcessFlirtOperation(Int32 memberID, string Radio_TeaseItem)
        {
            int selectedCategoryID = Convert.ToInt32(Radio_TeaseItem.Substring(0, Radio_TeaseItem.IndexOf("_")));
            int selectedTeaseID = Convert.ToInt32(Radio_TeaseItem.Substring(Radio_TeaseItem.IndexOf("_") + 1));

            System.Web.UI.Page page = new System.Web.UI.Page();

            FlirtCollapsiblePopup ctrl = page.LoadControl("/Applications/Email/FlirtCollapsiblePopup.ascx") as FlirtCollapsiblePopup;

            FlirtOperationResult flirtResult;
            flirtResult = ctrl.sendTease(selectedCategoryID, selectedTeaseID, memberID);

            ctrl.setResultResource(flirtResult);
            page.Controls.Add(ctrl);
            _g.ResetSearchStartRow = true;
            ctrl.LoadSuggestedProfiles();
            StringWriter writer = new StringWriter();
            HttpContext.Current.Server.Execute(page, writer, false);
            string s = writer.ToString();
            return s;

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string ProcessPushFlirts(string memberIDs)
        {
            FlirtManager flirtManager = new FlirtManager(_g);
            bool success = true;
            string[] memberIDsArray = memberIDs.Split(',');
            foreach(string memberID in memberIDsArray)
            {
                FlirtOperationResult result = flirtManager.SendPushFlirt(Convert.ToInt32(memberID));
                if(result != FlirtOperationResult.FlirtSent)
                {
                    success = false;
                    break;
                }
            }

            if(success)
            {
                return GetPushFlirtsConfirmation(PushFlirtConfirmationMode.Successfull);
            }
            else
            {
                return GetPushFlirtsConfirmation(PushFlirtConfirmationMode.QuotaReached);
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string GetPushFlirts()
        {
            FlirtManager flirtManager = new FlirtManager(_g);
            if (flirtManager.EnoughResultsToShowPushFlirtsToMember())
            {
                System.Web.UI.Page page = new System.Web.UI.Page();

                PushFlirtsPopup ctrl = page.LoadControl("/Applications/Email/PushFlirtsPopup.ascx") as PushFlirtsPopup;
                page.Controls.Add(ctrl);

                StringWriter writer = new StringWriter();
                HttpContext.Current.Server.Execute(page, writer, false);
                string s = writer.ToString();
                return s;
            }
            else
            {
                return GetPushFlirtsConfirmation(PushFlirtConfirmationMode.NotEnoughResults);
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string GetPushFlirtsConfirmation(PushFlirtConfirmationMode mode)
        {
            System.Web.UI.Page page = new System.Web.UI.Page();

            PushFlirtsPopupConfirmation ctrl = page.LoadControl("/Applications/Email/PushFlirtsPopupConfirmation.ascx") as PushFlirtsPopupConfirmation;

            switch(mode)
            {
                case PushFlirtConfirmationMode.Successfull:
                    ctrl.ShowConfirmationSuccess();
                    break;
                case PushFlirtConfirmationMode.QuotaReached:
                    ctrl.ShowQuotaReached();
                    break;
                case PushFlirtConfirmationMode.NotEnoughResults:
                    ctrl.ShowNotEnoughResults();
                    break;
            }
            
            page.Controls.Add(ctrl);

            StringWriter writer = new StringWriter();
            HttpContext.Current.Server.Execute(page, writer, false);
            string s = writer.ToString();
            return s;
        }

        /// <summary>
        /// This is used to send a no photo request message; does not require subscription; similar to flirt
        /// </summary>
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public UpdateResult SendNoPhotoRequestMessageAsFlirt(int targetMemberID)
        {
            UpdateResult updateResult = new UpdateResult();
            try
            {
                if (_g != null && _g.Member != null)
                {
                    if (targetMemberID > 0)
                    {
                        FlirtManager flirtManager = new FlirtManager(_g);
                        FlirtOperationResult operationResult = flirtManager.SendNoPhotoRequestMessageAsFlirt(targetMemberID);
                        updateResult.Status = (int)operationResult;
                    }
                    else
                    {
                        throw new Exception("Missing data for sending message");
                    }
                }
                else
                {
                    //session expired
                    updateResult.Status = (int)JSONStatus.SessionExpired;
                }
            }
            catch (Exception ex)
            {
                _g.ProcessException(ex);
                updateResult.Status = (int)FlirtOperationResult.OperationFailed;
                updateResult.StatusMessage = ex.Message;
            }

            return updateResult;
        }
    }
}
