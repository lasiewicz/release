﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Matchnet.Web.Applications.API.JSONResult
{
    [Serializable]
    public class GetFBLikesResult : ResultBase
    {
        public int MemberID { get; set; }
        public List<Spark.FacebookLike.ValueObjects.FacebookLike> FacebookLikes { get; set; }
    }
}