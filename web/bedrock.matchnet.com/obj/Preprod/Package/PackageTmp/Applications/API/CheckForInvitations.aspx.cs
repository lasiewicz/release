using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using Matchnet.InstantMessenger.ServiceAdapters;
using Matchnet.InstantMessenger.ValueObjects;


namespace Matchnet.Web.Applications.API
{
	/// <summary>
	/// Summary description for CheckForInvitations.
	/// </summary>
	public class CheckForInvitations : SessionDependentAPIBase
	{
		protected System.Web.UI.WebControls.Literal litLoadGetInvitations;
		protected System.Web.UI.WebControls.Literal litNoInvitationsFound;
		protected System.Web.UI.WebControls.Literal litNotLoggedIn;
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			
			//get a list of current Instant Messenger invitations. Returns javascript
			//function name to be evaluated by script (which will then call IFRAME checkIM)
			bool foundInvitations = false;
			try 
			{
				if (MemberID > 0 && Brand != null) 
				{
					ConversationInvites invites = InstantMessengerSA.Instance.GetInvites(MemberID ,Brand.Site.Community.CommunityID);
					foundInvitations =  ((invites != null) && (invites.Count > 0));	
					this.litLoadGetInvitations.Visible = foundInvitations;
					this.litNoInvitationsFound.Visible = !foundInvitations;

				}
				else 
				{
					//return indication that user is not logged in
					this.litNotLoggedIn.Visible = true;

				}
			}
			catch
			{

			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
	}
}
