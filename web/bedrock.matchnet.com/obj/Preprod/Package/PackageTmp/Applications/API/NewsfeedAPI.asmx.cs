﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Web.Applications.API.JSONResult;
using Matchnet.Web.Applications.UserNotifications.Controls;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Ui.BasicElements;
using Matchnet.Web.Applications.UserNotifications;

namespace Matchnet.Web.Applications.API
{
    /// <summary>
    /// Summary description for NewsfeedAPI
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    [ScriptService]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class NewsfeedAPI : System.Web.Services.WebService
    {

        private Brand _brand;
        private ContextGlobal _g;

        public NewsfeedAPI()
		{
            if (null == _g)
            {
                if (Context != null)
                {
                    if (Context.Items["g"] != null)
                    {
                        _g = (ContextGlobal)Context.Items["g"];
                    }
                    else
                    {
                        _g = new ContextGlobal(Context);
                    }
                    _g.SaveSession = false;
                }
            }
        }


        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public NewsfeedResult LoadNewsfeed(int startNum, int pageSize)
        {
            var result = new NewsfeedResult();

            try
            {
                result.Status = (int)JSONStatus.Success;
                var page = new System.Web.UI.Page();
                var newsFeedControl = page.LoadControl("/Applications/UserNotifications/Controls/Newsfeed.ascx") as Newsfeed;
                newsFeedControl.LoadNewsfeed(startNum, pageSize, false);
            
                if (newsFeedControl.NumberOfLoadedNotifications > 0)
                {
                    result.HasItems = true;
                    var form = new System.Web.UI.HtmlControls.HtmlForm();
                    form.Controls.Add(newsFeedControl);
                    page.Controls.Add(form);

                    var writer = new StringWriter();
                    HttpContext.Current.Server.Execute(page, writer, false);
                    result.NewsfeedHTML = writer.ToString();
                }
            }
            catch (Exception ex)
            {
                result.Status = (int)JSONStatus.Failed;
                _g.ProcessException(ex);
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public UpdateResult MarkNewsfeedAsViewed(string clearDate)
        {
            var result = new UpdateResult();
            try
            {
                result.Status = (int)JSONStatus.Success;
                if (_g != null && _g.Member != null && _g.Member.MemberID > 0)
                {
                    DateTime viewedDate = Convert.ToDateTime(clearDate);
                    UserNotificationManager userNotificationManager = new UserNotificationManager();
                    userNotificationManager.MarkNewsfeedAsViewed(_g.Member.MemberID, _g.Brand);
                }

            }
            catch (Exception ex)
            {
                result.Status = (int)JSONStatus.Failed;
                _g.ProcessException(ex);
            }

            return result;
        }
    }
}
