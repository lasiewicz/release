﻿namespace Matchnet.Web.Applications.API.JSONResult
{
    public class PackagePricingDetails : ResultBase
    {
        public string PackageOverrideTotalAmount { get; set; }
    }
}
