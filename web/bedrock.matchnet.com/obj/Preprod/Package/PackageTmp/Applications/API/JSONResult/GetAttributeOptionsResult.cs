﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.Web.Framework.Util;

namespace Matchnet.Web.Applications.API.JSONResult
{
    public class GetAttributeOptionsResult : ResultBase
    {
        public List<OptionItem> OptionItems { get; set; }

        public GetAttributeOptionsResult()
        {
            OptionItems = new List<OptionItem>();
        }
    }
}
