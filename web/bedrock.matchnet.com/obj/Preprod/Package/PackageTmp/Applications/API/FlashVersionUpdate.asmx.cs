﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.Web.Script.Serialization;

using Matchnet;
using Matchnet.Web.Framework;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Content.ValueObjects.Admin;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects;
using Matchnet.Member.ValueObjects.Privilege;

namespace Matchnet.Web.Applications.API
{
    /// <summary>
    /// Summary description for FlashVersionUpdate
    /// </summary>
    [WebService(Namespace = "http://spark.net/API")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    [ScriptService]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class FlashVersionUpdate : WebService
    {
        private ContextGlobal _g;

        public FlashVersionUpdate()
		{
			if (null == _g)
            {
                if (Context != null)
                {
                    if (Context.Items["g"] != null)
                    {
                        _g = (ContextGlobal)Context.Items["g"];
                    }
                    else
                    {
                        _g = new ContextGlobal(Context);
                    }
                    if (_g != null)
                        _g.SaveSession = false;
                }
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void SetFlashVersion(string version)
        {
            if (_g != null && _g.Member != null && _g.Member.MemberID > 0)
            {
                _g.Member.SetAttributeText(_g.Brand, "UserFlashVersion", version, TextStatusType.Auto);
                MemberSA.Instance.SaveMember(_g.Member);
            }
        }
    }
}
