﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using Matchnet.Web.Framework;
using System.Web.Script.Services;
using Matchnet.Web.Applications.API.JSONResult;

namespace Matchnet.Web.Applications.API
{
    /// <summary>
    /// Summary description for AttributeService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class AttributeService : System.Web.Services.WebService
    {
        private ContextGlobal _g;

        public AttributeService()
		{
            if (null == _g)
            {
                if (Context != null)
                {
                    if (Context.Items["g"] != null)
                    {
                        _g = (ContextGlobal)Context.Items["g"];
                    }
                    else
                    {
                        _g = new ContextGlobal(Context);
                    }
                    if (_g != null)
                        _g.SaveSession = false;
                }
            }
		}

        /// <summary>
        /// This is used to retrieve options for a search preference
        /// </summary>
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public GetAttributeOptionsResult GetSearchPreferenceOptions(string prefName)
        {
            GetAttributeOptionsResult result = new GetAttributeOptionsResult();
            try
            {
                if (_g != null)
                {
                    //get control strictly for resources
                    System.Web.UI.Page page = new System.Web.UI.Page();
                    System.Web.UI.Control ResourceControl = page.LoadControl("/Framework/Ui/ProfileElements/ProfileElementsAPIResource.ascx");

                    //get contact history items
                    result.Status = (int)JSONStatus.Success;
                }
                else
                {
                    throw new Exception("Missing data error");
                }

            }
            catch (Exception ex)
            {
                _g.ProcessException(ex);
                result.Status = (int)JSONStatus.Failed;
                result.StatusMessage = ex.Message;
            }

            return result;
        }

    }
}
