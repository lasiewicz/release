using System;
using System.Collections;
using System.Data;
using System.Web.UI.WebControls;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Web.Framework;

using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.List.ServiceAdapters;
using Matchnet.List.ValueObjects;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects.Photos;
using Matchnet.Web.Framework.Managers;

namespace Matchnet.Web.Applications.API
{
	/// <summary>
	/// Summary description for EmailAPI.
	/// </summary>
	public class FlashListAPI : System.Web.UI.Page
	{
		protected Repeater xmlRepeater;

		private Int32 _communityID;
		private Brand _brand;

		private void Page_Load(object sender, System.EventArgs e)
		{
			Int32 memberID = Conversion.CInt(Request["MemberID"]);
			Int32 brandID = Conversion.CInt(Request["BrandID"]);

			_brand = BrandConfigSA.Instance.GetBrandByID(brandID);

			_communityID = _brand.Site.Community.CommunityID;

			Matchnet.List.ServiceAdapters.List list = ListSA.Instance.GetList(memberID);
			Int32 temp = 0;

			DataTable actions = new DataTable();
			actions.Columns.Add("Time", typeof(DateTime));
			actions.Columns.Add("Action", typeof(string));
			actions.Columns.Add("TargetMemberID", typeof(Int32));

			foreach (Int32 categoryID in Enum.GetValues(typeof(HotListCategory)))
			{
				HotListCategory category = (HotListCategory) Enum.Parse(typeof(HotListCategory), categoryID.ToString());

				foreach (Int32 targetMemberID in list.GetListMembers(category, _brand.Site.Community.CommunityID, _brand.Site.SiteID, 1, 10, out temp))
				{
					ListItemDetail detail = list.GetListItemDetail(category, _brand.Site.Community.CommunityID, _brand.Site.SiteID, targetMemberID);

					DataRow row = actions.NewRow();
					row["Time"] = detail.ActionDate;
					row["Action"] = category.ToString();
					row["TargetMemberID"] = targetMemberID;
					actions.Rows.Add(row);
				}
			}

			DataView dv = new DataView(actions);
			dv.Sort = "Time desc";

			xmlRepeater.DataSource = dv;
			xmlRepeater.DataBind();
		}

		private void xmlRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{
				Literal litUsername = (Literal) e.Item.FindControl("litUsername");
				Literal litAction = (Literal) e.Item.FindControl("litAction");
				Literal litInsertDate = (Literal) e.Item.FindControl("litInsertDate");
				Literal litUrl = (Literal) e.Item.FindControl("litUrl");
				Literal litThumbUrl = (Literal) e.Item.FindControl("litThumbUrl");
				Literal litRegion = (Literal) e.Item.FindControl("litRegion");
				Literal litGender = (Literal) e.Item.FindControl("litGender");

				DataRow row = (DataRow) ((DataRowView) e.Item.DataItem).Row;

				Int32 targetMemberID = Conversion.CInt(row["TargetMemberID"]);
				Matchnet.Member.ServiceAdapters.Member member = MemberSA.Instance.GetMember(targetMemberID, MemberLoadFlags.None);

				litUsername.Text = member.GetUserName(_brand);
				litAction.Text = row["Action"].ToString();
				litInsertDate.Text = row["Time"].ToString();
				litUrl.Text = "";
			
				//refactored: gets thumbnail photo, or else defaults to "no photo" or private photo

                //MemberPhotoHelper photoHelper = new MemberPhotoHelper(RuntimeSettings.Instance);
                Photo photo = MemberPhotoDisplayManager.Instance.GetDefaultPhoto(member, member, _brand);
                litThumbUrl.Text = MemberPhotoDisplayManager.Instance.GetPhotoDisplayURL(member, member, _brand, photo,
                                                                        PhotoType.Thumbnail,
                                                                        PrivatePhotoImageType.Thumb,
                                                                        NoPhotoImageType.Thumb);
				
				litRegion.Text = FrameworkGlobals.GetRegionString(member.GetAttributeInt(_communityID, Constants.NULL_INT, Constants.NULL_INT, WebConstants.ATTRIBUTE_NAME_REGIONID, Constants.NULL_INT),
					(Int32) Matchnet.Language.English, false, true, true);

				litGender.Text = member.GetAttributeInt(_communityID, Constants.NULL_INT, Constants.NULL_INT, 69, Constants.NULL_INT).ToString();
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
			xmlRepeater.ItemDataBound += new RepeaterItemEventHandler(xmlRepeater_ItemDataBound);
		}
		#endregion
	}
}
