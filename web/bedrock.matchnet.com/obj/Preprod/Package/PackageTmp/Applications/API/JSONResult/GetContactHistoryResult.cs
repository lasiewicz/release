﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.Web.Framework.Ui.ProfileElements;

namespace Matchnet.Web.Applications.API.JSONResult
{
    /// <summary>
    /// Result class to encapsulate result data due to an API to get Contact History
    /// </summary>
    [Serializable]
    public class GetContactHistoryResult : ResultBase
    {
        public int MemberID { get; set; }
        public int TargetMemberID { get; set; }
        public List<ContactHistoryItem> HistoryItems { get; set; }

        public GetContactHistoryResult()
        {
            HistoryItems = new List<ContactHistoryItem>();
        }
    }
}
