using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using Matchnet.InstantMessenger.ServiceAdapters;
using Matchnet.InstantMessenger.ValueObjects;

namespace Matchnet.Web.Applications.API
{
	/// <summary>
	/// Summary description for RemoveInvitation.
	/// </summary>
	public class RemoveInvitation : SessionDependentAPIBase
	{
		private const string PARAM_REJECT_INVITATION = "IMReject"; // optional: true or false whether to reject an invitation request or not
		private const string PARAM_CONVERSATION_INVITATION_KEY =  "ConversationInvitationKey"; //optional: conversation invitation key of accepted or rejected conversation (required if IMReject is true)
		private string _conversationInvitationKey; // variable to hold optional conversation invitation key

		private void Page_Load(object sender, System.EventArgs e)
		{
			
			//Removes any Rejected or Accepted conversation invitations that may have been (optionally) passed in here.
			//It's important to do this right away and not wait for flash movie to load and for Userplane to call startIC XML request
			//because that might take too long.
			if (Request.QueryString[PARAM_CONVERSATION_INVITATION_KEY] != null && Request.QueryString[PARAM_CONVERSATION_INVITATION_KEY] !=string.Empty) 
			{
				_conversationInvitationKey = Request.QueryString[PARAM_CONVERSATION_INVITATION_KEY];
				removeInvitation();
			}

			//TODO: send a rejection message to the sender if their invitation was rejected
			if (Request.QueryString[PARAM_REJECT_INVITATION] != null) 
			{
				bool rejectInvitation = Convert.ToBoolean(Request.QueryString[PARAM_REJECT_INVITATION]);
				if (rejectInvitation) 
				{
					sendRejectionNotification();
				}
			}
		}

		/// <summary>
		/// Removes invitation from the invitation collection so that it won't bother user again. This applies to both
		/// conversation invitations accepted and rejected! They both need to be removed.
		/// </summary>
		private void removeInvitation()	
		{		
			// Decline the IM invite, continue processing future notifications.
			ConversationInvite invite = new ConversationInvite();
			invite.ConversationKey = Matchnet.InstantMessenger.ValueObjects.ConversationKey.New(_conversationInvitationKey);
			invite.CommunityID = Brand.Site.Community.CommunityID;
			invite.RecipientID = MemberID;
			InstantMessengerSA.Instance.DeclineInvitation(invite);
		}

		/// <summary>
		/// Not Implemented Yet
		/// Todo: add message back to sending IM user that invitation was rejected: 15639 awaiting Userplane functionality
		/// </summary>
		private void sendRejectionNotification() 
		{	
			//TODO: do something here to pass a message to the sender: 15639 awaiting Userplane functionality
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
