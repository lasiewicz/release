using System;
using System.Collections;
using System.Web.UI.WebControls;

using Matchnet.Email.ServiceAdapters;
using Matchnet.Email.ValueObjects;

namespace Matchnet.Web.Applications.API
{
	/// <summary>
	/// Summary description for EmailAPI.
	/// </summary>
	public class FolderAPI : System.Web.UI.Page
	{
		protected Repeater xmlRepeater;

		private void Page_Load(object sender, System.EventArgs e)
		{
			Int32 memberID = Conversion.CInt(Request["MemberID"]);
			Int32 communityID = Conversion.CInt(Request["CommunityID"]);

			ICollection folders = EmailFolderSA.Instance.RetrieveFolders(memberID, communityID, false);

			xmlRepeater.DataSource = folders;
			xmlRepeater.DataBind();
		}

		private void xmlRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{
				Literal litID = (Literal) e.Item.FindControl("litID");
				Literal litDescription = (Literal) e.Item.FindControl("litDescription");

				EmailFolder folder = (EmailFolder) e.Item.DataItem;

				litID.Text = folder.MemberFolderID.ToString();
				litDescription.Text = folder.Description;
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
			xmlRepeater.ItemDataBound += new RepeaterItemEventHandler(xmlRepeater_ItemDataBound);
		}
		#endregion
	}
}
