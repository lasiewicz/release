﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.MatchTest.ServiceAdapters;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ServiceAdapters.Interfaces;
using Matchnet.Member.ValueObjects.Interfaces;
using Matchnet.Member.ValueObjects.Photos;
using Matchnet.MembersOnline.ServiceAdapters;
using Matchnet.MembersOnline.ValueObjects;
using Matchnet.Web.Applications.CompatibilityMeter;
using Matchnet.Web.Applications.MembersOnline.Controls;
using Matchnet.Web.Framework;
using Matchnet.Web.Applications.API.JSONResult;
using Matchnet.Web.Applications.Search;
using System.IO;
using Matchnet.Web.Framework.Managers;
using Matchnet.Web.Framework.Ui;
using Matchnet.Web.Applications.CompatibilityMeter.Controls;
using Matchnet.PhotoSearch.ValueObjects;
using Matchnet.Web.Applications.Home;

namespace Matchnet.Web.Applications.API
{
    /// <summary>
    /// Related apis for search results
    /// </summary>
    [WebService(Namespace = "http://spark.net/API")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    [System.Web.Script.Services.ScriptService]
    public class SearchResults1 : System.Web.Services.WebService
    {
        private ContextGlobal _g;

        public SearchResults1()
        {
            if (null == _g)
            {
                if (Context != null)
                {
                    if (Context.Items["g"] != null)
                    {
                        _g = (ContextGlobal)Context.Items["g"];
                    }
                    else
                    {
                        _g = new ContextGlobal(Context);
                    }
                    if (_g != null)
                        _g.SaveSession = false;
                }
            }

        }

        #region Search Page related
        /// <summary>
        /// This is used to retrieve updated results for SearchResults page due to Registration Overlay
        /// </summary>
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public GetSearchResult GetRegOverlaySearchResults()
        {
            GetSearchResult result = new GetSearchResult();
            try
            {
                if (_g != null)
                {
                    System.Web.UI.Page page = new System.Web.UI.Page();
                    if (BetaHelper.IsSearchRedesign30Enabled(_g))
                    {
                        SearchResults30 ctl = (SearchResults30)page.LoadControl("/Applications/Search/SearchResults30.ascx");
                        ctl.IsPartialRender = true;
                        ctl.IsRegOverlayRefresh = true;
                        System.Web.UI.HtmlControls.HtmlForm Form1 = new System.Web.UI.HtmlControls.HtmlForm();
                        Form1.Controls.Add(ctl);
                        page.Controls.Add(Form1);
                        result.IsSearchResultNested = true;
                    }
                    else
                    {
                        SearchResults20 ctl = (SearchResults20)page.LoadControl("/Applications/Search/SearchResults20.ascx");
                        ctl.IsPartialRender = true;
                        ctl.IsRegOverlayRefresh = true;
                        System.Web.UI.HtmlControls.HtmlForm Form1 = new System.Web.UI.HtmlControls.HtmlForm();
                        Form1.Controls.Add(ctl);
                        page.Controls.Add(Form1);
                        result.IsSearchResultNested = true;
                    }

                    StringWriter writer = new StringWriter();
                    HttpContext.Current.Server.Execute(page, writer, false);
                    result.SearchResultHTML = writer.ToString();
                    result.Status = (int)JSONStatus.Success;
                }
                else
                {
                    throw new Exception("Missing data error");
                }

            }
            catch (Exception ex)
            {
                _g.ProcessException(ex);
                result.Status = (int)JSONStatus.Failed;
                result.StatusMessage = ex.Message;
            }

            return result;
        }

        #endregion

        #region JMeter Match related
        /// <summary>
        /// This is used to retrieve JMeter matches results
        /// </summary>
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public JMeterSearchResult GetJMeterMatches(int startRow, int pageSize)
        {
            var response = new JMeterSearchResult();

            try
            {
                // Get JMeter matches preferences
                System.Web.UI.Page page = new System.Web.UI.Page();
                MatchesFilter30 mf = (MatchesFilter30)page.LoadControl("/Applications/CompatibilityMeter/Controls/MatchesFilter30.ascx");
                System.Web.UI.HtmlControls.HtmlForm Form1 = new System.Web.UI.HtmlControls.HtmlForm();
                Form1.Controls.Add(mf);
                page.Controls.Add(Form1);
                StringWriter sw = new StringWriter();
                HttpContext.Current.Server.Execute(page, sw, false);

                // Get Jmeter matches from API

                var bmresults = MatchMeterSA.Instance.GetBestMatches(_g.Member.MemberID, _g.Brand, mf.intRegionID, mf.intAgeMin, mf.intAgeMax, mf.GetGenderMask(), MemberPrivilegeAttr.IsCureentSubscribedMember(_g), startRow - 1, pageSize, mf.PhotoRequired);

                if (bmresults != null && bmresults.MatchesCount > 0)
                {
                    Dictionary<IMemberDTO, decimal> matchResultsMemberScoreDictionary = bmresults.MatchResultsDic;
                    ArrayList members = new ArrayList(matchResultsMemberScoreDictionary.Keys);
                    //totalRows = bmresults.MatchesCount;

                    response.ProfilesList = new List<Profile>(pageSize);
                    CompatibilityMeterHandler compatibilityMeterHandler = new CompatibilityMeterHandler(_g);
                    bool isPremiumUser = compatibilityMeterHandler.IsJmeterPremium();
                    for (int i = 0; i < pageSize && i < members.Count; i++)
                    {
                        Matchnet.Member.ServiceAdapters.Member member = (Matchnet.Member.ServiceAdapters.Member)members[i];

                        Photo photo = MemberPhotoDisplayManager.Instance.GetDefaultPhoto(_g.Member, member, _g.Brand);
                        string thumbPath = MemberPhotoDisplayManager.Instance.GetPhotoDisplayURL(_g.Member, member, _g.Brand, photo,
                                                                                PhotoType.Thumbnail,
                                                                                PrivatePhotoImageType.Thumb,
                                                                                NoPhotoImageType.Thumb);
                        string link = "/Applications/CompatibilityMeter/OneOnOne.aspx?MemberID=" + member.MemberID;
                        if (!isPremiumUser)
                        {
                            link = FrameworkGlobals.GetSubscriptionLink(false, 1410);
                        }
                        response.ProfilesList.Add(new Profile() { ThumbnailURL = thumbPath, Link = link });

                    }
                    response.Status = (int)JSONStatus.Success;
                    response.StatusMessage = "Success";
                }
                else
                {
                    response.Status = (int)JSONStatus.Failed;
                    response.StatusMessage = "No Results";
                }

            }
            catch (Exception ex)
            {
                _g.ProcessException(ex);
                response.Status = (int)JSONStatus.Failed;
                response.StatusMessage = "Error";
            }

            return response;
        }

        /// <summary>
        /// This is used to retrieve MOL for JMeter sidebar
        /// </summary>
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public JMeterSearchResult GetJMeterMOL(int startRow, int pageSize)
        {
            var response = new JMeterSearchResult();

            try
            {
                SortFieldType sortField = SortFieldType.HasPhoto;
                SortDirectionType sortDirection = SortDirectionType.Desc;

                System.Web.UI.Page page = new System.Web.UI.Page();
                MembersOnlineFilter20 molf = (MembersOnlineFilter20)page.LoadControl("/Applications/MembersOnline/Controls/MembersOnlineFilter20.ascx");
                System.Web.UI.HtmlControls.HtmlForm Form1 = new System.Web.UI.HtmlControls.HtmlForm();
                Form1.Controls.Add(molf);
                page.Controls.Add(Form1);
                StringWriter sw = new StringWriter();
                HttpContext.Current.Server.Execute(page, sw, false);


                var genderMask = molf.GetGenderMask();
                var regionID = molf.intRegionID;
                var ageMin = molf.intAgeMin;
                var ageMax = molf.intAgeMax;
                var languageMask = molf.intLanguageMask;

                var collection = new MOCollection(SortFieldType.HasPhoto.ToString(), genderMask, regionID, ageMin, ageMax, languageMask);

                Int16 genderMaskShort = 0;

                if (collection.GenderMask != Constants.NULL_INT)
                {
                    genderMaskShort = (Int16)collection.GenderMask;
                }

                Matchnet.MembersOnline.ValueObjects.MOLQueryResult MOLResults =
                  MembersOnlineSA.Instance.GetMemberIDs(_g.Session.Key.ToString(),
                                                        _g.Brand.Site.Community.CommunityID,
                                                        genderMaskShort,
                                                        collection.RegionID,
                                                        (Int16)collection.AgeMin,
                                                        (Int16)((Int16)collection.AgeMax + 1),
                                                        collection.LanguageMask,
                                                        sortField,
                                                        sortDirection,
                                                        startRow - 1,
                                                        pageSize,
                                                        _g.Member == null ? Constants.NULL_INT : _g.Member.MemberID);


                if (MOLResults.MembersReturned > 0)
                {
                    response.ProfilesList = new List<Profile>(pageSize);
                    ArrayList members = MemberSA.Instance.GetMembers(MOLResults.ToArrayList(), MemberLoadFlags.None);
                    CompatibilityMeterHandler compatibilityMeterHandler = new CompatibilityMeterHandler(_g);
                    bool isPremiumUser = compatibilityMeterHandler.IsJmeterPremium();

                    foreach (Matchnet.Member.ServiceAdapters.Member member in members)
                    {
                        
                        Photo photo = MemberPhotoDisplayManager.Instance.GetDefaultPhoto(_g.Member, member, _g.Brand);

                        string thumbPath = MemberPhotoDisplayManager.Instance.GetPhotoDisplayURL(_g.Member, member, _g.Brand, photo,
                                                                                 PhotoType.Thumbnail,
                                                                                 PrivatePhotoImageType.Thumb,
                                                                                 NoPhotoImageType.Thumb);

                        string link = "/Applications/CompatibilityMeter/OneOnOne.aspx?MemberID=" + member.MemberID;
                        if ((compatibilityMeterHandler.CheckJMeterFlag(WebConstants.MatchMeterFlags.PremiumFor1On1) && !isPremiumUser) || (compatibilityMeterHandler.CheckJMeterFlag(WebConstants.MatchMeterFlags.PayFor1On1) && !compatibilityMeterHandler.IsPayingMember()))
                        {
                            link = FrameworkGlobals.GetSubscriptionLink(false, 1411);
                        }
                        else if (!compatibilityMeterHandler.IsMemeberHasFeedback(member))
                        {
                            link = "/Applications/MemberProfile/ViewProfile.aspx?ProfileTab=JMeter&MemberID=" + member.MemberID;
                        }
                        response.ProfilesList.Add(new Profile() { ThumbnailURL = thumbPath, Link = link });
                    }

                    response.Status = (int)JSONStatus.Success;
                    response.StatusMessage = "Success";
                }
                else
                {
                    response.Status = (int)JSONStatus.Failed;
                    response.StatusMessage = "No Results";
                }

            }
            catch (Exception ex)
            {
                _g.ProcessException(ex);
                response.Status = (int)JSONStatus.Failed;
                response.StatusMessage = "Error";
            }

            return response;
        }

        #endregion

        #region Photo Gallery Search related
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public PhotoGalleryResult UpdateFiltersAndGetPhotoGalleryBatch(string gender, string postedDayRange, string regionId, string minAge, string maxAge, int pageSize, string sortBy)
        {
            var response = new PhotoGalleryResult();

            var validFilters = PhotoGalleryManager.Instance.ValidateFilterOptions(gender, postedDayRange, regionId, minAge, maxAge);
            if (!validFilters)
            {
                response.Status = (int)JSONStatus.Failed;
                response.StatusMessage = "Filters Invalid";
                return response;
            }

            int sortByInt;
            if (!int.TryParse(sortBy, out sortByInt))
            {
                sortByInt = (int)SortByType.random;
            }
            PhotoGalleryManager.Instance.SetFilterOptionsAndSaveToSession(_g.Brand, _g.Member, int.Parse(gender), int.Parse(postedDayRange), int.Parse(regionId), int.Parse(minAge), int.Parse(maxAge), sortByInt);
            
            response = GetPhotoGalleryBatch(1, pageSize);

            return response;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public PhotoGalleryResult GetPhotoGalleryBatch(int startRow, int pageSize)
        {
            PhotoGalleryResult response = new PhotoGalleryResult();

            try
            {
                if (_g != null)
                {
                    var filterExpirationDays = PhotoGalleryManager.Instance.GetFilterPersistenceExpirationDays(_g.Brand);
                    
                    response.PhotoGalleryMemberList = new List<PhotoSearch.ValueObjects.PhotoGalleryMember>();
                    response.MemberID = (_g.Member != null) ? _g.Member.MemberID : 0;
                    response.TotalResultCount = 0;
                    PhotoGalleryQuery query = Applications.PhotoGallery.PhotoGalleryUtils.GetQueryFromSession(_g.Brand, _g.Member == null ? Constants.NULL_INT : _g.Member.MemberID, filterExpirationDays);
                    System.Web.UI.Page pResource = new System.Web.UI.Page();
                    FrameworkControl InternalResourceControl = pResource.LoadControl("/Applications/PhotoGallery/PhotoGalleryResource.ascx") as FrameworkControl;
                    PhotoGalleryResultList searchResults = PhotoGalleryManager.Instance.SearchPhotoGallery(_g.Brand, response.MemberID, startRow, pageSize, query, InternalResourceControl);
                    if (searchResults != null && searchResults.PhotoGalleryResults != null)
                    {
                        response.TotalResultCount = searchResults.MatchesFound;
                        foreach (PhotoResultItem pri in searchResults.PhotoGalleryResults)
                        {
                            response.PhotoGalleryMemberList.Add(pri.GalleryMember);
                        }
                    }
                    response.BatchResultCount = response.PhotoGalleryMemberList.Count;
                    response.Status = (int)JSONStatus.Success;
                    response.StatusMessage = "Success";
                }
                else
                {
                    response.Status = (int)JSONStatus.Failed;
                    response.StatusMessage = "Missing ContextGlobal";
                }
            }
            catch (Exception ex)
            {
                _g.ProcessException(ex);
                response.Status = (int)JSONStatus.Failed;
                response.StatusMessage = "Error";
            }
            return response;
        }

        #endregion

        #region Homepage slider/filmstrip related
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public GetSearchResult GetSliderProfiles(int memberID, int startRow, int pageSize, bool checkMatches, bool checkMOL)
        {
            GetSearchResult result = new GetSearchResult();
            try
            {
                if (_g != null && _g.Member != null)
                {
                    System.Web.UI.Page page = new System.Web.UI.Page();
                    System.Web.UI.Control slideResourceControl = page.LoadControl("/Applications/Home/Controls/SliderResource.ascx");

                    //get slider profiles
                    result = HomeUtil.GetSliderProfilesJSON(_g.Member, _g.Brand, _g, _g.SearchPreferences, _g.Session, startRow, pageSize, checkMatches, checkMOL, HomeUtil.MATCH_SLIDER_USERNAME_MAX, slideResourceControl);
                }
                else
                {
                    result.IsMemberNull = true;
                    throw new Exception("Member is not logged in");
                }

            }
            catch (Exception ex)
            {
                _g.ProcessException(ex);
                result.Status = (int)JSONStatus.Failed;
                result.StatusMessage = ex.Message;
            }

            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public GetSearchResult GetSliderProfilesWithEntrypoint(int memberID, int startRow, int pageSize, bool checkMatches, bool checkMOL, int entryPointID)
        {
            GetSearchResult result = new GetSearchResult();
            try
            {
                if (_g != null && _g.Member != null)
                {
                    System.Web.UI.Page page = new System.Web.UI.Page();
                    System.Web.UI.Control slideResourceControl = page.LoadControl("/Applications/Home/Controls/SliderResource.ascx");

                    BreadCrumbHelper.EntryPoint entryPoint = (BreadCrumbHelper.EntryPoint)Enum.Parse(typeof (BreadCrumbHelper.EntryPoint),
                                                                        entryPointID.ToString());

                    //get slider profiles
                    result = HomeUtil.GetSliderProfilesJSON(_g.Member, _g.Brand, _g, _g.SearchPreferences, _g.Session, startRow, pageSize, checkMatches, checkMOL, HomeUtil.MATCH_SLIDER_USERNAME_MAX, slideResourceControl, entryPoint);
                }
                else
                {
                    result.IsMemberNull = true;
                    throw new Exception("Member is not logged in");
                }

            }
            catch (Exception ex)
            {
                _g.ProcessException(ex);
                result.Status = (int)JSONStatus.Failed;
                result.StatusMessage = ex.Message;
            }

            return result;
        }
        #endregion
    }
}
