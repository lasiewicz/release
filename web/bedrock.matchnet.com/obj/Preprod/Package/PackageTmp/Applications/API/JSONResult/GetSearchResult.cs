﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Matchnet.Web.Applications.API.JSONResult
{
    /// <summary>
    /// Result class to encapsulate data returned for updating search results
    /// </summary>
    [Serializable]
    public class GetSearchResult : ResultBase
    {
        public string SearchResultHTML { get; set; }
        public bool IsSearchResultNested { get; set; }
        public List<SearchProfile> SearchProfileList { get; set; }
        public bool IsMatchResults { get; set; }
        public bool IsMOLResults { get; set; }
        public int TotalResults { get; set; }
        public bool IsMemberNull { get; set; }
    }

    [Serializable]
    public class SearchProfile
    {
        public int Ordinal { get; set; }
        public string PhotoURL { get; set; }
        public string LargePhotoURL { get; set; }
        public string ProfileURL { get; set; }
        public string ProfileURLWithoutOrdinal { get; set; }
        public int MemberID { get; set; }
        public string Username { get; set; }
        public string UsernameShort { get; set; }
        public bool Highlighted { get; set; }
        public DateTime Birthdate { get; set; }
        public int RegionID { get; set; }
        public int NumberApprovedPhotos { get; set; }
        public string Age { get; set; }
        public string Location { get; set; }
        public string ViewMorePhotos { get; set; }
        public int MatchRating { get; set; }
        public string Gender { get; set; }
        public int YNMCurrentVote { get; set; }
    }
}