﻿using System;

namespace Matchnet.Web.Applications.API.JSONResult
{
    [Serializable]
    public class NewsfeedResult : ResultBase
    {
        public string NewsfeedHTML { get; set; }
        public bool HasItems { get; set; }
    }
}