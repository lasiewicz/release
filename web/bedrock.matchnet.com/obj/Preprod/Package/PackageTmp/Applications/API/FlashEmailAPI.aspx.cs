using System;
using System.Collections;
using System.Web.UI.WebControls;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Web.Framework;

using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Email.ServiceAdapters;
using Matchnet.Email.ValueObjects;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects.Photos;
using Matchnet.Web.Framework.Managers;

namespace Matchnet.Web.Applications.API
{
	/// <summary>
	/// Summary description for EmailAPI.
	/// </summary>
	public class FlashEmailAPI : SessionDependentAPIBase
	{
		protected Repeater xmlRepeater;

		private Int32 _memberFolderID;
		private string _orderBy;
		private Int32 _currentMessageNumber;
		private Int32 _messageCount;

		private void Page_Load(object sender, System.EventArgs e)
		{
			_memberFolderID = Conversion.CInt(Request["MemberFolderID"]);
			Int32 startRow = Conversion.CInt(Request["StartRow"]);
			Int32 pageSize = Conversion.CInt(Request["PageSize"]);
			_orderBy = Request["OrderBy"];
			
			_messageCount = EmailMessageSA.Instance.RetrieveMessages(MemberID, Brand.Site.Community.CommunityID, _memberFolderID).Count;
			_currentMessageNumber = 0;

			ICollection messages = EmailMessageSA.Instance.RetrieveMessages(MemberID, Brand.Site.Community.CommunityID, _memberFolderID, startRow, pageSize, _orderBy, Brand);

			xmlRepeater.DataSource = messages;
			xmlRepeater.DataBind();
		}

		private void xmlRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{
				_currentMessageNumber++;
				Literal litFrom = (Literal) e.Item.FindControl("litFrom");
				Literal litSubject = (Literal) e.Item.FindControl("litSubject");
				Literal litInsertDate = (Literal) e.Item.FindControl("litInsertDate");
				Literal litUrl = (Literal) e.Item.FindControl("litUrl");
				Literal litThumbUrl = (Literal) e.Item.FindControl("litThumbUrl");
				Literal litRegion = (Literal) e.Item.FindControl("litRegion");
				Literal litGender = (Literal) e.Item.FindControl("litGender");

				EmailMessage message = (EmailMessage) e.Item.DataItem;

				litFrom.Text = message.FromUserName;
				litSubject.Text = message.Subject;
				litInsertDate.Text = message.InsertDate.ToString();
				litUrl.Text = "http://dev.jdate.com/Applications/Email/ViewMessage.aspx?MemberMailID=" + message.MemberMailID.ToString() + "&MemberFolderID=" + _memberFolderID + "&OrderBy=" + _orderBy + "&CurrentMessageNumber=" + _currentMessageNumber + "&MessageCount=" + _messageCount;

				Matchnet.Member.ServiceAdapters.Member member = MemberSA.Instance.GetMember(message.FromMemberID, MemberLoadFlags.None);

                //MemberPhotoHelper photoHelper = new MemberPhotoHelper(RuntimeSettings.Instance);
                Photo photo = MemberPhotoDisplayManager.Instance.GetDefaultPhoto(member, member, Brand);
                litThumbUrl.Text = MemberPhotoDisplayManager.Instance.GetPhotoDisplayURL(member, member, Brand, photo,
			                                                            PhotoType.Thumbnail,
			                                                            PrivatePhotoImageType.Thumb,
			                                                            NoPhotoImageType.Thumb);



				litRegion.Text = FrameworkGlobals.GetRegionString(member.GetAttributeInt(Brand, WebConstants.ATTRIBUTE_NAME_REGIONID),
					(Int32) Matchnet.Language.English, false, true, true);

				litGender.Text = member.GetAttributeInt(Brand, WebConstants.ATTRIBUTE_NAME_GENDERMASK).ToString();
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
			xmlRepeater.ItemDataBound += new RepeaterItemEventHandler(xmlRepeater_ItemDataBound);
		}
		#endregion
	}
}
