using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.Web.Script.Serialization;

using Matchnet.Web.Framework;

using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.List.ServiceAdapters;
using Matchnet.List.ValueObjects;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects.Photos;

namespace Matchnet.Web.Applications.API
{

    [WebService(Namespace="http://spark.net/API")]
	[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
	[System.ComponentModel.ToolboxItem(false)]
	[ScriptService]
	public class FavoritesService : WebService
	{
        private Int32 _communityID;
        private Brand _brand;
        private ContextGlobal _g;

        public FavoritesService()
		{
			InitializeComponent();
            if (null == _g)
            {
                if (Context != null)
                {
                    if (Context.Items["g"] != null)
                    {
                        _g = (ContextGlobal)Context.Items["g"];
                    }
                    else
                    {
                        _g = new ContextGlobal(Context);
                    }
                    _g.SaveSession = false;
                }
                //System.Web.UI.Page page = new System.Web.UI.Page();
                //Matchnet.Web.Analytics.Omniture omniture = (Matchnet.Web.Analytics.Omniture)page.LoadControl("/Analytics/Omniture.ascx");
                //_g.AnalyticsOmniture = omniture;
            }
        }


		#region Component Designer generated code
		
		private IContainer components = null;
				
		private void InitializeComponent()
		{
		}

		protected override void Dispose( bool disposing )
		{
			if(disposing && components != null)
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}
		
		#endregion

		[WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string GetFavorites(Int32 brandID, Int32 memberID, Int32 startIndex, Int32 pageSize, Int32 prtId, string omniturePageName)
		{
			_brand = BrandConfigSA.Instance.GetBrandByID(brandID);
            System.Web.UI.Page page = new System.Web.UI.Page();
            Favorites.FavoritesControl ctrl = page.LoadControl("/Applications/Favorites/FavoritesControl.ascx") as Favorites.FavoritesControl;
            System.Collections.Generic.Dictionary<string, Favorites.FavoriteMember> favorites = ctrl.GetFavorites(_brand, memberID, startIndex, pageSize, prtId, omniturePageName);
            int totalFavorites = ctrl.GetTotalFavoritesNumber(_brand, memberID);

            return new JavaScriptSerializer().Serialize(new object[] {favorites, totalFavorites});
		}

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string GetFavoritesOnlineNumber(Int32 brandID, Int32 memberID, string omniturePageName)
        {
			_brand = BrandConfigSA.Instance.GetBrandByID(brandID);
            System.Web.UI.Page page = new System.Web.UI.Page();
            Favorites.FavoritesControl ctrl = page.LoadControl("/Applications/Favorites/FavoritesControl.ascx") as Favorites.FavoritesControl;
            int numOnline = ctrl.GetTotalFavoritesOnlineNumber(_brand, memberID, omniturePageName);
            Hashtable h=new Hashtable();
            h.Add("favsOnline", numOnline);
            return new JavaScriptSerializer().Serialize(h);
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string ProcessFavoriteOperation(Int32 favoriteMemberID)
        {
            string result=string.Empty;
            //_brand = BrandConfigSA.Instance.GetBrandByID(brandID);
            System.Web.UI.Page page = new System.Web.UI.Page();
            Favorites.FavoritesControl ctrl = page.LoadControl("/Applications/Favorites/FavoritesControl.ascx") as Favorites.FavoritesControl;
            result=ctrl.ProcessFavoriteOperation(null, favoriteMemberID);
            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string AddFavorite(int memberID, int favoriteMemberID)
        {
            string result = string.Empty;
            if (_g != null && _g.Member != null && _g.Member.MemberID == memberID)
            {
                System.Web.UI.Page page = new System.Web.UI.Page();
                Favorites.FavoritesControl ctrl = page.LoadControl("/Applications/Favorites/FavoritesControl.ascx") as Favorites.FavoritesControl;
                result = ctrl.ProcessFavoriteOperation(null, favoriteMemberID, false, true);

                //save to session to write during GET for omniture (currently controlled by Add2List.ascx)
                if (_g.Session != null)
                    _g.Session.Add(WebConstants.SESSION_PROPERTY_NAME_HOTLIST_ACTION, "Hot List", Matchnet.Session.ValueObjects.SessionPropertyLifetime.Temporary);
            }
            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string AddOrRemoveFavorite(int memberID, int favoriteMemberID)
        {
            string result = string.Empty;
            if (_g != null && _g.Member != null && _g.Member.MemberID == memberID)
            {
                System.Web.UI.Page page = new System.Web.UI.Page();
                Favorites.FavoritesControl ctrl = page.LoadControl("/Applications/Favorites/FavoritesControl.ascx") as Favorites.FavoritesControl;
                result = ctrl.ProcessFavoriteOperation(null, favoriteMemberID, true, true);

                //save to session to write during GET for omniture (currently controlled by Add2List.ascx)
                if (_g.Session != null)
                    _g.Session.Add(WebConstants.SESSION_PROPERTY_NAME_HOTLIST_ACTION, "Hot List", Matchnet.Session.ValueObjects.SessionPropertyLifetime.Temporary);
            }
            return result;
        }
	}
}
