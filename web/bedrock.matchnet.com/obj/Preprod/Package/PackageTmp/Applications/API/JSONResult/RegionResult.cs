﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections.Specialized;

namespace Matchnet.Web.Applications.API.JSONResult
{
    [Serializable]
    public class RegionResult : ResultBase
    {
        public string LocationDisplayText { get; set; }
        public int RegionID { get; set; }
        public string ValidAreaCodes { get; set; }
        public bool isUSA { get; set; }
        public bool isCanada { get; set; }
        public bool hasStates { get; set; }
        public Dictionary<string, string> StatesList { get; set; }
        public List<string> CityList { get; set; }

        public RegionResult()
        {
            StatesList = new Dictionary<string, string>();
            CityList = new List<string>();
        }
    }
}
