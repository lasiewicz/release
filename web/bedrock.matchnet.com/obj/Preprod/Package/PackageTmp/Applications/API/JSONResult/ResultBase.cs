﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Matchnet.Web.Applications.API.JSONResult
{
    /// <summary>
    /// Base class for returning JSON results for API calls
    /// </summary>
    [Serializable]
    public class ResultBase
    {
        public int Status { get; set; }
        public string StatusMessage { get; set; }

        public ResultBase()
        {
            Status = (int)JSONStatus.Failed;
            StatusMessage = "";
        }
    }

    public enum JSONStatus : int
    {
        None = 0,
        Pending = 1,
        Success = 2,
        Failed = 3,
        SubNeeded = 4,
        SessionExpired = 5
    }
}
