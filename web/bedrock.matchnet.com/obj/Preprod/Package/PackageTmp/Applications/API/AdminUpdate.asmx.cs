﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.Web.Script.Serialization;

using Matchnet;
using Matchnet.Web.Framework;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Content.ValueObjects.Admin;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects;
using Matchnet.Member.ValueObjects.Privilege;

namespace Matchnet.Web.Applications.API
{
    /// <summary>
    /// Summary description for AdminUpdate
    /// </summary>
    [WebService(Namespace = "http://spark.net/API")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    [ScriptService]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class AdminUpdate : WebService
    {
        private ContextGlobal _g;

        public AdminUpdate()
		{
			if (null == _g)
            {
                if (Context != null)
                {
                    if (Context.Items["g"] != null)
                    {
                        _g = (ContextGlobal)Context.Items["g"];
                    }
                    else
                    {
                        _g = new ContextGlobal(Context);
                    }
                }
            }
        }
        
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public bool UpdateMemberAttributes(int memberID, bool adminSuspended, bool badEmail, bool emailVerified)
        {
            bool success = true;

            try
            {
                GlobalStatusMask globalStatusMask = (GlobalStatusMask) Constants.NULL_INT;
				Matchnet.Member.ServiceAdapters.Member member = MemberSA.Instance.GetMember(memberID, MemberLoadFlags.None);

				globalStatusMask = (GlobalStatusMask) member.GetAttributeInt(_g.TargetBrand, WebConstants.ATTRIBUTE_NAME_GLOBALSTATUSMASK);
                if (globalStatusMask != (GlobalStatusMask)Constants.NULL_INT)
                {
                    GlobalStatusMask oldGlobalStatusMask = globalStatusMask;

                    if (adminSuspended)
                    {
                        globalStatusMask = globalStatusMask | GlobalStatusMask.AdminSuspended;
                    }
                    else
                    {
                        globalStatusMask = globalStatusMask & (~GlobalStatusMask.AdminSuspended);
                    }

                    if (badEmail)
                    {
                        globalStatusMask = globalStatusMask | GlobalStatusMask.BouncedEmail;
                    }
                    else
                    {
                        globalStatusMask = globalStatusMask & (~GlobalStatusMask.BouncedEmail);
                    }

                    if (emailVerified)
                    {
                        globalStatusMask = globalStatusMask | GlobalStatusMask.VerifiedEmail;
                        _g.EmailVerify.SetVerifiedMemberAttributes(member);

                    }
                    else
                    {
                        globalStatusMask = globalStatusMask & (~GlobalStatusMask.VerifiedEmail);
                    }
                    //Update the GlobalStatusMask
                    member.SetAttributeInt(_g.TargetBrand, WebConstants.ATTRIBUTE_NAME_GLOBALSTATUSMASK, (Int32)globalStatusMask);
                    SetLastUpdateDateForAllSites(member);
                    MemberSaveResult msr = MemberSA.Instance.SaveMember(member);

                    if (msr.SaveStatus == MemberSaveStatusType.Success)
                    {
                        // Log any of the mask flags that changed
                        AdminAction adminAction = AdminAction.Default;
                        if ((oldGlobalStatusMask & GlobalStatusMask.AdminSuspended) != (globalStatusMask & GlobalStatusMask.AdminSuspended))
                        {
                            if ((globalStatusMask & GlobalStatusMask.AdminSuspended) == GlobalStatusMask.AdminSuspended)
                                adminAction |= AdminAction.AdminSuspendMember;
                            else
                                adminAction |= AdminAction.AdminUnsuspendMember;
                        }

                        if ((oldGlobalStatusMask & GlobalStatusMask.VerifiedEmail) != (globalStatusMask & GlobalStatusMask.VerifiedEmail))
                        {
                            adminAction |= AdminAction.ChangedEmailVerified;
                        }

                        if ((oldGlobalStatusMask & GlobalStatusMask.BouncedEmail) != (globalStatusMask & GlobalStatusMask.BouncedEmail))
                        {
                            adminAction |= AdminAction.ChangedBadEmail;
                        }

                        AdminSA.Instance.AdminActionLogInsert(
                            memberID
                            , Constants.GROUP_PERSONALS
                            , (Int32)adminAction
                            , _g.Session.GetInt(WebConstants.SESSION_PROPERTY_NAME_MEMBERID));
                    }
                    else
                    {
                        //Update to GlobalStatusMask failed
                        success = false;
                    }
                }
                else
                {
                    success = false;
                }
            }
            catch (Exception ex)
            {
                success = false;
                _g.ProcessException(ex);
            }

            return success;
        }

        private void SetLastUpdateDateForAllSites(Matchnet.Member.ServiceAdapters.Member member)
        {
            int[] siteIDList = member.GetSiteIDList();
            if (siteIDList.Length > 0)
            {
                foreach (int siteID in siteIDList)
                {
                    Brand brand = FrameworkGlobals.GetBrandForSite(siteID);
                    member.SetAttributeDate(brand, "LastUpdated", DateTime.Now);
                }
            }
        }
    }
}
