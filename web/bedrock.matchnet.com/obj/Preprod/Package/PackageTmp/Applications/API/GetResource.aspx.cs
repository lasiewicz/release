﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Util;

namespace Matchnet.Web.Applications.API
{
    public partial class GetResource : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // brandID, attribute name, attribute value, isMask should be passed in
            Int32 brandID = Conversion.CInt(Request["BrandID"]);
            string attributeName = Request["AttributeName"];
            Int32 attributeValue = Conversion.CInt(Request["AttributeValue"]);
            Int32 isMask = Conversion.CInt(Request["IsMask"]);

            string output = string.Empty;
            Matchnet.Content.ValueObjects.BrandConfig.Brand brand = Matchnet.Content.ServiceAdapters.BrandConfigSA.Instance.GetBrandByID(brandID);

            if (attributeName.ToLower() == "height")
            {
                output = FrameworkGlobals.GlobalHeight(brand.Site.CultureInfo.LCID, attributeValue, brand, false);
            }
            else
            {
                if (isMask == 1)
                {
                    output = Option.GetMaskContent(attributeName, attributeValue, brand);
                }
                else
                {
                    output = Option.GetContent(attributeName, attributeValue, brand);
                }
            }
            
            Response.Write(output);
        }
    }
}
