using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Matchnet.Member.ServiceAdapters;
using Matchnet.MembersOnline.ServiceAdapters;
using Matchnet.MembersOnline.ValueObjects;

namespace Matchnet.Web.Applications.API
{
	/// <summary>
	/// Summary description for UpdateMembersOnline.
	/// </summary>
	public class UpdateMembersOnline : SessionDependentAPIBase
	{
		private void Page_Load(object sender, System.EventArgs e)
		{
            try
            {
                if (MemberID > 0 && Brand != null)
                {
                    //MembersOnline Service throws an error, when a request is made to add a member not registered to a community.
                    //Check if member has registerd to the community.
                    Matchnet.Member.ServiceAdapters.Member member = MemberSA.Instance.GetMember(MemberID, MemberLoadFlags.None);

                    int brandID = 0;
                    member.GetLastLogonDate(Brand.Site.Community.CommunityID, out brandID);

                    if (brandID > 0)
                    {
                        //add to Members Online list (as of this writing, this is the only known event that adds to the Members Online collection (Adam 10/24/05))
                        MembersOnlineSA.Instance.Add(Brand.Site.Community.CommunityID, MemberID);
                    }
                }
            }
            catch (Exception ex)
            {
                Matchnet.Web.Framework.Diagnostics.ContextExceptions contextexception = new Matchnet.Web.Framework.Diagnostics.ContextExceptions();
                contextexception.LogException(ex);
            }
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
