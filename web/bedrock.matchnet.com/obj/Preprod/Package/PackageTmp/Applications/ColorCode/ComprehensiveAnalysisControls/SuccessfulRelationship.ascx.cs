﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Matchnet.Web.Applications.ColorCode.ComprehensiveAnalysisControls
{
    public partial class SuccessfulRelationship : ComprehensiveAnalysisBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            decimal primaryscore=0;
            decimal secondaryscore=0;

            string letter1 = "";
            string letter2 = "";
            string letter3 = "";
            string letter4 = "";
            MemberQuiz _memberquiz = ColorCodeHelper.GetMemberQuiz(g.Member, g.Brand);

          


            IOrderedEnumerable<Color> scores = (from s in _memberquiz.Scores.Keys
                                                orderby _memberquiz.Scores[s] descending
                                                select s);
            if (SecondaryColorCode != Color.none)
            {
                primaryscore = _memberquiz.Scores[FourthColorCode];
                secondaryscore = _memberquiz.Scores[ThirdColorCode];
                letter1 = PrimaryColorCode.ToString().ToLower().Substring(0, 1);
                letter2 = SecondaryColorCode.ToString().ToLower().Substring(0, 1);
                letter3 = ThirdColorCode.ToString().ToLower().Substring(0, 1);
                letter4 = FourthColorCode.ToString().ToLower().Substring(0, 1);

            }
            else
            {
                letter1 = PrimaryColorCode.ToString().ToLower().Substring(0, 1);

                SecondaryColor = scores.ElementAt<Color>(1).ToString().ToUpper();
                ThirdColor = scores.ElementAt<Color>(2).ToString().ToUpper();
                FourthColor = scores.ElementAt<Color>(3).ToString().ToUpper();
                letter2 = scores.ElementAt<Color>(1).ToString().ToLower().Substring(0, 1);
                letter3 = scores.ElementAt<Color>(2).ToString().ToLower().Substring(0, 1);
                letter4 = scores.ElementAt<Color>(3).ToString().ToLower().Substring(0, 1);

            }
            

            txtContent.Text=g.GetResource("TXT_CONTENT",  new string[]{FirstName,primaryscore.ToString(),ThirdColor.ToUpper(), secondaryscore.ToString(),FourthColor.ToUpper(),
                                            ThirdColor.ToUpper(),FourthColor.ToUpper(),PrimaryColor.ToUpper(),PrimaryColor.ToUpper(),
                                            letter1,letter4,letter3,letter2,letter1,letter1},true,this);



        }
    }
}