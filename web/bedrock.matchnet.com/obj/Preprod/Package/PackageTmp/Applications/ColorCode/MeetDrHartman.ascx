﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MeetDrHartman.ascx.cs" Inherits="Matchnet.Web.Applications.ColorCode.MeetDrHartman" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>

<div id="page-container" class="clearfix cc-content-main cc-content-no-bars">

    <h2 class="tagline">The Color Code <small class="sans-serif">- Understand yourself and your matches on a deeper level.</small></h2>

    <div class="wrapper">
        <div id="cc-content" class="cc-content-full">

            <div class="editorial">

            <h1>About Dr. Hartman</h1>
            <p>In a day and age when whining for the spotlight and shameless notoriety are often confused with earned fame, Dr. Taylor Hartman strikes you, instead, as the genuine article. He is warm, charismatic and intelligent. He is a much sought after speaker, and literally tens of thousands of people have felt the force of his unconditional love and have used his powerful words to change their lives for the better. He is one of those rare people who are simply unforgettable.</p>

            <p>Dr. Hartman has appeared on <em>The Today Show</em>, <em>The Home and Family Show</em>, <em>The Gayle King Show</em>, <em>The View with Barbara Walters</em> and hundreds of syndicated radio shows. Articles by and about him have appeared in <em>New Woman Magazine</em>, <em>Entrepreneur Magazine</em>, <em>Woman's World</em>, <em>Zest</em> (United Kingdom) and <em>Mademoiselle Magazine</em>. Appearances by Dr. Hartman range from China to South America and from Africa to Canada.</p>

            <p>Dr. Hartman's first book, <em>The Color Code</em>, is an international best seller and has already been translated into several languages. His work has quickly become recognized for its simplicity and accuracy in understanding the unique complexities of human behavior. No other literary work is as user-friendly in helping its readers quickly apply invaluable insights in every facet of their personal and professional lives.</p>


            </div><!-- end editorial -->


            <p class="text-center editorial cc-button-primary">	
	            <a href="Landing.aspx" class="link-primary">Back to Color Code Profile</a>
            </p>


        </div><!-- end .cc-content -->
    </div><!-- end .wrapper -->


</div><!-- end #page-container -->

<p id="cc-copy">&copy; 2010 Color Code Communications, Inc. All rights reserved.</p>
