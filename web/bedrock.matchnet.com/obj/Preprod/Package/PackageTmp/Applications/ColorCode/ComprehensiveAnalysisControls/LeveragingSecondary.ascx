﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LeveragingSecondary.ascx.cs" Inherits="Matchnet.Web.Applications.ColorCode.ComprehensiveAnalysisControls.LeveragingSecondary" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>

<%--	<h2 class="title"><mn:Txt runat="server" ID="txtTitle" ResourceConstant="TXT_SECTION_TITLE" /></h2>
	<div class="<asp:Literal id=litPrimaryColorClass runat=server />">
		<mn:Txt runat="server" ID="txt1" ResourceConstant="TXT_SECTION_COLOR_TITLE" />
	</div>--%>
	<h2 class="title">	<mn:Txt runat="server" ID="txtSubTitle" /></h2>
	
	<div class="margin-medium">
			<mn:Txt runat="server" ID="txtContent"/>
			
			<ul class="traits clearfix">
			<li>
				<h2 class="<asp:Literal ID=litPrimaryColorClass runat=server/>"><mn:Txt runat="server" ID="txtPrimaryColor" /><em>Strengths</em></h2>
				<ul>
					<asp:Repeater ID="rptPrimaryStrength" runat="server" OnItemDataBound="BindTraits">
					<ItemTemplate>
					<li><asp:Literal runat="server" ID="litTrait" /></li>
					</ItemTemplate>
					</asp:Repeater>
				</ul>
			</li>

			<li>
				<h2 class="<asp:Literal ID=litPrimaryColorClass1 runat=server/>"><mn:Txt runat="server" ID="txtPrimaryColor1" /><em>Limitations</em></h2>
				<ul>
					<asp:Repeater ID="rptPrimaryLimit" runat="server" OnItemDataBound="BindTraits">
					<ItemTemplate>
					<li><asp:Literal runat="server" ID="litTrait" /></li>
					</ItemTemplate>
					</asp:Repeater>
				</ul>
			</li>
			
		</ul>
		
			<mn:Txt runat="server" ID="txtContent1" />
	</div><!-- end .margin-medium -->
	<asp:PlaceHolder runat="server" ID="phNavigation" >
	<div class="bottom_nav">
		<h4 class="float-inside"><a href="/Applications/ColorCode/ComprehensiveAnalysis.aspx?section=gifts"><mn:Txt runat="server" ID="txt4" ResourceConstant="TXT_BACK_LNK" /></a></h4>
		<h4 class="float-outside"><a href="/Applications/ColorCode/ComprehensiveAnalysis.aspx?section=buildingcharacter"><mn:Txt runat="server" ID="txt5" ResourceConstant="TXT_FORWARD_LNK" /></a></h4>
	</div>
	</asp:PlaceHolder>
