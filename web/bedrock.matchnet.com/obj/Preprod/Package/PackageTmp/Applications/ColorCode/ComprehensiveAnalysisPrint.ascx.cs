﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Web.Framework;
using Matchnet.Web.Applications.ColorCode.ComprehensiveAnalysisControls;
namespace Matchnet.Web.Applications.ColorCode
{
    public partial class ComprehensiveAnalysisPrint : FrameworkControl
    {
        public enum PrintModeEnum:int
        {
           section=0,
           page=1,
           whole=2
        }
        public Color PrimaryColorCode { get; set; }
        public Color SecondaryColorCode { get; set; }
        public List<ColorCodeAnalysisPage> Sections { get; set; }

        public PrintModeEnum PrintMode { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (g.Member == null)
                {
                    g.Transfer("/Applications/Logon/Logon.aspx?DestinationURL=" + HttpUtility.UrlEncode(Request.RawUrl));

                }
                if (!ColorCodeHelper.HasMemberCompletedQuiz(g.Member, g.Brand))
                    g.Transfer("/Applications/ColorCode/Landing.aspx?NavPoint=sub&colortracking=sub");

                int purchase = g.Member.GetAttributeInt(g.Brand, "ColorAnalysis");

                if (purchase != 1)
                    g.Transfer("/applications/colorcode/comppurchase.aspx");

                txtCopyrights.Text = g.GetResource("TXT_COPYRIGHT", this, new string[] { DateTime.Now.Year.ToString() });
                PrimaryColorCode = ColorCodeHelper.GetPrimaryColor(g.Member, g.Brand);
                SecondaryColorCode = ColorCodeHelper.GetSecondaryColor(g.Member, g.Brand);

                MemberQuiz _memberquiz = ColorCodeHelper.GetMemberQuiz(g.Member, g.Brand);


                IOrderedEnumerable<Color> scores = (from s in _memberquiz.Scores.Keys
                                                    orderby _memberquiz.Scores[s] descending
                                                    select s);
                setTestData();

                string section = Request["section"];
              

                string page = Request["page"];

                if (section == "0")
                    PrintMode = PrintModeEnum.whole;
                else if (String.IsNullOrEmpty(page))
                {
                    PrintMode = PrintModeEnum.section;
                    if (String.IsNullOrEmpty(section))
                        section = "1";
                }
                else
                {
                    PrintMode = PrintModeEnum.page;
                }

                decimal primary = ColorCodeHelper.GetScore(g.Member, g.Brand, PrimaryColorCode);
                if (primary == 100)
                    SecondaryColorCode = Color.none;

           

                PopulateSectionsPages();
                if (PrintMode == PrintModeEnum.whole)
                {
                    foreach (ColorCodeAnalysisPage p in Sections)
                    {
                        AddControl(p.PageName, scores);
                    }
               
                }
                else if (PrintMode == PrintModeEnum.page)
                {
                    AddControl(page, scores);
                }
                else if (PrintMode == PrintModeEnum.section)
                {
                    List<ColorCodeAnalysisPage> sections = (from s in Sections
                                                           where s.SectionID == Conversion.CInt(section)
                                                           select s).ToList<ColorCodeAnalysisPage>();


                    foreach (ColorCodeAnalysisPage p in sections)
                    {
                        AddControl(p.PageName, scores);
                    }

                }


               

            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        private void PopulateSectionsPages()
        {

            Sections = new List<ColorCodeAnalysisPage>();

            Sections.Add(new ColorCodeAnalysisPage(1, "main"));
            Sections.Add(new ColorCodeAnalysisPage(1, "introduction"));
            Sections.Add(new ColorCodeAnalysisPage(2, "basiccorecolors"));
            Sections.Add(new ColorCodeAnalysisPage(2, "filters"));
            Sections.Add(new ColorCodeAnalysisPage(2, "demographics"));


            Sections.Add(new ColorCodeAnalysisPage(3, "results"));
            Sections.Add(new ColorCodeAnalysisPage(3, "traits"));
            Sections.Add(new ColorCodeAnalysisPage(3, "coremotive"));
            Sections.Add(new ColorCodeAnalysisPage(3, "personalstyle"));
            if(SecondaryColorCode != Color.none)
                Sections.Add(new ColorCodeAnalysisPage(3, "personalsecondarystyle"));

            Sections.Add(new ColorCodeAnalysisPage(3, "role"));
            Sections.Add(new ColorCodeAnalysisPage(3, "needs"));
            Sections.Add(new ColorCodeAnalysisPage(3, "naturaleqcompetency"));
            Sections.Add(new ColorCodeAnalysisPage(3, "developmenttasks"));

            Sections.Add(new ColorCodeAnalysisPage(4, "applyingpersonality"));
            Sections.Add(new ColorCodeAnalysisPage(4, "congruence"));
            Sections.Add(new ColorCodeAnalysisPage(4, "getcoremotive"));
            Sections.Add(new ColorCodeAnalysisPage(4, "gifts"));
            if (SecondaryColorCode != Color.none)
                Sections.Add(new ColorCodeAnalysisPage(4, "leveragingsecondary"));
            Sections.Add(new ColorCodeAnalysisPage(4, "buildingcharacter"));

            Sections.Add(new ColorCodeAnalysisPage(5, "relatingtoothers"));
            Sections.Add(new ColorCodeAnalysisPage(5, "relationtoothercolors"));
            Sections.Add(new ColorCodeAnalysisPage(5, "successfulrelationship"));
            Sections.Add(new ColorCodeAnalysisPage(5, "relationshiptips"));
            Sections.Add(new ColorCodeAnalysisPage(5, "evaluatingrelationship"));
            Sections.Add(new ColorCodeAnalysisPage(5, "conclusion"));
        }

        private void PopulateTraits(ComprehensiveAnalysisBase control)
        {

            OptionTraitMap map = Utils.PopulateOptionTraitMap(g, this);
            MemberQuiz _memberquiz = ColorCodeHelper.GetMemberQuiz(g.Member, g.Brand);

            OptionTraitMap memberMap = Utils.PopulateMemberOptionTraitMap(_memberquiz, map);


            IOrderedEnumerable<Color> scores = (from s in _memberquiz.Scores.Keys
                                                orderby _memberquiz.Scores[s] descending
                                                select s);


            control.PrimaryColorStrength = memberMap.GetTraitList(_memberquiz.PrimaryColor.ToString(), TraitTypeEnum.strength);
            control.PrimaryColorLimitation = memberMap.GetTraitList(_memberquiz.PrimaryColor.ToString(), TraitTypeEnum.limitation);
            if (SecondaryColorCode != Color.none)
            {
                control.SecondaryColorStrength = memberMap.GetTraitList(_memberquiz.SecondaryColor.ToString(), TraitTypeEnum.strength);

                control.ThirdColorStrength = memberMap.GetTraitList(scores.ElementAt<Color>(2).ToString(), TraitTypeEnum.strength);

                control.FourthColorStrength = memberMap.GetTraitList(scores.ElementAt<Color>(3).ToString(), TraitTypeEnum.strength);

                control.PrimaryColorLimitation = memberMap.GetTraitList(_memberquiz.PrimaryColor.ToString(), TraitTypeEnum.limitation);

                control.SecondaryColorLimitation = memberMap.GetTraitList(_memberquiz.SecondaryColor.ToString(), TraitTypeEnum.limitation);

                control.ThirdColorLimitation = memberMap.GetTraitList(scores.ElementAt<Color>(2).ToString(), TraitTypeEnum.limitation);

                control.FourthColorLimitation = memberMap.GetTraitList(scores.ElementAt<Color>(3).ToString(), TraitTypeEnum.limitation);
            }

        }

        private void setTestData()
        {

            if (!String.IsNullOrEmpty(Request["c1"]))
            {
                PrimaryColorCode = (Color)Enum.Parse(typeof(Color), Request["primarycolorcode"]);
            }

            if (!String.IsNullOrEmpty(Request["c2"]))
            {
                SecondaryColorCode = (Color)Enum.Parse(typeof(Color), Request["secondarycolorcode"]);
            }

            if (!String.IsNullOrEmpty(Request["c3"]))
            {
                SecondaryColorCode = (Color)Enum.Parse(typeof(Color), Request["secondarycolorcode"]);
            }
            if (!String.IsNullOrEmpty(Request["c4"]))
            {
                SecondaryColorCode = (Color)Enum.Parse(typeof(Color), Request["secondarycolorcode"]);
            }



        }


        private void AddControl(string section, IOrderedEnumerable<Color> scores)
        {
            ComprehensiveAnalysisControls.ComprehensiveAnalysisBase control = (ComprehensiveAnalysisControls.ComprehensiveAnalysisBase)LoadControl("/Applications/ColorCode/ComprehensiveAnalysisControls/" + section + ".ascx");
            control.PrimaryColorCode = PrimaryColorCode;
            control.SecondaryColorCode = SecondaryColorCode;

            control.PrimaryColor = g.GetResource(String.Format("TXT_COLOR_{0}", PrimaryColorCode.ToString().ToUpper()), this);
          
            if (SecondaryColorCode != Color.none)
            {
                control.SecondaryColor = g.GetResource(String.Format("TXT_COLOR_{0}", SecondaryColorCode.ToString().ToUpper()), this);
                control.SecondaryColorCode = SecondaryColorCode;

               
                    control.ThirdColor = g.GetResource(String.Format("TXT_COLOR_{0}", scores.ElementAt<Color>(2).ToString().ToUpper()), this);
                    control.ThirdColorCode = scores.ElementAt<Color>(2);
                

               
                    control.FourthColor = g.GetResource(String.Format("TXT_COLOR_{0}", scores.ElementAt<Color>(3).ToString().ToUpper()), this);
                    control.FourthColorCode = scores.ElementAt<Color>(3);
               

            } else
                {
                    control.SecondaryColor = "";
                    control.ThirdColor = "";
                    control.FourthColor = "";
                }

            control.CoreMotive = g.GetResource(String.Format("TXT_CORE_MOTIVE_{0}", PrimaryColorCode.ToString().ToUpper()), this);
            control.TagLine = g.GetResource(String.Format("TXT_TAGLINE_{0}", PrimaryColorCode.ToString().ToUpper()), this);
            control.EQ = g.GetResource(String.Format("TXT_EQ_{0}", PrimaryColorCode.ToString().ToUpper()), this);
            control.Motivated = g.GetResource(String.Format("TXT_MOTIVATED_{0}", PrimaryColorCode.ToString().ToUpper()), this);
            setFirstLastName(control);
            control.Weight = g.GetResource("TXT_WEIGHT_" + PrimaryColorCode.ToString().ToUpper(), this);
            PopulateTraits(control);
            txtComprehensiveAnalysis.Text = g.GetResource("TXT_COMPREHENSIVE_ANALYSIS", this, new string[] { control.FirstLastName });


            phContent.Controls.Add(control);
        }

        private void setFirstLastName(ComprehensiveAnalysisBase control)
        {
            control.FirstName = g.Member.GetAttributeText(g.Brand, "SiteFirstName");
            control.LastName = g.Member.GetAttributeText(g.Brand, "SiteLastName");

            if (String.IsNullOrEmpty(control.FirstName))
            {
                control.FirstName = g.Member.GetUserName(_g.Brand);
                control.LastName = "";
            }

        }
    }
}