﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Analysis.ascx.cs" Inherits="Matchnet.Web.Applications.ColorCode.Analysis" %>

<div id="page-container" class="clearfix cc-content-main cc-content-no-bars">
    <h2 class="tagline">The Color Code <small class="sans-serif">- Understand yourself and your matches on a deeper level.</small></h2>
    <a href="/Applications/MemberServices/ChemistrySettings.aspx" class="link-secondary">
        <asp:Literal ID="literalShowSetting1" runat="server" Text="Show your Color Code results"></asp:Literal>
        <asp:Literal ID="literalHideSetting1" runat="server" Text="Hide your Color Code results"></asp:Literal>
    </a>
    <%--Appropriate Analysis control will render here --%>
    <asp:PlaceHolder ID="phAnalysisControl" runat="server"></asp:PlaceHolder>
    
</div><!-- end #page-container -->
<p id="cc-copy" class="clear-both">&copy; 2010 Color Code Communications, Inc. All rights reserved.</p>


