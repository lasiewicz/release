﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.Web.Framework;

namespace Matchnet.Web.Applications.ColorCode.Controls
{
    public class ColorCodeControl : FrameworkControl
    {
        protected MemberQuiz _MemberQuiz;
        protected ReportType _ReportType;
        protected string _ColorText;
        protected string _BlueScore;
        protected string _RedScore;
        protected string _WhiteScore;
        protected string _YellowScore;
        protected string _UserName = "";

        #region Properties
        public MemberQuiz MemberQuiz
        {
            get { return _MemberQuiz; }
            set { _MemberQuiz = value; }
        }

        public string ColorText
        {
            get { return _ColorText; }
            set { _ColorText = value; }
        }

        public string BlueScore
        {
            get { return _BlueScore; }
            set { _BlueScore = value; }
        }

        public string RedScore
        {
            get { return _RedScore; }
            set { _RedScore = value; }
        }

        public string WhiteScore
        {
            get { return _WhiteScore; }
            set { _WhiteScore = value; }
        }

        public string YellowScore
        {
            get { return _YellowScore; }
            set { _YellowScore = value; }
        }

        public string UserName
        {
            get { return _UserName; }
            set { _UserName = value; }
        }

        #endregion

        #region Public/Protected Methods
        /// <summary>
        /// Loads the member's specific analysis report information; 
        /// Derived classes should override and provide specific implementation.
        /// </summary>
        public virtual void LoadReport(MemberQuiz memberQuiz, ReportType reportType)
        {
            _MemberQuiz = memberQuiz;
            _ReportType = reportType;

            switch (memberQuiz.PrimaryColor)
            {
                case Color.blue:
                    _ColorText = "Blue";
                    break;
                case Color.red:
                    _ColorText = "Red";
                    break;
                case Color.white:
                    _ColorText = "White";
                    break;
                case Color.yellow:
                    _ColorText = "Yellow";
                    break;
            }

            _BlueScore = memberQuiz.Scores[Color.blue].ToString("N2");
            _RedScore = memberQuiz.Scores[Color.red].ToString("N2");
            _WhiteScore = memberQuiz.Scores[Color.white].ToString("N2");
            _YellowScore = memberQuiz.Scores[Color.yellow].ToString("N2");

            _UserName = String.IsNullOrEmpty(g.Member.GetUserName(_g.Brand)) ? g.Member.MemberID.ToString() : g.Member.GetUserName(_g.Brand);
        }

        #endregion
    }
}
