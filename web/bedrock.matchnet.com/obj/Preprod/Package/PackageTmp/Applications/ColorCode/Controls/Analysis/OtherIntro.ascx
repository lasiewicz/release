﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OtherIntro.ascx.cs" Inherits="Matchnet.Web.Applications.ColorCode.Controls.Analysis.OtherIntro" %>
<%@ Register src="AnalysisNav.ascx" tagname="AnalysisNav" tagprefix="uc1" %>
<%@ Register src="AnalysisSideBar.ascx" tagname="AnalysisSideBar" tagprefix="uc1" %>

    <div class="wrapper">
        <div id="cc-content">
            <div class="editorial">
                <div class="cc-color-banner cc-<%=ColorText.ToLower() %>-bg">Your Color Code Personality: <span>Learning About Others</span></div>

                <h1>Love Potion No. 9</h1>
                <p>So we've spent some time talking about you and what makes you more (or less) attractive to the opposite sex, so now you may be asking yourself, 'What can I do to create positive chemistry with each of the other Colors?' For example, let's say the person you're interested in is a <span class="cc-yellow">YELLOW</span>. You know that there are certain things that you can do to better peak their interest in you; likewise there are other things that just may put you so far out of the game that no love potion could ever save you.</p>
                <p>Of course, in order to sustain a positive relationship with any Color, you must be sincere while following these tips. Manipulation will get you nowhere, and is the worst possible thing on which to build a relationship. A strong relationship will come through increasing your self-awareness, acting out of pure motives (legitimate reasons for doing things; or acting in such a way that makes win-win situations possible), and stretching to get over yourself in a way that allows you to sincerely make the relationship about your partner and not about yourself (i.e., getting what you want first; selfishness).</p>
                <p class="text-outside">Next, learn about&nbsp;<a href="Analysis.aspx?report=otherR"> the color <span class="cc-red">RED</span> &raquo;</a></p>
            </div><!-- end .editorial-->

            <!--Analysis common navigation-->
            <uc1:AnalysisNav ID="AnalysisNavControl" runat="server"></uc1:AnalysisNav>

        </div><!-- end .cc-content -->
    </div><!-- end .wrapper -->

    <!--Analysis sidebar-->
    <uc1:AnalysisSideBar ID="AnalysisSideBar1" runat="server"></uc1:AnalysisSideBar>
