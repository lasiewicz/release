﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Need.ascx.cs" Inherits="Matchnet.Web.Applications.ColorCode.Controls.Analysis.Need" %>
<%@ Register src="AnalysisNav.ascx" tagname="AnalysisNav" tagprefix="uc1" %>
<%@ Register src="AnalysisSideBar.ascx" tagname="AnalysisSideBar" tagprefix="uc1" %>

    <div class="wrapper">
        <div id="cc-content">

            <div class="editorial">

                <asp:PlaceHolder ID="phBlue" runat="server" Visible="false">
                    <!-- start blue -->
                    <div class="cc-color-banner cc-blue-bg">Your Color Code Personality: <span class="cc-blue">BLUE</span></div>

                    <h1>More About You &ndash; Your Needs</h1>

                    <p>Now that you know how others see you, you should also know that there are certain things that you subconsciously need from your relationships in order to feel fulfilled and happy. These are your very own little happy buttons. When you find people who can push them for you, you will have relationships that you treasure. </p>

                    <h2>You Need Others to Understand You</h2>
                    <p>As a <span class="cc-blue">BLUE</span>, driven by intimacy, you seek deep, personal connections with others. That doesn't just mean that you want to understand everything about them. You wouldn't feel that your relationship was complete unless they understood you completely either. You should look for others who can move beyond superficial conversation and are willing to understand every bit about what makes you you. </p>

                    <h2>You Need to Feel Appreciated By Others</h2>
                    <p>You love to give openly and always go the extra mile to please others. All you desire in return is that they appreciate the effort that you make to do what you do. You will be happiest in finding someone who is comfortable and open in expressing that appreciation and who doesn't take your 110% effort for granted. </p>
                
                    <h2>You Need to Be Good Morally</h2>
                    <p>You have a personal code of ethics that you believe in and a very real sense of right and wrong that you take very seriously. In fact, <span class="cc-blue">BLUES</span> have the strongest sense of integrity of all of the Colors (for example, you would rather lose when playing a game than cheat). You need people who support you in your beliefs, and ideally, would like others to follow the same guidelines and have the same commitment to integrity that you do. </p>
                
                    <h2>You Need General Acceptance</h2>
                    <p>As a <span class="cc-blue">BLUE</span>, you really do care about other people and what they think of you. The <span class="cc-blue">BLUE</span> personality is the only of the four Colors that is dependent on others to fulfill their Core Motive of intimacy, which requires human interaction. Therefore, other people's opinions (and especially those of your circle of friends and family) really do mean something to you, so you want to feel comfortable and accepted by them.</p>
                
                </asp:PlaceHolder>
                
                <asp:PlaceHolder ID="phRed" runat="server" Visible="false">
                    <!-- start red -->
                    <div class="cc-color-banner cc-red-bg">Your Color Code Personality: <span class="cc-red">RED</span></div>

                    <h1>More About You &ndash; Your Needs</h1>

                    <p>Now that you know how others see you, you should also know that there are certain things that you subconsciously need from your relationships in order to feel fulfilled and happy. These are your very own little happy buttons. When you find people who can push them for you, you will have relationships that you treasure. </p>

                    <h2>You Need Respect</h2>
                    <p>As a <span class="cc-red">RED</span>, you naturally want to be respected. In fact, it may be more important for you to be respected than loved. You also need for others to be worthy (and if needs be, demanding) of your respect. How can you love what you can't respect? You appreciate others more if they demand your respect than if they simply allow you free license to walk all over them. </p>

                    <h2>You Need to Appear Knowledgeable</h2>
                    <p>You value your intellectual and logic-based abilities. You consider your ability to reason and problem-solve to be one of your greatest attributes. You want others to value this about you as well and to promote this image to others. If someone were to ever embarrass you publicly, you would have a hard time ever getting over it. </p>

                    <h2>You Need to Be Right</h2>
                    <p>You are decisive and need to be right about things. The truth of it is that you may have a rather fragile ego and typically resent personal attacks. You should find friends who support you in your correct decisions, but who can also challenge your incorrect decisions in a way that frees you to pay attention while preserving your dignity. </p>

                    <h2>You Need Approval</h2>
                    <p>As a <span class="cc-red">RED</span>, you need approval from those whom you hold in high regard (as opposed to the general masses) and for others to show you that they approve of and respect you.</p>
                </asp:PlaceHolder>
                
                <asp:PlaceHolder ID="phWhite" runat="server" Visible="false">
                    <!--white-->
                    <div class="cc-color-banner cc-white-bg">Your Color Code Personality: <span class="cc-white">WHITE</span></div>

                    <h1>More About You &ndash; Your Needs</h1>
                    <p>Now that you know how others see you, you should also know that there are certain things that you subconsciously need from your relationships in order to feel fulfilled and happy. These are your very own little happy buttons. When you find people who can push them for you, you will have relationships that you treasure. </p>

                    <h2>You Need to Feel Good Inside</h2>
                    <p>As a <span class="cc-white">WHITE</span>, feeling good and comfortable on the inside is more important to you than being good or doing the right thing, so you should look for friends who do two things for you. One, they should not create unnecessary conflict or confrontation, and two; they need to be able to help you become comfortable confronting the necessary issues and not allow you an escape route through dishonesty. </p>

                    <h2>You Need to Be Allowed Your Own Space</h2>
                    <p>You enjoy being with people, but you do not need constant social interaction by any stretch of the imagination. You like your alone time, which allows you to process thoughts and to daydream. You should ask others to allow you to have your release time. </p>

                    <h2>You Need to Be Respected</h2>
                    <p><span class="cc-white">WHITES </span> are very tolerant, accepting people. You respect and value diversity and differences in others, and you want that same respect and consideration in return. Also, you have a very logical mind which allows you to excel in many different areas. </p>

                    <h2>You Need Your Partner to Be Tolerant</h2>
                    <p>You are a very patient person. You are willing to give people a chance to figure things out, even when they mess up. You don't easily get offended or rubbed the wrong way by idiosyncrasies, and you need to have the same level of tolerance in return.</p>
                </asp:PlaceHolder>
                
                <asp:PlaceHolder ID="phYellow" runat="server" Visible="false">
                    <!--yellow-->
                    <div class="cc-color-banner cc-yellow-bg">Your Color Code Personality: <span class="cc-yellow">YELLOW</span></div>

                    <h1>More About You &ndash; Your Needs</h1>
                    <p>Now that you know how others see you, you should also know that there are certain things that you subconsciously need from your relationships in order to feel fulfilled and happy. These are your very own little happy buttons. When you find people who can push them for you, you will have relationships that you treasure. </p>

                    <h2>You Need to Look Good Socially</h2>
                    <p>You pride yourself on your people skills, and are terribly embarrassed by social faux pas. The way that others both perceive you socially and enhance your appearance socially is a big deal to you, so find people with whom you feel comfortable in a social environment. </p>

                    <h2>You Need to Be Noticed By Others</h2>
                    <p>Because you do have a sense of flair and flash, you don't want it to go unnoticed. People's opinions are especially important to you, and you want to know that they see what you're doing. The worst possible thing for you would be to spend time with a person who ignores you. You would be devastated. </p> 

                    <h2>You Need Approval From The Masses</h2>
                    <p>You have the desire to be popular and appeal to the masses, not for your intellect, but for your social skills. You like to know that people adore you and that you fit in well. You should choose friends who can support this need of yours in social environments. </p>

                    <h2>You Need Your Praise And Adoration</h2>
                    <p>The best way to win a <span class="cc-yellow">YELLOW'S</span> attention and affection is to let them know that they are adored and worthy of praise. If others can demonstrate this to you, and you know that they hold you high on a pedestal, you will be in seventh heaven.</p>
                </asp:PlaceHolder>
                
                <p class="text-outside">Next, find out about&nbsp;<a href="Analysis.aspx?report=want"> Your Wants &raquo;</a></p>
 
            </div><!-- end .editorial-->	

            <!--Analysis common navigation-->
            <uc1:AnalysisNav ID="AnalysisNavControl" runat="server"></uc1:AnalysisNav>

        </div><!-- end .cc-content -->
    </div><!-- end .wrapper -->

   
    <!--Analysis sidebar-->
    <uc1:AnalysisSideBar ID="AnalysisSideBar1" runat="server"></uc1:AnalysisSideBar>
