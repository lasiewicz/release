﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Gifts.ascx.cs" Inherits="Matchnet.Web.Applications.ColorCode.ComprehensiveAnalysisControls.Gifts" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="uc2" TagName="Traits" Src="~/Applications/ColorCode/ComprehensiveAnalysisControls/Traits.ascx" %>

<%--	<h2 class="title"><mn:Txt runat="server" ID="txtTitle" ResourceConstant="TXT_SECTION_TITLE" /></h2>
	<div class="<asp:Literal id=litPrimaryColorClass runat=server />">
		<mn:Txt runat="server" ID="txt1" ResourceConstant="TXT_SECTION_COLOR_TITLE" />
	</div>--%>
	<h2 class="title"><mn:Txt runat="server" ID="txtSubTitle"  ResourceConstant="TXT_SECTION_SUB_TITLE"/></h2>
	
	<div class="margin-medium">
	<p>
	<mn:Image runat="server" height="23" width="157"  ID="imgActivity"/>
	</p>
	<uc2:Traits runat="server" ID="ucTraits" />
			
	<mn:Txt runat="server" ID="txtContent1" />
	</div><!-- end .margin-medium -->
	
	<div class="bottom_nav">
		<h4 class="float-inside"><a href="/Applications/ColorCode/ComprehensiveAnalysis.aspx?section=getcoremotive"><mn:Txt runat="server" ID="txtBack" ResourceConstant="TXT_BACK_LNK" /></a></h4>
		<h4 class="float-outside"><a href="<asp:Literal id=litForward runat=server/>"><mn:Txt runat="server" ID="txtForward"  /></a></h4>
	</div>