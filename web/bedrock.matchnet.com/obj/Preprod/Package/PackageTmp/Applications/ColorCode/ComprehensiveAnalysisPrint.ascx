﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ComprehensiveAnalysisPrint.ascx.cs" Inherits="Matchnet.Web.Applications.ColorCode.ComprehensiveAnalysisPrint" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<script type="text/javascript">
    $j(document).ready(function() {
        $j(".bottom_nav").css({display:"none"});
        $j("body").css({background:"none"});
    });
</script>

<div id="page-container" class="cc-content-main cc-content-bars-small">
<a href="javascript:window.print()"><mn:Txt runat="server" id="txtPrint"  ResourceConstant="TXT_PRINT" /></a>

<div id="cc-content">
<div class="editorial clearfix">
<h1><mn:Txt runat="server" ID="txtComprehensiveAnalysis" /></h1>

<div id="ccc-content" class="ccc-print">

	<div id="ccc-body">
	    <asp:PlaceHolder ID="phContent" runat="server" />
	    <p id="cc-copy"><mn:Txt ID="txtCopyrights"  runat="server"/></p>
	    <script type="text/javascript">
            $j(document).ready(function() {
                $j(".ccc-pie-chart-loading").css({display:"none"});
            });
        </script>
	</div><%-- end #ccc-body --%>
	
</div><%-- end #ccc-content --%>
</div><%-- end .editotial --%>
</div><%-- end #cc-content --%>
</div><%-- end #page-container --%>