﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;

namespace Matchnet.Web.Applications.ColorCode
{
    public class MemberQuiz : Quiz
    {
        Color _PrimaryColor = Color.none;
        Color _SecondaryColor = Color.none;
        Dictionary<Color, decimal> _Scores = new Dictionary<Color, decimal>();
        DateTime _QuizCompleteDate = DateTime.MinValue;

        #region Properties
        public Color PrimaryColor
        {
            get { return _PrimaryColor; }
            set { _PrimaryColor = value; }
        }

        public Color SecondaryColor
        {
            get { return _SecondaryColor; }
            set { _SecondaryColor = value; }
        }

        public Dictionary<Color, decimal> Scores
        {
            get { return _Scores; }
            set { _Scores = value; }
        }

        public DateTime QuizCompleteDate
        {
            get { return _QuizCompleteDate; }
            set { _QuizCompleteDate = value; }
        }

        #endregion

        #region Public Methods
        public void AddQuestionAnswer(int questionID, int choiceID, Quiz quizReference)
        {
            if (this.FixedAdjustFactor == Constants.NULL_DECIMAL)
            {
                this.FixedAdjustFactor = quizReference.FixedAdjustFactor;
                this.FixedTotalBlue = quizReference.FixedTotalBlue;
                this.FixedTotalRed = quizReference.FixedTotalRed;
                this.FixedTotalWhite = quizReference.FixedTotalWhite;
                this.FixedTotalYellow = quizReference.FixedTotalYellow;
            }

            Question quizQuestion = quizReference.GetQuestion(questionID);
            Choice quizChoice = quizQuestion.GetChoice(choiceID);

            Question memberQuestion = this.GetQuestion(questionID);
            if (memberQuestion != null)
            {
                memberQuestion.Choices.Clear();

                Choice memberChoice = new Choice();
                memberChoice.ID = quizChoice.ID;
                memberChoice.Color = quizChoice.Color;
                memberChoice.Weight = quizChoice.Weight;
                memberQuestion.Choices.Add(memberChoice);
            }
            else
            {
                memberQuestion = new Question();
                memberQuestion.ID = quizQuestion.ID;
                memberQuestion.QuestionType = quizQuestion.QuestionType;

                Choice memberChoice = new Choice();
                memberChoice.ID = quizChoice.ID;
                memberChoice.Color = quizChoice.Color;
                memberChoice.Weight = quizChoice.Weight;
                memberQuestion.Choices.Add(memberChoice);

                if (!this.Questions.Keys.Contains(quizQuestion.QuestionType))
                    this.Questions.Add(quizQuestion.QuestionType, new List<Question>());

                this.Questions[quizQuestion.QuestionType].Add(memberQuestion);
            }

        }

        public void CalculateResults(Quiz quizReference)
        {
            //Sum up the weight for each answer
            Dictionary<Color, decimal> Sums = new Dictionary<Color, decimal>();
            Sums[Color.blue] = 0m;
            Sums[Color.red] = 0m;
            Sums[Color.white] = 0m;
            Sums[Color.yellow] = 0m;

            foreach (QuestionType qt in Questions.Keys)
            {
                foreach (Question q in Questions[qt])
                {
                    foreach (Choice c in q.Choices)
                    {
                        if (c.Color == Color.none || c.Weight == Constants.NULL_DECIMAL || c.Weight <= 0)
                        {
                            Choice refChoice = quizReference.GetQuestion(q.ID).GetChoice(c.ID);
                            c.Weight = refChoice.Weight;
                            c.Color = refChoice.Color;
                        }

                        Sums[c.Color] += c.Weight;
                    }
                }
            }

            //Adjusted Score = Sum * (Adjust Factor / Possible)
            decimal blueAdjustedScore = Sums[Color.blue] * (FixedAdjustFactor / FixedTotalBlue);
            decimal redAdjustedScore = Sums[Color.red] * (FixedAdjustFactor / FixedTotalRed);
            decimal whiteAdjustedScore = Sums[Color.white] * (FixedAdjustFactor / FixedTotalWhite);
            decimal yellowAdjustedScore = Sums[Color.yellow] * (FixedAdjustFactor / FixedTotalYellow);

            //Total Adjusted Score
            decimal totalAdjustedScore = blueAdjustedScore + redAdjustedScore + whiteAdjustedScore + yellowAdjustedScore;

            //Total score for each color = (Adjusted Score / Total Adjusted Score ) * 100
            Scores.Clear();
            Scores[Color.blue] = (blueAdjustedScore / totalAdjustedScore) * 100;
            Scores[Color.red] = (redAdjustedScore / totalAdjustedScore) * 100;
            Scores[Color.white] = (whiteAdjustedScore / totalAdjustedScore) * 100;
            Scores[Color.yellow] = (yellowAdjustedScore / totalAdjustedScore) * 100;

            //Primary and Secondary Color
            decimal[] arrScores = Scores.Values.ToArray();
            Array.Sort(arrScores); //sorts by ascending order for primitive types
            foreach (Color c in Scores.Keys)
            {
                if (Scores[c] == arrScores[3]) //fourth (last) value in array should be biggest
                {
                    PrimaryColor = c;
                }
                else if (Scores[c] == arrScores[2]) //third value in array should be second biggest
                {
                    SecondaryColor = c;
                }
            }

        }

        public int GetSelectedChoiceID(int questionID)
        {
            int choiceID = Constants.NULL_INT;

            Question memberQuestion = GetQuestion(questionID);
            if (memberQuestion != null && memberQuestion.Choices.Count > 0)
            {
                choiceID = memberQuestion.Choices[0].ID;
            }

            return choiceID;
        }

        public bool HasTakenQuiz()
        {
            bool hasTakenQuiz = false;

            if (this.Questions != null && this.Questions.ContainsKey(QuestionType.part1) && this.Questions[QuestionType.part1].Count > 0)
            {
                hasTakenQuiz = true;
            }

            return hasTakenQuiz;
        }

        public bool IsQuizPart1Complete()
        {
            bool isComplete = false;

            if (this.Questions == null || !this.Questions.ContainsKey(QuestionType.part1) || this.Questions[QuestionType.part1].Count < 30)
            {
                isComplete = false;
            }
            else if (this.Questions[QuestionType.part1].Count == 30)
            {
                isComplete = true;
            }

            return isComplete;
        }

        public bool IsQuizComplete()
        {
            bool isComplete = false;

            if (this.Questions == null || !this.Questions.ContainsKey(QuestionType.part1) || this.Questions[QuestionType.part1].Count < 30
                || !this.Questions.ContainsKey(QuestionType.part2) || this.Questions[QuestionType.part2].Count < 15)
            {
                isComplete = false;
            }
            else if (this.Questions[QuestionType.part1].Count == 30 && this.Questions[QuestionType.part2].Count == 15)
            {
                isComplete = true;
            }

            return isComplete;
        }

        #endregion
    }
}
