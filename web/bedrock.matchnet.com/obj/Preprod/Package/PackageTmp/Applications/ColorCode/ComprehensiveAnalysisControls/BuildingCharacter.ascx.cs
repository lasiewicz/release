﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Matchnet.Web.Applications.ColorCode.ComprehensiveAnalysisControls
{
    public partial class BuildingCharacter :ComprehensiveAnalysisBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            List<OptionTrait> characterTraits=null;
            List<OptionTrait> disfTraits = null;

            if (SecondaryColorCode != Color.none)
            {
                characterTraits = SecondaryColorStrength.Union<OptionTrait>(ThirdColorStrength).Union<OptionTrait>(FourthColorStrength).ToList<OptionTrait>();
                 disfTraits = SecondaryColorLimitation.Union<OptionTrait>(ThirdColorLimitation).Union<OptionTrait>(FourthColorLimitation).ToList<OptionTrait>();
            }
           
            rptCharacterTraits.DataSource = characterTraits;
            rptCharacterTraits.DataBind();

            rptHealthyTraits.DataSource = PrimaryColorStrength;
            rptHealthyTraits.DataBind();

            rptUnhealthyTraits.DataSource = PrimaryColorLimitation;
            rptUnhealthyTraits.DataBind();

            rptDysfuncTraits.DataSource = disfTraits;
            rptDysfuncTraits.DataBind();

            txtContent.Text = g.GetResource("TXT_CONTENT" ,new string[] { PrimaryColorCode.ToString().ToLower().Substring(0, 1) },true,this);
            litPrimaryColorClass.Text = BackgroundClass;
            txtContent1.Text = g.GetResource("TXT_CONTENT1", new string[] { PrimaryColor.ToUpper()}, true, this);
            if (SecondaryColorCode != Color.none)
            {
                litBack.Text = "/Applications/ColorCode/ComprehensiveAnalysis.aspx?section=leveragingsecondary";
                txtBack.Text = g.GetResource("TXT_BACK1", this);
            }
            else
            {


                litBack.Text = "/Applications/ColorCode/ComprehensiveAnalysis.aspx?section=gifts";
                txtBack.Text = g.GetResource("TXT_BACK2", this);
            }
        }

        protected void BindTraits(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                OptionTrait trait = (OptionTrait)e.Item.DataItem;

                Literal litTrait = (Literal)e.Item.FindControl("litTrait");

                litTrait.Text = trait.TraitDisplay;





            }
            catch (Exception ex)
            { g.ProcessException(ex); }

        }
    }
}