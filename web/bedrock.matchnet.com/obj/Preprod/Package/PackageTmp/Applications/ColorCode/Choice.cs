﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Matchnet.Web.Applications.ColorCode
{
    [Serializable]
    public class Choice
    {
        int _ID = Constants.NULL_INT;
        decimal _Weight = Constants.NULL_DECIMAL;
        Color _Color = Color.none;
        string _Text = "";

        #region Properties
        public int ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        public decimal Weight
        {
            get { return _Weight; }
            set { _Weight = value; }
        }

        public Color Color
        {
            get { return _Color; }
            set { _Color = value; }
        }

        public string Text
        {
            get { return _Text; }
            set { _Text = value; }
        }

        #endregion

    }
}
