﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Purchase.ValueObjects;
using Matchnet.Web.Framework;
using Matchnet.Web.Applications.Subscription.UnifiedPaymentSystem;

namespace Matchnet.Web.Applications.ColorCode
{
    public partial class CompPurchase : FrameworkControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!ColorCodeHelper.HasMemberCompletedQuiz(g.Member, g.Brand))
            {
                g.Transfer(FrameworkGlobals.LinkHref("/Applications/ColorCode/Landing.aspx", true));
            }

            // Check to see if the user has already purchased. Should not be allowed on this page.
            if (g.Member.GetAttributeBool(g.Brand, "ColorAnalysis"))
            {
                g.Transfer(FrameworkGlobals.LinkHref("/Applications/ColorCode/ComprehensiveAnalysis.aspx", true));
            }

            if (Convert.ToBoolean((RuntimeSettings.GetSetting("IS_UPS_INTEGRATION_ENABLED", g.Brand.Site.Community.CommunityID,
                g.Brand.Site.SiteID, g.Brand.BrandID))))
            {
                try
                {
                    PaymentUIConnector connector = new PaymentUIConnector();

                    ColorCodeCompPurchasePage page = new ColorCodeCompPurchasePage();

                    ((IPaymentUIJSON)page).PaymentType = PaymentType.CreditCard;
                    ((IPaymentUIJSON)page).SaveLegacyData = true;

                    connector.RedirectToPaymentUI(page);

                    return;
                }
                catch (Exception ex)
                {
                    g.ProcessException(ex);

                    return;
                }
            }

            return;
        }
    }
}