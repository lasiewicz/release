﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
 using Matchnet.Web.Framework;
namespace Matchnet.Web.Applications.ColorCode.ComprehensiveAnalysisControls
{
    public partial class Traits : ComprehensiveAnalysisBase
    {
        bool showNav = true;
        public FrameworkControl ResourceControl{get;set;}
        public bool ShowNavigation { get { return showNav; } set { showNav = value; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (ResourceControl == null)
                ResourceControl = this;
        

            txtContent.Text = g.GetResource("TXT_CONTENT", ResourceControl, new string[] { FirstName });
            rptPrimaryStrength.DataSource = PrimaryColorStrength;
            rptPrimaryStrength.DataBind();

            rptSecStrength.DataSource = SecondaryColorStrength;
            rptSecStrength.DataBind();

            rptTercStrength.DataSource = ThirdColorStrength;
            rptTercStrength.DataBind();
            rptQuartStrength.DataSource = FourthColorStrength;
            rptQuartStrength.DataBind();

            rptPrimaryLimit.DataSource = PrimaryColorLimitation;
            rptPrimaryLimit.DataBind();

            rptSecLimit.DataSource = SecondaryColorLimitation;
            rptSecLimit.DataBind();

            rptTercLimit.DataSource = ThirdColorLimitation;
            rptTercLimit.DataBind();
            rptQuadLimit.DataSource = FourthColorLimitation;
            rptQuadLimit.DataBind();


            if (SecondaryColorCode == Color.none)
            {
                MemberQuiz _memberquiz = ColorCodeHelper.GetMemberQuiz(g.Member, g.Brand);




                IOrderedEnumerable<Color> scores = (from s in _memberquiz.Scores.Keys
                                                    orderby _memberquiz.Scores[s] descending
                                                    select s);


                SecondaryColorCode = scores.ElementAt<Color>(1);
                ThirdColorCode = scores.ElementAt<Color>(2);
                FourthColorCode = scores.ElementAt<Color>(3);

                SecondaryColor = scores.ElementAt<Color>(1).ToString().ToUpper();
                ThirdColor = scores.ElementAt<Color>(2).ToString().ToUpper();
                FourthColor = scores.ElementAt<Color>(3).ToString().ToUpper();
              

            }

            litPrimaryColorClass.Text = GetTraitBackGroundClass(PrimaryColorCode);
            litPrimaryColorClass1.Text = GetTraitBackGroundClass(PrimaryColorCode);

            litSecColorClass.Text = GetTraitBackGroundClass(SecondaryColorCode);
            litSecColorClass1.Text = GetTraitBackGroundClass(SecondaryColorCode);

            litTercColorClass.Text = GetTraitBackGroundClass(ThirdColorCode);
            litTercColorClass1.Text = GetTraitBackGroundClass(ThirdColorCode);

            litQuartColorClass.Text = GetTraitBackGroundClass(FourthColorCode);
            litQuartColorClass1.Text = GetTraitBackGroundClass(FourthColorCode);


            txtPrimaryColor.Text = PrimaryColor.ToUpper();
            txtPrimaryColor1.Text = PrimaryColor.ToUpper();

            txtSecColor.Text = SecondaryColor.ToUpper();
            txtSecColor1.Text = SecondaryColor.ToUpper();

            txtTercColor.Text = ThirdColor.ToUpper();
            txtTercColor1.Text = ThirdColor.ToUpper();

            txtQuartColor.Text = FourthColor.ToUpper();
            txtQuadColor1.Text = FourthColor.ToUpper();

            phNavigation.Visible = ShowNavigation;
            txtSubTitle.Visible = ShowNavigation;
            //foreach(OptionTrait t in map.OptionTraitList)
            //{
            //Response.Write("<data name=\"TRAIT_"  + t.TraitResource + "\" xml:space=\"preserve\">\r\n");
            //Response.Write("<value>" + t.Trait + "</value>\r\n");
            //Response.Write("</data>\r\n");
            //}
            //Response.Flush();
        }


        protected void BindTraits(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                OptionTrait trait = (OptionTrait)e.Item.DataItem;

                Literal litTrait = (Literal)e.Item.FindControl("litTrait");

                litTrait.Text = trait.TraitDisplay;





            }
            catch (Exception ex)
            { g.ProcessException(ex); }

        }
    }
}