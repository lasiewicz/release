﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:2.0.50727.3082
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Matchnet.Web.Applications.ColorCode.Controls.Analysis {
    
    
    public partial class Other {
        
        /// <summary>
        /// phOtherRed control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.PlaceHolder phOtherRed;
        
        /// <summary>
        /// phOtherBlue control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.PlaceHolder phOtherBlue;
        
        /// <summary>
        /// phOtherWhite control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.PlaceHolder phOtherWhite;
        
        /// <summary>
        /// phOtherYellow control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.PlaceHolder phOtherYellow;
        
        /// <summary>
        /// AnalysisNavControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Matchnet.Web.Applications.ColorCode.Controls.Analysis.AnalysisNav AnalysisNavControl;
        
        /// <summary>
        /// AnalysisSideBar1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Matchnet.Web.Applications.ColorCode.Controls.Analysis.AnalysisSideBar AnalysisSideBar1;
    }
}
