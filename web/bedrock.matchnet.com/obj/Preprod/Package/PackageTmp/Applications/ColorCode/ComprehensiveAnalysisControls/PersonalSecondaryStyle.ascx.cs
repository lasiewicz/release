﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Matchnet.Web.Applications.ColorCode.ComprehensiveAnalysisControls
{
    public partial class PersonalSecondaryStyle : ComprehensiveAnalysisBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            txtSubTitle.Text = g.GetResource("TXT_SECTION_SUB_TITLE", this, new string[] { PrimaryColor.ToUpper(), SecondaryColor.ToUpper() });

            txtContent.Text = g.GetResource("TXT_MOTIVE_" + PrimaryColorCode.ToString().ToUpper() + "_" + SecondaryColor.ToUpper(), this, new string[] { FirstName });
            txtForward.Text = g.GetResource("TXT_FORWARD_LNK", this, new string[] { Weight });
        }
    }
}