﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Web.Framework;

namespace Matchnet.Web.Applications.ColorCode
{
    public partial class QuizConfirmation : FrameworkControl
    {
        protected MemberQuiz _memberQuiz;
        protected string _ColorText = "";
        protected string _BlueScore = "";
        protected string _RedScore = "";
        protected string _WhiteScore = "";
        protected string _YellowScore = "";
        protected string _UserName = "";

        #region Properties
        public string ColorText
        {
            get { return _ColorText; }
            set { _ColorText = value; }
        }

        public string BlueScore
        {
            get { return _BlueScore; }
            set { _BlueScore = value; }
        }

        public string RedScore
        {
            get { return _RedScore; }
            set { _RedScore = value; }
        }

        public string WhiteScore
        {
            get { return _WhiteScore; }
            set { _WhiteScore = value; }
        }

        public string YellowScore
        {
            get { return _YellowScore; }
            set { _YellowScore = value; }
        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (g.Member != null && ColorCodeHelper.IsColorCodeEnabled(g.Brand))
            {
                //get member's quiz
                _memberQuiz = ColorCodeHelper.GetMemberQuiz(g.Member, g.Brand);

                //check if member has already completed the quiz
                if (_memberQuiz.IsQuizComplete())
                {
                    switch (_memberQuiz.PrimaryColor)
                    {
                        case Color.blue:
                            _ColorText = "Blue";
                            break;
                        case Color.red:
                            _ColorText = "Red";
                            break;
                        case Color.white:
                            _ColorText = "White";
                            break;
                        case Color.yellow:
                            _ColorText = "Yellow";
                            break;
                    }

                    _BlueScore = _memberQuiz.Scores[Color.blue].ToString("N2");
                    _RedScore = _memberQuiz.Scores[Color.red].ToString("N2");
                    _WhiteScore = _memberQuiz.Scores[Color.white].ToString("N2");
                    _YellowScore = _memberQuiz.Scores[Color.yellow].ToString("N2");

                    AnalysisSideBar1.LoadReport(_memberQuiz, ReportType.none);

                    _UserName = String.IsNullOrEmpty(g.Member.GetUserName(_g.Brand)) ? g.Member.MemberID.ToString() : g.Member.GetUserName(_g.Brand);
                }
                else
                {
                    g.Transfer("/Applications/ColorCode/Landing.aspx");
                }
            }
            else
            {
                g.Transfer("/Applications/ColorCode/Landing.aspx");
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            //show appropriate right content
            g.RightNavControl.ShowColorCodeAbout();
            g.RightNavControl.HideGamChannels();

            //omniture
            g.AnalyticsOmniture.AddEvent("event36"); //test complete
            g.AnalyticsOmniture.Prop6 = "Y"; //test complete

            base.OnPreRender(e);
        }
    }
}