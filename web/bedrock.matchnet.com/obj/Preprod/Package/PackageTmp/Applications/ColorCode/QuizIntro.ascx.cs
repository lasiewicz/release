﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Web.Framework;

namespace Matchnet.Web.Applications.ColorCode
{
    public partial class QuizIntro : FrameworkControl
    {
        MemberQuiz _memberQuiz;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (g.Member != null && ColorCodeHelper.IsColorCodeEnabled(g.Brand))
            {
                _memberQuiz = ColorCodeHelper.GetMemberQuiz(g.Member, g.Brand);

                if (_memberQuiz.IsQuizComplete())
                {
                    g.Transfer("/Applications/ColorCode/Analysis.aspx");
                }
            }
            else
            {
                g.Transfer("/Applications/ColorCode/Landing.aspx");
            }
        }

        protected override void  OnPreRender(EventArgs e)
        {
            //show appropriate right content
            g.RightNavControl.ShowColorCodeFeedback();
            g.RightNavControl.HideGamChannels();

            if (!Page.IsPostBack)
            {
                //color test link tracking (available on email, force, banner links that brought user here)
                ColorCodeHelper.HandleColorCodeTestLinkTracking(Request, g);
            }
 	         base.OnPreRender(e);
        }
    }
}