﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Web.Framework;
using Matchnet.Web.Applications.ColorCode.Controls;

namespace Matchnet.Web.Applications.ColorCode
{
    public partial class Analysis : FrameworkControl
    {
        private MemberQuiz _memberQuiz = null;
        private ReportType _reportType = ReportType.intro;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (g.Member != null && ColorCodeHelper.IsColorCodeEnabled(g.Brand))
            {
                //get member's quiz
                _memberQuiz = ColorCodeHelper.GetMemberQuiz(g.Member, g.Brand);

                //check if member has already completed the quiz
                if (_memberQuiz.IsQuizComplete())
                {
                    //load appropriate analysis report page
                    if (Request.QueryString["report"] != null)
                    {
                        try
                        {
                            _reportType = (ReportType)Enum.Parse(typeof(ReportType), Request.QueryString["report"], true);
                        }
                        catch
                        {
                            _reportType = ReportType.intro;
                        }
                    }

                    ColorCodeControl AnalysisControl = null;
                    
                    switch (_reportType)
                    {
                        case ReportType.hot:
                            AnalysisControl = LoadControl("Controls/Analysis/Hot.ascx") as ColorCodeControl;
                            break;
                        case ReportType.matchIntro:
                            AnalysisControl = LoadControl("Controls/Analysis/MatchIntro.ascx") as ColorCodeControl;
                            break;
                        case ReportType.matchB:
                        case ReportType.matchR:
                        case ReportType.matchW:
                        case ReportType.matchY:
                            AnalysisControl = LoadControl("Controls/Analysis/Match.ascx") as ColorCodeControl;
                            break;
                        case ReportType.need:
                            AnalysisControl = LoadControl("Controls/Analysis/Need.ascx") as ColorCodeControl;
                            break;
                        case ReportType.not:
                            AnalysisControl = LoadControl("Controls/Analysis/Not.ascx") as ColorCodeControl;
                            break;
                        case ReportType.otherIntro:
                            AnalysisControl = LoadControl("Controls/Analysis/OtherIntro.ascx") as ColorCodeControl;
                            break;
                        case ReportType.otherB:
                        case ReportType.otherR:
                        case ReportType.otherW:
                        case ReportType.otherY:
                            AnalysisControl = LoadControl("Controls/Analysis/Other.ascx") as ColorCodeControl;
                            break;
                        case ReportType.want:
                            AnalysisControl = LoadControl("Controls/Analysis/Want.ascx") as ColorCodeControl;
                            break;
                        default:
                            AnalysisControl = LoadControl("Controls/Analysis/Intro.ascx") as ColorCodeControl;
                            break;
                        
                    }

                    phAnalysisControl.Controls.Add(AnalysisControl);
                    AnalysisControl.LoadReport(_memberQuiz, _reportType);

                    bool memberHiddenColorCode = ColorCodeHelper.IsMemberColorCodeHidden(g.Member, g.Brand);
                    if (memberHiddenColorCode)
                    {
                        literalHideSetting1.Visible = false;
                        literalShowSetting1.Visible = true;
                    }
                    else
                    {
                        literalHideSetting1.Visible = true;
                        literalShowSetting1.Visible = false;
                    }
                }
                else
                {
                    g.Transfer("/Applications/ColorCode/Landing.aspx");
                }

            }
            else
            {
                g.Transfer("/Applications/ColorCode/Landing.aspx");
            }
        }

        protected override void  OnPreRender(EventArgs e)
        {
            //show appropriate right content
            g.RightNavControl.ShowColorCodeAbout();
            g.RightNavControl.HideGamChannels();

 	         base.OnPreRender(e);
        }
    }
}