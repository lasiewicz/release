﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Matchnet.Web.Applications.ColorCode.ComprehensiveAnalysisControls
{
    public partial class CoreMotive : ComprehensiveAnalysisBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            txtSubTitle.Text = g.GetResource("TXT_SECTION_SUB_TITLE", this, new string[] { CoreMotive.ToUpper() });
            txtContent.Text = g.GetResource("TXT_CONTENT", this, new string[] { PrimaryColor.ToUpper() , CoreMotive.ToUpper()});
            txtContent1.Text = g.GetResource("TXT_MOTIVE_" + PrimaryColorCode.ToString().ToUpper(), this,new string[]{FirstName});
            
        }
    }
}