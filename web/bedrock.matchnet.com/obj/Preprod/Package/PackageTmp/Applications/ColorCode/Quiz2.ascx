﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Quiz2.ascx.cs" Inherits="Matchnet.Web.Applications.ColorCode.Quiz2" %>

<div id="page-container" class="clearfix cc-content-main cc-content-bars-small">

    <h2 class="tagline">The Color Code <small class="sans-serif">- Understand yourself and your matches on a deeper level.</small></h2>
    <div id="cc-content">
        <div class="editorial">
            <h1>The Color Code Personality Test</h1>
            <p>You're almost done! This is the last section. Only 15 more questions to go.</p>
            <h3>Part 2: Situations</h3>
            <p>This section consists of 15 situations with four possible reactions to each. Again, consider the possible reactions in each question and mark the one response that best reflects you AS A CHILD. <a href="#" class="cc-quiz-help">Need Help?</a></p>
        </div>
        <div id="cc-questions-container">
            <asp:Repeater ID="repeaterQuestions" runat="server" OnItemDataBound="RepeaterSections_ItemDataBound">
                <HeaderTemplate><ul class="cc-questions-full clearfix"></HeaderTemplate>
                <ItemTemplate><li>
                    <h2><asp:Literal ID="literalQuestionText" runat="server"></asp:Literal></h2>
                    <ul>
                        <li><asp:Literal ID="radioChoice1" runat="server"></asp:Literal></li>
                        <li><asp:Literal ID="radioChoice2" runat="server"></asp:Literal></li>
                        <li><asp:Literal ID="radioChoice3" runat="server"></asp:Literal></li>
                        <li><asp:Literal ID="radioChoice4" runat="server"></asp:Literal></li>
                    </ul></li>
                </ItemTemplate>
                <FooterTemplate></ul></FooterTemplate>
            </asp:Repeater>
            <%--
            <ul class="cc-questions-full clearfix">
            <li>
	            <h2>1. As a child, when my friend was in trouble, I was:</h2>

	            <ul>
		            <li><input id="q1a1" type="radio" name="question1" /><label for="q1a1">Concerned, empathetic, and loyal -- regardless of the problem</label></li>
		            <li><input id="q1a2" type="radio" name="question1" /><label for="q1a2">Supportive, patient, and a good listener</label></li>
		            <li><input id="q1a3" type="radio" name="question1" /><label for="q1a3">Nonjudgmental, optimistic, and downplaying the seriousness of the situation</label></li>
		            <li><input id="q1a4" type="radio" name="question1" /><label for="q1a4">Protective, resourceful, and recommending of solutions</label></li>
	            </ul>
            </li>
            </ul>
            --%>
        </div>

    </div><!-- end .cc-content-->
    <p class="text-center editorial cc-button-primary">	
	    <asp:LinkButton ID="btnSubmit" runat="server" CssClass="link-primary" OnClick="btnSubmit_Click">Show My Results</asp:LinkButton>
    </p>
    
</div><!-- end #page-container -->
<p id="cc-copy">&copy; 2010 Color Code Communications, Inc. All rights reserved.</p>
