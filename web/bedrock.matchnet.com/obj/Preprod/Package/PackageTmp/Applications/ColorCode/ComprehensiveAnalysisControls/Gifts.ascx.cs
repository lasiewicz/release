﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Web.Framework;
namespace Matchnet.Web.Applications.ColorCode.ComprehensiveAnalysisControls
{
    public partial class Gifts : ComprehensiveAnalysisBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ucTraits.ResourceControl = (FrameworkControl) this;
            ucTraits.PrimaryColor = PrimaryColor;
            ucTraits.PrimaryColorCode = PrimaryColorCode;
            ucTraits.SecondaryColor = SecondaryColor;
            ucTraits.SecondaryColorCode = SecondaryColorCode;
            ucTraits.SecondaryColor = SecondaryColor;
            ucTraits.CoreMotive = CoreMotive;
            ucTraits.ThirdColor = ThirdColor;
            ucTraits.ThirdColorCode = ThirdColorCode;
            ucTraits.FourthColor = FourthColor;
            ucTraits.FourthColorCode = FourthColorCode;
            ucTraits.ShowNavigation = false;
            ucTraits.PrimaryColorStrength = PrimaryColorStrength;
            ucTraits.SecondaryColorStrength = SecondaryColorStrength;
            ucTraits.ThirdColorStrength = ThirdColorStrength;
            ucTraits.FourthColorStrength = FourthColorStrength;
            ucTraits.PrimaryColorLimitation = PrimaryColorLimitation;
            ucTraits.SecondaryColorLimitation = SecondaryColorLimitation;
            ucTraits.ThirdColorLimitation = ThirdColorLimitation;
            ucTraits.FourthColorLimitation = FourthColorLimitation;
            imgActivity.FileName = String.Format("ccc-app-activity-{0}.gif", PrimaryColorCode.ToString().Substring(0,1).ToUpper());

            txtContent1.Text = g.GetResource("TXT_CONTENT1",this, new string[]{ PrimaryColorCode.ToString()});
            if (SecondaryColorCode != Color.none)
            {
                litForward.Text = "/Applications/ColorCode/ComprehensiveAnalysis.aspx?section=leveragingsecondary";
                txtForward.Text = g.GetResource("TXT_FORWARD1", this);
            }
            else
            {

                litForward.Text = "/Applications/ColorCode/ComprehensiveAnalysis.aspx?section=buildingcharacter";
                txtForward.Text = g.GetResource("TXT_FORWARD2", this);
            }

        }

    }
}
