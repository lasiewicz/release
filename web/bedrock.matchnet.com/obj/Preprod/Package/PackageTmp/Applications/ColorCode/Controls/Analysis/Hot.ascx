﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Hot.ascx.cs" Inherits="Matchnet.Web.Applications.ColorCode.Controls.Analysis.Hot" %>
<%@ Register src="AnalysisNav.ascx" tagname="AnalysisNav" tagprefix="uc1" %>
<%@ Register src="AnalysisSideBar.ascx" tagname="AnalysisSideBar" tagprefix="uc1" %>

    <div class="wrapper">
        <div id="cc-content">
            <div class="editorial">
                
                <asp:PlaceHolder ID="phBlue" runat="server" Visible="false">
                    <!-- start blue-->
                    <div class="cc-color-banner cc-blue-bg">Your Color Code Personality: <span class="cc-blue">BLUE</span></div>
                    
                    <h1>Personality Pros &mdash; What Makes You Hot!</h1>
                    
                    <p>The selfless nature of <span class="cc-blue">Blues</span> enables them to make their significant other feel consummately important. They always put their partner first with thoughtful consideration that the rest of us can only envy.</p>
                    <p>Blues have both the ability and desire to open up their heart and give all that they have. The companion of a <span class="cc-blue">Blue</span> can feel secure knowing a <span class="cc-blue">Blue</span> will hold nothing back. They don't make commitments lightly and you can be sure they will follow through.</p>
                    <ul class="ui-arrows">
	                    <li>You are a first-class act.</li>
	                    <li>You think of others before yourself.</li>
	                    <li>You give your whole heart and express your emotions honestly.</li>
	                    <li>You are always dependable.</li>
                    </ul>
                </asp:PlaceHolder>
                
                <asp:PlaceHolder ID="phRed" runat="server" Visible="false">
                    <!-- start red -->
                    <div class="cc-color-banner cc-red-bg">Your Color Code Personality: <span class="cc-red">RED</span></div>
                    
                    <h1>Personality Pros &mdash; What Makes You Hot!</h1>
                    
                    <p>Nobody dates like <span class="cc-red">Reds</span> &mdash; they tend to go all out. They dress to impress, get the limo, flowers and the best seats in the house. Their competitive nature drives them to win over their love interest and, at the same time, beat out the competition. Nothing is too good for the partner of a <span class="cc-red">Red</span>.</p>
                    <p><span class="cc-red">Reds</span> keep their promises and have a naturally strong presence. <span class="cc-red">Reds</span> go out of their way to make a good impression and you can count on <span class="cc-red">Reds</span> to be the date or the spouse you expect them to be.</p>
                    <ul class="ui-arrows">
                      <li>You love the hunt and make things happen.</li>
                      <li>You promote interesting experiences.</li>
                      <li>You take the lead.</li>
                      <li>Your confidence is sexy.</li>
                    </ul>
                </asp:PlaceHolder>
                
                <asp:PlaceHolder ID="phWhite" runat="server" Visible="false">
                    <!-- start white -->
                    <div class="cc-color-banner cc-white-bg">Your Color Code Personality: <span class="cc-white">WHITE</span></div>
                    
                    <h1>Personality Pros &mdash; What Makes You Hot!</h1>
                    
                    <p><span class="cc-white">Whites</span> are wonderful partners. They have the natural ability to set ego aside and benefit a relationship in many ways. They are superb listeners and can help sort out difficult issues with their clear objectivity.  <span class="cc-white">Whites</span> are adaptable and stay calm under pressure, so they provide important balance in crisis or sudden change.</p>
                    <p><span class="cc-white">Whites</span> are the kindest people on the earth, giving everyone the benefit of the doubt and always accepting everyone as they are.</p>
                    <ul class="ui-arrows">
                      <li>You are clear and perceptive: you bring the voice of reason.</li>
                      <li>You listen without judgment.</li>
                      <li>You accept and value diversity.</li>
                      <li>You understand the immense gift of kindness.</li>
                    </ul>
                </asp:PlaceHolder>
                
                <asp:PlaceHolder ID="phYellow" runat="server" Visible="false">
                    <!-- start yellow -->
                    <div class="cc-color-banner cc-yellow-bg">Your Color Code Personality: <span class="cc-yellow">YELLOW</span></div>
                    
                    <h1>Personality Pros &mdash; What Makes You Hot!</h1>
                    
                    <p><span class="cc-yellow">Yellows</span> are exciting to be with. They are the life of any party and always have a funny story to tell.  <span class="cc-yellow">Yellows</span> are easy to fall in love with because they love easily and openly with lots of physical demonstrations of affection.</p>
                    <p><span class="cc-yellow">Yellows</span> do things with flair. They are charismatic and accept others easily because they naturally like themselves. People and potential partners are very drawn to <span class="cc-yellow">Yellows</span>. You always make a great and sexy first impression.</p>
                    <ul class="ui-arrows">
                      <li>You make every moment magical.</li>
                      <li>You are very charismatic and willing to make a fool of yourself.</li>
                      <li>You enjoy adventure and are willing to try new things.</li>
                      <li>You have a heart of gold.</li>
                    </ul>
                </asp:PlaceHolder>
                
                <p class="text-outside">Next, find out&nbsp;<a href="Analysis.aspx?report=not"> What Makes You Not! &raquo;</a></p>
            </div><!-- end .editorial-->
            
            <!--Analysis common navigation-->
            <uc1:AnalysisNav ID="AnalysisNavControl" runat="server"></uc1:AnalysisNav>
            
        </div><!-- end .cc-content -->
    </div><!-- end .wrapper -->

    <!--Analysis sidebar-->
    <uc1:AnalysisSideBar ID="AnalysisSideBar1" runat="server"></uc1:AnalysisSideBar>