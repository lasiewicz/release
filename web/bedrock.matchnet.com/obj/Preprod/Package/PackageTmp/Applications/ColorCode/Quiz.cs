﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Matchnet.Web.Applications.ColorCode
{
    [Serializable]
    public class Quiz
    {
        decimal _FixedAdjustFactor = Constants.NULL_DECIMAL;
        decimal _FixedTotalWhite = Constants.NULL_DECIMAL;
        decimal _FixedTotalRed = Constants.NULL_DECIMAL;
        decimal _FixedTotalBlue = Constants.NULL_DECIMAL;
        decimal _FixedTotalYellow = Constants.NULL_DECIMAL;
        Dictionary<QuestionType, List<Question>> _Questions = new Dictionary<QuestionType, List<Question>>();

        #region Properties
        public decimal FixedAdjustFactor
        {
            get { return _FixedAdjustFactor; }
            set { _FixedAdjustFactor = value; }
        }

        public decimal FixedTotalWhite
        {
            get { return _FixedTotalWhite; }
            set { _FixedTotalWhite = value; }
        }

        public decimal FixedTotalRed
        {
            get { return _FixedTotalRed; }
            set { _FixedTotalRed = value; }
        }

        public decimal FixedTotalBlue
        {
            get { return _FixedTotalBlue; }
            set { _FixedTotalBlue = value; }
        }

        public decimal FixedTotalYellow
        {
            get { return _FixedTotalYellow; }
            set { _FixedTotalYellow = value; }
        }

        public Dictionary<QuestionType, List<Question>> Questions
        {
            get { return _Questions; }
            set { _Questions = value; }
        }

        #endregion

        #region Constructors

        public Quiz()
        {
            
        }

        #endregion

        #region Public Methods
        public Question GetQuestion(int questionID)
        {
            Question question = null;

            if (questionID > 0)
            {
                if (questionID <= 30 && Questions.Keys.Contains(QuestionType.part1) && Questions[QuestionType.part1] != null)
                {
                    foreach (Question q in Questions[QuestionType.part1])
                    {
                        if (q.ID == questionID)
                        {
                            question = q;
                            break;
                        }
                    }
                }
                else if (Questions.Keys.Contains(QuestionType.part2) && Questions[QuestionType.part2] != null)
                {
                    foreach (Question q in Questions[QuestionType.part2])
                    {
                        if (q.ID == questionID)
                        {
                            question = q;
                            break;
                        }
                    }
                }
            }

            return question;
        }


        public List<Question> GetQuestions(int startQuestionID, int count)
        {
            List<Question> list = new List<Question>();
            try
            {
                for (int i = startQuestionID; i < startQuestionID + count; i++)
                {
                    Question q = GetQuestion(i);
                    if (q != null)
                        list.Add(q);

                }
                return list;

            }
            catch (Exception ex)
            { return list; }

        }
        #endregion

    }
}
