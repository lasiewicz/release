﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RelationtoOtherColors.ascx.cs" Inherits="Matchnet.Web.Applications.ColorCode.ComprehensiveAnalysisControls.RelationtoOtherColors" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>

	<h2 class="title">	<mn:Txt runat="server" ID="txtSubTitle" /></h2>
	
	<div class="margin-medium">
			<mn:Txt runat="server" ID="txtContent"  ExpandImageTokens="true" />
			<mn:Txt runat="server" ID="txtContent1"  ExpandImageTokens="true"  />
	</div><!-- end .margin-medium -->
	
	<div class="bottom_nav">
		<h4 class="float-inside"><a href="/Applications/ColorCode/ComprehensiveAnalysis.aspx?section=relatingtoothers"><mn:Txt runat="server" ID="txt4" ResourceConstant="TXT_BACK_LNK" /></a></h4>
		<h4 class="float-outside"><a href="/Applications/ColorCode/ComprehensiveAnalysis.aspx?section=successfulrelationship"><mn:Txt runat="server" ID="txt5" ResourceConstant="TXT_FORWARD_LNK" /></a></h4>
	</div>
