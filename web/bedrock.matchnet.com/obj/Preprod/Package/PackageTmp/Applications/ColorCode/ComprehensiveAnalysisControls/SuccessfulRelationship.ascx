﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SuccessfulRelationship.ascx.cs" Inherits="Matchnet.Web.Applications.ColorCode.ComprehensiveAnalysisControls.SuccessfulRelationship" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>

	<h2 class="title">	<mn:Txt runat="server" ID="txtSubTitle" ResourceConstant="TXT_SECTION_SUB_TITLE" /></h2>
	
	<div class="margin-medium">
			<mn:Txt runat="server" ID="txtContent" ExpandImageTokens="true" />
	</div><!-- end .margin-medium -->
	
	<div class="bottom_nav">
		<h4 class="float-inside"><a href="/Applications/ColorCode/ComprehensiveAnalysis.aspx?section=relationtoothercolors"><mn:Txt runat="server" ID="txt4" ResourceConstant="TXT_BACK_LNK" /></a></h4>
		<h4 class="float-outside"><a href="/Applications/ColorCode/ComprehensiveAnalysis.aspx?section=relationshiptips"><mn:Txt runat="server" ID="txt5" ResourceConstant="TXT_FORWARD_LNK" /></a></h4>
	</div>
