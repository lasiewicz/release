﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Matchnet.Web.Applications.ColorCode.ComprehensiveAnalysisControls
{
    public partial class NaturalEQCompetency : ComprehensiveAnalysisBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            txtSubTitle.Text = g.GetResource("TXT_SECTION_SUB_TITLE_" + PrimaryColorCode.ToString().ToUpper(), this);

            txtContent1.Text = g.GetResource("TXT_MOTIVE_" + PrimaryColorCode.ToString().ToUpper() + "_" + SecondaryColor.ToUpper(), this, new string[] { FirstName });
           
        }
    }
}