﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Matchnet.Web.Applications.ColorCode.Controls.Analysis
{
    public partial class PieChart : ColorCodeControl
    {
        protected string _PieCSS = "cc-chart cc-chart-pie"; //default pie css
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public override void LoadReport(MemberQuiz memberQuiz, ReportType reportType)
        {
            base.LoadReport(memberQuiz, reportType);

            

            //load pie table rows in descending order
            decimal[] arrScores = memberQuiz.Scores.Values.ToArray();
            string[] pieTable = new string[4];
            Array.Sort(arrScores); //sorts by ascending order for primitive types
            int zeroScoreCount = 0;
            foreach (Color c in memberQuiz.Scores.Keys)
            {
                if (memberQuiz.Scores[c] == arrScores[3] && String.IsNullOrEmpty(pieTable[0])) //fourth (last) value in array should be biggest
                {
                    pieTable[0] = ("<tr><th>" + ColorCodeHelper.GetFormattedColorText(c).ToUpper() + "</th><td>" + memberQuiz.Scores[c].ToString("N2") + "</td></tr>");
                }
                else if (memberQuiz.Scores[c] == arrScores[2] && String.IsNullOrEmpty(pieTable[1])) //third value in array should be second biggest
                {
                    pieTable[1] = ("<tr><th>" + ColorCodeHelper.GetFormattedColorText(c).ToUpper() + "</th><td>" + memberQuiz.Scores[c].ToString("N2") + "</td></tr>");
                }
                else if (memberQuiz.Scores[c] == arrScores[1] && String.IsNullOrEmpty(pieTable[2]))
                {
                    pieTable[2] = ("<tr><th>" + ColorCodeHelper.GetFormattedColorText(c).ToUpper() + "</th><td>" + memberQuiz.Scores[c].ToString("N2") + "</td></tr>");
                }
                else if (memberQuiz.Scores[c] == arrScores[0] && String.IsNullOrEmpty(pieTable[3]))
                {
                    pieTable[3] = ("<tr><th>" + ColorCodeHelper.GetFormattedColorText(c).ToUpper() + "</th><td>" + memberQuiz.Scores[c].ToString("N2") + "</td></tr>");
                }

                if (memberQuiz.Scores[c] < 1)
                {
                    zeroScoreCount++;
                }

            }

            System.Text.StringBuilder sbPieTable = new System.Text.StringBuilder();
            for (int i = 0; i < pieTable.Length; i++)
            {
                sbPieTable.Append(pieTable[i]);
            }

            LiteralPieTableRows.Text = sbPieTable.ToString();

            //change css class if there are 3 scores with value of less than 1
            if (zeroScoreCount >= 3)
            {
                _PieCSS = "cc-chart cc-chart-vert-large";
            }
        }
    }
}