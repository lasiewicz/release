﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DevelopmentTasks.ascx.cs" Inherits="Matchnet.Web.Applications.ColorCode.ComprehensiveAnalysisControls.DevelopmentTasks" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>	

<div class="dev-tasks">
<%--	<h2 class="title"><mn:Txt runat="server" ID="txtTitle" ResourceConstant="TXT_SECTION_TITLE" /></h2>
	<div class="<asp:Literal id=litPrimaryColorClass runat=server />">
		<mn:Txt runat="server" ID="txt1" ResourceConstant="TXT_SECTION_COLOR_TITLE" />
	</div>--%>
	<h2 class="title"><mn:Txt runat="server" ID="txtSubTitle"  /></h2>
	
	<div class="margin-medium">
	<mn:Txt runat="server" ID="txtContent1"/>
			<mn:Txt runat="server" ID="txtContent"/>
			
	
	</div><!-- end .margin-medium -->
	
	<div class="bottom_nav">
		<h4 class="float-inside"><a href="/Applications/ColorCode/ComprehensiveAnalysis.aspx?section=naturaleqcompetency"><mn:Txt runat="server" ID="txtBack" ResourceConstant="TXT_BACK_LNK" /></a></h4>
		<h4 class="float-outside"><a href="/Applications/ColorCode/ComprehensiveAnalysis.aspx?section=applyingpersonality"><mn:Txt runat="server" ID="txtForward"  ResourceConstant="TXT_FORWARD_LNK" /></a></h4>
	</div>
</div>