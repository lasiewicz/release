﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Matchnet.Web.Applications.ColorCode.Controls.Analysis
{
    public partial class Match : ColorCodeControl
    {
        #region Event Handlers
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        #endregion

        #region Public Methods
        public override void LoadReport(MemberQuiz memberQuiz, ReportType reportType)
        {
            base.LoadReport(memberQuiz, reportType);
            AnalysisNavControl.LoadReport(memberQuiz, reportType);
            AnalysisSideBar1.LoadReport(memberQuiz, reportType);

            //Title
            switch (memberQuiz.PrimaryColor)
            {
                case Color.blue:
                    phBlue.Visible = true;
                    break;
                case Color.red:
                    phRed.Visible = true;
                    break;
                case Color.white:
                    phWhite.Visible = true;
                    break;
                case Color.yellow:
                    phYellow.Visible = true;
                    break;
            }

            //Do's and don'ts
            switch (reportType)
            {
                case ReportType.matchB:
                    phMatchBlue.Visible = true;
                    break;
                case ReportType.matchR:
                    phMatchRed.Visible = true;
                    break;
                case ReportType.matchW:
                    phMatchWhite.Visible = true;
                    break;
                case ReportType.matchY:
                    phMatchYellow.Visible = true;
                    break;
            }

            //Color vs Color
            if (memberQuiz.PrimaryColor == Color.blue)
            {
                switch (reportType)
                {
                    case ReportType.matchB:
                        phBlueBlue.Visible = true;
                        break;
                    case ReportType.matchR:
                        phBlueRed.Visible = true;
                        break;
                    case ReportType.matchW:
                        phBlueWhite.Visible = true;
                        break;
                    case ReportType.matchY:
                        phBlueYellow.Visible = true;
                        break;
                }
            }
            else if (memberQuiz.PrimaryColor == Color.red)
            {
                switch (reportType)
                {
                    case ReportType.matchB:
                        phRedBlue.Visible = true;
                        break;
                    case ReportType.matchR:
                        phRedRed.Visible = true;
                        break;
                    case ReportType.matchW:
                        phRedWhite.Visible = true;
                        break;
                    case ReportType.matchY:
                        phRedYellow.Visible = true;
                        break;
                }
            }
            else if (memberQuiz.PrimaryColor == Color.white)
            {
                switch (reportType)
                {
                    case ReportType.matchB:
                        phWhiteBlue.Visible = true;
                        break;
                    case ReportType.matchR:
                        phWhiteRed.Visible = true;
                        break;
                    case ReportType.matchW:
                        phWhiteWhite.Visible = true;
                        break;
                    case ReportType.matchY:
                        phWhiteYellow.Visible = true;
                        break;
                }
            }
            else if (memberQuiz.PrimaryColor == Color.yellow)
            {
                switch (reportType)
                {
                    case ReportType.matchB:
                        phYellowBlue.Visible = true;
                        break;
                    case ReportType.matchR:
                        phYellowRed.Visible = true;
                        break;
                    case ReportType.matchW:
                        phYellowWhite.Visible = true;
                        break;
                    case ReportType.matchY:
                        phYellowYellow.Visible = true;
                        break;
                }
            }

        }
        #endregion
    }
}