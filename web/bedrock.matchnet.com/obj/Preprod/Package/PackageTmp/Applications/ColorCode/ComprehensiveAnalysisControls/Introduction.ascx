﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Introduction.ascx.cs" Inherits="Matchnet.Web.Applications.ColorCode.ComprehensiveAnalysisControls.Introduction" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>

	<h2 class="title"><mn:Txt runat="server" ID="txtIntroduction" ResourceConstant="TXT_INTRODUCTION" /></h2>
	<h2 class="title"><mn:Txt runat="server" ID="txt1" ResourceConstant="TXT_WHY" /></h2>
	
	<div class="margin-medium">
		<mn:Txt runat="server" ID="txtContent" ResourceConstant="TXT_CONTENT" />
	</div><!-- end .margin-medium -->
	
	<div class="bottom_nav">
		<h4 class="float-inside"><a href="/Applications/ColorCode/ComprehensiveAnalysis.aspx?section=main"><mn:Txt runat="server" ID="txt2" ResourceConstant="TXT_BACK_LNK" /></a></h4>
		<h4 class="float-outside"><a href="/Applications/ColorCode/ComprehensiveAnalysis.aspx?section=basiccorecolors"><mn:Txt runat="server" ID="txt3" ResourceConstant="TXT_FORWARD_LNK" /></a></h4>
	</div>
