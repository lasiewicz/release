﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Other.ascx.cs" Inherits="Matchnet.Web.Applications.ColorCode.Controls.Analysis.Other" %>
<%@ Register src="AnalysisNav.ascx" tagname="AnalysisNav" tagprefix="uc1" %>
<%@ Register src="AnalysisSideBar.ascx" tagname="AnalysisSideBar" tagprefix="uc1" %>

    <div class="wrapper">
        <div id="cc-content">
            <div class="editorial">
                <div class="cc-color-banner cc-<%=ColorText.ToLower() %>-bg">Your Color Code Personality: <span>Learning About Others</span></div>

                <asp:PlaceHolder ID="phOtherRed" runat="server" Visible="false">
                    <h1><span class="cc-badge cc-red-bg">Red</span> The Power-Wielders</h1>
                    <div class="clearfix">

                        <div class="two-column" style="width:33%">
                            <h2>Core Motive: Power</h2>
                            <ul class="bulletless">
                              <li>Moving from A to B</li>
                            </ul>

                            <h2>Natural Gifts</h2>
                            <ul class="bulletless">
                              <li>Vision &amp; Leadership</li>

                            </ul>

                            <h2>Needs</h2>
                            <ul class="bulletless">
	                            <li>To look good intellectually</li>
	                            <li>To be right</li>
	                            <li>To be respected</li>
	                            <li>Approval from a select few</li>
                            </ul>

                            <h2>Wants</h2>
                            <ul class="bulletless">
	                            <li>To be productive</li>
	                            <li>To hide securities tightly</li>
	                            <li>To be a leader</li>
	                            <li>Challenging adventure</li>
                            </ul>
                        </div>

                        <div class="cc-content-inline two-column" style="width:64%">
                            <div class="two-column">
                                <h2>Strengths</h2>
                                <ul class="bulletless">
	                                <li>Action-oriented</li>
	                                <li>Articulate</li>
	                                <li>Assertive</li>
	                                <li>Confident</li>
	                                <li>Decisive</li>
	                                <li>Determined</li>
	                                <li>Focused</li>
	                                <li>Leader</li>
	                                <li>Motivated</li>
	                                <li>Powerful</li>
	                                <li>Pragmatic</li>
	                                <li>Proactive</li>
	                                <li>Productive</li>
	                                <li>Responsible</li>
	                                <li>Visionary</li>
                                </ul>
                            </div>
                            <div class="two-column">
                                <h2>Limitations</h2>
                                <ul class="bulletless">
	                                <li>Always right</li>
	                                <li>Argumentative</li>
	                                <li>Arrogant</li>
	                                <li>Bossy</li>
	                                <li>Calculating</li>
	                                <li>Critical of others</li>
	                                <li>Demanding</li>
	                                <li>Impatient</li>
	                                <li>Insensitive</li>
	                                <li>Intimidating</li>
	                                <li>Obsessive</li>
	                                <li>Overly aggressive</li>
	                                <li>Relentless</li>
	                                <li>Selfish</li>
	                                <li>Tactless</li>
                                </ul>
                            </div>
                        </div>

                    </div>
                    <p class="cc-nav-next">Next, learn about&nbsp;<a href="Analysis.aspx?report=otherB"> the Color <span class="cc-blue">BLUE</span> &raquo;</a></p>
                </asp:PlaceHolder>
                
                <asp:PlaceHolder ID="phOtherBlue" runat="server" Visible="false">
                    <h1><span class="cc-badge cc-blue-bg">Blue</span> The Do-Gooders</h1>
                    <div class="clearfix">

                        <div class="two-column" style="width:33%">
                        <h2>Core Motive: Intimacy</h2>
                        <ul class="bulletless">
                          <li>Developing Legitimate Connections</li>
                        </ul>

                        <h2>Natural Gifts</h2>
                        <ul class="bulletless">
                          <li>Quality &amp; Service</li>

                        </ul>

                        <h2>Needs</h2>
                        <ul class="bulletless">
	                        <li>To be good morally</li>
	                        <li>To be understood</li>
	                        <li>To be appreciated</li>
	                        <li>Acceptance</li>
                        </ul>

                        <h2>Wants</h2>
                        <ul class="bulletless">
	                        <li>To ensure quality</li>
	                        <li>To have autonomy</li>
	                        <li>To have security</li>
	                        <li>To please others</li>
                        </ul>
                        </div>

                        <div class="cc-content-inline two-column" style="width:64%">
                            <div class="two-column">
                                <h2>Strengths</h2>
                                <ul class="bulletless">
	                                <li>Analytical</li>
	                                <li>Caring</li>
	                                <li>Compassionate</li>
	                                <li>Deliberate</li>
	                                <li>Dependable</li>
	                                <li>Detail conscious</li>
	                                <li>Intimate</li>
	                                <li>Intuitive</li>
	                                <li>Loyal</li>
	                                <li>Nurturing</li>
	                                <li>Quality-oriented</li>
	                                <li>Respectful</li>
	                                <li>Sincere</li>
	                                <li>Thoughtful</li>
	                                <li>Well-mannered</li>
                                </ul>
                            </div>
                            <div class="two-column">
                                <h2>Limitations</h2>
                                <ul class="bulletless">
	                                <li>Emotionally intense</li>
	                                <li>Guilt-prone</li>
	                                <li>Hard-to-please</li>
	                                <li>Jealous</li>
	                                <li>Judgmental</li>
	                                <li>Low self-esteem</li>
	                                <li>Moody</li>
	                                <li>Overly sensitive</li>
	                                <li>Perfectionist</li>
	                                <li>Self-critical</li>
	                                <li>Self-righteous</li>
	                                <li>Suspicious</li>
	                                <li>Unforgiving</li>
	                                <li>Unrealistic expectations</li>
	                                <li>Worry-prone</li>
                                </ul>
                            </div>

                        </div>
                    </div>
                    <p class="cc-nav-next">Next, learn about&nbsp;<a href="Analysis.aspx?report=otherW"> the Color <span class="cc-white">WHITE</span> &raquo;</a></p>
                </asp:PlaceHolder>
                
                <asp:PlaceHolder ID="phOtherWhite" runat="server" Visible="false">
                    <h1><span class="cc-badge cc-white-bg">White</span> The Peacekeepers</h1>

                    <div class="clearfix">

                        <div class="two-column" style="width:33%">
                            <h2>Core Motive: Peace</h2>
                            <ul class="bulletless">
                              <li>Maintaining Balance in Life</li>
                            </ul>

                            <h2>Natural Gifts</h2>
                            <ul class="bulletless">
                              <li>Clarity &amp; Tolerance</li>

                            </ul>

                            <h2>Needs</h2>
                            <ul class="bulletless">
	                            <li>To feel good inside</li>
	                            <li>To be allowed their space</li>
	                            <li>To be respected</li>
	                            <li>Tolerance</li>
                            </ul>

                            <h2>Wants</h2>
                            <ul class="bulletless">
	                            <li>To be kind</li>
	                            <li>To be independent</li>
	                            <li>To please self and others</li>
	                            <li>To be content</li>
                            </ul>
                        </div>

                        <div class="cc-content-inline two-column" style="width:64%">
                            <div class="two-column">
                                <h2>Strengths</h2>
                                <ul class="bulletless">
	                                <li>Accepting</li>
	                                <li>Balanced</li>
	                                <li>Clear perspective</li>

	                                <li>Diplomatic</li>
	                                <li>Even-tempered</li>
	                                <li>Good listener</li>
	                                <li>Inventive</li>
	                                <li>Kind</li>
	                                <li>Non-discriminate</li>

	                                <li>Objective</li>
	                                <li>Patient</li>
	                                <li>Peaceful</li>
	                                <li>Satisfied</li>
	                                <li>Self-regulated</li>
	                                <li>Voice of reason</li>

                                </ul>

                            </div>
                            <div class="two-column">
                                <h2>Limitations</h2>
                                <ul class="bulletless">
	                                <li>Ambivalent</li>
	                                <li>Avoids conflict</li>
	                                <li>Boring</li>
	                                <li>Detached</li>

	                                <li>Disinterested</li>
	                                <li>Indecisive</li>
	                                <li>Indifferent</li>
	                                <li>Indirect communicator</li>
	                                <li>Reluctant</li>
	                                <li>Silently stubborn</li>

	                                <li>Timid</li>
	                                <li>Unexpressive</li>
	                                <li>Uninvolved</li>
	                                <li>Unmotivated</li>
	                                <li>Unproductive</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <p class="cc-nav-next">Next, learn about&nbsp;<a href="Analysis.aspx?report=otherY">the Color <span class="cc-yellow">YELLOW</span> &raquo;</a></p>
                </asp:PlaceHolder>
                
                <asp:PlaceHolder ID="phOtherYellow" runat="server" Visible="false">
                    <h1><span class="cc-badge cc-yellow-bg">Yellow</span> The Fun-Lovers</h1>
                    <div class="clearfix">

                        <div class="two-column" style="width:33%">
                            <h2>Core Motive: Fun</h2>
                            <ul class="bulletless">
                              <li>Living in the Moment</li>
                            </ul>

                            <h2>Natural Gifts</h2>
                            <ul class="bulletless">
                              <li>Enthusiasm &amp; Optimism</li>
                            </ul>

                            <h2>Needs</h2>
                            <ul class="bulletless">
	                            <li>To look good socially</li>

	                            <li>To be praised</li>
	                            <li>To be noticed</li>
	                            <li>General approval</li>
                            </ul>

                            <h2>Wants</h2>
                            <ul class="bulletless">
	                            <li>To be happy</li>

	                            <li>To have freedom</li>
	                            <li>To hide insecurities loosely</li>
	                            <li>Playful adventure</li>
                            </ul>
                        </div>

                        <div class="cc-content-inline two-column" style="width:64%">
                            <div class="two-column">
                            <h2>Strengths</h2>

                            <ul class="bulletless">
	                            <li>Carefree</li>
	                            <li>Charismatic</li>
	                            <li>Creative thinker</li>
	                            <li>Engaging of others</li>
	                            <li>Enthusiastic</li>
	                            <li>Flexible</li>
	                            <li>Forgiving</li>
	                            <li>Fun-loving</li>
	                            <li>Happy</li>
	                            <li>Inclusive</li>
	                            <li>Insightful</li>
	                            <li>Persuasive</li>
	                            <li>Positive</li>
	                            <li>Sociable</li>
	                            <li>Spontaneous</li>
                            </ul>

                            </div>
                            <div class="two-column">
                            <h2>Limitations</h2>
                            <ul class="bulletless">
	                            <li>Afraid to face facts</li>
	                            <li>Disorganized</li>
	                            <li>Poor follow-through</li>
	                            <li>Impulsive</li>
	                            <li>Inconsistent</li>
	                            <li>Interrupter</li>
	                            <li>Irresponsible</li>
	                            <li>Naive</li>
	                            <li>Obnoxious</li>
	                            <li>Self-centered</li>
	                            <li>Uncommitted</li>
	                            <li>Undisciplined</li>
	                            <li>Unfocused</li>
	                            <li>Vain</li>
                            </ul>
                            </div>
                        </div>

                    </div>
                    <p class="cc-nav-next">Next, see&nbsp;<a href="Analysis.aspx?report=matchIntro">how you match with each Color &raquo;</a></p>
                </asp:PlaceHolder>

            </div><!-- end .editorial-->	

            <!--Analysis common navigation-->
            <uc1:AnalysisNav ID="AnalysisNavControl" runat="server"></uc1:AnalysisNav>

        </div><!-- end .cc-content -->
    </div><!-- end .wrapper -->

    <!--Analysis sidebar-->
    <uc1:AnalysisSideBar ID="AnalysisSideBar1" runat="server"></uc1:AnalysisSideBar>
