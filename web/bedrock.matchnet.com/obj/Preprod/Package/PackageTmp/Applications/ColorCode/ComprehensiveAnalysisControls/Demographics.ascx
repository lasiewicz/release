﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Demographics.ascx.cs" Inherits="Matchnet.Web.Applications.ColorCode.ComprehensiveAnalysisControls.Demographics" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>

<%--	<h2 class="title"><mn:Txt runat="server" ID="txtTitle" ResourceConstant="TXT_SECTION_TITLE" /></h2>
	<div class="<asp:Literal id=litPrimaryColorClass runat=server />">
		<mn:Txt runat="server" ID="txt1" ResourceConstant="TXT_SECTION_COLOR_TITLE" />
	</div>--%>
	<h2 class="title">	<mn:Txt runat="server" ID="txt2" ResourceConstant="TXT_SECTION_SUB_TITLE" /></h2>
	
	<div class="margin-medium">
	        <mn:Txt runat="server" ID="txt1" ResourceConstant="TXT_CONTENT1" />
	       <div class="general_center">
	       <mn:Image runat="server" height="211" width="376" ResourceConstant="TXT_CHART_TITLE" FileName="ccc-sec1-demographics.gif" />
	       </div>
	       
			<mn:Txt runat="server" ID="txt3" ResourceConstant="TXT_CONTENT2" />
	</div><!-- end .margin-medium -->
	
	<div class="bottom_nav">
		<h4 class="float-inside"><a href="/Applications/ColorCode/ComprehensiveAnalysis.aspx?section=filters"><mn:Txt runat="server" ID="txt4" ResourceConstant="TXT_BACK_LNK" /></a></h4>
		<h4 class="float-outside"><a href="/Applications/ColorCode/ComprehensiveAnalysis.aspx?section=results"><mn:Txt runat="server" ID="txt5" ResourceConstant="TXT_FORWARD_LNK" /></a></h4>
	</div>
