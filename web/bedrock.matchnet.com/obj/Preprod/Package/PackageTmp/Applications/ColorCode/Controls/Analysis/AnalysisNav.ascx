﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AnalysisNav.ascx.cs" Inherits="Matchnet.Web.Applications.ColorCode.Controls.Analysis.AnalysisNav" %>

<div id="cc-information-menu" class="clearfix">
    <div class="three-column">
        <h2>More About You</h2>
        <ul>
            <li><a href="Analysis.aspx">Overview</a></li>
            <li><a href="Analysis.aspx?report=hot">What Makes You Hot!</a></li>
            <li><a href="Analysis.aspx?report=not">What Makes You Not!</a></li>

            <li><a href="Analysis.aspx?report=need">Your Needs</a></li>
            <li><a href="Analysis.aspx?report=want">Your Wants</a></li>
        </ul>
    </div>
    <div class="three-column">
        <h2>More About Others</h2>
        <ul>
            <li><a href="Analysis.aspx?report=otherIntro">Overview</a></li>

            <li><a href="Analysis.aspx?report=otherR">Profile: RED</a></li>
            <li><a href="Analysis.aspx?report=otherB">Profile: BLUE</a></li>
            <li><a href="Analysis.aspx?report=otherW">Profile: WHITE</a></li>
            <li><a href="Analysis.aspx?report=otherY">Profile: YELLOW</a></li>
        </ul>
    </div>
    <div class="three-column">
        <h2>Your Love Matches</h2>

        <ul>
            <li><a href="Analysis.aspx?report=matchIntro">Overview</a></li>
            <li><a href="Analysis.aspx?report=matchR"><%=ColorText.ToUpper() %> - RED</a></li>
            <li><a href="Analysis.aspx?report=matchB"><%=ColorText.ToUpper() %> - BLUE</a></li>
            <li><a href="Analysis.aspx?report=matchW"><%=ColorText.ToUpper() %> - WHITE</a></li>
            <li><a href="Analysis.aspx?report=matchY"><%=ColorText.ToUpper() %> - YELLOW</a></li>

        </ul>
    </div>

</div>