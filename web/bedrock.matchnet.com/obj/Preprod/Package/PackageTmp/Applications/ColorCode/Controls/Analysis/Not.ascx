﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Not.ascx.cs" Inherits="Matchnet.Web.Applications.ColorCode.Controls.Analysis.Not" %>
<%@ Register src="AnalysisNav.ascx" tagname="AnalysisNav" tagprefix="uc1" %>
<%@ Register src="AnalysisSideBar.ascx" tagname="AnalysisSideBar" tagprefix="uc1" %>

    <div class="wrapper">
        <div id="cc-content">

            <div class="editorial">

                <asp:PlaceHolder ID="phBlue" runat="server" Visible="false">
                    <div class="cc-color-banner cc-blue-bg">Your Color Code Personality: <span class="cc-blue">BLUE</span></div>
                    
                    <h1>Personality Pros &ndash; What Makes You Not!</h1>
                    
                    <p><span class="cc-blue">Blues</span> have very high standards that can easily lead to unrealistic expectations. Dating them can be a challenge because it is hard to live up to their exacting standards. <span class="cc-blue">Blues</span> also tend to be overly guilt-prone and worry about a wide expanse of potential problems. It can cause them to be too serious minded and unable to relax. One other struggle <span class="cc-blue">Blues</span> face is forgiving others. Since they feel so deeply, when they are wronged or betrayed, they can remember the offense for a lifetime.</p>
                    <ul class="ui-arrows">
                      <li>You tend to think you are on a mission to save the world.</li>
                      <li>You think whining is a good thing and forgiveness is not.</li>
                      <li>Your mysterious mood swings drive the rest of us crazy.</li>
                      <li>You can be overly sensitive.</li>
                    </ul>
                </asp:PlaceHolder>

                
                <asp:PlaceHolder ID="phRed" runat="server" Visible="false">
                    <!-- start red -->
                    <div class="cc-color-banner cc-red-bg">Your Color Code Personality: <span class="cc-red">RED</span></div>
                    
                    <h1>Personality Pros &ndash; What Makes You Not!</h1>
                    
                    <p>It is true that nobody dates like <span class="cc-red">Reds</span>, but once they have "won" the heart of their partner, they often think that they don't have to invest in the relationship any longer. They can become cheap and inattentive to their partner.  <span class="cc-red">Reds</span> can be blunt and tactless. They think they are just being honest, when in reality they can be hurtful and overly critical.</p>
                    <p>The strong presence of a <span class="cc-red">Red</span> can be very intimidating to others. The strong tendency to lead must be bridled so that <span class="cc-red">Reds</span> allow others to step forward.</p>
                    <ul class="ui-arrows">
                      <li>You always have to be right.</li>
                      <li>You tend to be selfish and take care of yourself first.</li>
                      <li>You don't express your feelings and emotions well.</li>
                      <li>You can be cheap.</li>
                    </ul>
                </asp:PlaceHolder>
                
                <asp:PlaceHolder ID="phWhite" runat="server" Visible="false">
                    <!-- start white -->
                    <div class="cc-color-banner cc-white-bg">Your Color Code Personality: <span class="cc-white">WHITE</span></div>
                    
                    <h1>Personality Pros &ndash; What Makes You Not!</h1>
                    
                    <p><span class="cc-white">Whites</span> are generally very conflict averse and that can prove a deterrent to emotionally honest conversation. They don't necessarily voice their opinions well and don't communicate their feelings any better. They prefer to give the appearance of agreement than risk an argument. Rather than fight, they can silently entrench in their position and wait you out.</p>
                    <p>Because of their calm and relaxed personality, they can also be boring and unanimated.</p>
                    <ul class="ui-arrows">
                      <li>You are emotionally dishonest.</li>
                      <li>You are notoriously indecisive.</li>
                      <li>You have serious conflict-avoidance issues.</li>
                      <li>You rarely express emotional vulnerability.</li>
                    </ul>
                </asp:PlaceHolder>
                
                <asp:PlaceHolder ID="phYellow" runat="server" Visible="false">
                    <!-- start yellow -->
                    <div class="cc-color-banner cc-yellow-bg">Your Color Code Personality: <span class="cc-yellow">YELLOW</span></div>
                    
                    <h1>Personality Pros &ndash; What Makes You Not!</h1>
                    
                    <p><span class="cc-yellow">Yellows</span> live in the moment, so commitments can be easily forgotten or dismissed. They tend to be irresponsible and unreliable, operating on the "better offer" theory. If they make plans and something better comes up, they will blow off the initial plans for the second.</p>
                    <p><span class="cc-yellow">Yellows</span> are often loud and obnoxious and have a terrible habit of interrupting others. They like themselves enough to assume that others will find them charming despite their faults.</p>
                    <ul class="ui-arrows">
                      <li>You are vain and undisciplined.</li>
                      <li>You are irresponsible.</li>
                      <li>You lack focus and refuse to commit long-term.</li>
                      <li>You honestly believe the sun revolves around you.</li>
                    </ul>
                </asp:PlaceHolder>
                
                <p class="text-outside">Next, find out about&nbsp;<a href="Analysis.aspx?report=need"> Your Needs &raquo;</a></p>
 
            </div><!-- end .editorial-->
            
            <!--Analysis common navigation-->
            <uc1:AnalysisNav ID="AnalysisNavControl" runat="server"></uc1:AnalysisNav>	

        </div><!-- end .cc-content -->
    </div><!-- end .wrapper -->

    <!--Analysis sidebar-->
    <uc1:AnalysisSideBar ID="AnalysisSideBar1" runat="server"></uc1:AnalysisSideBar>