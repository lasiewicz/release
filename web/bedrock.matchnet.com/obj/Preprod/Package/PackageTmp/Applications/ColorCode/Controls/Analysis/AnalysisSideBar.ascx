﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AnalysisSideBar.ascx.cs" Inherits="Matchnet.Web.Applications.ColorCode.Controls.Analysis.AnalysisSideBar" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>

<div id="cc-inline">
	    <p><a href="/Applications/SendToFriend/SendToFriendColorCode.aspx"><span class="spr s-icon-send-to-friend" title="Send to friend"><span></span></span> Send to Friend</a></p>
    	
	    <div id="cc-inline-container">
	        <div id="cc-inline-pie" class="clearfix">
		        <h2>Your Core Color = <span class="cc-<%= _ColorText.ToLower() %>"><%= _ColorText.ToUpper() %></span></h2>
		        
		        <script type="text/javascript">
		            $j('<div id="cc-pie-chart-loading"><mn:image id="mnimage6491" runat="server" filename="ajax-loader.gif" alt="" CssClass="centered" /></div>').appendTo('#cc-inline-pie');
		        </script>
		        
		        <table id="pie" class="<%=_PieCSS%>">
		        <caption>Color Code</caption>
		        <thead>
			        <tr>
				        <td>Color</td>
				        <th>Percentage</th>
			        </tr>
		        </thead>
		        <tbody>
		            <%-- 
			        <tr>
				        <th>Blue</th>
				        <td><%=_BlueScore %></td>
			        </tr>
			        <tr>
				        <th>White</th>
				        <td><%= _WhiteScore %></td>
			        </tr>
			        <tr>
				        <th>Yellow</th>
				        <td><%= _YellowScore %></td>
			        </tr>
			        <tr>
				        <th>Red</th>
				        <td><%= _RedScore %></td>
			        </tr>	
			        --%>
			        <asp:Literal ID="LiteralPieTableRows" runat="server"></asp:Literal>	
		        </tbody>
		        </table>
	        </div>
	        <div id="cc-color-adjectives" class="clearfix">
	            <asp:PlaceHolder ID="phBlue" runat="server" Visible="false">
		            <p><span class="cc-blue">BLUES</span> are:</p>
		            <ul>
			            <li>Analytical</li>
			            <li>Committed</li>
			            <li>Compassionate</li>
			            <li>Dedicated</li>
			            <li>Deliberate</li>
			            <li>Dependable</li>
			            <li>Emotional</li>
			            <li>Loyal</li>
			            <li>Nurturing</li>
			            <li>Quality-seeking</li>
			            <li>Respectful</li>
			            <li>Sincere</li>
			            <li>Thoughtful</li>
			            <li>Well-mannered</li>
		            </ul>
		        </asp:PlaceHolder>
		        <asp:PlaceHolder ID="phRed" runat="server" Visible="false">
		            <p><span class="cc-red">REDS</span> are:</p>
                    <ul>
                        <li>Action-oriented</li>
                        <li>Assertive</li>
                        <li>Confident</li>
                        <li>Decisive</li>
                        <li>Determined</li>
                        <li>Disciplined</li>
                        <li>Independent</li>
                        <li>Leaders</li>
                        <li>Logical</li>
                        <li>Pragmatic</li>
                        <li>Proactive</li>
                        <li>Productive</li>
                        <li>Responsible</li>
                        <li>Task-dominant</li>
                    </ul>
		        </asp:PlaceHolder>
		        <asp:PlaceHolder ID="phWhite" runat="server" Visible="false">
		            <p><span class="cc-white">WHITES</span> are:</p>
                    <ul>
                        <li>Accepting</li>
                        <li>Adaptable</li>
                        <li>Agreeable</li>
                        <li>Considerate</li>
                        <li>Diplomatic</li>
                        <li>Easy-going</li>
                        <li>Even-tempered</li>
                        <li>Good listener</li>
                        <li>Inventive</li>
                        <li>Kind</li>
                        <li>Patient</li>
                        <li>Pleasant</li>
                        <li>Satisfied</li>
                        <li>Tolerant</li>
                    </ul>
		        </asp:PlaceHolder>
		        <asp:PlaceHolder ID="phYellow" runat="server" Visible="false">
		            <p><span class="cc-yellow">YELLOWS</span> are:</p>
                    <ul>
                        <li>Carefree</li>
                        <li>Charismatic</li>
                        <li>Enthusiastic</li>
                        <li>Forgiving</li>
                        <li>Happy</li>
                        <li>Hopeful</li>
                        <li>Lively</li>
                        <li>Optimistic</li>
                        <li>Outgoing</li>
                        <li>Playful</li>
                        <li>Positive</li>
                        <li>Sociable</li>
                        <li>Spontaneous</li>
                        <li>Trusting</li>
                    </ul>
		        </asp:PlaceHolder>
	            
	        </div>
	    </div>
    </div>
