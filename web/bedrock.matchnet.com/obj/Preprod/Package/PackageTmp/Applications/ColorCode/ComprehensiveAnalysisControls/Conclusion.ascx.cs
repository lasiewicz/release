﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Matchnet.Web.Applications.ColorCode.ComprehensiveAnalysisControls
{
    public partial class Conclusion : ComprehensiveAnalysisBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            StringDictionary tokens = new StringDictionary();
            tokens.Add("PRIMARYCOLOR", PrimaryColor.ToUpper());
            tokens.Add("SECONDARYCOLOR", SecondaryColor.ToUpper());
            tokens.Add("FIRSTNAME", FirstName);
            tokens.Add("COREMOTIVE", CoreMotive);

            txtContent.Text = g.GetResource("TXT_CONTENT", this, tokens);
        }
    }
}