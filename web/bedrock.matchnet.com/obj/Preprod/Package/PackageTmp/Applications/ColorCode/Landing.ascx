﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Landing.ascx.cs" Inherits="Matchnet.Web.Applications.ColorCode.Landing" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>

<div id="page-container" class="cc-content-main cc-content-bars clearfix">
    <h2 class="tagline">The Color Code <small class="sans-serif">- Understand yourself and your matches on a deeper level.</small></h2>
    <div id="cc-content" class="editorial">
        <h1>About the Color Code Personality Test</h1>
        	
        <p>Ever wonder why some people are so easy to love, work for, and befriend, while it's difficult to build and maintain healthy relationships with others? All of these relationships begin with you. Imagine the power of truly knowing yourself, including what motivates you, and how the relationships in your life are impacted as a result.</p>
        <p>The Color Code is one of the most revolutionary and accurate measurements of your personality and is your best bet for understanding how to make sense out of your relationships.</p>
        <p>Taking this FREE test will enable you to:</p>

        <ul class="ui-arrows">
	        <li>Determine your own Color, or driving Core Motive, in less than 15 minutes</li>
            <li>"Speak the Language" of others by identifying their Color</li>
            <li>Build stronger and more meaningful relationships by understanding what drives people and their behavior</li>
        </ul>

        <p>The Color Code Personality Test will provide you with in-depth understanding of why you do the things you do and how you can best interact with the other Color personalities. </p>
        <p>What's Your Color? Take the test now, it's FREE!</p>	
        <p class="text-center editorial cc-button-primary">	
	        <a href="Quiz1.aspx" class="link-primary">Take the Color Code Test Now</a>
        </p>

        
    </div><!-- end .cc-content-->	
</div><!-- end #page-container -->
<p id="cc-copy">&copy; 2010 Color Code Communications, Inc. All rights reserved.</p>