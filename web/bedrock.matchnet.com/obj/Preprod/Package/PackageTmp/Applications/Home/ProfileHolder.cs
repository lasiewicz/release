﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.List.ValueObjects;
using Matchnet.Member.ValueObjects.Interfaces;
using Matchnet.Web.Framework.Ui;

namespace Matchnet.Web.Applications.Home
{
    /// <summary>
    /// Class used to hold a Member and related properties in preparation for 
    /// binding to various Profile controls.
    /// </summary>
    [Serializable]
    public class ProfileHolder
    {
        #region Fields
        private IMemberDTO _member;
        private int _ordinal;
        private HotListCategory _hotlistCategory = HotListCategory.Default;
        private bool _isSpotlight = false;
        private BreadCrumbHelper.EntryPoint _myEntryPoint;
        private int _matchRating = Constants.NULL_INT;
        #endregion

        #region Properties
        public IMemberDTO Member
        {
            set { _member = value; }
            get { return _member; }
        }

        public int Ordinal
        {
            set { _ordinal = value; }
            get { return _ordinal; }
        }

        public HotListCategory HotlistCategory
        {
            set { _hotlistCategory = value; }
            get { return _hotlistCategory; }
        }

        public bool IsSpotlight
        {
            set { _isSpotlight = value; }
            get { return _isSpotlight; }
        }

        public BreadCrumbHelper.EntryPoint MyEntryPoint
        {
            get { return (_myEntryPoint); }
            set { _myEntryPoint = value; }
        }

        public int MatchRating
        {
            get { return _matchRating; }
            set { _matchRating = value; }
        }

        #endregion


    }
}
