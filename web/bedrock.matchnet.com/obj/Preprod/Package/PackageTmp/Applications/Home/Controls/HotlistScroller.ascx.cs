﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.List.ValueObjects;
using Matchnet.Web.Framework.Ui;
using System.Collections;
using Matchnet.Web.Framework;
using Matchnet.Member.ValueObjects.Interfaces;
using Matchnet.Web.Framework.Managers;
using Matchnet.Member.ValueObjects.MemberDTO;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Web.Framework.Ui.BasicElements;
using Matchnet.Lib;
using Matchnet.Web.Analytics;
using Matchnet.Email.ServiceAdapters;
using Matchnet.Email.ValueObjects;

namespace Matchnet.Web.Applications.Home.Controls
{
    public partial class HotlistScroller : FrameworkControl
    {
        protected string _TitleResourceConstant = "";
        protected HotListCategory _HotListCategory = HotListCategory.MembersYouTeased;
        protected BreadCrumbHelper.EntryPoint _EntryPoint = BreadCrumbHelper.EntryPoint.HotLists;
        protected int _PageSize = 24;
        protected int _StartRow = 1;
        private int _Ordinal = 1;
        private string _OmnitureWidgetName = "";
        private ArrayList members = null;
        private string _HotlistText = "";

        #region Properties
        public string OmnitureWidgetName
        {
            get { return _OmnitureWidgetName; }
            set { _OmnitureWidgetName = value; }
        }

        public BreadCrumbHelper.EntryPoint EntryPoint
        {
            get { return _EntryPoint; }
            set { _EntryPoint = value; }
        }

        public HotListCategory HotlistCategory
        {
            get { return _HotListCategory; }
            set { _HotListCategory = value; }
        }

        public int PageSize
        {
            get { return _PageSize; }
            set { _PageSize = value; }
        }

        public int StartRow
        {
            get { return _StartRow; }
            set { _StartRow = value; }
        }

        public string TitleResourceConstant
        {
            get { return _TitleResourceConstant; }
            set { _TitleResourceConstant = value; }
        }

        public int NewHotlistNotificationCount { get; set; }
        public bool IsEmpty { get; set; }
        public bool HideCountIndicators { get; set; }
        #endregion

        #region Event Handlers
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void rpProfiles_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                IMemberDTO member = e.Item.DataItem as IMemberDTO;

                string profileURL = BreadCrumbHelper.MakeViewProfileLink(_EntryPoint, member.MemberID, _Ordinal, null, (int)_HotListCategory);

                //username
                HyperLink lnkUserName = e.Item.FindControl("lnkUsername") as HyperLink;
                string userName = member.GetUserName(g.Brand);
                lnkUserName.Text = FrameworkGlobals.Ellipsis(userName, 15, "&hellip;");
                lnkUserName.ToolTip = userName;
                lnkUserName.NavigateUrl = g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.ViewProfile, WebConstants.Action.ProfileName, profileURL, _OmnitureWidgetName);
                
                //hotlist date
                ListItemDetail listItemDetail = g.List.GetListItemDetail(_HotListCategory,
                        g.Brand.Site.Community.CommunityID,
                        g.Brand.Site.SiteID,
                        member.MemberID);

                DateTime hotlistDate = (listItemDetail != null) ? listItemDetail.ActionDate : DateTime.Now;
                LastTime lastTimeHotlistDate = e.Item.FindControl("lastTimeHotlistDate") as LastTime;
                lastTimeHotlistDate.LoadLastTime(hotlistDate);

                //photo
                HyperLink lnkProfilePhoto = e.Item.FindControl("lnkProfilePhoto") as HyperLink;
                lnkProfilePhoto.NavigateUrl = g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.ViewProfile, WebConstants.Action.ProfilePhoto, profileURL, _OmnitureWidgetName);
                lnkProfilePhoto.ToolTip = userName;

                System.Web.UI.WebControls.Image imageProfilePhoto = e.Item.FindControl("imageProfilePhoto") as System.Web.UI.WebControls.Image;
                var photo = MemberPhotoDisplayManager.Instance.GetDefaultPhoto(g.Member, member, g.Brand);
                imageProfilePhoto.ImageUrl = MemberPhotoDisplayManager.Instance.GetPhotoDisplayURL(g.Member, member, g.Brand,
                                                                           photo, PhotoType.Thumbnail,
                                                                           PrivatePhotoImageType.Thumb,
                                                                            NoPhotoImageType.Thumb);    
                
                //hotlist text
                Literal litHotlistText = e.Item.FindControl("litHotlistText") as Literal;
                litHotlistText.Text = _HotlistText;

                _Ordinal++;
            }
        }

        #endregion

        #region Public Methods
        public void LoadHotlistScroller(int maxProfilesCount)
        {
            if (maxProfilesCount > 0)
            {
                _PageSize = maxProfilesCount;
            }

            LoadHotlistScroller();
        }

        public void LoadHotlistScroller()
        {
            if (string.IsNullOrEmpty(_OmnitureWidgetName))
            {
                _OmnitureWidgetName = "Hotlist Profile List";
            }

            //title
            if (string.IsNullOrEmpty(_TitleResourceConstant))
            {
                _TitleResourceConstant = "TXT_HOTLIST_TITLE_" + _HotListCategory.ToString();
            }
            txtHotlistTitle.ResourceConstant = _TitleResourceConstant;
            txtHotlistTitle.TitleResourceConstant = _TitleResourceConstant;
            txtHotlistTitleNonSub.ResourceConstant = _TitleResourceConstant;
            txtHotlistTitleNonSub.TitleResourceConstant = _TitleResourceConstant;
            txtHotlistTitleNoContent.ResourceConstant = _TitleResourceConstant;
            txtHotlistTitleNoContent.TitleResourceConstant = _TitleResourceConstant;

            //notication count
            var listManager = new ListManager();
            this.NewHotlistNotificationCount = listManager.GetUnseenNotificationCount(g.List, g.Member, g.Brand, _HotListCategory);
            if (this.NewHotlistNotificationCount > 0)
            {
                phHotlistNotificationCount.Visible = true;
                litNotificationCount.Text = this.NewHotlistNotificationCount.ToString();

                phHotlistNotificationCountNonSub.Visible = true;
                litNotificationCountNonSub.Text = this.NewHotlistNotificationCount.ToString();
            }
            else
            {
                this.NewHotlistNotificationCount = 0;
            }

            if (HideCountIndicators)
            {
                phHotlistNotificationCount.Visible = false;
                phHotlistNotificationCountNonSub.Visible = false;
            }

            //set hotlist text
            _HotlistText = g.GetResource("TXT_HOTLIST_TEXT_" + _HotListCategory.ToString(), this);

            //get hotlist profiles
            int communityID = g.Brand.Site.Community.CommunityID;
            int siteID = g.Brand.Site.SiteID;
            int totalrows = 0;
            ArrayList targetMemberIDs = g.List.GetListMembers(_HotListCategory, communityID, siteID,
                                                               _StartRow, _PageSize, out totalrows);

            if (targetMemberIDs != null && targetMemberIDs.Count > 0)
            {
                //get profiles
                members = MemberDTOManager.Instance.GetIMemberDTOs(g, targetMemberIDs, MemberType.Search, MemberLoadFlags.None);
            }


            bool hasUnreadAllAccessEmails = false;
            bool isNonSub = !g.Member.IsPayingMember(g.Brand.Site.SiteID);
            if (_HotListCategory == HotListCategory.WhoEmailedYou)
            {
                //check if there are any unread all access emails
                int unreadAllAccessEmailCount = EmailMessageSA.Instance.GetMessageCountByStatus(g.Member.MemberID, g.Brand, (int) SystemFolders.VIPInbox,
                                                                MessageStatus.New, false, false);
                if (unreadAllAccessEmailCount > 0)
                {
                    hasUnreadAllAccessEmails = true;
                }
            }

            //TEMP: Creative to see no activity section
            //members = null;
            //hasUnreadAllAccessEmails = true;

            //display appropriate view
            if ((members != null && members.Count > 0) || hasUnreadAllAccessEmails)
            {
                bool showNonSubView = false;
                IsEmpty = false;
                if (isNonSub)
                {
                    if (_HotListCategory == HotListCategory.WhoEmailedYou
                        || _HotListCategory == HotListCategory.WhoTeasedYou)
                    {
                        showNonSubView = true;
                    }
                }

                if (!showNonSubView)
                {
                    //has profiles to display
                    phSubscriberProfiles.Visible = true;

                    //load profiles
                    _Ordinal = _StartRow;
                    rpProfiles.DataSource = members;
                    rpProfiles.DataBind();

                    txtHotlistTitle.Href = "/Applications/HotList/View.aspx?CategoryID=" +
                                      ((int)_HotListCategory).ToString();
                    txtHotlistTitle.Href = g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.View,
                                                                       WebConstants.Action.ViewPageTitle, txtHotlistTitle.Href,
                                                                       _OmnitureWidgetName);

                    txtViewAll.Href = "/Applications/HotList/View.aspx?CategoryID=" +
                                      ((int) _HotListCategory).ToString();
                    txtViewAll.Href = g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.View,
                                                                       WebConstants.Action.ViewMoreLink, txtViewAll.Href,
                                                                       _OmnitureWidgetName);
                }
                else
                {
                    phNonSubscribers.Visible = true;

                    //display gender specific non-sub photo
                    int genderMask = g.Member.GetAttributeInt(g.Brand, "gendermask");
                    if ((genderMask & ConstantsTemp.GENDERID_MALE) == ConstantsTemp.GENDERID_MALE)
                    {
                        phFemalePhotoNonSub.Visible = true;

                        phFemalePhotoNonSubAllAccess.Visible = true;
                        lnkSearchFemaleNonSubAllAccess.HRef = g.AnalyticsOmniture.GetActionURL(
                            WebConstants.PageIDs.MailBox, WebConstants.Action.ViewPageImageLink,
                            lnkSearchFemaleNonSubAllAccess.HRef, _OmnitureWidgetName);
                    }
                    else
                    {
                        phMalePhotoNonSub.Visible = true;

                        phMalePhotoNonSubAllAccess.Visible = true;
                        lnkSearchMaleNonSubAllAccess.HRef = g.AnalyticsOmniture.GetActionURL(
                            WebConstants.PageIDs.MailBox, WebConstants.Action.ViewPageImageLink,
                            lnkSearchMaleNonSubAllAccess.HRef, _OmnitureWidgetName);

                    }

                    if (hasUnreadAllAccessEmails && _HotListCategory == HotListCategory.WhoEmailedYou)
                    {
                        phNonSubAllAccess.Visible = true;
                        txtHotlistTitleNonSub.Href = string.Format(txtHotlistTitleNonSub.Href, "3254");

                        string allAccessText = g.GetResource("TXT_ALL_ACCESS_TEXT_NONSUB", this);
                        string mailboxOmnitureParams = Omniture.GetActionURLParams(WebConstants.PageIDs.MailBox,
                                                                                   WebConstants.Action.ViewPageLink,
                                                                                   g.AnalyticsOmniture.PageName,
                                                                                   _OmnitureWidgetName);

                        litAllAccessCopy.Text = string.Format(allAccessText, mailboxOmnitureParams, mailboxOmnitureParams);

                    }
                    else
                    {
                        phNonSubSubScribe.Visible = true;
                        //subscribe links prtid
                        string membersHave = g.GetResource("TXT_HOTLIST_TEXT_NONSUB_" + _HotListCategory.ToString(),
                                                           this);
                        txtSubscribeNow.ResourceConstant = "TXT_SUBSCRIBE_NOW_" + _HotListCategory.ToString();
                        if (_HotListCategory == HotListCategory.WhoTeasedYou)
                        {
                            lnkSubscribe.HRef = string.Format(lnkSubscribe.HRef, "3253");
                            lnkSearchMaleNonSub.HRef = string.Format(lnkSearchMaleNonSub.HRef, "3253");
                            lnkSearchFemaleNonSub.HRef = string.Format(lnkSearchFemaleNonSub.HRef, "3253");
                            txtHotlistTitleNonSub.Href = string.Format(txtHotlistTitleNonSub.Href, "3253");
                            txtMembersHave.Text = string.Format(membersHave, "3253");
                        }
                        else
                        {
                            lnkSubscribe.HRef = string.Format(lnkSubscribe.HRef, "3254");
                            lnkSearchMaleNonSub.HRef = string.Format(lnkSearchMaleNonSub.HRef, "3254");
                            lnkSearchFemaleNonSub.HRef = string.Format(lnkSearchFemaleNonSub.HRef, "3254");
                            txtHotlistTitleNonSub.Href = string.Format(txtHotlistTitleNonSub.Href, "3254");
                            txtMembersHave.Text = string.Format(membersHave, "3254");
                        }
                    }
                }

            }
            else
            {
                //no profiles to display
                phNoProfiles.Visible = true;
                IsEmpty = true;

                txtHotlistTitleNoContent.Href = g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.SearchResults,
                                                                       WebConstants.Action.ViewPageTitle, txtHotlistTitleNoContent.Href,
                                                                       _OmnitureWidgetName);

                string noContentCopy = g.GetResource("HOTLIST_NOCONTENT_COPY_" + _HotListCategory.ToString(), this);
                litHotlistNoContentCopy.Text = string.Format(noContentCopy,
                                                             Omniture.GetActionURLParams(
                                                                 WebConstants.PageIDs.SearchResults,
                                                                 WebConstants.Action.ViewPageLink, _g.AnalyticsOmniture.PageName,
                                                                 _OmnitureWidgetName));

                //display gender specific non-sub photo
                int genderMask = g.Member.GetAttributeInt(g.Brand, "gendermask");
                if ((genderMask & ConstantsTemp.GENDERID_MALE) == ConstantsTemp.GENDERID_MALE)
                {
                    phFemalePhotoNoContent.Visible = true;
                    lnkSearchFemaleNoContent.HRef = g.AnalyticsOmniture.GetActionURL(
                        WebConstants.PageIDs.SearchResults, WebConstants.Action.ViewPageImageLink,
                        lnkSearchFemaleNoContent.HRef, _OmnitureWidgetName);
                }
                else
                {
                    phMalePhotoNoContent.Visible = true;
                    lnkSearchMaleNoContent.HRef = g.AnalyticsOmniture.GetActionURL(
                        WebConstants.PageIDs.SearchResults, WebConstants.Action.ViewPageImageLink,
                        lnkSearchMaleNoContent.HRef, _OmnitureWidgetName);
                }
            }
        }
        #endregion

    }
}
