﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MatchesSliderYNM.ascx.cs" Inherits="Matchnet.Web.Applications.Home.Controls.MatchesSliderYNM" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>

<h2 class="component-header"><a href="/Applications/Search/SearchResults.aspx?SearchOrderBy=2" id="lnkFilmstripTitle" runat="server"><mn:Txt ID="txtComponentTitle" runat="server" ResourceConstant="TXT_COMPONENT_HEADER_MATCHES" /></a> <small><a href="/Applications/Search/SearchResults.aspx?SearchOrderBy=2" id="lnkViewMore" runat="server" class="view-more"><mn:Txt ID="Txt1" runat="server" ResourceConstant="TXT_VIEW_MORE" /> </a></small></h2>
<div id="match-slider" class="match-slider-20 clearfix">
    <div id="ajax-loader" class="ajax-loader">
        <p class="info">
            <mn:Image id="mnimage4704" runat="server" filename="ajax-loader.gif" titleresourceconstant="" resourceconstant="" />
            <mn:Txt id="mntxt7220" runat="server" resourceconstant="TXT_LOADING" expandimagetokens="true" />
        </p>
    </div>

    <button id="match-slider-button-prev" class="spr-btn btn-arrow-lg-prev-20 disabled">&#9668;</button>
    <button id="match-slider-button-next" class="spr-btn btn-arrow-lg-next-20">&#9658;</button>

    <%--Profiles--%>
    <div id="match-slider-profiles-container" class="match-slider-20-container clearfix"></div>
</div>
<div id="match-slider-none" class="match-slider-20 clearfix" style="display:none;">
    <%--No Matches--%>
    <div class="tips">
        <p><mn:Txt ID="txtNoMatches" runat="server" ResourceConstant="TXT_NOMATCHES_BODY" /></p>
    </div>
</div>
<ul class="nav-away">
    <li><a href="/Applications/Search/SearchPreferences.aspx" id="lnkMatchPreferences" runat="server"><mn:Txt ID="txtUpdatePreferences" runat="server" ResourceConstant="TXT_UPDATE_PREFRENCES" /></a></li>
    <li><a href="/Applications/HotList/SlideShow.aspx" id="lnkSlideshow" runat="server"><mn:Txt ID="txtViewYNM" runat="server" ResourceConstant="TXT_VIEW_YNM" /></a></li>
    <li><a href="/Applications/AdvancedSearch/AdvancedSearch.aspx" id="lnkSearch" runat="server"><mn:Txt ID="txtSearch" runat="server" ResourceConstant="TXT_SEARCH" /></a></li>
    <li><a href="/Applications/PhotoGallery/PhotoGallery.aspx" id="lnkPhotoGallery" runat="server"><mn:Txt ID="txtPhotoGallery" runat="server" ResourceConstant="TXT_PHOTO_GALLERY" /></a></li>
</ul>

<script id="profile_template" type="text/template">
    <li class="mini-profile" id="{{profileContainerID}}">
        <a href="{{photoProfileURL}}" title="{{username}}" onclick="matchSliderManager.onProfileLinkClick(this, {{profileIndex}}, {{memberID}}); return true;">
            <span class="photo">
                <img src="{{photoURL}}" alt="{{username}}" />
            </span>
        </a>
        <span class="rate">{{matchRatingDisplay}}%</span>
        <a href="{{usernameProfileURL}}" title="{{username}}" class="name" onclick="matchSliderManager.onProfileLinkClick(this, {{profileIndex}}, {{memberID}}); return true;">
            {{usernameShort}}
        </a>
        <span class="info">{{age}} - {{location}}</span>
        <a href="{{viewMorePhotosURL}}" title="{{username}}" class="more-photo" onclick="matchSliderManager.onProfileLinkClick(this, {{profileIndex}}, {{memberID}}); return true;">
            {{viewMorePhotos}}
        </a>
        <div id="{{ynmContainerID}}" class="ynm-container">
            <a onclick="matchSliderManager.ProcessYNM({{profileIndex}},1,{{memberID}}); return false;" title="<%=g.GetResource("TXT_YNM_YES_ALT", this) %>">
                <span class="{{ynmYesCss}}" id={{ynmSpanYesID}}>Y</span>
            </a>
            <a onclick="matchSliderManager.ProcessYNM({{profileIndex}},2,{{memberID}}); return false;" title="<%=g.GetResource("TXT_YNM_NO_ALT", this) %>">
                <span class="{{ynmNoCss}}" id={{ynmSpanNoID}}>N</span>
            </a>
            <a onclick="matchSliderManager.ProcessYNM({{profileIndex}},3,{{memberID}}); return false;" title="<%=g.GetResource("TXT_YNM_MAYBE_ALT", this) %>">
                <span class="{{ynmMaybeCss}}" id={{ynmSpanMaybeID}}>M</span>
            </a>
        </div>
    </li>
</script>

<script type="text/javascript">
    var matchSliderManager = {
        profileList : [],
        memberID : 0,
        startRow : 1,
        previousStartRow : 1,
        pageSize : 0,
        totalResultCount : 0,
        updateInProgress : false,
        isVerbose : true,
        matchRatingEnabled : false,
        checkMatches : true,
        checkMOL : true,
        omnitureUsernameProfileURLTrackingParams : '',
        omniturePhotoProfileURLTrackingParams : '',
        omnitureViewMorePhotosProfileURLTrackingParams : '',
        omnitureControlName: '',
        entryPointID: 0,
        preloaded : true,
        loadingModeTypes : {
            0 : 'CappedBrowserCacheOnly',
            1 : 'AjaxNoBrowserCache',
            2 : 'AjaxWithBrowserCache'
        },
        loadingModeDefault : '',
        loadingModeActive : '',
        loadingCheckDuplicate : true,
        profileContainerID : 'match-slider-profiles-container',
        yesSliderContainerID : 'match-slider',
        noSliderContainerID : 'match-slider-none',
        buttonNextID : 'match-slider-button-next',
        buttonPrevID : 'match-slider-button-prev',
        ajaxLoaderID : 'ajax-loader',
        ynmYesCssDefault : 'secret-y',
        ynmYesCssSelected : 'secret-y on',
        ynmYesCssUnSelected : 'secret-y off',
        ynmNoCssDefault : 'secret-n',
        ynmNoCssSelected : 'secret-n on',
        ynmNoCssUnSelected : 'secret-n off',
        ynmMaybeCssDefault : 'secret-m',
        ynmMaybeCssSelected : 'secret-m on',
        ynmMaybeCssUnSelected : 'secret-m off',
        ynmInProgress : false,
        uiOptions : {
            speed: 1,
            sliderWidth : 890,
            profileCount : 5,
            profileWidth: 162,
            profileLayout: [10, 10, 10, 10]
        },
        createProfile : function (searchProfile) {
            var profile1 = {
                profileIndex: matchSliderManager.profileList.length,
                profileOrdinal: searchProfile.Ordinal,
                photoURL: searchProfile.PhotoURL,
                memberID: searchProfile.MemberID,
                usernameProfileURL: searchProfile.ProfileURLWithoutOrdinal + "&" + matchSliderManager.omnitureUsernameProfileURLTrackingParams,
                photoProfileURL: searchProfile.ProfileURLWithoutOrdinal + "&" + matchSliderManager.omniturePhotoProfileURLTrackingParams,
                username: searchProfile.Username,
                highlighted: searchProfile.Highlighted,
                usernameShort: searchProfile.UsernameShort,
                matchRating: searchProfile.MatchRating,
                matchRatingDisplay: searchProfile.MatchRating,
                gender: searchProfile.Gender,
                age: searchProfile.Age,
                location: searchProfile.Location,
                viewMorePhotos: searchProfile.ViewMorePhotos,
                viewMorePhotosURL: searchProfile.ProfileURLWithoutOrdinal + "&" + matchSliderManager.omnitureViewMorePhotosProfileURLTrackingParams,
                profileContainerID: "liProfileContainerID" + + searchProfile.MemberID, 
                ynmCurrentVote: searchProfile.YNMCurrentVote,
                ynmSpanYesID: 'ynmYesID' + searchProfile.MemberID,
                ynmSpanNoID: 'ynmNoID' + searchProfile.MemberID,
                ynmSpanMaybeID: 'ynmMaybeID' + searchProfile.MemberID,
                ynmContainerID: 'ynmContainerID' + searchProfile.MemberID,
                ynmYesCss: matchSliderManager.ynmYesCssDefault,
                ynmNoCss: matchSliderManager.ynmNoCssDefault,
                ynmMaybeCss: matchSliderManager.ynmMaybeCssDefault
            };

            if (searchProfile.MatchRating < 50){
                profile1.matchRatingDisplay = "&lt;50";
            }
            
            if ((searchProfile.YNMCurrentVote & 1) == 1) {
                profile1.ynmYesCss = matchSliderManager.ynmYesCssSelected;
                profile1.ynmNoCss = matchSliderManager.ynmNoCssUnSelected;
                profile1.ynmMaybeCss = matchSliderManager.ynmMaybeCssUnSelected;
            }
            else if ((searchProfile.YNMCurrentVote & 2) == 2) {
                profile1.ynmNoCss = matchSliderManager.ynmNoCssSelected;
                profile1.ynmYesCss = matchSliderManager.ynmYesCssUnSelected;
                profile1.ynmMaybeCss = matchSliderManager.ynmMaybeCssUnSelected;
            }
            else if ((searchProfile.YNMCurrentVote & 4) == 4) {
                profile1.ynmMaybeCss = matchSliderManager.ynmMaybeCssSelected;
                profile1.ynmYesCss = matchSliderManager.ynmYesCssUnSelected;
                profile1.ynmNoCss = matchSliderManager.ynmNoCssUnSelected;
            }

            return profile1;
        },
        onProfileLinkClick : function(linkObj, profileIndex, memberID) {
            //add dynamic parameters
            var profile = matchSliderManager.profileList[profileIndex];
            if (!profile || profile.memberID != memberID) {
                while ((!profile || profile.memberID != memberID) && profileIndex >= 0) {
                    profileIndex = profileIndex - 1;
                    profile = matchSliderManager.profileList[profileIndex];
                }
            }
            $j(linkObj).attr("href", $j(linkObj).attr("href") + "&Ordinal=" + profile.profileOrdinal);
        },
        getCurrentPageNumber : function(){
            var currentPage = 1;
            if ((matchSliderManager.startRow - 1) > 0){
                currentPage = Math.ceil(((matchSliderManager.startRow - 1) + matchSliderManager.pageSize) / matchSliderManager.pageSize);
            }
            return currentPage;
        },
        onNextPageClick : function () {
            //paginate to next page
            if (!matchSliderManager.updateInProgress) {
                matchSliderManager.getSliderProfiles(matchSliderManager.startRow + matchSliderManager.pageSize);
            }
        },
        onPrevPageClick : function () {
            //paginate to previous page
            if (!matchSliderManager.updateInProgress) {
                matchSliderManager.getSliderProfiles(matchSliderManager.startRow - matchSliderManager.pageSize);
            }
        },
        getSliderProfiles : function(newStartRow){
            if (!matchSliderManager.updateInProgress) {
                var requiresAjax = true;
                
                //set startRow
                matchSliderManager.previousStartRow = matchSliderManager.startRow;
                matchSliderManager.startRow = newStartRow;
                if (matchSliderManager.profileList.length <= 0 || matchSliderManager.startRow <= matchSliderManager.pageSize) {
                    matchSliderManager.startRow = 1;
                }

                //the actual startRow and pageSize may change due to a need for pulling fill-gap profiles
                var calculatedStartRow = matchSliderManager.startRow;
                var calculatedPageSize = matchSliderManager.pageSize;
                
                if (matchSliderManager.profileList.length >= matchSliderManager.totalResultCount) {
                    //already last page
                    requiresAjax = false;
                }
                else if (matchSliderManager.loadingModeActive == matchSliderManager.loadingModeTypes["0"]) {
                    //CappedBrowserCacheOnly
                    requiresAjax = false;
                }
                else if (matchSliderManager.loadingModeActive == matchSliderManager.loadingModeTypes["1"]) {
                    //AjaxNoBrowserCache
                    if (matchSliderManager.startRow <= matchSliderManager.totalResultCount) {
                        requiresAjax = true;
                    } else {
                        requiresAjax = false;
                    }
                    
                } 
                else {
                    //AjaxWithBrowserCache
                    if (matchSliderManager.startRow < matchSliderManager.profileList.length) {
                        requiresAjax = false;
                        
                        //due to potential of removing voted profiles, the next cached page of profiles may have less than the page size so we'll do a fillgap
                        if (matchSliderManager.profileList.length < (matchSliderManager.startRow + matchSliderManager.pageSize - 1)) {
                            
                            var remainingProfiles = matchSliderManager.profileList.length % matchSliderManager.pageSize;
                            if (remainingProfiles > 0) {
                                requiresAjax = true;
                                calculatedPageSize = matchSliderManager.pageSize - remainingProfiles;
                                calculatedStartRow = matchSliderManager.startRow + matchSliderManager.pageSize - calculatedPageSize;
                                if (calculatedStartRow >= matchSliderManager.totalResultCount) {
                                    requiresAjax = false;
                                }
                            }
                        }
                    }
                }

                if (requiresAjax){
                    $j('#' + matchSliderManager.ajaxLoaderID).show();

                    //get profiles from service
                    $j.ajax({
                        type: "POST",
                        url: "/Applications/API/SearchResults.asmx/GetSliderProfilesWithEntrypoint",
                        data: "{memberID:" + matchSliderManager.memberID + ", startRow:" + calculatedStartRow + ", pageSize:" + calculatedPageSize + ", checkMatches: " + matchSliderManager.checkMatches + ", checkMOL: " + matchSliderManager.checkMOL + ", entryPointID: " + matchSliderManager.entryPointID + "}",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        beforeSend: function () {
                            matchSliderManager.updateInProgress = true;
                        },
                        complete: function () {
                            matchSliderManager.updateInProgress = false;
                        },
                        success: function (result) {
                            var getSearchResult = (typeof result.d == 'undefined') ? result : result.d;
                            if (getSearchResult.Status != 2) {
                                $j('#' + matchSliderManager.ajaxLoaderID).hide();
                                if (getSearchResult.IsMemberNull) {
                                    //member is not logged in, will refresh page
                                    window.location.href = window.location.href;
                                }
                                else if (matchSliderManager.isVerbose) {
                                    alert(getSearchResult.StatusMessage);
                                }
                            }
                            else {
                                ////load profiles
                                try{
                                    console.log('next page - getSearchResult: ', getSearchResult);
                                }
                                catch(e) {}
                                matchSliderManager.loadPage(getSearchResult);

                                //update omniture
                                if (s != null) {
                                    PopulateS(true); //clear existing values in omniture "s" object
                                    var originalPageName = s.pageName;
                                    s.pageName = s.pageName + " - " + matchSliderManager.omnitureControlName;
                                    s.eVar25 = "" + matchSliderManager.getCurrentPageNumber() + "";
                                    s.t();
                                    s.pageName = originalPageName;
                                }
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            $j('#' + matchSliderManager.ajaxLoaderID).hide();
                            if (matchSliderManager.isVerbose) {
                                alert(textStatus);
                            }
                            try{
                                console.log('Error getting next page: ' + textStatus);
                            }
                            catch(e){}
                        }
                    });
                }
                else{
                    matchSliderManager.loadPage(null);
                }
            }
        },
        loadPage : function (getSearchResult) {
            var $ajaxLoader = $j('#' + matchSliderManager.ajaxLoaderID),
                $yesSliderContainer = $j('#' + matchSliderManager.yesSliderContainerID),
                $noSliderContainer = $j('#' + matchSliderManager.noSliderContainerID);

            $ajaxLoader.show();

            ////load profiles from JSON
            if (getSearchResult != null) {
                if (getSearchResult.TotalResults > 0 || matchSliderManager.totalResultCount == 0){
                    matchSliderManager.totalResultCount = getSearchResult.TotalResults;
                }
                matchSliderManager.checkMatches = getSearchResult.IsMatchResults;
                matchSliderManager.checkMOL = getSearchResult.IsMOLResults;
                if (getSearchResult.SearchProfileList.length > 0) {
                    
                    var tempProfileList = [];
                    var duplicateStartIndex = matchSliderManager.profileList.length - 1;
                    for (var i = 0; i < getSearchResult.SearchProfileList.length; i++) {
                        var searchProfile = getSearchResult.SearchProfileList[i];
                        var profile1 = matchSliderManager.createProfile(searchProfile);
                        
                        if (matchSliderManager.loadingCheckDuplicate && matchSliderManager.loadingModeActive == matchSliderManager.loadingModeTypes["2"]) {
                            if (matchSliderManager.startRow > 1 && matchSliderManager.startRow > matchSliderManager.previousStartRow && duplicateStartIndex >= 0) {
                                //check for duplicate profiles, which means server side cache expired, we'll switch over to ajax only temporarily
                                //to avoid duplicate profile issues with existing profiles cached in js
                                var checkNum = matchSliderManager.pageSize + 2;
                                var checkCount = 0;
                                for (var k = duplicateStartIndex; k < matchSliderManager.profileList.length && k >= 0; k--) {
                                    checkCount++;
                                    if (matchSliderManager.profileList[k].memberID == profile1.memberID) {
                                        matchSliderManager.loadingModeActive = matchSliderManager.loadingModeTypes["1"];
                                        break;
                                    } else if (checkCount >= checkNum) {
                                        break;
                                    }
                                }
                            }
                        }
                        matchSliderManager.profileList.push(profile1);
                        tempProfileList.push(profile1);
                    }
                    
                    if (matchSliderManager.loadingModeActive == matchSliderManager.loadingModeTypes["1"]) {
                        //AjaxNoBrowserCache, we'll clear out browser cached list by replacing it with current profiles only
                        matchSliderManager.profileList = tempProfileList;
                    }
                }
            }

            if (matchSliderManager.profileList.length > 0){
                
                //show profiles
                $yesSliderContainer.show();
                $noSliderContainer.hide();
                
                //recheck startRow
                if (matchSliderManager.startRow <= matchSliderManager.pageSize) {
                    matchSliderManager.startRow = 1;
                } else if (matchSliderManager.startRow > matchSliderManager.totalResultCount && matchSliderManager.startRow > matchSliderManager.profileList.length) {
                    matchSliderManager.startRow = matchSliderManager.previousStartRow;
                }

                //load page html
                var startIndex = matchSliderManager.startRow - 1,
                    endIndex = null;
                
                if (startIndex <= 0){
                    startIndex = 0;
                }
                
                if (matchSliderManager.loadingModeActive == matchSliderManager.loadingModeTypes["1"]) {
                    //AjaxNoBrowserCache, the profileList always only contains one page of profile
                    startIndex = 0;
                }
                
                endIndex = startIndex + matchSliderManager.pageSize - 1;

                if (endIndex >= matchSliderManager.profileList.length){
                    endIndex = matchSliderManager.profileList.length - 1;
                }

                var photohtml = '',
                    phototemplate = '';
                
                phototemplate = $j('#profile_template').html();
                
                for (var i = startIndex; i <= endIndex; i++){
                    photohtml += Mustache.to_html(phototemplate, matchSliderManager.profileList[i]);  
                }

                var $newItems = $j('<ul>').html(photohtml),
                    $container = $j('#' + matchSliderManager.profileContainerID),
                    scrollWidth = $container.innerWidth();
                
                $newItems.find('> li').each(function(index){ //apply left position for the 'n' animation
                                        $j(this).css('left', index * (matchSliderManager.uiOptions.profileWidth + matchSliderManager.uiOptions.profileLayout[0]));
                                    });

                function scroller($items, dir, callback){
                    $items.animate({
                        left: (dir == 'left') ? '-=' + scrollWidth : '+=' + scrollWidth
                    }, {queue: false, duration:1500, complete: callback});
                }

                if(matchSliderManager.previousStartRow < matchSliderManager.startRow){
                    var $oldItems = $container.find('ul');

                    $container.append($newItems);
                    $newItems.css({left: scrollWidth + 'px'});
                    scroller($oldItems, 'left', function(){$oldItems.remove()});
                    scroller($newItems, 'left');

                } else if(matchSliderManager.previousStartRow > matchSliderManager.startRow){
                    var $oldItems = $container.find('ul');

                    $container.prepend($newItems);
                    $newItems.css({left: '-' + scrollWidth + 'px'});
                    scroller($oldItems, '', function(){$oldItems.remove()});
                    scroller($newItems, '');

                } else {
                    $container.html($newItems);
                }

                try { console.log('Loaded match slider page: ' + matchSliderManager.getCurrentPageNumber() + ', Total Results: ' + matchSliderManager.totalResultCount + ', Mode: ' + matchSliderManager.loadingModeActive);
                } catch(e) {}

                if (matchSliderManager.totalResultCount > 0) {
                    //hide or show next/prev
                    var currentPageNumber = matchSliderManager.getCurrentPageNumber();
                    var totalPages = Math.ceil(matchSliderManager.totalResultCount / matchSliderManager.pageSize);

                    if (currentPageNumber > 1) {
                        $j('#' + matchSliderManager.buttonPrevID).removeClass('disabled');
                    } else {
                        $j('#' + matchSliderManager.buttonPrevID).addClass('disabled');
                    }

                    if (currentPageNumber < totalPages) {
                        $j('#' + matchSliderManager.buttonNextID).removeClass('disabled');
                    } else {
                        $j('#' + matchSliderManager.buttonNextID).addClass('disabled');
                    }

                    //this is first page, we'll check if we should switch back to ajax with caching
                    if (currentPageNumber == 1 && matchSliderManager.loadingModeDefault == matchSliderManager.loadingModeTypes["2"]) {
                        matchSliderManager.loadingModeActive = matchSliderManager.loadingModeTypes["2"];
                    }
                }
            }
            else{
                //show no profiles content
                $yesSliderContainer.hide();
                $noSliderContainer.show();
            }

            $ajaxLoader.hide();
        },
        ProcessYNM : function(profileIndex, newVoteType, memberID) {
            if (!matchSliderManager.ynmInProgress) {
                try {
                    matchSliderManager.ynmInProgress = true;
                    
                    //note: newVoteType is ClickType whereas profile.ynmCurrentVote is ClickMask
                    var profile;

                    if (matchSliderManager.profileList.length > profileIndex) {
                        profile = matchSliderManager.profileList[profileIndex];
                    }
                    if (!profile || profile.memberID != memberID) {
                        while ((!profile || profile.memberID != memberID) && profileIndex >= 0) {
                            profileIndex = profileIndex - 1;
                            profile = matchSliderManager.profileList[profileIndex];
                        }
                    }

                    //TODO: check if we need to provide alert for changing vote
                    var confirmedUserWantsToChangeYNM = true;

                    if (confirmedUserWantsToChangeYNM && profile) {
                        //console.log('current vote: ' + profile.ynmCurrentVote);

                        $j.ajax({
                            type: "POST",
                            url: "/Applications/API/Hotlist.asmx/YNMVote",
                            data: "{voteType:" + newVoteType + ", fromMemberID:" + matchSliderManager.memberID + ", toMemberID:" + profile.memberID + "}",
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function(result) {
                                var updateResult = (typeof result.d == 'undefined') ? result : result.d;
                                if (updateResult.Status != 2) {
                                    try {
                                        console.log('Error updating YNM Vote for ' + profile.memberID);
                                    } catch(e) {
                                    }
                                } else {
                                    try {
                                        console.log('Updated YNM Vote for ' + profile.memberID);
                                    } catch(e) {
                                    }
                                }
                            },
                            error: function(jqXHR, textStatus, errorThrown) {
                                try {
                                    console.log('Error updating YNM Vote: ' + textStatus);
                                } catch(e) {
                                }
                            }
                        });

                        $j('#' + profile.ynmSpanYesID).removeClass().addClass(matchSliderManager.ynmYesCssDefault);
                        $j('#' + profile.ynmSpanNoID).removeClass().addClass(matchSliderManager.ynmNoCssDefault);
                        $j('#' + profile.ynmSpanMaybeID).removeClass().addClass(matchSliderManager.ynmMaybeCssDefault);
                        profile.ynmYesCss = matchSliderManager.ynmYesCssDefault;
                        profile.ynmNoCss = matchSliderManager.ynmNoCssDefault;
                        profile.ynmMaybeCss = matchSliderManager.ynmMaybeCssDefault;

                        var ynmType = '';
                        switch (newVoteType) {
                        case 1:
                            ynmType = "Y";
                            if ((profile.ynmCurrentVote & 2) == 2) {
                                profile.ynmCurrentVote = profile.ynmCurrentVote ^ 2;
                            }
                            if ((profile.ynmCurrentVote & 4) == 4) {
                                profile.ynmCurrentVote = profile.ynmCurrentVote ^ 4;
                            }
                            profile.ynmCurrentVote = profile.ynmCurrentVote | 1;
                            profile.ynmYesCss = matchSliderManager.ynmYesCssSelected;
                            profile.ynmNoCss = matchSliderManager.ynmNoCssUnSelected;
                            profile.ynmMaybeCss = matchSliderManager.ynmMaybeCssUnSelected;
                            $j('#' + profile.ynmSpanYesID).removeClass().addClass(matchSliderManager.ynmYesCssSelected);
                            $j('#' + profile.ynmSpanNoID).removeClass().addClass(matchSliderManager.ynmNoCssUnSelected);
                            $j('#' + profile.ynmSpanMaybeID).removeClass().addClass(matchSliderManager.ynmMaybeCssUnSelected);
                            break;
                        case 2:
                            ynmType = "N";
                            if ((profile.ynmCurrentVote & 1) == 1) {
                                profile.ynmCurrentVote = profile.ynmCurrentVote ^ 1;
                            }
                            if ((profile.ynmCurrentVote & 4) == 4) {
                                profile.ynmCurrentVote = profile.ynmCurrentVote ^ 4;
                            }
                            profile.ynmCurrentVote = profile.ynmCurrentVote | 2;
                            profile.ynmNoCss = matchSliderManager.ynmNoCssSelected;
                            profile.ynmMaybeCss = matchSliderManager.ynmMaybeCssUnSelected;
                            profile.ynmYesCss = matchSliderManager.ynmYesCssUnSelected;
                            $j('#' + profile.ynmSpanNoID).removeClass().addClass(matchSliderManager.ynmNoCssSelected);
                            $j('#' + profile.ynmSpanYesID).removeClass().addClass(matchSliderManager.ynmYesCssUnSelected);
                            $j('#' + profile.ynmSpanMaybeID).removeClass().addClass(matchSliderManager.ynmMaybeCssUnSelected);
                            break;
                        case 3:
                            ynmType = "M";
                            if ((profile.ynmCurrentVote & 1) == 1) {
                                profile.ynmCurrentVote = profile.ynmCurrentVote ^ 1;
                            }
                            if ((profile.ynmCurrentVote & 2) == 2) {
                                profile.ynmCurrentVote = profile.ynmCurrentVote ^ 2;
                            }
                            profile.ynmCurrentVote = profile.ynmCurrentVote | 4;
                            profile.ynmMaybeCss = matchSliderManager.ynmMaybeCssSelected;
                            profile.ynmNoCss = matchSliderManager.ynmNoCssUnSelected;
                            profile.ynmYesCss = matchSliderManager.ynmYesCssUnSelected;
                            $j('#' + profile.ynmSpanMaybeID).removeClass().addClass(matchSliderManager.ynmMaybeCssSelected);
                            $j('#' + profile.ynmSpanYesID).removeClass().addClass(matchSliderManager.ynmYesCssUnSelected);
                            $j('#' + profile.ynmSpanNoID).removeClass().addClass(matchSliderManager.ynmNoCssUnSelected);
                            break;
                        }

                        //update omniture
                        if (s != null) {
                            PopulateS(true); //clear existing values in omniture "s" object

                            var originalPageName = s.pageName;
                            s.pageName = s.pageName + " - Secret Admirer";
                            //s.events = "event41";
                            s.prop30 = ynmType + " - " + originalPageName + " - " + matchSliderManager.omnitureControlName;
                            s.prop31 = profile.memberID;
                            s.t(); //send omniture updated values as page load
                            s.events = '';
                            s.pageName = originalPageName;
                        }

                        if (newVoteType == 2) {
                            matchSliderManager.RemoveVotedProfile(profileIndex);
                        } else {
                            matchSliderManager.ynmInProgress = false;
                        }

                    }

                    //console.log('final current vote: ' + profile.ynmCurrentVote);
                } catch(e) {
                    //alert(e);
                }
            }
        },
        RemoveVotedProfile : function(profileIndex) {
            //REMOVE PROFILE FROM FILMSTRIP AND SLIDE IN NEXT PROFILE
            $j('#' + matchSliderManager.ajaxLoaderID).show();
            
            //profile to remove
            var votedProfile = matchSliderManager.profileList[profileIndex];
            var removedProfileMemberID = votedProfile.memberID;
            
            //remove current profile data
            matchSliderManager.totalResultCount = matchSliderManager.totalResultCount - 1;
            if (matchSliderManager.profileList.length == 1) {
                matchSliderManager.profileList = [];
            } else {
                matchSliderManager.profileList.splice(profileIndex, 1);
            }
            
            //update subsequent profile data to fix ordinal and index
            if (matchSliderManager.profileList.length > profileIndex) {
                for (var i = profileIndex; i < matchSliderManager.profileList.length; i++) {
                    matchSliderManager.profileList[i].profileIndex = i;
                    matchSliderManager.profileList[i].profileOrdinal = matchSliderManager.profileList[i].profileOrdinal - 1;
                }
            }
            
            //get next profile, if any
            var nextStartRow = matchSliderManager.startRow + matchSliderManager.pageSize - 1; //subtract 1 for removed item
            var nextProfileIndex = nextStartRow - 1; //index is zero-based so one less than startRow
            var nextProfile;
            var loadNextAjax = true;
            var foundDuplicateProfile = false;
            var checkNum = 1;
            var checkCount = 0;
            if (matchSliderManager.profileList.length > 0) {
                if (nextProfileIndex < matchSliderManager.profileList.length) {
                    //get next profile from js cache
                    nextProfile = matchSliderManager.profileList[nextProfileIndex];
                    foundDuplicateProfile = false;
                    
                    checkNum = matchSliderManager.pageSize;
                    checkCount = 0;
                    var duplicateStartIndex = nextProfileIndex - 1;
                    if (matchSliderManager.loadingCheckDuplicate) {
                        for (var k = duplicateStartIndex; k < matchSliderManager.profileList.length && k >= 0; k--) {
                            checkCount++;
                            if ((matchSliderManager.profileList[k].memberID == nextProfile.memberID) || (nextProfile.memberID == removedProfileMemberID)) {
                                foundDuplicateProfile = true;
                                try {
                                    console.log('Next profile duplicate found non-ajax: ' + nextProfile.memberID);
                                } catch(e) {
                                }
                                break;
                            } else if (checkCount >= checkNum) {
                                break;
                            }
                        }
                    }

                    if (foundDuplicateProfile) {
                        nextProfile = null;
                        var remainderCount = matchSliderManager.profileList.length - nextProfileIndex;
                        matchSliderManager.profileList.splice(nextProfileIndex, remainderCount);
                    } else {
                        loadNextAjax = false;
                        matchSliderManager.loadNextProfile(votedProfile, nextProfile);
                    }
                }
                
                if (loadNextAjax && nextStartRow < matchSliderManager.totalResultCount) {
                    //get next profile from service
                    $j.ajax({
                        type: "POST",
                        url: "/Applications/API/SearchResults.asmx/GetSliderProfilesWithEntrypoint",
                        data: "{memberID:" + matchSliderManager.memberID + ", startRow:" + nextStartRow + ", pageSize:5, checkMatches: " + matchSliderManager.checkMatches + ", checkMOL: " + matchSliderManager.checkMOL + ", entryPointID: " + matchSliderManager.entryPointID + "}",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        beforeSend: function() {
                            matchSliderManager.updateInProgress = true;
                        },
                        complete: function() {
                            matchSliderManager.updateInProgress = false;
                        },
                        success: function(result) {
                            var getSearchResult = (typeof result.d == 'undefined') ? result : result.d;
                            if (getSearchResult.Status != 2) {
                                $j('#' + matchSliderManager.ajaxLoaderID).hide();
                                if (getSearchResult.IsMemberNull) {
                                    //member is not logged in, will refresh page
                                    window.location.href = window.location.href;
                                }
                                else if (matchSliderManager.isVerbose) {
                                    alert(getSearchResult.StatusMessage);
                                }
                            } else {
                                try {
                                    console.log('next profile - getSearchResult: ', getSearchResult);
                                } catch(e) {
                                }

                                ////load profile from JSON
                                if (getSearchResult.TotalResults > 0 || matchSliderManager.totalResultCount == 0) {
                                    matchSliderManager.totalResultCount = getSearchResult.TotalResults;
                                }
                                matchSliderManager.checkMatches = getSearchResult.IsMatchResults;
                                matchSliderManager.checkMOL = getSearchResult.IsMOLResults;
                                if (getSearchResult.SearchProfileList.length > 0) {
                                    for (var p = 0; p < getSearchResult.SearchProfileList.length; p++) {
                                        foundDuplicateProfile = false;
                                        var searchProfile = getSearchResult.SearchProfileList[p];
                                        nextProfile = matchSliderManager.createProfile(searchProfile);

                                        checkNum = matchSliderManager.pageSize;
                                        checkCount = 0;
                                        if (matchSliderManager.loadingCheckDuplicate) {
                                            for (var k = matchSliderManager.profileList.length - 1; k < matchSliderManager.profileList.length && k >= 0; k--) {
                                                checkCount++;
                                                if ((matchSliderManager.profileList[k].memberID == nextProfile.memberID) || (nextProfile.memberID == removedProfileMemberID)) {
                                                    foundDuplicateProfile = true;
                                                    try {
                                                        console.log('Next profile duplicate found: ' + nextProfile.memberID);
                                                    } catch(e) {
                                                    }
                                                    break;
                                                } else if (checkCount >= checkNum) {
                                                    break;
                                                }
                                            }
                                        }

                                        if (!foundDuplicateProfile) {
                                            break;
                                        }
                                    }
                                }
                                if (nextProfile) {
                                    matchSliderManager.profileList.push(nextProfile);
                                } else {
                                    //no more profiles found or only duplicates
                                    matchSliderManager.totalResultCount = nextStartRow - 1;
                                }

                                matchSliderManager.loadNextProfile(votedProfile, nextProfile);
                            }
                        },
                        error: function(jqXHR, textStatus, errorThrown) {
                            matchSliderManager.loadNextProfile(votedProfile, nextProfile);
                            if (matchSliderManager.isVerbose) {
                                alert(textStatus);
                            }
                            try {
                                console.log('Error getting next profile: ' + textStatus);
                            } catch(e) {
                            }
                        }
                    });
                } else {
                    matchSliderManager.loadNextProfile(votedProfile, nextProfile);
                }
            } else {
                matchSliderManager.totalResultCount = 0;
                matchSliderManager.loadNextProfile(votedProfile, nextProfile);
            }
        },
        loadNextProfile : function(votedProfile, nextProfile) {
            var uiOptions = matchSliderManager.uiOptions,
                container = $j('#' + matchSliderManager.profileContainerID);

            //add next profile to display
            if (nextProfile) {
                try {
                    console.log('Loading next profile: ' + nextProfile.memberID);
                } catch(e) {
                }
                var photohtml = '',
                    phototemplate = '',
                    currentItems = container.find('ul');
				
				phototemplate = $j('#profile_template').html();

				photohtml += Mustache.to_html(phototemplate, nextProfile);
                
                currentItems.append(photohtml);

                // place next profie at the end of the list
                $j('#' + nextProfile.profileContainerID).css('left', uiOptions.profileCount * (uiOptions.profileWidth + uiOptions.profileLayout[0]));
            }

            if (votedProfile) {
                //remove current profile from display and apply slide-in animation for next profiles
                var $this = $j('#' + votedProfile.profileContainerID);

                    $this.fadeOut(750 / uiOptions.speed, function(){
                        $this.nextAll('li').each(function(){
                            var $$this = $j(this);

                            $$this.animate({
                                left: $$this.position().left - (uiOptions.profileWidth + uiOptions.profileLayout[0])
                            }, 350 / uiOptions.speed);
                        });

                    $this.remove();
                    matchSliderManager.ynmInProgress = false;
                });
            } else {
                matchSliderManager.ynmInProgress = false;
            }
            
            //check to hide next page arrow
            var loadPreviousPage = false;
            if (nextProfile) {
                var totalProfilesUpToCurrentPage = (matchSliderManager.startRow + matchSliderManager.pageSize - 1);
                if (totalProfilesUpToCurrentPage >= matchSliderManager.profileList.length
                    && totalProfilesUpToCurrentPage >= matchSliderManager.totalResultCount) {
                    $j('#' + matchSliderManager.buttonNextID).addClass('disabled');
                }
            } else if (matchSliderManager.profileList.length == 0 || matchSliderManager.totalResultCount == 0) {
                //filmstrip is now empty
                $j('#' + matchSliderManager.yesSliderContainerID).hide();
                $j('#' + matchSliderManager.noSliderContainerID).show();
            } 
            else {
                $j('#' + matchSliderManager.buttonNextID).addClass('disabled');
                
                //current filmstrip page is empty, auto-load previous page
                if ((matchSliderManager.profileList.length % matchSliderManager.pageSize) <= 0) {
                    loadPreviousPage = true;
                    matchSliderManager.onPrevPageClick();
                }
            }

            if (!loadPreviousPage) {
                $j('#' + matchSliderManager.ajaxLoaderID).hide();
            }
        },
        initialize : function (){
            //initialize
            matchSliderManager.memberID = <%=_MemberID%>;
            matchSliderManager.pageSize = <%=_PageSize%>;
            matchSliderManager.loadingModeActive = matchSliderManager.loadingModeTypes["<%=_LoadingTypeID%>"];
            matchSliderManager.loadingModeDefault = matchSliderManager.loadingModeTypes["<%=_LoadingTypeID%>"];
            matchSliderManager.omnitureUsernameProfileURLTrackingParams = '<%=_OmnitureUsernameProfileURLTrackingParams%>';
            matchSliderManager.omniturePhotoProfileURLTrackingParams = '<%=_OmniturePhotoProfileURLTrackingParams%>';
            matchSliderManager.omnitureViewMorePhotosProfileURLTrackingParams = '<%=_OmnitureViewMorePhotosProfileURLTrackingParams%>';
            matchSliderManager.omnitureControlName = '<%=_OmnitureControlName%>';
            matchSliderManager.matchRatingEnabled = <%=_matchRatingEnabled.ToString().ToLower()%>;
            matchSliderManager.entryPointID = <%=((int)_entryPoint)%>;

            $j('#match-slider-button-prev').click(function(e) {
                e.preventDefault();
                if (!$j('#match-slider-button-prev').hasClass('disabled')){
                    matchSliderManager.onPrevPageClick();
                }
            });

            $j('#match-slider-button-next').click(function(e) {
                e.preventDefault();
                if (!$j('#match-slider-button-next').hasClass('disabled')){
                    matchSliderManager.onNextPageClick();
                }
            });
            
            //no preload
            //matchSliderManager.getSliderProfiles(1);

            //add 'ynm' to the match-slider-profiles-container
            $j('#match-slider-profiles-container').addClass('ynm');

            //yes preloaded
            <asp:Literal ID="litPreloadJS" runat="server"></asp:Literal>
        }
    };

    $j(function() {
        matchSliderManager.initialize();
    });
    
    
</script>
