﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SlideshowProfileAPI.ascx.cs" Inherits="Matchnet.Web.Applications.Home.SlideshowProfileAPI" %>
<%@ Register src="/Framework/Ui/BasicElements/SlideshowProfile.ascx" tagname="SlideshowProfile" tagprefix="uc1" %>
<%@ Register src="/Framework/Ui/BasicElements/MiniSlideshowProfile.ascx" tagname="MiniSlideshowProfile" tagprefix="uc1" %>
<form id="Form1" method="post" runat="server">
<uc1:SlideshowProfile ID="ucSlideshowProfile" runat="server" Visible="false"/>
<uc1:MiniSlideshowProfile ID="ucMiniSlideshowProfile" runat="server" Visible="false" />
</form>
