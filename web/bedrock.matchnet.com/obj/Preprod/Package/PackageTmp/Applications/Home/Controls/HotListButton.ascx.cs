﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Content.ServiceAdapters.Links;
using Matchnet.List.ValueObjects;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Managers;

namespace Matchnet.Web.Applications.Home.Controls
{
    public partial class HotListButton : FrameworkControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public void LoadHotListButton(HotListCategory categoryId, string titleResourceConstant)
        {
            LoadHotListButton(categoryId, titleResourceConstant, 0);
        }

        public void LoadHotListButton(HotListCategory categoryId, string titleResourceConstant, int notificationCount)
        {
            if(notificationCount == 0 )
            {
                divNotificationCount.Visible = false;
            }
            else
            {
                divNotificationCount.InnerText = (notificationCount < 100) ? notificationCount.ToString() : "99+";
            }

            txtHotListTitle.ResourceConstant = titleResourceConstant;
            txtHotListTitle.TitleResourceConstant = titleResourceConstant;
            txtHotListTitle.Href = txtHotListTitle.Href + categoryId.ToString("d");
            txtHotListTitle.Href = g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.View, WebConstants.Action.HotlistButton, txtHotListTitle.Href, "Hotlist Module");

            //viewed your profile as subscriber feature
            if (categoryId == HotListCategory.WhoViewedYourProfile)
            {
                ListManager listManager = new ListManager();
                if (!listManager.IsViewedYourProfileListAvailableToMember(g.Member, g.Brand))
                {
                    txtHotListTitle.Href = FrameworkGlobals.GetSubscriptionLink(false, (int)PurchaseReasonType.HotlistViewedYourProfile);
                }

            }
        }
    }
}