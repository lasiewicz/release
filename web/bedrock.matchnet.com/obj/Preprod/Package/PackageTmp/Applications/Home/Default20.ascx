﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Default20.ascx.cs" Inherits="Matchnet.Web.Applications.Home.Default20" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mnl" Namespace="Matchnet.Web.Framework.Ui.BasicElements.Links"  Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mnbe" Namespace="Matchnet.Web.Framework.Ui.BasicElements"   Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<%@ Register Src="../../Framework/Ui/BasicElements/HeroProfile.ascx" TagName="HeroProfile"   TagPrefix="uc2" %>
<%@ Register Src="../../Framework/Ui/BasicElements/MicroProfile20.ascx" TagName="MicroProfile20" TagPrefix="uc2" %>
<%@ Register TagPrefix="uc1" TagName="SpotlightInfoDisplay" Src="../../Framework/Ui/BasicElements/SpotlightInfoDisplay20.ascx" %>
<%@ Register TagPrefix="uc1" TagName="HotlistProfileStrip" Src="/Applications/Hotlist/HotlistProfileStrip.ascx" %>
<%@ Register TagPrefix="uc1" TagName="MicroProfileList" Src="../../Framework/Ui/BasicElements/MicroProfileList.ascx" %>
<%@ Register TagPrefix="uc1" TagName="MicroProfileStrip" Src="../../Framework/Ui/BasicElements/MicroProfileStrip.ascx" %>
<%@ Register TagPrefix="uc1" TagName="HotListBar" Src="/Applications/Hotlist/HotListBar.ascx" %>
<%@ Register TagPrefix="qa" TagName="MiniQuestionModule" Src="~/Applications/QuestionAnswer/Controls/MiniQuestionModule.ascx" %>
<%@ Register TagPrefix="qa" TagName="MiniAnswerModule" Src="~/Applications/QuestionAnswer/Controls/MiniAnswerModule.ascx" %>
<%@ Register TagPrefix="uc1" TagName="SlideshowProfile" Src="../../Framework/Ui/BasicElements/SlideshowProfile.ascx" %>
<%@ Register TagPrefix="uc2" TagName="ProfileFilmStrip" Src="~/Framework/Ui/BasicElements/ProfileFilmStrip.ascx" %>
<%@ Register TagPrefix="mn" TagName="AdUnit" Src="/Framework/UI/Advertising/AdUnit.ascx" %>
<%@ Register TagPrefix="qa" TagName="SubmitQuestionClientModule" Src="~/Applications/QuestionAnswer/Controls/SubmitQuestionClientModule.ascx" %>

<div id="page-container">
    <div id="hp-hero" class="border-box clearfix <asp:Literal id='litMainClass' runat='server' />">
        <asp:PlaceHolder ID="phHotLists" runat="server">
            <div id="hp-hotlist-stats">
                <uc1:HotListBar id="ucHotlist2" runat="server" Direction="2" ClassSelected="selected"
                    Class="hp-hotlist-stats" ListItemClass="" />
                <uc1:HotListBar id="ucHotlist1" runat="server" Direction="1" ClassSelected="selected"
                    Class="hp-hotlist-stats" ListItemClass="" />
                <p class="view-more">
                    <a href="/Applications/HotList/View.aspx?CategoryID=-1">
                        <mn2:FrameworkLiteral ID="FrameworkLiteral6" runat="server" ResourceConstant="TXT_VIEW_ALL_HOTLISTS"></mn2:FrameworkLiteral></a></p>
                <asp:PlaceHolder ID="phShortcutList" runat="server" Visible="false">
                    <h2 class="component-header">
                        <mn:Txt ID="txtShortcutHeader" runat="server" ResourceConstant="TXT_SHORTCUT" />
                    </h2>
                    <ul class="hp-hotlist-stats">
                        <li><a name="noclick"><span class="spr s-icon-history-blank"><span></span></span></a>
                            &nbsp;<a href="/Applications/HotList/View.aspx?CategoryID=-9"><mn:Txt ID="txtShortcutYourCheckingOut"
                                runat="server" ResourceConstant="TXT_SHORTCUT_YOUR_CHECKING_OUT" ExpandImageTokens="true" />
                            </a></li>
                        <li><a name="noclick">
                            <asp:MultiView ID="multiviewShortcutMailIcon" runat="server" EnableViewState="false"
                                ActiveViewIndex="0">
                                <asp:View ID="viewShortcutMailBlankIcon" runat="server">
                                    <span class="spr s-icon-history-blank"><span></span></span>
                                </asp:View>
                                <asp:View ID="viewShortcutMailFlashingIcon" runat="server">
                                    <span class="spr s-icon-icon-email-new"><span></span></span>
                                </asp:View>
                            </asp:MultiView></a>&nbsp;<a href="/Applications/Email/MailBox.aspx"><mn:Txt ID="txtShortcutMail"
                                runat="server" ResourceConstant="TXT_SHORTCUT_MAIL" />
                                <asp:Literal ID="literalShortcutNewMailCount" runat="server"></asp:Literal></a>
                        </li>
                        <li><a name="noclick"><span class="spr s-icon-history-blank"><span></span></span></a>
                            &nbsp;<a href="/Applications/HotList/View.aspx?CategoryID=-10"><mn:Txt ID="txtShortcutMatches"
                                runat="server" ResourceConstant="TXT_SHORTCUT_MATCHES" />
                                <asp:Literal ID="literalShortcutNewMatchesCount" runat="server"></asp:Literal></a>
                        </li>
                        <li><a name="noclick"><span class="spr s-icon-history-blank"><span></span></span></a>
                            &nbsp;<a href="/Applications/PhotoGallery/PhotoGallery.aspx"><mn:Txt ID="txtShortcutPhotos"
                                runat="server" ResourceConstant="TXT_SHORTCUT_PHOTOS" />
                                <asp:Literal ID="literalShortcutNewPhotosCount" runat="server"></asp:Literal></a>
                        </li>
                    </ul>
                </asp:PlaceHolder>
            </div>
        </asp:PlaceHolder>
        <asp:PlaceHolder ID="phMemberSlideshow" runat="server" Visible="false">
            <div id="slideshow">
                <uc1:SlideshowProfile ID="ucSlideshowProfile" runat="server" />
            </div>
        </asp:PlaceHolder>
        <asp:PlaceHolder ID="phHeroSpotlightTitle" runat="server" Visible="false">
            <h2 class="component-header">
                <mn2:FrameworkLiteral ID="literalSpotlightTitle" runat="server" ResourceConstant="TXT_SPOTLIGHT_TITLE"></mn2:FrameworkLiteral></h2>
            <div class="links">
                <mn:Txt ID="SearchPreferences" runat="server" Href="/Applications/Search/SearchPreferences.aspx"
                    ResourceConstant="REVISE_YOUR_SEARCH_PREFERENCES" />
            </div>
            <div class="pop-layer">
                <!-- SpotlightInfoDisplay -->
                <uc1:SpotlightInfoDisplay runat="server" ID="ucSpotlightProfileInfoDisplay" ShowImageNoText="true" />
            </div>
            <div class="your-matches-view-more">
                <h3 class="component-header-second">
                    <mn2:FrameworkLiteral ID="literalMoreMatchesTitle" runat="server" ResourceConstant="TXT_MOREMATCHES_TITLE"></mn2:FrameworkLiteral></h3>
                <div class="pagination-buttons image-text-pair">
                    <a href="/Applications/Search/SearchResults.aspx" class="button btn link-secondary small"><span class="next">
                        <mn2:FrameworkLiteral ID="ViewMore1" runat="server" ResourceConstant="TXT_VIEW_MORE"></mn2:FrameworkLiteral></span><span
                            class="direction"><mn:Txt ID="directionArrow" runat="server" ResourceConstant="DIRECTION_ARROW" />
                        </span></a>
                </div>
            </div>
        </asp:PlaceHolder>
        <asp:PlaceHolder ID="phHeroMoreMatchesTitle" runat="server" Visible="false">
            <div class="clearfix">
                <h2 class="component-header">
                    <mn2:FrameworkLiteral ID="FrameworkLiteral3" runat="server" ResourceConstant="TXT_MOREMATCHES_TITLE"></mn2:FrameworkLiteral></h2>
                <div class="links">
                    <mn:Txt ID="SearchPreferences1" runat="server" Href="/Applications/Search/SearchPreferences.aspx"
                        ResourceConstant="REVISE_YOUR_SEARCH_PREFERENCES" />
                </div>
                <div class="pagination-buttons image-text-pair">
                    <a class="button btn link-secondary small" href="/Applications/Search/SearchResults.aspx"><span class="next">
                        <mn2:FrameworkLiteral ID="FrameworkLiteral1" runat="server" ResourceConstant="TXT_VIEW_MORE"></mn2:FrameworkLiteral></span><span
                            class="direction"><mn:Txt ID="Txt1" runat="server" ResourceConstant="DIRECTION_ARROW" />
                        </span></a>
                </div>
            </div>
        </asp:PlaceHolder>
        <asp:PlaceHolder ID="phHeroAndMatchesProfile" runat="server" Visible="false">
            <!--hero profile-->
            <uc2:HeroProfile ID="heroProfile" runat="server" DisplayContext="Default" />
            <!--your matches-->
            <asp:PlaceHolder ID="phHeroMatches" runat="server">
                <div id="hp-hero-your-matches">
                    <uc1:MicroProfileList ID="listMatches" runat="server" />
                </div>
            </asp:PlaceHolder>
        </asp:PlaceHolder>
        <!--big marketing copy, no hero or your matches-->
        <asp:PlaceHolder ID="phNoHeroAndMatchesMarketingCopy" runat="server" Visible="false">
            <div class="marketing">
                <mn:Txt ID="txtNoHeroAndMatchesMarketingCopy" runat="server" ResourceConstant="NO_HERO_AND_MATCHES_MARKETING"
                    ExpandImageTokens="true" />
            </div>
        </asp:PlaceHolder>
    </div>
    <!-- end hp-hero -->
    <%--Filmstrip for your matches--%>
    <asp:PlaceHolder ID="phFilmstrip" runat="server" Visible="false">
        <uc2:ProfileFilmStrip ID="ProfileFilmStripYourMatches" runat="server" TrackingParam="ClickFrom=Homepage - Your Matches" />
    </asp:PlaceHolder>
    <%--GAM Module--%>
    <asp:PlaceHolder ID="phGAMChannels" runat="server" Visible="false">
        <div id="hp-gam-slots" class="grid-block-full">
            <div class="header-options clearfix">
                <h2 class="component-header">
                    <mn:Txt ID="mntxt7105" runat="server" ResourceConstant="TXT_GAM_MODULE_HEADER" ExpandImageTokens="true" />
                </h2>
            </div>
            <div id="hp-gam-slots-container" class="clear-both clearfix">
                <div class="column">
                    <mn:AdUnit id="AdUnitGAMChannel3" runat="server" expandImageTokens="true" Size="Square"
                        GAMAdSlot="right_channel3_300x250" GAMPageMode="Auto" Disabled="true" />
                    <mn:AdUnit id="AdUnitGAMChannel5" runat="server" expandImageTokens="true" Size="Square"
                        GAMAdSlot="right_channel5_300x250" GAMPageMode="Auto" Disabled="true" />
                </div>
                <div class="column">
                    <mn:AdUnit id="AdUnitGAMChannel6" runat="server" expandImageTokens="true" Size="Square"
                        GAMAdSlot="right_channel6_300x250" GAMPageMode="Auto" Disabled="true" />
                    <mn:AdUnit id="AdUnitGAMChannel7" runat="server" expandImageTokens="true" Size="Square"
                        GAMAdSlot="right_channel7_300x250" GAMPageMode="Auto" Disabled="true" />
                </div>
            </div>
        </div>
    </asp:PlaceHolder>
    <%--Question Module--%>
    <asp:PlaceHolder ID="phQuestionAnswer" runat="server" Visible="false">
        <div id="qanda" class="qanda border-box clearfix">
            <h2 class="component-header">
                <mn:Txt ID="mntxt5646" runat="server" ResourceConstant="TXT_QANDA_HEADER" ExpandImageTokens="true" />
            </h2>
            <!--Question module-->
            <qa:MiniQuestionModule ID="MiniQuestionModule1" runat="server" />
            <!--Answer module-->
            <qa:MiniAnswerModule ID="MiniAnswerModule1" runat="server" TrackingParam="ClickFrom=Homepage%20%u2013%20Question%20of%20the%20Week" />
            <qa:SubmitQuestionClientModule ID="sendQuestionClientModule" runat="server" />
            
        </div>
    </asp:PlaceHolder>
    <!--hotlist sections-->
    <asp:PlaceHolder ID="phHotListStrips" runat="server">
        <uc1:HotlistProfileStrip id="profileStrip" runat="server" HotlistDirection="1" NoResultsResource="MEMBER_WHO_HAVE_MARKETING" />
        <uc1:HotlistProfileStrip id="profileStripYou" runat="server" HotlistDirection="2"
            NoResultsResource="MEMBER_YOU_HAVE_MARKETING" />
        <div class="edit-custom-lists">
            <span class="spr s-icon-folder-create-manage"><span></span></span>&nbsp;<mn:Txt ID="txtEditCat"
                runat="server" ResourceConstant="TXT_EDIT_CATEGORIES_ARROWS" Href="/Applications/HotList/ManageListCategory.aspx" />
        </div>
    </asp:PlaceHolder>
    <asp:PlaceHolder ID="phMatchesStrip" runat="server">
        <div id="hp-your-matches-strip" class="clear-both clearfix micro-profile-strip">
            <asp:PlaceHolder ID="phMatchesStripTitle" runat="server">
                <div class="pagination-buttons image-text-pair clearfix">
                    <h2 class="component-header">
                        <mn2:FrameworkLiteral ID="FrameworkLiteral5" runat="server" ResourceConstant="TXT_MOREMATCHES_TITLE"></mn2:FrameworkLiteral></h2>
                    <a href="/Applications/Search/SearchResults.aspx" class="button btn link-secondary small"><span class="next">
                        <mn2:FrameworkLiteral ID="FrameworkLiteral4" runat="server" ResourceConstant="TXT_VIEW_MORE"></mn2:FrameworkLiteral>
                    </span><span class="direction">
                        <mn:Txt ID="Txt2" runat="server" ResourceConstant="DIRECTION_ARROW" />
                    </span></a>
                </div>
            </asp:PlaceHolder>
            <uc1:MicroProfileStrip ID="listMatches2" runat="server" NoResultContentResource="NO_HERO_AND_MATCHES_MARKETING"
                TrackingParam="ClickFrom=Homepage%20%u2013%20Your%20Matches%20%u2013Mini%20Profile" />
            <uc1:MicroProfileStrip ID="listMatches3" runat="server" TrackingParam="ClickFrom=Homepage%20%u2013%20Your%20Matches%20%u2013Mini%20Profile"
                Visible="false" />
        </div>
    </asp:PlaceHolder>
    <asp:PlaceHolder ID="phQuickLink" runat="server">
        <!--Matchmail, Photos, and Mutual Matches Quick counts-->
        <div id="quick-link" class="border-box clearfix padding-heavy">
            <div class="quick-item">
                <h2 class="force-h1">
                    <mn2:FrameworkLiteral ID="literalMatchesByMailTitle" runat="server" ResourceConstant="TXT_MATCHES_BY_MAIL_TITLE"></mn2:FrameworkLiteral></h2>
                <asp:HyperLink runat="server" ID="lnkMatchesByMailImage1" NavigateUrl="/Applications/HotList/View.aspx?CategoryID=-10">
                    <mn:Image ID="imgMatchesByMail" runat="server" Border="0" ImageAlign="Left" CssClass="quick-link-icon"
                        TitleResourceConstant="TXT_MATCHES_BY_MAIL_TITLE" FileName="icon-homepage-email.gif" />
                </asp:HyperLink>
                <div class="quick-link-info">
                    <p>
                        <mn2:FrameworkLiteral ID="literalMatchesByMailText" runat="server" ResourceConstant="TXT_MATCHES_BY_MAIL"></mn2:FrameworkLiteral></p>
                    <mnl:Link runat="server" ID="lnkMatchesByMailText" NavigateUrl="/Applications/HotList/View.aspx?CategoryID=-10">
                <span class="matches-num-big"><asp:Literal ID="literalMatchesByMailCount" runat="server"></asp:Literal></span>
                &nbsp;<mn2:FrameworkLiteral ID="literalMatchesByMailMatches" runat="server" ResourceConstant="TXT_MATCHES"></mn2:FrameworkLiteral>
                    </mnl:Link>
                </div>
            </div>
            <asp:PlaceHolder runat="server" ID="plcYNM">
                <div class="quick-item">
                    <h2 class="component-header">
                        <mn2:FrameworkLiteral ID="literalMutualMatchesTitle" runat="server" ResourceConstant="TXT_MUTUAL_MATCHES_TITLE"></mn2:FrameworkLiteral></h2>
                    <div class="whats-this clearfix">
                        (<a href="/Applications/Article/ArticleView.aspx?CategoryID=1980&ArticleID=6296"><mn2:FrameworkLiteral
                            ID="FrameworkLiteral2" runat="server" ResourceConstant="TXT_WHATS_THIS"></mn2:FrameworkLiteral></a>)</div>
                    <asp:HyperLink runat="server" ID="lnkMutualMatchesImage1" NavigateUrl="/Applications/HotList/View.aspx?CategoryID=-15">
                        <mn:Image ID="imgMutualMatches" runat="server" Border="0" ImageAlign="Left" CssClass="quick-link-icon"
                            TitleResourceConstant="TXT_YOUR_CLICK" FileName="icon-homepage-yy.gif" />
                    </asp:HyperLink>
                    <div class="quick-link-info">
                        <p>
                            <mn2:FrameworkLiteral ID="literalMutualMatchesText" runat="server" ResourceConstant="TXT_MUTUAL_MATCHES"></mn2:FrameworkLiteral></p>
                        <mnl:Link runat="server" ID="lnkMututalMatchesText" TitleResourceConstant="TXT_YOUR_CLICK"
                            NavigateUrl="/Applications/HotList/View.aspx?CategoryID=-15">
                <span class="matches-num-big"><asp:Literal ID="literalMutualMatchesCount" runat="server"></asp:Literal></span>
                &nbsp;<mn2:FrameworkLiteral ID="literalMutualMatchesMatches" runat="server" ResourceConstant="TXT_MATCHES"></mn2:FrameworkLiteral>
                        </mnl:Link>
                    </div>
                </div>
            </asp:PlaceHolder>
            <div class="quick-item last-child">
                <h2 id="newphotos" class="force-h1">
                    <mn2:FrameworkLiteral ID="literalNewPhotosTitle" runat="server" ResourceConstant="TXT_NEW_PHOTOS_TITLE"></mn2:FrameworkLiteral></h2>
                <asp:HyperLink runat="server" ID="lnkNewPhotosImage1" NavigateUrl="/Applications/PhotoGallery/PhotoGallery.aspx">
                    <mn:Image ID="imgNewPhotos" runat="server" Border="0" ImageAlign="Left" CssClass="quick-link-icon"
                        TitleResourceConstant="TXT_NEW_PHOTOS_TITLE" FileName="icon-homepage-photos.gif" />
                </asp:HyperLink>
                <div class="quick-link-info">
                    <p>
                        <mn2:FrameworkLiteral ID="literalNewPhotosText" runat="server" ResourceConstant="TXT_NEW_PHOTOS_PREFIX"></mn2:FrameworkLiteral></p>
                    <mnl:Link runat="server" ID="lnkNewPhotosText" TitleResourceConstant="TXT_NEW_PHOTOS_TITLE"
                        NavigateUrl="/Applications/PhotoGallery/PhotoGallery.aspx">
                <span class="matches-num-big"><asp:Literal ID="literalNewPhotosCount" runat="server"></asp:Literal></span>
                &nbsp;<mn2:FrameworkLiteral ID="literalNewPhotosNewPhotos" runat="server" ResourceConstant="TXT_NEW_PHOTOS"></mn2:FrameworkLiteral>
                    </mnl:Link>
                </div>
            </div>
        </div>
    </asp:PlaceHolder>
    <!-- iframe used by SaveYNMVote function -->
    <iframe name="FrameYNMVote" tabindex="-1" frameborder="0" scrolling="no" width="1"
        height="1">
        <layer name="FrameYNMVote" frameborder="0" scrolling="no" width="1" height="1"></layer>
    </iframe>
</div>
<!-- end #page-container -->
