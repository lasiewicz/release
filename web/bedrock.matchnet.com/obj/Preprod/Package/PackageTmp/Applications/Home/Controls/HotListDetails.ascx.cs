#region System References
using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
#endregion

#region Matchnet Web App References
using Matchnet.Web.Framework;
#endregion

#region Matchnet Service References
using Matchnet.List.ServiceAdapters;
using Matchnet.List.ValueObjects;
using Matchnet.Web.Framework.Managers;
using Matchnet.Content.ServiceAdapters.Links;
#endregion

namespace Matchnet.Web.Applications.Home.Controls
{
	/// <summary>
	///		Summary description for HotListDetails.
	/// </summary>
	public class HotListDetails : FrameworkControl
	{
		protected Matchnet.Web.Framework.Image imgYY;
		protected Matchnet.Web.Framework.Image imgEmailedYou;
		protected Matchnet.Web.Framework.Image Image1;
		protected Matchnet.Web.Framework.Image imgTeasedYou;
		protected Matchnet.Web.Framework.Image imgIMYou;
		protected Matchnet.Web.Framework.Image imgViewedYou;
		protected Matchnet.Web.Framework.Image imgYouveEmailed;
		protected Matchnet.Web.Framework.Image imgListed;
		protected Matchnet.Web.Framework.Image imgTeased;
		protected Matchnet.Web.Framework.Image imgIMd;
		protected Matchnet.Web.Framework.Image imgViewed;
		protected HyperLink eachSaidYesLink;
		protected HyperLink emailedLink;
		protected HyperLink hotListedFavoriteLink;
		protected HyperLink teasedLink;
		protected HyperLink imdLink;
		protected HyperLink viewedLink;
		protected HyperLink emailedYouLink;
		protected HyperLink listedYouLink;
		protected HyperLink teasedYouLink;
		protected HyperLink imdYouLink;
		protected HyperLink viewedYouLink;

        protected HyperLink ECardYouLink;
        protected HyperLink YouECardLink;


		private const string linkFormat = "{0} <strong>{1}</strong>";

		private void Page_Init(object sender, System.EventArgs e) 
		{
			try
			{
				eachSaidYesLink.Text = g.GetResource("HOM_YOU_EACH_SAID_YES", this);
				emailedLink.Text = g.GetResource("HOT_EMAILED", this);
				hotListedFavoriteLink.Text = g.GetResource("HOT_HOT_LISTED", this);
				teasedLink.Text = g.GetResource("HOT_TEASED", this);
				imdLink.Text = g.GetResource("HOT_IMD", this);
				viewedLink.Text = g.GetResource("HOT_VIEWED", this);
				emailedYouLink.Text = g.GetResource("HOT_EMAILED_YOU", this);
				listedYouLink.Text = g.GetResource("HOT_HOT_LISTED_YOU", this);
				teasedYouLink.Text = g.GetResource("HOT_TEASED_YOU", this);
				imdYouLink.Text = g.GetResource("HOT_IMD_YOU", this);
				viewedYouLink.Text = g.GetResource("HOT_VIEWED_YOU", this);




				if(g.EcardsEnabled)
				{
					ECardYouLink.Text=g.GetResource("HOT_ECARD_YOU", this);
					YouECardLink.Text=g.GetResource("HOT_ECARD", this);

				}
				else
				{
					ECardYouLink.Visible=false;
					YouECardLink.Visible=false;
				}


			}
			catch(Exception ex) 
			{
				g.ProcessException(ex);
			}
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			try
			{
				BindHotListCounts();


			}
			catch (Exception ex)
			{
				g.ProcessException(ex);
			}
		}

		private void BindHotListCounts()
		{	
			eachSaidYesLink.Text = string.Format(linkFormat, eachSaidYesLink.Text,
				g.List.GetCount(HotListCategory.MutualYes,
				g.Brand.Site.Community.CommunityID,
				g.Brand.Site.SiteID).ToString());
			eachSaidYesLink.ToolTip = g.GetResource("TTL_CLICK_YOU_BOTH_SAID_YES_EXCLAMATION", this);

			emailedLink.Text = string.Format(linkFormat, emailedLink.Text,
				g.List.GetCount(HotListCategory.MembersYouEmailed,
				g.Brand.Site.Community.CommunityID,
				g.Brand.Site.SiteID).ToString());
			emailedLink.ToolTip = g.GetResource("TTL_MEMBERS_YOUVE_EMAILED", this);

			hotListedFavoriteLink.Text = string.Format(linkFormat, hotListedFavoriteLink.Text,
				g.List.GetCount(HotListCategory.Default,
				g.Brand.Site.Community.CommunityID,
				g.Brand.Site.SiteID).ToString());
			hotListedFavoriteLink.ToolTip = g.GetResource("TTL_MEMBERS_YOUVE_HOT_LISTED", this);

			teasedLink.Text = string.Format(linkFormat, teasedLink.Text,
				g.List.GetCount(HotListCategory.MembersYouTeased,
				g.Brand.Site.Community.CommunityID,
				g.Brand.Site.SiteID).ToString());
			teasedLink.ToolTip = g.GetResource("TTL_MEMBERS_YOUVE_TEASED", this);

			imdLink.Text = string.Format(linkFormat, imdLink.Text,
				g.List.GetCount(HotListCategory.MembersYouIMed,
				g.Brand.Site.Community.CommunityID,
				g.Brand.Site.SiteID).ToString());
			imdLink.ToolTip = g.GetResource("TTL_MEMBERS_YOUVE_IMD", this);

			viewedLink.Text = string.Format(linkFormat, viewedLink.Text,
				g.List.GetCount(HotListCategory.MembersYouViewed,
				g.Brand.Site.Community.CommunityID,
				g.Brand.Site.SiteID).ToString());
			viewedLink.ToolTip = g.GetResource("TTL_MEMBERS_YOUVE_VIEWED", this);

			emailedYouLink.Text = string.Format(linkFormat, emailedYouLink.Text,
				g.List.GetCount(HotListCategory.WhoEmailedYou,
				g.Brand.Site.Community.CommunityID,
				g.Brand.Site.SiteID).ToString());
			emailedYouLink.ToolTip = g.GetResource("TTL_MEMBERS_WHO_HAVE_EMAILED_YOU", this);

			listedYouLink.Text = string.Format(linkFormat, listedYouLink.Text,
				g.List.GetCount(HotListCategory.WhoAddedYouToTheirFavorites,
				g.Brand.Site.Community.CommunityID,
				g.Brand.Site.SiteID).ToString());
			listedYouLink.ToolTip = g.GetResource("TTL_MEMBERS_WHO_HAVE_HOT_LISTED_YOU", this);

			teasedYouLink.Text = string.Format(linkFormat, teasedYouLink.Text,
				g.List.GetCount(HotListCategory.WhoTeasedYou,
				g.Brand.Site.Community.CommunityID,
				g.Brand.Site.SiteID).ToString());
			teasedYouLink.ToolTip = g.GetResource("TTL_MEMBERS_WHO_HAVE_TEASED_YOU", this);

			imdYouLink.Text = string.Format(linkFormat, imdYouLink.Text,
				g.List.GetCount(HotListCategory.WhoIMedYou,
				g.Brand.Site.Community.CommunityID,
				g.Brand.Site.SiteID).ToString());
			imdYouLink.ToolTip = g.GetResource("TTL_MEMBERS_WHO_HAVE_IMD_YOU", this);

			viewedYouLink.Text = string.Format(linkFormat, viewedYouLink.Text,
				g.List.GetCount(HotListCategory.WhoViewedYourProfile,
				g.Brand.Site.Community.CommunityID,
				g.Brand.Site.SiteID).ToString());
			viewedYouLink.ToolTip = g.GetResource("TTL_MEMBERS_WHO_HAVE_VIEWED_YOU", this);

            //viewed your profile as subscriber feature
            ListManager listManager = new ListManager();
            if (!listManager.IsViewedYourProfileListAvailableToMember(g.Member, g.Brand))
            {
                viewedYouLink.NavigateUrl = FrameworkGlobals.GetSubscriptionLink(false, (int)PurchaseReasonType.HotlistViewedYourProfile);
            }

			ECardYouLink.Text = string.Format(linkFormat, ECardYouLink.Text,
				g.List.GetCount(HotListCategory.WhoSentYouECards,
				g.Brand.Site.Community.CommunityID,
				g.Brand.Site.SiteID).ToString());
			ECardYouLink.ToolTip = g.GetResource("TTL_MEMBERS_WHO_HAVE_ECARDED_YOU", this);

			YouECardLink.Text = string.Format(linkFormat, YouECardLink.Text,
				g.List.GetCount(HotListCategory.MembersYouSentECards,
				g.Brand.Site.Community.CommunityID,
				g.Brand.Site.SiteID).ToString());
			YouECardLink.ToolTip = g.GetResource("TTL_MEMBERS_YOUVE_ECARDED", this);

		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
			this.Init += new System.EventHandler(this.Page_Init);
		}
		#endregion
	}
}
