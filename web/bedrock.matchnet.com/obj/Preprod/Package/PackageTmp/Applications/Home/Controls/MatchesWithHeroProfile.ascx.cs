﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Web.Applications.API.JSONResult;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Ui;
using Image = Matchnet.Web.Framework.Image;
using Newtonsoft.Json;
using System.Text;
using Matchnet.Web.Analytics;

namespace Matchnet.Web.Applications.Home.Controls
{
    public partial class MatchesWithHeroProfile : FrameworkControl
    {
        protected int _MemberID = 0;
        protected int _PageSize = 9;
        protected string _OmnitureUsernameProfileURLTrackingParams = "";
        protected string _OmniturePhotoProfileURLTrackingParams = "";
        protected string _OmnitureHeroUsernameProfileURLTrackingParams = "";
        protected string _OmnitureHeroPhotoProfileURLTrackingParams = "";
        protected string _OmnitureHeroViewMoreProfileURLTrackingParams = "";
        protected bool _hasMatches = false;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (g.Member != null)
            {
                _MemberID = g.Member.MemberID;

                try
                {
                    //set up omniture tracking params for profile links
                    _OmnitureUsernameProfileURLTrackingParams = Omniture.GetActionURLParams(WebConstants.PageIDs.ViewProfile, WebConstants.Action.ProfileName, g.AnalyticsOmniture.PageName, "Filmstrip");
                    _OmniturePhotoProfileURLTrackingParams = Omniture.GetActionURLParams(WebConstants.PageIDs.ViewProfile, WebConstants.Action.ProfilePhoto, g.AnalyticsOmniture.PageName, "Filmstrip");
                    _OmnitureHeroUsernameProfileURLTrackingParams = Omniture.GetActionURLParams(WebConstants.PageIDs.ViewProfile, WebConstants.Action.ProfileName, g.AnalyticsOmniture.PageName, "Hero");
                    _OmnitureHeroPhotoProfileURLTrackingParams = Omniture.GetActionURLParams(WebConstants.PageIDs.ViewProfile, WebConstants.Action.ProfilePhoto, g.AnalyticsOmniture.PageName, "Hero");
                    _OmnitureHeroViewMoreProfileURLTrackingParams = Omniture.GetActionURLParams(WebConstants.PageIDs.ViewProfile, WebConstants.Action.ViewMoreLink, g.AnalyticsOmniture.PageName, "Hero");

                    //get slider profiles
                    bool hasResults = false;
                    GetSearchResult getSearchResult = HomeUtil.GetSliderProfilesJSON(g.Member, g.Brand, g, g.SearchPreferences, g.Session, 1, _PageSize, true, true, HomeUtil.MATCH_SLIDER_USERNAME_MAX, this);

                    //render js profile objects
                    StringBuilder preloadJS = new StringBuilder();
                    if (getSearchResult != null && getSearchResult.SearchProfileList != null)
                    {
                        if (getSearchResult.SearchProfileList.Count > 0)
                        {
                            hasResults = true;
                            if (getSearchResult.IsMatchResults)
                            {
                                _hasMatches = true;
                            }
                        }
                        string getSearchResultJSON = JsonConvert.SerializeObject(getSearchResult);
                        preloadJS.AppendLine("var getSearchResult = " + getSearchResultJSON + ";");
                        preloadJS.AppendLine("matchSliderManager.loadPage(getSearchResult);");
                    }
                    litPreloadJS.Text = preloadJS.ToString();

                    //update title
                    lnkViewMore.Title = g.GetResource("TXT_VIEW_MORE", this);
                    if (hasResults && getSearchResult.IsMOLResults)
                    {
                        txtComponentTitle.ResourceConstant = "TXT_COMPONENT_HEADER_MOL";
                        lnkViewMore.HRef = "/Applications/MembersOnline/MembersOnline.aspx";
                        lnkViewMore.HRef = g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.MembersOnline, WebConstants.Action.ViewMoreLink, lnkViewMore.HRef, "Filmstrip");
                    }
                    else
                    {
                        lnkViewMore.HRef = g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.SearchResults, WebConstants.Action.ViewMoreLink, lnkViewMore.HRef, "Filmstrip");
                    }

                }
                catch (Exception ex)
                {
                    g.ProcessException(ex);
                }
            }
        }
    }
}
