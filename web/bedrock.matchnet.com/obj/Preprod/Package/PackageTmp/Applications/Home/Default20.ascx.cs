﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Member.ValueObjects.Interfaces;
using Matchnet.Member.ValueObjects.MemberDTO;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Managers;
using Matchnet.Web.Framework.Ui;
using Matchnet.Web.Framework.Ui.BasicElements;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Search.ServiceAdapters;
using Matchnet.Search.ValueObjects;
using Matchnet.MembersOnline.ValueObjects;
using Matchnet.MembersOnline.ServiceAdapters;
using System.Collections;
using Matchnet.List.ValueObjects;
using Matchnet.PhotoSearch.ServiceAdapters;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Configuration.ServiceAdapters.Analitics;
using Matchnet.Web.Applications.QuestionAnswer;

namespace Matchnet.Web.Applications.Home
{
    public partial class Default20 : FrameworkControl
    {
        #region Fields
        private int _memberID = 0;
        private const string DOMAINID = "DomainID";
        private int _highlightedProfileCount = 0;
        private bool _hasSpotlightProfile = false;
        int _iterations = 0;
        int _matchCount = 3;
        int _hotlistCount = 3;
        int _matchOrdinalTracker = 1;
        ArrayList _membersSearch;
        Scenario _scenario = Scenario.C;
        private bool _hasMatches = false;
        #endregion

        #region Event Handlers
        protected override void OnInit(EventArgs e)
        {

            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //load homepage; ensure member is logged in (not available for visitors)
                if (g.Member != null && g.Member.MemberID > 0)
                {
                    try
                    {
                        bool enableMemberSlideshow = Conversion.CBool(RuntimeSettings.GetSetting("ENABLE_SLIDESHOW", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID));
                        bool enableMiniSlideshow = Conversion.CBool(RuntimeSettings.GetSetting("ENABLE_CONFIGURABLE_SLIDESHOW", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID));
                        enableMemberSlideshow = enableMemberSlideshow && !IsInvalidRequest();

                        _memberID = g.Member.MemberID;
                        _scenario = g.GetABScenario("HOMEPAGE");
                        litMainClass.Text = "homepage-" + _scenario;
                        //load profiles for dislay



                        if (enableMiniSlideshow)
                        {
                            g.RightNavControl.ShowMiniMemberSlideShow();
                        }


                        if (enableMemberSlideshow)
                        {
                            phHotLists.Visible = true;
                            phHeroMoreMatchesTitle.Visible = false;
                            phHotListStrips.Visible = false;
                            phMatchesStrip.Visible = true;
                            listMatches2.ResourceControl = this;
                            LoadSlideshow();
                            litMainClass.Text = "homepage-memslideshow";
                            _matchCount = Conversion.CInt(g.GetResource("MATCH_COUNT", this));
                            listMatches3.Visible = true;
                            this.LoadSlideshowMatches();
                            this.LoadQuickLinks();
                        }
                        else
                        {
                            // this.LoadHotlists();
                            if (_scenario == Scenario.B)
                            {
                                phHotLists.Visible = true;
                                ucHotlist1.Visible = false;
                                LoadShortcutList();

                                phHeroMoreMatchesTitle.Visible = false;
                                phHotListStrips.Visible = false;
                                phMatchesStrip.Visible = false;
                                phQuickLink.Visible = false;
                                listMatches2.ResourceControl = this;
                                if (HotList.HotListViewHandler.IsNotificationsEnabled(g))
                                {
                                    listMatches3.Visible = true;
                                    _matchCount = Conversion.CInt(g.GetResource("MATCH_COUNT", this));
                                }

                                //load profile film strip
                                LoadProfileFilmStrip();

                                //load gam channel slots on homepage instead of Right control
                                if (HomeUtil.IsHomepageChannelAdsOverrideEnabled(g.Brand))
                                {
                                    phGAMChannels.Visible = true;
                                    AdUnitGAMChannel3.Disabled = false;
                                    AdUnitGAMChannel5.Disabled = false;
                                    AdUnitGAMChannel6.Disabled = false;
                                    AdUnitGAMChannel7.Disabled = false;
                                }
                            }
                            else
                            {
                                phHotLists.Visible = false;
                                phHeroMoreMatchesTitle.Visible = true;
                                phHotListStrips.Visible = true;
                                phMatchesStrip.Visible = false;
                                this.LoadQuickLinks();
                            }
                            this.LoadHeroMatches();

                        }



                        //save results url used in view profile's "back to results" feature
                        FrameworkGlobals.SaveBackToResultsURL(g);

                        //question and answers
                        this.LoadQuestionAnswer();

                        // if any notification messages that came through session, display it
                        HandleNotificationMessage();

                    }
                    catch (Exception ex)
                    {
                        g.ProcessException(ex);
                    }

                }
                else
                {
                    //member is not logged in, send to the splash page
                    g.Transfer("/Default.aspx");

                }

                plcYNM.Visible = g.IsYNMEnabled;
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }


        protected override void OnPreRender(EventArgs e)
        {
            try
            {
                // Omniture Analytics - Event and property variable tracking
                if (Convert.ToBoolean(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ANALYTICS_OMNITURE", _g.Brand.Site.Community.CommunityID, _g.Brand.Site.SiteID)))
                {
                    string scenName = (_scenario == Scenario.A) ? "E" : "F";
                    // Track total profiles that are highlighted in the search result
                    _g.AnalyticsOmniture.AddEvent("event5");
                    _g.AnalyticsOmniture.AddProductEvent("event5=" + Convert.ToString(this._highlightedProfileCount));


                    if (_hasSpotlightProfile)
                    {
                        _g.AnalyticsOmniture.AddEvent("event13");

                        _g.AnalyticsOmniture.AddProductEvent("event13=1");
                    }

                    // MPR-940 JewishSearch
                    if ((_g.Brand.Site.Community.CommunityID == (int)WebConstants.COMMUNITY_ID.JDate))
                    {

                        int optionValue = _g.Member.GetAttributeInt(g.Brand, "JdateReligion", Constants.NULL_INT);

                        // Selected Any
                        if (optionValue == Constants.NULL_INT)
                        {
                            _g.AnalyticsOmniture.Evar41 = "Standard";
                        }
                        else
                        {
                            if (((optionValue & 16384) == 16384) || //Willing to Convert
                            ((optionValue & 2048) == 2048) || //Not Willing to Convert
                            ((optionValue & 512) == 512)) //Not sure if I’m willing to convert
                            {
                                _g.AnalyticsOmniture.Evar41 = "Standard";
                            }
                            else
                            {
                                _g.AnalyticsOmniture.Evar41 = "Jewish Only";
                            }
                        }
                    }

                    _g.AnalyticsOmniture.Evar43 = "Home Page - Scenario " + scenName.ToUpper();
                    _g.AnalyticsOmniture.Prop32 = (ucHotlist2.HasHotListActivity) ? "Has Hot List Activity" : "Has No Activity";
                    _g.AnalyticsOmniture.Prop33 = (this._hasMatches) ? "Has Matches" : "Has No Matches";

                    //Add omniture tracking for profile in Activity Center
                    _g.RightNavControl.ActivityCenter.TrackingParam = "ClickFrom=" + Server.UrlEncode("Homepage - Activity Center");

                }

                base.OnPreRender(e);
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        protected void repeaterYourMatches_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                ProfileHolder profile = e.Item.DataItem as ProfileHolder;

                MicroProfile20 microProfile = e.Item.FindControl("microProfileMatches") as MicroProfile20;
                microProfile.member = profile.Member;
                microProfile.HotListCategoryID = (int)profile.HotlistCategory;
                microProfile.Ordinal = profile.Ordinal;
                microProfile.MyEntryPoint = profile.MyEntryPoint;
                microProfile.LoadMemberProfile();

                if (microProfile.IsHighlighted)
                {
                    PlaceHolder phHighlight = e.Item.FindControl("phHighlight") as PlaceHolder;
                    phHighlight.Visible = true;
                }
                else
                {
                    PlaceHolder phSpotlight = e.Item.FindControl("phRegular") as PlaceHolder;
                    phSpotlight.Visible = true;
                }
            }
        }

        #endregion

        #region Methods
        private void HandleNotificationMessage()
        {
            if (g.Session.Get(WebConstants.SESSION_NOTIFICATION_MESSAGE) != null)
            {
                g.Notification.AddMessage(g.Session.Get(WebConstants.SESSION_NOTIFICATION_MESSAGE).ToString());
                g.Session.Remove(WebConstants.SESSION_NOTIFICATION_MESSAGE);
            }
        }

        private void LoadSlideshow()
        {


            phMemberSlideshow.Visible = true;
            ucSlideshowProfile.Load(true);
        }

        private void LoadSlideshowMatches()
        {
            ProfileHolder heroMember = new ProfileHolder();

            //get matches profiles to display (need to get this first, because it is used along with Spotlight to determine hero profile)
            _membersSearch = GetYourMatches(g.SearchPreferences);
            ArrayList matchesList = new ArrayList();

            //get matchOrdinalTracker, used to rotate "your matches" on each view
            if (!String.IsNullOrEmpty(g.Session[WebConstants.SESSION_PROPERTY_NAME_MATCHLIST_ORDINAL_TRACKER]))
            {
                _matchOrdinalTracker = Convert.ToInt32(g.Session[WebConstants.SESSION_PROPERTY_NAME_MATCHLIST_ORDINAL_TRACKER]);
            }

            //get initial matches to display
            DetermineMatchesToDisplay(matchesList);

            List<ProfileHolder> matches = new List<ProfileHolder>();
            if (matchesList != null)
            {
                foreach (ProfileHolder p in matchesList)
                {
                    matches.Add(p);
                }
            }

            listMatches2.MemberList = matches;
            if (HotList.HotListViewHandler.IsNotificationsEnabled(g))
            {

                if (matches.Count > 3)
                {
                    listMatches3.MemberList = matches.GetRange(3, matches.Count - 3);
                }
                else
                {
                    listMatches3.Visible = false;
                }
            }
            //save match ordinal tracker to rotate
            g.Session.Add(WebConstants.SESSION_PROPERTY_NAME_MATCHLIST_ORDINAL_TRACKER, _matchOrdinalTracker, Matchnet.Session.ValueObjects.SessionPropertyLifetime.Temporary);
        }

        private void LoadHeroMatches()
        {
            ProfileHolder heroMember = new ProfileHolder();

            //get matches profiles to display (need to get this first, because it is used along with Spotlight to determine hero profile)
            _membersSearch = GetYourMatches(g.SearchPreferences);
            ArrayList matchesList = new ArrayList();

            //get matchOrdinalTracker, used to rotate "your matches" on each view
            if (!String.IsNullOrEmpty(g.Session[WebConstants.SESSION_PROPERTY_NAME_MATCHLIST_ORDINAL_TRACKER]))
            {
                _matchOrdinalTracker = Convert.ToInt32(g.Session[WebConstants.SESSION_PROPERTY_NAME_MATCHLIST_ORDINAL_TRACKER]);
            }

            //get initial matches to display
            DetermineMatchesToDisplay(matchesList);

            //MPR-995 TL - Disabling spotlight for hero profile, for now we will always use profile from Your Matches
            #region Get Spotlight profile
            //get spotlight profile for hero display
            //heroMember.IsSpotlight = true;
            //heroMember.Ordinal = 1;
            //heroMember.MyEntryPoint = BreadCrumbHelper.EntryPoint.HomePage;
            //bool promoMemberFlag = false;
            //System.Collections.Generic.List<int> searchIDs = new System.Collections.Generic.List<int>();
            //if (matchesList != null)
            //{
            //    for (int i = 0; i < matchesList.Count; i++)
            //    {
            //        searchIDs.Add(((ProfileHolder)matchesList[i]).Member.MemberID);
            //    }
            //}
            //heroMember.Member = FrameworkGlobals.GetSpotlightProfile(g, searchIDs, out promoMemberFlag);
            #endregion

            //get more matches profile for hero display, if necessary
            if (heroMember.Member == null)
            {
                if (matchesList.Count > 0)
                {
                    //use match profile for hero display
                    heroMember = matchesList[0] as ProfileHolder;

                    //get "your matches" to display
                    if (_membersSearch.Count > _matchCount + 1)
                    {
                        matchesList.RemoveAt(0);
                        DetermineMatchesToDisplay(matchesList);
                    }
                    else
                    {
                        matchesList = null;
                    }
                }
            }
            List<ProfileHolder> matches = new List<ProfileHolder>();
            if (matchesList != null)
            {
                foreach (ProfileHolder p in matchesList)
                {
                    matches.Add(p);
                }
            }

            //Load hero profile and matches
            if (heroMember != null && heroMember.Member != null)
            {
                this.phHeroAndMatchesProfile.Visible = true;

                #region Load Hero Profile

                if (heroMember.IsSpotlight)
                {
                    this.phHeroSpotlightTitle.Visible = true;
                    _hasSpotlightProfile = true;
                    this.ucSpotlightProfileInfoDisplay.Visible = true;
                    this.ucSpotlightProfileInfoDisplay.ViewedMemberID = _memberID;
                    this.ucSpotlightProfileInfoDisplay.Spotlight = true;
                    this.ucSpotlightProfileInfoDisplay.ShowImageNoText = true;
                }
                else
                {
                    this.phHeroMoreMatchesTitle.Visible = true;
                    this.ucSpotlightProfileInfoDisplay.Visible = false;
                }

                this.heroProfile.member = heroMember.Member;
                this.heroProfile.IsSpotlighted = heroMember.IsSpotlight;
                this.heroProfile.Ordinal = heroMember.Ordinal;
                this.heroProfile.MyEntryPoint = heroMember.MyEntryPoint;

                ListItemDetail viewedYouDetail;
                viewedYouDetail = g.List.GetListItemDetail(HotListCategory.WhoViewedYourProfile,
                                    g.Brand.Site.Community.CommunityID,
                                    g.Brand.Site.SiteID,
                                    heroMember.Member.MemberID);

                if (viewedYouDetail != null)
                    this.heroProfile.LastViewedDate = viewedYouDetail.ActionDate;

                this.heroProfile.LoadMemberProfile();

                #endregion

                //bind your matches profiles
                if (_scenario == Scenario.A)
                {
                    phHeroMatches.Visible = true;
                    if (matchesList != null && matchesList.Count > 0 && matchesList.Count <= _matchCount)
                    {
                        listMatches.MemberList = matches;
                    }
                    else
                    {
                        //no your matches
                        listMatches.MemberList = matches;
                    }
                }
                else
                {
                    phHeroMatches.Visible = false;
                    phHeroMoreMatchesTitle.Visible = false;
                    if (!phFilmstrip.Visible)
                    {
                        listMatches2.MemberList = matches;
                    }
                }
            }
            else
            {
                //no hero profile or your matches
                if (_scenario == Scenario.A)
                    this.phNoHeroAndMatchesMarketingCopy.Visible = true;
                phMatchesStripTitle.Visible = false;
                phHeroMoreMatchesTitle.Visible = false;

            }

            if (_scenario == Scenario.B && HotList.HotListViewHandler.IsNotificationsEnabled(g) && !phFilmstrip.Visible)
            {
                if (matches.Count > 3)
                {
                    listMatches3.MemberList = matches.GetRange(2, 3);
                }
                else
                {
                    listMatches3.Visible = false;
                }
            }
            //save match ordinal tracker to rotate
            g.Session.Add(WebConstants.SESSION_PROPERTY_NAME_MATCHLIST_ORDINAL_TRACKER, _matchOrdinalTracker, Matchnet.Session.ValueObjects.SessionPropertyLifetime.Temporary);

        }



        private void DetermineHotlistProfiles(List<ProfileHolder> profileList, HotListCategory cat)
        {
            int count;
            ArrayList memberIDs = g.List.GetListMembers(cat,
                                g.Brand.Site.Community.CommunityID,
                                g.Brand.Site.SiteID,
                                1,
                                _hotlistCount,
                                out count);

            ArrayList MemberList = MemberDTOManager.Instance.GetIMemberDTOs(g, memberIDs, MemberType.Search,
                                                                            MemberLoadFlags.None);
            for (int i = 0; i < MemberList.Count; i++)
            {
                ProfileHolder profileMWH = new ProfileHolder();
                profileMWH.MyEntryPoint = BreadCrumbHelper.EntryPoint.HotLists;
                profileMWH.Ordinal = i + 1; //hotlist ordinals start at 1
                profileMWH.HotlistCategory = cat;
                profileMWH.Member = MemberList[i] as Member.ServiceAdapters.Member;

                profileList.Add(profileMWH);
            }
        }

        private void LoadHotlistProfile(MicroProfile20 microProfile, ProfileHolder profileHolder)
        {

            ListItemDetail viewedYouMemberListDetail = g.List.GetListItemDetail(profileHolder.HotlistCategory,
                                    g.Brand.Site.Community.CommunityID,
                                    g.Brand.Site.SiteID,
                                    profileHolder.Member.MemberID);

            microProfile.member = profileHolder.Member;
            microProfile.HotListCategoryID = (int)profileHolder.HotlistCategory;
            microProfile.MyEntryPoint = profileHolder.MyEntryPoint;
            microProfile.Ordinal = profileHolder.Ordinal;
            if (viewedYouMemberListDetail != null)
                microProfile.LastHotlistActionDate = viewedYouMemberListDetail.ActionDate;
            microProfile.LoadMemberProfile();

        }

        private ArrayList GetYourMatches(SearchPreferenceCollection prefs)
        {
            // declare local varialbes for resuse and clarity
            const string HASPHOTOFLAG = "HasPhotoFlag";
            int startRow = 0;
            int pageSize = 50;
            MatchnetQueryResults searchResults = null;

            //get current photo preference before we modify it
            string photoPreference = prefs.Value(HASPHOTOFLAG);
            prefs.Add(HASPHOTOFLAG, "1");

            //Ensure that domainID is set on search preferences for the correct Community.
            prefs.Add(DOMAINID, g.Brand.Site.Community.CommunityID.ToString());
            prefs.Add("ColorCode", "");

            //pass flag to determine if search redesign is enabled
            if (BetaHelper.IsSearchRedesign30Enabled(g))
            {
                prefs.Add("SearchRedesign30", "1");
            }
            else
            {
                prefs.Add("SearchRedesign30", "0");
            }

            try
            {
                // Potential for exception due to invalid search preferences on this member's stored set of preferences.
                searchResults = MemberSearchSA.Instance.Search(
                    prefs,
                    g.Brand.Site.Community.CommunityID,
                    g.Brand.Site.SiteID,
                    startRow,
                    pageSize);
            }
            catch (Exception)
            {
                //invalid search preferences, initialize an empty results list so the home page will render correctly.
                searchResults = new MatchnetQueryResults(0);
            }

            //set photo preference back to old value
            prefs.Add(HASPHOTOFLAG, photoPreference);

            ArrayList members = new ArrayList();
            if (searchResults != null && searchResults.ToArrayList().Count > 0)
            {
                members = MemberDTOManager.Instance.GetIMemberDTOsFromResults(searchResults, g, MemberType.Search,
                                                                              MemberLoadFlags.None);
            }

            if (members.Count > 0)
            {
                _hasMatches = true;
            }
            return members;
        }



        private void DetermineMatchesToDisplay(ArrayList matchesList)
        {
            int restartCount = 0;

            //display 3 match profile, rotating each page view; session based
            while ((matchesList.Count < _matchCount) && _membersSearch.Count > 0)
            {
                if (_membersSearch.Count >= _matchOrdinalTracker)
                {
                    //add member
                    ProfileHolder ph = new ProfileHolder();
                    ph.Member = _membersSearch[_matchOrdinalTracker - 1] as IMemberDTO;
                    ph.Ordinal = _matchOrdinalTracker;
                    ph.HotlistCategory = HotListCategory.YourMatches;
                    ph.MyEntryPoint = BreadCrumbHelper.EntryPoint.HomePage;

                    matchesList.Add(ph);
                    _matchOrdinalTracker++;
                }
                else
                {
                    //start from beginning of list
                    _matchOrdinalTracker = 1;
                    restartCount++;
                }

                if (restartCount > 1)
                    break;

            }
        }


        private void LoadQuestionAnswer()
        {
            if (QuestionAnswerHelper.IsQuestionAnswerEnabled(g.Brand))
            {
                Matchnet.PremiumServiceSearch.ValueObjects.QuestionAnswer.Question question;
                question = QuestionAnswer.QuestionAnswerHelper.GetActiveQuestion(g.Member, g, true);
                if (question != null)
                {
                    phQuestionAnswer.Visible = true;
                    MiniQuestionModule1.LoadQuestion(question);
                    MiniAnswerModule1.LoadAnswers(question);
                }
                else
                {
                    phQuestionAnswer.Visible = false;
                }
            }
            else
            {
                phQuestionAnswer.Visible = false;
            }

        }

        private void LoadProfileFilmStrip()
        {
            phFilmstrip.Visible = true;
            ProfileFilmStripYourMatches.LoadYourMatches(BreadCrumbHelper.EntryPoint.HomePage);
            _hasMatches = ProfileFilmStripYourMatches.HasMatches;
        }

        private void LoadQuickLinks()
        {
            phQuickLink.Visible = true;

            //matches by mail count
            this.literalMatchesByMailCount.Text = g.List.GetCount(HotListCategory.YourMatches, g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID).ToString();

            //new photos count
            int photoCount = PhotoGallerySA.Instance.GetCommunityPhotoCount(g.Brand.Site.Community.CommunityID, 24);
            this.literalNewPhotosCount.Text = (photoCount >= 0) ? photoCount.ToString() : "0";

            //mutual matches count
            this.literalMutualMatchesCount.Text = g.List.GetCount(HotListCategory.MutualYes, g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID).ToString();

        }

        private void LoadShortcutList()
        {
            phShortcutList.Visible = true;

            //new matches by mail
            this.literalShortcutNewMatchesCount.Text = g.List.GetCount(HotListCategory.YourMatches, g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID).ToString();

            //new photos count
            int photoCount = PhotoGallerySA.Instance.GetCommunityPhotoCount(g.Brand.Site.Community.CommunityID, 24);
            this.literalShortcutNewPhotosCount.Text = (photoCount >= 0) ? photoCount.ToString() : "0";

            //new mail indicator
            if (g.hasNewMessage)
            {
                multiviewShortcutMailIcon.SetActiveView(viewShortcutMailFlashingIcon);
            }

        }

        private List<int> GetWhoIsCheckingYouOutCategoryList()
        {
            List<int> categories = new List<int>();

            categories.Add(-1);
            categories.Add(-4);
            categories.Add(-2);
            //categories.Add(-3);
            //categories.Add(-25);
            categories.Add(-5);
            categories.Add(-15);

            return categories;
        }

        private bool IsInvalidRequest()
        {
            //this is a HUGE HACK. There's a problem with some gam ads calling to images and stylesheets that
            //no longer exist, and the resulting 404 causes default.aspx to be reloaded and the slideshow to advance. 
            //This code prevents that, and can be removed once the gam issue is resolved. 
            bool invalid = false;
            string controlname = g.AppPage.ControlName;
            string uri = HttpContext.Current.Request.RawUrl;
            string referrer = HttpContext.Current.Request.UrlReferrer != null ? HttpContext.Current.Request.UrlReferrer.AbsoluteUri : string.Empty;


            if (controlname.ToLower() == "default" &&
                (uri.ToLower().IndexOf("/?") >= 0 ||
                uri.ToLower().IndexOf("default.aspx?siteid=") >= 0 ||
                uri.ToLower().IndexOf("default.aspx?v=") >= 0) ||
                referrer.ToLower().IndexOf("red01.css") >= 0)
            {
                Trace.Write("is: true");
                invalid = true;
            }

            return invalid;
        }

        #endregion
    }
}