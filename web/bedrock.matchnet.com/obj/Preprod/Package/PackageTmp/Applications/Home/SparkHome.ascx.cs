﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Member.ValueObjects.Interfaces;
using Matchnet.Member.ValueObjects.MemberDTO;
using Matchnet.Web.Applications.Email;
using Matchnet.Web.Framework;
using Matchnet.Web.Applications.QuestionAnswer;
using System.Collections;
using Matchnet.Search.ValueObjects;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Search.ServiceAdapters;
using Matchnet.List.ValueObjects;
using Matchnet.Web.Framework.Ui;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.MembersOnline.ServiceAdapters;
using Matchnet.MembersOnline.ValueObjects;
using Matchnet.Web.Framework.Managers;

using Matchnet.PhotoSearch.ServiceAdapters;

using Matchnet.PremiumServiceSearch.ValueObjects.QuestionAnswer;
using Matchnet.Web.Framework.Ui.PageAnnouncements;

namespace Matchnet.Web.Applications.Home
{
    /// <summary>
    /// Control represents new redesign home page for Mingle.
    /// 
    /// Note: There is expectation that Mingle homepages will be radically different from other BH sites, and possibly even
    /// within Mingle so we'll start with this.
    /// </summary>
    public partial class SparkHome : FrameworkControl
    {
        #region Fields
        private int _memberID = 0;
        private const string DOMAINID = "DomainID";
        private int _highlightedProfileCount = 0;
        private bool _hasMatches = false;
        private BreadCrumbHelper.EntryPoint _heroProfileEntry = BreadCrumbHelper.EntryPoint.HomeHeroProfileMatches;
        #endregion

        #region Event Handlers

        protected override void OnInit(EventArgs e)
        {
           base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //load homepage; ensure member is logged in (not available for visitors)
                if (g.Member != null && g.Member.MemberID > 0)
                {
                    _memberID = g.Member.MemberID;

                    //load profile film strip
                    LoadProfileFilmStrip();

                    //load profile activity
                    //LoadProfileActivity();

                    //question and answers
                    LoadQuestionAnswer();

                    LoadShortcutList();


                    bool showSubNonSubRightRail = Convert.ToBoolean(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("SHOW_SUB_NON_SUB_RIGHT_RAIL", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID));
                    if (g.Member != null && showSubNonSubRightRail)
                    {
                        bool enableMiniSlideshow = Conversion.CBool(RuntimeSettings.GetSetting("ENABLE_CONFIGURABLE_SLIDESHOW", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID)); ;

                        if (enableMiniSlideshow)
                        {
                            g.RightNavControl.ShowMiniMemberSlideShow();
                        }
                    }

                    //save results url used in view profile's "back to results" feature
                    FrameworkGlobals.SaveBackToResultsURL(g);

                    // if any notification messages that came through session, display it
                    HandleNotificationMessage();

                    //push flirts
                    LoadPushFlirtsPopup();

                    if (g.BreadCrumbTrailHeader != null && g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.JDateCoIL && g.AppPage.ID == (int)WebConstants.PageIDs.Default)
                    {
                        g.BreadCrumbTrailHeader.Visible = false;
                    }

                    if (SubscriptionExpirationManager.Instance.DisplaySubscriptionExpirationAlert(g.Brand, g.Member, g))
                    {
                        SubscriptionExpirationAlert subscriptionExpirationAlert = Page.LoadControl("/Framework/Ui/PageAnnouncements/SubscriptionExpirationAlert.ascx") as SubscriptionExpirationAlert;
                        g.LayoutTemplateBase.AddPageAccouncementControl(subscriptionExpirationAlert);

                    }
                }
                else
                {

                    if (SettingsManager.GetSettingBool("VISITORS_GET_LOGON_PAGE_WHEN_REQUESTING_HOMEPAGE", g.Brand))
                    {
                        g.LogonRedirect("/Applications/Home/Default.aspx");
                    }
                    else
                    {
                        //member is not logged in, send to the splash page
                        g.Transfer("/Default.aspx");
                    }
                }
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            try
            {
                if (ucHotlist2.TotalListsCount == 0 && ucHotlist2.HideIfAllListsEmpty)
                {
                    phHotLists.Visible = false;
                    phEmptyListDisplay.Visible = true;
                }

                // Omniture Analytics - Event and property variable tracking
                if (Convert.ToBoolean(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ANALYTICS_OMNITURE", _g.Brand.Site.Community.CommunityID, _g.Brand.Site.SiteID)))
                {
                    // Track total profiles that are highlighted in the search result
                    _g.AnalyticsOmniture.AddEvent("event5");
                    _g.AnalyticsOmniture.AddProductEvent("event5=" + Convert.ToString(this._highlightedProfileCount));

                    // MPR-940 JewishSearch
                    if ((_g.Brand.Site.Community.CommunityID == (int)WebConstants.COMMUNITY_ID.JDate))
                    {
                        int optionValue = _g.Member.GetAttributeInt(g.Brand, "JdateReligion", Constants.NULL_INT);

                        // Selected Any
                        if (optionValue == Constants.NULL_INT)
                        {
                            _g.AnalyticsOmniture.Evar41 = "Standard";
                        }
                        else
                        {
                            if (((optionValue & 16384) == 16384) || //Willing to Convert
                            ((optionValue & 2048) == 2048) || //Not Willing to Convert
                            ((optionValue & 512) == 512)) //Not sure if I’m willing to convert
                            {
                                _g.AnalyticsOmniture.Evar41 = "Standard";
                            }
                            else
                            {
                                _g.AnalyticsOmniture.Evar41 = "Jewish Only";
                            }
                        }
                    }

                    // _g.AnalyticsOmniture.Prop32 = (this.ProfileActivity1.HasHotListActivity) ? "Has Hot List Activity" : "Has No Activity";
                    _g.AnalyticsOmniture.Prop33 = (this._hasMatches) ? "Has Matches" : "Has No Matches";

                    //Add omniture tracking for profile in Activity Center
                    _g.RightNavControl.ActivityCenter.TrackingParam = "ClickFrom=" + Server.UrlEncode("Homepage - Activity Center");
                }

                base.OnPreRender(e);
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        #endregion


        #region Private Methods
        private void HandleNotificationMessage()
        {
            if (g.Session.Get(WebConstants.SESSION_NOTIFICATION_MESSAGE) != null)
            {
                g.Notification.AddMessage(g.Session.Get(WebConstants.SESSION_NOTIFICATION_MESSAGE).ToString());
                g.Session.Remove(WebConstants.SESSION_NOTIFICATION_MESSAGE);
            }
        }

        private void LoadPushFlirtsPopup()
        {
            FlirtManager flirtManager = new FlirtManager(g);
            if (flirtManager.ShowPushFlirtsToMember(PushFlirtViewMode.ForcedOverlay))
            {
                pushFlirts.Visible = true;
                plcPopupPushFlirtsDialog.Visible = true;
                g.LayoutTemplateBase.DisableGamOverlay();
                flirtManager.IncrementPushFlirtsShownCount();
            }
            else if (flirtManager.ShowPushFlirtsToMember(PushFlirtViewMode.Link))
            {
                pushFlirts.Visible = true;
                plcPopupPushFlirtsDialog.Visible = false;
            }
            else
            {
                pushFlirts.Visible = false;
                plcPopupPushFlirtsDialog.Visible = false;
            }
        }

        private void LoadQuestionAnswer()
        {
            if (QuestionAnswerHelper.IsQuestionAnswerEnabled(g.Brand))
            {
                Matchnet.PremiumServiceSearch.ValueObjects.QuestionAnswer.Question question;
                question = QuestionAnswer.QuestionAnswerHelper.GetActiveQuestion(g.Member, g, true);
                if (question != null)
                {
                    phQuestionAnswer.Visible = true;
                    MiniQuestionModule1.LoadQuestion(question);

                    Answer[] approvedAnswers = QuestionAnswerHelper.GetApprovedAnswers(question, g.Member.MemberID, g);
                    if (approvedAnswers.Length > 0)
                    {
                        MiniAnswerModule1.LoadAnswers(question);
                        phNoAnswers.Visible = false;
                    }
                    else if (!MiniQuestionModule1.MemberAnswerFlag)
                    {
                        phNoAnswers.Visible = true;
                    }
                }
                else
                {
                    phQuestionAnswer.Visible = false;
                }
            }
            else
            {
                phQuestionAnswer.Visible = false;
            }
        }


        private void LoadProfileFilmStrip()
        {
            try
            {
                MOCollection mocollection = null;
                HeroProfileFilmStripMatches.MemberList = GetHeroFilmStripProfiles(g.SearchPreferences, 17, out mocollection);
                if (HeroProfileFilmStripMatches.MemberList != null && HeroProfileFilmStripMatches.MemberList.Count > 0)
                {
                    HeroProfileFilmStripMatches.member = HeroProfileFilmStripMatches.MemberList[0].Member;
                    HeroProfileFilmStripMatches.Ordinal = HeroProfileFilmStripMatches.MemberList[0].Ordinal;
                    HeroProfileFilmStripMatches.MyEntryPoint = _heroProfileEntry;
                    HeroProfileFilmStripMatches.HotListCategoryID = (int)HeroProfileFilmStripMatches.MemberList[0].HotlistCategory;
                    HeroProfileFilmStripMatches.MyMOCollection = mocollection;
                }
                else
                {
                    HeroProfileFilmStripMatches.member = g.Member;
                    HeroProfileFilmStripMatches.Ordinal = 0;
                    HeroProfileFilmStripMatches.MyEntryPoint = BreadCrumbHelper.EntryPoint.HomeHeroProfileFilmStripYourProfile;
                    //HeroProfileFilmStripMatches.HotListCategoryID = (int)HeroProfileFilmStripMatches.MemberList[0].HotlistCategory;
                    HeroProfileFilmStripMatches.MyMOCollection = mocollection;

                }
            }
            catch (Exception ex)
            { g.ProcessException(ex); }
        }

        private List<ProfileHolder> GetHeroFilmStripProfiles(SearchPreferenceCollection prefs, int maxProfileDisplay, out MOCollection mocollection)
        {
            // declare local varialbes for resuse and clarity
            const string HASPHOTOFLAG = "HasPhotoFlag";
            int startRow = 0;
            int pageSize = maxProfileDisplay * 5;
            MatchnetQueryResults searchResults = null;

            mocollection = null;
            //get current photo preference before we modify it
            string photoPreference = prefs.Value(HASPHOTOFLAG);
            prefs.Add(HASPHOTOFLAG, "1");

            //Ensure that domainID is set on search preferences for the correct Community.
            prefs.Add(DOMAINID, g.Brand.Site.Community.CommunityID.ToString());
            prefs.Add("ColorCode", "");
            try
            {
                // Potential for exception due to invalid search preferences on this member's stored set of preferences.
                searchResults = MemberSearchSA.Instance.Search(prefs
                     , g.Brand.Site.Community.CommunityID
                     , g.Brand.Site.SiteID
                     , startRow
                     , pageSize
                     , g.Member == null ? Constants.NULL_INT : g.Member.MemberID
                     , Matchnet.Search.Interfaces.SearchEngineType.FAST
                     , Matchnet.Search.Interfaces.SearchType.ProfileFilmStripWebSearch
                     , Matchnet.Search.Interfaces.SearchEntryPoint.ProfileFilmStripMatches
                     , false
                     , false
                     , false
                     );
            }
            catch (Exception)
            {
                //invalid search preferences, initialize an empty results list so the home page will render correctly.
                searchResults = new MatchnetQueryResults(0);
            }

            //set photo preference back to old value
            prefs.Add(HASPHOTOFLAG, photoPreference);

            List<ProfileHolder> profileList = new List<ProfileHolder>();
            ArrayList results = null;
            ArrayList members = new ArrayList();
            ArrayList randomizedmembers = null;
            int resultsCount = searchResults.ToArrayList().Count;
            HotListCategory cat = HotListCategory.YourMatches;
            BreadCrumbHelper.EntryPoint entrypoint = BreadCrumbHelper.EntryPoint.HomeHeroProfileFilmStripMatches;

            if (searchResults == null || resultsCount <= 1)
            {
                mocollection = GetMembersOnlineCriteria(g.Member.GetAttributeInt(g.Brand, "RegionID"));
                results = GetMembersOnline(mocollection, pageSize);
                resultsCount = results.Count;
                if (results == null || results.Count <= 1)
                {
                    MOCollection mocollectiondefaultregion = GetMembersOnlineCriteria(g.Brand.Site.DefaultRegionID);
                    results = GetMembersOnline(mocollectiondefaultregion, pageSize);
                    resultsCount = results.Count;
                }
                entrypoint = BreadCrumbHelper.EntryPoint.HomeHeroProfileFilmStripMOL;
                _heroProfileEntry = BreadCrumbHelper.EntryPoint.HomeHeroProfileMOL;
            }
            else
            {
                results = searchResults.ToArrayList();
            }

            List<int> ordinals = null;
            if (resultsCount > 7)
            {
                if (Request.QueryString["random"] == "false")
                    randomizedmembers = results;
                else
                    randomizedmembers = RandomizeMemberIDs(results, 17, out ordinals);

//                members = MemberSA.Instance.GetMembers(randomizedmembers, MemberLoadFlags.None);
                members = MemberDTOManager.Instance.GetIMemberDTOs(g, randomizedmembers, MemberType.Search, MemberLoadFlags.None);
            }
            else
            {
//                members = MemberSA.Instance.GetMembers(results, MemberLoadFlags.None);
                members = MemberDTOManager.Instance.GetIMemberDTOs(g, results, MemberType.Search, MemberLoadFlags.None);
            }
            startRow = 0;
            foreach (IMemberDTO member in members)
            {
                //add member
                ProfileHolder ph = new ProfileHolder();
                ph.Member = member;
                if (ordinals != null && ordinals.Count > startRow)
                    ph.Ordinal = ordinals[startRow];
                else
                    ph.Ordinal = startRow + 1;
                ph.HotlistCategory = HotListCategory.YourMatches;
                ph.MyEntryPoint = entrypoint;

                profileList.Add(ph);

                startRow++;

            }
            if (profileList.Count > 0 && _heroProfileEntry == BreadCrumbHelper.EntryPoint.HomeHeroProfileMatches)
            {
                _hasMatches = true;
            }

            return profileList;
        }


        private MOCollection GetMembersOnlineCriteria(int regionid)
        {
            try
            {

                // old - get gender mask from online members if or from member
                //int gendermask = g.Session.GetInt("MembersOnlineGender", g.Member.GetAttributeInt(g.Brand, "gendermask"));

                //fix MRP-2533 - get gendermask from search preferences
                int gendermask = Conversion.CInt(g.SearchPreferences["gendermask"]);

                int agemin = g.Session.GetInt("MembersOnlineAgeMin", g.Brand.DefaultAgeMin);
                int agemax = g.Session.GetInt("MembersOnlineAgeMax", g.Brand.DefaultAgeMax);
                int languagemask = g.Session.GetInt("MembersOnlineLanguage", 0);

                MOCollection mocollection = new MOCollection(SortFieldType.HasPhoto.ToString(), gendermask, regionid, agemin, agemax, languagemask);

                return mocollection;
            }
            catch (Exception ex)
            {
                g.ProcessException(ex); return null;
            }

        }

        private ArrayList GetMembersOnline(MOCollection molcollection, int maxProfileDisplay)
        {
            try
            {

                Matchnet.MembersOnline.ValueObjects.MOLQueryResult MOLResults = MembersOnlineSA.Instance.GetMemberIDs(g.Session.Key.ToString(),
                 g.Brand.Site.Community.CommunityID,
                 (short)molcollection.GenderMask,
                 molcollection.RegionID,
                 (Int16)molcollection.AgeMin,
                 (Int16)((Int16)molcollection.AgeMax + 1),	// See TT 15298.
                 molcollection.LanguageMask,
                 SortFieldType.HasPhoto,
                 SortDirectionType.Asc,
                 1,
                 maxProfileDisplay,
                 g.Member == null ? Constants.NULL_INT : g.Member.MemberID);

                return MOLResults.ToArrayList();
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
                return null;
            }


        }
        public ArrayList RandomizeMemberIDs(ArrayList memberIDs, int resultSize, out List<int> ordinals)
        {
            ArrayList members = new ArrayList();
            int count = 0;
            ordinals = new List<int>();
            try
            {

                int size = memberIDs.Count;
                int iternum = size < resultSize ? size : resultSize;
                while (count < iternum)
                {
                    Guid uniqueID = Guid.NewGuid();
                    byte[] bArr = uniqueID.ToByteArray();
                    int autonum = BitConverter.ToInt32(bArr, 0);
                    int mynum = Math.Abs(autonum) % size;
                    if (!members.Contains(memberIDs[mynum]))
                    {
                        members.Add(memberIDs[mynum]);
                        ordinals.Add(mynum);
                        count += 1;
                    }
                }
                return members;
            }
            catch (Exception ex)
            { g.ProcessException(ex); return null; }



        }

        public ArrayList RandomizeMemberIDs(ArrayList memberIDs, int resultSize, out int startOrdinal)
        {
            ArrayList members = new ArrayList();
            int count = 0;
            startOrdinal = 0;
            try
            {

                int size = memberIDs.Count;
                int maxcount = size < resultSize ? size : resultSize;

                Guid uniqueID = Guid.NewGuid();
                byte[] bArr = uniqueID.ToByteArray();
                int autonum = BitConverter.ToInt32(bArr, 0);
                int mynum = Math.Abs(autonum) % size;
                startOrdinal = mynum;
                while (count < maxcount)
                {

                    if (!members.Contains(memberIDs[mynum]))
                    {
                        members.Add(memberIDs[mynum]);

                        count += 1;
                        mynum += 1;
                        if (mynum == size)
                        {
                            mynum = 0;
                        }
                    }
                }
                return members;
            }
            catch (Exception ex)
            { g.ProcessException(ex); return null; }



        }
        #endregion

        private void LoadShortcutList()
        {
            if (g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.AmericanSingles)
            {
                ucHotlist2.HideNotificationIcon = true;
            }
            else
            {
                ucHotlist2.HideNotificationIcon = false;
            }

            //ucHotlist2.HotlistFormat = "{0}: {1} [{2} new]";
            phShortcutList.Visible = true;

            //new matches by mail
            // this.literalShortcutNewMatchesCount.Text = g.List.GetCount(HotListCategory.YourMatches, g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID).ToString();

            //new photos count
            //int photoCount = PhotoGallerySA.Instance.GetCommunityPhotoCount(g.Brand.Site.Community.CommunityID, 24);
            //this.literalShortcutNewPhotosCount.Text = (photoCount >= 0) ? photoCount.ToString() : "0";

            //new mail indicator
            //if (g.hasNewMessage)
            //{
            //    multiviewShortcutMailIcon.SetActiveView(viewShortcutMailFlashingIcon);
            //}

        }

        private List<int> GetWhoIsCheckingYouOutCategoryList()
        {
            List<int> categories = new List<int>();

            categories.Add(-1);
            categories.Add(-4);
            categories.Add(-2);
            //categories.Add(-3);
            //categories.Add(-25);
            categories.Add(-5);
            categories.Add(-15);

            return categories;
        }
    }
}
