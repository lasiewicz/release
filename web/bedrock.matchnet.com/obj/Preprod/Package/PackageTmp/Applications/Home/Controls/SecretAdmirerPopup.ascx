﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SecretAdmirerPopup.ascx.cs" Inherits="Matchnet.Web.Applications.Home.Controls.SecretAdmirerPopup" %>
<%@ Register src="/Framework/Ui/BasicElements/SlideshowProfile.ascx" tagname="SlideshowProfile" tagprefix="uc1" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>

<div id="slideshowSecretAdmirerPopup" class="hide">
    <div id="slideshowSecretAdmirerPopupData" runat="server">
        <div id="secretAdmirer" class="secret-admirer-cont">
            <div id="slideshow" class="slideshow block-on-load">
                <uc1:SlideshowProfile ID="ucSlideshowProfileSAPopup" runat="server" SlideShowMode="SecretAdmirerGame"  />
            </div>
        </div>
    </div>
</div>
<%--
<button id="secretAdmirerModalBtn" type="button" class="textlink">Launch Secret Admirer modal</button>
--%>