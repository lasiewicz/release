﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Web.Framework;
using Matchnet.Search.ValueObjects;
using Matchnet.Search.ServiceAdapters;
using Matchnet.Web.Framework.Ui;
using System.Collections;
using Matchnet.List.ValueObjects;
using Matchnet.Member.ServiceAdapters;
using Matchnet.MembersOnline.ValueObjects;
using Matchnet.MembersOnline.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Session.ValueObjects;
using Matchnet.Web.Applications.API.JSONResult;
using System.Text;
using Newtonsoft.Json;
using Matchnet.Web.Analytics;
using Matchnet.Web.Framework.Managers;

namespace Matchnet.Web.Applications.Home.Controls
{
    public partial class MatchesSlider : FrameworkControl
    {
        protected int _MemberID = 0;
        protected int _PageSize = 7;
        protected string _OmnitureUsernameProfileURLTrackingParams = "";
        protected string _OmniturePhotoProfileURLTrackingParams = "";
        protected bool _hasMatches = false;
        protected bool _matchRatingEnabled = false;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (g.Member != null)
            {
                _MemberID = g.Member.MemberID;
                _matchRatingEnabled = MatchRatingManager.Instance.DisplayMatchRatingOnFilmstrip(_MemberID, g.Brand);

                try
                {
                    //set up omniture tracking params for profile links
                    _OmnitureUsernameProfileURLTrackingParams = Omniture.GetActionURLParams(WebConstants.PageIDs.ViewProfile, WebConstants.Action.ProfileName, g.AnalyticsOmniture.PageName, "Filmstrip");
                    _OmniturePhotoProfileURLTrackingParams = Omniture.GetActionURLParams(WebConstants.PageIDs.ViewProfile, WebConstants.Action.ProfilePhoto, g.AnalyticsOmniture.PageName, "Filmstrip");

                    //get slider profiles
                    bool hasResults = false;
                    GetSearchResult getSearchResult = HomeUtil.GetSliderProfilesJSON(g.Member, g.Brand, g, g.SearchPreferences, g.Session, 1, _PageSize, true, true, HomeUtil.MATCH_SLIDER_USERNAME_MAX, this);

                    //render js profile objects
                    StringBuilder preloadJS = new StringBuilder();
                    if (getSearchResult != null && getSearchResult.SearchProfileList != null)
                    {
                        if (getSearchResult.SearchProfileList.Count > 0)
                        {
                            hasResults = true;
                            if (getSearchResult.IsMatchResults)
                            {
                                _hasMatches = true;
                            }
                        }
                        string getSearchResultJSON = JsonConvert.SerializeObject(getSearchResult);
                        preloadJS.AppendLine("var getSearchResult = " + getSearchResultJSON + ";");
                        preloadJS.AppendLine("matchSliderManager.loadPage(getSearchResult);");
                    }
                    litPreloadJS.Text = preloadJS.ToString();

                    //update title
                    lnkViewMore.Title = g.GetResource("TXT_VIEW_MORE", this);
                    if (hasResults && getSearchResult.IsMOLResults)
                    {
                        txtComponentTitle.ResourceConstant = "TXT_COMPONENT_HEADER_MOL";
                        lnkViewMore.HRef = "/Applications/MembersOnline/MembersOnline.aspx";
                        lnkViewMore.HRef = g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.MembersOnline, WebConstants.Action.ViewMoreLink, lnkViewMore.HRef, "Filmstrip");
                    }
                    else
                    {
                        lnkViewMore.HRef = g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.SearchResults, WebConstants.Action.ViewMoreLink, lnkViewMore.HRef, "Filmstrip");
                    }
                    
                }
                catch (Exception ex)
                {
                    g.ProcessException(ex);
                }
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            try
            {
                // Omniture Analytics - Event and property variable tracking
                if (Convert.ToBoolean(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ANALYTICS_OMNITURE", _g.Brand.Site.Community.CommunityID, _g.Brand.Site.SiteID)))
                {
                    _g.AnalyticsOmniture.Prop33 = (this._hasMatches) ? "Has Matches" : "Has No Matches";
                }
                base.OnPreRender(e);
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        #region Private methods
        
        #endregion
    }
}
