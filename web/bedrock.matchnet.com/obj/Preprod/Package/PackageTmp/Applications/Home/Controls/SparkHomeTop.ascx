﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SparkHomeTop.ascx.cs" Inherits="Matchnet.Web.Applications.Home.Controls.SparkHomeTop" %>
<%@ Register src="/Framework/Ui/ProfileStatusSummary.ascx" tagname="ProfileStatusSummary" tagprefix="uc" %>
<%@ Register src="/Applications/Home/Controls/HotListButton.ascx" tagname="HotListButton" tagprefix="uc" %>
<%@ Register src="/Applications/Home/Controls/MatchesSlider.ascx" tagname="MatchesSlider" tagprefix="uc" %>
<div id="hotlistStats" class="hotlist-stats">
    <ul>
        <li class="profile"><%--Profile Activity--%> 
            <uc:ProfileStatusSummary runat="server" ID="ucProfileStatusSummary"/>
        </li>
        <li class="email clickable"><%--Emailed You--%>
            <uc:HotListButton runat="server" ID="hotListButtonEmailed"/>
        </li>
        <li class="flirt clickable"><%--Flirted with You--%>
            <uc:HotListButton runat="server" ID="hotListButtonFlirted"/>
        </li>
        <li class="im clickable"><%--Viewed You--%>
             <uc:HotListButton runat="server" ID="hotListButtonViewed"/>
        </li>
        <li class="favorite clickable"><%--My Favorites--%>
            <uc:HotListButton runat="server" ID="hotListButtonFavorites"/>
        </li>
    </ul>
</div>
<script type="text/javascript">
    $j('#hotlistStats .clickable').on('click', function (e) {
        window.location = $j(this).find("a").first().attr("href");
    });
</script>
<div class="new-matches-wrap"><%--New Matches Near You--%>
    <uc:MatchesSlider runat="server" ID="ucMatchesSlider" />
</div>
