﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HotListButton.ascx.cs" Inherits="Matchnet.Web.Applications.Home.Controls.HotListButton" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>

<div class="hotlist-stats-wrapper">
    <em id="divNotificationCount" runat="server"></em> <mn:txt id="txtHotListTitle" runat="server" Href="/Applications/HotList/View.aspx?CategoryID=" />
</div>
