using System;
using System.Web.UI.WebControls;
using Matchnet.Security;
using Matchnet.Session.ValueObjects;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Util;
using Image=System.Web.UI.WebControls.Image;

namespace Matchnet.Web.Applications.Home
{
	/// <summary>
	///		Tests an image generation.
	/// </summary>
	public class JpegImageTest :FrameworkControl
	{
		protected Label Label1;
		protected Image imgCAPTCHA;
		protected System.Web.UI.WebControls.TextBox txtCAPTCHA;
		protected Button btnVerifyCAPTCHA;

		private void Page_Load(object sender, EventArgs e)
		{
			//if a post back
			if(!Page.IsPostBack)
			{		
				GenerateNewCAPTCHAImage();
			}
		}
		
		private void GenerateNewCAPTCHAImage()
		{
			//generate and encrypt the text
			string generatedText = RandomTextGenerator.Generate(5);
			string encryptedText = Crypto.Encrypt(WebConstants.SPARK_WS_CRYPT_KEY, generatedText);
				
			//display the text in the captcha image
			imgCAPTCHA.ImageUrl = "JpegImage.aspx?CAPTCHAText=" + Server.UrlEncode((encryptedText));	
					
			//save the generated text
			g.Session.Add("CAPTCHAText", generatedText, SessionPropertyLifetime.Temporary);
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.btnVerifyCAPTCHA.Click += new System.EventHandler(this.btnVerifyCAPTCHA_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void btnVerifyCAPTCHA_Click(object sender, System.EventArgs e)
		{
			if (g.Session["CAPTCHAText"].ToUpper() != txtCAPTCHA.Text.ToUpper())
			{
				//we have a problem
				Response.Write("NOT cool");
				GenerateNewCAPTCHAImage();
			}
			else
			{
				g.Session.Remove("CAPTCHAText");
				Response.Write("Cool");
			}

		}

	}
}
