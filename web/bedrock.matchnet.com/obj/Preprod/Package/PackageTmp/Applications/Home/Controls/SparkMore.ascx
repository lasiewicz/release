﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SparkMore.ascx.cs" Inherits="Matchnet.Web.Applications.Home.Controls.SparkMore" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>

<asp:PlaceHolder ID="phJDate" runat="server">
<div class="more-content">
    <ul class="clearfix">
        <li class="clearfix"><%--JMAG--%>
            <img src="http://static.jdate.com/Components/jd-more-items/ui-more-items-thumb-01.jpg" alt="JMag" class="thumb" />
            <h2><a href="/jmag/" title="JMag"  class="ir jmag">JMag</a></h2>
            <p class="desc">As JDate's online magazine, JMag is where celebrities, experts and JDaters® come to kibitz! Part entertainment, part dating advice and completely Jewish! <a href="/jmag/" title="read more" class="read-more">read more »</a></p>
            <ol>
                <li class="col-1"><a href="/jmag/category/forjdatersbyjdaters/" title="For JDaters by JDaters">For JDaters by JDaters</a></li>
                <li class="col-1"><a href="/jmag/category/expertadvice/" title="Expert Advice">Expert Advice</a></li>
                <li class="col-1"><a href="/jmag/category/interviews/" title="Interviews">Interviews</a></li>
                <li class="col-2 reset"><a href="https://www.youtube.com/user/jdate" title="Videos">Videos</a></li>
                <li class="col-2"><a href="/jmag/category/jcentral/" title="JCentral">JCentral</a></li>
            </ol>
        </li>
        <li class="clearfix"><%--Success Stories--%>
            <img src="http://static.jdate.com/Components/jd-more-items/ui-more-items-thumb-02.jpg" alt="Success Stories" class="thumb" />
            <h2><a href="/jmag/category/successstories/" title="Success Stories"  class="ir success">Success Stories</a></h2>
            <p class="desc">JDate is responsible for more Jewish marriages than all other dating sites combined! Meet real JDate Success Stories and be inspired to meet your Beshert! <a href="/jmag/category/successstories/" title="read more" class="read-more">read more »</a></p>
            <ol>
                <li class="col-1"><a href="/jmag/category/successstories/" title="Success Stories">Success Stories</a></li>
                <li class="col-1"><a href="/jmag/submitstory/" title="Submit Your Story">Submit Your Story</a></li>
                <li class="col-1"><a href="/jmag/did-you-know/" title="Did You Know?">Did You Know?</a></li>
                <li class="col-2 reset"><a href="/jmag/best-of/" title="Best Of">Best Of</a></li>
                <li class="col-2"><a href="/jmag/alumni-news/" title="Alumni News">Alumni News</a></li>
            </ol>
        </li>
        <li class="clearfix"><%--Events--%>
            <img src="http://static.jdate.com/Components/jd-more-items/ui-more-items-thumb-03.jpg" alt="Mobile" class="thumb" />
            <h2><a href="http://www.jdate.com/mobile" title="Mobile Offerings!"  class="ir mobile">Mobile Offerings!</a></h2>
            <p class="desc">New matches at your fingertips! Whether you're at home, on the train, or in line at the bank, we've made it easy for you to connect with Jewish singles near you. <a href="http://www.jdate.com/mobile" title="read more" class="read-more">read more »</a></p>
            <ol>
                <li class="col-1"><a href="/MobileLanding.aspx?App=ios" title="iOS App">iOS App</a></li><%-- use ?App=ios, ?App=android, ?App=mos --%>
                <%--<li class="col-1"><a href="#" title="Android App">Android App</a></li>--%>
                <li class="col-1"><a href="http://m.jdate.com" title="Mobile Site">Mobile Site</a></li>
            </ol>
        </li>
    </ul>
</div>
</asp:PlaceHolder>