﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Managers;
using Matchnet.Web.Framework.Ui;
using Matchnet.Member.ValueObjects.Interfaces;
using Matchnet.List.ValueObjects;
using System.Collections;
using Matchnet.Member.ValueObjects.MemberDTO;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Web.Framework.Ui.PageAnnouncements;
using Matchnet.Web.Analytics;
using Matchnet.Web.Framework.Util;

namespace Matchnet.Web.Applications.Home
{
    public partial class SparkHome50 : FrameworkControl
    {
        protected int _memberID = 0;
        protected bool _memberIsSub = false;
        private int _maxListItemCount = 0;
        private int _maxNewsfeedCount = 0;
        private string _OmnitureYNMList = HomeUtil.HomeTabOmnitureName[2] + " - Secret Admirer";
        private string _OmnitureContentBlocks = "Content Blocks";
        private ListManager listManager = new ListManager();
        private int _highlightedProfileCount = 0;
        private bool HideCountIndicatorsOnYourList = true;

        #region Event Handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //load homepage; ensure member is logged in (not available for visitors)
                if (g.Member != null && g.Member.MemberID > 0)
                {
                    _memberID = g.Member.MemberID;
                    _memberIsSub = g.Member.IsPayingMember(g.Brand.Site.SiteID);

                    SettingsManager settingsManager = new SettingsManager();

                    _maxListItemCount = settingsManager.GetSettingInt("HOMEPAGE_HOTLIST_TAB_CONTENT_MAX_COUNT", g.Brand, 24);
                    _maxNewsfeedCount = settingsManager.GetSettingInt("HOMEPAGE_NEWSFEED_TAB_CONTENT_MAX_COUNT", g.Brand, 6);

                    //save results url used in view profile's "back to results" feature
                    FrameworkGlobals.SaveBackToResultsURL(g);

                    // if any notification messages that came through session, display it
                    HandleNotificationMessage();

                    //profile status summary
                    ucProfileStatusSummary.EntryPoint = Framework.Ui.BreadCrumbHelper.EntryPoint.HomePage;

                    //hotlist buttons
                    int emailNotificationCount = listManager.GetUnseenNotificationCount(g.List, g.Member, g.Brand,
                                                                                        HotListCategory.WhoEmailedYou);
                    int flirtedNotificationCount = listManager.GetUnseenNotificationCount(g.List, g.Member, g.Brand,
                                                                                          HotListCategory.WhoTeasedYou);
                    int viewedNotificationCount = listManager.GetUnseenNotificationCount(g.List, g.Member, g.Brand,
                                                                                         HotListCategory.
                                                                                             WhoViewedYourProfile);

                    hotListButtonEmailed.LoadHotListButton(HotListCategory.WhoEmailedYou, "TXT_EMAILED_YOU",
                                                           emailNotificationCount);
                    hotListButtonFlirted.LoadHotListButton(HotListCategory.WhoTeasedYou, "TXT_FLIRTED_WITH_YOU",
                                                           flirtedNotificationCount);
                    hotListButtonViewed.LoadHotListButton(HotListCategory.WhoViewedYourProfile, "TXT_VIEWED_YOU",
                                                          viewedNotificationCount);
                    hotListButtonFavorites.LoadHotListButton(HotListCategory.Default, "TXT_YOUR_FAVORITES");

                    // filmstrip based on page version
                    ControlVersion version = HomeUtil.GetHomepageVersion(g.Member, g.Brand);
                    if (version == ControlVersion.Version50)
                    {
                        phMatchesSliderYNM.Visible = true;
                    }
                    else
                    {
                        phMatchesSlider.Visible = true;
                    }


                    //newsfeed tab content
                    newsfeedDoubleControl.LoadNewsfeed(0, _maxNewsfeedCount);
                    if (newsfeedDoubleControl.NewNotificationsCount > 0)
                    {
                        phNewsfeedNotificationCount.Visible = true;
                        litNewsfeedNotificationCount.Text = newsfeedDoubleControl.NewNotificationsCount.ToString();
                    }

                    //your visitors tab content
                    hotlistEmailedYou.EntryPoint = BreadCrumbHelper.EntryPoint.HomePage;
                    hotlistEmailedYou.OmnitureWidgetName = HomeUtil.HomeTabOmnitureName[1] + " - Emailed You";
                    hotlistEmailedYou.LoadHotlistScroller(_maxListItemCount);

                    hotlistFlirtedWithYou.EntryPoint = BreadCrumbHelper.EntryPoint.HomePage;
                    hotlistFlirtedWithYou.OmnitureWidgetName = HomeUtil.HomeTabOmnitureName[1] + " - Flirted With You";
                    hotlistFlirtedWithYou.LoadHotlistScroller(_maxListItemCount);

                    hotlistViewedYourProfile.EntryPoint = BreadCrumbHelper.EntryPoint.HomePage;
                    hotlistViewedYourProfile.OmnitureWidgetName = HomeUtil.HomeTabOmnitureName[1] + " - Viewed Your Profile";
                    hotlistViewedYourProfile.LoadHotlistScroller(_maxListItemCount);
                    int yourVisitorsNotificationCount = hotlistEmailedYou.NewHotlistNotificationCount +
                                                    hotlistFlirtedWithYou.NewHotlistNotificationCount +
                                                    hotlistViewedYourProfile.NewHotlistNotificationCount;
                    if (yourVisitorsNotificationCount > 0)
                    {
                        phYourVisitorsNotificationCount.Visible = true;
                        litYourVisitorsNotificationCount.Text = yourVisitorsNotificationCount.ToString();
                    }

                    if (hotlistEmailedYou.IsEmpty && hotlistFlirtedWithYou.IsEmpty && hotlistViewedYourProfile.IsEmpty)
                    {
                        phYourVisitorsNotificationCount.Visible = false;
                        phVisitorsScrollers.Visible = false;
                        phVisitorsNoScrollerContent.Visible = true;

                        lnkNewsfeedEditProfile.HRef = g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.ViewProfile, WebConstants.Action.ViewPageImageLink, lnkNewsfeedEditProfile.HRef, HomeUtil.HomeTabOmnitureName[1]);
                        lnkNewsfeedEditProfile2.HRef = g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.ViewProfile, WebConstants.Action.ViewPageTitle, lnkNewsfeedEditProfile2.HRef, HomeUtil.HomeTabOmnitureName[1]);

                        lnkNewsfeedMemberPhotoEdit.HRef = g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.MemberPhotoEdit, WebConstants.Action.ViewPageImageLink, lnkNewsfeedMemberPhotoEdit.HRef, HomeUtil.HomeTabOmnitureName[1]);
                        lnkNewsfeedMemberPhotoEdit2.HRef = g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.MemberPhotoEdit, WebConstants.Action.ViewPageTitle, lnkNewsfeedMemberPhotoEdit2.HRef, HomeUtil.HomeTabOmnitureName[1]);

                        lnkNewsfeedMatchPreferences.HRef = g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.SearchResults, WebConstants.Action.ViewPageImageLink, lnkNewsfeedMatchPreferences.HRef, HomeUtil.HomeTabOmnitureName[1]);
                        lnkNewsfeedMatchPreferences2.HRef = g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.SearchResults, WebConstants.Action.ViewPageTitle, lnkNewsfeedMatchPreferences2.HRef, HomeUtil.HomeTabOmnitureName[1]);

                        lnkNewsfeedSearch.HRef = g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.SearchResults, WebConstants.Action.ViewPageImageLink, lnkNewsfeedSearch.HRef, HomeUtil.HomeTabOmnitureName[1]);
                        lnkNewsfeedSearch2.HRef = g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.SearchResults, WebConstants.Action.ViewPageTitle, lnkNewsfeedSearch2.HRef, HomeUtil.HomeTabOmnitureName[1]);
                    }

                    //your list tab content
                    hotlistYourFavorites.EntryPoint = BreadCrumbHelper.EntryPoint.HomePage;
                    hotlistYourFavorites.OmnitureWidgetName = HomeUtil.HomeTabOmnitureName[2] + " - Your Favorites";
                    hotlistYourFavorites.HideCountIndicators = HideCountIndicatorsOnYourList;
                    hotlistYourFavorites.LoadHotlistScroller(_maxListItemCount);

                    hotlistYouViewed.EntryPoint = BreadCrumbHelper.EntryPoint.HomePage;
                    hotlistYouViewed.OmnitureWidgetName = HomeUtil.HomeTabOmnitureName[2] + " - You Viewed";
                    hotlistYouViewed.HideCountIndicators = HideCountIndicatorsOnYourList;
                    hotlistYouViewed.LoadHotlistScroller(_maxListItemCount);

                    //your list ynm title and view all
                    txtHotlistTitle.Href = "/Applications/HotList/SlideShow.aspx";
                    txtHotlistTitle.Href = g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.SlideShow, WebConstants.Action.ViewPageTitle, txtHotlistTitle.Href, _OmnitureYNMList);

                    txtViewAllYNM.Href = "/Applications/HotList/SlideShow.aspx";
                    txtViewAllYNM.Href = g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.SlideShow, WebConstants.Action.ViewMoreLink, txtViewAllYNM.Href, _OmnitureYNMList);

                    //your list mutual
                    int newYNMMutualCount = listManager.GetUnseenNotificationCount(g.List, g.Member, g.Brand, HotListCategory.MutualYes);
                    txtViewAllMutualTitle.Href = "/Applications/HotList/View.aspx?CategoryID=" + ((int)HotListCategory.MutualYes).ToString();
                    txtViewAllMutual.Href = "/Applications/HotList/View.aspx?CategoryID=" + ((int)HotListCategory.MutualYes).ToString();
                    txtViewAllMutualTitle.Href = g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.View, WebConstants.Action.ViewPageLink, txtViewAllMutualTitle.Href, _OmnitureYNMList);
                    txtViewAllMutual.Href = g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.View, WebConstants.Action.ViewMoreLink, txtViewAllMutual.Href, _OmnitureYNMList);

                    //your list yes
                    int newYNMYesCount = listManager.GetUnseenNotificationCount(g.List, g.Member, g.Brand, HotListCategory.MembersClickedYes);
                    txtViewAllYesTitle.Href = "/Applications/HotList/View.aspx?CategoryID=" + ((int)HotListCategory.MembersClickedYes).ToString();
                    txtViewAllYes.Href = "/Applications/HotList/View.aspx?CategoryID=" + ((int)HotListCategory.MembersClickedYes).ToString();
                    txtViewAllYesTitle.Href = g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.View, WebConstants.Action.ViewPageLink, txtViewAllYesTitle.Href, _OmnitureYNMList);
                    txtViewAllYes.Href = g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.View, WebConstants.Action.ViewMoreLink, txtViewAllYes.Href, _OmnitureYNMList);

                    //your list maybe
                    int newYNMMaybeCount = listManager.GetUnseenNotificationCount(g.List, g.Member, g.Brand, HotListCategory.MembersClickedMaybe);
                    txtViewAllMaybeTitle.Href = "/Applications/HotList/View.aspx?CategoryID=" + ((int)HotListCategory.MembersClickedMaybe).ToString();
                    txtViewAllMaybe.Href = "/Applications/HotList/View.aspx?CategoryID=" + ((int)HotListCategory.MembersClickedMaybe).ToString();
                    txtViewAllMaybeTitle.Href = g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.View, WebConstants.Action.ViewPageLink, txtViewAllMaybeTitle.Href, _OmnitureYNMList);
                    txtViewAllMaybe.Href = g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.View, WebConstants.Action.ViewMoreLink, txtViewAllMaybe.Href, _OmnitureYNMList);

                    int newYNMCount = newYNMMaybeCount + newYNMMutualCount + newYNMYesCount;
                    if (newYNMCount > 0 && !HideCountIndicatorsOnYourList)
                    {
                        phListYNMNotificationCount.Visible = true;
                        litListYNMNotificationCount.Text = newYNMCount.ToString();
                    }

                    int yourListNotificationCount = hotlistYourFavorites.NewHotlistNotificationCount +
                                                    hotlistYouViewed.NewHotlistNotificationCount +
                                                    newYNMCount;

                    if (yourListNotificationCount > 0 && !HideCountIndicatorsOnYourList)
                    {
                        phYourListsNotificationCount.Visible = true;
                        litYourListNotificationCount.Text = yourListNotificationCount.ToString();
                    }

                    //subscription expiration alert
                    if (SubscriptionExpirationManager.Instance.DisplaySubscriptionExpirationAlert(g.Brand, g.Member, g))
                    {
                        SubscriptionExpirationAlert subscriptionExpirationAlert = Page.LoadControl("/Framework/Ui/PageAnnouncements/SubscriptionExpirationAlert.ascx") as SubscriptionExpirationAlert;
                        g.LayoutTemplateBase.AddPageAccouncementControl(subscriptionExpirationAlert);

                    }

                    //push flirts
                    LoadPushFlirtsPopup();

                    //content links (includes omniture)
                    lnkViewMoreEventsPhoto.HRef = g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.Events,
                                                                                WebConstants.Action.ViewMoreImageLink,
                                                                                lnkViewMoreEventsPhoto.HRef,
                                                                                _OmnitureContentBlocks);
                    lnkViewMoreEventsHeader.HRef = g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.Events,
                                                                                WebConstants.Action.ViewPageTitle,
                                                                                lnkViewMoreEventsHeader.HRef,
                                                                                _OmnitureContentBlocks);
                    lnkViewMoreEvents.HRef = g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.Events,
                                                                                WebConstants.Action.ViewMoreLink,
                                                                                lnkViewMoreEvents.HRef,
                                                                                _OmnitureContentBlocks);
                    lnkTravelAdventures.HRef = g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.Events,
                                                                                WebConstants.Action.ViewPageLink,
                                                                                lnkTravelAdventures.HRef,
                                                                                _OmnitureContentBlocks);
                    lnkPartiesEvents.HRef = g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.Events,
                                                                                WebConstants.Action.ViewPageLink,
                                                                                lnkPartiesEvents.HRef,
                                                                                _OmnitureContentBlocks);
                    lnkSpeedDating.HRef = lnkSpeedDating.HRef + "&memberid=" + g.Member.MemberID.ToString() + "&siteid=" +
                                          g.Brand.Site.SiteID.ToString();


                }
                else
                {
                    //member is not logged in, send to the splash page
                    g.Transfer("/");
                }
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        #endregion

        #region Private Methods
        private void HandleNotificationMessage()
        {
            if (g.Session.Get(WebConstants.SESSION_NOTIFICATION_MESSAGE) != null)
            {
                g.Notification.AddMessage(g.Session.Get(WebConstants.SESSION_NOTIFICATION_MESSAGE).ToString());
                g.Session.Remove(WebConstants.SESSION_NOTIFICATION_MESSAGE);
            }
        }

        private void LoadPushFlirtsPopup()
        {
            FlirtManager flirtManager = new FlirtManager(g);

            if (flirtManager.ShowPushFlirtsToMember(PushFlirtViewMode.ForcedOverlay))
            {
                phPushFlirtsJS.Visible = true;
                g.LayoutTemplateBase.DisableGamOverlay();
                flirtManager.IncrementPushFlirtsShownCount();
            }
            else
            {
                phPushFlirtsJS.Visible = false;
                pushFlirts.Visible = false;
            }
        }
        #endregion
    }
}
