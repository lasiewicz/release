﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SparkHomeTopB.ascx.cs" Inherits="Matchnet.Web.Applications.Home.Controls.SparkHomeTopB" %>
<%@ Register src="/Framework/Ui/ProfileStatusSummary.ascx" tagname="ProfileStatusSummary" tagprefix="uc" %>
<%@ Register src="/Applications/Home/Controls/HotListButton.ascx" tagname="HotListButton" tagprefix="uc" %>
<%@ Register src="/Applications/Home/Controls/MatchesWithHeroProfile.ascx" tagname="MatchesWithHeroProfile" tagprefix="uc" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>

<div id="hotlistStats" class="hotlist-stats-wrap stack">
    <h2 class="component-header"><mn:Txt runat="server" ResourceConstant="TXT_HOTLIST_HEADER" /> <small><a href="/Applications/HotList/View.aspx?CategoryID=-1" id="hotlistLnkViewMore" runat="server"><mn:Txt runat="server" ResourceConstant="TXT_VIEW_MORE" /> </a></small></h2>
    <div class="hotlist-stats stack">
        <ul>
            <li class="profile"><%--Profile Activity--%> 
                <uc:ProfileStatusSummary runat="server" ID="ucProfileStatusSummary"/>
            </li>
            <li class="email clickable"><%--Emailed You--%>
                <uc:HotListButton runat="server" ID="hotListButtonEmailed"/>
            </li>
            <li class="flirt clickable"><%--Flirted with You--%>
                <uc:HotListButton runat="server" ID="hotListButtonFlirted"/>
            </li>
            <li class="im clickable"><%--Viewed You--%>
                 <uc:HotListButton runat="server" ID="hotListButtonViewed"/>
            </li>
            <li class="favorite clickable"><%--My Favorites--%>
                <uc:HotListButton runat="server" ID="hotListButtonFavorites"/>
            </li>
        </ul>
    </div>
</div>
<script type="text/javascript">
    $j('#hotlistStats .clickable').on('click', function (e) {
        window.location = $j(this).find("a").first().attr("href");
    });
</script>
<div class="new-matches-wrap static"><%--New Matches Near You--%>
    <uc:MatchesWithHeroProfile runat="server" ID="ucMatchesWithHeroProfile" />
</div>