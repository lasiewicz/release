﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SparkHome.ascx.cs" Inherits="Matchnet.Web.Applications.Home.SparkHome" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mnl" Namespace="Matchnet.Web.Framework.Ui.BasicElements.Links" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mnbe" Namespace="Matchnet.Web.Framework.Ui.BasicElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="qa" TagName="MiniQuestionModule" Src="~/Applications/QuestionAnswer/Controls/MiniQuestionModule.ascx" %>
<%@ Register TagPrefix="qa" TagName="MiniAnswerModule" Src="~/Applications/QuestionAnswer/Controls/MiniAnswerModule.ascx" %>
<%@ Register tagprefix="uc2" tagname="HeroProfileFilmStrip" src="~/Framework/Ui/BasicElements/HeroProfileFilmStrip.ascx"  %>
<%@ Register tagprefix="uc2" tagname="ProfileActivity" src="~/Framework/Ui/BasicElements/ProfileActivity.ascx"  %>
<%@ Register TagPrefix="uc1" TagName="HotlistProfileStrip" Src="/Applications/Hotlist/HotlistProfileStrip.ascx" %>
<%@ Register TagPrefix="uc1" TagName="MicroProfileList" Src="../../Framework/Ui/BasicElements/MicroProfileList.ascx" %>
<%@ Register TagPrefix="uc1" TagName="MicroProfileStrip" Src="../../Framework/Ui/BasicElements/MicroProfileStrip.ascx" %>
<%@ Register TagPrefix="uc1" TagName="HotListBar" Src="/Applications/Hotlist/HotListBar.ascx" %>
<%@ Register TagPrefix="qa" TagName="SubmitQuestionClientModule" Src="~/Applications/QuestionAnswer/Controls/SubmitQuestionClientModule.ascx" %>
<%@ Register TagPrefix="qa2" TagName="MiniAnswerClientModule" Src="~/Applications/QuestionAnswer/Controls/MiniAnswerClientModule.ascx" %>
<%@ Register tagprefix="uc1" Tagname="SecretAdmirerPopup" src="~/Applications/Home/Controls/SecretAdmirerPopup.ascx"   %>

<div id="page-container">
<div id="hp-hero" class="clearfix rbox-style-high">
    <div id="hp-hotlist-stats">
    <asp:PlaceHolder ID="phHotLists" runat="server">
            <uc1:HotListBar id="ucHotlist2" runat="server" Direction="2" ClassSelected="selected" Class="hp-hotlist-stats" ListItemClass="" HideIfAllListsEmpty="true" />
            <%--<uc1:HotListBar id="ucHotlist1" runat="server" Direction="1" ClassSelected="selected" Class="hp-hotlist-stats" ListItemClass="" />--%>
            <p class="view-more"><a href="/Applications/HotList/View.aspx?CategoryID=-1"><mn2:FrameworkLiteral ID="FrameworkLiteral6" runat="server" ResourceConstant="TXT_VIEW_ALL_HOTLISTS"></mn2:FrameworkLiteral></a></p>
            
            <asp:PlaceHolder ID="phShortcutList" runat="server" Visible="false">
                <h2 class="component-header"><mn:Txt ID="txtShortcutHeader" runat="server" ResourceConstant="TXT_SHORTCUT" /></h2>
                <ul class="hp-hotlist-stats">
                    <li class="clearfix">
                        <a name="noclick" class="hotlist-stat-icon"><span class="spr s-icon-history-blank"><span></span></span></a><a href="/Applications/HotList/View.aspx?CategoryID=-9" class="hotlist-stat-category"><mn:txt id="txtShortcutYourCheckingOut" runat="server" resourceconstant="TXT_SHORTCUT_YOUR_CHECKING_OUT" expandimagetokens="true" />
                      </a></li>
                    <li class="clearfix"><a name="noclick" class="hotlist-stat-icon">
                        <asp:MultiView ID="multiviewShortcutMailIcon" runat="server" EnableViewState="false" ActiveViewIndex="0">
                            <asp:View ID="viewShortcutMailBlankIcon" runat="server"><span class="spr s-icon-history-blank"><span></span></span></asp:View>
                            <asp:View ID="viewShortcutMailFlashingIcon" runat="server"><span class="spr s-icon-icon-email-new"><span></span></span></asp:View>
                        </asp:MultiView></a><a href="/Applications/Email/MailBox.aspx" class="hotlist-stat-category"><mn:Txt ID="txtShortcutMail" runat="server" ResourceConstant="TXT_SHORTCUT_MAIL" /><asp:Literal ID="literalShortcutNewMailCount" runat="server"></asp:Literal></a>
                    </li>
                    <li class="clearfix"><a name="noclick" class="hotlist-stat-icon"><span class="spr s-icon-history-blank"><span></span></span></a><a href="/Applications/HotList/View.aspx?CategoryID=-10" class="hotlist-stat-category"><mn:Txt ID="txtShortcutMatches" runat="server" ResourceConstant="TXT_SHORTCUT_MATCHES" /> <asp:Literal ID="literalShortcutNewMatchesCount" runat="server"></asp:Literal></a></li>
                    <li class="clearfix"><a name="noclick" class="hotlist-stat-icon"><span class="spr s-icon-history-blank"><span></span></span></a><a href="/Applications/PhotoGallery/PhotoGallery.aspx" class="hotlist-stat-category"><mn:Txt ID="txtShortcutPhotos" runat="server" ResourceConstant="TXT_SHORTCUT_PHOTOS" /> <asp:Literal ID="literalShortcutNewPhotosCount" runat="server"></asp:Literal></a></li>
                    
                    <li><asp:Label runat="server" ID="lblPushflirts" Visible="false">Shown</asp:Label></li>
                </ul>
            </asp:PlaceHolder>
      </asp:PlaceHolder>
      <asp:PlaceHolder runat="server" ID="pushFlirts">
        <div class="clearfix stats-new"><span class="stats-new-tag"><mn:txt runat="server" id="txtNew" resourceconstant="TXT_NEW" expandimagetokens="true" /></span> <button type="button" id="pushFlirtsButton" class="textlink"><mn:txt runat="server" id="txtButtonStartFlirting" resourceconstant="TXT_BUTTON_START_FLIRTING" expandimagetokens="true" /></button>
            <div id="pushFlirtsContainer" class="hide">
                <div id="pushFlirtsContInner" class="padding-heavy"><!--filled via ajax --></div>
            </div><!-- end pushFlirtsContainer -->

            <script>
                __addToNamespace__('spark.pushFlirts', {
                    sendAmount: '0',
                    dialogTitle: '<%=g.GetResource("TXT_FLIRT_MODAL_TITLE",this) %>',
                    messageNoneSelected: '<%=g.GetResource("TXT_FLIRT_MESSAGE_NONE_SELECTED",this) %>',
                    messageErrorGeneral: '<%=g.GetResource("TXT_FLIRT_MESSAGE_UNKNOWN_ERROR",this) %>'
                });

                $j(spark.pushFlirts).bind("pushFlirtsRefresh", function (event) {
                    $j.ajax({
                        type: "POST",
                        url: "/Applications/API/Flirts.asmx/GetPushFlirts",
                        contentType: "application/json; charset=ISO-8859-1",
                        dataType: "json",
                        beforeSend: function () {
                            $j('#pushFlirtsContInner').html(spark.util.ajaxLoader);
                        },
                        success: function (response) {
                            $j('#pushFlirtsContInner').html(response.d).find('#pushFlirtsSend').bind('click', function (event) {
                                $j(spark.pushFlirts).trigger("pushFlirtsValidate", $j('#pushFlirtsContainer'));
                            });
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            $j(spark).trigger("jqueryUIDialogMessaging", [spark.pushFlirts.messageErrorGeneral, "notification error"]);
                        }
                    });
                });

                $j(spark.pushFlirts).bind("pushFlirtsValidate", function (event, obj) {
                    var $checkBoxes = $j(obj).find('input[type=checkbox]'),
                    $checkedCheckboxes = $j(obj).find('input[type=checkbox]:checked');

                    $checkBoxes.bind('change', function () {
                        $j('#pushFlirtsButtonContainer').unblock();
                    });

                    // set number of selected members to flirt with
                    spark.pushFlirts.sendAmount = $checkedCheckboxes.length;

                    if ($checkedCheckboxes.length) {
                        $j(spark.pushFlirts).trigger("pushFlirtsSubmit");
                    }
                    else {
                        $j('#pushFlirtsButtonContainer').block({
                            message: spark.pushFlirts.messageNoneSelected,
                            overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.9
                            },
                            css: {
                                width: 'auto',
                                top: '0',
                                left: '0px',
                                backgroundColor: null,
                                border: null,
                                padding: null,
                                color: null,
                                margin: null,
                                textAlign: null
                            }
                        }).find('.blockMsg').addClass('notification');
                    }
                });

                $j(spark.pushFlirts).bind("pushFlirtsSubmit", function (event) {

                    var targetMembersString = "";
                    var count = 0;

                    $j('#pushFlirtsContainer').find('input[type=checkbox]:checked').each(function () {
                        if (targetMembersString == "") {
                            targetMembersString = $j(this).data('memberid');
                        }
                        else {
                            targetMembersString = targetMembersString + "," + $j(this).data('memberid');
                        }
                        count++;
                    });

                    sendPushFlirtsOmniture(count, targetMembersString);

                    $j.ajax({
                        type: "POST",
                        url: "/Applications/API/Flirts.asmx/ProcessPushFlirts",
                        data: "{ 'memberIDs': '" + targetMembersString + "'}",
                        contentType: "application/json; charset=ISO-8859-1",
                        dataType: "json",
                        beforeSend: function () {
                            $j('#pushFlirtsContInner').html(spark.util.ajaxLoader);
                        },
                        success: function (response) {
                            $j('#pushFlirtsContInner').html(response.d);
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            $j(spark).trigger("jqueryUIDialogMessaging", [spark.pushFlirts.messageErrorGeneral, "notification error"]);

                        }
                    });
                });

                /* bind feature specific custom behaviors */
                $j(spark.pushFlirts).bind("pushFlirtsDialog", function (event, title) {

                    if ($j('#pushFlirtsContainer').length === 0) {
                        $j('<div id="pushFlirtsContainer" class="hide"></div>').appendTo('body');
                    }

                    $j('#pushFlirtsContainer')
                    .dialog({
                        width: 669,
                        modal: true,
                        title: spark.pushFlirts.dialogTitle,
                        position: ['center', 'top'],
                        dialogClass: 'modal-push-flirts',
                        minHeight: 400,
                        open: function () {
                            var $this = $j(this),
                                ajax_load = '<div class="loading spinner-only" />';
                            $j(spark.pushFlirts).trigger('pushFlirtsRefresh');
                        },
                        close: function () {
                            $j('#pushFlirtsContainer').find('.notification, .conf-flirt').remove();
                        }
                    })
                    .parent('.ui-dialog').css('top', function () {
                        var x = $j(this).position();
                        return x.top + 12;
                    });
                });

                /* bind custom behaviors on dom ready */
                $j(function () {
                    $j('#pushFlirtsButton').bind('click', function (event) {
                        $j(spark.pushFlirts).trigger("pushFlirtsDialog");
                    });
                });


                function sendPushFlirtsOmniture(numberSent, targetMembersString) {

                    var sentString = numberSent + " flirts sent";
                    var s = s_gi(s_account);
                    PopulateS(true);
                    s.prop48 = sentString;
                    s.events = 'event41,event5,event2';
                    s.t(true);

                    var i = 0;
                    for (i = 0; i < numberSent; i++) {
                        s = s_gi(s_account);
                        s.prop48 = '';
                        s.linkTrackVars = 'events';
                        s.linkTrackEvents = 'event11';
                        s.events = 'event11';
                        s.eVar26 = "Tease";
                        s.prop35 = targetMembersString;
                        s.t(true);
                    }
                }
                        
            </script>    
        </div>                
                    
    </asp:PlaceHolder>

    <asp:PlaceHolder ID="plcPopupPushFlirtsDialog" runat="server" Visible="false">
        <script type="text/javascript">
            $j(function () {
                $j(spark.pushFlirts).trigger("pushFlirtsDialog");
            });
        </script>
    </asp:PlaceHolder>

      <asp:PlaceHolder ID="phEmptyListDisplay" runat="server" Visible="false">
      <mn:Txt ID="txtEMptyListDisplay" ResourceConstant="TXT_EMPTY_LISTS_TEXT" runat="server" />
      </asp:PlaceHolder>
        </div>
       <%-- <uc2:ProfileActivity ID="ProfileActivity1" runat="server" TrackingParam="ClickFrom=Homepage%20%u2013%20Profile%20Activity%20Module" />--%>
    <uc2:HeroProfileFilmStrip ID="HeroProfileFilmStripMatches" runat="server" TrackingParam="ClickFrom=Homepage - Your Matches" />
</div>


     <uc1:SecretAdmirerPopup ID="ucSecretAdmirerPopup" runat="server" />



    <asp:PlaceHolder ID="phQuestionAnswer" runat="server" Visible="false">
        <div id="qanda" class="qanda border-box clearfix">
            <h2 class="component-header"><mn2:FrameworkLiteral ID="literalQuestionAnswerHeader" runat="server" ResourceConstant="TXT_TODAYSPARKS"></mn2:FrameworkLiteral>&#160;<mn:Txt runat="server"  ResourceConstant="TXT_TODAYS_SPARKS" ID="mnTxtTodaysSparks" />
            </h2>
            
            <!--Question module-->
            <qa:MiniQuestionModule ID="MiniQuestionModule1" runat="server" />
            <!--Answer module-->
            <qa:MiniAnswerModule ID="MiniAnswerModule1" runat="server" TrackingParam="ClickFrom=Homepage%20%u2013%20Question%20of%20the%20Week" />

            <asp:PlaceHolder ID="phNoAnswers" runat="server" Visible="false">
                <div id="mini-a-module" class="mini-a-module clearfix">
                    <qa2:MiniAnswerClientModule ID="MiniAnswerClientModule1" ElementId="answers-list" runat="server" />    
                    <div id="answers-list">
                        <div id="qanda-no-answers"><mn:Txt ID="txtNoAnswers" runat="server" ResourceConstant="TXT_NO_ANSWERS" /></div>
                    </div>
                </div>
                <script type="text/javascript">
                    $j(".header-options").hide();
                </script>        
            </asp:PlaceHolder>
            <qa:SubmitQuestionClientModule ID="sendQuestionClientModule" runat="server" />
        </div>
    </asp:PlaceHolder>



<!--iframe used by SaveYNMVote function-->
<iframe name="FrameYNMVote" tabindex="-1" frameborder="0" scrolling="no" width="1" height="1">
	<layer name="FrameYNMVote" frameborder="0" scrolling="no" width="1" height="1"></layer>
</iframe>
</div>

