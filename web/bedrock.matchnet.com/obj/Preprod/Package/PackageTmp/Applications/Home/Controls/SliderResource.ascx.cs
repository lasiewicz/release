﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Web.Framework;

namespace Matchnet.Web.Applications.Home.Controls
{
    /// <summary>
    /// Empty control used for shared resources for Slider text via ajax or server side
    /// </summary>
    public partial class SliderResource : FrameworkControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
    }
}