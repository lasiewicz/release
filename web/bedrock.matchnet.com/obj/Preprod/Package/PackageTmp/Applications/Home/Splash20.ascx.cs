﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;

using Matchnet.Web.Framework;
using Matchnet.Lib;
using Matchnet.Lib.Util;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Web.Applications.Registration;
using Matchnet.Web.Framework.Enumerations;

namespace Matchnet.Web.Applications.Home
{
    public partial class Splash20 : FrameworkControl
    {
        private const string OMNITURE_PAGE_NAME = "Splash Page {0} - {2}";
        Registration.Registration idRegStep;
        private bool redirected = false;
        protected override void OnInit(EventArgs e)
        {
            try
            {
                base.OnInit(e);

                //redirect root (to support auto-redirect by network to reg/splash app)
                if (RuntimeSettings.GetSetting("DISABLE_BEDROCK_SPLASH", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID) == "true")
                {
                    redirected = true;
                    _g.Transfer("/");
                    Response.End();
                }

                // Splash page redirects are permanent, so response code 301 is passed to the client.
                string redirectUrl = RuntimeSettings.GetSetting("ENABLE_SPLASH_REDIRECT", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID);
                if (redirectUrl != String.Empty)
                {
                    redirected = true;
                    g.Transfer(redirectUrl, "301");
                }

                idRegStep = (Registration.Registration)LoadControl("/Applications/Registration/Registration.ascx");
                //idRegStep.TemplatePath = "/Applications/Registration/XML/SplashRegistration.xml";
                idRegStep.AjaxEnabled = true;
                idRegStep.IsSplash = true;
                idRegStep.FlowType = RegistrationFlowType.Splash;
                if (!Page.IsPostBack)
                    idRegStep.LoadNewWizard = true;

                idRegStep.SetOmniture = false;
                phContent.Controls.Add(idRegStep);

            }
            catch (Exception ex)
            { g.ProcessException(ex); }

        }

        private void Page_Load(object sender, System.EventArgs e)
        {

        }

        private void Page_PreRender(object sender, System.EventArgs e)
        {
            if (!redirected)
            {
                if (idRegStep != null)
                {
                    g.AnalyticsOmniture.PageName = idRegStep.GetOmnitureRegistrationPageName(OMNITURE_PAGE_NAME);

                    //g.AnalyticsOmniture.Evar16 = idRegStep.GetOmnitureRegistrationScenario();
                }

                if (g.Brand.Site.SiteID == (int) WebConstants.SITE_ID.Cupid)
                {
                    g.SetMetaDescriptionWithTag(g.GetResource("META_DESCRIPTION", this));
                }
            }
        }
    }
}
