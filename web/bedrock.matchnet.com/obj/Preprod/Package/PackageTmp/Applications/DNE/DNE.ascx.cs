﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Web.Framework;
using Matchnet.ExternalMail.ServiceAdapters;
using Matchnet.ExternalMail.ValueObjects.DoNotEmail;
using Matchnet.Member.ServiceAdapters;
using System.Text.RegularExpressions;


namespace Matchnet.Web.Applications.DNE
{
    public partial class DNE : FrameworkControl
    {


        protected void Page_Load(object sender, EventArgs e)
        {
            txtDNEError.Style.Add("display", "none");
            txtDNESuccess.Style.Add("display", "none");
        }

        protected void btnContinue_OnClick(object sender, EventArgs e)
        {
            try
            {
                AddEmailAddress();
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        protected void btnReconsider_OnClick(object sender, EventArgs e)
        {
            Response.Redirect("http://www." + g.Brand.Site.Name);
        }

        public void AddEmailAddress()
        {
            string emailAddress = txtEmailAddress.Text;

            if (IsValidDNEmail(emailAddress))
            {
                DoNotEmailEntry entry = DoNotEmailSA.Instance.GetEntryBySiteAndEmailAddress(g.Brand.Site.SiteID,
                                                                                            emailAddress.ToLower());
                if (entry != null)
                {
                    txtDNEError.Style.Add("display", "block");
                    txtInvalidEmailAddress.ResourceConstant = "ALREADYONLIST";
                }
                else
                {

                    int memberID = MemberSA.Instance.GetMemberIDByEmail(emailAddress);

                    bool success = DoNotEmailSA.Instance.AddEmailAddress(g.Brand.BrandID, emailAddress.ToLower(), memberID);
                    if (success)
                    {
                       txtDNESuccess.Style.Add("display", "block");
                    }
                    else
                    {
                        txtFailure.Visible = true;
                    }
                }
            }
            else
            {
                txtDNEError.Style.Add("display", "block");
            }
        }


        private bool IsValidDNEmail(string email)
        {
            bool isValid = true;

            if (!String.IsNullOrEmpty(email) && email.Length > 3 )
            {
                if (!IsValidEmail(email))
                    isValid = false;
            }
            else
            {
                isValid = false;
            }

            return isValid;
        }


        private bool IsValidEmail(string email)
        {
            string theReg = @"^[\w!#$%&'*+\-/=?\^_`{|}~]+(\.[\w!#$%&'*+\-/=?\^_`{|}~]+)*"
                            + "@"
                            + @"((([\-\w]+\.)+[אבגדהוזחטיכלמנסעפצקרשתץףןםa-zA-Z]{2,4})|(([0-9]{1,3}\.){3}[0-9]{1,3}))$";

            Regex regex = new Regex(theReg);
            Match match = regex.Match(email);

            if (match.Success)
                return true;

            return false;
        }

    }
}