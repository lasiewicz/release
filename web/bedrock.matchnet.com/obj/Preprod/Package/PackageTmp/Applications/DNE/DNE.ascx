﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DNE.ascx.cs" Inherits="Matchnet.Web.Applications.DNE.DNE" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="cc1" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>


<div ID="txtDNEError" runat="server" Class="PageMessage notification" style="display: none;">
<img id="_ctl0__ctl4_imgIcon" border="0" src="/img/Community/JDate/page_message.gif"/>
<mn:Txt runat="server" id="txtInvalidEmailAddress" ResourceConstant="TXT_INVALID_EMAIL" /> 
</div>
<div ID="txtDNESuccess" runat="server" Class="PageMessage notification" style="display: none;">
    <mn:Txt ID="txtSuccess" runat="server" ResourceConstant="SUCCESS" />
</div>
<mn:Txt ID="txtFailure" runat="server" ResourceConstant="FAILURE" Visible="false"/>  
<div id="page-container">
    <mn:txt id="txtCopy" runat="server" ResourceConstant="DNECOPY" />
    <div class="rbox-wrapper-member" style="margin-top:10px;">
    <fieldset id="opt-out" class="rbox-style-high login-box member">
        <dl class="dl-form" style="padding:20px 20px 20px 20px">
            <dt></dt>
            <dd style="padding-bottom: 10px;">
                <strong><mn:txt id="txtEmailAddressText" runat="server" ResourceConstant="EMAILADDRESS" />:</strong>     
                <asp:TextBox ID="txtEmailAddress" runat="server" MaxLength="50"/>
            </dd>
            <dt></dt>
            <dd>
               <cc1:FrameworkButton ID="btnContinue" runat="server" ResourceConstant="CONTINUE" OnClick="btnContinue_OnClick" />
               <cc1:FrameworkButton ID="btnReconsider" runat="server" ResourceConstant="RECONSIDER" OnClick="btnReconsider_OnClick" />    
            </dd>
        </dl>
    </fieldset>
    
</div>

<script type="text/javascript">
    //validation

    $j(document).ready(function () {

        $j('#_ctl0__ctl4_btnContinue').unbind('click').click(function (event) {

            var theEmail = $j('#_ctl0__ctl4_txtEmailAddress').val();

            if (theEmail != "" && theEmail.length > 3 && IsValidDNEEmail(theEmail)) {
               $j('#Form1').submit();

            } else {
                $j('#_ctl0__ctl4_txtDNEError').show();
                return false;
            }
        });

    });

    //validates a good email address takes into account hebrew characters. 
    function IsValidDNEEmail(email) {
        var regex = /^([אבגדהוזחטיכלמנסעפצקרשתץףןםa-zA-Z0-9_\?\.\-\+])+\@(([אבגדהוזחטיכלמנסעפצקרשתץףןםa-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return regex.test(email);
    }

</script>
</div>




