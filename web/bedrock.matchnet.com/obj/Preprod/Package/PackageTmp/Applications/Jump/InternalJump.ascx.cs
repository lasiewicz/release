﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Web.Framework;
using Matchnet.Email.ValueObjects;
using Matchnet.Email.ServiceAdapters;
using Matchnet.ExternalMail.ServiceAdapters;

namespace Matchnet.Web.Applications.Jump
{
    public partial class InternalJump : FrameworkControl
    {
        private const string NO_THANKS_EMAIL_TARGET_MEMBERID = "ntetmid";
        private const string NO_THANKS_EMAIL_SUBJECT = "NO_THANKS_EMAIL_SUBJECT";
        private const string NO_THANKS_EMAIL_BODY = "NO_THANKS_EMAIL_BODY";
        private const string REPLY_TO_MESSAGELISTID = "rtmlid";
        private const string NO_THANKS_EMAIL_CONFIRMATION = "NO_THANKS_CONFIRMATION_MESSAGE";
        private const string NO_THANKS_EMAIL_FAILED = "NO_THANKS_FAILED_MESSAGE";

        protected void Page_Load(object sender, EventArgs e)
        {
            int targetMemberId = Constants.NULL_INT;
            int replyToMessageListId = Constants.NULL_INT;

            if (Request[NO_THANKS_EMAIL_TARGET_MEMBERID] != null)
            {
                targetMemberId = int.Parse(Request[NO_THANKS_EMAIL_TARGET_MEMBERID]);
            }

            if (Request[REPLY_TO_MESSAGELISTID] != null)
            {
                replyToMessageListId = int.Parse(Request[REPLY_TO_MESSAGELISTID]);
            }

            if (targetMemberId != Constants.NULL_INT)
            {
                //MessageSave messageSave = new MessageSave(g.Member.MemberID, targetMemberId, g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, MailType.Email,
                //    g.GetResource(NO_THANKS_EMAIL_SUBJECT, this), g.GetResource(NO_THANKS_EMAIL_BODY, this));

                //var result = EmailMessageSA.Instance.SendMessage(messageSave, false, replyToMessageListId, true);

                // set the message in session so the member sees this when they are taken to the home page
                //if (result.Status == MessageSendStatus.Success)
                //{
                    // send out external mail
                    int targetBrandId = Constants.NULL_INT;
                    var targetMember = Matchnet.Member.ServiceAdapters.MemberSA.Instance.GetMember(targetMemberId, Matchnet.Member.ServiceAdapters.MemberLoadFlags.None);
                    targetMember.GetLastLogonDate(g.Brand.Site.Community.CommunityID, out targetBrandId);

                    ExternalMailSA.Instance.SendAllAccessNoThanksEmail(g.Member.MemberID, g.Brand.BrandID, targetMemberId, targetBrandId);

                    g.Session.Add(WebConstants.SESSION_NOTIFICATION_MESSAGE, g.GetResource(NO_THANKS_EMAIL_CONFIRMATION, this), Matchnet.Session.ValueObjects.SessionPropertyLifetime.Temporary);
                //}
                //else
                //{
                //    g.Session.Add(WebConstants.SESSION_NOTIFICATION_MESSAGE, g.GetResource(NO_THANKS_EMAIL_FAILED, this), Matchnet.Session.ValueObjects.SessionPropertyLifetime.Temporary);
                //}
            }

            g.Transfer("/Applications/Email/MailBox.aspx?overlay=nothanks"); 
        }
    }
}