﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FavoritesClientTab.ascx.cs" Inherits="Matchnet.Web.Applications.Favorites.FavoritesClientTab" %>
<% if (IsFavoritesEnabled && IsLoggedIn) { %>
    <script type="text/javascript" src="/Applications/Favorites/favorites.js"></script>
    <script type="text/javascript">
        //<![CDATA[
        (function (){
            __addToNamespace__('spark.favorites', {
                MinPRTIdValue:<%=int.MinValue%>,
                BrandId:'<%=BrandId%>',
                MemberId:'<%=MemberId%>',
                BrowseMoreText:'<%=GetText("TXT_BROWSE_MORE",true,true)%>',
                getTimeInterval:function() {
                    return <%=TimeInterval%>;
                },
                __init__:function() {
                    spark.favorites.addConfig({
                        onlineFavs: {},
                        idleFavs: {},
                        offlineFavs: {}, 
                        StripClass:'vertstrip-favs',
                        ListId:'online-favs',
                        ListClass:'item',
                        ListOfflineClass:'offline',
                        ListOnlineClass:'online',                        
                        ListOnlineTitle:'<%=GetText("TXT_LI_ONLINE_TITLE",true,true)%>',
                        ListOfflineTitle:'<%=GetText("TXT_LI_OFFLINE_TITLE",true,true)%>',
                        HrefOnlineTitle:'<%=GetText("TXT_HREF_ONLINE_TITLE",true,true)%>',
                        HrefClass:'favslist onlineFavs',     
                        HrefOfflineClass:'favslist offlineFavs',                     
                        ClickId:'<%=ClickableElementId%>',
                        PrtId:787,                        
                        ViewMoreText:'<a id="viewMore<%=ClickableElementId%>" href="javascript:void(0);"><%=GetText("TXT_VIEW_MORE_FAVORITES",true,true)%></a>',
                        Carousel:function() {
                            jQuery(".vertstrip-favs").jCarouselLite({
                                btnNext: ".vertstrip-next-favs",
                                btnPrev: ".vertstrip-prev-favs",
                                circular: false,
                                mouseWheel: true,
                                scroll: 2,
                                visible: 3,
                                colNum:3,
                                vertical: true,
                                pageSize:9
                            });
                        }
                    });
                }
            });
        })();

        jQuery(document).ready(function() {
            jQuery("#<%=ClickableElementId%>").click(function(evt) {
                var config=spark.favorites.getConfig('ClickId','<%=ClickableElementId%>');
                if (!config.IsVisible){
                    var config=spark.favorites.getConfig('ClickId', '<%=ClickableElementId%>');
                    var pageSize=(config)?spark.favorites.totalFavs(config):0;
                    spark.favorites.loadFavoritesWithConfig(evt,((pageSize>50)?pageSize:50), true);
                    jQuery('#favsTab').show();
                    //omniture tracking
                    spark.tracking.addEvent("event44",true);
                    spark.tracking.addLinkTrackEvent("event44",true);
                    spark.tracking.addLinkTrackVar("events",true);
                    spark.tracking.trackLink({ firstParam: true, linkName: "Click to View Favorite-Favorite Tab" });
                    config.IsVisible=true;
               }
            });
            jQuery("body").click(function(evt) {
                if (jQuery('#favsTab').is(':hidden')) {
                    var config=spark.favorites.getConfig('ClickId','<%=ClickableElementId%>');
                    if(config.IsVisible) { delete config.IsVisible; }
                }                          
            });
            
            jQuery("#viewMore<%=ClickableElementId%>").live("click",function(evt){
                var offset=10;
                var config=spark.favorites.getConfig('ClickId','<%=ClickableElementId%>');
                spark.favorites.__loadFavoritesWithConfig__({currentTarget:{id:'<%=ClickableElementId%>'}},spark.favorites.totalFavs(config)+offset);
                jQuery('#favsTab').show();
                evt.stopPropagation();
            });            
                                    
            if("null"!="<%=OnlineIndicatorElementId%>" && ""!="<%=OnlineIndicatorElementId%>" && jQuery("#<%=OnlineIndicatorElementId%>").size()>0){
                spark.util.addItem(spark.favorites.onlineIndicatorIds,"<%=OnlineIndicatorElementId%>");
                //only create update interval once
                if(spark.favorites.onlineIndicatorIds.length==1) {
                    spark.favorites.loadOnlineIndicator();
                }
            }
            
            //omniture tracking
            jQuery("a.onlineFavs").live("click",function() {
                spark.tracking.addEvent("event45",true);
                spark.tracking.addLinkTrackEvent("event45",true);
                spark.tracking.addLinkTrackVar("events",true);
                spark.tracking.addLinkTrackVar("prop34");
                spark.tracking.addLinkTrackVar("prop35");
                spark.tracking.addProp(34, "home page module");
                spark.tracking.addProp(35, "Favorite");
                spark.tracking.trackLink({ firstParam: true, linkName: "Click to Initiate IM" });
            });
        });
        //]]>
    </script>
<div id="favsTab" style="display:none;" class="favs-online outer-wrapper">
    <div class="vertstrip-favs feed-wrapper">
	    <div class="vertstrip-favs-feed feed">
	    <ul id="online-favs"></ul>
	    </div>
	    <div class="vertstrip-favs-controls control">
		    <button class="vertstrip-prev-favs disabled">&#9650;</button>
		    <button class="vertstrip-next-favs">&#9660;</button>
	    </div>
    </div>
	<div class="edit-favs clear-both edit">
		<a href="/Applications/HotList/View.aspx?CategoryID=0" title="<%=GetText("TXT_EDIT_FAVORITIES",true,false)%>"><%=GetText("TXT_EDIT_FAVORITIES",true,false)%></a> | 
		<a id="hide-nav-lnk" href="/Applications/MemberServices/DisplaySettings.aspx?NavPoint=sub" title="<%=GetText("TXT_HIDE_MASK",true,false).Replace("%s",GetStatus())%>"><%=GetText("TXT_HIDE_MASK",true,false).Replace("%s",GetStatus())%></a>
	</div>
</div>	       	    
<% } %>
