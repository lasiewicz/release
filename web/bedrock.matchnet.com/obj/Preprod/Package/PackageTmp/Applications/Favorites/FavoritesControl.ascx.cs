﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Script.Serialization;
using System.IO;
using System.Net;

using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Member.ValueObjects.Interfaces;
using Matchnet.Member.ValueObjects.MemberDTO;
using Matchnet.Member.ValueObjects.Photos;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Managers;
using Matchnet.Web.Framework.Ui.BasicElements;
using Matchnet.Web.Applications.API;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Web.Framework.Ui;
using Matchnet.List.ServiceAdapters;
using Matchnet.List.ValueObjects;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.CachingTemp;
using Matchnet.List.ValueObjects;
using Matchnet.Web.Applications.UserNotifications;
using Matchnet.UserNotifications.ValueObjects;
using Matchnet.UserNotifications.ServiceAdapters;
using Matchnet.Web.Analytics;

namespace Matchnet.Web.Applications.Favorites
{
    public enum FriendStatus
    {
        Online,
        Offline,
        Idle
    }

    public partial class FavoritesControl : FrameworkControl
    {
        public static string FAVORITE_SERVICE_URL = "http://{0}/Applications/API/Favorites.asmx/GetFavorites";
        private static object _lock = new object();
        private static ICaching _cache;

        public List<FavoriteMember> Favorites { get; set; }

        private ICaching GetCache()
        {
            if (null == _cache)
            {
                lock (_lock)
                {
                    if (null == _cache)
                    {
                        bool useMembase = Convert.ToBoolean(RuntimeSettings.GetSetting(WebConstants.MEMBASE_SETTING_CONSTANT));
                        bool useMembaseForWeb = Convert.ToBoolean(RuntimeSettings.GetSetting(WebConstants.MEMBASE_WEB_SETTING_CONSTANT));
                        if (useMembase && useMembaseForWeb)
                        {
                            MembaseConfig membaseConfig =
                                RuntimeSettings.GetMembaseConfigByBucket(WebConstants.MEMBASE_WEB_BUCKET_NAME);
                            _cache = Spark.Caching.MembaseCaching.GetSingleton(membaseConfig);
                        }
                        else
                            _cache = CachingTemp.Cache.GetInstance();
                    }
                }
            }
            return _cache;
        }

        public bool IsFavoritesEnabled
        {
            get { return FavoritesControl.IsFavoritesOnlineEnabled(g); }
        }

        public bool IsLoggedIn
        {
            get { return null != g.Member && g.Member.MemberID > 0; }
        }

        public bool IsVisible
        {
            get
            {
                bool isVisible = true;
                if (IsLoggedIn)
                {
                    if ((g.Member.GetAttributeInt(g.Brand, "HideMask") & (Int32)WebConstants.AttributeOptionHideMask.HideMembersOnline) == (Int32)WebConstants.AttributeOptionHideMask.HideMembersOnline)
                    {
                        isVisible = false;
                    }
                }
                return isVisible;
            }
        }

        public string ClickableElementId { get; set; }

        public string OnlineIndicatorElementId { get; set; }

        public string OmnitureVar { get; set; }

        public int BrandId
        {
            get { return g.Brand.BrandID; }
        }

        public string Direction
        {
            get
            {
                return g.Brand.Site.Direction.ToString();
            }
        }

        public int MemberId
        {
            get { return g.Member.MemberID; }
        }

        public int TimeInterval
        {
            get
            {
                int interval = SettingsManager.GetSettingInt(SettingConstants.FAVORITES_ONLINE_TIME_INTERVAL, g.Brand);
                return interval;
            }
        }

        public int CacheTimeToLive
        {
            get
            {
                int interval = SettingsManager.GetSettingInt(SettingConstants.FAVORITES_CACHE_TTL_SECONDS, g.Brand);
                return interval;
            }
        }

        public static bool IsFavoritesOnlineEnabled(ContextGlobal _g)
        {
            SettingsManager settingsManager= new SettingsManager();
            return settingsManager.GetSettingBool(SettingConstants.ENABLE_FAVORITES_ONLINE, _g.Brand);
        }

        public string GetStatus()
        {
            return (IsVisible) ? GetText("TXT_ONLINE_STATUS", true, false) : GetText("TXT_OFFLINE_STATUS", true, false);
        }

        public string GetText(string key, bool useParent, bool javascriptEncode)
        {
            string resText = g.GetResource(key, ((useParent) ? this.LoadControl("FavoritesControl.ascx") : this));
            if (javascriptEncode)
            {
                resText = EncodeForJS(resText);
            }
            return resText;
        }

        public string EncodeForJS(string s)
        {
            return FrameworkGlobals.JavaScriptEncode(s);
        }

        public virtual Dictionary<string, Favorites.FavoriteMember> GetFavorites(Brand brand, int memberID, int startIndex, int pageSize, int prtId, string omniturePageName = "" )
        {
            if (null == brand)
            {
                brand = g.Brand;
            }

            //MemberPhotoHelper photoHelper = new MemberPhotoHelper(RuntimeSettings.Instance);
            int communityID = brand.Site.Community.CommunityID;
            int siteID = brand.Site.SiteID;
            Dictionary<string, FavoriteMember> favorites = null;
            FavoritesCachedItem favoritesCachedItem = GetCache().Get(FavoritesCachedItem.GetCacheKeyString(siteID, communityID, memberID)) as FavoritesCachedItem;
            if (favoritesCachedItem == null)
            {

                Matchnet.Member.ServiceAdapters.Member member = (null != g.Member && g.Member.MemberID == memberID)
                    ? g.Member
                    : MemberSA.Instance.GetMember(memberID, MemberLoadFlags.None);
                Matchnet.List.ServiceAdapters.List list = (null != g.Member && g.Member.MemberID == memberID &&
                                                           null != g.List)
                    ? g.List
                    : ListSA.Instance.GetList(memberID);

                Int32 temp = 0;
                System.Collections.ArrayList favs = list.GetListMembers(HotListCategory.Default, communityID, siteID,
                    startIndex, pageSize, out temp);
                favorites = new Dictionary<string, FavoriteMember>();
                System.Collections.ArrayList members = null;
                if (favs != null && favs.Count > 0)
                {
                    //load dtos
                    members = MemberDTOManager.Instance.GetIMemberDTOs(g, favs, MemberType.Search, MemberLoadFlags.None);


                    int favOrdinal = 0;
                    foreach (IMemberDTO targetMember in members)
                    {
                        favOrdinal++;
                        int targetMemberID = targetMember.MemberID;
                        bool isOnline = false;
                        FavoriteMember fm = new FavoriteMember();
                        OnlineLinkHelper lnkHelper =
                            OnlineLinkHelper.getOnlineLinkHelper(
                                Matchnet.Web.Framework.Ui.BasicElements.ResultContextType.None,
                                targetMember, member, Matchnet.Content.ServiceAdapters.Links.LinkParent.FavoritsOnline,
                                prtId, out isOnline);
                        fm.IMLink = lnkHelper.OnlineLink;

                        //Generate offline link to View Profile:
                        BreadCrumbHelper.EntryPoint myEntryPoint = BreadCrumbHelper.EntryPoint.HotLists;
                        HotListCategory hotListCategory = HotListCategory.Default;
                        fm.ProfileLink = BreadCrumbHelper.MakeViewProfileLink(myEntryPoint, targetMemberID, favOrdinal,
                            null, (int) hotListCategory);
                        string myOmnitureLinkTrackingParams =
                            Omniture.GetActionURLParams(WebConstants.PageIDs.ViewProfile,
                                WebConstants.Action.ProfileName, omniturePageName, "Offline Favorites List");
                        fm.ProfileLink += "&" + myOmnitureLinkTrackingParams;

                        fm.MemberId = targetMemberID;
                        string username = targetMember.GetUserName(brand);
                        fm.Name = (String.IsNullOrEmpty(username)) ? targetMemberID + "" : username;
                        //TODO: Figure out how to provide IDLE status
                        fm.Status = (isOnline) ? FriendStatus.Online.ToString() : FriendStatus.Offline.ToString();

                        if (targetMember.HasApprovedPhoto(communityID))
                        {
                            Photo photo = MemberPhotoDisplayManager.Instance.GetDefaultPhoto(g.Member, targetMember,
                                g.Brand);
                            fm.Thumbnail = MemberPhotoDisplayManager.Instance.GetPhotoDisplayURL(g.Member, targetMember,
                                g.Brand, photo,
                                PhotoType.Thumbnail,
                                PrivatePhotoImageType.TinyThumb,
                                NoPhotoImageType.BackgroundTinyThumb);
                        }
                        else
                        {
                            bool isMale = FrameworkGlobals.IsMaleGender(targetMemberID, brand);
                            fm.Thumbnail =
                                MemberPhotoDisplayManager.Instance.GetNoPhotoFile(NoPhotoImageType.BackgroundTinyThumb,
                                    isMale);
                        }

                        favorites[fm.MemberId + ""] = fm;
                    }
                    favoritesCachedItem = new FavoritesCachedItem();
                    favoritesCachedItem.Favorites = favorites;
                    favoritesCachedItem.CacheTTLSeconds = CacheTimeToLive;
                    favoritesCachedItem.MemberID = memberID;
                    favoritesCachedItem.CommunityID = communityID;
                    favoritesCachedItem.SiteID = siteID;
                    GetCache().Add(favoritesCachedItem);
                }
            }
            else
            {
                favorites = favoritesCachedItem.Favorites;
            }
            return favorites;
        }

        public int GetTotalFavoritesNumber(Brand brand, int memberID)
        {
            if (null == brand)
            {
                brand = g.Brand;
            }
            int communityID = brand.Site.Community.CommunityID;
            int siteID = brand.Site.SiteID;
            int totalFavorites = 0;
            TotalFavoritesNumberCachedItem cachedItem = GetCache().Get(TotalFavoritesNumberCachedItem.GetCacheKeyString(siteID, communityID, memberID)) as TotalFavoritesNumberCachedItem;
            if (null == cachedItem)
            {
                Matchnet.List.ServiceAdapters.List list = (null != g.Member && g.Member.MemberID == memberID && null != g.List) ? g.List : ListSA.Instance.GetList(memberID);
                totalFavorites = list.GetCount(HotListCategory.Default, communityID, siteID);
                cachedItem = new TotalFavoritesNumberCachedItem();
                cachedItem.TotalFavoritesNumber = totalFavorites;
                cachedItem.CacheTTLSeconds = CacheTimeToLive;
                cachedItem.MemberID = memberID;
                cachedItem.CommunityID = communityID;
                cachedItem.SiteID = siteID;
                GetCache().Add(cachedItem);
            }
            else
            {
                totalFavorites = cachedItem.TotalFavoritesNumber;
            }
            return totalFavorites;
        }

        public int GetTotalFavoritesOnlineNumber(Brand brand, int memberID, string omniturePageName)
        {
            if (null == brand)
            {
                brand = g.Brand;
            }
            int communityID = brand.Site.Community.CommunityID;
            int siteID = brand.Site.SiteID;
            int numOnline = 0;
            int totalFavorites = GetTotalFavoritesNumber(brand, memberID);
            System.Collections.Generic.Dictionary<string, Favorites.FavoriteMember> favorites = GetFavorites(brand, memberID, 1, totalFavorites, int.MinValue, omniturePageName);
            foreach (FavoriteMember fm in favorites.Values)
            {
                if (FriendStatus.Online.ToString().Equals(fm.Status))
                {
                    numOnline++;
                }
            }
            return numOnline;
        }

        public virtual void LoadFavoritesOnline()
        {
            // create a client object
            using (System.Net.WebClient client = new System.Net.WebClient())
            {
                string xml = string.Format("brandID={0}&memberID={1}", _g.Brand.BrandID, _g.Member.MemberID);
                //string xml = string.Format("brandID={0}&memberID={1}", _g.Brand.BrandID, "100004510");

                // performs an HTTP POST
                string url = string.Format(FAVORITE_SERVICE_URL, Request.ServerVariables.Get("HTTP_HOST"));
                String response = string.Empty;
                try
                {
                    Stream stream = client.OpenRead(url + "?" + xml);
                    StreamReader reader = new StreamReader(stream);
                    response = reader.ReadToEnd();
                }
                catch (WebException ex)
                {
                    if (ex.Response is HttpWebResponse)
                    {
                        switch (((HttpWebResponse)ex.Response).StatusCode)
                        {
                            case HttpStatusCode.NotFound:
                                response = null;
                                break;

                            default:
                                throw ex;
                        }
                    }
                }

                string str = "<string xmlns=\"http://spark.net/API\">";
                int startIdx = response.IndexOf(str) + str.Length;
                int jsonLength = response.IndexOf("</string>") - startIdx;
                string jsonString = response.Substring(startIdx, jsonLength);
                JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
                Favorites = jsSerializer.Deserialize<List<FavoriteMember>>(jsonString);
            }
        }

        public string ProcessFavoriteOperation(Brand brand, int favoriteMemberId)
        {
            return ProcessFavoriteOperation(brand, favoriteMemberId, true, false);
        }

        public string ProcessFavoriteOperation(Brand brand, int favoriteMemberId, bool removeIfExists, bool updateUserNotification)
        {
            string result = string.Empty;
            try
            {
                if (null == brand)
                {
                    brand = g.Brand;
                }
                int communityID = brand.Site.Community.CommunityID;
                int siteID = brand.Site.SiteID;
                if (null != g.Member && g.Member.MemberID > 0 && favoriteMemberId > 0 && g.Member.MemberID != favoriteMemberId)
                {
                    if (g.List.IsHotListed(HotListCategory.Default, communityID, favoriteMemberId))
                    {
                        if (removeIfExists)
                        {
                            ListSA.Instance.RemoveListMember(HotListCategory.Default, communityID, g.Member.MemberID, favoriteMemberId);
                            result = "del";
                        }
                        else
                        {
                            result = "add";
                        }
                    }
                    else
                    {
                        ListSaveResult lsr=ListSA.Instance.AddListMember(HotListCategory.Default,
                            communityID,
                            siteID,
                            g.Member.MemberID,
                            favoriteMemberId,
                            string.Empty,
                            Constants.NULL_INT,
                            !((g.Member.GetAttributeInt(g.Brand, "HideMask", 0) & (Int32)WebConstants.AttributeOptionHideMask.HideHotLists) == (Int32)WebConstants.AttributeOptionHideMask.HideHotLists),
                            false);
                        if (lsr.Status == ListActionStatus.Success)
                        {
                            result = "add";

                            if (updateUserNotification && UserNotificationFactory.IsUserNotificationsEnabled(g))
                            {
                                UserNotificationParams unParams = UserNotificationParams.GetParamsObject(favoriteMemberId, g.Brand.Site.SiteID, g.Brand.Site.Community.CommunityID);
                                UserNotification notification = UserNotificationFactory.Instance.GetUserNotification(new AddedYouAsFavoriteUserNotification(), favoriteMemberId, g.Member.MemberID, g);
                                if (notification.IsActivated())
                                {
                                    UserNotificationsServiceSA.Instance.AddUserNotification(unParams, notification.CreateViewObject());
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
            return result;
        }
    }
    
    [Serializable]
    public class FavoriteMember
    {
        public int MemberId{get; set;}
        public string Name{get; set;}
        public string Status { get; set; }
        public string Thumbnail { get; set; }
        public string IMLink { get; set; }
        public string ProfileLink { get; set; }
    }

    [Serializable]
    public class TotalFavoritesNumberCachedItem : ICacheable
    {
        int _TotalFavoritesNumber = 0;
        int _MemberID = Constants.NULL_INT;
        int _SiteID = Constants.NULL_INT;
        int _CommunityID = Constants.NULL_INT;
        #region Properties
        public int TotalFavoritesNumber
        {
            get { return _TotalFavoritesNumber; }
            set { _TotalFavoritesNumber = value; }
        }

        public int MemberID
        {
            get { return _MemberID; }
            set { _MemberID = value; }
        }

        public int SiteID
        {
            get { return _SiteID; }
            set { _SiteID = value; }
        }

        public int CommunityID
        {
            get { return _CommunityID; }
            set { _CommunityID = value; }
        }
        #endregion


        #region ICacheable Members
        private int _cacheTTLSeconds = 540;
        private Matchnet.CacheItemPriorityLevel _cachePriority = Matchnet.CacheItemPriorityLevel.Normal;
        private Matchnet.CacheItemMode _cacheItemMode = Matchnet.CacheItemMode.Absolute;
        const string CACHEKEYFORMAT = "TotalFavorites-{0}-{1}-{2}";

        public CacheItemMode CacheMode
        {
            get { return _cacheItemMode; }
            set { _cacheItemMode = value; }
        }

        public CacheItemPriorityLevel CachePriority
        {
            get { return _cachePriority; }
            set { _cachePriority = value; }
        }

        public int CacheTTLSeconds
        {
            get { return _cacheTTLSeconds; }
            set { _cacheTTLSeconds = value; }
        }

        public string GetCacheKey()
        {
            return GetCacheKeyString(_SiteID, _CommunityID, _MemberID);
        }

        public static string GetCacheKeyString(int siteID, int community, int memberid)
        {
            return String.Format(CACHEKEYFORMAT, siteID, community, memberid);
        }
        #endregion
    }
    
    [Serializable]
    public class FavoritesCachedItem : ICacheable
    {
        Dictionary<string, FavoriteMember> _Favorites = new Dictionary<string, FavoriteMember>();
        int _MemberID = Constants.NULL_INT;
        int _SiteID = Constants.NULL_INT;
        int _CommunityID = Constants.NULL_INT;

        #region Properties
        public Dictionary<string,FavoriteMember> Favorites
        {
            get { return _Favorites; }
            set { _Favorites = value; }
        }

        public int MemberID
        {
            get { return _MemberID; }
            set { _MemberID = value; }
        }

        public int SiteID
        {
            get { return _SiteID; }
            set { _SiteID = value; }
        }

        public int CommunityID
        {
            get { return _CommunityID; }
            set { _CommunityID = value; }
        }
        #endregion


        #region ICacheable Members
        private int _cacheTTLSeconds = 540;
        private Matchnet.CacheItemPriorityLevel _cachePriority = Matchnet.CacheItemPriorityLevel.Normal;
        private Matchnet.CacheItemMode _cacheItemMode = Matchnet.CacheItemMode.Absolute;
        const string CACHEKEYFORMAT = "Favorites-{0}-{1}-{2}";

        public CacheItemMode CacheMode
        {
            get { return _cacheItemMode; }
            set { _cacheItemMode = value; }
        }

        public CacheItemPriorityLevel CachePriority
        {
            get { return _cachePriority; }
            set { _cachePriority = value; }
        }

        public int CacheTTLSeconds
        {
            get { return _cacheTTLSeconds; }
            set { _cacheTTLSeconds = value; }
        }

        public string GetCacheKey()
        {
            return GetCacheKeyString(_SiteID, _CommunityID, _MemberID);
        }

        public static string GetCacheKeyString(int siteID, int community, int memberid)
        {
            return String.Format(CACHEKEYFORMAT, siteID, community, memberid);
        }

        #endregion

    }
}
