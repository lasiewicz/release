﻿__addToNamespace__('spark.favorites', {
    __loadOnlineIndicator__: function () {
        var omniturePageName = '';
        if (s != null) {
            omniturePageName = s.pageName;
        }
        jQuery.ajax({
            type: "POST",
            url: "/Applications/API/Favorites.asmx/GetFavoritesOnlineNumber",
            data: "{brandID:" + spark.favorites.BrandId + "," +
                "memberID:" + spark.favorites.MemberId + "," +
                "omniturePageName:'" + omniturePageName + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            timeout:10000,
            success: function(msg, status, onlineIndicatorId) {
                //TODO:Upgrade jQuery so we can use parseJSON
                //eval the jason response
                var favsOnline = eval("(" + msg.d + ")");
                jQuery.each(spark.favorites.onlineIndicatorIds, function(key, value) {
                    var onlineElement = jQuery("#" + value);
                    if (onlineElement.length > 0) {
                        var spans = onlineElement.children("a").children(".s-icon-favorites");
                        if (spans.length > 0) {
                            spans.remove();
                        }
                        if (favsOnline.favsOnline != "0") {
                            onlineElement.children("a").prepend('<span class="spr s-icon-favorites spr-superscript"><span>' + favsOnline.favsOnline + '</span></span>');
                        } else {
                            onlineElement.children("a").prepend('<span class="spr s-icon-favorites"><span></span></span>');
                        }
                    }
                });
                window.setTimeout("if (!isOnlineTimeLimitReached) spark.favorites.__loadOnlineIndicator__()", spark.favorites.getTimeInterval()); //update every 60 seconds                
            }
        });
    },
    loadOnlineIndicator: function() {
        spark.favorites.__loadOnlineIndicator__();
    },
    __loadFavoritesWithConfig__: function(evt, pageSize, startIdx, realtime) {
        var startIndex = (startIdx && startIdx > 0) ? startIdx : 1;
        var config = spark.favorites.getConfig('ClickId', evt.currentTarget.id);
        var prtId = (null != config && config.PrtId) ? config.PrtId : spark.favorites.MinPRTIdValue;
        var omniturePageName = '';
        if (s != null) {
            omniturePageName = s.pageName;
        }
        jQuery.ajax({
            type: "POST",
            url: "/Applications/API/Favorites.asmx/GetFavorites",
            data: "{brandID:" + spark.favorites.BrandId + "," +
            "memberID:" + spark.favorites.MemberId + "," +
            "startIndex:" + startIndex + "," +
            "pageSize:" + pageSize + "," +
            "prtId:" + prtId + "," +
            "omniturePageName:'" + omniturePageName + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            timeout: 10000,
            success: function(msg, status) {
                spark.favorites.parseFavorites(msg, status, config);
                if (realtime) {
                    window.setTimeout("if (!isOnlineTimeLimitReached) spark.favorites.__loadFavoritesWithConfig__({currentTarget:{id:\"" + evt.currentTarget.id + "\"}},spark.favorites.totalFavs(spark.favorites.getConfig(\"ClickId\",\"" + evt.currentTarget.id + "\")),null,"+realtime+")", spark.favorites.getTimeInterval()); //update every 60 seconds
                }
            }
        });
    },
    parseFavorites: function(msg, status, config) {
        //TODO:Upgrade jQuery so we can use parseJSON
        //eval the jason response
        var favs = eval("(" + msg.d + ")");
        var currentTotal = spark.util.mapSize(favs[0]);
        var total = favs[1];
        var showViewMore = (total > currentTotal || total == 0);
        //separate json into maps by status
        jQuery.each(favs[0], function(key, value) {
            value["added"] = true;
            if (value.Status == 'Online') {
                config.onlineFavs[key] = value;
            } else if (value.Status == 'Offline') {
                config.offlineFavs[key] = value;
            } else if (value.status == 'Idle') {
                config.idleFavs[key] = value;
            }
        });
        //sort maps by first name
        config.onlineFavs = spark.util.mapSort(config.onlineFavs, 'Name');
        config.idleFavs = spark.util.mapSort(config.idleFavs, 'Name');
        config.offlineFavs = spark.util.mapSort(config.offlineFavs, 'Name');

        //this function will write the html for each favorite
        config.innerText = '';
        jQuery.each(config.onlineFavs, function(k, v) {
            spark.favorites.makeList(k, v, config.onlineFavs, config);
        });
        jQuery.each(config.idleFavs, function(k, v) {
            spark.favorites.makeList(k, v, config.idleFavs, config);
        });
        jQuery.each(config.offlineFavs, function(k, v) {
            spark.favorites.makeList(k, v, config.offlineFavs, config);
        });

        var lastItemInnerText = '<li id="' + config.ListId + '-lastli" class="item view-more">';
        lastItemInnerText += (spark.favorites.totalFavs(config) > 0) ? config.ViewMoreText : spark.favorites.BrowseMoreText;
        lastItemInnerText += '</li>';
        if (showViewMore) { config.innerText += lastItemInnerText; }
        var list = jQuery("#" + config.ListId);
        list.empty().append(config.innerText);
        delete config['innerText'];

        if (spark.favorites.totalFavs(config) > 0 && config.Carousel) {
            config.Carousel();
        } else {
            list.parent().css("visibility", "visible");
        }
    },
    makeList: function(key, val, myFavs, config) {
        var favorite = val;
        if (!favorite.added) {
            delete myFavs[favorite.MemberId];
        } else {
            var innerText = '';
            var isOffline = (favorite.Status == 'Offline');
            innerText += '<li id="' + config.ListId + favorite.MemberId + '"';
            innerText += ' class="' + config.ListClass;
            innerText += ((favorite.Status == 'Online') ? ' ' + config.ListOnlineClass : ' ' + config.ListOfflineClass) + '"';
            if (config.ListStyle) {
                innerText += ' style="' + config.ListStyle + '"';
            }
            innerText += ' title="' + ((favorite.Status == 'Online') ? config.ListOnlineTitle : config.ListOfflineTitle) + '">';
            if (!isOffline) {
                innerText += '<a class="' + config.HrefClass;
                innerText += '" href="' + favorite.IMLink + '" title="' + config.HrefOnlineTitle + '">';
            }
            else {
                innerText += '<a class="' + config.HrefOfflineClass;
                //Currently using the ListOfflineTitle text which is "User is not online".  Could create a new 
                //config.HrefOfflineTitle like "View My Profile or "View Profile"
                innerText += '" href="' + favorite.ProfileLink + '" title="' + config.ListOfflineTitle + '">';
//                innerText += '" href="' + favorite.ProfileLink + '" title="' + config.HrefOfflineTitle + '">';
            }

            innerText += '<img class="thumbfav" src="' + ((favorite.Thumbnail && favorite.Thumbnail.length > 0) ? favorite.Thumbnail : "/img/noPhoto_small.gif") + '" width="22" height="29" alt="' + favorite.Name + '" />';
            innerText += '<span>' + spark.util.ellipsis(favorite.Name, 16) + '</span>';

            innerText += '</a>';

            innerText += '</li>';
            config.innerText += innerText;
            delete favorite["added"];
        }
    },
    loadFavoritesWithConfig: function(evt, pageSize, realtime) {
        spark.favorites.__loadFavoritesWithConfig__(evt, pageSize, null, realtime);
    },
    getTimeInterval: function() { return 60000; },
    __init__: function() {
        jQuery(".thumbFav").live("error", function() {
            jQuery(this).attr("src", "/img/noPhoto_small.gif");
        });
    }
});
