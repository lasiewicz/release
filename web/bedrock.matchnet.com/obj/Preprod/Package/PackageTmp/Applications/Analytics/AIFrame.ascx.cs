using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
	
using System.Text;
using System.Web.UI;

using Matchnet.Web.Framework;

namespace Matchnet.Web.Applications.Analytics
{
	/// <summary>
	///		Summary description for _Default.
	/// </summary>
	public class AIFrame : FrameworkControl
	{
		void RenderSageInclude(HtmlTextWriter htw)
		{
			//htw.Write("<script language=\"JavaScript1.1\" src=\"//st.sageanalyst.net/tag-744.js\"></script>\n");
			htw.WriteLine("<script language=\"JavaScript1.1\">var st_iu=st_rs=st_cd=st_rf=st_us=st_hn=st_qs=st_ce=\"\";");
			htw.WriteLine("var d=document;var l=location;var n=navigator;");
			htw.WriteLine("st_ce=n.cookieEnabled;st_qs=escape(l.search.substring(1));st_us=escape(l.pathname);st_hn=l.host;st_rf=escape(d.referrer);");
			htw.WriteLine("if(st_v==1.2){var s=screen;st_rs=escape(s.width+\"x\"+s.height);st_cd=s.colorDepth;}");
			htw.WriteLine("proto=\"http:\";if(location.protocol==\"https:\"){proto=\"https:\";}");
			htw.WriteLine("st_iu=proto+\"//\"+st_dd+\"/\" + (new Date()).getTime() + \"/JS?ci=\"+st_ci+\"&di=\"+st_di+\"&pg=\"+st_pg+\"&us=\"+st_us+\"&qs=\"");
			htw.WriteLine(" +st_qs+\"&rf=\"+st_rf+\"&rs=\"+st_rs+\"&cd=\"+st_cd+\"&hn=\"+st_hn+\"&ce=\"+st_ce+\"&jv=\"+st_v+\"&tai=\"+st_tai+\"&ai=\"+st_ai;");
			htw.WriteLine("var iXz=new Image();iXz.src=st_iu;");
			htw.WriteLine("</script>");
		}

		protected override void Render(HtmlTextWriter htw)
		{
			string query = Request.Url.Query.Substring(1);
			string ai64 = Request["ai"];
			if (ai64 != null)
			{
				ai64.Replace('-','+');
				ai64.Replace('_','/');
			}
			else
			{
				ai64 = string.Empty;
			}
			string ai = Encoding.ASCII.GetString(Convert.FromBase64String(ai64));
			char[] semi = { ';' };
			string[] aiArgs = ai.Split(semi);	
			if (aiArgs.Length == 2 && aiArgs[0] != "") 
			{
				string diCode = aiArgs[0];
				string aiParams = aiArgs[1];
				string aiEncoded = HttpUtility.UrlEncode(aiParams);
				htw.Write("<html><body>");
				//	TOFIX - What is the new equivalent of Development in PrivateLabel? - dcornell
				if (g.IsDevMode)
				{
					htw.Write("<!--{0}-->", HttpUtility.HtmlEncode(query));
					htw.Write(aiParams);
				}

			//	htw.Write("\n<!--BEGIN-SMB-1.2.3.744."); htw.Write(diCode); htw.Write("-->\n");
				//htw.Write("<script language=\"JavaScript\">var st_pg=\"\"; var st_ai=\""); htw.Write(aiParams);
				//htw.Write("\"; var st_v=1.0; var st_ci=\"744\"; var st_di=\""); htw.Write(diCode);
				//htw.Write("\"; var st_dd=\"st.sageanalyst.net\"; var st_tai=\"v:1.2.3\";</script>\n");
				//htw.Write("<script language=\"JavaScript1.1\">st_v=1.1;</script><script language=\"JavaScript1.2\">st_v=1.2;</script>\n");

				//RenderSageInclude(htw);

				//htw.Write("<script language=\"JavaScript\">if (st_v==1.0) { var st_dn=(new Date()).getTime(); var st_rf=escape(document.referrer);\n");
				//htw.Write(" var st_uj=\"//\"+st_dd+\"/\"+st_dn+\"/JS?ci=\"+st_ci+\"&di=\"+st_di+\"&pg=\"+st_pg+\"&rf=\"+st_rf+\"&jv=\"+st_v+\"&tai=\"+st_tai+\"&ai=\"+st_ai;\n");
			//	htw.Write(" var iXz=new Image(); iXz.src=st_uj;\n"); // add comment in javascript code here to disable image fetch
				//htw.Write("}</script><noscript><img src=\"//st.sageanalyst.net/NS?ci=744&di="); htw.Write(diCode);
			//	htw.Write("&pg=&ai="); htw.Write(aiEncoded); htw.Write("\"></noscript>\n");
				//htw.Write("<!--END-SMB-1.2.3.744."); htw.Write(diCode); htw.Write("-->\n");
				
				htw.Write("</body></html>");
			}
		}
		
		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
	}
}
