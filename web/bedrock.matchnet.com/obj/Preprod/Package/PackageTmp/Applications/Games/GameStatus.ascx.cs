﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Session.ValueObjects;
using Matchnet.Web.Framework;

namespace Matchnet.Web.Applications.Games
{
    public partial class GameStatus : FrameworkControl
    {
        private const string GAME_CACHE_KEY = "GAME_";
        private string GetGameCacheKey(string gameKey)
        {
            return GAME_CACHE_KEY + gameKey;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            if (!string.IsNullOrEmpty(Request["invitekey"]) && !string.IsNullOrEmpty(Request["action"]))
            {
                if (Request["action"].ToLower() == "reject")
                {
                    GamesHelper.RejectInvite(g.Member.MemberID, Request["invitekey"], g.Brand.Site.Community.CommunityID);
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(Request["inviteeMemberID"]) && !string.IsNullOrEmpty(Request["invitekey"]))
                {
                    if (string.IsNullOrEmpty(g.Session[GetGameCacheKey(Request["invitekey"])]))
                    {
                        // Add invitation to cache in order to check how many seconds have passed
                        g.Session.Add(GetGameCacheKey(Request["invitekey"]), "0", 1, false);
                    }

                    int inviteeMemberID = 0;
                    if (int.TryParse(Request["inviteeMemberID"], out inviteeMemberID))
                    {
                        int secondsWaited = int.Parse(g.Session[GetGameCacheKey(Request["invitekey"])]);
                        if (secondsWaited >= 120)
                        {
                            Response.Write(FrameworkGlobals.GetHomepageAbsURL(g, Request.ServerVariables.Get("HTTP_HOST")) + "/Applications/Games/Game.aspx?DestinationMemberID=" + inviteeMemberID.ToString() +
                                    "&InviteKey=" + Request["invitekey"] + "&Status=NoAnswer");
                        }
                        else
                        {
                            secondsWaited += 5;
                            g.Session.Remove(GetGameCacheKey(Request["invitekey"]));
                            g.Session.Add(GetGameCacheKey(Request["invitekey"]), secondsWaited.ToString(), 1, false);

                            if (GamesHelper.CheckInviteAccepted(inviteeMemberID, g.Brand.Site.Community.CommunityID, Request["invitekey"]))
                            {
                                Response.Write(FrameworkGlobals.GetHomepageAbsURL(g, Request.ServerVariables.Get("HTTP_HOST")) + "/Applications/Games/Game.aspx?DestinationMemberID=" + inviteeMemberID.ToString() +
                                           "&InviteKey=" + Request["invitekey"] + "&Status=Accepted");
                            }
                            else
                            {
                                if (GamesHelper.CheckInviteRejected(inviteeMemberID, g.Brand.Site.Community.CommunityID, Request["invitekey"]))
                                {
                                    Response.Write(FrameworkGlobals.GetHomepageAbsURL(g, Request.ServerVariables.Get("HTTP_HOST")) + "/Applications/Games/Game.aspx?DestinationMemberID=" + inviteeMemberID.ToString() +
                                    "&InviteKey=" + Request["invitekey"] + "&Status=Rejected");
                                }
                                else
                                {
                                    Response.Write("nochange");
                                }
                            }
                        }
                    }
                }
            }
            Response.End();
        }
    }
}