﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.List.ServiceAdapters;
using Matchnet.List.ValueObjects;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Web.Framework;
using Matchnet.Games.ServiceAdapters;
using Matchnet.Games.ValueObjects;

namespace Matchnet.Web.Applications.Games
{
    public partial class Game : FrameworkControl
    {
        #region Fields (2)

        private GameStep _currentStep;
        private const int NUMBER_OF_GAMES_PER_ROW = 3;
        private const string WAITING_JS_KEY = "WAITING";
        private bool _isInvitee = false;
        private string _InviteKey = string.Empty;

        #endregion Fields

        #region Enums (1)

        private enum GameStep
        {
            ChooseGame = 1,
            WaitingForPlayer = 2,
            Game = 3,
            InvitationDeclined = 4,
            Error = 5
        }

        #endregion Enums

        #region Methods (11)

        // Protected Methods (5) 

        protected string CloseTR(int itemIndex)
        {
            string result = string.Empty;
            if ((itemIndex + 1) % NUMBER_OF_GAMES_PER_ROW == 0)
            {
                result = "</tr>";
            }
            return result;
        }

        protected string OpenTR(int itemIndex)
        {
            string result = string.Empty;
            if (itemIndex % NUMBER_OF_GAMES_PER_ROW == 0)
            {
                result = "<tr>";
            }
            return result;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ShowCurrentStep();
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
                ShowError();
            }
        }

        protected void rptrGamesList_ItemCommand(object sender, RepeaterCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "GameImageClicked" || e.CommandName == "GameLinkClicked")
                {
                    // Move to next step - WaitingForPlayer
                    hfStep.Value = ((int)GameStep.WaitingForPlayer).ToString();
                    hfGameID.Value = e.CommandArgument.ToString();
                    if (GetDestinationMemberID() > 0)
                    {
                        hfInviteKey.Value = GamesHelper.SendInvitation(g.Member.MemberID, GetDestinationMemberID(), g.Brand.Site.Community.CommunityID, hfGameID.Value);
                        g.Session.Add("GameID_" + hfInviteKey.Value, hfGameID.Value, 1, false);

                        ListSA.Instance.AddListMember(HotListCategory.WhoGamedYou,
                        g.Brand.Site.Community.CommunityID,
                        g.Brand.Site.SiteID,
                        GetDestinationMemberID(),
                        g.Member.MemberID,
                        null,
                        Constants.NULL_INT,
                        true,
                        true);

                        ListSA.Instance.AddListMember(HotListCategory.MembersYouGamed,
                            g.Brand.Site.Community.CommunityID,
                            g.Brand.Site.SiteID,
                            g.Member.MemberID,
                            GetDestinationMemberID(),
                            null,
                            Constants.NULL_INT,
                            true,
                            true);
                    }

                    ShowCurrentStep();
                }
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
                ShowError();
            }
        }

        protected void rptrGamesList_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if ((e.Item.ItemType == ListItemType.Item) ||
            (e.Item.ItemType == ListItemType.AlternatingItem))
                {
                    Matchnet.Games.ValueObjects.Game game = (Matchnet.Games.ValueObjects.Game)e.Item.DataItem;
                    ((ImageButton)e.Item.FindControl("imgGame")).ImageUrl = game.ImageURL;
                    ((ImageButton)e.Item.FindControl("imgGame")).CommandArgument = game.GameID.ToString();
                    ((LinkButton)e.Item.FindControl("lnkGame")).CommandArgument = game.GameID.ToString();
                    ((LinkButton)e.Item.FindControl("lnkGame")).Text = game.Title;
                }
            }
            catch (Exception ex)
            {

                g.ProcessException(ex);
                ShowError();
            }

        }
        // Private Methods (6) 

        private void SetCurrentStep()
        {
            if (string.IsNullOrEmpty(hfStep.Value))
                hfStep.Value = ((int)GameStep.ChooseGame).ToString();
            if (GetDestinationMemberID() > 0 && !string.IsNullOrEmpty(Request["InviteKey"]))
            {
                hfStep.Value = ((int)GameStep.Game).ToString();
                _InviteKey = Request["InviteKey"];
                _isInvitee = true;
            }
            if (!string.IsNullOrEmpty(Request["Status"]))
            {
                if (Request["Status"].ToLower() == "accepted")
                {
                    hfStep.Value = ((int)GameStep.Game).ToString();
                    _InviteKey = Request["InviteKey"];
                    _isInvitee = false;
                }
                else
                {
                    if (Request["Status"].ToLower() == "rejected" || Request["Status"].ToLower() == "noanswer")
                    {
                        hfStep.Value = ((int)GameStep.InvitationDeclined).ToString();
                        _InviteKey = Request["InviteKey"];
                        _isInvitee = false;
                    }
                }
            }

            _currentStep = (GameStep)(int.Parse(hfStep.Value));
        }

        private void ShowChooseGame()
        {
            _g.AnalyticsOmniture.PageName = "Games - Choose game";
            plcChooseGame.Visible = true;
            rptrGamesList.DataSource = GamesSA.Instance.GetCommunityGamesList(g.Brand.Site.Community.CommunityID);
            rptrGamesList.DataBind();
        }

        private void ShowCurrentStep()
        {
            plcChooseGame.Visible = false;
            plcWaitingForPlayer.Visible = false;
            plcGame.Visible = false;
            plcInvitationDeclined.Visible = false;
            plcError.Visible = false;

            SetCurrentStep();

            switch (_currentStep)
            {
                case GameStep.ChooseGame:
                    ShowChooseGame();
                    break;
                case GameStep.WaitingForPlayer:
                    ShowWaitingForPlayer();
                    break;
                case GameStep.Game:
                    ShowGame();
                    break;
                case GameStep.InvitationDeclined:
                    ShowInvitationDeclined();
                    break;
                case GameStep.Error:
                    ShowError();
                    break;
                default:
                    ShowChooseGame();
                    break;
            }
        }

        private void ShowError()
        {
            _g.AnalyticsOmniture.PageName = "Games - Error";

            plcChooseGame.Visible = false;
            plcWaitingForPlayer.Visible = false;
            plcGame.Visible = false;
            plcInvitationDeclined.Visible = false;
            plcError.Visible = true;
        }


        private void ShowGame()
        {
            _g.AnalyticsOmniture.PageName = "Games - Game";
            plcGame.Visible = true;
            if (_isInvitee)
            {
                // Set GameInvite as accepted and save
                hfGameID.Value = GamesHelper.AcceptInvite(g.Member.MemberID, _InviteKey, g.Brand.Site.Community.CommunityID);
            }
            else
            {
                // Remove game invite
                GamesHelper.DeleteInvite(GetDestinationMemberID(), _InviteKey, g.Brand.Site.Community.CommunityID);
                hfGameID.Value = g.Session["GameID_" + _InviteKey];
            }
            ifrmGame.Attributes["src"] = GamesHelper.GetGameURL(g, g.Member, GetDestinationMemberID(), hfGameID.Value, _isInvitee, Request);
        }

        private void ShowInvitationDeclined()
        {
            bool isNoAnswer = false;

            _g.AnalyticsOmniture.PageName = "Games - Invitation declined";

            if (Request["Status"].ToLower() == "noanswer")
            {
                isNoAnswer = true;
            }

            if (isNoAnswer)
            {
                StringDictionary expansionTokens = new StringDictionary();
                expansionTokens.Add("USERNAME", MemberSA.Instance.GetMember(GetDestinationMemberID(), MemberLoadFlags.None).GetUserName(_g.Brand));

                _g.AnalyticsOmniture.PageName = "Games - No answer";
                GamesHelper.SendMissedGameMessage(g.Member, GetDestinationMemberID(), g, g.GetResource("GAME_MESSAGES_MISSED", this),
                    expansionTokens,
                    g.GetResource("AT_QUOTA_ERROR_SUBJECT", this),
                    g.GetResource("AT_QUOTA_ERROR_MESSAGE", this, expansionTokens));

            }

            plcInvitationDeclined.Visible = true;
            // remove invitation
            GamesHelper.DeleteInvite(GetDestinationMemberID(), _InviteKey, g.Brand.Site.Community.CommunityID);
        }

        private void ShowWaitingForPlayer()
        {
            _g.AnalyticsOmniture.PageName = "Games - Waiting for player - Game " + g.Session["GameID_" + hfInviteKey.Value];
            plcWaitingForPlayer.Visible = true;
            string script = "CheckGameStatus('" + GetDestinationMemberID().ToString() + "', '" + hfInviteKey.Value + "');";
            this.Page.ClientScript.RegisterStartupScript(this.GetType(), WAITING_JS_KEY, script, true);
        }

        private int GetDestinationMemberID()
        {
            int result = 0;
            if (!string.IsNullOrEmpty(Request["DestinationMemberID"]))
            {
                int.TryParse(Request["DestinationMemberID"], out result);
            }
            return result;
        }

        #endregion Methods
    }
}
