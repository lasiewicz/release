﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.EmailNotifier.ValueObjects;
using Matchnet.EmailNotifier.ServiceAdapters;
using Matchnet.Web.Framework;
using Matchnet.Search.ValueObjects;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects;
using Matchnet.Web.Framework.Diagnostics;
using Matchnet.Web.Framework.Managers;
using Matchnet.Web.Interfaces;

namespace Matchnet.Web.Applications.Registration
{

    public class RegistrationCapture
    {

        public const string COOKIE_REG_GUID = "regguid";
        public const string COOKIE_REG_EVENTID = "regeventid";
        public const string COOKIE_REG_EVENT_NAME = "registration";
        private static SettingsManager _settingsManager = new SettingsManager();



        public static Hashtable GetExcludedFields()
        {
            Hashtable t = new Hashtable();
            t.Add("password", "");
            t.Add("session_id", "");


            return t;

        }

        public static ScheduledEvent CreateRegistrationEventFromCookie(ITemporaryPersistence persistence, Brand brand, Hashtable excludedfields)
        {
            ScheduledEvent registrationEvent;
            try
            {
                registrationEvent = new ScheduledEvent();
                if (!String.IsNullOrEmpty(persistence[RegistrationCapture.COOKIE_REG_GUID]))
                {
                    registrationEvent.ScheduleGUID = persistence[RegistrationCapture.COOKIE_REG_GUID];
                }

                registrationEvent.EmailScheduleID = Conversion.CInt(persistence[RegistrationCapture.COOKIE_REG_EVENTID], 0);
                registrationEvent.EmailAddress = HttpUtility.UrlDecode(persistence["emailaddress"]);
                registrationEvent.ScheduleName = COOKIE_REG_EVENT_NAME;
                registrationEvent.ScheduleDetails = new ScheduleDetail();
                registrationEvent.GroupID = brand.BrandID;
                foreach (string key in persistence.Values.AllKeys)
                {
                    if (key == null)
                        continue;
                    if (!excludedfields.ContainsKey(key.ToLower()))
                        registrationEvent.ScheduleDetails.Add(key, persistence[key]);
                }
                return registrationEvent;

            }
            catch (Exception ex)
            {

                return null;
            }

        }


        public static ScheduledEvent CreateRegistrationEventFromCookie(HttpCookie cookie,ContextGlobal g, Hashtable excludedfields)
        {
            ScheduledEvent registrationEvent;
            try
            {
                registrationEvent = new ScheduledEvent();
                if (!String.IsNullOrEmpty(cookie[RegistrationCapture.COOKIE_REG_GUID]))
                {  
                    registrationEvent.ScheduleGUID = cookie[RegistrationCapture.COOKIE_REG_GUID];
                }

                registrationEvent.EmailScheduleID =Conversion.CInt(cookie[RegistrationCapture.COOKIE_REG_EVENTID],0);
                registrationEvent.EmailAddress=HttpUtility.UrlDecode(cookie["emailaddress"]);
                registrationEvent.ScheduleName = COOKIE_REG_EVENT_NAME;
                registrationEvent.ScheduleDetails = new ScheduleDetail();
                registrationEvent.GroupID = g.Brand.BrandID;
                foreach (string key in cookie.Values.AllKeys)
                {  if(key==null)
                    continue;
                if (!excludedfields.ContainsKey(key.ToLower()))
                     registrationEvent.ScheduleDetails.Add(key, cookie[key]);
                }
                return registrationEvent;

            }
            catch (Exception ex)
            {
              
                return null;
            }
            
        }

        public static ScheduledEvent PopulateWizardFromEvent(string guid, string cookiename, ContextGlobal g)
        {
            try{
              
                ScheduledEvent ev=WCFEmailNotifierSA.Instance.GetScheduledEvent(guid);
                if(ev==null)
                    return null ;
                Hashtable excludedfields = GetExcludedFields();
                HttpCookie cookie = new HttpCookie(cookiename,cookiename);
               
                foreach(string key in ev.ScheduleDetails.Details.Keys)
                {
                  if (!excludedfields.ContainsKey(key.ToLower()))
                     cookie[key]=ev.ScheduleDetails.Get(key);
                    
                }
                cookie["LAST_COMPLETED_STEP"] = "1";
                cookie["CURRENT_STEP"] = "1";
                HttpContext.Current.Request.Cookies.Add(cookie);
                return ev;

            }catch(Exception ex)
            {
                g.ProcessException(ex);
                return null;
            }

        }


        public static bool PopulateSearchPreferencesFromEvent(ScheduledEvent ev,ContextGlobal g)
        {
            try
            {

                SearchPreferenceCollection preferences = g.SearchPreferences;

                int searchTypeVal = Conversion.CInt(HttpContext.Current.Request["SearchTypeID"]);
                

                SearchTypeID searchTypeID;
                if (searchTypeVal != Constants.NULL_INT)
                {
                    searchTypeID = (SearchTypeID)searchTypeVal;
                }
                else
                {
                    searchTypeID = SearchTypeID.PostalCode;
                }
                preferences.Add("SearchTypeID", ((int)searchTypeID).ToString());

                int genderMask = Constants.NULL_INT;
                 genderMask = Conversion.CInt(ev.ScheduleDetails.Details["GenderMask"]);
                if(genderMask <= 0)
                    return false;

                 int regionid = Conversion.CInt(ev.ScheduleDetails.Details["RegionID"]);
                if(regionid <=0)
                    return false;

                Matchnet.Content.ValueObjects.Region.RegionLanguage region= RegionSA.Instance.RetrievePopulatedHierarchy(regionid, g.Brand.Site.LanguageID);
                if (region == null)
                {
                   return false;
                }

               
                preferences.Add("CountryRegionID", region.CountryRegionID.ToString());
                preferences.Add("RegionID", regionid.ToString());
                preferences.Add("GenderMask", genderMask.ToString());
                preferences.Add("Distance", g.Brand.DefaultSearchRadius.ToString());
                preferences.Add("SearchOrderBy", "4");

                int minAge = 0;
                int maxAge = 0;
                int agerange = Conversion.CInt(ev.ScheduleDetails.Details["AgeRange"]);
                if (agerange < 0)
                {
                    minAge = g.Brand.DefaultAgeMin;
                    maxAge = g.Brand.DefaultAgeMax;
                }
                
                GetAgeRangeFromID(agerange, ref minAge, ref maxAge);
                

                preferences.Add("MinAge", minAge.ToString());
                preferences.Add("MaxAge", maxAge.ToString());

               

                g.SaveSearchPreferences();
                return true;

            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
                return false;
            }

        }

        public static void UpdateRegistrationEventFromPersistence(ScheduledEvent registrationEvent, Hashtable excludedfields, ITemporaryPersistence persistence)
        {
            try
            {
                registrationEvent.EmailScheduleID = Conversion.CInt(persistence[RegistrationCapture.COOKIE_REG_EVENTID], 0);
                registrationEvent.EmailAddress = HttpUtility.UrlDecode(persistence["emailaddress"]);
                if (registrationEvent.ScheduleDetails == null)
                    registrationEvent.ScheduleDetails = new ScheduleDetail();

                foreach (string key in persistence.Values.AllKeys)
                {
                    if (key == null)
                        continue;
                    if (!excludedfields.ContainsKey(key.ToLower()))
                        registrationEvent.ScheduleDetails.Add(key, persistence[key]);
                }
            }
            catch (Exception ex)
            {
                ContextExceptions exceptionHandler = new ContextExceptions();
                exceptionHandler.LogException(ex);
            }
        }

        public static void UpdateRegistrationEventFromCookie(ScheduledEvent registrationEvent,Hashtable excludedfields,HttpCookie cookie, ContextGlobal g)
        {   
            try
            {
                registrationEvent.EmailScheduleID = Conversion.CInt(cookie[RegistrationCapture.COOKIE_REG_EVENTID], 0);
                registrationEvent.EmailAddress =HttpUtility.UrlDecode( cookie["emailaddress"]);
                if(registrationEvent.ScheduleDetails == null)
                    registrationEvent.ScheduleDetails = new ScheduleDetail();
                
                foreach (string key in cookie.Values.AllKeys)
                {
                    if (key == null)
                        continue;
                    if(!excludedfields.ContainsKey(key.ToLower()))
                        registrationEvent.ScheduleDetails.Add(key, cookie[key]);
                }
                

            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }

        }

        public static void SaveRegistrationEvent(ScheduledEvent registrationEvent, ITemporaryPersistence persistence)
        {
            try
            {
                if (registrationEvent == null)
                    return;

                registrationEvent = WCFEmailNotifierSA.Instance.SaveScheduledEvent(registrationEvent);
                persistence[COOKIE_REG_GUID] = registrationEvent.ScheduleGUID;
                persistence[COOKIE_REG_EVENTID] = registrationEvent.EmailScheduleID.ToString();
                persistence[COOKIE_REG_EVENT_NAME] = registrationEvent.ScheduleName;
            }
            catch (Exception ex)
            {
                ContextExceptions exceptionHandler = new ContextExceptions();
                exceptionHandler.LogException(ex);
            }
        }


        public static void SaveRegistrationEvent(ScheduledEvent registrationEvent,HttpCookie cookie, ContextGlobal g)
        {
            
            try
            {
                if (registrationEvent == null)
                    return;
                
                registrationEvent= WCFEmailNotifierSA.Instance.SaveScheduledEvent(registrationEvent);
                cookie[COOKIE_REG_GUID] = registrationEvent.ScheduleGUID;
                cookie[COOKIE_REG_EVENTID] = registrationEvent.EmailScheduleID.ToString();
                cookie[COOKIE_REG_EVENT_NAME] = registrationEvent.ScheduleName;
                }
            catch (Exception ex)
            {
                g.ProcessException(ex);
              
            }

        }
        public static void DeleteRegistrationEvent(ScheduledEvent registrationEvent, ContextGlobal g)
        {

            try
            {
                if (registrationEvent == null || String.IsNullOrEmpty(registrationEvent.ScheduleGUID))
                    return;

                WCFEmailNotifierSA.Instance.DeleteScheduledEvent(registrationEvent.ScheduleGUID);
              
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);

            }

        }
        public static bool IsRegistrationCaptureEnabled(ContextGlobal g)
        {
            try
            {
                return _settingsManager.GetSettingBool(SettingConstants.ENABLE_REGISTRATION_CAPTURE, g.Brand);
            }
            catch (Exception ex)
            { return false; }
        }
        //wow, it was hardcoded so I just copied it
        private static void GetAgeRangeFromID(int AgeRangeID, ref int MinAge, ref int MaxAge)
        {
            switch (AgeRangeID)
            {
                case 0:
                    MinAge = 18;
                    MaxAge = 24;
                    break;
                case 1:
                    MinAge = 25;
                    MaxAge = 34;
                    break;
                case 2:
                    MinAge = 35;
                    MaxAge = 44;
                    break;
                case 3:
                    MinAge = 45;
                    MaxAge = 54;
                    break;
                case 4:
                    MinAge = 55;
                    MaxAge = 99;
                    break;
                default:
                    MinAge = 18;
                    MaxAge = 99;
                    break;
            }
        }
    }
}
