﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Ui.BasicElements;
using Matchnet.Web.Applications.Registration.Controls;

namespace Matchnet.Web.Applications.Registration.Templates
{
    public partial class Template2 : RegistrationTemplateBase, IRegistrationTemplate
    {
        private const string IMG_LOGO_FILE_NAME = "jdate-general-logo.png";

        public override Button ButtonContinue
        { get { return btnContinue; } }
        public override Button ButtonNext
        { get { return btnNext; } }
        public override Button ButtonPrevious
        { get { return btnPrevious; } }
        public override ProgressBar Progress
        { get { return idProgress; } }
        public override ValidationMessage ValidationMessageText
        { get { return txtValidation; } }
        public override Txt Tip
        { get { return txtTip; } }
        public PlaceHolder ContentPlaceholder { get { return phContent; } }
        public override Literal StepCSSClassLiteral
        { get { return litCSSClass; } }
        public override Literal StepCSSSecondaryClassLiteral
        { get { return litCSSSecondaryClass; } }
        public override Literal StepMainContainerCSSClassLiteral
        { get { return litCntMain; } }
        protected override void OnInit(EventArgs e)
        {
            btnContinue.ResourceControl = ResourceControl;
            btnNext.ResourceControl = ResourceControl;
            btnPrevious.ResourceControl = ResourceControl;
            txtDecorationTitle.ResourceControl = ResourceControl;

            txtLogon.ResourceControl = ResourceControl;
            txt3.ResourceControl = ResourceControl;

            Control amadesaControl = g.LayoutTemplateBase.FindControl("Amadesa");

            if (amadesaControl != null)
            {
                amadesaControl.Visible = false;
            }
        }

        public override Txt AdditionalHtmlTxt
        { get { return txtAdditionalHtml; } }

        protected void Page_Load(object sender, EventArgs e)
        {

            //idClass.Text=
            if (g.RecipientMemberID > 0)
            {
                GalleryMiniProfile20 miniProfile = (GalleryMiniProfile20)LoadControl("/Framework/Ui/BasicElements/GalleryMiniProfile20.ascx");
                miniProfile.Member = Member.ServiceAdapters.MemberSA.Instance.GetMember(g.RecipientMemberID, Member.ServiceAdapters.MemberLoadFlags.None);
                miniProfile.DisplayContext = ResultContextType.RegistrationCarrot;
                divMiniProfile.Controls.Add(miniProfile);
            }
            else
            {
                divMiniProfile.Style.Add("display", "none");
            }

            #region Logo image setting
            string imagePath = Matchnet.Web.Framework.Image.GetURLFromFilename(IMG_LOGO_FILE_NAME);
            if (g.BrandAlias != null)
            {
                imagePath = Matchnet.Web.Framework.Image.GetBrandAliasImagePath(IMG_LOGO_FILE_NAME, g.BrandAlias);
            }

            string host = HttpContext.Current.Request.Url.Host;
            imgLogo.ImageUrl = imagePath;
            #endregion


            string httpHost = Request.ServerVariables["HTTP_HOST"].Split(new char[] { '.' })[0];
            lnkJDFRReg.NavigateUrl = "http://" + httpHost + ".jdate.fr/Applications/Registration/Registration.aspx?fbreg=1&fbregsid=" + (int)WebConstants.SITE_ID.JDateFR;
            lnkJDUKReg.NavigateUrl = "http://" + httpHost + ".jdate.co.uk/Applications/Registration/Registration.aspx?fbreg=1&fbregsid=" + (int)WebConstants.SITE_ID.JDateUK;
            lnkJDILReg.NavigateUrl = "http://" + httpHost + ".jdate.co.il/Applications/Registration/Registration.aspx?fbreg=1&fbregsid=" + (int)WebConstants.SITE_ID.JDateCoIL;
        }

        private void Page_PreRender(object sender, EventArgs e)
        {
            txtTitle.Text = GetResourceString(TitleResource, ResourceControl);
            if (!String.IsNullOrEmpty(TipResource))
                txtTip.Text = GetResourceString(TipResource, ResourceControl);
            else
                divTip.Style.Add("display", "none");

        }
    }
}