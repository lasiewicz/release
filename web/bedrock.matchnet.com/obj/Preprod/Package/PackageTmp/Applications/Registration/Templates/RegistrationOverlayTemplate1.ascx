﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RegistrationOverlayTemplate1.ascx.cs" Inherits="Matchnet.Web.Applications.Registration.Templates.RegistrationOverlayTemplate11" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>

<script type='text/javascript' src='/javascript20/registrationOverlay/jquery.autocomplete.js'></script>
<script type='text/javascript' src='/javascript20/registrationOverlay/jquery.cookie.js'></script>
<script type='text/javascript' src='/javascript20/registrationOverlay/registration_overlay.js'></script>
<script type='text/javascript' src='/javascript20/registrationOverlay/ajaxfileupload.js'></script>
<link rel="stylesheet" type="text/css" href="/css/jquery.autocomplete.css" />

<div class="bgCover">&nbsp;</div>
<div class="overlayBox">
    <div id="overlaybox-header">
        <div id="overlaybox-header-content">
            &nbsp;</div>
    </div>
    <div id="navigation-wrap">
        <div id="demoNavigation">
            <div id="prev-lnk">
                <a href="javascript:void(0);" id="back">
                    <mn:Txt ID="txtPrev" runat="server" ResourceConstant="TXT_PREV" />
                </a>
            </div>
            <div id="next-lnk">
                <a href="javascript:void(0);" id="next">
                    <mn:Txt ID="txtNext" runat="server" ResourceConstant="TXT_NEXT" />
                </a>
            </div>
        </div>
        <div id="progressbar-wrap">
            Check out your progress
            <div id="progressbar"></div>
        </div>
    </div>
    <div id="seperator-div">
        <hr class="seperator"/>
    </div>
    <div id="overlay-error" class="hide error">
        <mn:Txt runat="server" ResourceConstant="GENDERAL_ERROR_TXT" ID="ErrorTxt" />
    </div>
    <div id="overlay-content-wrapper">
        <div id="overlay-content">
            <asp:PlaceHolder ID="phContent" runat="server"></asp:PlaceHolder>
            <a href="javascript:void(0);" id="nextBtn" class="overlay-next-button">
                <mn:Image runat="server" FileName="ui-reg-btn-next.gif" />
            </a>
        </div>
        <div id="overlat-save-details" class="hide">
            <div class="hide" id="save-details_title">
                <mn:Txt ID="Txt2" runat="server" ResourceConstant="REGISTRATION_STEP_END" />
            </div>
            <div id="save-details-content">
                <mn:Txt runat="server" ResourceConstant="TXT_SAVE_DETAILS" ID="Txt3" />
            </div>
            <div id="save-details-loader">
                <mn:Image ID="Image1" runat="server" FileName="ajax-loader.gif" />
            </div>
        </div>
        <div id="loading-next-bar">           
            <div id="ajax-loader" class="hide">
                <mn:Image runat="server" FileName="ajax-loader.gif" />
            </div>
        </div>        
    </div>
</div>
