﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Web;
using Matchnet.Web.Framework;
using System.Xml.Serialization;
using Matchnet.Web.Framework.HTTPContextWrappers;
using Matchnet.Web.Framework.Managers;
using Matchnet.Web.Framework.Ui.FormElements;
using Matchnet.Web.Framework.TemplateControls;

using System.Net;
using Matchnet.ExternalMail.ServiceAdapters;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects;
using Matchnet.Member.ValueObjects.Photos;
using Matchnet.Web.Framework.Globalization;
using Matchnet.Web.Applications.Registration.Templates;
using Matchnet.Session.ValueObjects;
using Matchnet.Session.ServiceAdapters;
using Matchnet.Web.Applications.MemberProfile;
using Matchnet.Content.ServiceAdapters.Links;
using Matchnet.ExternalMail.ValueObjects.DoNotEmail;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Web.Applications.ColorCode;
using Matchnet.Web.Framework.Util;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.Admin;
using Matchnet.Content.ValueObjects.Region;
using Matchnet.MembersOnline.ServiceAdapters;
using Matchnet.Content.ValueObjects.AttributeOption;
using Spark.Common;

namespace Matchnet.Web.Applications.Registration
{
    public partial class Registration : RegistrationBase
    {
        //private const string OMNITURE_SCRIPT = "<script type=\"text/javascript\">var s2 = s_gi(\"{0}\"); s2.events = \"{1}\"; s2.pageName = \"Registration - {2}\"; s2.t(); alert(s2.pageName);</script>";
        private const int FACEBOOK_JDIL_DEFAULT_PROMOTIONID = 72182;
        private const int FACEBOOK_JDUK_DEFAULT_PROMOTIONID = 72604;
        private const int FACEBOOK_JDFR_DEFAULT_PROMOTIONID = 72605;

        private const string FACEBOOK_SCENARIO_NAME = "FBRegScen";

        private const string OMNITURE_SCRIPT = "var s2 = s_gi(\"{0}\"); s2.events = \"{1}\"; s2.pageName = \"{2}\"; s2.eVar48=\"{3}\"; s2.t(); ";


        Step _regStep;



        int _progress;

        bool _loadNewWizard;
        ////should be branded in wizard
        string _templatePath = "/Applications/Registration/XML/Registration.xml";


        bool _focusScriptReg;
        bool _ajaxEnabled = true;

        private bool _isSplash = false;
        private bool _isFacebookReg = false;
        private string _originalScenarioName = string.Empty;

        public bool AjaxEnabled
        {
            set { _ajaxEnabled = value; }
            get { return _ajaxEnabled; }
        }

        public bool IsSplash
        {
            set { _isSplash = value; }
            get { return _isSplash; }
        }

        private PlaceHolder ContentPanel
        {
            get
            {
                if (AjaxEnabled)
                {
                    return phContent;
                }
                else
                {
                    return phContentNonAjax;
                }
            }
        }

        public string TemplatePath
        {
            set { _templatePath = value; }
            get { return _templatePath; }
        }

        public bool LoadNewWizard
        { set { _loadNewWizard = value; } }

        public string ScenarioName { get; set; }


        public RegistrationTemplateBase TemplateControl
        {
            get { return _templateControl; }
        }

        #region Page Life Cycle

        protected override void OnInit(EventArgs e)
        {
            try
            {
                base.OnInit(e);

                RedirectToNewRegSiteBasedOnSetting();

                MediaTrafficLoadBalancer();

                jQueryValidation = new JQueryValidation();

                if (g.Member != null && string.IsNullOrEmpty(Request["ShowSplashPageForPostLoginUsers"]))
                    g.Transfer("/Applications/Home/Default.aspx");

                if (Request.Browser.Browser.IndexOf("AppleWeb") >= 0)
                {
                    anthemPnl1.EnableCallBack = false;
                    anthemPnl1.AutoUpdateAfterCallBack = false;
                }
                __debugTrace = Conversion.CBool(System.Configuration.ConfigurationSettings.AppSettings.Get("DebugTrace"));

                LoadStep();

                if (_isOverlayReg && (!string.IsNullOrEmpty(Request.QueryString["checkstep"]) || HttpUtility.UrlDecode(Context.Request.Url.AbsoluteUri).Contains("checkstep")))
                {
                    //if (!string.IsNullOrEmpty(Request["stepID"]))
                    //{
                    //    int.TryParse(Request["stepID"], out _clientStepID);
                    //}

                    //_wizard.StepID = _clientStepID + 1;

                    SubmitStep();
                    ReturnJSONResponse();
                }

                // This is a hack to remove AJAX from the photo upload step on JDIL
                if (g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.JDateCoIL ||
                    g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.JDateFR ||
                    g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.Cupid)
                {
                    if (_wizard.StepID == 9 || _wizard.StepID == 11)
                    {
                        anthemPnl1.EnableCallBack = false;
                        anthemPnl1.AutoUpdateAfterCallBack = false;
                    }
                }

                RegistrationReason.AddDirectLinkToSession(g);

            }
            catch (Exception ex)
            { g.ProcessException(ex); }

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!AjaxEnabled)
            {
                //anthemPnl1.Visible = false;
                anthemPnl1.Parent.Controls.Remove(anthemPnl1);
                pnlNonAjax.Visible = true;
            }
            else
            {
                anthemPnl1.Visible = true;
                pnlNonAjax.Visible = false;
            }

            if (_wizard.RegistrationScenario.IsOnePageReg)
            {
                anthemPnl1.EnableCallBack = false;
                anthemPnl1.AutoUpdateAfterCallBack = false;
            }

            //if ((g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.JDate)
            //    && (g.AppPage.ControlName == "Splash20")
            //    && _wizard.StepID == 1)
            //{
            //    PlaceHolderOmnitureMboxSplash.Visible = true;
            //    PlaceHolderOmnitureMboxSplashBeginDiv.Visible = true;
            //}


            //if ((g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.JDate)
            //    && (g.AppPage.ControlName == "Registration")
            //    && _wizard.StepID == 1)
            //    PlaceHolderOmnitureMboxRegBegin.Visible = true;

            if (_wizard.RegistrationStep.MBox)
            {
                // if we are going to have multiple MBoxes on the page for different regions, we need to disable the MBox div that wraps
                // the whole page content and the MBoxCreate call for it.
                if (!_wizard.RegistrationStep.MultiRegionMBox)
                {
                    PlaceHolderOmnitureMboxSplash.Visible = true;
                    PlaceHolderOmnitureMboxSplashBeginDiv.Visible = true;
                    PlaceHolderEntirePageMboxCreate.Visible = true;
                    litmboxPageName.Text = _wizard.RegistrationStep.MBoxPageName;
                }

                PlaceHolderOmnitureMboxRegBegin.Visible = true;
                litmboxEVar.Text = _wizard.RegistrationStep.MBoxEVar;

            }

        }

        private void Page_PreRender(object sender, System.EventArgs e)
        {
            try
            {
                litScript.Text = GetClientScript();

                ShowStepControls();
                if (_templateControl.Progress != null)
                {
                    _templateControl.Progress.ResourceControl = ResourceControl;
                    _progress = 100 * _wizard.CompletedSteps / _wizard.RegistrationScenario.Steps.Count;
                    _templateControl.Progress.ProgressPercent = _progress;
                }

                if (_templateControl.StepCSSClassLiteral != null && _wizard.RegistrationScenario.Steps[_wizard.StepID - 1].CSSClass != null)
                    _templateControl.StepCSSClassLiteral.Text = _wizard.RegistrationScenario.Steps[_wizard.StepID - 1].CSSClass;

                if (_templateControl.StepCSSSecondaryClassLiteral != null && _wizard.RegistrationScenario.Steps[_wizard.StepID - 1].CssSecondaryContainer != null)
                    if (_templateControl.StepMainContainerCSSClassLiteral != null && _wizard.RegistrationScenario.Steps[_wizard.StepID - 1].CssMainContainer != null)
                        _templateControl.StepMainContainerCSSClassLiteral.Text = _wizard.RegistrationScenario.Steps[_wizard.StepID - 1].CssMainContainer;


                if (!String.IsNullOrEmpty(_wizard.RegistrationScenario.Steps[_wizard.StepID - 1].AdditionalHtmlResource))
                {
                    string additionalHtml = g.GetResource(_wizard.RegistrationScenario.Steps[_wizard.StepID - 1].AdditionalHtmlResource, this);
                    _templateControl.AdditionalHtmlTxt.Text = additionalHtml;
                }
                if (!String.IsNullOrEmpty(_wizard.RegistrationScenario.Steps[_wizard.StepID - 1].StepButtonResource))
                    _templateControl.ButtonContinue.Text = g.GetResource(_wizard.RegistrationScenario.Steps[_wizard.StepID - 1].StepButtonResource, this);


                string pageName = !string.IsNullOrEmpty(_wizard.RegistrationScenario.PageName) ? _wizard.RegistrationScenario.PageName : String.Format(OMNITURE_PAGE_NAME, _wizard.StepID.ToString("00"), _wizard.RegistrationScenario.ID.ToString("00"), _attributesNames, _wizard.RegistrationScenario.Name);
                string registrationEvent = "";

                //omniture can be set on splash page
                if (_setOmniture)
                {
                    // business wants this event7 to fire each time, we land on step 1
                    if (_wizard.CompletedSteps == 0)
                    {
                        registrationEvent = "event7";
                    }

                    if (String.IsNullOrEmpty(g.Session.GetString("OMNITURE_REGISTRATION_BEGIN_EVENT")))
                    {
                        //registrationEvent = "event7";
                        g.Session.Add("OMNITURE_REGISTRATION_BEGIN_EVENT", "true", SessionPropertyLifetime.Temporary);
                        _wizard.StartStepID = 0;
                    }
                    else
                    {
                        // On JDIL, event7 should fire only once for this type of reg MPR-2814
                        if (g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.JDateCoIL)
                        {
                            registrationEvent = "";
                        }
                    }
                    if (IsPostBack)
                    {
                        _g.AnalyticsOmniture.PageName = pageName;
                        string omniScript = String.Format(OMNITURE_SCRIPT, g.AnalyticsOmniture.S_AccountName, registrationEvent, pageName, _wizard.RegistrationScenario.Name);

                        if (AjaxEnabled)
                        {
                            Anthem.Manager.RegisterStartupScript(this.Page.GetType(), "Omniture", omniScript, true);
                        }
                        else
                        {
                            this.Page.ClientScript.RegisterStartupScript(this.Page.GetType(), "Omniture", omniScript, true);
                        }
                        //antOmniture.Text = omniScript;

                    }
                    else
                    {
                        _g.AnalyticsOmniture.PageName = pageName;
                        if (string.IsNullOrEmpty(Request.QueryString["media"]))
                        {
                            _g.AnalyticsOmniture.AddEvent(registrationEvent);
                        }
                        else
                        {
                            this.Page.ClientScript.RegisterStartupScript(this.GetType(), "OmnitureRegStartEvent", "var omnitureRegStartEvent='" + registrationEvent + "';", true);
                        }

                        _g.AnalyticsOmniture.Evar48 = _wizard.RegistrationScenario.Name;

                        if (string.IsNullOrEmpty(g.Session[WebConstants.SESSION_PROPERTY_REG_SCENARIO_NAME]) || g.Session[WebConstants.SESSION_PROPERTY_REG_SCENARIO_NAME] != _wizard.RegistrationScenario.Name)
                        {
                            g.Session.Add(WebConstants.SESSION_PROPERTY_REG_SCENARIO_NAME, _wizard.RegistrationScenario.Name, SessionPropertyLifetime.Temporary);
                        }

                        //if (g.Brand.Site.SiteID != (int)WebConstants.SITE_ID.JDate)
                        //    _g.AnalyticsOmniture.Evar47 = "";

                        //Registration link from Q/A Gate page
                        if (!String.IsNullOrEmpty(Request.QueryString["PRM"]))
                        {
                            if (Request.QueryString["PRM"] == "66799")
                            {
                                _g.AnalyticsOmniture.Evar20 = "Q&A Gate Page:Registration";
                            }
                        }

                        //g.AnalyticsOmniture.Evar16 = GetOmnitureRegistrationScenario();
                    }

                }


                if (IsSplash || !string.IsNullOrEmpty(Request.QueryString["media"]))
                {
                    _g.AnalyticsOmniture.Evar43 = _wizard.RegistrationScenario.EVar43;
                    _g.AnalyticsOmniture.Evar48 = _wizard.RegistrationScenario.EVar48;
                    if (IsSplash && !string.IsNullOrEmpty(_wizard.RegistrationScenario.EVar43))
                    {
                        _g.Session.Add(EVAR43_SESSION_KEY, _wizard.RegistrationScenario.EVar43, SessionPropertyLifetime.Persistent);
                    }

                    if (IsSplash && !string.IsNullOrEmpty(_wizard.RegistrationScenario.EVar47))
                    {
                        _g.AnalyticsOmniture.Evar47 = _wizard.RegistrationScenario.EVar47;
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(_g.Session[EVAR43_SESSION_KEY]))
                    {
                        _g.AnalyticsOmniture.Evar43 = _g.Session[EVAR43_SESSION_KEY];
                        _g.Session.Remove(EVAR43_SESSION_KEY);
                    }
                }

                Page.ClientScript.RegisterStartupScript(this.GetType(), "jqueryvalidation",
                                                         jQueryValidation.GetValidationJSCode(), true);

                Page.ClientScript.RegisterStartupScript(this.GetType(), "siteid", String.Format("var siteId={0};", g.Brand.Site.SiteID.ToString()), true);

            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.Write(ex.ToString());
                System.Diagnostics.EventLog.WriteEntry("WWW", "Err:\r\n" + ex.ToString() + "\r\nStack Trace" + ex.StackTrace);
                g.ProcessException(ex);
            }
        }

        protected void btnContinue_OnClick(object sender, EventArgs e)
        {
            try
            {
                SubmitStep();

            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        protected void btnFBNoThanks_OnClick(object sender, EventArgs e)
        {
            try
            {
                SubmitStep();

            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        protected void btnFBRegister_OnClick(object sender, EventArgs e)
        {
            try
            {
                SubmitFBRegistrationInfo();
                SubmitStep();
            }
            catch (Exception ex)
            { g.ProcessException(ex); }

        }


        private void SubmitFBRegistrationInfo()
        {
            if (_templateControl.FBRegistration != null)
            {
                FacebookValueTranslator fbvt = new FacebookValueTranslator();

                string firstName = fbvt.TranslateFirstName(_templateControl.FBRegistration.FBName);
                DateTime birthdate = fbvt.TranslateBirthdate(_templateControl.FBRegistration.FBBirthday);
                string emailAddress = _templateControl.FBRegistration.FBEmail;

                _wizard.Persistence["FirstName"] = firstName;
                SetControlValue("FirstName", firstName);

                _wizard.Persistence["EmailAddress"] = emailAddress;
                SetControlValue("EmailAddress", emailAddress);

                if (birthdate != DateTime.MinValue)
                {
                    _wizard.Persistence["BirthDate"] = birthdate.ToString();
                    SetControlValue("BirthDate", birthdate.ToString());
                }
            }
        }

        private void SetControlValue(string attributeName, string value)
        {
            IAttributeControl attr = (IAttributeControl)_templateControl.ContentPlaceholder.FindControl("id" + attributeName);
            if (attr != null)
            {
                attr.SetValue(value);
            }
        }

        protected void btnNext_OnClick(object sender, EventArgs e)
        {
            try
            {
                SubmitStep();

            }
            catch (Exception ex)
            { g.ProcessException(ex); }

        }
        protected void btnPrevious_OnClick(object sender, EventArgs e)
        {
            try
            {
                bool valid = true;

                _wizard.StepID = _wizard.StepID - 1;

                while (_wizard.SkipStep(_wizard.StepID))
                    _wizard.StepID = _wizard.StepID - 1;

                _wizard.Persist(_isFacebookReg, _originalScenarioName);

                //_wizard.PersistToCookie(_isFacebookReg, _originalScenarioName);
                //ShowStepControls();


            }
            catch (Exception ex)
            { g.ProcessException(ex); }

        }
        #endregion

        public void LoadStep()
        {
            try
            {

                //not really
                _controls = new List<AttributeControl>();
                // Added "091202" to the name of the cookie to ignore the existing values.
                int fbRegSiteID = 0;
                if (!string.IsNullOrEmpty(Request.QueryString[WebConstants.URL_PARAMETER_FACEBOOK_REGISTRATION]))
                {
                    fbRegSiteID = g.Brand.Site.SiteID;
                    _isFacebookReg = true;

                    if (!string.IsNullOrEmpty(Request.QueryString[WebConstants.URL_PARAMETER_FACEBOOK_REGISTRATION_SITE_ID]))
                    {
                        if (!int.TryParse(Request.QueryString[WebConstants.URL_PARAMETER_FACEBOOK_REGISTRATION_SITE_ID], out fbRegSiteID))
                        {
                            fbRegSiteID = g.Brand.Site.SiteID;
                        }
                    }


                    var targetBrand = FrameworkGlobals.GetBrandForSite(fbRegSiteID);
                    if (targetBrand != null)
                    {
                        string httpHost = Request.ServerVariables["HTTP_HOST"].Split(new char[] { '.' })[0];
                        string targetURL = "http://" + httpHost + "." + targetBrand.Uri +
                                           "/Applications/Registration/Registration.aspx?fbreg=1&" +
                                           WebConstants.URL_PARAMETER_FACEBOOK_REGISTRATION_SITE_ID +
                                           "=" + fbRegSiteID;

                        if (g.Brand.Site.SiteID != fbRegSiteID)
                        {
                            g.Transfer(targetURL);
                        }
                        else
                        {
                            if (string.IsNullOrEmpty(Request.QueryString[WebConstants.URL_PARAMETER_NAME_PRM]))
                            {
                                int fbprm = DEFAULT_PROMOTIONID;
                                switch (g.Brand.Site.SiteID)
                                {
                                    case (int)WebConstants.SITE_ID.JDateCoIL: fbprm = FACEBOOK_JDIL_DEFAULT_PROMOTIONID; break;
                                    case (int)WebConstants.SITE_ID.JDateUK: fbprm = FACEBOOK_JDUK_DEFAULT_PROMOTIONID; break;
                                    case (int)WebConstants.SITE_ID.JDateFR: fbprm = FACEBOOK_JDFR_DEFAULT_PROMOTIONID; break;
                                }

                                targetURL += "&" + WebConstants.URL_PARAMETER_NAME_PRM + "=" + fbprm;

                                g.Transfer(targetURL);
                            }
                        }
                    }

                    _wizard = new RegistrationWizard(g.Brand, FlowType);
                }
                else
                {
                    _wizard = new RegistrationWizard(g.Brand, FlowType);
                }

                if (RegistrationCapture.IsRegistrationCaptureEnabled(g) && !Conversion.CBool(g.Session.Get("REGISTRATION_POPULATED")))
                {
                    if (!String.IsNullOrEmpty(Request["RegistrationGUID"]))
                    {
                        RegistrationCapture.PopulateWizardFromEvent(Request["RegistrationGUID"], _wizard.Name, g);
                        g.Session.Add("REGISTRATION_POPULATED", "true", SessionPropertyLifetime.Temporary);
                    }

                }

                _wizard.DebugTrace = __debugTrace;
                FrameworkGlobals.Trace(__debugTrace, "Registration.LoadStep", "Create wizard:" + _templatePath, null);
                _wizard.Load(_loadNewWizard);
                FrameworkGlobals.Trace(__debugTrace, "Registration.LoadStep", "Load wizard:" + _templatePath, null);
                if (!Page.IsPostBack)
                {
                    _wizard.CacheRequest(new CurrentRequest());
                }

                FrameworkGlobals.Trace(__debugTrace, "Registration.LoadStep", "PersistToCookie", "PromotionID=" + PromotionID.ToString());

                _wizard.Persistence["prm"] = PromotionID.ToString();
                if (!String.IsNullOrEmpty(Request["ScenarioName"]))
                {
                    _wizard.ScenarioName = Server.UrlDecode(Request["ScenarioName"]);
                }
                else if (!String.IsNullOrEmpty(Request["ScenarioID"]))
                {
                    _wizard.ScenarioName = "Scenario " + Server.UrlDecode(Request["ScenarioID"]);
                }
                else if (!string.IsNullOrEmpty(ScenarioName))
                {
                    _wizard.ScenarioName = ScenarioName;
                }

                if (fbRegSiteID > 0)
                {

                    _originalScenarioName = _wizard.ScenarioName;
                    _wizard.ScenarioName = FACEBOOK_SCENARIO_NAME;
                }


                _wizard.LoadRegistrationSteps(_isFacebookReg);

                _wizard.Persist(_isFacebookReg, _originalScenarioName);

                _regScenario = _wizard.RegistrationScenario;

                if (_regScenario.Steps[0].IsFacebookReg)
                {
                    //Enable facebook api control in template. 
                    g.LayoutTemplateBase.LoadFacebookScript();
                }


                if (HttpContext.Current.Request[WebConstants.URL_PARAMETER_STATIC_REGISTRATION_FORM] == "1")
                {
                    foreach (String key in HttpContext.Current.Request.Params)
                    {
                        _wizard.Persistence[key.ToUpper()] = HttpContext.Current.Request[key];
                    }
                    if (String.IsNullOrEmpty(_wizard.GetRegistrationPersistenceValue("GENDERMASK")))
                    {
                        _wizard.Persistence["GENDERMASK"] =
                            ((!String.IsNullOrEmpty(_wizard.GetRegistrationPersistenceValue("GENDERID")) ? Convert.ToInt32(_wizard.Persistence["GENDERID"]) : 0) +
                            (!String.IsNullOrEmpty(_wizard.GetRegistrationPersistenceValue("SEEKINGGENDERID")) ? Convert.ToInt32(_wizard.Persistence["SEEKINGGENDERID"]) : 0)).ToString();
                    }

                    if (String.IsNullOrEmpty(_wizard.GetRegistrationPersistenceValue("REGIONID")))
                    {
                        RegionID regionid = RegionSA.Instance.FindRegionIdByPostalCode(Convert.ToInt32(_wizard.GetRegistrationPersistenceValue("COUNTRYREGIONID")), _wizard.GetRegistrationPersistenceValue("ZIPCODE"));
                        if (regionid != null)
                        {
                            _wizard.Persistence["REGIONID"] = regionid.ID.ToString();
                        }
                    }

                    _wizard.Persist();
                    //g.Transfer(((string)Context.Items["FullURL"]).Replace("readit=1", "readit=0"));
                }

                // MPR-1734
                if (g.LayoutTemplate == Matchnet.Content.ValueObjects.PageConfig.LayoutTemplate.WideReset && _regScenario.Blank == true)
                {
                    g.Head20.IsBlank = true;
                    ((Matchnet.Web.LayoutTemplates.WideReset)g.LayoutTemplateBase).IsBlank = true;
                    // Footer20 depends on g.Head20.IsBlank 's value.
                }

                if (_regScenario.IsOnePageReg == true)
                {
                    g.Head20.IsOnePageReg = true;
                }

                // MPR-2243
                if (g.LayoutTemplate == Matchnet.Content.ValueObjects.PageConfig.LayoutTemplate.WideReset && _regScenario.GAMEnableOnly == true)
                {
                    g.Head20.GAMEnableOnly = true;
                }

                FrameworkGlobals.Trace(__debugTrace, "Registration.LoadStep", "Wizard and template loaded", _wizard.ToString());

                SetTemplate();

                _isOverlayReg = _wizard.RegistrationScenario.IsRegOverlay;
                bool isShowLabelInsideInput = false;
                g.LoggingManager.LogInfoMessage("Registration", "SessionID: " + g.Session.SessionID + " current step : " + _wizard.StepID.ToString() + " for " + _regScenario.Name);

                for (int k = 0; k < _regScenario.Steps.Count; k++)
                {
                    g.LoggingManager.LogInfoMessage("Registration", "SessionID: " + g.Session.SessionID + " Loading step : " + (k + 1).ToString() + " ControlCount: " + _regScenario.Steps[k].Controls.Count);
                    int controlsCreated = 0;

                    FrameworkGlobals.Trace(__debugTrace, "Registration.LoadStep", "Loading Step", _wizard.ToString());
                    for (int i = 0; i < _regScenario.Steps[k].Controls.Count; i++)
                    {


                        FrameworkGlobals.Trace(__debugTrace, "Registration.LoadStep", "Loading Controls for Step", "StepID=" + k.ToString() + ", ControlID=" + i.ToString());
                        bool show = true;
                        AttributeControl ctrl = (AttributeControl)_regScenario.Steps[k].Controls[i];
                        g.LoggingManager.LogInfoMessage("Registration", "SessionID: " + g.Session.SessionID + " Loading control : " + ctrl.AttributeName);
                        ctrl.IsSplash = IsSplash;
                        ctrl.LoadFromPersistence(_wizard.Persistence);
                        if (k + 1 != _wizard.RegistrationStep.ID)
                            show = false;
                        else
                        {
                            show = true;

                        }
                        FrameworkGlobals.Trace(__debugTrace, "Registration.LoadStep", "Loading Controls for Step", "StepID=" + k.ToString() + "ControlID=" + i.ToString() + ", " + ctrl.ToString());
                        if (ExecuteDisplayCondition(ctrl))
                        {
                            //_templateControl.ResourceControl = ResourceControl;
                            IAttributeControl attrcontrol = _templateControl.Add(ctrl, show, _isOverlayReg);
                            attrcontrol.ResourceControl = ResourceControl;
                            // litScript.Text = litScript.Text + "\r\n" + attrcontrol.ScriptBlock();

                            ctrl.OnePageReg = _regScenario.IsOnePageReg;

                            _controls.Add(ctrl);
                            controlsCreated += 1;

                        }
                        else
                        {
                            IAttributeControl attrcontrol = _templateControl.Add(ctrl, show);
                            attrcontrol.ResourceControl = ResourceControl;
                            // litScript.Text = litScript.Text + "\r\n" + attrcontrol.ScriptBlock();

                            _controls.Add(ctrl);

                        }

                        if (ctrl.ShowLabelInsideInput)
                        {
                            isShowLabelInsideInput = true;
                        }
                    }

                    if (controlsCreated == 0 && !_regScenario.Steps[k].IsFacebookReg && _wizard.ConditionalSkipSteps != null && String.IsNullOrEmpty(_regScenario.Steps[k].NextStepURL))
                    {
                        _wizard.ConditionalSkipSteps.Add(k + 1);
                    }

                }


                _templateControl.Wizard = _wizard;

                ContentPanel.Controls.Add(_templateControl);

                if (_wizard.SkipStep(_wizard.StepID))
                {
                    if (_wizard.StepID + 1 < _wizard.RegistrationScenario.Steps.Count)
                    {
                        _wizard.StepID += 1;
                        _wizard.Persist(_isFacebookReg, _originalScenarioName);
                    }

                }
                SetTemplateNavigation();

                if (isShowLabelInsideInput)
                {
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "ShowLabelInsideInput", "var ShowLabelInsideInput=true;", true);
                }


                Page.ClientScript.RegisterStartupScript(this.GetType(), "IsIFrame", "var IsIFrame=" + _wizard.RegistrationScenario.IsIFrame.ToString().ToLower() + ";", true);

                if (_wizard.RegistrationScenario.IsIFrame)
                {
                    _g.AnalyticsOmniture.DisableFireOmnitureCode = true;
                }

                if (!string.IsNullOrEmpty(_wizard.RegistrationScenario.RegCompleteDivID))
                {
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "RegCompleteDivID", "var RegCompleteDivID='" + _wizard.RegistrationScenario.RegCompleteDivID + "';", true);
                }

            }
            catch (Exception ex)
            {
                string x = ex.ToString();
                g.ProcessException(ex);
            }

        }

        private bool Valid()
        {
            bool allValid = true;
            for (int i = 0; i < _wizard.RegistrationStep.Controls.Count; i++)
            {

                FrameworkGlobals.Trace(__debugTrace, "Registration.Valid", "Validating Controls for Saving Step", "StepID=" + _wizard.RegistrationStep.ToString() + ", ControlID=" + i.ToString());

                AttributeControl ctrl = _wizard.RegistrationStep.Controls[i];


                IAttributeControl attr = (IAttributeControl)_templateControl.ContentPlaceholder.FindControl("id" + _wizard.RegistrationStep.Controls[i].Name);
                if (attr != null)
                {
                    string val = attr.GetValue();
                    bool valid = attr.Validate();
                    FrameworkGlobals.Trace(__debugTrace, "Registration.Valid", "Validating Control, valid=" + valid.ToString(), ctrl.ToString());
                    //attr.PersistToCookie(_wizard.PersistCookie);
                    attr.Persist(_wizard.Persistence);
                    if (valid)
                    {

                        if (!String.IsNullOrEmpty(ctrl.OnSubmit))
                            valid = ExecuteOnSubmit(ctrl, attr);

                        if (ctrl.SearchPref != SearchPrefType.No)
                        {
                            nvcSearchPreferencesParamaters.Add(attr.GetSearchPreferenceParamaterNVC());
                        }
                    }
                    else
                    {
                        _clientIDErrorMessage.Add(attr.ContainerDivClientID, attr.ValidationMessage.Text);
                    }

                    allValid = allValid & valid;
                }
            }

            return allValid;
        }




        private void SetTemplateNavigation()
        {
            try
            {
                if (_templateControl.FBRegistration != null)
                {
                    _templateControl.FBRegistration.ButtonFBRegister.Click += new System.EventHandler(this.btnFBRegister_OnClick);
                    _templateControl.FBRegistration.ButtonNoThanks.Click += new System.EventHandler(this.btnFBNoThanks_OnClick);
                }

                _templateControl.ButtonContinue.Click += new System.EventHandler(this.btnContinue_OnClick);
                if (_wizard.ShowPreviousStep() && _templateControl.ButtonPrevious != null && !_wizard.RegistrationStep.IsFacebookReg)
                {
                    _templateControl.ButtonPrevious.Visible = true;
                    _templateControl.ButtonPrevious.Click += new System.EventHandler(this.btnPrevious_OnClick);
                }
                else
                {
                    _templateControl.ButtonPrevious.Visible = false;
                }

                if (_wizard.StepID <= _wizard.CompletedSteps && _templateControl.ButtonNext != null && !_wizard.RegistrationStep.IsFacebookReg)
                {
                    _templateControl.ButtonNext.Visible = true;
                    _templateControl.ButtonNext.Click += new System.EventHandler(this.btnNext_OnClick);
                }

                _templateControl.ConfigureStep(_wizard.RegistrationStep);
            }
            catch (Exception ex)
            { }


        }

        private void SubmitStep()
        {
            bool skipStep = false;
            try
            {
                bool allValid = true;

                allValid = Valid();
                bool completed = false;
                if (allValid)
                {
                    FrameworkGlobals.Trace(__debugTrace, "Registration.SubmitStep", "All controls are valid", _wizard.ToString());
                    if (String.IsNullOrEmpty(_wizard.RegistrationStep.NextStepURL))
                    {
                        if (_wizard.CompletedSteps < _wizard.StepID)
                            _wizard.CompletedSteps += 1;
                        if (_wizard.StepID < _regScenario.Steps.Count)
                        {
                            _wizard.StepID += 1;

                        }
                        else
                            completed = true;

                        if (_wizard.SkipStep(_wizard.StepID))
                        {
                            skipStep = true;
                            if (_wizard.StepID + 1 > _wizard.RegistrationScenario.Steps.Count)
                                completed = true;
                            else
                            {
                                _wizard.StepID += 1;
                            }

                        }
                        FrameworkGlobals.Trace(__debugTrace, "RegistrationWizard.LoadRegistrationSteps", "About to persist to cookie", _wizard.ToString());
                        if (!String.IsNullOrEmpty(_wizard.GetRegistrationPersistenceValue("emailaddress")) && RegistrationCapture.IsRegistrationCaptureEnabled(g))
                        {
                            RegistrationCapture.UpdateRegistrationEventFromPersistence(_wizard.RegistrationEvent, _wizard.ExcludedFields, _wizard.Persistence);
                            RegistrationCapture.SaveRegistrationEvent(_wizard.RegistrationEvent, _wizard.Persistence);
                        }

                        _wizard.Persist(_isFacebookReg, _originalScenarioName);
                        //_wizard.PersistToCookie(_isFacebookReg, _originalScenarioName);
                    }
                    else
                    {
                        string url = _wizard.RegistrationStep.NextStepURL;
                        if (_controls.Count > 0)
                        {
                            _wizard.CompletedSteps = _wizard.StepID;
                            _wizard.StepID = _wizard.StepID + 1;
                            _wizard.StartStepID = _wizard.StepID;
                        }
                        _wizard.ScenarioName = "";

                        //_wizard.ScenarioFile = "";
                        FrameworkGlobals.Trace(__debugTrace, "RegistrationWizard.LoadRegistrationSteps", "NextStepURL empty", _wizard.ToString());
                        _wizard.Persist(_isFacebookReg, _originalScenarioName);
                        //_wizard.PersistToCookie(_isFacebookReg, _originalScenarioName);
                        url = AddSearchPreferencesParamatersToURL(url);
                        g.Transfer(url);

                    }
                    if (completed)
                    {
                        //need to update controls for the last step
                        FrameworkGlobals.Trace(__debugTrace, "Registration.SubmitStep", "Registration last step", _wizard.ToString());
                        int totalControls = _controls.Count;
                        int laststepControls = _wizard.RegistrationStep.Controls.Count;

                        int indLastStepControls = totalControls - laststepControls - 1;
                        if (indLastStepControls > 0 || _wizard.RegistrationScenario.IsOnePageReg)
                        {
                            if (_wizard.RegistrationScenario.IsOnePageReg)
                            {
                                indLastStepControls = 0;
                            }
                            for (int i = indLastStepControls; i < totalControls; i++)
                            {
                                _controls[i].LoadFromPersistence(_wizard.Persistence);
                            }
                        }
                        FrameworkGlobals.Trace(__debugTrace, "Registration.SubmitStep", "Registration last step - RegisterNewUser", _wizard.ToString());

                        int mid = RegisterNewUser(_wizard);
                        if (mid > 0)
                        {
                            FrameworkGlobals.Trace(__debugTrace, "Registration.SubmitStep", "Registration last step - RegisterNewUser - success", _wizard.ToString());
                            Member.ServiceAdapters.Member member = g.Member;
                            FrameworkGlobals.Trace(__debugTrace, "Registration.SubmitStep", "Registration last step - SaveMember", _wizard.ToString());

                            //Get Balckbox value from hidden field
                            IOBlackBoxValue = ioBlackBox.Value;
                            SaveMember(member);
                            // Member.ServiceAdapters.MemberSA.Instance.SaveMember(member);


                        }
                        else
                        {
                            if (_templateControl.ValidationMessageText.Text == string.Empty)
                            {
                                _templateControl.ValidationMessageText.Text = g.GetResource("ERR_SYSTEM", this); ;
                                _clientIDErrorMessage.Add("_ctl0__ctl2__ctl0__ctl0_idTermsAndConditions_divControl", g.GetResource("ERR_SYSTEM", this));
                            }
                            else
                            {
                                _clientIDErrorMessage.Add("_ctl0__ctl2__ctl0__ctl0_idTermsAndConditions_divControl", _templateControl.ValidationMessageText.Text);
                            }
                            _templateControl.ValidationMessageText.Visible = true;
                            if (skipStep)
                                _wizard.StepID -= 1;


                        }
                    }
                    else
                    {
                        _templateControl.ButtonNext.Visible = false;
                    }
                }

            }
            catch (System.Threading.ThreadAbortException ex)
            {

            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
                FrameworkGlobals.Trace(__debugTrace, "Registration.SubmitStep", "Exception: " + ex.ToString(), null);
                _templateControl.ValidationMessageText.Text = g.GetResource("ERR_SYSTEM", this); ;
                _templateControl.ValidationMessageText.Visible = true;
                if (skipStep)
                    _wizard.StepID -= 1;

                _clientIDErrorMessage.Add("_ctl0__ctl2__ctl0__ctl0_idTermsAndConditions_divControl", g.GetResource("ERR_SYSTEM", this));
            }

        }







        private void ShowStepControls()
        {
            _attributesNames = "";
            _templateControl.TitleResource = _wizard.RegistrationStep.ResourceConstant;
            _templateControl.TipResource = _wizard.RegistrationStep.TipResourceConstant;
            for (int k = 0; k < _regScenario.Steps.Count; k++)
            {
                for (int i = 0; i < _regScenario.Steps[k].Controls.Count; i++)
                {
                    IAttributeControl attribute = (IAttributeControl)_templateControl.FindControl("id" + _regScenario.Steps[k].Controls[i].Name);
                    if (attribute != null)
                    {
                        if (k + 1 != _wizard.StepID)
                        {
                            attribute.SetVisible(false);

                        }
                        else
                        {
                            if (ExecuteDisplayCondition(_regScenario.Steps[k].Controls[i]))
                            {
                                attribute.SetVisible(true);

                                if (!String.IsNullOrEmpty(_regScenario.Steps[k].Controls[i].AttributeName))
                                    _attributesNames += _regScenario.Steps[k].Controls[i].AttributeName;
                                else
                                    _attributesNames += _regScenario.Steps[k].Controls[i].Name;
                                if (i < _regScenario.Steps[k].Controls.Count - 1)
                                {
                                    _attributesNames += ",";
                                }
                                if (i == 0 && !_focusScriptReg)
                                {
                                    GetFocusScript(attribute.FocusControlClientID);
                                    _focusScriptReg = true;

                                }
                            }
                            else
                            {
                                attribute.SetVisible(false);
                                _regScenario.Steps[k].Controls[i].ConditionalShowFlag = false;
                            }
                            #region attempt to add JQuery validation
                            //IJQValidation validationInterface = null;
                            //try
                            //{
                            //    validationInterface = (IJQValidation)attribute;
                            //}
                            //catch (Exception ex)
                            //{ }
                            //if (validationInterface != null)
                            //{
                            //    idJQValidator.AddRule(validationInterface.InputFieldID, _regScenario.Steps[k].Controls[i].CreateValidationRule());
                            //    idJQValidator.AddMessage(validationInterface.InputFieldID, _regScenario.Steps[k].Controls[i].CreateValidationMessage(attribute.ResourceControl,g));
                            //}
                            #endregion

                            #region Adding Client side validation

                            if (attribute is IJQValidation)
                            {
                                jQueryValidation.AddValidationRules(((IJQValidation)attribute).GetValidationRules);
                            }

                            #endregion

                        }
                    }
                }


            }
            SetTemplateNavigation();


        }

        public void SaveStep()
        {

            bool allValid = true;
            for (int i = 0; i < _controls.Count; i++)
            {
                IAttributeControl attr = (IAttributeControl)_templateControl.FindControl("id" + _controls[i].Name);
                string val = attr.GetValue();
                bool valid = attr.Validate();
                if (valid)
                { attr.Persist(_wizard.Persistence); }
                allValid = valid;
            }

            if (allValid && _controls.Count > 0)
            {
                if (_wizard.CompletedSteps < _wizard.StepID)
                    _wizard.CompletedSteps += 1;
                _wizard.StepID += 1;
                _wizard.Persist(_isFacebookReg, _originalScenarioName);

            }

        }

        public bool ExecuteOnSubmit(AttributeControl ctrl, IAttributeControl control)
        {
            bool validated = true;
            if (ctrl.OnSubmit == "ValidateEmail")
            {
                validated = ValidateEmail(control.GetValue(), control);

            }
            else if (ctrl.OnSubmit == "ValidateDOB")
            {
                validated = ValidateBirthDate(control.GetValue(), control);
            }
            return validated;


        }

        public bool ExecuteDisplayCondition(AttributeControl ctrl)
        {

            if (String.IsNullOrEmpty(ctrl.DisplayConditionName))
            { return true; }

            string val = _wizard.GetProperty(ctrl.DisplayConditionName.ToLower());


            if (val.ToLower() == ctrl.DisplayConditionValue.ToLower())
            { return true; }
            else { return false; }

        }
        private bool ValidateEmail(string email, IAttributeControl control)
        {
            try
            {
                int id = MemberSA.Instance.GetMemberIDByEmail(email);


                if (id > 0)
                {
                    string errEmailExistsResourceConstant = "ERR_EMAIL_EXISTS";

                    //if (!string.IsNullOrEmpty(Request.QueryString[WebConstants.URL_PARAMETER_FACEBOOK_REGISTRATION]))
                    if (_isFacebookReg)
                    {
                        errEmailExistsResourceConstant += "_FB";
                    }
                    else if (_wizard.RegistrationScenario.IsOnePageReg)
                    {
                        errEmailExistsResourceConstant += "_ONE_PAGE_REG";
                    }
                    if (_templateControl.ValidationMessageText == null)
                        regValidation.Text = g.GetResource(errEmailExistsResourceConstant, this);
                    else
                    {
                        _templateControl.ValidationMessageText.Text = g.GetResource(errEmailExistsResourceConstant, this);
                        _templateControl.ValidationMessageText.Visible = true;
                    }

                    _clientIDErrorMessage.Add(control.ContainerDivClientID, g.GetResource(errEmailExistsResourceConstant, this));

                    return false;
                }

                DoNotEmailEntry entry = DoNotEmailSA.Instance.GetEntryBySiteAndEmailAddress(g.Brand.Site.SiteID, email);
                if (entry != null)
                {
                    if (_templateControl.ValidationMessageText == null)
                        regValidation.Text = g.GetResource("ERR_DNE", this);
                    else
                    {
                        _templateControl.ValidationMessageText.Text = g.GetResource("ERR_DNE", this);
                        _templateControl.ValidationMessageText.Visible = true;
                    }
                    return false;
                }
                else
                    return true;
            }
            catch (Exception ex)
            {
                FrameworkGlobals.Trace(__debugTrace, "ValidateEmail", "Exception:" + ex.ToString(), null);
                _templateControl.ValidationMessageText.Text = g.GetResource("ERR_SYSTEM", this); ;
                _templateControl.ValidationMessageText.Visible = true;
                _clientIDErrorMessage.Add(control.ContainerDivClientID, g.GetResource("ERR_DNE", this));

                g.ProcessException(ex);
                return false;
            }
        }

        private bool ValidateBirthDate(string dob, IAttributeControl ctrl)
        {
            DateTime dateofbirth = Conversion.CDateTime(dob);

            if (FrameworkGlobals.GetAge(dateofbirth) < 18)
            {
                ctrl.ValidationMessage.Text = g.GetResource("ERR_SHOULD_BE_18", this);
                ctrl.ValidationMessage.Visible = true;
                _clientIDErrorMessage.Add(ctrl.ContainerDivClientID, g.GetResource("ERR_SHOULD_BE_18", this));

                return false;
            }
            else
            {
                return true;
            }

        }




        private void checkForUserNameWarning()
        {
            //retrieveMemberRegistration(); // Update the member registration
            string username = GetAttributeStringFromControls("UserName");
            if (username != g.Member.GetUserName(_g.Brand))
            {
                _ShowUserNameWarning = true;
            }
        }






        public string GetClientScript()
        {
            string script = "";
            try
            {
                for (int i = 0; i < _controls.Count; i++)
                {
                    AttributeControl ctrl = _controls[i];
                    IAttributeControl attr = (IAttributeControl)_templateControl.ContentPlaceholder.FindControl("id" + _controls[i].Name);
                    script = script + "\r\n" + attr.ScriptBlock();

                }
                return script;
            }
            catch (Exception ex)
            {
                return script;
            }
        }


        public void GetFocusScript(string focusElementId)
        {
            System.Text.StringBuilder bld = new System.Text.StringBuilder();



            bld.Append("var element = document.getElementById(\"" + focusElementId + "\");\r\n");
            bld.Append("  if (element != null) {\r\n");
            bld.Append("    try{element.focus();}catch(err){} }\r\n");

            if (AjaxEnabled)
            {
                Anthem.Manager.RegisterStartupScript(this.Page.GetType(), "Focus", bld.ToString(), true);
            }
            else
            {
                Page.ClientScript.RegisterStartupScript(this.Page.GetType(), "Focus", bld.ToString(), true);
            }


        }

        private void RedirectToNewRegSiteBasedOnSetting()
        {
            //only redirect if enabled AND this is the reg pag, otherwise this will pick up splash page redirection as well

            if ((g.AppPage.PageName.ToLower().Contains("registration") || SettingsManager.GetSettingBool(SettingConstants.REGISTRATION_USE_NEW_SITE_FOR_ALL_PAGES, g.Brand)) &&
                SettingsManager.GetSettingBool(SettingConstants.REGISTRATION_USE_NEW_SITE, g.Brand))
            {
                bool qsAppended = false;
                StringBuilder regURL = new StringBuilder();

                if (g.AppPage.PageName.ToLower().Contains("registration") && !string.IsNullOrEmpty(SettingsManager.GetSettingString(SettingConstants.NEW_REG_SITE_DEFAULT_URL_WITH_REGISTRATION_PATH, g.Brand)))
                {
                    regURL.Append(SettingsManager.GetSettingString(SettingConstants.NEW_REG_SITE_DEFAULT_URL_WITH_REGISTRATION_PATH, g.Brand));
                }
                else
                {
                    regURL.Append(SettingsManager.GetSettingString(SettingConstants.NEW_REG_SITE_URL_WITH_REGISTRATION_PATH, g.Brand));
                }


                if (Request.UrlReferrer != null && !Request.UrlReferrer.ToString().ToLower().Contains(g.Brand.Uri.ToLower()))
                {
                    regURL.Append(regURL.ToString().Contains("?") ? "&" : "?");
                    regURL.Append(WebConstants.URL_REFERRER_TO_PASS + "=" + Server.UrlEncode(Request.UrlReferrer.AbsoluteUri) + "&");
                    qsAppended = true;
                }

                if (Request.QueryString.Count > 0)
                {
                    if (!qsAppended)
                    {
                        regURL.Append(regURL.ToString().Contains("?") ? "&" : "?");
                    }

                    foreach (var key in (Request.QueryString.AllKeys))
                    {
                        if (!Request.QueryString[key].Contains("404;"))
                        {
                            regURL.Append(key + "=" + Request.QueryString[key] + "&");
                        }
                    }
                }

                Response.Redirect(regURL.ToString());
            }
        }
    }
}
