﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Web.Framework;
using System.Xml.Serialization;
using Matchnet.Web.Framework.Ui.FormElements;
using Matchnet.Web.Framework.TemplateControls;
using Matchnet.Web.Interfaces;


namespace Matchnet.Web.Applications.Registration.Controls
{
    public partial class AttributeDropDownList : FrameworkControl, IAttributeControl
    {
        AttributeControl _attrControl;
        bool _useAttributeDescription;
        bool _includeBlanks;
        int _selectedValue = Constants.NULL_INT;

        FrameworkControl _resourceControl;

        public string ContainerDivClientID { get { return divControl.ClientID; } }

        public bool IsOverlayReg { get; set; }


        public FrameworkControl ResourceControl
        {
            get { return _resourceControl; }
            set { _resourceControl = value; }
        }

        public void Save()
        {

        }
        public ValidationMessage ValidationMessage { get { return txtValidation; } }
        public void SetValue(string value)
        {
            _selectedValue = Conversion.CInt(value);
            // AttributeOptions.SelectedValue = _selectedValue.ToString();
        }
        public string GetValue()
        {
            if (IsOverlayReg)
            {
                return Request[AttributeOptions.UniqueID];
            }
            else
            {
                return AttributeOptions.SelectedValue;
            }
        }
        public string ScriptBlock()
        {
            return "";
        }
        public string FocusControlClientID
        {
            get
            {
                try
                {
                    return AttributeOptions.ClientID;
                }
                catch (Exception ex)
                {
                    return "";
                }

            }
        }
        public void PersistToCookie(HttpCookie cookie)
        {
            cookie[_attrControl.AttributeName] = GetValue();
        }

        public void Persist(ITemporaryPersistence persistence)
        {
            persistence[_attrControl.AttributeName] = GetValue();
        }

        public bool Validate()
        {
            bool valid = false;

            if (!_attrControl.RequiredFlag)
                return true;

            _selectedValue = Conversion.CInt(AttributeOptions.SelectedValue);

            if (IsOverlayReg)
            {
                _selectedValue = Conversion.CInt(Request[AttributeOptions.UniqueID]);
            }
            if (_selectedValue <= 0 && !(_attrControl.AllowZeroValue && _selectedValue==0))
            {
                txtValidation.Visible = true;
                txtValidation.Text = _attrControl.GetValidationMessage(_attrControl.ErrorMessageRequired, g, _resourceControl);
            }
            else
            {
                txtValidation.Visible = false;
                txtValidation.Text = "";
                valid = true;
            }

            return valid;
        }
        public void SetVisible(bool visible)
        {
            if (!visible)
                divControl.Style.Add("display", "none");
            else
                divControl.Style.Add("display", "block");

        }


        public AttributeControl AttrControl
        { get { return _attrControl; } set { _attrControl = value; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (_attrControl.AttributeName.ToLower() == "promotionid")
            {
                if (g.Session.GetString(WebConstants.SESSION_PROPERTY_NAME_PROMOTIONID, null) != null)
                {
                    this.Visible = false;
                    return;
                }
            }

            txtCaption.Text = _attrControl.GetResourceString(_attrControl.ResourceConstant, g, _resourceControl);

            if (_attrControl.AttributeName.ToLower() == "height")
            {
                AttributeOptions.DataSource = FrameworkGlobals.CreateHeightListItems(g, false);
            }
            else if (_attrControl.AttributeName.ToLower() == "agerange")
            {

                AttributeOptions.DataSource = FrameworkGlobals.CreateAgeRangeListItems(g, true);
            }
            else
            {
                FrameworkControl ctlResource = null;
                if (_attrControl.AttributeName.ToLower() == "jdatereligion" && IsOverlayReg)
                {
                    ctlResource = ResourceControl;
                }
                AttributeOptions.DataSource = _attrControl.GetAttributeOptionCollectionAsListItems(g, ctlResource, false);
            }
            AttributeOptions.DataValueField = "Value";
            AttributeOptions.DataTextField = "Text";

            AttributeOptions.DataBind();

            if (_attrControl.ShowLabelInsideInput == true)
            {
                AttributeOptions.Items.Insert(0, new ListItem(txtCaption.Text, "-2"));
            }

            if (_selectedValue > 0 || (_attrControl.AllowZeroValue && _selectedValue==0))
            {
                if (AttributeOptions.Items.FindByValue(_selectedValue.ToString()) != null)
                {
                    AttributeOptions.SelectedValue = _selectedValue.ToString();
                }
            }
            else
            {
                AttributeOptions.SelectedValue = _attrControl.SelectedValue;
            }

            if (_attrControl.RequiredFlag)
            {
                AttributeOptions.CssClass += " mandatory";
            }
            else
            {
                AttributeOptions.CssClass += " optional";
            }

            divControl.Attributes["class"] = _attrControl.Name;

            divControl.Attributes.Add("CookieKey", _attrControl.AttributeName);
        }

        public NameValueCollection GetSearchPreferenceParamaterNVC()
        {
            NameValueCollection result = new NameValueCollection();
            string parameterName = _attrControl.AttributeName.ToLower();
            if (parameterName == "agerange")
            {
                parameterName = "AgeRangeID";
            }
            result.Add(parameterName, GetValue());
            return result;
        }

        protected string GetTitleWrapperOpen
        {
            get
            {
                return (_attrControl.OnePageReg) ? "<div class=\"label\">" : "<h2>";
            }
        }
        protected string GetTitleWrapperClose
        {
            get
            {
                return (_attrControl.OnePageReg) ? "</div>" : "</h2>";
            }
        }
        protected string GetFieldWrapperOpen
        {
            get { return (_attrControl.OnePageReg) ? "<div class=\"field\">" : string.Empty; }
        }
        protected string GetFieldWrapperClose
        {
            get { return (_attrControl.OnePageReg) ? "</div>" : string.Empty; }
        }
    }
}