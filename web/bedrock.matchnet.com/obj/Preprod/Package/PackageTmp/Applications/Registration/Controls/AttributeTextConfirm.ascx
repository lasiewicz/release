﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AttributeTextConfirm.ascx.cs" Inherits="Matchnet.Web.Applications.Registration.Controls.AttributeTextConfirm" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="uc2" TagName="ValidationMessage" Src="/Applications/Registration/Controls/ValidationMessage.ascx" %>

<div id="divControl" runat="server">

<uc2:ValidationMessage runat="server" ID="txtConfirm" Visible=false />

<label class="h2"><mn:txt runat="server" id="txtCaption" /></label>

<asp:TextBox  id="AttributeTextControl" runat="server" ></asp:TextBox>
<uc2:ValidationMessage runat="server" ID="txtValidation" Visible=false />
<br />
<label class="h2"><mn:txt runat="server" id="txtCaptionConfirm" /></label>

<asp:TextBox  id="AttributeTextControlConfirm" runat="server" ></asp:TextBox>
<uc2:ValidationMessage runat="server" ID="txtValidationConfirm" Visible=false />

</div>