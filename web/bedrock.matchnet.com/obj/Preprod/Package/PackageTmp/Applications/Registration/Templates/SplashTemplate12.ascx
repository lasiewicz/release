﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SplashTemplate12.ascx.cs" Inherits="Matchnet.Web.Applications.Registration.Templates.SplashTemplate12" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="uc1" TagName="ValidationMessage" Src="/Applications/Registration/Controls/ValidationMessage.ascx" %>
<%@ Register TagPrefix="mn" TagName="Copyright" Src="/Framework/Ui/PageElements/Copyright.ascx" %>
<%@ Register TagPrefix="mn1" TagName="Footer20" Src="/Framework/Ui/Footer20.ascx" %>
<%@ Register TagPrefix="mn" TagName="AdUnit" Src="/Framework/UI/Advertising/AdUnit.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ImageRotator" Src="/Framework/Ui/BasicElements/ImageRotator.ascx" %>

<div id="min-max-container">
	<div id="header" class="clearfix">
		<div id="header-logo">
			<p class="header-message"><mn:image id="imgLogo" runat="server" titleResourceConstant="TTL_SITE_LOGO" ResourceConstant="ALT_SITE_LOGO" FileName="trans.gif" NavigateURLClass="logo" /><mn:txt id="mntxt5508" runat="server" resourceconstant="TXT_HEADER_MESSAGE" expandimagetokens="true" />
			</p>
		</div>
		<ul id="header-nav">
			<li><a href="http://www.jdate.co.il/"></a><mn:txt id="mntxt1627" runat="server" resourceconstant="TXT_HEBREW" expandimagetokens="true" />
			</li>
			<li><a href="http://www.jdate.fr/"><mn:txt id="mntxt3959" runat="server" resourceconstant="TXT_FRENCH" expandimagetokens="true" />
			</a></li>
		</ul>
		<div id="header-login"><a href="/Applications/Logon/Logon.aspx"><mn:txt id="mntxt4348" runat="server" resourceconstant="TXT_LOGON" expandimagetokens="true" />
		</a></div>
	</div>
	<div id="content-container">
		<div id="content-main" class="clearfix">
			<div id="rotator">
				<div id="img_contents">
				    <mn:txt id="mntxt2864" runat="server" resourceconstant="HTML_CONTENT_PHOTO" expandimagetokens="true" />
				</div>
			</div>
			<div id="marketing-wrap">
				<mn:txt id="mntxt1638" runat="server" resourceconstant="HTML_MARKETING_CONTENT" expandimagetokens="true" />
				<div id="cta">
					<a href="/Applications/Membersonline/membersonline.aspx?regoverlay=true" class="link-primary"><mn:txt id="mntxt8138" runat="server" resourceconstant="TXT_REG_BUTTON" expandimagetokens="true" />
					</a>
				</div>
			</div>
		</div>
		<div id="vis-info" class="clear-both editorial">
            <mn:txt id="mntxt6945" runat="server" resourceconstant="TXT_SPLASH_SEO" expandimagetokens="true" />
		</div>
	</div>
	
</div>

<div style="width:100%;">

        <div id="footerContainer" runat="server">
            <div id="footer">
              <div class="footer-container"><mn1:Footer20 ID="Footer20" Runat="server" /></div>
            </div>
        </div>
        <div id="footer-narrow">
            <div class="footer-container"><mn:Copyright id="copyright" runat="server"></mn:Copyright></div>
        </div>    

    </div>



