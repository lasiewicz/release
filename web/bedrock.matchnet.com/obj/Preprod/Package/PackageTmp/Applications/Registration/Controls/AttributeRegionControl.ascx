﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AttributeRegionControl.ascx.cs"
    Inherits="Matchnet.Web.Applications.Registration.Controls.AttributeRegionControl" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="uc2" TagName="ValidationMessage" Src="/Applications/Registration/Controls/ValidationMessage.ascx" %>
<%@ Register TagPrefix="anthem" Namespace="Anthem" Assembly="Anthem" %>
<div id="divControl" runat="server">

    <script type="text/javascript">
	<asp:Literal ID="litShowCityLinkJS" Runat="server" Visible="True" />

	//Show "Look up City" link if a valid state/region is selected.  Otherwise hide it.
	function ShowCityLink()
	{
		var StateDropDownID = document.getElementById("StateDropDownID").value;
		var stateDropDown = document.getElementById(StateDropDownID);
		
		
		if(!document.getElementById('LookupCityLink'))
		return;
		
		var varLookupCityLink = document.getElementById('LookupCityLink');
		
		if (stateDropDown != null && stateDropDown.value != "")
		{
			varLookupCityLink.style.display = "block";
		}
		else
		{
			varLookupCityLink.style.display = "none";
		}
	}
    </script>

    <asp:PlaceHolder runat="server" ID="phPickRegion">
        <h2 class="region">
            <mn:Txt runat="server" ID="txtCaption" />
        </h2>
        <uc2:ValidationMessage runat="server" ID="txtValidation" Visible="false" />
        <div id="divCountry" runat="server">
            <h2>
                <mn:Txt ID="lblCountry" runat="server" ResourceConstant="DDL_COUNTRY" />
            </h2>
            <asp:DropDownList ID="country" AutoPostBack="True" DataTextField="description" EnableViewState="False"
                runat="server">
            </asp:DropDownList>
        </div>
        <div id="divState" runat="server">
            <h2>
                <mn:RegionLabel runat="server" ID="txtState" />
                <mn:Txt runat="server" ID="txtFRCityRegion" ResourceConstant="DDL_CITY" Visible="false" />
            </h2>
            <asp:DropDownList ID="state" AutoPostBack="True" DataTextField="description" EnableViewState="False"
                runat="server">
            </asp:DropDownList>
            <asp:DropDownList ID="frCityRegion" runat="server" AutoPostBack="true" EnableViewState="false"
                Visible="false">
                <asp:ListItem Text="Bordeaux" Value="a3483616" />
                <asp:ListItem Text="Grenoble" Value="b3483636" />
                <asp:ListItem Text="Lille" Value="c3483631" />
                <asp:ListItem Text="Lyon" Value="d3483636" />
                <asp:ListItem Text="Marseille" Value="e3483633" />
                <asp:ListItem Text="Montpellier" Value="f3483627" />
                <asp:ListItem Text="Nice" Value="g3483635" />
                <asp:ListItem Text="Paris" Value="h3483626" />
                <asp:ListItem Text="Strasbourg" Value="i3483615" />
                <asp:ListItem Text="Toulouse" Value="j3483624" />
                <%--<asp:ListItem Text="Autre ville..." Value="-1" />--%>
            </asp:DropDownList>
        </div>
        <div id="divCity" runat="server">
            <h2>
                <mn:Txt ID="Txt1" runat="server" ResourceConstant="DDL_CITY" />
            </h2>
            <asp:TextBox ID="city" runat="server" ReadOnly="False"></asp:TextBox>
            <a onclick="javascript:popCityList();" href="#" id="LookupCityLink">
                <mn:Txt ID="lblLookUpCity" runat="server" Visible="true" ResourceConstant="LOOK_UP_CITY">
                </mn:Txt>
            </a>
        </div>
        <div id="divPostalCode" runat="server">
            <mn:Txt ID="TxtZipPostalCode" runat="server" />
            <h2>
                <mn:Txt ID="lblPostal" runat="server" />
            </h2>
            <label id="lblCallBackMsg" style="display: none;">
                <em class="error">
                    <mn:Txt runat="server" ResourceConstant="TXT_CALLBACK_MSG" ID="Txt3" NAME="Txt2">
                    </mn:Txt>
                </em>
            </label>
            <anthem:TextBox ID="postalCode" runat="server" EnabledDuringCallBack="True" AutoPostBack="false"
                AutoUpdateAfterCallBack="false" AutoCallBack="true" ReadOnly="False" PreCallBackFunction="postalCode_PreCallBack"
                PostCallBackFunction="postalCode_PostCallBack"></anthem:TextBox>
        </div>
        <div id="divCities" runat="server" class="anthem-box">
            <h2>
                <mn:Txt ID="lblCity" runat="server" ResourceConstant="DDL_CITY" />
            </h2>
            <anthem:DropDownList ID="ddlCities" runat="server" AutoPostBack="false" AutoCallBack="false">
            </anthem:DropDownList>
        </div>
    </asp:PlaceHolder>
</div>
