﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SplashTemplate8CollageWithControls.ascx.cs" Inherits="Matchnet.Web.Applications.Registration.Templates.SplashTemplate8CollageWithControls" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="uc1" TagName="ValidationMessage" Src="/Applications/Registration/Controls/ValidationMessage.ascx" %>
<%@ Register TagPrefix="mn" TagName="Copyright" Src="/Framework/Ui/PageElements/Copyright.ascx" %>
<%@ Register TagPrefix="mn1" TagName="Footer20" Src="/Framework/Ui/Footer20.ascx" %>
<%@ Register TagPrefix="mn" TagName="AdUnit" Src="/Framework/UI/Advertising/AdUnit.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ImageRotator" Src="/Framework/Ui/BasicElements/ImageRotator.ascx" %>

<div id="min-max-container">
	<div id="header" class="clearfix">
		<div id="header-logo">
			<p class="header-message"><mn:image id="imgLogo" runat="server" titleResourceConstant="TTL_SITE_LOGO" ResourceConstant="ALT_SITE_LOGO" FileName="trans.gif" NavigateURLClass="logo" /><mn:txt id="mntxt5508" runat="server" resourceconstant="TXT_HEADER_MESSAGE" expandimagetokens="true" />
			</p>
		</div>
		<ul id="header-nav">
			<li><a href="http://www.jdate.co.il/"><mn:txt id="mntxt1627" runat="server" resourceconstant="TXT_HEBREW" expandimagetokens="true" /></a>
			</li>
			<li><a href="http://www.jdate.fr/"><mn:txt id="mntxt3959" runat="server" resourceconstant="TXT_FRENCH" expandimagetokens="true" />
			</a></li>
		</ul>
		<div id="header-login"><a href="/Applications/Logon/Logon.aspx"><mn:txt id="mntxt4348" runat="server" resourceconstant="TXT_LOGON" expandimagetokens="true" />
		</a></div>
	</div>
</div>
<div id="container">
	<div id="container-members">
		<div class="marketing-container">
			<div class="marketing-content">
				<div id="marketing-wrap">
					<big>Meet Jewish Singles</big>		
<fieldset class='primary-content <asp:Literal id='litCSSClass' runat=server/> ' > 
    <asp:PlaceHolder ID="phContent" runat="server">
        <uc1:ValidationMessage runat="server" ID="txtValidation" Visible=false />
    </asp:PlaceHolder>

    <ul id="splash-bullets" style="margin-bottom:8px;">
        <li><mn:Txt ID="Txt4" runat="server" ResourceConstant="TXT_MARKETING_COPY_ONE" ExpandImageTokens="True" /></li>
        <li><mn:Txt ID="Txt5" runat="server" ResourceConstant="TXT_MARKETING_COPY_TWO" ExpandImageTokens="True" /></li>
        <li><mn:Txt ID="Txt6" runat="server" ResourceConstant="TXT_MARKETING_COPY_THREE" ExpandImageTokens="True" />
            <a href="/Applications/MembersOnline/MembersOnline.aspx" class="mol-count">
                <mn:Txt ID="txtMOLCount" runat="server" />
            </a>
            <mn:Txt ID="Txt7" runat="server" ResourceConstant="TXT_MARKETING_COPY_FOUR" ExpandImageTokens="True" />
        </li>
    </ul>

<%--    <!-- Begin Amadesa Element cta Tag -->
    <asp:Panel runat="server" ID="pnlJSCta" CssClass="amadesa-tag">
        <script language="javascript" type="text/javascript">liveExperience('cta');</script>
    </asp:Panel>--%>
    <div id="cta">
        <div class="cta">
            <mn2:FrameworkButton runat="server" ID="btnContinue" ResourceConstant="TXT_CONTINUE_SPLASH"  CssClass="btn primary xlarge" />
        </div>
    </div>
<%--    </noscript>
    <!-- End Amadesa Element cta Tag -->   --%>
</fieldset>					
				</div>
			</div>
		</div>
		<ul id="couples">
		    <li id="splash-ourstories">
		       <a href="/Applications/Video/VideoCommercialCenter.aspx?NavPoint=top" title="Our Stories Began on JDate"><img src="../img/Community/JDate/splash-ourstories.jpg" alt="Our Stories Began on JDate" width="487" height="97" border="0" /></a>
		    </li>
			<li id="couple1" onmouseover="ddrivetip('Sara &amp; Steve', 'Married December  2010');" onmouseout="hideddrivetip();"><a href="http://www.jdate.com/jmag/2010/02/sara-and-steve/"></a></li>
			<li id="couple2" onmouseover="ddrivetip('Samara &amp; Kiva', 'Married April 2010');" onmouseout="hideddrivetip();"><a href="http://www.jdate.com/jmag/2010/05/samara-and-kiva/"></a></li>
			<li id="couple3" onmouseover="ddrivetip('Lindsay &amp; Ilan', 'Engaged August 2010');" onmouseout="hideddrivetip();"><a href="http://www.jdate.com/jmag/2010/10/lindsay-and-ilan/"></a></li>
			<li id="couple4" onmouseover="ddrivetip('Rebecca &amp; Philip', 'Married June 2012');" onmouseout="hideddrivetip();"><a href="http://www.jdate.com/jmag/2012/04/rebecca-and-philip/"></a></li>
			<li id="couple5" onmouseover="ddrivetip('Karinne &amp; Amos', 'Married July 2008');" onmouseout="hideddrivetip();"><a href="http://www.jdate.com/jmag/2010/03/karinne-and-amos/"></a></li>
			<li id="couple6" onmouseover="ddrivetip('Nicole &amp; Michael', 'Married September 2009');" onmouseout="hideddrivetip();"><a href="http://www.jdate.com/jmag/2010/06/nicole-and-michael/"></a></li>
			<li id="couple7" onmouseover="ddrivetip('Rachel &amp; Mark', 'Engaged December 2010');" onmouseout="hideddrivetip();"><a href="http://www.jdate.com/jmag/2010/12/rachel-and-mark/"></a></li>
			<li id="couple8" onmouseover="ddrivetip('Ronni &amp; John', 'Married October 2009');" onmouseout="hideddrivetip();"><a href="http://www.jdate.com/jmag/2010/01/ronni-and-john/"></a></li>
			<li id="couple9" onmouseover="ddrivetip('Bonnie &amp; Mike', 'Married March 2008');" onmouseout="hideddrivetip();"><a href="http://www.jdate.com/jmag/2009/12/bonnie-and-mike/"></a></li>
			<li id="couple10" onmouseover="ddrivetip('Sarah &amp; Zack', 'Engaged September 2010');" onmouseout="hideddrivetip();"><a href="http://www.jdate.com/jmag/2011/04/sarah-and-zack/"></a></li>
			<li id="couple11" onmouseover="ddrivetip('JBaby Jacob', 'Born May 2009');" onmouseout="hideddrivetip();"><a href="http://www.jdate.com/jmag/2009/04/lesley-and-jonathan/"></a></li>
			<li id="couple12" onmouseover="ddrivetip('Stephanie &amp; Derek', 'Married September 2009');" onmouseout="hideddrivetip();"><a href="http://www.jdate.com/jmag/2010/01/stephanie-and-derek/"></a></li>
			<li id="couple13" onmouseover="ddrivetip('Frannie &amp; Dani', 'Married July 2008');" onmouseout="hideddrivetip();"><a href="http://www.jdate.com/jmag/2010/04/frannie-and-dani/"></a></li>
			<li id="couple14" onmouseover="ddrivetip('Stacey &amp; Daniel', 'Married April 2009');" onmouseout="hideddrivetip();"><a href="http://www.jdate.com/jmag/2009/11/stacey-and-daniel/"></a></li>
			<li id="couple15" onmouseover="ddrivetip('Melissa &amp; Jonathan ', 'Married March 2010');" onmouseout="hideddrivetip();"><a href="http://www.jdate.com/jmag/2009/09/melissa-and-jonathan/"></a></li>
			<li id="couple16" onmouseover="ddrivetip('Davina &amp; Micah', 'Engaged August 2010');" onmouseout="hideddrivetip();"><a href="http://www.jdate.com/jmag/2011/01/davina-and-micah/"></a></li>
			<li id="couple17" onmouseover="ddrivetip('JBaby Jules', 'Born April 2009');" onmouseout="hideddrivetip();"><a href="http://www.jdate.com/jmag/2009/05/rachael-and-paul/"></a></li>
			<li id="couple18" onmouseover="ddrivetip('Elana &amp; Barry', 'Engaged December 2010');" onmouseout="hideddrivetip();"><a href="http://www.jdate.com/jmag/2011/01/elana-and-barry/"></a></li>
			<li id="couple19" onmouseover="ddrivetip('Tali &amp; Matthew', 'Engaged April 2009');" onmouseout="hideddrivetip();"><a href="http://www.jdate.com/jmag/2010/02/tali-and-matthew/"></a></li>
			<li id="couple20" onmouseover="ddrivetip('Jacquie &amp; Michael', 'Engaged June 2010');" onmouseout="hideddrivetip();"><a href="http://www.jdate.com/jmag/2011/01/jacquie-and-michael/"></a></li>
		</ul>
	</div>
</div>

<div style="margin:1.5em auto 1em;width:1014px;">
		    <div class="clearfix">
		        <div id="mediaLogosHalfWidth">
		            <p class="text-center"><strong>Recent JDate Success Stories have been featured in:</strong></p> 
		            <mn:image id="mnimage3087" runat="server" filename="splash-mediahalf-logos.jpg" titleresourceconstant="" resourceconstant="" />
                </div>
		        <div id="jdvideo">
		            <a href="/Applications/Video/VideoCommercialCenter.aspx?NavPoint=top" title="JDate Video"><img src="../img/Community/JDate/splash-video-icon.jpg" alt="JDate Video" width="106" height="87" border="0" class="video-icon" /></a>
		            <p class="text-title">Watch the Full Versions of “Our Story Began on JDate”</p>
		            <p class="text-body">You’ve seen the previews on TV, but only here can you watch the full stories of real couples who met on JDate. <a href="/Applications/Video/VideoCommercialCenter.aspx?NavPoint=top" title="Watch Now">Watch now &raquo;</a></p>
		        </div>
            </div>            
	<div id="content-container"><%--
		<div id="content-main" class="clearfix" style="display:none;">
			<div id="rotator">
				<div id="img_contents">
				    <mn:txt id="mntxt2864" runat="server" resourceconstant="HTML_CONTENT_PHOTO" expandimagetokens="true" />
				</div>
			</div>
			<div id="marketing-wrap">
				<mn:txt id="mntxt1638" runat="server" resourceconstant="HTML_MARKETING_CONTENT" expandimagetokens="true" />
                <ul id="splash-bullets">
                    <li><mn:Txt ID="Txt4" runat="server" ResourceConstant="TXT_MARKETING_COPY_ONE" expandImageTokens="True"/></li>
                    <li><mn:Txt ID="Txt5" runat="server" ResourceConstant="TXT_MARKETING_COPY_TWO" expandImageTokens="True"/></li>
                    <li><mn:Txt ID="Txt6" runat="server" ResourceConstant="TXT_MARKETING_COPY_THREE" expandImageTokens="True"/> <a href="/Applications/MembersOnline/MembersOnline.aspx" class="mol-count"><mn:Txt ID="txtMOLCount" runat="server" /></a> <mn:Txt ID="Txt7" runat="server" ResourceConstant="TXT_MARKETING_COPY_FOUR"  expandImageTokens="True"/></li>
                </ul>
				<div id="cta">
					<a href="/Applications/Registration/Registration.aspx" class="link-primary"><mn:txt id="mntxt8138" runat="server" resourceconstant="TXT_REG_BUTTON" expandimagetokens="true" />
					</a>
				</div>
			</div>
		</div>--%>
		
<div id="vis-info" class="clear-both">

<div id="footernew" class="clear-both clearfix">
<div id="jd-links">
    <big>JDate.com</big>
<ul class="linklist clearfix">
    <li><a href="http://www.jdate.com/Applications/Home/Default.aspx">Home</a></li>
    <li><a href="/Applications/Logon/Logon.aspx">Login</a></li>
    <li><a href="/Applications/ContactUs/ContactUs.aspx">Contact Us</a></li>
    <li><a href="/Applications/MemberServices/MemberServices.aspx">Your Account</a></li>
    <li><a href="/Applications/MemberServices/VerifyEmail.aspx">Verify Email</a></li>
    <li><a href="https://www.jdate.com/Applications/Subscription/Subscribe.aspx?prtid=16">Subscribe</a></li>
	<li><a href="http://static.JDate.com/Campaigns/MK07-0110_Gift_of_JDate/">Gift of JDate</a></li>
</ul>
<ul class="linklist clearfix">
	<li><a href="/Applications/Article/ArticleView.aspx?CategoryID=1998&amp;HideNav=True">About JDate</a></li>
	<li><a href="/Applications/Article/ArticleView.aspx?CategoryID=2060&amp;HideNav=true">Our Mission</a></li>
	<li><a href="/safety" target="_blank">Safety</a></li>
	<li><a href="/Applications/Video/VideoCenter.aspx">Video</a></li>
	<li><a href="http://www.JDate.com/jmag">JMag</a></li>
	<li><a href="http://www.JDate.com/blog/">JBlog</a></li>
	<li><a href="http://www.jdatemobile.com">JDate Mobile</a></li>
</ul>
<ul class="linklist clearfix">
	<li><a href="/Applications/Events/Events.aspx">Travel &amp; Events</a></li>
	<li><a href="http://www.JDate.com/synagoguesdirectory">Synagogue Directory</a></li>
    <li><a href="http://www.JDate.com/jewish-holidays/">Jewish Holiday Calendar</a></li>
	<li><a href="/Applications/Article/FAQMain.aspx">Help &amp; Advice</a></li>
	<li><a href="/Applications/SiteMap/SiteMap.aspx">Site Map</a></li>
	<li><a href="http://www.JDate.com/blog/2011-09/jdate-infographic/">JDate Success</a></li>
</ul>
</div>
<div id="sn-links">
    <big>Spark Networks</big>
    <ul class="linklist clearfix">
	    <li><a href="http://www.spark.net/about.htm">About Spark Networks</a></li>
	    <li><a href="http://www.spark.net/our-portfolio/portfolio-overview/">Spark Networks' Sites</a></li>
	    <li><a href="http://www.spark.net/advertise.asp" >Advertise With Us</a></li>
	    <li><a href="http://affiliates.spark.net/">Affiliate Program</a></li>
	    <li><a href="http://www.spark.net/investor.htm">Investor Relations</a></li>
	    <li><a href="http://www.spark.net/careers.htm">Jobs</a></li>
	</ul>
	<ul class="linklist clearfix">
	    <li><a href="/Applications/Article/ArticleView.aspx?CategoryID=1948&amp;ArticleID=6498&amp;HideNav=True#privacy">Privacy</a></li>
	    <li><a href="/Applications/Article/ArticleView.aspx?CategoryID=1948&amp;ArticleID=6498&amp;HideNav=True#service">Terms of Service</a>  </li>
        <li><a href="/Applications/Article/ArticleView.aspx?CategoryID=1948&amp;ArticleID=6498&amp;HideNav=True#service">Terms of Service</a>  </li>
	    <li><a href="http://search.JDate.com/">Local Online Dating</a></li>
	    <li><a target="_blank" href="http://www.JDate.com/singles/">Jewish Singles</a></li>
	    <li><a href="http://www.spark.net/intellectual.htm">Our Intellectual Property </a></li>
    </ul>
</div>
    <div id="footernewfooter" class="clearfix">
        <p class="legal-copyright editorial">Copyright &copy; 2012 Spark Networks&reg; USA, LLC. All rights reserved. Spark Networks USA, LLC is a wholly-owned subsidiary of Spark Networks, Inc., a NYSE MKT Company (<a target="_blank" href="http://www.nyse.com/about/listed/lcddata.html?ticker=LOV">LOV</a>)</p>
<%--        <div id="socialbuttons">
            <a id="buttonTwitter" href="http://www.Twitter.com/JDate" target="_blank"></a>
            <a id="buttonFacebook" href="http://www.Facebook.com/JDate" target="_blank"></a>
        </div>--%>
    </div>
</div>
		<div class="editorial">
            <mn:txt id="mntxt6945" runat="server" resourceconstant="TXT_SPLASH_SEO" expandimagetokens="true" /></div>
		</div>
	</div>
</div>
<div id="footer">
	<div id="footer-content">
	    <p class="legal-background-checks"><b>Spark Networks USA, LLC does not conduct background checks on the members or subscribers of this website.</b></p>
	</div>
</div>

<%--<div id="footer">
	<div id="footer-content">
		<mn1:Footer20 ID="Footer20" Runat="server" />
	</div>
</div>--%>
<!--div to put member info inside-->
<div class="couple-callout" id="callout"></div>
<script type="text/javascript">
var offsetxpoint=6; //Customize x offset of tooltip
var offsetypoint=6; //Customize y offset of tooltip
var ie=document.all;
var ns6=document.getElementById && !document.all;
var enabletip=false;
var tipobj = null;

function ietruebody()
{
	return (document.compatMode && document.compatMode!="BackCompat")? document.documentElement : document.body;
}

function ddrivetip(coupleName, coupleMarriedText)
{
	if (ns6||ie)
	{
		tipobj=document.all? document.all["callout"] : document.getElementById? document.getElementById("callout") : "";
		
		tipobj.style.display="block";
		tipobj.innerHTML="<strong>" + coupleName + "</strong><br /><span style='font-size:10px; color:#666666;'>" + coupleMarriedText + "</span>";
		enabletip=true;
		return false;
	}
}

function positiontip(e)
{
	if (enabletip)
	{
		var curX=(ns6)?e.pageX : event.clientX+ietruebody().scrollLeft;
		var curY=(ns6)?e.pageY : event.clientY+ietruebody().scrollTop;

		var rightedge=ie&&!window.opera? ietruebody().clientWidth-event.clientX-offsetxpoint : window.innerWidth-e.clientX-offsetxpoint-20;
		var bottomedge=ie&&!window.opera? ietruebody().clientHeight-event.clientY-offsetypoint : window.innerHeight-e.clientY-offsetypoint-20;
		
		var leftedge=(offsetxpoint<0)? offsetxpoint*(-1) : -1000;
		
		if (rightedge<tipobj.offsetWidth)
		{
			tipobj.style.left=ie? ietruebody().scrollLeft+event.clientX-tipobj.offsetWidth+"px" : window.pageXOffset+e.clientX-tipobj.offsetWidth+"px";
		}
		else if (curX<leftedge)
		{
			tipobj.style.left="5px";
		}
		else
		{
			tipobj.style.left=curX+offsetxpoint+"px";
		}

		if (bottomedge<tipobj.offsetHeight)
		{
			tipobj.style.top=ie? ietruebody().scrollTop+event.clientY-tipobj.offsetHeight-offsetypoint+"px" : window.pageYOffset+e.clientY-tipobj.offsetHeight-offsetypoint+"px";
		}
		else
		{
			tipobj.style.top=curY+offsetypoint+"px";
			tipobj.style.display="block";
		}
	}
}

function hideddrivetip()
{
	if (ns6||ie)
	{
		enabletip=false;
		tipobj.style.display="none";
		tipobj.style.left="-1000px";
		tipobj.style.backgroundColor='';
		tipobj.style.width='';
	}
}

document.onmousemove=positiontip;

var resetImageID;
var coordImageID;

var hoverImageCoord = new Array("-8px -491px", "-229px -491px", "-346px -491px", "-462px -491px", "-765px -491px", "-890px -491px", "-229px -567px", "-462px -578px", "-8px -643px", "-136px -643px", "-229px -685px", "-346px -657px", "-504px -772px", "-622px -772px", "-771px -772px", "-8px -873px", "-109px -873px", "-208px -873px", "-444px -873px", "-622px -873px");
var resetImageCoord=new Array("-8px -8px","229px 8px","346px 8px","462px 8px","765px 8px","890px 8px","229px 84px","462px 95px","8px 160px","136px 160px","229px 202px","346px 174px","504px 289px","622px 289px","771px 289px","8px 390px","109px 390px","208px 390px","444px 390px","622px 390px");

function resetImage()
{	
	if (resetImageID != null)
	{
		var ulImage = document.getElementById("couples");
		ulImage.style.backgroundPosition = "0px 0px";
		
		element = document.getElementById(resetImageID);
		element.style.backgroundImage = "url(../img/Community/JDate/splash-collage-couples-grid.jpg)";
		element.style.backgroundRepeat = "no-repeat";
		element.style.backgroundPosition = resetImageCoord[coordImageID];
		//element.style.backgroundPosition = "0px 0px";
		//alert(element.style.backgroundPosition);
	}
}

function TimedImage()
{
  resetImage();
  //slide(timedNum,'mypic10','mylbl10');
  var randomnumber=Math.floor(Math.random()*20);
  coordImageID = randomnumber == 0 ? 0 : randomnumber-1;
  //alert(coordImageID);
  //dont want zero, so modify if 0 is encountered
  randomnumber = randomnumber == 0 ? 1 : randomnumber;
  
  var coupleID = "couple" + randomnumber;
  resetImageID = coupleID;
  var element = document.getElementById(coupleID);
  element.style.backgroundImage = "url(../img/Community/JDate/splash-collage-couples-grid.jpg)";
  element.style.backgroundRepeat = "no-repeat";
  //element.style.backgroundPosition = "-346px -483px";
  element.style.backgroundPosition = hoverImageCoord[coordImageID];
  //alert(coupleID);
}

var timedFunc = '';
function InitTimeInterval() {
  timeFunc = setInterval("TimedImage()", 2000);
} 
</script>


<!-- Google Code for Spark.com - Test D Remarketing List --> 
<script type="text/javascript"> 
/* <![CDATA[ */ 
var google_conversion_id = 1029757028; 
var google_conversion_language = "en"; 
var google_conversion_format = "3"; 
var google_conversion_color = "666666"; 
var google_conversion_label = "PiMQCK6O2AEQ5LCD6wM"; 
var google_conversion_value = 0; 
/* ]]> */ 
</script> 
<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js"> 
</script> 
<noscript> 
<div style="display:inline;"> 
<img height="1" width="1" style="border-style:none;" alt="" src="http://www.googleadservices.com/pagead/conversion/1029757028/?label=PiMQCK6O2AEQ5LCD6wM&amp;guid=ON&amp;script=0"/> 
</div> 
</noscript>



<script type="text/javascript">
    document.onload = InitTimeInterval();

    document.getElementById("_ctl0__ctl2__ctl0__ctl0_btnContinue").setAttribute('onclick', '');

    /* functions from spark.js, which is not loaded in this page */
    function RemoveFrames() {
        if (self.location != top.location) top.location = self.location;
    }

</script>
