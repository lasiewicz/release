﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Web;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Ui.BasicElements;
using Matchnet.Web.Applications.Registration.Controls;

namespace Matchnet.Web.Applications.Registration.Templates
{
    public partial class SplashTemplate5 : RegistrationTemplateBase, IRegistrationTemplate
    {
        private const string IMG_LOGO_FILE_NAME = "logo-header-img.png";

        public override PlaceHolder ContentPlaceholder { get { return phContent; } }
        public override Literal StepCSSClassLiteral
        { get { return litCSSClass; } }
        public override System.Web.UI.WebControls.Button ButtonContinue
        { get { return btnContinue; } }
        public override ValidationMessage ValidationMessageText
        { get { return txtValidation; } }
        protected override void OnInit(EventArgs e)
        {
            //txtTitle.Text = GetResourceString(TitleResource, ResourceControl);
            btnContinue.ResourceControl = ResourceControl;

            #region Moved from WideSimple.ascx.cs
            CustomizeTemplate();
            imgLogo.NavigateUrl = FrameworkGlobals.GetHomepageAbsURL(g, Request.ServerVariables.Get("HTTP_HOST"));

            if (g.Brand.Site.Community.CommunityID != (int)WebConstants.COMMUNITY_ID.JDate)
            {
                ulJdateSiteLinks.Visible = false;
            }
            #endregion

            imageRotator.ResourceControl = this;

            //Pass the destinationurl if it came with one
            string logonHref = "/Applications/Logon/Logon.aspx";

            if (Request.Params["destinationurl"] != null)
                logonHref = logonHref + "?destinationurl=" + Request.Params["destinationurl"];

            txtLogon.Href = logonHref;
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Convert.ToBoolean(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("AMADESA_SITE_ENABLE", _g.Brand.Site.Community.CommunityID, _g.Brand.Site.SiteID)) ||
               Convert.ToBoolean(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("DISABLE_SPLASH_LIVE_EXPERIENCE", _g.Brand.Site.Community.CommunityID, _g.Brand.Site.SiteID)))
            {
                pnlJSCta.Visible = false;
                pnlJSHeadline.Visible = false;
            }

            litSiteCss.Text = String.Format("/css/style.{0}.css", _g.Brand.Site.SiteID);
            txtMOLCount.Text = Matchnet.Session.ServiceAdapters.SessionSA.Instance.GetSessionCount(g.Brand.Site.Community.CommunityID).ToString();

            #region Moved from WideSimple.ascx.cs
            string imagePath = Matchnet.Web.Framework.Image.GetURLFromFilename(IMG_LOGO_FILE_NAME);
            if (g.BrandAlias != null)
            {
                imagePath = Matchnet.Web.Framework.Image.GetBrandAliasImagePath(IMG_LOGO_FILE_NAME, g.BrandAlias);
            }

            string host = HttpContext.Current.Request.Url.Host;
            string url = "http://" + host + "/default.aspx";
            imgLogo.NavigateUrl = url;
            imgLogo.ImageUrl = imagePath;
            #endregion
        }
        private void CustomizeTemplate()
        {

            if (g.AppPage.ControlName.ToLower() == "splash20")
            {
                adSplashLeaderboard.GAMAdSlot = "splash_bottom_728x90";
                copyright.Visible = false;
                liAlreadyAMember.Visible = false;

                if (g.Brand.Site.Community.CommunityID != (int)WebConstants.COMMUNITY_ID.JDate)
                {
                    ulJdateSiteLinks.Visible = false;
                }
            }
            // We no longer need this because this method was moved from WideSimple layout template. We are already inside the Splash
            // page at this point, so we don't have to support this logic.  Instead, Registration page is missing some header and footer
            // components.
            //else if (g.AppPage.ControlName.ToLower() == "registration")
            //{
            //    divHeaderMsg.Visible = false;
            //    liFrench.Visible = false;
            //    liHebrew.Visible = false;
            //    footerContainer.Visible = false;
            //    txtSeo.Visible = false;
            //    liAlreadyAMember.Visible = true;
            //}
        }
    }
}