﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Web.Applications.Registration.Controls;

namespace Matchnet.Web.Applications.Registration.Templates
{
    public partial class SplashTemplate2 : RegistrationTemplateBase, IRegistrationTemplate
    {
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            xtxtTitle.Text = GetResourceString(TitleResource, ResourceControl);
            xbtnContinue.ResourceControl = ResourceControl;

            //imageRotator.ResourceControl = this;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Convert.ToBoolean(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("AMADESA_SITE_ENABLE", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID)) ||
                Convert.ToBoolean(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("DISABLE_SPLASH_LIVE_EXPERIENCE", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID)))
            {
                xpnlJSCta.Visible = false;
                xpnlJSHeadline.Visible = false;
            }
        }

      
        public override System.Web.UI.WebControls.Button ButtonContinue
        { get { return xbtnContinue; } }
        public override ValidationMessage ValidationMessageText
        { get { return xtxtValidation; } }

        #region IRegistrationTemplate Members

        public override PlaceHolder ContentPlaceholder { get { return xphContent; } }

        #endregion
    }
}