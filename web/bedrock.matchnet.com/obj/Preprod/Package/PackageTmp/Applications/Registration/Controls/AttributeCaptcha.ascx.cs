﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Web.Framework;
using System.Xml.Serialization;
using Matchnet.Web.Framework.Ui.FormElements;
using Matchnet.Web.Framework.TemplateControls;
using Matchnet.Member.ValueObjects;
using Matchnet.Member.ValueObjects.Photos;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Session.ValueObjects;
using Matchnet.Security;
using Matchnet.Web.Framework.Util;
using Matchnet.Web.Interfaces;

namespace Matchnet.Web.Applications.Registration.Controls
{
    public partial class AttributeCaptcha : FrameworkControl, IAttributeControl, IJQValidation
    {
        AttributeControl _attrControl;
        string _selectedValue;

        FrameworkControl _resourceControl;

        public bool IsOverlayReg { get; set; }

        public void SetVisible(bool visible)
        {
            if (!visible)
                divControl.Style.Add("display", "none");
            else
                divControl.Style.Add("display", "block");

        }
        public FrameworkControl ResourceControl
        {
            get { return _resourceControl; }
            set { _resourceControl = value; }
        }
        public ValidationMessage ValidationMessage { get { return txtValidation; } }
        public void Save()
        {

        }
        public void SetValue(string value)
        {
            _selectedValue = value;

        }
        public string GetValue()
        {
            if (IsOverlayReg)
            {
                _selectedValue = Request[AttributeCaptchaControl.UniqueID];
            }
            else
            {
                _selectedValue = AttributeCaptchaControl.Text;
            }


            return _selectedValue.ToString();
            //_selectedValue.ToString();

        }
        public string ScriptBlock()
        {
            return "";
        }
        public string FocusControlClientID
        {
            get
            {
                try
                {
                    return AttributeCaptchaControl.ClientID;
                }
                catch (Exception ex)
                {
                    return "";
                }

            }
        }
        public void PersistToCookie(HttpCookie cookie)
        {
            if (!String.IsNullOrEmpty(_attrControl.AttributeName))
                cookie[_attrControl.AttributeName] = GetValue();
            else
                cookie[_attrControl.Name] = GetValue();

        }

        public void Persist(ITemporaryPersistence persistence)
        {
            if (!String.IsNullOrEmpty(_attrControl.AttributeName))
                persistence[_attrControl.AttributeName] = GetValue();
            else
                persistence[_attrControl.Name] = GetValue();
        }

        public bool Validate()
        {
            bool valid = true;
            if (_attrControl.RequiredFlag)
            {
                GetValue();
                string captchaoverride = SettingsManager.GetSettingString(SettingConstants.CAPTCHA_OVERRIDE,g.Brand);

                if (!String.IsNullOrEmpty(captchaoverride))
                {
                    if (_selectedValue == captchaoverride)
                        return true;
                }
                if (String.IsNullOrEmpty(_selectedValue.ToUpper()) && _attrControl.RequiredFlag)
                {
                    txtValidation.Text = _attrControl.GetValidationMessage(_attrControl.ErrorMessageRequired, g, _resourceControl);
                    txtValidation.Visible = true;
                    valid = false;
                }
                else if (_selectedValue.ToUpper() != g.Session["CAPTCHAText"].ToUpper())
                {
                    txtValidation.Text = _attrControl.GetValidationMessage(_attrControl.ErrorMessageValidation, g, _resourceControl);
                    txtValidation.Visible = true;
                    valid = false;
                }
            }
            return valid;
        }

        public string ContainerDivClientID { get { return divControl.ClientID; } }


        public AttributeControl AttrControl
        { get { return _attrControl; } set { _attrControl = value; } }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            txtCaptchaTextbox.ResourceControl = _resourceControl;

            if (_attrControl.RequiredFlag)
            {
                AttributeCaptchaControl.CssClass += " mandatory";
            }
            else
            {
                AttributeCaptchaControl.CssClass += " optional";
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            bool generate;
            generate = (Request.Form["__EVENTARGUMENT"] == "$continue");
            SetUpCaptchaImage(generate);
            //   txtCaptchaTextbox.Text = _attrControl.GetResourceString(_attrControl.ResourceConstant, g, _resourceControl);

            divControl.Attributes["class"] = _attrControl.Name;

            if (_attrControl.OnePageReg)
            {
                imgCaptchaOnePageReg.Visible = true;
                imgCaptcha.Visible = false;
                AttributeCaptchaControl.Style.Clear();
            }
        }

        protected void SetUpCaptchaImage(bool generateNewCaptchaText)
        {
            string generatedText;

            plcRefreshCaptcha.Visible = _attrControl.OnePageReg;

            //remove any lingering capcha text from before
            if (g.Session.Get("CAPTCHAText") != null && !generateNewCaptchaText)
            {
                generatedText = (string)g.Session.Get("CAPTCHAText");
            }
            else
            {
                //generate and encrypt the text
                if (_attrControl.OnePageReg)
                {
                    generatedText = RandomTextGenerator.Generate(4, 4);
                }
                else
                {
                    generatedText = RandomTextGenerator.Generate(4, 7);
                }
                
                g.Session.Add("CAPTCHAText", generatedText, SessionPropertyLifetime.Temporary);
                AttributeCaptchaControl.Text = "";
            }

            string encryptedText = Crypto.Encrypt(WebConstants.SPARK_WS_CRYPT_KEY, generatedText);

            //tt # 20365 - for testing purposes we emit the text
            Page.RegisterHiddenField("CAPTCHAText", encryptedText);


            //display the text in the captcha image
            imgCaptcha.ImageUrl = "/Applications/Home/JpegImage.aspx";
            if (_attrControl.OnePageReg)
            {
                imgCaptcha.ImageUrl += "?onePageReg=1";
            }

            imgCaptchaOnePageReg.ImageUrl = imgCaptcha.ImageUrl;
            //CAPTCHAText=" + Server.HtmlEncode(Server.UrlEncode(encryptedText))	
        }


        private void Page_PreRender(object sender, System.EventArgs e)
        {
            //AttributeTextControl.Text = _attrControl.AttributeTextValue;


            // txtCaptchaTextbox.Text = _attrControl.GetResourceString(_attrControl.ResourceConstant, g, _resourceControl);

        }

        public NameValueCollection GetSearchPreferenceParamaterNVC()
        {
            return new NameValueCollection();
        }

        protected string GetTitleWrapperOpen
        {
            get
            {
                return (_attrControl.OnePageReg) ? "<div class=\"label\">" : "<label class=\"h2\">";
            }
        }
        protected string GetTitleWrapperClose
        {
            get
            {
                return (_attrControl.OnePageReg) ? "</div>" : "</label>";
            }
        }
        protected string GetFieldWrapperOpen
        {
            get { return (_attrControl.OnePageReg) ? "<div class=\"field\">" : string.Empty; }
        }
        protected string GetFieldWrapperClose
        {
            get { return (_attrControl.OnePageReg) ? "</div>" : string.Empty; }
        }

        protected string GetBR(int count)
        {
            string brString = "";
            if (!_attrControl.OnePageReg)
            {
                for (int i = 0; i < count; i++)
                {
                    brString += "<br/>";
                }
            }
            return brString;
        }

        #region IJQValidation Members

        public string InputFieldID
        {
            get { return AttributeCaptchaControl.UniqueID; }
        }

        public List<ValidationDictionaryEntry> GetValidationRules
        {
            get
            {
                List<ValidationDictionaryEntry> result = new List<ValidationDictionaryEntry>();
                ValidationDictionaryEntry rules = new ValidationDictionaryEntry();

                rules.UniqueID = AttributeCaptchaControl.UniqueID;

                if (_attrControl.RequiredFlag)
                {
                    rules.ValidationEntryList.Add(new ValidationEntry()
                    {
                        errorKey = "required",
                        message = _attrControl.GetValidationMessage(_attrControl.ErrorMessageRequired, g, _resourceControl),
                        value = "true"
                    });
                }

                result.Add(rules);
                return result;
            }
        }

        #endregion
    }
}