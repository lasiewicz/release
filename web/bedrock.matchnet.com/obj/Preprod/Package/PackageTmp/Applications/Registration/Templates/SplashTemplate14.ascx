﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SplashTemplate14.ascx.cs"
    Inherits="Matchnet.Web.Applications.Registration.Templates.SplashTemplate14" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="uc1" TagName="ValidationMessage" Src="/Applications/Registration/Controls/ValidationMessage.ascx" %>
<%@ Register TagPrefix="mn" TagName="Copyright" Src="/Framework/Ui/PageElements/Copyright.ascx" %>
<%@ Register TagPrefix="mn1" TagName="Footer20" Src="/Framework/Ui/Footer20.ascx" %>
<%@ Register TagPrefix="mn" TagName="AdUnit" Src="/Framework/UI/Advertising/AdUnit.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ImageRotator" Src="/Framework/Ui/BasicElements/ImageRotator.ascx" %>

<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
<script type="text/javascript">window.jQuery || document.write('<script type="text/javascript" src="/javascript20/library/jquery-1.4.2.min.js">\x3C/script>')</script>
<div id="min-max-container">
    <asp:PlaceHolder runat="server" ID="plcOpenMbox" Visible="false">
        <div class="mboxDefault">
    </asp:PlaceHolder>
    <div id="header" class="clearfix">
        <div id="header-logo">
            <p class="header-message">
                <mn:Image ID="imgLogo" runat="server" TitleResourceConstant="TTL_SITE_LOGO" ResourceConstant="ALT_SITE_LOGO"
                    FileName="trans.gif" NavigateURLClass="logo" /><mn:Txt ID="mntxt5508" runat="server"
                        ResourceConstant="TXT_HEADER_MESSAGE" ExpandImageTokens="true" />
            </p>
        </div>
        <ul id="header-nav">
            <li><a href="http://www.jdate.co.il/">
                <mn:Txt ID="mntxt1627" runat="server" ResourceConstant="TXT_HEBREW" ExpandImageTokens="true" />
            </a></li>
            <li><a href="http://www.jdate.fr/">
                <mn:Txt ID="mntxt3959" runat="server" ResourceConstant="TXT_FRENCH" ExpandImageTokens="true" />
            </a></li>
        </ul>
        <div id="header-login">
            <a href="/Applications/Logon/Logon.aspx">
                <mn:Txt ID="mntxt4348" runat="server" ResourceConstant="TXT_LOGON" ExpandImageTokens="true" />
            </a>
        </div>
    </div>
    <div id="content-container">
        <div id="content-main" class="clearfix">
            <div id="rotator">
                <div id="img_contents">
                    <mn:Txt ID="mntxt2864" runat="server" ResourceConstant="HTML_CONTENT_PHOTO" ExpandImageTokens="true" />
                </div>
            </div>
            <div id="marketing-wrap">
                <mn:Txt ID="mntxt1638" runat="server" ResourceConstant="HTML_MARKETING_CONTENT" ExpandImageTokens="true" />
                <ul id="splash-bullets">
                    <li>
                        <mn:Txt ID="Txt4" runat="server" ResourceConstant="TXT_MARKETING_COPY_ONE" ExpandImageTokens="True" />
                    </li>
                    <li>
                        <mn:Txt ID="Txt5" runat="server" ResourceConstant="TXT_MARKETING_COPY_TWO" ExpandImageTokens="True" />
                    </li>
                    <li>
                        <mn:Txt ID="Txt6" runat="server" ResourceConstant="TXT_MARKETING_COPY_THREE" ExpandImageTokens="True" />
                        <a href="/Applications/MembersOnline/MembersOnline.aspx" class="mol-count">
                            <mn:Txt ID="txtMOLCount" runat="server" />
                        </a>
                        <mn:Txt ID="Txt7" runat="server" ResourceConstant="TXT_MARKETING_COPY_FOUR" ExpandImageTokens="True" />
                    </li>
                </ul>
                <div id="cta">
                    <a href="/Applications/Registration/Registration.aspx" class="link-primary">
                        <mn:Txt ID="mntxt8138" runat="server" ResourceConstant="TXT_REG_BUTTON" ExpandImageTokens="true" />
                    </a>
                </div>
            </div>
        </div>
    </div>
    <asp:PlaceHolder runat="server" ID="plcCloseMbox" Visible="false">
        </div>
        <script language="javascript">    mboxCreate('<%=SplashFullContentMBoxName %>');</script>
    </asp:PlaceHolder>
<div id="vis-info" class="clear-both editorial">
    <mn:Txt ID="mntxt6945" runat="server" ResourceConstant="TXT_SPLASH_SEO" ExpandImageTokens="true" />
</div>
</div>
<div id="footer">
    <div id="footer-content">
        <mn1:Footer20 ID="Footer20" Runat="server" />
    </div>
</div>
