﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="JCSplashTemplate.ascx.cs"
    Inherits="Matchnet.Web.Applications.Registration.Templates.JCSplashTemplate" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="uc1" TagName="ValidationMessage" Src="/Applications/Registration/Controls/ValidationMessage.ascx" %>
<%@ Register TagPrefix="mn" TagName="Copyright" Src="/Framework/Ui/PageElements/Copyright.ascx" %>
<%@ Register TagPrefix="mn1" TagName="Footer20" Src="/Framework/Ui/Footer20.ascx" %>
<%@ Register TagPrefix="mn" TagName="AdUnit" Src="/Framework/UI/Advertising/AdUnit.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ImageRotator" Src="/Framework/Ui/BasicElements/ImageRotator.ascx" %>

<div id="min-max-container">
       <div id="header" class="clearfix">
            <div id="header-logo">
                <p class="header-message">
                    <mn:Image ID="imgLogo" runat="server" TitleResourceConstant="TTL_SITE_LOGO" ResourceConstant="ALT_SITE_LOGO"
                        FileName="trans.gif" NavigateURLClass="logo" />
                </p>
            </div>
            <ul id="header-nav">
                <li><a href="http://www.jdate.co.il/">
                    <mn:Txt ID="mntxt1627" runat="server" ResourceConstant="TXT_HEBREW" ExpandImageTokens="true" />
                </a></li>
                <li><a href="http://www.jdate.fr/">
                    <mn:Txt ID="mntxt3959" runat="server" ResourceConstant="TXT_FRENCH" ExpandImageTokens="true" />
                </a></li>
            </ul>
            <div id="header-login">
                <a href="/Applications/Logon/Logon.aspx">
                    <mn:Txt ID="mntxt4348" runat="server" ResourceConstant="TXT_LOGON" ExpandImageTokens="true" />
                </a>
            </div>
        </div>
<div id="JC-splash-content">
    <div id="content-1">
        <div id="header"><mn:Image runat="server" FileName="jclogo.png" /></div>
        <div id="cn-1"><mn:Txt ID="Txt2" runat="server" ResourceConstant="CONTENT_TEXT_1" /></div>
        <div id="cn-2"><mn:Txt ID="Txt3" runat="server" ResourceConstant="CONTENT_TEXT_2" /></div>
    </div>
    <div id="content-2">
    <div id="cn-3">
    <mn:Image ID="Image1" runat="server" FileName="sale-button.png" />
     <div id="sale-1"><mn:Txt ID="Txt6" runat="server" ResourceConstant="CONTENT_TEXT_3_1" ExpandImageTokens="true" /></div>
     <div id="sale-2"><mn:Txt ID="Txt7" runat="server" ResourceConstant="CONTENT_TEXT_3_2" ExpandImageTokens="true" /></div>
     <div id="sale-3"><mn:Txt ID="Txt8" runat="server" ResourceConstant="CONTENT_TEXT_3_3" ExpandImageTokens="true" /></div>
     <div id="sale-4"><mn:Txt ID="Txt9" runat="server" ResourceConstant="CONTENT_TEXT_3_4" ExpandImageTokens="true" /></div>
     <div id="sale-5"><mn:Txt ID="Txt10" runat="server" ResourceConstant="CONTENT_TEXT_3_5" ExpandImageTokens="true" /></div>
     <div id="sale-6"><mn:Txt ID="Txt11" runat="server" ResourceConstant="CONTENT_TEXT_3_6" ExpandImageTokens="true" /></div>
     <a href="/Applications/Registration/Registration.aspx" class="link-primary">
                            <mn:Txt ID="mntxt8138" runat="server" ResourceConstant="TXT_REG_BUTTON" ExpandImageTokens="true" />
                            
                        </a>
    </div>
    <div id="cn-4">
        <mn:Txt ID="mntxt2864" runat="server" ResourceConstant="HTML_CONTENT_PHOTO" ExpandImageTokens="true" />
        <mn:Txt ID="Txt5" runat="server" ResourceConstant="CONTENT_TEXT_4" />
    </div>
    </div>
    <div id="empty-div">&nbsp;</div>
</div>
</div>
<div id="footer">
    <div id="footer-content">
        <mn1:Footer20 ID="Footer20" Runat="server" />
    </div>
</div>