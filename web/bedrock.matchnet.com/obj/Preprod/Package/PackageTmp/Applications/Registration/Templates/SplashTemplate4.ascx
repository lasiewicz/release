﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SplashTemplate4.ascx.cs" Inherits="Matchnet.Web.Applications.Registration.Templates.SplashTemplate4" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
 <%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
 <%@ Register TagPrefix="uc1" TagName="ValidationMessage" Src="/Applications/Registration/Controls/ValidationMessage.ascx" %>
 <%@ Register TagPrefix="mn" TagName="Copyright" Src="/Framework/Ui/PageElements/Copyright.ascx" %>
 <%@ Register TagPrefix="mn1" TagName="Footer20" Src="/Framework/Ui/Footer20.ascx" %>
 <%@ Register TagPrefix="mn" TagName="AdUnit" Src="/Framework/UI/Advertising/AdUnit.ascx" %>
 <%@ Register TagPrefix="uc1" TagName="ImageRotator" Src="/Framework/Ui/BasicElements/ImageRotator.ascx" %>
   
    <link id="_ctl0_Head20_structStyleSheet" rel="stylesheet" type="text/css" href="/css/structure-ltr.css" />
    <link id="_ctl0_Head20_styleStyleSheet" rel="stylesheet" type="text/css" href="/css/style.103.css" />

   
    <div id="min-max-container">
   
        <div id="header" class="clearfix">
            <asp:PlaceHolder runat="server" ID="NavTopHeader" Visible="true">

                <div id="header-contentainer" class="clearfix">
                    <div id="header-logo">
		                <p class="header-message"><mn:image id="imgLogo" runat="server" CssClass="logo" titleResourceConstant="TTL_SITE_LOGO" ResourceConstant="ALT_SITE_LOGO" FileName="trans.gif" />
                            <span id="divHeaderMsg" runat="server"><mn:Txt ID="txtHeaderMessage" runat="server" ResourceConstant="TXT_HEADER_MESSAGE" /></span>
                        </p>
                    </div>
                    <div class="header-login" id="headerLogin" runat="server"><mn:Txt ID="txtLogon" runat="server" ResourceConstant="TXT_LOGON" /></div>
                    <ul class="header-nav" id="ulJdateSiteLinks" runat="server">
                        <li id="liHebrew" runat="server"><a href="http://www.jdate.co.il/"><mn:Txt ID="txt2" runat="server" ResourceConstant="TXT_HEBREW" /></a></li>
                        <li id="liFrench" runat="server"><a href="http://www.jdate.fr/"><mn:Txt ID="txt1" runat="server" ResourceConstant="TXT_FRENCH" /></a></li>
                        <li id="liAlreadyAMember" runat="server" class="header-nav-filler"><mn:Txt ID="txt3" runat="server" ResourceConstant="TXT_ALREADY_MEMBER" /></li>
                    </ul>
                </div>

            </asp:PlaceHolder>
        </div>
        
        <div id="content-container" class="one-column your-profile">
                                
                        <div id="content-main" <asp:Literal id="litCntMain" runat="server" />
                                
                            <div class="content-main clearfix">         
                            
                                <div class="rotator">
                                   <uc1:ImageRotator id="imageRotator" runat="server"  ResourceConstant="IMG_DATA_FILE"/>
                                </div>

                                <!-- Begin Amadesa Element headline Tag -->
                                <asp:Panel runat="server" ID="pnlJSHeadline">
                                    <script language="javascript" type="text/javascript">liveExperience('headline');</script>
                                </asp:Panel>
                                <div id="headline">
                                
                                    <big><mn:Txt ID="txtTitle" runat="server" /></big>
                                
                                </div>
                                </noscript>
                                <!-- End Amadesa Element headline Tag -->
                            
                                <fieldset class="primary-content <asp:Literal id='litCSSClass' runat=server/>" >
                                   
                                   <asp:PlaceHolder ID="phContent" runat="server">
                                    <uc1:ValidationMessage runat="server" ID="txtValidation" Visible=false />
                                    
                                    </asp:PlaceHolder>
                                    <div class="cta-wrapper">
                                    <!-- Begin Amadesa Element cta Tag -->
                                    <asp:Panel runat="server" ID="pnlJSCta" CssClass="amadesa-tag">
                                        <script language="javascript" type="text/javascript">liveExperience('cta');</script>
                                    </asp:Panel>
                                    <div id="cta">
                                    
                                        <div class="cta">
                                            <mn2:FrameworkButton runat="server" ID="btnContinue" ResourceConstant="TXT_CONTINUE_SPLASH"  CssClass="btn primary" />
                                        </div>
                                        
                                    </div>
                                    </noscript>
                                    <!-- End Amadesa Element cta Tag -->   
                                    </div>
                                </fieldset>

                                <div style="clear: both"></div>
                            
                            </div>
                        
	                    </div>
            
            <asp:PlaceHolder ID="PlaceHolderAdSplashLeaderboard" runat="server">
            <div class="splashLeaderboardAd"><mn:AdUnit id="adSplashLeaderboard" Size="Leaderboard" ExpandImageTokens="true" runat="server"  /></div>
            </asp:PlaceHolder>
            <div class="vis-info"><mn:Txt ID="txtSeo" runat=server ResourceConstant="TXT_SPLASH_SEO" /></div>

        </div>

    </div>

    <div style="width:100%;">

        <div id="footerContainer" runat="server">
            <div id="footer" style="padding:20px 0 35px 0;">
              <div class="footer-container"><mn1:Footer20 ID="Footer20" Runat="server" /></div>
            </div>
        </div>
        <div id="footer-narrow">
            <div class="footer-container"><mn:Copyright id="copyright" runat="server"></mn:Copyright></div>
        </div>    

    </div>