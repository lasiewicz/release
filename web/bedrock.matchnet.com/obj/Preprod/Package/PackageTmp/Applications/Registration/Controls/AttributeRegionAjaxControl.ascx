﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AttributeRegionAjaxControl.ascx.cs"
    Inherits="Matchnet.Web.Applications.Registration.Controls.AttributeRegionAjaxControl" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="uc2" TagName="ValidationMessage" Src="/Applications/Registration/Controls/ValidationMessage.ascx" %>
<%@ Register TagPrefix="anthem" Namespace="Anthem" Assembly="Anthem" %>
<anthem:Panel ID="anthemPnl1" runat="server" AutoUpdateAfterCallBack="true" EnabledDuringCallBack="false">
    <div id="divControl" runat="server">
        <script type="text/javascript">
	<asp:Literal ID="litShowCityLinkJS" Runat="server" Visible="True" />

	//Show "Look up City" link if a valid state/region is selected.  Otherwise hide it.
	function ShowCityLink()
	{
		var StateDropDownID = document.getElementById("StateDropDownID").value;
		var stateDropDown = document.getElementById(StateDropDownID);
		
		
		if(!document.getElementById('LookupCityLink'))
		return;
		
		var varLookupCityLink = document.getElementById('LookupCityLink');
		
		if (stateDropDown != null && stateDropDown.value != "")
		{
			varLookupCityLink.style.visibility = "visible";
		}
		else
		{
			varLookupCityLink.style.visibility = "hidden";
		}
	}
        </script>
        <asp:Literal ID="litScript" runat="server" />
        <asp:PlaceHolder runat="server" ID="phPickRegion">
            <%--  <h2>
            <mn:Txt runat="server" ID="txtCaption" />
        </h2>--%>
            <uc2:ValidationMessage runat="server" ID="txtValidation" Visible="false" />
            <div id="divCountry" runat="server" class="IframeCountry">
                <%=GetTitleWrapperOpen %>
                <mn:Txt ID="lblCountry" runat="server" ResourceConstant="DDL_COUNTRY" />
                <%=GetTitleWrapperClose %>
                <%=GetFieldWrapperOpen %>
                <asp:DropDownList ID="country" AutoPostBack="True" DataTextField="description" EnableViewState="False"
                    runat="server">
                    <%-- <anthem:DropDownList ID="country" AutoPostBack="True" DataTextField="description"
                        EnableViewState="False" runat="server">
                    </anthem:DropDownList>--%>
                </asp:DropDownList>
                <%=GetFieldWrapperClose %>
            </div>
            <div id="divState" runat="server">
                <%=GetTitleWrapperOpen %>
                <mn:RegionLabel runat="server" ID="txtState" />
                <mn:Txt runat="server" ID="txtFRCityRegionSeperator" ResourceConstant="REGION_CITY_SEPERATOR"
                    Visible="false" />
                <mn:Txt runat="server" ID="txtFRCityRegion" ResourceConstant="DDL_CITY" Visible="false" />
                <%=GetTitleWrapperClose %>
                <%=GetFieldWrapperOpen %>
                <asp:DropDownList ID="state" AutoPostBack="True" DataTextField="description" EnableViewState="False"
                    runat="server">
                </asp:DropDownList>
                <asp:DropDownList ID="frCityRegion" CssClass="fr-city-region" runat="server" AutoPostBack="true"
                    EnableViewState="true" Visible="false">
                    <asp:ListItem Text=" " Value=" " />
                    <asp:ListItem Text="Bordeaux" Value="a3483616" />
                    <asp:ListItem Text="Grenoble" Value="b3483636" />
                    <asp:ListItem Text="Lille" Value="c3483631" />
                    <asp:ListItem Text="Lyon" Value="d3483636" />
                    <asp:ListItem Text="Marseille" Value="e3483633" />
                    <asp:ListItem Text="Montpellier" Value="f3483627" />
                    <asp:ListItem Text="Nice" Value="g3483635" />
                    <asp:ListItem Text="Paris" Value="h3483626" />
                    <asp:ListItem Text="Strasbourg" Value="i3483615" />
                    <asp:ListItem Text="Toulouse" Value="j3483624" />
                    <asp:ListItem Text="Autre ville..." Value="-1" />
                </asp:DropDownList>
                <%=GetFieldWrapperClose %>
            </div>
            <div id="divCity" runat="server">
                <%=GetTitleWrapperOpen %>
                <mn:Txt ID="Txt1" runat="server" ResourceConstant="DDL_CITY" />
                <%=GetTitleWrapperClose %>
                <%=GetFieldWrapperOpen %>
                <asp:DropDownList runat="server" ID="ddlFullCityList" Visible="False" CssClass="full-city-list hide">
                </asp:DropDownList>
                <asp:TextBox ID="city" runat="server" ReadOnly="False"></asp:TextBox>
                <%=GetFieldWrapperClose%>
                <a href="javascript:popCityList();" id="LookupCityLink">
                    <mn:Txt ID="lblLookUpCity" runat="server" Visible="true" ResourceConstant="LOOK_UP_CITY">
                    </mn:Txt>
                </a>
            </div>
            <div id="divPostalCode" runat="server">
                <mn:Txt ID="TxtZipPostalCode" runat="server" />
                <%=GetTitleWrapperOpen %>
                <mn:Txt ID="lblPostal" runat="server" />
                <%=GetTitleWrapperClose %>
                <label id="lblCallBackMsg" style="display: none;">
                    <em class="error">
                        <mn:Txt runat="server" ResourceConstant="TXT_CALLBACK_MSG" ID="Txt3" NAME="Txt2">
                        </mn:Txt>
                    </em>
                </label>
                <%=GetFieldWrapperOpen %>
                <anthem:TextBox ID="postalCode" runat="server" EnabledDuringCallBack="True" AutoPostBack="false"
                    AutoUpdateAfterCallBack="false" AutoCallBack="true" ReadOnly="False" PreCallBackFunction="postalCode_PreCallBack"
                    PostCallBackFunction="postalCode_PostCallBack"></anthem:TextBox>
                <%=GetFieldWrapperClose%>
            </div>
            <div id="divCities" runat="server">
                <%=GetTitleWrapperOpen %>
                <mn:Txt ID="lblCity" runat="server" ResourceConstant="DDL_CITY" />
                <%=GetTitleWrapperClose %>
                <%=GetFieldWrapperOpen %>
                <anthem:DropDownList ID="ddlCities" runat="server" AutoPostBack="false" AutoCallBack="false">
                </anthem:DropDownList>
                <%=GetFieldWrapperClose %>
            </div>
        </asp:PlaceHolder>
    </div>
</anthem:Panel>
