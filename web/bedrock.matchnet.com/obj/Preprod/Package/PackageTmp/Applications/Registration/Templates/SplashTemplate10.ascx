﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SplashTemplate10.ascx.cs"
    Inherits="Matchnet.Web.Applications.Registration.Templates.SplashTemplate10" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="uc1" TagName="ValidationMessage" Src="/Applications/Registration/Controls/ValidationMessage.ascx" %>
<%@ Register TagPrefix="mn" TagName="Copyright" Src="/Framework/Ui/PageElements/Copyright.ascx" %>
<%@ Register TagPrefix="mn1" TagName="Footer20" Src="/Framework/Ui/Footer20.ascx" %>
<%@ Register TagPrefix="mn" TagName="AdUnit" Src="/Framework/UI/Advertising/AdUnit.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ImageRotator" Src="/Framework/Ui/BasicElements/ImageRotator.ascx" %>

<script type="text/javascript" src="/javascript20/jquery.jcarousel.pack.js"></script>

<link rel="stylesheet" type="text/css" href="/css/style.15.css" />
<link rel="stylesheet" type="text/css" href="/css/cupid_splash_carousel/jquery.jcarousel.css" />
<link rel="stylesheet" type="text/css" href="/css/cupid_splash_carousel/skin.css" />

<script type="text/javascript">
    var mycarousel_itemList = [{ url: 'http://static.cupid.co.il/Components/carouselImages_Cupid/adi30399135.jpg', title: 'עדי, 30', username: 'עדי, 30', href: 'http://www.cupid.co.il/Applications/MemberProfile/ViewProfile.aspx?MemberID=7158123&EntryPoint=7' }, { url: 'http://static.cupid.co.il/Components/carouselImages_Cupid/shmulik32541235.jpg', title: 'שמוליק, 32', username: 'שמוליק, 32', href: 'http://www.cupid.co.il/Applications/MemberProfile/ViewProfile.aspx?MemberID=8699716&EntryPoint=7' }, { url: 'http://static.cupid.co.il/Components/carouselImages_Cupid/shiri674565.jpg', title: 'שירי, 34', username: 'שירי, 34', href: 'http://www.cupid.co.il/Applications/MemberProfile/ViewProfile.aspx?MemberID=112713842&EntryPoint=7' }, { url: 'http://static.cupid.co.il/Components/carouselImages_Cupid/ben35small848647.jpg', title: 'בן, 35', username: 'בן, 35', href: 'http://www.cupid.co.il/Applications/MemberProfile/ViewProfile.aspx?MemberID=107911734&EntryPoint=7' }, { url: 'http://static.cupid.co.il/Components/carouselImages_Cupid/yael33170093.jpg', title: 'יעל,33', username: 'יעל,33', href: 'http://www.cupid.co.il/Applications/MemberProfile/ViewProfile.aspx?MemberID=104199519&EntryPoint=7' }, { url: 'http://static.cupid.co.il/Components/carouselImages_Cupid/moti34144144.jpg', title: 'מוטי, 34', username: 'מוטי, 34', href: 'http://www.cupid.co.il/Applications/MemberProfile/ViewProfile.aspx?MemberID=12007245&EntryPoint=7' }, { url: 'http://static.cupid.co.il/Components/carouselImages_Cupid/oran577770.jpg', title: 'אורן, 26', username: 'אורן, 26', href: 'http://www.cupid.co.il/Applications/MemberProfile/ViewProfile.aspx?MemberID=107829640&EntryPoint=7' }, { url: 'http://static.cupid.co.il/Components/carouselImages_Cupid/avinoam35902822.jpg', title: 'אבינועם, 35', username: 'אבינועם, 35', href: 'http://www.cupid.co.il/Applications/MemberProfile/ViewProfile.aspx?MemberID=498652&EntryPoint=7' }, { url: 'http://static.cupid.co.il/Components/carouselImages_Cupid/elismall576129.jpg', title: 'אלי, 34', username: 'אלי, 34', href: 'http://www.cupid.co.il/Applications/MemberProfile/ViewProfile.aspx?MemberID=111919594&EntryPoint=7' }, { url: 'http://static.cupid.co.il/Components/carouselImages_Cupid/adismall868565.jpg', title: 'עדי, 38', username: 'עדי, 38', href: 'http://www.cupid.co.il/Applications/MemberProfile/ViewProfile.aspx?MemberID=811885&EntryPoint=7' }, { url: 'http://static.cupid.co.il/Components/carouselImages_Cupid/nurit415461.jpg', title: 'נורית, 34', username: 'נורית, 34', href: 'http://www.cupid.co.il/Applications/MemberProfile/ViewProfile.aspx?MemberID=103115909&EntryPoint=7' }, { url: 'http://static.cupid.co.il/Components/carouselImages_Cupid/keren29847077.jpg', title: 'קרן, 29', username: 'קרן, 29', href: 'http://www.cupid.co.il/Applications/MemberProfile/ViewProfile.aspx?MemberID=42096352&EntryPoint=7' }, { url: 'http://static.cupid.co.il/Components/carouselImages_Cupid/eran965892.jpg', title: 'ערן, 34', username: 'ערן, 34', href: 'http://www.cupid.co.il/Applications/MemberProfile/ViewProfile.aspx?MemberID=43556498&EntryPoint=7' }, { url: 'http://static.cupid.co.il/Components/carouselImages_Cupid/140007491629965.jpg', title: 'שירי, 25', username: 'שירי, 25', href: 'http://www.cupid.co.il/Applications/MemberProfile/ViewProfile.aspx?PersistLayoutTemplate=1&EntryPoint=7&Ordinal=1&MemberID=51230627&LayoutTemplateID=11' }, { url: 'http://static.cupid.co.il/Components/carouselImages_Cupid/eyal28315610.jpg', title: 'אייל, 28', username: 'אייל, 28', href: 'http://www.cupid.co.il/Applications/MemberProfile/ViewProfile.aspx?MemberID=100117648&EntryPoint=7' }, { url: 'http://static.cupid.co.il/Components/carouselImages_Cupid/tsahi25small230539.jpg', title: 'צחי, 25', username: 'צחי, 25', href: 'http://www.cupid.co.il/Applications/MemberProfile/ViewProfile.aspx?PersistLayoutTemplate=1&EntryPoint=7&Ordinal=1&MemberID=10981266&LayoutTemplateID=11' }, { url: 'http://static.cupid.co.il/Components/carouselImages_Cupid/tal38253.jpg', title: 'טל, 26', username: 'טל, 26', href: 'http://www.cupid.co.il/Applications/MemberProfile/ViewProfile.aspx?MemberID=111784725&EntryPoint=7' }, { url: 'http://static.cupid.co.il/Components/carouselImages_Cupid/shirley33582761.jpg', title: 'שירלי, 34', username: 'שירלי, 34', href: 'http://www.cupid.co.il/Applications/MemberProfile/ViewProfile.aspx?MemberID=9978818&EntryPoint=7' }, { url: 'http://static.cupid.co.il/Components/carouselImages_Cupid/Avivsmall698583.jpg', title: 'אביב, 37', username: 'אביב, 37', href: 'http://www.cupid.co.il/Applications/MemberProfile/ViewProfile.aspx?MemberID=33079438&EntryPoint=7' }, { url: 'http://static.cupid.co.il/Components/carouselImages_Cupid/ravit33683885.jpg', title: 'רוית, 33', username: 'רוית, 33', href: 'http://www.cupid.co.il/Applications/MemberProfile/ViewProfile.aspx?MemberID=40224669&EntryPoint=7' }, { url: 'http://static.cupid.co.il/Components/carouselImages_Cupid/yael877095.jpg', title: 'יעל, 35', username: 'יעל, 35', href: 'http://www.cupid.co.il/Applications/MemberProfile/ViewProfile.aspx?MemberID=52125674&EntryPoint=7' }, { url: 'http://static.cupid.co.il/Components/carouselImages_Cupid/sigal36110902.jpg', title: 'סיגל, 36', username: 'סיגל, 36', href: 'http://www.cupid.co.il/Applications/MemberProfile/ViewProfile.aspx?MemberID=111544585&EntryPoint=7' }, { url: 'http://static.cupid.co.il/Components/carouselImages_Cupid/romi148068.jpg', title: 'רומי, 36', username: 'רומי, 36', href: 'http://www.cupid.co.il/Applications/MemberProfile/ViewProfile.aspx?MemberID=33210274&EntryPoint=7' }, { url: 'http://static.cupid.co.il/Components/carouselImages_Cupid/keren212193.jpg', title: 'קרן, 29', username: 'קרן, 29', href: 'http://www.cupid.co.il/Applications/MemberProfile/ViewProfile.aspx?MemberID=113292491&EntryPoint=7' }, { url: 'http://static.cupid.co.il/Components/carouselImages_Cupid/galit29888871.jpg', title: 'גלית, 29', username: 'גלית, 29', href: 'http://www.cupid.co.il/Applications/MemberProfile/ViewProfile.aspx?MemberID=42134146&EntryPoint=7' }, ];
</script>

<script type="text/javascript">
    function login() {
        var amEmail = document.getElementsByName("emailaddress")[0].value;
        var amPass = document.getElementsByName("password1")[0].value;

        var chkRememberMe = document.getElementById("remember");
        if (chkRememberMe.checked) {
            createCookie("username2", document.getElementById("emailaddress").value, 100);
            createCookie("remember", "yes", 100);
        }
        else {
            eraseCookie("username2");
            eraseCookie("remember");
        }

        var newUrl = window.location.toString().split("co.il")[0] + "co.il/applications/logon/logon.aspx?emailaddress=";
        newUrl = newUrl + amEmail + "&password=" + amPass;
        window.location = newUrl;
    }

    function keypress(t, event) {
        if (event.keyCode == '13') {
            login();
        }
    }


    function eraseCookie(name) {
        createCookie(name, "", -1);
    }

    function createCookie(name, value, days) {
        if (days) {
            var date = new Date();
            date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
            var expires = "; expires=" + date.toGMTString();
        }
        else var expires = "";
        document.cookie = name + "=" + value + expires + "; path=/";
    }

    function readCookiesData() {
        var rem = readCookie("remember");
        if (rem == "yes") {
            document.getElementById("remember").checked = true;
            document.getElementById("emailaddress").value = readCookie("username2");
            jQuery(document).ready(function() {
                jQuery('#div-login-form').show();
                jQuery('#login-btn-div').css("background-color", "#E8E8E8");
                jQuery('#div-login-form').css("background-color", "#E8E8E8");

                show = true;
            });
        }
    }

    function readCookie(name) {
        var nameEQ = name + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') c = c.substring(1, c.length);
            if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
        }
        return null;
    }

    function amGoTo() {

        amCreateInfoCookie();

        //create cookie for A
        var amRegUrl = window.location.toString().split(".co.il")[0] + ".co.il/Applications/Registration/Registration.aspx";

        window.location = amRegUrl;
    }


    function amCreateCookie(name, value, days) {
        if (days) {
            var date = new Date();
            date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
            var expires = "; expires=" + date.toGMTString();
        }
        else var expires = "";
        document.cookie = name + "=" + value + expires + "; path=/";
    }
    function amCreateInfoCookie() {
        var woman = 1;
        var man = 2;
        var myGender = document.getElementsByName("amMyGender")[0].value;
        var seekGender = document.getElementsByName("amToMeetGender")[0].value;

        var amCookieVal;
        amCookieVal = "REG091202=REG091202&prm=55020&ScenarioFile=/Applications/Registration/XML/Registration_4.xml&ScenarioName=Scen2&LAST_COMPLETED_STEP=0&CURRENT_STEP=1&SESSION_ID=22c82c83-999d-4c70-892c-0b8f45761f7f&START_STEP_ID=1"
        if (myGender == woman && seekGender == woman) { amCookieVal += "&GenderMask=10"; }
        else if (myGender == woman && seekGender == man) { amCookieVal += "&GenderMask=6"; }
        else if (myGender == man && seekGender == woman) { amCookieVal += "&GenderMask=9"; }
        else if (myGender == man && seekGender == man) { amCookieVal += "&GenderMask=5"; }

        amCookieVal += "&UserName=" + encodeURIComponent(jQuery('#txtUsername1').val());
        amCookieVal += "&Password=" + encodeURIComponent(jQuery('#lgnPassword').val());
        //amCookieVal += "&Relationshipmask=" + document.getElementsByName("amRelation")[0].value;
        amCreateCookie("REG091202", amCookieVal, 1);
    }
    function mycarousel_itemVisibleInCallback(carousel, item, i, state, evt) {
        // The index() method calculates the index from a
        // given index who is out of the actual item range.
        var idx = carousel.index(i, mycarousel_itemList.length - 1);
        carousel.add(i, mycarousel_getItemHTML(mycarousel_itemList[idx - 1]));
    };

    function mycarousel_itemVisibleOutCallback(carousel, item, i, state, evt) {
        carousel.remove(i);
    };
    function mycarousel_getItemHTML(item) {
        return '<div onclick="OpenPic(\'' + item.href + '\')" style="cursor:pointer;text-align:center;" onmouseover="setBorder(this);" onmouseout="removeBorder(this);"><img style="opacity:0.8;filter:alpha(opacity=80);border:1px solid white;" src="' + item.url + '" height="75" alt="" /><div class="userDetails" dir="rtl"><div>' + item.username + '</div></div></div>';
    };
    function setBorder(elementId) {
        elementId.childNodes[0].style.border = "1px solid red";
        elementId.childNodes[0].style.opacity = "1";
        if (navigator.appName == "Microsoft Internet Explorer")
            elementId.childNodes[0].filters.alpha.opacity = "100";

        elementId.childNodes[1].style.background = "url(images/usernameBackgound.jpg) no-repeat";
        //elementId.childNodes[1].style.color="white";
        //elementId.childNodes[1].style.margin="0 3px";
    }

    function removeBorder(elementId) {
        elementId.childNodes[0].style.border = "1px solid white";
        elementId.childNodes[0].style.opacity = "0.8";
        if (navigator.appName == "Microsoft Internet Explorer")
            elementId.childNodes[0].filters.alpha.opacity = "80";

        elementId.childNodes[1].style.background = "";
        elementId.childNodes[1].style.color = "";
    }

    function OpenPic(Link) {
        //open link in popup mode
        //window.open(Link, 'name', 'height=650,width=600');

        window.open(Link);
    }
    function searchLink() {
        gender = document.getElementsByName("GenderID")[0].value;
        seeking = document.getElementsByName("SeekingGenderID")[0].value;
        areaCode = document.getElementsByName("AreaCode1")[0].value;
        age = document.getElementsByName("AgeRangeID")[0].value;
        link = "http://www.cupid.co.il/Applications/Search/SearchResults.aspx?GenderID=" + gender + "&SeekingGenderID=" + seeking + "&AgeRangeID=" + age + "&AreaCode1=" + areaCode + "&MinHeight=-2147483647&MaxHeight=-2147483647&SearchTypeID=2&CountryRegionID=105&HasPhotoFlag=1&StateRegionID=&LastCountryRegionID=&ZipCode=&PostalCode=&SearchOrderBy=4&GenderID=2&ForceSavePrefs=1";
        window.open(link, '_top');
    }
    var show = false;

    jQuery(document).ready(function() {
        jQuery('#mycarousel').jcarousel({
            scroll: 5,
            wrap: 'circular',
            auto: 10,
            itemVisibleInCallback: { onBeforeAnimation: mycarousel_itemVisibleInCallback },
            itemVisibleOutCallback: { onAfterAnimation: mycarousel_itemVisibleOutCallback }

        });
        readCookiesData();

        jQuery('#login-img-div img').click(function() {
            if (show == false) {
                jQuery('#div-login-form').show();
                jQuery('#login-btn-div').css("background-color", "#E8E8E8");
                jQuery('#div-login-form').css("background-color", "#E8E8E8");

                show = true;
            }
            else {
                jQuery('#div-login-form').hide();
                jQuery('#login-btn-div').css("background-color", "");
                jQuery('#div-login-form').css("background-color", "");
                show = false;
            }
        });


        jQuery('#btn-send img').click(function() {
            login();
        });

        //show/hide splash contents
        jQuery("#splash-content-header h2").click(
            function() {
                for (i = 1; i <= 4; i++) {
                    if (jQuery("#content-title-" + i).attr('class').split(' ').slice(-1) == "hover") {
                        jQuery("#content-title-" + i).toggleClass("unhover");
                        jQuery("#content-title-" + i).toggleClass("hover", false);
                        jQuery("#content-" + i).css("z-index", -999);
                    }
                }
                var lastClass = jQuery(this).attr('class').split(' ').slice(-1);
                var id = jQuery(this).attr('id').split('-').slice(-1);
                jQuery("#content-" + id).css("z-index", 1);
                if (lastClass == "unhover") {
                    jQuery(this).toggleClass("hover");
                    jQuery(this).toggleClass("unhover", false);
                }
                else {
                    jQuery(this).toggleClass("unhover");
                    jQuery(this).toggleClass("hover", false);
                }

            }
        );

    });

</script>

<div id="div-splash">
    <div id="main-splash">
        <div id="login-btn-div">
            <div id="login-img-div">
                <mn:Image ID="Image1" runat="server" TitleResourceConstant="TTL_LOGIN" ResourceConstant="ALT_LOGIN"
                    FileName="logn-btn.gif" />
            </div>
        </div>
        <div id="div-login-form">
            <div id="email">
                <span>אימייל:</span>
                <input type="text" name="emailaddress" id="emailaddress" onkeypress="return keypress(this, event);" /></div>
            <div id="password">
                <span>סיסמא:</span>
                <input type="password" name="password1" onkeypress="return keypress(this, event);" /></div>
            <div id="btn-send">
                <mn:Image ID="Image2" runat="server" TitleResourceConstant="TTL_BTN_SEND" ResourceConstant="ALT_BTN_SEND"
                    FileName="btn_send2.gif" />
            </div>
            <input type="checkbox" name="remember" id="remember" />
            זכור אותי | <a href="http://www.cupid.co.il/applications/memberservices/forgotpassword.aspx"
                style="text-decoration: underline; color: #000000;">שכחתי סיסמא</a>
        </div>
        <div id="registration-box">
            <div id="splash-logo">
            </div>
            <div id="register-form">
                <div id="join-now">
                    הצטרפו חינם</div>
                <table id="register-tbl">
                    <tr>
                        <td>
                            אני:
                        </td>
                        <td>
                            רוצה להכיר:
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <select name="amMyGender" id="amMyGender">
                                <option value="1" selected="selected">אישה</option>
                                <option value="2">גבר</option>
                            </select>
                        </td>
                        <td>
                            <select name="amToMeetGender">
                                <option value="2" selected="selected">גבר</option>
                                <option value="1">אישה</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            בחר שם משתמש:
                        </td>
                        <td>
                            בחר סיסמא:
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <input type="text" id="txtUsername1" name="Username" />
                        </td>
                        <td>
                            <input type="password" id="lgnPassword" name="Password" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" id="joinnow-td">
                            <a href="javascript:amGoTo()">
                                <mn:Image ID="Image3" runat="server" TitleResourceConstant="TTL_REGISTER_BTN" ResourceConstant="ALT_REGISTER_BTN"
                                    FileName="joinnow.gif" />
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" id="page-main-title">
                            <h1>
                                <mn:Txt ID="Txt13" runat="server" ResourceConstant="TXT_PAGE_MAIN_TITLE" />
                            </h1>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <div id="search-carousel">
        <div id="online-div">
            <!--
            <div id="carousel-title">
                מי באתר עכשיו?
            </div>
            <div id="search-title">
                חפשו מישהו לאהוב
            </div>
            <ul id="mycarousel" class="jcarousel-skin-tango">
            </ul>
            -->
            <div id="search-title">
                חפשו מישהו לאהוב</div>
            <div id="searchDiv">
                <div id="gender-div">
                    <span>אני:</span>
                    <select class="searchSelect" name="GenderID">
                        <option value="1">גבר</option>
                        <option value="2">אישה</option>
                    </select>
                </div>
                <div id="seek-div">
                    <span>רוצה להכיר:</span>
                    <select class="searchSelect" name="SeekingGenderID">
                        <option value="8">אישה </option>
                        <option value="4">גבר </option>
                    </select>
                </div>
                <div id="age-div">
                    <span>בגיל:</span>
                    <select class="searchSelect" name="AgeRangeID">
                        <option value="0">18-25 </option>
                        <option selected="" value="1">25-34 </option>
                        <option value="2">35-44</option>
                        <option value="3">45-55</option>
                        <option value="4">55+</option>
                    </select>
                </div>
                <div id="area-div">
                    <span>מאזור:</span>
                    <select class="searchSelect" name="AreaCode1">
                        <option value="02">ירושלים</option>
                        <option selected="selected" value="03">מרכז</option>
                        <option value="04">צפון</option>
                        <option value="08">דרום</option>
                        <option value="09">שרון</option>
                    </select>
                </div>
                <div id="find-btn-div">
                    <a href="javascript:searchLink();">
                        <mn:Image ID="Image6" runat="server" TitleResourceConstant="TTL_FIND_BTN" ResourceConstant="ALT_FIND_BTN"
                            FileName="new-find-btn.gif" />
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div id="splash-content">
        <div id="splash-content-header">
            <h2 id="content-title-1" class="four-column hover">
                <mn:Txt ID="Txt2" runat="server" ResourceConstant="TXT_CONTENT_TITLE_1" />
            </h2>
            <h2 id="content-title-2" class="four-column unhover">
                <mn:Txt ID="Txt1" runat="server" ResourceConstant="TXT_CONTENT_TITLE_2" />
            </h2>
            <h2 id="content-title-3" class="four-column unhover">
                <mn:Txt ID="Txt3" runat="server" ResourceConstant="TXT_CONTENT_TITLE_3" />
            </h2>
            <h2 id="content-title-4" class="four-column unhover">
                <mn:Txt ID="Txt4" runat="server" ResourceConstant="TXT_CONTENT_TITLE_4" />
            </h2>
        </div>
        <div id="splash-content-container">
            <div id="content-1" class="content-wrapper" style="position: absolute; width: 767px;
                z-index: 1;">
                <div class="content-title">
                    <mn:Txt ID="Txt9" runat="server" ResourceConstant="TXT_CONTENT_MAIN_1" />
                </div>
                <mn:Txt ID="Txt5" runat="server" ResourceConstant="TXT_CONTENT_1" />
                <div class="content-title-2">
                    <mn:Txt ID="Txt14" runat="server" ResourceConstant="TXT_CONTENT_MAIN_1_2" />
                </div>
                <mn:Txt ID="Txt15" runat="server" ResourceConstant="TXT_CONTENT_1_1" />
                <mn:Image runat="server" FileName="splash-banner-1.png" ResourceConstant="TXT_CONTENT_TITLE_1" />
            </div>
            <div id="content-2" class="content-wrapper" style="position: absolute; width: 767px;
                z-index: -999;">
                <div class="content-title">
                    <mn:Txt ID="Txt10" runat="server" ResourceConstant="TXT_CONTENT_MAIN_2" />
                </div>
                <mn:Txt ID="Txt6" runat="server" ResourceConstant="TXT_CONTENT_2" />
                <div class="content-title-2">
                    <mn:Txt ID="Txt16" runat="server" ResourceConstant="TXT_CONTENT_MAIN_2_2" />
                </div>
                <mn:Txt ID="Txt17" runat="server" ResourceConstant="TXT_CONTENT_2_2" />
                <mn:Image ID="Image7" runat="server" FileName="splash-banner-2.png" ResourceConstant="TXT_CONTENT_TITLE_2" />
            </div>
            <div id="content-3" class="content-wrapper" style="position: absolute; width: 767px;
                z-index: -999;">
                <div>
                    <mn:Image ID="Image4" runat="server" TitleResourceConstant="TTL_SUCCESS_1" ResourceConstant="ALT_SUCCESS_1"
                        FileName="megali.jpg" />
                    <div class="content-title">
                        <mn:Txt ID="Txt7" runat="server" ResourceConstant="TXT_CONTENT_MAIN_3" />
                    </div>
                    <mn:Txt ID="Txt11" runat="server" ResourceConstant="TXT_CONTENT_3" />
                    <div>
                        <mn:Txt ID="Txt18" runat="server" ResourceConstant="TXT_CONTENT_LINK_3" />
                    </div>
                </div>
                <div id="splash-success-div-2">
                    <mn:Image ID="Image5" runat="server" TitleResourceConstant="TTL_SUCCESS_2" ResourceConstant="ALT_SUCCESS_2"
                        FileName="guy.jpg" />
                    <div class="content-title">
                        <mn:Txt ID="Txt19" runat="server" ResourceConstant="TXT_CONTENT_MAIN_3_2" />
                    </div>
                    <mn:Txt ID="Txt20" runat="server" ResourceConstant="TXT_CONTENT_3_2" />
                    <div>
                        <mn:Txt ID="Txt21" runat="server" ResourceConstant="TXT_CONTENT_LINK_3_2" />
                    </div>
                </div>
            </div>
            <div id="content-4" class="content-wrapper" style="position: absolute; width: 767px;
                z-index: -999;">
                <div class="content-title">
                    <mn:Txt ID="Txt12" runat="server" ResourceConstant="TXT_CONTENT_MAIN_4" />
                </div>
                <mn:Txt ID="Txt8" runat="server" ResourceConstant="TXT_CONTENT_4" />
                <div class="content-title-2">
                    <mn:Txt ID="Txt22" runat="server" ResourceConstant="TXT_CONTENT_MAIN_4_2" />
                </div>
                <mn:Txt ID="Txt23" runat="server" ResourceConstant="TXT_CONTENT_4_2" />
                <mn:Image ID="Image8" runat="server" FileName="splash-banner-3.png" ResourceConstant="TXT_CONTENT_TITLE_4" />
            </div>
        </div>
    </div>
</div>
<div id="footer">
    <div id="footer-container">
        <mn1:Footer20 ID="Footer20" Runat="server" />
    </div>
</div>
