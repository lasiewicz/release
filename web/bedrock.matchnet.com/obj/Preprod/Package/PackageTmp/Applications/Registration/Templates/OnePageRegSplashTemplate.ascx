﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OnePageRegSplashTemplate.ascx.cs"
    Inherits="Matchnet.Web.Applications.Registration.Templates.OnePageRegSplashTemplate" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="uc1" TagName="ValidationMessage" Src="/Applications/Registration/Controls/ValidationMessage.ascx" %>
<%@ Register TagPrefix="mn" TagName="Copyright" Src="/Framework/Ui/PageElements/Copyright.ascx" %>
<%@ Register TagPrefix="mn1" TagName="Footer20" Src="/Framework/Ui/Footer20.ascx" %>
<%@ Register TagPrefix="mn" TagName="AdUnit" Src="/Framework/UI/Advertising/AdUnit.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ImageRotator" Src="/Framework/Ui/BasicElements/ImageRotator.ascx" %>
<%--third party js--%><script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script><%--must be in noConflict mode--%>
<%--jQuery CDN fallback--%><script type="text/javascript">window.jQuery || document.write('<script type="text/javascript" src="/javascript20/library/jquery-1.7.1.min.js">\x3C/script>')</script>
<script type='text/javascript' src='/javascript20/registrationOverlay/jquery.autocomplete.js'></script>
<script type='text/javascript' src='/javascript20/onePageReg/one-page-reg.js'></script>
<script type='text/javascript' src='/javascript20/registrationOverlay/ajaxfileupload.js'></script>
<script type='text/javascript'>
    mobileLinksFromFullWebsite();
</script>
<div id="min-max-container">
    <div id="header" class="clearfix">
        <div id="header-logo">
            <p class="header-message">
                <mn:Image ID="imgLogo" runat="server" TitleResourceConstant="TTL_SITE_LOGO" ResourceConstant="ALT_SITE_LOGO"
                    FileName="trans.gif" NavigateURLClass="logo" />
                <div>
                    <mn:Txt ID="mntxt5508" runat="server" ResourceConstant="TXT_HEADER_MESSAGE" ExpandImageTokens="true" />
                </div>
            </p>
        </div>
        <div id="languages">
            <a href="http://www.jdate.co.il/"></a>
            <mn:Txt ID="mntxt1627" runat="server" ResourceConstant="TXT_HEBREW" ExpandImageTokens="true" />
            | <a href="http://www.jdate.fr/">
                <mn:Txt ID="mntxt3959" runat="server" ResourceConstant="TXT_FRENCH" ExpandImageTokens="true" />
            </a>
            <br />
        </div>
        <div id="login-form-btn">
            <a href="javascript:showLogin();">
                <mn:Txt ID="Txt1" runat="server" ResourceConstant="SHOW_LOGIN_TXT" />
            </a>
        </div>
        <div id="loginForm">
            <div id="title">
                <div id="email-title">
                    <mn:Txt ID="Txt2" runat="server" ResourceConstant="EMAIL_TITLE" />
                </div>
                <div id="pass-title">
                    <mn:Txt ID="Txt3" runat="server" ResourceConstant="PASSWORD_TITLE" />
                </div>
                <a href="javascript:showLogin();" id="close-login-div">
                    <mn:Image ID="Image41" FileName="close-btn.png" runat="server" />
                </a>
            </div>
            <div id="login-inputs">
                <input type="text" id="email_login" name="email_login" />
                <input type="password" id="password_login" name="password_login" />
            </div>
            <div id="login-btn-div">
                <a href="javascript:login();">
                    <mn:Image ID="Image2" FileName="login-enter-btn.png" runat="server" />
                </a>
            </div>
            <div id="rememberme-div">
                <input type="checkbox" id="remember" />
                <mn:Txt ID="Txt4" runat="server" ResourceConstant="REMEMBER_ME_TXT" />
            </div>
            <div id="forgot-pass-div">
                <a href="/applications/memberservices/ForgotPassword.aspx">
                    <mn:Txt ID="Txt5" runat="server" ResourceConstant="FORGOT_PASS_TXT" />
                </a>
            </div>
        </div>
    </div>
    <div id="content-container" <%=GetOnePageRegContentDivCSS() %>>
        <div id="content">
            <div id="one-page-reg-content" <%=GetOnePageRegContentDivCSS() %>>
                <asp:PlaceHolder runat="server" ID="plcMol" Visible="false">
                    <div id="mol-counter">
                        <mn:Txt ID="molCounter" runat="server" ResourceConstant="TXT_MOL" ExpandImageTokens="true" />
                    </div>
                </asp:PlaceHolder>
                <div id="one-page-reg-form-bg">
                    &nbsp;
                </div>
                <div id="one-page-reg-form">
                    <div id="general-error">
                        <mn:Txt ID="Txt6" runat="server" ResourceConstant="GENERAL_ERROR_TXT" />
                    </div>
                    <asp:PlaceHolder ID="phContent" runat="server">
                        <uc1:ValidationMessage runat="server" ID="txtValidation" Visible="false" />
                    </asp:PlaceHolder>
                    <div class="submit-btn"></div>
                   <%-- <mn:Image ID="Image4" FileName="join-free-btn.png" CssClass="submit-btn" runat="server" />--%>
                </div>
                <div id="ajax-loader">
                    <div>
                        <mn:Image ID="Image5" runat="server" FileName="ajax-loader.gif" />
                    </div>
                </div>
            </div>
            <div id="one-page-reg-tip" <%=GetOnePageRegContentDivTipCSS() %>>
                <mn:Txt runat="server" ID="txtOnePageRegTip" />
            </div>
        </div>
        <mn:Txt ID="mntxt6945" runat="server" ResourceConstant="TXT_SPLASH_SEO" ExpandImageTokens="true" />
    </div>
</div>
<div id="footer">
    <div id="footer-content">
        <mn1:Footer20 ID="Footer20" runat="server" />
    </div>
</div>
<mn:txt id="txtGoogleRemarketingCode" runat="server" resourceconstant="JAVASCRIPT_GOOGLE_REMARKETING_TAG" expandimagetokens="true" />
