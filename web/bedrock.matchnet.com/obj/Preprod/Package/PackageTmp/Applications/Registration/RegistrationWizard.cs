﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Web;
using Matchnet.Web.Framework;
using System.Xml.Serialization;
using Matchnet.Web.Framework.Enumerations;
using Matchnet.Web.Framework.Managers;
using Matchnet.Web.Framework.Ui.FormElements;
using Matchnet.Web.Framework.TemplateControls;

using System.Net;
using Matchnet.ExternalMail.ServiceAdapters;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects;
using Matchnet.Member.ValueObjects.Photos;
using Matchnet.Web.Framework.Globalization;
using Matchnet.Web.Applications.Registration.Templates;
using Matchnet.Session.ValueObjects;
using Matchnet.Session.ServiceAdapters;
using Matchnet.Web.Applications.MemberProfile;
using Matchnet.EmailNotifier.ValueObjects;
using System.Collections;
using Matchnet.Web.Interfaces;

namespace Matchnet.Web.Applications.Registration
{
    public class RegistrationWizard : Framework.TemplateControls.Wizard
    {
        private const string FACEBOOK_SCENARIO_NAME = "FBRegScen";

        private Scenario _regScenario;
        private Step _regStep;
        private RegistrationTemplateBase templateControl;
        private List<AttributeControl> _controls;
        private Dictionary<string, string> _properties;
        private bool __debugTrace;
        private String _sessionID;
        private ScheduledEvent _registrationEvent;
        private Hashtable _excludedFromScheduler;
        private SettingsManager _settingsManager = null;

        public SettingsManager SettingsManager
        {
            get
            {
                if (_settingsManager == null)
                {
                    _settingsManager = new SettingsManager();
                }
                return _settingsManager;
            }
        }

        public Scenario RegistrationScenario
        {
            get { return _regScenario; }
        }

        public Step RegistrationStep
        {
            get
            {

                return _regScenario.Steps[StepID - 1];
            }
        }

        public String SessionID
        {
            get { return _sessionID; }
            set { _sessionID = value; }
        }

        public Hashtable ExcludedFields
        {
            get { return _excludedFromScheduler; }
        }

        public Dictionary<string, string> Property
        {
            get { return _properties; }
        }
        
        public ScheduledEvent RegistrationEvent { get { return _registrationEvent; } set { _registrationEvent = value; } }

        public bool DebugTrace
        {
            set { __debugTrace = value; }
        }

        public RegistrationFlowType FlowType { get; private set; }


        public RegistrationWizard(Brand brand, RegistrationFlowType flowType):base(brand)
        {
            FlowType = flowType;
        }

        public void LoadRegistrationSteps(bool overrideScenariosCache)
        {
            try
            {
                ScenarioChooser scenarioChooser = new ScenarioChooser();
                if (CompletedSteps <= 0 && String.IsNullOrEmpty(ScenarioName))
                {
                    FrameworkGlobals.Trace(__debugTrace, "RegistrationWizard.LoadRegistrationSteps", "GetRandomScenario - 1", this);
                    _regScenario = scenarioChooser.GetRandomScenario(_brand, FlowType, false, new List<string>() {FACEBOOK_SCENARIO_NAME});
                    FrameworkGlobals.Trace(__debugTrace, "RegistrationWizard.LoadRegistrationSteps", "GetRandomScenario - 2", this);
                }
                else
                {
                    FrameworkGlobals.Trace(__debugTrace, "RegistrationWizard.LoadRegistrationSteps", "GetNamedScenario - 1", this);
                    _regScenario = scenarioChooser.GetNamedScenario(_brand, FlowType, ScenarioName, false, new List<string>() { FACEBOOK_SCENARIO_NAME });
                    if (_regScenario == null)
                    {
                        FrameworkGlobals.Trace(__debugTrace, "RegistrationWizard.LoadRegistrationSteps", "GetNamedScenario - not found - ", this);
                        _regScenario = scenarioChooser.GetRandomScenario(_brand, FlowType, false, new List<string>() { FACEBOOK_SCENARIO_NAME });
                    }
                    FrameworkGlobals.Trace(__debugTrace, "RegistrationWizard.LoadRegistrationSteps", "GetNamedScenario - 2", this);
                }
                if (StepID > _regScenario.Steps.Count)
                {
                    //possibility of 2 wizards opened
                    StepID = 1;
                    CompletedSteps = 0;
                    _regStep = _regScenario.GetStep(StepID);
                    FrameworkGlobals.Trace(__debugTrace, "RegistrationWizard.LoadRegistrationSteps", string.Format("StepId {0} greater than steps count ({1})", StepID.ToString(), _regScenario.Steps.Count.ToString()), this);
                }
                else
                {
                    _regStep = _regScenario.GetStep(StepID);
                }
                ScenarioName = _regScenario.Name;
            }
            catch (Exception ex)
            {

            }
        }

        public void AddProperty(string name, string value)
        {
            if (_properties == null)
                _properties = new Dictionary<string, string>();

            if (!String.IsNullOrEmpty(name) && !String.IsNullOrEmpty(value))
                _properties[name.ToLower()] = value;
        }


        public string GetProperty(string name)
        {
            string prop = "";
            try
            {
                if (_properties == null)
                    return "";

                if (!String.IsNullOrEmpty(name))
                    prop = _properties["prop_" + name];

                return prop;
            }
            catch (Exception ex)
            {
                return prop;
            }
        }

        public override void Persist()
        {
            base.Persist();

            if (_properties == null)
                return;

            foreach (string key in _properties.Keys)
            {
                Persistence[key] = _properties[key];
                FrameworkGlobals.Trace(__debugTrace, "RegistrationWizard.PersistToCookie", "PersistToCookie", key + "=" + _properties[key]);
            }

            Persistence.Persist(_brand);
        }

       public override void Load(bool loadNewWizard)
        {

            base.Load(loadNewWizard);
            if (!loadNewWizard)
            {
                FrameworkGlobals.Trace(__debugTrace, "RegistrationWizard.Load", "Loading", null);
                System.Collections.Specialized.NameValueCollection namevalues = Persistence.Values;
                if (namevalues == null)
                    return;


                FrameworkGlobals.Trace(__debugTrace, "RegistrationWizard.Load", "Reading cookie nameval collection", null);
                FrameworkGlobals.Trace(__debugTrace, "RegistrationWizard.Load", "Cookie keys are", namevalues.AllKeys.ToString());
                for (int i = 0; i < namevalues.Count; i++)
                {
                    string key = namevalues.AllKeys[i];
                    if (key == null)
                        continue;
                    FrameworkGlobals.Trace(__debugTrace, "RegistrationWizard.Load", "Reading values from cookie", key + "=" + namevalues[key]);
                    if (key.IndexOf("prop_") >= 0)
                    {
                        string val = namevalues[key];

                        FrameworkGlobals.Trace(__debugTrace, "RegistrationWizard.Load", "AddingProperty from cookie", key + "=" + val);
                        AddProperty(key, val);
                    }
                }
            }

            bool regRecepatureEnabled = SettingsManager.GetSettingBool(SettingConstants.ENABLE_REGISTRATION_CAPTURE, _brand);
            if (regRecepatureEnabled)
            {
                _excludedFromScheduler = RegistrationCapture.GetExcludedFields();
                _registrationEvent = RegistrationCapture.CreateRegistrationEventFromCookie(Persistence, _brand, _excludedFromScheduler);
            }
        }

        public override string ToString()
        {
            string retStr = "";
            System.Text.StringBuilder bld = new System.Text.StringBuilder();
            try
            {
                bld.Append("ScenarioName=" + ((ScenarioName != null) ? ScenarioName : "") + ", ");
                bld.Append("CompletedSteps=" + CompletedSteps.ToString() + ", ");
                bld.Append("StepID=" + StepID.ToString() + ", ");
                retStr = bld.ToString();
                bld = null;
                return retStr;
            }
            catch (Exception ex) { bld = null; return retStr; }

        }
        public void CacheRequest(ICurrentRequest req)
        {
            try
            {

                if (req.Form != null)
                {
                    foreach (string key in req.Form.Keys)
                    {
                        string name = "prop_" + key.ToLower();
                        string val = req.Form[key];
                        AddProperty(name, HttpUtility.UrlEncode(val));
                        if (__debugTrace)
                            FrameworkGlobals.Trace(__debugTrace, "RegistrationWizard.CacheRequest", "AddingProperty from request form", name + "=" + val);
                    }

                }
                if (req.QueryString.Count > 0)
                {
                    foreach (string key in req.QueryString.Keys)
                    {
                        if (!key.Contains("404;")) // A fix for friendly url becuase it messed up the cookie
                        {
                            string name = "prop_" + key.ToLower();
                            string val = req.QueryString[key];
                            AddProperty(name, val);
                            FrameworkGlobals.Trace(__debugTrace, "RegistrationWizard.CacheRequest", "AddingProperty from request qstring", name + "=" + val);
                        }
                    }
                }
            }
            catch (Exception ex)
            { }
        }

        public bool ShowPreviousStep()
        {
            bool show = true;
            try
            {
                if (StepID > 1)
                {
                    show = true;
                }
                else
                {
                    show = false;
                }
                return show;
            }
            catch (Exception ex)
            { return false; }

        }

        public string GetRegistrationPersistenceValue(string key)
        {
            try
            {
                return Persistence[key];
            }
            catch (Exception ex)
            {
                return "";
            }
        }
    }
}
