﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RegistrationOverlay.ascx.cs"
    Inherits="Matchnet.Web.Applications.Registration.RegistrationOverlay" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<asp:PlaceHolder ID="phContent" runat="server"></asp:PlaceHolder>
<div class="error">
    <mn:Txt ID="regValidation" runat="server" />
</div>
<asp:PlaceHolder ID="PlaceHolderOmnitureMboxRegBegin" runat="server" Visible="false">
    <div class="mboxDefault">
    </div>

    <script type="text/javascript">

        mboxCreate('<asp:Literal id=litmboxPageName runat=server/>');

        //set TNT variable when page loads (needs to be after mboxes have loaded) 
        var tntInput = s.trackTNT();

        //call this function after everything else has loaded 
        trackTNT(s_account, tntInput);

        /* For tracking TNT data in SC */
        function trackTNT(s_account, tntInput) {
            var s = s_gi(s_account);

            s.linkTrackVars = "tnt,<asp:Literal id=litmboxEVar runat=server/>";
            s.linkTrackEvents = "None";

            //variable for TNT classifications 
            s.eVar47 = s.tnt = tntInput;

            s.tl(false, 'o', 'For tracking TNT');
        } 
    </script>

</asp:PlaceHolder>

<!-- Script to capture black box values -->
<asp:PlaceHolder ID="IovationBlackboxScript" runat="server">
<input type="hidden" name="ioBlackBox" id="overlayioBlackBox"/>
<!-- Blackbox javascript -->
<script type="text/javascript" language="JavaScript" src="https://mpsnare.iesnare.com/snare.js">
</script>
 <script type="text/javascript" language="JavaScript">
     var io_install_flash = false; // do not install Flash
     var io_enable_rip = true; // collect Real IP information
     GetIOBB(0);

     function GetIOBB(pass) {
         if (typeof ioGetBlackbox == 'function') {
             var bb_data = ioGetBlackbox();
             var blackbox = bb_data.blackbox;
             if (bb_data.finished || pass > 10) {
                 document.getElementById('overlayioBlackBox').value = blackbox;
                 //Store blackbox value in the cookie. This will be retrieved while sending ROLF request.
                 jQuery.cookie('regiobb', blackbox, { expires: 1 });
                 return false;
             }
             setTimeout("GetIOBB(" + (pass + 1) + ")", 100);
         }
         return false;
     }
     
//     var io_bb_callback = function (bb, isComplete) {
//         var ioBlackBoxfield = document.getElementById('overlayioBlackBox');
//         ioBlackBoxfield.value = bb;
//         //Store blackbox value in the cookie. This will be retrieved while sending ROLF request.
//         jQuery.cookie('regiobb', bb, { expires: 1 });
//     };
</script>
</asp:PlaceHolder>

