﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SplashTemplate11b.ascx.cs"
    Inherits="Matchnet.Web.Applications.Registration.Templates.SplashTemplate11b" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="uc1" TagName="ValidationMessage" Src="/Applications/Registration/Controls/ValidationMessage.ascx" %>
<%@ Register TagPrefix="mn" TagName="Copyright" Src="/Framework/Ui/PageElements/Copyright.ascx" %>
<%@ Register TagPrefix="mn1" TagName="Footer20" Src="/Framework/Ui/Footer20.ascx" %>
<%@ Register TagPrefix="mn" TagName="AdUnit" Src="/Framework/UI/Advertising/AdUnit.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ImageRotator" Src="/Framework/Ui/BasicElements/ImageRotator.ascx" %>

<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
<script type="text/javascript">    window.jQuery || document.write('<script type="text/javascript" src="/javascript20/library/jquery-1.4.2.min.js">\x3C/script>')</script>
<div id="min-max-container">
<mn:Txt ID="txtMinMaxContainerContent" runat="server" ResourceConstant="TXT_MIN_MAX_CONTAINER_CONTENT" ExpandImageTokens="false" />
  
</div>
<%--<mn:Txt runat="server" ResourceConstant="AM_SPLASH_BANNER_BELOW" ID="txtAmSplashBannerBelow" />--%>
<div style="width: 100%;">
    <div id="footerContainer" runat="server">
        <div id="footer">
            <div class="footer-container">
                <mn1:Footer20 ID="Footer20" Runat="server" /></div>
        </div>
    </div>
    <div id="footer-narrow">
        <div class="footer-container">
            <mn:Copyright id="copyright" runat="server">
            </mn:Copyright></div>
    </div>
</div>
<%--
<script type="text/javascript">
//set TNT variable when page loads (needs to be after mboxes have loaded) 
        var tntInput = s.trackTNT(); 
        //call this function after everything else has loaded 
        trackTNT( s_account , tntInput ); 

        /* For tracking TNT data in SC */ 
        function trackTNT(s_account , tntInput) 
                { 
                var s=s_gi( s_account ); 
                 
                s.linkTrackVars="tnt,<asp:Literal id='litmboxEVar' runat='server'/>" 
                s.linkTrackEvents="None" 
                 
                //variable for TNT classifications 
                s.eVar47 = s.tnt = tntInput; 
                 
                s.tl( false , 'o' , 'For tracking TNT' ); 
                } 
</script>--%>
<%--<mn:Txt runat="server" ResourceConstant="AM_SPLASH_FOOTER_BELOW" ID="txtAmSplashFooterBelow" />--%>
