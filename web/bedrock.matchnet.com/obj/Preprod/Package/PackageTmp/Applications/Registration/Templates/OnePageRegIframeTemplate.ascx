﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OnePageRegIframeTemplate.ascx.cs"
    Inherits="Matchnet.Web.Applications.Registration.Templates.OnePageRegIframeTemplate" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="uc1" TagName="ValidationMessage" Src="/Applications/Registration/Controls/ValidationMessage.ascx" %>
<%@ Register TagPrefix="mn" TagName="Copyright" Src="/Framework/Ui/PageElements/Copyright.ascx" %>
<%@ Register TagPrefix="mn1" TagName="Footer20" Src="/Framework/Ui/Footer20.ascx" %>
<%@ Register TagPrefix="mn" TagName="AdUnit" Src="/Framework/UI/Advertising/AdUnit.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ImageRotator" Src="/Framework/Ui/BasicElements/ImageRotator.ascx" %>
<%--third party js--%><script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script><%--must be in noConflict mode--%>
<%--jQuery CDN fallback--%><script type="text/javascript">window.jQuery || document.write('<script type="text/javascript" src="/javascript20/library/jquery-1.7.1.min.js">\x3C/script>')</script>

<script type='text/javascript' src='/javascript20/registrationOverlay/jquery.autocomplete.js'></script>
<script type='text/javascript' src='/javascript20/onePageReg/one-page-reg.js'></script>
<script type='text/javascript' src='/javascript20/registrationOverlay/ajaxfileupload.js'></script>
<div id="iframe-container">
    <div id="content-container" <%=GetOnePageRegContainerDivCSS() %>>
        <div id="content">
            <div id="one-page-reg-content" <%=GetOnePageRegContentDivCSS() %>>
                <asp:PlaceHolder runat="server" ID="plcMol" Visible="false">
                    <div id="mol-counter">
                        <mn:Txt ID="molCounter" runat="server" ResourceConstant="TXT_MOL" ExpandImageTokens="true" />
                    </div>
                </asp:PlaceHolder>
                <div id="one-page-reg-form-bg">
                    &nbsp;
                </div>
                <div id="one-page-reg-form">
                    <div id="general-error">
                        <mn:Txt ID="Txt6" runat="server" ResourceConstant="GENERAL_ERROR_TXT" />
                    </div>
                    <asp:PlaceHolder ID="phContent" runat="server">
                        <uc1:ValidationMessage runat="server" ID="txtValidation" Visible="false" />
                    </asp:PlaceHolder>
                    <div class="submit-btn"></div>
                    <!--<mn:Image ID="Image4" FileName="goto-site-iframe.jpg" CssClass="submit-btn" runat="server" />-->
                </div>
                <div id="ajax-loader">
                    <div>
                        <mn:Image ID="Image5" runat="server" FileName="ajax-loader.gif" />
                    </div>
                </div>
            </div>
            <div id="after_reg_cont" style="display: none;">
                <div id="after_reg">
                    <div id="after_reg_link_container">
                        <asp:HyperLink runat="server" ID="lnkSearchResults" rel="external" Target="_blank"
                            Style="display: block; width: 117px; height: 35px;" NavigateUrl="/Applications/Search/SearchResults.aspx">
                            <mn:Image ID="Link_Image" runat="server" FileName="after_reg_btn.jpg" />
                        </asp:HyperLink>
                    </div>
                </div>
            </div>
            <div id="fbiframe_after_reg_cont" style="display: none;">
                <div id="fb_after_reg">
                    <div id="buy_container">
                        <asp:HyperLink runat="server" ID="lnkSub" rel="external" Target="_blank"
                            Style="display: block; width: 126px; height: 35px;" NavigateUrl="/Applications/Subscription/Subscribe.aspx?prtid=41">
                            <mn:Image ID="Image1" runat="server" FileName="tobuy.jpg" />
                        </asp:HyperLink>
                    </div>
                    <div id="complete_container">
                        <asp:HyperLink runat="server" ID="lnkRegStep2" rel="external" Target="_blank"
                            Style="display: block; width: 112px; height: 34px;" NavigateUrl="/Applications/MemberProfile/ViewProfile.aspx?Mode=Edit">
                            <mn:Image ID="Image2" runat="server" FileName="tocomplete.jpg" />
                        </asp:HyperLink>
                    </div>
                    <div id="matches_container">
                        <asp:HyperLink runat="server" ID="lnkSearchResults2" rel="external" Target="_blank"
                            Style="display: block; width: 112px; height: 33px;" NavigateUrl="/Applications/Search/SearchResults.aspx?NavPoint=sub">
                            <mn:Image ID="Image3" runat="server" FileName="yourmatches.jpg" />
                        </asp:HyperLink>
                    </div>
                </div>
            </div>
            <div id="fbiframe_new_after_reg_cont" style="display: none;">
                <div id="new_after_reg_cont">
                    <div id="buy_container">
                        <asp:HyperLink runat="server" ID="lnkSub2" rel="external" Target="_blank"
                            Style="display: block; width: 126px; height: 35px;" NavigateUrl="/Applications/Subscription/Subscribe.aspx?prtid=41">
                            <mn:Image ID="Image14" runat="server" FileName="tobuy_new.png" />
                        </asp:HyperLink>
                    </div>
                    <div id="complete_container">
                        <asp:HyperLink runat="server" ID="lnkRegStep3" rel="external" Target="_blank"
                            Style="display: block; width: 112px; height: 34px;" NavigateUrl="/Applications/MemberProfile/ViewProfile.aspx?Mode=Edit">
                            <mn:Image ID="Image16" runat="server" FileName="tocomplete_new.png" />
                        </asp:HyperLink>
                    </div>
                    <div id="matches_container">
                        <asp:HyperLink runat="server" ID="lnkSearchResults3" rel="external" Target="_blank"
                            Style="display: block; width: 112px; height: 33px;" NavigateUrl="/Applications/Search/SearchResults.aspx?NavPoint=sub">
                            <mn:Image ID="Image17" runat="server" FileName="yourmatches_new.png" />
                        </asp:HyperLink>
                    </div>
                </div>
            </div>
            <div id="mobile_after_reg_cont" style="display: none;">
                <div id="mobile_after_reg">
                    <div id="matches_container">
	                    <asp:HyperLink runat="server" ID="lnkSearchResults4" rel="external" 
		                    Style="display: block; width: 300px; height: 100px;" NavigateUrl="http://m.jdate.co.il/search/results">
		                    <mn:Image ID="Image21" runat="server" FileName="yourmatches_mobile.png" />
	                    </asp:HyperLink>
                    </div>
                </div>
            </div>
            <div id="mobile_after_reg_cont_uk" style="display: none;">
                <div id="mobile_after_reg_uk">
                    <div id="matches_container">
	                    <asp:HyperLink runat="server" ID="lnkSearchResults5" rel="external" Target="_blank"
		                    Style="display: block; width: 112px; height: 33px;" NavigateUrl="http://m.jdate.co.uk/search/results">
		                    <mn:Image ID="Image22" runat="server" FileName="yourmatchesuk_mobile.png" />
	                    </asp:HyperLink>
                    </div>
                </div>
            </div>
            <div id="after_reg_cont_walla" style="display: none;">
                <div id="after_reg_walla">
                    <div id="matches_container">
                        <asp:HyperLink runat="server" ID="lnkSearchResults6" rel="external" Target="_blank"
                            Style="display: block; width: 189px; height: 49px;" NavigateUrl="/Applications/Search/SearchResults.aspx">
                            <mn:Image ID="Image23" runat="server" FileName="site/jdate-co-il/iframe_after_matches.png" />
                        </asp:HyperLink>
                    </div>
                </div>
            </div>
            <div id="fbiframe_after_reg_cont_tl" style="display: none;">
                <div id="after-reg-ditd">
                    <div id="buy_container">
                        <asp:HyperLink runat="server" ID="lnkSub3" rel="external" Target="_blank"
                            Style="display: block; width: 175px; height: 48px;" NavigateUrl="/Applications/Subscription/Subscribe.aspx?prtid=41">
                            <mn:Image ID="Image27" runat="server" FileName="site/jdate-co-il/tobuy-tl.png" />
                        </asp:HyperLink>
                    </div>
                    <div id="complete_container">
                        <asp:HyperLink runat="server" ID="lnkRegStep4" rel="external" Target="_blank"
                            Style="display: block; width: 148px; height: 36px;" NavigateUrl="/Applications/MemberProfile/ViewProfile.aspx?Mode=Edit">
                            <mn:Image ID="Image28" runat="server" FileName="site/jdate-co-il/tocomplete-tl.png" />
                        </asp:HyperLink>
                    </div>
                    <div id="matches_container">
                        <asp:HyperLink runat="server" ID="lnkSearchResults7" rel="external" Target="_blank"
                            Style="display: block; width: 129px; height: 36px;" NavigateUrl="/Applications/Search/SearchResults.aspx?NavPoint=sub">
                            <mn:Image ID="Image29" runat="server" FileName="site/jdate-co-il/yourmatches-tl.png" />
                        </asp:HyperLink>
                    </div>
                </div>
            </div>
            <div id="logo_link" title="www.jdate.co.il" style="display: none;">
                &nbsp;
            </div>
        </div>
    </div>
</div>
