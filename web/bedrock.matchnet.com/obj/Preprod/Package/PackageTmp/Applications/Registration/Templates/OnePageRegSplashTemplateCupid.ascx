﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OnePageRegSplashTemplateCupid.ascx.cs"
    Inherits="Matchnet.Web.Applications.Registration.Templates.OnePageRegSplashTemplateCupid" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="uc1" TagName="ValidationMessage" Src="/Applications/Registration/Controls/ValidationMessage.ascx" %>
<%@ Register TagPrefix="mn" TagName="Copyright" Src="/Framework/Ui/PageElements/Copyright.ascx" %>
<%@ Register TagPrefix="mn1" TagName="Footer20" Src="/Framework/Ui/Footer20.ascx" %>
<%@ Register TagPrefix="mn" TagName="AdUnit" Src="/Framework/UI/Advertising/AdUnit.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ImageRotator" Src="/Framework/Ui/BasicElements/ImageRotator.ascx" %>

<%--third party js--%><script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script><%--must be in noConflict mode--%>
<%--jQuery CDN fallback--%><script type="text/javascript">window.jQuery || document.write('<script type="text/javascript" src="/javascript20/library/jquery-1.7.1.min.js">\x3C/script>')</script>

<script type='text/javascript' src='/javascript20/registrationOverlay/jquery.autocomplete.js'></script>

<script type='text/javascript' src='/javascript20/onePageReg/one-page-reg.js'></script>

<script type='text/javascript' src='/javascript20/registrationOverlay/ajaxfileupload.js'></script>

<script type='text/javascript'>
    function setBorder(elementId) {
        elementId.childNodes[0].style.border = "1px solid red";
        elementId.childNodes[0].style.opacity = "1";
        if (navigator.appName == "Microsoft Internet Explorer")
            elementId.childNodes[0].filters.alpha.opacity = "100";

        elementId.childNodes[1].style.background = "url(images/usernameBackgound.jpg) no-repeat";
        //elementId.childNodes[1].style.color="white";
        //elementId.childNodes[1].style.margin="0 3px";
    }

    function removeBorder(elementId) {
        elementId.childNodes[0].style.border = "1px solid white";
        elementId.childNodes[0].style.opacity = "0.8";
        if (navigator.appName == "Microsoft Internet Explorer")
            elementId.childNodes[0].filters.alpha.opacity = "80";

        elementId.childNodes[1].style.background = "";
        elementId.childNodes[1].style.color = "";
    }

    function OpenPic(Link) {
        //open link in popup mode
        //window.open(Link, 'name', 'height=650,width=600');

        window.open(Link);
    }
    function searchLink() {
        gender = document.getElementsByName("GenderID")[0].value;
        seeking = document.getElementsByName("SeekingGenderID")[0].value;
        areaCode = document.getElementsByName("AreaCode1")[0].value;
        age = document.getElementsByName("AgeRangeID")[0].value;
        link = "http://www.cupid.co.il/Applications/Search/SearchResults.aspx?GenderID=" + gender + "&SeekingGenderID=" + seeking + "&AgeRangeID=" + age + "&AreaCode1=" + areaCode + "&MinHeight=-2147483647&MaxHeight=-2147483647&SearchTypeID=2&CountryRegionID=105&HasPhotoFlag=1&StateRegionID=&LastCountryRegionID=&ZipCode=&PostalCode=&SearchOrderBy=4&GenderID=2&ForceSavePrefs=1";
        window.open(link, '_top');
    }
    var show = false;

    jQuery(document).ready(function() {

        jQuery('#login-img-div img').click(function() {
            if (show == false) {
                jQuery('#div-login-form').show();
                jQuery('#login-btn-div').css("background-color", "#E8E8E8");
                jQuery('#div-login-form').css("background-color", "#E8E8E8");

                show = true;
            }
            else {
                jQuery('#div-login-form').hide();
                jQuery('#login-btn-div').css("background-color", "");
                jQuery('#div-login-form').css("background-color", "");
                show = false;
            }
        });


        jQuery('#btn-send img').click(function() {
            login();
        });

        //show/hide splash contents
        jQuery("#splash-content-header h2").click(
            function() {
                for (i = 1; i <= 4; i++) {
                    if (jQuery("#content-title-" + i).attr('class').split(' ').slice(-1) == "hover") {
                        jQuery("#content-title-" + i).toggleClass("unhover");
                        jQuery("#content-title-" + i).toggleClass("hover", false);
                        jQuery("#content-" + i).css("z-index", -999);
                    }
                }
                var lastClass = jQuery(this).attr('class').split(' ').slice(-1);
                var id = jQuery(this).attr('id').split('-').slice(-1);
                jQuery("#content-" + id).css("z-index", 1);
                if (lastClass == "unhover") {
                    jQuery(this).toggleClass("hover");
                    jQuery(this).toggleClass("unhover", false);
                }
                else {
                    jQuery(this).toggleClass("unhover");
                    jQuery(this).toggleClass("hover", false);
                }

            }
        );

    });

</script>

</script>
<div id="div-splash">
    <div id="main-splash">
        <div id="login-btn-div">
            <div id="login-img-div">
                <mn:Image ID="imgLogo" runat="server" TitleResourceConstant="TTL_LOGIN" ResourceConstant="ALT_LOGIN"
                    FileName="logn-btn.gif" />
            </div>
        </div>
        <div id="div-login-form">
            <div id="email">
                <span>אימייל:</span>
                <input type="text" name="email_login" id="email_login" onkeypress="return keypress(this, event);" /></div>
            <div id="password">
                <span>סיסמא:</span>
                <input type="password" name="password_login" id="password_login" onkeypress="return keypress(this, event);" /></div>
            <div id="btn-send">
                <a href="javascript:login();">
                <mn:Image ID="Image2" runat="server" TitleResourceConstant="TTL_BTN_SEND" ResourceConstant="ALT_BTN_SEND"
                    FileName="btn_send2.gif" />
                    </a>
            </div>
            <input type="checkbox" name="remember" id="remember" />
            זכור אותי | <a href="http://www.cupid.co.il/applications/memberservices/forgotpassword.aspx"
                style="text-decoration: underline; color: #000000;">שכחתי סיסמא</a>
        </div>
        <div id="registration-box">
            <div id="splash-logo">
            </div>
            <div id="register-form">
                
                <div id="one-page-reg-form">
                <!--<div id="join-now">
                    הצטרפו חינם</div>-->
                    <div id="general-error">
                        <mn:Txt ID="Txt13" runat="server" ResourceConstant="GENERAL_ERROR_TXT" />
                    </div>
                    <asp:PlaceHolder ID="phContent" runat="server">
                        <uc1:ValidationMessage runat="server" ID="txtValidation" Visible="false" />
                    </asp:PlaceHolder>
                    <mn:Image ID="Image3" FileName="join-free-btn.png" CssClass="submit-btn" runat="server" />
                </div>
                <div id="ajax-loader">
                    <div>
                        <mn:Image ID="Image9" runat="server" FileName="ajax-loader.gif" />
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="search-carousel">
        <div id="online-div">
            <!--
            <div id="carousel-title">
                מי באתר עכשיו?
            </div>
            <div id="search-title">
                חפשו מישהו לאהוב
            </div>
            <ul id="mycarousel" class="jcarousel-skin-tango">
            </ul>
            -->
            <div id="search-title">
                חפשו מישהו לאהוב</div>
            <div id="searchDiv">
                <div id="gender-div">
                    <span>אני:</span>
                    <select class="searchSelect" name="GenderID">
                        <option value="1">גבר</option>
                        <option value="2">אישה</option>
                    </select>
                </div>
                <div id="seek-div">
                    <span>רוצה להכיר:</span>
                    <select class="searchSelect" name="SeekingGenderID">
                        <option value="8">אישה </option>
                        <option value="4">גבר </option>
                    </select>
                </div>
                <div id="age-div">
                    <span>בגיל:</span>
                    <select class="searchSelect" name="AgeRangeID">
                        <option value="0">18-25 </option>
                        <option selected="" value="1">25-34 </option>
                        <option value="2">35-44</option>
                        <option value="3">45-55</option>
                        <option value="4">55+</option>
                    </select>
                </div>
                <div id="area-div">
                    <span>מאזור:</span>
                    <select class="searchSelect" name="AreaCode1">
                        <option value="02">ירושלים</option>
                        <option selected="selected" value="03">מרכז</option>
                        <option value="04">צפון</option>
                        <option value="08">דרום</option>
                        <option value="09">שרון</option>
                    </select>
                </div>
                <div id="find-btn-div">
                    <a href="javascript:searchLink();">
                        <mn:Image ID="Image6" runat="server" TitleResourceConstant="TTL_FIND_BTN" ResourceConstant="ALT_FIND_BTN"
                            FileName="new-find-btn.gif" />
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div id="splash-content">
        <div id="splash-content-header">
            <h2 id="content-title-1" class="four-column hover">
                <mn:Txt ID="Txt2" runat="server" ResourceConstant="TXT_CONTENT_TITLE_1" />
            </h2>
            <h2 id="content-title-2" class="four-column unhover">
                <mn:Txt ID="Txt1" runat="server" ResourceConstant="TXT_CONTENT_TITLE_2" />
            </h2>
            <h2 id="content-title-3" class="four-column unhover">
                <mn:Txt ID="Txt3" runat="server" ResourceConstant="TXT_CONTENT_TITLE_3" />
            </h2>
            <h2 id="content-title-4" class="four-column unhover">
                <mn:Txt ID="Txt4" runat="server" ResourceConstant="TXT_CONTENT_TITLE_4" />
            </h2>
        </div>
        <div id="splash-content-container">
            <div id="content-1" class="content-wrapper" style="position: absolute; width: 767px;
                z-index: 1;">
                <div class="content-title">
                    <mn:Txt ID="Txt9" runat="server" ResourceConstant="TXT_CONTENT_MAIN_1" />
                </div>
                <mn:Txt ID="Txt5" runat="server" ResourceConstant="TXT_CONTENT_1" />
                <div class="content-title-2">
                    <mn:Txt ID="Txt14" runat="server" ResourceConstant="TXT_CONTENT_MAIN_1_2" />
                </div>
                <mn:Txt ID="Txt15" runat="server" ResourceConstant="TXT_CONTENT_1_1" />
                <mn:Image ID="Image1" runat="server" FileName="splash-banner-1.png" ResourceConstant="TXT_CONTENT_TITLE_1" />
            </div>
            <div id="content-2" class="content-wrapper" style="position: absolute; width: 767px;
                z-index: -999;">
                <div class="content-title">
                    <mn:Txt ID="Txt10" runat="server" ResourceConstant="TXT_CONTENT_MAIN_2" />
                </div>
                <mn:Txt ID="Txt6" runat="server" ResourceConstant="TXT_CONTENT_2" />
                <div class="content-title-2">
                    <mn:Txt ID="Txt16" runat="server" ResourceConstant="TXT_CONTENT_MAIN_2_2" />
                </div>
                <mn:Txt ID="Txt17" runat="server" ResourceConstant="TXT_CONTENT_2_2" />
                <mn:Image ID="Image7" runat="server" FileName="splash-banner-2.png" ResourceConstant="TXT_CONTENT_TITLE_2" />
            </div>
            <div id="content-3" class="content-wrapper" style="position: absolute; width: 767px;
                z-index: -999;">
                <div>
                    <mn:Image ID="Image4" runat="server" TitleResourceConstant="TTL_SUCCESS_1" ResourceConstant="ALT_SUCCESS_1"
                        FileName="megali.jpg" />
                    <div class="content-title">
                        <mn:Txt ID="Txt7" runat="server" ResourceConstant="TXT_CONTENT_MAIN_3" />
                    </div>
                    <mn:Txt ID="Txt11" runat="server" ResourceConstant="TXT_CONTENT_3" />
                    <div>
                        <mn:Txt ID="Txt18" runat="server" ResourceConstant="TXT_CONTENT_LINK_3" />
                    </div>
                </div>
                <div id="splash-success-div-2">
                    <mn:Image ID="Image5" runat="server" TitleResourceConstant="TTL_SUCCESS_2" ResourceConstant="ALT_SUCCESS_2"
                        FileName="guy.jpg" />
                    <div class="content-title">
                        <mn:Txt ID="Txt19" runat="server" ResourceConstant="TXT_CONTENT_MAIN_3_2" />
                    </div>
                    <mn:Txt ID="Txt20" runat="server" ResourceConstant="TXT_CONTENT_3_2" />
                    <div>
                        <mn:Txt ID="Txt21" runat="server" ResourceConstant="TXT_CONTENT_LINK_3_2" />
                    </div>
                </div>
            </div>
            <div id="content-4" class="content-wrapper" style="position: absolute; width: 767px;
                z-index: -999;">
                <div class="content-title">
                    <mn:Txt ID="Txt12" runat="server" ResourceConstant="TXT_CONTENT_MAIN_4" />
                </div>
                <mn:Txt ID="Txt8" runat="server" ResourceConstant="TXT_CONTENT_4" />
                <div class="content-title-2">
                    <mn:Txt ID="Txt22" runat="server" ResourceConstant="TXT_CONTENT_MAIN_4_2" />
                </div>
                <mn:Txt ID="Txt23" runat="server" ResourceConstant="TXT_CONTENT_4_2" />
                <mn:Image ID="Image8" runat="server" FileName="splash-banner-3.png" ResourceConstant="TXT_CONTENT_TITLE_4" />
            </div>
        </div>
    </div>
</div>
<div id="footer">
    <div id="footer-container">
        <mn1:Footer20 ID="Footer20" Runat="server" />
    </div>
</div>
