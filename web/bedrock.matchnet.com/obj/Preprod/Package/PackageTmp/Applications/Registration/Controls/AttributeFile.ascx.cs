﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using Matchnet.Web.Framework;
using System.Xml.Serialization;
using Matchnet.Web.Framework.Ui.FormElements;
using Matchnet.Web.Framework.TemplateControls;
using Matchnet.Member.ValueObjects;
using Matchnet.Member.ValueObjects.Photos;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Session.ValueObjects;
using Matchnet.Web.Interfaces;

namespace Matchnet.Web.Applications.Registration.Controls
{
    public partial class AttributeFile : FrameworkControl, IAttributeControl
    {

        AttributeControl _attrControl;
        String _selectedValue;
        PhotoUpdate _photoUpdate;
        FrameworkControl _resourceControl;

        public string ContainerDivClientID { get { return divControl.ClientID; } }

        public bool IsOverlayReg { get; set; }

        public FrameworkControl ResourceControl
        {
            get { return _resourceControl; }
            set { _resourceControl = value; }
        }
        protected override void OnInit(EventArgs e)
        {
            if (IsOverlayReg && fileupload != null)
            {
                fileupload.CssClass += (_attrControl.RequiredFlag) ? " mandatory" : " optional";
            }
        }

        public void Save()
        {

        }
        public void SetValue(string value)
        {
            //_selectedValue = value;
            // _photoUpdate=(PhotoUpdate) g.Session.Get("REG_PHOTO");
        }
        public string GetValue()
        {
            if (IsOverlayReg)
            {
                if (Request.Files.Count > 0)
                {
                    _selectedValue = Request.Files[0].FileName;
                }
            }
            else
            {
                if (fileupload.PostedFile != null)
                    _selectedValue = fileupload.PostedFile.FileName;
            }

            return _selectedValue;
            //_selectedValue.ToString();

        }
        public string ScriptBlock()
        {
            return "";
        }
        public void PersistToCookie(HttpCookie cookie)
        {
            //   cookie[_attrControl.AttributeName] = GetValue();

        }

        public void Persist(ITemporaryPersistence persistence)
        {
           
        }

        public bool Validate()
        {

            bool valid = IsValidImage();
            if (valid)
                valid = uploadMemberPhoto();

            return valid;
        }


        public string FocusControlClientID
        {
            get
            {
                try
                {
                    return fileupload.ClientID;
                }
                catch (Exception ex)
                {
                    return "";
                }

            }
        }

        public AttributeControl AttrControl
        { get { return _attrControl; } set { _attrControl = value; } }

        protected void Page_Load(object sender, EventArgs e)
        {

            txtCaption.Text = _attrControl.GetResourceString(_attrControl.ResourceConstant, g, _resourceControl);

            divControl.Attributes["class"] = _attrControl.Name;
        }


        public bool IsValidImage()
        {
            bool valid = true;

            string phototypes = @"(.*\.[j/J][p/P][e/E][g/G]$)|(.*\.[j/J][p/P][g/G]$)|(.*\.[g/G][i/I][f/F]$)|(.*\.[b/B][m/M][p/P]$)";

            string filename;

            if (IsOverlayReg)
            {
                filename = GetValue();
            }
            else
            {
                filename = fileupload.FileName;
            }

            if (String.IsNullOrEmpty(filename))
                return !_attrControl.RequiredFlag;

            System.Text.RegularExpressions.Regex regValidation = new System.Text.RegularExpressions.Regex(phototypes);
            if (!regValidation.IsMatch(filename))
            {
                txtValidation.Text = _attrControl.GetValidationMessage("ERR_PHOTO_FORMAT_INVALID", g, _resourceControl);
                txtValidation.Visible = true;
                return false;
            }

            HttpPostedFile file = fileupload.PostedFile;

            if (IsOverlayReg)
            {
                file = Request.Files[0];
            }

            // Size check
            if (file == null)
                return !_attrControl.RequiredFlag;
            if (file.ContentLength > _attrControl.MaxLen)
            {
                txtValidation.Text = _attrControl.GetValidationMessage("ERR_MAXIMUM_FILE_SIZE_EXCEEDED", g, _resourceControl);
                valid = false;
                txtValidation.Visible = true;
                return valid;
            }

            // File format check
            try
            {
                System.Drawing.Image image = System.Drawing.Image.FromStream(file.InputStream);
            }
            catch (ArgumentException)
            {

                txtValidation.Text = _attrControl.GetValidationMessage("ERR_UNABLE_TO_UPLOAD_A_PHOTO", g, _resourceControl);
                valid = false;
                txtValidation.Visible = true;
                return valid;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.WriteLine((string)ex.ToString());

                txtValidation.Text = _attrControl.GetValidationMessage("INVALID_PHOTO_TYPE", g, _resourceControl);
                valid = false;
                return valid;
            }

            return valid;
        }

        public void SetVisible(bool visible)
        {
            if (!visible)
                divControl.Style.Add("display", "none");
            else
                divControl.Style.Add("display", "inline");

        }
        public ValidationMessage ValidationMessage { get { return txtValidation; } }

        private bool uploadMemberPhoto()
        {
            try
            {
                HttpPostedFile newFile = null;

                if (fileupload != null && fileupload.PostedFile != null)
                {
                    newFile = fileupload.PostedFile;
                }
                else if ((g.IsRemoteRegistration || IsOverlayReg) && Request.Files.Count > 0)
                {
                    newFile = Request.Files[0];
                }

                // don't attempt save if no uploaded file and no current file 
                if ((newFile != null) && (newFile.ContentLength > 0))
                {
                    try
                    {
                        //don't attempt if file is too big
                        if (newFile.ContentLength > _attrControl.MaxLen)
                        {
                            txtValidation.Text = _attrControl.GetValidationMessage("MAXIMUM_FILE_SIZE_EXCEEDED", g, _resourceControl);
                            txtValidation.Visible = true;
                            return false;
                        }

                        Stream fileStream = newFile.InputStream;
                        fileStream.Position = 0;
                        byte[] newFileBytes = new byte[newFile.ContentLength];
                        fileStream.Read(newFileBytes, 0, newFile.ContentLength);
                        newFile.InputStream.Flush();


                        g.Session.Add("REG_PHOTO", newFileBytes, SessionPropertyLifetime.Temporary);

                    }
                    catch
                    {
                        //Member Registration will succeed, adding notification message.
                        txtValidation.Text = _attrControl.GetValidationMessage("MAXIMUM_FILE_SIZE_EXCEEDED", g, _resourceControl);
                        txtValidation.Visible = true;
                        return false;
                    }

                }
                return true;
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
                return false;
            }
        }

        protected void btnUpload_OnClick(object sender, EventArgs e)
        {
            bool valid = Validate();

        }

        public NameValueCollection GetSearchPreferenceParamaterNVC()
        {
            return new NameValueCollection();
        }
    }
}