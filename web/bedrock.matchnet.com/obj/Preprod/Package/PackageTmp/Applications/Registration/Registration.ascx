﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Registration.ascx.cs"
    Inherits="Matchnet.Web.Applications.Registration.Registration" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="uc2" TagName="JQValidator" Src="/Framework/Validation/JQValidator.ascx" %>
<%@ Register Assembly="Anthem" Namespace="Anthem" TagPrefix="anthem" %>
<asp:PlaceHolder ID="PlaceHolderOmnitureMboxSplashBeginDiv" runat="server" Visible="false">
    <div class="mboxDefault">
</asp:PlaceHolder>
<asp:Literal ID="litScript" runat="server" />
<asp:Literal ID="litOmniture" runat="server" />
<div id="divPanel">
    <anthem:Panel ID="anthemPnl1" runat="server" AutoUpdateAfterCallBack="true" EnabledDuringCallBack="false">
        <asp:Literal ID="litRegErrorMessage" runat="server" />
        <div class="error">
            <mn:Txt ID="regValidation" runat="server" />
        </div>
        <asp:PlaceHolder ID="phContent" runat="server" />
    </anthem:Panel>
    <asp:Panel runat="server" ID="pnlNonAjax" Visible="false">
        <asp:Literal ID="litRegErrorMessageNonAjax" runat="server" />
        <div class="error">
            <mn:Txt ID="regValidationNonAjax" runat="server" />
        </div>
        <asp:PlaceHolder ID="phContentNonAjax" runat="server" />
    </asp:Panel>
</div>
<asp:PlaceHolder ID="PlaceHolderOmnitureMboxSplash" runat="server" Visible="false"></div>
</asp:PlaceHolder>

<asp:PlaceHolder ID="PlaceHolderEntirePageMboxCreate" runat="server" Visible="false">
    <script type="text/javascript">
        mboxCreate('<asp:Literal id=litmboxPageName runat=server/>'); 
    </script>
</asp:PlaceHolder>
    
<asp:PlaceHolder ID="PlaceHolderOmnitureMboxRegBegin" runat="server" Visible="false">

    <script type="text/javascript">
        //set TNT variable when page loads (needs to be after mboxes have loaded) 
        var tntInput = s.trackTNT();

        //call this function after everything else has loaded 
        trackTNT(s_account, tntInput);

        /* For tracking TNT data in SC */
        function trackTNT(s_account, tntInput) {
            var s = s_gi(s_account);

            s.linkTrackVars = "tnt,<asp:Literal id=litmboxEVar runat=server/>";
            s.linkTrackEvents = "None";

            //variable for TNT classifications 
            s.eVar16 = s.tnt = tntInput;

           s.tl(this, 'o', 'For tracking TNT');
        } 
    </script>

</asp:PlaceHolder>

<!-- Script to capture black box values -->
<asp:PlaceHolder ID="IovationBlackbox" runat="server">
<input type="hidden" name="ioBlackBox" id="ioBlackBox" runat="server" />
 <script type="text/javascript" language="JavaScript">
     if (typeof jQuery != 'undefined') {
         var script = document.createElement('script');
         script.type = "text/javascript";
         script.src = "/javascript20/registrationOverlay/jquery.cookie.js";
         document.getElementsByTagName('head')[0].appendChild(script);
     }
</script>

<!-- Blackbox javascript -->
<script type="text/javascript" language="JavaScript" src="https://mpsnare.iesnare.com/snare.js">
</script>

 <script type="text/javascript" language="JavaScript">
     var blackboxfieldId = '<% = ioBlackBox.ClientID %>';
     var io_install_flash = false; // do not install Flash
     var io_enable_rip = true; // collect Real IP information
     GetIOBB(0);
     
     function GetIOBB(pass) {
         if (typeof ioGetBlackbox == 'function') {
             var bb_data = ioGetBlackbox();
             var blackbox = bb_data.blackbox;
             if(bb_data.finished || pass > 10) {
                 document.getElementById(blackboxfieldId).value = blackbox;
                 if (typeof jQuery != 'undefined') {
                     jQuery.cookie('regiobb', blackbox, { expires: 1 });
                 }
                 return false;
             }
             setTimeout("GetIOBB(" + (pass + 1) + ")", 100);
         }
         return false;
     }

</script>

</asp:PlaceHolder>
