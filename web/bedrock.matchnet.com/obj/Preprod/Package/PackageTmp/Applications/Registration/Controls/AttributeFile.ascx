﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AttributeFile.ascx.cs"
    Inherits="Matchnet.Web.Applications.Registration.Controls.AttributeFile" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="uc2" TagName="ValidationMessage" Src="/Applications/Registration/Controls/ValidationMessage.ascx" %>
<%@ Register TagPrefix="anthem" Namespace="Anthem" Assembly="Anthem" %>
<div id="divControl" runat="server">
    <anthem:Panel ID="pnlFile" runat="server" AutoUpdateAfterCallBack="true">
        <h2>
            <mn:Txt runat="server" ID="txtCaption" />
        </h2>
        <uc2:ValidationMessage runat="server" ID="txtValidation" Visible="false" />
        <anthem:FileUpload ID="fileupload" runat="server" AutoUpdateAfterCallBack="true"
            CssClass="photo" />
        <%--<asp:FileUpload  ID="fileupload" runat="server" />--%>
    </anthem:Panel>
</div>
