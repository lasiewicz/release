﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Web;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Ui.BasicElements;
using Matchnet.Web.Applications.Registration.Controls;

namespace Matchnet.Web.Applications.Registration.Templates
{
    public partial class SplashTemplate11 : RegistrationTemplateBase, IRegistrationTemplate
    {
        private const string IMG_LOGO_FILE_NAME = "logo-header-img.png";
        /*
        public override Literal LitmboxEVar
        {
            get
            {
                return litmboxEVar;
            }
        }*/

        protected override void OnInit(EventArgs e)
        {


            #region Moved from WideSimple.ascx.cs

            imgLogo.NavigateUrl = FrameworkGlobals.GetHomepageAbsURL(g, Request.ServerVariables.Get("HTTP_HOST"));

            #endregion
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            //add mbox create/close
            if (Wizard.RegistrationStep.MBox)
            {
                plcOpenMbox.Visible = true;
                plcCloseMbox.Visible = true;
            }

            #region Moved from WideSimple.ascx.cs
            string imagePath = Matchnet.Web.Framework.Image.GetURLFromFilename(IMG_LOGO_FILE_NAME);
            if (g.BrandAlias != null)
            {
                imagePath = Matchnet.Web.Framework.Image.GetBrandAliasImagePath(IMG_LOGO_FILE_NAME, g.BrandAlias);
            }

            string host = HttpContext.Current.Request.Url.Host;
            string url = "http://" + host + "/default.aspx";
            imgLogo.NavigateUrl = url;
            imgLogo.ImageUrl = imagePath;
            #endregion
        }

    }
}

