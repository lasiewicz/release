﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.TemplateControls;
using Matchnet.Web.Interfaces;

namespace Matchnet.Web.Applications.Registration.Controls
{
    public partial class AttributeHidden : FrameworkControl, IAttributeControl
    {
        AttributeControl _attrControl;
        
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        #region IAttributeControl Members

        public void Save()
        {
           
        }

        public void SetValue(string value)
        {
            hdnAttribute.Value = value;
        }

        public string GetValue()
        {
            if (IsOverlayReg)
            {
                return Request[hdnAttribute.UniqueID];
            }
            else
            {
                return hdnAttribute.Value;
            }
        }

        public void PersistToCookie(HttpCookie cookie)
        {
            cookie[_attrControl.AttributeName] = GetValue();
        }

        public void Persist(ITemporaryPersistence persistence)
        {
            persistence[_attrControl.AttributeName] = GetValue();
        }

        public bool Validate()
        {
            //value is stored in hidden field and not editable, so always return valid.
            return true;
            
        }

        public string ScriptBlock()
        {
            return string.Empty;
        }

        public FrameworkControl ResourceControl
        {
            get { return this; }
            set
            {
                
            }
        }

        public void SetVisible(bool visible)
        {
            //this is a hidden field, so this method doesn't really do anything
        }

        public ValidationMessage ValidationMessage
        {
            get { return null; }
        }

        public string FocusControlClientID
        {
            get { return string.Empty; }
        }

        public string ContainerDivClientID
        {
            get { return string.Empty; }
        }

        public bool IsOverlayReg { get; set; }

        public NameValueCollection GetSearchPreferenceParamaterNVC()
        {
            return new NameValueCollection();
        }

        public AttributeControl AttrControl
        {
            get { return _attrControl; } set { _attrControl = value; } 
        }

        #endregion
    }
}