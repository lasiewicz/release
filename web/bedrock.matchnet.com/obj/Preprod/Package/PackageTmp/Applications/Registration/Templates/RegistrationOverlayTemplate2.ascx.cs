﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Matchnet.Web.Applications.Registration.Templates
{
    public partial class RegistrationOverlayTemplate2 : RegistrationTemplateBase, IRegistrationTemplate
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Wizard.RegistrationScenario.FirstStepContinueButtonResourceConstant))
            {
                string script = "var nextButtonText='" + g.GetResource("TXT_CONTINUE", this) + "';\n";
                script += "var firstNextButtonText='" + g.GetResource(Wizard.RegistrationScenario.FirstStepContinueButtonResourceConstant, this) + "';\n";

                Page.ClientScript.RegisterStartupScript(this.GetType(), "NextButtonText", script, true);
            }
        }

        #region IRegistrationTemplate Members

        PlaceHolder IRegistrationTemplate.ContentPlaceholder
        {
            get { return phContent; }
        }

        #endregion
    }
}