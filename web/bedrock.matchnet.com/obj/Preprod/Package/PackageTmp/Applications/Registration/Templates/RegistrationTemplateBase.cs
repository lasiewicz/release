﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Matchnet.Web;
using Matchnet.Web.Framework;
using System.Xml.Serialization;
using Matchnet.Web.Framework.Ui.FormElements;
using Matchnet.Web.Framework.TemplateControls;
using Matchnet.Web.Framework.Ui.BasicElements;
using System.Net;
using Matchnet.ExternalMail.ServiceAdapters;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects;
using Matchnet.Member.ValueObjects.Photos;
using Matchnet.Web.Framework.Globalization;
using Matchnet.Web.Applications.Registration.Templates;
using Matchnet.Session.ValueObjects;
using Matchnet.Session.ServiceAdapters;
using Matchnet.Web.Applications.MemberProfile;
using Matchnet.Web.Applications.Registration.Controls;
using Matchnet.Configuration.ServiceAdapters;

namespace Matchnet.Web.Applications.Registration.Templates
{
    public class RegistrationTemplateBase : FrameworkControl
    {
        #region declarations
        protected const string structCSSSetting = "CSS_STRUCT";
        protected const string styleCSSSetting = "CSS_STYLE";
        protected const string CSS_PATH = "/css/";
        protected const string HREF = "href";

        string _titleResource;
        string _tipResource;
        string _cssClass;
        bool _resourceGlobal;
        FrameworkControl _resourceControl;
        RegistrationWizard _wizard;
        List<AttributeControl> _controls;
        PlaceHolder _content;

        ProgressBar _progress;

        public RegistrationWizard Wizard
        {
            get { return _wizard; }
            set { _wizard = value; }
        }

        public virtual PlaceHolder ContentPlaceholder
        {
            get
            {
                return _content;
            }
            set
            {

                _content = value;
            }
        }

        #endregion

        #region virtual properties to br overridev in actual template
        public virtual System.Web.UI.WebControls.Button ButtonContinue
        { get { return null; } }
        public virtual System.Web.UI.WebControls.Button ButtonNext
        { get { return null; } }
        public virtual System.Web.UI.WebControls.Button ButtonPrevious
        { get { return null; } }
        public virtual System.Web.UI.WebControls.Button ButtonFBRegister
        { get { return null; } }
        public virtual FacebookRegistration FBRegistration
        { get { return null; }}
        public virtual Literal LitmboxEVar
        { get { return null; } }

        public virtual ProgressBar Progress
        {
            get { return _progress; }
            set { _progress = value; }
        }

        public virtual ValidationMessage ValidationMessageText
        { get { return null; } }
        public virtual Txt Tip
        { get { return null; } }

        public virtual Literal StepCSSClassLiteral
        { get { return null; } }

        public virtual Literal StepCSSSecondaryClassLiteral
        { get { return null; } }

        public virtual Literal StepMainContainerCSSClassLiteral
        { get { return null; } }

        public virtual Txt AdditionalHtmlTxt
        { get { return null; } }
        #endregion


        #region properties


        public string TitleResource { get { return _titleResource; } set { _titleResource = value; } }
        public string TipResource { get { return _tipResource; } set { _tipResource = value; } }
        public bool ResourceGlobal { get { return _resourceGlobal; } set { _resourceGlobal = value; } }
        public FrameworkControl ResourceControl { get { return _resourceControl; } set { _resourceControl = value; } }
        public List<AttributeControl> RegistrationControls
        {
            get { return _controls; }
            set { _controls = value; }
        }
        public string StepCSSClass { get { return _cssClass; } set { _cssClass = value; } }

        public string SplashHeaderMBoxName { get { return g.Brand.Site.Name.ToLower().Replace(".", string.Empty) + "_splash_header"; } }
        public string SplashContentMBoxName { get { return g.Brand.Site.Name.ToLower().Replace(".", string.Empty) + "_splash_content"; } }
        public string SplashFooterMBoxName { get { return g.Brand.Site.Name.ToLower().Replace(".", string.Empty) + "_splash_footer"; } }
        public string SplashFullContentMBoxName { get { return g.Brand.Site.Name.ToLower().Replace(".", string.Empty) + "_splash_full_content"; } }
        public string OnePageRegContentDivCSSClass { get; set; }
        public string SubmitButtonImageFile { get; set; }
        #endregion


        #region methods

        public virtual void ConfigureStep(Step registrationStep)
        {
            
        }


        public IAttributeControl Add(AttributeControl control, bool show)
        {
            return Add(control, show, false);
        }

        public IAttributeControl Add(AttributeControl control, bool show, bool isOverlayReg)
        {
            IAttributeControl attrControl = null;
            switch (control.Type)
            {
                case ControlType.mask:
                    attrControl = AddMaskControl(control, show);
                    break;
                case ControlType.ddlist:
                    attrControl = AddDDLControl(control, show);
                    break;
                case ControlType.list:
                    attrControl = AddListControl(control, show);
                    break;
                case ControlType.region:
                    attrControl = AddRegionControl(control, show, isOverlayReg);
                    break;
                case ControlType.text:
                case ControlType.textarea:
                case ControlType.password:
                    attrControl = AddTextControl(control, show);
                    break;
                case ControlType.dateofbirth:
                    attrControl = AddDOBControl(control, show);
                    break;
                case ControlType.file:
                    attrControl = AddUploadControl(control, show);
                    break;
                case ControlType.checkbox:
                    attrControl = AddCheckboxControl(control, show);
                    break;
                case ControlType.captcha:
                    attrControl = AddCaptchaControl(control, show);
                    break;

                case ControlType.colorcode:
                    attrControl = AddQuizPart1Control(control, show);
                    break;
                case ControlType.textboxconfirm:
                    attrControl = AddTextConfirmControl(control, show);
                    break;


                case ControlType.listbox:
                    attrControl = AddListBoxControl(control, show);
                    break;

                case ControlType.regionajax:
                    attrControl = AddRegionControl(control, show, true);
                    break;
                case ControlType.hidden:
                    attrControl = AddHiddenControl(control);
                    break;
                case ControlType.info:
                    attrControl = AddInfoControl(control, show);
                    break;
                    

                    ((IAttributeControl)attrControl).SetVisible(show);
            }

            attrControl.IsOverlayReg = isOverlayReg;

            return attrControl;


        }

        protected virtual IAttributeControl AddListControl(AttributeControl control, bool show)
        {

            AttributeListItemControl maskControl = (AttributeListItemControl)LoadControl("/Applications/Registration/Controls/AttributeListItemControl.ascx");
            maskControl.AttrControl = control;
            maskControl.ID = "id" + maskControl.AttrControl.Name;
            if (control.AttributeIntValue > 0)
                maskControl.SetValue(control.AttributeIntValue.ToString());
            else if (!String.IsNullOrEmpty(control.SelectedValue))
            {
                maskControl.SetValue(control.SelectedValue);
            }
            //   maskControl.Visible=show;
            if (!control.ResourceGlobal)
                maskControl.ResourceControl = _resourceControl;

            this.ContentPlaceholder.Controls.Add(maskControl);

            return (IAttributeControl)maskControl;
        }

        protected virtual IAttributeControl AddMaskControl(AttributeControl control, bool show)
        {

            AttributeMask maskControl = (AttributeMask)LoadControl("/Applications/Registration/Controls/AttributeMask.ascx");
            maskControl.AttrControl = control;
            maskControl.ID = "id" + maskControl.AttrControl.Name;
            if (control.AttributeIntValue > 0)
                maskControl.SetValue(control.AttributeIntValue.ToString());
            //      maskControl.Visible=show;
            if (!control.ResourceGlobal)
                maskControl.ResourceControl = _resourceControl;
            this.ContentPlaceholder.Controls.Add(maskControl);

            return (IAttributeControl)maskControl;
        }

        protected virtual IAttributeControl AddListBoxControl(AttributeControl control, bool show)
        {
            AttributeListBox listboxControl = (AttributeListBox)LoadControl("/Applications/Registration/Controls/AttributeListBox.ascx");
            listboxControl.AttrControl = control;
            listboxControl.ID = "id" + listboxControl.AttrControl.Name;
            if (control.AttributeIntValue > 0)
                listboxControl.SetValue(control.AttributeIntValue.ToString());

            if (!control.ResourceGlobal)
                listboxControl.ResourceControl = _resourceControl;
            this.ContentPlaceholder.Controls.Add(listboxControl);
            return (IAttributeControl)listboxControl;

        }

        protected virtual IAttributeControl AddHiddenControl(AttributeControl control)
        {
            AttributeHidden hiddenControl = (AttributeHidden)LoadControl("/Applications/Registration/Controls/AttributeHidden.ascx");
            hiddenControl.AttrControl = control;
            hiddenControl.ID = "id" + hiddenControl.AttrControl.Name;
            hiddenControl.SetValue(control.AttributeTextValue);
            this.ContentPlaceholder.Controls.Add(hiddenControl);
            return (IAttributeControl)hiddenControl;
        }


        protected virtual IAttributeControl AddDDLControl(AttributeControl control, bool show)
        {

            AttributeDropDownList ddlcontrol = (AttributeDropDownList)LoadControl("/Applications/Registration/Controls/AttributeDropDownList.ascx");
            ddlcontrol.AttrControl = control;
            ddlcontrol.ID = "id" + ddlcontrol.AttrControl.Name;
            if (control.AttributeIntValue > 0)
                ddlcontrol.SetValue(control.AttributeIntValue.ToString());
            //  ddlcontrol.Visible = show;
            if (!control.ResourceGlobal)
                ddlcontrol.ResourceControl = _resourceControl;
            this.ContentPlaceholder.Controls.Add(ddlcontrol);
            return (IAttributeControl)ddlcontrol;

        }


        protected virtual IAttributeControl AddRegionControl(AttributeControl control, bool show, bool isOverlayReg)
        {
            //AttributeRegionControl regioncontrol;
            IAttributeControl result;

            if (isOverlayReg)
            {
                AttributeRegionAjaxControl regionajaxcontrol = (AttributeRegionAjaxControl)LoadControl("/Applications/Registration/Controls/AttributeRegionAjaxControl.ascx");

                regionajaxcontrol.AttrControl = control;
                regionajaxcontrol.ID = "id" + regionajaxcontrol.AttrControl.Name;
                if (control.AttributeIntValue > 0)
                {
                    regionajaxcontrol.SetValue(control.AttributeIntValue.ToString());
                }
                else
                {
                    if (!string.IsNullOrEmpty(control.SelectedValue))
                    {
                        regionajaxcontrol.SetValue(control.SelectedValue);
                    }
                }

                //     ddlcontrol.Visible = show;
                if (!control.ResourceGlobal)
                    regionajaxcontrol.ResourceControl = _resourceControl;
                this.ContentPlaceholder.Controls.Add(regionajaxcontrol);

                result = regionajaxcontrol;
            }
            else
            {
                AttributeRegionControl regioncontrol = (AttributeRegionControl)LoadControl("/Applications/Registration/Controls/AttributeRegionControl.ascx");

                regioncontrol.AttrControl = control;
                regioncontrol.ID = "id" + regioncontrol.AttrControl.Name;
                if (control.AttributeIntValue > 0)
                {
                    regioncontrol.SetValue(control.AttributeIntValue.ToString());
                }
                else
                {
                    if (!string.IsNullOrEmpty(control.SelectedValue))
                    {
                        regioncontrol.SetValue(control.SelectedValue);
                    }
                }

                //     ddlcontrol.Visible = show;
                if (!control.ResourceGlobal)
                    regioncontrol.ResourceControl = _resourceControl;
                this.ContentPlaceholder.Controls.Add(regioncontrol);

                result = regioncontrol;
            }

            return (IAttributeControl)result;
        }


        protected virtual IAttributeControl AddTextControl(AttributeControl control, bool show)
        {

            AttributeText txtControl = (AttributeText)LoadControl("/Applications/Registration/Controls/AttributeText.ascx");
            txtControl.AttrControl = control;
            txtControl.ID = "id" + txtControl.AttrControl.Name;
            if (!String.IsNullOrEmpty(control.AttributeTextValue))
                txtControl.SetValue(control.AttributeTextValue.ToString());
            //   txtControl.Visible = show;
            if (!control.ResourceGlobal)
                txtControl.ResourceControl = _resourceControl;
            this.ContentPlaceholder.Controls.Add(txtControl);

            return (IAttributeControl)txtControl;
        }

        protected virtual IAttributeControl AddTextConfirmControl(AttributeControl control, bool show)
        {

            AttributeTextConfirm txtControl = (AttributeTextConfirm)LoadControl("/Applications/Registration/Controls/AttributeTextConfirm.ascx");
            txtControl.AttrControl = control;
            txtControl.ID = "id" + txtControl.AttrControl.Name;
            if (!String.IsNullOrEmpty(control.AttributeTextValue))
                txtControl.SetValue(control.AttributeTextValue.ToString());
            //   txtControl.Visible = show;
            if (!control.ResourceGlobal)
                txtControl.ResourceControl = _resourceControl;
            this.ContentPlaceholder.Controls.Add(txtControl);

            return (IAttributeControl)txtControl;
        }

        protected virtual IAttributeControl AddUploadControl(AttributeControl control, bool show)
        {

            AttributeFile txtControl = (AttributeFile)LoadControl("/Applications/Registration/Controls/AttributeFile.ascx");
            txtControl.AttrControl = control;
            txtControl.ID = "id" + txtControl.AttrControl.Name;
            txtControl.Attributes.Add("name", txtControl.AttrControl.Name);
            if (!String.IsNullOrEmpty(control.AttributeTextValue))
                txtControl.SetValue(control.AttributeTextValue.ToString());
            //   txtControl.Visible = show;
            if (!control.ResourceGlobal)
                txtControl.ResourceControl = _resourceControl;
            this.ContentPlaceholder.Controls.Add(txtControl);
            return (IAttributeControl)txtControl;

        }


        protected virtual IAttributeControl AddCheckboxControl(AttributeControl control, bool show)
        {
            AttributeCheckBox txtControl = null;
            try
            {
                txtControl = (AttributeCheckBox)LoadControl("/Applications/Registration/Controls/AttributeCheckBox.ascx");
                txtControl.AttrControl = control;
                txtControl.ID = "id" + txtControl.AttrControl.Name;

                //   txtControl.Visible = show;
                if (!control.ResourceGlobal)
                    txtControl.ResourceControl = _resourceControl;
                this.ContentPlaceholder.Controls.Add(txtControl);
                if (control.AttributeIntValue > 0)
                    txtControl.SetValue(control.AttributeIntValue.ToString());
                else
                    txtControl.SetValue(control.SelectedValue);

                return (IAttributeControl)txtControl;
            }
            catch (Exception ex)
            {
                return (IAttributeControl)txtControl;
            }

        }

        protected virtual IAttributeControl AddDOBControl(AttributeControl control, bool show)
        {

            AttributeDOBControl txtControl = (AttributeDOBControl)LoadControl("/Applications/Registration/Controls/AttributeDOBControl.ascx");
            txtControl.AttrControl = control;
            txtControl.ID = "id" + txtControl.AttrControl.Name;

            txtControl.SetValue(control.AttributeDateValue.ToString());
            //   txtControl.Visible = show;
            if (!control.ResourceGlobal)
                txtControl.ResourceControl = _resourceControl;
            this.ContentPlaceholder.Controls.Add(txtControl);

            return (IAttributeControl)txtControl;
        }

        protected virtual IAttributeControl AddInfoControl(AttributeControl control, bool show)
        {

            InfoControl txtControl = (InfoControl)LoadControl("/Applications/Registration/Controls/InfoControl.ascx");
            txtControl.AttrControl = control;
            txtControl.ID = "id" + txtControl.AttrControl.Name;

           // txtControl.SetValue(control.AttributeDateValue.ToString());
            //   txtControl.Visible = show;
            if (!control.ResourceGlobal)
                txtControl.ResourceControl = _resourceControl;
            this.ContentPlaceholder.Controls.Add(txtControl);

            return (IAttributeControl)txtControl;
        }

        public string GetResourceString(string key, FrameworkControl resourceControl)
        {
            string resString = "";
            if (ResourceGlobal || resourceControl == null)
                resString = g.GetResource(key);
            else
                resString = g.GetResource(key, resourceControl);
            return resString;
        }

        protected virtual IAttributeControl AddCaptchaControl(AttributeControl control, bool show)
        {

            AttributeCaptcha txtControl = (AttributeCaptcha)LoadControl("/Applications/Registration/Controls/AttributeCaptcha.ascx");
            txtControl.AttrControl = control;
            txtControl.ID = "id" + txtControl.AttrControl.Name;
            if (!String.IsNullOrEmpty(control.AttributeTextValue))
                txtControl.SetValue(control.AttributeTextValue.ToString());
            //   txtControl.Visible = show;
            if (!control.ResourceGlobal)
                txtControl.ResourceControl = _resourceControl;
            this.ContentPlaceholder.Controls.Add(txtControl);

            return (IAttributeControl)txtControl;
        }



        protected virtual IAttributeControl AddQuizPart1Control(AttributeControl control, bool show)
        {

            QuizControl1 txtControl = (QuizControl1)LoadControl("/Applications/Registration/Controls/QuizControl1.ascx");

            txtControl.AttrControl = control;
            txtControl.ID = "id" + txtControl.AttrControl.Name;

            if (!control.ResourceGlobal)
                txtControl.ResourceControl = _resourceControl;
            this.ContentPlaceholder.Controls.Add(txtControl);

            return (IAttributeControl)txtControl;
        }



        protected string AppendVersion(string path)
        {
            return path + "?v=" + FrameworkGlobals.GetLastWriteTimeString(path);
        }

        protected string GetConfigurableStyleSheetName(string cssSettingName)
        {
            return CSS_PATH + RuntimeSettings.GetSetting(cssSettingName, g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID) + ".css";
        }

        #endregion

        #region events
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            // g.HeaderControl.ShowBlankHeader(Matchnet.Web.Framework.Ui.HeaderImageType.Registration);



        }
        private void Page_PreRender(object sender, System.EventArgs e)
        {

            // ShowStepControls();
        }
        protected void ButtonContinue_OnClick(object sender, EventArgs e)
        {
            // templateControl.Controls


            //  SubmitStep();


        }
        protected void ButtonNext_OnClick(object sender, EventArgs e)
        {
            // templateControl.Controls





        }
        protected void ButtonPrevious_OnClick(object sender, EventArgs e)
        {
            // templateControl.Controls



        }

        #endregion


        internal void StartStep(string id, string cssClass, string stepName, string stepTitleResource, bool dontSkip)
        {
            Literal divStep = new Literal();
            string stepTitle = GetResourceString(stepTitleResource, ResourceControl);
            if (!string.IsNullOrEmpty(stepTitle))
            {
                stepTitle = string.Format("\n<div class=\"stepTitle\" style=\"display: none\">{0}</div>", stepTitle);
            }

            if (!string.IsNullOrEmpty(cssClass))
            {
                cssClass = string.Format("class=\"{0} {1}\"", cssClass, stepName);
            }

            string dontSkipString = string.Empty;
            if (dontSkip)
            {
                dontSkipString = "dontSkip=\"true\"";
            }
            divStep.Text = string.Format("<div id=\"{0}\" {1} {2}>{3}", id, cssClass, dontSkipString, stepTitle);
            ContentPlaceholder.Controls.Add(divStep);
        }

        internal void EndStep(string stepTipResource, int stepID, string pageName)
        {
            Literal divStep = new Literal();
            if (!String.IsNullOrEmpty(stepTipResource))
            {
                divStep.Text = string.Format("\n<div class=\"steptip\">\n\t{0}\n</div>\n",
                                             GetResourceString(stepTipResource, ResourceControl));
            }
            divStep.Text += string.Format("<script type=\"text/javascript\">\n\tvar step_{0}_pageName = \"{1}\"\n</script>\n", stepID, pageName);
            divStep.Text += "</div>";
            ContentPlaceholder.Controls.Add(divStep);
        }
    }
}
