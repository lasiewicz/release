﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SplashTemplate6.ascx.cs" Inherits="Matchnet.Web.Applications.Registration.Templates.SplashTemplate6" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>

<%@ Register TagPrefix="uc1" TagName="ValidationMessage" Src="/Applications/Registration/Controls/ValidationMessage.ascx" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="uc1" TagName="LogonControlSplash20" Src="/Applications/Logon/Controls/LogonControlSplash20.ascx" %>

<link rel="shortcut icon" href="../img/d/3/1/Shortcut.ico">

<!-- SIFR STUFF start -->
	<link rel="stylesheet" href="/css/sIFR-screen.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="/css/sIFR-print.css" type="text/css" media="print" />
	<script src="/javascript20/sifr.js" type="text/javascript"></script>
	<!--<script src="/javascript20/sifr-addons.js" type="text/javascript"></script>-->
	<!-- SIFR STUFF end -->
	
	<!-- JQUERY STUFF start --> 
	<link type="text/css" href="/css/ui-lightness/jquery-ui-1.7.2.custom.css" rel="stylesheet" />	
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js" ></script>
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.5.3/jquery-ui.min.js" ></script>
	<script type="text/javascript" src="/javascript20/flash_detect.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			$("#content_bodyBot > ul").tabs({fx:{opacity: "toggle"}}).tabs("rotate", 5000, true);
		});
	</script>
<!-- JQUERY STUFF end -->

<!-- GLOBAL CSS start -->
	<style type="text/css" media="all">
		@import "/css/splash.css";
	</style>
	<!--[if IE]>
	<style type="text/css" media="all">
		@import "/css/splash_ie.css";
	</style>
	<![endif]-->
	<!--[if IE 6]>
	<style type="text/css" media="all">
	    @import "/css/splash_ie6.css";
	    img, div, a, input {behavior: url('/javascript20/library/iepnefix.htc') }
	    fieldset div#_ctl0__ctl2__ctl0__ctl0_idUserName_divControl {float:left;width:275px;}
        fieldset div#_ctl0__ctl2__ctl0__ctl0_idPassword_divControl {float:left;width:275px;}
        fieldset div#_ctl0__ctl2__ctl0__ctl0_idGender_divControl {float:left;width:120px;}
        fieldset div#_ctl0__ctl2__ctl0__ctl0_idSeekingGender_divControl {float:left;width:135px;}
        fieldset div#_ctl0__ctl2__ctl0__ctl0_idTermsAndConditions_divControl {float:left;width:255px;margin-top:10px;}
        fieldset div#_ctl0__ctl2__ctl0__ctl0_idTermsAndConditions_divControl input#_ctl0__ctl2__ctl0__ctl0_btnContinue {float:left;padding-bottom:15px;margin-right:4px;}
        				
        fieldset div.cta-wrapper {float:left;width:275px;}
        				
        fieldset div#_ctl0__ctl2__ctl0__ctl0_idUserName_divControl label {display:block;}
        fieldset div#_ctl0__ctl2__ctl0__ctl0_idPassword_divControl label {display:block;}
        fieldset div#_ctl0__ctl2__ctl0__ctl0_idUserName_divControl input {border:none;font-family:Trebuchet MS, Helvetica, sans-serif;font-size:14px;color:#333;padding:6px;background:url('../img/Community/JDate/splash_register_field.gif') no-repeat top left;width:244px;}
        fieldset div#_ctl0__ctl2__ctl0__ctl0_idPassword_divControl input {border:none;font-family:Trebuchet MS, Helvetica, sans-serif;font-size:14px;color:#333;padding:6px;background:url('../img/Community/JDate/splash_register_field.gif') no-repeat top left;width:244px;}
        fieldset div#_ctl0__ctl2__ctl0__ctl0_idGender_divControl {margin-top:10px;}
        fieldset div#_ctl0__ctl2__ctl0__ctl0_idSeekingGender_divControl {margin-top:10px;}
        fieldset div#_ctl0__ctl2__ctl0__ctl0_idGender_divControl label {float:left;margin:4px 2px 0 4px;}
        fieldset div#_ctl0__ctl2__ctl0__ctl0_idGender_divControl select {float:left;}
        fieldset div#_ctl0__ctl2__ctl0__ctl0_idSeekingGender_divControl label {float:left;margin:4px 5px 0 5px;}
        fieldset div#_ctl0__ctl2__ctl0__ctl0_idSeekingGender_divControl select {float:left;}
        fieldset div#_ctl0__ctl2__ctl0__ctl0_idTermsAndConditions_divControl a {color:#fff;}
        fieldset div#_ctl0__ctl2__ctl0__ctl0_idTermsAndConditions_divControl a:hover {text-decoration:none;}
        fieldset div#_ctl0__ctl2__ctl0__ctl0_idTermsAndConditions_divControl input#_ctl0__ctl2__ctl0__ctl0_idTermsAndConditions_AttributeCheckBoxControl {float:left;margin-right:10px;}
	</style>
	<![endif]-->
<!-- GLOBAL CSS end -->


<div id="nonFooter">
		<div id="content">
			<div id="wrap_hdr">
				<div id="wrap_login">
					<a href="http://www.jdate.co.il/" class="text" title="אתר הכרויות">אתר הכרויות</a> <a href="http://www.jdate.co.il/"><img src="../img/Community/JDate/splash_flag_israel.gif" border="0" title="Hebrew" /></a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="http://www.jdate.fr/" class="text" title="Rencontres Juives">Rencontres Juives</a> <a href="http://www.jdate.fr/"><img src="../img/Community/JDate/splash_flag_france.gif" border="0" title="French" /></a> 
					<div id="wrap_loginForm">
						<uc1:LogonControlSplash20 id="LogonControl" runat="server" />
					</div>
				</div>
				<div id="wrap_logo">
					<div id="slogan">
						<script type="text/javascript"> 
						if(!FlashDetect.installed){
							document.write("<b class=\"slogan_lrg\" id=\"slogan_lrg\">Welcome to JDate</b><br /><b class=\"slogan_sml\" id=\"slogan_sml\">the Premier Jewish Community for Jewish Singles</b>");
							
						}else{
							document.write("<b class=\"slogan_lrg\">Welcome to JDate</b><br /><b class=\"slogan_sml\">the Premier Jewish Community for Jewish Singles</b>");     	
						}
						</script>
					</div>
					<a href="http://www.jdate.com/default.aspx" class="logo" title="JDate.com - The Largest Jewish Singles Network"></a>
				</div>
			</div>
			<div id="wrap_bodyMid">
				<div id="content_bodyMid"></div>
			</div>
			<div id="wrap_bodyBot">
			
				<div id="content_bodyBot">
				
					<div class="boxMain">
						<div class="boxMain_top">
							Connect with thousands of members in your area and around the world - photos, email, chat, IM & MORE. Sign up below!
						</div>
						<div class="boxMain_mid">
							<div id="fragment-1" class="ui-tabs-panel" style="background:url('../img/Community/JDate/splash_couple7.jpg');">
								<div class="wrap_quote">
									<span class="quote">"JDate was like this comfortable, trustworthy sweatshirt. The one you always go back to."</span><br />
									<span class="quote_couple"><b><i>Margarita &amp; Avi</i></b>; Married April 2009</span><br />
								</div>
							</div>
							<div id="fragment-2" class="ui-tabs-panel ui-tabs-hide" style="background:url('../img/Community/JDate/splash_couple6.jpg');">
								<div class="wrap_quote">
									<span class="quote">"Christmas morning 2008, Jeff wrapped an engagement ring in a big box…Did I mention we’re Jewish?"</span><br />
									<span class="quote_couple"><b><i>Paula &amp; Jeff</i></b>; Engaged December 2008</span><br />
								</div>
							</div>                                                                                                                                                                                                                        
							<div id="fragment-3" class="ui-tabs-panel ui-tabs-hide" style="background:url('../img/Community/JDate/splash_couple3.jpg');">
								<div class="wrap_quote">
									<span class="quote">"We credit divine intervention through JDate for our pairing. It's much bigger than us."</span><br />
									<span class="quote_couple"><b><i>Brenda &amp; David</i></b>; Married September 2008</span><br />
								</div>
							</div>
							<div id="fragment-4" class="ui-tabs-panel ui-tabs-hide" style="background:url('../img/Community/JDate/splash_couple5.jpg');">
								<div class="wrap_quote">
									<span class="quote">"I had no idea that I would meet my best friend and the man that I would spend my life with."</span><br />
									<span class="quote_couple"><b><i>Haleh &amp; Itai</i></b>; Engaged December 2008</span><br />
								</div>
							</div>
							<div class="register_content">
							    <!--<mn:Txt ID="txtTitle" runat="server" />-->
							    
								<div class="register_hdr">
									<span class="register_hdr_med">Register and post your </span><br /><span class="register_hdr_lrg"><b class="wht italic">FREE</b> profile</span>
								</div>
								<div class="register_hdr_shadow">
									<span class="register_hdr_med">Register and post your </span><br /><span class="register_hdr_lrg"><b class="italic">FREE</b> profile</span>
								</div>
							
								<fieldset class="primary-content <asp:Literal id='ylitCSSClass' runat=server/>" >
       
                                    <asp:PlaceHolder ID="phContent" runat="server">
                                        <uc1:ValidationMessage runat="server" ID="txtValidation" Visible=false />
                                    </asp:PlaceHolder>
                                    <div class="cta-wrapper">
                                        <!-- Begin Amadesa Element cta Tag -->
                                        <asp:Panel runat="server" ID="pnlJSCta" CssClass="amadesa-tag">
                                            <script language="javascript" type="text/javascript">liveExperience('cta');</script>
                                        </asp:Panel>
                                        <div id="cta">
                                            <div class="cta">
                                                <mn2:FrameworkButton runat="server" ID="btnContinue" ResourceConstant="TXT_CONTINUE_SPLASH"  CssClass="btn primary" />
                                            </div>
                                        </div>
                                        </noscript>
                                        <!-- End Amadesa Element cta Tag -->   
                                    </div>
                                </fieldset>
							</div>
							<div class="wrap_register"></div>
						</div>
						<div class="boxMain_bot"></div>
					</div>
					
					<div class="wrap_successstories">
						<script type="text/javascript"> 
						if(!FlashDetect.installed){
							document.write("<h2 class=\"success\" id=\"success\">Success Stories with real people</h2><h2 class=\"success_sml\" id=\"success_sml\">JDate is Where Dating Jewish Singles Go To Find Love.</h2>");
							
						}else{
							document.write("<h2 class=\"success\">Success Stories with real people</h2><h2 class=\"success_sml\">JDate is Where Dating Jewish Singles Go To Find Love.</h2>");     	
						}
						</script>
					</div>
					
					<ul class="ui-tabs-nav">
						<li id="nav-fragment-1" class="ui-tabs-nav-item ui-tabs-selected" style="background:url('../img/Community/JDate/splash_couple7_thumb.jpg');">
							<a href="#fragment-1" class="thumb"><div>Margarita &amp; Avi</div></a>
						</li>
						<li id="nav-fragment-2" class="ui-tabs-nav-item" style="background:url('../img/Community/JDate/splash_couple6_thumb.jpg');">
							<a href="#fragment-2" class="thumb"><div>Paula &amp; Jeff</div></a>
						</li>
						<li id="nav-fragment-3" class="ui-tabs-nav-item" style="background:url('../img/Community/JDate/splash_couple3_thumb.jpg');">
							<a href="#fragment-3" class="thumb"><div>Brenda &amp; David</div></a>
						</li>
						<li id="nav-fragment-4" class="ui-tabs-nav-item" style="background:url('../img/Community/JDate/splash_couple5_thumb.jpg');">
							<a href="#fragment-4" class="thumb"><div>Haleh &amp; Itai</div></a>
						</li>
					</ul>
				
					<div class="wrap_desc">
						<div class="desc_lft">
							<script type="text/javascript"> 
							if(!FlashDetect.installed){
								document.write("<h2 class=\"desc\" id=\"desc\">JDate has been building the Jewish community for more than a decade,</h2>");
								
							}else{
								document.write("<h2 class=\"desc\">JDate has been building the Jewish community for more than a decade,</h2>");     	
							}
							</script>
							so it's not surprising that it seems like everyone knows someone who <b class="red italic">fell in love</b> <b>on JDate</b> and it's no wonder why <b>countless marriages</b> begin right here!
							<script type="text/javascript"> 
							if(!FlashDetect.installed){
								document.write("<h2 class=\"desc_sml\" id=\"desc_sml\">Each week, hundreds of JDaters meet their soul mates.</h2>");
								
							}else{
								document.write("<h2 class=\"desc_sml\">Each week, hundreds of JDaters meet their soul mates.</h2>");     	
							}
							</script>
							To see why JDate is the undisputed <b>leader in Jewish dating</b> and the modern alternative to traditional Jewish matchmaking, just take a look at our <a href="/Applications/Article/ArticleView.aspx?CategoryID=2006&amp;ShowTitle=1">Success Stories</a> section.<br />
							<div class="wrap_desc_btn">
								<a href="/Applications/Article/ArticleView.aspx?CategoryID=2006&amp;ShowTitle=1" class="btn">Read Success Stories</a>
							</div>
						</div>
						<div class="desc_rgt">
							<script type="text/javascript"> 
							if(!FlashDetect.installed){
								document.write("<h2 class=\"desc_med\" id=\"desc_med\">JDate is Where Tens of Thousands of Jewish Singles Interact Every Day!</h2>");
								
							}else{
								document.write("<h2 class=\"desc_med\">JDate is Where Tens of Thousands of Jewish Singles Interact Every Day!</h2>");     	
							}
							</script>
							<div class="desc_box">
								<div class="desc_box_content">
									<script type="text/javascript"> 
									if(!FlashDetect.installed){
										document.write("<h3 class=\"desc_title\" id=\"desc_title\">JDate is an ideal destination</h3>");
										
									}else{
										document.write("<h3 class=\"desc_title\">JDate is an ideal destination</h3>");     	
									}
									</script>
									<p>for Jewish men and Jewish women to <b>make connections</b>, and <b class="italic">find friends, dates and soul mates</b>, all within the faith. </p>									
									<p>Having <b class="lrg">hundreds of thousands</b> of members, <a href="http://www.jdate.com/Applications/Article/ArticleView.aspx?CategoryID=1998&HideNav=True" class="txt">fun and easy online features</a> and <b>fantastic</b> offline activity options (including, <a href="http://www.jdate.com/Applications/Events/Events.aspx" class="txt">travel and events</a>), makes...</p>
									<p><b class="italic">JDate</b> is the number one place for <b class="italic">Jewish</b> <b class="red italic">romance</b> in the world!</p>
								</div>
							</div>
							<div class="desc_box">
								<div class="desc_box_content">
									<script type="text/javascript"> 
									if(!FlashDetect.installed){
										document.write("<h3 class=\"desc_title\" id=\"desc_title2\">Create Your Profile</h3>");
										
									}else{
										document.write("<h3 class=\"desc_title\">Create Your Profile</h3>");     	
									}
									</script>
									<p>and in minutes, you’ll be ready to <b class="italic">start communicating</b> with the JDaters who live near you.</p>
									<p>After you join, check out our:
										<ul>
											<li><a href="/Applications/Video/VideoCenter.aspx" class="txt">JDate Videos</a></li>
											<li><a href="http://static.jdate.com/microsites/jewishholidays/" class="txt">Jewish Holiday Calendar</a></li>
											<li><a href="http://static.jdate.com/microsites/synagoguedirectory/default.asp" class="txt">Synagogue Directory</a></li>
											<li><a href="http://www.jdate.com/jmag/" class="txt">Jewish Magazine</a></li>
											<li><a href="http://www.jdate.com/blog/" class="txt">Blog</a>, where you can follow real-life stories of JDaters in <a href="http://search.jdate.com/California/LosAngeles/" class="txt">Los Angeles</a>, <a href="http://search.jdate.com/NewYork/NewYork/" class="txt">New York City</a> and much, much more.</li>
										</ul>
									</p>
								</div>
							</div>
							<div class="desc_box">
								<div class="desc_box_content">
									<script type="text/javascript"> 
									if(!FlashDetect.installed){
										document.write("<h3 class=\"desc_title\" id=\"desc_title3\">Register and Post your FREE JDate Profile Today!</h3>");
										
									}else{
										document.write("<h3 class=\"desc_title\">Register and Post your<br />FREE JDate Profile Today!</h3>");     	
									}
									</script>
									On JDate, your beshert is just a click away.<br />
									
									<div class="wrap_desc_btn_adj">
										<a href="http://www.jdate.com/Applications/Registration/Registration.aspx" class="btn_adj">Click to register for FREE</a>
									</div>
									
								</div>
							</div>
						</div>
					</div>
				
				</div>
			</div>
		</div>
	</div>
	<div id="footer">
		<div id="wrap_ftr">
			<div id="content_ftr">
			
				<div class="ftr_box_rgt">
					<p>Copyright © 2009 Spark Networks® Limited. All rights reserved.</p>
					<p>Spark Networks Limited is a wholly-owned subsidiary of Spark Networks, Inc., a NYSE MKT Company (<a href="http://www.amex.com/equities/listCmp/EqLCPrint.jsp?Product_Symbol=LOV&monthVal=12mths" class="wht">LOV</a>).</p>
					<p><b class="wht" style="font-size:12px;">SPARK NETWORKS LIMITED DOES NOT CONDUCT BACKGROUND CHECKS ON THE MEMBERS OR SUBSCRIBERS OF THIS WEBSITE.</b></p>
				</div>
			
				<div class="ftr_box">
					<div class="content_ftr_hdr">Helpful Links</div>
					<div class="content_ftr_hdr_shadow">Helpful Links</div>
					<ul>
						<li><a href="http://www.jdate.com/">Home</a></li>
						<li><a href="/Applications/Logon/Logon.aspx">Login</a></li>
						<li><a href="/Applications/ContactUs/ContactUs.aspx">Contact Us</a></li>
						<li><a href="/Applications/MemberServices/MemberServices.aspx">Member Services</a></li>
						<li><a href="/Applications/MemberServices/VerifyEmail.aspx">Verify Email</a></li>
						<li><a href="https://www.jdate.com/Applications/Subscription/Subscribe.aspx?prtid=16">Subscribe</a></li>
						<li><a href="/Applications/Article/ArticleView.aspx?CategoryID=1948&amp;ArticleID=6498&amp;HideNav=True#service">Terms of Service</a></li>
						<li><a href="/Applications/Article/ArticleView.aspx?CategoryID=1998&amp;HideNav=True">About JDate</a></li>
					</ul>
				</div>
				
				<div class="ftr_box">
					<ul>
						<li><a href="http://static.jdate.com/Campaigns/MK07-0110_Gift_of_JDate/">Gift of JDate</a></li>
						<li><a href="/Applications/Video/VideoEpisodeCenter.aspx">Video</a></li>
						<li><a href="http://www.jdate.com/jmag">JMag</a></li>
						<li><a href="/Applications/Events/Events.aspx">Travel & Events</a></li>
						<li><a href="/Applications/MemberServices/smsalert.aspx">Mobile Alerts</a></li>
						<li><a href="http://static.jdate.com/microsites/jewishholidays/">Jewish Holiday Calendar</a></li>
						<li><a href="http://www.jdate.com/synagoguesdirectory">Synagogue Directory</a></li>
						<li><a href="http://www.jdate.com/newscenter">News Center</a></li>
						<li><a href="/Applications/Article/ArticleView.aspx?CategoryID=2060&amp;HideNav=true">Our Mission</a></li>
						<li><a href="/Applications/Article/ArticleView.aspx?CategoryID=114&amp;HideNav=true">Safety</a></li>
						<li><a href="/Applications/SiteMap/SiteMap.aspx">Site Map</a></li>
					</ul>
				</div>
				
				<div class="ftr_box">
					<ul>
						<li><a href="http://www.jdate.com/blog/">Blog</a></li>
						<li><a href="/Applications/Article/FAQMain.aspx">Help & Advice</a></li>
						<li><a href="http://www.spark.net/about.htm">About Spark Networks</a></li>
						<li><a href="http://www.spark.net/our-portfolio/portfolio-overview/">Spark Networks' Sites</a></li>
						<li><a href="http://static.jdate.com/components/advertise/jd/">Advertise With Us</a></li>
						<li><a href="https://affiliates.jdate.com/">Affiliate Program</a></li>
						<li><a href="http://www.spark.net/investor.htm">Investor Relations</a></li>
						<li><a href="http://www.spark.net/careers.htm">Jobs</a></li>
						<li><a href="/Applications/Article/ArticleView.aspx?CategoryID=1948&amp;ArticleID=6498&amp;HideNav=True#privacy">Privacy</a></li>
						<li><a href="http://search.jdate.com/">Local Online Dating</a></li>
						<li><a href="http://www.spark.net/intellectual.htm">Our Intellectual Property</a></li>
					</ul>
				</div>
				
			</div>
		</div>
	</div>
	
<script type="text/javascript">
//<![CDATA[
/* Replacement calls. Please see documentation for more information. */

if(typeof sIFR == "function"){

// This is the preferred "named argument" syntax
	sIFR.replaceElement(named({sSelector:"b.subhead", sFlashSrc:"../img/Community/JDate/myriadpro.swf", sColor:"#ffffff", sLinkColor:"#ffffff", sBgColor:"#6f8296", sHoverColor:"#6f8296", nPaddingTop:0, nPaddingBottom:0, sFlashVars:"textalign=left&offsetTop=0"}));
	sIFR.replaceElement(named({sSelector:"h2.success", sFlashSrc:"../img/Community/JDate/myriadpro.swf", sColor:"#ffffff", sLinkColor:"#ffffff", sBgColor:"#c0c6cf", sHoverColor:"#c0c6cf", nPaddingTop:0, nPaddingBottom:0, sFlashVars:"textalign=left&offsetTop=0"}));
	sIFR.replaceElement(named({sSelector:"h2.success_sml", sFlashSrc:"../img/Community/JDate/myriadpro.swf", sColor:"#ffffff", sLinkColor:"#ffffff", sBgColor:"#c0c6cf", sHoverColor:"#c0c6cf", nPaddingTop:0, nPaddingBottom:0, sFlashVars:"textalign=left&offsetTop=0"}));
	sIFR.replaceElement(named({sSelector:"h2.desc", sFlashSrc:"../img/Community/JDate/myriadpro.swf", sColor:"#222222", sLinkColor:"#222222", sBgColor:"#eeeeee", sHoverColor:"#eeeeee", nPaddingTop:0, nPaddingBottom:0, sFlashVars:"textalign=left&offsetTop=0"}));
	sIFR.replaceElement(named({sSelector:"h2.desc_sml", sFlashSrc:"../img/Community/JDate/myriadpro.swf", sColor:"#222222", sLinkColor:"#222222", sBgColor:"#eeeeee", sHoverColor:"#eeeeee", nPaddingTop:0, nPaddingBottom:0, sFlashVars:"textalign=left&offsetTop=0"}));
	sIFR.replaceElement(named({sSelector:"h2.desc_med", sFlashSrc:"../img/Community/JDate/myriadpro.swf", sColor:"#222222", sLinkColor:"#222222", sBgColor:"#eeeeee", sHoverColor:"#eeeeee", nPaddingTop:0, nPaddingBottom:0, sFlashVars:"textalign=left&offsetTop=0"}));
	sIFR.replaceElement(named({sSelector:"h3.desc_title", sFlashSrc:"../img/Community/JDate/myriadpro.swf", sColor:"#222222", sLinkColor:"#222222", sBgColor:"#ffffff", sHoverColor:"#ffffff", nPaddingTop:0, nPaddingBottom:0, sFlashVars:"textalign=left&offsetTop=0"}));

	sIFR.replaceElement("b.slogan_lrg", "../img/Community/JDate/myriadpro.swf", "#666666", null, null, null, 0, 0, 0, 0);
	sIFR.replaceElement("b.slogan_sml", "../img/Community/JDate/myriadpro.swf", "#666666", null, null, null, 0, 0, 0, 0);
};

//]]>
</script>	



    <!-- Begin Amadesa Element headline Tag -->
    <asp:Panel runat="server" ID="pnlJSHeadline">
        <script language="javascript" type="text/javascript">liveExperience('headline');</script>
    </asp:Panel>
    


    </noscript>
    <!-- End Amadesa Element headline Tag -->
    
    