﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
namespace Matchnet.Web.Applications.Registration.Templates
{
    interface IRegistrationTemplate
    {
        PlaceHolder ContentPlaceholder { get; }
    }
}
