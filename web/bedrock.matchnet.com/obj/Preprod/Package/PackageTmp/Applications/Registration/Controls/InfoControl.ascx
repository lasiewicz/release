﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="InfoControl.ascx.cs"
    Inherits="Matchnet.Web.Applications.Registration.Controls.InfoControl" %>
<%@ Register TagPrefix="uc2" TagName="ValidationMessage" Src="/Applications/Registration/Controls/ValidationMessage.ascx" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<div id="divControl" runat="server">
    <uc2:ValidationMessage runat="server" ID="txtValidation" Visible="false" />
    <div id="page-container" class="cc-content-main cc-content-bars mandatory">
        <mn:Txt runat="server" ID="txtCaption" />
    </div>
</div>
