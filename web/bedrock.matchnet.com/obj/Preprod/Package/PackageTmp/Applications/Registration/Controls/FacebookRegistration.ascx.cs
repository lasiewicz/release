﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Web.Framework;

namespace Matchnet.Web.Applications.Registration.Controls
{
    public partial class FacebookRegistration : FrameworkControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        
        public LinkButton ButtonNoThanks
        {
            get { return btnNoThanks; }
        }

        public Button ButtonFBRegister
        {
            get { return this.btnFBRegister; }
        }

        public string FBName
        {
            get { return hdnFBName.Value; }
        }

        public string FBBirthday
        {
            get { return hdnFBBirthday.Value; }
        }

        public string FBLocation
        {
            get { return hdnFBLocation.Value; }
        }

        public string FBEmail
        {
            get { return hdnFBEmail.Value; }
        }
    }
}