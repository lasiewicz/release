﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ValidationMessage.ascx.cs" Inherits="Matchnet.Web.Applications.Registration.Controls.ValidationMessage" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>

<em class="error"><mn:Txt runat="server" ID="txtValidation"  /></em>