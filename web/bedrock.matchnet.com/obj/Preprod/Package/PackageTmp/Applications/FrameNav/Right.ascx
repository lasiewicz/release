﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Right.ascx.cs" Inherits="Matchnet.Web.Applications.FrameNav.Right" %>
<%@ Register TagPrefix="mn1" TagName="AdUnit" Src="~/Framework/Ui/Advertising/AdUnit.ascx" %>


<asp:PlaceHolder ID="phRightSquareAd" runat="server" Visible="false">
  <mn1:AdUnit id="adSquareRight" runat="server" expandImageTokens="true" Size="Square"
                      GAMAdSlot="right_300x250" GAMPageMode="Auto" />
</asp:PlaceHolder>