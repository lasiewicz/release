﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="QuickSearch20.ascx.cs" Inherits="Matchnet.Web.Applications.QuickSearch.QuickSearch20" %>
<%@ Register TagPrefix="anthem" Namespace="Anthem" Assembly="Anthem" %>
<%@ Register TagPrefix="mnfe" TagName="PickRegionNew" Src="../../Framework/Ui/FormElements/PickRegionNew.ascx" %>
<%@ Register TagPrefix="mnfe" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn" TagName="AdUnit" Src="/Framework/UI/Advertising/AdUnit.ascx" %>
<%@ Register TagPrefix="cc1" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>

<div class="page-container">
<asp:PlaceHolder ID="divRightNewStart" Runat="server">
</asp:PlaceHolder>
<asp:PlaceHolder ID="plcTitle" Runat="server">
	
	<div id="divTitle" runat="server">
		<mn:Title runat="server" id="ttlPreferences" ResourceConstant="NAV_QUICK_SEARCH"/>
	</div>	
	</asp:PlaceHolder>
			<mn:MultiValidator id="AgeMinValidator" tabIndex="-1" runat="server" Display="Dynamic" RequiredType="IntegerType"
				ControlToValidate="tbAgeMin" MinValResourceConstant="MIN_AGE_NOTICE" MaxValResourceConstant="MIN_AGE_NOTICE"  FieldNameResourceConstant="MIN_AGE_NOTICE" IsRequired="True" MinVal="18"
				MaxVal="99" AppendToErrorMessage="<br />" />
			<mn:MultiValidator id="AgeMaxValidator" tabIndex="-1" runat="server" Display="Dynamic" RequiredType="IntegerType"
				ControlToValidate="tbAgeMax" MinValResourceConstant="MAX_AGE_NOTICE" MaxValResourceConstant="MAX_AGE_NOTICE" FieldNameResourceConstant="MAX_AGE_NOTICE" IsRequired="True" MinVal="18"
				MaxVal="99" AppendToErrorMessage="<br />" />
	        <mn:MultiValidator id="LocationValidator"  runat="server" Display="Dynamic" MinimumLength="1"
		        MaximumLength="50" RequiredType="StringType" FieldNameResourceConstant="LOCATION_REQUIRED"
		        IsRequired="False">
	        </mn:MultiValidator>

			<div class="filter clearfix">
				<label>
					<mn:Txt id="txtGender" runat="server" ResourceConstant="TXT_YOURE_A" /><br />
				    <asp:DropDownList ID="ddlGender" Runat="server" />
				</label>
				<label>
				    <mn:Txt id="txtSeekingGender" runat="server" ResourceConstant="TXT_SEEKING_A" /><br />
				    <asp:DropDownList ID="ddlSeekingGender" Runat="server" />
			    </label>
			    <div class="age-container">
				    <label class="age-from">
				        <mn:Txt id="txtAgeMin" runat="server" ResourceConstant="TXT_AGE" /><br />
				        <asp:TextBox ID="tbAgeMin" Runat="server" MaxLength="2" />
				    </label>
				    <label class="age-to"><br />
				        <mn:Txt id="txtAgeMax" runat="server" ResourceConstant="TO_" />
				        <asp:TextBox ID="tbAgeMax" Runat="server" MaxLength="2" />
				    </label>
				</div>
				<label>
					<anthem:PlaceHolder ID="plcDistance" Runat="server">
						<mn:Txt id="txtWithin" ResourceConstant="TXT_LOCATED_WITHIN" runat="server"></mn:Txt><br />
                        <asp:DropDownList id="ddlDistance" Runat="server"></asp:DropDownList>&nbsp; 
                        <mn:Txt id="txtOf" ResourceConstant="TXT_OF" runat="server"></mn:Txt>
					</anthem:PlaceHolder>
					<anthem:PlaceHolder ID="plcAreaCodes" Runat="server" Visible="false">
						<mn:Txt id="txtAreaCodes" ResourceConstant="TXT_IN_AREA_CODES" runat="server"></mn:Txt>
					</anthem:PlaceHolder>
					<anthem:PlaceHolder ID="plcCollege" Runat="server" Visible="false">
						<mn:Txt id="txtCollege" ResourceConstant="TXT_AT_COLLEGE" runat="server"></mn:Txt>
					</anthem:PlaceHolder>
				</label>
				<div class="prefSearchPRInfoContDynamic no-wrap-class text-inside">
					<anthem:HyperLink ID="lnkLocation" Runat="server" />
					<mnfe:PickRegionNew id="prSearchRegion" Runat="server" />
				</div>
			<div class="quicksearch-submit">
			    <div class="quicksearch-submit-hasphoto">
			        <div><asp:CheckBox ID="cbJewishOnly" Runat="server" Visible="false"/></div>
			        <div><asp:CheckBox ID="cbHasPhoto" Runat="server" /></div>
                    <div><asp:CheckBox ID="cbxRamahDate" Runat="server" Visible="false" /></div>
                </div>
			    <div class="quicksearch-submit-btn"><asp:Button ID="btnSave" Runat="server" class="btn secondary" /></div>
			</div>

            <hr />

</div> <!-- end selection container -->
<%--<asp:PlaceHolder ID="plcQuickSearchAd" Runat="server">
	<div id="divQuickSearchAd" class="quickSearchAd" runat="server">
		<!--<mn:AdUnit id="adSquare" Size="Square" runat="server" GAMAdSlot="quicksearch_middle_300x250"/>---0->
	</div>
</asp:PlaceHolder>--%>
<asp:PlaceHolder ID="divRightNewEnd" Runat="server">

</asp:PlaceHolder>
</div>
