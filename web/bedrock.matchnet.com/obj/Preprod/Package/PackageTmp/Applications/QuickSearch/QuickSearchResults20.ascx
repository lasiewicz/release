﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="QuickSearchResults20.ascx.cs" Inherits="Matchnet.Web.Applications.QuickSearch.QuickSearchResults20" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="result" TagName="ResultList" Src="../../Framework/Ui/BasicElements/ResultList20.ascx" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn" TagName="miniProfile" Src="/Framework/Ui/BasicElements/MiniProfile20.ascx" %>
<%@ Register TagPrefix="cc1" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn" TagName="QuickSearchEdit" Src="QuickSearch20.ascx" %>
<%@ Register TagPrefix="mn1" TagName="ResultsViewType" Src="../../Framework/Ui/BasicElements/ResultsViewType.ascx" %>
<%@ Register TagPrefix="mn1" TagName="ColorCodeSelect" Src="/Framework/Ui/BasicElements/ColorCodeSelect.ascx" %>
<div class="header-options clearfix">
	<mn:Title runat="server" id="txtMatches" ResourceConstant="NAV_SUB_MATCHES"/>
	<div class="pagination">
		<asp:label id="lblListNavigationTop" Runat="server" />
	</div>
</div>

<mn:QuickSearchEdit id="ucQuickSearchEdit" runat="server" />

<div class="sort-display" style="border:none">
    <ul class="nav-rounded-tabs no-click clearfix">
        <li class="no-tab search-type">
	        <mn:txt runat="server" id="txtView" resourceconstant="TXT_VIEW_BY" />
        </li>
        <asp:Repeater ID="rptSearchOption" Runat="server">
            <ItemTemplate>
	            <li class="<asp:Literal id=litSortSpan runat=server /> tab">
		            <asp:HyperLink ID="lnkSort" Runat="server" />
		           <span class="x"> <asp:Literal ID="litSortTitle" Runat="server" Visible="False" /></span>
            </ItemTemplate>
        </asp:Repeater>
        <li class="no-tab view-type">
            <mn1:ResultsViewType id="idResultsViewType" runat="server" />   
        </li>
    </ul>

    <mn1:ColorCodeSelect id="colorCodeSelect" runat="server" />

    <input type="hidden" name="hidSearchOrderBy" runat="server" id="hidSearchOrderBy" />
</div>

<div id="results-container" class="clearfix clear-both">
    <asp:placeholder id="Results" runat="server" visible="true">
        <asp:PlaceHolder ID="plcPromotionalProfile" Runat="server" visible="false">
	        <mn:miniprofile runat="server" id="PromotionProfile" visible="true" />
        </asp:PlaceHolder>
	    <result:resultlist id="searchResultList" runat="server" resultlistcontexttype="QuickSearchResult"></result:resultlist>
	    <div class="pagination">
		    <asp:label id="lblListNavigationBottom" Runat="server"></asp:label>
		</div>
	    <input type="hidden" value="<% = ChangeVoteMessage %>" name="ChangeVoteMessage" /> <iframe tabIndex="-1" name="FrameYNMVote" frameBorder="0" width="1" scrolling="no" height="1">
		    <layer name="FrameYNMVote" frameborder="0" scrolling="no" width="1" height="1"></layer>
	    </iframe>
    </asp:placeholder>
</div>
