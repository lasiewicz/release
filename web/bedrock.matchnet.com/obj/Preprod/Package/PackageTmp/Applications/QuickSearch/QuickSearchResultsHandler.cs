﻿using System;
using System.Collections;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Web.Framework;

using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.Region;
using Matchnet.Search.ValueObjects;
using Matchnet.Session.ValueObjects;
using Matchnet.Web.Framework.Managers;
using Matchnet.Web.Framework.Util;
using Matchnet.Web.Framework.Ui.BasicElements;

using System.Text;
using System.Xml;
using Matchnet.Web.Framework.Ui.FormElements;
using Spark.SAL;
using System.Drawing;
using Matchnet.Web.Applications.Search;

namespace Matchnet.Web.Applications.QuickSearch
{
    public class QuickSearchResultsHandler
    {
        IQuickSearchResults _control;
        ContextGlobal g;
        SettingsManager _settingsManager = null;

        public QuickSearchResultsHandler(IQuickSearchResults control, ContextGlobal context)
        {
            _control = control;
            g = context;
            _settingsManager = new SettingsManager();
        }

        public void SetUpQuickSearchEdit()
        {
            if (_control.QuickSearchEdit.PlcTitle != null)
            {
                _control.QuickSearchEdit.PlcTitle.Visible = false;
            }
            if (_control.QuickSearchEdit.QuickSearchAd != null)
            {
                _control.QuickSearchEdit.QuickSearchAd.Visible = false;
            }
            if (_control.QuickSearchEdit.PlcDivRightNewStart != null)
            {
                _control.QuickSearchEdit.PlcDivRightNewStart.Visible = false;
            }
            if (_control.QuickSearchEdit.PlcDivRightNewEnd != null)
            {
                _control.QuickSearchEdit.PlcDivRightNewEnd.Visible = false;
            }
        }


        public bool ValidRegionID(string RegionID)
        {
            if (RegionID != null && RegionID != string.Empty && Conversion.CInt(RegionID, Constants.NULL_INT) != Constants.NULL_INT)
            {
                return true;
            }
            return false;
        }


        public void SetSearchPreferencesFromQueryString()
        {
            SearchPreferenceCollection preferences = g.SearchPreferences;

            int searchTypeVal = Conversion.CInt(HttpContext.Current.Request["SearchTypeID"]);

            SearchTypeID searchTypeID;
            if (searchTypeVal != Constants.NULL_INT)
            {
                searchTypeID = (SearchTypeID)searchTypeVal;
            }
            else
            {
                searchTypeID = SearchTypeID.PostalCode;
            }
            preferences.Add("SearchTypeID", ((int)searchTypeID).ToString());

            int genderMask = Constants.NULL_INT;
            int genderID = Conversion.CInt(HttpContext.Current.Request["GenderID"]);
            int seekingGenderID = Conversion.CInt(HttpContext.Current.Request["SeekingGenderID"]);

            //If we get only a SeekingGenderID this is a legacy JDate search so it should be hetero.
            if (genderID == Constants.NULL_INT && seekingGenderID != Constants.NULL_INT)
            {
                if (seekingGenderID == Matchnet.Lib.ConstantsTemp.GENDERID_SEEKING_MALE)
                {
                    genderID = Matchnet.Lib.ConstantsTemp.GENDERID_FEMALE;
                }
                else
                {
                    genderID = Matchnet.Lib.ConstantsTemp.GENDERID_MALE;
                }
            }

            genderMask = GenderUtils.FlipMaskIfHeterosexual(genderID + seekingGenderID);

            int countryRegionID = Constants.NULL_INT;
            if (HttpContext.Current.Request["CountryRegionID"] != null)
            {
                countryRegionID = Conversion.CInt(HttpContext.Current.Request["CountryRegionID"]);
            }
            else
            {
                countryRegionID = g.Brand.Site.DefaultRegionID;
            }
            preferences.Add("CountryRegionID", countryRegionID.ToString());
            preferences.Add("RegionID", HttpContext.Current.Request["RegionID"]);

            //Set RegionID based on ZipCode or City searches
            RegionID regionID = null;
            string zipCode = HttpContext.Current.Request["ZipCode"];
            if (zipCode != null)
            {
                regionID = RegionSA.Instance.FindRegionIdByPostalCode(countryRegionID, zipCode);
            }

            //Sometimes " " gets replaced by "+" in encoded URL params
            string city = HttpContext.Current.Request["City"];
            int stateRegionID = Conversion.CInt(HttpContext.Current.Request["StateRegionID"]);
            if (stateRegionID != Constants.NULL_INT && city != null)
            {
                city = city.Replace("+", " ");
                regionID = RegionSA.Instance.FindRegionIdByCity(stateRegionID, city, g.Brand.Site.LanguageID);
            }

            if (regionID != null)
            {
                preferences.Add("RegionID", regionID.ID.ToString());
            }


            preferences.Add("GenderMask", genderMask.ToString());
            if (HttpContext.Current.Request["Distance"] == null)
            {
                preferences.Add("Distance", g.Brand.DefaultSearchRadius.ToString());
            }
            else
            {
                preferences.Add("Distance", HttpContext.Current.Request["Distance"]);
            }
            if (HttpContext.Current.Request["SearchOrderBy"] == null || HttpContext.Current.Request["SearchOrderBy"] == String.Empty)
            {
                preferences.Add("SearchOrderBy", "1");
            }
            else
            {
                preferences.Add("SearchOrderBy", HttpContext.Current.Request["SearchOrderBy"]);
            }

            int minAge = Conversion.CInt(HttpContext.Current.Request["MinAge"]);
            int maxAge = Conversion.CInt(HttpContext.Current.Request["MaxAge"]);

            if (HttpContext.Current.Request["AgeRangeID"] != null)
            {
                GetAgeRangeFromID(Conversion.CInt(HttpContext.Current.Request["AgeRangeID"]), ref minAge, ref maxAge);
            }

            preferences.Add("MinAge", minAge.ToString());
            preferences.Add("MaxAge", maxAge.ToString());

            preferences.Add("SchoolID", HttpContext.Current.Request["SchoolID"]);
            preferences.Add("HasPhotoFlag", HttpContext.Current.Request["HasPhotoFlag"]);
            preferences.Add("EducationLevel", HttpContext.Current.Request["EducationLevel"]);
            preferences.Add("Religion", HttpContext.Current.Request["Religion"]);
            preferences.Add("LanguageMask", HttpContext.Current.Request["LanguageMask"]);
            preferences.Add("Ethnicity", HttpContext.Current.Request["Ethnicity"]);
            preferences.Add("SmokingHabits", HttpContext.Current.Request["SmokingHabits"]);
            preferences.Add("DrinkingHabits", HttpContext.Current.Request["DrinkingHabits"]);
            preferences.Add("MinHeight", HttpContext.Current.Request["MinHeight"]);
            preferences.Add("MaxHeight", HttpContext.Current.Request["MaxHeight"]);
            preferences.Add("JDateReligion", HttpContext.Current.Request["JDateReligion"]);
            preferences.Add("JDateEthnicity", HttpContext.Current.Request["JDateEthnicity"]);
            preferences.Add("SynagogueAttendance", HttpContext.Current.Request["SynagogueAttendance"]);
            preferences.Add("KeepKosher", HttpContext.Current.Request["KeepKosher"]);
            preferences.Add("MajorType", HttpContext.Current.Request["MajorType"]);
            preferences.Add("RelationshipMask", HttpContext.Current.Request["RelationshipType"]);		//RelationshipType from collegeclub.com
            preferences.Add("RelationshipStatus", HttpContext.Current.Request["RelationshipStatus"]);
            preferences.Add("AreaCode1", HttpContext.Current.Request["AreaCode1"]);

            CreatePreferencesTabCookie("City");

            g.SaveSearchPreferences();

        }

        public void GetAgeRangeFromID(int AgeRangeID, ref int MinAge, ref int MaxAge)
        {
            switch (AgeRangeID)
            {
                case 0:
                    MinAge = 18;
                    MaxAge = 24;
                    break;
                case 1:
                    MinAge = 25;
                    MaxAge = 34;
                    break;
                case 2:
                    MinAge = 35;
                    MaxAge = 44;
                    break;
                case 3:
                    MinAge = 45;
                    MaxAge = 54;
                    break;
                case 4:
                    MinAge = 55;
                    MaxAge = 99;
                    break;
                default:
                    MinAge = 18;
                    MaxAge = 99;
                    break;
            }
        }

        public void CreatePreferencesTabCookie(string pTabValue)
        {
            HttpCookie prefTab = HttpContext.Current.Request.Cookies.Get("mn_searchPrefTabState");

            if (prefTab == null)
            {
                prefTab = new HttpCookie("mn_searchPrefTabState");
            }
            prefTab.Value = pTabValue;
            HttpContext.Current.Response.Cookies.Add(prefTab);
        }

        public void BindSortDropDown()
        {
            string key = "SearchOrderBy";
            DataTable cct = Option.GetOptions(key, g);

            if (_settingsManager.GetSettingBool(SettingConstants.ENABLE_COLORCODE_SEARCH, g.Brand))
            {
                DataRow row = cct.NewRow();
                row["Content"] = "Color Code";
                row["ListOrder"] = cct.Rows.Count + 1;
                row["Value"] = (int)Matchnet.Search.Interfaces.QuerySorting.ColorCode;
                cct.Rows.Add(row);
               
            }
            // Proximity sorting option must be removed when searching by AreaCode or College.
            if (g.QuickSearchPreferences["SearchTypeID"] != null)
            {
                int iSearchTypeID = Matchnet.Conversion.CInt(g.QuickSearchPreferences["SearchTypeID"], Constants.NULL_INT);

                if ((SearchTypeID.AreaCode.Equals((SearchTypeID)iSearchTypeID))
                    || (SearchTypeID.College.Equals((SearchTypeID)iSearchTypeID)))
                {
                    int proximity = (int)Matchnet.Search.Interfaces.QuerySorting.Proximity;
                    DataRow proximitySortOption = null;

                    foreach (DataRow row in cct.Rows)
                    {
                        if (Conversion.CInt(row["Value"]) == proximity)
                        {
                            proximitySortOption = row;
                        }
                    }

                    if (proximitySortOption != null)
                    {
                        proximitySortOption.Delete();
                    }
                }
            }

            // Popularity sorting option removed when not enabled
            if (!SearchUtil.IsSearchSortByPopularityEnabled(g.Brand.BrandID, g.Brand.Site.SiteID, g.Brand.Site.Community.CommunityID))
            {
                DataRow popularitySortOption = null;
                foreach (DataRow row in cct.Rows)
                {
                    if (Conversion.CInt(row["Value"]) == (int)Matchnet.Search.Interfaces.QuerySorting.Popularity)
                    {
                        popularitySortOption = row;
                    }
                }

                if (popularitySortOption != null)
                {
                    popularitySortOption.Delete();
                }
            }

            _control.RptSearchOption.DataSource = cct;
            _control.RptSearchOption.DataBind();
        }

        public string GetSearchOrderBy()
        {
            string ret = string.Empty;
            string searchOrder = string.Empty;

            // check the request for change in SearchOrderBy
            if (HttpContext.Current.Request["SearchOrderBy"] != null)
                searchOrder = HttpContext.Current.Request["SearchOrderBy"];
            else
                searchOrder = g.QuickSearchPreferences["SearchOrderBy"];

            switch (searchOrder)
            {
                case "1":
                    ret = "Newest";
                    break;
                case "2":
                    ret = "Most Active";
                    break;
                case "3":
                    ret = "Closest To You";
                    break;
                case "4":
                    ret = "Most Popular";
                    break;
                case "5":
                    ret = "Color Code";
                    break;
            }

            return ret;
        }

        public Matchnet.Web.Framework.Ui.BasicElements.ResultsViewType.ListViewType GetSearchResultViewMode(bool isPostBack)
        {
            string defaultView = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("IS_SEARCH_RESULT_DEFAULT_VIEW_GALLERY", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID);
            string galleryView = String.Empty; // TT #13433 - make galleryview the default for search results

            // check the request for change in ViewMode
            string ViewMode = HttpContext.Current.Request["ViewMode"];
            if (ViewMode != null)
            {
                if (ViewMode.ToLower() == "gallery")
                {
                    galleryView = "true";
                }
                else if (ViewMode.ToLower() == "list")
                {
                    galleryView = "false";
                }
            }
            else
            {
                galleryView = defaultView;

                if (isPostBack)
                {
                    if (g.Member != null)
                    {
                        //get list view type from member attribute
                        Matchnet.Web.Framework.Ui.BasicElements.ResultsViewType.ListViewType lvType = ResultsViewType.GetMemberListType(g);
                        if (lvType != ResultsViewType.ListViewType.none)
                        {
                            if (lvType == Matchnet.Web.Framework.Ui.BasicElements.ResultsViewType.ListViewType.gallery)
                                galleryView = "true";
                            else
                                galleryView = "false";
                        }
                    }
                    else if (g.Session[ResultListHandler.SESSIONOBJ_GALLERYVIEW] != null)
                    {
                        galleryView = g.Session[ResultListHandler.SESSIONOBJ_GALLERYVIEW].ToString();
                        if (galleryView == String.Empty)    // just in case the session value is set to string.empty. this would wipe out our initial default value
                            galleryView = defaultView;
                    }

                }
                else
                    // if not postback, get the value of the GalleryView control
                    galleryView = HttpContext.Current.Request.Params[_control.HidGalleryView.UniqueID];
            }

            if (galleryView == "true")
                return Matchnet.Web.Framework.Ui.BasicElements.ResultsViewType.ListViewType.gallery;
            else
                return Matchnet.Web.Framework.Ui.BasicElements.ResultsViewType.ListViewType.list;
        }
    }
}
