﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.Configuration.ServiceAdapters;

namespace Matchnet.Web.Applications.QuickSearch
{
    public class QuickSearchPersistence
    {
        private int _memberID;
        private string _cookieName = "QS_{0}";
        private HttpCookie _persistCookie;
        private int _expirationDays;

        public int GenderMask { get; set; }
        public int MinAge { get; set; }
        public int MaxAge { get; set; }
        public int Distance { get; set; }
        public int RegionID { get; set; }
        public int HasPhotoFlag { get; set; }
        public int SchoolID { get; set; }
        public int SearchTypeID { get; set; }
        public int SearchOrderBy { get; set; }
        public int JdateReligion { get; set; }
        public int AreaCode1 { get; set; }
        public int AreaCode2 { get; set; }
        public int AreaCode3 { get; set; }
        public int AreaCode4 { get; set; }
        public int AreaCode5 { get; set; }
        public int AreaCode6 { get; set; }
        public int RamahAlum { get; set; }

        public int MemberID
        {
            get
            {
                return _memberID;
            }
        }

        public QuickSearchPersistence(int memberID, int expirationDays)
        {
            _memberID = memberID;
            string _name = string.Format(_cookieName, memberID.ToString());

            _persistCookie = HttpContext.Current.Request.Cookies[_name];

            if (_persistCookie == null)
            {
                _persistCookie = new HttpCookie(_name, _name);
                InitializeNullValues();
            }
            else
            {
                LoadValues();
            }

            _expirationDays = expirationDays;

            _persistCookie.Expires = DateTime.Now.AddDays(_expirationDays);
        }

        public void PersistValues()
        {
            _persistCookie["GenderMask"] = GenderMask.ToString();
            _persistCookie["MinAge"] = MinAge.ToString();
            _persistCookie["MaxAge"] = MaxAge.ToString();
            _persistCookie["Distance"] = Distance.ToString();
            _persistCookie["RegionID"] = RegionID.ToString();
            _persistCookie["HasPhotoFlag"] = HasPhotoFlag.ToString();
            _persistCookie["SchoolID"] = SchoolID.ToString();
            _persistCookie["SearchTypeID"] = SearchTypeID.ToString();
            _persistCookie["JdateReligion"] = JdateReligion.ToString();
            _persistCookie["AreaCode1"] = AreaCode1.ToString();
            _persistCookie["AreaCode2"] = AreaCode2.ToString();
            _persistCookie["AreaCode3"] = AreaCode3.ToString();
            _persistCookie["AreaCode4"] = AreaCode4.ToString();
            _persistCookie["AreaCode5"] = AreaCode5.ToString();
            _persistCookie["AreaCode6"] = AreaCode6.ToString();
            _persistCookie["SearchOrderBy"] = SearchOrderBy.ToString();
            _persistCookie["RamahAlum"] = RamahAlum.ToString();

            _persistCookie.Expires = DateTime.Now.AddDays(_expirationDays);
            HttpContext.Current.Response.Cookies.Add(_persistCookie);
        }

        public void InitializeNullValues()
        {
            GenderMask = Constants.NULL_INT;
            MinAge = Constants.NULL_INT;
            MaxAge = Constants.NULL_INT;
            Distance = Constants.NULL_INT;
            RegionID = Constants.NULL_INT;
            HasPhotoFlag = Constants.NULL_INT;
            SchoolID = Constants.NULL_INT;
            SearchTypeID = Constants.NULL_INT;
            SearchOrderBy = Constants.NULL_INT;
            JdateReligion = Constants.NULL_INT;
            AreaCode1 = Constants.NULL_INT;
            AreaCode2 = Constants.NULL_INT;
            AreaCode3 = Constants.NULL_INT;
            AreaCode4 = Constants.NULL_INT;
            AreaCode5 = Constants.NULL_INT;
            AreaCode6 = Constants.NULL_INT;
            RamahAlum = Constants.NULL_INT;
        }

        public string GetPersistedValue(string key)
        {
            return _persistCookie[key].ToString();
        }

        public void Clear()
        {
            if (_persistCookie != null)
                _persistCookie.Expires = DateTime.Now.AddYears(-30);
            HttpContext.Current.Response.Cookies.Add(_persistCookie);
        }

        private void LoadValues()
        {
            GenderMask = Convert.ToInt32(_persistCookie["GenderMask"]);
            MinAge = Convert.ToInt32(_persistCookie["MinAge"]);
            MaxAge = Convert.ToInt32(_persistCookie["MaxAge"]);
            Distance = Convert.ToInt32(_persistCookie["Distance"]);
            RegionID = Convert.ToInt32(_persistCookie["RegionID"]);
            HasPhotoFlag = Convert.ToInt32(_persistCookie["HasPhotoFlag"]);
            SchoolID = _persistCookie["SchoolID"] == string.Empty ? Constants.NULL_INT : Convert.ToInt32(_persistCookie["SchoolID"]);
            SearchTypeID = _persistCookie["SearchTypeID"] == string.Empty ? Constants.NULL_INT : Convert.ToInt32(_persistCookie["SearchTypeID"]);
            SearchOrderBy = _persistCookie["SearchOrderBy"] == string.Empty ? Constants.NULL_INT : Convert.ToInt32(_persistCookie["SearchOrderBy"]);
            JdateReligion = _persistCookie["JdateReligion"] == string.Empty ? Constants.NULL_INT : Convert.ToInt32(_persistCookie["JdateReligion"]);
            AreaCode1 = _persistCookie["AreaCode1"] == string.Empty ? Constants.NULL_INT : Convert.ToInt32(_persistCookie["AreaCode1"]);
            AreaCode2 = _persistCookie["AreaCode2"] == string.Empty ? Constants.NULL_INT : Convert.ToInt32(_persistCookie["AreaCode2"]);
            AreaCode3 = _persistCookie["AreaCode3"] == string.Empty ? Constants.NULL_INT : Convert.ToInt32(_persistCookie["AreaCode3"]);
            AreaCode4 = _persistCookie["AreaCode4"] == string.Empty ? Constants.NULL_INT : Convert.ToInt32(_persistCookie["AreaCode4"]);
            AreaCode5 = _persistCookie["AreaCode5"] == string.Empty ? Constants.NULL_INT : Convert.ToInt32(_persistCookie["AreaCode5"]);
            AreaCode6 = _persistCookie["AreaCode6"] == string.Empty ? Constants.NULL_INT : Convert.ToInt32(_persistCookie["AreaCode6"]);
            RamahAlum =  string.IsNullOrEmpty(_persistCookie["RamahAlum"]) ? Constants.NULL_INT : Convert.ToInt32(_persistCookie["RamahAlum"]);
        }


    }
}
