﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Web.Framework;
using System.Collections;
using Matchnet.Search.ValueObjects;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.Region;
using System.Data;
using Matchnet.Web.Framework.Util;
using Matchnet.Web.Framework.Ui.BasicElements;
using Matchnet.Web.Applications.Search;

namespace Matchnet.Web.Applications.AdvancedSearch
{
    public partial class AdvancedSearchResults : FrameworkControl
    {
        protected Int32 SearchOrderBy;

        ArrayList _sortSpans = new ArrayList();
        ArrayList _sortTitles = new ArrayList();
        protected string ChangeVoteMessage;
        protected PlaceHolder plcPromotionProfile;
        ArrayList _sortLinks = new ArrayList();

        private void Page_Init(object sender, EventArgs e)
        {
            try
            {
                if (!SettingsManager.GetSettingBool(SettingConstants.ADVSEARCH_ENABLE, g.Brand))
                    g.Transfer("/Applications/Home/Default.aspx");

                ChangeVoteMessage = g.GetResource("ARE_YOU_SURE_YOU_WANT_TO_CHANGE", this);
                SearchPreferences.Href = "/Applications/AdvancedSearch/AdvancedSearch.aspx";

                // check to see if the current user access the site has any search preferences specified,
                // if not then the user is redirected to SearchPrefs for search specifications, otherwise
                // the results of the search are displayed
                SearchPreferenceCollection searchPreferences = g.AdvancedSearchPreferences;

                // If there are no search preferences in the SearchPreference object or the regionID is invalid,
                // redirect to SearchPreferences so that the user can fill in search preferences.
                // We look for searchPreferences.Count <= 1 because sometimes there will be 1 searchPref (searchTypeID)
                // and that does not count as valid search preferences (see TT 13887).  Checking against a count of 0 was
                // causing problems.
                if (searchPreferences.Count <= 1 || (!ValidRegionID(searchPreferences["RegionID"]) && searchPreferences["SearchTypeID"] != ((Int32)SearchTypeID.AreaCode).ToString()))
                {
                    g.Transfer("/Applications/AdvancedSearch/AdvancedSearch.aspx");
                }

                //Check to see if a search order has been specified -- default to Most Popular if not.
                if (Request[WebConstants.URL_PARAMETER_NAME_SEARCHORDERBY] == null)
                {
                    if (SearchOrderBy <= 0)
                    {
                        SearchOrderBy = (Int32)Matchnet.Search.Interfaces.QuerySorting.JoinDate;
                    }
                }
                else
                {
                    SearchOrderBy = Convert.ToInt32(Request[WebConstants.URL_PARAMETER_NAME_SEARCHORDERBY]);
                }

                bool colorCodeEnabled = SettingsManager.GetSettingBool(SettingConstants.ENABLE_COLORCODE_SEARCH, g.Brand);
                if (SearchOrderBy == (Int32)Matchnet.Search.Interfaces.QuerySorting.ColorCode)
                {
                    if (colorCodeEnabled)
                    {
                        colorCodeSelect.Visible = true;
                        colorCodeSelect.ShowLink = true;
                    }
                    else
                    {
                        colorCodeSelect.Visible = false;
                        colorCodeSelect.ShowLink = false;
                        SearchOrderBy = (Int32)Matchnet.Search.Interfaces.QuerySorting.JoinDate;
                    }
                }
                else if (SearchOrderBy == (int)Matchnet.Search.Interfaces.QuerySorting.Popularity
                    && !SearchUtil.IsSearchSortByPopularityEnabled(g.Brand.BrandID, g.Brand.Site.SiteID, g.Brand.Site.Community.CommunityID))
                {
                    SearchOrderBy = (Int32)Matchnet.Search.Interfaces.QuerySorting.JoinDate;
                }

                if (SearchOrderBy == (int)Matchnet.Search.Interfaces.QuerySorting.KeywordRelevance)
                {
                    SearchOrderBy = (Int32)Matchnet.Search.Interfaces.QuerySorting.JoinDate;
                }

                g.AdvancedSearchPreferences["SearchOrderBy"] = SearchOrderBy.ToString();

                BindSortDropDown();

                // Wire up the List Navigation.
                g.ListNavigationTop = lblListNavigationTop;
                g.ListNavigationBottom = lblListNavigationBottom;

            }
            catch (Exception ex) { g.ProcessException(ex); }
        }

        private void Page_Load(object sender, System.EventArgs e)
        {
            RenderPage();
        }

        private void Page_PreRender(object sender, System.EventArgs e)
        {
            // PreRender defaults for any html/controls on the page

            // TT #15790 - specifically, set hidden field value back to false by default in all cases
            // ResultViewChanged.Value = "false";

            // Omniture page name override. This cannot be done in Omniture.ascx.cs because the gallery/list view mode is set here which gets executed after Omniture code
            if (Convert.ToBoolean(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ANALYTICS_OMNITURE", _g.Brand.Site.Community.CommunityID, _g.Brand.Site.SiteID)))
            {
                _g.AnalyticsOmniture.PageName = "Advanced Search Results - " + GetSearchOrderBy() + " " + idResultsViewType.GetSearchResultViewMode();

                #region Commented Out
                //// MPR-940 JewishSearch
                //int optionValue = Conversion.CInt(g.SearchPreferences["JdateReligion"], Constants.NULL_INT);

                //// Selected Any
                //if (optionValue == Constants.NULL_INT)
                //{
                //    _g.AnalyticsOmniture.Evar41 = "Standard";
                //    return;
                //}

                //if (((optionValue & 16384) == 16384) || //Willing to Convert
                //((optionValue & 2048) == 2048) || //Not Willing to Convert
                //((optionValue & 512) == 512)) //Not sure if I’m willing to convert
                //{
                //    _g.AnalyticsOmniture.Evar41 = "Standard";
                //}
                //else
                //{
                //    _g.AnalyticsOmniture.Evar41 = "Jewish Only";
                //}
                #endregion

            }
        }

        private void RenderPage()
        {
            try
            {

                idResultsViewType.GetSearchResultViewMode();
                SearchResultList.GalleryView = idResultsViewType.GalleryViewFlag;

                // showPromotionalProfile();

                // CAUTION: This is a hack to meet the campaign deadline for CBS
                // Show our promoDiv for female members only
                bool showCampaignDiv = Convert.ToBoolean(Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ENABLE_CAMPAIGN_DIV_DISPLAY", g.Brand.Site.Community.CommunityID,
                    g.Brand.Site.SiteID));
                if (showCampaignDiv)
                {
                    if (g.Member != null)
                    {
                        if ((g.Member.GetAttributeInt(g.Brand, "gendermask") & Matchnet.Lib.ConstantsTemp.GENDERID_FEMALE) == Matchnet.Lib.ConstantsTemp.GENDERID_FEMALE)
                            promoDiv.Visible = true;
                    }
                }

            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }
        
        private bool ValidRegionID(string RegionID)
        {
            if (RegionID != null && RegionID != string.Empty && Conversion.CInt(RegionID, Constants.NULL_INT) != Constants.NULL_INT)
            {
                return true;
            }
            return false;
        }

        private void GetAgeRangeFromID(int AgeRangeID, ref int MinAge, ref int MaxAge)
        {
            switch (AgeRangeID)
            {
                case 0:
                    MinAge = 18;
                    MaxAge = 24;
                    break;
                case 1:
                    MinAge = 25;
                    MaxAge = 34;
                    break;
                case 2:
                    MinAge = 35;
                    MaxAge = 44;
                    break;
                case 3:
                    MinAge = 45;
                    MaxAge = 54;
                    break;
                case 4:
                    MinAge = 55;
                    MaxAge = 99;
                    break;
                default:
                    MinAge = 18;
                    MaxAge = 99;
                    break;
            }
        }

        private void BindSortDropDown()
        {
            string key = "SearchOrderBy";
            DataTable cct = Option.GetOptions(key, g);

            if (SettingsManager.GetSettingBool(SettingConstants.ENABLE_COLORCODE_SEARCH, g.Brand))
            {
                DataRow row = cct.NewRow();
                row["Content"] = "Color Code";
                row["ListOrder"] = cct.Rows.Count + 1;
                row["Value"] = (int)Matchnet.Search.Interfaces.QuerySorting.ColorCode;
                cct.Rows.Add(row);
                colorCodeSelect.NavigateURL = "/Applications/AdvancedSearch/AdvancedSearchResults.aspx?SearchOrderBy=5";

            }
            // Proximity sorting option must be removed when searching by AreaCode or College.
            if (g.AdvancedSearchPreferences["SearchTypeID"] != null)
            {
                int iSearchTypeID = Matchnet.Conversion.CInt(g.AdvancedSearchPreferences["SearchTypeID"], Constants.NULL_INT);

                if ((SearchTypeID.AreaCode.Equals((SearchTypeID)iSearchTypeID))
                    || (SearchTypeID.College.Equals((SearchTypeID)iSearchTypeID)))
                {
                    int proximity = (int)Matchnet.Search.Interfaces.QuerySorting.Proximity;
                    DataRow proximitySortOption = null;

                    foreach (DataRow row in cct.Rows)
                    {
                        if (Conversion.CInt(row["Value"]) == proximity)
                        {
                            proximitySortOption = row;
                        }
                    }

                    if (proximitySortOption != null)
                    {
                        proximitySortOption.Delete();
                    }
                }
            }

            // Popularity sorting option removed when not enabled
            if (!SearchUtil.IsSearchSortByPopularityEnabled(g.Brand.BrandID, g.Brand.Site.SiteID, g.Brand.Site.Community.CommunityID))
            {
                DataRow popularitySortOption = null;
                foreach (DataRow row in cct.Rows)
                {
                    if (Conversion.CInt(row["Value"]) == (int)Matchnet.Search.Interfaces.QuerySorting.Popularity)
                    {
                        popularitySortOption = row;
                    }
                }

                if (popularitySortOption != null)
                {
                    popularitySortOption.Delete();
                }
            }

            rptSearchOption.DataSource = cct;
            rptSearchOption.DataBind();
        }

        private string GetSearchOrderBy()
        {
            string ret = string.Empty;
            string searchOrder = string.Empty;

            // check the request for change in SearchOrderBy
            if (Request["SearchOrderBy"] != null)
            {
                searchOrder = Request["SearchOrderBy"];
                _g.AdvancedSearchPreferences["SearchOrderBy"] = searchOrder;
            }
            else
                searchOrder = _g.AdvancedSearchPreferences["SearchOrderBy"];

            switch (searchOrder)
            {
                case "1":
                    ret = "Newest";
                    break;
                case "2":
                    ret = "Most Active";
                    break;
                case "3":
                    ret = "Closest To You";
                    break;
                case "4":
                    ret = "Most Popular";
                    break;
                case "5":
                    ret = "Color Code";
                    break;
            }

            return ret;
        }

        private void rptSearchOption_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            DataRowView row = (DataRowView)e.Item.DataItem;

            HyperLink lnkSort = (HyperLink)e.Item.FindControl("lnkSort");
            lnkSort.Text = row["Content"].ToString();
            lnkSort.NavigateUrl = "/Applications/AdvancedSearch/AdvancedSearchResults.aspx?" + WebConstants.URL_PARAMETER_NAME_SEARCHORDERBY + "=" + row["Value"].ToString();

            Literal litSortSpan = (Literal)e.Item.FindControl("litSortSpan");
            Literal litSortTitle = (Literal)e.Item.FindControl("litSortTitle");

            litSortTitle.Text = row["Content"].ToString();

            _sortSpans.Add(litSortSpan);
            _sortLinks.Add(lnkSort);
            _sortTitles.Add(litSortTitle);


            if (row["Value"].ToString() == SearchOrderBy.ToString())
            {
                //hidSearchOrderBy.Value = SearchOrderBy.ToString();
                litSortSpan.Text = "selected";
                lnkSort.Visible = false;
                litSortTitle.Visible = true;
            }
            else
            {
                litSortSpan.Text = "";
            }
        }

        //public bool showSpotlightProfile(System.Collections.Generic.List<int> searchIDs)
        //{
        //    bool result = false; ;
        //    try
        //    {

        //        bool promoMemberFlag = false;
        //        Matchnet.Web.Applications.PremiumServices.VelocityHandler.DebugTrace("SearchResults", "Start retrieving spotlight profile", g);
        //        Matchnet.Member.ServiceAdapters.Member m = FrameworkGlobals.GetSpotlightProfile(g, searchIDs, out promoMemberFlag);
        //        Matchnet.Web.Applications.PremiumServices.VelocityHandler.DebugTrace("SearchResults", "End retrieving spotlight profile", g);
        //        if (m != null)
        //        {
        //            Matchnet.Web.Applications.PremiumServices.VelocityHandler.DebugTrace("SearchResults", "Spotlight profile=" + m.MemberID, g);
        //            plcPromotionalProfile.Visible = true;
        //            // PromotionProfile.Visible = false;
        //            MiniProfile20 pr = (MiniProfile20)LoadControl("/Framework/Ui/BasicElements/MiniProfile20.ascx");
        //            pr.Member = m;
        //            pr.IsSpotlight = true;
        //            pr.IsHighlighted = false;
        //            pr.IsPromotionalMember = promoMemberFlag;
        //            pr.OnlineImageName = "icon-status-online.gif";
        //            pr.OfflineImageName = "icon-status-offline.gif";
        //            plcPromotionalProfile.Controls.Add(pr);
        //            result = true;
        //        }


        //        int count = SearchResultList.TargetMemberIDs.Count;
        //        return result;
        //    }
        //    catch (Exception ex) { return result; }
        //}

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rptSearchOption.ItemDataBound += new System.Web.UI.WebControls.RepeaterItemEventHandler(this.rptSearchOption_ItemDataBound);
            this.Load += new System.EventHandler(this.Page_Load);
            this.Init += new System.EventHandler(this.Page_Init);
            this.PreRender += new System.EventHandler(this.Page_PreRender);

        }
        #endregion
    }
}