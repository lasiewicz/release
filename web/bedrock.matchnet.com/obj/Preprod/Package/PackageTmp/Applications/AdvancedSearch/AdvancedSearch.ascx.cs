﻿using System;
using System.Collections;
using System.Data;
using System.Web.UI.WebControls;
using System.Text;
using System.Xml;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Ui.FormElements;
using Matchnet.Web.Framework.Util;
using Matchnet.Web.Applications.Search;

using Spark.SAL;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.MemberSlideshow.ServiceAdapters;
using Matchnet.Session.ServiceAdapters;
using Matchnet.Session.ValueObjects;
using SearchVO = Matchnet.Search.ValueObjects;
using Matchnet.Search.Interfaces;


namespace Matchnet.Web.Applications.AdvancedSearch
{
    public partial class AdvancedSearch : FrameworkControl
    {
        #region Protected UI members
        protected Matchnet.Web.Framework.Title ttlPreferences;
        protected DropDownList ddlGender;
        protected DropDownList ddlSeekingGender;
        protected TextBox tbAgeMin;
        protected TextBox tbAgeMax;
        protected Anthem.PlaceHolder plcDistance;
        protected Anthem.PlaceHolder plcAreaCodes;
        protected Anthem.PlaceHolder plcCollege;
        protected DropDownList ddlDistance;
        protected Anthem.HyperLink lnkLocation;
        protected CheckBox cbHasPhoto;
        protected SearchPreferenceRepeater rptBasics;
        protected Repeater rptAdditionalSections;
        protected Button btnSave;
        protected Anthem.PlaceHolder plcPickRegion;
        protected PickRegionNew prSearchRegion;
        protected Matchnet.Web.Framework.MultiValidator AgeMinValidator;
        protected Matchnet.Web.Framework.MultiValidator AgeMaxValidator;
        protected MemberSearch _memberSearch;
        #endregion

        private const int COOKIE_EXPIRATION_DAYS = 365;

        private void Page_Init(object sender, EventArgs e)
        {
            try
            {
                if (!SettingsManager.GetSettingBool(SettingConstants.ADVSEARCH_ENABLE, g.Brand))
                    g.Transfer("/Applications/Home/Default.aspx");
            }
            catch (Exception ex) { g.ProcessException(ex); }
        }


        private void Page_Load(object sender, System.EventArgs e)
        {
            try
            {
                //Load advanced search data
                _memberSearch = LoadMemberSearch();

                //Bind data to controls
                bindGender();
                bindAge();
                bindDistance();
                bindLocation();
                bindHasPhoto();
                bindXMLPreferences();
                BindRamahDate();

                //keyword
                if (SearchUtil.IsKeywordMatchesSearchPrefEnabled(g.Brand.BrandID, g.Brand.Site.SiteID, g.Brand.Site.Community.CommunityID))
                {
                    phKeyword.Visible = true;
                    if (!string.IsNullOrEmpty(_memberSearch.KeywordSearch) && _memberSearch.KeywordSearch.Trim() != "")
                    {
                        txtKeyword.Text = _memberSearch.KeywordSearch;
                    }
                }

                btnSave.Text = g.GetResource("BTN_SAVE_PREFERENCES", this);
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        private MemberSearch LoadMemberSearch()
        { 
            MemberSearch memberSearch = MemberSearch.GetDefaultSearch(g.Brand);

            //we want language mask to be defaulted to "any"
            MemberSearchPreferenceMask languageMaskPref = memberSearch[Preference.GetInstance("LanguageMask")] as MemberSearchPreferenceMask;
            languageMaskPref.Value = Constants.NULL_INT;

            // If we don't set his memberID to null_int, any .Save() call will overwrite the match preferences.
            // Setting the MemberID to null_int makes it so that it's saved to the session.  We don't even want this
            // because QuickSearch uses this session storage space for its preferences.  For AdvancedSearch's case,
            // we are only using the MemberSearch class to hold data.  .Save() shouldn't be called ever.
            memberSearch.MemberID = Constants.NULL_INT;

            // run it through the cookie
            if (!LoadMemberSearchFromPersistence(memberSearch))
            {
                LoadMemberSearchFromMatchSearchPreferences(memberSearch);
            }

            return memberSearch;
        }

        /// <summary>
        /// Load the basic search criteria from the match preferences
        /// </summary>
        /// <param name="search"></param>
        private void LoadMemberSearchFromMatchSearchPreferences(MemberSearch search)
        {
            if (g.SearchPreferences.ContainsKey("gendermask"))
            {
                search.Gender = Spark.SAL.GenderUtils.GetGender(Conversion.CInt(g.SearchPreferences["gendermask"]));
                search.SeekingGender = Spark.SAL.GenderUtils.GetSeekingGender(Conversion.CInt(g.SearchPreferences["gendermask"]));
            }

            if (g.SearchPreferences.ContainsKey("minage"))
                search.Age.MinValue = Conversion.CInt(g.SearchPreferences["minage"]);

            if (g.SearchPreferences.ContainsKey("maxage"))
                search.Age.MaxValue = Conversion.CInt(g.SearchPreferences["maxage"]);

            if (g.SearchPreferences.ContainsKey("distance"))
                search.Distance = Conversion.CInt(g.SearchPreferences["distance"]);

            if (g.SearchPreferences.ContainsKey("regionid"))
                search.RegionID = Conversion.CInt(g.SearchPreferences["regionid"]);
        }

        /// <summary>
        /// Loads MemberSearch from the cookie
        /// </summary>
        /// <param name="search">MemberSearch object to load to</param>
        /// <returns>True if MemberSearch was loaded from the cookie, False if not.</returns>
        private bool LoadMemberSearchFromPersistence(MemberSearch search)
        {
            int memberID = Constants.NULL_INT;
            if (g.Member != null) memberID = g.Member.MemberID;

            AdvancedSearchPersistence persistence = new AdvancedSearchPersistence(memberID, GetCookieExpirationDuration());

            if (persistence.GenderMask == Constants.NULL_INT || persistence.RegionID == Constants.NULL_INT ||
                persistence.MinAge == Constants.NULL_INT || persistence.MaxAge == Constants.NULL_INT || persistence.Distance == Constants.NULL_INT)
                return false;

            search.Gender = Spark.SAL.GenderUtils.GetGender(persistence.GenderMask);
            search.SeekingGender = Spark.SAL.GenderUtils.GetSeekingGender(persistence.GenderMask);
            search.Age.MinValue = persistence.MinAge;
            search.Age.MaxValue = persistence.MaxAge;
            search.Distance = persistence.Distance;
            search.RegionID = persistence.RegionID;

            #region Optional search parameters
            if (persistence.HasPhotoFlag != Constants.NULL_INT)
                search.HasPhoto = Convert.ToBoolean(persistence.HasPhotoFlag);
            if (persistence.SchoolID != Constants.NULL_INT)
                search.SchoolID = persistence.SchoolID;
            if (persistence.SearchTypeID != Constants.NULL_INT)
                search.SearchType = (Spark.SAL.SearchType)persistence.SearchTypeID;
            if (persistence.AreaCode1 != Constants.NULL_INT)
            {
                MemberSearchPreferenceInt areaCodePreference1 = search[Preference.GetInstance("AreaCode1")] as MemberSearchPreferenceInt;
                areaCodePreference1.Value = persistence.AreaCode1;
            }
            if (persistence.AreaCode2 != Constants.NULL_INT)
            {
                MemberSearchPreferenceInt areaCodePreference2 = search[Preference.GetInstance("AreaCode2")] as MemberSearchPreferenceInt;
                areaCodePreference2.Value = persistence.AreaCode2;
            }
            if (persistence.AreaCode1 != Constants.NULL_INT)
            {
                MemberSearchPreferenceInt areaCodePreference3 = search[Preference.GetInstance("AreaCode3")] as MemberSearchPreferenceInt;
                areaCodePreference3.Value = persistence.AreaCode3;
            }
            if (persistence.AreaCode4 != Constants.NULL_INT)
            {
                MemberSearchPreferenceInt areaCodePreference4 = search[Preference.GetInstance("AreaCode4")] as MemberSearchPreferenceInt;
                areaCodePreference4.Value = persistence.AreaCode4;
            }
            if (persistence.AreaCode5 != Constants.NULL_INT)
            {
                MemberSearchPreferenceInt areaCodePreference5 = search[Preference.GetInstance("AreaCode5")] as MemberSearchPreferenceInt;
                areaCodePreference5.Value = persistence.AreaCode5;
            }
            if (persistence.AreaCode6 != Constants.NULL_INT)
            {
                MemberSearchPreferenceInt areaCodePreference6 = search[Preference.GetInstance("AreaCode6")] as MemberSearchPreferenceInt;
                areaCodePreference6.Value = persistence.AreaCode6;
            }
            if (persistence.JdateReligion != Constants.NULL_INT)
            {
                MemberSearchPreference jdatereligionPreference = search[Preference.GetInstance("jdatereligion")];
                (jdatereligionPreference as MemberSearchPreferenceMask).Value = persistence.JdateReligion;
            }
            if (persistence.MinHeight != Constants.NULL_INT)
            {
                MemberSearchPreferenceRange heightPref = search[Preference.GetInstance("Height")] as MemberSearchPreferenceRange;
                heightPref.MinValue = persistence.MinHeight;
            }
            if (persistence.MaxHeight != Constants.NULL_INT)
            {
                MemberSearchPreferenceRange heightPref = search[Preference.GetInstance("Height")] as MemberSearchPreferenceRange;
                heightPref.MaxValue = persistence.MaxHeight;
            }
            if (persistence.Ethnicity != Constants.NULL_INT)
            {
                MemberSearchPreferenceMask ethnicityPref = search[Preference.GetInstance("Ethnicity")] as MemberSearchPreferenceMask;
                ethnicityPref.Value = persistence.Ethnicity;
            }
            if (persistence.EducationLevel != Constants.NULL_INT)
            {
                MemberSearchPreferenceMask educationLevelPref = search[Preference.GetInstance("EducationLevel")] as MemberSearchPreferenceMask;
                educationLevelPref.Value = persistence.EducationLevel;
            }
            if (persistence.MaritalStatus != Constants.NULL_INT)
            {
                MemberSearchPreferenceMask maritalStatusPref = search[Preference.GetInstance("MaritalStatus")] as MemberSearchPreferenceMask;
                maritalStatusPref.Value = persistence.MaritalStatus;
            }
            if (persistence.BodyType != Constants.NULL_INT)
            {
                MemberSearchPreferenceMask bodyTypePref = search[Preference.GetInstance("BodyType")] as MemberSearchPreferenceMask;
                bodyTypePref.Value = persistence.BodyType;
            }
            if (persistence.SmokingHabits != Constants.NULL_INT)
            {
                MemberSearchPreferenceMask smokingHabitPref = search[Preference.GetInstance("SmokingHabits")] as MemberSearchPreferenceMask;
                smokingHabitPref.Value = persistence.SmokingHabits;
            }
            if (persistence.DrinkingHabits != Constants.NULL_INT)
            {
                MemberSearchPreferenceMask drinkingHabitPref = search[Preference.GetInstance("DrinkingHabits")] as MemberSearchPreferenceMask;
                drinkingHabitPref.Value = persistence.DrinkingHabits;
            }
            if (persistence.Religion != Constants.NULL_INT)
            {
                MemberSearchPreferenceMask religionPref = search[Preference.GetInstance("Religion")] as MemberSearchPreferenceMask;
                religionPref.Value = persistence.Religion;
            }
            if (persistence.JDateEthnicity != Constants.NULL_INT)
            {
                MemberSearchPreferenceMask pref = search[Preference.GetInstance("JDateEthnicity")] as MemberSearchPreferenceMask;
                pref.Value = persistence.JDateEthnicity;
            }
            if (persistence.SynagogueAttendance != Constants.NULL_INT)
            {
                MemberSearchPreferenceMask pref = search[Preference.GetInstance("SynagogueAttendance")] as MemberSearchPreferenceMask;
                pref.Value = persistence.SynagogueAttendance;
            }
            if (persistence.KeepKosher != Constants.NULL_INT)
            {
                MemberSearchPreferenceMask pref = search[Preference.GetInstance("KeepKosher")] as MemberSearchPreferenceMask;
                pref.Value = persistence.KeepKosher;
            }
            if (persistence.SearchOrderBy != Constants.NULL_INT)
            {
                MemberSearchPreferenceInt pref = search[Preference.GetInstance("SearchOrderBy")] as MemberSearchPreferenceInt;
                pref.Value = persistence.SearchOrderBy;
            }

            if (!string.IsNullOrEmpty(g.AdvancedSearchPreferences["SearchOrderBy"]))
            {
                MemberSearchPreferenceInt pref = search[Preference.GetInstance("SearchOrderBy")] as MemberSearchPreferenceInt;
                pref.Value = Convert.ToInt32(g.AdvancedSearchPreferences["SearchOrderBy"]);
            }

            if (persistence.MoreChildrenFlag != Constants.NULL_INT)
            {
                MemberSearchPreferenceMask pref = search[Preference.GetInstance("MoreChildrenFlag")] as MemberSearchPreferenceMask;
                pref.Value = persistence.MoreChildrenFlag;
            }
            if (persistence.Custody != Constants.NULL_INT)
            {
                MemberSearchPreferenceMask pref = search[Preference.GetInstance("Custody")] as MemberSearchPreferenceMask;
                pref.Value = persistence.Custody;
            }
            if (persistence.ActivityLevel != Constants.NULL_INT)
            {
                MemberSearchPreferenceMask pref = search[Preference.GetInstance("ActivityLevel")] as MemberSearchPreferenceMask;
                pref.Value = persistence.ActivityLevel;
            }

            //do not check for null_int on language mask so that it can persist "any" since default search selects brand language
            MemberSearchPreferenceMask languageMaskPref = search[Preference.GetInstance("LanguageMask")] as MemberSearchPreferenceMask;
            languageMaskPref.Value = persistence.LanguageMask;

            if (persistence.RelocateFlag != Constants.NULL_INT)
            {
                MemberSearchPreferenceMask pref = search[Preference.GetInstance("RelocateFlag")] as MemberSearchPreferenceMask;
                pref.Value = persistence.RelocateFlag;
            }
            if (persistence.ChildrenCount != Constants.NULL_INT)
            {
                MemberSearchPreferenceMask pref = search[Preference.GetInstance("ChildrenCount")] as MemberSearchPreferenceMask;
                pref.Value = persistence.ChildrenCount;
            }

            if (persistence.SearchOrderBy != Constants.NULL_INT)
            {
                MemberSearchPreferenceInt pref = search[Preference.GetInstance("RamahAlum")] as MemberSearchPreferenceInt;
                pref.Value = persistence.RamahAlum;
            }

            if (!string.IsNullOrEmpty(persistence.KeywordSearch))
            {
                search.KeywordSearch = persistence.KeywordSearch;
            }

            return true;

            #endregion

        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (prSearchRegion.RegionName.Length > 0)
            {
                LocationValidator.IsRequired = false;
            }
            else
            {
                LocationValidator.IsRequired = true;
            }

            Page.Validate();

            if (Page.IsValid)
            {
                _memberSearch.Gender = (Gender)Enum.Parse(typeof(Gender), ddlGender.SelectedValue);
                _memberSearch.SeekingGender = (Gender)Enum.Parse(typeof(Gender), ddlSeekingGender.SelectedValue);

                Int32 minAge = Conversion.CInt(tbAgeMin.Text);
                Int32 maxAge = Conversion.CInt(tbAgeMax.Text);

                if (minAge > maxAge)
                {
                    tbAgeMin.Text = tbAgeMax.Text;
                    tbAgeMax.Text = minAge.ToString();

                    minAge = maxAge;
                    maxAge = Conversion.CInt(tbAgeMax.Text);
                }

                _memberSearch.Age.MinValue = minAge;
                _memberSearch.Age.MaxValue = maxAge;

                _memberSearch.Distance = Conversion.CInt(ddlDistance.SelectedValue);

                // if this is a photo required site, we want to set this search pref manually
                if (Convert.ToBoolean(RuntimeSettings.GetSetting("PHOTO_REQUIRED_SITE", g.Brand.Site.Community.CommunityID,
                    g.Brand.Site.SiteID, g.Brand.BrandID)))
                {
                    _memberSearch.HasPhoto = true;
                }
                else
                {
                    _memberSearch.HasPhoto = cbHasPhoto.Checked;
                }

                if (g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.JDate)
                {
                    if (cbxRamahDate.Checked)
                        (_memberSearch[Preference.GetInstance("RamahAlum")] as MemberSearchPreferenceInt).Value = 1;
                    else
                        (_memberSearch[Preference.GetInstance("RamahAlum")] as MemberSearchPreferenceInt).Value = Constants.NULL_INT;                    
               }

                string oldKeyword = _memberSearch.KeywordSearch;
                _memberSearch.KeywordSearch = "";
                if (SearchUtil.IsKeywordMatchesSearchPrefEnabled(g.Brand.BrandID, g.Brand.Site.SiteID, g.Brand.Site.Community.CommunityID))
                {
                    if (g.GetResource("TXT_KEYWORD_EXAMPLE", this.txtKeyword).ToLower() != txtKeyword.Text.Trim())
                    {
                        _memberSearch.KeywordSearch = txtKeyword.Text.Trim();
                        if (_memberSearch.KeywordSearch.Length > SearchUtil.KEYWORD_SEARCH_MAXCHAR_LENGTH)
                        {
                            _memberSearch.KeywordSearch = _memberSearch.KeywordSearch.Substring(0, SearchUtil.KEYWORD_SEARCH_MAXCHAR_LENGTH);
                        }

                        //if removed keywords, change sort to newest
                        if (string.IsNullOrEmpty(_memberSearch.KeywordSearch))
                        {
                            MemberSearchPreferenceInt sortByPref = _memberSearch[Preference.GetInstance("searchorderby")] as MemberSearchPreferenceInt;
                            sortByPref.Value = Convert.ToInt32(QuerySorting.JoinDate);
                        }
                        //if changing keyword values, default sort to keyword relevance
                        else if (oldKeyword != _memberSearch.KeywordSearch)
                        {
                            MemberSearchPreferenceInt sortByPref = _memberSearch[Preference.GetInstance("searchorderby")] as MemberSearchPreferenceInt;
                            sortByPref.Value = Convert.ToInt32(QuerySorting.KeywordRelevance);
                        }
                    }
                }
                
                SaveAdvancedSearch(_memberSearch);

                try
                {
                    bool enableMemberSlideshow = Conversion.CBool(RuntimeSettings.GetSetting("ENABLE_SLIDESHOW", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID));
                    if (enableMemberSlideshow)
                    {
                        MemberSlideshowSA.Instance.RemoveFromCache(g.Member.MemberID, g.Brand.Site.Community.CommunityID);
                    }
                }
                catch (Exception ex)
                { g.ProcessException(ex); }
                g.Transfer("/Applications/AdvancedSearch/AdvancedSearchResults.aspx");
            }
        }

        private void SaveAdvancedSearch(MemberSearch search)
        {
            // Save both to session and cookies
            PersistAdvSearchValues(search);
            g.Session.Add(WebConstants.SESSION_ADV_SEARCH, GetSearchPreferenceCollection(search), SessionPropertyLifetime.TemporaryDurable);            
        }

        private SearchVO.SearchPreferenceCollection GetSearchPreferenceCollection(MemberSearch memberSearch)
        {
            SearchVO.SearchPreferenceCollection searchPreferenceCollection = new SearchVO.SearchPreferenceCollection();

            foreach (Preference Preference in memberSearch.SearchPreferences)
            {
                string prefName = Preference.Attribute.Name;

                switch (Preference.PreferenceType)
                {
                    case PreferenceType.Int:
                    case PreferenceType.Mask:
                        MemberSearchPreferenceInt prefInt = (MemberSearchPreferenceInt)memberSearch[Preference];
                        if (prefInt.Value != Constants.NULL_INT && prefInt.Value != 0)
                        {
                            searchPreferenceCollection.Add(prefName, prefInt.Value.ToString());
                        }
                        break;
                    case PreferenceType.Range:
                        if (prefName.ToLower() == "height")
                        {
                            MemberSearchPreferenceRange prefRange = (MemberSearchPreferenceRange)memberSearch[Preference];
                            searchPreferenceCollection.Add("Min" + prefName, prefRange.MinValue.ToString());
                            searchPreferenceCollection.Add("Max" + prefName, prefRange.MaxValue.ToString());
                        }
                        break;
                    case PreferenceType.Text:
                        MemberSearchPreferenceText prefText = (MemberSearchPreferenceText)memberSearch[Preference];
                        searchPreferenceCollection.Add(prefName, prefText.Value);
                        break;
                }
            }

            MemberSearchPreferenceRange agePref = memberSearch[Spark.SAL.Preference.GetInstance("Birthdate")] as MemberSearchPreferenceRange;
            searchPreferenceCollection.Add("MinAge", agePref.MinValue.ToString());
            searchPreferenceCollection.Add("MaxAge", agePref.MaxValue.ToString());

            return searchPreferenceCollection;
        }

        private void PersistAdvSearchValues(MemberSearch memberSearch)
        {
            int memberID = Constants.NULL_INT;
            if (g.Member != null) memberID = g.Member.MemberID;

            AdvancedSearchPersistence persistence = new AdvancedSearchPersistence(memberID, GetCookieExpirationDuration());

            foreach (Preference Preference in memberSearch.SearchPreferences)
            {
                string prefName = Preference.Attribute.Name;
                switch (prefName.ToLower())
                {
                    case "birthdate":
                        MemberSearchPreferenceRange prefRange = (MemberSearchPreferenceRange)memberSearch[Preference];
                        persistence.MinAge = prefRange.MinValue;
                        persistence.MaxAge = prefRange.MaxValue;
                        break;
                    case "gendermask":
                        persistence.GenderMask = ((MemberSearchPreferenceInt)memberSearch[Preference]).Value;
                        break;
                    case "distance":
                        persistence.Distance = ((MemberSearchPreferenceInt)memberSearch[Preference]).Value;
                        break;
                    case "regionid":
                        persistence.RegionID = ((MemberSearchPreferenceInt)memberSearch[Preference]).Value;
                        break;
                    case "hasphotoflag":
                        persistence.HasPhotoFlag = ((MemberSearchPreferenceInt)memberSearch[Preference]).Value;
                        break;
                    case "schoolid":
                        persistence.SchoolID = ((MemberSearchPreferenceInt)memberSearch[Preference]).Value;
                        break;
                    case "searchtypeid":
                        persistence.SearchTypeID = ((MemberSearchPreferenceInt)memberSearch[Preference]).Value;
                        break;
                    case "jdatereligion":
                        persistence.JdateReligion = ((MemberSearchPreferenceInt)memberSearch[Preference]).Value;
                        break;
                    case "areacode1":
                        persistence.AreaCode1 = ((MemberSearchPreferenceInt)memberSearch[Preference]).Value;
                        break;
                    case "areacode2":
                        persistence.AreaCode2 = ((MemberSearchPreferenceInt)memberSearch[Preference]).Value;
                        break;
                    case "areacode3":
                        persistence.AreaCode3 = ((MemberSearchPreferenceInt)memberSearch[Preference]).Value; 
                        break;
                    case "areacode4":
                        persistence.AreaCode4 = ((MemberSearchPreferenceInt)memberSearch[Preference]).Value;
                        break;
                    case "areacode5":
                        persistence.AreaCode5 = ((MemberSearchPreferenceInt)memberSearch[Preference]).Value;
                        break;
                    case "areacode6":
                        persistence.AreaCode6 = ((MemberSearchPreferenceInt)memberSearch[Preference]).Value;
                        break;
                    case "height":
                        persistence.MinHeight = ((MemberSearchPreferenceRange)memberSearch[Preference]).MinValue;
                        persistence.MaxHeight = ((MemberSearchPreferenceRange)memberSearch[Preference]).MaxValue;
                        break;
                    case "ethnicity":
                        persistence.Ethnicity = CheckForAnyOptionSelected((MemberSearchPreferenceInt)memberSearch[Preference]);
                        break;
                    case "educationlevel":
                        persistence.EducationLevel = CheckForAnyOptionSelected((MemberSearchPreferenceInt)memberSearch[Preference]);
                        break;
                    case "maritalstatus":
                        persistence.MaritalStatus = CheckForAnyOptionSelected((MemberSearchPreferenceInt)memberSearch[Preference]);
                        break;
                    case "bodytype":
                        persistence.BodyType = CheckForAnyOptionSelected((MemberSearchPreferenceInt)memberSearch[Preference]);
                        break;
                    case "smokinghabits":
                        persistence.SmokingHabits = CheckForAnyOptionSelected((MemberSearchPreferenceInt)memberSearch[Preference]);
                        break;
                    case "drinkinghabits":
                        persistence.DrinkingHabits = CheckForAnyOptionSelected((MemberSearchPreferenceInt)memberSearch[Preference]);
                        break;
                    case "religion":
                        persistence.Religion = CheckForAnyOptionSelected((MemberSearchPreferenceInt)memberSearch[Preference]);
                        break;
                    case "jdateethnicity":
                        persistence.JDateEthnicity = CheckForAnyOptionSelected((MemberSearchPreferenceInt)memberSearch[Preference]);
                        break;
                    case "synagogueattendance":
                        persistence.SynagogueAttendance = CheckForAnyOptionSelected((MemberSearchPreferenceInt)memberSearch[Preference]);
                        break;
                    case "keepkosher":
                        persistence.KeepKosher = CheckForAnyOptionSelected((MemberSearchPreferenceInt)memberSearch[Preference]);
                        break;
                    case "searchorderby":
                        persistence.SearchOrderBy = ((MemberSearchPreferenceInt)memberSearch[Preference]).Value;
                        break;
                    case "morechildrenflag":
                        persistence.MoreChildrenFlag = CheckForAnyOptionSelected((MemberSearchPreferenceInt)memberSearch[Preference]);
                        break;
                    case "custody":
                        persistence.Custody = CheckForAnyOptionSelected((MemberSearchPreferenceInt)memberSearch[Preference]);
                        break;
                    case "activitylevel":
                        persistence.ActivityLevel = CheckForAnyOptionSelected((MemberSearchPreferenceInt)memberSearch[Preference]);
                        break;
                    case "languagemask":
                        persistence.LanguageMask = CheckForAnyOptionSelected((MemberSearchPreferenceInt)memberSearch[Preference]);
                        break;
                    case "childrencount":
                        persistence.ChildrenCount = CheckForAnyOptionSelected((MemberSearchPreferenceInt)memberSearch[Preference]);
                        break;
                    case "relocateflag":
                        persistence.RelocateFlag = CheckForAnyOptionSelected((MemberSearchPreferenceInt)memberSearch[Preference]);
                        break;
                    case "keywordsearch":
                        persistence.KeywordSearch = memberSearch.KeywordSearch;
                        break;
                    case "ramahalum":
                        persistence.RamahAlum = ((MemberSearchPreferenceInt)memberSearch[Preference]).Value;
                        break;
                }
            }

            persistence.PersistValues();
        }

        private int CheckForAnyOptionSelected(MemberSearchPreferenceInt msPrefInt)
        {
            if (msPrefInt.Value == 0)
                return Constants.NULL_INT;

            return msPrefInt.Value;
        }

        private int GetCookieExpirationDuration()
        {
            int days = COOKIE_EXPIRATION_DAYS;

            try
            {
                days = Conversion.CInt(Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ADVSEARCH_CRITERIA_EXPIRATION", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID), COOKIE_EXPIRATION_DAYS);
                return days;
            }
            catch (Exception ex)
            {
                return days;
            }      
        }

        private void bindGender()
        {
            ListItem itemMale = new ListItem(g.GetResource("MALE"), ((Int32)Gender.Male).ToString());
            ListItem itemFemale = new ListItem(g.GetResource("FEMALE"), ((Int32)Gender.Female).ToString());

            ddlGender.Items.Add(itemMale);
            ddlGender.Items.Add(itemFemale);

            itemMale = new ListItem(g.GetResource("MALE"), ((Int32)Gender.Male).ToString());
            itemFemale = new ListItem(g.GetResource("FEMALE"), ((Int32)Gender.Female).ToString());

            ddlSeekingGender.Items.Add(itemMale);
            ddlSeekingGender.Items.Add(itemFemale);


            /*fixing possibility of wrong selectvalue passed 
             * ddlGender.SelectedValue = ((Int32) _memberSearch.Gender).ToString();
             *ddlSeekingGender.SelectedValue = ((Int32) _memberSearch.SeekingGender).ToString();
            */
            FrameworkGlobals.SelectItem(ddlGender, ((Int32)_memberSearch.Gender).ToString());
            FrameworkGlobals.SelectItem(ddlSeekingGender, ((Int32)_memberSearch.SeekingGender).ToString());

        }

        private void bindAge()
        {
            tbAgeMin.Text = _memberSearch.Age.MinValue.ToString();
            tbAgeMax.Text = _memberSearch.Age.MaxValue.ToString();
        }

        private void bindDistance()
        {
            DataTable distanceOptions = Option.GetOptions(SearchUtil.GetDistanceAttribute(g), g);

            ddlDistance.DataSource = distanceOptions;
            ddlDistance.DataTextField = "Content";
            ddlDistance.DataValueField = "Value";
            ddlDistance.DataBind();

            FrameworkGlobals.SelectItem(ddlDistance, _memberSearch.Distance.ToString(), SettingsManager.GetSettingInt(SettingConstants.DEFAULT_DISTANCE_LIST_ORDER, g.Brand) - 1);
            //ddlDistance.SelectedValue = _memberSearch.Distance.ToString();
        }

        private void bindLocation()
        {
            #region Bind PickRegion Control
            if (!IsPostBack)
            {
                if (g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.JDateFR ||
                    g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.JDateUK)
                {
                    // Initially select the Region tab when opening the Search
                    // Region popup in JDate.fr and JDate.co.uk
                    prSearchRegion.SearchType = Spark.SAL.SearchType.Region;
                }
                else
                {
                    prSearchRegion.SearchType = _memberSearch.SearchType;
                }
            }

            prSearchRegion.RegionID = _memberSearch.RegionID;
            prSearchRegion.SchoolID = _memberSearch.SchoolID;

            Int32 countryRegionID = 0;

            //TODO: abstract area code stuff into the SAL.MemberSearch object
            StringBuilder sbAreaCodes = new StringBuilder();
            for (Int32 i = 1; i <= 6; i++)
            {
                MemberSearchPreferenceInt areaCode = _memberSearch[Preference.GetInstance("AreaCode" + i)] as MemberSearchPreferenceInt;
                if (areaCode != null && Conversion.CInt(areaCode.Value) > 0)
                {
                    countryRegionID = Matchnet.Content.ServiceAdapters.RegionSA.Instance.RetrieveAreaCodes()[areaCode.Value];
                    prSearchRegion.AddAreaCode(areaCode.Value.ToString(), countryRegionID);
                    sbAreaCodes.Append(areaCode.Value.ToString() + ", ");
                }
            }
            if (sbAreaCodes.Length > 0)
            {
                sbAreaCodes.Remove(sbAreaCodes.Length - 2, 2);
            }

            prSearchRegion.AreaCodeCountryRegionID = countryRegionID;
            #endregion

            lnkLocation.UpdateAfterCallBack = true;
            lnkLocation.Attributes.Add("onclick", "popUpDiv(\"searchPopupDiv\",\"16.67em\",\"-3px\");return false");
            lnkLocation.Attributes.Add("href", "#");

            plcDistance.Visible = true;
            plcDistance.UpdateAfterCallBack = true;
            plcAreaCodes.Visible = false;
            plcAreaCodes.UpdateAfterCallBack = true;
            plcCollege.Visible = false;
            plcCollege.UpdateAfterCallBack = true;

            switch (_memberSearch.SearchType)
            {
                case Spark.SAL.SearchType.PostalCode:
                case Spark.SAL.SearchType.Region:
                    lnkLocation.Text = FrameworkGlobals.GetRegionString(_memberSearch.RegionID, g.Brand.Site.LanguageID);
                    break;
                case Spark.SAL.SearchType.AreaCode:
                    plcDistance.Visible = false;
                    plcAreaCodes.Visible = true;
                    lnkLocation.Text = sbAreaCodes.ToString();
                    break;
                case Spark.SAL.SearchType.College:
                    plcDistance.Visible = false;
                    plcCollege.Visible = true;
                    MemberSearchPreferenceInt schoolIDPreference = _memberSearch[Preference.GetInstance("SchoolID")] as MemberSearchPreferenceInt;
                    lnkLocation.Text = Matchnet.Content.ServiceAdapters.RegionSA.Instance.RetrieveSchoolName(schoolIDPreference.Value).SchoolName;
                    break;
            }

            prSearchRegion.RegionName = lnkLocation.Text;
            lnkLocation.Text += g.GetResource("EDIT", this);
        }

        private void bindHasPhoto()
        {
            // if this is a photo required site, it doesn't make sense to offer this option
            if (Convert.ToBoolean(RuntimeSettings.GetSetting("PHOTO_REQUIRED_SITE", g.Brand.Site.Community.CommunityID,
                    g.Brand.Site.SiteID, g.Brand.BrandID)))
            {
                cbHasPhoto.Visible = false;
            }
            else
            {
                cbHasPhoto.Text = g.GetResource("TXT_SHOW_PHOTO_PROFILES_ONLY", this);
                cbHasPhoto.Checked = _memberSearch.HasPhoto;
            }
        }

        private void BindRamahDate()
        {
            if (g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.JDate)
            {
                cbxRamahDate.Visible = true;
                cbxRamahDate.Text = g.GetResource("TXT_SHOW_RAMAH_ONLY",this);

                MemberSearchPreferenceInt ramaPref = _memberSearch[Preference.GetInstance("RamahAlum")] as MemberSearchPreferenceInt;
                cbxRamahDate.Checked = (ramaPref.Value == 1);
                 
            }
        }


        private void bindXMLPreferences()
        {
            // Our Basics section is going to be empty in the xml file.
            // Put everything under More Search Options.
            XmlDocument document = new XmlDocument();
            document.Load(MapPathSecure(TemplateSourceDirectory + @"\AdvancedSearch." + g.Brand.Site.SiteID + ".xml"));

            foreach (XmlNode node in document.ChildNodes[0].ChildNodes)
            {
                if (node.Name.ToLower() != "section")
                {
                    throw new Exception("Invalid node name: " + node.Name + ".  Expected 'Section'.");
                }

                switch (node.Attributes["Name"].Value.ToLower())
                {
                    case "basics":
                        rptBasics.XmlNode = node;
                        rptBasics.MemberSearch = _memberSearch;
                        rptBasics.DataBind();
                        break;
                    case "additional":
                        rptAdditionalSections.DataSource = node.ChildNodes;
                        rptAdditionalSections.DataBind();
                        break;
                }
            }
        }

        private void rptAdditionalSections_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            XmlNode node = (XmlNode)e.Item.DataItem;

            SearchPreferenceRepeater rptSection = (SearchPreferenceRepeater)e.Item.FindControl("rptSection");
            rptSection.XmlNode = node;
            rptSection.MemberSearch = _memberSearch;
            rptSection.DataBind();

            Txt txtSectionTitle = (Txt)e.Item.FindControl("txtSectionTitle");
            txtSectionTitle.ResourceConstant = node.Attributes["ResourceConstant"].Value;
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Init += new EventHandler(this.Page_Init);
            this.Load += new System.EventHandler(this.Page_Load);
            btnSave.Click += new EventHandler(btnSave_Click);
            rptAdditionalSections.ItemDataBound += new RepeaterItemEventHandler(rptAdditionalSections_ItemDataBound);
            prSearchRegion.RegionSaved += new Matchnet.Web.Framework.Ui.FormElements.PickRegionNew.RegionSavedEventHandler(prSearchRegion_RegionSaved);
            prSearchRegion.Closed += new Matchnet.Web.Framework.Ui.FormElements.PickRegionNew.ClosedEventHandler(prSearchRegion_Closed);
            prSearchRegion.RegionReset += new Matchnet.Web.Framework.Ui.FormElements.PickRegionNew.RegionResetEventHandler(prSearchRegion_RegionReset);
        }
        #endregion

        private void prSearchRegion_RegionSaved(EventArgs e)
        {
            //TODO: abstract area code stuff into the SAL.MemberSearch object

            IEnumerator areaCodeEnumerator = prSearchRegion.GetAreaCodes();

            bool done = false;

            for (Int32 i = 1; i <= 6; i++)
            {
                MemberSearchPreferenceInt areaCodePreference = _memberSearch[Preference.GetInstance("AreaCode" + i)] as MemberSearchPreferenceInt;

                if (!done)
                {
                    done = !areaCodeEnumerator.MoveNext();
                }

                areaCodePreference.Value = done ? Constants.NULL_INT : Conversion.CInt(areaCodeEnumerator.Current);
            }

            _memberSearch.RegionID = prSearchRegion.RegionID;
            _memberSearch.SchoolID = prSearchRegion.SchoolID;
            _memberSearch.SearchType = prSearchRegion.SearchType;
            
            SaveAdvancedSearch(_memberSearch);

            bindLocation();
        }

        private void prSearchRegion_Closed(EventArgs e)
        {
            prSearchRegion.RegionID = _memberSearch.RegionID;
            prSearchRegion.SearchType = _memberSearch.SearchType;
        }

        private void prSearchRegion_RegionReset(EventArgs e)
        {
            prSearchRegion.RegionID = g.Member.GetAttributeInt(g.Brand, "RegionID");
        }
    }
}