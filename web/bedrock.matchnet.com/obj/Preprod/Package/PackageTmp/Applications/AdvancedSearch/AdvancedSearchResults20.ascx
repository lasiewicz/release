﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="AdvancedSearchResults20.ascx.cs" Inherits="Matchnet.Web.Applications.AdvancedSearch.AdvancedSearchResults20" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="result" TagName="ResultList" Src="../../Framework/Ui/SearchElements/ResultList.ascx" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="cc1" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn1" TagName="ResultsViewType" Src="/Framework/Ui/BasicElements/ResultsViewType.ascx" %>
<%@ Register TagPrefix="mn1" TagName="ColorCodeSelect" Src="/Framework/Ui/BasicElements/ColorCodeSelect.ascx" %>

<div id="divAdvanceSearchPageContainer">
    <div class="header-options results30 clearfix">
        <mn:Title runat="server" id="txtMatches" ResourceConstant="NAV_SUB_ADVANCED_SEARCH" ImageName="hdr_search_results.gif" CommunitiesForImage="CI;" />
	    <div class="links">
		    <mn:txt id="SearchPreferences" runat="server" href="/Applications/AdvancedSearch/AdvancedSearch.aspx" resourceconstant="REVISE_YOUR_SEARCH_PREFERENCES" />
	    </div>
    </div>

    <%--Sort and top navigation--%>
    <div class="sort-display clearfix">
        <div class="view-by">
            <mn:txt runat="server" id="txtView" resourceconstant="TXT_VIEW_BY" />
            <select id="ddlSearchOption" onchange="OnSearchOrderByChange();">
                <asp:Repeater id="rptSearchOption" Runat="server">
	                <ItemTemplate>
	                    <li class="<asp:Literal id=litSortSpan runat=server /> no-tab">
		                    <asp:HyperLink id="lnkSort" Runat="server" />
		                    <span class="x"><asp:Literal id="litSortTitle" Runat="server" Visible="False" /></span>
                        </li>
                        <asp:Literal ID="litOption" runat="server"></asp:Literal>
                    </ItemTemplate>
                </asp:Repeater>
            </select>
        </div>
    
        <div class="pagination">
		    <asp:label id="lblListNavigationTop" Runat="server" />
	    </div>

        <asp:PlaceHolder ID="phViewMode" runat="server">
        <div class="view-as">
            <%--gallery/list--%>
            <mn1:ResultsViewType id="idResultsViewType" runat="server" 
                ListResourceConstant="TXT_LIST30"
                ListSelectedResourceConstant="TXT_LIST30_SELECTED"
                GalleryResourceConstant="TXT_GALLERY30"
                GallerySelectedResourceConstant="TXT_GALLERY30_SELECTED"
            />
        </div>
        </asp:PlaceHolder>
        <input type="hidden" name="hidSearchOrderBy" runat="server" id="hidSearchOrderBy" />
        <mn1:ColorCodeSelect id="colorCodeSelect" runat="server" />
    </div>

    <%--Search results--%>
    <div id="results30" class="results30 clearfix clear-both">
        <asp:PlaceHolder id="plcPromotionalProfile" Runat="server" Visible="false"></asp:PlaceHolder>

        <asp:PlaceHolder ID="Results" runat="server" Visible="true">
            <result:ResultList ID="SearchResultList" runat="server" ResultListContextType="AdvancedSearchResult" EnableMultipleSearchViews="false" PageSize="16" LoadControlAutomatically="false"></result:ResultList>
            <input type="hidden" value="<% = ChangeVoteMessage %>" name="ChangeVoteMessage" />
            <iframe tabindex="-1" name="FrameYNMVote" frameborder="0" width="1" scrolling="no" height="1">
                <layer name="FrameYNMVote" frameborder="0" scrolling="no" width="1" height="1"></layer>
            </iframe>
        </asp:PlaceHolder>
    </div>
    <div class="pagination">
        <asp:Label ID="lblListNavigationBottom" runat="server"></asp:Label>
    </div>

    <script type="text/javascript">
        function OnSearchOrderByChange() {
            window.location = "/Applications/AdvancedSearch/AdvancedSearchResults.aspx?SearchOrderBy=" + $j('#ddlSearchOption').val();
        }
    </script>
    
    <asp:PlaceHolder ID="phKeywordHighlightJS" runat="server" Visible="false">
    <script type="text/javascript">
        //keyword highlighting only for the list-view
        var enableKeywordHighlight = true,
            searchScope = $j("#results30"),
            searchResult = searchScope.find(".results.list-view-30 .highlight-text").length,
            keywordHighlightOptions = {
                exact: "exact",
                style_name_suffix: false,
                highlight: ".highlight-text",
                keys: "<%=_JSKeywordSearch%>"
            };

        $j(function () {
            if (enableKeywordHighlight && searchResult && keywordHighlightOptions.keys != '') {
                searchScope.SearchHighlight(keywordHighlightOptions);
            }
        });
    </script>
    </asp:PlaceHolder>
</div>