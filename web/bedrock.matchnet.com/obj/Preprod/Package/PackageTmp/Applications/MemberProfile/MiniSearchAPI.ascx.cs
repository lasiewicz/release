﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Web.Framework;

namespace Matchnet.Web.Applications.MemberProfile
{
    /// <summary>
    /// This control represents the API page called by the MiniSearch control using Ajax.
    /// </summary>
    public partial class MiniSearchAPI : FrameworkControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            bool isBachelorPage = !string.IsNullOrEmpty(Request.QueryString["fromBachelorPage"]);
            if (!String.IsNullOrEmpty(Request.QueryString["MiniSearchBar"]))
            {
                this.MiniSearchBar1.Visible = true;
                this.MiniSearchBar1.DisplayMiniSearchAjaxLoadingDiv = true;
                this.MiniSearchBar1.DisplayMiniSearchContainingDiv = false;
                this.MiniSearchBar1.DisplayMiniSearchMarketingDiv = false;
                this.MiniSearchBar1.IsPageNav = true;
                this.MiniSearchBar1.IsBachelorPage = isBachelorPage;
                this.MiniSearchBar1.LoadMiniSearch();
            }
            else
            {
                this.MiniSearch1.Visible = true;
                this.MiniSearch1.DisplayMiniSearchAjaxLoadingDiv = true;
                this.MiniSearch1.DisplayMiniSearchContainingDiv = false;
                this.MiniSearch1.DisplayMiniSearchMarketingDiv = false;
                this.MiniSearch1.IsPageNav = true;
                this.MiniSearch1.IsPageNav = isBachelorPage;
                this.MiniSearch1.LoadMiniSearch();
            }
        }
    }
}