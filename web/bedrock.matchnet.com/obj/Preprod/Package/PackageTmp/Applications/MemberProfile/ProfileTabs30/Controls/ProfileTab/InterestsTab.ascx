﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="InterestsTab.ascx.cs" Inherits="Matchnet.Web.Applications.MemberProfile.ProfileTabs30.Controls.ProfileTab.InterestsTab" %>
<%@ Register TagPrefix="uc1" TagName="EssayDataGroup" Src="/Applications/MemberProfile/ProfileTabs30/Controls/ProfileDataGroup/EssayDataGroup.ascx" %>
<%@ Register TagPrefix="uc1" TagName="LikesWidget" Src="/Applications/MemberProfile/ProfileTabs30/Controls/Facebook/LikesWidget.ascx" %>

<asp:PlaceHolder ID="phPopupAd" runat="server" Visible="false">
    <div class="ad-reservior"></div>
</asp:PlaceHolder>

<uc1:LikesWidget ID="FBLikesWidget" runat="server" Visible="false"></uc1:LikesWidget>
<uc1:EssayDataGroup ID="essaysDataGroup" runat="server"></uc1:EssayDataGroup>