﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BadgeSavedData.ascx.cs" Inherits="Matchnet.Web.Applications.MemberProfile.ProfileTabs30.Controls.Facebook.BadgeSavedData" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>

<div class="fb-likes-badge saved">
	<a href="#" class="title" title="<%=g.GetResource("TXT_BADGE_TITLE", this)%>"><span><mn:Txt ID="badgeTitle" ResourceConstant="TXT_BADGE_TITLE" runat="server" /></span></a>
	<span class="spr s-icon-fb-sm"></span>
	<ul class="fb-likes-list">
		<asp:Repeater ID="savedFBLikes" runat="server" OnItemDataBound="savedFBLikes_OnItemDataBound">
            <ItemTemplate>
                <li class="<asp:Literal ID='likeCssClass' runat='server'/>">                    
                   <asp:Image ID="likeImage" Visible="False" runat="server"/>
                </li>                    
            </ItemTemplate>
		</asp:Repeater>
	</ul>
    <asp:PlaceHolder ID="phPublishStatus" visible="False" runat="server">
        <blockquote>
            <h4 class="status"><mn:Txt ID="badgeStatusTitle" ResourceConstant="TXT_BADGE_STATUS_TITLE" runat="server" /> <span><asp:Literal ID="badgeStatusValue" runat="server" /></span></h4>
            <p><asp:Literal ID="badgeStatusValueDescription" runat="server" /> <a href="#" title="Edit Settings"><mn:Txt ID="editSettings" ResourceConstant="TXT_EDIT_SETTINGS_LINK" runat="server" /></a></p>
        </blockquote>
    </asp:PlaceHolder>
</div>

<script type="text/javascript">
    $j(document).ready(function () {
        if (typeof fbLikeManager !== "undefined" && fbLikeManager.isOwnProfile) {
            // Add 'hidden' class when status is 'hide'
            if (fbLikeManager.fbConnectStatusID != fbLikeManager.connectStatusIDShowData) {
                $j('.fb-likes-badge').addClass('hidden');
            }
        }
    });

    $j('.fb-likes-badge').click(function (e) {
        e.preventDefault();
        FBBadgeBumpToLikesInterests();
    });
    function FBBadgeBumpToLikesInterests() {
        if (tabGroup != null) {
            tabGroup.goToTab(1);
        }
        return false;
    }
</script>

