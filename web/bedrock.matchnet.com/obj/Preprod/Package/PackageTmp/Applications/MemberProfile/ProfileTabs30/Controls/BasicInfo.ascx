﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BasicInfo.ascx.cs" Inherits="Matchnet.Web.Applications.MemberProfile.ProfileTabs30.Controls.BasicInfo" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="uc2" TagName="OmnidateInvitationButton" Src="/Applications/Omnidate/Controls/OmnidateInvitationButton.ascx" %>
<%@ Register TagPrefix="uc2" TagName="MatchMeterInfoDisplay" Src="~/Framework/Ui/BasicElements/MatchMeterInfoDisplay.ascx" %>
<div id="profileSectionBasic" editsectionname="BasicInfo" edittype="modal" class="prof-edit-region clearfix"
    runat="server">
    <asp:PlaceHolder ID="phEditIcon" runat="server" Visible="false">
        <div class="prof-edit-button">
            <asp:Literal ID="litEditIcon" runat="server"></asp:Literal>
        </div>
    </asp:PlaceHolder>
    <!--BASIC MEMBER INFO: View-->
    <%=GetOmnidateDiv("open") %>
    <div class="profile30-member-pulse">
        <h2>
            <asp:Literal ID="literalUsername" runat="server"></asp:Literal>
            <!--online/offline status-->
            <asp:PlaceHolder ID="phStatusOnline" runat="server">
                <asp:HyperLink ID="lnkIMMe" runat="server">
                    <span class="spr s-icon-status-online-sm" id="onLine"><span>
                        <mn:Txt ID="Txt1" ResourceConstant="ALT_IM_ONLINE" runat="server" />
                    </span></span>
                </asp:HyperLink>
            </asp:PlaceHolder>
            <asp:PlaceHolder ID="phStatusOffline" runat="server">
                <span class="spr s-icon-status-offline-sm" id="offLine">
                    <span><mn:Txt ID="Txt2" ResourceConstant="ALT_IM_OFFLINE" runat="server" /></span>
                </span>
            </asp:PlaceHolder>
            <!--new or updated-->
            <span id="spanNewMember" runat="server" class="spr s-icon-new-member" visible="false">
                <span><mn2:FrameworkLiteral ID="literalNewMember" runat="server" ResourceConstant="ALT_NEW_MEMBER" /></span>
            </span>
            <span id="spanUpdatedMember" runat="server" class="spr s-icon-updated" visible="false">
                <span><mn2:FrameworkLiteral ID="literalUpdatedMember" runat="server" ResourceConstant="ALT_UPDATED_MEMBER" /></span>
            </span>
            <asp:PlaceHolder ID="phMatchRating2" runat="server" Visible="false">
                <%--Mutual Match Rating--%>
                <div class="mutual-match-badge">
                    <a href="#" title="<%=g.GetResource("TXT_MUTUAL_MATCH_LEARN_MORE_TITLE", this) %>" class="trigger">
                        <span class="arrow-box bottom">
                            <mn:Txt ID="Txt3" ResourceConstant="TXT_MUTUAL_MATCH_HEADER" runat="server" />
                        </span>
                        <asp:PlaceHolder runat="server" ID="phMatchRatingValuePercentage2" Visible="false">
                            <big><asp:Literal ID="litMatchRating2" runat="server"></asp:Literal>%</big>
                        </asp:PlaceHolder>
                        <asp:PlaceHolder runat="server" ID="phMatchRatingValueLess50Percentage2" Visible="false">
                            <big>&lt;50%</big> 
                        </asp:PlaceHolder>
                    </a>
                </div>
                <div class="bubble-layer mutual-match right">
                    <span class="spr s-icon-close"></span>
                    <blockquote>
                        <mn:Txt ID="Txt5" runat="server" ResourceConstant="TXT_MUTUAL_MATCH_TOOLTIP_HEADER" />
                    </blockquote>
                    <p>
                        <mn:Txt ID="Txt6" runat="server" ResourceConstant="TXT_MUTUAL_MATCH_TOOLTIP_BODY" />
                    </p>
                </div>
                <script>
                    var trigger = $j('.mutual-match-badge'),
                        layer = $j('.bubble-layer.mutual-match'),
                        close = layer.find('.s-icon-close');

                    trigger.click(function (e) {
                        layer.toggle();
                        e.preventDefault();
                    });
                    close.click(function () {
                        layer.hide();
                    });
                </script>
            </asp:PlaceHolder>
            <!--mutual yes-->
            <!-- ramah alum -->
            <asp:PlaceHolder ID="pnlRamahBadge" EnableViewState="false" runat="server" Visible="false">
                
                    <div class="ramah-date-badge">
                        <a href="#" title="<%=g.GetResource("TXT_MUTUAL_MATCH_LEARN_MORE_TITLE", this) %>" class="trigger"><span class="spr-b s-icon-b-ramah">Ramah Badge</span></a>
                    </div>
                    <div class="bubble-layer ramah-date right" style="top: -45px; font-size: 15px; width: 290px;">
                        <span class="spr s-icon-closeR"></span>
                        <mn:Txt ID="Txt8" runat="server" ResourceConstant="RAMAH_DATE_TOOL_TIP" />
                    </div>
                    <script>
                        var trigger2 = $j('.ramah-date-badge'),
                            layer2 = $j('.bubble-layer.ramah-date'),
                            close2 = layer2.find('.s-icon-closeR');

                        trigger2.click(function (e) {
                            layer2.toggle();
                            e.preventDefault();
                        });
                        close2.click(function () {
                            layer2.hide();
                        });
                    </script>
             
            </asp:PlaceHolder>
            <!-- end of ramah alum -->
            <span id="spanSecretAdmirerYY" runat="server" style="display: none;" class="spr s-icon-click-yy">
                <span>
                    <mn:Txt ID="txt4" runat="server" ResourceConstant="YNM_YY" />
                </span>
            </span>
        </h2>
    </div>
    <ul class="interest">
        <li>
            <asp:Literal ID="literalMaritalAndGender" runat="server"></asp:Literal></li>
        <li>
            <asp:Literal ID="literalAgeLocation" runat="server"></asp:Literal></li>
    </ul>
    <%=GetOmnidateDiv("close") %>
    <asp:PlaceHolder ID="phMatchRating" runat="server" Visible="false">
        <%--Mutual Match Rating--%>
        <div class="mutual-match-badge">
            <a href="#" title="<%=g.GetResource("TXT_MUTUAL_MATCH_LEARN_MORE_TITLE", this) %>"
                class="trigger"><span class="arrow-box bottom">
                    <mn:Txt ResourceConstant="TXT_MUTUAL_MATCH_HEADER" runat="server" />
                </span>
                <asp:PlaceHolder runat="server" ID="phMatchRatingValuePercentage" Visible="false"><big>
                    <asp:Literal ID="litMatchRating" runat="server"></asp:Literal>%</big> </asp:PlaceHolder>
                <asp:PlaceHolder runat="server" ID="phMatchRatingValueLess50Percentage" Visible="false">
                    <big>&lt;50%</big> </asp:PlaceHolder>
            </a>
        </div>
        <div class="bubble-layer mutual-match right">
            <span class="spr s-icon-close"></span>
            <blockquote>
                <mn:Txt runat="server" ResourceConstant="TXT_MUTUAL_MATCH_TOOLTIP_HEADER" />
            </blockquote>
            <p>
                <mn:Txt runat="server" ResourceConstant="TXT_MUTUAL_MATCH_TOOLTIP_BODY" />
            </p>
        </div>
        <script>
            var trigger = $j('.mutual-match-badge'),
                layer = $j('.bubble-layer.mutual-match'),
                close = layer.find('.s-icon-close');

            trigger.click(function (e) {
                layer.toggle();
                e.preventDefault();
            });
            close.click(function () {
                layer.hide();
            });
        </script>
    </asp:PlaceHolder>
    <asp:PlaceHolder ID="phMutualMatchPOC" runat="server" Visible="false">
        <%-- MutualMatch Proof of Concept starts --%>
        <div class="mutual-match-badge">
            <a href="#" title="Are you a good match? Find out more" class="trigger"><span class="arrow-box bottom">
                Match?</span> <big>%</big> </a>
        </div>
        <div class="bubble-layer mutual-match right">
            <h2>
                Are you a Mutual Match?</h2>
            <span class="spr s-icon-close"></span>
            <blockquote>
                We're working on a new feature and would love to get your opinion!</blockquote>
            <fieldset>
                <h3 class="question">
                    <big>Q.</big>Would you be interested in a new rating system, showing how compatible
                    you are with other JDaters<sup>&reg;</sup>?</h3>
                <ul>
                    <li>
                        <input type="radio" name="mutual-match" id="very-interested" value="very-interested" /><label
                            for="very-interested">Very interested</label></li>
                    <li>
                        <input type="radio" name="mutual-match" id="interested" value="interested" /><label
                            for="interested">Interested</label></li>
                    <li>
                        <input type="radio" name="mutual-match" id="somewhat-interested" value="somewhat-interested" /><label
                            for="somewhat-interested">Somewhat interested</label></li>
                    <li>
                        <input type="radio" name="mutual-match" id="indifferent" value="indifferent" /><label
                            for="indifferent">Indifferent</label></li>
                    <li>
                        <input type="radio" name="mutual-match" id="not-interested" value="not-interested" /><label
                            for="not-interested">Not interested at all</label></li>
                </ul>
                <div class="cta">
                    <input type="button" value="Submit" class="link-primary" />
                </div>
            </fieldset>
            <p class="confirmation">
                <span class="spr s-icon-page-message"></span>Thanks for your input!</p>
        </div>
        <script type="text/javascript">
            var submittingMMPOC = false;
            var selectedValMMPOC = '';
            (function () {
                var trigger = $j('.mutual-match-badge'),
                layer = $j('.bubble-layer.mutual-match'),
                question = layer.find('fieldset'),
                close = layer.find('.s-icon-close'),
                submit = layer.find('.cta'),
                confirmationText = layer.find('.confirmation');

                trigger.click(function (e) {
                    layer.toggle();
                    e.preventDefault();
                    var isLayerHidden = layer.is(':hidden');

                    if (!isLayerHidden){
                        //update omniture
                        if (s != null) {
                            PopulateS(true); //clear existing values in omniture "s" object
                            s.pageName = 'Mutual Match Questionaire';
                            s.eVar64 = 'Match Icon ON';
                            s.t(); //send omniture updated values as page load
                        }
                    }
                });
                close.click(function () {
                    layer.hide();
                });
                submit.click(function () {
                    if (!submittingMMPOC){
                        submittingMMPOC = true;
                        //update member has submitted answer
                        selectedValMMPOC = $j('input:radio[name=mutual-match]:checked').val();
                        //alert(selectedValMMPOC);
                        if (selectedValMMPOC != '' && (typeof selectedValMMPOC != 'undefined')){
                            $j.ajax({
                                type: "POST",
                                url: "/Applications/API/Member.asmx/UpdateAttributeDate",
                                data: "{memberID:" + <%=_JSViewerMemberID.ToString() %> + ", attributeName: \"MutualMatchPOCAnsweredDate\"}",
                                contentType: "application/json; charset=utf-8",
                                dataType: "json",
                                success: function(result) {
                                    var updateResult = (typeof result.d == 'undefined') ? result : result.d;
                                    if (updateResult.Status != 2) {
                                        alert(updateResult.StatusMessage);
                                    }
                                },
                                error: function (jqXHR, textStatus, errorThrown) {
                                    alert(textStatus);
                                }
                            });

                            //update omniture
                            if (s != null) {
                                PopulateS(true); //clear existing values in omniture "s" object
                                s.pageName = 'Mutual Match Questionaire';
                                s.eVar64 = selectedValMMPOC;
                                s.t(); //send omniture updated values as page load
                            }
                            $j(this).hide();
                            question.hide();
                            confirmationText.show();
                        }
                        submittingMMPOC = false;
                    }
                });
            })();
        </script>
        <%--MutualMatch ends--%>
    </asp:PlaceHolder>
    <uc2:OmnidateInvitationButton runat="server" ID="btnOmnidate" DisplayContext="FullProfile30" />
    <asp:PlaceHolder ID="phCustomMatchMeter" Visible="false" runat="server">
        <uc2:MatchMeterInfoDisplay runat="server" ID="ucMatchMeterInfoDisplay" DispalyType="5"
            Visible="false" />
    </asp:PlaceHolder>
</div>
<asp:PlaceHolder ID="phBasicEdit" runat="server" Visible="false">
    <div id="profileSectionBasicEdit" runat="server" class="form-set hide">
        <div id="errorSectionContainer" runat="server" class="form-error hide">
        </div>
        <!--BASIC MEMBER INFO: Edit-->
        <fieldset class="padding-heavy">
            <asp:Literal ID="litRequiredMessage" runat="server"></asp:Literal>
            <%--Edit controls--%>
            <asp:PlaceHolder ID="phEditControls" runat="server"></asp:PlaceHolder>
            <%--Submit--%>
            <div class="form-item form-submit">
                <mn2:FrameworkButton ID="btnEditSave" runat="server" CssClass="btn primary button-show"
                    ResourceConstant="BTN_EDIT_SAVE" UseSubmitBehavior="false" OnClientClick="editProfileManager.SaveProfile('BasicInfo');return false;" />
                <mn2:FrameworkButton ID="btnEditCancel" runat="server" CssClass="btn secondary btn-cancel"
                    ResourceConstant="BTN_EDIT_CANCEL" UseSubmitBehavior="false" OnClientClick="editProfileManager.CancelSectionEdit('BasicInfo');return false;" />
            </div>
        </fieldset>
    </div>
</asp:PlaceHolder>
<asp:PlaceHolder runat="server" ID="phMatchRatingMistmatchData" Visible="false">
    <!--
    MATCH RATING MISMATCH INFO
    
    Member1 Wants: <asp:Literal runat="server" ID="litMatchRatingMismatchMember1Score"></asp:Literal>
    Member2 Wants: <asp:Literal runat="server" ID="litMatchRatingMismatchMember2Score"></asp:Literal>
    Final Score: <asp:Literal runat="server" ID="litMatchRatingMismatchFinalScore"></asp:Literal>

    Member 1 mismatches (viewer)
    <asp:Literal runat="server" ID="litMember1MatchRatingMismatch"></asp:Literal>
    
    Member 2 mismatches (viewed)
    <asp:Literal runat="server" ID="litMember2MatchRatingMismatch"></asp:Literal>


    Member 1 missing attributes (viewer)
    <asp:Literal runat="server" ID="litMember1MatchRatingMissing"></asp:Literal>

    Member 2 missing attributes (viewer)
    <asp:Literal runat="server" ID="litMember2MatchRatingMissing"></asp:Literal>
    
    
    SAVED OR DEFAULT PREFERENCES
    Member1 preferences: <asp:Literal runat="server" ID="litMember1DefaultPreferences"></asp:Literal>
    Member2 preferences: <asp:Literal runat="server" ID="litMember2DefaultPreferences"></asp:Literal>
    -->
</asp:PlaceHolder>
