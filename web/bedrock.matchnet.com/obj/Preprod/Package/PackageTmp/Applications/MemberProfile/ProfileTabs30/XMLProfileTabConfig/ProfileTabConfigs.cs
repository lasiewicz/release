﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace Matchnet.Web.Applications.MemberProfile.ProfileTabs30.XMLProfileTabConfig
{
    /// <summary>
    /// This class represents all profile tab configurations for a site.
    /// It is deserialized from the Tab_siteID.xml files.
    /// <ProfileTabConfigs></ProfileTabConfigs>
    /// </summary>
    [Serializable]
    public class ProfileTabConfigs
    {
        [XmlElement("TabConfig")]
        public List<ProfileTabConfig> ProfileTabConfigList { get; set; }
    }
}
