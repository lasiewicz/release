﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProfileDetails.ascx.cs" Inherits="Matchnet.Web.Applications.MemberProfile.ProfileTabs30.Controls.ProfileDetails" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="uc1" TagName="NameValueDataGroup" Src="/Applications/MemberProfile/ProfileTabs30/Controls/ProfileDataGroup/NameValueDataGroup.ascx" %>

<div id="ProfileDetailsControl" class="group-value">
    <h2><mn2:FrameworkLiteral ID="literalDetailsTitle" runat="server" ResourceConstant="HIS_DETAILS"></mn2:FrameworkLiteral>
        <span class="timestamp">
            <mn2:FrameworkLiteral ID="literalMemberID" runat="server" ResourceConstant="TXT_MEMBERID"></mn2:FrameworkLiteral>
            <asp:Literal ID="literalMemberIDValue" runat="server"></asp:Literal>
        </span>
    </h2>

    <!--details-->
    <uc1:NameValueDataGroup ID="nvDataDetailsInfo" runat="server"></uc1:NameValueDataGroup>
    
    <!--ideal match-->
    <h2><mn2:FrameworkLiteral ID="literalIdealMatchTitle" runat="server" ResourceConstant="TITLE_IDEALMATCH"></mn2:FrameworkLiteral></h2>
    <uc1:NameValueDataGroup ID="nvDataIdealMatch" runat="server"></uc1:NameValueDataGroup>
</div>