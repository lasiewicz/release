﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EditPhone.ascx.cs" Inherits="Matchnet.Web.Applications.MemberProfile.ProfileTabs30.Controls.Edit.EditPhone" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>

<div class="form-item">
    <div class="form-label">
        <asp:Label ID="lblEdit" runat="server" AssociatedControlID="lnkPhone">
            <asp:Literal ID="litEdit" runat="server"></asp:Literal>
        </asp:Label>
    </div>
    <div class="form-input">
        <a href="/Applications/Mobile/MobileSettings.aspx" runat="server" id="lnkPhone"><asp:Literal ID="litEditPhone" runat="server"></asp:Literal></a>
        <asp:Literal ID="litRequired" runat="server"></asp:Literal>
        <div id="itemErrorContainer" runat="server" style="display:none;" class="form-error-inline"></div>
    </div>
</div>