﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ColorColorCopy.ascx.cs" Inherits="Matchnet.Web.Applications.MemberProfile.ProfileTabs30.Controls.ColorCode.ColorColorCopy" %>

<%--Blue vs.. --%>
<asp:PlaceHolder ID="phBlueBlue" runat="server" Visible="false">
    <p>BLUE personalities love to be in relationships and commit deeply to their partners. To them, a relationship is all or nothing, and you can expect the utmost loyalty from them. They are exceptionally thoughtful, remember every detail, and love to serve their partners. However, they can also be excessively needy emotionally, and have the tendency to read more into things than is really there. Their biggest downfalls are their tendency to have unrealistic expectations of themselves and others, and their tendency to be overly judgmental.</p>
</asp:PlaceHolder>
<asp:PlaceHolder ID="phBlueRed" runat="server" Visible="false">
    <p>BLUE personalities love to be in relationships and commit deeply to their partners. To them, a relationship is all or nothing, and you can expect the utmost loyalty from them. They are exceptionally thoughtful, remember every detail, and love to serve their partners. However, they can also be excessively needy emotionally, and have the tendency to read more into things than is really there. Their biggest downfalls are their tendency to have unrealistic expectations of themselves and others, and their tendency to be overly judgmental.</p>
</asp:PlaceHolder>
<asp:PlaceHolder ID="phBlueWhite" runat="server" Visible="false">
    <p>BLUE personalities love to be in relationships and commit deeply to their partners. To them, a relationship is all or nothing, and you can expect the utmost loyalty from them. They are exceptionally thoughtful, remember every detail, and love to serve their partners. However, they can also be excessively needy emotionally, and have the tendency to read more into things than is really there. Their biggest downfalls are their tendency to have unrealistic expectations of themselves and others, and their tendency to be overly judgmental.</p>
</asp:PlaceHolder>
<asp:PlaceHolder ID="phBlueYellow" runat="server" Visible="false">
    <p>BLUE personalities love to be in relationships and commit deeply to their partners. To them, a relationship is all or nothing, and you can expect the utmost loyalty from them. They are exceptionally thoughtful, remember every detail, and love to serve their partners. However, they can also be excessively needy emotionally, and have the tendency to read more into things than is really there. Their biggest downfalls are their tendency to have unrealistic expectations of themselves and others, and their tendency to be overly judgmental.</p>
</asp:PlaceHolder>

<%--Red vs.. --%>
<asp:PlaceHolder ID="phRedBlue" runat="server" Visible="false">
<p>RED personalities are strong-willed, logical people.  In a relationship, they like to call the shots. They are also willing to fight life's battles for and with their partners. They are ultra-responsible, and are usually excellent providers.  They date well, but tend to drop the "extras" once a serious commitment is made (e.g., trips, concert tickets, expensive dinners, etc.), because now that they have someone – why waste the money? Their biggest downfalls are their lack of ability to relate emotionally, and their tendency to be selfish.</p>
</asp:PlaceHolder>
<asp:PlaceHolder ID="phRedRed" runat="server" Visible="false">
<p>RED personalities are strong-willed, logical people.  In a relationship, they like to call the shots. They are also willing to fight life's battles for and with their partners. They are ultra-responsible, and are usually excellent providers.  They date well, but tend to drop the "extras" once a serious commitment is made (e.g., trips, concert tickets, expensive dinners, etc.), because now that they have someone – why waste the money? Their biggest downfalls are their lack of ability to relate emotionally, and their tendency to be selfish.</p>
</asp:PlaceHolder>
<asp:PlaceHolder ID="phRedWhite" runat="server" Visible="false">
<p>RED personalities are strong-willed, logical people.  In a relationship, they like to call the shots. They are also willing to fight life's battles for and with their partners. They are ultra-responsible, and are usually excellent providers.  They date well, but tend to drop the "extras" once a serious commitment is made (e.g., trips, concert tickets, expensive dinners, etc.), because now that they have someone – why waste the money? Their biggest downfalls are their lack of ability to relate emotionally, and their tendency to be selfish.</p>
</asp:PlaceHolder>
<asp:PlaceHolder ID="phRedYellow" runat="server" Visible="false">
<p>RED personalities are strong-willed, logical people.  In a relationship, they like to call the shots. They are also willing to fight life's battles for and with their partners. They are ultra-responsible, and are usually excellent providers.  They date well, but tend to drop the "extras" once a serious commitment is made (e.g., trips, concert tickets, expensive dinners, etc.), because now that they have someone – why waste the money? Their biggest downfalls are their lack of ability to relate emotionally, and their tendency to be selfish.</p>
</asp:PlaceHolder>

<%--White vs.. --%>
<asp:PlaceHolder ID="phWhiteBlue" runat="server" Visible="false">
<p>WHITE personalities are calm and steady. They are pleasant to be with, and always kind-hearted to their partners. You will never feel judged when dating a White, as things will always feel comfortable and informal.  They are balanced, even-tempered, logical, and listen exceptionally well. However, they can also seem detached or unavailable emotionally, as they have a hard time verbalizing what they feel. Their biggest downfalls are their lack of motivation, and their tendency to not communicate very often or meaningfully.</p>
</asp:PlaceHolder>
<asp:PlaceHolder ID="phWhiteRed" runat="server" Visible="false">
    <p>WHITE personalities are calm and steady. They are pleasant to be with, and always kind-hearted to their partners. You will never feel judged when dating a White, as things will always feel comfortable and informal.  They are balanced, even-tempered, logical, and listen exceptionally well. However, they can also seem detached or unavailable emotionally, as they have a hard time verbalizing what they feel. Their biggest downfalls are their lack of motivation, and their tendency to not communicate very often or meaningfully.</p>
</asp:PlaceHolder>
<asp:PlaceHolder ID="phWhiteWhite" runat="server" Visible="false">
    <p>WHITE personalities are calm and steady. They are pleasant to be with, and always kind-hearted to their partners. You will never feel judged when dating a White, as things will always feel comfortable and informal.  They are balanced, even-tempered, logical, and listen exceptionally well. However, they can also seem detached or unavailable emotionally, as they have a hard time verbalizing what they feel. Their biggest downfalls are their lack of motivation, and their tendency to not communicate very often or meaningfully.</p>
</asp:PlaceHolder>
<asp:PlaceHolder ID="phWhiteYellow" runat="server" Visible="false">
    <p>WHITE personalities are calm and steady. They are pleasant to be with, and always kind-hearted to their partners. You will never feel judged when dating a White, as things will always feel comfortable and informal.  They are balanced, even-tempered, logical, and listen exceptionally well. However, they can also seem detached or unavailable emotionally, as they have a hard time verbalizing what they feel. Their biggest downfalls are their lack of motivation, and their tendency to not communicate very often or meaningfully.</p>
</asp:PlaceHolder>

<%--Yellow vs.. --%>
<asp:PlaceHolder ID="phYellowBlue" runat="server" Visible="false">
    <p>YELLOW personalities are a lot of fun.  They are exciting, loud, and charismatic. Relationships with YELLOWS are always filled with social activities, riotous laughter, and a meaningful emotional connection as well. Expect them to be spontaneous, and live in the moment, free of any cares. The difficulty in being with Yellow is they can be irresponsible. They don't plan in advance and take shortcuts through life. Their biggest downfalls are their inability to follow through and completely commit.</p>
</asp:PlaceHolder>
<asp:PlaceHolder ID="phYellowRed" runat="server" Visible="false">
    <p>YELLOW personalities are a lot of fun.  They are exciting, loud, and charismatic. Relationships with YELLOWS are always filled with social activities, riotous laughter, and a meaningful emotional connection as well. Expect them to be spontaneous, and live in the moment, free of any cares. The difficulty in being with Yellow is they can be irresponsible. They don't plan in advance and take shortcuts through life. Their biggest downfalls are their inability to follow through and completely commit.</p>
</asp:PlaceHolder>
<asp:PlaceHolder ID="phYellowWhite" runat="server" Visible="false">
    <p>YELLOW personalities are a lot of fun.  They are exciting, loud, and charismatic. Relationships with YELLOWS are always filled with social activities, riotous laughter, and a meaningful emotional connection as well. Expect them to be spontaneous, and live in the moment, free of any cares. The difficulty in being with Yellow is they can be irresponsible. They don't plan in advance and take shortcuts through life. Their biggest downfalls are their inability to follow through and completely commit.</p>
</asp:PlaceHolder>
<asp:PlaceHolder ID="phYellowYellow" runat="server" Visible="false">
     <p>YELLOW personalities are a lot of fun.  They are exciting, loud, and charismatic. Relationships with YELLOWS are always filled with social activities, riotous laughter, and a meaningful emotional connection as well. Expect them to be spontaneous, and live in the moment, free of any cares. The difficulty in being with Yellow is they can be irresponsible. They don't plan in advance and take shortcuts through life. Their biggest downfalls are their inability to follow through and completely commit.</p>
</asp:PlaceHolder>

