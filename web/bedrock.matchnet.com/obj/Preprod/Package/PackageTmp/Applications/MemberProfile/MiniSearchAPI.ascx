﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MiniSearchAPI.ascx.cs" Inherits="Matchnet.Web.Applications.MemberProfile.MiniSearchAPI" %>
<%@ Register src="/Framework/Ui/BasicElements/MiniSearch.ascx" tagname="MiniSearch" tagprefix="uc1" %>
<%@ Register src="/Framework/Ui/SearchElements/MiniSearchBar.ascx" tagname="MiniSearchBar" tagprefix="uc1" %>

<uc1:MiniSearch ID="MiniSearch1" runat="server" visible="false" />
<uc1:MiniSearchBar ID="MiniSearchBar1" runat="server" visible="false"></uc1:MiniSearchBar>