﻿using System;
using System.Data;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Member.ValueObjects.Photos;
using Matchnet.Session.ValueObjects;
using Matchnet.Web.Framework;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Web.Framework.Managers;

namespace Matchnet.Web.Applications.MemberProfile
{
    public partial class AttributeEdit : FrameworkControl
    {
        string _titleFormat = "TXT_TITLE_{0}";
        string _descFormat = "TXT_DESC_{0}";
        string _attributeName;
        private string _secondaryAttributeName = string.Empty;
        private bool _multipleAttributeEdit = false;
        string _destURL = "/default.aspx";
        int _communityID;
        int _siteID;
        int _brandid;
        Member.ServiceAdapters.Member _member;

        //Omniture and JS related
        protected string _omniturePageVersions = "";
        protected bool _attributeEditFraudPledge = false;
        protected bool _attributeEditAboutMe = false;
        protected bool _attributeEditNoPhoto = false;

        protected string ForcePageResultString
        {
            get
            {
                if (SettingsManager.GetSettingBool("REGISTRATION_USE_NEW_SITE", g.Brand) &&
                    (g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.JDateUK || g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.JDateCoIL || g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.JDateFR))
                {
                    return "More Reg Info Result";
                }
                else
                {
                    return "Force Page Result";
                }
            }
        }

        protected override void OnInit(EventArgs e)
        {
            //try
            //{
            //   btnSave.Click += new System.EventHandler(this.btnSave_OnClick);

            //}
            //catch (Exception ex)
            //{ g.ProcessException(ex); }

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                g.HeaderControl.ShowBlankHeader();
                g.SetTopAuxMenuVisibility(false, true);
                g.FooterControl20.HideFooterLinks();
                
                _attributeName = Request["Attribute"];

                if (_attributeName == null)
                { throw new Exception("Attribute Name is undefined"); }

                if (Request["SecondAttribute"] != null)
                {
                    _secondaryAttributeName = Request["SecondAttribute"];
                    _multipleAttributeEdit = true;
                }

                if (g.Member == null)
                { throw new Exception("Member is not logged in"); }

                g.Session.Add("ATTR_EDIT_" + _attributeName.ToUpper(), "true", Matchnet.Session.ValueObjects.SessionPropertyLifetime.Temporary);
                if (!String.IsNullOrEmpty(Request["DestinationURL"]))
                    _destURL = Request["DestinationURL"];

                int _communityID = g.Brand.Site.Community.CommunityID;
                int _siteID = g.Brand.Site.SiteID;
                int _brandid = g.Brand.BrandID;

                //get Member
                _member = g.Member;

                //display force page version
                if (_attributeName.ToUpper() == "SITEFIRSTNAME")
                {
                    phSiteFirstname.Visible = true;
                    _omniturePageVersions = "first name";
                }
                else if (_attributeName.ToUpper() == "ABOUTME" || _secondaryAttributeName.ToUpper() == "ABOUTME")
                {
                    phAboutMe.Visible = true;
                    _omniturePageVersions = "about me essay";
                    _attributeEditAboutMe = true;
                    if (_attributeName.ToUpper() == "PHOTO")
                    {
                        plcUploadPhoto.Visible = true;
                        _omniturePageVersions += ",no photo";
                        _attributeEditNoPhoto = true;
                    }
                }
                else if (_attributeName.ToUpper() == "PLEDGETOCOUNTERFROUD")
                {
                    phPledgeToCounterFroud.Visible = true;
                    htmlPageTitle.Visible = false;
                    _omniturePageVersions = "fraud pledge";
                    _attributeEditFraudPledge = true;
                    _g.AnalyticsOmniture.Evar17 = "FP_Display";
                }
                else if (_attributeName.ToUpper() == "PHOTO")
                {
                    plcUploadPhoto.Visible = true;
                    _omniturePageVersions = "no photo";
                    _attributeEditNoPhoto = true;
                }
                else
                {
                    int attributeVal = _member.GetAttributeInt(_communityID, _siteID, _brandid, _attributeName, Constants.NULL_INT);
                    phAttribute.Visible = true;

                    if (_attributeName.ToLower() == "religion" || _attributeName.ToLower() == "jdatereligion")
                        _omniturePageVersions = "religion";
                    else
                        _omniturePageVersions = _attributeName.ToLower();

                    ListItemCollection items = FrameworkGlobals.GetAttributeOptionCollectionAsListItems(_attributeName, g, null, false);
                    ddlAttributeOptions.DataSource = items;
                    ddlAttributeOptions.DataValueField = "Value";
                    ddlAttributeOptions.DataTextField = "Text";
                    ddlAttributeOptions.DataBind();
                }

                txtAttributeEditTitle.ResourceConstant = String.Format(_titleFormat, _attributeName.ToUpper());
                txtAttributeEditText.ResourceConstant = String.Format(_descFormat, _attributeName.ToUpper());

                g.LayoutTemplateBase.HideIconLinks();

                if (_multipleAttributeEdit && _attributeName.ToUpper() == "PHOTO" && _secondaryAttributeName.ToUpper() == "ABOUTME")
                {
                    btnUploadPhotos.Visible = false;
                    btnPhotoUpdateLater.Visible = false;
                }

            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            //Omniture
            if (SettingsManager.GetSettingBool("REGISTRATION_USE_NEW_SITE", g.Brand) &&
                (g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.JDateUK || g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.JDateCoIL || g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.JDateFR))
            {
                _g.AnalyticsOmniture.PageName = "more reg. Info - short version";
            }
            else
            {
                _g.AnalyticsOmniture.PageName = "Force Page";
            }

            _g.AnalyticsOmniture.Prop8 = _omniturePageVersions;


            base.OnPreRender(e);
        }

        protected void btnSave_OnClick(object sender, EventArgs e)
        {
            try
            {
                if (_attributeName.ToUpper() == "SITEFIRSTNAME")
                {
                    string firstn = SiteFirstName.Text;

                    if (string.IsNullOrEmpty(firstn))
                    {
                        g.Notification.AddErrorString("To continue, please fill in your first name");
                        return;
                    }

                    _member.SetAttributeText(g.Brand, _attributeName, firstn, Matchnet.Member.ValueObjects.TextStatusType.None);
                    Member.ServiceAdapters.MemberSA.Instance.SaveMember(_member);
                }
                else
                {
                    int attrVal = Conversion.CInt(ddlAttributeOptions.SelectedValue);

                    if (attrVal <= 0)
                    {
                        g.Notification.AddErrorString("To continue, please select an option from the drop-down menu");
                        return;
                    }

                    _member.SetAttributeInt(_communityID, _siteID, _brandid, _attributeName, attrVal);
                    Member.ServiceAdapters.MemberSA.Instance.SaveMember(_member);
                }


                g.Transfer(_destURL);

            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }


        protected void btnAboutMeSave_OnClick(object sender, EventArgs e)
        {
            try
            {
                string errorMessage = string.Empty;
                bool aboutMeSupplied = false;
                bool photoSupplied = false;

                string textAboutMe = txtAboutMe.Text;

                if (String.IsNullOrEmpty(textAboutMe) || textAboutMe.Length < 50)
                {
                    errorMessage = g.GetResource("ERR_SUPPLY_ABOUT_ME");
                }
                else
                {
                    _member.SetAttributeText(g.Brand, "aboutme", textAboutMe, Matchnet.Member.ValueObjects.TextStatusType.None);
                    MemberSA.Instance.SaveMember(_member);
                    aboutMeSupplied = true;
                }

                if (_multipleAttributeEdit && _attributeName.ToUpper() == "PHOTO")
                {
                    //this is multi-edit and also handling
                    bool isValidImage = IsValidImage(out errorMessage);
                    if (isValidImage)
                    {
                        bool uploadSuccess = UploadMemberPhoto(out errorMessage);
                        if (uploadSuccess)
                        {
                            photoSupplied = true;
                        }
                    }
                }

                if (aboutMeSupplied && (_multipleAttributeEdit && !photoSupplied))
                {
                    AddPhotoNudgeLaterReminder();
                }
                else if ((_multipleAttributeEdit && photoSupplied) && !aboutMeSupplied)
                {
                    AddAboutMeNudgeLaterReminder();
                }
                else if (_multipleAttributeEdit && !photoSupplied && !aboutMeSupplied)
                {
                    errorMessage = g.GetResource("ERR_NO_ABOUT_ME_OR_PHOTO", this);
                    g.Notification.AddErrorString(errorMessage);
                    return;
                }
                else if (!String.IsNullOrEmpty(errorMessage))
                {
                    g.Notification.AddErrorString(errorMessage);
                    return;
                }

                LeavePage();

            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        protected void btnAboutMeUpdateLater_OnClick(object sender, EventArgs e)
        {
            AddAboutMeNudgeLaterReminder();

            if (_multipleAttributeEdit && _attributeName.ToUpper() == "PHOTO")
            {
                AddPhotoNudgeLaterReminder();
            }

            g.Transfer(_destURL);
        }


        protected void btnPhotoUpdateLater_OnClick(object sender, EventArgs e)
        {
            AddPhotoNudgeLaterReminder();
            LeavePage();
        }

        protected void btnPledgeToCounterFroudSave_OnClick(object sender, EventArgs e)
        {
            try
            {

                _member.SetAttributeInt(g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID, "PledgeToCounterFroud", 1);
                _member.SetAttributeDate(g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID, "PledgeToCounterFroudAcceptedDate", DateTime.Now);
                Member.ServiceAdapters.MemberSA.Instance.SaveMember(_member);

                LeavePage();
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        protected void btnPledgeToCounterFroudUpdateLater_OnClick(object sender, EventArgs e)
        {
            AddPledgeLaterReminder();
            g.Transfer("/Applications/Article/ArticleView.aspx?CategoryID=114&HideNav=true" + g.AppendDestinationURL());
        }

        #region Event Handlers
        protected void btnUploadPhotos_OnClick(object sender, EventArgs e)
        {
            string errorMessage = string.Empty;
            bool isValidImage = IsValidImage(out errorMessage);
            if (!isValidImage)
            {
                g.Notification.AddErrorString(errorMessage);
                return;
            }
            else
            {
                bool uploadSuccess = UploadMemberPhoto(out errorMessage);
                if (uploadSuccess)
                {
                    LeavePage();
                }
                else
                {
                    g.Notification.AddErrorString(errorMessage);
                    return;
                }
            }
        }
        #endregion

        #region Public Methods

        public bool IsValidImage(out string errorMessage)
        {
            MemberPhotoUpdateManager photoUploadManager = new MemberPhotoUpdateManager(g.Member, g.Brand, false);
            errorMessage = string.Empty;

            bool valid = true;

            // Size check
            if (upBrowse.PostedFile == null)
            {
                errorMessage = g.GetResource("ERR_SUPPLY_PHOTO");
                return false;
            }

            HttpPostedFile file = upBrowse.PostedFile;

            if (!photoUploadManager.PhotoRulesManager.FileNameExtensionAllowed(file.FileName))
            {
                errorMessage = g.GetResource("ERR_PHOTO_FORMAT_INVALID");
                return false;
            }

            if (!photoUploadManager.PhotoRulesManager.IsFileSizeAllowed(file.ContentLength))
            {
                errorMessage = g.GetResource("ERR_MAXIMUM_FILE_SIZE_EXCEEDED");
                return false;
            }

            // File format check
            try
            {
                System.Drawing.Image image = System.Drawing.Image.FromStream(file.InputStream);
            }
            catch (ArgumentException)
            {
                errorMessage = g.GetResource("ERR_UNABLE_TO_UPLOAD_A_PHOTO");
                return false;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.WriteLine((string)ex.ToString());

                errorMessage = g.GetResource("INVALID_PHOTO_TYPE");
                return false;
            }

            return valid;
        }
        #endregion

        private void AddPledgeLaterReminder()
        {
            int forcedPageIntervalDays = SettingsManager.GetSettingInt(SettingConstants.FORCED_PAGE_INTERVAL_DAYS, g.Brand);
            if (forcedPageIntervalDays > 0)
            {
                g.Session.Add(WebConstants.SESSION_PROPERTY_PLEDGE_FRAUD_LATER, "true", forcedPageIntervalDays, false);
            }
            else
            {
                g.Session.Add(WebConstants.SESSION_PROPERTY_PLEDGE_FRAUD_LATER, "true", Matchnet.Session.ValueObjects.SessionPropertyLifetime.Temporary);
            }
        }

        private void AddPhotoNudgeLaterReminder()
        {
            int forcedPageIntervalDays = SettingsManager.GetSettingInt(SettingConstants.FORCED_PAGE_INTERVAL_DAYS, g.Brand);
            if (forcedPageIntervalDays > 0)
            {
                g.Session.Add(WebConstants.SESSION_PROPERTY_NUDGE_PHOTO_LATER, "true", forcedPageIntervalDays, false);
            }
            else
            {
                g.Session.Add(WebConstants.SESSION_PROPERTY_NUDGE_PHOTO_LATER, "true", Matchnet.Session.ValueObjects.SessionPropertyLifetime.Temporary);
            }

        }

        private void AddAboutMeNudgeLaterReminder()
        {
            int forcedPageIntervalDays = SettingsManager.GetSettingInt(SettingConstants.FORCED_PAGE_INTERVAL_DAYS, g.Brand);

            if (forcedPageIntervalDays > 0)
            {
                g.Session.Add(WebConstants.SESSION_PROPERTY_NUDGE_ABOUTME_LATER, "true", forcedPageIntervalDays, false);
            }
            else
            {
                g.Session.Add(WebConstants.SESSION_PROPERTY_NUDGE_ABOUTME_LATER, "true", Matchnet.Session.ValueObjects.SessionPropertyLifetime.Temporary);
            }
        }

        private bool UploadMemberPhoto(out string errorMessage)
        {
            errorMessage = string.Empty;
            try
            {
                MemberPhotoUpdateManager photoUploadManager = new MemberPhotoUpdateManager(g.Member, g.Brand, false);

                HttpPostedFile newFile = null;

                if (upBrowse != null)
                {
                    newFile = upBrowse.PostedFile;
                }
                else if (g.IsRemoteRegistration && Request.Files.Count > 0)
                {
                    newFile = Request.Files[0];
                }

                // don't attempt save if no uploaded file and no current file 
                if ((newFile != null) && (newFile.ContentLength > 0))
                {
                    try
                    {
                        //don't attempt if file is too big
                        if (!photoUploadManager.PhotoRulesManager.IsFileSizeAllowed(newFile.ContentLength))
                        {
                            errorMessage = g.GetResource("MAXIMUM_FILE_SIZE_EXCEEDED");
                            return false;
                        }

                        Stream fileStream = newFile.InputStream;
                        fileStream.Position = 0;
                        byte[] newFileBytes = new byte[newFile.ContentLength];
                        fileStream.Read(newFileBytes, 0, newFile.ContentLength);
                        newFile.InputStream.Flush();

                        photoUploadManager.AddUpdate(Constants.NULL_INT, string.Empty, false, 1, newFileBytes, false);
                        photoUploadManager.SaveUpdates();
                    }
                    catch (Exception exIn)
                    {
                        g.ProcessException(exIn);
                        //Member Registration will succeed, adding notification message.
                        errorMessage = g.GetResource("MAXIMUM_FILE_SIZE_EXCEEDED");
                        return false;
                    }

                }
                return true;
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
                return false;
            }
        }

        private void LeavePage()
        {
            string destinationUrl = g.GetDestinationURL();

            if (destinationUrl.Length != 0)
            {
                g.DestinationURLRedirect();
            }
            else
            {
                g.Transfer("/Applications/Home/Default.aspx");
            }
        }
    }
}
