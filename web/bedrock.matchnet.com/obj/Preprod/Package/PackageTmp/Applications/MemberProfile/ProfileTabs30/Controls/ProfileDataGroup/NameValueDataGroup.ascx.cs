﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Member.ValueObjects.Interfaces;
using Matchnet.Web.Applications.MemberProfile.ProfileTabs30.XMLProfileData;
using Matchnet.Web.Applications.MemberProfile.ProfileTabs30.Controls.Edit;
using Matchnet.Web.Framework.Ui.FormElements;
using System.Web.UI.HtmlControls;

namespace Matchnet.Web.Applications.MemberProfile.ProfileTabs30.Controls.ProfileDataGroup
{
    /// <summary>
    /// Renders profile data as a simple name:value format.
    /// </summary>
    public partial class NameValueDataGroup : BaseDataGroup
    {
        public DataGroupDisplayMode DisplayMode { get; set; }
        public int MaxDisplayLength { get; set; }
        protected string _GroupTypeID = "";
        protected XMLProfileData.ProfileDataSection _CurrentProfileDataSection = null;         

        public NameValueDataGroup()
        {
            DisplayMode = DataGroupDisplayMode.OneColumn;
            MaxDisplayLength = 0;
        }

        #region Event Handlers
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        protected void repeaterSection_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                XMLProfileData.ProfileDataSection profileDataSection = e.Item.DataItem as XMLProfileData.ProfileDataSection;
                _CurrentProfileDataSection = profileDataSection;

                if (DisplayMode == DataGroupDisplayMode.TwoColumn)
                {
                    NameValueDataSectionDouble nameValueDataSection = e.Item.FindControl("nameValueDataSection") as NameValueDataSectionDouble;
                    nameValueDataSection.MaxDisplayLength = MaxDisplayLength;
                    nameValueDataSection.IsPartialRender = IsPartialRender;
                    nameValueDataSection.ResourceControl = this;
                    nameValueDataSection.LoadDataSection(_MemberProfile, _ProfileDataGroup, _CurrentProfileDataSection);
                }
                else
                {
                    NameValueDataSectionSingle nameValueDataSection = e.Item.FindControl("nameValueDataSection") as NameValueDataSectionSingle;
                    nameValueDataSection.MaxDisplayLength = MaxDisplayLength;
                    nameValueDataSection.IsPartialRender = IsPartialRender;
                    nameValueDataSection.ResourceControl = this;
                    nameValueDataSection.LoadDataSection(_MemberProfile, _ProfileDataGroup, _CurrentProfileDataSection);   
                }

            }
        }

        #endregion

        #region Public Methods
        public override void LoadDataGroup(IMemberDTO member, XMLProfileData.ProfileDataGroup profileDataGroup)
        {
            _MemberProfile = member;
            _ProfileDataGroup = profileDataGroup;
            if (g.Member != null && member.MemberID == g.Member.MemberID)
                this.WhoseProfile = WhoseProfileEnum.Self;

            if (profileDataGroup != null && profileDataGroup.ProfileDataSectionList != null && profileDataGroup.ProfileDataSectionList.Count > 0)
            {
                _GroupTypeID = ((int)profileDataGroup.ProfileDataGroupType).ToString();

                if (DisplayMode == DataGroupDisplayMode.TwoColumn)
                {
                    phTwoColumn.Visible = true;
                    repeaterSectionTwoColumn.DataSource = profileDataGroup.ProfileDataSectionList;
                    repeaterSectionTwoColumn.DataBind();
                }
                else
                {
                    phOneColumn.Visible = true;
                    repeaterSectionOneColumn.DataSource = profileDataGroup.ProfileDataSectionList;
                    repeaterSectionOneColumn.DataBind();
                }
                
            }

        }

        #endregion
        
    }
}