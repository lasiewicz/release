using System;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using Matchnet.Web.Framework;

using Matchnet.Content.ValueObjects.LandingPage;

namespace Matchnet.Web.Applications.MemberProfile.Registration.LandingPages
{
	/// <summary>
	///		Summary description for LandingPageHolder.
	/// </summary>
	public class LandingPageHolder : FrameworkControl
	{
		protected PlaceHolder plcLandingPageContent;

		private void Page_Load(object sender, System.EventArgs e)
		{
			LandingPage landingPage = g.LandingPage;

			if (landingPage == null)
				return;

			string landingPageControlPath = string.Format("/Applications/MemberProfile/Registration/LandingPages/{0}.ascx", landingPage.ControlName);
			plcLandingPageContent.Controls.Add(LoadControl(landingPageControlPath));
		}


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
