﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EditTextArea.ascx.cs" Inherits="Matchnet.Web.Applications.MemberProfile.ProfileTabs30.Controls.Edit.EditTextArea" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>

<div id="essayItemEdit" runat="server">
    <h3><asp:Literal ID="literalName" runat="server"></asp:Literal></h3>
    <fieldset>
        <asp:Literal ID="litEditNote" runat="server"></asp:Literal>
        <asp:TextBox ID="txtEdit" runat="server" TextMode="MultiLine"></asp:TextBox>
        <div id="itemErrorContainer" runat="server" style="display:none;" class="form-error-inline"></div>
    </fieldset>
</div>