﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace Matchnet.Web.Applications.MemberProfile.ProfileTabs30.XMLProfileTabConfig
{
    /// <summary>
    /// This class represents configuration for a single profile tab.
    /// It is deserialized from a subset of xml defined in the Tab_SiteID.xml files.
    /// <TabConfig></TabConfig>
    /// </summary>
    [Serializable]
    public class ProfileTabConfig
    {
        [XmlAttribute()]
        public ProfileTabEnum ProfileTabType { get; set; }
        [XmlAttribute()]
        public string ResourceConstant { get; set; }
        [XmlAttribute()]
        public bool IsEnabled { get; set; }
        [XmlAttribute()]
        public bool IsPopupOnly { get; set; }
        public string TabControl { get; set; }
        public string GAMAdSlotRight { get; set; }
        public string GAMAdSlotTop { get; set; }
        public string GAMAdSlotBelowCompose { get; set; }
        public ProfileTabOmniture Omniture { get; set; }
    }
}
