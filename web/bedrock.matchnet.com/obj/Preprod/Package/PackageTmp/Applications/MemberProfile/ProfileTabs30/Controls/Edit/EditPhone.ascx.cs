﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Matchnet.Web.Applications.MemberProfile.ProfileTabs30.Controls.Edit
{
    public partial class EditPhone : BaseEdit
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public override void LoadEditControl(XMLProfileData.ProfileDataGroup ProfileDataGroup, XMLProfileData.ProfileDataSection ProfileDataSection, XMLProfileData.ProfileDataItem ProfileDataItem, string JavascriptSectionVarName, System.Web.UI.Control ResourceControl)
        {
            _ProfileDataGroup = ProfileDataGroup;
            _ProfileDataSection = ProfileDataSection;
            _ProfileDataItem = ProfileDataItem;
            _JavascriptSectionVarName = JavascriptSectionVarName;
            _ResourceControl = ResourceControl;

            //populate label
            litEdit.Text = g.GetResource(String.IsNullOrEmpty(ProfileDataItem.EditResourceConstant) ? ProfileDataItem.ResourceConstant : ProfileDataItem.EditResourceConstant, ResourceControl);
            litEditPhone.Text = g.GetResource("TXT_CHANGE_PHONE", ResourceControl);
        }
    }
}