﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Member.ValueObjects.Interfaces;
using Matchnet.Purchase.ValueObjects;
using Matchnet.Web.Applications.MemberProfile.ProfileTabs30.XMLProfileTabConfig;
using Matchnet.Web.Framework;
using Matchnet.Lib;
using Matchnet.Web.Framework.Managers;
using Matchnet.Web.Framework.Ui;
using Matchnet.Web.Applications.MemberProfile;
using Matchnet.Email.ValueObjects;
using Matchnet.Email.ServiceAdapters;
using Matchnet.List.ServiceAdapters;
using Matchnet.List.ValueObjects;
using Matchnet.Content.ServiceAdapters.Links;
using Matchnet.Web.Framework.Ui.BasicElements;
using Matchnet.Web.Analytics;
using Matchnet.Web.Applications.Email;

namespace Matchnet.Web.Applications.MemberProfile.ProfileTabs30.Controls
{
    public partial class M2MCommunication : BaseProfile
    {
        protected string _EmailSubject = "";
        protected string _EmailWatermarkText = "";
        protected string _EmailSubjectWatermarkText = "";
        private string _PageName = "";
        private string _add2ListActionCall = "";
        private bool renderAdHalfBannerIFrameIDToJS = false;
        private int targetMemberId = Constants.NULL_INT;

        //All Access related
        protected bool _displayVIPOverlay = false;
        protected bool _isVIPEnabled = false;
        protected int _remainingVIPCount = 0;
        protected bool _IsVIPMember = false;
        protected int _MaxDisplayOverlayPerSession = 0;
        protected int _CurrentDisplayOverlayPerSession = 0;
        protected bool _isOnClickAllAccessEnabled = false;
        protected string _useYourVIPCBText = string.Empty;
        protected string _useYourVIPLBLText = string.Empty;

        public string MutualYesElementID { get; set; }
        public bool IsMutualMatch { get; protected set; }


        protected int TargetMemberID
        {
            get
            {
                return this.MemberId;
            }
        }

        protected string DefaultSubject
        {
            get { return g.GetResource("TXT_DEFAULT_SUBJECT", this); }
        }

        protected string DefaultBodyMessage
        {
            get { return g.GetResource("TXT_DEFAULT_BODY_MESSAGE", this); }
        }


        public M2MCommunication()
        {
            MutualYesElementID = "";
        }

        public bool CanSaveAsDraftEnabled
        {
            get
            {
                return ComposeHandler.canNonsubSaveAsDraft(g);
            }
        }

        public bool CanNonsubSendFreeEmail
        {
            get
            {
                return ComposeHandler.canNonSubSendFreeEmail(g);
            }
        }

        protected override void OnInit(EventArgs e)
        {
            //this.adHalfBanner.GAMAdSlot = "profile_basics_right_300x250";
            //this.adHalfBanner.GAMPageMode = Matchnet.Web.Framework.Ui.Advertising.AdUnit.GAMPageModeType.None;
            //this.adHalfBanner.GAMIframe = true;
            //this.adHalfBanner.GAMIframeHeight = 60;
            //this.adHalfBanner.GAMIframeWidth = 234;

            if (ProfileUtility.IsDisplayingProfile30(g))
            {
                ProfileTabConfigs tabConfigs = ProfileUtility.GetProfileTabConfigs(g.Brand.Site.SiteID, g);
                if (tabConfigs != null && tabConfigs.ProfileTabConfigList != null && tabConfigs.ProfileTabConfigList.Count > 0)
                {
                    ProfileTabEnum activeTab = ViewProfileTabUtility.DetermineActiveTab(tabConfigs.ProfileTabConfigList[0].ProfileTabType);
                    if (activeTab != ProfileTabEnum.None)
                    {
                        ProfileTabConfig tabConfig = ProfileUtility.GetProfileTabConfig(g.Brand.Site.SiteID, g, activeTab);
                        if (tabConfig != null && !String.IsNullOrEmpty(tabConfig.GAMAdSlotBelowCompose))
                        {
                            plcGAMAdSlotBelowCompose.Visible = true;
                            renderAdHalfBannerIFrameIDToJS = true;
                            this.adHalfBanner.GAMAdSlot = tabConfig.GAMAdSlotBelowCompose;
                        }
                    }
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            IceBreaker.ProfileMember = _MemberProfile;
            //bool isIcebreakerEnabled = Conversion.CBool(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("SHOW_ICEBREAKER", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID));
            //if (isIcebreakerEnabled && (g.Member != null) && (g.Member.IsPayingMember(g.Brand.Site.SiteID) || CanSaveAsDraftEnabled))
            //{
            //    phIcebreaker.Visible = true;
            //}
            //else
            //{
            //    phIcebreaker.Visible = false;
            //}
        }

        protected override void OnPreRender(EventArgs e)
        {
            //add click to page for omniture
            lnkFlirt.NavigateUrl = lnkFlirt.NavigateUrl + Omniture.GetOmnitureClickToPageURLParams(ProfileUtility.ProfilePageName, WebConstants.PageIDs.Tease, "", WebConstants.Action.Flirt, false);
            lnkEcard.NavigateUrl = lnkEcard.NavigateUrl + Omniture.GetOmnitureClickToPageURLParams(ProfileUtility.ProfilePageName, WebConstants.PageIDs.ViewProfile, "", WebConstants.Action.ECard, true);

            //add hotlist postback for omniture
            this.add2List.ActionCallPage = ProfileUtility.ProfilePageName;
            this.add2List.ActionCallPageDetail = _add2ListActionCall;
            Page.ClientScript.RegisterClientScriptBlock(typeof(System.Web.UI.Page), "HotlistActionCallPageDetail",
                        "<script type=\"text/javascript\">"
                        + "tabProfileObject.HotlistActionCallPageDetailID = \"" + this.add2List.ActionCallPageDetailClientID + "\";"
                        + "</script>");

            //add client side click for omniture
            this.lnkIMOnline.Attributes.Add("onclick", g.AnalyticsOmniture.GetOnClickCustomLinkTracking(WebConstants.Action.IM, ProfileUtility.ProfilePageName, "", true, false));

            if (this.renderAdHalfBannerIFrameIDToJS)
            {
                Page.ClientScript.RegisterClientScriptBlock(typeof(System.Web.UI.Page), "HalfBannerJSAd",
                       "<script type=\"text/javascript\">"
                       + "var belowComposeAd234x60IFrameID = \"" + this.adHalfBanner.AdIFrameID + "\";"
                       + "</script>");
            }

            base.OnPreRender(e);
        }

        public void LoadM2MCommunication(IMemberDTO member, string pageName)
        {
            this.MemberProfile = member;
            _PageName = pageName;


            if (MemberId == member.MemberID)
            {
                WhoseProfile = WhoseProfileEnum.Self;
                phHideM2M.Visible = true;
            }

            string emailUsername = MemberUserName;
            if (String.IsNullOrEmpty(emailUsername))
            {
                emailUsername = MemberId.ToString();
            }
            _EmailSubject = string.Format(g.GetResource("COMMENT_ABOUT_YOUR_PROFILE", this), emailUsername);
            _EmailWatermarkText = g.GetResource("TXT_EMAIL_WATERMARK", this.txtEmailMessage);
            _EmailSubjectWatermarkText = g.GetResource("TXT_EMAIL_WATERMARKSUBJECT", this.txtEmailSubject);

            //set IM
            bool isOnline = false;
            OnlineLinkHelper onlineLinkHelper = OnlineLinkHelper.getOnlineLinkHelper(Matchnet.Web.Framework.Ui.BasicElements.ResultContextType.None, this.MemberProfile, g.Member, LinkParent.FullProfile, (int)PurchaseReasonType.AttemptToIMFullProfileM2M, out isOnline);
            if (!isOnline)
            {
                this.lnkIMOnline.Visible = false;
                this.txtIMOffline.Visible = true;
            }
            else
            {
                this.lnkIMOnline.NavigateUrl = onlineLinkHelper.OnlineLink;
                this.lnkIMOnline.Visible = true;
                this.txtIMOffline.Visible = false;
            }

            //set send e-card link
            if (g.EcardsEnabled)
            {
                // Generate mingle ecard page url. (to parameter is the username of the reciever)
                string destPageUrl = FrameworkGlobals.BuildConnectFrameworkLink("cards/categories.html?to=" + member.GetUserName(_g.Brand) + "&MemberID=" + member.MemberID.ToString() + "&return=1", g, true);

                if (IsLoggedIn)
                {
                    // Assign url to ECard link.                
                    this.lnkEcard.NavigateUrl = destPageUrl;
                }
                else
                {
                    this.lnkEcard.NavigateUrl = g.GetLogonRedirect(HttpContext.Current.Request.RawUrl.ToString(), null);
                }

            }
            else
            {
                this.phEcardLink.Visible = false;
            }

            //set flirt/tease link
            bool returnToMember = (g.GetEntryPoint(Request) == BreadCrumbHelper.EntryPoint.LookUpMember);
            if (IsLoggedIn)
            {
                this.lnkFlirt.NavigateUrl = ProfileDisplayHelper.GetFlirtLink(g, member.MemberID);
                ProfileDisplayHelper.AddFlirtOnClick(g, ref this.lnkFlirt);
            }
            else
            {
                this.lnkFlirt.NavigateUrl = g.GetLogonRedirect(HttpContext.Current.Request.RawUrl.ToString(), null);
            }

            //favorites
            add2List.MemberID = this.MemberProfile.MemberID;

            //set email section
            SetEmailSection();

            //set YNM
            if (g.IsYNMEnabled)
            {
                try
                {
                    //Yes Vote button
                    lnkYes.ToolTip = g.GetResource("YNM_Y_IMG_ALT_TEXT", this);
                    //No Vote button
                    lnkNo.ToolTip = g.GetResource("YNM_N_IMG_ALT_TEXT", this);
                    //Maybe Vote button
                    lnkMaybe.ToolTip = g.GetResource("YNM_M_IMG_ALT_TEXT", this);

                    if (MemberId == 0)
                    {
                        //send visitor to registration or logged out user to login
                        lnkYes.NavigateUrl = g.GetLogonRedirect(HttpContext.Current.Request.RawUrl.ToString(), null);
                        lnkNo.NavigateUrl = g.GetLogonRedirect(HttpContext.Current.Request.RawUrl.ToString(), null);
                        lnkMaybe.NavigateUrl = g.GetLogonRedirect(HttpContext.Current.Request.RawUrl.ToString(), null);

                        //show the default secret admirer copy on visitor profile
                        secretAdmirerPopup_desc.Style["display"] = "block";
                    }
                    else
                    {

                        ClickMask clickMask = g.List.GetClickMask(g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, this.MemberProfile.MemberID);

                        //Put the clickMask in a hidden input for use by JavaScript (YNMVote)
                        YNMVoteStatus.Value = ((int)clickMask).ToString();

                        if ((((clickMask & ClickMask.TargetMemberYes) == ClickMask.TargetMemberYes) && ((clickMask & ClickMask.MemberYes) == ClickMask.MemberYes)))
                        {
                            //spanSecretAdmirerYY.Style["display"] = "inline-block";
                            secretAdmirerPopup_descYY.Style["display"] = "block";
                            spanSecretAdmirerYY.Style["display"] = "inline-block";
                            spanYes.Attributes["class"] = "spr s-icon-click-y-on";
                            IsMutualMatch = true;
                        }
                        else if ((clickMask & ClickMask.MemberYes) == ClickMask.MemberYes)
                        {
                            secretAdmirerPopup_descY.Style["display"] = "block";
                            spanYes.Attributes["class"] = "spr s-icon-click-y-on";
                        }
                        else if ((clickMask & ClickMask.MemberNo) == ClickMask.MemberNo)
                        {
                            secretAdmirerPopup_descN.Style["display"] = "block";
                            spanNo.Attributes["class"] = "spr s-icon-click-n-on";
                        }
                        else if ((clickMask & ClickMask.MemberMaybe) == ClickMask.MemberMaybe)
                        {
                            secretAdmirerPopup_descM.Style["display"] = "block";
                            spanMaybe.Attributes["class"] = "spr s-icon-click-m-on";
                        }
                        else
                        {
                            secretAdmirerPopup_desc.Style["display"] = "block";
                        }

                        YNMVoteParameters parameters = new YNMVoteParameters();

                        parameters.SiteID = _g.Brand.Site.SiteID;
                        parameters.CommunityID = _g.Brand.Site.Community.CommunityID;
                        parameters.FromMemberID = _g.Member.MemberID;
                        parameters.ToMemberID = this.MemberProfile.MemberID;
                        string encodedParameters = parameters.EncodeParams();

                        //if member recognized setup js links
                        lnkYes.NavigateUrl = string.Format("javascript:M2M_ProcessYNM('{0}','{1}','{2}');", lnkYes.NamingContainer.ClientID, 1, encodedParameters);
                        lnkNo.NavigateUrl = string.Format("javascript:M2M_ProcessYNM('{0}','{1}','{2}');", lnkNo.NamingContainer.ClientID, 2, encodedParameters);
                        lnkMaybe.NavigateUrl = string.Format("javascript:M2M_ProcessYNM('{0}','{1}','{2}');", lnkMaybe.NamingContainer.ClientID, 3, encodedParameters);
                    }
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine(ex.Message);
                    //this.divMutualYes.Style.Add("display", "none");
                    YNMVoteStatus.Value = Convert.ToInt32(ClickMask.None).ToString();
                }

            }
            else
            {
                phSecretAdmirer.Visible = false;
            }
        }

        private void SetEmailSection()
        {
            try
            {

                if (g.Member != null && (ComposeHelper.PassesSendNonReplyIMailCheck(g) || CanSaveAsDraftEnabled) && ComposeHelper.PassesIMailFraudCheck(g))
                {
                    if (g.Member != null && g.Member.IsPayingMember(g.Brand.Site.SiteID) && ShowSubjectLineFeatureThrottler.IsFeatureEnabled(g.Member.MemberID, g))
                    {
                        txtEmailSubject.Visible = true;
                        phSubjectAndTitleTags.Visible = true;
                    }


                    // Subscriber or Free site or Promotional access
                    panelNonSub.Visible = false;
                    panelNotPassFraud.Visible = false;

                    var oneClickPurchaseManager = new OneClickPurchaseManager();
                    _isOnClickAllAccessEnabled = oneClickPurchaseManager.IsOneClickAllAccessEnabled(g.Brand, g.Member);

                    // Check ALL Access (VIP Email)
                    bool isVIPEmailReady = true;
                    if (isVIPEmailReady)
                    {
                        if (VIPMailUtils.IsVIPSendEnabledSite(g.Brand))
                        {
                            phSendNonVIP.Visible = false;
                            phSendAsVIP.Visible = true;

                            //show overlay and correct resources
                            _isVIPEnabled = true;
                            _displayVIPOverlay = false;
                            _MaxDisplayOverlayPerSession = VIPMailUtils.GetMaxDisplayOverlayPerSession(g, out _CurrentDisplayOverlayPerSession);

                            string emailcount = "0";
                            VIPMember vipmember = VIPMailUtils.GetVIPMember(g, g.Member);
                            string ToUserName = _MemberProfile.GetUserName(g.Brand);
                            if (VIPMailUtils.IsVIPMember(g, vipmember))
                            {
                                emailcount = vipmember.EmailCount.ToString();
                                _remainingVIPCount = vipmember.EmailCount;
                                _IsVIPMember = true;

                                //All Access member with VIP Email
                                literalVIPOverlay.Text = g.GetResource("TXT_VIP_OVERLAY_USE_VIP", new string[] { ToUserName, emailcount }, true, this);
                                _displayVIPOverlay = VIPMailUtils.DisplayOverlay(g, VIPOverlayMode.vipuseemail);

                                //All Access member with 0 VIP Email
                                literalRanoutVIPOverlay.Text = g.GetResource("TXT_VIP_OVERLAY_UPSELL", new string[] { ToUserName }, true, this);
                                literaOneClicklRanoutVIPOverlay.Text = g.GetResource("TXT_ONE_CLICK_VIP_OVERLAY_UPSELL", new string[] { ToUserName }, true, this);
                                if (vipmember.EmailCount <= 0)
                                    _displayVIPOverlay = VIPMailUtils.DisplayOverlay(g, VIPOverlayMode.vipupsell);
                                if (_isOnClickAllAccessEnabled)
                                {
                                    plcOneClickAllAccessExistingVIP.Visible = true;
                                    litOneClickRanOutInfo.Text = g.GetResource("HTML_ONE_CLICK_RANOUT_INFO", this);
                                    // Add to source in case a one click purchase is being made 
                                    _useYourVIPCBText = g.GetResource("CHK_USE_YOUR_VIP", new string[] { emailcount }, true, this).Replace("\r\n", string.Empty);
                                    _useYourVIPLBLText = g.GetResource("TXT_USE_YOUR_VIP", new string[] { emailcount, emailcount }, true, this).Replace("\r\n", string.Empty);
                                }
                                else
                                {
                                    phExistingVIPOverlay.Visible = true;
                                }

                                lblVIPRemaining.Text = g.GetResource("TXT_USE_YOUR_VIP", new string[] { emailcount, emailcount }, true, this);
                                cbSendAsVIP.Text = g.GetResource("CHK_USE_YOUR_VIP", new string[] { emailcount }, true, this);
                            }
                            else
                            {
                                //Non-All Access member
                                literalNonVIPOverlay.Text = g.GetResource("TXT_VIP_OVERLAY_BUY_VIP", new string[] { ToUserName }, true, this);
                                _displayVIPOverlay = VIPMailUtils.DisplayOverlay(g, VIPOverlayMode.subscriberbuy);

                                if (_isOnClickAllAccessEnabled)
                                {
                                    literalOneClickNonVIPOverlay.Text = g.GetResource("TXT_ONE_CLICK_VIP_OVERLAY_BUY_VIP", new string[] { ToUserName }, true, this);
                                    litOneClickPaymentInfo.Text = g.GetResource("HTML_ONE_CLICK_PAYMENT_INFO", this);
                                    mntxtOneClickInfo.Text = g.GetResource("HTML_ONE_CLICK_INFO", this);
                                    plcOneClickAllAccessNonVIP.Visible = true;

                                    // Add to source in case a one click purchase is being made 
                                    _useYourVIPCBText = g.GetResource("CHK_USE_YOUR_VIP", new string[] { emailcount }, true, this).Replace("\r\n", string.Empty);
                                    _useYourVIPLBLText = g.GetResource("TXT_USE_YOUR_VIP", new string[] { emailcount, emailcount }, true, this).Replace("\r\n", string.Empty);
                                }
                                else
                                {
                                    phNonVIPOverlay.Visible = true;
                                }

                                lblVIPRemaining.Text = g.GetResource("TXT_SEND_AS_VIP", this);
                                cbSendAsVIP.Text = g.GetResource("CHK_SEND_AS_VIP", this);
                            }
                        }
                    }
                }
                else
                {
                    //Email Not Allowed
                    if (g.Member != null && !ComposeHelper.PassesIMailFraudCheck(g))
                    {
                        // Fraud check
                        panelNotPassFraud.Visible = true;
                        txtFraudCheck.Text = g.GetResource("TXT_EMAIL_PENDING_FRAUD_CHECK", this);
                    }
                    else
                    {
                        // Non-subscriber
                        panelNonSub.Visible = true;
                        string subscribeHook = g.GetResource("TXT_SUBSCRIBE", this);

                        if (g.Member != null)
                        {
                            subscribeHook = String.Format(subscribeHook,
                                FrameworkGlobals.LinkHref("/Applications/Subscription/Subscribe.aspx?" + WebConstants.URL_PARAMETER_PURCHASEREASONTYPEID + "=" + PurchaseReasonType.EmailOnMyProfileUpgrade.ToString("d") + "&DestinationURL=" + HttpUtility.UrlEncode(Request.RawUrl), false),
                                this.MemberProfile.GetUserName(g.Brand));
                        }
                        else
                        {
                            subscribeHook = g.GetResource("TXT_COMM_BNR_JOIN_NOW", this);
                            subscribeHook = String.Format(subscribeHook,
                                g.GetLogonRedirect(HttpContext.Current.Request.RawUrl.ToString(), null),
                                this.MemberProfile.GetUserName(g.Brand));
                        }

                        txtSubscribe.Text = subscribeHook;
                    }

                    buttonSendEmail.CssClass = "button-hide";
                    txtEmailMessage.WatermarkCssClass = "textbox-hide";
                    txtEmailMessage.CssClass = "textbox-hide";

                }
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }
    }
}
