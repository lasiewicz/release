﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ChangeEmail20.ascx.cs" Inherits="Matchnet.Web.Applications.MemberProfile.ChangeEmail20" %>
<%@ Register TagPrefix="cc1" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>

<script type="text/javascript">
<!--
	function lookupHandleKey(event, buttonName)
	{
		if (!event) {
			return true;
        }
		if (event.keyCode == 13) {
			//alert(buttonName); 
			button = document.forms[0][buttonName];
			if(button)
			{
				button.click();
				return false;
			}
			else
			{
				// alert("Did not get a button back.  Invalid buttonName parameter.");
			}
		}
		else {
			//	Key press we don't care about.
		}
		return true;
	}
-->
</script>

<div id="change-email">
	<mn:Title runat="server" id="ttlChangeEmailPassword" ResourceConstant="TTL_CHANGE_EMAIL_PASSWORD"/>
    <fieldset>
        <h2><mn:txt runat="server" id="changeEmail" resourceconstant="SUB_CHANGE_EMAIL" /></h2>
        <label><mn:txt runat="server" id="currentEmail" resourceconstant="TXT_CURRENT_EMAIL" /></label>
        <span class="field"><asp:Label ID="lbEmailAddress" Runat="server" /></span>
        <label><mn:txt runat="server" id="newEmail" resourceconstant="TXT_NEW_EMAIL" /></label>
        <span class="field"><asp:TextBox ID="tbNewEmailAddress" Runat="server" Width="196px" MaxLength="50" dir="ltr" />
                <asp:RequiredFieldValidator id="rfvNewEmailAddress" EnableClientScript="False" Runat="server" ControlToValidate="tbNewEmailAddress">
			    <br />
			    <mn:txt runat="server" id="txtNewEmailAddressIsRequired" cssclass="error" resourceconstant="ERR_NEW_EMAIL_ADDRESS_IS_REQUIRED" />
			    </asp:RequiredFieldValidator>
				<asp:RegularExpressionValidator id="revNewEmailAddress" EnableClientScript="False" runat="server" ControlToValidate="tbNewEmailAddress" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">
				<br />
				<mn:txt runat="server" id="txtNewEmailAddressIsNotValid"  cssclass="error" resourceconstant="ERR_NEW_EMAIL_ADDRESS_IS_NOT_VALID" />
				</asp:RegularExpressionValidator>
		</span>
		<label><mn:txt runat="server" id="reenterEmail" resourceconstant="TXT_REENTER_EMAIL" />
            <asp:CompareValidator id="cvEmailAddress" EnableClientScript="False" Type="String" runat="server" ControlToValidate="tbNewEmailAddress" ControlToCompare="tbNewEmailAddressConfirm" Operator="Equal">
			    <br />
			    <mn:txt runat="server" id="txtEmailAddressMustMatch" cssclass="error" resourceconstant="ERR_EMAIL_ADDRESS_MUST_MATCH" />
			</asp:CompareValidator>
		</label>
        <span class="field"><asp:TextBox ID="tbNewEmailAddressConfirm" Runat="server" width="196px"  MaxLength="50" dir="ltr" /></span>
        <span class="submit"><cc1:FrameworkButton class="btn primary" id="btnSaveEmail" runat="server" ResourceConstant="BTN_SAVE" /></span>
    </fieldset>
    <fieldset>
        <h2><mn:txt runat="server" id="changePassword" resourceconstant="SUB_CHANGE_PASSWORD" /></h2>
        <label><mn:txt runat="server" id="currentPassword" resourceconstant="TXT_CURRENT_PASSWORD" /></label>
        <span class="field"><asp:TextBox ID="tbPassword" TextMode="Password" Runat="server" Width="196px" dir="ltr" MaxLength="16" />
                <asp:RequiredFieldValidator Runat="server" ID="rfvPassword" EnableClientScript="False" ControlToValidate="tbPassword">
				<br />
				<mn:txt runat="server" id="txtCurrentPasswordIsRequired" cssclass="error" resourceconstant="ERR_CURRENT_PASSWORD_IS_REQUIRED" />
				</asp:RequiredFieldValidator>
				<asp:CustomValidator Runat="server" ID="csvPassword" EnableClientScript="False" ControlToValidate="tbPassword">
				<br />
				<mn:txt runat="server" id="txtCurrentPasswordIsIncorrect" cssclass="error" resourceconstant="ERR_CURRENT_PASSWORD_CHECK_FAILED" />
				</asp:CustomValidator>
	    </span>
        <label><mn:txt runat="server" id="newPassword" resourceconstant="TXT_NEW_PASSWORD" /></label>
        <span class="field"><asp:TextBox ID="tbNewPassword" TextMode="Password" Runat="server" Width="196px" dir="ltr" MaxLength="16" />
				<asp:RequiredFieldValidator Runat="server" ID="rfvNewPassword" EnableClientScript="False" ControlToValidate="tbNewPassword">
				<br />
				<mn:txt runat="server" id="txtNewPasswordIsRequired" cssclass="error" resourceconstant="ERR_NEW_PASSWORD_IS_REQUIRED" />
				</asp:RequiredFieldValidator>
		</span>
		<label><mn:txt runat="server" id="reenterPassword" resourceconstant="TXT_REENTER_PASSWORD" /></label>
        <span class="field"><asp:TextBox ID="tbNewPasswordConfirm" TextMode="Password" Runat="server" Width="196px"  dir="ltr" MaxLength="16" />
                <asp:CompareValidator ID="cvPassword" Type="String" EnableClientScript="False" Runat="server" ControlToValidate="tbNewPassword"
				ControlToCompare="tbNewPasswordConfirm" Operator="Equal">
				<br />
				<mn:txt runat="server" id="txtNewPasswordNotConfirmed" cssclass="error" resourceconstant="ERR_NEW_PASSWORD_NOT_CONFIRMED" />
				</asp:CompareValidator>
        </span>
        <span class="submit"><cc1:FrameworkButton class="btn primary" id="btnSavePassword" runat="server" ResourceConstant="BTN_SAVE" /></span>
    </fieldset>

</div> 
