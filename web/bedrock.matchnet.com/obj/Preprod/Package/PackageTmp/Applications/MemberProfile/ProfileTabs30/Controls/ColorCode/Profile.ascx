﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Profile.ascx.cs" Inherits="Matchnet.Web.Applications.MemberProfile.ProfileTabs30.Controls.ColorCode.Profile" %>

<asp:PlaceHolder ID="phRedProfile" runat="server" Visible="false">
        <h3>Core Motive: Power</h3>
        <ul class="bulletless">
          <li>Moving from A to B</li>
        </ul>

        <h3>Natural Gifts</h3>
        <ul class="bulletless">
          <li>Vision &amp; Leadership</li>
        </ul>

        <h3>Needs</h3>
        <ul class="bulletless">
            <li>To look good intellectually</li>
            <li>To be right</li>
            <li>To be respected</li>
            <li>Approval from a select few</li>
        </ul>

        <h3>Wants</h3>
        <ul class="bulletless">
            <li>To be productive</li>
            <li>To hide securities tightly</li>
            <li>To be a leader</li>
            <li>Challenging adventure</li>
        </ul>
</asp:PlaceHolder>

<asp:PlaceHolder ID="phBlueProfile" runat="server" Visible="false">
    
        <h3>Core Motive: Intimacy</h3>
        <ul class="bulletless">
          <li>Developing Legitimate Connections</li>
        </ul>

        <h3>Natural Gifts</h3>
        <ul class="bulletless">
          <li>Quality &amp; Service</li>
        </ul>

        <h3>Needs</h3>
        <ul class="bulletless">
            <li>To be good morally</li>
            <li>To be understood</li>
            <li>To be appreciated</li>
            <li>Acceptance</li>
        </ul>

        <h3>Wants</h3>
        <ul class="bulletless">
            <li>To ensure quality</li>
            <li>To have autonomy</li>
            <li>To have security</li>
            <li>To please others</li>
        </ul>
</asp:PlaceHolder>

<asp:PlaceHolder ID="phWhiteProfile" runat="server" Visible="false">
        <h3>Core Motive: Peace</h3>
        <ul class="bulletless">
          <li>Maintaining Balance in Life</li>
        </ul>

        <h3>Natural Gifts</h3>
        <ul class="bulletless">
          <li>Clarity &amp; Tolerance</li>
        </ul>

        <h3>Needs</h3>
        <ul class="bulletless">
            <li>To feel good inside</li>
            <li>To be allowed their space</li>
            <li>To be respected</li>
            <li>Tolerance</li>
        </ul>

        <h3>Wants</h3>
        <ul class="bulletless">
            <li>To be kind</li>
            <li>To be independent</li>
            <li>To please self and others</li>
            <li>To be content</li>
        </ul>
</asp:PlaceHolder>

<asp:PlaceHolder ID="phYellowProfile" runat="server" Visible="false">
        <h3>Core Motive: Fun</h3>
        <ul class="bulletless">
          <li>Living in the Moment</li>
        </ul>

        <h3>Natural Gifts</h3>
        <ul class="bulletless">
          <li>Enthusiasm &amp; Optimism</li>
        </ul>

        <h3>Needs</h3>
        <ul class="bulletless">
            <li>To look good socially</li>
            <li>To be praised</li>
            <li>To be noticed</li>
            <li>General approval</li>
        </ul>

        <h3>Wants</h3>
        <ul class="bulletless">
            <li>To be happy</li>
            <li>To have freedom</li>
            <li>To hide insecurities loosely</li>
            <li>Playful adventure</li>
        </ul>
</asp:PlaceHolder>