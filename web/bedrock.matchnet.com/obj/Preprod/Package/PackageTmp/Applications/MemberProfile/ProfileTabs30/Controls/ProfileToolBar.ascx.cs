﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Member.ValueObjects.Interfaces;
using Matchnet.Web.Framework;
using Matchnet.Web.Applications.MemberProfile;
using Matchnet.List.ValueObjects;

namespace Matchnet.Web.Applications.MemberProfile.ProfileTabs30.Controls
{
    public partial class ProfileToolBar : BaseProfile
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public void LoadProfileToolBar(IMemberDTO member)
        {
            MemberProfile = member;
            if (g.Member != null && g.Member.MemberID == member.MemberID)
            {
                WhoseProfile = WhoseProfileEnum.Self;
            }

            string urlParams = ProfileDisplayHelper.GetURLParamsNeededByNextProfile();

            //contact history
            contactHistory1.LoadContactHistory(MemberProfile);

            //set report member link
            if (g.Member != null && g.Member.MemberID > 0 && FrameworkGlobals.DetermineReportLinkVisibility(g.Member.MemberID, g.Brand))
            {
                lnkReportConcern.Visible = true;

                lnkReportConcern.NavigateUrl =
                    "/Applications/MemberServices/ReportMember.aspx?MemberID=" +
                    MemberProfile.MemberID +
                    urlParams;
            }

            if (_MemberProfile != null)
            {
                this.lnkSendToFriend.Visible = true;
                this.lnkSendToFriend.NavigateUrl = ProfileDisplayHelper.SetSendToFriendLink(_MemberProfile.MemberID) + urlParams;
            }

            //block
            blockProfile.LoadBlockControl(MemberProfile);

            //Output javascript when viewing own profile to disable certain things but still make it visible
            if (WhoseProfile == WhoseProfileEnum.Self)
            {
                phHideToolBar.Visible = true;
            }

        }

        #region Private methods

        #endregion
    }
}