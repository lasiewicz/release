﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace Matchnet.Web.Applications.MemberProfile.ProfileTabs30.XMLProfileData
{
    /// <summary>
    /// This class represents a single section of data within a profile data group, which may contain
    /// one or more data items.
    /// It is deserialized from a subset of xml defined in the ProfileData_SiteID.xml files.
    /// </summary>
    [Serializable]
    public class ProfileDataSection
    {
        [XmlAttribute("SectionName")]
        public ProfileDataSectionEnum ProfileDataSectionName { get; set; }
        [XmlAttribute("TitleResourceConstant")]
        public string SectionTitleResourceConstant { get; set; }
        [XmlAttribute("EditTitleResourceConstant")]
        public string SectionEditTitleResourceConstant { get; set; }
        [XmlAttribute()]
        public bool IsMatchPreferences { get; set; }
        [XmlElement("Item")]
        public List<ProfileDataItem> ProfileDataItemList { get; set; }

        #region Public Methods
        public ProfileDataItem GetProfileDataItem(string attributeName)
        {
            ProfileDataItem dataItem = null;
            if (ProfileDataItemList != null)
            {
                foreach (ProfileDataItem pdi in ProfileDataItemList)
                {
                    if (pdi.AttributeName.ToLower() == attributeName.ToLower())
                    {
                        return pdi;
                    }
                }
            }

            return dataItem;
        }

        #endregion

    }
}
