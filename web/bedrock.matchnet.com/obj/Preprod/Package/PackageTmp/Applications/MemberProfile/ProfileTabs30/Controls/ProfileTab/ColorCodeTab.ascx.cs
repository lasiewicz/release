﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Member.ValueObjects.Interfaces;
using Matchnet.Web.Applications.ColorCode;
using Matchnet.Web.Framework;
using Matchnet.Web.Applications.MemberProfile;

namespace Matchnet.Web.Applications.MemberProfile.ProfileTabs30.Controls.ProfileTab
{
    public partial class ColorCodeTab : BaseTab
    {
        protected MemberQuiz _MemberQuiz = null;
        protected MemberQuiz _ViewerMemberQuiz = null;
        protected Color _MemberColor = Color.none;
        protected string _MemberColorText = "";
        protected Color _ViewerColor = Color.none;
        protected string _ViewerColorText = "";
        protected string _UserName = "";
        protected string _BlockElementCSS = "";

        public ColorCodeTab()
        {
            this.ProfileTabType = ProfileTabEnum.ColorCode;
        }

        #region Event Handlers        

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// Handler for PreRender event
        /// </summary>
        /// <param name="e"></param>
        protected override void OnPreRender(EventArgs e)
        {
            //omniture
            if (_MemberColor != Color.none)
            {
                g.AnalyticsOmniture.Evar33 = _MemberColorText;
            }

            base.OnPreRender(e);
        }

        protected void btnSendColorCodeInvitation_Click(object sender, EventArgs e)
        {
            btnSendColorCodeInvitation2_Click(sender, e);
        }

        protected void btnSendColorCodeInvitation2_Click(object sender, EventArgs e)
        {
            try
            {
                string recipientEmail = _MemberProfile.EmailAddress;
                string recipientUserName = String.IsNullOrEmpty(_MemberProfile.GetUserName(_g.Brand)) ? _MemberProfile.MemberID.ToString() : _MemberProfile.GetUserName(_g.Brand);
                string senderEmail = g.Member.EmailAddress;
                int senderMemberID = g.Member.MemberID;
                string senderUserName = String.IsNullOrEmpty(g.Member.GetUserName(_g.Brand)) ? g.Member.MemberID.ToString() : g.Member.GetUserName(_g.Brand);
                int brandID = g.Brand.BrandID;

                //send color code test invitation email
                ExternalMail.ServiceAdapters.ExternalMailSA.Instance.SendColorCodeQuizInvite(recipientEmail,
                    recipientUserName,
                    senderEmail,
                    senderMemberID,
                    senderUserName,
                    brandID);

                //use temporary session to store the fact that member has already sent invitation to this member
                string sentList = String.IsNullOrEmpty(g.Session["CCInviteSent"]) ? "" : g.Session["CCInviteSent"];
                if (sentList == "")
                    sentList = _MemberProfile.MemberID.ToString();
                else
                    sentList += "," + _MemberProfile.MemberID.ToString();

                g.Session.Add("CCInviteSent", sentList, Matchnet.Session.ValueObjects.SessionPropertyLifetime.Temporary);

                //display messages
                phSendInvitationButtons.Visible = false;
                phInvitationAlreadySent.Visible = true;
                g.Notification.AddMessageString(g.GetResource("TXT_INVITESENTCONFIRMATION", this));

            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }


        }
        #endregion

        #region Public Methods
        public override void LoadTab(IMemberDTO member, bool isActive)
        {
            this._MemberProfile = member;
            this.IsActive = isActive;
            if (g.Member != null && member.MemberID == g.Member.MemberID)
                this.WhoseProfile = WhoseProfileEnum.Self;

            //get member color code info
            if (g.Member != null)
            {
                //member color code info
                bool memberTakenTest = ColorCodeHelper.HasMemberCompletedQuiz(this.MemberProfile, g.Brand);
                bool memberHiddenColorCode = ColorCodeHelper.IsMemberColorCodeHidden(this.MemberProfile, g.Brand);
                _MemberQuiz = ColorCodeHelper.GetMemberQuiz(member, g.Brand);
                _MemberColor = _MemberQuiz.PrimaryColor;
                _MemberColorText = ColorCodeHelper.GetFormattedColorText(_MemberColor);
                _UserName = String.IsNullOrEmpty(member.GetUserName(_g.Brand)) ? member.MemberID.ToString() : member.GetUserName(_g.Brand);

                //display appropriate content
                if (g.Member.MemberID == member.MemberID)
                {
                    //member viewing own profile
                    if (memberTakenTest)
                    {
                        phOwnProfileMemberTaken.Visible = true;
                        ChartOwnProfile.LoadChart(member, _MemberQuiz, WhoseProfileEnum.Self);
                        ProfileOwnProfile.LoadProfile(_MemberQuiz);

                        int ccpurchase = g.Member.GetAttributeInt(g.Brand, "ColorAnalysis");
                        bool purchaseEnabled = SettingsManager.GetSettingBool(SettingConstants.ENABLE_COLORCODE_PURCHASE, g.Brand);


                        phComprehensiveAnalysis.Visible = ((ccpurchase == 1) && purchaseEnabled);
                        phComprehensiveAnalysisPurchase.Visible = ((ccpurchase != 1) && purchaseEnabled);


                        //check setting
                        if (memberHiddenColorCode)
                        {
                            _BlockElementCSS = " blockelement";
                            literalHideSetting1.Visible = false;
                            literalShowSetting1.Visible = true;
                        }
                        else
                        {
                            literalHideSetting1.Visible = true;
                            literalShowSetting1.Visible = false;
                        }
                    }
                    else
                    {
                        phOwnProfileMemberNotTaken.Visible = true;
                    }
                }
                else
                {
                    //viewer color code info
                    bool viewerTakenTest = ColorCodeHelper.HasMemberCompletedQuiz(g.Member, g.Brand);
                    _ViewerMemberQuiz = ColorCodeHelper.GetMemberQuiz(g.Member, g.Brand);
                    _ViewerColor = _ViewerMemberQuiz.PrimaryColor;
                    _ViewerColorText = ColorCodeHelper.GetFormattedColorText(_ViewerColor);
                    bool viewerHiddenColorCode = ColorCodeHelper.IsMemberColorCodeHidden(g.Member, g.Brand);


                    if (memberTakenTest && !memberHiddenColorCode)
                    {
                        if (viewerTakenTest)
                        {
                            //both has taken test
                            phBothHasTaken.Visible = true;
                            ChartBothHasTakenMember.LoadChart(member, _MemberQuiz, WhoseProfileEnum.Other);
                            ChartBothHasTakenViewer.LoadChart(g.Member, _ViewerMemberQuiz, WhoseProfileEnum.Self);
                            ProfileBothHasTakenMember.LoadProfile(_MemberQuiz);
                            profileBothHasTakenViewer.LoadProfile(_ViewerMemberQuiz);
                            ColorColorCopyBothHasTaken.LoadColorCodeCopy(_MemberColor, _ViewerColor);
                            //phColorCodeFooter.Visible = true;
                        }
                        else
                        {
                            //only member has taken test
                            phMemberOnlyHasTaken.Visible = true;
                            ChartMemberOnlyHasTaken.LoadChart(member, _MemberQuiz, WhoseProfileEnum.Other);
                            ProfileMemberOnlyHasTaken.LoadProfile(_MemberQuiz);
                            //phColorCodeFooter.Visible = true;
                        }

                        //check setting
                        if (viewerHiddenColorCode)
                            _BlockElementCSS = " blockelement";
                    }
                    else if (viewerTakenTest)
                    {
                        //only viewer has taken test
                        phViewerOnlyHasTaken.Visible = true;

                        //check if viewer has already sent invitation for this session
                        string sentList = g.Session["CCInviteSent"];
                        if (sentList.Contains(_MemberProfile.MemberID.ToString()))
                        {
                            phInvitationAlreadySent.Visible = true;
                        }
                        else
                        {
                            phSendInvitationButtons.Visible = true;
                        }
                    }
                    else
                    {
                        //neither has taken test
                        phNeitherHasTaken.Visible = true;

                    }

                    //set links
                    lnkEmailMe.HRef = ProfileDisplayHelper.GetEmailLink(member.MemberID, true);
                }

            }

        }
        #endregion

        #region Private Methods

        #endregion
    }
}