﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Matchnet.Web.Framework.Util;
using Matchnet.Web.Framework;

namespace Matchnet.Web.Applications.MemberProfile.ProfileTabs30.Controls.Edit
{
    public partial class EditDropdown : BaseEdit
    {
        protected int _CurrentSelectedValue = Constants.NULL_INT;

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        #region Public Methods
        public override void LoadEditControl(XMLProfileData.ProfileDataGroup ProfileDataGroup, XMLProfileData.ProfileDataSection ProfileDataSection, XMLProfileData.ProfileDataItem ProfileDataItem, string JavascriptSectionVarName, System.Web.UI.Control ResourceControl)
        {
            _ProfileDataGroup = ProfileDataGroup;
            _ProfileDataSection = ProfileDataSection;
            _ProfileDataItem = ProfileDataItem;
            _JavascriptSectionVarName = JavascriptSectionVarName;
            _ResourceControl = ResourceControl;

            //populate label
            litEdit.Text = g.GetResource(String.IsNullOrEmpty(ProfileDataItem.EditResourceConstant) ? ProfileDataItem.ResourceConstant : ProfileDataItem.EditResourceConstant, ResourceControl);

            //required indicator
            if (_ProfileDataItem.IsRequired)
            {
                litRequired.Text = g.GetResource("TXT_REQUIRED", _InternalEditResourceControl);
            }

            //populate edit fields
            switch (ProfileDataItem.AttributeName.ToLower())
            {
                case "gendermask":
                    //get current gendermask pref
                    int currentGenderMask = g.Member.GetAttributeInt(g.Brand, "gendermask", 0);

                    //load gendermask options
                    string[] maskValues = ProfileDataItem.EditValues.Split(new char[] { ',' });
                    string[] maskResources = ProfileDataItem.EditResources.Split(new char[] { ',' });
                    ddlEdit.Items.Clear();
                    if (maskValues != null && maskResources != null)
                    {
                        for (int i = 0; i < maskValues.Length; i++)
                        {
                            ddlEdit.Items.Add(new ListItem(g.GetResource(maskResources[i], ResourceControl), maskValues[i]));
                        }
                        ddlEdit.SelectedValue = currentGenderMask.ToString();
                    }
                    break;
                case "desiredmaritalstatus":
                    int selectedDesiredMaritalStatus = g.Member.GetAttributeInt(g.Brand, "desiredmaritalstatus", 0);
                    DataTable dataTableDesiredMaritalStatus = Option.GetOptions(ProfileDataItem.AttributeName, g);
                    ddlEdit.DataSource = dataTableDesiredMaritalStatus;
                    ddlEdit.DataTextField = "Content";
                    ddlEdit.DataValueField = "Value";
                    ddlEdit.DataBind();

                    ddlEdit.SelectedValue = selectedDesiredMaritalStatus.ToString();

                    //don't allow Married to be an option on DesiredMaritalStatus. We're not that kind of site.
                    ListItem marriedItem = ddlEdit.Items.FindByValue("64");
                    if (null != marriedItem)
                    {
                        ddlEdit.Items.Remove(marriedItem);
                    }
                    break;
                case "height":
                    DropDownList dHeight = FrameworkGlobals.CreateHeightList(g, "Height", Conversion.CInt(g.Member.GetAttributeInt(g.Brand, "Height")));
                    foreach (ListItem li in dHeight.Items)
                    {
                        if (!string.IsNullOrEmpty(li.Value) && Matchnet.Conversion.CInt(li.Value) == Constants.NULL_INT)
                        {
                            //do not add, this will break search
                        }
                        else
                        {
                            ddlEdit.Items.Add(li);
                        }
                    }
                    break;
                case "weight":
                    DropDownList dWeight = FrameworkGlobals.CreateWeightList(g, "Weight", Conversion.CInt(g.Member.GetAttributeInt(g.Brand, "Weight")));
                    foreach (ListItem li in dWeight.Items)
                    {
                        if (!string.IsNullOrEmpty(li.Value) && Matchnet.Conversion.CInt(li.Value) == Constants.NULL_INT)
                        {
                            //do not add, this will break search
                        }
                        else
                        {
                            ddlEdit.Items.Add(li);
                        }
                    }
                    break;
                case "morechildrenflag":
                    int selectedMoreChildrenFlag = MemberProfile.GetAttributeInt(g.Brand, ProfileDataItem.AttributeName);
                    DataTable dtMoreChildren = Option.GetMoreChildrenOptions(g, _ResourceControl);
                    ddlEdit.DataSource = dtMoreChildren;
                    ddlEdit.DataTextField = "Content";
                    ddlEdit.DataValueField = "Value";
                    ddlEdit.DataBind();

                    ddlEdit.SelectedValue = selectedMoreChildrenFlag.ToString();
                    break;
                case "desiredmorechildrenflag":
                    int selectedDesiredMoreChildrenFlag = MemberProfile.GetAttributeInt(g.Brand, ProfileDataItem.AttributeName);
                    DataTable dtDesiredMoreChildren = Option.GetDesiredMoreChildrenOptions(g, _ResourceControl);
                    ddlEdit.DataSource = dtDesiredMoreChildren;
                    ddlEdit.DataTextField = "Content";
                    ddlEdit.DataValueField = "Value";
                    ddlEdit.DataBind();

                    ddlEdit.SelectedValue = selectedDesiredMoreChildrenFlag.ToString();
                    break;
                default:
                    int selectedValue = g.Member.GetAttributeInt(g.Brand, ProfileDataItem.AttributeName, 0);
                    DataTable dataTable = Option.GetOptions(ProfileDataItem.AttributeName, g);
                    ddlEdit.DataSource = dataTable;
                    ddlEdit.DataTextField = "Content";
                    ddlEdit.DataValueField = "Value";
                    ddlEdit.DataBind();

                    if (ProfileDataItem.AttributeName.ToLower() == "desiredchildrencount")
                    {
                        //mapping old attribute options (1, 2, 3+) to Yes, (none) to No
                        if (selectedValue == 2)
                        {
                            selectedValue = 64;
                        }
                        else if (selectedValue == 4 || selectedValue == 8 || selectedValue == 16)
                        {
                            selectedValue = 32;
                        }
                    }

                    ddlEdit.SelectedValue = selectedValue.ToString();
                    break;
            }

            hidOriginalValue.Value = ddlEdit.SelectedValue;

            //render JS Edit Item object to contain info on this edit control
            AddEditItemJS("{EditControlID:'" + ddlEdit.ClientID + "',HidOriginalValueID:'" + hidOriginalValue.ClientID + "'}", true, itemErrorContainer.ClientID);
        }


        #endregion
    }
}