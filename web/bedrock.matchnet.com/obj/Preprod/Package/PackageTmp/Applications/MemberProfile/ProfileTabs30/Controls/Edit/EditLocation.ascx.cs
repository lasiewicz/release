﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.Region;
using Matchnet.Web.Framework;

namespace Matchnet.Web.Applications.MemberProfile.ProfileTabs30.Controls.Edit
{
    public partial class EditLocation : BaseEdit
    {
        //for JS ONLY
        protected string _InitialCityParentRegionID = "";
        protected string _AccountRegionID = "";
        protected string _AccountCountryRegionID = "";
        protected string _AccountStateRegionID = "";
        protected string _AccountCityName = "";

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        #region Public Methods
        public override void LoadEditControl(XMLProfileData.ProfileDataGroup ProfileDataGroup, XMLProfileData.ProfileDataSection ProfileDataSection, XMLProfileData.ProfileDataItem ProfileDataItem, string JavascriptSectionVarName, System.Web.UI.Control ResourceControl)
        {
            // This is to avoid duplicate JS objects beacuse EditControl was added to the Profile Complete overlay
            if (OnePageCompleteProfile2.ShowProfileCompleteOverlay(g) &&
                JavascriptSectionVarName != "OnePageCompleteProfile_Obj")
            {
                plcEditLocationObj.Visible = false;
                return;
            }
                

            _ProfileDataGroup = ProfileDataGroup;
            _ProfileDataSection = ProfileDataSection;
            _ProfileDataItem = ProfileDataItem;
            _JavascriptSectionVarName = JavascriptSectionVarName;
            _ResourceControl = ResourceControl;

            //populate label
            litEdit.Text = g.GetResource(String.IsNullOrEmpty(ProfileDataItem.EditResourceConstant) ? ProfileDataItem.ResourceConstant : ProfileDataItem.EditResourceConstant, ResourceControl);
            litLookUpCity.Text = g.GetResource("TXT_LOOK_UP_CITY", ResourceControl);
            lblEditCountry.Text = g.GetResource("TXT_EDIT_COUNTRY", _InternalEditResourceControl);
            lblEditCity.Text = g.GetResource("TXT_EDIT_CITY", _InternalEditResourceControl);
            lblEditZipCode.Text = g.GetResource("TXT_EDIT_ZIPCODE", _InternalEditResourceControl);
            lblEditCityForZip.Text = g.GetResource("TXT_EDIT_CITY", _InternalEditResourceControl);
            lblEditState.Text = g.GetResource("TXT_EDIT_STATE", _InternalEditResourceControl);


            //required indicator
            if (_ProfileDataItem.IsRequired)
            {
                litRequired.Text = g.GetResource("TXT_REQUIRED", _InternalEditResourceControl);
            }

            //get current region
            int RegionID = g.Member.GetAttributeInt(g.Brand, "regionid");
            RegionLanguage currentRegion = null;
            hidRegionID.Value = "0";
            if (RegionID > 0)
            {
                hidRegionID.Value = RegionID.ToString();
                _AccountRegionID = RegionID.ToString();
                currentRegion = RegionSA.Instance.RetrievePopulatedHierarchy(RegionID, g.Brand.Site.LanguageID);

                if (currentRegion != null)
                {
                    _AccountCountryRegionID = currentRegion.CountryRegionID.ToString();
                    _AccountStateRegionID = currentRegion.StateRegionID.ToString();
                    _AccountCityName = currentRegion.CityName;
                }
            }

            //determine whether to initially show location by zip code or city
            if (currentRegion != null && (currentRegion.CountryRegionID == 38 || currentRegion.CountryRegionID == 223))
            {
                pnlEditByStateCity.Attributes.Add("style", "display:none;");
                RegionCollection CitiesByZipCode = RegionSA.Instance.RetrieveCitiesByPostalCode(currentRegion.PostalCode);
                //Do no display City for Zip dropdown if only 1:
                if (CitiesByZipCode.Count <= 1 )
                {
                    pnlEditByCityForZip.Attributes.Add("style", "display:none;");
                }
                ddlEditCityForZip.Items.Clear();
                ddlEditCityForZip.DataSource = CitiesByZipCode;
                ddlEditCityForZip.DataTextField = "Description";
                ddlEditCityForZip.DataValueField = "RegionID";
                ddlEditCityForZip.DataBind();

                if (currentRegion.PostalCodeRegionID != null)
                {
                    FrameworkGlobals.SelectItem(ddlEditCityForZip, currentRegion.PostalCodeRegionID.ToString());
                }

            }
            else
            {
                pnlEditByZipCode.Attributes.Add("style", "display:none;");
                pnlEditByCityForZip.Attributes.Add("style", "display:none;");
            }

            //load countries
            ddlEditCountry.Items.Clear();
            ddlEditCountry.DataSource = RegionSA.Instance.RetrieveCountries(g.Brand.Site.LanguageID);
            ddlEditCountry.DataTextField = "Description";
            ddlEditCountry.DataValueField = "RegionID";
            ddlEditCountry.DataBind();

            if (currentRegion != null)
            {
                FrameworkGlobals.SelectItem(ddlEditCountry, currentRegion.CountryRegionID.ToString());
            }

            //load states/region
            if (currentRegion != null)
            {
                int countryRegionID = currentRegion.CountryRegionID;

                if ((countryRegionID <= 0 || !countryHasStates(countryRegionID)))
                {
                    pnlEditState.Attributes.Add("style", "display:none;");
                    _InitialCityParentRegionID = countryRegionID.ToString();
                }
                else
                {
                    ddlEditState.Items.Clear();
                    RegionCollection stateRegions = RegionSA.Instance.RetrieveChildRegions(countryRegionID, g.Brand.Site.LanguageID);
                    ddlEditState.DataSource = stateRegions;
                    ddlEditState.DataTextField = "Description";
                    ddlEditState.DataValueField = "RegionID";
                    ddlEditState.DataBind();

                    if (currentRegion.StateRegionID > 0)
                    {
                        FrameworkGlobals.SelectItem(ddlEditState, currentRegion.StateRegionID.ToString());
                        _InitialCityParentRegionID = currentRegion.StateRegionID.ToString();
                    }
                }
            }

            //load cities (for initially selected country, if country is not usa or canada)
            if (currentRegion != null)
            {
                txtEditCity.Text = currentRegion.CityName;
            }

            //load zip code
            if (currentRegion != null)
            {
                txtEditZipCode.Text = currentRegion.PostalCode;
            }


            //render JS Edit Item object to contain info on this edit control
            AddEditItemJS("{countryID: '" + ddlEditCountry.ClientID + "',stateID:'" + ddlEditState.ClientID + "',cityID:'" + txtEditCity.ClientID + "',zipcodeID:'" + txtEditZipCode.ClientID + "',cityforzipID:'" + ddlEditCityForZip.ClientID + "'}", true, itemErrorContainer.ClientID);

        }

        #endregion

        #region Private methods
        private bool countryHasStates(Int32 countryRegionID)
        {
            return RegionSA.Instance.RetrieveRegionByID(countryRegionID, g.Brand.Site.LanguageID).ChildrenDepth == 2;
        }

        #endregion
    }
}