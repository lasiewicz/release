﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using Matchnet.Web.Applications.CompatibilityMeter;
using Matchnet.Web.Framework;
using Matchnet.Web.Applications.MemberProfile.ProfileTabs30.XMLProfileData;
using System.Xml.Serialization;
using System.IO;
using Matchnet.Web.Applications.MemberProfile.ProfileTabs30.XMLProfileTabConfig;
using Matchnet.Web.Framework.Ui;

namespace Matchnet.Web.Applications.MemberProfile.ProfileTabs30
{
    /// <summary>
    /// Various utility APIs for profile30
    /// </summary>
    public static class ProfileUtility
    {
        //constants
        public const string ProfilePageName = "View Profile";
        public const string profileNavPoint = "profile";
        public const int MaxDisplayLengthNVDouble = 16;
        public const int MaxDisplayLengthNVSingle = 18;

        public static ProfileDataGroup GetProfileDataGroup(ProfileDataGroupEnum profileDataGroupEnum, int siteID, ContextGlobal g)
        {
            if (profileDataGroupEnum == ProfileDataGroupEnum.None)
                return null;

            ProfileDataGroups profileDataGroups = null;
            ProfileDataGroup profileDataGroup = null;
            string profileDataGroupsCacheKey = "ProfileDataGroups_" + siteID.ToString();

            try
            {
                //check cache
                profileDataGroups = g.Cache[profileDataGroupsCacheKey] as ProfileDataGroups;

                if (profileDataGroups == null)
                {
                    //load from xml
                    string xmlPath = System.Web.HttpContext.Current.Server.MapPath("/Applications/MemberProfile/ProfileTabs30/XMLProfileData/ProfileData_" + siteID.ToString() + ".xml");
                    TextReader textReader = new StreamReader(xmlPath);

                    XmlSerializer deserializer = new XmlSerializer(typeof(ProfileDataGroups));
                    profileDataGroups = deserializer.Deserialize(textReader) as ProfileDataGroups;

                    textReader.Close();

                    //save to cache
                    if (profileDataGroups != null)
                    {
                        g.Cache.Insert(profileDataGroupsCacheKey, profileDataGroups);
                    }

                }

                //get specific data group
                if (profileDataGroups != null)
                {
                    profileDataGroup = profileDataGroups.GetProfileDataGroup(profileDataGroupEnum);
                }
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }

            return profileDataGroup;
        }

        public static ProfileTabConfig GetProfileTabConfig(int siteID, ContextGlobal g, ProfileTabEnum profileTabType)
        {
            ProfileTabConfig profileTabConfig = null;
            ProfileTabConfigs profileTabConfigs = GetProfileTabConfigs(siteID, g);
            if (profileTabConfigs != null && profileTabConfigs.ProfileTabConfigList != null)
            {
                foreach (ProfileTabConfig tabConfig in profileTabConfigs.ProfileTabConfigList)
                {
                    if (tabConfig.ProfileTabType == profileTabType)
                    {
                        profileTabConfig = tabConfig;
                        break;
                    }
                }
            }

            return profileTabConfig;
        }

        public static ProfileTabConfigs GetProfileTabConfigs(int siteID, ContextGlobal g)
        {
            ProfileTabConfigs profileTabConfigs = null;
            string profileTabConfigsCacheKey = "ProfileTabConfigs_" + siteID.ToString();

            try
            {
                //check cache
                profileTabConfigs = g.Cache[profileTabConfigsCacheKey] as ProfileTabConfigs;

                if (profileTabConfigs == null)
                {
                    //load from xml
                    string xmlPath = System.Web.HttpContext.Current.Server.MapPath("/Applications/MemberProfile/ProfileTabs30/XMLProfileTabConfig/Tab_" + siteID.ToString() + ".xml");
                    TextReader textReader = new StreamReader(xmlPath);

                    XmlSerializer deserializer = new XmlSerializer(typeof(ProfileTabConfigs));
                    profileTabConfigs = deserializer.Deserialize(textReader) as ProfileTabConfigs;

                    textReader.Close();

                    //save to cache
                    if (profileTabConfigs != null && profileTabConfigs.ProfileTabConfigList != null)
                    {
                        //only include enabled tabs
                        List<ProfileTabConfig> profileTabConfigEnabledList = new List<ProfileTabConfig>();
                        foreach (ProfileTabConfig ptc in profileTabConfigs.ProfileTabConfigList)
                        {
                            if (ptc.IsEnabled)
                            {
                                profileTabConfigEnabledList.Add(ptc);
                            }
                        }

                        profileTabConfigs.ProfileTabConfigList = profileTabConfigEnabledList;
                        g.Cache.Insert(profileTabConfigsCacheKey, profileTabConfigs);
                    }

                }

            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }



            if (!CompatibilityMeterHandler.IsJMeterV3(g.Brand))
            {
                ProfileTabConfig JMeterTab = profileTabConfigs.ProfileTabConfigList.Find(
                    tab => tab.ProfileTabType == ProfileTabEnum.JMeter
                    );
                if (JMeterTab != null)
                {
                    profileTabConfigs.ProfileTabConfigList.RemoveAt(profileTabConfigs.ProfileTabConfigList.IndexOf(JMeterTab))  ;
                }
            }

            return profileTabConfigs;
        }

        public static ProfileTabConfigs GetMicroProfileTabConfigs(int siteID, ContextGlobal g)
        {
            ProfileTabConfigs profileTabConfigs = null;
            string MicroProfileTabConfigsCacheKey = "MicroProfileTabConfigs_" + siteID.ToString();

            try
            {
                //check cache
                profileTabConfigs = g.Cache[MicroProfileTabConfigsCacheKey] as ProfileTabConfigs;

                if (profileTabConfigs == null)
                {
                    //load from xml
                    string xmlPath = System.Web.HttpContext.Current.Server.MapPath("/Applications/MemberProfile/ProfileTabs30/XMLProfileTabConfig/MicroProfileTabs_" + siteID.ToString() + ".xml");
                    TextReader textReader = new StreamReader(xmlPath);

                    XmlSerializer deserializer = new XmlSerializer(typeof(ProfileTabConfigs));
                    profileTabConfigs = deserializer.Deserialize(textReader) as ProfileTabConfigs;

                    textReader.Close();

                    //save to cache
                    if (profileTabConfigs != null && profileTabConfigs.ProfileTabConfigList != null)
                    {
                        //only include enabled tabs
                        List<ProfileTabConfig> profileTabConfigEnabledList = new List<ProfileTabConfig>();
                        foreach (ProfileTabConfig ptc in profileTabConfigs.ProfileTabConfigList)
                        {
                            if (ptc.IsEnabled)
                                profileTabConfigEnabledList.Add(ptc);
                        }

                        profileTabConfigs.ProfileTabConfigList = profileTabConfigEnabledList;
                        g.Cache.Insert(MicroProfileTabConfigsCacheKey, profileTabConfigs);
                    }

                }

            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }

            return profileTabConfigs;
        }

        public static bool IgnoreMaxLength(string textValue, string officialNoAnswerText)
        {
            bool ignore = false;
            if (textValue.ToLower() == "will tell you later"
                || textValue.ToLower() == officialNoAnswerText)
            {
                ignore = true;
            }

            return ignore;
        }

        public static string GetDateDisplay(DateTime pDate, ContextGlobal g)
        {
            if ((int)Matchnet.Language.Hebrew == g.Brand.Site.LanguageID)
            {
                string month = string.Empty;
                string day = string.Empty;

                if (pDate.Month < 10)
                {
                    month = "0" + pDate.Month;
                }
                else
                {
                    month = pDate.Month.ToString();
                }

                if (pDate.Day < 10)
                {
                    day = "0" + pDate.Day;
                }
                else
                {
                    day = pDate.Day.ToString();
                }

                return day + "/" + month + "/" + pDate.Year;
            }
            else
            {
                return pDate.ToShortDateString();
            }
        }

        #region Test Code
        public static string GetProfileDataGroupsXMLTest()
        {
            ProfileDataGroups pdgs = new ProfileDataGroups();
            pdgs.ProfileDataGroupList = new List<ProfileDataGroup>();

            ProfileDataGroup pdg = new ProfileDataGroup();
            pdg.ProfileDataGroupType = ProfileDataGroupEnum.Basic;
            pdg.ProfileDataSectionList = new List<ProfileDataSection>();
            pdgs.ProfileDataGroupList.Add(pdg);

            ProfileDataSection pds = new ProfileDataSection();
            pds.SectionTitleResourceConstant = "MyResourceTitle1";
            pds.ProfileDataItemList = new List<ProfileDataItem>();
            pdg.ProfileDataSectionList.Add(pds);

            ProfileDataItem pdi = new ProfileDataItem();
            pdi.AttributeName = "attribute1";
            pdi.ResourceConstant = "attributeRC";
            pds.ProfileDataItemList.Add(pdi);

            ProfileDataItem pdi2 = new ProfileDataItem();
            pdi2.AttributeName = "attribute2";
            pdi2.ResourceConstant = "attributeRC";
            pds.ProfileDataItemList.Add(pdi2);

            ProfileDataGroup pdg2 = new ProfileDataGroup();
            pdg2.ProfileDataGroupType = ProfileDataGroupEnum.Dealbreakers;
            pdg2.ProfileDataSectionList = new List<ProfileDataSection>();
            pdgs.ProfileDataGroupList.Add(pdg2);
            pdg2.ProfileDataSectionList.Add(pds);

            System.IO.StringWriter sw = new System.IO.StringWriter();
            System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(typeof(ProfileDataGroups));
            serializer.Serialize(sw, pdgs);

            return sw.ToString();
        }
        public static string GetProfileTabConfigsXMLTest()
        {
            ProfileTabConfigs tabConfigs = new ProfileTabConfigs();
            tabConfigs.ProfileTabConfigList = new List<ProfileTabConfig>();

            ProfileTabConfig tabConfig = new ProfileTabConfig();
            tabConfig.ProfileTabType = ProfileTabEnum.Essays;
            tabConfig.ResourceConstant = "EssayRC";
            tabConfig.IsEnabled = true;
            tabConfig.TabControl = "EssayTab.ascx";
            tabConfig.Omniture = new ProfileTabOmniture() { OmniTabName = "Essay" };
            tabConfigs.ProfileTabConfigList.Add(tabConfig);

            ProfileTabConfig tabConfig2 = new ProfileTabConfig();
            tabConfig2.ProfileTabType = ProfileTabEnum.Icebreakers;
            tabConfig2.ResourceConstant = "IcebreakersRC";
            tabConfig2.IsEnabled = false;
            tabConfig2.Omniture = new ProfileTabOmniture() { OmniTabName = "Icebreaker" };
            tabConfigs.ProfileTabConfigList.Add(tabConfig2);

            System.IO.StringWriter sw = new System.IO.StringWriter();
            System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(typeof(ProfileTabConfigs));
            serializer.Serialize(sw, tabConfigs);

            return sw.ToString();

        }
        #endregion

        #region Setting Related
        /// <summary>
        /// Indicates whether Profile30 redesigned page is enabled
        /// This takes into account beta testing and whether member has beta enabled
        /// </summary>
        /// <returns></returns>
        public static bool IsProfile30Enabled(ContextGlobal g)
        {
            bool result = false;

            try
            {
                //ENABLE_PROFILE30_MASK: 0 (off), 1 (on), 2 (beta-defaultOn), 4 (beta-defaultOff), 8 (a/b test)
                int enableProfile30Mask = Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ENABLE_PROFILE30_MASK", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID));
                if (enableProfile30Mask > 0)
                {
                    //check "On" state
                    if ((enableProfile30Mask & 1) == 1)
                    {
                        result = true;

                        //check beta (only one beta test can be enabled at a time)
                        //note: this only needs to be done for members, not visitors
                        if (g.Member != null)
                        {
                            if ((enableProfile30Mask & 2) == 2)
                            {
                                //default on - opt out approach
                                result = !BetaHelper.HasBetaTest(g, BetaHelper.BetaTestType.ProfileRedesign30_DefaultOn);
                            }
                            else if ((enableProfile30Mask & 4) == 4)
                            {
                                //default off - opt in approach
                                result = BetaHelper.HasBetaTest(g, BetaHelper.BetaTestType.ProfileRedesign30_DefaultOff);
                            }

                            //check a/b
                        }

                    }
                }

            }
            catch (Exception ex)
            {
                //missing setting
                g.ProcessException(ex);
                result = false;
            }

            return result;
        }

        /// <summary>
        /// Indicates whether Profile30 redesigned page will be displayed
        /// </summary>
        /// <returns></returns>
        public static bool IsDisplayingProfile30(ContextGlobal g)
        {
            bool result = false;

            try
            {
                if (g != null && (g.AppPage.ControlName.ToLower() == "viewtabbedprofile20" || g.AppPage.ControlName.ToLower() == "viewprofile") && string.IsNullOrEmpty(HttpContext.Current.Request.QueryString["disableprofile30"]))
                {
                    result = IsProfile30Enabled(g);

                    if (result)
                    {
                        if (!FrameworkGlobals.IsValidMember(g.Member, g.Brand))
                        {
                            result = IsVisitorProfile30Enabled(g);
                        }
                        else if (g.Member.MemberID == ViewProfileTabUtility.GetMemberIdForProfileView(g, true))
                        {
                            //View own profile, need to check setting
                            result = IsProfile30EditEnabled(g);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //missing setting
                g.ProcessException(ex);
                result = false;
            }

            return result;
        }

        public static bool IsProfile30EditEnabled(ContextGlobal g)
        {
            string isEnabled = "false";
            try
            {
                isEnabled = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ENABLE_PROFILE30_EDIT", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID);
            }
            catch (Exception ex)
            {
                //setting missing
                isEnabled = "false";
            }

            if (isEnabled.ToLower().Trim() == "true")
                return true;
            else
                return false;

        }

        public static bool IsVisitorProfile30Enabled(ContextGlobal g)
        {
            string isEnabled = "false";
            try
            {
                isEnabled = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ENABLE_VISITOR_PROFILE30", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID);
            }
            catch (Exception ex)
            {
                //setting missing
                isEnabled = "false";
            }

            if (isEnabled.ToLower().Trim() == "true")
                return true;
            else
                return false;
        }

        public static bool IsHideProfile30RightAdSlotEnabled(ContextGlobal g)
        {
            string isEnabled = "false";
            try
            {
                isEnabled = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("HIDE_PROFILE30_RIGHT_300_250_ADSLOT", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID);
            }
            catch (Exception ex)
            {
                //setting missing
                isEnabled = "false";
            }

            if (isEnabled.ToLower().Trim() == "true")
                return true;
            else
                return false;
        }

        public static bool HideProfile30RightAd(ContextGlobal g)
        {
            bool hide = false;

            if (IsHideProfile30RightAdSlotEnabled(g))
            {
                //must be subscriber and on new profile 3.0 page
                if (g.Member != null && g.Member.IsPayingMember(g.Brand.Site.SiteID) && IsDisplayingProfile30(g))
                    hide = true;
            }

            return hide;
        }
        #endregion
    }



}
