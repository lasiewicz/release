﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ServiceAdapters.Interfaces;
using Matchnet.Member.ValueObjects.Interfaces;
using Matchnet.Member.ValueObjects.MemberDTO;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Managers;
using Matchnet.Web.Framework.Ui;
using Matchnet.Web.Framework.Ui.BasicElements;
using Matchnet.Web.Framework.Util;

namespace Matchnet.Web.Applications.MemberProfile
{
    public class ProfileDisplayHelper
    {
        public const int ESSAY_CHARACTERS_PER_LINE = 20;
        private const string NON_BREAKING_SPACE = " ";

        private ProfileDisplayHelper()
        {
        }

        public static void showMiniProfile(ContextGlobal g, string requestMemberID, MiniProfile aMiniProfile,
            PlaceHolder miniProfilePlaceHolder)
        {
            int viewMemberID = g.RecipientMemberID;
            if (viewMemberID <= 0)
            {
                viewMemberID = Conversion.CInt(requestMemberID);
            }

            if (viewMemberID > 0)
            {
                IMemberDTO member = MemberDTOManager.Instance.GetIMemberDTO(viewMemberID, g, MemberType.Search, MemberLoadFlags.None);

                if ((member != null) && (miniProfilePlaceHolder != null))
                {
                    aMiniProfile.Member = member;
                    aMiniProfile.Transparency = true;
                    miniProfilePlaceHolder.Controls.Add(aMiniProfile);
                }
            }
        }

        public static string GetFormattedEssayText(IMemberDTO pMember
            , ContextGlobal g
            , string pEssayName)
        {
            return GetFormattedEssayText(pMember, g, pEssayName, ESSAY_CHARACTERS_PER_LINE);
        }

        public static string GetFormattedEssayText(IMemberDTO pMember
            , ContextGlobal g
            , string pEssayName
            , int pCharactersPerBlock)
        {
            if (pMember == null)
            {
                return string.Empty;
            }

            string essayText;
            if (g.Member != null)
                essayText = pMember.GetAttributeTextApproved(g.Brand, pEssayName, g.Member.MemberID, g.GetResource("FREETEXT_NOT_APPROVED", null));
            else
                essayText = pMember.GetAttributeTextApproved(g.Brand, pEssayName, g.GetResource("FREETEXT_NOT_APPROVED", null));


            return GetFormattedEssayText(essayText, pCharactersPerBlock);
        }

        public static string GetFormattedEssayText(string essayText, int pCharactersPerBlock)
        {
            int locationOfSpace = 0;
            int startingIndex = 0;
            bool essayFormatted = false;

            StringBuilder builder = new StringBuilder(essayText);
            if (builder.Length > pCharactersPerBlock)
            {	//candidate for run-on text
                while (!essayFormatted)
                {
                    locationOfSpace = builder.ToString().IndexOf(NON_BREAKING_SPACE, startingIndex, pCharactersPerBlock);
                    if (locationOfSpace < 0)
                    {
                        locationOfSpace = builder.ToString().IndexOf("\r\n", startingIndex, pCharactersPerBlock);
                    }

                    if (locationOfSpace < 0)
                    {
                        locationOfSpace = builder.ToString().IndexOf("\n", startingIndex, pCharactersPerBlock);
                    }

                    if (locationOfSpace < 0)
                    {
                        locationOfSpace = builder.ToString().IndexOf("\r", startingIndex, pCharactersPerBlock);
                    }

                    if (locationOfSpace < 0)
                    {
                        //no spaces found or line breaks found, requires insertion of blank space
                        startingIndex += pCharactersPerBlock;
                        builder.Insert(startingIndex, NON_BREAKING_SPACE);
                        startingIndex++;
                    }
                    else
                    {
                        //reset starting position
                        startingIndex = locationOfSpace + 1;
                    }

                    if ((builder.Length - startingIndex) < pCharactersPerBlock)
                    {	// finished with formatting.
                        essayFormatted = true;
                    }
                }
            }
            return builder.ToString();
        }


        /// <summary>
        /// Returns a string for displaying the supplied member's Marital Status and Gender preferences according to the following
        /// Hebrew:
        ///		Woman, Single seeking a Man
        ///	Non-Hebrew:
        ///		Single Woman seeking a Man
        ///		
        ///	These changes have been implemented in accordance with Test Track Issue #11945
        /// </summary>
        /// <param name="pMember"></param>
        /// <param name="g"></param>
        /// <returns></returns>
        public static string GetMaritalStatusSeekingGenderDisplay(IMemberDTO pMember, ContextGlobal g)
        {
            if (pMember == null)
            {
                return string.Empty;
            }

            StringBuilder sb = new StringBuilder();
            int genderMask = pMember.GetAttributeInt(g.Brand, "gendermask");
            string gender = FrameworkGlobals.GetGenderString(genderMask);
            string seeking = FrameworkGlobals.GetSeekingGenderString(genderMask);
            string maritalStatus = GetOptionValue(pMember, g, "MaritalStatus", "MaritalStatus");

            // If this is Glimpse, do not prefix any status.
            if (maritalStatus != null && maritalStatus != String.Empty && g.Brand.Site.Community.CommunityID != (int)WebConstants.COMMUNITY_ID.Glimpse)
            {
                sb.Append(maritalStatus + ", ");
            }

            if (seeking != string.Empty || gender != string.Empty)
            {
                sb.Append(gender + " " + g.GetResource("SEEKING", g) + " " + seeking);
            }

            return sb.ToString();
        }

        public static string GetMaritalStatusSeekingGenderDisplay(IMemberDTO pMember, Brand brand, ResourceManager resourceManager)
        {
            if (pMember == null)
            {
                return string.Empty;
            }

            StringBuilder sb = new StringBuilder();
            int genderMask = pMember.GetAttributeInt(brand, "gendermask");
            string gender = FrameworkGlobals.GetGenderString(genderMask);
            string seeking = FrameworkGlobals.GetSeekingGenderString(genderMask);
            string maritalStatus = GetOptionValue(pMember, "MaritalStatus", resourceManager, brand);

            // If this is Glimpse, do not prefix any status.
            if (maritalStatus != null && maritalStatus != String.Empty && brand.Site.Community.CommunityID != (int)WebConstants.COMMUNITY_ID.Glimpse)
            {
                sb.Append(maritalStatus + ", ");
            }

            if (seeking != string.Empty || gender != string.Empty)
            {
                sb.Append(gender + " " + resourceManager.GetResource(brand, "SEEKING") + " " + seeking);
            }

            return sb.ToString();
        }

        public static string GetSeekingGenderDisplay(IMemberDTO pMember, ContextGlobal g)
        {
            if (pMember == null)
            {
                return string.Empty;
            }

            int genderMask = pMember.GetAttributeInt(g.Brand, "gendermask");
            string seeking = FrameworkGlobals.GetSeekingGenderString(genderMask);

            return seeking;
        }

        public static string GetHeightDisplay(IMemberDTO pMember, ContextGlobal g)
        {
            if (pMember == null)
            {
                return string.Empty;
            }

            string heightAttribute = null;
            string heightDisplay = null;
            heightAttribute = pMember.GetAttributeInt(g.Brand, "Height").ToString();
            if (heightAttribute != null && !"".Equals(heightAttribute) && heightAttribute != Constants.NULL_INT.ToString())
            {
                heightDisplay = FrameworkGlobals.GlobalHeight(g.Brand.Site.CultureInfo.LCID, int.Parse(heightAttribute), g.Brand);
            }
            return heightDisplay;
        }

        public static string GetFormattedAgeLocation(IMemberDTO pMember, FrameworkControl resourceControl, Brand brand, ResourceManager resourceManager)
        {
            try
            {
                StringBuilder age = new StringBuilder();
                age.Append(FrameworkGlobals.GetAge(pMember, brand).ToString());
                if (String.IsNullOrEmpty(age.ToString()))
                {
                    age.Append(GetRegionDisplay(pMember, brand));
                }
                else
                {
                    if (FrameworkGlobals.isHebrewSite(brand))
                    {
                        age.Insert(0, resourceManager.GetResource("TXT_YEARS_OLD", resourceControl) + " ");
                    }
                    else
                    {
                        age.Append(" " + resourceManager.GetResource("TXT_YEARS_OLD", resourceControl));
                    }

                    age.Append(" " + resourceManager.GetResource("TXT_FROM", resourceControl));
                    age.Append((brand.Site.LanguageID == (int)Matchnet.Language.Hebrew ? string.Empty : " ") + GetRegionDisplay(pMember, brand));
                }

                return age.ToString();

            }
            catch (Exception)
            {
                return "";
            }
        }

        public static string GetFormattedAgeLocation(IMemberDTO pMember, Matchnet.Web.Framework.FrameworkControl resourceControl, ContextGlobal g)
        {
            try
            {
                StringBuilder age = new StringBuilder();
                age.Append(Matchnet.Web.Framework.FrameworkGlobals.GetAge(pMember, g.Brand).ToString());
                if (String.IsNullOrEmpty(age.ToString()))
                {
                    age.Append(GetRegionDisplay(pMember, g));
                }
                else
                {
                    if (Matchnet.Web.Framework.FrameworkGlobals.isHebrewSite(g.Brand))
                    {
                        age.Insert(0, g.GetResource("TXT_YEARS_OLD", resourceControl) + " ");
                    }
                    else
                    {
                        age.Append(" " + g.GetResource("TXT_YEARS_OLD", resourceControl));
                    }

                    age.Append(" " + g.GetResource("TXT_FROM", resourceControl));
                    age.Append((g.Brand.Site.LanguageID == (int)Matchnet.Language.Hebrew ? string.Empty : " ") + GetRegionDisplay(pMember, g));
                }

                return age.ToString();

            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
                return "";
            }

        }

        public static string GetMaskContent(IMemberDTO pMemberProfile, ContextGlobal g, string pAttributeName)
        {
            if (pMemberProfile == null)
            {
                return string.Empty;
            }

            int attributeIntValue = pMemberProfile.GetAttributeInt(g.Brand, pAttributeName);

            if (pAttributeName.ToLower() == "desiredchildrencount")
            {
                //mapping old attribute options (1, 2, 3+) to Yes, (none) to No
                if (attributeIntValue == 2)
                {
                    attributeIntValue = 64;
                }
                else if (attributeIntValue == 4 || attributeIntValue == 8 || attributeIntValue == 16)
                {
                    attributeIntValue = 32;
                }
            }
            if (attributeIntValue != Constants.NULL_INT)
            {
                return Option.GetMaskContent(pAttributeName, attributeIntValue, g);
            }

            return null;
        }

        public static string GetMaskContent(IMemberDTO pMemberProfile, Brand brand, string pAttributeName)
        {
            if (pMemberProfile == null)
            {
                return string.Empty;
            }

            int attributeIntValue = pMemberProfile.GetAttributeInt(brand, pAttributeName);

            if (attributeIntValue != Constants.NULL_INT)
            {
                return Option.GetMaskContent(pAttributeName, attributeIntValue, brand);
            }

            return null;
        }

        public static string GetOptionValue(IMemberDTO pMember, ContextGlobal g, string pAttributeLabel, string pOptionLabel)
        {
            if (pMember == null)
            {
                return string.Empty;
            }

            return Option.GetContent(pAttributeLabel, pMember.GetAttributeInt(g.Brand, pAttributeLabel), g);
        }

        public static string GetOptionValue(IMemberDTO member, string attributeName, ResourceManager resourceManager, Brand brand)
        {
            if (member == null)
            {
                return string.Empty;
            }

            return Option.GetContent(attributeName, member.GetAttributeInt(brand, attributeName), resourceManager, brand);
        }

        public static string GetOptionValue(IMemberDTO pMember, Brand brand, string pAttributeLabel, string pOptionLabel)
        {
            if (pMember == null)
            {
                return string.Empty;
            }

            return Option.GetContent(pAttributeLabel, pMember.GetAttributeInt(brand, pAttributeLabel), brand);
        }

        public static string GetRegionDisplay(IMemberDTO pMember, ContextGlobal g)
        {
            return GetRegionDisplay(pMember, g.Brand);
        }

        public static string GetRegionDisplay(IMemberDTO pMember, Brand brand)
        {
            if (pMember == null)
            {
                return string.Empty;
            }

            return FrameworkGlobals.GetRegionString(pMember.GetAttributeInt(brand, "regionid"),
                brand.Site.LanguageID,
                false, true, false);
        }

        public static string GetWeightDisplay(IMemberDTO pMember, ContextGlobal g)
        {
            if (pMember == null)
            {
                return string.Empty;
            }

            string attribute = null;
            string display = null;
            attribute = pMember.GetAttributeInt(g.Brand, "Weight").ToString();
            if (attribute != null && !"".Equals(attribute))
            {
                display = GlobalWeight(g, int.Parse(attribute));
            }
            return display;
        }

        //TODO: Relocate this method to FrameworkGlobals once the item below is resolved.
        private static string GlobalWeight(ContextGlobal g, int pWeightGrams)
        {
            long pounds;
            bool siteUsesPounds = false;

            if (RuntimeSettings.GetSetting("USE_US_WEIGHT", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID) == "true")
            {
                siteUsesPounds = true;
            }

            if (pWeightGrams > 0)
            {
                if (siteUsesPounds || Constants.LOCALE_US_ENGLISH == g.Brand.Site.CultureInfo.LCID)
                {
                    pounds = Matchnet.Lib.Util.Util.GramsToPounds(pWeightGrams);
                    if (pounds > 400 && (g.Brand.Site.Community.CommunityID == (int)WebConstants.COMMUNITY_ID.Glimpse || g.Brand.Site.Community.CommunityID == (int)WebConstants.COMMUNITY_ID.College))
                    {
                        return g.GetResource("X_400_POUNDS_520751", null);
                    }
                    else
                    {
                        return pounds + "&nbsp;" + g.GetResource("POUNDS", null)
                            + "&nbsp;(" + (FrameworkGlobals.RoundWeight(pWeightGrams))
                            + "&nbsp;" + g.GetResource("KG", null) + ")";
                    }
                }
                else
                {
                    // for everyone else that uses kg
                    return FrameworkGlobals.RoundWeight(pWeightGrams) + "&nbsp;" + g.GetResource("KG", null);

                    // TODO: Need a definition for LOCALE_UK_ENGLISH
                    //					if (g.Brand.Site.CultureInfo.LCID == ConstantsTemp.LOCALE_UK_ENGLISH)
                    //					{
                    //						return Util.GramsToStone(pWeightGrams) + " stone";
                    //					}
                    //					else
                    //					{
                    //						return pounds +  "&nbsp;" + g.Translator.Value("POUNDS", g.Brand.Site.LanguageID, g.Brand.BrandID)
                    //							+ "&nbsp;(" + (pWeightGrams / 1000)
                    //							+ "&nbsp;" + g.Translator.Value("KG", g.Brand.Site.LanguageID, g.Brand.BrandID) + ")";
                    //					}
                }
            }
            else
            {
                return string.Empty;
            }
        }

        public static string GetEmailLink(int ViewingMemberID)
        {
            return GetEmailLink(ViewingMemberID, false);
        }

        public static string GetEmailLink(int ViewingMemberID, bool returnToMember)
        {
            string emailLink = "/Applications/Email/Compose.aspx?MemberId=" + ViewingMemberID;

            //tt # 15984
            if (returnToMember)
            {
                emailLink += "&ReturnToMember=True";
            }

            return emailLink;
        }

        /// <summary>
        /// Sets the Email and Tease links on the profile.
        /// Used by other controls (i.e. ProfileHeaderView.ascx, ProfileFooterView.ascx).
        /// NOTE:  Moved from ProfileDisplayHeader and made static so that it can be shared among other controls.
        /// </summary>
        /// <param name="g"></param>
        /// <param name="lnkEmail">A ref to the Email anchor.</param>
        /// <param name="lnkTease">A ref to the Tease anchor.</param>
        public static void SetEmailTeaseLinks(ContextGlobal g, int ViewingMemberID, ref HtmlAnchor lnkEmail, ref HtmlAnchor lnkTease)
        {
            SetEmailTeaseLinks(g, ViewingMemberID, ref lnkEmail, ref lnkTease, false);
        }

        /// <summary>
        /// Sets the Email and Tease links on the profile.
        /// Used by other controls (i.e. ProfileHeaderView.ascx, ProfileFooterView.ascx).
        /// NOTE:  Moved from ProfileDisplayHeader and made static so that it can be shared among other controls.
        /// </summary>
        /// <param name="g"></param>
        /// <param name="lnkEmail">A ref to the Email anchor.</param>
        /// <param name="lnkTease">A ref to the Tease anchor.</param>
        /// <param name="returnToMember">whether to return to the member profi</param>
        public static void SetEmailTeaseLinks(ContextGlobal g, int ViewingMemberID, ref HtmlAnchor lnkEmail, ref HtmlAnchor lnkTease, bool returnToMember)
        {
            lnkEmail.HRef = GetEmailLink(ViewingMemberID, returnToMember);
            lnkTease.HRef = GetFlirtLink(g, ViewingMemberID);
        }

        public static string GetFlirtLink(ContextGlobal g, int ViewingMemberID)
        {
            string flirtLink;

            bool useOverlay = Convert.ToBoolean(RuntimeSettings.GetSetting("USE_FLIRT_OVERLAY", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID));

            if (useOverlay)
            {
                string controlName = g.AppPage == null ? string.Empty : g.AppPage.ControlName.ToLower();
                flirtLink = "/Applications/Email/FlirtCollapsiblePopup.aspx?MemberId=" + ViewingMemberID +
                            "&LayoutTemplateID=7&SkipLayoutOverride=0&ControlName=" + controlName;
            }
            else
            {
                flirtLink = "/Applications/Email/Tease.aspx?MemberId=" + ViewingMemberID;
            }

            return flirtLink;
        }

        public static string GetFlirtLink(ContextGlobal g, int ViewingMemberID, int StartRow)
        {
            string flirtLink;

            flirtLink = GetFlirtLink(g, ViewingMemberID) + "&StartRow=" + StartRow;

            return flirtLink;
        }

        public static void AddFlirtOnClick(ContextGlobal g, ref System.Web.UI.HtmlControls.HtmlAnchor flirtLink)
        {
            bool useOverlay = Convert.ToBoolean(RuntimeSettings.GetSetting("USE_FLIRT_OVERLAY", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID));
            if (useOverlay)
            {
                flirtLink.Attributes.Add("onclick", "spark_launchTeaseDialog(this, '" + g.GetResource("SEND_A_FLIRT") + "', '" + g.GetResource("SEND_A_FLIRT_CLOSE") + "'); return false;");
            }
        }

        public static void AddFlirtOnClick(ContextGlobal g, ref System.Web.UI.WebControls.HyperLink flirtLink)
        {
            bool useOverlay = Convert.ToBoolean(RuntimeSettings.GetSetting("USE_FLIRT_OVERLAY", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID));
            if (useOverlay)
            {
                flirtLink.Attributes.Add("onclick", "spark_launchTeaseDialog(this, '" + g.GetResource("SEND_A_FLIRT") + "', '" + g.GetResource("SEND_A_FLIRT_CLOSE") + "'); return false;");
            }
        }

        /// <summary>
        /// Sets the HotList link on the profile.
        /// Used by other controls (i.e. ProfileHeaderView.ascx, ProfileFooterView.ascx).
        /// NOTE:  Moved from ProfileDisplayHeader and made static so that it can be shared among other controls.
        /// </summary>
        /// <param name="g"></param>
        /// <param name="ViewingMemberID">The MemberID of the MemberProfile being viewed.</param>
        /// <param name="lnkHotList">A ref to the HotList HyperLink.</param>
        /// <param name="txtHotList">A ref to the HotList text.</param>
        public static void SetHotListLink(ContextGlobal g, int ViewingMemberID, ref HyperLink lnkHotList, ref Txt txtHotList)
        {
            lnkHotList.NavigateUrl = string.Format("/Applications/HotList/View.aspx?a=add&MemberID={0}&ToMemberID={1}&FromMemberID={0}",
                g.Session.GetInt(WebConstants.SESSION_PROPERTY_NAME_MEMBERID),
                ViewingMemberID);
            txtHotList.ResourceConstant = "PRO_HOT_LIST";
        }

        public static void SetECardLink(ContextGlobal g, string userName, HtmlAnchor lnkECard)
        {
            string page = "cards/categories.html?to=" + userName + "&return=1";
            lnkECard.HRef = FrameworkGlobals.BuildConnectFrameworkLink(page, g, true);
        }

        /// <summary>
        /// Sets the HotList link on the profile.
        /// Used by other controls (i.e. ProfileHeaderView.ascx, ProfileFooterView.ascx).
        /// NOTE:  Moved from ProfileDisplayHeader and made static so that it can be shared among other controls.
        /// </summary>
        /// <param name="ViewingMemberID">The MemberID of the MemberProfile being viewed.</param>
        /// <param name="lnkSendToFriend">A ref to the SendToFriend HyperLink.</param>
        public static string SetSendToFriendLink(int ViewingMemberID)
        {
            return "/Applications/SendToFriend/SendToFriend.aspx?MemberID=" + ViewingMemberID;
        }

        /// <summary>
        /// Gets additional URL params needed for building direct links that goes away from profile pages, but will circle back to profile.
        /// These params are needed on profile pages to build the next profile link.
        /// </summary>
        /// <returns></returns>
        public static string GetURLParamsNeededByNextProfile()
        {
            //add other querystring params to links
            StringBuilder urlParams = new StringBuilder();

            //Entrypoint - specifies what collection of results to use
            if (System.Web.HttpContext.Current.Request[WebConstants.URL_PARAMETER_NAME_ENTRYPOINT] != null)
                urlParams.Append("&" + WebConstants.URL_PARAMETER_NAME_ENTRYPOINT + "=" + System.Web.HttpContext.Current.Request[WebConstants.URL_PARAMETER_NAME_ENTRYPOINT]);

            //ordinal - specifies the index in the results they were looking at
            if (System.Web.HttpContext.Current.Request[WebConstants.URL_PARAMETER_NAME_ORDINAL] != null)
                urlParams.Append("&" + WebConstants.URL_PARAMETER_NAME_ORDINAL + "=" + System.Web.HttpContext.Current.Request[WebConstants.URL_PARAMETER_NAME_ORDINAL]);

            //persist layout template - specifies whether to use the layouttemplateID in url, if any
            if (System.Web.HttpContext.Current.Request["PersistLayoutTemplate"] != null)
                urlParams.Append("&" + "PersistLayoutTemplate=" + System.Web.HttpContext.Current.Request["PersistLayoutTemplate"]);

            //layouttemplateid
            if (System.Web.HttpContext.Current.Request["LayoutTemplateID"] != null)
                urlParams.Append("&" + "LayoutTemplateID=" + System.Web.HttpContext.Current.Request["LayoutTemplateID"]);

            //hotlist categoryID to use if their entrypoint was for a hotlist
            if (System.Web.HttpContext.Current.Request["HotListCategoryID"] != null)
                urlParams.Append("&" + "HotListCategoryID=" + System.Web.HttpContext.Current.Request["HotListCategoryID"]);

            //MOL - member online parameters, if they came from mol page
            MOCollection moCol = BreadCrumbHelper.AssembleMOCollection(System.Web.HttpContext.Current.Request);
            Matchnet.Content.ServiceAdapters.Links.ParameterCollection pCol = BreadCrumbHelper.AddMOLParameters(new Matchnet.Content.ServiceAdapters.Links.ParameterCollection(), moCol);
            string urlParamsString = "?" + urlParams.ToString(); //add temporary question mark to be used in AddParameters()
            urlParamsString = pCol.AddParameters(urlParamsString);

            if (IsReversSearchRequest()) urlParamsString = AppendReverseSearchParameters(urlParamsString);

            return urlParamsString.Replace("?", String.Empty);

        }

        private static String AppendReverseSearchParameters(String url)
        {
            var request = HttpContext.Current.Request;

            var viewProfileUrl = url;

            Int32 page;
            if ((Int32.TryParse(request["rp"], out page) == false &&
                Int32.TryParse(request["pg"], out page) == false) ||
                page < 1) page = 1;

            viewProfileUrl += "&pg=" + page;

            viewProfileUrl += "&rp=" + page;

            if (String.IsNullOrEmpty(request["sl"]) == false)
            {
                viewProfileUrl += "&sl=" + request["sl"];
            }

            if (String.IsNullOrEmpty(request["op"]) == false)
            {
                viewProfileUrl += "&op=" + request["op"];
            }

            return viewProfileUrl;
        }

        private static Boolean IsReversSearchRequest()
        {
            var request = HttpContext.Current.Request;

            Int32 entryPoint;

            return request != null && Int32.TryParse(request["EntryPoint"], out entryPoint) &&
                   entryPoint == (Int32)BreadCrumbHelper.EntryPoint.ReverseSearchResults;
        }


        public static string GetSpotlightMatchString(ContextGlobal g, IMemberDTO member)
        {

            List<string> attrs = new List<string>();
            attrs.Add("EntertainmentLocation");
            attrs.Add("LeisureActivity");
            attrs.Add("Cuisine");
            attrs.Add("Music");
            attrs.Add("Reading");
            attrs.Add("PhysicalActivity");
            attrs.Add("Pets");
            string matches = FrameworkGlobals.MatchAttributeOptions(g, member, attrs);
            return matches;

        }

        public static string GetGender(IMemberDTO pMember, ContextGlobal g, FrameworkControl recourcecontrol)
        {
            if (pMember == null)
            {
                return string.Empty;
            }

            StringBuilder sb = new StringBuilder();
            int genderMask = pMember.GetAttributeInt(g.Brand, "gendermask");
            string gender = FrameworkGlobals.GetGenderString(genderMask, g, recourcecontrol);

            return gender;
        }
    }
}