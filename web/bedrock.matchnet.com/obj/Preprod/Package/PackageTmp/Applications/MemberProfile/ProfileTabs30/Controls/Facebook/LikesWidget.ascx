﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LikesWidget.ascx.cs" Inherits="Matchnet.Web.Applications.MemberProfile.ProfileTabs30.Controls.Facebook.LikesWidget" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<div id="likesWidgetControlContainer" runat="server">
<div id="blockDiv" runat="server">
<asp:PlaceHolder ID="phWidgetNonConnected" runat="server" Visible="false">
<div id="fbNonConnectedPrompt" runat="server" style="display:none;" class="fb-likes-widget-header">
    <h2 class="float-inside"><mn:Txt ID="fbLikesWidgetHeaderTitle" ResourceConstant="TXT_FB_LIKES_WIDGET_HEADER_TITLE" runat="server" /></h2> 
    <div class="rel-layer-container float-inside">
        <a href="#" title="learn more" class="non-bold float-inside" rel="click"><mn:Txt ID="txtLearnMore" ResourceConstant="TXT_LEARN_MORE" runat="server" /></a>
	    <div class="rel-layer-div">
			<p class="float-outside"><a href="#" class="click-close"><mn:Txt runat="server" id="Txt2" ResourceConstant="TXT_HELP_LAYER_CLOSE_LINK" /></a></p>
		    <h5><mn:txt runat="server" id="txtKeywordTooltipHeader" ResourceConstant="TXT_FB_LIKES_TOOLTIP_HEADER" /></h5>
            <mn:txt runat="server" id="txtKeywordTooltip" ResourceConstant="TXT_FB_LIKES_TOOLTIP_BODY" />
	    </div>
	</div>
    <p class="clear-both"><mn:Txt ID="fbLikesWidgetHeaderBody" ResourceConstant="TXT_FB_LIKES_WIDGET_HEADER_BODY_LIKES_ONLY" runat="server" /></p><%-- after fb photo turned on => 'TXT_FB_LIKES_WIDGET_HEADER_BODY' --%>
    <a id="lnkFBConnect" href="#" class="btn-fb" runat="server"><span class="spr s-icon-fb-sq-sm"></span><mn:Txt ID="btnFbLikesWidgetHeader" ResourceConstant="BTN_FB_LIKES_WIDGET_HEADER" runat="server" /></a>
</div>
</asp:PlaceHolder>
<asp:PlaceHolder ID="phWidgetNoFBData" runat="server" Visible="false">
<div id="fbNoFBData" runat="server" style="display:none;" class="fb-likes-widget-header">
    <%--Widget: CONNECTED - NO FB DATA --%>
    <h2 class="float-inside"><mn:Txt ID="Txt7" ResourceConstant="TXT_FB_LIKES_WIDGET_HEADER_TITLE_NO_FB_DATA" runat="server" /></h2>
    <div class="rel-layer-container float-inside">
        <a href="#" title="learn more" class="non-bold float-inside" rel="click"><mn:Txt ID="txt8" ResourceConstant="TXT_LEARN_MORE" runat="server" /></a>
	    <div class="rel-layer-div">
			<p class="float-outside"><a href="#" class="click-close"><mn:Txt runat="server" id="txt9" ResourceConstant="TXT_HELP_LAYER_CLOSE_LINK" /></a></p>
		    <h5><mn:Txt runat="server" id="txt10" ResourceConstant="TXT_FB_LIKES_TOOLTIP_HEADER" /></h5>
            <mn:Txt runat="server" id="txt11" ResourceConstant="TXT_FB_LIKES_TOOLTIP_BODY" />
	    </div>
	</div>
    <a id="lnkFBLogoutNoFBData" href="#" class="fb-logout" style="display:none;" title="<%=g.GetResource("TXT_FB_LOGOUT_LINK", this)%>"><mn:Txt ID="txt35" ResourceConstant="TXT_FB_LOGOUT_LINK" runat="server" /></a>
    <h3 class="status"><mn:Txt ID="txtStatus" ResourceConstant="TXT_STATUS" runat="server" /> <mn:Txt ID="status" ResourceConstant="TXT_NO_DATA" runat="server" /></h3>
    <p class="clear-both"><mn:Txt ID="Txt12" ResourceConstant="TXT_FB_LIKES_WIDGET_HEADER_BODY_NO_FB_DATA" runat="server" /></p>
    <a id="lnkFBConnectNoFBData" href="#" class="btn-fb" runat="server"><span class="spr s-icon-fb-sq-sm"></span><mn:Txt ID="btnFbLikesWidgetHeaderNoData" ResourceConstant="BTN_FB_LIKES_WIDGET_HEADER_NO_FB_DATA" runat="server" /></a>
</div>
</asp:PlaceHolder>
<asp:PlaceHolder ID="phWidgetNoSavedData" runat="server" Visible="false">
<div id="fbNoSavedData" runat="server" style="display:none;" class="fb-likes-widget-header">
    <%--Widget: CONNECTED- NO SAVED DATA / NOT LOGGED IN --%>
    <h2 class="float-inside"><mn:Txt ID="fbLikesWidgetHeaderTitleNoData" ResourceConstant="TXT_FB_LIKES_WIDGET_HEADER_TITLE_NO_SAVED_DATA" runat="server" /></h2>
    <div class="rel-layer-container float-inside">
        <a href="#" title="learn more" class="non-bold float-inside" rel="click"><mn:Txt ID="txt1" ResourceConstant="TXT_LEARN_MORE" runat="server" /></a>
	    <div class="rel-layer-div">
			<p class="float-outside"><a href="#" class="click-close"><mn:Txt runat="server" id="txt3" ResourceConstant="TXT_HELP_LAYER_CLOSE_LINK" /></a></p>
		    <h5><mn:Txt runat="server" id="txt4" ResourceConstant="TXT_FB_LIKES_TOOLTIP_HEADER" /></h5>
            <mn:Txt runat="server" id="txt5" ResourceConstant="TXT_FB_LIKES_TOOLTIP_BODY" />
	    </div>
	</div>
    <a id="lnkFBLogoutNoSavedData" href="#" class="fb-logout" style="display:none;" title="<%=g.GetResource("TXT_FB_LOGOUT_LINK", this)%>"><mn:Txt ID="txt34" ResourceConstant="TXT_FB_LOGOUT_LINK" runat="server" /></a>
    <p class="clear-both"><mn:Txt ID="fbLikesWidgetHeaderBodyNoData" ResourceConstant="TXT_FB_LIKES_WIDGET_HEADER_BODY_NO_SAVED_DATA" runat="server" /></p>
    <a id="lnkFBConnectNoSavedData" href="#" class="btn-fb" runat="server"><span class="spr s-icon-fb-sq-sm"></span><mn:Txt ID="Txt6" ResourceConstant="BTN_FB_LIKES_WIDGET_HEADER_NO_SAVED_DATA" runat="server" /></a>
</div>
</asp:PlaceHolder>

<asp:PlaceHolder ID="phWidgetHasData" runat="server" Visible="false">
<%-- ******* VIEW MODE ******* --%>
<div id="fbData" runat="server" style="display:none;" class="fb-likes-widget-wrapper clearfix">
    <%--Widget: Connected - FB Icon - Status bar - goes here--%>
    <h3 class="title float-inside"><mn:Txt ID="txt24" ResourceConstant="TXT_FAVORITES" runat="server" /></h3>
    <span class="spr s-icon-fb-sm float-inside"></span>
    <div class="rel-layer-container float-inside">
        <a href="#" title="<%=g.GetResource("TXT_LEARN_MORE", this)%>" class="non-bold float-inside" rel="click"><mn:Txt ID="txt25" ResourceConstant="TXT_LEARN_MORE" runat="server" /></a>
	    <div class="rel-layer-div">
			<p class="float-outside"><a href="#" class="click-close"><mn:Txt runat="server" id="txt26" ResourceConstant="TXT_HELP_LAYER_CLOSE_LINK" /></a></p>
		    <h5><mn:Txt runat="server" id="txt27" ResourceConstant="TXT_FB_LIKES_TOOLTIP_HEADER" /></h5>
            <mn:Txt runat="server" id="txt28" ResourceConstant="TXT_FB_LIKES_TOOLTIP_FOR_TITLE" />
	    </div>
	</div>
    <a id="lnkFBLogoutHasData" href="#" class="fb-logout" style="display:none;" title="<%=g.GetResource("TXT_FB_LOGOUT_LINK", this)%>"><mn:Txt ID="txtFbLogoutLink" ResourceConstant="TXT_FB_LOGOUT_LINK" runat="server" /></a>
    <%-- Command Bar - Status, Control --%>
    <asp:PlaceHolder ID="phPublishStatus" runat="server" Visible="false">
    <div class="fb-likes-widget-command">
        <div id="fbNotPublishedStatusContainer" style="display:none;" runat="server">
            <h4 class="status"><mn:Txt ID="txt21" ResourceConstant="TXT_STATUS" runat="server" /> <mn:Txt ID="Txt22" ResourceConstant="TXT_NOT_PUBLISHED" runat="server" /></h4>
            <p class="desc"><mn:Txt ID="txt31" ResourceConstant="TXT_NOTPUBLISHED_MESSAGE" runat="server" /></p>
        </div>
        <div id="fbPublishedStatusContainer" style="display:none;" runat="server">
            <h4 class="status"><mn:Txt ID="txt29" ResourceConstant="TXT_STATUS" runat="server" /> <mn:Txt ID="Txt30" ResourceConstant="TXT_PUBLISHED" runat="server" /></h4>
            <p class="desc"><mn:Txt ID="txt32" ResourceConstant="TXT_PUBLISHED_MESSAGE" runat="server" /></p>
        </div>
        <div class="setting">
            <h5><mn:Txt ID="txt23" ResourceConstant="TXT_FB_LIKES_SETTINGS" runat="server" /></h5>
            <asp:RadioButton ID="radioFBPublishShow" GroupName="fbPublishOption" runat="server" value="show" />
            <asp:RadioButton ID="radioFBPublishHide" GroupName="fbPublishOption" runat="server" value="hide" />
            <input id="btnFBSaveStatus" type="button" value="<%=g.GetResource("BTN_EDIT_SAVE", this)%>" class="btn primary" />
        </div>
    </div>
    </asp:PlaceHolder>

    <div id="fbPendingApproval" style="display:none;" class="notification">
        <span class="spr s-icon-page-message"></span> <mn:Txt ID="txt36" ResourceConstant="TXT_PENDING_APPROVAL" runat="server" />
    </div>
    <%--<div id="fbLikesView" class="prof-edit-region prof-edit-inline prof-edit-active">--%>
    <div id="fbLikesView" class="prof-edit-region prof-edit-inline">
        <div class="prof-edit-button">
            <button id="lnkFBConnectData" type="button" runat="server"><mn:Txt ID="txt33" ResourceConstant="TXT_CLICK_TO_EDIT" runat="server" /></button>
        </div>
        <!--View Likes goes here-->
        <ul id="user-info-all" class="fb-likes-widget view"></ul>
    </div>
</div>
</asp:PlaceHolder>

<asp:PlaceHolder ID="phWidgetHasDataEdit" runat="server" Visible="false">
<%-- ******* EDIT MODE ******* --%>
<div id="fbDataEdit" runat="server" style="display:none;" class="fb-likes-widget-wrapper clearfix">
    <%--Widget: Connected - Edit MODE header with Save button goes here--%>
    <h3 class="title float-inside"><mn:Txt ID="txtFavorites" ResourceConstant="TXT_FAVORITES" runat="server" /></h3>
    <span class="spr s-icon-fb-sm float-inside"></span>
    <div class="rel-layer-container float-inside">
        <a href="#" title="<%=g.GetResource("TXT_LEARN_MORE", this)%>" class="non-bold float-inside" rel="click"><mn:Txt ID="txt13" ResourceConstant="TXT_LEARN_MORE" runat="server" /></a>
	    <div class="rel-layer-div">
			<p class="float-outside"><a href="#" class="click-close"><mn:Txt runat="server" id="txt14" ResourceConstant="TXT_HELP_LAYER_CLOSE_LINK" /></a></p>
		    <h5><mn:Txt runat="server" id="txt15" ResourceConstant="TXT_FB_LIKES_TOOLTIP_HEADER" /></h5>
            <mn:Txt runat="server" id="txt16" ResourceConstant="TXT_FB_LIKES_TOOLTIP_FOR_TITLE" />
	    </div>
	</div>

    <%--Intro copy--%>
    <div class="fb-likes-widget-intro clear-both">
        <mn:Txt ID="txtEditIntro" ResourceConstant="TXT_FB_LIKES_WIDGET_EDIT_INTRO" runat="server" />
        <div class="rel-layer-container">
            <a href="#" title="More Info" class="non-bold float-inside" rel="click"><mn:Txt ID="txt17" ResourceConstant="TXT_MORE_INFO" runat="server" /></a>
	        <div class="rel-layer-div">
			    <p class="float-outside"><a href="#" class="click-close"><mn:Txt runat="server" id="txt18" ResourceConstant="TXT_HELP_LAYER_CLOSE_LINK" /></a></p>
		        <h5><mn:Txt runat="server" id="txt19" ResourceConstant="TXT_FB_LIKES_TOOLTIP_EDIT_HEADER" /></h5>
                <mn:Txt runat="server" id="txt20" ResourceConstant="TXT_FB_LIKES_TOOLTIP_EDIT_BODY" />
	        </div>
	    </div>
    </div>
    
    <div class="inline-edit">
        <blockquote class="notice">
            <mn:Txt ID="txtEditNotice" ResourceConstant="TXT_FB_LIKES_WIDGET_EDIT_NOTICE" runat="server" />
        </blockquote>
        <%--Submit--%>
        <mn2:FrameworkButton ID="btnEditSaveTop" runat="server" CssClass="btn primary float-outside" ResourceConstant="BTN_EDIT_SAVE" UseSubmitBehavior="false" OnClientClick="return false;" />
        <%--Cancel--%>
        <mn2:FrameworkButton ID="btnEditCancelTop" runat="server" CssClass="btn secondary cancel" ResourceConstant="BTN_EDIT_CANCEL" UseSubmitBehavior="false" OnClientClick="return false;" />
        <div id="editHasDataLoginCancelPromptContainer" style="display:none;" class="fb-likes-widget-command">
            <!--cancel login prompt message-->
            <mn:Txt ID="txtEditHasDataLoginCancel" ResourceConstant="TXT_FBLIKE_LOGIN_CANCEL_HAS_DATA_EDIT_PROMPT" runat="server" />
        </div>

        <!--Edit Likes goes here-->
        <ul id="user-info-all-edit" class="fb-likes-widget edit clearfix"></ul>

        <%--Submit--%>
        <mn2:FrameworkButton ID="btnEditSave" runat="server" CssClass="btn primary float-outside" ResourceConstant="BTN_EDIT_SAVE" UseSubmitBehavior="false" OnClientClick="return false;" />
        <%--Cancel--%>
        <mn2:FrameworkButton ID="btnEditCancel" runat="server" CssClass="btn secondary cancel" ResourceConstant="BTN_EDIT_CANCEL" UseSubmitBehavior="false" OnClientClick="return false;" />
    </div>
</div>
</asp:PlaceHolder>
</div>
</div>
<script type="text/javascript">

    //initialize objects
    var fbLikeManager = new FBLikeManager();
    fbLikeManager.isVerbose = true;
    fbLikeManager.memberID = <%=ProfileMemberID%>;
    fbLikeManager.fbConnectStatusID = <%=FacebookConnectStatusID %>;
    fbLikeManager.fbHasSavedLikes = <%=HasSavedLikes.ToString().ToLower()%>;
    fbLikeManager.fbLikesPerRow = <%=LikesPerRow.ToString()%>;
    fbLikeManager.likesWidgetControlContainerID = '<%=likesWidgetControlContainer.ClientID%>';
    fbLikeManager.likesContainerID = 'user-info-all';
    fbLikeManager.isOwnProfile = <%=IsOwnProfile.ToString().ToLower()%>;
    fbLikeManager.connectStatusIDNotConnected = <%=(int)(Matchnet.Web.Framework.Managers.FacebookManager.FacebookConnectStatus.NonConnected)%>;
    fbLikeManager.connectStatusIDNoFBData = <%=(int)(Matchnet.Web.Framework.Managers.FacebookManager.FacebookConnectStatus.ConnectedNoFBData)%>;
    fbLikeManager.connectStatusIDNoSavedData = <%=(int)(Matchnet.Web.Framework.Managers.FacebookManager.FacebookConnectStatus.ConnectedNoSavedData)%>;
    fbLikeManager.connectStatusIDShowData = <%=(int)(Matchnet.Web.Framework.Managers.FacebookManager.FacebookConnectStatus.ConnectedShowData)%>;
    fbLikeManager.connectStatusIDHideData = <%=(int)(Matchnet.Web.Framework.Managers.FacebookManager.FacebookConnectStatus.ConnectedHideData)%>;
    fbLikeManager.widgetHasDataContainerID = '<%=fbData.ClientID%>';
    fbLikeManager.blockDivId = '<%=blockDiv.ClientID %>';
    fbLikeManager.scrollToDiv = $j("a[href*='#tab']:first");
    fbLikeManager.tabContainerId = '<%=TabContentContainerID%>';
    fbLikeManager.textLikesCollapse = '<%=GetJSEncodedResource("TXT_LIKES_COLLAPSE")%>';
    fbLikeManager.textLikesExpand = '<%=GetJSEncodedResource("TXT_LIKES_EXPAND")%>';
    fbLikeManager.textCategoriesCollapse = '<%=GetJSEncodedResource("TXT_CATEGORIES_COLLAPSE")%>';
    fbLikeManager.textCategoriesExpand = '<%=GetJSEncodedResource("TXT_CATEGORIES_EXPAND")%>';
</script>

<asp:PlaceHolder ID="phFBLikeEditScripts" runat="server" Visible="false">
<script type="text/javascript">
    if (fbLikeManager.isOwnProfile == true){
        //support for edit only
        fbLikeManager.widgetNoFBDataContainerID = '<%=fbNoFBData.ClientID%>';
        fbLikeManager.widgetNonConnectedContainerID = '<%=fbNonConnectedPrompt.ClientID%>';
        fbLikeManager.widgetNoSavedDataContainerID = '<%=fbNoSavedData.ClientID%>';
        fbLikeManager.widgetHasDataEditContainerID = '<%=fbDataEdit.ClientID%>';
        fbLikeManager.pendingApprovalContainerID = 'fbPendingApproval';
        fbLikeManager.likesRegionContainerEditID = 'fbLikesView';
        fbLikeManager.likesContainerEditID = 'user-info-all-edit';
        fbLikeManager.editHasDataLoginCancelPromptContainerID = 'editHasDataLoginCancelPromptContainer';
        fbLikeManager.radioPublishID = '<%=radioFBPublishShow.ClientID%>';
        fbLikeManager.fbNotPublishedStatusContainerID = '<%=fbNotPublishedStatusContainer.ClientID%>';
        fbLikeManager.fbPublishedStatusContainerID = '<%=fbPublishedStatusContainer.ClientID%>';
        fbLikeManager.fbLogoutLinkIDList = ['lnkFBLogoutHasData','lnkFBLogoutNoFBData' ,'lnkFBLogoutNoSavedData'];
        fbLikeManager.textLogoutMessage = '<%=GetJSEncodedResource("TXT_FB_LOGOUT_MESSAGE")%>';

        $j('#<%=lnkFBConnect.ClientID %>').bind('click', function (event) {
            event.preventDefault();
            fbLikeManager.fbLikesConnect();
        });

        $j('#<%=lnkFBConnectNoFBData.ClientID %>').bind('click', function (event) {
            event.preventDefault();
            fbLikeManager.fbLikesConnect();
        });

        $j('#<%=lnkFBConnectNoSavedData.ClientID %>').bind('click', function (event) {
            event.preventDefault();
            fbLikeManager.fbLikesConnect();
        });

        $j('#fbLikesView').bind('click', function (event) {
            event.preventDefault();
            fbLikeManager.fbLikesConnect();
        });

        $j('#<%=btnEditSave.ClientID %>, #<%=btnEditSaveTop.ClientID %>').bind('click', function (event) {
            event.preventDefault();
            fbLikeManager.processSaveLikes();
        });

        $j('#<%=btnEditCancel.ClientID %>, #<%=btnEditCancelTop.ClientID %>').bind('click', function (event) {
            event.preventDefault();
            fbLikeManager.loadView();
            fbLikeManager.GoToByScroll();
        });

        $j('#btnFBSaveStatus').bind('click', function (event) {
            event.preventDefault();
            fbLikeManager.changePublishStatusClick();
        });

        if (fbLikeManager.fbLogoutLinkIDList != null){
            for(var i = 0; i < fbLikeManager.fbLogoutLinkIDList.length; i++) {
                $j('#' + fbLikeManager.fbLogoutLinkIDList[i]).bind('click', function (event) {
                    event.preventDefault();
                    fbLikeManager.fbLogout();
                });
            }
        }

        
    }
</script>
</asp:PlaceHolder>

<script type="text/javascript">
    //generated category and saved likes
    <asp:Literal ID="litFBLikesData" runat="server"></asp:Literal>

    $j(function () {
        //Try rendering saved likes items on client side, may move this to server side if needed
        if (fbLikeManager.fbHasSavedLikes == true){
            fbLikeManager.loadView();
        }        

        //Check if user is already logged into FB and show logout link
        if (spark.facebook.fbInitialized == true){
            fbLikeManager.fbLikesConnectStatusInitialize();
        }
        else{
            //try twice, up to a total of 3 seconds
            window.setTimeout(
                function(){
                    if (spark.facebook.fbInitialized == true){
                        fbLikeManager.fbLikesConnectStatusInitialize();
                    }
                    else{
                        window.setTimeout(
                            function(){
                                if (spark.facebook.fbInitialized == true){
                                    fbLikeManager.fbLikesConnectStatusInitialize();
                                }
                            }, 1500);
                    }
                }, 1500);
        }
    });
</script>
