﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.Member.ValueObjects.Interfaces;
using Matchnet.Web.Framework.Ui;

namespace Matchnet.Web.Applications.MemberProfile.ProfileTabs30
{
    /// <summary>
    /// Base class for providing common implementation and interfaces for the various Profile Containers,
    /// each of which organizes and displays content differently
    /// </summary>
    public class BaseProfileTemplate : BaseProfile
    {
        public ProfileTabEnum ActiveTab { get; set; }
        public BreadCrumbHelper.EntryPoint entryPoint { get; set; }

        #region Public Methods

        /// <summary>
        /// Loads the member's profile information; Derived classes should override this with specific implementation.
        /// </summary>
        /// <param name="member"></param>
        /// <param name="activeTab"></param>
        public virtual void LoadProfile(IMemberDTO member, ProfileTabEnum activeTab)
        {
        }

        #endregion
        
    }
}
