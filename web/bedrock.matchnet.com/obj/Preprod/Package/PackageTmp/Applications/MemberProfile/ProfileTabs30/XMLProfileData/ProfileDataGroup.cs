﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace Matchnet.Web.Applications.MemberProfile.ProfileTabs30.XMLProfileData
{
    /// <summary>
    /// This class represents a single profile data group, which may contain one or more data sections.
    /// It is deserialized from a subset of xml defined in the ProfileData_SiteID.xml files.
    /// </summary>
    [Serializable]
    public class ProfileDataGroup
    {
        [XmlAttribute("GroupType")]
        public ProfileDataGroupEnum ProfileDataGroupType { get; set; }
        [XmlAttribute()]
        public string EditRefreshIncludeGroupType { get; set; } //comma-delimited list of data groups that should also be refreshed when this group is edited
        [XmlElement("Section")]
        public List<ProfileDataSection> ProfileDataSectionList { get; set; }

        #region Public Methods
        public ProfileDataSection GetProfileDataSection(ProfileDataSectionEnum profileDataSectionName)
        {
            ProfileDataSection dataSection = null;
            if (ProfileDataSectionList != null)
            {
                foreach (ProfileDataSection pds in ProfileDataSectionList)
                {
                    if (pds.ProfileDataSectionName == profileDataSectionName)
                    {
                        return pds;
                    }
                }
            }

            return dataSection;
        }

        public List<ProfileDataGroupEnum> GetEditRefreshIncludeGroupType()
        {
            List<ProfileDataGroupEnum> listDataGroups = new List<ProfileDataGroupEnum>();

            if (!string.IsNullOrEmpty(EditRefreshIncludeGroupType))
            {
                string[] groupTypes = EditRefreshIncludeGroupType.Split(new char[] { ',' });
                foreach (string s in groupTypes)
                {
                    listDataGroups.Add((ProfileDataGroupEnum)Enum.Parse(typeof(ProfileDataGroupEnum), s.Trim()));
                }
            }

            return listDataGroups;

        }
        #endregion
    }
}
