﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RegistrationWelcomeContent.ascx.cs" Inherits="Matchnet.Web.Applications.MemberProfile.RegistrationWelcomeContent" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="uc1" Namespace="Matchnet.Web.Framework.Ui" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="uc2" TagName="PieChart" Src="~/Applications/ColorCode/Controls/Analysis/PieChart.ascx" %>


<asp:PlaceHolder ID="phMiniProfile" runat="server" />

<h1><mn:Txt runat="server" ID="txtThankYou" ResourceConstant="THANK_YOU" /></h1>
<h3 class="tag-line"><mn:Txt runat="server" ID="txtTagline" ResourceConstant="TAGLINE" /></h3>

<asp:Panel runat="server" id="pnlMainArea">
<div class="main-info rbox-wrapper">
    <div class="rbox-style-high padding-heavy">
        <h2><mn:Txt runat="server" ID="txt11" ResourceConstant="TXT_SUBTITLE_FIRST" /></h2>
        <mn:Txt runat="server" ID="txt4" ResourceConstant="TXT_SUBSCRIBER_BENEFIT" />
        <p class="block-text"><mn:Txt runat="server" ID="txt5" ResourceConstant="TXT_BLOCK_FIRST" /></p>
        <asp:Panel ID="pnlFirstLink" runat="server" class="cta-wrapper">
            <div class="cta-bevel sequre">
                <mn:Txt runat="server" ID="linkSubscribe" ResourceConstant="TXT_HREF_FIRST" Href="/Applications/Subscription/Subscribe.aspx?prtid=41" CssClass="link-primary" />
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlFirstLinkMingle" runat="server" CssClass="cta">
            <mn:Txt runat="server" ID="linkMembersOnline" ResourceConstant="TXT_HREF_FIRST" Href="/Applications/MembersOnline/MembersOnline.aspx" CssClass="link-secondary" />
        </asp:Panel>
    </div>
</div>

<div class="marketing rbox-wrapper margin-left">
    <div class="rbox-style-clear padding-heavy">
        <h2><mn:Txt runat="server" ID="txt2" ResourceConstant="TXT_SUBTITLE_SECOND" /></h2>
        <mn:Image runat="server" ID="imgRegWelcomePic" FileName="scrn-profile.jpg" ResourceConstant="ALT_IMG_PROFILE" />
        <p class="block-text"><mn:Txt runat="server" ID="txt6" ResourceConstant="TXT_BLOCK_SECOND" /></p>
            <div class="cta">
                <mn:Txt runat="server" ID="linkCompleteProfile" ResourceConstant="TXT_HREF_SECOND" Href="/Applications/MemberProfile/ViewProfile.aspx" CssClass="link-secondary" />
            </div>
    </div>
</div>

<div class="marketing rbox-wrapper margin-left mobile-wrapper">
    <div class="rbox-style-clear padding-heavy">
        <asp:PlaceHolder runat="server" ID="phMatches">
            <h2><mn:Txt runat="server" ID="txt3" ResourceConstant="TXT_SUBTITLE_THIRD" /></h2>
            <mn:Image runat="server" ID="Image1" FileName="scrn-matches.jpg" ResourceConstant="ALT_IMG_MATCHES" />
            <p class="block-text"><mn:Txt runat="server" ID="txt7" ResourceConstant="TXT_BLOCK_THIRD" /></p>
                <div class="cta">
                    <mn:Txt runat="server" ID="linkSearchMatches" ResourceConstant="TXT_HREF_THIRD" Href="/Applications/Search/SearchResults.aspx?NavPoint=sub" CssClass="link-secondary" />
                </div>
        </asp:PlaceHolder>            
        <asp:PlaceHolder runat="server" ID="phColorCode" Visible="false">
            <h2 class="color-code"><mn:Txt ID="Txt1" runat="server" ResourceConstant="TTL_COLORCODE" /></h2>
            <mn:Txt ID="txtColorCodeResults" runat="server" ResourceConstant="TTL_COLORCODE_RESULTS" />
            <div id="cc-inline-pie"><uc2:PieChart runat="server" ID="regColorCodeResults" /></div>
            <p><mn:Txt ID="Txt8" runat="server" ResourceConstant="TTL_COLORCODE_INFO" /></p>
            <div class="cta">
                <mn:Txt runat="server" ID="linkColorCode" ResourceConstant="TXT_HREF_COLORCODE" Href="/Applications/ColorCode/QuizConfirmation.aspx" CssClass="link-secondary" />
            </div>
        </asp:PlaceHolder>
    </div>
    <script type="text/javascript">
        $j('<div id="cc-pie-chart-loading"><mn:image id="mnimage6491" runat="server" filename="ajax-loader.gif" alt="" CssClass="centered" /></div>').appendTo('#cc-inline-pie');
    </script>
</div>
</asp:Panel>
<div class="clearfix"></div>

<mn:Txt runat="server" ID="htmlTakeAdvantage" ResourceConstant="HTML_TAKE_ADVANTAGE" ExpandImageTokens="true" />

<asp:HyperLink ID="lnkFiveDFT" runat="server" Visible="False">
    <mn:Image runat="server" ID="imgFiveDFT" FileName="Promo\fdft_get5DFTnow.gif" />
</asp:HyperLink>