﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.Web.Framework;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.ExternalMail.ServiceAdapters;
using Matchnet.Web.Applications.UserNotifications;
using Matchnet.List.ServiceAdapters;
using Matchnet.List.ValueObjects;
using Matchnet.UserNotifications.ValueObjects;
using Matchnet.UserNotifications.ServiceAdapters;
using Matchnet.Web.Applications.API.JSONResult;
using System.IO;
using System.Web.UI;
using Matchnet.Lib;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.Region;
using System.Text;
using Matchnet.Web.Applications.MemberProfile.ProfileTabs30.XMLProfileData;
using Matchnet.Web.Applications.MemberProfile.ProfileTabs30.Controls.ProfileDataGroup;

namespace Matchnet.Web.Applications.MemberProfile.ProfileTabs30.Controls.Edit
{
    public class EditProfileUtility
    {
        public const int MAX_ESSAY_LENGTH = 3000;

        public static void UpdateProfileDataSection(int memberID, ContextGlobal g, ProfileDataGroupEnum dataGroup, ProfileDataSectionEnum dataSection, Dictionary<string, string> nvParams, EditProfileResult result)
        {
            if (g != null && g.Member != null && g.Member.MemberID == memberID && nvParams.Count > 0)
            {
                XMLProfileData.ProfileDataGroup profileDataGroup = null;
                XMLProfileData.ProfileDataSection profileDataSection = null;
                System.Web.UI.Page pResource = new System.Web.UI.Page();
                FrameworkControl InternalEditResourceControl = pResource.LoadControl("/Applications/MemberProfile/ProfileTabs30/Controls/Edit/EditResourceControl.ascx") as FrameworkControl;
                XMLProfileData.ProfileDataItem profileDataItem = null;
                bool isSingleInlineEditing = false;

                profileDataGroup = ProfileUtility.GetProfileDataGroup(dataGroup, g.Brand.Site.SiteID, g);
                if (profileDataGroup != null)
                {
                    profileDataSection = profileDataGroup.GetProfileDataSection(dataSection);
                    if (profileDataSection != null)
                    {
                        if (nvParams.Count == 1)
                        {
                            //Update for single data item (i.e. inline editing of one item)
                            isSingleInlineEditing = true;
                            profileDataItem = profileDataSection.GetProfileDataItem(nvParams.Keys.ToArray()[0]);

                            if (profileDataItem != null)
                            {
                                UpdateMemberAttribute(memberID, g, dataGroup, dataSection, nvParams, result, profileDataItem, InternalEditResourceControl);
                            }
                        }
                        else
                        {
                            //Update for a whole section of data items
                            //get section based on xml
                            //get posted data and save for each profile data attribute
                            for (int i = 0; i < profileDataSection.ProfileDataItemList.Count; i++)
                            {
                                profileDataItem = profileDataSection.ProfileDataItemList[i];
                                UpdateMemberAttribute(memberID, g, dataGroup, dataSection, nvParams, result, profileDataItem, InternalEditResourceControl);
                            }
                        }
                    }
                }

                if (result.ValidationErrors.Count <= 0)
                {
                    //Save Profile
                    bool isDirtyHere = g.Member.IsDirty;
                    MemberSaveResult saveResult = SaveProfile(g.Member, g);

                    if ((saveResult == null && !isDirtyHere) || (saveResult != null && saveResult.SaveStatus == MemberSaveStatusType.Success))
                    {
                        result.ItemHTML = "";
                        result.SectionHTMLs = new List<SectionHTML>();

                        if (isSingleInlineEditing)
                        {
                            if (profileDataItem != null && profileDataItem.EditControlType == EditControlTypeEnum.Checkbox)
                            {
                                //inline checkbox editing, need to return back updated html for display
                                result.ItemHTML = ProfileDisplayHelper.GetMaskContent(g.Member, g.Brand, profileDataItem.AttributeName);
                                if (string.IsNullOrEmpty(result.ItemHTML))
                                {
                                    result.ItemHTML = GetNoAnswer(g, InternalEditResourceControl);
                                }
                            }
                            else if (profileDataItem != null && profileDataItem.EditDataType == EditDataTypeEnum.Essay)
                            {
                                //inline essay editing, return back "no answer" if essay is empty
                                Matchnet.Member.ValueObjects.TextStatusType textStatus = Member.ValueObjects.TextStatusType.None;
                                string essayText = g.Member.GetAttributeText(g.Brand, profileDataItem.AttributeName, out textStatus);
                                if (string.IsNullOrEmpty(essayText))
                                {
                                    result.ItemHTML = GetNoAnswer(g, InternalEditResourceControl);
                                }
                            }
                            else if (profileDataItem != null && profileDataItem.EditControlType == EditControlTypeEnum.Dropdown)
                            {
                                //inline dropdown editing, need to return back updated html for display
                                result.ItemHTML = ProfileDisplayHelper.GetOptionValue(g.Member, g, profileDataItem.AttributeName, profileDataItem.AttributeName);
                                if (string.IsNullOrEmpty(result.ItemHTML))
                                {
                                    result.ItemHTML = GetNoAnswer(g, InternalEditResourceControl);
                                }
                            }
                        }
                        else
                        {
                            //Render updated section html
                            //Note: When certain sections are edited, we may need to refresh another datagroup (and its sections)
                            System.Web.UI.Page page = new System.Web.UI.Page();

                            if (dataSection != ProfileDataSectionEnum.Essays)
                            {
                                //refresh edited section
                                SectionHTML editedSectionHTML = GetSectionHTML(g, profileDataGroup, profileDataSection);
                                if (editedSectionHTML != null)
                                {
                                    result.SectionHTMLs.Add(editedSectionHTML);

                                    //refresh sections in other dependent data groups
                                    List<ProfileDataGroupEnum> dependentDataGroups = profileDataGroup.GetEditRefreshIncludeGroupType();
                                    if (dependentDataGroups != null && dependentDataGroups.Count > 0)
                                    {
                                        foreach (ProfileDataGroupEnum pdg in dependentDataGroups)
                                        {
                                            if (pdg != profileDataGroup.ProfileDataGroupType)
                                            {
                                                XMLProfileData.ProfileDataGroup dependentDataGroup = ProfileUtility.GetProfileDataGroup(pdg, g.Brand.Site.SiteID, g);
                                                foreach (ProfileDataSection s in dependentDataGroup.ProfileDataSectionList)
                                                {
                                                    SectionHTML sh = GetSectionHTML(g, dependentDataGroup, s);
                                                    if (sh != null)
                                                    {
                                                        result.SectionHTMLs.Add(sh);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        //send profile changed confirmation email
                        if (result.ProfileChangedAttributesForEmailConfirmation.Count > 0 && saveResult != null && saveResult.SaveStatus == MemberSaveStatusType.Success)
                        {
                            ExternalMail.ServiceAdapters.ExternalMailSA.Instance.SendProfileChangedConfirmationEmail(g.Member.MemberID, g.Brand.BrandID, g.Member.EmailAddress, g.Member.GetAttributeText(g.Brand, "sitefirstname"), g.Member.GetUserName(g.Brand), result.ProfileChangedAttributesForEmailConfirmation);
                        }
                    }
                    else
                    {
                        string errmsg = (saveResult != null) ? saveResult.SaveStatus.ToString() : "null MemberSaveResult";
                        throw new Exception("Save member error: " + errmsg);
                    }
                }
                else
                {
                    //validation errors exist
                    //throw new Exception(g.GetResource("TXT_VALIDATION_ERROR", InternalEditResourceControl));
                    result.Status = (int)JSONStatus.Failed;
                    result.StatusMessage = g.GetResource("TXT_VALIDATION_ERROR", InternalEditResourceControl);
                }
            }
            else
            {
                throw new Exception("Member not found: " + memberID.ToString());
            }
        }

        public static string GetJSEditItem(ContextGlobal g, XMLProfileData.ProfileDataGroup ProfileDataGroup, XMLProfileData.ProfileDataSection ProfileDataSection, XMLProfileData.ProfileDataItem ProfileDataItem, string JavascriptSectionVarName, string editControlID, bool isEditControlIDJSON, string editContainerID, string viewContainerID, string errorContainerID, int idx)
        {
            StringBuilder sb = new StringBuilder();

            sb.Append("try{");
            sb.Append("var itemObj" + idx + " = new spark_profile30_EditItem();");
            sb.Append("itemObj" + idx + ".attributeName = '" + ProfileDataItem.AttributeName.ToLower() + "';");
            if (isEditControlIDJSON)
            {
                sb.Append("itemObj" + idx + ".editControlID = " + editControlID + ";");
            }
            else
            {
                sb.Append("itemObj" + idx + ".editControlID = '" + editControlID + "';");
            }

            if (!String.IsNullOrEmpty(editContainerID))
            {
                sb.Append("itemObj" + idx + ".editContainerID = '" + editContainerID + "';");
            }

            if (!String.IsNullOrEmpty(viewContainerID))
            {
                sb.Append("itemObj" + idx + ".viewContainerID = '" + viewContainerID + "';");
            }

            if (!String.IsNullOrEmpty(errorContainerID))
            {
                sb.Append("itemObj" + idx + ".errorContainerID = '" + errorContainerID + "';");
            }
            sb.Append("itemObj" + idx + ".editControlTypeID = " + ((int)ProfileDataItem.EditControlType).ToString() + ";");
            sb.Append("itemObj" + idx + ".editDataTypeID = " + ((int)ProfileDataItem.EditDataType).ToString() + ";");

            //determine if this item is duplicated in other sections that will be included in refreshed
            bool hasDuplicate = false;
            List<ProfileDataGroupEnum> dependentDataGroups = ProfileDataGroup.GetEditRefreshIncludeGroupType();
            if (dependentDataGroups != null && dependentDataGroups.Count > 0)
            {
                foreach (ProfileDataGroupEnum pdg in dependentDataGroups)
                {
                    if (pdg != ProfileDataGroup.ProfileDataGroupType)
                    {
                        XMLProfileData.ProfileDataGroup dependentDataGroup = ProfileUtility.GetProfileDataGroup(pdg, g.Brand.Site.SiteID, g);
                        foreach (ProfileDataSection s in dependentDataGroup.ProfileDataSectionList)
                        {
                            ProfileDataItem i = s.GetProfileDataItem(ProfileDataItem.AttributeName);
                            if (i == null)
                            {
                                //exceptional cases where attribute names across sections don't match
                                if (ProfileDataItem.AttributeName.ToLower() == "gendermask")
                                {
                                    i = s.GetProfileDataItem("seekinggendermask");
                                }
                                else if (ProfileDataItem.AttributeName.ToLower() == "seekinggendermask")
                                {
                                    i = s.GetProfileDataItem("gendermask");
                                }
                            }

                            if (i != null)
                            {
                                hasDuplicate = true;
                                sb.Append("itemObj" + idx + ".addRefreshSectionName('" + s.ProfileDataSectionName.ToString() + "');");
                                break;
                            }
                        }
                    }
                }
            }

            if (hasDuplicate)
            {
                sb.Append("itemObj" + idx + ".hasDuplicate = true;");
            }
            sb.Append(JavascriptSectionVarName + ".addEditItem(itemObj" + idx + ");");
            sb.Append("}catch(e){}");

            return sb.ToString();
        }

        public static string GetJSEditSection(ContextGlobal g, Control ResourceControl, ProfileDataSection profileDataSection, ProfileDataGroupEnum dataGroupType, string viewContainerID, string editContainerID, string errorContainerID, bool editBySection, out string sectionJSName)
        {
            StringBuilder sbSectionObj = new StringBuilder();
            sectionJSName = dataGroupType.ToString() + "_section" + profileDataSection.ProfileDataSectionName.ToString() + "Obj";
            sbSectionObj.Append("var " + sectionJSName + " = new spark_profile30_EditSection();");
            sbSectionObj.Append(sectionJSName + ".groupType = '" + dataGroupType.ToString() + "';");
            sbSectionObj.Append(sectionJSName + ".groupTypeID = " + ((int)dataGroupType).ToString() + ";");
            sbSectionObj.Append(sectionJSName + ".sectionName = '" + profileDataSection.ProfileDataSectionName.ToString() + "';");
            if (!String.IsNullOrEmpty(profileDataSection.SectionEditTitleResourceConstant) || !String.IsNullOrEmpty(profileDataSection.SectionTitleResourceConstant))
            {
                sbSectionObj.Append(sectionJSName + ".title = '" + g.GetResource(!String.IsNullOrEmpty(profileDataSection.SectionEditTitleResourceConstant) ? profileDataSection.SectionEditTitleResourceConstant : profileDataSection.SectionTitleResourceConstant, ResourceControl) + "';");
            }
            sbSectionObj.Append(sectionJSName + ".viewContainerID = '" + viewContainerID + "';");
            sbSectionObj.Append(sectionJSName + ".editContainerID = '" + editContainerID + "';");
            sbSectionObj.Append(sectionJSName + ".errorContainerID = '" + errorContainerID + "';");
            sbSectionObj.Append(sectionJSName + ".editBySection = " + editBySection.ToString().ToLower() + ";");
            sbSectionObj.Append(sectionJSName + ".isMatchPreferences = " + profileDataSection.IsMatchPreferences.ToString().ToLower() + ";");
            sbSectionObj.Append("editProfileManager.addEditSection(" + sectionJSName + ");");

            return sbSectionObj.ToString();
        }

        public static void AddEditSectionJS(ContextGlobal g, string editSectionObjJS)
        {
            if (g.ProfileSectionItemJS == null)
            {
                g.ProfileSectionItemJS = new StringBuilder();
            }

            g.ProfileSectionItemJS.Append(editSectionObjJS);
        }

        public static BaseEdit GetEditControl(System.Web.UI.Page page, EditControlTypeEnum editControlType, string attributeName)
        {
            BaseEdit editControl = null;
            switch (editControlType)
            {
                case EditControlTypeEnum.Textbox:
                    editControl = page.LoadControl("/Applications/MemberProfile/ProfileTabs30/Controls/Edit/EditTextbox.ascx") as EditTextbox;
                    break;
                case EditControlTypeEnum.Dropdown:
                    editControl = page.LoadControl("/Applications/MemberProfile/ProfileTabs30/Controls/Edit/EditDropdown.ascx") as EditDropdown;
                    break;
                case EditControlTypeEnum.TextArea:
                    editControl = page.LoadControl("/Applications/MemberProfile/ProfileTabs30/Controls/Edit/EditTextArea.ascx") as EditTextArea;
                    break;
                case EditControlTypeEnum.Checkbox:
                    editControl = page.LoadControl("/Applications/MemberProfile/ProfileTabs30/Controls/Edit/EditCheckbox.ascx") as EditCheckbox;
                    break;
                case EditControlTypeEnum.Radio:
                    editControl = page.LoadControl("/Applications/MemberProfile/ProfileTabs30/Controls/Edit/EditRadio.ascx") as EditRadio;
                    break;
                case EditControlTypeEnum.Custom:
                    switch (attributeName.ToLower())
                    {
                        case "birthdate":
                            editControl = page.LoadControl("/Applications/MemberProfile/ProfileTabs30/Controls/Edit/EditBirthday.ascx") as EditBirthday;
                            break;
                        case "regionid":
                            editControl = page.LoadControl("/Applications/MemberProfile/ProfileTabs30/Controls/Edit/EditLocation.ascx") as EditLocation;
                            break;
                        case "desiredminage":
                            editControl = page.LoadControl("/Applications/MemberProfile/ProfileTabs30/Controls/Edit/EditAge.ascx") as EditAge;
                            break;
                        case "mobilephone":
                            editControl = page.LoadControl("/Applications/MemberProfile/ProfileTabs30/Controls/Edit/EditPhone.ascx") as EditPhone;
                            break;
                    }
                    break;
            }

            return editControl;
        }

        #region Private Methods
        private static void UpdateMemberAttribute(int memberID, ContextGlobal g, ProfileDataGroupEnum dataGroup, ProfileDataSectionEnum dataSection, Dictionary<string, string> nvParams, EditProfileResult result, XMLProfileData.ProfileDataItem profileDataItem, System.Web.UI.Control InternalEditResourceControl)
        {
            bool processedAttribute = false;

            //process attributes with special cases 
            switch (profileDataItem.AttributeName.ToLower())
            {
                case "username":
                    processedAttribute = true;
                    if (nvParams.ContainsKey("username") && !string.IsNullOrEmpty(nvParams["username"]) && nvParams["username"] != "null")
                    {
                        string username = nvParams["username"].Trim();
                        bool validated = true;
                        if (username.Length < 4)
                        {
                            //minimum 4 characters
                            validated = false;
                            AddValidationError(profileDataItem, result, g.GetResource("TXT_VALIDATION_USERNAME_MIN_LENGTH", InternalEditResourceControl));
                        }
                        else if (!ViewProfileTabUtility.IsAlphanumeric(username))
                        {
                            //alphanumeric only
                            validated = false;
                            AddValidationError(profileDataItem, result, g.GetResource("TXT_VALIDATION_USERNAME_ALPHNUMERIC", InternalEditResourceControl));
                        }

                        if (validated)
                        {
                            g.Member.SetUsername(username, g.Brand);
                        }
                    }
                    else
                    {
                        ValidateRequired(g, profileDataItem, result, InternalEditResourceControl);
                    }
                    break;
                case "desiredminage":
                    processedAttribute = true;
                    if (nvParams.ContainsKey("minage") && nvParams.ContainsKey("maxage") && !string.IsNullOrEmpty(nvParams["minage"]) && !string.IsNullOrEmpty(nvParams["maxage"]) && nvParams["minage"] != "null" && nvParams["maxage"] != "null")
                    {
                        string validationMessage = "";
                        bool validated = ValidationHelper.IsValidAgeRange(g, nvParams["minage"], nvParams["maxage"], null, out validationMessage);

                        if (validated)
                        {
                            int minAge = Convert.ToInt32(nvParams["minage"]);
                            int maxAge = Convert.ToInt32(nvParams["maxage"]);

                            g.Member.SetAttributeInt(g.Brand, "DesiredMinAge", minAge);
                            g.Member.SetAttributeInt(g.Brand, "DesiredMaxAge", maxAge);
                        }
                        else
                        {
                            AddValidationError(profileDataItem, result, validationMessage);
                        }
                    }
                    else
                    {
                        ValidateRequired(g, profileDataItem, result, InternalEditResourceControl);
                    }
                    break;
                case "birthdate":
                    processedAttribute = true;
                    if (nvParams.ContainsKey("birthdate") && !string.IsNullOrEmpty(nvParams["birthdate"]) && nvParams["birthdate"] != "null")
                    {
                        DateTime birthdate = DateTime.Now;
                        bool validated = false;
                        string validationMessage = g.GetResource("TEXT_INVALID_BIRTH_DATE", null);
                        try
                        {
                            birthdate = DateTime.ParseExact(nvParams["birthdate"].Trim(), "M/d/yyyy", new System.Globalization.DateTimeFormatInfo());
                            validated = ValidationHelper.IsValidBirthDate(g, birthdate, null, out validationMessage);
                        }
                        catch (Exception e)
                        {

                        }

                        if (validated)
                        {
                            g.Member.SetAttributeDate(g.Brand, profileDataItem.AttributeName, birthdate);
                        }
                        else
                        {
                            AddValidationError(profileDataItem, result, validationMessage);
                        }
                    }
                    else
                    {
                        ValidateRequired(g, profileDataItem, result, InternalEditResourceControl);
                    }
                    break;
                case "seekingmask":
                    processedAttribute = true;
                    if (nvParams.ContainsKey("seekingmask") && !string.IsNullOrEmpty(nvParams["seekingmask"]) && nvParams["seekingmask"] != "null")
                    {
                        int seekingmask = Convert.ToInt32(nvParams["seekingmask"]);
                        if (seekingmask > 0)
                        {
                            g.Member.SetAttributeInt(g.Brand, "relationshipmask", seekingmask);
                        }
                    }
                    else
                    {
                        ValidateRequired(g, profileDataItem, result, InternalEditResourceControl);
                    }
                    break;
                case "seekinggendermask":
                    processedAttribute = true;
                    if (nvParams.ContainsKey("seekinggendermask") && !string.IsNullOrEmpty(nvParams["seekinggendermask"]) && nvParams["seekinggendermask"] != "null")
                    {
                        int seekingGenderMask = Convert.ToInt32(nvParams["seekinggendermask"]);
                        if (seekingGenderMask > 0)
                        {
                            int genderMask = g.Member.GetAttributeInt(g.Brand, "gendermask");
                            if (seekingGenderMask == ConstantsTemp.GENDERID_SEEKING_MALE)
                            {
                                if ((genderMask & ConstantsTemp.GENDERID_SEEKING_FEMALE) == ConstantsTemp.GENDERID_SEEKING_FEMALE)
                                {
                                    genderMask = genderMask - ConstantsTemp.GENDERID_SEEKING_FEMALE;
                                }

                                if ((genderMask & seekingGenderMask) != seekingGenderMask)
                                {
                                    genderMask = genderMask + seekingGenderMask;
                                    g.Member.SetAttributeInt(g.Brand, "gendermask", genderMask);
                                    result.ProfileChangedAttributesForEmailConfirmation.Add(g.GetResource("TXT_GENDER", InternalEditResourceControl));
                                }
                            }
                            else if (seekingGenderMask == ConstantsTemp.GENDERID_SEEKING_FEMALE)
                            {
                                if ((genderMask & ConstantsTemp.GENDERID_SEEKING_MALE) == ConstantsTemp.GENDERID_SEEKING_MALE)
                                {
                                    genderMask = genderMask - ConstantsTemp.GENDERID_SEEKING_MALE;
                                }

                                if ((genderMask & seekingGenderMask) != seekingGenderMask)
                                {
                                    genderMask = genderMask + seekingGenderMask;
                                    g.Member.SetAttributeInt(g.Brand, "gendermask", genderMask);
                                    result.ProfileChangedAttributesForEmailConfirmation.Add(g.GetResource("TXT_GENDER", InternalEditResourceControl));
                                }
                            }
                        }
                    }
                    else
                    {
                        ValidateRequired(g, profileDataItem, result, InternalEditResourceControl);
                    }
                    break;
                case "regionid":
                    processedAttribute = true;

                    if (nvParams.ContainsKey("countryregionid") && !string.IsNullOrEmpty(nvParams["countryregionid"]) && nvParams["countryregionid"] != "null")
                    {
                        RegionID regionID = null;
                        Region region = null;

                        try
                        {
                            int currentRegionID = g.Member.GetAttributeInt(g.Brand, "regionid");

                            int countryRegionID = Convert.ToInt32(nvParams["countryregionid"].Trim());
                            if (countryRegionID > 0)
                            {
                                if (countryRegionID == ConstantsTemp.REGION_US || countryRegionID == ConstantsTemp.REGIONID_CANADA)
                                {
                                    //regionID by zip code
                                    string zip = nvParams["zipcode"];
                                    if (!string.IsNullOrEmpty(zip))
                                    {
                                        regionID = RegionSA.Instance.FindRegionIdByPostalCode(countryRegionID, zip);
                                        if (regionID == null || regionID.ID <= 0)
                                        {
                                            AddValidationError(profileDataItem, result, g.GetResource("TXT_VALIDATION_ZIPCODE", InternalEditResourceControl));
                                        }
                                        else
                                        {
                                            string cityForZip = nvParams["cityforzipcode"];
                                            if (!string.IsNullOrEmpty(cityForZip))
                                            {
                                                int languageId = g.Brand.Site.LanguageID;

                                                try
                                                {
                                                    int cityForZipInt = Convert.ToInt32(cityForZip);
                                                    region = RegionSA.Instance.RetrieveRegionByID(cityForZipInt, languageId);
                                                    if (region == null || region.RegionID <= 0)
                                                    {
                                                        AddValidationError(profileDataItem, result, g.GetResource("TXT_VALIDATION_LOCATION", InternalEditResourceControl));
                                                    }

                                                }
                                                catch
                                                {
                                                    AddValidationError(profileDataItem, result, g.GetResource("TXT_VALIDATION_LOCATION", InternalEditResourceControl));
                                                }
                                            } 
                                            else
                                            {  
                                                AddValidationError(profileDataItem, result, g.GetResource("TXT_VALIDATION_LOCATION", InternalEditResourceControl));
                                            }
                                        } 
                                    }  
                                    else
                                    {
                                        AddValidationError(profileDataItem, result, g.GetResource("TXT_VALIDATION_ZIPCODE", InternalEditResourceControl));
                                    }  
                                } 
                                else
                                {
                                    //regionID by city
                                    bool validated = true;
                                    int languageId = g.Brand.Site.LanguageID;

                                    //verify if country requires state/region
                                    bool isStatesRequired = RegionSA.Instance.RetrieveRegionByID(countryRegionID, g.Brand.Site.LanguageID).ChildrenDepth == 2;

                                    int stateRegionID = 0;
                                    if (isStatesRequired)
                                    {
                                        try
                                        {
                                            stateRegionID = Convert.ToInt32(nvParams["stateregionid"]);
                                        }
                                        catch
                                        {
                                            validated = false;
                                            AddValidationError(profileDataItem, result, g.GetResource("TXT_VALIDATION_LOCATION", InternalEditResourceControl));
                                        }
                                    }

                                    if (validated)
                                    {
                                        string cityName = nvParams["cityname"];

                                        //get region by city based on country/state or just country
                                        if (stateRegionID > 0 && languageId > 0 && !string.IsNullOrEmpty(cityName))
                                        {
                                            regionID = RegionSA.Instance.FindRegionIdByCity(stateRegionID, cityName, languageId);
                                        }
                                        else if (countryRegionID > 0 && languageId > 0 && !string.IsNullOrEmpty(cityName))
                                        {
                                            regionID = RegionSA.Instance.FindRegionIdByCity(countryRegionID, cityName, languageId);
                                        }

                                        if (regionID == null || regionID.ID <= 0)
                                        {
                                            AddValidationError(profileDataItem, result, g.GetResource("TXT_VALIDATION_CITY", InternalEditResourceControl));
                                        }
                                    }
                                }  

                                int newRegionID = 0;
                                if (region != null && region.RegionID > 0)
                                {
                                    newRegionID = region.RegionID;
                                }
                                else if (regionID != null && regionID.ID > 0)
                                {
                                    newRegionID = regionID.ID;
                                }

                                if (newRegionID > 0)
                                {
                                    g.Member.SetAttributeInt(g.Brand, "regionid", newRegionID);
                                    if (currentRegionID != newRegionID)
                                    {
                                        result.ProfileChangedAttributesForEmailConfirmation.Add(g.GetResource("TXT_REGION", InternalEditResourceControl));
                                    }
                                    // put new region in session for use with MOL
                                    g.Session.Add(WebConstants.ATTRIBUTE_NAME_MOLREGIONID, newRegionID,
                                                  Matchnet.Session.ValueObjects.SessionPropertyLifetime.Temporary);
                                }

                            }
                        }
                        catch
                        {
                            AddValidationError(profileDataItem, result, g.GetResource("TXT_VALIDATION_LOCATION", InternalEditResourceControl));
                        }
                    }
                    else
                    {
                        ValidateRequired(g, profileDataItem, result, InternalEditResourceControl);
                    }
                    break;
            }

            //process attribute by edit data type
            if (!processedAttribute)
            {
                switch (profileDataItem.EditDataType)
                {
                    case EditDataTypeEnum.Text:
                        string textValue = "";
                        if (nvParams.ContainsKey(profileDataItem.AttributeName.ToLower()) && !string.IsNullOrEmpty(nvParams[profileDataItem.AttributeName.ToLower()]) && nvParams[profileDataItem.AttributeName.ToLower()] != "null")
                        {
                            textValue = nvParams[profileDataItem.AttributeName.ToLower()].Trim();
                        }

                        bool validatedText = true;
                        if (textValue.Length <= 0)
                        {
                            validatedText = !ValidateRequired(g, profileDataItem, result, InternalEditResourceControl);
                        }
                        else if (ValidateText(textValue, g, profileDataItem, result, InternalEditResourceControl))
                        {
                            //checked valid text (i.e. no html)
                            validatedText = false;
                        }

                        if (validatedText)
                        {
                            Matchnet.Member.ValueObjects.TextStatusType textType = Matchnet.Member.ValueObjects.TextStatusType.None;
                            switch (profileDataItem.AttributeName.ToLower())
                            {
                                case "emailaddress":
                                    textType = Matchnet.Member.ValueObjects.TextStatusType.Auto;
                                    break;
                                case "sitefirstname":
                                    textType = Matchnet.Member.ValueObjects.TextStatusType.Auto;
                                    if (g.Member.GetAttributeText(g.Brand, profileDataItem.AttributeName) != textValue)
                                    {
                                        result.ProfileChangedAttributesForEmailConfirmation.Add(g.GetResource("TXT_SITEFIRSTNAME", InternalEditResourceControl));
                                    }
                                    break;
                                case "sitelastname":
                                    textType = Matchnet.Member.ValueObjects.TextStatusType.Auto;
                                    if (g.Member.GetAttributeText(g.Brand, profileDataItem.AttributeName) != textValue)
                                    {
                                        result.ProfileChangedAttributesForEmailConfirmation.Add(g.GetResource("TXT_SITELASTNAME", InternalEditResourceControl));
                                    }
                                    break;
                            }

                            if (textValue == "")
                            {
                                //auto-approve empty text (happens when member clears it out
                                g.Member.SetAttributeText(g.Brand, profileDataItem.AttributeName, null, TextStatusType.Auto);
                            }
                            else
                            {
                                g.Member.SetAttributeText(g.Brand, profileDataItem.AttributeName, textValue, textType);
                            }
                        }
                        break;
                    case EditDataTypeEnum.Essay:
                        string essayValue = "";
                        if (nvParams.ContainsKey(profileDataItem.AttributeName.ToLower()) && !string.IsNullOrEmpty(nvParams[profileDataItem.AttributeName.ToLower()]) && nvParams[profileDataItem.AttributeName.ToLower()] != "null")
                        {
                            essayValue = nvParams[profileDataItem.AttributeName.ToLower()].Trim();
                        }

                        bool validatedEssay = true;
                        if (essayValue.Length <= 0)
                        {
                            //check required field
                            validatedEssay = !ValidateRequired(g, profileDataItem, result, InternalEditResourceControl);
                        }

                        if (validatedEssay)
                        {
                            if (ValidateText(essayValue, g, profileDataItem, result, InternalEditResourceControl))
                            {
                                //checked valid text (i.e. no html)
                                validatedEssay = false;
                            }
                            else if (essayValue.Length > 0 && profileDataItem.MinLength > 0 && essayValue.Length < profileDataItem.MinLength)
                            {
                                //min length required
                                validatedEssay = false;
                                AddValidationError(profileDataItem, result, g.GetResource("TXT_VALIDATION_ESSAY_MIN_LENGTH", InternalEditResourceControl).Replace("{min}", profileDataItem.MinLength.ToString()));
                            }
                            else if (essayValue.Length > MAX_ESSAY_LENGTH)
                            {
                                //max length
                                validatedEssay = false;
                                AddValidationError(profileDataItem, result, g.GetResource("TXT_VALIDATION_ESSAY_MAX_LENGTH", InternalEditResourceControl).Replace("{max}", MAX_ESSAY_LENGTH.ToString()));
                            }
                        }

                        if (validatedEssay)
                        {
                            if (essayValue == "")
                            {
                                //auto-approve if essay is empty (happens when member decides to clear out their existing essay)
                                g.Member.SetAttributeText(g.Brand, profileDataItem.AttributeName, null, TextStatusType.Auto);
                            }
                            else
                            {
                                //all other essays requires approval
                                if (essayValue.IndexOf("\n") >= 0)
                                {
                                    //ensure all line breaks are saved as windows "\r\n" so it shows up correctly in admin tool
                                    string templinebreak = "{{br}}";
                                    essayValue = essayValue.Replace("\r\n", templinebreak);
                                    essayValue = essayValue.Replace("\n", templinebreak);
                                    essayValue = essayValue.Replace("\r", templinebreak);
                                    essayValue = essayValue.Replace(templinebreak, "\r\n");
                                }

                                g.Member.SetAttributeText(g.Brand, profileDataItem.AttributeName, essayValue, TextStatusType.None);
                            }
                        }
                        break;
                    case EditDataTypeEnum.Mask:
                    case EditDataTypeEnum.Int:
                        bool validatedInt = true;
                        int intValue = Constants.NULL_INT;
                        if (nvParams.ContainsKey(profileDataItem.AttributeName.ToLower()) && (string.IsNullOrEmpty(nvParams[profileDataItem.AttributeName.ToLower()]) || nvParams[profileDataItem.AttributeName.ToLower()] == "null") && !profileDataItem.IsRequired)
                        {
                            //optional value (this means user removed their answer)
                            intValue = 0;

                            if (profileDataItem.AttributeName.ToLower() == "childrencount" || profileDataItem.AttributeName.ToLower() == "desiredmorechildrenflag"
                                || profileDataItem.AttributeName.ToLower() == "morechildrenflag")
                            {
                                intValue = Constants.NULL_INT;
                            }
                        }
                        else if (nvParams.ContainsKey(profileDataItem.AttributeName.ToLower()) && !string.IsNullOrEmpty(nvParams[profileDataItem.AttributeName.ToLower()]) && nvParams[profileDataItem.AttributeName.ToLower()] != "null")
                        {
                            int.TryParse(nvParams[profileDataItem.AttributeName.ToLower()].Trim(), out intValue);
                        }

                        if (intValue <= 0 && profileDataItem.IsRequired)
                        {
                            intValue = Constants.NULL_INT;
                            validatedInt = false;
                        }

                        if (validatedInt)
                        {
                            if (profileDataItem.AttributeName.ToLower() == "gendermask")
                            {
                                int genderMask = g.Member.GetAttributeInt(g.Brand, "gendermask");
                                if (genderMask != intValue)
                                {
                                    result.ProfileChangedAttributesForEmailConfirmation.Add(g.GetResource("TXT_GENDER", InternalEditResourceControl));
                                }
                            }

                            g.Member.SetAttributeInt(g.Brand, profileDataItem.AttributeName, intValue);
                        }
                        else
                        {
                            ValidateRequired(g, profileDataItem, result, InternalEditResourceControl);
                        }
                        break;
                    case EditDataTypeEnum.Date:
                        if (nvParams.ContainsKey(profileDataItem.AttributeName.ToLower()) && !string.IsNullOrEmpty(nvParams[profileDataItem.AttributeName.ToLower()]) && nvParams[profileDataItem.AttributeName.ToLower()] != "null")
                        {
                            DateTime dateValue = Convert.ToDateTime(nvParams[profileDataItem.AttributeName.ToLower()].Trim());// DateTime.Now;// DateTime.ParseExact(monthDropDownList.SelectedValue + "/" + dayDropDownList.SelectedValue + "/" + yearTextBox.Text, "M/d/yyyy", new System.Globalization.DateTimeFormatInfo());
                            g.Member.SetAttributeDate(g.Brand, profileDataItem.AttributeName, dateValue);
                        }
                        else
                        {
                            ValidateRequired(g, profileDataItem, result, InternalEditResourceControl);
                        }
                        break;
                }
            }
        }

        private static bool ValidateRequired(ContextGlobal g, ProfileDataItem profileDataItem, EditProfileResult result, System.Web.UI.Control InternalEditResourceControl)
        {
            if (profileDataItem.IsRequired)
            {
                string validationMessage = "";
                switch (profileDataItem.AttributeName.ToLower())
                {
                    case "regionid":
                        validationMessage = g.GetResource("TXT_VALIDATION_LOCATION", InternalEditResourceControl);
                        break;
                    case "birthdate":
                        validationMessage = g.GetResource("TEXT_INVALID_BIRTH_DATE", null);
                        break;
                    default:
                        validationMessage = g.GetResource("TXT_VALIDATION_" + profileDataItem.AttributeName.ToUpper(), InternalEditResourceControl) + " " + g.GetResource("TXT_VALIDATION_REQUIRED", InternalEditResourceControl);
                        break;
                }

                AddValidationError(profileDataItem, result, validationMessage);
                return true;
            }

            return false;
        }

        private static bool ValidateText(string text, ContextGlobal g, ProfileDataItem profileDataItem, EditProfileResult result, System.Web.UI.Control InternalEditResourceControl)
        {
            string attributeName = profileDataItem.AttributeName.ToLower();

            if (text.IndexOfAny(new char[] { '<', '>' }) >= 0)
            {
                string validationMessage = "";
                if (profileDataItem.EditDataType == EditDataTypeEnum.Essay)
                {
                    validationMessage = g.GetResource("TXT_VALIDATION_ESSAY_HTML", InternalEditResourceControl);
                }
                else
                {
                    validationMessage = g.GetResource("TXT_VALIDATION_" + profileDataItem.AttributeName.ToUpper(), InternalEditResourceControl) + " " + g.GetResource("TXT_VALIDATION_HTML", InternalEditResourceControl);
                }
                AddValidationError(profileDataItem, result, validationMessage);
                return true;
            }
            else if (attributeName == "sitefirstname")
            {
                //special alphanumeric check (letters, numbers, spaces, hyphens)
                if (!ViewProfileTabUtility.IsAlphanumericForName(text))
                {
                    AddValidationError(profileDataItem, result, g.GetResource("TXT_VALIDATION_FIRSTNAME_ALPHANUMERIC", InternalEditResourceControl));
                    return true;
                }
            }
            else if (attributeName == "sitelastname")
            {
                //special alphanumeric check (letters, numbers, spaces, hyphens)
                if (!ViewProfileTabUtility.IsAlphanumericForName(text))
                {
                    AddValidationError(profileDataItem, result, g.GetResource("TXT_VALIDATION_LASTNAME_ALPHANUMERIC", InternalEditResourceControl));
                    return true;
                }
            }

            return false;
        }

        private static void AddValidationError(ProfileDataItem profileDataItem, EditProfileResult result, string validationMessage)
        {
            EditProfileValidationError valError = new EditProfileValidationError();
            valError.AttributeName = profileDataItem.AttributeName.ToLower();
            valError.ValidationMessage = validationMessage;

            result.ValidationErrors.Add(valError);
        }

        private static MemberSaveResult SaveProfile(Member.ServiceAdapters.Member member, ContextGlobal g)
        {
            MemberSaveResult saveResult = null;
            try
            {
                if (member.IsDirty)
                {
                    member.SetAttributeDate(g.Brand, "LastUpdated", DateTime.Now);
                    //if member did not previously belong to community make it so now
                    Int32 brandLogonCount = member.GetAttributeInt(g.Brand, "BrandLogonCount", 0);
                    if (brandLogonCount == 0)
                    {
                        DateTime now = DateTime.Now;
                        member.SetAttributeDate(g.Brand, "BrandInsertDate", now);
                        //this attribute is immutable so it is safe to attempt updates all day
                        member.SetAttributeDate(g.Brand, "BrandLastLogonDate", now);
                        member.SetAttributeInt(g.Brand, "LastBrandID", g.Brand.BrandID);
                        member.SetAttributeInt(g.Brand, "BrandLogonCount", brandLogonCount + 1);
                    }

                    if (brandLogonCount == 0)
                    {
                        saveResult = MemberSA.Instance.SaveMember(member, g.Brand, true);
                    }
                    else
                    {
                        saveResult = MemberSA.Instance.SaveMember(member, g.Brand.Site.Community.CommunityID);
                    }

                    if (saveResult.SaveStatus == MemberSaveStatusType.Success)
                    {
                        // if this community has MESSMO_ENABLED, send the profile info to Messmo.  Although we are only enabling JDATE.COM at launch, we have to send
                        // profiles from all sites within the enabled community because sender does not have to come from JDATE.COM due to the fact that our messaging
                        // system is community-based.
                        if (Convert.ToBoolean(RuntimeSettings.GetSetting("MESSMO_ENABLED", g.Brand.Site.Community.CommunityID)))
                        {
                            ExternalMailSA.Instance.SendMessmoFullProfileRequest(g.Brand.BrandID, g.Member.MemberID, DateTime.Now);
                        }


                        if (UserNotificationFactory.IsUserNotificationsEnabled(g))
                        {
                            Matchnet.List.ServiceAdapters.List list = ListSA.Instance.GetList(member.MemberID);
                            int totalFavorites = list.GetCount(HotListCategory.WhoAddedYouToTheirFavorites, g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID);
                            Int32 temp = 0;
                            System.Collections.ArrayList whoAddedYou = list.GetListMembers(HotListCategory.WhoAddedYouToTheirFavorites, g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, 1, 500, out temp);

                            foreach (Int32 whoAddedYouId in whoAddedYou)
                            {
                                UserNotificationParams unParams = UserNotificationParams.GetParamsObject(whoAddedYouId, g.Brand.Site.SiteID, g.Brand.Site.Community.CommunityID);
                                UserNotification notification = UserNotificationFactory.Instance.GetUserNotification(new FavoriteUpdatedProfileUserNotification(), whoAddedYouId, member.MemberID, g);
                                if (notification.IsActivated())
                                {
                                    UserNotificationsServiceSA.Instance.AddUserNotification(unParams, notification.CreateViewObject());
                                }
                            }
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }

            return saveResult;
        }

        private static SectionHTML GetSectionHTML(ContextGlobal g, XMLProfileData.ProfileDataGroup profileDataGroup, XMLProfileData.ProfileDataSection profileDataSection)
        {
            SectionHTML sectionHTML = null;
            bool renderingControl = true;

            System.Web.UI.Page page = new System.Web.UI.Page();
            switch (profileDataSection.ProfileDataSectionName)
            {
                case ProfileDataSectionEnum.BasicInfo:
                    BasicInfo ctl = (BasicInfo)page.LoadControl("/Applications/MemberProfile/ProfileTabs30/Controls/BasicInfo.ascx");
                    page.Controls.Add(ctl);
                    ctl.IsPartialRender = true;
                    ctl.LoadBasicInfo(g.Member);
                    break;
                case ProfileDataSectionEnum.Essays:
                    renderingControl = false;
                    break;
                case ProfileDataSectionEnum.Dealbreakers:
                    NameValueDataGroup ctlNVTwoColumns = (NameValueDataGroup)page.LoadControl("/Applications/MemberProfile/ProfileTabs30/Controls/ProfileDataGroup/NameValueDataGroup.ascx");
                    NameValueDataSectionDouble ctlNVDouble = (NameValueDataSectionDouble)page.LoadControl("/Applications/MemberProfile/ProfileTabs30/Controls/ProfileDataGroup/NameValueDataSectionDouble.ascx");
                    page.Controls.Add(ctlNVDouble);
                    ctlNVDouble.MaxDisplayLength = ProfileUtility.MaxDisplayLengthNVDouble;
                    ctlNVDouble.IsPartialRender = true;
                    ctlNVDouble.ResourceControl = ctlNVTwoColumns;
                    ctlNVDouble.LoadDataSection(g.Member, profileDataGroup, profileDataSection);
                    break;
                case ProfileDataSectionEnum.Background:
                case ProfileDataSectionEnum.IdealMatch:
                case ProfileDataSectionEnum.Lifestyle:
                case ProfileDataSectionEnum.PhysicalInfo:
                    NameValueDataGroup ctlNVOneColumn = (NameValueDataGroup)page.LoadControl("/Applications/MemberProfile/ProfileTabs30/Controls/ProfileDataGroup/NameValueDataGroup.ascx");
                    NameValueDataSectionSingle ctlNVSingle = (NameValueDataSectionSingle)page.LoadControl("/Applications/MemberProfile/ProfileTabs30/Controls/ProfileDataGroup/NameValueDataSectionSingle.ascx");
                    page.Controls.Add(ctlNVSingle);
                    ctlNVSingle.IsPartialRender = true;
                    ctlNVSingle.ResourceControl = ctlNVOneColumn;
                    ctlNVSingle.LoadDataSection(g.Member, profileDataGroup, profileDataSection);
                    break;
                default:
                    renderingControl = false;
                    break;
            }

            if (renderingControl)
            {
                StringWriter writer = new StringWriter();
                HttpContext.Current.Server.Execute(page, writer, false);
                sectionHTML = new SectionHTML();
                sectionHTML.GroupTypeID = (int)profileDataGroup.ProfileDataGroupType;
                sectionHTML.SectionName = profileDataSection.ProfileDataSectionName.ToString();
                sectionHTML.HTML = writer.ToString();
            }

            return sectionHTML;
        }

        private static string GetNoAnswer(ContextGlobal g, Matchnet.Web.Framework.FrameworkControl resourceControl)
        {
            string returnValue = g.GetResource("NOT_ANSWERED_YET", resourceControl);
            if (returnValue.ToLower().IndexOf("resource") >= 0)
                returnValue = "";

            return returnValue;
        }


        #endregion
    }
}
