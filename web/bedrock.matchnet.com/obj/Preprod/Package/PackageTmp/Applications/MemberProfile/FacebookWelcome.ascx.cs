﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Web.Framework;

namespace Matchnet.Web.Applications.MemberProfile
{
    public partial class FacebookWelcome : FrameworkControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["new"]))
            {
                plcFBNewMember.Visible = true;
                lnkBuySub.NavigateUrl = g.AppendEncryptedSessionID(lnkBuySub.NavigateUrl);
                lnkHomepage.NavigateUrl =
                    g.AppendEncryptedSessionID(FrameworkGlobals.GetHomepageAbsURL(g,
                                                                                  Request.ServerVariables["HTTP_HOST"]));
                lnkFinishProfile.NavigateUrl = g.AppendEncryptedSessionID(lnkFinishProfile.NavigateUrl);
                lnkSearchMatches.NavigateUrl = g.AppendEncryptedSessionID(lnkSearchMatches.NavigateUrl);
            }

            else
            {
                plcFBExistingMember.Visible = true;
            }
        }
    }
}