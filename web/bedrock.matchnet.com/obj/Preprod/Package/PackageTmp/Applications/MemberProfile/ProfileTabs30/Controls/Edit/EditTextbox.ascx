﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EditTextbox.ascx.cs" Inherits="Matchnet.Web.Applications.MemberProfile.ProfileTabs30.Controls.Edit.EditTextbox" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>

<%--Textbox--%>
<div class="form-item">
    <div class="form-label">
        <asp:Label ID="lblEdit" runat="server" AssociatedControlID="txtEdit">
            <asp:Literal ID="litEdit" runat="server"></asp:Literal>
        </asp:Label>
    </div>
    <div class="form-input">
        <asp:TextBox ID="txtEdit" runat="server" CssClass="xlarge"></asp:TextBox>
        <asp:Literal ID="litRequired" runat="server"></asp:Literal>
        <asp:Literal ID="litEditNote" runat="server"></asp:Literal>
        <div id="itemErrorContainer" runat="server" style="display:none;" class="form-error-inline"></div>
    </div>
</div>