﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace Matchnet.Web.Applications.MemberProfile.ProfileTabs30.XMLProfileTabConfig
{
    /// <summary>
    /// This class represents omniture configuration for a single profile tab.
    /// It is deserialized from a subset of xml defined in the Tab_SiteID.xml files.
    /// <Omniture></Omniture>
    /// </summary>
    [Serializable]
    public class ProfileTabOmniture
    {
        [XmlElement()]
        public string OmniTabName { get; set; }

    }
}
