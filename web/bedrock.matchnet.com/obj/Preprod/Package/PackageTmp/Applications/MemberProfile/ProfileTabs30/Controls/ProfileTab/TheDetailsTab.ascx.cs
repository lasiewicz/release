﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Member.ValueObjects.Interfaces;
using Matchnet.Web.Framework.Ui;

namespace Matchnet.Web.Applications.MemberProfile.ProfileTabs30.Controls.ProfileTab
{
    public partial class TheDetailsTab : BaseTab
    {
        public TheDetailsTab()
        {
            this.ProfileTabType = ProfileTabEnum.TheDetails;
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public override void LoadTab(IMemberDTO member, bool isActive)
        {
            this._MemberProfile = member;
            this.IsActive = isActive;
            if (g.Member != null && member.MemberID == g.Member.MemberID)
            {
                this.WhoseProfile = WhoseProfileEnum.Self;
            }

            phPopupAd.Visible = !BreadCrumbHelper.IsProfilePopupDisabled(member.MemberID);
            if (g.LayoutTemplate == Content.ValueObjects.PageConfig.LayoutTemplate.WidePopup)
                phPopupAd.Visible = true;

            profileDetails.LoadProfileDetails(member);
        }
    }
}