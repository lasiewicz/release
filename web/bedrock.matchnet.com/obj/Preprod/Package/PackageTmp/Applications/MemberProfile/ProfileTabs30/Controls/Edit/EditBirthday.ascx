﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EditBirthday.ascx.cs" Inherits="Matchnet.Web.Applications.MemberProfile.ProfileTabs30.Controls.Edit.EditBirthday" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>

<%--Custom: birthday--%>
<div class="form-item">
    <div class="form-label">
        <asp:Label ID="lblEdit" runat="server" AssociatedControlID="ddlEditBirthdayMonth">
            <asp:Literal ID="litEdit" runat="server"></asp:Literal>
        </asp:Label>
    </div>    
    <div class="form-input">
        <asp:DropDownList ID="ddlEditBirthdayMonth" runat="server"></asp:DropDownList>
        <asp:DropDownList ID="ddlEditBirthdayDay" runat="server"></asp:DropDownList>
        <asp:TextBox ID="txtEditBirthdayYear" runat="server" MaxLength="4" CssClass="short"></asp:TextBox>
        <asp:Literal ID="litRequired" runat="server"></asp:Literal>
        <div id="itemErrorContainer" runat="server" style="display:none;" class="form-error-inline"></div>
    </div>
</div>