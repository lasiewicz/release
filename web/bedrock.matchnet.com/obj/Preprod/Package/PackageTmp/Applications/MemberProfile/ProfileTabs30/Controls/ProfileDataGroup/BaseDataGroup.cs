﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.Member.ValueObjects.Interfaces;
using Matchnet.Web.Framework;

namespace Matchnet.Web.Applications.MemberProfile.ProfileTabs30.Controls.ProfileDataGroup
{
    /// <summary>
    /// Base class for data group controls, which are specifically created
    /// to determine how to render profile data defined as ProfileDataGroup objects.
    /// </summary>
    public class BaseDataGroup : BaseProfile
    {
        protected XMLProfileData.ProfileDataGroup _ProfileDataGroup;
        public bool IsPartialRender { get; set; }

        public BaseDataGroup()
        {
            
        }

        public virtual void LoadDataGroup(IMemberDTO member, XMLProfileData.ProfileDataGroup profileDataGroup)
        {
        }

        public string GetDataGroupAttributeDisplayValue(IMemberDTO member, string attributeName, FrameworkControl resourceControl, bool noBlankText)
        {
            _MemberProfile = member;
            return base.GetMemberAttributeValue(attributeName, resourceControl, noBlankText);
        }
    }

    public enum DataGroupDisplayMode
    {
        None = 0,
        OneColumn = 1,
        TwoColumn = 2
    }
}
