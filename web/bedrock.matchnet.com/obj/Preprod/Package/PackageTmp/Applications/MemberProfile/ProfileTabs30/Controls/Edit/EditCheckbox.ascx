﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EditCheckbox.ascx.cs" Inherits="Matchnet.Web.Applications.MemberProfile.ProfileTabs30.Controls.Edit.EditCheckbox" %>

<%--Checkbox as dt item (used as part of modal forms)--%>
<asp:PlaceHolder ID="phDTEditItem" runat="server" Visible="false">
<div class="form-item">
    <div class="form-label">
        <asp:Label ID="lblEdit" runat="server">
            <asp:Literal ID="litEdit" runat="server"></asp:Literal>
        </asp:Label>
    </div>    
    <div class="form-input">
        <%--Single column--%>
        <asp:PlaceHolder ID="phDTEditItemSingle" runat="server" Visible="false">
        <ul class="checkbox-list">
            <asp:Repeater ID="repeaterDTCheckBoxEdit" runat="server" OnItemDataBound="rptOptions_ItemDataBound">
                <ItemTemplate>
                    <li><asp:CheckBox ID="chkItemEdit" runat="server" /></li>
                </ItemTemplate>
            </asp:Repeater>
        </ul>
        </asp:PlaceHolder>

        <%--Double columns--%>
        <asp:PlaceHolder ID="phDTEditItemDouble" runat="server" Visible="false">
        <ul class="two-column checkbox-list">
            <asp:Repeater ID="repeaterDTCheckboxEditDoubleA" runat="server" OnItemDataBound="rptOptions_ItemDataBound">
                <ItemTemplate>
                    <li><asp:CheckBox ID="chkItemEdit" runat="server" /></li>
                </ItemTemplate>
            </asp:Repeater>
        </ul>
        <ul class="two-column checkbox-list">
            <asp:Repeater ID="repeaterDTCheckboxEditDoubleB" runat="server" OnItemDataBound="rptOptions_ItemDataBound">
                <ItemTemplate>
                    <li><asp:CheckBox ID="chkItemEdit" runat="server" /></li>
                </ItemTemplate>
            </asp:Repeater>
        </ul>
        </asp:PlaceHolder>

        <div id="itemErrorContainer" runat="server" style="display:none;" class="form-error-inline"></div>
    </div>
</div>

</asp:PlaceHolder>

<%--Checkbox as regular item only (used as part of in-line essay)--%>
<asp:PlaceHolder ID="phEditItem" runat="server" Visible="false">
<div>
    <h3><asp:Literal ID="literalEditName" runat="server"></asp:Literal></h3>
    <%--Single column--%>
    <asp:PlaceHolder ID="phEditItemSingle" runat="server" Visible="false">
    <fieldset>
        <ul class="checkbox-list">
            <asp:Repeater ID="repeaterCheckBoxEdit" runat="server" OnItemDataBound="rptOptions_ItemDataBound">
                <ItemTemplate>
                    <li><asp:CheckBox ID="chkItemEdit" runat="server" /></li>
                </ItemTemplate>
            </asp:Repeater>
        </ul>
    </fieldset>
    </asp:PlaceHolder>

    <%--Double columns--%>
    <asp:PlaceHolder ID="phEditItemDouble" runat="server" Visible="false">
    <fieldset>
        <ul class="two-column checkbox-list">
            <asp:Repeater ID="repeaterEditItemDoubleA" runat="server" OnItemDataBound="rptOptions_ItemDataBound">
                <ItemTemplate>
                    <li><asp:CheckBox ID="chkItemEdit" runat="server" /></li>
                </ItemTemplate>
            </asp:Repeater>
        </ul>
        <ul class="two-column checkbox-list">
            <asp:Repeater ID="repeaterEditItemDoubleB" runat="server" OnItemDataBound="rptOptions_ItemDataBound">
                <ItemTemplate>
                    <li><asp:CheckBox ID="chkItemEdit" runat="server" /></li>
                </ItemTemplate>
            </asp:Repeater>
        </ul>
    </fieldset>
    </asp:PlaceHolder>
    <div id="itemErrorContainer2" runat="server" style="display:none;" class="form-error-inline"></div>
</div>
</asp:PlaceHolder>

<asp:HiddenField ID="hidEditValue" runat="server" />
<asp:HiddenField ID="hidOriginalValue" runat="server" />