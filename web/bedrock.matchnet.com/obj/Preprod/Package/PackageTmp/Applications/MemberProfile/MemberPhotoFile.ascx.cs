using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Net;
using System.IO;

using Matchnet.Web.Framework;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects;


namespace Matchnet.Web.Applications.MemberProfile
{
	public class MemberPhotoFile : FrameworkControl
	{

		private void Page_Load(object sender, System.EventArgs e)
		{
			try
			{
				Response.Clear();

				string path = Request.QueryString["Path"];

				if (path == null)
				{
					show404();
				}
				else
				{
					//make sure we are not using this page to proxy requests to any http location
					Uri uri = new Uri(path.ToLower());
					if (BrandConfigSA.Instance.GetBrands().GetBrand(uri.Host) != null || uri.Host.EndsWith("matchnet.com"))
					{
						HttpWebRequest webRequest = (HttpWebRequest)WebRequest.CreateDefault(uri);
						HttpWebResponse webResponse = (HttpWebResponse)webRequest.GetResponse();
						Response.ContentType = "image/jpeg";
						System.IO.Stream s = webResponse.GetResponseStream(); 
						byte[] buffer = new Byte[1024]; 
						int iread = s.Read(buffer, 0, buffer.Length); 
						while(iread > 0) 
						{ 
							Response.BinaryWrite(buffer); 
							iread = s.Read(buffer, 0, buffer.Length); 
						} 
						s.Close(); 
						webResponse.Close();
					}
					else
					{
						show404();
					}
				}


				Response.End();
			}
			catch (Exception ex)
			{
				g.ProcessException(ex);
				show404();
			}
		}


		private void show404()
		{
			Response.StatusCode = 404;
			Response.Write("404 - not found");
		}


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
