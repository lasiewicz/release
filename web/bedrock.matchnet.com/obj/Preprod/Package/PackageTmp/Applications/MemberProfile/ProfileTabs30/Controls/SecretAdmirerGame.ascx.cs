﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Member.ValueObjects.Interfaces;
using Matchnet.Web.Framework.Managers;
using Matchnet.Web.Framework.Ui;
using Matchnet.Web.Framework;
using Matchnet.List.ValueObjects;
using Matchnet.Web.Framework.Ui.BasicElements;
using Matchnet.Web.Applications.Home;

namespace Matchnet.Web.Applications.MemberProfile.ProfileTabs30.Controls
{
    public partial class SecretAdmirerGame : BaseProfile
    {
        BreadCrumbHelper.EntryPoint _entryPoint = BreadCrumbHelper.EntryPoint.Unknown;

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public void LoadSecretAdmirerGame(IMemberDTO member, BreadCrumbHelper.EntryPoint entryPoint)
        {
            if (member != null)
            {
                _MemberProfile = member;
                _entryPoint = entryPoint;
                if (entryPoint == BreadCrumbHelper.EntryPoint.SlideShowPage || entryPoint == BreadCrumbHelper.EntryPoint.MemberSlideshow
                    || HomeUtil.DisplaySecretAdmirerGameFromHomepageFilmstrip(entryPoint))
                {
                    SetSlideshowHeaderLinks();
                }
                else
                {
                    this.Visible = false;
                }
            }
        }

        private void SetSlideshowHeaderLinks()
        {
            lnkBackToHome.NavigateUrl = "http://" + HttpContext.Current.Request.Url.Host + "/";
            if (_entryPoint == BreadCrumbHelper.EntryPoint.SlideShowPage)
            {
                lnkBackToHome.NavigateUrl = "/Applications/HotList/SlideShow.aspx";
                literalBackToHome.Text = g.GetResource("TXT_BACK_TO_SLIDESHOW_PAGE", this);
            }
            else if (_entryPoint == BreadCrumbHelper.EntryPoint.MemberSlideshow 
                || _entryPoint == BreadCrumbHelper.EntryPoint.HomeProfileFilmstripWithVoting
                || _entryPoint == BreadCrumbHelper.EntryPoint.HomeProfileFilmstripWithVotingMOL)
            {
                literalBackToHome.Text = g.GetResource("TXT_BACK_TO_HOME", this);
            }

            Int32 ordinal;
            Int32.TryParse(HttpContext.Current.Request.Params["Ordinal"], out ordinal);
            ordinal++;

            string viewProfileURL = BreadCrumbHelper.MakeViewProfileLink(_entryPoint, 0, ordinal, null, Constants.NULL_INT);
            string slideshowParam = Matchnet.Web.Framework.Search.Constants.Parameters.SlideshowNavigationRequest + "=true";
            if (_entryPoint == BreadCrumbHelper.EntryPoint.HomeProfileFilmstripWithVoting)
            {
                slideshowParam += "&" +
                                  Matchnet.Web.Framework.Search.Constants.Parameters.SlideShowFilmstripNavigationRequest +
                                  "=true";
            }
            else if (_entryPoint == BreadCrumbHelper.EntryPoint.HomeProfileFilmstripWithVotingMOL)
            {
                slideshowParam += "&" +
                                  Matchnet.Web.Framework.Search.Constants.Parameters.SlideShowFilmstripMOLNavigationRequest +
                                  "=true";
            }
            viewProfileURL = BreadCrumbHelper.AppendParamToProfileLink(viewProfileURL, slideshowParam);

            WebConstants constants = new WebConstants();

            //NOTE: These YNM buttons are wired up automatically in slideshow.js to record the vote and redirect to next profile
            lnkYes.Attributes.Add("data-displaytype", SlideshowDisplayType.Full.ToString("d"));
            lnkYes.Attributes.Add("data-urlparamslideshowdisplaytype", WebConstants.URL_PARAMETER_SLIDESHOW_DISPLAY_TYPE);
            lnkYes.Attributes.Add("data-ynmparams", GetYNMParams());
            lnkYes.Attributes.Add("data-slideshowmemberid", _MemberProfile.MemberID.ToString());
            lnkYes.Attributes.Add("data-clicktype", ((int)ClickListType.Yes).ToString());
            //lnkYes.Attributes.Add("onclick", string.Format(constants.JSCRIPT_SLIDESHOW_VOTE, GetYNMParams(), (int)ClickListType.Yes, _MemberProfile.MemberID));
            lnkYes.NavigateUrl = BreadCrumbHelper.AppendParamToProfileLink(viewProfileURL, "YNMClickType=" + ((int)ClickListType.Yes).ToString() + "&VMID=" + _MemberProfile.MemberID.ToString());

            lnkNo.Attributes.Add("data-displaytype", SlideshowDisplayType.Full.ToString("d"));
            lnkNo.Attributes.Add("data-urlparamslideshowdisplaytype", WebConstants.URL_PARAMETER_SLIDESHOW_DISPLAY_TYPE);
            lnkNo.Attributes.Add("data-ynmparams", GetYNMParams());
            lnkNo.Attributes.Add("data-slideshowmemberid", _MemberProfile.MemberID.ToString());
            lnkNo.Attributes.Add("data-clicktype", ((int)ClickListType.No).ToString());
            //lnkNo.Attributes.Add("onclick", string.Format(constants.JSCRIPT_SLIDESHOW_VOTE, GetYNMParams(), (int)ClickListType.No, _MemberProfile.MemberID));
            lnkNo.NavigateUrl = BreadCrumbHelper.AppendParamToProfileLink(viewProfileURL, "YNMClickType=" + ((int)ClickListType.No).ToString() + "&VMID=" + _MemberProfile.MemberID.ToString());

            lnkMaybe.Attributes.Add("data-displaytype", SlideshowDisplayType.Full.ToString("d"));
            lnkMaybe.Attributes.Add("data-urlparamslideshowdisplaytype", WebConstants.URL_PARAMETER_SLIDESHOW_DISPLAY_TYPE);
            lnkMaybe.Attributes.Add("data-ynmparams", GetYNMParams());
            lnkMaybe.Attributes.Add("data-slideshowmemberid", _MemberProfile.MemberID.ToString());
            lnkMaybe.Attributes.Add("data-clicktype", ((int)ClickListType.Maybe).ToString());
            //lnkMaybe.Attributes.Add("onclick", string.Format(constants.JSCRIPT_SLIDESHOW_VOTE, GetYNMParams(), (int)ClickListType.Maybe, _MemberProfile.MemberID));
            lnkMaybe.NavigateUrl = BreadCrumbHelper.AppendParamToProfileLink(viewProfileURL, "YNMClickType=" + ((int)ClickListType.Maybe).ToString() + "&VMID=" + _MemberProfile.MemberID.ToString());
        }

        private string GetYNMParams()
        {
            YNMVoteParameters parameters = new YNMVoteParameters();

            parameters.SiteID = _g.Brand.Site.SiteID;
            parameters.CommunityID = _g.Brand.Site.Community.CommunityID;
            parameters.FromMemberID = _g.Member.MemberID;
            parameters.ToMemberID = _MemberProfile.MemberID;

            return parameters.EncodeParams();

        }
    }
}