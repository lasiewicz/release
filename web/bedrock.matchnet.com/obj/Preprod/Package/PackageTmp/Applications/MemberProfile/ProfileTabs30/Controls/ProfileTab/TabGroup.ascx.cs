﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Member.ValueObjects.Interfaces;
using Matchnet.Web.Applications.MemberProfile.ProfileTabs30.XMLProfileTabConfig;
using System.Web.UI.HtmlControls;
using System.Text;
using Matchnet.Web.Framework.Ui;

namespace Matchnet.Web.Applications.MemberProfile.ProfileTabs30.Controls.ProfileTab
{
    /// <summary>
    /// This control is responsible for managing a group of tabs, loading up the various Tab controls
    /// based on tabs defined in the Tab_siteID.xml files for each site.
    /// </summary>
    public partial class TabGroup : BaseProfile
    {
        private ProfileTabEnum _ActiveTab = ProfileTabEnum.None;
        protected bool _IsViewingOwnProfile = false;
        protected int _ActiveTabIndex = 0;

        #region Event Handlers
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void RepeaterTabs_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                //get data object
                ProfileTabConfig tabConfig = e.Item.DataItem as ProfileTabConfig;

                Literal literalLinkText = e.Item.FindControl("literalLinkText") as Literal;
                literalLinkText.Text = g.GetResource(tabConfig.ResourceConstant, this);

                Literal literalOpeningAnchor = e.Item.FindControl("literalOpeningAnchor") as Literal;
                literalOpeningAnchor.Text = "<a href=\"#tab-" + tabConfig.ProfileTabType.ToString() + "\">";
            }
        }

        protected void RepeaterTabContent_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                //get data object
                ProfileTabConfig tabConfig = e.Item.DataItem as ProfileTabConfig;
                if (tabConfig.ProfileTabType == _ActiveTab)
                    _ActiveTabIndex = e.Item.ItemIndex;

                Literal literalOpeningTabDiv = e.Item.FindControl("literalOpeningTabDiv") as Literal;
                string tabContentContainerID = "tab-" + tabConfig.ProfileTabType.ToString();
                literalOpeningTabDiv.Text = "<div class=\"tab-body clearfix\" id=\"" + tabContentContainerID + "\">";

                //load tab control
                if (!String.IsNullOrEmpty(tabConfig.TabControl))
                {
                    PlaceHolder phTabControl = e.Item.FindControl("phTabControl") as PlaceHolder;
                    BaseTab tabControl = LoadControl(tabConfig.TabControl) as BaseTab;
                    phTabControl.Controls.Add(tabControl);

                    bool isActive = (tabControl.ProfileTabType == this._ActiveTab);
                    tabControl.TabContentContainerID = tabContentContainerID;
                    tabControl.LoadTab(this.MemberProfile, isActive);
                }
            }
        }

        protected void RepeaterTabJS_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                //get data object
                ProfileTabConfig tabConfig = e.Item.DataItem as ProfileTabConfig;

                Literal literalTabJS = e.Item.FindControl("literalTabJS") as Literal;
                //create JS Tab object
                StringBuilder s = new StringBuilder("try {");
                s.Append("var tabObj" + e.Item.ItemIndex + "=new spark_profile30_Tab();");
                s.Append("tabObj" + e.Item.ItemIndex + ".tabType = '" + tabConfig.ProfileTabType.ToString() + "';");
                s.Append("tabObj" + e.Item.ItemIndex + ".tabIndex = " + e.Item.ItemIndex + ";");
                s.Append("tabObj" + e.Item.ItemIndex + ".GAMAdslotRight = '" + tabConfig.GAMAdSlotRight + "';");
                s.Append("tabObj" + e.Item.ItemIndex + ".GAMAdslotTop = '" + tabConfig.GAMAdSlotTop + "';");
                s.Append("tabObj" + e.Item.ItemIndex + ".GAMAdslotBelowCompose = '" + tabConfig.GAMAdSlotBelowCompose + "';");
                if (tabConfig.Omniture != null)
                {
                    s.Append("tabObj" + e.Item.ItemIndex + ".omniTabName = '" + tabConfig.Omniture.OmniTabName + "';");
                }

                //add to TabGroup
                s.Append("tabGroup.addTab(tabObj" + e.Item.ItemIndex + ");\n");
                s.Append("}catch(e){}\n");

                literalTabJS.Text = s.ToString();
            }
        }

        #endregion

        public PlaceHolder PhJavaScript
        {
            get
            {
                return phJavaScript;
            }
        }


        #region Public Methods
        public void LoadTabGroup(IMemberDTO member, ProfileTabEnum activeTab)
        {
            this._MemberProfile = member;
            this._ActiveTab = activeTab;
            if (g.Member != null && member.MemberID == g.Member.MemberID)
            {
                this.WhoseProfile = WhoseProfileEnum.Self;
                _IsViewingOwnProfile = true;
            }

            //get list of tabs to load for site
            ProfileTabConfigs profileTabConfigs = ProfileUtility.GetProfileTabConfigs(g.Brand.Site.SiteID, g);

            if (profileTabConfigs != null && profileTabConfigs.ProfileTabConfigList != null && profileTabConfigs.ProfileTabConfigList.Count > 0)
            {
                ProfileTabConfigs profileTabConfigsFinal = new ProfileTabConfigs();
                if (BreadCrumbHelper.IsProfilePopupDisabled(member.MemberID))
                {
                    profileTabConfigsFinal.ProfileTabConfigList = new List<ProfileTabConfig>();
                    foreach (ProfileTabConfig ptc in profileTabConfigs.ProfileTabConfigList)
                    {
                        if (!ptc.IsPopupOnly)
                            profileTabConfigsFinal.ProfileTabConfigList.Add(ptc);
                    }
                }
                else
                {
                    profileTabConfigsFinal = profileTabConfigs;
                }

                //bind to define tab structure and content
                repeaterTabs.DataSource = profileTabConfigsFinal.ProfileTabConfigList;
                repeaterTabs.DataBind();

                repeaterTabContent.DataSource = profileTabConfigsFinal.ProfileTabConfigList;
                repeaterTabContent.DataBind();

                repeaterTabJS.DataSource = profileTabConfigsFinal.ProfileTabConfigList;
                repeaterTabJS.DataBind();
            }

        }

        public void LoadMicroProfileTabGroup(IMemberDTO member, ProfileTabEnum activeTab)
        {
            this._MemberProfile = member;
            this._ActiveTab = activeTab;
            if (g.Member != null && member.MemberID == g.Member.MemberID)
            {
                this.WhoseProfile = WhoseProfileEnum.Self;
                _IsViewingOwnProfile = true;
            }

            // microMicroProfile doesn't need this js stuff.
            PhJavaScript.Visible = false;


            //get list of tabs to load for site
            ProfileTabConfigs profileTabConfigs = ProfileUtility.GetMicroProfileTabConfigs(g.Brand.Site.SiteID, g);

            if (profileTabConfigs != null && profileTabConfigs.ProfileTabConfigList != null && profileTabConfigs.ProfileTabConfigList.Count > 0)
            {
                //bind to define tab structure and content
                repeaterTabs.DataSource = profileTabConfigs.ProfileTabConfigList;
                repeaterTabs.DataBind();

                repeaterTabContent.DataSource = profileTabConfigs.ProfileTabConfigList;
                repeaterTabContent.DataBind();

                repeaterTabJS.DataSource = profileTabConfigs.ProfileTabConfigList;
                repeaterTabJS.DataBind();
            }

        }

        #endregion
    }
}
