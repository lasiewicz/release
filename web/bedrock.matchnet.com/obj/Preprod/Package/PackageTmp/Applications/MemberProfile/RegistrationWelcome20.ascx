﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RegistrationWelcome20.ascx.cs"
    Inherits="Matchnet.Web.Applications.MemberProfile.RegistrationWelcome20" %>
<asp:PlaceHolder ID="plcMBoxDefaultStart" runat="server" Visible="false">
    <div class="mboxDefault">
</asp:PlaceHolder>
<div id="page-container">
    <asp:PlaceHolder runat="server" ID="phRegistrationWelcome"></asp:PlaceHolder>
</div>
<asp:PlaceHolder ID="plcMBoxDefaultEnd" runat="server" Visible="false"></div> </asp:PlaceHolder>
<asp:PlaceHolder ID="PlaceHolderOmnitureMboxRegEnd" runat="server" Visible="false">

    <script type="text/javascript">
    try
    {
        mboxCreate(registrationConfirmationType); 
    }
    catch (e)
    {
        ;
    }
    </script>

    <script type="text/javascript">
        //set TNT variable when page loads (needs to be after mboxes have loaded) 
        var tntInput = s.trackTNT();

        //call this function after everything else has loaded 
        trackTNT(s_account, tntInput);

        /* For tracking TNT data in SC */
        function trackTNT(s_account, tntInput) {
            PopulateS(true); //this will clear out existing values that has already been submitted for the page

            var s = s_gi(s_account);
            s.linkTrackVars = "tnt,eVar16";
            s.linkTrackEvents = "None";

            //variable for TNT classifications 
            s.eVar16 = s.tnt = tntInput;

            s.tl(true, 'o', 'For tracking TNT');
        } 
    </script>

</asp:PlaceHolder>
<asp:PlaceHolder ID="plcRedirectToSubPage" runat="server" Visible="false">
    <style>
        #footer {display: none;}
        .nav-alternative {background-image: url();}
        .ie #content-container {border: 0px solid transparent;}
        body {background-image: url(''); background-color: #ffffff;}
    </style>
    <script type="text/javascript">
        $j(document).ready(function () {
            window.location.replace('/Applications/Subscription/Subscribe.aspx?prtid=41&RedirectToSubPage=1');
        });
    </script>
</asp:PlaceHolder>