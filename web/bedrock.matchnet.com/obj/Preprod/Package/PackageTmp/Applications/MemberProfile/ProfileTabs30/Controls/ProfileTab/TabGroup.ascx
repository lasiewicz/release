﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TabGroup.ascx.cs" Inherits="Matchnet.Web.Applications.MemberProfile.ProfileTabs30.Controls.ProfileTab.TabGroup" %>
<asp:PlaceHolder ID="phJavaScript" runat="server" Visible="true">
<script type="text/javascript">
    //define objects
    //1. Tab
    //2. TabGroup
    function spark_profile30_Tab() {
        this.tabType = null;
        this.tabIndex = null;
        this.tabContentId = null;
        this.isGAM = true;
        this.GAMAdslotRight = null;
        this.GAMAdslotTop = null;
        this.omniTabName = null;
    }
        
    function spark_profile30_TabGroup() {
        this.tabList = [];
        this.currentIndex = 0;
        this.previousIndex = 0;
        this.isViewingOwnProfile = null;
        this.tabNav = null;
        this.tabBody = null;
        this.tomaFunc = function(s) {
            console.log(s);
        }
        this.onTabClick = function(selectedIndex) {
            this.previousIndex = this.currentIndex;
            this.currentIndex = selectedIndex;
            
            var selectedTab = this.tabList[selectedIndex];
            if (selectedTab != null && selectedIndex != this.previousIndex)
            {
                //refresh ad
                this.refreshAds();

                //update omniture
                if (s != null) {
                    PopulateS(true); //clear existing values in omniture "s" object
                    s.events = "event4,event2";
                    s.eVar9 = selectedTab.omniTabName;
                    s.pageName = "View Profile - Tab " + (selectedTab.tabIndex + 1) + " - " + selectedTab.omniTabName;
                    spark_profile30_OmnitureCurrentPageName = s.pageName;
                    s.t(); //send omniture updated values as page load
                }
            }
        }
        this.goToNext = function() {
            var nextIndex = this.currentIndex + 1;
            if (nextIndex >= this.tabList.length) {
                nextIndex = 0;
            }
            //$j("#profileTabGroup").tabs('select', nextIndex); // jquery tab ui
            this.updateTab(nextIndex);
            this.onTabClick(nextIndex);
        }
        this.goToPrevious = function() {
            var prevIndex = this.currentIndex - 1;
            if (prevIndex < 0) {
                prevIndex = this.tabList.length - 1;
            }
            //$j("#profileTabGroup").tabs('select', prevIndex); // jquery tab ui
            this.updateTab(prevIndex);
            this.onTabClick(prevIndex);
        }
        this.goToTab = function(tabIndex) {
            if (tabIndex >= 0 && tabIndex < this.tabList.length) {
                //$j("#profileTabGroup").tabs('select', tabIndex); // jquery tab ui
                this.updateTab(tabIndex);
                this.onTabClick(tabIndex);
            }
        }
        this.updateTab = function(tabIndex){

            var $firstTab = this.tabBody.first();

            this.tabNav.removeClass('active').eq(tabIndex).addClass('active');

            if(tabIndex != 0 && $firstTab.is(':visible')){
                $firstTab.hide();
            } else if(tabIndex == 0){
                $firstTab.show();
            }

            this.tabBody.removeClass('active').eq(tabIndex).addClass('active');

        }
        this.addTab = function(tabObj) {
            spark.util.addItem(this.tabList, tabObj);
        }
        this.refreshAds = function() {
            var selectedTab = this.tabList[this.currentIndex];
            
            if (selectedTab != null)
            {
                //refresh ad
                if (selectedTab.GAMAdslotTop && selectedTab.GAMAdslotTop.length > 0) {
                    try {
                        updateTop728by90SR(topAd728x90IFrameID, selectedTab.GAMAdslotTop, selectedTab.isGAM);
                    } catch (e) { }
                }
                if (selectedTab.GAMAdslotRight && selectedTab.GAMAdslotRight.length > 0) {
                    try {
                        updateRight300by250SR(rightAd300x250IFrameID, selectedTab.GAMAdslotRight, selectedTab.isGAM);
                    } catch (e) { }
                }
                 if (selectedTab.GAMAdslotBelowCompose && selectedTab.GAMAdslotBelowCompose.length > 0) {
                    try {
                        updateBelowCompose234by60SR(belowComposeAd234x60IFrameID, selectedTab.GAMAdslotBelowCompose, selectedTab.isGAM);
                    } catch (e) { }
                }
            }
        }
    }

    //initialize objects
    var tabGroup = new spark_profile30_TabGroup();
    tabGroup.isViewingOwnProfile = <%=_IsViewingOwnProfile.ToString().ToLower() %>;
    tabGroup.previousIndex = <%= _ActiveTabIndex %>;
    tabGroup.currentIndex = <%= _ActiveTabIndex %>;
    
    $j(function() {
        // Essay ellipsis
        $j("#tab-Essays p, #tab-MoreEssays p", $j('#profileTabGroup')[0]).expander({
                slicePoint:1000,
                expandEffect: 'show',
                expandPrefix:'&nbsp;',
                expandText:'[...]',
                userCollapsePrefix:'&nbsp;',
                userCollapseText:'[^]'
            });
        
        var $tabEssays = $j("#tab-Essays, #tab-MoreEssays");
        
        $tabEssays.find(".comment-block textarea").watermark("<%=g.GetResource("TXT_COMMENT_WATERMARK",this) %>");
        $tabEssays.bind('click',function(event){
            var $target = $j(event.target);
            $target.closest('.comment-label').toggleClass('hide').parents('.like-container').addClass('on').find('.comment-block').toggleClass('hide');
            $target.closest('.close').parents('.comment-block').toggleClass('hide').parents('.like-container').removeClass('on').find('.comment-label').toggleClass('hide');
        });
    });
    
</script>
</asp:PlaceHolder>

<div id="profileTabGroup" class="profile30-tabs sp-tab">
    <a name="tabBookmark"></a>
    <!--tabs-->
    <ul class="tab-nav clearfix">
    <asp:Repeater ID="repeaterTabs" runat="server" OnItemDataBound="RepeaterTabs_ItemDataBound">
        <ItemTemplate>
            <li id="tabItem" runat="server">
                <asp:Literal ID="literalOpeningAnchor" runat="server"></asp:Literal>
                <asp:Literal ID="literalLinkText" runat="server"></asp:Literal>
                <asp:Literal ID="literalClosingAnchor" runat="server" Text="</a>"></asp:Literal>
            </li>
        </ItemTemplate>
    </asp:Repeater>
    </ul>
    
    <!--tab content areas-->
    <asp:Repeater ID="repeaterTabContent" runat="server" OnItemDataBound="RepeaterTabContent_ItemDataBound">
        <ItemTemplate>
            <asp:Literal ID="literalOpeningTabDiv" runat="server"></asp:Literal>
            <asp:PlaceHolder ID="phTabControl" runat="server"></asp:PlaceHolder>
            <asp:Literal ID="literalClosingTabDiv" runat="server" Text="</div>"></asp:Literal>
        </ItemTemplate>
    </asp:Repeater>
    
    <!--tab JS-->
    <asp:Repeater ID="repeaterTabJS" runat="server" OnItemDataBound="RepeaterTabJS_ItemDataBound">
        <ItemTemplate>
            <script type="text/javascript">
                <asp:Literal ID="literalTabJS" runat="server"></asp:Literal>
            </script>
        </ItemTemplate>
    </asp:Repeater>
    
</div>
<script>
    if (typeof tabGroup !== "undefined") {
        tabGroup.tabNav = $j("#profileTabGroup .tab-nav > li");
        tabGroup.tabBody = $j("#profileTabGroup .tab-body");

        tabGroup.tabNav.first().addClass('active');
        tabGroup.tabBody.first().addClass('active');

        if (tabGroup.currentIndex > 0) {
            tabGroup.goToTab(tabGroup.currentIndex);
        }

        tabGroup.tabNav.on("click", "a", function (e) {
            var $this = $j(this),
                $thisTab = $this.closest('li'),
                tabIndex = tabGroup.tabNav.index($thisTab);

            e.preventDefault();
            tabGroup.goToTab(tabIndex);
        });
    }
</script>
