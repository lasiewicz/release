﻿using System;
using System.Threading;
using Matchnet.Web.Framework;

namespace Matchnet.Web.Applications.MemberProfile
{
    /// <summary>
    /// Throttles the RegistrationWelcome Page.
    /// 
    /// ModValue: 2 // Feature is enabled for all
    /// ModValue: 0 // Feature is enabled for Even MIDs
    /// ModValue: 1 // Feature is enabled for Odd MIDs
    /// </summary>
    public static class RegistrationWelcomePageThrottler
    {
        private const String RegistrationWelcomePageModSettingName = "RegistrationWelcomePageMod";

        private const Int32 ModValue = 2;

        private const Int32 SiteId = 103;

        private const Int32 ExpirationInSeconds = 15;

        private static readonly TimeSpan thresholdExpiration = new TimeSpan(0, 0, 0, ExpirationInSeconds);

        private static Int64 lastUpdate = DateTime.UtcNow.Ticks;

        private static Int32 threshold = GetNewThreshold();

        private static readonly Object lockObject = new Object();

        /// <summary>
        /// Gets or sets the threshold. If the threshold is
        /// equal to 0, nothing's getting through.
        /// </summary>
        /// <value>The threshold.</value>
        public static Int32 Threshold
        {
            get
            {
                if (DateTime.UtcNow.Subtract(new DateTime(lastUpdate, DateTimeKind.Utc)) > thresholdExpiration)
                {
                    lock (lockObject)
                    {
                        Interlocked.Exchange(ref threshold, GetNewThreshold());
                        Interlocked.Exchange(ref lastUpdate, DateTime.UtcNow.Ticks);
                    }
                }

                return threshold;
            }
        }

        /// <summary>
        /// Determines whether this feature is enabled for a global context (hint: it looks
        /// at the <see cref="ContextGlobal"/>'s Member). Wait... that sounds just wrong.
        /// </summary>
        /// <param name="contextGlobal">The global context.</param>
        /// <returns>
        /// 	<c>true</c> if the feature is enabled; otherwise, <c>false</c>.
        /// </returns>
        public static Boolean IsFeatureEnabled(ContextGlobal contextGlobal)
        {
            int threshold = GetNewThreshold(contextGlobal);
            return IsEnabled(threshold, contextGlobal);
        }



        /// <summary>
        /// Determines whether this feature is enabled for a particular member.
        /// </summary>
        /// <param name="member">The member.</param>
        /// <returns>
        /// 	<c>true</c> if the this feature is enabled; otherwise, <c>false</c>.
        /// </returns>
        public static Boolean IsFeatureEnabled(Member.ServiceAdapters.Member member, ContextGlobal g)
        {
            int threshold = GetNewThreshold(g);
            return IsEnabled(threshold, member, g);
        }

        /// <summary>
        /// Determines whether this feature is enabled for a particular member id.
        /// </summary>
        /// <param name="memberId">The member id.</param>
        /// <returns>
        /// 	<c>true</c> if the feature is enabled; otherwise, <c>false</c>.
        /// </returns>
        public static Boolean IsFeatureEnabled(Int32 memberId, ContextGlobal g)
        {
            int threshold = GetNewThreshold(g);
            if (threshold == ModValue) return true;

            return IsEnabled(threshold, memberId);
        }

        private static Boolean IsEnabled(Int32 featureThreshold, ContextGlobal contextGlobal)
        {
            if (contextGlobal == null || contextGlobal.Brand == null || contextGlobal.Brand.Site == null) return false;

            if (!IsMasterSettingEnabled(contextGlobal)) return false;

            //if (contextGlobal.Brand.Site.SiteID != SiteId) return false;

            return IsEnabled(featureThreshold, contextGlobal.Member, contextGlobal);
        }

        private static Boolean IsEnabled(Int32 featureThreshold, Member.ServiceAdapters.Member member, ContextGlobal contextGlobal)
        {
            if (!IsMasterSettingEnabled(contextGlobal)) return false;
            if (featureThreshold == ModValue) return true;
            

            return member != null && IsEnabled(featureThreshold, member.MemberID);
        }

        private static Boolean IsEnabled(Int32 featureThreshold, Int32 memberId)
        {
            return memberId > 0 && memberId % ModValue == featureThreshold;
        }

        private static Int32 GetNewThreshold()
        {
            return Int32.Parse(
                Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting(
                    RegistrationWelcomePageModSettingName,
                    SiteId
                )
            );
        }

        private static Int32 GetNewThreshold(ContextGlobal g)
        {
            return Int32.Parse(
                Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting(
                    RegistrationWelcomePageModSettingName,
                    g.Brand.Site.SiteID
                )
            );
        }

        private static bool IsMasterSettingEnabled(ContextGlobal g)
        {
            return bool.Parse(
                Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting(
                    "ENABLE_WELCOME_REDIRECT_TO_SUB",
                    g.Brand.Site.Community.CommunityID,
                    g.Brand.Site.SiteID,
                    g.Brand.BrandID
                )
            );
        }

    }
}