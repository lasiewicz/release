﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NameValueDataSectionSingle.ascx.cs" Inherits="Matchnet.Web.Applications.MemberProfile.ProfileTabs30.Controls.ProfileDataGroup.NameValueDataSectionSingle" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>

<%--section view container--%>
<div id="viewSectionContainer" class="prof-edit-region " runat="server">
    <asp:PlaceHolder ID="phEditIcon" runat="server" Visible="false">
        <div class="prof-edit-button">
            <asp:Literal ID="litEditIcon" runat="server"></asp:Literal>
        </div>
    </asp:PlaceHolder>
    <asp:PlaceHolder ID="phTitle" runat="server" Visible="false">
        <h3>
            <asp:Literal ID="literalTitle" runat="server"></asp:Literal></h3>
    </asp:PlaceHolder>
    <dl class="dl-form form-inside clearfix">
        <asp:Repeater ID="repeaterItem" runat="server" OnItemDataBound="repeaterItem_ItemDataBound">
            <ItemTemplate>
                <asp:PlaceHolder runat="server" ID="plcShow" Visible="true">
                    <dt>
                        <asp:Literal ID="literalName" runat="server"></asp:Literal></dt>
                    <dd id="ddValue" runat="server">
                        <asp:Literal ID="literalValue" runat="server"></asp:Literal></dd>
                </asp:PlaceHolder>
            </ItemTemplate>
        </asp:Repeater>
    </dl>
</div>

<%--edit view container--%>
<asp:PlaceHolder ID="phEditSection" runat="server" Visible="false">
    <div id="editSectionContainer" runat="server" class="form-set hide">
        <div id="errorSectionContainer" runat="server" class="form-error hide"></div>
        <fieldset class="padding-heavy">
            <asp:Literal ID="litRequiredMessage" runat="server"></asp:Literal>
            <asp:PlaceHolder ID="phEditItemControl" runat="server"></asp:PlaceHolder>
            <%--Submit--%>
            <div class="form-item form-submit">
                <asp:Button ID="buttonEditSave" runat="server" CssClass="btn primary button-show" UseSubmitBehavior="false" />
                <asp:Button ID="buttonEditCancel" runat="server" CssClass="btn secondary btn-cancel" UseSubmitBehavior="false" />
            </div>
        </fieldset>
    </div>
</asp:PlaceHolder>
