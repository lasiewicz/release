﻿using System;
using System.Web;
using Matchnet.Member.ValueObjects.Interfaces;
using Matchnet.Web.Framework.Ui;
using Matchnet.Web.Framework;
using Matchnet.Web.Analytics;
using Matchnet.Web.Framework.Ui.Feedback;

namespace Matchnet.Web.Applications.MemberProfile.ProfileTabs30
{
    /// <summary>
    /// Standard template which acts as the overall profile page container.
    /// </summary>
    public partial class VisitorProfileTemplate : BaseProfileTemplate
    {
        #region Event Handlers
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected override void OnPreRender(EventArgs e)
        {
            try
            {
                //omniture
                lnkJoinNowBottom.NavigateUrl = lnkJoinNowBottom.NavigateUrl + Omniture.GetOmnitureClickToPageURLParams(ProfileUtility.ProfilePageName, WebConstants.PageIDs.Registration, "", WebConstants.Action.ViewProfile, false);

                base.OnPreRender(e);
            }
            catch (Exception ex)
            { g.ProcessException(ex); }
        }

        #endregion

        /// <summary>
        /// Loads member profile information
        /// </summary>
        /// <param name="member"></param>
        /// <param name="activeTab"></param>
        public override void LoadProfile(IMemberDTO member, ProfileTabEnum activeTab)
        {
            if (member == null) return;
            //for testing purposes only.  remove before submit
            //Matchnet.Web.Framework.VisitorLimitHelper.ResetVisitorLimits(ref _g);

            //set base properties
            this._MemberProfile = member;
            this.ActiveTab = activeTab;

            //timestamps
            DateTime loginDate = _MemberProfile.GetLastLogonDate(g.Brand.Site.Community.CommunityID);
            lastLoginDate.LastLoginDateValue = loginDate;
            lastUpdateDate.Date = getDateDisplay(_MemberProfile.GetAttributeDate(g.Brand, "LastUpdated", loginDate));

            //set Entry Point
            entryPoint = BreadCrumbHelper.GetEntryPoint(Conversion.CInt(Request[WebConstants.URL_PARAMETER_NAME_ENTRYPOINT]));

            //Load photo slider
            PhotoSlider1.LoadPhotoSlider(_MemberProfile);

            //Load M2MCommunication module
            m2mCommunication1.MutualYesElementID = basicInfo1.SecretAdmirerControl.ClientID;
            m2mCommunication1.LoadM2MCommunication(_MemberProfile, ProfileUtility.ProfilePageName);

            //Load Basic Info
            basicInfo1.LoadBasicInfo(_MemberProfile);
            
            //Load Dealbreaker info
            nvDataGroupDealBreakerInfo.MaxDisplayLength = ProfileUtility.MaxDisplayLengthNVDouble;
            nvDataGroupDealBreakerInfo.LoadDataGroup(member, ProfileUtility.GetProfileDataGroup(ProfileDataGroupEnum.Dealbreakers, g.Brand.Site.SiteID, g));

            //Hide Icon Links in master Layout Template
            if (g.LayoutTemplateBase != null)
            {
                g.LayoutTemplateBase.HideIconLinks();
            }

            //Load About Me Essay
            essayDataGroupAboutMe.LoadDataGroup(member, ProfileUtility.GetProfileDataGroup(ProfileDataGroupEnum.VisitorViewEssay, g.Brand.Site.SiteID, g));

            //Email Me section at the bottom
            lnkJoinNowBottom.NavigateUrl = g.GetLogonRedirect(HttpContext.Current.Request.RawUrl.ToString(), null);

            //Show Feedback widget
            if (g.LayoutTemplateBase != null)
            {
                if (FeedbackHelper.IsProfile30FeedbackEnabled(g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID))
                {
                    g.LayoutTemplateBase.ShowFeedback(Matchnet.Web.Framework.Ui.Feedback.FeedbackHelper.FeedbackType.BetaProfile);
                }
            }

        }

        
    }
}