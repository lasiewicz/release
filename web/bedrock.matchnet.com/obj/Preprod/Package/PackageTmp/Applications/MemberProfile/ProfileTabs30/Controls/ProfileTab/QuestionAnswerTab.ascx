﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="QuestionAnswerTab.ascx.cs" Inherits="Matchnet.Web.Applications.MemberProfile.ProfileTabs30.Controls.ProfileTab.QuestionAnswerTab" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="uc2" Src="~/Framework/Ui/BasicElements/LastTime.ascx" TagName="LastTime" %>
<%@ Register TagPrefix="ml" TagName="MemberLikeAnswerClientModule" Src="~/Applications/MemberLike/MemberLikeAnswerClientModule.ascx" %>
<%@ Register TagPrefix="qa" TagName="MiniAnswerProfile" Src="~/Applications/QuestionAnswer/Controls/MiniAnswerProfile.ascx" %>
<%@ Register TagPrefix="qa2" TagName="BothAnsweredQuestionModule" Src="~/Applications/QuestionAnswer/Controls/BothAnsweredQuestionModule.ascx" %>
<%@ Register TagPrefix="qa2" TagName="BlockedAnsweredQuestionModule" Src="~/Applications/QuestionAnswer/Controls/BlockedAnsweredQuestionModule.ascx" %>

<script type="text/javascript">
    //<![CDATA[
    (function() {
        __addToNamespace__('spark.qanda', {
            IsQandADouble:<%=isQandADoubleEnabled().ToString().ToLower()%>,
            IsCommentsEnabled:<%=IsCommentsEnabled.ToString().ToLower() %>,       
            IsMemberLikeEnabled:<%=IsMemberLikeEnabled.ToString().ToLower() %>
        });
    })();
    
    function ShowQADeleteConfirmation(answerID, questionID) {

        //set selected answer
        $j('#hidAnswerID').val(answerID);
        $j('#hidQuestionID').val(questionID);
        
        $j('#hidAction').val('remove');
        
        //show modal
        var modalcontent = $j('#qanda-delete-message');
        $j.blockUI({
                message: modalcontent,
                overlayCSS: {
                    backgroundColor: '#000',
                    opacity: '0.5'
                },
                css: {}
        });
    
        $j('.blockOverlay, #qanda-delete-message .delete-no').click($j.unblockUI);
        $j('#qanda-delete-message .delete-yes').click($j.unblockUI);
        $j('#qanda-delete-message .delete-yes').click(function() {
            //console.log('fire delete function here');
            SubmitQADelete();
        });


    }

    function SubmitQADelete() {
        //switch to Ajax later
        document.forms[0].submit();
    }
    //]]>            
</script>        

<asp:panel id="panelQuestionAnswerTab" Runat="server">
    
    <%--Display Answers--%>
    <asp:PlaceHolder ID="phHasAnswers" runat="server">    
        <dl id="qanda-view-profile" class="profile-content">

            <asp:Repeater ID="repeaterQuestionAnswers" runat="server" OnItemDataBound="repeaterQuestionAnswers_ItemDataBound">
                <ItemTemplate>
                    <dt>
                        <%if (IsViewingOwnProfile)
                          { %>
                        <input type="button" value="Delete" class="btn secondary delete" runat="server" id="btnDelete" />
                        <%} %>
                        <mn2:FrameworkLiteral ID="literalQ" runat="server" ResourceConstant="TXT_Q"></mn2:FrameworkLiteral>
                        <asp:HyperLink ID="lnkQuestion" runat="server" 
                            NavigateUrl="/Applications/QuestionAnswer/Question.aspx"><asp:Literal 
                            ID="literalQuestion" runat="server"></asp:Literal></asp:HyperLink>
                    </dt>
                    <dd class="answer-profile">
                        <div class="answer">
                            <mn2:FrameworkLiteral ID="literalA" runat="server" ResourceConstant="TXT_A"></mn2:FrameworkLiteral>
                            <asp:Literal ID="literalAnswer" runat="server"></asp:Literal>
                            <mn:Txt ID="mnTxtDirection" runat="server" ResourceConstant="TXT_DIRECTION" />
                        </div>
                        <div class="answer-footer">
                            <span class="timestamp">
                            <uc2:LastTime ID="lastTimeAction" runat="server" />
                            </span><span class="answer-actions">
                            <% if (IsCommentsEnabled && IsLoggedIn && !IsViewingOwnProfile) { %><label 
                                class="link-label" 
                                for="<asp:Literal id='commentLabelId' runat='server'/>"><mn:Txt 
                                ID="mntxt2513" runat="server" expandimagetokens="true" 
                                resourceconstant="TXT_COMMENT_LABEL" />
                            </label>
                            <% } %> <% if (IsMemberLikeEnabled && IsLoggedIn && !IsViewingOwnProfile) { %>
                            <span class="answer-like"> &#9642; 
                            <span onclick="spark.qanda.LikeAnswer(this,<%=MemberId%>,<asp:Literal id='likeAnswerId' runat='server'/>,{QuestionId:<asp:Literal id='questionId' runat='server'/>,Event:'event51,event52',Overwrite:true,Props:[{Num:38,Value:'Another Member Profile'}]})">
                            <asp:Literal ID="likeTxt" runat="server" visible="false" />
                            </span></span><% } %> </span>
                            <ml:MemberLikeAnswerClientModule ID="memberLikeClientModule" runat="server" 
                                Enabled="true" />
                        </div>
                        <%-- end Answer Footer --%>
                        <% if (IsCommentsEnabled && IsLoggedIn && !IsViewingOwnProfile) { %>
                        <div class="member-comment clearfix hide">
                            <span class="close">
                            <mn:Txt ID="mntxt5239" runat="server" expandimagetokens="true" 
                                resourceconstant="TXT_COMMENT_CLOSE_BOX" />
                            </span>
                            <textarea ID="comment-<asp:Literal id='commentAnswerId' runat='server'/>-<asp:Literal id='commentMemberId' runat='server'/>" 
                                class="watermark" cols="70" rows="2"></textarea>
                            <mn2:FrameworkButton ID="buttonAnswerId" runat="server" 
                                CssClass="secondary commentSubmit" ResourceConstant="TXT_COMMENT_LABEL" />
                        </div>
                        <% } %>
                        <asp:PlaceHolder ID="phPendingAnswer" runat="server" Visible="false">
                            <div class="notification">
                                <mn:Image ID="imgAnsweredNotification" runat="server" 
                                    FileName="icon-page-message.gif" />
                                <mn2:FrameworkLiteral ID="literalQuestionAnsweredApproval" runat="server" 
                                    ResourceConstant="TXT_QUESTIONANSWEREDAPPROVAL"></mn2:FrameworkLiteral>
                            </div>
                        </asp:PlaceHolder>
                    </dd>
                </ItemTemplate>
            </asp:Repeater>
        </dl>
        
        <input type="hidden" id="hidAction" name="hidAction" value="" />
        <input type="hidden" id="hidQuestionID" name="hidQuestionID" value="" />
        <input type="hidden" id="hidAnswerID" name="hidAnswerID" value="" />
        
        <div id="qanda-delete-message" style="display:none;">
            <p><mn2:FrameworkLiteral ID="literalDeleteConfirmationMessage" runat="server" ResourceConstant="TXT_DELETE_CONFIRMATION_MESSAGE"></mn2:FrameworkLiteral></p>
            <p>
                <input type="button" class="btn delete-yes secondary" id="btnYes" runat="server" />&nbsp; 
                <input type="button" class="btn delete-no secondary" id="btnNo" runat="server" />
            </p>
        </div>
    </asp:PlaceHolder>
    
    <%--Member Viewing Own Profile with No Answers--%>
    <asp:PlaceHolder ID="phOwnProfileNoAnswers" runat="server" Visible="false">
        <div class="noanswer" id="qanda-view-profile">
            <mn2:FrameworkLiteral ID="literalNoAnswersEdit" runat="server" ResourceConstant="TXT_NOANSWERS_EDIT"></mn2:FrameworkLiteral>
        </div>
    </asp:PlaceHolder>
        
    <%--Member Viewing Other Profile with No Answers--%>
    <asp:PlaceHolder ID="phOtherProfileNoAnswers" runat="server" Visible="false">
        <div class="noanswer" id="qanda-view-profile">
            <mn2:FrameworkLiteral ID="literalNoAnswerView" runat="server" ResourceConstant="TXT_NOANSWERS_VIEW"></mn2:FrameworkLiteral>
        </div>
    </asp:PlaceHolder>
    
    <asp:PlaceHolder ID="phHasAnswersDouble" runat="server">    
        <!-- SPRINT QNA tab html -->
        <script type="text/javascript">
        //<![CDATA[
        (function() {
            __addToNamespace__('spark.qanda', {
                BrandId:<%=g.Brand.BrandID %>,
                __init__: function() {
                    if (!$j.render) {
                        $j("head").append("<script type=\"text/javascript\" src=\"/javascript20/library/jquery.tmpl.js\"><" + "/script>");
                    }
                    if (!spark || !spark.qanda || !spark.qanda.AnswerBlockedQuestion) {
                        $j("head").append("<script type=\"text/javascript\" src=\"/Applications/QuestionAnswer/Controls/qanda.js\"><" + "/script>");
                    }
                }
            });        
        })();
        //]]>            
        </script>         
        <div id="double-q-cont" class="double-q-cont">
            <qa2:BothAnsweredQuestionModule id="BothAnsweredQuestionModule1" runat="server" PageSize="3" />
            
            <qa2:BlockedAnsweredQuestionModule id="BlockedAnsweredQuestionModule1" runat="server" PageSize="3" RunJSOnAnswerPostSuccess="tabGroup.refreshAds();" />
    </div><!-- end double-q-cont -->
    </asp:PlaceHolder>
        
<% if (IsCommentsEnabled && IsLoggedIn){ %>
<script type="text/javascript" src="/Applications/QuestionAnswer/Controls/qanda.js"></script>
<script type="text/javascript">
    //<![CDATA[
    (function() {
        __addToNamespace__('spark.qanda', {
            RedirectUrl:'<%=GetRedirectUrl(888) %>',
            BrandId:<%=g.Brand.BrandID %>,
            txtViewMyProfile: '<%=GetResourceForJS("TXT_VIEW_MY_PROFILE") %>',
            txtCommentCloseBox: '<%=GetResourceForJS("TXT_COMMENT_CLOSE_BOX") %>',
            txtCommentLabel: '<%=GetResourceForJS("TXT_COMMENT_LABEL") %>',
            txtCommentSubmit:'<%=GetResourceForJS("TXT_COMMENT_SEND") %>',
            txtCommentWatermarkCopy:'<%=GetResourceForJS("TXT_WATERMARK_COMMENT") %>',
            txtCommentConfirmationCopy:'<%=GetResourceForJS("TXT_COMMENT_CONFIRM") %>',
            txtEmptyCommentErrorCopy:'<%=GetResourceForJS("TXT_EMPTY_COMMENT_ERROR") %>',
            txtCommentErrorCopy:'<%=GetResourceForJS("TXT_COMMENT_ERROR") %>',
            txtCommentMailSubject:'<%=GetResourceForJS("TXT_COMMENT_SUBJECT") %>',
            txtFreeTextCommentMailSubject:'<%=GetResourceForJS("TXT_FREE_COMMENT_SUBJECT") %>',
            txtPhotoCommentMailSubject:'<%=GetResourceForJS("TXT_PHOTO_COMMENT_SUBJECT") %>',
            __init__: function() {
                if (!$j.render) {
                    $j("head").append("<script type=\"text/javascript\" src=\"/javascript20/library/jquery.tmpl.js\"><"+ "/script>");
                }
            }        
       });
    })();

    $j(document).ready(function(){
        
            if (spark.qanda.RedirectUrl && spark.qanda.RedirectUrl.length > 0) {
                $j("#qanda-view-profile .link-label").live('click',function(event){
                    document.location.href = spark.qanda.RedirectUrl;
                });
                $j("#double-q-cont .link-label").live('click',function(event){
                    document.location.href = spark.qanda.RedirectUrl;
                });

            } else {
                $j("#qanda-view-profile").live('click',function(event){
                    $j(event.target).closest('label').parents('.answer-footer').toggleClass('hide')
                        .parents('.answer-profile').find('.member-comment').toggleClass('hide');
                    $j(event.target).closest('.close').parents('.member-comment').toggleClass('hide')
                        .parents('.answer-profile').find('.answer-footer').toggleClass('hide');
                });                    
                $j("#double-q-cont").live('click',function(event){
                    $j(event.target).closest('label').parents('.answer-info').toggleClass('hide')
                        .parents('.answer-container').find('.member-comment').toggleClass('hide');
                    $j(event.target).closest('.close').parents('.member-comment').toggleClass('hide')
                        .parents('.answer-container').find('.answer-info').toggleClass('hide');
                });                    
            }
                       
        $j("textarea", ".member-comment").watermark('<%=GetResourceForJS("TXT_WATERMARK_COMMENT") %>');

        $j("textarea", ".answer-yours").watermark('<%=GetResourceForJS("TXT_WATERMARK_ANSWER") %>');
            
        $j("input.commentSubmit").live("click",function(evt) {
            var ids=$j(this).attr('id').split("|");            
            var myFunc=function() {
                return spark.qanda.SendMail('comment-'+ids[1]+'-'+ids[2],ids[3],<%=MemberId%>,'<%=MemberUserName%>',{Event:"event51",Overwrite:true,Props:[{Num:38,Value:"Comment-Another Member Profile"}]});
            }
            spark.qanda.ajaxCallWithBlock($j(this).parent(),{ 
                  message: '<%=GetResourceForJS("HTML_BLOCK_ELEMENT_MSG") %>',
                  centerY: false,
                  css: {backgroundColor: 'transparent', border: 'none', top:'10%'},
                  overlayCSS: {backgroundColor: '#fff', opacity: 0.8 }
            },myFunc,'/Applications/API/IMailService.asmx/SendComment');            
            evt.stopPropagation();            
            return false;
        });    
        $j("input.freeTextCommentSubmit").live("click",function(evt) {
            var ids=$j(this).attr('id').split("-");  
            var toMemberId = ids[2];
            var myFunc=function() {
                return spark.qanda.FreeTextSendMail('comment-'+ids[1]+'-'+ids[2]+'-'+ids[3],ids[3],<%=MemberId%>,'<%=MemberUserName%>',{Event:"event51",Overwrite:true,Props:[{Num:38,Value:"Comment-Another Member Profile"},{Num:35,Value:toMemberId}]});
            }
            spark.qanda.ajaxCallWithBlock($j(this).closest('.comment-block'),{ 
                  message: '<%=GetResourceForJS("HTML_BLOCK_ELEMENT_MSG") %>',
                  centerY: false,
                  css: {backgroundColor: 'transparent', border: 'none', top:'10%'},
                  overlayCSS: {backgroundColor: '#fff', opacity: 0.8 }
            },myFunc,'/Applications/API/IMailService.asmx/FreeTextSendComment');            
            evt.stopPropagation();            
            return false;
        });     
        $j("input.photoCommentSubmit").live("click",function(evt) {
            var ids=$j(this).attr('id').split("-");
            var toMemberId = ids[2];
            var myFunc=function() {
                return spark.qanda.PhotoCommentSendMail('comment-'+ids[1]+'-'+ids[2]+'-'+ids[3],ids[3],<%=MemberId%>,'<%=MemberUserName%>',{Event:"event50",Overwrite:true,Props:[{Num:18,Value:"Comment-Profile-Photo"},{Num:35,Value:toMemberId}]});
            }
            spark.qanda.ajaxCallWithBlock($j(this).closest('.comment-block'),{ 
                  message: '<%=GetResourceForJS("HTML_BLOCK_ELEMENT_MSG") %>',
                  centerY: false,
                  css: {backgroundColor: 'transparent', border: 'none', top:'10%'},
                  overlayCSS: {backgroundColor: '#fff', opacity: 0.8 }
            },myFunc,'/Applications/API/IMailService.asmx/PhotoSendComment');            
            evt.stopPropagation();            
            return false;
        }); 
        $j("label.link-label").live('click',function(evt){
            spark.tracking.addEvent("event50", true);
            spark.tracking.addProp(38, "Comment-Another Member Profile", true);
            spark.tracking.track();                            
        });
    });
    //]]>

function send_photo_comment(obj, evt)
{
            var ids=$j(obj).attr('id').split("-");
            var toMemberId = ids[2];
            var myFunc=function() {
                return spark.qanda.PhotoCommentSendMail('comment-'+ids[1]+'-'+ids[2]+'-'+ids[3],ids[3],<%=MemberId%>,'<%=MemberUserName%>',{Event:"event50",Overwrite:true,Props:[{Num:18,Value:"Comment-Profile-Photo"},{Num:35,Value:toMemberId}]});
            }
            spark.qanda.ajaxCallWithBlock($j(obj).closest('.comment-block'),{ 
                  message: '<%=GetResourceForJS("HTML_BLOCK_ELEMENT_MSG") %>',
                  centerY: false,
                  css: {backgroundColor: 'transparent', border: 'none', top:'10%'},
                  overlayCSS: {backgroundColor: '#fff', opacity: 0.8 }
            },myFunc,'/Applications/API/IMailService.asmx/PhotoSendComment');            
            evt.stopPropagation();            
            return false;
}
</script>
<% } %>
<% if (IsMemberLikeEnabled && IsLoggedIn && !IsViewingOwnProfile){ %>
<script type="text/javascript">
    //<![CDATA[
    (function() {
        __addToNamespace__('spark.qanda', {            
            BrandId:<%=g.Brand.BrandID %>,
            MemberId:<%=MemberId %>,
            txtLike: '<%=GetResourceForJS("TXT_ANSWER_LIKE") %>',
            txtUnLike: '<%=GetResourceForJS("TXT_ANSWER_UNLIKE") %>',
            txtLikeNum: '<%=GetResourceForJS("TXT_LIKE_NUM") %>',
            __init__: function() { 
                if (!spark || !spark.qanda || !spark.qanda.LikeAnswer) {
                    $j("head").append("<script type=\"text/javascript\" src=\"/Applications/QuestionAnswer/Controls/qanda.js\"><"+ "/script>");
                }
            }        
       });
    })();
    //]]>
</script>
<% } %>
</asp:panel>
