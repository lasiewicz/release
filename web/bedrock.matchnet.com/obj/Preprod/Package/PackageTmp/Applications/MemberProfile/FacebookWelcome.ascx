﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FacebookWelcome.ascx.cs"
    Inherits="Matchnet.Web.Applications.MemberProfile.FacebookWelcome" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn" TagName="miniProfile" Src="~\Framework\Ui\BasicElements\MiniProfile20.ascx" %>
<%@ Register TagPrefix="uc1" Namespace="Matchnet.Web.Framework.Ui" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<asp:PlaceHolder runat="server" ID="plcFBNewMember" Visible="false">
    <div id="facebook-welcome">
        <div class="facebook-returning-header">
            <asp:HyperLink ID="lnkHomepage" runat="server" NavigateUrl="/" Target="_blank">
                <mn:Image ID="Image8" runat="server" FileName="jdate-general-logo.png" DefaultTitleResourceConstant="ALT_LOGO"
                    TitleResourceConstant="ALT_LOGO" />
            </asp:HyperLink>
        </div>
        <div id="facebook-welcome-container">
            <div id="welcome-message-div">
                <p id="welcome-title-spn">
                    <mn:Txt ID="Txt1" runat="server" ResourceConstant="TXT_WELCOME" />
                </p>
                <h1>
                    <mn:Txt ID="Txt5" runat="server" ResourceConstant="TXT_WELCOME_2" />
                </h1>
                <p id="welcome-message">
                    <mn:Txt ID="Txt2" runat="server" ResourceConstant="TXT_WELCOME_MESSAGE" />
                </p>
            </div>
            <div id="welcomepage-content">
                <div id="first-container">
                    <p id="first-content-title">
                        <mn:Txt ID="Txt3" runat="server" ResourceConstant="TXT_FIRST_CONTENT_TITLE" />
                    </p>
                    <p>
                        <mn:Txt ID="Txt4" runat="server" ResourceConstant="TXT_FIRST_CONTENT_MESSAGE" />
                    </p>
                    <mn:Txt ID="Txt6" runat="server" ResourceConstant="TXT_FIRST_CONTENT_BENEFITS" />
                    <p>
                        <mn:Txt ID="Txt7" runat="server" ResourceConstant="TXT_FIRST_CONTENT_MORE" />
                    </p>
                    <p class="block-text">
                        <mn:Txt ID="Txt8" runat="server" ResourceConstant="TXT_FIRST_CONTENT_END" />
                    </p>
                    <div class="subscribe-btn-div">
                        <asp:HyperLink runat="server" ID="lnkBuySub" NavigateUrl="/Applications/Subscription/Subscribe.aspx?prtid=41"
                            Target="_blank">
                            <mn:Image ID="Image1" runat="server" FileName="welcome-buy-sub.jpg" /></div>
                    </asp:HyperLink>
                </div>
                <div id="second-container">
                    <h2>
                        <mn:Txt ID="Txt9" runat="server" ResourceConstant="TXT_SECOND_CONTENT_TITLE" />
                    </h2>
                    <p class="block-text">
                        <mn:Txt ID="Txt10" runat="server" ResourceConstant="TXT_SECOND_CONTENT_MESSAGE" />
                    </p>
                    <div class="subscribe-btn-div">
                        <asp:HyperLink runat="server" ID="lnkSearchMatches" CssClass="link-secondary" NavigateUrl="/Applications/Search/SearchResults.aspx?NavPoint=sub"
                            Target="_blank">
                            <mn:Image ID="Image3" runat="server" FileName="search-your-matches.jpg" />
                        </asp:HyperLink>
                    </div>
                </div>
                <div id="third-container">
                    <h2>
                        <mn:Txt ID="Txt17" runat="server" ResourceConstant="TXT_THIRD_CONTENT_TITLE" />
                    </h2>
                    <p class="block-text">
                        <mn:Txt ID="Txt18" runat="server" ResourceConstant="TXT_THIRD_CONTENT_MESSAGE" />
                    </p>
                    <div class="subscribe-btn-div">
                        <asp:HyperLink runat="server" ID="lnkFinishProfile" CssClass="link-secondary" NavigateUrl="/Applications/MemberProfile/ViewProfile.aspx?Mode=Edit"
                            Target="_blank">
                            <mn:Image ID="Image10" runat="server" FileName="complete-your-profile.jpg" />
                        </asp:HyperLink>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:PlaceHolder>
<asp:PlaceHolder runat="server" ID="plcFBExistingMember" Visible="false">
    <div id="facebook-returning-container">
        <div class="facebook-returning-header">
            <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="/" Target="_blank">
                <mn:Image ID="Image7" runat="server" FileName="jdate-general-logo.png" DefaultTitleResourceConstant="ALT_LOGO"
                    TitleResourceConstant="ALT_LOGO" />
            </asp:HyperLink>
        </div>
        <div id="facebook-returning">
            <div id="return-content-3">
                <h2>
                    <mn:Txt runat="server" ID="Txt15" ResourceConstant="TXT_PHOTO" Href="/Applications/PhotoGallery/PhotoGallery.aspx"
                        Target="_blank" />
                </h2>
                <div class="quick-link-info">
                    <mn:Image CssClass="marketing" ID="Image6" runat="server" FileName="mkt-nomatch-img-02.jpg" />
                    <p>
                        <mn:Txt runat="server" ID="Txt16" ResourceConstant="TXT_PHOTO_MESSAGE" />
                    </p>
                </div>
            </div>
            <div id="return-content-2">
                <h2>
                    <mn:Txt runat="server" ID="Txt13" ResourceConstant="TXT_MOL" Href="/Applications/MembersOnline/MembersOnline.aspx"
                        Target="_blank" />
                </h2>
                <div class="quick-link-info">
                    <mn:Image CssClass="marketing" ID="Image5" runat="server" FileName="mkt-nomatch-img-03.jpg" />
                    <p>
                        <mn:Txt runat="server" ID="Txt14" ResourceConstant="TXT_MOL_MESSAGE" />
                    </p>
                </div>
            </div>
            <div id="return-content-1">
                <h2>
                    <mn:Txt runat="server" ID="Txt11" ResourceConstant="TXT_QUICK_SEARCH" Href="/Applications/QuickSearch/QuickSearch.aspx"
                        Target="_blank" />
                </h2>
                <div class="quick-link-info">
                    <mn:Image CssClass="marketing" ID="Image4" runat="server" FileName="mkt-nomatch-img-01.jpg" />
                    <p>
                        <mn:Txt runat="server" ID="Txt12" ResourceConstant="TXT_QUICK_SEARCH_MESSAGE" />
                    </p>
                </div>
            </div>
        </div>
    </div>
</asp:PlaceHolder>
