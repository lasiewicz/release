﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects;
using Matchnet.Web.Framework;
using Matchnet.Configuration.ServiceAdapters;

namespace Matchnet.Web.Applications.MemberProfile
{
    /// <summary>
    /// Page to update member's email and password
    /// </summary>
    public partial class ChangeEmail20 : FrameworkControl
    {
        #region Event handler
        protected override void OnInit(EventArgs e)
        {
            this.btnSaveEmail.Click += new System.EventHandler(this.btnSaveEmail_Click);
            this.btnSavePassword.Click += new System.EventHandler(this.btnSavePassword_Click);
            this.csvPassword.ServerValidate += new ServerValidateEventHandler(csvPassword_ServerValidate);

            base.OnInit(e);

        }

        private void Page_Load(object sender, System.EventArgs e)
        {
            try
            {
                // stealth logon admins shouldn't be here
                if (g.StealthLogonAdmin != null)
                    g.Transfer("/Applications/Home/Default.aspx");

                Matchnet.Member.ServiceAdapters.Member aMember = null;
                aMember = MemberSA.Instance.GetMember(g.Session.GetInt(WebConstants.SESSION_PROPERTY_NAME_MEMBERID), MemberLoadFlags.None);

                setupBreadCrumbs();

                lbEmailAddress.Text = aMember.EmailAddress;

                //	Set up ENTER key handling				 
                tbNewEmailAddressConfirm.Attributes.Add("onkeydown", "setAutoSubmitButton(" + btnSaveEmail.ClientID + ",event)");
                tbNewPasswordConfirm.Attributes.Add("onkeydown", "setAutoSubmitButton(" + btnSavePassword.ClientID + ",event)");
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        private void setupBreadCrumbs()
        {
            g.BreadCrumbTrailHeader.SetViewProfileCrumbs(g.GetResource("NAV_EDIT_YOUR_PROFILE", this)
                , g.AppPage.App.DefaultPagePath + "?EntryPoint=99999"
                , g.GetTargetResource("NAV_PERSONAL_INFO", this)
                , "/Applications/MemberProfile/ViewProfile.aspx?Mode=Edit"
                , g.GetTargetResource("TTL_CHANGE_EMAIL_PASSWORD", this)
                , string.Empty
                , string.Empty
                , string.Empty
                , string.Empty);

            g.BreadCrumbTrailFooter.SetViewProfileCrumbs(g.GetResource("NAV_EDIT_YOUR_PROFILE", this)
                , g.AppPage.App.DefaultPagePath + "?EntryPoint=99999"
                , g.GetTargetResource("NAV_PERSONAL_INFO", this)
                , "/Applications/MemberProfile/ViewProfile.aspx?Mode=Edit"
                , g.GetTargetResource("TTL_CHANGE_EMAIL_PASSWORD", this)
                , string.Empty
                , string.Empty
                , string.Empty
                , string.Empty);
        }

        private void btnSaveEmail_Click(Object sender, System.EventArgs e)
        {
            MemberSaveResult result;

            cvPassword.Enabled = false;
            rfvPassword.Enabled = false;
            rfvNewPassword.Enabled = false;

            Page.Validate();
            if (!Page.IsValid)
                return;

            cvPassword.Visible = false;
            rfvPassword.Visible = false;

            if (tbNewEmailAddress.Text != null && tbNewEmailAddress.Text.Trim().Length > 0 &&
                tbNewEmailAddressConfirm.Text != null && tbNewEmailAddressConfirm.Text.Trim().Length > 0 &&
                revNewEmailAddress.IsValid && cvEmailAddress.IsValid)
            {
                string currentEmail = g.Member.EmailAddress;

                g.Member.EmailAddress = tbNewEmailAddress.Text;
                if (g.EmailVerify.EnableEmailVerification)
                {
                    if (g.EmailVerify.IfMustBlock(WebConstants.EmailVerificationSteps.AfterEmailChange) || g.EmailVerify.IfMustHide(WebConstants.EmailVerificationSteps.AfterEmailChange))
                    {
                        g.EmailVerify.SetNotVerifiedMemberAttributes(g.Member, WebConstants.EmailVerificationSteps.AfterEmailChange);

                    }
                }
                else
                {
                    GlobalStatusMask globalStatusMask = (GlobalStatusMask)g.Member.GetAttributeInt(g.TargetBrand, WebConstants.ATTRIBUTE_NAME_GLOBALSTATUSMASK);
                    globalStatusMask = globalStatusMask & (~GlobalStatusMask.VerifiedEmail);
                    g.Member.SetAttributeInt(g.Brand, WebConstants.ATTRIBUTE_NAME_GLOBALSTATUSMASK, (int)globalStatusMask);
                }
                result = MemberSA.Instance.SaveMember(g.Member, g.Brand.Site.Community.CommunityID);

                if (result.SaveStatus == MemberSaveStatusType.Success)
                {
                    g.Notification.AddMessage("SUCCESSFULLY_SAVED_SETTINGS");

                    //profile changed email notification (send to previous email address)
                    List<string> changedFields = new List<string>();
                    changedFields.Add(g.GetResource("TXT_EMAIL", this));
                    ExternalMail.ServiceAdapters.ExternalMailSA.Instance.SendProfileChangedConfirmationEmail(g.Member.MemberID, g.Brand.BrandID, currentEmail, g.Member.GetAttributeText(g.Brand, "sitefirstname"), g.Member.GetUserName(g.Brand), changedFields);

                    //email verification
                    Matchnet.ExternalMail.ServiceAdapters.ExternalMailSA.Instance.SendEmailVerification(g.Member.MemberID, g.Brand.BrandID);

                    if (g.EmailVerify.EnableEmailVerification)
                        g.Transfer("/Applications/MemberServices/VerifyEmail.aspx");
                }
                else
                {
                    g.Notification.AddError(
                        Matchnet.Web.Framework.Globalization.RegisterStatusTypeResources.GetResourceConstant(RegisterStatusType.AlreadyRegistered));
                }
            }
            else
            {
                // only display an error notification if there are no validation errors
                if (revNewEmailAddress.IsValid && cvEmailAddress.IsValid)
                {
                    g.Notification.AddError("ERR_NEW_EMAIL_ADDRESS_IS_REQUIRED");
                }
            }
        }

        private void btnSavePassword_Click(Object sender, System.EventArgs e)
        {
            MemberSaveResult result;

            cvEmailAddress.Enabled = false;
            revNewEmailAddress.Enabled = false;
            rfvNewEmailAddress.Enabled = false;

            Page.Validate();
            if (!Page.IsValid)
                return;

            g.Member.Password = tbNewPassword.Text;
            result = MemberSA.Instance.SaveMember(g.Member, g.Brand.Site.Community.CommunityID);
            if (result.SaveStatus == MemberSaveStatusType.Success)
            {
                g.Notification.AddMessage("SUCCESSFULLY_SAVED_SETTINGS");

                //profile changed email notification
                List<string> changedFields = new List<string>();
                changedFields.Add(g.GetResource("TXT_PASSWORD", this));
                ExternalMail.ServiceAdapters.ExternalMailSA.Instance.SendProfileChangedConfirmationEmail(g.Member.MemberID, g.Brand.BrandID, g.Member.EmailAddress, g.Member.GetAttributeText(g.Brand, "sitefirstname"), g.Member.GetUserName(g.Brand), changedFields);

                // First check to see if we are Messmo enabled for the site
                if (Conversion.CBool(RuntimeSettings.GetSetting("MESSMO_ENABLED", g.Brand.Site.Community.CommunityID)))
                {
                    // If this member has Messmo enabled, we have to let Messmo of the new password
                    int messmoCapable = g.Member.GetAttributeInt(g.Brand, "MessmoCapable", 0);
                    if (messmoCapable == (int)Matchnet.HTTPMessaging.Constants.MessmoCapableStatus.Pending ||
                        messmoCapable == (int)Matchnet.HTTPMessaging.Constants.MessmoCapableStatus.Capable)
                    {
                        string mobileNumber = g.Member.GetAttributeText(g.Brand, "MessmoMobileNumber", string.Empty);
                        string password = Member.ServiceAdapters.MemberSA.Instance.GetPassword(g.Member.MemberID);
                        string sha1Password = Matchnet.Security.Crypto.HashForMessmo(password);

                        Matchnet.ExternalMail.ServiceAdapters.ExternalMailSA.Instance.SendMessmoSubscriptionRequest(g.Brand.BrandID,
                            g.Member.MemberID, DateTime.Now, mobileNumber, sha1Password);
                    }
                }
            }
            else
            {
                g.Notification.AddError("ERR_ERROR_SAVING_REGISTRATION");
            }
        }

        private void csvPassword_ServerValidate(object source, ServerValidateEventArgs args)
        {
            bool isValid = MemberSA.Instance.VerifyPassword(g.Member.EmailAddress, g.Brand.Site.Community.CommunityID, args.Value);
            args.IsValid = isValid;
        }



        #endregion


    }
}
