using Matchnet.Member.ServiceAdapters;
using Matchnet.Web.Framework;
using System;
using System.Web.UI.WebControls;
using Matchnet.Web.Framework.Search;
using Matchnet.Web.Framework.Ui;
using Matchnet.Web.Framework.Ui.PageElements;

namespace Matchnet.Web.Applications.MemberProfile
{

	/// <summary>
	///		Summary description for ProfileUnavailable.
	/// </summary>
	public class ProfileUnavailable : FrameworkControl
	{
		protected Txt txtYourMessageHasBeenSent;
		protected Txt txtMemberMessage;
		protected NoPhoto noPhoto1;
		protected NoPhoto noPhoto20;
		protected Framework.Image ImageThumb;
		protected Txt txtProfileUnavailableWho;
		protected Txt txtProfileUnavailableTipsHeader;
		protected Txt txtProfileUnavailableTipsOneBold;
		protected Txt txtProfileUnavailableTipsOneRegular;
		protected Txt txtProfile_UnavailableTipsOneLink;
		protected Txt txtProfileUnavailableTipsTwoBold;
		protected Txt txtProfileUnavailableTipsTwoRegular;
		protected Txt txtProfileUnavailableTipsThreeBold;
		protected Txt txtProfileUnavailableTipsThreeRegular;
		protected Txt txtProfileUnavailableTipsFourBold;
		protected Txt txtProfileUnavailableTipsFourRegular;
		protected Txt txtProfileUnavailableEnd;
		protected Literal ltlMemberName;
		protected HyperLink lnkNext;
		protected HyperLink lnkNext2;
		protected HyperLink lnkBacktoResults;
		protected HyperLink lnkPrev;
		protected PlaceHolder phNextProfile;
		protected PlaceHolder phSite20Nav;

		private void Page_Load(object sender, EventArgs e)
		{
			try
			{
				ltlMemberName.Text = g.GetResource("MEMBER", this);
                if (ImageThumb != null)
				    ImageThumb.Visible = false;

				CreateMemberContent();
			}
			catch (Exception ex)
			{
				g.ProcessException(ex);
			}
		}

		private void CreateMemberContent()
		{
			if (Request.Params.Get("MemberID") == null) return;

			int memberID = Conversion.CInt(Request.Params.Get("MemberId"));

			if (memberID < 1) return;

			var aMember = MemberSA.Instance.GetMember(memberID, MemberLoadFlags.None);

			if (aMember != null && (aMember.GetUserName(_g.Brand) != null && aMember.GetUserName(_g.Brand).Trim() != String.Empty))
			{
				ltlMemberName.Text = aMember.GetUserName(_g.Brand);
			}

            if (g.IsSite20Enabled)
            {
                if (noPhoto20 != null)
                {
                    noPhoto20.Visible = true;
                    noPhoto20.MemberID = memberID;
                    noPhoto20.Mode = NoPhoto.PhotoMode.NoPhoto;
                }
            }
            else
            {
                if (noPhoto1 != null)
                {
                    noPhoto1.Visible = true;
                    noPhoto1.MemberID = memberID;
                    noPhoto1.Mode = NoPhoto.PhotoMode.NoPhoto;
                }
            }

			if (g.IsSite20Enabled)
			{
				CreateSite20Navigation(memberID);
			}
			else
			{
				CreateOldNavication(memberID);
			}
		}

		private void CreateOldNavication(int memberID)
		{
			phSite20Nav.Visible = false;
			//next profile link
			string nextProfileLink = BreadCrumbHelper.StaticGetNextProfileLinkWithActiveTab(memberID, Request, g);
			if (!FrameworkGlobals.StringIsEmpty(nextProfileLink))
			{
				phNextProfile.Visible = true;
				lnkNext.NavigateUrl = nextProfileLink;
			}
		}

		private void CreateSite20Navigation(int memberID)
		{
			phSite20Nav.Visible = true;
			phNextProfile.Visible = false;

            Int32 entryPoint=0;

		    Int32.TryParse(Request.Params["EntryPoint"], out entryPoint);

            if(entryPoint != 0 &&entryPoint == (Int32)BreadCrumbHelper.EntryPoint.ReverseSearchResults && g.Member == null)
            {
                //if this is from reverse search and there's no logged in member, no sense in trying to make the next/previous links because they won't work. 
                return;
            }


			CreateNextProfileLink(memberID);

			CreatePreviousProfileLink(memberID);

			CreateBackToResultsControl();

			if (g.IsSite20Enabled) g.RightNavControl.MiniSearchControl.LoadMiniSearch();
		}

		private void CreateBackToResultsControl()
		{
			string urlVal = FrameworkGlobals.GetCookie(WebConstants.SESSION_PROPERTY_NAME_BACKTORESULT_URL, null, true);

			Int32 entryPoint;

			if (Int32.TryParse(Request.Params["EntryPoint"], out entryPoint) && 
			    entryPoint == (Int32)BreadCrumbHelper.EntryPoint.KeywordSearchResults )
			{
				lnkBacktoResults.NavigateUrl = SearchPageHelper.GetKeywordSearchResultsUrl(Request);
			}
			else if( entryPoint == (Int32)BreadCrumbHelper.EntryPoint.ReverseSearchResults)
			{
				lnkBacktoResults.NavigateUrl = GetReverseSearchUrl();
			}
			else
			{
				lnkBacktoResults.NavigateUrl = !string.IsNullOrEmpty(urlVal) ? urlVal : "~/Applications/Search/SearchResults.aspx";
			}
		}

		private String GetReverseSearchUrl()
		{
			var request = Request;

			var viewProfileUrl = "/Applications/Search/ReverseSearch.aspx?";

			Int32 page;
			if ((Int32.TryParse(request["rp"], out page) == false &&
				Int32.TryParse(request["pg"], out page) == false) ||
				page < 1) page = 1;

			viewProfileUrl += "pg=" + page;

			if (String.IsNullOrEmpty(request["sl"]) == false)
			{
				viewProfileUrl += "&sl=" + request["sl"];
			}

			if (String.IsNullOrEmpty(request["op"]) == false)
			{
				viewProfileUrl += "&op=" + request["op"];
			}

			return viewProfileUrl;
		}

		private void CreatePreviousProfileLink(int memberID)
		{
			string prevProfileLink = BreadCrumbHelper.StaticGetPreviousProfileLinkWithActiveTab(memberID, Request, g);
			if (!FrameworkGlobals.StringIsEmpty(prevProfileLink))
			{
				lnkPrev.Visible = true;
				lnkPrev.NavigateUrl = prevProfileLink;
			}
		}

		private void CreateNextProfileLink(int memberID)
		{
			string nextProfileLink = BreadCrumbHelper.StaticGetNextProfileLinkWithActiveTab(memberID, Request, g);

			if (FrameworkGlobals.StringIsEmpty(nextProfileLink)) return;

			Int32 entryPoint;

			if (Int32.TryParse(Request.Params["EntryPoint"], out entryPoint) && 
				entryPoint == (Int32)BreadCrumbHelper.EntryPoint.KeywordSearchResults)
			{
				var results = KeywordSearcher.GetKeywordSearchResults(
                    g.Member == null ? Constants.NULL_INT : g.Member.MemberID,
					SearchPageHelper.GetKeywordSearchPreferences(Request));

				Int32 ordinal;

				if (Int32.TryParse(Request.Params["ordinal"], out ordinal) && ordinal >= results.total) return;
			}
			
			lnkNext2.Visible = true;
			lnkNext2.NavigateUrl = nextProfileLink;
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}

		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
	}
}
