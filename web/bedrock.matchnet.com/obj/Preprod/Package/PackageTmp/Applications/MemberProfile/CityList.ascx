<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Control Language="c#" AutoEventWireup="false" Codebehind="CityList.ascx.cs" Inherits="Matchnet.Web.Applications.MemberProfile.CityList" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>

<style type="text/css">
html {overflow-y: scroll;}
body {background-image:none;background-color:white;}
#city-list .city-link{
    display:block;
    padding:8px 4px;
    width:110px;
    border-radius:3px;
    -moz-border-radius:3px;
    -webkit-border-radius:3px;
}
#city-list .city-link:hover{
    background-color:#eee;
}
#city-list a{
    font-weight:normal;
}
#city-list table{
    font-size:12px;
}
#city-list h1{
    font-size:22px;
    margin:0;
    padding:4px;
}
#city-list-choose{
    padding:4px;
}
#city-list-letters{
    padding:4px;
    background-color:#eee;
}
#city-list-letters a.default{
    display:inline-block;
    padding:4px 0;
    width:18px;
    text-align:center;
    border-radius:3px;
    -moz-border-radius:3px;
    -webkit-border-radius:3px;
}
#city-list-letters a.default:hover{
    background-color:#fff;
}
#city-list-cities{
    padding:4px;
}
</style>

<div id="city-list">
<table dir="<%= TextDirection%>" cellspacing="0" cellpadding="2" width="100%" align="left">
	<tr>
		<td>
			<mn:Title runat="server" id="ttlSelect" ResourceConstant="TXT_SELECT_YOUR_CITY" ImageName="hdr_city_list.gif" CommunitiesForImage="CI;" />
		</td>
	</tr>
	<tr>
		<td id="city-list-choose"><mn:txt id="Txt2" runat="server" name="ChooseCity" ResourceConstant="CHOOSE_A_CITY_FROM_THE_LIST_BELOW"></mn:txt></td>
	</tr>
	<tr>
		<td id="city-list-letters"><asp:literal id="litLetterList" runat="server"></asp:literal></td>
	</tr>
	<tr>
		<td id="city-list-cities"><asp:datalist id="resultList" runat="server" CellPadding="8" Width="450" ItemStyle-VerticalAlign="Top">
				<ItemTemplate>
					<asp:HyperLink CssClass="city-link" Runat="server" ID="cityLink">
						<%# DataBinder.Eval(Container.DataItem, "Description")%>
					</asp:HyperLink>
				</ItemTemplate>
				<HeaderStyle CssClass="stdBold"></HeaderStyle>
			</asp:datalist></td>
	</tr>
</table>
</div>