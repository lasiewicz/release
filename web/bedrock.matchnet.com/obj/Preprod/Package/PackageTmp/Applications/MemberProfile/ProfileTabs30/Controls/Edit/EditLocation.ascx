﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EditLocation.ascx.cs" Inherits="Matchnet.Web.Applications.MemberProfile.ProfileTabs30.Controls.Edit.EditLocation" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>

<%--Custom: location--%>
<div class="form-item form-edit-location">
    <div class="form-label">
        <asp:Label ID="lblEdit" runat="server" AssociatedControlID="ddlEditCountry" CssClass="lbl-edit-myregion">
            <asp:Literal ID="litEdit" runat="server"></asp:Literal>
        </asp:Label>
    </div>    
    <div class="form-input">
        <asp:Label ID="lblEditCountry" runat="server" AssociatedControlID="ddlEditCountry" CssClass="lbl-edit-country" />
        <asp:DropDownList ID="ddlEditCountry" runat="server" CssClass="txt-edit-country"></asp:DropDownList>
        <asp:Literal ID="litRequired" runat="server"></asp:Literal>
    
        <div id="pnlEditByZipCode" runat="server">
            <%--ZipCode: USA or Canada --%>
            <asp:Label ID="lblEditZipCode" runat="server" AssociatedControlID="txtEditZipCode" CssClass="lbl-edit-zipcode" />
            <asp:TextBox ID="txtEditZipCode" runat="server" CssClass="short txt-edit-zipcode"></asp:TextBox>
        </div>

        <div id="pnlEditByCityForZip" runat="server">
            <asp:Label ID="lblEditCityForZip" runat="server" AssociatedControlID="ddlEditCityForZip" />
            <asp:DropDownList ID="ddlEditCityForZip" runat="server" CssClass="ddl-city-zip"></asp:DropDownList>
        </div>
           
        <div id="pnlEditByStateCity" runat="server">
            <%--City: Everybody else--%>
            <asp:Label ID="lblEditState" runat="server" AssociatedControlID="ddlEditState" CssClass="hide lbl-edit-state" />
            <div id="pnlEditState" runat="server">
                <asp:DropDownList ID="ddlEditState" runat="server" CssClass="ddl-edit-state"></asp:DropDownList>
            </div>
            <asp:Label ID="lblEditCity" runat="server" AssociatedControlID="txtEditCity" CssClass="lbl-edit-city" />
            <asp:TextBox ID="txtEditCity" runat="server" CssClass="txt-edit-city"></asp:TextBox>
            <asp:HyperLink ID="lnkCityLookup" runat="server" style="display:none;" NavigateUrl="#" CssClass="city-lookup">
                <span id="lookuplink" name="lookuplink"><asp:Literal ID="litLookUpCity" runat="server"></asp:Literal></span>
            </asp:HyperLink>
        </div>
    
        <asp:HiddenField ID="hidRegionID" runat="server" />
        <div id="itemErrorContainer" runat="server" style="display:none;" class="form-error-inline"></div>
    </div>

<asp:placeholder runat="server" ID="plcEditLocationObj">
    <script type="text/javascript">
        var EditLocationObj =
        {
            IsVerbose: false,
            BrandID: "<%=_g.Brand.BrandID.ToString()%>",
            AccountRegionID: "<%=_AccountRegionID %>",
            AccountCountryRegionID: "<%=_AccountCountryRegionID %>",
            AccountStateRegionID: "<%=_AccountStateRegionID %>",
            AccountCityName: "<%=_AccountCityName %>",
            InitialCityParentRegionID: "<%=_InitialCityParentRegionID %>",
            PanelEditByZipCode: "<%=pnlEditByZipCode.ClientID %>",
            PanelEditByStateCity: "<%=pnlEditByStateCity.ClientID %>",
            PanelEditState: "<%=pnlEditState.ClientID %>",
            pnlEditByCityForZip: "<%=pnlEditByCityForZip.ClientID %>",
            HidRegionID: "<%=hidRegionID.ClientID %>",
            ddlEditCountry: "<%=ddlEditCountry.ClientID %>",
            txtEditZipCode: "<%=txtEditZipCode.ClientID %>",
            ddlEditState: "<%=ddlEditState.ClientID %>",
            ddlEditCityForZip: "<%=ddlEditCityForZip.ClientID %>",
            txtEditCity: "<%=txtEditCity.ClientID %>",
            lnkCityLookup: "<%=lnkCityLookup.ClientID %>"
        }
    
    </script>
</asp:placeholder>

</div>