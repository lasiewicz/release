﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Member.ValueObjects.Interfaces;
using Matchnet.Web.Framework.Ui;
using Matchnet.Web.Framework.Managers;
using Spark.FacebookLike.ValueObjects;

namespace Matchnet.Web.Applications.MemberProfile.ProfileTabs30.Controls.ProfileTab
{
    /// <summary>
    /// This tab will load essay content
    /// </summary>
    public partial class EssayTab : BaseTab
    {
        public EssayTab()
        {
            this.ProfileTabType = ProfileTabEnum.Essays;
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected override void OnPreRender(EventArgs e)
        {
            //Loading badge in pre-render to give facebook likes process to resync connect status attribute if needed
            if (FacebookManager.Instance.IsFacebookLikesInterestsDataEnabled(g.Brand))
            {
                FacebookManager.FacebookConnectStatus fbConnectStatus = FacebookManager.Instance.GetFacebookConnectStatus(this._MemberProfile, g.Brand);
                if ((fbConnectStatus == FacebookManager.FacebookConnectStatus.NonConnected
                    || fbConnectStatus == FacebookManager.FacebookConnectStatus.ConnectedNoFBData
                    || fbConnectStatus == FacebookManager.FacebookConnectStatus.ConnectedNoSavedData)
                    && this.WhoseProfile == WhoseProfileEnum.Self)
                {
                    BadgeNoData1.Visible = true;
                }
                else
                {
                    if (this.WhoseProfile == WhoseProfileEnum.Self || fbConnectStatus == FacebookManager.FacebookConnectStatus.ConnectedShowData)
                    {
                        int randomFacebookLikeCount = FacebookManager.Instance.GetRandomFacebookLikeCount(g.Brand);
                        List<FacebookLike> randomFacebookSavedLikes =
                            FacebookManager.Instance.GetRandomFacebookSavedLikes(this._MemberProfile, g.Brand, randomFacebookLikeCount, this.WhoseProfile != WhoseProfileEnum.Self);
                        if (null != randomFacebookSavedLikes && randomFacebookSavedLikes.Count > 0)
                        {
                            BadgeSavedData1.LoadBadge(randomFacebookSavedLikes);
                            BadgeSavedData1.Visible = true;
                        }
                        else if (this.WhoseProfile == WhoseProfileEnum.Self)
                        {
                            BadgeNoData1.Visible = true;
                        }
                    }
                }
            }

            base.OnPreRender(e);
        }

        public override void LoadTab(IMemberDTO member, bool isActive)
        {
            this._MemberProfile = member;
            this.IsActive = isActive;
            bool isViewingOwnProfile = (g.Member != null && member.MemberID == g.Member.MemberID);
            if(isViewingOwnProfile)
            {
                this.WhoseProfile = WhoseProfileEnum.Self;
            }

            phPopupAd.Visible = !BreadCrumbHelper.IsProfilePopupDisabled(member.MemberID);
            if (g.LayoutTemplate == Content.ValueObjects.PageConfig.LayoutTemplate.WidePopup)
                phPopupAd.Visible = true;

            essaysDataGroup.LoadDataGroup(member, ProfileUtility.GetProfileDataGroup(ProfileDataGroupEnum.Essays, g.Brand.Site.SiteID, g));

            
        }
    }
}