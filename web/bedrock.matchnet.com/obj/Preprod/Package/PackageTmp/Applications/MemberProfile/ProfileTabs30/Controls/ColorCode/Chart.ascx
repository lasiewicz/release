﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Chart.ascx.cs" Inherits="Matchnet.Web.Applications.MemberProfile.ProfileTabs30.Controls.ColorCode.Chart" %>

<div class="cc-chart-container">
    <asp:PlaceHolder ID="phViewerHeading" runat="server" Visible="false">
        <%--viewer heading--%>
        <h3 class="cc-color-heading cc-<%=_ColorText.ToLower() %>-bg">You're a <%=_ColorText.ToUpper() %>. <a href="/Applications/ColorCode/Landing.aspx">Read&nbsp;more&nbsp;&raquo;</a></h3>
    </asp:PlaceHolder>
    
    <asp:PlaceHolder ID="phMemberHeading" runat="server" Visible="false">
        <%--member heading--%>
        <h3 class="cc-color-heading cc-<%=_ColorText.ToLower() %>-bg"><strong><%=_UserName %></strong> is a <%=_ColorText.ToUpper() %>.</h3>
    </asp:PlaceHolder>
    
    <%--chart --%>
    <div class="cc-chart-horizontal-list">
    <ul>
    <asp:Literal ID="literalChartLi" runat="server"></asp:Literal>
    </ul>
    </div>
    
</div>