﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Member.ValueObjects.Interfaces;
using Matchnet.Web.Applications.ColorCode;
using Matchnet.Web.Framework;

namespace Matchnet.Web.Applications.MemberProfile.ProfileTabs30.Controls.ColorCode
{
    public partial class Chart : FrameworkControl
    {
        protected Color _Color = Color.none;
        protected string _ColorText = "";
        protected WhoseProfileEnum _WhoseProfile = WhoseProfileEnum.None;
        protected string _UserName = "";

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public void LoadChart(IMemberDTO member, MemberQuiz memberQuiz, WhoseProfileEnum whoseProfile)
        {
            _Color = memberQuiz.PrimaryColor;
            _ColorText = ColorCodeHelper.GetFormattedColorText(_Color);
            _WhoseProfile = whoseProfile;
            _UserName = String.IsNullOrEmpty(member.GetUserName(g.Brand)) ? member.MemberID.ToString() : member.GetUserName(g.Brand);

            switch (whoseProfile)
            {
                case WhoseProfileEnum.Other:
                    phMemberHeading.Visible = true;
                    break;
                case WhoseProfileEnum.Self:
                    phViewerHeading.Visible = true;
                    break;
            }

            //load chart table rows in descending order
            decimal[] arrScores = memberQuiz.Scores.Values.ToArray();
            string[] pieTable = new string[4];
            Array.Sort(arrScores); //sorts by ascending order for primitive types
            int zeroScoreCount = 0;
            foreach (Color c in memberQuiz.Scores.Keys)
            {
                string width = (memberQuiz.Scores[c] < 1 && memberQuiz.Scores[c] > 0) ? "1" : memberQuiz.Scores[c].ToString("F0");

                if (memberQuiz.Scores[c] == arrScores[3] && String.IsNullOrEmpty(pieTable[0])) //fourth (last) value in array should be biggest
                {
                    pieTable[0] = ("<li><span class=\"cc-bar cc-" + c.ToString().ToLower() + "-bg\" style=\"width:" + width + "px;\"></span><span class=\"cc-bar-key\">" + ColorCodeHelper.GetFormattedColorText(c).ToUpper() + "&nbsp;<b>" + memberQuiz.Scores[c].ToString("0.##") + "%</b></span></li>");
                }
                else if (memberQuiz.Scores[c] == arrScores[2] && String.IsNullOrEmpty(pieTable[1])) //third value in array should be second biggest
                {
                    pieTable[1] = ("<li><span class=\"cc-bar cc-" + c.ToString().ToLower() + "-bg\" style=\"width:" + width + "px;\"></span><span class=\"cc-bar-key\">" + ColorCodeHelper.GetFormattedColorText(c).ToUpper() + "&nbsp;<b>" + memberQuiz.Scores[c].ToString("0.##") + "%</b></span></li>");
                }
                else if (memberQuiz.Scores[c] == arrScores[1] && String.IsNullOrEmpty(pieTable[2]))
                {
                    pieTable[2] = ("<li><span class=\"cc-bar cc-" + c.ToString().ToLower() + "-bg\" style=\"width:" + width + "px;\"></span><span class=\"cc-bar-key\">" + ColorCodeHelper.GetFormattedColorText(c).ToUpper() + "&nbsp;<b>" + memberQuiz.Scores[c].ToString("0.##") + "%</b></span></li>");
                }
                else if (memberQuiz.Scores[c] == arrScores[0] && String.IsNullOrEmpty(pieTable[3]))
                {
                    pieTable[3] = ("<li><span class=\"cc-bar cc-" + c.ToString().ToLower() + "-bg\" style=\"width:" + width + "px;\"></span><span class=\"cc-bar-key\">" + ColorCodeHelper.GetFormattedColorText(c).ToUpper() + "&nbsp;<b>" + memberQuiz.Scores[c].ToString("0.##") + "%</b></span></li>");
                }

                if (memberQuiz.Scores[c] < 1)
                {
                    zeroScoreCount++;
                }

            }

            System.Text.StringBuilder sbPieTable = new System.Text.StringBuilder();
            for (int i = 0; i < pieTable.Length; i++)
            {
                sbPieTable.Append(pieTable[i]);
            }

            literalChartLi.Text = sbPieTable.ToString();


        }
    }
}