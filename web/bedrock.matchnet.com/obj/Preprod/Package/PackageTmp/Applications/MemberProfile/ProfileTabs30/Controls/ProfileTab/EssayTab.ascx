﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EssayTab.ascx.cs" Inherits="Matchnet.Web.Applications.MemberProfile.ProfileTabs30.Controls.ProfileTab.EssayTab" %>
<%@ Register TagPrefix="uc1" TagName="EssayDataGroup" Src="/Applications/MemberProfile/ProfileTabs30/Controls/ProfileDataGroup/EssayDataGroup.ascx" %>
<%@ Register TagPrefix="uc1" TagName="BadgeNoData" Src="/Applications/MemberProfile/ProfileTabs30/Controls/Facebook/BadgeNoData.ascx" %>
<%@ Register TagPrefix="uc1" TagName="BadgeSavedData" Src="/Applications/MemberProfile/ProfileTabs30/Controls/Facebook/BadgeSavedData.ascx" %>

<asp:PlaceHolder ID="phPopupAd" runat="server" Visible="false">
    <div class="ad-reservior"></div>
</asp:PlaceHolder>
<uc1:BadgeNoData ID="BadgeNoData1" runat="server" Visible="false" />
<uc1:BadgeSavedData ID="BadgeSavedData1" runat="server" Visible="false" />
<uc1:EssayDataGroup ID="essaysDataGroup" runat="server"></uc1:EssayDataGroup>