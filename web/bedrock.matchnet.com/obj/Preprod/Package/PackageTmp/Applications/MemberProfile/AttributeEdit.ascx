﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AttributeEdit.ascx.cs" Inherits="Matchnet.Web.Applications.MemberProfile.AttributeEdit" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn1" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<div id="page-container" class="page-container">

    <h1 runat="server" id="htmlPageTitle"><mn:Txt runat="server" id="txtAttributeEditTitle" /></h1>

    <asp:PlaceHolder ID="phAttribute" runat="server" Visible="false">
    <fieldset class="form-set form-set-indent">
        <p class="editorial"><mn:Txt runat="server" id="txtAttributeEditText" /></p>

        <asp:DropDownList ID="ddlAttributeOptions" runat="server" ></asp:DropDownList>&nbsp;

        <mn1:FrameworkButton runat="server" ID="FrameworkButton1" ResourceConstant="TXT_SAVE" CssClass="btn primary wide" OnClick="btnSave_OnClick" OnClientClick="return AttributeEditButtonClick(this, true);" />
    </fieldset>
    </asp:PlaceHolder>
    
     
    
    <asp:PlaceHolder ID="phSiteFirstname" runat="server" Visible="false">
    <fieldset class="form-set form-set-indent">
        <p class="editorial"><mn:txt id="txtFirstNameDescription" runat="server" ResourceConstant="TXT_FIRST_NAME_DESCRIPTION" /></p>
        <div class="form-set-stacked margin-heavy">
            <asp:Label AssociatedControlID="SiteFirstName" runat="server"><mn:Txt runat="server" id="lblFirstName" ResourceConstant="TXT_LBL_FIRSTNAME" />:</asp:Label>
            <div class="form-set-item">
            
                <asp:TextBox runat="server" ID="SiteFirstName" CssClass="large" maxlength="15" size="20" />&nbsp;

            
                <mn1:FrameworkButton runat="server" ID="btnSave" ResourceConstant="TXT_SAVE" CssClass="btn primary wide" OnClick="btnSave_OnClick" OnClientClick="return AttributeEditButtonClick(this, true);" />
            </div>
        </div>
        <p><mn:Txt ID="txtFirstNameFindprint" Runat="server" ResourceConstant="TXT_FINEPRINT_FIRSTNAME"/></p>
    </fieldset>
    </asp:PlaceHolder>
    
    <asp:Placeholder Visible="false" runat="server" ID="plcUploadPhoto">
    <div id="pics-forced-main">
        <mn:txt runat="server" id="txtPhotoUpload" ResourceConstant="TXT_PHOTO_UPLOAD" />
        
        <div id="pics-forced-instructions" class="border-gen">
                <div id="divControl" runat="server">
                                                   
                    <h2><mn:txt runat="server" id="txtCaption" ResourceConstant="MAIN_CAPTION" /></h2>
                    <mn:txt id="txtPhotoUploadInstructions" runat="server" resourceconstant="TXT_PHOTO_UPLOAD_INSTRUCTIONS" expandimagetokens="true" />

                    <div id="photo-upload-control">
                        <input class="browse" type="file" runat="server" id="upBrowse" />
                    </div>
                
                    <div id="photo-save-button">
                        <asp:LinkButton Runat="server" ID="btnUploadPhotos" OnClick="btnUploadPhotos_OnClick" OnClientClick="return AttributeEditButtonClick(this, true);"><mn:image id="btnSaveImage" runat="server" filename="btn-save-changes.png" resourceconstant="ALT_CLICK_TO_SUBMIT" /></asp:LinkButton>
                        <mn1:FrameworkButton runat="server" ID="btnPhotoUpdateLater" ResourceConstant="TXT_UPDATE_LATER" CssClass="textlink" OnClick="btnPhotoUpdateLater_OnClick" OnClientClick="return AttributeEditButtonClick(this, false);" /></div>
                    </div>
                </div>

<%--            <script type="text/javascript">
                function blockSubmits() {
                    $j('#photo-save-button').block({
                        message: null,
                        overlayCSS:  {backgroundColor: '#fff',opacity:0.8,cursor: 'default'}
                    });
                }
                blockSubmits();
                $j(document).ready(function () {
                    $j('#photo-upload-control input').change(function () {
                        $thisValue = $j(this).val();
                        if ($thisValue != "") {
                            $j('#photo-save-button').unblock();
                        }
                    });

                });
            </script>--%>

        </div>
    </asp:Placeholder>

    <asp:PlaceHolder ID="phAboutMe" runat="server" Visible="false">
    <fieldset id="about-me" class="form-set">
        <p class="editorial"><asp:Label ID="Label1" AssociatedControlID="txtAboutMe" runat="server"><mn:txt id="txtAboutMeDescription" runat="server" ResourceConstant="TXT_ABOUT_ME_DESCRIPTION" /></asp:Label></p>
        <h2><mn:Txt runat="server" id="txtAboutMeHeader" ResourceConstant="TXT_ABOUT_ME" /></h2>
        <div>
            <p class="float-inside intro-note"><mn:Txt runat="server" id="lblAboutMe" ResourceConstant="TXT_LBL_ABOUTME" /></p>
            <div class="form-set-counter float-outside"><span id="formSetCounter"></span>&nbsp;<mn:txt id="txtCharactersLeft" runat="server" ResourceConstant="TXT_CHARACTERS_LEFT" /></div>
        </div>
        
        <div class="form-set-inline">
            
            <div class="form-set-item">
                <asp:TextBox runat="server" ID="txtAboutMe" CssClass="full" TextMode="MultiLine" maxlength="15" size="50" />
                <div class="clearfix text-outside">
                    <div class="float-outside"><mn1:FrameworkButton runat="server" ID="btnSaveAboutMe" ResourceConstant="TXT_SAVE_CHANGES" CssClass="btn" OnClick="btnAboutMeSave_OnClick" OnClientClick="return AttributeEditButtonClick(this, true);" /><br />
                    <mn1:FrameworkButton runat="server" ID="btnAboutMeUpdateLater" ResourceConstant="TXT_UPDATE_LATER" CssClass="textlink" OnClick="btnAboutMeUpdateLater_OnClick" OnClientClick="return AttributeEditButtonClick(this, false);" /></div>
                </div>
            </div>
        </div>

        <script type="text/javascript">
            $j("[id*=txtAboutMe]").NobleCount('#formSetCounter', {
                max_chars: 3000,
                on_negative: 'count-neg',
                on_positive: 'count-pos',
                block_negative: true,
                on_update: function(t_obj, char_area, c_settings, char_rem){
                    var max_chars = 3000;
                    var char_used = max_chars - char_rem;
                    //console.log(char_used);

                    if(char_used < 50){
                        $j(char_area).addClass('count-neg-min');
                    }
                    if(char_used >= 50){
                        $j(char_area).removeClass('count-neg-min');
                    }
                }
            });

        </script>
        </fieldset>
    </asp:PlaceHolder>


<asp:PlaceHolder ID="phPledgeToCounterFroud" runat="server" Visible="false">
<div id="pledge" class="editorial">
    <h1 class="text-center"><mn:txt runat="server" id="txtPledgeTitle" resourceconstant="TXT_PLEDGE_TITLE" expandimagetokens="true" /></h1>
    <p class="text-center"><mn:txt runat="server" id="txtPledgeSubTitle" resourceconstant="TXT_PLEDGE_SUB_TITLE" expandimagetokens="true" /></p>
    <blockquote class="curlyquotes">
        <mn:txt runat="server" id="txtPledgeBlockquote" resourceconstant="TXT_PLEDGE_BLOCKQUOTE" expandimagetokens="true" />
    </blockquote>
    <p class="text-center"><mn1:FrameworkButton runat="server" ID="btnPledgeOK" ResourceConstant="TXT_I_PLEDGE_OK" CssClass="btn large" OnClick="btnPledgeToCounterFroudSave_OnClick" OnClientClick="return AttributeEditButtonClick(this, true);" /></p>
    <p class="text-center"><mn1:FrameworkButton runat="server" ID="btnPledgeLater" ResourceConstant="TXT_I_PLEDGE_LATER" CssClass="textlink" OnClick="btnPledgeToCounterFroudUpdateLater_OnClick" OnClientClick="return AttributeEditButtonClick(this, false);" /></p>
    <%--<p class="text-center"><a href="/Applications/Article/ArticleView.aspx?CategoryID=114&HideNav=true"><mn:txt runat="server" id="txtPledgeSafetyLink" resourceconstant="TXT_I_PLEDGE_LATER" expandimagetokens="true" /></a></p>--%>
</div>


</asp:PlaceHolder>

<script type="text/javascript">
    var attributeEditObj;
    var attributeEditState = true;
    var attributeEditFraudPledge = <%=_attributeEditFraudPledge.ToString().ToLower()%>;
    var attributeEditAboutMe = <%=_attributeEditAboutMe.ToString().ToLower()%>;
    var attributeEditNoPhoto = <%=_attributeEditNoPhoto.ToString().ToLower()%>;

    // delay is required because FF redirects the page before "s" finishes its job
    function AttributeEditButtonClick(obj, isSave) {
        if (attributeEditState) {
            attributeEditObj = obj;
            attributeEditState = false;

             //Omniture
           
            s.pageName = "Force Page Result";
             s.pageName = "<%=ForcePageResultString %>";

            //set what is being updated for omniture
            //prop 8 is already set when page loads for all attributes (fraud pledge, religion, about me essay, no photo, first name)
            //here we'll make additional updates that needs to be changed on client side for any of these attributes
            if (isSave){
                if (attributeEditFraudPledge) {
                    s.eVar17 = 'FP_Accept';
                }
                else if (attributeEditAboutMe && attributeEditNoPhoto) {
                    //this page version is when both about me and no photo displays at the same time
                    //update prop 8 with only what user is submitting: no photo, about me essay, or both (default)
                    var savingAboutMe = false;
                    var savingPhoto = false;

                    var aboutMeTextbox = $j('#<%=txtAboutMe.ClientID %>');
                    if (aboutMeTextbox){
                        savingAboutMe = $j.trim(aboutMeTextbox.val()).length > 0;
                    }

                    var photoTextbox = $j('#photo-upload-control input');
                    if (photoTextbox){
                        savingPhoto = photoTextbox.val().length > 0;
                    }

                    if (savingAboutMe && savingPhoto){
                        s.prop8 = 'about me essay,no photo';
                    }
                    else if (savingAboutMe){
                        s.prop8 = 'about me essay';
                    }
                    else if (savingPhoto){
                        s.prop8 = 'no photo';
                    }
                }

                s.prop55 = "Save Changes";
            }
            else {
                s.prop55 = "Update Later";
            }

            try{
                console.log(s.prop55 + ': ' + s.prop8);
            }
            catch (e){}

            //remove event3 (login) if it exists
            if (s.events != ''){
                s.events = ReplaceSingle(s.events, 'event3', '');
                if (s.events.substring(0, 1) == ','){
                    s.events = $j.trim(s.events.substring(1));
                }
            }
            //alert(s.events);
            s.t(true);
            setTimeout("attributeEditObj.click();", 1500);

            return false;
        }
        else
            return true;
    }

</script>
</div>