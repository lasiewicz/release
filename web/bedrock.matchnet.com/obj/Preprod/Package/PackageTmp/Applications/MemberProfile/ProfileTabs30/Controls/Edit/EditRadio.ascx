﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EditRadio.ascx.cs" Inherits="Matchnet.Web.Applications.MemberProfile.ProfileTabs30.Controls.Edit.EditRadio" %>

<asp:PlaceHolder ID="phDTEditItem" runat="server">
<div class="form-item">
    <div class="form-label">
        <asp:Label ID="lblEdit" runat="server">
            <asp:Literal ID="litEdit" runat="server"></asp:Literal>
        </asp:Label>
    </div>
    <div class="form-input">
        <ul>
            <asp:Repeater ID="repeaterDTRadioEdit" runat="server" OnItemDataBound="rptOptions_ItemDataBound">
                <ItemTemplate>
                    <li class="float-inside"><asp:Literal ID="radioItemEdit" runat="server"></asp:Literal></li>
                </ItemTemplate>
            </asp:Repeater>
        </ul>
        <div id="itemErrorContainer" runat="server" style="display:none;" class="form-error-inline"></div>
    </div>
</div>

</asp:PlaceHolder>