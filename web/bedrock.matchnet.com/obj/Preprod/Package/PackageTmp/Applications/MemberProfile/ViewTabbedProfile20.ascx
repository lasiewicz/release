﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ViewTabbedProfile20.ascx.cs" Inherits="Matchnet.Web.Applications.MemberProfile.ViewTabbedProfile20" %>
<%@ Register TagPrefix="mp" TagName="StandardProfileTemplate" Src="/Applications/MemberProfile/ProfileTabs30/StandardProfileTemplate.ascx"  %>
<%@ Register TagPrefix="mp" TagName="VisitorProfileTemplate" Src="/Applications/MemberProfile/ProfileTabs30/VisitorProfileTemplate.ascx"  %>
<!--Start ViewTabbedProfile20-->
<div id="divViewTabbedProfileContainer">
  
	<asp:PlaceHolder ID="phTabGroup" Runat="server" >
    
	    <!--this is a new profile view being redesigned-->
	    <mp:StandardProfileTemplate ID="StandardProfileTemplate1" runat="server" visible="false"></mp:StandardProfileTemplate>
	    <!--this is a new profile for visitor viewing only-->
	    <mp:VisitorProfileTemplate ID="VisitorProfileTemplate1" runat="server" visible="false"></mp:VisitorProfileTemplate>
	</asp:PlaceHolder>
	
	<%--Toma Access Test--%>
	<!--
	<asp:Label ID="lblToma" runat="server" Visible="false"></asp:Label>
	-->
	
</div>

<asp:Literal id="phPixel" runat="server" />

<!--End ViewTabbedProfile20-->