﻿using System;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects.Interfaces;
using Matchnet.Web.Framework.Ui;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Ui.SearchElements;
using Matchnet.Web.Applications.MemberProfile.ProfileTabs30.Controls;
using Matchnet.Web.Analytics;
using Matchnet.Web.Framework.Ui.Feedback;
using Matchnet.Web.Framework.Search;
using Matchnet.Web.Applications.Search;
using Matchnet.Web.Framework.Managers;
using Matchnet.Web.Framework.Ui.PageAnnouncements;
using Matchnet.Web.Applications.Home;

namespace Matchnet.Web.Applications.MemberProfile.ProfileTabs30
{
    /// <summary>
    /// Standard template which acts as the overall profile page container.
    /// </summary>
    public partial class StandardProfileTemplate : BaseProfileTemplate
    {
        protected string _NoEssayAnswer = "";
        protected int _MaxEssayLength = Matchnet.Web.Applications.MemberProfile.ProfileTabs30.Controls.Edit.EditProfileUtility.MAX_ESSAY_LENGTH;
        protected string _JSKeywordSearch = "";

        #region Event Handlers
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            _NoEssayAnswer = GetNoAnswer(editResourceControl1);
            _g.LayoutTemplateBase.LoadFacebookScript();
        }

        protected override void OnPreRender(EventArgs e)
        {
            try
            {
                //note: Rendering JS here is to support flaw in using jquery modals
                //render any edit section JS
                if (g.ProfileSectionItemJS != null)
                {
                    litEditSectionJS.Text = g.ProfileSectionItemJS.ToString();
                }

                //render any edit item JS
                if (g.ProfileEditItemJS != null)
                {
                    litEditItemJS.Text = g.ProfileEditItemJS.ToString();
                }

                //omniture
                lnkEmailMeBottom.NavigateUrl = lnkEmailMeBottom.NavigateUrl + Omniture.GetOmnitureClickToPageURLParams(ProfileUtility.ProfilePageName, WebConstants.PageIDs.Compose, "", WebConstants.Action.Email, false);

                base.OnPreRender(e);
            }
            catch (Exception ex)
            { g.ProcessException(ex); }
        }

        #endregion

        /// <summary>
        /// Loads member profile information
        /// </summary>
        /// <param name="member"></param>
        /// <param name="activeTab"></param>
        public override void LoadProfile(IMemberDTO member, ProfileTabEnum activeTab)
        {
            if (member == null) return;

            //set base properties
            this._MemberProfile = member;
            this.ActiveTab = activeTab;

            if (g.Member != null && g.Member.MemberID == member.MemberID)
                WhoseProfile = WhoseProfileEnum.Self;

            //timestamps
            DateTime loginDate = _MemberProfile.GetLastLogonDate(g.Brand.Site.Community.CommunityID);
            lastLoginDate.LastLoginDateValue = loginDate;
            lastUpdateDate.Date = getDateDisplay(_MemberProfile.GetAttributeDate(g.Brand, "LastUpdated", loginDate));

            //TEMP FOR QA
            if (!Page.IsPostBack)
            {
                txtTempLastLogonDate.Text = loginDate.ToString();
                DateTime lastProfileViewedAlertEmailDate = _MemberProfile.GetAttributeDate(g.Brand, "ProfileViewedAlertLastSentDate", DateTime.MinValue);
                if (lastProfileViewedAlertEmailDate > DateTime.MinValue)
                {
                    txtTempLastProfileViewedAlertDate.Text = lastProfileViewedAlertEmailDate.ToString();
                }
            }

            //set Entry Point
            entryPoint = BreadCrumbHelper.GetEntryPoint(Conversion.CInt(Request[WebConstants.URL_PARAMETER_NAME_ENTRYPOINT]));

            //show popup-header
            if (!BreadCrumbHelper.IsProfilePopupDisabled(member.MemberID))
            {
                //ensure wide2column is not being used (could happen due to searching by memberID even when popup is enabled)
                if (g.LayoutTemplate != Content.ValueObjects.PageConfig.LayoutTemplate.Wide2Columns)
                {
                    phPopupHeader.Visible = true;

                    //set next/prev links
                    SetNextPrevLinks();

                    //show toolbar
                    profileToolBar1.LoadProfileToolBar(member);
                }
            }

            //set Back To Result links
            string backtoResultsURL = String.Empty;

            string urlVal = FrameworkGlobals.GetCookie(WebConstants.SESSION_PROPERTY_NAME_BACKTORESULT_URL, null, true);
            if ((!IsEditEnabled || (IsEditEnabled && entryPoint != BreadCrumbHelper.EntryPoint.NoArgs)) && !String.IsNullOrEmpty(urlVal))
            {
                backtoResultsURL = urlVal;
            }

            //Determine whether to display mini-search
            MiniSearchBar miniSearchBar = null;
            if (g.HeaderControl != null)
            {
                miniSearchBar = (g.HeaderControl as Header20).MiniSearchBar;
                var isPhotoGallery30Enabled = PhotoGalleryManager.Instance.IsPhotoGallery30Enabled(_g.Brand);

                if (miniSearchBar != null  
                    && !(entryPoint == BreadCrumbHelper.EntryPoint.PhotoGallery && isPhotoGallery30Enabled)  
                    && (!IsEditEnabled || (IsEditEnabled && entryPoint != BreadCrumbHelper.EntryPoint.NoArgs)) 
                    && entryPoint != BreadCrumbHelper.EntryPoint.MemberSlideshow 
                    && entryPoint != BreadCrumbHelper.EntryPoint.SlideShowPage
                    && !HomeUtil.DisplaySecretAdmirerGameFromHomepageFilmstrip(entryPoint))
                {
                    //load mini-search
                    miniSearchBar.Visible = true;
                    miniSearchBar.LoadMiniSearch();
                }
            }

            if (g.LayoutTemplateBase != null)
            {
                if (miniSearchBar != null && miniSearchBar.Visible)
                {
                    //add custom body CSS class when mini-search is displayed
                    g.LayoutTemplateBase.AppendBodyCSS("minisearch-bar-on");
                }
                else
                {
                    //add custom body CSS class when mini-search is not displayed
                    //g.LayoutTemplateBase.AppendBodyCSS("minisearch-bar-off");
                }
            }

            //Load Secret Admirer Game
            if (g.HeaderControl != null)
            {
                if (entryPoint == BreadCrumbHelper.EntryPoint.MemberSlideshow 
                    || entryPoint == BreadCrumbHelper.EntryPoint.SlideShowPage 
                    || HomeUtil.DisplaySecretAdmirerGameFromHomepageFilmstrip(entryPoint))
                {
                    SecretAdmirerGame secretAdmirerGame = (g.HeaderControl as Header20).SecretAdmirerGame;
                    secretAdmirerGame.Visible = true;
                    secretAdmirerGame.LoadSecretAdmirerGame(_MemberProfile, entryPoint);
                }
            }

            //Load photo slider
            PhotoSlider1.LoadPhotoSlider(_MemberProfile);

            //Load Profile toolbar
            if (g.RightNavControl != null)
            {
                ProfileToolBar profileToolBar = g.RightNavControl.ProfileToolBar;
                if (profileToolBar != null)
                {
                    profileToolBar.Visible = true;
                    profileToolBar.LoadProfileToolBar(_MemberProfile);
                }
            }

            //Load M2MCommunication module
            m2mCommunication1.MutualYesElementID = basicInfo1.SecretAdmirerControl.ClientID;
            m2mCommunication1.LoadM2MCommunication(_MemberProfile, ProfileUtility.ProfilePageName);

            //Load Basic Info
            basicInfo1.LoadBasicInfo(_MemberProfile);
            
            //Load Dealbreaker info
            nvDataGroupDealBreakerInfo.MaxDisplayLength = ProfileUtility.MaxDisplayLengthNVDouble;
            nvDataGroupDealBreakerInfo.LoadDataGroup(member, ProfileUtility.GetProfileDataGroup(ProfileDataGroupEnum.Dealbreakers, g.Brand.Site.SiteID, g));

            //Load Profile Tabs
            ProfileTabGroup1.LoadTabGroup(member, activeTab);

            //Load Profile Details
            ProfileDetails profileDetails = g.RightNavControl.ProfileDetails;
            if (profileDetails != null)
            {
                profileDetails.Visible = true;
                profileDetails.LoadProfileDetails(_MemberProfile);
            }

            //Hide Icon Links in master Layout Template
            if (g.LayoutTemplateBase != null)
            {
                g.LayoutTemplateBase.HideIconLinks();
            }

            //Email Me section at the bottom
            lnkEmailMeBottom.NavigateUrl = ProfileDisplayHelper.GetEmailLink(member.MemberID, true);

            //Show Feedback widget
            if (g.LayoutTemplateBase != null)
            {
                if (FeedbackHelper.IsProfile30FeedbackEnabled(g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID))
                {
                    g.LayoutTemplateBase.ShowFeedback(Matchnet.Web.Framework.Ui.Feedback.FeedbackHelper.FeedbackType.BetaProfile);
                }
            }

            if (WhoseProfile == WhoseProfileEnum.Self)
            {
                //Display edit profile message at the top of page
                (g.HeaderControl as Header20).ShowEditProfileMessage = true;

                //Output javascript when viewing own profile to disable certain things but still make it visible
                phHideEmail.Visible = true;

                //Output edit related JS
                phEditJS.Visible = true;
                phEditJSAfter.Visible = true;

                if (FacebookManager.Instance.IsFacebookLikesInterestsDataEnabled(g.Brand))
                {
                    bool showFBLikesAnnouncementOverride = false;
                    if (!Page.IsPostBack && !String.IsNullOrEmpty(Request["showFBLikesAnnouncement"]) &&
                        Request["showFBLikesAnnouncement"] == "true")
                    {
                        showFBLikesAnnouncementOverride = true;
                    }

                    //display Facebook feature announcement
                    if (showFBLikesAnnouncementOverride || (!FacebookManager.Instance.HasMemberSeenFacebookPrompt(g.Member, g.Brand) && FacebookManager.Instance.GetFacebookConnectStatus(g.Member, g.Brand) == FacebookManager.FacebookConnectStatus.NonConnected))
                    {
                        FacebookFeatureAnnouncement fbAnnouncement = Page.LoadControl("/Framework/Ui/PageAnnouncements/FacebookFeatureAnnouncement.ascx") as FacebookFeatureAnnouncement;
                        fbAnnouncement.LoadAnnouncement();
                        g.LayoutTemplateBase.AddPageAccouncementControl(fbAnnouncement);
                    }
                }
            }
            else
            {
                //set keyword highlighting for JS
                if (SearchUtil.IsKeywordHighlightEnabled(g))
                {
                    if (entryPoint == BreadCrumbHelper.EntryPoint.SearchResults)
                    {
                        phKeywordHighlightJS.Visible = true;
                        if (!string.IsNullOrEmpty(g.SearchPreferences["KeywordSearch"]))
                        {
                            _JSKeywordSearch = g.SearchPreferences["KeywordSearch"].Replace("\"", "");
                        }
                    }
                    else if (entryPoint == BreadCrumbHelper.EntryPoint.AdvancedSearchResults)
                    {
                        phKeywordHighlightJS.Visible = true;
                        if (!string.IsNullOrEmpty(g.AdvancedSearchPreferences["KeywordSearch"]))
                        {
                            _JSKeywordSearch = g.AdvancedSearchPreferences["KeywordSearch"].Replace("\"", "");
                        }
                    }
                }
            }            
        }

        private void SetNextPrevLinks()
        {
            if (entryPoint != BreadCrumbHelper.EntryPoint.MemberSlideshow && entryPoint != BreadCrumbHelper.EntryPoint.SlideShowPage)
            {
                string nextProfileLink = string.Empty;
                string prevProfileLink = string.Empty;
                try
                {
                    nextProfileLink = BreadCrumbHelper.StaticGetNextProfileLinkWithActiveTab(this._MemberProfile.MemberID, Request, g);
                    prevProfileLink = BreadCrumbHelper.StaticGetPreviousProfileLinkWithActiveTab(this._MemberProfile.MemberID, Request, g);
                }
                catch (Exception ex)
                {
                    g.ProcessException(ex);
                }

                if (entryPoint == BreadCrumbHelper.EntryPoint.KeywordSearchResults)
                {
                    var results = KeywordSearcher.GetKeywordSearchResults(
                        g.Member == null ? Constants.NULL_INT : g.Member.MemberID,
                        SearchPageHelper.GetKeywordSearchPreferences(Request));

                    Int32 ordinal;

                    if (Int32.TryParse(Request.Params["ordinal"], out ordinal) && ordinal >= results.total)
                    {
                        nextProfileLink = "";
                    }
                }

                if (!string.IsNullOrEmpty(nextProfileLink))
                {
                    lnkNextProfile.Visible = true;
                    lnkNextProfile.NavigateUrl = nextProfileLink;
                }

                if (!string.IsNullOrEmpty(prevProfileLink))
                {
                    lnkPreviousProfile.Visible = true;
                    lnkPreviousProfile.NavigateUrl = prevProfileLink;
                }
            }
        }

        protected void btnTempQASubmit_Click(object sender, EventArgs e)
        {
            litTempProfileAlert.Text = "Attempting to save member dates";
            if (_MemberProfile != null && _MemberProfile.MemberID > 0)
            {
                Member.ServiceAdapters.Member aMember = Member.ServiceAdapters.MemberSA.Instance.GetMember(_MemberProfile.MemberID, MemberLoadFlags.None);
                aMember.SetAttributeDate(g.Brand, "BrandLastLogonDate", Convert.ToDateTime(txtTempLastLogonDate.Text));
                if (!string.IsNullOrEmpty(txtTempLastProfileViewedAlertDate.Text))
                {
                    aMember.SetAttributeDate(g.Brand, "ProfileViewedAlertLastSentDate", Convert.ToDateTime(txtTempLastProfileViewedAlertDate.Text));
                }
                Matchnet.Member.ServiceAdapters.MemberSA.Instance.SaveMember(aMember);
                litTempProfileAlert.Text = "Saved member dates";
            }
        }
    }
}
