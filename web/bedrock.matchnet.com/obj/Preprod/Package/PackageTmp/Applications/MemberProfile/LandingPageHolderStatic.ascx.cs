using System;
using System.IO;
using System.Net;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using Matchnet.Web.Framework;
using Matchnet.Web;
using Matchnet.Content.ValueObjects.LandingPage;

namespace Matchnet.Web.Applications.MemberProfile
{
	/// <summary>
	///		Summary description for LandingPageHolderStatic.
	/// </summary>
	public class LandingPageHolderStatic : FrameworkControl
	{
		protected Literal litLandingPageContent;

		private void Page_Load(object sender, System.EventArgs e)
		{
			LandingPage landingPage = g.LandingPage;

			Uri landingPageUri = new Uri(landingPage.StaticURL);
			WebRequest landingPageRequest = WebRequest.Create(landingPageUri);
			WebResponse landingPageResponse = landingPageRequest.GetResponse();

			StreamReader streamReader = new StreamReader(landingPageResponse.GetResponseStream(), System.Text.Encoding.GetEncoding("utf-8"));

			string imagePath = string.Format("img/Brand/{0}/LandingPage/{1}/images/", g.Brand.Uri.Replace(".", "-"), landingPage.LandingPageName);

			string sourcePath = "images/";
			//Hack to support legacy landing pages LPID=37,38.
			//These Landing Pages use non-standard image path names.
			if (landingPage.LandingPageID == 37)
			{
				sourcePath = "glimpse_guys_files/";
			}
			else if (landingPage.LandingPageID == 38)
			{
				sourcePath = "glimpse_mix_files/";
			}

			litLandingPageContent.Text = streamReader.ReadToEnd().Replace(sourcePath, imagePath);

			streamReader.Close();
			landingPageResponse.Close();
		}

		private void Page_Init(object sender, System.EventArgs e)
		{	
			// Omniture Analytics - Use Sage Place Holder for now.
			if (Convert.ToBoolean(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ANALYTICS_OMNITURE", _g.Brand.Site.Community.CommunityID, _g.Brand.Site.SiteID)))
			{
				Matchnet.Web.Analytics.Omniture omniture = (Matchnet.Web.Analytics.Omniture) Page.LoadControl("/Analytics/Omniture.ascx");
				PlaceHolder placeHolderAnalytics = new PlaceHolder();
				placeHolderAnalytics.Controls.Add(omniture);
				this.Controls.Add(placeHolderAnalytics);
				_g.AnalyticsOmniture = omniture;
			}
		}

		protected override void OnPreRender(EventArgs e)
		{
			// Omniture Analytics - Use Sage Place Holder for now.
			if (Convert.ToBoolean(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ANALYTICS_OMNITURE", _g.Brand.Site.Community.CommunityID, _g.Brand.Site.SiteID)))
			{
				// Registration start event.
				_g.AnalyticsOmniture.AddEvent("event7"); 
			}

			base.OnPreRender (e);
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Init +=new EventHandler(this.Page_Init);
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
