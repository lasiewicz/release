﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Member.ValueObjects.Interfaces;
using Matchnet.Search.ValueObjects;
using Matchnet.Web.Framework;
using System.Web.UI.HtmlControls;
using Matchnet.Content.ServiceAdapters.Links;
using Matchnet.List.ValueObjects;
using Matchnet.Web.Framework.Ui.BasicElements;
using System.Data;
using Matchnet.Web.Framework.Util;
using Matchnet.Web.Applications.MemberProfile.ProfileTabs30.Controls.Edit;
using Matchnet.Web.Framework.Managers;
using Matchnet.Search.Interfaces;
using System.Text;
using Matchnet.Search.ServiceAdapters;

namespace Matchnet.Web.Applications.MemberProfile.ProfileTabs30.Controls
{
    public partial class BasicInfo : BaseProfile
    {
        protected string _CurrentJSSectionObjName = "";
        protected string _ViewContainerEditCSSClass = "prof-edit-region prof-edit-active";
        protected int _JSViewerMemberID = 0;

        public HtmlGenericControl SecretAdmirerControl
        {
            get { return spanSecretAdmirerYY; }
        }

        public bool IsPartialRender { get; set; }

        #region Event handlers
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        #endregion

        #region Public methods
        public void LoadBasicInfo(IMemberDTO member)
        {
            /*
             * NOTE
             * For Basic Info, the view is not standard so we will not be loading it dynamically via XML.
             * But the edit version for basic info will use the XML to dynamically render the form and to process it on submit (via ajax)
             * */

            MemberProfile = member;
            if (g.Member != null && g.Member.MemberID == member.MemberID)
            {
                WhoseProfile = WhoseProfileEnum.Self;
            }

            if (g.Member != null)
            {
                _JSViewerMemberID = g.Member.MemberID;
            }

            #region View Basic Info
            //Load Standard Member Info
            this.literalUsername.Text = _MemberProfile.GetUserName(g.Brand).Trim() != "" ? _MemberProfile.GetUserName(g.Brand) : "&nbsp;";
            this.spanNewMember.Visible = _MemberProfile.IsNewMember(g.Brand.Site.Community.CommunityID);
            this.spanUpdatedMember.Visible = !spanNewMember.Visible && _MemberProfile.IsUpdatedMember(g.Brand.Site.Community.CommunityID);

            bool isOnline = false;
            OnlineLinkHelper onlineLinkHelper = OnlineLinkHelper.getOnlineLinkHelper(Matchnet.Web.Framework.Ui.BasicElements.ResultContextType.None, this.MemberProfile, g.Member, LinkParent.FullProfile, (int)PurchaseReasonType.AttemptToIMFullProfile, out isOnline);
            if (isOnline)
            {
                phStatusOnline.Visible = true;
                lnkIMMe.NavigateUrl = onlineLinkHelper.OnlineLink;
                phStatusOffline.Visible = false;
            }
            else
            {
                phStatusOnline.Visible = false;
                phStatusOffline.Visible = true;
            }
            this.literalMaritalAndGender.Text = GetMemberAttributeValue("gendermask", this, true);
            this.literalAgeLocation.Text = GetMemberAttributeValue("agelocation", this, true);

            if (g.Member != null && g.Member.MemberID > 0)
            {
                //YY Icon
                ClickMask clickMask = g.List.GetClickMask(g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, this.MemberProfile.MemberID);
                if ((((clickMask & ClickMask.TargetMemberYes) == ClickMask.TargetMemberYes) && ((clickMask & ClickMask.MemberYes) == ClickMask.MemberYes)))
                {
                    spanSecretAdmirerYY.Style["display"] = "inline-block";
                }

                spanSecretAdmirerYY.Attributes.Add("title", g.GetResource("TXT_YNM_YY_TITLE", this));
            }
            //OmniDate
            btnOmnidate.TargetMember = _MemberProfile;


            // JMeter
            int mmsett = Int32.Parse(RuntimeSettings.GetSetting("MatchTest_App_Settings", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID));
            int mmenumval = (Int32)WebConstants.MatchMeterFlags.EnableApp;
            if ((mmsett & mmenumval) == mmenumval)
            {
                phCustomMatchMeter.Visible = true;
                ucMatchMeterInfoDisplay.Visible = true;
                ucMatchMeterInfoDisplay.IsMatchMeterEnabled = true;
                ucMatchMeterInfoDisplay.MemberID = _MemberProfile.MemberID;
            }

            //Match Rating
            phMatchRating.Visible = false;
            phMatchRating2.Visible = false;
            // JDIL and JDFR uses a different mutual match view.
            bool showAlternateMatchRating = (g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.JDateCoIL ||
                                             g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.JDateFR);
            if (g.Member != null)
            {
                if (WhoseProfile == WhoseProfileEnum.Other &&
                    MatchRatingManager.Instance.DisplayMatchRatingOnProfile(g.Member.MemberID, g.Brand))
                {
                    //Calculate match rating
                    MatchRatingResult matchRatingResult = MatchRatingManager.Instance.CalculateMatchPercentage(g.Member.MemberID,
                                                                                           _MemberProfile.MemberID,
                                                                                           g.Brand.Site.Community
                                                                                            .CommunityID,
                                                                                           g.Brand.Site.SiteID, SearchType.None);
                    if (matchRatingResult != null && matchRatingResult.MatchScore >= 0)
                    {
                        if (showAlternateMatchRating)
                        {
                            phMatchRating2.Visible = true;
                        }
                        else
                        {
                            phMatchRating.Visible = true;
                        }


                        if (matchRatingResult.MatchScore >= 50)
                        {
                            if (showAlternateMatchRating)
                            {
                                phMatchRatingValuePercentage2.Visible = true;
                            }
                            else
                            {
                                phMatchRatingValuePercentage.Visible = true;    
                            }
                            
                            if(showAlternateMatchRating)
                            {
                                litMatchRating2.Text = matchRatingResult.MatchScore.ToString();
                            }
                            else
                            {
                                litMatchRating.Text = matchRatingResult.MatchScore.ToString();
                            }
                            
                        }
                        else
                        {
                            if (showAlternateMatchRating)
                            {
                                phMatchRatingValueLess50Percentage2.Visible = true;
                            }
                            else
                            {
                                phMatchRatingValueLess50Percentage.Visible = true;    
                            }
                            
                        }
                        g.AnalyticsOmniture.Evar64 = "Match Icon ON";

                        if (
                            MatchRatingManager.Instance.IsMatchRatingCaptureMismatchDataOnProfileEnabled(
                                g.Brand.Site.Community.CommunityID))
                        {
                            phMatchRatingMistmatchData.Visible = true;
                            litMatchRatingMismatchFinalScore.Text = matchRatingResult.MatchScore.ToString() + "%";
                            litMatchRatingMismatchMember1Score.Text = matchRatingResult.MatchScoreMember1.ToString() + "%";
                            litMatchRatingMismatchMember2Score.Text = matchRatingResult.MatchScoreMember2.ToString() + "%";
                            if (matchRatingResult.MismatchesMember1 != null)
                            {
                                StringBuilder sbMatchRatingMismatchMember1 = new StringBuilder();
                                foreach (string key in matchRatingResult.MismatchesMember1.Keys)
                                {
                                    sbMatchRatingMismatchMember1.AppendLine(key + ": " + matchRatingResult.MismatchesMember1[key]);
                                }
                                litMember1MatchRatingMismatch.Text = sbMatchRatingMismatchMember1.ToString();
                            }

                            if (matchRatingResult.MismatchesMember2 != null)
                            {
                                StringBuilder sbMatchRatingMismatchMember2 = new StringBuilder();
                                foreach (string key in matchRatingResult.MismatchesMember2.Keys)
                                {
                                    sbMatchRatingMismatchMember2.AppendLine(key + ": " + matchRatingResult.MismatchesMember2[key]);
                                }
                                litMember2MatchRatingMismatch.Text = sbMatchRatingMismatchMember2.ToString();
                            }

                            if (matchRatingResult.MissingAttributeMember1 != null)
                            {
                                StringBuilder sbMatchRatingMissingMember1 = new StringBuilder();
                                foreach (string key in matchRatingResult.MissingAttributeMember1.Keys)
                                {
                                    sbMatchRatingMissingMember1.AppendLine(matchRatingResult.MissingAttributeMember1[key]);
                                }
                                litMember1MatchRatingMissing.Text = sbMatchRatingMissingMember1.ToString();
                            }

                            if (matchRatingResult.MissingAttributeMember2 != null)
                            {
                                StringBuilder sbMatchRatingMissingMember2 = new StringBuilder();
                                foreach (string key in matchRatingResult.MissingAttributeMember2.Keys)
                                {
                                    sbMatchRatingMissingMember2.AppendLine(matchRatingResult.MissingAttributeMember2[key]);
                                }
                                litMember2MatchRatingMissing.Text = sbMatchRatingMissingMember2.ToString();
                            }

                            SearchPreferenceCollection searchPreferences1 = SearchPreferencesSA.Instance.GetSearchPreferences(matchRatingResult.Member1ID, g.Brand.Site.Community.CommunityID);
                            SearchPreferenceCollection searchPreferences2 = SearchPreferencesSA.Instance.GetSearchPreferences(matchRatingResult.Member2ID, g.Brand.Site.Community.CommunityID);
                            litMember1DefaultPreferences.Text = searchPreferences1.IsDefaultPreferences
                                                                    ? "Default"
                                                                    : "Saved";
                            litMember2DefaultPreferences.Text = searchPreferences2.IsDefaultPreferences
                                                                    ? "Default"
                                                                    : "Saved";
                        }
                    }
                }
            }

            //Mutual Match Proof of Concept
            phMutualMatchPOC.Visible = false;
            if (!IsPartialRender && !phMatchRating.Visible)
            {
                g.AnalyticsOmniture.Evar64 = "Match Icon OFF";
                if (WhoseProfile == WhoseProfileEnum.Other && g.Member != null)
                {
                    if (MatchRatingManager.Instance.IsMutualMatchPOCEnabled(g.Brand.BrandID, g.Brand.Site.SiteID, g.Brand.Site.Community.CommunityID))
                    {
                        //only show if user has never answered
                        DateTime mmpocAnsweredDate = g.Member.GetAttributeDate(g.Brand, "MutualMatchPOCAnsweredDate", DateTime.MinValue);
                        if (mmpocAnsweredDate == DateTime.MinValue)
                        {
                            phMutualMatchPOC.Visible = true;
                            g.AnalyticsOmniture.Evar64 = "Match Icon ON";
                        }
                    }
                }
            }

            //display Ramah badge

            if (g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.JDate && Convert.ToBoolean(member.GetAttributeInt(_g.Brand, "RamahAlum")))
                pnlRamahBadge.Visible = true;


            #endregion

            #region Edit Basic Info
            //Edit Basic Info
            if (WhoseProfile == WhoseProfileEnum.Self)
            {
                phEditIcon.Visible = true;
                litEditIcon.Text = g.GetResource("EDIT_ICON", this);
                profileSectionBasic.Attributes["class"] = _ViewContainerEditCSSClass;
                litRequiredMessage.Text = g.GetResource("TXT_REQUIRED_MESSAGE", this);
            }

            if (!IsPartialRender && WhoseProfile == WhoseProfileEnum.Self)
            {
                //show edit section
                phBasicEdit.Visible = true;

                //get basic section based on xml
                XMLProfileData.ProfileDataGroup basicDataGroup = ProfileUtility.GetProfileDataGroup(ProfileDataGroupEnum.Basic, g.Brand.Site.SiteID, g);
                if (basicDataGroup != null)
                {
                    XMLProfileData.ProfileDataSection basicDataSection = basicDataGroup.GetProfileDataSection(ProfileDataSectionEnum.BasicInfo);

                    //render JS section object
                    EditProfileUtility.AddEditSectionJS(g, EditProfileUtility.GetJSEditSection(g, this, basicDataSection, basicDataGroup.ProfileDataGroupType,
                        profileSectionBasic.ClientID, profileSectionBasicEdit.ClientID, errorSectionContainer.ClientID, true, out _CurrentJSSectionObjName));

                    //create edit controls
                    if (basicDataSection != null)
                    {
                        for (int i = 0; i < basicDataSection.ProfileDataItemList.Count; i++)
                        {
                            XMLProfileData.ProfileDataItem dataItem = basicDataSection.ProfileDataItemList[i];

                            BaseEdit editControl = EditProfileUtility.GetEditControl(Page, dataItem.EditControlType, dataItem.AttributeName);
                            if (editControl != null)
                            {
                                phEditControls.Controls.Add(editControl);
                                editControl.LoadEditControl(basicDataGroup, basicDataSection, dataItem, _CurrentJSSectionObjName, this);
                            }
                        }
                    }
                }

            }

            #endregion

        }

        #endregion

        #region Private methods
        protected string GetOmnidateDiv(string type)
        {
            if (btnOmnidate.IsOmnidateDisplayed || g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.JDateCoIL)
            {
                return (type == "open" ? "<div class='profile30-title'>" : "</div>");
            }

            return string.Empty;
        }

        #endregion
    }
}
