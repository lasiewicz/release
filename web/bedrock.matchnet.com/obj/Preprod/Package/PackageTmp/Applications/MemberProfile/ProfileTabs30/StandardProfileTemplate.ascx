﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="StandardProfileTemplate.ascx.cs"
    Inherits="Matchnet.Web.Applications.MemberProfile.ProfileTabs30.StandardProfileTemplate" %>
<%@ Register TagPrefix="uc1" TagName="LastLoginDate" Src="/Framework/Ui/BasicElements/LastLoginDate.ascx" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="uc1" TagName="LastUpdateDate" Src="/Framework/Ui/BasicElements/LastUpdateDate.ascx" %>
<%@ Register TagPrefix="uc1" TagName="NameValueDataGroup" Src="/Applications/MemberProfile/ProfileTabs30/Controls/ProfileDataGroup/NameValueDataGroup.ascx" %>
<%@ Register TagPrefix="uc1" TagName="TabGroup" Src="/Applications/MemberProfile/ProfileTabs30/Controls/ProfileTab/TabGroup.ascx" %>
<%@ Register TagPrefix="mn" TagName="M2MCommunication" Src="/Applications/MemberProfile/ProfileTabs30/Controls/M2MCommunication.ascx" %>
<%@ Register TagPrefix="mn" TagName="PhotoSlider" Src="/Applications/MemberProfile/ProfileTabs30/Controls/PhotoSlider.ascx" %>
<%@ Register TagPrefix="mn" TagName="BasicInfo" Src="/Applications/MemberProfile/ProfileTabs30/Controls/BasicInfo.ascx" %>
<%@ Register TagPrefix="mn" TagName="EditResourceControl" Src="/Applications/MemberProfile/ProfileTabs30/Controls/Edit/EditResourceControl.ascx" %>
<%@ Register TagPrefix="mn" TagName="ProfileToolBar" Src="/Applications/MemberProfile/ProfileTabs30/Controls/ProfileToolBar.ascx" %>

<asp:PlaceHolder ID="phEditJS" runat="server" Visible="false">
    <script type="text/javascript">
        //define objects
        //1. EditItem
        //2. EditSection
        //3. EditManager
        function spark_profile30_EditItem() {
            this.attributeName = '';
            this.editControlID = '';
            this.editControlTypeID = '';
            this.editDataTypeID = '';
            this.viewContainerID = '';
            this.editContainerID = '';
            this.errorContainerID = '';
            this.hasDuplicate = false;
            this.refreshSectionNameList = [];
        }

        spark_profile30_EditItem.prototype.addRefreshSectionName = function(refreshSectionName) {
            spark.util.addItem(this.refreshSectionNameList, refreshSectionName);
        }

        function spark_profile30_EditSection() {
            this.editItemList = [];
            this.groupType = '';
            this.groupTypeID = '';
            this.sectionName = '';
            this.title = '';
            this.viewContainerID = '';
            this.editContainerID = '';
            this.editBySection = true;
            this.errorContainerID = '';
            this.isMatchPreferences = false;
        }
        
        spark_profile30_EditSection.prototype.addEditItem = function(editItemObj) {
            spark.util.addItem(this.editItemList, editItemObj);
        }

        spark_profile30_EditSection.prototype.getEditItem = function(attributeName) {
            var editItemObj = null;
            if (attributeName != null && attributeName != ''){
                if (this.editItemList.length > 0) {
                    for (var i = 0; i < this.editItemList.length; i++) {
                        if (this.editItemList[i].attributeName == attributeName) {
                            editItemObj = this.editItemList[i];
                            break;
                        }
                    }
                }
            }

            return editItemObj;
        }

        function spark_profile30_EditManager() {
            this.updateInProgress = false;
            this.editSectionList = [];
            this.memberID = 0;
            this.maxEssayLength = <%=_MaxEssayLength%>;
            this.noEssayAnswer = "<%=_NoEssayAnswer %>";
            this.addEditSection = function(editSectionObj) {
                spark.util.addItem(this.editSectionList, editSectionObj);
            }
            this.getSection = function(sectionName) {
                var editSectionObj = null;
                if (this.editSectionList.length > 0) {
                    for (var i = 0; i < this.editSectionList.length; i++) {
                        if (this.editSectionList[i].sectionName == sectionName) {
                            editSectionObj = this.editSectionList[i];
                            break;
                        }
                    }
                }

                return editSectionObj;
            }
            this.refreshAds = function() {
                //refresh ad
                if (tabGroup != null){
                    tabGroup.refreshAds();
                }
            }
            this.CancelSectionEdit = function(sectionName){
                //for now we will refresh the page
                window.location = window.location;
            }
            this.CancelItemEdit = function(sectionName, attributeName){
                var sectionObj = this.getSection(sectionName);
                var itemObj = sectionObj.getEditItem(attributeName);

                $j('#' + itemObj.viewContainerID).toggleClass('hide');
                $j('#' + itemObj.editContainerID).toggleClass('hide');

                if (itemObj.editControlTypeID == 3){
                    //reset checkboxes
                    var originalMaskVal = parseInt($j('#' + itemObj.editControlID.HidOriginalValueID).val());
                    $j("input[type='checkbox']", "#" + itemObj.editContainerID).each(function(index) {
                        var chkValue = parseInt($j(this).val());
                        if ((chkValue & originalMaskVal) == chkValue){
                            $j(this).attr('checked','checked');
                        }
                        else{
                            $j(this).removeAttr('checked');
                        }
                    });
                    $j('#' + itemObj.editControlID.HidEditValueID).val(originalMaskVal);
                }
                else if (itemObj.editControlTypeID == 4){
                    //reset dropdown
                    $j('#' + itemObj.editControlID.EditControlID).val($j('#' + itemObj.editControlID.HidOriginalValueID).val());
                }
            }
            this.SaveProfile = function(sectionName, attributeName) {
                var isVerbose = true;
                if (attributeName == 'undefined' || attributeName == null){
                    attributeName = '';
                }
                //grab section obj
                var sectionObj = this.getSection(sectionName);
                var editContainerID = sectionObj.editContainerID;
                var viewContainerID = sectionObj.viewContainerID;
                var itemObj = null;

                //grab form data
                var dataParams = "";
                for (var i = 0; i < sectionObj.editItemList.length; i++)
                {
                    itemObj = sectionObj.editItemList[i];
                    if (attributeName == '' || itemObj.attributeName == attributeName){
                        if (attributeName != ''){
                            editContainerID = itemObj.editContainerID;
                            viewContainerID = itemObj.viewContainerID;
                        }

                        if (itemObj.editControlTypeID == 2){
                            //radio
                            dataParams = dataParams + itemObj.attributeName + "=" + $j('input:radio[name=' + itemObj.editControlID + ']:checked').val() + "&";
                        }
                        else if (itemObj.editControlTypeID == 3){
                            //checkbox
                            dataParams = dataParams + itemObj.attributeName + "=" + spark.util.uniescape($j('#' + itemObj.editControlID.HidEditValueID).val()) + "&";
                        }
                        else if (itemObj.editControlTypeID == 4){
                            //dropdown
                            dataParams = dataParams + itemObj.attributeName + "=" + spark.util.uniescape($j('#' + itemObj.editControlID.EditControlID).val()) + "&";
                        }
                        else if (itemObj.editControlTypeID < 20){
                            //other standard form controls
                            dataParams = dataParams + itemObj.attributeName + "=" + spark.util.uniescape($j('#' + itemObj.editControlID).val()) + "&";
                        }
                        else{
                            //custom controls, handle each one specifically
                            if (itemObj.attributeName == "birthdate"){
                                var month = $j('#' + itemObj.editControlID.monthID).val();
                                var day = $j('#' + itemObj.editControlID.dayID).val();
                                var year = $j('#' + itemObj.editControlID.yearID).val();
                                dataParams = dataParams + itemObj.attributeName + "=" + month + "/" + day + "/" + year + "&";
                            }
                            else if (itemObj.attributeName == "regionid"){
                                var country = $j('#' + itemObj.editControlID.countryID).val();
                                var state = $j('#' + itemObj.editControlID.stateID).val();
                                var city = $j('#' + itemObj.editControlID.cityID).val();
                                var zipcode = $j('#' + itemObj.editControlID.zipcodeID).val();
                                var cityforzipcode = $j('#' + itemObj.editControlID.cityforzipID).val();
                                dataParams = dataParams + "countryRegionID=" + country + "&";
                                dataParams = dataParams + "stateRegionID=" + state + "&";
                                dataParams = dataParams + "cityName=" + city + "&";
                                dataParams = dataParams + "zipCode=" + zipcode + "&";
                                dataParams = dataParams + "cityForZipCode=" + cityforzipcode + "&";
                            }
                            else if (itemObj.attributeName == "desiredminage"){
                                var minAge = $j('#' + itemObj.editControlID.minAgeID).val();
                                var maxAge = $j('#' + itemObj.editControlID.maxAgeID).val();
                                dataParams = dataParams + "minAge=" + minAge + "&maxAge=" + maxAge + "&";
                            }
                        }
                    }
                }
                
                if (dataParams.length > 1){
                    dataParams = dataParams.substring(0, dataParams.length - 1);
                }

                if (dataParams.length > 0  && !this.updateInProgress){
                    var $loading = $j('<div class="loading-message hide"><h2><%=g.GetResource("TXT_SENDING",this) %></h2></div>');

                    $j.ajax({
                        type: "POST",
                        url: "/Applications/API/Member.asmx/UpdateProfileDataSection",
                        data: "{ memberID: " + editProfileManager.memberID
                        + ", dataGroupID: " + sectionObj.groupTypeID
                        + ", dataSectionName: \"" + sectionObj.sectionName + "\""
                        + ", nvProfileDataParams: \"" + dataParams + "\"}",
                        contentType: "application/json; charset=utf-8",
                        beforeSend:function(){
                            $j('#' + editContainerID).find('fieldset').css('visibility','hidden');
                            $loading.appendTo($j('#' + editContainerID)).removeClass('hide');
                            editProfileManager.updateInProgress = true;
                        },
                        complete: function() {
                            //setTimeout("$j('#divHoverSendingMessage').css('display', 'none');", 5000); //make ajax loader stay up for testing
                            editProfileManager.updateInProgress = false;
                            $j('#' + editContainerID).find('.loading-message').remove();
                            $j('#' + editContainerID).find('fieldset').css('visibility','visible');
                        },
                        error: function(jqXHR, textStatus, errorThrown) {
                            if (isVerbose) { alert(textStatus); }
                        },
                        success: function(result) {
                            var EditProfileResult = (typeof result.d == 'undefined') ? result : result.d;
                            var resultStatus = EditProfileResult.Status;
                            if (resultStatus == 3) {
                                //if (isVerbose) { alert(EditProfileResult.StatusMessage, 3); }
                                //hide all validation errors in this section to reset
                                if (sectionObj.errorContainerID != ''){
                                    $j('#' + sectionObj.errorContainerID).hide();
                                }
                                $j('#' + editContainerID).find('.form-error-inline').hide();

                                //error at section level
                                if (sectionObj.errorContainerID != ''){
                                    $j('#' + sectionObj.errorContainerID).show().text(EditProfileResult.StatusMessage);
                                }
                                //error at field level
                                if (EditProfileResult.ValidationErrors){
                                    for (var i = 0; i < EditProfileResult.ValidationErrors.length; i++){
                                        var valObj = EditProfileResult.ValidationErrors[i];
                                        var editItemObj = sectionObj.getEditItem(valObj.AttributeName);
                                        if (editItemObj != null && editItemObj.errorContainerID != ''){
                                            $j('#' + editItemObj.errorContainerID).show().html(valObj.ValidationMessage);
                                        }
                                    }
                                }
                            }
                            else if (resultStatus == 2) {
                                if (sectionObj.editBySection){
                                    //refresh by section
                                    $j('#' + editContainerID).dialog("close");

                                    //refresh view for edited section and dependent sections
                                    for (var i = 0; i < EditProfileResult.SectionHTMLs.length; i++){
                                        var sectionHTMLObj = EditProfileResult.SectionHTMLs[i];
                                        var sectionRefreshObj = editProfileManager.getSection(sectionHTMLObj.SectionName);
                                        $j('#' + sectionRefreshObj.viewContainerID).html($j(sectionHTMLObj.HTML).html());
                                    }
                                    
                                    //animate view for edited section only
                                    //var viewContainerScroll = $j('#' + viewContainerID).scrollTop();
                                    //$j('html').scrollTop(viewContainerScroll);
                                    //console.log(viewContainerScroll);
                                    $j('#' + viewContainerID).addClass('saved').animate({ backgroundColor: "white" }, 1000, null, function(){$j(this).removeClass('saved').css('background-color','')});

                                    //hide all validation errors in this section
                                    if (sectionObj.errorContainerID != ''){
                                        $j('#' + sectionObj.errorContainerID).hide();
                                    }
                                    $j('#' + editContainerID).find('.form-error-inline').hide();

                                    //hide page notification message, if any
                                    $j('#content-main').find('div.PageMessage.notification').first().hide();

                                    //refresh edit item form field for dependent sections
                                    for (var i = 0; i < sectionObj.editItemList.length; i++){
                                        var editItemObj = sectionObj.editItemList[i];
                                        if (editItemObj.hasDuplicate){
                                            for (var j = 0; j < editItemObj.refreshSectionNameList.length; j++){
                                                var sectionRefreshObj = editProfileManager.getSection(editItemObj.refreshSectionNameList[j]);
                                                var editItemRefreshObj = sectionRefreshObj.getEditItem(editItemObj.attributeName);

                                                if (editItemRefreshObj != null){
                                                    if (editItemObj.attributeName == 'gendermask'){
                                                        editItemRefreshObj = sectionRefreshObj.getEditItem('seekinggendermask');
                                                        if (editItemObj.editControlTypeID == 4 && editItemRefreshObj.editControlTypeID == 2){
                                                            var genderMaskValue = $j('#' + editItemObj.editControlID).val();
                                                            if (genderMaskValue == '9' || genderMaskValue == '10'){
                                                                //seeking female
                                                                $j('input:radio[name=' + editItemRefreshObj.editControlID + ']').filter("[value=8]").attr('checked', true);
                                                            }
                                                            else if (genderMaskValue == '5' || genderMaskValue == '6'){
                                                                //seeking male
                                                                $j('input:radio[name=' + editItemRefreshObj.editControlID + ']').filter("[value=4]").attr('checked', true);
                                                            }
                                                        }
                                                    }
                                                    else if (editItemObj.attributeName == 'seekinggendermask'){
                                                        editItemRefreshObj = sectionRefreshObj.getEditItem('gendermask');
                                                        if (editItemObj.editControlTypeID == 2 && editItemRefreshObj.editControlTypeID == 4){
                                                            var seekingGenderMaskValue = $j('input:radio[name=' + editItemObj.editControlID + ']:checked').val();
                                                            var genderMaskValue = $j('#' + editItemRefreshObj.editControlID).val();
                                                            if (seekingGenderMaskValue == '8'){
                                                                //seeking female
                                                                if (genderMaskValue == '6'){
                                                                    $j('#' + editItemRefreshObj.editControlID).val('10');
                                                                }
                                                                else if (genderMaskValue == '5'){
                                                                    $j('#' + editItemRefreshObj.editControlID).val('9');
                                                                }
                                                            }
                                                            else if (seekingGenderMaskValue == '4'){
                                                                //seeking male
                                                                if (genderMaskValue == '9'){
                                                                    $j('#' + editItemRefreshObj.editControlID).val('5');
                                                                }
                                                                else if (genderMaskValue == '10'){
                                                                    $j('#' + editItemRefreshObj.editControlID).val('6');
                                                                }
                                                            }
                                                        }
                                                    }
                                                    else if (editItemRefreshObj.editControlTypeID == 2 && editItemRefreshObj.editControlTypeID == editItemObj.editControlTypeID){
                                                        //radio
                                                        $j('input:radio[name=' + editItemRefreshObj.editControlID + ']').filter('[value=' + $j('input:radio[name=' + editItemObj.editControlID + ']:checked').val() + ']').attr('checked', true);
                                                    }
                                                    else if (editItemRefreshObj.editControlTypeID == 4 && editItemRefreshObj.editControlTypeID == editItemObj.editControlTypeID){
                                                        //dropdown
                                                        $j('#' + editItemRefreshObj.editControlID.EditControlID).val($j('#' + editItemObj.editControlID.EditControlID).val());
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                else{
                                    //refresh by single item
                                    itemObj = sectionObj.getEditItem(attributeName);
                                    if (itemObj != null && itemObj.editDataTypeID == 5){
                                        //refresh inline essay
                                        if (EditProfileResult.ItemHTML.length > 0){
                                            //show no-answer for view essay
                                            $j('#' + itemObj.viewContainerID).toggleClass('hide').find('.essay-value').html(EditProfileResult.ItemHTML);
                                            //hide pending notification
                                            $j('#' + itemObj.viewContainerID).find(".notification").hide().css("visibility", "hidden");
                                        }
                                        else{
                                            //copy edited answer to view essay
                                            $j('#' + itemObj.viewContainerID).toggleClass('hide').find('.essay-value').html(nl2br($j('#' + itemObj.editControlID).val()));
                                            //show pending notification
                                            $j('#' + itemObj.viewContainerID).find(".notification").show().css("visibility", "visible");
                                        }
                                        $j('#' + itemObj.editContainerID).toggleClass('hide');
                                    }
                                    else if (itemObj != null && itemObj.editControlTypeID == 3){
                                        //refresh inline checkbox
                                        $j('#' + itemObj.viewContainerID).toggleClass('hide').find('.essay-value').html(EditProfileResult.ItemHTML);
                                        $j('#' + itemObj.editContainerID).toggleClass('hide');
                                        $j('#' + itemObj.editControlID.HidOriginalValueID).val($j('#' + itemObj.editControlID.HidEditValueID).val());
                                    }
                                    else if (itemObj != null && itemObj.editControlTypeID == 4){
                                        //refresh inline dropdown
                                        $j('#' + itemObj.viewContainerID).toggleClass('hide').find('.essay-value').html(EditProfileResult.ItemHTML);
                                        $j('#' + itemObj.editContainerID).toggleClass('hide');
                                        $j('#' + itemObj.editControlID.HidOriginalValueID).val($j('#' + itemObj.editControlID.EditControlID).val());
                                    }

                                    //add animation after save
                                    $j('#' + itemObj.viewContainerID).addClass('saved').animate({ backgroundColor: "white" }, 1000, null, function(){$j(this).removeClass('saved').css('background-color','')});

                                    //hide all validation errors for this item
                                    if (sectionObj.errorContainerID != ''){
                                        $j('#' + sectionObj.errorContainerID).hide();
                                    }
                                    $j('#' + itemObj.errorContainerID).hide();
                                }

                                //refresh ads
                                editProfileManager.refreshAds();

                                //update omniture
                                if (s != null) {
                                    PopulateS(true); //clear existing values in omniture "s" object

                                    s.events = "event14";
                                    if (sectionObj.editBySection){
                                        s.eVar14 = sectionObj.groupType + "_" + sectionObj.sectionName;
                                    }
                                    else{
                                        s.eVar14 = sectionObj.groupType + "_" + sectionObj.sectionName + "_" + itemObj.attributeName;
                                    }
                                    s.t(); //send omniture updated values as page load
                                }
                            }
                        }
                    });
                }
            }
        }

        //initialize objects
        var editProfileManager = new spark_profile30_EditManager();
        editProfileManager.memberID = <%=MemberProfile.MemberID%>;

    </script>
</asp:PlaceHolder>

<script type="text/javascript">
    if (s != null) {
        spark_profile30_OmnitureCurrentPageName = s.pageName;
    }
</script>

<asp:PlaceHolder ID="phPopupHeader" runat="server" Visible="false">
<%--Header shown on popup only--%>
<div class="clearfix margin-light">
   <div class="nav-profile"> 
    <%--next/prev--%>
        <asp:HyperLink ID="lnkPreviousProfile" runat="server" Visible="false" CssClass="prev">
            <mn2:FrameworkLiteral ID="literalPreviousProfile" runat="server" ResourceConstant="TXT_PREVIOUS_PROFILE"></mn2:FrameworkLiteral>
        </asp:HyperLink>

        <asp:HyperLink ID="lnkNextProfile" runat="server" Visible="false" CssClass="next">
            <mn2:FrameworkLiteral ID="literalNextProfile" runat="server" ResourceConstant="TXT_NEXT_PROFILE"></mn2:FrameworkLiteral>
        </asp:HyperLink>
    </div>

    <%--toolbar--%>
    <mn:ProfileToolBar ID="profileToolBar1" runat="server"></mn:ProfileToolBar>
</div>
</asp:PlaceHolder>

<div id="profileTop" class="clearfix">

    <%--Photo area--%>
    <mn:PhotoSlider ID="PhotoSlider1" runat="server" />
    
    <!--Profile Data-->
    <div id="profile30Important" class="profile30-overview clearfix">
        
        <%--Basic Info--%>
        <mn:BasicInfo ID="basicInfo1" runat="server" />
        
        <%--Timestamps --%>
        <ul class="timestamp">
            <li><uc1:LastLoginDate ID="lastLoginDate" runat="server"></uc1:LastLoginDate></li>
            <li><uc1:LastUpdateDate ID="lastUpdateDate" runat="server"></uc1:LastUpdateDate></li>
        </ul>

        <!--DEALBREAKER INFO-->
        <div class="profile30-details clearfix">
            <uc1:NameValueDataGroup ID="nvDataGroupDealBreakerInfo" runat="server" DisplayMode="TwoColumn" />
        </div>
        
        <!--M2MCommunication-->
        <mn:M2MCommunication ID="m2mCommunication1" runat="server" />
    </div>
    <mn:EditResourceControl ID="editResourceControl1" runat="server" />
</div>

<!--PROFILE TABS-->
<uc1:TabGroup ID="ProfileTabGroup1" runat="server" />

<%--EMAIL ME--%>
<asp:PlaceHolder ID="phEmailMeBottom" runat="server">
    <div class="action-email clearfix">
        <h3><mn2:FrameworkLiteral ID="literalLikeWhatYouSee" runat="server" ResourceConstant="TXT_LIKE_WHAT_YOU_SEE" /></h3>
        <asp:HyperLink ID="lnkEmailMeBottom" runat="server" CssClass="btn link-primary large">
            <mn2:FrameworkLiteral ID="literalEmailMeBottom" runat="server" ResourceConstant="TXT_EMAIL_ME_NOW" />
        </asp:HyperLink>
    </div>
    <asp:PlaceHolder ID="phHideEmail" runat="server" Visible="false">
    <%--Block it if see your own profile--%>
        <script type="text/javascript">
            $j(function () {
                BlockElem($j('#content-main .action-email'));
            });
        </script>
    </asp:PlaceHolder>
</asp:PlaceHolder>

<asp:PlaceHolder ID="phTempQADateChanges" runat="server" Visible="false">
<%--TOMA TEMP CODE FOR QA --%>
<p>
<asp:Literal ID="litTempProfileAlert" runat="server"></asp:Literal>
Last Logon Date: <asp:TextBox ID="txtTempLastLogonDate" runat="server"></asp:TextBox><br />
Last Profile Viewed alert email Date: <asp:TextBox ID="txtTempLastProfileViewedAlertDate" runat="server"></asp:TextBox><br />
<asp:Button 
        ID="btnTempQASubmit" runat="server" Text="Save" 
        onclick="btnTempQASubmit_Click" /></p>
</asp:PlaceHolder>

<!--iframe used by SaveYNMVote function-->
<iframe name="FrameYNMVote" tabindex="-1" frameborder="0" scrolling="no" width="1" height="1">
    <layer name="FrameYNMVote" frameborder="0" scrolling="no" width="1" height="1"></layer>
</iframe>

<asp:PlaceHolder ID="phEditJSAfter" runat="server" Visible="false">
<script type="text/javascript">
   $j('#content-container').bind('click', function(event) {
        var $targetObj = $j(event.target).closest('.prof-edit-region');
        var editType = $targetObj.attr('edittype');

        if(event.type === 'click' && $targetObj){
            //var editData = $targetObj.data('edit-region');
            var editSectionName = $targetObj.attr('editsectionname');
            var sectionObj = editProfileManager.getSection(editSectionName);
            var editObj = sectionObj;
            if (!sectionObj){
	            if(editType === "photoedit"){
                    window.location = "/Applications/MemberProfile/MemberPhotoEdit.aspx";
	            }
                return;
            }
            else if (sectionObj.isMatchPreferences == true){
                window.location = "/Applications/Search/SearchPreferences.aspx";
                return;
            }

            if (!sectionObj.editBySection){
                var itemObj = sectionObj.getEditItem($targetObj.attr('edititemname'));
                editObj = itemObj;
            }

            var targetID = editObj.editContainerID;
            if(editType === "modal"){
                $j("html").scrollTop(0);
                $j('#' + targetID).dialog({
                    width: 488,
		            modal: true,
		            title: editObj.title,
                    position:['center','top'],
                    dialogClass: 'padding-heavy prof-edit-modal-container'
	            });
                $j('.ui-dialog.prof-edit-modal-container').css('margin-top', '12px');
	        }
            else if(editType === "inline"){
                var essayObj = $j('#' + editObj.viewContainerID).find('.essay-value'),
                    essayValue = $j(essayObj.has('.details')) ? essayObj.clone().find('.read-more, .re-collapse').remove().end().clone().find('.details').replaceWith(function(){return this.innerHTML;}).end().html() : essayObj.html(),
                    essayValueText = $j('#' + editObj.viewContainerID).find('.essay-value').text(),
                    $targetID = $j('#' + targetID);

                // js to show/hide essay text with textarea
                $targetObj.toggleClass('hide');
                $targetID.toggleClass('hide').addClass('inline-edit form-set clearfix');
                if (editObj.editDataTypeID == 5){
                    var noEssayAnswerText = $j(editProfileManager.noEssayAnswer).text();
                    if(essayValueText == noEssayAnswerText){
                        essayValue = '';
                    }
                    else{
                        essayValue = br2nl(essayValue);
                    }
                    $targetID.find('textarea').each(function(){
                        $j(this).val(essayValue);

                        /* focus at the end of text */
                        if (this.createTextRange) {
                            var r = this.createTextRange();
                            r.collapse(false);
                            r.select();
                        }
                        //$j(this).focus(); //set focus
                        $j(this).focus().trigger('keydown'); //set focus, trigger nobleCount by simulating keydown
                    });


                }
	        }
        }
    });

    //clear the background color for fade animation
    $j("#content-container").delegate(".prof-edit-region", "mouseenter mouseleave", function(event){
         if(event.type === 'mouseenter'){
            $j(this).css('background-color', '');
        }
    });
    
    //remove the online icon popup js
    $j('a[id*="lnkIMMe"]').removeAttr('href');
    
    //Contains JS representing EditSection objects
    <asp:Literal ID="litEditSectionJS" runat="server"></asp:Literal>

    //Contains JS representing EditItem objects
    <asp:Literal ID="litEditItemJS" runat="server"></asp:Literal>
    
    //Checkbox related
    function EditCheckbox_Click(chkID, hidID, val){
        var hidObj = $j('#' + hidID);
        //alert('before: ' + hidObj.val());
        if ($j("#" + chkID).is(':checked')){
            hidObj.val(parseInt(hidObj.val()) + val);
        }
        else{
            hidObj.val(parseInt(hidObj.val()) - val);
        }
        //alert('after: ' + hidObj.val());
    }

    //Location related
    $j('#' + EditLocationObj.ddlEditCountry).change(function() {
        //alert('hi from region country change');
        EditLocation_CountryChanged("", "");
        return false;
    });


     $j('#' + EditLocationObj.txtEditZipCode).keyup(function() {
        EditLocation_ZipCodeChanged();
        return false;
    });

    $j('#' + EditLocationObj.ddlEditState).change(function() {
        var newStateRegionID = $j('#' + EditLocationObj.ddlEditState).val();

        $j('#' + EditLocationObj.txtEditCity).val('');
        EditLocation_LoadCityAutoComplete(newStateRegionID, "");

        return false;
    });

    $j('#' + EditLocationObj.lnkCityLookup).click(function() {
        try {
            var parentRegionID = $j('#' + EditLocationObj.ddlEditCountry).val();
            if ($j('#' + EditLocationObj.ddlEditState).is(':visible')) {
                parentRegionID = $j('#' + EditLocationObj.ddlEditState).val();
            }
            window.open('/Applications/MemberProfile/CityList.aspx?plid=' + EditLocationObj.BrandID
                + '&CityControlName=' + EditLocationObj.txtEditCity + '&ParentRegionID=' + parentRegionID, '', 'height=415px,width=500px,scrollbars=yes');
        }
        catch (e) {
            if (EditLocationObj.IsVerbose) { alert(e); }
        }
        return false;
    });

    function EditLocation_CountryChanged(preSelectedStateRegionID, preSelectedCityName) {
        var newCountryRegionID = $j('#' + EditLocationObj.ddlEditCountry).val();

        if (newCountryRegionID == "38" || newCountryRegionID == "223") {
            //USA or Canada enters in zip code for profile
            $j('#' + EditLocationObj.txtEditZipCode).val('');
            var ddlCityForZip = $j('#' + EditLocationObj.ddlEditCityForZip);
            ddlCityForZip.empty();
            $j('#' + EditLocationObj.PanelEditByZipCode).show();
            $j('#' + EditLocationObj.pnlEditByCityForZip).hide();
            $j('#' + EditLocationObj.PanelEditByStateCity).hide();
            
        }
        else {
            $j('#' + EditLocationObj.PanelEditByZipCode).hide();
            $j('#' + EditLocationObj.pnlEditByCityForZip).hide();
            $j('#' + EditLocationObj.PanelEditByStateCity).show();
            
            //get new state list, if applicable
            $j.ajax({
                type: "POST",
                url: "/Applications/API/RegionService.asmx/GetRegionStates",
                data: "{countryRegionId:" + newCountryRegionID + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(result) {
                    var regionResult = (typeof result.d == 'undefined') ? result : result.d;
                    if (regionResult.Status == 2) {
                        var divState = $j('#' + EditLocationObj.PanelEditState);
                        var ddlState = $j('#' + EditLocationObj.ddlEditState);
                        ddlState.empty();

                        $j.each(regionResult.StatesList, function(key, value) {
                            $j("<option>").attr("value", key).text(value).appendTo(ddlState);
                        });

                        if (regionResult.hasStates) {
                            divState.show();
                            if (preSelectedStateRegionID != "") {
                                ddlState.val(preSelectedStateRegionID);
                            }
                            EditLocation_LoadCityAutoComplete(ddlState.val(), "");
                        } else {
                            divState.hide();
                            EditLocation_LoadCityAutoComplete(newCountryRegionID, "");
                        }

                        if (preSelectedCityName != "") {
                            $j('#' + EditLocationObj.txtEditCity).val(preSelectedCityName);
                        }
                        else {
                            $j('#' + EditLocationObj.txtEditCity).val('');
                        }

                        //$j('#errorMessage').hide();
                    }
                    else {
                        //$j('#errorMessage').show().html(regionResult.StatusMessage);
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    if (EditLocationObj.IsVerbose) { alert(textStatus); }
                }
            });
        }
    }

       function EditLocation_ZipCodeChanged() {
        var newZipCode = $j('#' + EditLocationObj.txtEditZipCode).val();
        var newCountryRegionID = $j('#' + EditLocationObj.ddlEditCountry).val();
        
        var divCityForZip = $j('#' + EditLocationObj.pnlEditByCityForZip);
     
          //Set cities if Canada postal code is at least 6 or USA and postal code is at least 5
          if ((newCountryRegionID == "38" && newZipCode.length >= 6) || (newCountryRegionID == "223" && newZipCode.length >= 5)) {
                 var zipCodeData = "{zipCode:'" + newZipCode + "'}";
                //get new City for Zip list, if applicable
                $j.ajax({
                    type: "POST",
                    url: "/Applications/API/RegionService.asmx/GetCities",
                    data: zipCodeData,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function(result) {
                      var regionResult = (typeof result.d == 'undefined') ? result : result.d;

                      var ddlCityForZip = $j('#' + EditLocationObj.ddlEditCityForZip);
                      ddlCityForZip.empty();
                      
                     regionResult = $j.parseJSON(regionResult);

                     var i = 0;
                       $j.each(regionResult, function(index, element) {
                            i = i + 1;
                            $j("<option>").attr("value", element.RegionID).text(element.Description).appendTo(ddlCityForZip);
                       });
                   
                      //Display the City for Zip dropdown if more than one value:
                      if (i > 1) {
    //                      ddlCityForZip.prepend("<option value='' selected='selected'></option>");  //Only need this if defaulting to no value
                          divCityForZip.show();
                        }
                        else {
                          divCityForZip.hide();
                        }

                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        if (EditLocationObj.IsVerbose) { alert(textStatus); }
                    }
                });

            } 
            else {
                divCityForZip.hide();
            }
    }

    function EditLocation_LoadCityAutoComplete(parentRegionID, description) {
        //get new city list
        $j.ajax({
            type: "POST",
            url: "/Applications/API/RegionService.asmx/GetRegionCities",
            data: "{parentRegionID:" + parentRegionID + ", description: \"" + description + "\"}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function(result) {
                var regionResult = (typeof result.d == 'undefined') ? result : result.d;
                if (regionResult.Status == 2) {
                    if (regionResult.CityList != null) {
                        $j('#' + EditLocationObj.txtEditCity).autocomplete({
                            source: regionResult.CityList,
                            minLength: 3
                        });

                        $j('.ui-autocomplete').click(function(e) {
                            e.stopPropagation();
                        });
                    }

                    //$j('#errorMessage').hide();
                }
                else {
                    //$j('#errorMessage').show().html(regionResult.StatusMessage);
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                if (EditLocationObj.IsVerbose) { alert(textStatus); }
            }
        });
    }
    
    //prepopulate initial autocomplete for city
    if (EditLocationObj.AccountCountryRegionID != "" && EditLocationObj.AccountCountryRegionID != "38" && EditLocationObj.AccountCountryRegionID != "223"){
        if (EditLocationObj.InitialCityParentRegionID != "") {
            EditLocation_LoadCityAutoComplete(EditLocationObj.InitialCityParentRegionID, "");
        }
    }
    
    // auto resize textarea (minHeight, maxHeight)
    $j('textarea[name$="txtEdit"]').TextAreaExpander(40,600);

    // character counts
    $j("textarea[id$='txtEdit']").each(function (i) {
        var countCopy = '<%=g.GetResource("TXT_COUNT_COPY", this) %>';
        var countContainer = "<div class=\"count-container\"><span class=\"count-profile-essays-counter\"></span> " + countCopy + "</div>";
        $j(this).before(countContainer);

        var countSpan = '.count-profile-essays-counter:eq(' + i + ')';
        $j(this).NobleCount(countSpan, {
            on_negative: 'count-neg',
            on_positive: 'count-pos',
            max_chars: editProfileManager.maxEssayLength,
            block_negative: true
            //,on_update: function(t_obj, char_area, c_settings, char_rem){console.log(char_rem);}
        });
    });

</script>
</asp:PlaceHolder>

<asp:PlaceHolder ID="phKeywordHighlightJS" runat="server" Visible="false">
<script type="text/javascript">
    //keyword highlighting
    var enableKeywordHighlight = true,
        keywordHighlightOptions = {
            exact: "exact",
            style_name_suffix: false,
            highlight: ".highlight-text",
            keys: "<%=_JSKeywordSearch%>"
        };

    $j(function () {
        if (enableKeywordHighlight && keywordHighlightOptions.keys != '') {
            $j("#content-container").SearchHighlight(keywordHighlightOptions);
        }
    });
</script>
</asp:PlaceHolder>
