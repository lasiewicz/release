﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Web.Framework;
using Matchnet.Web.Applications.Registration;

namespace Matchnet.Web.Applications.MemberProfile
{
    public partial class RegistrationWelcomeOnePage : FrameworkControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            g.AnalyticsOmniture.PageName = "Registration complete of One-page-reg w. profile fields";
            g.AnalyticsOmniture.AddEvent("event8");

            if (!string.IsNullOrEmpty(g.Session[RegistrationBase.EVAR43_SESSION_KEY]))
            {
                g.AnalyticsOmniture.Evar43 = g.Session[RegistrationBase.EVAR43_SESSION_KEY];
                g.Session.Remove(RegistrationBase.EVAR43_SESSION_KEY);
            }

            if (!string.IsNullOrEmpty(Request.QueryString["FromRegOverlay"]))
            {
                plcDefault.Visible = false;
                plcFromRegOverlay.Visible = true;
            }
        }
    }
}