﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Util;

namespace Matchnet.Web.Applications.MemberProfile
{
    public class AstrologyHelper
    {
        public static string GetZodiacSign(ContextGlobal _g, DateTime personsDOB, bool returnType)
        {
            int tempYr = Convert.ToInt16(personsDOB.Year.ToString());
            string astroImg = string.Empty;
            string sign = string.Empty;
            DataTable dt;
            
            dt = Option.GetOptions("Zodiac", _g);
            dt.DefaultView.RowFilter = string.Empty;

            // MARTEN:  improve this ugliness later, but for now, make it work
            if (personsDOB >= Convert.ToDateTime(tempYr + "/3/21") && personsDOB <= Convert.ToDateTime(tempYr + "/4/19"))
            {
                astroImg = "Aries";  // aries
                sign = GetSign(dt, astroImg);
            }
            else if (personsDOB >= Convert.ToDateTime(tempYr + "/4/20") && personsDOB <= Convert.ToDateTime(tempYr + "/5/20"))
            {
                astroImg = "Taurus";  // taurus
                sign = GetSign(dt, astroImg);
            }
            else if (personsDOB >= Convert.ToDateTime(tempYr + "/5/21") && personsDOB <= Convert.ToDateTime(tempYr + "/6/21"))
            {
                astroImg = "Gemini";	// gemini
                sign = GetSign(dt, astroImg);
            }
            else if (personsDOB >= Convert.ToDateTime(tempYr + "/6/22") && personsDOB <= Convert.ToDateTime(tempYr + "/7/22"))
            {
                astroImg = "Cancer";	// cancer
                sign = GetSign(dt, astroImg);
            }
            else if (personsDOB >= Convert.ToDateTime(tempYr + "/7/23") && personsDOB <= Convert.ToDateTime(tempYr + "/8/22"))
            {
                astroImg = "Leo";	// leo
                sign = GetSign(dt, astroImg);
            }
            else if (personsDOB >= Convert.ToDateTime(tempYr + "/8/23") && personsDOB <= Convert.ToDateTime(tempYr + "/9/22"))
            {
                astroImg = "Virgo";	// virgo
                sign = GetSign(dt, astroImg);
            }
            else if (personsDOB >= Convert.ToDateTime(tempYr + "/9/23") && personsDOB <= Convert.ToDateTime(tempYr + "/10/22"))
            {
                astroImg = "Libra";	// libra;
                sign = GetSign(dt, astroImg);
            }
            else if (personsDOB >= Convert.ToDateTime(tempYr + "/10/23") && personsDOB <= Convert.ToDateTime(tempYr + "/11/22"))
            {
                astroImg = "Scorpio";	// scorpio
                sign = GetSign(dt, astroImg);
            }
            else if (personsDOB >= Convert.ToDateTime(tempYr + "/11/23") && personsDOB <= Convert.ToDateTime(tempYr + "/12/21"))
            {
                astroImg = "Sagittarius";	// sagittarius
                sign = GetSign(dt, astroImg);
            }
            else if (
                (personsDOB >= Convert.ToDateTime(tempYr + "/12/22") && personsDOB <= Convert.ToDateTime(tempYr + "/12/31"))
                    ||
                (personsDOB >= Convert.ToDateTime(tempYr + "/1/1") && personsDOB <= Convert.ToDateTime(tempYr + "/1/19"))
                )
            {
                astroImg = "Capricorn";	// capricorn
                sign = GetSign(dt, astroImg);
            }
            else if (personsDOB >= Convert.ToDateTime(tempYr + "/1/20") && personsDOB <= Convert.ToDateTime(tempYr + "/2/18"))
            {
                astroImg = "Aquarius";	// aquarius
                sign = GetSign(dt, astroImg);
            }
            else if (personsDOB >= Convert.ToDateTime(tempYr + "/2/19") && personsDOB <= Convert.ToDateTime(tempYr + "/3/20"))
            {
                astroImg = "Pisces";	// pisces
                sign = GetSign(dt, astroImg);
            }

            if (returnType)
                return astroImg;
            else
                return sign;
        }

        private static string GetSign(DataTable dt, string sign)
        {
            string v = string.Empty;
            for (int i = 0; i <= dt.Rows.Count - 1; ++i)
            {
                if (dt.Rows[i][1].ToString() == sign)
                    v = dt.Rows[i][0].ToString();
            }

            return v;
        }
    }
}