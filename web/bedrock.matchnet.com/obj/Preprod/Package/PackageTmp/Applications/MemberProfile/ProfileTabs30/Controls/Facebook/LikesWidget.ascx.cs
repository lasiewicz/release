﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Member.ValueObjects.Interfaces;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Managers;
using System.Text;
using Spark.FacebookLike.ValueObjects;
using Matchnet.Member.ServiceAdapters;

namespace Matchnet.Web.Applications.MemberProfile.ProfileTabs30.Controls.Facebook
{
    public partial class LikesWidget : BaseProfile
    {
        private FacebookManager.FacebookConnectStatus fbConnectStatus = FacebookManager.FacebookConnectStatus.None;

        public int LikesPerRow = 6;
        public bool HasSavedLikes = false;
        public int FacebookConnectStatusID { get; set; }
        public bool IsOwnProfile { get; set; }
        public string TabContentContainerID {get; set;}

        protected override void OnInit(EventArgs e)
        {
            TabContentContainerID = "";
            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        protected override void OnPreRender(EventArgs e)
        {
            if (fbConnectStatus == FacebookManager.FacebookConnectStatus.ConnectedShowData)
            {
                g.AnalyticsOmniture.Evar56 = "Y";
            }
            else
            {
                g.AnalyticsOmniture.Evar56 = "N";
            }

            base.OnPreRender(e);
        }

        public void LoadLikesWidget(IMemberDTO member)
        {
            this._MemberProfile = member;
            this.HasSavedLikes = false;
            this.IsOwnProfile = false;

            fbConnectStatus = FacebookManager.Instance.GetFacebookConnectStatus(member, g.Brand);
            FacebookConnectStatusID = (int)fbConnectStatus;
            
            if (g.Member != null && member.MemberID == g.Member.MemberID)
            {
                //viewing own profile
                this.WhoseProfile = WhoseProfileEnum.Self;
                this.IsOwnProfile = true;
                this.phFBLikeEditScripts.Visible = true;
                this.phWidgetHasData.Visible = true;
                this.phWidgetHasDataEdit.Visible = true;
                this.phWidgetNoFBData.Visible = true;
                this.phWidgetNonConnected.Visible = true;
                this.phWidgetNoSavedData.Visible = true;
                this.phPublishStatus.Visible = true;
                this.radioFBPublishHide.Text = g.GetResource("TXT_HIDE", this);
                this.radioFBPublishShow.Text = g.GetResource("TXT_SHOW", this);
               
                //has saved data
                GenerateSavedLikesJS();
                if (HasSavedLikes)
                {
                    //connect status is out of sync (likely due to member svc caching issue)
                    if (fbConnectStatus != FacebookManager.FacebookConnectStatus.ConnectedShowData)
                    {
                        if (fbConnectStatus != FacebookManager.FacebookConnectStatus.ConnectedHideData)
                        {
                            fbConnectStatus = FacebookManager.FacebookConnectStatus.ConnectedHideData;
                            FacebookConnectStatusID = (int)fbConnectStatus;
                            Member.ServiceAdapters.Member aMember = MemberSA.Instance.GetMember(member.MemberID, MemberLoadFlags.None);
                            aMember.SetAttributeInt(g.Brand, "FacebookConnectStatusID", (int)fbConnectStatus);
                            MemberSA.Instance.SaveMember(aMember);
                        }
                    }

                    if (fbConnectStatus == FacebookManager.FacebookConnectStatus.ConnectedShowData)
                    {
                        fbPublishedStatusContainer.Style.Remove("display");
                        radioFBPublishShow.Checked = true;
                    }
                    else
                    {
                        fbNotPublishedStatusContainer.Style.Remove("display");
                        radioFBPublishHide.Checked = true;
                    }
                }
                else
                {
                    //no saved data
                    if (fbConnectStatus == FacebookManager.FacebookConnectStatus.NonConnected)
                    {
                        //non connected
                        fbNonConnectedPrompt.Style.Remove("display");
                        fbNotPublishedStatusContainer.Style.Remove("display");
                        radioFBPublishHide.Checked = true;
                    }
                    else if (fbConnectStatus == FacebookManager.FacebookConnectStatus.ConnectedNoFBData)
                    {
                        //no fb data
                        fbNoFBData.Style.Remove("display");
                        fbNotPublishedStatusContainer.Style.Remove("display");
                        radioFBPublishHide.Checked = true;
                    }
                    else if (fbConnectStatus == FacebookManager.FacebookConnectStatus.ConnectedNoSavedData)
                    {
                        //no saved data
                        fbNoSavedData.Style.Remove("display");
                        fbNotPublishedStatusContainer.Style.Remove("display");
                        radioFBPublishHide.Checked = true;
                    }
                    else {
                        //connect status is out of sync, switch to no saved data
                        fbConnectStatus = FacebookManager.FacebookConnectStatus.ConnectedNoSavedData;
                        FacebookConnectStatusID = (int)fbConnectStatus;
                        Member.ServiceAdapters.Member aMember = MemberSA.Instance.GetMember(member.MemberID, MemberLoadFlags.None);
                        aMember.SetAttributeInt(g.Brand, "FacebookConnectStatusID", (int)fbConnectStatus);
                        MemberSA.Instance.SaveMember(aMember);
                        fbNoSavedData.Style.Remove("display");
                        fbNotPublishedStatusContainer.Style.Remove("display");
                        radioFBPublishHide.Checked = true;
                    }
                }
            }
            else
            {
                //viewing somebody else's profile
                this.WhoseProfile = WhoseProfileEnum.Other;
                this.phWidgetHasData.Visible = true;

                if (fbConnectStatus == FacebookManager.FacebookConnectStatus.ConnectedShowData)
                {
                    GenerateSavedLikesJS();
                }
            }

            if (FacebookManager.Instance.IsFacebookLikesInterestsDataEnabled(g.Brand))
            {
                if (g.Head20 != null)
                {
                    g.Head20.IncludeFacebookLikesScript = true;
                }
            }
        }

        public string GetJSEncodedResource(string resourceName)
        {
            //escape characters for JS
            string textValue = g.GetResource(resourceName, this);
            textValue = textValue.Replace("\"", "\\\"");
            textValue = textValue.Replace("'", "\\'");
            return textValue;
        }

        private void GenerateSavedLikesJS()
        {
            //Generate JS Facebook Saved Likes
            StringBuilder sbFacebookSavedLikesJS = new StringBuilder();

            FacebookLikeCategoryGroupList fbGroupList = FacebookManager.Instance.GetFacebookLikesCategoryGroupList(g.Brand);
            FacebookLikeList facebookLikesList = FacebookManager.Instance.GetFacebookSavedLikes(_MemberProfile, g.Brand);

            if (fbGroupList != null && fbGroupList.FacebookLikeCategoryGroups != null)
            {
                foreach (FacebookLikeCategoryGroup fbGroup in fbGroupList.FacebookLikeCategoryGroups)
                {
                    //category group
                    string varFbLikeCategoryGroupObj = "fbLikeCategoryGroupObj" + fbGroup.FacebookLikeCategoryGroupId.ToString();
                    sbFacebookSavedLikesJS.AppendLine("var " + varFbLikeCategoryGroupObj + " = new FBLikeCategoryGroupObj();");
                    sbFacebookSavedLikesJS.AppendLine(varFbLikeCategoryGroupObj + ".categoryGroupID = " + fbGroup.FacebookLikeCategoryGroupId.ToString() + ";");
                    //sbFacebookSavedLikesJS.AppendLine(varFbLikeCategoryGroupObj + ".categoryGroupName = \"" + fbGroup.FacebookCategoryGroupName + "\";");
                    sbFacebookSavedLikesJS.AppendLine(varFbLikeCategoryGroupObj + ".categoryGroupName = \"" + g.GetResource(fbGroup.FacebookCategoryGroupNameResourceKey, this) + "\";");
                    sbFacebookSavedLikesJS.AppendLine(varFbLikeCategoryGroupObj + ".containerID = \"fbLikesGroup" + fbGroup.FacebookLikeCategoryGroupId.ToString() + "\";");
                    sbFacebookSavedLikesJS.AppendLine(varFbLikeCategoryGroupObj + ".containerEditID = \"fbLikesGroupEdit" + fbGroup.FacebookLikeCategoryGroupId.ToString() + "\";");
                    
                    //categories
                    if (this.WhoseProfile == WhoseProfileEnum.Self)
                    {
                        if (fbGroup.FacebookLikeCategories != null)
                        {
                            foreach (FacebookLikeCategory fbCategory in fbGroup.FacebookLikeCategories)
                            {
                                //categories within a group
                                string varFbLikeCategoryObj = "fbLikeCategoryObj" + fbCategory.FacebookLikeCategoryId.ToString();
                                sbFacebookSavedLikesJS.AppendLine("var " + varFbLikeCategoryObj + " = new FBLikeCategoryObj();");
                                sbFacebookSavedLikesJS.AppendLine(varFbLikeCategoryObj + ".categoryID = " + fbCategory.FacebookLikeCategoryId.ToString() + ";");
                                sbFacebookSavedLikesJS.AppendLine(varFbLikeCategoryObj + ".categoryName = \"" + fbCategory.FacebookLikeCategoryName + "\";");
                                sbFacebookSavedLikesJS.AppendLine("spark.util.addItem(" + varFbLikeCategoryGroupObj + ".fbLikeCategoryObjList, " + varFbLikeCategoryObj + ");");
                            }
                        }
                    }

                    //saved likes
                    if (facebookLikesList != null && facebookLikesList.FacebookLikesGroupDictionary.ContainsKey(fbGroup.FacebookLikeCategoryGroupId))
                    {
                        List<FacebookLike> facebookLikes = facebookLikesList.FacebookLikesGroupDictionary[fbGroup.FacebookLikeCategoryGroupId];

                        if (facebookLikes.Count > 0)
                        {
                            //var fbLikeObj = new FBLikeObj();
                            //fbLikeObj.id = obj.id;
                            //fbLikeObj.name = obj.name;
                            //fbLikeObj.category = obj.category;
                            //fbLikeObj.link = obj.link;
                            //fbLikeObj.picture = obj.picture ? obj.picture :  'http://profile.ak.fbcdn.net/static-ak/rsrc.php/v1/yr/r/fwJFrO5KjAQ.png';
                            //fbLikeObj.type = obj.type;
                            //fbLikeObj.chkEditID = 'chk' + obj.id;
                            //fbLikeObj.categoryGroupID
                            //fbLikeObj.categoryID
                            //fbLikeObj.selected
                            //fbLikeObj.saved
                            //fbLikeObj.approved
                            //fbLikeManager.addFBLike(fbLikeObj);

                            int likesCounter = 0;
                            int renderedLikesCounter = 0;
                            foreach (FacebookLike fbLike in facebookLikes)
                            {
                                if (this.WhoseProfile == WhoseProfileEnum.Self || fbLike.FacebookLikeStatus == Spark.FacebookLike.ValueObjects.ServiceDefinitions.FacebookLikeStatus.Approved)
                                {
                                    HasSavedLikes = true;
                                    likesCounter++;

                                    bool renderLike = true;
                                    if (this.WhoseProfile == WhoseProfileEnum.Other && renderedLikesCounter >= LikesPerRow)
                                    {
                                        renderLike = false;
                                    }

                                    if (renderLike)
                                    {
                                        renderedLikesCounter++;
                                        string varFBLikeObj = "fbLikeObj" + fbLike.FacebookId.ToString();
                                        sbFacebookSavedLikesJS.AppendLine("var " + varFBLikeObj + " = new FBLikeObj();");
                                        sbFacebookSavedLikesJS.AppendLine(varFBLikeObj + ".id = " + fbLike.FacebookId.ToString() + ";");
                                        sbFacebookSavedLikesJS.AppendLine(varFBLikeObj + ".name = \"" + fbLike.FacebookName + "\";");
                                        //sbFacebookSavedLikesJS.AppendLine(varFBLikeObj + ".category = \"" + fbLike.FacebookName + "\";");
                                        sbFacebookSavedLikesJS.AppendLine(varFBLikeObj + ".link = \"" + fbLike.FacebookLinkHref + "\";");
                                        sbFacebookSavedLikesJS.AppendLine(varFBLikeObj + ".picture = \"" + fbLike.FacebookImageUrl + "\";");
                                        sbFacebookSavedLikesJS.AppendLine(varFBLikeObj + ".type = \"" + fbLike.FacebookType + "\";");
                                        sbFacebookSavedLikesJS.AppendLine(varFBLikeObj + ".chkEditID = \"chk" + fbLike.FacebookId.ToString() + "\";");
                                        sbFacebookSavedLikesJS.AppendLine(varFBLikeObj + ".categoryID = " + fbLike.FacebookLikeCategoryId.ToString() + ";");
                                        sbFacebookSavedLikesJS.AppendLine(varFBLikeObj + ".categoryGroupID = " + fbLike.FacebookLikeCategoryGroupId.ToString() + ";");
                                        sbFacebookSavedLikesJS.AppendLine(varFBLikeObj + ".selected = true;");
                                        sbFacebookSavedLikesJS.AppendLine(varFBLikeObj + ".saved = true;");
                                        if (fbLike.FacebookLikeStatus == Spark.FacebookLike.ValueObjects.ServiceDefinitions.FacebookLikeStatus.Approved)
                                        {
                                            sbFacebookSavedLikesJS.AppendLine(varFBLikeObj + ".approved = true;");
                                        }
                                        //sbFacebookSavedLikesJS.AppendLine("fbLikeManager.addFBLike(" + varFBLikeObj + ");");
                                        sbFacebookSavedLikesJS.AppendLine("spark.util.addItem(" + varFbLikeCategoryGroupObj + ".fbLikeObjList, " + varFBLikeObj + ");");
                                    }
                                }
                            }

                            if (this.WhoseProfile == WhoseProfileEnum.Self)
                            {
                                sbFacebookSavedLikesJS.AppendLine(varFbLikeCategoryGroupObj + ".fbLikeInitialCountAll = " + facebookLikes.Count.ToString() + ";");
                            }
                            else
                            {
                                sbFacebookSavedLikesJS.AppendLine(varFbLikeCategoryGroupObj + ".fbLikeInitialCountAll = " + likesCounter.ToString() + ";");
                                if (likesCounter > renderedLikesCounter)
                                {
                                    sbFacebookSavedLikesJS.AppendLine(varFbLikeCategoryGroupObj + ".fbLikeFullyLoaded = false;");
                                }
                            }
                        }
                    }

                    sbFacebookSavedLikesJS.AppendLine("spark.util.addItem(fbLikeManager.fbLikeCategoryGroupObjList, " + varFbLikeCategoryGroupObj + ");");
                }
            }

            if (this.WhoseProfile == WhoseProfileEnum.Self || this.HasSavedLikes)
            {
                litFBLikesData.Text = sbFacebookSavedLikesJS.ToString();
            }
        }

    }
}
