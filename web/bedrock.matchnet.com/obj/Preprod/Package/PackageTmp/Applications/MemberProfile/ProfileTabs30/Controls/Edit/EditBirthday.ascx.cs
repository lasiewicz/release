﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Matchnet.Web.Applications.MemberProfile.ProfileTabs30.Controls.Edit
{
    public partial class EditBirthday : BaseEdit
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        #region Public Methods
        public override void LoadEditControl(XMLProfileData.ProfileDataGroup ProfileDataGroup, XMLProfileData.ProfileDataSection ProfileDataSection, XMLProfileData.ProfileDataItem ProfileDataItem, string JavascriptSectionVarName, System.Web.UI.Control ResourceControl)
        {
            _ProfileDataGroup = ProfileDataGroup;
            _ProfileDataSection = ProfileDataSection;
            _ProfileDataItem = ProfileDataItem;
            _JavascriptSectionVarName = JavascriptSectionVarName;
            _ResourceControl = ResourceControl;

            //populate label
            litEdit.Text = g.GetResource(String.IsNullOrEmpty(ProfileDataItem.EditResourceConstant) ? ProfileDataItem.ResourceConstant : ProfileDataItem.EditResourceConstant, ResourceControl);

            //required indicator
            if (_ProfileDataItem.IsRequired)
            {
                litRequired.Text = g.GetResource("TXT_REQUIRED", _InternalEditResourceControl);
            }

            //get current birthday
            DateTime birthdate = g.Member.GetAttributeDate(g.Brand, "Birthdate");

            //load month
            ddlEditBirthdayMonth.Items.Clear();
            for (int i = 1; i <= 12; i++)
            {
                ddlEditBirthdayMonth.Items.Add(new ListItem(g.GetResource("MONTHABBR" + i.ToString()), i.ToString()));
            }
            ddlEditBirthdayMonth.SelectedValue = birthdate.Month.ToString();

            //load day
            ddlEditBirthdayDay.Items.Clear();
            for (int i = 1; i <= 31; i++)
            {
                ddlEditBirthdayDay.Items.Add(new ListItem(i.ToString(), i.ToString()));
            }
            ddlEditBirthdayDay.SelectedValue = birthdate.Day.ToString();

            //load year
            txtEditBirthdayYear.Text = birthdate.Year.ToString();

            //render JS Edit Item object to contain info on this edit control
            AddEditItemJS("{monthID: '" + ddlEditBirthdayMonth.ClientID + "',dayID:'" + ddlEditBirthdayDay.ClientID + "',yearID:'" + txtEditBirthdayYear.ClientID + "'}", true, itemErrorContainer.ClientID);

        }

        #endregion
    }
}