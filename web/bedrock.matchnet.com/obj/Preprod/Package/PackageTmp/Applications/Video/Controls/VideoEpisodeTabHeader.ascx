﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="VideoEpisodeTabHeader.ascx.cs" Inherits="Matchnet.Web.Applications.Video.Controls.VideoEpisodeTabHeader" %>
<%@ Register TagPrefix="vid" Tagname="Tab" Src="/Applications/Video/Controls/VideoEpisodeTab.ascx" %>
<div id="video-tabs">
        <ul class="nav-rounded-tabs clearfix">
        <asp:Repeater runat="server" ID="rptTabs" OnItemDataBound="rptTabs_ItemDataBound">
        <ItemTemplate>
        <li class="tab" runat="server" id="liTab"><vid:Tab runat="server" id="tab" TitleResourceConstant='<%# DataBinder.Eval(Container.DataItem, "TitleResourceConstant") %>' DescriptionResourceConstant='<%# DataBinder.Eval(Container.DataItem, "DescriptionResourceConstant") %>' SubTitleResourceConstant='<%# DataBinder.Eval(Container.DataItem, "SubTitleResourceConstant") %>' Link='<%# DataBinder.Eval(Container.DataItem, "Link") %>' > </vid:Tab></li>
        </ItemTemplate>
        </asp:Repeater>
        </ul>
</div>