﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="VideoCommercialCenter.ascx.cs" Inherits="Matchnet.Web.Applications.Video.VideoCommercialCenter" %>
<%@ Register TagPrefix="vid" Tagname="YouTubePlayer" Src="/Applications/Video/Controls/YouTubePlayer.ascx" %>
<%@ Register TagPrefix="vid" Tagname="OoyalaPlayer" Src="/Applications/Video/Controls/OoyalaPlayer.ascx" %>
<%@ Register TagPrefix="vid" Tagname="Playlist" Src="/Applications/Video/Controls/VideoPlaylist.ascx" %>
<%@ Register TagPrefix="vid" Tagname="MoreVideos" Src="/Applications/Video/Controls/MoreVideos.ascx" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mnfe" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn1" TagName="ActivityCenter" Src="/Framework/Ui/ActivityCenter.ascx" %>
<%@ Register TagPrefix="mn" TagName="AdUnit" Src="/Framework/UI/Advertising/AdUnit.ascx" %> 

<script id="playNext" type="text/javascript">
//get wired into the form's onsubmit handler so we can can cancel submission
//if the user clicks to play the video that's already playing. 
document.getElementById("<%=Page.Form.ClientID %>").onsubmit = checkAlreadyPlaying;
var clickedOnVideo;

function loadNextPlaylistVideo() 
{

    //if we're playing videos from the playlist, do a postback to play the next one. 
    var IsPlaylist = document.getElementById('<%=hdnIsPlayListModee.ClientID %>').value;
    if(IsPlaylist == "True")
    {
        __doPostBack("<%=GeneratePostbackControlTarget() %>", "");
    }
}

function checkAlreadyPlaying()
{
    //this function is defined in YouTubePlayer.ascx. Do this check first because if a 
    //video just stopped playing we should always process the submit. 
    if(isPlayerFinished())
    {
        return true;
    }
    
    //if the clickedo on video is the same as the video playing, return false to cancel the form submission
    var selectedVideo = document.getElementById('<%=hdnSelectedVideo.ClientID %>').value;
    if(clickedOnVideo != selectedVideo)
    {
        return true;
    }
    else
    {
        return false;
    }
}

function setClickedVideo(videoID)
{
    clickedOnVideo = videoID;
}

</script>

<div runat="server" id="videoCommercialBanner" style="float:right">
<a href="/Applications/Registration/Registration.aspx"><mn:Image runat="server" FileName="Promo/video-JDCommercial-Reg-Banner.jpg" /></a>
</div>

<mn:Title runat="server" id="ttlVideo" ResourceConstant="NAV_VIDEO"/>
<mnfe:FrameworkLiteral runat="server" ResourceConstant="TXT_CREDITS" ID="litCredits" />
<div class="videoPanel">
    <div id="video-background">
        <div class="video-player">
            <vid:YouTubePlayer id="youTubePlayer" runat="server" Visible="false"></vid:YouTubePlayer>
            <vid:OoyalaPlayer id="ooyalaPlayer" runat="server" Visible="false"></vid:OoyalaPlayer>
        </div>
    </div>
</div>
<div class="featuredArticles" runat="server" id="divfeaturedArticles"  style="display:none">
    <div class="categoryHeader">featured articles</div>
    <div class="articleTeaserContainer">
	    <div class="articleTeaser">
            <mn:txt id="txtArticle1" resourceconstant="TXT_FEATURED_ARTICLE1" expandImageTokens="True" runat="server" />
	    </div>
	    <div class="articleTeaser">
		    <mn:txt id="txtArticle2" resourceconstant="TXT_FEATURED_ARTICLE2" expandImageTokens="True" runat="server" />
	    </div>
	    <div class="articleTeaser">
            <mn:txt id="txtArticle3" resourceconstant="TXT_FEATURED_ARTICLE3" expandImageTokens="True" runat="server" />
	    </div>
	    <div class="articleTeaser" style="border-bottom: 0px solid transparent;">
            <mn:txt id="txtArticle4" resourceconstant="TXT_FEATURED_ARTICLE4" expandImageTokens="True" runat="server" />
	    </div>
    </div>
</div>
<div class="playList" runat="server" id="divPlayList" style="display:none">
    <vid:Playlist id="playlist" runat="server"></vid:Playlist>
</div>
<div class="wrapper">
    <div id="content-main-video">
        <div class="featured-articles" runat="server">
        <div class="video-preview" style="border-right: 1px dotted #333333;">
                <mn:txt id="txt1" resourceconstant="TXT_FEATURED_ARTICLE1" expandImageTokens="True" runat="server" />
	    </div>
	    <div class="video-preview">    
	        <mn:txt id="txt2" resourceconstant="TXT_FEATURED_ARTICLE2" expandImageTokens="True" runat="server" />
	    </div>

        <mn:txt id="txt3" resourceconstant="TXT_FEATURED_ARTICLE3" expandImageTokens="True" runat="server" />
        <mn:txt id="txt4" resourceconstant="TXT_FEATURED_ARTICLE4" expandImageTokens="True" runat="server" />

        </div>   
    </div>
</div>
    <div id="content-promo-video" class="clearfix">
    <mn1:ActivityCenter runat="server" id="ActivityCenter1" />
    <mn:AdUnit id="adSquare" Size="Square" runat="server" GAMAdSlot="right_300x250" GAMPageMode="Auto" />
</div>

<input type="hidden" id="hdnIsPlayListModee" runat="server"/>
<input type="hidden" id="hdnSelectedVideo" runat="server" />
