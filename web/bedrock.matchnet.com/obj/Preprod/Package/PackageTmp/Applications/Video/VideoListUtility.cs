﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml;
using System.Xml.Linq;
using System.Collections.Generic;
using Matchnet.Web.Framework;
using Matchnet.Member.ServiceAdapters;


namespace Matchnet.Web.Applications.Video
{
    public enum VideoViewingType
    {
        Playlist,
        MoreVideos
    }

    public enum VideoPlayerType
    {
        YouTube,
        Ooyala
    }

    /// <summary>
    /// Utility class for reading in one of two community-indexed xml files with a list of videos (based on the viewing type),
    /// with various helper methods to select particular videos from the list by specified attributes
    /// </summary>
    public class VideoListUtility
    {
        #region Private Properties
        private int _communityID;
        private XElement _videoList;
        private VideoViewingType _viewingType;
        private VideoPlayerType _playerType;
        private Dictionary<VideoPlayerType, string> _playerTypeToAttributeMapping;
        #endregion

        #region Public Methods
        /// <summary>
        /// Reads in the xml data based on the supplied VideoViewingType
        /// </summary>
        /// <param name="viewingType"></param>
        public VideoListUtility(VideoViewingType viewingType, VideoPlayerType playerType)
        {
            ContextGlobal g = System.Web.HttpContext.Current.Items["g"] as ContextGlobal;
            _communityID = g.Brand.Site.Community.CommunityID;
            _viewingType = viewingType;
            _playerType = playerType;

            string xmlPath;
            string videoListCacheKey;

            if (viewingType == VideoViewingType.Playlist)
            {
                xmlPath = System.Web.HttpContext.Current.Server.MapPath("/Applications/Video/XML/PlaylistCommunity_" + _communityID + ".xml");
                videoListCacheKey = "XMLCommunityPlaylist" + _communityID.ToString();
            }
            else
            {
                xmlPath = System.Web.HttpContext.Current.Server.MapPath("/Applications/Video/XML/MoreVideosCommunity_" + _communityID + ".xml");
                videoListCacheKey = "XMLCommunityMoreVideos" + _communityID.ToString();
            }

            if (g.Cache[videoListCacheKey] == null)
            {
                //if not in cache, read the xml string in from the selected file
                _videoList = XElement.Load(xmlPath);

                //cache the xml
                //System.Web.Caching.CacheDependency cacheFileDependencies = new System.Web.Caching.CacheDependency(new string[] { xmlPath });
                g.Cache.Insert(videoListCacheKey, _videoList.ToString());
            }
            else
            {
                //read xml directly in from the cache
                _videoList = XElement.Parse(g.Cache[videoListCacheKey].ToString());
            }

            _playerTypeToAttributeMapping = new Dictionary<VideoPlayerType, string>();
            _playerTypeToAttributeMapping.Add(VideoPlayerType.YouTube, "YouTubeID");
            _playerTypeToAttributeMapping.Add(VideoPlayerType.Ooyala, "OoyalaID");

        }

        /// <summary>
        /// Gets all videos of the type specified in the constructor
        /// </summary>
        /// <returns></returns>
        public List<VideoSummary> GetVideos()
        {
            List<VideoSummary> thumbnails = new List<VideoSummary>();
            VideoSummary summary = null;

            foreach (XElement element in _videoList.Elements("video"))
            {
                summary = new VideoSummary();
                summary.ID = element.Attribute(_playerTypeToAttributeMapping[_playerType]).Value;
                summary.TitleResourceConstant = element.Attribute("TitleResourceConstant").Value;
                summary.DescriptionResourceConstant = element.Attribute("DescriptionResourceConstant").Value;
                summary.ImagePath = element.Attribute("ImagePath").Value;
                if (element.Attribute("Order") != null)
                {
                    summary.PlaylistOrder = Convert.ToInt32(element.Attribute("Order").Value);
                }
                thumbnails.Add(summary);
            }

            return thumbnails;
        }

        /// <summary>
        /// Returns the Id of the video marked with the default attribute
        /// </summary>
        /// <returns></returns>
        public string GetDefaultVideoID()
        {
            string ID = string.Empty;

            if (this._viewingType != VideoViewingType.Playlist)
            {
                return ID;
            }

            var defaultVideo = (from el in _videoList.Elements("video") where el.Attribute("Default").Value == "true" select el).First();
            if (defaultVideo != null)
            {
                ID = defaultVideo.Attribute(_playerTypeToAttributeMapping[_playerType]).Value;
            }
            return ID;
        }

        /// <summary>
        /// Returns a VideoSummary that represents the video with the supplied videoID
        /// </summary>
        /// <param name="videoID"></param>
        /// <returns></returns>
        public VideoSummary GetSummaryByID(string videoID)
        {
            VideoSummary summary = null;
            var videoElement = (from el in _videoList.Elements("video") where el.Attribute(_playerTypeToAttributeMapping[_playerType]).Value == videoID select el).First();

            if (videoElement != null)
            {
                summary = new VideoSummary();
                summary.ID = videoElement.Attribute(_playerTypeToAttributeMapping[_playerType]).Value;
                summary.TitleResourceConstant = videoElement.Attribute("TitleResourceConstant").Value;
                summary.DescriptionResourceConstant = videoElement.Attribute("DescriptionResourceConstant").Value;
                summary.ImagePath = videoElement.Attribute("ImagePath").Value;
                if (videoElement.Attribute("Order") != null)
                {
                    summary.PlaylistOrder = Convert.ToInt32(videoElement.Attribute("Order").Value);
                }
            }

            return summary;
        }

        /// <summary>
        /// Returns a VideoSummary that represents the video with the supplied OrderID
        /// </summary>
        /// <param name="order"></param>
        /// <returns></returns>
        public VideoSummary GetSummaryByOrderID(string order)
        {
            VideoSummary summary = null;
            var videoElement = (from el in _videoList.Elements("video") where el.Attribute("Order").Value == order select el).First();
            if (videoElement != null)
            {
                summary = new VideoSummary();
                summary.ID = videoElement.Attribute(_playerTypeToAttributeMapping[_playerType]).Value;
                summary.TitleResourceConstant = videoElement.Attribute("TitleResourceConstant").Value;
                summary.DescriptionResourceConstant = videoElement.Attribute("DescriptionResourceConstant").Value;
                summary.ImagePath = videoElement.Attribute("ImagePath").Value;
                if (videoElement.Attribute("Order") != null)
                {
                    summary.PlaylistOrder = Convert.ToInt32(videoElement.Attribute("Order").Value);
                }
            }

            return summary;
        }

        /// <summary>
        /// Returns the next video in the Playlist, based on the supplied current position. 
        /// If the current position is the end of the list, starts back at the beginning. 
        /// </summary>
        /// <param name="currentOrder"></param>
        /// <returns></returns>
        public VideoSummary GetNextVideoInPlaylist(int currentOrder)
        {
            VideoSummary summary = null;

            //only playlist videos have a 'next' concept
            if (this._viewingType != VideoViewingType.Playlist)
            {
                return summary;
            }

            int nextVideoOrder = currentOrder + 1;
            int numberVideosInPlaylist = _videoList.Elements().Max(c => int.Parse(c.Attribute("Order").Value));

            //if we've reached the end of the playlist, return 1 to start it over
            if (nextVideoOrder > numberVideosInPlaylist)
            {
                nextVideoOrder = 1;
            }

            return GetSummaryByOrderID(nextVideoOrder.ToString());
        }
        #endregion
    }
}
