﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace Matchnet.Web.Applications.Video
{
    /// <summary>
    /// Simple data container that encapsulates the attibutes of a video read in from one of the
    /// XML files. 
    /// </summary>
    public class VideoSummary
    {
        public string ID { get; set; }
        public string TitleResourceConstant { get; set; }
        public string DescriptionResourceConstant { get; set; }
        public string ImagePath { get; set; }
        public int PlaylistOrder { get; set; }
    }
}
