﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Globalization;
using Matchnet.Web.Framework.Ui;
using Matchnet.Configuration.ServiceAdapters;

namespace Matchnet.Web.Applications.Video
{
    public partial class VideoEpisodeCenter20 : FrameworkControl
    {
        private const string SELECTED_TAB_REQUEST_STRING = "etid";
        private const string LINK = "../VideoEpisodeCenter.aspx?" + SELECTED_TAB_REQUEST_STRING + "=";
        private const string SETTING_VIDEO_URL_PARAMS = "VIDEO_CHANNEL_PARAMS";
                
        protected void Page_Load(object sender, EventArgs e)
        {
            int selectedTabIndex = 1;
            if (Request.QueryString[SELECTED_TAB_REQUEST_STRING] != null)
            {
                selectedTabIndex = Convert.ToInt32(Request.QueryString[SELECTED_TAB_REQUEST_STRING]);
            }
            
            string videoID1 = RuntimeSettings.GetSetting("JDATING_SEASON1_VIDEOCHANNEL1");
            string videoID2 = RuntimeSettings.GetSetting("JDATING_SEASON1_VIDEOCHANNEL2");
            string videoID3 = RuntimeSettings.GetSetting("JDATING_SEASON1_VIDEOCHANNEL3");
            string videoID4 = RuntimeSettings.GetSetting("JDATING_SEASON1_VIDEOCHANNEL4");
            string videoID5 = RuntimeSettings.GetSetting("JDATING_SEASON1_VIDEOCHANNEL5");
            string videoID6 = RuntimeSettings.GetSetting("JDATING_SEASON1_VIDEOCHANNEL6");

            headerTabs.Tabs.Add(new EpisodeTab("WEEK1_TITLE", "WEEK1_SUBTITLE", "WEEK1_DESCRIPTION", LINK + "1", videoID1));
            headerTabs.Tabs.Add(new EpisodeTab("WEEK2_TITLE", "WEEK2_SUBTITLE", "WEEK2_DESCRIPTION", LINK + "2", videoID2));
            headerTabs.Tabs.Add(new EpisodeTab("WEEK3_TITLE", "WEEK3_SUBTITLE", "WEEK3_DESCRIPTION", LINK + "3", videoID3));
            headerTabs.Tabs.Add(new EpisodeTab("WEEK4_TITLE", "WEEK4_SUBTITLE", "WEEK4_DESCRIPTION", LINK + "4", videoID4));
            headerTabs.Tabs.Add(new EpisodeTab("WEEK5_TITLE", "WEEK5_SUBTITLE", "WEEK5_DESCRIPTION", LINK + "5", videoID5));
            headerTabs.Tabs.Add(new EpisodeTab("WEEK6_TITLE", "WEEK6_SUBTITLE", "WEEK6_DESCRIPTION", LINK + "6", videoID6));
            headerTabs.SelectedTabIndex = selectedTabIndex;

            ooyalaPlayer.PlayerMode = "channel";
            ooyalaPlayer.VideoID = headerTabs.Tabs[selectedTabIndex - 1].VideoID;
            ooyalaPlayer.VideoParams = RuntimeSettings.GetSetting(SETTING_VIDEO_URL_PARAMS, g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID);

            //update omniture with week number
            g.AnalyticsOmniture.PageName = g.AnalyticsOmniture.PageName + " Week " + selectedTabIndex.ToString();
        }
    }
}
