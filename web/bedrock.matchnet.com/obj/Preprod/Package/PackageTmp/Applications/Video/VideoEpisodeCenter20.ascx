﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="VideoEpisodeCenter20.ascx.cs" Inherits="Matchnet.Web.Applications.Video.VideoEpisodeCenter20" %>
<%@ Register TagPrefix="vid" Tagname="EpisodeTabs" Src="/Applications/Video/Controls/VideoEpisodeTabHeader.ascx" %>
<%@ Register TagPrefix="vid" Tagname="OoyalaPlayer" Src="/Applications/Video/Controls/OoyalaPlayer.ascx" %>  
<%@ Register TagPrefix="mn" TagName="AdUnit" Src="/Framework/UI/Advertising/AdUnit.ascx" %> 
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn1" TagName="ActivityCenter" Src="/Framework/Ui/ActivityCenter.ascx" %>

<h1>JDate Original Videos</h1>

<div id="video-container" class="clearfix">
    <vid:EpisodeTabs runat="server" id="headerTabs"></vid:EpisodeTabs>
    <div id="video-background">
        <div class="video-player">
            <vid:OoyalaPlayer id="ooyalaPlayer" runat="server" ></vid:OoyalaPlayer>
        </div>
    </div>
</div>

<ul id="video-navlist">
    <li><span class="right-arrow">&#9658;</span><a href="/Applications/Search/SearchResults.aspx" target="_blank">Search for a Member</a></li>
    <li><span class="right-arrow">&#9658;</span><a href="/Applications/MembersOnline/MembersOnline.aspx" target="_blank">IM Another Subscriber</a></li>
    <li><span class="right-arrow">&#9658;</span><a href="/Applications/Chat/Default.aspx" target="_blank">Chat with Other Subscribers</a></li>
</ul>

<div class="wrapper">
    <div id="content-main-video">
        <mn:txt id="txtWeeklyContent" runat="server" ResourceConstant="VIDEO_WEEKLY_CONTENT" ExpandImageTokens="true" />
        <!--ActivityCenter-->
    </div>
</div>

<div id="content-promo-video" class="clearfix">
    <mn1:ActivityCenter runat="server" id="ActivityCenter1" />
    <mn:AdUnit id="adSquare" Size="Square" runat="server" GAMAdSlot="right_300x250" GAMPageMode="Auto" />
</div>

<script type="text/javascript">
    jQuery('#video-tabs ul.nav-rounded-tabs li.tab div').hide();
    jQuery('#video-tabs ul.nav-rounded-tabs li.tab.selected div').show();
    jQuery('#video-tabs ul.nav-rounded-tabs li.tab.selected div p').show();
</script>
