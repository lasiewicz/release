﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Matchnet.Web.Framework;

namespace Matchnet.Web.Applications.Video.Controls
{
    /// <summary>
    /// This control simply contains the YouTube player
    /// </summary>
    public partial class YouTubePlayer : FrameworkControl
    {
        #region Public properties
        protected string VideoID { get; set; }
        #endregion

        #region Event Handlers
        /// <summary>
        /// Registers the include file needed to get the YouTube player to work correctly. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Page.ClientScript.RegisterClientScriptInclude("swfobject", "/javascript/swfobject.js");
        }

        /// <summary>
        /// Uses a VideoSummary to set the necessary properties
        /// </summary>
        /// <param name="summary"></param>
        public void SetPlayerProperties(VideoSummary summary)
        {
            VideoID = summary.ID;
            literalTitle.ResourceConstant = summary.TitleResourceConstant;
            literalDescription.ResourceConstant = summary.DescriptionResourceConstant;
        }

        override protected void OnInit(EventArgs e)
        {
            this.Load += new System.EventHandler(this.Page_Load);
            base.OnInit(e);
        }
        #endregion
    }
}