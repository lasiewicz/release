﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Matchnet.Web.Applications.Video
{
    public class EpisodeTab
    {
        public string TitleResourceConstant { get; set; }
        public string SubTitleResourceConstant { get; set; }
        public string DescriptionResourceConstant { get; set; }
        public string Link { get; set; }
        public string VideoID { get; set; }

        public EpisodeTab(string titleResourceConstant, string subTitleResourceConstant, string descriptionResourceConstant, string link, string videoID)
        {
            TitleResourceConstant = titleResourceConstant;
            SubTitleResourceConstant = subTitleResourceConstant;
            DescriptionResourceConstant = descriptionResourceConstant;
            Link = link;
            VideoID = videoID;
        }
    }
}
