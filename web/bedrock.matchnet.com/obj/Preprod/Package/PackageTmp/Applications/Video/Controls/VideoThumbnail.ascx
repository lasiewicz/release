﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="VideoThumbnail.ascx.cs" Inherits="Matchnet.Web.Applications.Video.Controls.VideoThumbnail" EnableViewState="false"%>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<div class="playListTeaser">
    <span class="playListTeaserPhoto"><asp:ImageButton ID="imgThumbnail" 
        runat="server" CommandName="ChangeVideo" 
        oncommand="imgThumbnail_Command" /></span>
    <span class="playListTeaserContent"><h2><mn2:FrameworkLinkButton ID="lnkTitle" runat="server" CommandName="ChangeVideo" 
        oncommand="lnkTitle_Command" ></mn2:FrameworkLinkButton></h2>
    <mn2:FrameworkLiteral runat="server" id="literalDescription"></mn2:FrameworkLiteral></span>
 </div>   
<asp:HiddenField ID="hdnVideoID" runat="server" />