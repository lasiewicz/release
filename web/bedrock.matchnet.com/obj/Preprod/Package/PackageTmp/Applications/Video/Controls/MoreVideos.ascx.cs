﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Ui.FormElements;
using Matchnet.Web.Applications.Video;

namespace Matchnet.Web.Applications.Video.Controls
{
    /// <summary>
    /// User control that represents the 'More Videos' section of the page
    /// </summary>
    public partial class MoreVideos : FrameworkControl
    {
        #region Events
        public event VideoSelectedHandler VideoSelected;
        #endregion

        #region Public Properties
        public VideoPlayerType PlayerType { get; set; }
        #endregion

        #region Event Handlers
        /// <summary>
        /// Retrieves the 'more videos' and binds them to the data list
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                VideoListUtility moreVideos = new VideoListUtility(VideoViewingType.MoreVideos, PlayerType);
                dlstMoreVideos.DataSource = moreVideos.GetVideos();
                dlstMoreVideos.DataBind();
            }

            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        /// <summary>
        /// Sets various values for each item as it is bound 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dlstMoreVideos_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            try
            {
                VideoSummary summary = (VideoSummary)e.Item.DataItem;
                ImageButton button = (ImageButton)e.Item.FindControl("imgBtnMoreVideo");
                FrameworkLinkButton link = (FrameworkLinkButton)e.Item.FindControl("lnkBtnMoreVideo");
                button.OnClientClick = link.OnClientClick = string.Format("setClickedVideo('{0}');", summary.ID);
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        /// <summary>
        /// Raises the VideoSelected event when the user clicks on the imagebutton
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void imgBtnMoreVideo_Command(object sender, CommandEventArgs e)
        {
            VideoSelected(this, new VideoSelectedEventArgs(e.CommandArgument.ToString(), VideoViewingType.MoreVideos));
        }

        /// <summary>
        /// Raises the VideoSelected event when the user clicks on the linkbutton
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lnkBtnMoreVideo_Command(object sender, CommandEventArgs e)
        {
            VideoSelected(this, new VideoSelectedEventArgs(e.CommandArgument.ToString(), VideoViewingType.MoreVideos));
        }

        override protected void OnInit(EventArgs e)
        {
            this.Load += new System.EventHandler(this.Page_Load);
            base.OnInit(e);
        }
        #endregion
    }
}