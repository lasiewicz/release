﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using Matchnet.Web.Framework;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Web.Applications.Video;

namespace Matchnet.Web.Applications.Video.Controls
{
    /// <summary>
    /// Contains all functionality to manage a playlist of videos
    /// </summary>
    public partial class VideoPlaylist : FrameworkControl
    {
        #region Events
        public event VideoSelectedHandler VideoSelected;
        #endregion

        #region Public Properties
        public string SelectedVideoID { get; set; }
        public VideoPlayerType PlayerType { get; set; }
        #endregion

        #region Event Handlers
        /// <summary>
        /// Retrieves the videos for the playlist and databinds them to our repeater of
        /// VideoThumbnail controls
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                VideoListUtility playlist = new VideoListUtility(VideoViewingType.Playlist, PlayerType);
                rptPlaylist.DataSource = playlist.GetVideos();
                rptPlaylist.DataBind();
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        /// <summary>
        /// Sets various values for each item as it is bound
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void rptPlaylist_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                VideoThumbnail thumbnail = (VideoThumbnail)e.Item.FindControl("vid");
                VideoSummary summary = (VideoSummary)e.Item.DataItem;
                if (summary.ID == SelectedVideoID)
                {
                    thumbnail.Selected = true;
                }

                //We're supposed to trim the description to two lines in the playlist. Since there's no way to 
                //know exactly how long a 'line' of text will be on screen, I've chose an approximation
                //of 90 characters. Also, make sure to not cut off in the middle of a word.
                if (summary.DescriptionResourceConstant.Length > 90)
                {
                    thumbnail.DescriptionResourceConstant = summary.DescriptionResourceConstant.Substring(0, summary.DescriptionResourceConstant.Substring(0, 90).LastIndexOf(" ")) + " ...";
                }
                thumbnail.VideoSelected += new VideoSelectedHandler(thumbnail_VideoSelected);
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        /// <summary>
        /// Raises the VideoSelected event when a user clicks on the imagebutton
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void thumbnail_VideoSelected(object sender, VideoSelectedEventArgs e)
        {
            VideoSelected(sender, e);
        }

        override protected void OnInit(EventArgs e)
        {
            this.Load += new System.EventHandler(this.Page_Load);
            base.OnInit(e);
        }
        #endregion
    }
}