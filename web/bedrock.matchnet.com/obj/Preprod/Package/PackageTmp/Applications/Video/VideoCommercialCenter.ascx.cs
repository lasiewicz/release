﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Globalization;
using Matchnet.Web.Framework.Ui;

namespace Matchnet.Web.Applications.Video
{
    public partial class VideoCommercialCenter : FrameworkControl, IPostBackEventHandler
    {
        #region Protected Properties
        private const string SETTING_VIDEO_MODE = "VIDEO_MODE_FOR_COMMERCIAL";
        private const string SETTING_VIDEO_ID = "VIDEO_CHANNEL_ID_FOR_COMMERCIAL";
        private const string SETTING_VIDEO_URL_PARAMS = "VIDEO_CHANNEL_PARAMS_FOR_COMMERCIAL";

        public const string VIDEO_MODE_PLAYLIST = "playlist";
        public const string VIDEO_MODE_CHANNEL = "channel";
        string _video_mode = "";
        string _video_id = "";
        string _video_params = "";
        /// <summary>
        /// The currently selected video
        /// </summary>
        protected string SelectedVideoID { get; set; }

        private VideoPlayerType _playerType;

        /// <summary>
        /// Gets/sets this property from Session using lazy instantiation
        /// </summary>
        protected bool IsPlaylistMode
        {
            get
            {
                if (g.Session[WebConstants.SESSION_PROPERTY_NAME_VIDEO_ISPLAYLIST] == string.Empty)
                {
                    g.Session.Add(WebConstants.SESSION_PROPERTY_NAME_VIDEO_ISPLAYLIST, true, Matchnet.Session.ValueObjects.SessionPropertyLifetime.Temporary);
                    hdnIsPlayListModee.Value = true.ToString();
                }
                return Convert.ToBoolean(g.Session[WebConstants.SESSION_PROPERTY_NAME_VIDEO_ISPLAYLIST]);
            }
            set
            {
                g.Session.Add(WebConstants.SESSION_PROPERTY_NAME_VIDEO_ISPLAYLIST, value, Matchnet.Session.ValueObjects.SessionPropertyLifetime.Temporary);
                hdnIsPlayListModee.Value = value.ToString();
            }
        }

        /// <summary>
        /// Gets/sets this property from Session using lazy instantiation
        /// </summary>
        protected int CurrentVideoOrder
        {
            get
            {
                if (g.Session[WebConstants.SESSION_PROPERTY_NAME_VIDEO_CURRENT_ORDER] == string.Empty)
                {
                    g.Session.Add(WebConstants.SESSION_PROPERTY_NAME_VIDEO_CURRENT_ORDER, 1, Matchnet.Session.ValueObjects.SessionPropertyLifetime.Temporary);
                }
                return Convert.ToInt32(g.Session[WebConstants.SESSION_PROPERTY_NAME_VIDEO_CURRENT_ORDER]);
            }
            set
            {
                g.Session.Add(WebConstants.SESSION_PROPERTY_NAME_VIDEO_CURRENT_ORDER, value, Matchnet.Session.ValueObjects.SessionPropertyLifetime.Temporary);
            }
        }
        #endregion

        #region Event Handlers
        /// <summary>
        /// Adds the appropriate LeftNav control to the GlobalContext placeholder
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Init(object sender, EventArgs e)
        {
           
        }

        /// <summary>
        /// Handles the postback event for the control (this is caused by client javascript so we
        /// need a custom handler
        /// </summary>
        /// <param name="eventArgument"></param>
        public void RaisePostBackEvent(string eventArgument)
        {
            //We need this because otherwise the value of the control is reset by the postback
            hdnSelectedVideo.Value = SelectedVideoID;
        }

        /// <summary>
        /// Loads the playlist videos, selects the appropriate video if postback, and sets the player properties 
        /// to play the appropriate video
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e) 
        {
            try 
            {
                // Do not show the banner to logged in members
                videoCommercialBanner.Visible = (g.Member == null) ? true : false;
                

                _playerType = (VideoPlayerType)Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("VIDEO_PLAYER_TYPE", _g.Brand.Site.Community.CommunityID, _g.Brand.Site.SiteID));
                playlist.PlayerType = _playerType;
                getSettings();
                VideoSummary summary = null;
                VideoListUtility playlistVideos = new VideoListUtility(VideoViewingType.Playlist, _playerType);


                //If it's not a postback, get the default video
                if (!IsPostBack)
                {
                    if (IsPlaylistMode)
                    {
                        summary = playlistVideos.GetSummaryByID(playlistVideos.GetDefaultVideoID());
                        IsPlaylistMode = true;
                        CurrentVideoOrder = 1;
                        SetPlayerProperties(summary);

                        playlist.VideoSelected += new VideoSelectedHandler(videos_VideoSelected);
                    }
                    else
                    {
                        SetPlayerProperties();
                    }
                }
                else
                {
                    //If it's a postback and we're in playlist mode, get the next video to play. 
                    //If it's a postback due to the user clicking on a video rather than an autopostback due
                    //to the current video finishing, then the VideoSelected will fire after this and select
                    //a different video. 
                    if (IsPlaylistMode)
                    {
                        summary = playlistVideos.GetNextVideoInPlaylist(CurrentVideoOrder);
                        CurrentVideoOrder = Convert.ToInt32(summary.PlaylistOrder.ToString());
                        SetPlayerProperties(summary);

                        playlist.VideoSelected += new VideoSelectedHandler(videos_VideoSelected);
                    }
                    else
                    {
                        SetPlayerProperties();
                    }
                }
                if (!IsPlaylistMode)
                {
                    divfeaturedArticles.Visible = false;
                    divPlayList.Visible = false;

                }

            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        protected override void OnPreRender(EventArgs e)
        {          
            try
            {
                _g.AnalyticsOmniture.PageName = "Commercial Videos";
                base.OnPreRender(e);
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        /// <summary>
        /// Will be called when a video is clicked on by the user, either in the playlist or the
        /// 'more videos' section. Retrieves attributes for the selected video based on the 
        /// supplied VideoSelectedEventArgs
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">Contains both the id and VideoViewingType of the selected video</param>
        protected void videos_VideoSelected(object sender, VideoSelectedEventArgs e)
        {
            try
            {
                VideoListUtility playlistVideos = new VideoListUtility(e.ViewingType, _playerType);
                VideoSummary summary = playlistVideos.GetSummaryByID(e.VideoID);
                SetPlayerProperties(summary);

                if (e.ViewingType == VideoViewingType.Playlist)
                {
                    IsPlaylistMode = true;
                    CurrentVideoOrder = summary.PlaylistOrder;
                }
                else
                {
                    IsPlaylistMode = false;
                    CurrentVideoOrder = 1;
                }
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        override protected void OnInit(EventArgs e)
        {
            this.Load += new System.EventHandler(this.Page_Load);
            this.Init += new EventHandler(this.Page_Init);
            base.OnInit(e);
        }
        #endregion

        #region Protected/Private Methods

        /// <summary>
        /// Sets the necessary properties of the player
        /// </summary>
        /// <param name="summary"></param>
        private void SetPlayerProperties(VideoSummary summary)
        {
            switch (_playerType)
            {
                case VideoPlayerType.Ooyala:
                    ooyalaPlayer.Visible = true;
                    ooyalaPlayer.SetPlayerProperties(summary);
                    ooyalaPlayer.PlayerMode = Configuration.ServiceAdapters.RuntimeSettings.GetSetting(SETTING_VIDEO_MODE, g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID);
                    ooyalaPlayer.VideoID = Configuration.ServiceAdapters.RuntimeSettings.GetSetting(SETTING_VIDEO_ID, g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID);
                    ooyalaPlayer.VideoParams = Configuration.ServiceAdapters.RuntimeSettings.GetSetting(SETTING_VIDEO_URL_PARAMS, g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID);
                    break;
                case VideoPlayerType.YouTube:
                    youTubePlayer.Visible = true;
                    youTubePlayer.SetPlayerProperties(summary);
                    break;
            }

            hdnSelectedVideo.Value = summary.ID;
            SelectedVideoID = summary.ID;
        }
        private void SetPlayerProperties()
        {
            ooyalaPlayer.Visible = true;
            ooyalaPlayer.PlayerMode = Configuration.ServiceAdapters.RuntimeSettings.GetSetting(SETTING_VIDEO_MODE, g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID);
            ooyalaPlayer.VideoID = Configuration.ServiceAdapters.RuntimeSettings.GetSetting(SETTING_VIDEO_ID, g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID);
            ooyalaPlayer.VideoParams = Configuration.ServiceAdapters.RuntimeSettings.GetSetting(SETTING_VIDEO_URL_PARAMS, g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID);
            ooyalaPlayer.SetPlayerProperties();

        }

        /// <summary>
        /// The aspx pages uses this to generate the Target for the postback we kick off with
        /// Javascript when a video finishes playing in playlist mode. 
        /// </summary>
        /// <returns></returns>
        protected string GeneratePostbackControlTarget()
        {
            return UniqueIDWithDollars(this);
        }

        /// <summary>
        /// Gets the ID of the control in the format that the postback call needs it
        /// </summary>
        /// <param name="ctrl"></param>
        /// <returns></returns>
        protected string UniqueIDWithDollars(Control ctrl)
        {
            string sId = ctrl.UniqueID;
            if (sId == null)
            {
                return null;
            }
            if (sId.IndexOf(':') >= 0)
            {
                return sId.Replace(':', '$');
            }
            return sId;
        }

        #endregion

        private void getSettings()
        {
            try
            {
                _video_mode = Configuration.ServiceAdapters.RuntimeSettings.GetSetting(SETTING_VIDEO_MODE, g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID);

                if (_video_mode.ToUpper() == VIDEO_MODE_PLAYLIST)
                    IsPlaylistMode = true;
                else
                    IsPlaylistMode = false;
            }
            catch (Exception ex)
            { }
        }
    }
}