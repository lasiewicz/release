﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Managers;
using Matchnet.Web.Framework.Util;
using System.Web.Script.Serialization;
using Matchnet.PremiumServiceSearch.ValueObjects.QuestionAnswer;

namespace Matchnet.Web.Applications.Email.Controls
{
    public partial class PhotoCommentEmailMessage : Matchnet.Web.Applications.MemberProfile.ProfileTabs30.BaseProfile
    {
        public PhotoCommentMessage FTM { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public void LoadPhotoCommentMessage(string jsonString, ContextGlobal _g, int FromMemberId, string FromMemberName)
        {
            FTM = new JavaScriptSerializer().Deserialize<Email.Controls.PhotoCommentMessage>(jsonString);
            
            base.MemberProfile = _g.Member;
            Member.ValueObjects.Photos.Photo photo;

            string fullDisplayValue = "";

            if (_g.Member.MemberID != FromMemberId)
            {
                try
                {
                    photo = base.MemberProfile.GetPhotos(_g.Brand.Site.Community.CommunityID).Find(FTM.PhotoID);
                    if (photo.IsCaptionApproved)
                    {
                        fullDisplayValue = photo.Caption;
                    }

                    //MemberPhotoHelper photoHelper = new MemberPhotoHelper(Configuration.ServiceAdapters.RuntimeSettings.Instance);
                    this.imgThumbnail.ImageUrl = MemberPhotoDisplayManager.Instance.GetPhotoDisplayURL(g.Member, MemberProfile, g.Brand,
                                                                                      photo, PhotoType.Thumbnail,
                                                                                      PrivatePhotoImageType.Thumb,
                                                                                      NoPhotoImageType.Thumb);
                }
                catch (Exception ex)
                {
                    this.imgThumbnail.Visible = false;
                }

                if (String.IsNullOrEmpty(fullDisplayValue))
                {
                    fullDisplayValue = GetNoAnswer(this);
                }

                this.txtOrigMessageLabel.Text = string.Format(g.GetResource("USERNAME_IS_RESPONDING_TO_YOUR_PHOTO_TITLE", this), Uri.UnescapeDataString(FromMemberName));

                if (!String.IsNullOrEmpty(fullDisplayValue))
                {
                    this.commentText.Text = fullDisplayValue;
                }
                else
                {
                    phCaption.Visible = false;
                }
            }
            else
            {
                photoCommentReplyText.Visible = false;
            }

        }
    }

    public class PhotoCommentMessage
    {
        public int PhotoID { get; set; }

    }
}