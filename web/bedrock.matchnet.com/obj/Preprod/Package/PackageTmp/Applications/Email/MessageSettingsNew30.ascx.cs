﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Web.Applications.MemberProfile;
using Matchnet.Web.Framework;
using Matchnet.ExternalMail.ValueObjects.DoNotEmail;
using Matchnet.ExternalMail.ServiceAdapters;
using System.Data;
using Matchnet.ExternalMail.ValueObjects;
using Matchnet.Content.ValueObjects.AttributeMetadata;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Web.Framework.Managers;
using Matchnet.Web.Framework.Util;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Configuration.ServiceAdapters;

namespace Matchnet.Web.Applications.Email
{
    public partial class MessageSettingsNew30 : FrameworkControl
    {
        private const string NEW_MEMBER_EMAIL_ENABLED = "NEW_MEMBER_EMAIL_ENABLED";

        #region Event Handlers
        protected override void OnInit(EventArgs e)
        {
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            this.Load += new System.EventHandler(this.Page_Load);

            base.OnInit(e);
        }

        protected void Page_Load(object sender, System.EventArgs e)
        {
            // We now allow members to come here without logging in.  Check for the MemberID in the URL.
            if (g.Member == null)
            {
                var memberIDFromURL = MessageManager.Instance.GetMemberIDFromURL(Request);

                if (memberIDFromURL != Constants.NULL_INT)
                {
                    g.Member = MemberSA.Instance.GetMember(memberIDFromURL, MemberLoadFlags.None);
                    ShowLoggedInUI(false);
                }
            }
            else
            {
                ShowLoggedInUI(true);
            }

            // if g.Member is null here, it means the URL parameters didn't pan out, so force a login here
            if (g.Member == null)
            {
                var pagePath = (string)Context.Items["PagePath"];
                _g.LogonRedirect(pagePath);
            }

            this.ShowCommonContent();

            DoNotEmailEntry entry = DoNotEmailSA.Instance.GetEntryBySiteAndEmailAddress(g.Brand.Site.SiteID, g.Member.EmailAddress);

            if (entry == null)
            {
                this.ShowContent();

                if (!IsPostBack)
                {
                    this.ShowCommonData();
                    this.ShowData();
                }
            }
            else
            {
                ShowDNEContent();
                if (!IsPostBack)
                {
                    this.ShowCommonData();
                }
            }

            if (g.BreadCrumbTrailHeader != null)
            {
                g.BreadCrumbTrailHeader.SetTwoLinkCrumb(g.GetResource("MESSAGE_SETTING", this),
                                                        g.AppPage.App.DefaultPagePath);

            }
            if (g.BreadCrumbTrailFooter != null)
            {
                g.BreadCrumbTrailFooter.SetTwoLinkCrumb(g.GetResource("MESSAGE_SETTING", this),
                                                       g.AppPage.App.DefaultPagePath);
            }

            // This is used when SKIP_EMAIL_UNSUBSCRIBE_PAGE is enabled
            const string emailTypeText = "et";

            string emailType = Constants.NULL_STRING;
            emailType = String.IsNullOrEmpty(Request[emailTypeText]) ? Constants.NULL_STRING : Request[emailTypeText];
            if (emailType != Constants.NULL_STRING)
            {
                lblOptedOut.Text = String.Format(g.GetResource("MESSAGE_OPTED_OUT", this), g.GetResource(emailType.ToUpper() + "_TEXT", this));
                lblOptedOut.Visible = true;
            }
        }

        private void ShowLoggedInUI(bool isLoggedIn)
        {

            if (isLoggedIn)
            {
                divLoggedIn.Visible = true;
                aMatchPrefernces.Visible = true;
                aMatchPreferncesRadio.Visible = true;
                divNotLoggedIn.Visible = false;
            }
            else
            {
                divLoggedIn.Visible = false;
                aMatchPrefernces.Visible = false;
                aMatchPreferncesRadio.Visible = false;
                divNotLoggedIn.Visible = true;

            }

        }

        protected void btnSave_Click(object sender, System.EventArgs e)
        {
            DoNotEmailEntry entry = DoNotEmailSA.Instance.GetEntryBySiteAndEmailAddress(g.Brand.Site.SiteID, g.Member.EmailAddress);

            if (entry == null)
            {
                Save();
                SaveCommon();
                ShowCommonData();
                ShowData();
            }
            else
            {
                SaveCommon();
                ShowCommonData();
            }
        }

        protected void rptMessageSetting_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                Label lbl = null;
                CheckBox chk = null;


                lbl = (Label)e.Item.FindControl("lblMessageSettingTitle");
                chk = (CheckBox)e.Item.FindControl("chkMessageSetting");

                DataRowView row = (DataRowView)e.Item.DataItem;
                string content = row["Content"].ToString();
                int value = Conversion.CInt(row["Value"].ToString());

                int mask1 = g.Member.GetAttributeInt(g.Brand, WebConstants.ATTRIBUTE_NAME_MAILBOXPREFERENCE, 0);

                if (mask1 == 0)
                {
                    Member.ValueObjects.AttributeOptionMailboxPreference defaultMailboxPreference = Member.ValueObjects.AttributeOptionMailboxPreference.IncludeOriginalMessagesInReplies | Member.ValueObjects.AttributeOptionMailboxPreference.SaveCopiesOfSentMessages;
                    int defaultMask = (int)defaultMailboxPreference;
                    mask1 = defaultMask;

                }
                if ((value & mask1) == value)
                    chk.Checked = true;

                string titleResource = "MESSAGE_SETTING_TITLE_" + value.ToString();
                lbl.Text = g.GetResource(titleResource, this);
            }
            catch (Exception ex)
            { g.ProcessException(ex); }

        }
        #endregion

        #region Private methods
        private void ShowDNEContent()
        {
            prefBorder.Visible = false;
            pnlDNE.Visible = true;
            this.DataBind();
        }

        private void ShowCommonData()
        {
            #region MESSAGE SETTINGS

            int mask1 = g.Member.GetAttributeInt(g.Brand, WebConstants.ATTRIBUTE_NAME_MAILBOXPREFERENCE, 0);
            //JIRA OI-17 Set "Include original msgs in replies" and "Save copies of sent msgs" as default
            if (mask1 == 0)
            {
                Member.ValueObjects.AttributeOptionMailboxPreference defaultMailboxPreference = Member.ValueObjects.AttributeOptionMailboxPreference.IncludeOriginalMessagesInReplies | Member.ValueObjects.AttributeOptionMailboxPreference.SaveCopiesOfSentMessages;
                int defaultMask = (int)defaultMailboxPreference;
                mask1 = defaultMask;

            }

            chkVacation.Checked = (g.Member.GetAttributeDate(g.Brand, "VacationStartDate") != DateTime.MinValue);

            #endregion
        }

        private void ShowData()
        {
            #region MESSAGE FROM SITE SETTING

            #region MATCH MAIL SETTING

            int mask2 = g.Member.GetAttributeInt(g.Brand, "MatchNewsletterPeriod", 0);
            if (phMatchesRadio.Visible)
            {
                rdoListMatchNewsLetterPeriod.SelectedValue = Matchnet.Lib.Util.Util.CString(mask2);
            }
            else
            {
                chkMatchesDaily.Checked = (mask2 > 0);
            }

            #endregion

            #region CLICK EMAIL SETTING

            int mask3 = g.Member.GetAttributeInt(g.Brand, "GotAClickEmailPeriod", 0);
            rdoListGotAClickEmailPeriod.SelectedValue = Matchnet.Lib.Util.Util.CString(mask3);

            #endregion

            #region	 NEWS EVENTS OFFERS SETTING

            int mask4 = g.Member.GetAttributeInt(g.Brand, "NewsletterMask", 0);
            chkInvitation.Checked = (mask4 & (int)NewsEventOfferMask.Events) == (int)NewsEventOfferMask.Events;
            chkNewsletters.Checked = (mask4 & (int)NewsEventOfferMask.News) == (int)NewsEventOfferMask.News;
            chkOffers.Checked = (mask4 & (int)NewsEventOfferMask.Offers) == (int)NewsEventOfferMask.Offers;
            #endregion

            #region ALERT SETTING

            if (this.YesMailEnabled)
            {
                AttributeGroup group = AttributeMetadataSA.Instance.GetAttributes().GetAttributeGroup(g.Brand.Site.Community.CommunityID, EmailAlertConstant.ATTRIBUTE_ID);
                int mask5 = g.Member.GetAttributeInt(g.Brand, EmailAlertConstant.ALERT_ATTRIBUTE_NAME, int.Parse(group.DefaultValue));

                chkClickAlert.Checked = (mask5 & (int)EmailAlertMask.GotClickAlert) == (int)EmailAlertMask.GotClickAlert;
                chkEmailAlert.Checked = (mask5 & (int)EmailAlertMask.NewEmailAlert) == (int)EmailAlertMask.NewEmailAlert;

                if (phAlertEcard.Visible)
                {
                    chkECardAlert.Checked = (mask5 & (int)EmailAlertMask.ECardAlert) == (int)EmailAlertMask.ECardAlert;
                }
                if (phAlertHotList.Visible)
                {
                    chkHotListAlert.Checked = (mask5 & (int)EmailAlertMask.HotListedAlert) == (int)EmailAlertMask.HotListedAlert;
                }
                if (phAlertProfileViewed.Visible)
                {
                    chkProfileViewedAlert.Checked = (mask5 & (int)EmailAlertMask.ProfileViewedAlertOptOut) != (int)EmailAlertMask.ProfileViewedAlertOptOut;
                }
                if (phAlertEssayRequest.Visible)
                {
                    chkEssayRequestAlert.Checked = (mask5 & (int)EmailAlertMask.NoEssayRequestEmail) == (int)EmailAlertMask.NoEssayRequestEmail;
                }
            }

            #endregion

            #endregion

            if (!g.IsYNMEnabled)
            {
                plcYNM.Visible = false;
                plcYNM2.Visible = false;
            }
        }

        private void ShowCommonContent()
        {
            lblUserName.Text = g.Member.GetUserName(_g.Brand);
            g.ExpansionTokens.Add("YOUREMAIL", g.Member.EmailAddress);
            litEmail.Text = g.Member.EmailAddress;

            rptMessageSetting.DataSource = Option.GetOptions(WebConstants.ATTRIBUTE_NAME_MAILBOXPREFERENCE, g);
            rptMessageSetting.DataBind();
            chkVacation.Text = g.GetResource("VACATION_CHECK_BOX_TEXT", this);

        }

        private void ShowContent()
        {
            prefBorder.Visible = true;
            pnlDNE.Visible = false;

            //click email
            rdoListGotAClickEmailPeriod.DataSource = Option.GetOptions("GotAClickEmailPeriod", g);

            //matches
            DataTable dtMatches = Option.GetOptions("MatchNewsletterPeriod", g);
            if (dtMatches.Rows.Count > 2)
            {
                //multiple options
                phMatchesRadio.Visible = true;
                phMatchesCheckbox.Visible = false;
                rdoListMatchNewsLetterPeriod.DataSource = Option.GetOptions("MatchNewsletterPeriod", g);
            }
            else
            {
                //toggle - never or daily
                phMatchesCheckbox.Visible = true;
                phMatchesRadio.Visible = false;
            }

            //set checkbox text
            chkMatchesDaily.Text = g.GetResource("MATCHES_CHECK_BOX_TEXT", this);
            chkInvitation.Text = g.GetResource("INVITATION_CHECK_BOX_TEXT", this);
            chkHotListAlert.Text = g.GetResource("HOT_LIST_CHECK_BOX_TEXT", this);
            chkEmailAlert.Text = g.GetResource("EMAIL_ALERT_CHECK_BOX_TEXT", this);
            chkECardAlert.Text = g.GetResource("ECARD_ALERT_CHECK_BOX_TEXT", this);
            chkEssayRequestAlert.Text = g.GetResource("ESSAY_REQUEST_ALERT_CHECK_BOX_TEXT", this);
            chkClickAlert.Text = g.GetResource("CLICK_ALERT_CHECK_BOX_TEXT", this);
            chkNewsletters.Text = g.GetResource("NEWSLETTERS_CHECK_BOX_TEXT", this);
            chkOffers.Text = g.GetResource("OFFERS_CHECK_BOX_TEXT", this);
            chkProfileViewedAlert.Text = g.GetResource("PROFILE_VIEWED_ALERT_CHECK_BOX_TEXT", this);

            //new members
            bool showNewMemberContent = Conversion.CBool(Configuration.ServiceAdapters.RuntimeSettings.GetSetting(NEW_MEMBER_EMAIL_ENABLED, g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID));
            if (showNewMemberContent)
            {
                pnlNewMemberEmailSettings.Visible = true;
                rdoListNewMemberEmailPeriod.DataSource = Option.GetOptions("NewMemberEmailPeriod", g);
                int mask = g.Member.GetAttributeInt(g.Brand, "NewMemberEmailPeriod", 0);
                rdoListNewMemberEmailPeriod.SelectedValue = Matchnet.Lib.Util.Util.CString(mask);
            }

            //Ecard
            if (g.EcardsEnabled)
            {
                phAlertEcard.Visible = true;
            }
            else
            {
                phAlertEcard.Visible = false;
            }

            //Hotlist
            bool enableHotlistAlerts = SettingsManager.GetSettingBool(SettingConstants.ENABLE_HOTLISTED_EMAIL_NOTIFICATION, g.Brand);
            if (enableHotlistAlerts)
            {
                phAlertHotList.Visible = true;
            }
            else
            {
                phAlertHotList.Visible = false;
            }

            //profile viewed
            bool enableProfileViewedAlerts = SettingsManager.GetSettingBool(SettingConstants.ENABLE_VIEWED_YOUR_PROFILE_EMAIL, g.Brand);
            if (enableProfileViewedAlerts)
            {
                phAlertProfileViewed.Visible = true;
            }
            else
            {
                phAlertProfileViewed.Visible = false;
            }

            //Essay Request Email
            bool enableEssayRequestAlertSetting = SettingsManager.GetSettingBool(SettingConstants.ESSAY_REQUEST_ALERT_SETTING_VISIBLE, g.Brand)
                    && ViewProfileTabUtility.IsEssayRequestEmailEnabled(g);
            if (enableEssayRequestAlertSetting)
            {
                phAlertEssayRequest.Visible = true;
            }
            else
            {
                phAlertEssayRequest.Visible = false;
            }

            this.DataBind();
        }

        private void SaveCommon()
        {
            #region MESSAGE SETTINGS

            g.Member.SetAttributeDate(g.Brand, "VacationStartDate", (chkVacation.Checked) ? DateTime.Now : DateTime.MinValue);
            SaveMessageSettings();
            #endregion

            Matchnet.Member.ServiceAdapters.MemberSA.Instance.SaveMember(g.Member);
            g.Notification.AddMessage("EMAIL_SETTINGS_SUCCESSFULLY_SAVED");
        }

        public void Save()
        {
            int mask = 0;

            #region NEW MEMBER MEAIL SETTING

            bool saveNewMemberContent = Conversion.CBool(Configuration.ServiceAdapters.RuntimeSettings.GetSetting(NEW_MEMBER_EMAIL_ENABLED, g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID));

            if (saveNewMemberContent)
            {
                g.Member.SetAttributeInt(g.Brand, "NewMemberEmailPeriod", Matchnet.Conversion.CInt(rdoListNewMemberEmailPeriod.SelectedValue, 0));
            }

            #endregion

            #region MATCH MAIL SETTING
            if (phMatchesRadio.Visible)
            {
                g.Member.SetAttributeInt(g.Brand, "MatchNewsletterPeriod", Matchnet.Conversion.CInt(rdoListMatchNewsLetterPeriod.SelectedValue, 0));
            }
            else
            {
                if (chkMatchesDaily.Checked)
                {
                    //daily
                    g.Member.SetAttributeInt(g.Brand, "MatchNewsletterPeriod", 1);
                }
                else
                {
                    //never
                    g.Member.SetAttributeInt(g.Brand, "MatchNewsletterPeriod", 0);
                }
            }

            #endregion

            #region CLICK EMAIL SETTING

            g.Member.SetAttributeInt(g.Brand, "GotAClickEmailPeriod", Matchnet.Conversion.CInt(rdoListGotAClickEmailPeriod.SelectedValue, 0));

            #endregion

            #region NEWS EVENTS OFFERS SETTING

            mask = 0;
            if (chkInvitation.Checked)
                mask = (mask | (int)NewsEventOfferMask.Events);

            if (chkNewsletters.Checked)
                mask = (mask | (int)NewsEventOfferMask.News);

            if (chkOffers.Checked)
                mask = (mask | (int)NewsEventOfferMask.Offers);


            g.Member.SetAttributeInt(g.Brand, "NewsletterMask", mask);

            #endregion

            #region ALERT SETTING

            if (this.YesMailEnabled)
            {
                mask = 0;

                if (phAlertHotList.Visible && chkHotListAlert.Checked)
                    mask = (mask | (int)EmailAlertMask.HotListedAlert);
                if (chkEmailAlert.Checked)
                    mask = (mask | (int)EmailAlertMask.NewEmailAlert);
                if (phAlertEcard.Visible && chkECardAlert.Checked)
                    mask = (mask | (int)EmailAlertMask.ECardAlert);
                if (chkClickAlert.Checked)
                    mask = (mask | (int)EmailAlertMask.GotClickAlert);
                if (!chkProfileViewedAlert.Checked)
                    mask = (mask | (int)EmailAlertMask.ProfileViewedAlertOptOut);
                if (chkEssayRequestAlert.Checked)
                    mask = (mask | (int)EmailAlertMask.NoEssayRequestEmail);

                g.Member.SetAttributeInt(g.Brand, EmailAlertConstant.ALERT_ATTRIBUTE_NAME, mask);
            }

            #endregion
        }

        private bool YesMailEnabled
        {
            get
            {
                return Convert.ToBoolean(RuntimeSettings.GetSetting(EmailAlertConstant.YES_MAIL_FLAG, g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID));
            }
        }

        private bool IsJDateIL
        {
            get
            {
                return (WebConstants.SITE_ID)g.Brand.Site.SiteID == WebConstants.SITE_ID.JDateCoIL;
            }
        }

        private bool IsCupidOrCollegeLove
        {
            get
            {
                return ((WebConstants.SITE_ID)g.Brand.Site.SiteID == WebConstants.SITE_ID.Cupid) ||
                        ((WebConstants.SITE_ID)g.Brand.Site.SiteID == WebConstants.SITE_ID.CollegeLuv);
            }
        }

        private void SaveMessageSettings()
        {
            try
            {
                int mask = 0;
                foreach (RepeaterItem oItem in rptMessageSetting.Items)
                {
                    CheckBox chk = oItem.FindControl("chkMessageSetting") as CheckBox;
                    if (chk.Checked)
                    {
                        string val = chk.Attributes["Value"].ToString();
                        int value = Conversion.CInt(val);
                        mask += value;
                    }
                }

                g.Member.SetAttributeInt(g.Brand, WebConstants.ATTRIBUTE_NAME_MAILBOXPREFERENCE, mask);
            }
            catch (Exception ex)
            { g.ProcessException(ex); }

        }

        #endregion
    }
}