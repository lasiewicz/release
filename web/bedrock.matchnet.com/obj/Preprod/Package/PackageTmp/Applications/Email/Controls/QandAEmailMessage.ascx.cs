﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Util;
using System.Web.Script.Serialization;
using Matchnet.PremiumServiceSearch.ValueObjects.QuestionAnswer;

namespace Matchnet.Web.Applications.Email.Controls
{
    public partial class QandAEmailMessage : FrameworkControl
    {
        public QAndAMessage QAndA { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public void LoadQAndAMessage(string jsonString)
        {
            QAndA = new JavaScriptSerializer().Deserialize<Email.Controls.QAndAMessage>(jsonString);
            Question question = Matchnet.PremiumServiceSearch.ServiceAdapters.QuestionAnswerSA.Instance.GetQuestion(QAndA.QuestionId);
            this.questionText.Text = question.Text;
            this.questionLink.NavigateUrl =  "/Applications/QuestionAnswer/Question.aspx?" + WebConstants.URL_PARAMETER_NAME_QUESTION_ID + "=" + question.QuestionID.ToString();
            int answerMemberId = Constants.NULL_INT;
            Answer[] answers = QuestionAnswer.QuestionAnswerHelper.GetApprovedAnswers(question, QAndA.FromMemberId, g);
            foreach (Answer answer in answers)
            {
                if (answer.AnswerID == QAndA.AnswerId)
                {
                    this.answerText.Text = answer.AnswerValue;
                    answerMemberId = answer.MemberID;
                }
            }
            this.txtOrigMessageLabel.Text = string.Format(g.GetResource("TXT_ANSWER_RESPONSE", this), Uri.UnescapeDataString(QAndA.FromMemberName));
        }
    }

    public class QAndAMessage
    {
        public string FromMemberName { get; set; }
        public int QuestionId { get; set; }
        public int AnswerId { get; set; }
        public int FromMemberId { get; set; }

    }
}