﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ComposeOverlay.ascx.cs" Inherits="Matchnet.Web.Applications.Email.ComposeOverlay" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn" TagName="miniProfile" Src="../../Framework/Ui/BasicElements/MiniProfile20.ascx" %>
<%@ Register TagPrefix="cc1" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="dg" TagName="FirstElementFocus" Src="../../Framework/JavaScript/FirstElementFocus.ascx" %>
<%@ Register TagPrefix="mn" TagName="microMicroProfile" Src="../../Framework/Ui/BasicElements/MicroMicroProfile20.ascx" %>
<%@ Register TagPrefix="mnicb" TagName="IceBreaker" Src="/Framework/Ui/BasicElements/IceBreaker.ascx" %>

<div class="page-container<%=_ColorCodePageCSS %>">
<asp:PlaceHolder ID="phComposeEmail" runat="server">
<mn:Title runat="server" id="ttlCompose" ResourceConstant="TXT_COMPOSE_MESSAGE"/>
<div class="compose-email">

<asp:PlaceHolder ID="phVIPFreeReply" runat="server" Visible="false" >
    <mn:Txt runat="server" ResourceConstant="TXT_FREE_REPLY_VIP"  ExpandImageTokens="true" />
</asp:PlaceHolder>
    <div class="compose-email-content">
        
        <div class="micro-micro-cont clearfix">
	        <label><mn:txt ID="Txt1" runat="server" ResourceConstant="TXT_TO" />: </label>
            <mn:microMicroProfile ID="microMicroProfile" runat="server" Visible="false"/>
            <asp:Label CssClass="item" ID="toLabel" Runat="server"></asp:Label>
        </div>

        <asp:Repeater Runat="server" ID="memberProfile" OnItemDataBound="miniProfileDataBind" Visible="False">
	        <ItemTemplate>
		        <mn:miniProfile runat="server" id="toMemberMiniProfile" member="<%# Container.DataItem %>" />
	        </ItemTemplate>
        </asp:Repeater>


        <asp:PlaceHolder ID="plcMailToList" runat="server" Visible="false">
            <mn:txt ID="Txt2" runat="server" ResourceConstant="TXT_TO" />:
            <asp:TextBox ID="toTextField" Runat="server"></asp:TextBox>&nbsp;&nbsp;<a id="hrefMailToList" runat="server" href="javascript:launchWindow('/Applications/Email/MailToList.aspx', 'View Profile',650,650,'')"><mn:txt ID="Txt3" runat="server" ResourceConstant="TXT_CLICK_HERE_TO_CHOOSE_FROM_YOUR_HOTLIST"/></a>
        </asp:PlaceHolder>

        <fieldset>
            <mnicb:IceBreaker runat="server" id="IceBreakerLink" OmnitureInsertString="Insert - Compose Page" OmnitureStartString="Start - Compose Page" Visible="false"/>
            <label><mn:txt ID="Txt4" runat="server" ResourceConstant="TXT_SUBJECT" />: </label><asp:TextBox Runat="server" ID="subjectTextField" MaxLength="50"></asp:TextBox><br />
            <label><mn:txt ID="Txt5" runat="server" ResourceConstant="TXT_MESSAGE" />: </label><asp:TextBox Runat="server" ID="messageBodyTextField" Rows="7" Columns="50" TextMode="MultiLine"></asp:TextBox>
        </fieldset>

        <div class="options">
	        <b><mn:txt ID="Txt6" runat="server" ResourceConstant="TXT_OPTIONS" />:</b> <span class="option-item"><asp:CheckBox Runat="server" ID="saveCopyCheckBox"></asp:CheckBox></span>
            <asp:PlaceHolder ID="plcSendPrivatePhotos" runat="server" Visible="false">
                <div id="privatePhotoDiv">
                <asp:CheckBox Runat="server" ID="cbSendPrivatePhotos"></asp:CheckBox>
                </div>
            </asp:PlaceHolder>
        </div>
    
        <asp:PlaceHolder ID="phColorCodeTips" runat="server" Visible="false">
        <div id="cc-compose-tips" class="editorial">
            <h2 class="cc-promo-top-headline">Communication Tips</h2>
            <asp:PlaceHolder ID="phBlueTips" runat="server" Visible="false">
                <h3>Create a Great First Impression with the Color BLUE!</h3>
                <ol>
                    <li>Take a sensitive, sincere approach.</li>
                    <li>Allow time for them to collect their thoughts.</li>
                    <li>Appreciate them.</li>
                </ol>
            </asp:PlaceHolder>
            <asp:PlaceHolder ID="phRedTips" runat="server" Visible="false">
                <h3>Create a Great First Impression with the Color RED!</h3>
                <ol>
                    <li>Present issues logically.</li>
                    <li>Be direct, brief, and specific.</li>
                    <li>Prepare yourself with facts and figures.</li>
                </ol>
            </asp:PlaceHolder>
            <asp:PlaceHolder ID="phWhiteTips" runat="server" Visible="false">
                <h3>Create a Great First Impression with the Color WHITE!</h3>
                <ol>
                    <li>Accept and support their individuality.</li>
                    <li>Create an informal, relaxed atmosphere.</li>
                    <li>Show patience; try not to rush them.</li>
                </ol>
            </asp:PlaceHolder>
            <asp:PlaceHolder ID="phYellowTips" runat="server" Visible="false">
                <h3>Create a Great First Impression with the Color YELLOW!</h3>
                <ol>
                    <li>Take a positive, upbeat approach.</li>
                    <li>Offer them praise and appreciation.</li>
                    <li>Accept some playful teasing, joking, or comic relief.</li>
                </ol>
            </asp:PlaceHolder>
            </div>
        </asp:PlaceHolder>
         
        <div class="text-outside buttons">
            <asp:PlaceHolder ID="plcSendAsVIP" runat="server" Visible="false">
            <div id="mail-send-as-vip">
                <asp:CheckBox runat="server" ID="cbxSendAsVIP" Checked="false" />
                <asp:Label runat="server" ID="lblVIPRemaining" CssClass="allaccess-remaining" />
                <div id="divSendVIPOverlay"> 
                <asp:PlaceHolder ID="phSendVIPOverlay" runat="server" Visible="true">
                    <mn:Txt ID="txtOverlay"  runat="server" />
                    <div class="cta">
                        <cc1:FrameworkButton CssClass="btn primary" runat="server" id="btnYes" ResourceConstant="BTN_YES" OnClick="ProcessVIPMail"/>
                        <cc1:FrameworkButton CssClass="textlink" runat="server" id="btnNo"   ResourceConstant="BTN_NO" OnClick="sendMessageClick"/>
                    </div>
                </asp:PlaceHolder>
                </div>
            </div>
            </asp:PlaceHolder>
	        <cc1:FrameworkButton class="btn secondary" id="saveDraftButton" runat="server" ResourceConstant="BTN_TXT_SAVE_AS_A_DRAFT" OnClick="saveDraftClick" />&#160;
	        <cc1:FrameworkButton class="btn primary" runat="server" ResourceConstant="BTN_TXT_SEND_MESSAGE"  id="sendMessageButton"  OnClick="sendMessageClick"/>
	        <cc1:FrameworkButton class="btn primary vipbutton" runat="server" ResourceConstant="BTN_TXT_SEND_MESSAGE"  id="sendVIPMessageButton" OnClientClick= "javascript:showVIPOverlay();return !overlay;" OnClick="sendMessageClick" />
        </div>
    </div>
</div>
<script type="text/javascript">
    var n = $j('#cc-compose-tips').length;
    if (n <= 0) {
        $j('.compose-email-content', '#content-main').addClass('no-cc');
    } else { };

    var $vipoverlay = $j('#divSendVIPOverlay');
    var overlay = false;
    function showVIPOverlay() {

        if (!$j('input:checkbox', '#mail-send-as-vip').is(":checked")) {
            $vipoverlay.detach().appendTo('#aspnetForm').after('<div class="ui-widget-overlay" style="position:fixed;" />').show();

            overlay = true;

            $j.ajax({
                type: "POST",
                url: "/Applications/API/SessionUpdate.asmx/UpdateSessionIntWithIncrement",
                data: "{key:'VIP_COMPOSE_OVERLAY_PER_SESSION',value:1,lifetime:0}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) { },
                error: function (msg) { }
            })

            return false;
        }
        else
            return true;
    }
    $j(document).ready(function () {
        var $allaccessTooltip = $j('#allaccess-tooltip');
        $j('.allaccess-help', '#mail-send-as-vip').hover(function (e) {
            var x = e.pageX;
            var y = e.pageY - $j(window).scrollTop();
            var tooltipWidth = $allaccessTooltip.outerWidth();
            if ($j('html').attr('dir') == 'rtl') {
                $allaccessTooltip.css({ 'position': 'fixed', 'left': x - tooltipWidth, 'top': y + 10 }).show();
            } else {
                $allaccessTooltip.css({ 'position': 'fixed', 'left': x + 10, 'top': y + 10 }).show();
            };
        },
            function () {
                $allaccessTooltip.hide();
            }
        );
    });

    //autogrow for compose message boxes
    $j('textarea[id$="messageBodyTextField"]').TextAreaExpander(82, 600);
</script>



</asp:PlaceHolder>
</div>
<div class="page-container-centered">
<!--classic profile-->
<asp:PlaceHolder ID="phClassicProfileDetail" Runat="server">
	<h2>
		<asp:Label Runat="server" ID="memberProfileUserName"></asp:Label>&nbsp;<mn:txt runat="server" id="txtProfileCont" ResourceConstant="TXT_PROFILE_CONT" />
	</h2>

	<asp:PlaceHolder Runat="server" id="phViewProfileInfo" />
	
</asp:PlaceHolder>

<!--tabbed profile-->
<asp:PlaceHolder ID="phTabbedProfileDetail" Runat="server" Visible="False"></asp:PlaceHolder>

</div>