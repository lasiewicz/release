﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="IMHistory.ascx.cs" Inherits="Matchnet.Web.Applications.Email.IMHistory" %>
<%@ Register TagPrefix="mnF" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn" TagName="TabBar" Src="../../Framework/Ui/BasicElements/TabBar.ascx" %>

<div>
    <h1><mnF:Txt ID="txtIMHistoryTitle" runat="server" ResourceConstant="TXT_IM_HISTORY_TITLE" /></h1>
</div>
<mn:TabBar ID="mailBoxTabBar" runat="server" ClassSelected="selected" />

<%--DYNAMTIC HTML--%>
<!--member list-->
<div id="listCont" class="list-cont im-history-col">
    <h2><mnF:Txt ID="txtIMHistoryListTitle" runat="server" ResourceConstant="TXT_IM_HISTORY_TITLE" /></h2>
    <ul id="standardList" class="list custom-scroll">
    </ul>
</div>

<!--messages-->
<div id="messageCont" class="message-cont">
    <div id="chatterMiniProfile" class="header clearfix">
    </div>
    <ul id="chatterMessages" class="chat_messages body custom-scroll">
    </ul>
</div>

<!-- HELPER TEMPLATES START -->

<!-- member list item template-->
<div id="memberlistItemTemplate" style="display:none;">
    <li id="memberlistItem{{ID}}">
        <div id="memberlistItemData{{MemberID}}" data-memberid="{{MemberID}}" data-thumbwebpath="{{ThumbWebPath}}" data-username="{{UserName}}" data-isonline="{{IsOnline}}" data-maritalseeking="{{MaritalSeekingDisplayText}}" data-agelocation="{{AgeLocationDisplayText}}">
            <a href="#" title="{{UserName}}" class="photo"><img src="{{ThumbWebPath}}" alt="{{UserName}}"></a>
            <div class="info">
                <a href="javascript:void(0)" title="{{UserName}}" class="username">{{UserName}}</a> 
                <span>{{AgeLocationDisplayText}}</span>
                <small class="timestamp">{{LastMessageDateString}}</small>
            </div>
        </div>
        <a href="javascript:void(0)" title="Delete" class="action" onclick="return spark.imHistoryManager.onConversationMemberRemoveClick(event,{{ID}});"><span class="spr-i ui-icons-delete"></span></a>
    </li>
</div>  
<!-- end of member item template -->

<!-- profile template -->
<div id="chatterMiniProfileTemplate" style="display:none;">
    <a href="{{ProfileURL}}" target="_blank" class="profile-link photo">
        <img class="thumbnail" src="{{ThumbWebPath}}" alt="">
    </a>
    <div class="profile-info">
        <a href="{{ProfileURL}}" target="_blank" class="profile-link username"><span>{{UserName}}</span></a>
        <a id="_ctl0__ctl4_StandardProfileTemplate1_basicInfo1_lnkIMMe" href="{{IMURL}}" class="spr-parent">
            <span class="spr {{IsOnlineCSS}}" id="onLine"><span>{{IsOnline}}</span></span>
        </a>
        <span>{{MaritalSeekingDisplayText}}</span>
        <span>{{AgeLocationDisplayText}}</span>
    </div>
    <a id="_ctl0__ctl4_SearchResultList_memberListRepeater__ctl1_MiniProfileListView1_lnkSayHi" tabindex="10" class="menu" href="{{EmailURL}}">
        <span class="spr s-icon-A-hover-comm"></span>
        <span class="title" title="Email"><mnF:Txt ID="txtIMHistorySendEMail" runat="server" ResourceConstant="TXT_IM_HISTORY_SEND_EMAIL" /></span>
    </a> 
</div>
<!-- end of profile template -->

<!-- Message templates -->
<div id="chatMessageMeContainer" style="display:none;">
    <li class="chat_message me">
        <div class="bubble_and_profile clearfix">
            <div class="profile">
                <span class="timestamp">{{MessageDateString}}</span>
            </div>
            <p class="chat_bubble bc-17">
                {{{MessageText}}}
            </p>
        </div>
    </li>
</div>



<div id="chatMessageOtherContainer" style="display:none;">
    <li class="chat_message other">
        <div class="bubble_and_profile clearfix">
            <div class="profile">
                <span class="timestamp">{{MessageDateString}}</span>
            </div>
            <p class="chat_bubble">
                {{{MessageText}}}
            </p>
        </div>
    </li>
</div>

<!-- End of Message templates -->

<!-- HELPER TEMPLATES END -->

<%--javascript im history manager--%>
<script type="text/javascript">
    var spark = spark || {};
    spark.imHistoryManager = spark.imHistoryManager ||  {
        initialized: false,
        isBlocked:  false,
        restAuthority: '<%=_RESTAuthority%>',
        brandId: <%=_BrandId%>,
        apiToken: '<%=_APItoken%>',
        memberID: <%=_MemberID%>,
        noPhotoMale: '<%=_NoPhotoMale%>',
        noPhotoFemale: '<%=_NoPhotoFemale%>',
        profileUrl: '<%=_ProfileUrl%>',
        imUrl: "<%=_IMUrl%>",
        emailUrl: '<%=_EmailUrl%>',
        noHistoryMessage: "<%=_NoHistoryMessage%>",
        maritalMtoF: "<%=_MaritalMtoF%>",
        maritalFtoM: "<%=_MaritalFtoM%>",
        currentTargetMemberID: 0,
        previousTargetMemberID: 0,
        memberListStartRow: 1,
        memberListPageSize: 1000,
        memberListTotalResultCount: 0,
        memberListUpdateInProgress: false,
        membersList: [],
        
        memberListObjTemplate: null,
        memberListTarget : null,
        memberListContainer : null,
        memberListTargetUiReady : false,

        memberProfileTemplate : null,
        memberProfileTarget: null,
        memberProfileContainer: null,
        memberProfileTargetUiReady : false,

        memberMyMessageTemplate : null,
        memberOtherMessageTemplate : null,
        memberMessageTarget: null,
        memberMessageContainer: null,
        memberMessageTargetUiReady: false,
        memberMessageScrollTimer: null,

        mainMessageContainer: null,

        messageListStartRow: 1,
        messageListPageSize: 50,
        messageListTotalResultCount: 0,
        messageListUpdateInProgress: false,
        messagesList: [],
        initialize: function () {
            //set up cors
            if (spark.cors){
                spark.cors.useFlXHRIfNeeded();
            }
            
            //initialze manager
            this.setUpTemplatePointers();
            //load member list
            if (this.membersList.length <= 0) {
                //data was not prefetched so go get data
                this.loadConversationMembersAPI();
            }

        },
        onConversationMemberClick: function(targetMemberID){
            if (targetMemberID != this.currentTargetMemberID) {
                this.log('onConversationMemberClick ' + targetMemberID);

                //set selected target memberID
                this.previousTargetMemberID = this.currentTargetMemberID;
                this.currentTargetMemberID = targetMemberID;

                //reset and clear existing conversation messages
                this.messageListStartRow = 1;
                this.memberMessageTarget.empty();

                //get and load messages
                this.loadConversationMessagesAPI();

                //reset message ui status
                this.memberMessageTargetUiReady = false;

            } else {
                this.log('onConversationMemberClick same member selected again');
            }
        },
        onConversationMemberRemoveClick: function(e, ID){                    
            $j('#memberlistItem' + ID).hide();

            $j.ajax({
                type: "GET",
                url: spark.imHistoryManager.restAuthority + "/brandId/" + spark.imHistoryManager.brandId + "/instantmessenger/removeInstantMessageMember/messageId/" + ID + "/undo/false?access_token=" + spark.imHistoryManager.apiToken,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    spark.imHistoryManager.memberListUpdateInProgress = true;
                    //spark.imHistoryManager.ajaxBlock(spark.imHistoryManager.memberListContainer);
                },
                complete: function () {
                    spark.imHistoryManager.memberListUpdateInProgress = false;
                },
                success: function (result) {
                    var MessageMemberRemoveResponse = result.hasOwnProperty('data') ? result['data'] : result;

                    if (result.code != 200) {
                        $j('#memberlistItem' + ID).show();
                        spark.imHistoryManager.log('Error removing member ID: ' + ID + ', Code: ' + result.code + ', status: ' + result.status + ', error code: ' + result.error.code + ', error message: ' + result.error.message);
                        //spark.imHistoryManager.ajaxUnBlock(spark.imHistoryManager.memberListContainer);
                        alert(result.error.message);
                    }
                    else {

                        spark.imHistoryManager.log('Member removed success: ' + MessageMemberRemoveResponse.success + ', messageID: ' + MessageMemberRemoveResponse.messageId + ', failedReason: ' + MessageMemberRemoveResponse.failReason);

                        //remove member from UI
                        $j('#memberlistItem' + ID).remove();

                        //spark.imHistoryManager.ajaxUnBlock(spark.imHistoryManager.memberListContainer);

                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    spark.imHistoryManager.log('Error removing member ID: ' + ID + ", " + textStatus);
                    //spark.imHistoryManager.ajaxUnBlock(spark.imHistoryManager.memberListContainer);
                    alert(textStatus);
                }
            });

            e = e || window.event;
            e.stopPropagation();
            e.preventDefault();

            return false;
        },
        onConversationMembersNextPage: function(){
            this.memberListStartRow = this.memberListStartRow + this.memberListPageSize;
            this.log('onConversationMemberNextPage from scrolling - new member startRow: ' + this.memberListStartRow);
            this.loadConversationMembers();
        },
        onConversationMembersMessageNextPage: function(){
            this.messageListStartRow = this.messageListStartRow + this.messageListPageSize;
            this.log('onConversationMESSAGENextPage from scrolling - new message startRow: ' + this.messageListStartRow);
            this.loadConversationMessagesAPI();
        },
        loadConversationMembersAPI: function(){ //NOTE: This loads via API
            
            //retrieve members list
            if (this.memberListStartRow <= 1 || this.memberListStartRow <= this.memberListTotalResultCount){
                if (!this.memberListUpdateInProgress) {

                    var existingStartRow = this.memberListStartRow;
                    if (this.memberListStartRow <= 0){
                        this.memberListStartRow = 1;
                    }
                    console.log("before GET");
                    
                    $j.ajax({
                        type: "GET",
                        url: spark.imHistoryManager.restAuthority + "/brandId/" + spark.imHistoryManager.brandId + "/instantmessenger/getInstantMessageHistoryMembers/true?access_token=" + spark.imHistoryManager.apiToken,
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        beforeSend: function () {
                            spark.imHistoryManager.memberListUpdateInProgress = true;
                            spark.imHistoryManager.ajaxBlock(spark.imHistoryManager.memberListContainer);
                        },
                        complete: function () {
                            spark.imHistoryManager.memberListUpdateInProgress = false;
                        },
                        success: function (result) {
                            var InstantMessageHistoryMembersResponse = result.hasOwnProperty('data') ? result['data'] : result;

                            if (result.code != 200) {
                                console.log("ajax result != 200");
                                spark.imHistoryManager.log('Code: ' + result.code + ', status: ' + result.status + ', error code: ' + result.error.code + ', error message: ' + result.error.message);
                                spark.imHistoryManager.ajaxUnBlock(spark.imHistoryManager.memberListContainer);
                                alert(result.error.message);
                            }
                            else {
                                spark.imHistoryManager.log('Total found: ' + InstantMessageHistoryMembersResponse.MatchesFound + ', Batch returned: ' + InstantMessageHistoryMembersResponse.MemberList.length);

                                //load members
                                var existingTotalResultCount = spark.imHistoryManager.memberListTotalResultCount;
                                spark.imHistoryManager.memberListTotalResultCount = InstantMessageHistoryMembersResponse.MatchesFound;
                                if (InstantMessageHistoryMembersResponse.MemberList.length > 0) {

                                    var batchMemberList = [];
                                    for (var i = 0; i < InstantMessageHistoryMembersResponse.MemberList.length; i++) {

                                        var marital = InstantMessageHistoryMembersResponse.MemberList[i].Member.maritalStatus;
                                        if (marital == null || marital == ''){
                                            marital = '';
                                        }else{
                                            marital = marital + ', ';
                                        }
                                        var dtoMemberListItem = {
                                            ID: InstantMessageHistoryMembersResponse.MemberList[i].Id,
                                            MemberID: InstantMessageHistoryMembersResponse.MemberList[i].MemberId,
                                            UserName: InstantMessageHistoryMembersResponse.MemberList[i].Member.username,
                                            ThumbWebPath: (InstantMessageHistoryMembersResponse.MemberList[i].Member.ThumbnailUrl) ? InstantMessageHistoryMembersResponse.MemberList[i].Member.ThumbnailUrl : '',
                                            AgeLocationDisplayText: InstantMessageHistoryMembersResponse.MemberList[i].Member.age + ', ' + InstantMessageHistoryMembersResponse.MemberList[i].Member.location,
                                            LastMessageDateString: spark.imHistoryManager.getDateDisplayNotReallyUTC(new Date(InstantMessageHistoryMembersResponse.MemberList[i].InsertDatePst)),
                                            IsOnline: InstantMessageHistoryMembersResponse.MemberList[i].Member.isOnline,
                                            //MaritalSeekingDisplayText: marital + InstantMessageHistoryMembersResponse.MemberList[i].Member.gender +' seeking a ' + InstantMessageHistoryMembersResponse.MemberList[i].Member.seekingGender,
                                        };

                                        if (InstantMessageHistoryMembersResponse.MemberList[i].Member.gender == 'Male'){
                                            dtoMemberListItem.MaritalSeekingDisplayText = marital + spark.imHistoryManager.maritalMtoF;
                                        }
                                        else{
                                            dtoMemberListItem.MaritalSeekingDisplayText = marital + spark.imHistoryManager.maritalFtoM;
                                        }

                                        if (dtoMemberListItem.ThumbWebPath == '' || dtoMemberListItem.ThumbWebPath == null || (typeof dtoMemberListItem.ThumbWebPath == 'undefined')){
                                            dtoMemberListItem.ThumbWebPath = (InstantMessageHistoryMembersResponse.MemberList[i].Member.gender == 'Male') ? spark.imHistoryManager.noPhotoMale : spark.imHistoryManager.noPhotoFemale;
                                        }
                                        spark.imHistoryManager.membersList.push(InstantMessageHistoryMembersResponse.MemberList[i].MemberId);
                                        batchMemberList.push(dtoMemberListItem);
                                    }

                                    //load messages for first member
                                    if (spark.imHistoryManager.currentTargetMemberID <= 0){
                                        spark.imHistoryManager.currentTargetMemberID = spark.imHistoryManager.membersList[0];
                                        spark.imHistoryManager.loadConversationMessagesAPI();
                                    }

                                    //render members
                                    spark.imHistoryManager.log('Rendering members page ' + spark.imHistoryManager.getCurrentMemberListPageNumber());
                                    spark.imHistoryManager.renderConversationMembers(batchMemberList);  
                                }
                                else{
                                    //no results or no more results
                                    spark.imHistoryManager.log('No more member results found');
                                    if (spark.imHistoryManager.membersList.length <= 0){
                                        //
                                        spark.imHistoryManager.memberListTarget.append("<li>" + spark.imHistoryManager.noHistoryMessage + "</li>");
                                    }
                                }

                                spark.imHistoryManager.ajaxUnBlock(spark.imHistoryManager.memberListContainer);

                                //note: member list pagination not currently supported in API
                                //spark.imHistoryManager.uiScrollInfinite(spark.imHistoryManager.memberListTarget);

                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            spark.imHistoryManager.log('Error getting member list: ' + textStatus);
                            spark.imHistoryManager.memberListStartRow = existingStartRow;
                            spark.imHistoryManager.ajaxUnBlock(spark.imHistoryManager.memberListContainer);
                            alert(textStatus);
                        }
                    });
                }
            }
            else{
                //no more results
                this.log('Already on last member page');
                spark.imHistoryManager.ajaxUnBlock(spark.imHistoryManager.memberListContainer);
            }

        },
        loadConversationMessagesAPI: function () { //Note: This loads via API
            //retrieve members list
            if (this.messageListStartRow <= 1 || this.messageListStartRow <= this.messageListTotalResultCount){
                if (!this.messageListUpdateInProgress) {

                    var existingStartRow = this.messageListStartRow;
                    if (this.messageListStartRow <= 0){
                        this.messageListStartRow = 1;
                    }

                    var pageNumber = this.getCurrentMessageListPageNumber();
                    var apiIgnoreCache = "";
                    if (pageNumber <= 1)
                    {
                        apiIgnoreCache = "/true";
                    }
                    
                    $j.ajax({
                        type: "GET",
                        url: spark.imHistoryManager.restAuthority + "/brandId/" + spark.imHistoryManager.brandId + "/instantmessenger/getInstantMessageHistory/targetMemberID/" + spark.imHistoryManager.currentTargetMemberID + "/page/" + pageNumber + "/pagesize/" + spark.imHistoryManager.messageListPageSize + apiIgnoreCache + "?access_token=" + spark.imHistoryManager.apiToken,
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        beforeSend: function () {
                            spark.imHistoryManager.messageListUpdateInProgress = true;
                            spark.imHistoryManager.ajaxBlock(spark.imHistoryManager.mainMessageContainer);
                        },
                        complete: function () {
                            spark.imHistoryManager.messageListUpdateInProgress = false;
                        },
                        success: function (result) {
                            var InstantMessageHistoryResponse = result.hasOwnProperty('data') ? result['data'] : result;

                            if (result.code != 200) {
                                spark.imHistoryManager.log('Code: ' + result.code + ', status: ' + result.status + ', error code: ' + result.error.code + ', error message: ' + result.error.message);
                                spark.imHistoryManager.ajaxUnBlock(spark.imHistoryManager.mainMessageContainer);
                                alert(result.error.message);
                            }
                            else {

                                spark.imHistoryManager.log('Total found: ' + InstantMessageHistoryResponse.MatchesFound + ', Batch returned: ' + InstantMessageHistoryResponse.MessageList.length);

                                //load messages
                                var existingTotalResultCount = spark.imHistoryManager.messageListTotalResultCount;
                                spark.imHistoryManager.messageListTotalResultCount = InstantMessageHistoryResponse.MatchesFound;
                                if (InstantMessageHistoryResponse.MessageList.length > 0) {
                                    //render messages
                                    spark.imHistoryManager.log('Rendering messages page ' + pageNumber);
                                    spark.imHistoryManager.renderConversationMessages(InstantMessageHistoryResponse);
                                }
                                else{
                                    //no results or no more results
                                    spark.imHistoryManager.log('No more results found');
                                }
                            }

                            //spark.imHistoryManager.ajaxUnBlock(spark.imHistoryManager.mainMessageContainer);
                            //spark.imHistoryManager.uiScrollInfinite(spark.imHistoryManager.memberMessageTarget);
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            spark.imHistoryManager.log('Error getting next batch: ' + textStatus);
                            spark.imHistoryManager.messageListStartRow = existingStartRow;
                            spark.imHistoryManager.ajaxUnBlock(spark.imHistoryManager.mainMessageContainer);
                        }
                    });
                }
            }
            else{
                //no more results
                this.log('Already on last page');
            }

        },
        renderConversationMembers: function (IMHistoryMemberList) {  //Note: this method doesnt NOT handle blocking and unblocking of the element whoes inner html is being manipulated, its up to the caller of this method to implement that
            var htmlForAll = '';

            if (IMHistoryMemberList.length > 0) {
                IMHistoryMemberList.forEach(function (entry) {
                    //spark.imHistoryManager.log(entry);
                    var html = Mustache.to_html(spark.imHistoryManager.memberListObjTemplate, entry);

                    htmlForAll += html; // reduce DOM rendering
                });

                spark.imHistoryManager.memberListTarget.append(htmlForAll);

                if(!spark.imHistoryManager.memberListTargetUiReady){
                    spark.imHistoryManager.uiBindingMember();
                }

            } else {
                this.log('WARNING Empty IMHistoryMemberList sent');
            }
                
        },
        renderConversationMessages: function (InstantMessageHistoryResponse) {
            //render list of messages for selected member via templating

            if (InstantMessageHistoryResponse != null && InstantMessageHistoryResponse.MatchesFound > 0) {
                
                var memberListItemDataObj = $j('#memberlistItemData' + InstantMessageHistoryResponse.TargetMemberId);
                if (memberListItemDataObj){
                    var dataObj = memberListItemDataObj.data();
                    var dtoTargetProfile = {
                        ProfileURL: this.profileUrl.replace('{memberid}', dataObj.memberid),
                        ThumbWebPath: dataObj.thumbwebpath,
                        UserName: dataObj.username,
                        IMURL: (dataObj.isonline == true) ? this.imUrl.replace('{memberid}', dataObj.memberid).replace('{username}', dataObj.username) : 'javascript:void(0)',
                        IsOnlineCSS: (dataObj.isonline == true) ? 's-icon-status-online-sm' : 's-icon-status-offline-sm',
                        MaritalSeekingDisplayText: dataObj.maritalseeking,
                        AgeLocationDisplayText: dataObj.agelocation,
                        EmailURL: this.emailUrl.replace('{memberid}', dataObj.memberid)
                    };

                    if (this.getCurrentMessageListPageNumber() <= 1){
                        //load profile
                        var htmlForProfile = Mustache.to_html(this.memberProfileTemplate, dtoTargetProfile);
                        this.memberProfileTarget.html(htmlForProfile);
                        this.log('Loaded mini-profile for ' + this.currentTargetMemberID);
                    }
                }
                
                // lets load the messages
                this.renderConversationMessages2(InstantMessageHistoryResponse.MessageList);
            } else {
                this.log('WARNING Empty imHistoryMessageListResult sent');
            }   

        },
        renderConversationMessages2: function(IMHistoryMessageList) {
            
            if(IMHistoryMessageList.length > 0) {

                IMHistoryMessageList.forEach(function (entry) {
                    //spark.imHistoryManager.log(entry);

                    var dtoMessage = {
                        SenderId: entry.senderId,
                        MessageText: entry.body,
                        MessageDateString: spark.imHistoryManager.getDateDisplayNotReallyUTC(new Date(entry.insertDatePst))
                    };

                    dtoMessage.MessageText = spark.messageconversion.convertCustomTagToHtml(dtoMessage.MessageText);  //clean up the message 
                    
                    var htmlForMessages = null;
                    
                    if(dtoMessage.SenderId == spark.imHistoryManager.memberID){
                        htmlForMessages = Mustache.to_html(spark.imHistoryManager.memberMyMessageTemplate, dtoMessage);
                    } else {
                        htmlForMessages = Mustache.to_html(spark.imHistoryManager.memberOtherMessageTemplate, dtoMessage);
                    }

                    spark.imHistoryManager.memberMessageTarget.prepend(htmlForMessages);
                    
                });

                spark.imHistoryManager.uiBindingMessage();
                spark.imHistoryManager.uiScrollInfinite(spark.imHistoryManager.memberMessageTarget);
                spark.imHistoryManager.ajaxUnBlock(spark.imHistoryManager.mainMessageContainer);
            }

        },
        setUpTemplatePointers: function() {

            this.memberListObjTemplate = $j('#memberlistItemTemplate').html();
            this.memberListTarget = $j('#standardList');
            this.memberListContainer = $j("#listCont");

            this.memberProfileTemplate = $j('#chatterMiniProfileTemplate').html();
            this.memberProfileTarget = $j('#chatterMiniProfile');
            
            this.memberMyMessageTemplate = $j('#chatMessageMeContainer').html();
            this.memberOtherMessageTemplate = $j('#chatMessageOtherContainer').html();
            this.memberMessageTarget = $j('#chatterMessages');

            this.mainMessageContainer =  $j('#messageCont');

        },
        uiBindingMember: function(){
            if(!spark.imHistoryManager.memberListTargetUiReady){
                var memberList = spark.imHistoryManager.memberListTarget.find('>li');
                
                memberList.first().addClass('active');
                
                memberList.on('click', function(e){
                    var $this = $j(this),
                        memberId = $this.find('> div').data('memberid');
                    
                    memberList.removeClass();
                    $this.addClass('active');
                    spark.imHistoryManager.onConversationMemberClick(memberId);
                });

                spark.imHistoryManager.memberListTargetUiReady = true;
            }
        },
        uiBindingMessage: function(){
            var messageList = spark.imHistoryManager.memberMessageTarget,
                numBottom = messageList[0].scrollHeight,
                numPage = messageList[0].scrollHeight / spark.imHistoryManager.getCurrentMessageListPageNumber();

            messageList.imagesLoaded(function(){
                if(!spark.imHistoryManager.memberMessageTargetUiReady){
                    // scroll to the bottom of the list at initial load
                    messageList.scrollTop(numBottom * 10);

                    spark.imHistoryManager.memberMessageTargetUiReady = true;
                } else {
                    // scroll to the top of the last messages
                    messageList.scrollTop(numPage);
                }
            });
        },
        uiScrollInfinite: function(obj){
            var objHeight = obj.height(),
                objScrollNum = obj[0].scrollHeight - objHeight;

            obj.unbind('scroll').debounce('scroll', function() {
                // infinite scroll binding for messages
                spark.imHistoryManager.log('uiScrollInfinite() unbind and debounce scroll for ' + obj.attr('id'));
                if(obj.attr('id') == 'chatterMessages'){
                    if(spark.imHistoryManager.memberMessageTargetUiReady && obj.scrollTop() == 0){
                        obj.unbind('scroll');
                        spark.imHistoryManager.onConversationMembersMessageNextPage();
                    }
                } else {
                    if(objScrollNum == obj.scrollTop() || objScrollNum == (obj.scrollTop() - 1)){
                        obj.unbind('scroll');
                        //console.log('scrollHeight: ', obj[0].scrollHeight, 'objHeight: ', objHeight, 'obj.scrollTop(): ', obj.scrollTop());
                        spark.imHistoryManager.onConversationMembersNextPage();
                    }
                }
                
            }, 250);

        },
        log: function(message) {
            if (typeof(console) !== "undefined") {
                console.log(message);
            }
        },
        getDateDisplayNotReallyUTC: function (date) {
            //the date here is passed in as UTC, but it's really PST
            if (date){
                var hours = date.getUTCHours();
                var minutes = date.getUTCMinutes();
                var ampm = hours >= 12 ? 'pm' : 'am';
                hours = hours % 12;
                hours = hours ? hours : 12; // the hour '0' should be '12'
                minutes = minutes < 10 ? '0' + minutes : minutes;
                var strTime = hours + ':' + minutes + ' ' + ampm;
                var dateString = "";
                if(spark.imHistoryManager.brandId=="1004")
                    dateString = date.getUTCDate() + "/" + (date.getUTCMonth() + 1) + "/" + date.getUTCFullYear() + "  " + strTime;
                else 
                    dateString = date.getUTCMonth() + 1 + "/" + date.getUTCDate() + "/" + date.getUTCFullYear() + "  " + strTime;
            }
            return dateString;
        },
        getCurrentMemberListPageNumber: function(){
            var currentPage = 1;
            if (this.membersList.length > this.memberListPageSize){
                currentPage = Math.ceil(this.membersList.length / this.memberListPageSize);
            }
            
            return currentPage;
        },
        getCurrentMessageListPageNumber: function(){
            var currentPage = 1;
            if ((this.messageListStartRow +  this.messageListPageSize - 1) > this.messageListPageSize){
                currentPage = Math.ceil((this.messageListStartRow +  this.messageListPageSize - 1) / this.messageListPageSize);
            }
            return currentPage;
        },
        ajaxBlock: function (blockObj) {
            if (!this.isBlocked) {
                this.isBlocked = true;
                var blockParams = {
                    message: '<div class="loading spinner-only" />',
                    centerY: false,
                    css: { backgroundColor: 'transparent', border: 'none', height: '85px', top: '30%' },
                    overlayCSS: { backgroundColor: '#ddd', opacity: 0.8 }
                };
                blockObj instanceof jQuery ? blockObj.block(blockParams) : $j(blockObj).block(blockParams); //blockUI jquery plugin
            }
        },
        ajaxUnBlock: function (blockObj) {
            if (this.isBlocked) {
                blockObj instanceof jQuery ? blockObj.unblock() : $j(blockObj).unblock();
            }
            this.isBlocked = false;
        },

    }

    if (!spark.imHistoryManager.initialized) {
        $j(function () {
            //temp override path to use prod url for our hard coded IM history messages/photos
            //spark.messageconversion.messageAttachmentURLPath = 'http://share.sparksites.com';

            spark.imHistoryManager.initialize();
            spark.imHistoryManager.initialized = true;

        });
    }
</script>