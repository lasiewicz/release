﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Messages.ascx.cs" Inherits="Matchnet.Web.Applications.Email.Messages" %>

<div id="membersTemplate" style="display:none;">
    <ul>
    {{#membersList}}
        <li class="clearfix" id="member_row_{{memberID}}">
            <a href="#" title="{{UserName}}" class="img"><img src="{{PhotoURL}}" id="imageThumb_{{memberID}}" alt="{{UserName}}" class="mail-thumb" ></a>
            <div class="info">
                <a href="#" title="{{UserName}}">{{UserName}}</a>
                <span class="date">{{MessageDate}}</span>
                <p>{{MessageText}}</p>
            </div>
        </li>
    {{/membersList}}
    </ul>
    
</div>

<div id="messagesTemplate" style="display:none;">

    <ul>
    {{#messagesList}}
        <li class="clearfix">
            {{#directionFlag}}
            <a href="#" title="{{UserName}}" class="img"><img src="{{PhotoURL}}" id="Img1" alt="{{UserName}}" class="mail-thumb" ></a>
            {{/directionFlag}} 
            <div class="info">
                <a href="#" title="{{UserName}}">{{UserName}}</a>
                <span class="date">{{MessageDate}}</span>
                <p>{{MessageText}}</p>
            </div>
        </li>
    {{/messagesList}}
    </ul>
    
</div>


<div id="members_div" class="inbox">
    <div class="list">
        <h2>Standard email</h2>
    </div>
    <div class="content">
    
    </div>
</div>
<div id="messages_div" class="inbox" style="display:none;">
     <h2>Messages with <label id="messagesUserName">test User Name</label></h2>
     <div class="list">
    </div>
    <div class="content">
    
    </div>
</div>
<script type="text/javascript">

    this.loadMessages = function () {
    
        alert('loadMessages');
    };

    $j(function () {
        var membersTemplate, membersHtml, membersVar, messagesTemplate, messagesHtml, messagesVar;

        membersTemplate = $j('#membersTemplate').html();

        messagesTemplate = $j('#messagesTemplate').html();


        //BEGIN For testing:  if you want to test with specific data, add the data here and uncomment:
        membersVar = {
            "membersList": [
            //DEV photo URL:
                {memberID: "168153746", UserName: "UserName222", MessageDate: "05/29/2013 07:36 AM", MessageText: "Brief message goes here...", PhotoURL: "/Photo1000/2012/11/28/11/120198247.jpg" },
                { memberID: "120198247", UserName: "UserName135", MessageDate: "03/08/2013 05:28 PM", MessageText: "Brief message goes here...", PhotoURL: "/Photo1000/2012/11/28/11/120198247.jpg" },
                { memberID: "120198247", UserName: "UserName34", MessageDate: "03/07/2013 04:28 PM", MessageText: "Brief message goes here...", PhotoURL: "/Photo1000/2012/11/28/11/120198247.jpg" },
                { memberID: "120198247", UserName: "UserName456", MessageDate: "02/06/2013 02:45 PM", MessageText: "Brief message goes here...", PhotoURL: "/Photo1000/2012/11/28/11/120198247.jpg" },
                { memberID: "120198247", UserName: "UserName89", MessageDate: "02/05/2013 03:21 PM", MessageText: "Brief message goes here...", PhotoURL: "/Photo1000/2012/11/28/11/120198247.jpg" },
                { memberID: "120198247", UserName: "UserName567", MessageDate: "01/22/2013 07:05 PM", MessageText: "Brief message goes here...", PhotoURL: "/Photo1000/2012/11/28/11/120198247.jpg" }
            //Stage photo URL:       
//              { memberID: "168153746", UserName: "UserName222", MessageDate: "05/29/2013 07:36 AM", MessageText: "Brief message goes here...", PhotoURL: "/Photo10000/2013/05/23/16/221210580.jpg" },
//              { memberID: "120198247", UserName: "UserName802", MessageDate: "03/08/2013 05:28 PM", MessageText: "Brief message goes here...", PhotoURL: "/Photo10000/2013/05/23/16/221210580.jpg" },
//              { memberID: "120198247", UserName: "UserName34", MessageDate: "03/07/2013 04:28 PM", MessageText: "Brief message goes here...", PhotoURL: "/Photo10000/2013/05/23/16/221210580.jpg" },
//              { memberID: "120198247", UserName: "UserName56", MessageDate: "02/06/2013 02:45 PM", MessageText: "Brief message goes here...", PhotoURL: "/Photo10000/2013/05/23/16/221210580.jpg" },
//              { memberID: "120198247", UserName: "UserName89", MessageDate: "02/05/2013 03:21 PM", MessageText: "Brief message goes here...", PhotoURL: "/Photo10000/2013/05/23/16/221210580.jpg" },
//              { memberID: "120198247", UserName: "UserName1000", MessageDate: "01/22/2013 07:05 PM", MessageText: "Brief message goes here...", PhotoURL: "/Photo10000/2013/05/23/16/221210580.jpg" }
            ]
        };
        //END For testing

        membersHtml = Mustache.render(membersTemplate, membersVar);
        $j('#members_div .list').append(membersHtml);

        //When click on member row Gets the wildcard ID starting with member_row:
        $j('[id^=member_row]').on('click', function (e) {
            //Get the username off the anchor title:
            var UserName = $j('a', this).attr('title');
            var lineID = $j(this).attr("id");
            var toRemove = 'member_row_';
            var toMemberID = lineID.replace(toRemove, '');  //Gets the toMemberID which will be needed to call the API method

            //BEGIN For testing:  if you want to test with specific data, add the data here and uncomment:
            messagesVar = {
                "messagesList": [
                //DEV photo URL:
                //Message.DirectionFlag  looks to be 0 if the Message.MemberID is sending.  1 if Message.TargetMemberID is sending.
                //DirectionFlag is correct to use if the current user (logged in user) is Message.MemberID.  If the current user (logged in user) is Message.TargetMemberID
                //then need to reverse DirectionFlag.  This should be done in the stored proc when retrieving.
                        { memberID: "168153746", directionFlag: true, UserName: "UserName222", MessageDate: "05/29/2013 07:36 AM", MessageText: "Entire message text Entire message text Entire message text Entire message text", PhotoURL: "/Photo1000/2012/11/28/11/120198247.jpg" },
                        { memberID: "120191247", directionFlag: false, UserName: "UserName1", MessageDate: "03/08/2013 05:28 PM", MessageText: "Entire message text Entire message text Entire message text Entire message text", PhotoURL: "/Photo1000/2012/11/28/11/120198247.jpg" },
                        { memberID: "120298247", directionFlag: true, UserName: "UserName34", MessageDate: "03/07/2013 04:28 PM", MessageText: "Entire message text Entire message text Entire message text Entire message text", PhotoURL: "/Photo1000/2012/11/28/11/120198247.jpg" },
                        { memberID: "120198347", directionFlag: false, UserName: "UserName1", MessageDate: "02/06/2013 02:45 PM", MessageText: "Entire message text Entire message text Entire message text Entire message text", PhotoURL: "/Photo1000/2012/11/28/11/120198247.jpg" },
                        { memberID: "124198247", directionFlag: true, UserName: "UserName89", MessageDate: "02/05/2013 03:21 PM", MessageText: "Entire message text Entire message text Entire message text Entire message text", PhotoURL: "/Photo1000/2012/11/28/11/120198247.jpg" },
                        { memberID: "120198547", directionFlag: false, UserName: "UserName1", MessageDate: "01/22/2013 07:05 PM", MessageText: "Entire message text Entire message text Entire message text Entire message text", PhotoURL: "/Photo1000/2012/11/28/11/120198247.jpg" }
                //Stage photo URL:           
                //      { memberID: "168153746", UserName: "UserName222", MessageDate: "05/29/2013 07:36 AM", MessageText: "Entire message text Entire message text Entire message text Entire message text Entire message text Entire message text Entire message text Entire message text Entire message text Entire message text Entire message text Entire message text Entire message text Entire message text Entire message text Entire message text", PhotoURL: "/Photo10000/2013/05/23/16/221210580.jpg" },
                //      { memberID: "120198247", UserName: "UserName802", MessageDate: "03/08/2013 05:28 PM", MessageText: "Entire message text Entire message text Entire message text Entire message text", PhotoURL: "/Photo10000/2013/05/23/16/221210580.jpg" },
                //      { memberID: "120198247", UserName: "UserName34", MessageDate: "03/07/2013 04:28 PM", MessageText: "Entire message text Entire message text Entire message text Entire message text", PhotoURL: "/Photo10000/2013/05/23/16/221210580.jpg" },
                //      { memberID: "120198247", UserName: "UserName56", MessageDate: "02/06/2013 02:45 PM", MessageText: "Entire message text Entire message text Entire message text Entire message text", PhotoURL: "/Photo10000/2013/05/23/16/221210580.jpg" },
                //      { memberID: "120198247", UserName: "UserName89", MessageDate: "02/05/2013 03:21 PM", MessageText: "Entire message text Entire message text Entire message text Entire message text", PhotoURL: "/Photo10000/2013/05/23/16/221210580.jpg" },
                //      { memberID: "120198247", UserName: "UserName1000", MessageDate: "01/22/2013 07:05 PM", MessageText: "Entire message text Entire message text Entire message text Entire message text", PhotoURL: "/Photo10000/2013/05/23/16/221210580.jpg" }
                    ]
            };
            //END For testing

            //Loads the messages div:
            messagesHtml = Mustache.render(messagesTemplate, messagesVar);
            $j('#messages_div .list').html("");
            $j('#messages_div .list').append(messagesHtml);

            //Set user name on the messages header:
            $j('#messagesUserName').text(UserName);
            $j('#messages_div').show();

            // loadMessages();

        });

    });

</script> 
