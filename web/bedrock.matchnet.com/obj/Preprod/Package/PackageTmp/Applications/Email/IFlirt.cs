﻿using System;
using System.Web.UI.WebControls;
using System.Text;
using Matchnet.Member.ValueObjects;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Ui.BasicElements;

using Matchnet.Email.ServiceAdapters;
using Matchnet.Email.ValueObjects;
using Matchnet.ExternalMail.ServiceAdapters;
using Matchnet.List.ServiceAdapters;
using Matchnet.List.ValueObjects;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Session.ValueObjects;

namespace Matchnet.Web.Applications.Email
{
    public interface IFlirt
    {

        Matchnet.Member.ServiceAdapters.Member Member { get; }
        System.Web.UI.WebControls.Repeater RepTeaseCategories { get; }
        System.Web.UI.WebControls.RadioButtonList RblTeaseContent { get; }


        FrameworkControl ResourceControl { get; }
        
    }
}
