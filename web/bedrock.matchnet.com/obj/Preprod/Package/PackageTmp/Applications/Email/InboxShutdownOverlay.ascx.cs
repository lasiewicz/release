﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Web.Framework;

namespace Matchnet.Web.Applications.Email
{
    public partial class InboxShutdownOverlay : FrameworkControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            lnkSub.NavigateUrl = "/Applications/Subscription/Subscribe.aspx?prtid=1248";
        }
    }
}