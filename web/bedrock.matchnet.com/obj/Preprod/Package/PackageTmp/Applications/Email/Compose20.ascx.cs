﻿#region System References

using System;
using System.Web;
using System.Collections;
using System.Text.RegularExpressions;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ServiceAdapters.Links;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Content.ValueObjects.PageConfig;
using Matchnet.Content.ValueObjects.Region;
using Matchnet.Email.ServiceAdapters;
using Matchnet.Email.ValueObjects;
using Matchnet.ExternalMail.ServiceAdapters;
using Matchnet.List.ServiceAdapters;
using Matchnet.List.ValueObjects;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects;
using Matchnet.Session.ValueObjects;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.JavaScript;
using Matchnet.Web.Framework.Ui.BasicElements;
using Matchnet.Web.Framework.Util;

#endregion

#region Matchnet Web App References
using Matchnet.Web.Applications.ColorCode;
using Matchnet.Web.Framework.Ui.FormElements;
using Matchnet.UserNotifications.ServiceAdapters;
using Matchnet.UserNotifications.ValueObjects;
using Matchnet.Web.Applications.UserNotifications;
using Matchnet.Web.Applications.MemberProfile;
using Matchnet.Web.Framework.Ui;
#endregion

namespace Matchnet.Web.Applications.Email
{
    public partial class Compose20 : FrameworkControl, ICompose
    {
        public static string MESSAGE_ORIGIN_URL_SESSION_KEY = "MessageOriginUrl";
        protected bool memberValid = false;
        protected bool _isReply = false;
        protected int _memberMailID = int.MinValue;
        #region ICompose implementation

        public bool SendAsVIP { get { return cbxSendAsVIP.Checked; } set { cbxSendAsVIP.Checked = value; } }
        public PlaceHolder PHSendAsVIP { get { return plcSendAsVIP; } }
        public PlaceHolder PHSendVIPOverlay { get { return phSendVIPOverlay; } }
        public Button BtnSendVIPMessage { get { return sendVIPMessageButton; } }
        public Txt VIPOverlayText { get { return txtOverlay; } }
        public Button BtnVIPMessageOverlayYes { get { return btnYes; } }
        public Button BtnVIPMessageOverlayNo { get { return btnNo; } }
        public PlaceHolder PHSendAsFREEReplyVIP { get { return phVIPFreeReply; } }

        public bool IsReply { get { return _isReply; } set { _isReply = value; } }

        public Title TtlCompose { get { return ttlCompose; } }
        public Repeater RptMemberProfile { get { return memberProfile; } }
        public Panel PnlToMemberTextField { get { return null; } }
        public TextBox TxtToTextField { get { return toTextField; } }
        public TextBox TxtSubjectTextField { get { return subjectTextField; } }
        public TextBox TxtMessageBodyTextField { get { return messageBodyTextField; } }
        public Button BtnSaveDraftButton { get { return saveDraftButton; } }
        public Button BtnSendMessageButton { get { return sendMessageButton; } }
        public Table TblViewProfile { get { return null; } }
        public Label LblMemberProfileUserName { get { return memberProfileUserName; } }
        public Panel LblMemberProfileUserNamePanel { get { return null; } }
        public Table TblToHeader { get { return null; } }

        public Table TblComposeMessage { get { return null; } }
        public HtmlAnchor HrefMailToList { get { return hrefMailToList; } }
        public PlaceHolder PhViewProfileInfo { get { return phViewProfileInfo; } }
        public TableRow TrViewProfile { get { return null; } }
        public FirstElementFocus FefControl { get { return null; } }
        public PlaceHolder PhClassicProfileDetail { get { return phClassicProfileDetail; } }
        public PlaceHolder PhTabbedProfileDetail { get { return phTabbedProfileDetail; } }

        public Label LBLVIPCheckbox { get { return lblVIPRemaining; } }
        public CheckBox VIPCheckbox { get { return cbxSendAsVIP; } }
        public Table ViewProfileTable { get { return null; } }

        #region for compose project
        public CheckBox CbSaveCopyCheckBox { get { return saveCopyCheckBox; } }
        public CheckBox CbSendPrivatePhotos { get { return cbSendPrivatePhotos; } }
        public bool MemberValid { get { return memberValid; } set { memberValid = value; } }
        public TableRow RowSaveCopyCheckBox { get { return null; } }
        public PlaceHolder PhRegistrantMessage { get { return null; } }
        public TableCell TcComposeMessage { get { return null; } }

        
        
        public int MemberMailID { get { return _memberMailID; } set { _memberMailID = value; } }

        public int MessageID
        {
            get
            {
                int theMessageID = Constants.NULL_INT;

                var messageIDFromQString = Request.QueryString["MemberMailID"];
                

                if (messageIDFromQString != null && Int32.TryParse(messageIDFromQString, out theMessageID))
                {
                    return theMessageID;
                }
                    
               
               return theMessageID;
            }
        }
        
        protected int TargetMemberID
        {
            get {
                if (_handler != null)
                    return _handler.GetToMemberID(plcMailToList.Visible);
                else
                    return Constants.NULL_INT;
                } 
        }

        protected string DefaultSubject
        {
            get { return g.GetResource("TXT_DEFAULT_SUBJECT", this); }
        }

        protected string DefaultBodyMessage
        {
            get { return g.GetResource("TXT_DEFAULT_BODY_MESSAGE", this); }
        }

        //
        
        #endregion

        public FrameworkControl ResourceControl { get { return this; } }
        public bool IsPagePostBack { get { return IsPostBack; } }
        protected Color _Color = Color.none;
        protected string _ColorText = "";
        protected string _ColorCodePageCSS = "";

        public Label GetToLabel()
        {
            return toLabel;

        }

        public bool SaveSendMessage()
        {
            //No longer have saveCopyCheckBox checkbox so save is always true:
            bool save = true;

            return save;
        }

        public bool SendPrivatePhotos()
        {

            if (_g.Brand.GetStatusMaskValue(StatusType.PrivatePhotos))
            {
                if (cbSendPrivatePhotos.Checked)
                {
                    return true;
                }
            }
            return false;
        }
        #endregion

        protected ComposeHandler _handler = null;
        private void Page_Init(object sender, EventArgs e)
        {
            try
            {
                _handler = new ComposeHandler(this, g);
                _handler.InitPage();

                bool isIcebreakerEnabled = Conversion.CBool(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("SHOW_ICEBREAKER_ON_COMPOSE_PAGE", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID));
                if (isIcebreakerEnabled)
                {
                    IceBreakerLink.ProfileMember = Matchnet.Member.ServiceAdapters.MemberSA.Instance.GetMember(_handler.GetToMemberID(plcMailToList.Visible), MemberLoadFlags.None);
                    IceBreakerLink.Visible = true;
                }



            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        protected void onPage_Load(object sender, EventArgs e)
        {
            Page_Load(sender, e);
        }

        private void Page_Load(object sender, EventArgs e)
        {

            try
            {
                _handler.ValidMemberAccess();

                handlePrivatePhotos();
                handleSendAsVIP();

                g.BreadCrumbTrailHeader.SetThreeLinkCrumb(g.GetResource("TXT_MESSAGES", this), g.AppPage.App.DefaultPagePath
                    , g.GetResource("TXT_COMPOSE_MESSAGE", this), string.Empty);
                g.BreadCrumbTrailFooter.SetThreeLinkCrumb(g.GetResource("TXT_MESSAGES", this), g.AppPage.App.DefaultPagePath
                    , g.GetResource("TXT_COMPOSE_MESSAGE", this), string.Empty);

                if (!IsPostBack)
                {
                    // Add the message origination URL to the MESSAGE_ORIGIN_URL_SESSION_KEY,
                    // include the appropriate startrow param, but remove any existing ones in the url
                    AddStartRowToSessionURL();
                }

                if (phClassicProfileDetail != null)
                    phClassicProfileDetail.Visible = false;

                if (phTabbedProfileDetail != null)
                    phTabbedProfileDetail.Visible = false;

                _handler.LoadPage(IsPostBack);

                #region Color Code
                if (ColorCodeHelper.IsColorCodeEnabled(g.Brand) && g.Brand.Site.Community.CommunityID == (int)WebConstants.COMMUNITY_ID.Spark)
                {
                    _ColorCodePageCSS = " color-code";
                    int toMemberID = _handler.GetToMemberID(plcMailToList.Visible);
                    if (toMemberID != Constants.NULL_INT)
                    {
                        Member.ServiceAdapters.Member toMember = MemberSA.Instance.GetMember(toMemberID, MemberLoadFlags.None);
                        if (toMember != null && !ColorCodeHelper.IsMemberColorCodeHidden(toMember, g.Brand) && ColorCodeHelper.HasMemberCompletedQuiz(toMember, g.Brand))
                        {
                            _Color = ColorCodeHelper.GetPrimaryColor(toMember, g.Brand);
                            _ColorText = ColorCodeHelper.GetFormattedColorText(_Color);

                            phColorCodeTips.Visible = true;
                            switch (_Color)
                            {
                                case Color.blue:
                                    phBlueTips.Visible = true;
                                    break;
                                case Color.red:
                                    phRedTips.Visible = true;
                                    break;
                                case Color.white:
                                    phWhiteTips.Visible = true;
                                    break;
                                case Color.yellow:
                                    phYellowTips.Visible = true;
                                    break;
                            }
                        }
                    }
                }
                #endregion

                // check against fraud tool before we let this user send anything? for replies, we don't need to check against the
                // fraud tool.
                if (!_isReply)
                {
                    bool passedFraud = ComposeHelper.PassesIMailFraudCheck(g);
                    if (!passedFraud)
                    {
                        phComposeEmail.Visible = false;
                        g.Notification.AddMessage("NOT_PASSED_FRAUD");
                    }
                }


                bool showMicroProfile = Conversion.CBool(RuntimeSettings.GetSetting("SHOW_MICRO_PROFILE_ON_COMPOSE_MESSAGE_PAGE", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID));
                if (showMicroProfile)
                {
                    //xxxxx
                    // micromicroprofile
                    this.microMicroProfile.Visible = true;
                    this.microMicroProfile.member = Matchnet.Member.ServiceAdapters.MemberSA.Instance.GetMember(_handler.GetToMemberID(plcMailToList.Visible), MemberLoadFlags.None);
                    //this.microMicroProfile.TxtToLabel.Visible = true;

                    bool showMiniProfileHistoryIcon = Conversion.CBool(RuntimeSettings.GetSetting("SHOW_MICRO_MICRO_PROFILE_HISTORY_ICON", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID));
                    this.microMicroProfile.LoadMemberProfile(showMiniProfileHistoryIcon);

                    // we don't want to see an extra 'To' label in the form. 'To' is already set in the microMiniProfile.
                    if (toLabel != null)
                    {
                        toLabel.Visible = false;
                    }

                }

                if (!string.IsNullOrEmpty(Request.QueryString["composeoverlay"]))
                {
                    g.AnalyticsOmniture.PageName = "Mail Compose overlay";
                    g.AnalyticsOmniture.Evar2 = "Mail Compose overlay";
                }
            }
            catch (Exception anException)
            {
                g.ProcessException(anException);
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            //override profile nav page name for omniture
            Page.ClientScript.RegisterClientScriptBlock(typeof(System.Web.UI.Page), "ComposeEmailProfileOmniture",
                        "<script type=\"text/javascript\">"
                        + "tabProfileObject.ProfileNavPageName = \"Messages Compose Profile\";"
                        + "</script>");

            if (Convert.ToBoolean(Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ANALYTICS_OMNITURE", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID)))
            {
                BreadCrumbHelper.EntryPoint entryPoint = BreadCrumbHelper.GetEntryPoint(Conversion.CInt(Request[WebConstants.URL_PARAMETER_NAME_ENTRYPOINT]));

                switch (entryPoint)
                {
                    case BreadCrumbHelper.EntryPoint.PhotoGallery:
                        g.AnalyticsOmniture.Evar1 = entryPoint.ToString();
                        break;
                }
            }

            base.OnPreRender(e);

        }

        private void handleSendAsVIP()
        {
            cbxSendAsVIP.Text = g.GetResource("TXT_SEND_AS_VIP", this);
        }

        private void AddStartRowToSessionURL()
        {
            string refURL = g.GetReferrerUrl();
            string startRow = Request.Params.Get("StartRow");
            if (refURL.IndexOf("StartRow") != -1)
            {
                refURL = g.RemoveParamFromURL(refURL, "StartRow", false);
            }

            // See if the navigatUrl has params already
            string qm = "?";
            if ((refURL.IndexOf("?") != -1))
            {
                qm = "&";
            }

            if (startRow != null && startRow.Trim().Length > 0)
            {
                refURL = refURL + qm + "StartRow=" + startRow;
            }

            // for certain flows where the user has to go through subscription to complete the intended action,
            // GetReferrerUrl returns the subscription page which isn't what we want, so avoid setting it if the url
            // contains the string subscriptionconfirmation.aspx
            if (!refURL.ToLower().Contains("subscriptionconfirmation.aspx"))
            {
                g.Session.Add(MESSAGE_ORIGIN_URL_SESSION_KEY, refURL, SessionPropertyLifetime.Temporary);
            }
        }

        private void handlePrivatePhotos()
        {


            if (_g.Brand.GetStatusMaskValue(StatusType.PrivatePhotos))
            {
                cbSendPrivatePhotos.Visible = true;
                plcSendPrivatePhotos.Visible = true;
            }
            else
            {
                cbSendPrivatePhotos.Visible = false;
                plcSendPrivatePhotos.Visible = false;
            }

            // associated checkbox with proper resources
            cbSendPrivatePhotos.Text = g.GetResource("TXT_SEND_PRIVATE_PHOTOS", this);
            saveCopyCheckBox.Text = g.GetResource("TXT_SAVE_A_COPY_IN_SENT", this);

            // ??? this needs to verified with product management per Josh Viney
            AttributeOptionMailboxPreference mailboxMask = (AttributeOptionMailboxPreference)g.Member.GetAttributeInt(g.Brand, WebConstants.ATTRIBUTE_NAME_MAILBOXPREFERENCE);
            if ((mailboxMask & AttributeOptionMailboxPreference.SaveCopiesOfSentMessages) == AttributeOptionMailboxPreference.SaveCopiesOfSentMessages)
            {
                saveCopyCheckBox.Checked = true;
            }
        }

        public void miniProfileDataBind(object sender, RepeaterItemEventArgs args)
        {
            MiniProfile20 miniProfile = (MiniProfile20)args.Item.FindControl("toMemberMiniProfile");

            if (miniProfile != null)
            {
                miniProfile.Transparency = true;
            }
        }


        protected bool returnToMember
        {
            get
            {
                bool shouldReturn = false;

                if (Request["ReturnToMember"] != null)
                {
                    if (Request["ReturnToMember"].ToLower() == "true")
                    {
                        shouldReturn = true;
                    }
                }

                return shouldReturn;
            }
        }

        // attempt to save a draft message
        protected void saveDraftClick(object sender, EventArgs e)
        {
            if (_handler.CompileSendMessage(true, plcMailToList.Visible))
            {
                // If target member is emailed through the pop-up window then decides to save that message as a draft,
                // then the member should be taken back to the target member's full profile. TT15848
                string URLString = string.Empty;
                if (!string.IsNullOrEmpty(Request.QueryString["composeoverlay"]))
                {
                    URLString += "&composeoverlay=1&LayoutTemplateID=2&LayoutOverride=none&ActionCallPage=" + Request.QueryString["ActionCallPage"];
                }

                if (g.LayoutTemplate == LayoutTemplate.PopupProfile || returnToMember)
                {
                    g.Transfer("/Applications/MemberProfile/ViewProfile.aspx?MemberID=" + _handler.GetMemberId() + "&rc=TXT_MESSAGE_SAVE_DRAFT_SUCCESS" + URLString);
                }
                else
                {
                    g.Transfer("/Applications/Email/MailBox.aspx?MemberFolderID=" + (int)SystemFolders.Inbox + "&rc=TXT_MESSAGE_SAVE_DRAFT_SUCCESS" + URLString);
                }
            }
            else
            {
                g.Notification.AddError("TXT_MESSAGE_SAVE_DRAFT_FAILURE");
            }
        }

        protected void sendMessageClick(object sender, EventArgs e) //bool subscriber   !memberValid
        {
            if (!VIPMailUtils.IsVIPEnabledSite(g.Brand) || !SendAsVIP)
                sendMail();
            else
            {
                ProcessVIPMail(sender, e);
            }
        }

        protected void ProcessVIPMail(object sender, EventArgs e) //bool subscriber   !memberValid
        {

            try
            {
                bool isButtonOnOverlay = false;

                if (sender is FrameworkButton)
                    isButtonOnOverlay = (((FrameworkButton)sender).ID == "btnYes");

                int memberMailID = Constants.NULL_INT;
                VIPMember vipmember = VIPMailUtils.GetVIPMember(g, g.Member.MemberID);
                if (VIPMailUtils.IsVIPMember(g, vipmember))
                {
                    g.LoggingManager.LogInfoMessage("Compose20", "ProcessVIPMail - IsVIPMember Member " + g.Member.MemberID.ToString());
                    if (vipmember.EmailCount > 0)
                    {
                        cbxSendAsVIP.Checked = true;
                        sendMail();
                    }
                    else
                    {

                        if (_handler.SaveVIPDraft(out memberMailID))
                        {
                            int prtid = (int)PurchaseReasonType.RefillAllAccessEmails;

                            if (isButtonOnOverlay)
                                prtid = (int)PurchaseReasonType.RefillAllAccessEmailsFromOverlay;

                            string composeURL = Request.Url.GetLeftPart(UriPartial.Path) + "?MemberMailID=" + memberMailID.ToString() + "&MemberID=" + _handler.GetMemberId().ToString() + "&" + WebConstants.URL_PARAMETER_NAME_LAYOUTTEMPLATEID + "=" + Request.QueryString[WebConstants.URL_PARAMETER_NAME_LAYOUTTEMPLATEID];
                            composeURL = composeURL.Replace("ViewMessage.aspx?", "compose.aspx?");

                            // Member has all access but no all access emails left
                            // Direct this member to the fixed pricing upsale page to sell just additional
                            // all access emails  
                            Redirect.Upsale(g.Brand, prtid, _handler.GetMemberId(), false, Server.UrlEncode(composeURL), _memberMailID, false);
                        }
                    }
                }
                else
                {
                    g.LoggingManager.LogInfoMessage("Compose20", "ProcessVIPMail - Not IsVIPMember Member " + g.Member.MemberID.ToString());
                    if (_handler.SaveVIPDraft(out memberMailID))
                    {
                        int prtid = (int)PurchaseReasonType.AddAllAccessToCurrentSub;

                        if (isButtonOnOverlay)
                            prtid = (int)PurchaseReasonType.AddAllAccessToCurrentSubFromOverlay;

                        if (_handler.CanNonSubSaveAsDraft) // if allowed to save as draft
                        {
                            prtid = (int)PurchaseReasonType.AttemptToEmailSubBlockComposePage;
                        }

                        string composeURL = Request.Url.GetLeftPart(UriPartial.Path) + "?MemberMailID=" +
                                            memberMailID.ToString() + "&MemberID=" + _handler.GetMemberId().ToString();

                        // If the compose page is not viewd in an overlay (iframe), add the layouttemplateid parameter.
                        if (!Request.Url.Query.Contains("composeoverlay=1"))
                        {
                            composeURL += "&" + WebConstants.URL_PARAMETER_NAME_LAYOUTTEMPLATEID + "=" + Request.QueryString[WebConstants.URL_PARAMETER_NAME_LAYOUTTEMPLATEID];
                        }

                        composeURL = composeURL.Replace("ViewMessage.aspx?", "compose.aspx?");
                        // Member does not have all access nor all access emails 
                        // Direct this member to the variable pricing upsale page to sell packages
                        // containing both all access and all access emails 
                        Redirect.Upsale(g.Brand, prtid, _handler.GetMemberId(), false, Server.UrlEncode(composeURL), _memberMailID, true);
                    }
                }
            }
            catch (Exception ex)
            { g.ProcessException(ex); }
        }

        private void sendMail()
        {
            if (memberValid)
            {
                bool isNonsubFreeEmail = false;
                if (!FrameworkGlobals.IfHadSubscription(_g))
                {
                    if (ComposeHandler.canNonSubSendFreeEmail(_g))
                    {
                        isNonsubFreeEmail = true;
                    }
                }


                if (_handler.CompileSendMessage(false, plcMailToList.Visible))
                {
                    string URLString = "";
                    string vip = "";
                    if (_handler.VIPMail)
                        vip = String.Format("vipemail={0}&vipfreereply={1}", _handler.VIPMail, _handler.VIPFreeReply);


                    if (isNonsubFreeEmail)
                    {
                        URLString += "&freeimail=1";

                        if (HttpContext.Current.Request.Params.Get("MemberID") != null)
                        {
                            URLString += "&MemberId=" + HttpContext.Current.Request.Params.Get("MemberID");
                        }
                    }

                    URLString += "&" + WebConstants.URL_MESSAGE_RECIPIENT_MEMBER_ID + "=" + _handler.GetMemberId().ToString();

                    if (_handler.IsReply)
                    {
                        URLString += "&Reply=1";
                    }

                    if (!string.IsNullOrEmpty(Request.QueryString["composeoverlay"]))
                    {
                        URLString += "&composeoverlay=1&LayoutTemplateID=2&LayoutOverride=none&ActionCallPage=" + Request.QueryString["ActionCallPage"];
                    }

                    if (_Color != Color.none)
                    {
                        g.Transfer("/Applications/Email/MessageSent.aspx" + "?RecipientCC=" + ((int)_Color).ToString() + "&" + vip + URLString);
                    }
                    else
                    {
                        g.Transfer("/Applications/Email/MessageSent.aspx" + "?" + vip + URLString);
                    }
                }
                else
                {
                    g.Notification.AddError("TXT_MESSAGE_SENT_FAILURE");
                }
            }

            else
            {


                //save draft
                if (_handler.CompileSendMessage(true, plcMailToList.Visible))
                {
                    //do previous transfer with message id added 
                    if (g.Member != null && !MemberPrivilegeAttr.IsPermitMember(g))
                    {
                        _memberMailID = _handler.DraftMemberMailID;

                        LinkParent linkParent;
                        if (Request["LinkParent"] != null)
                        {
                            linkParent = (LinkParent)Conversion.CInt(Request["LinkParent"]);
                        }
                        else
                        {
                            linkParent = LinkParent.FullProfile;
                        }

                        int prtid = Constants.NULL_INT;
                        switch (linkParent)
                        {
                            case LinkParent.MiniProfile:
                                prtid = (int)PurchaseReasonType.AttemptToEmailMiniProfile;
                                break;
                            case LinkParent.GalleryMiniProfile:
                                prtid = (int)PurchaseReasonType.AttemptToEmailGalleryMiniProfile;
                                break;
                            case LinkParent.FullProfile:
                                prtid = (int)PurchaseReasonType.AttemptToEmailFullProfile;
                                break;
                        }

                        #region IMailPermissions

                        prtid = _handler.ApplyIMail();
                        #endregion

                        string composeURL = Request.Url.GetLeftPart(UriPartial.Path) + "?MemberMailID=" + _memberMailID.ToString() + "&MemberID=" + _handler.GetMemberId().ToString() + "&" + WebConstants.URL_PARAMETER_NAME_LAYOUTTEMPLATEID + "=" + Request.QueryString[WebConstants.URL_PARAMETER_NAME_LAYOUTTEMPLATEID];

                        if (_handler.CanNonSubSaveAsDraft) // if allowed to save as draft
                        {
                            prtid = (int)PurchaseReasonType.AttemptToEmailSubBlockComposePage;
                        }
                        Redirect.Subscription(g.Brand, prtid, _handler.GetMemberId(), false, Server.UrlEncode(composeURL), _memberMailID);

                    }
                }
                else
                {
                    //error saving draft
                    g.Notification.AddError("TXT_MESSAGE_SAVE_DRAFT_FAILURE");
                }
            }
        }


    }
}
