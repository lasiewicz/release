﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PushFlirtsPopup.ascx.cs" Inherits="Matchnet.Web.Applications.Email.PushFlirtsPopup" %>
<%@ Register TagPrefix="mn" TagName="MiniProfile" src="/Framework/Ui/ProfileElements/MiniProfile.ascx"%>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>

<form id="Form1" method="post" runat="server">

<script language="javascript">

    var s = s_gi(s_account);
    PopulateS(true); //clear existing values in omniture "s" object
    var originalPageName = s.pageName;
    s.pageName = s.pageName + "- Push_Flirt";
    s.t(true);
    s.pageName = originalPageName;

</script>


<div id="pushFlirtsProfileContainer">
    <h2><mn:txt runat="server" id="txtPushFlirtsHeader" expandimagetokens="true" /></h2>
    <ul class="mag-profile results clearfix">
        <asp:repeater id="flirtMemberRepeater" runat="server" Visible="False" OnItemDataBound="flirtMemberRepeater_OnItemDataBound">
		    <itemtemplate>
                <li>
                <input type="checkbox" runat="server" ID="chkSendPushFlirt" checked="checkd" />
                <mn:MiniProfile runat="server" id="MiniProfile" member="<%# Container.DataItem %>" LoadControlAutomatically="false"/>
                </li>
		    </itemtemplate>
	    </asp:repeater>
    </ul>    
    <div id="pushFlirtsButtonContainer" class="pushflirt-button">
        <blockquote class="curlyquotes"><mn:txt runat="server" id="txtPushFlirtsSingleFlirt" expandimagetokens="true" /></blockquote>
        <p class="text-center"><input id="pushFlirtsSend" type="button" class="brn primary large" value="<%=g.GetResource("TXT_PUSH_FLIRTS_SEND_BUTTON",this) %>" /></p>
        <%--<p class="text-center"><button id="pushFlirtsSend" type="button" class="btn link-primary"><mn:txt runat="server" id="txtPushFlirtsSendButton" resourceconstant="TXT_PUSH_FLIRTS_SEND_BUTTON" expandimagetokens="true" /></button></p>--%>
    </div>
    </div>
</form>
