﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Unsubscribe.ascx.cs" Inherits="Matchnet.Web.Applications.Email.Unsubscribe" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>

<div id="rightNew">
<div id="optout">
<h1><asp:Label ID="lblHeader" runat="server" Text=""></asp:Label></h1>
<div id="currentEmail"><asp:Label ID="lblOptedOut" runat="server" Text=""></asp:Label></div>
<p class="bold center"><strong><asp:Label ID="lblOops" runat="server" Text=""></asp:Label></strong></p>
<p class="center">
    <asp:Button ID="btnRevise" runat="server" Text="" CssClass="activityButton" PostBackUrl="/applications/email/messagesettings.aspx" />
</p>
<p><asp:Label ID="lblYouCanAlso" runat="server" Text=""></asp:Label></p>
<p><asp:Label ID="lbl48Hours" runat="server" Text=""></asp:Label></p>
</div>
</div>