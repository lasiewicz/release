﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System;
using System.Collections;
using System.Collections.Specialized;
using System.Text;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Member.ValueObjects.Photos;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Managers;
using Matchnet.Web.Framework.Ui;

using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.Region;
using Matchnet.Email.ValueObjects;
using Matchnet.Email.ServiceAdapters;
using Matchnet.List.ServiceAdapters;
using Matchnet.List.ValueObjects;
using Matchnet.Member.ServiceAdapters;


namespace Matchnet.Web.Applications.Email
{
    public partial class MailBox20 : FrameworkControl, IMailBox
    {
        #region Constants


        private const int MESSAGE_LIMIT_BYTES = 3000;
        private const int MESSAGE_SUBJECT_CROP_SIZE = 100;
        private const int MAILBOX_PAGE_SIZE = 10;
        private const int MAILBOX_PAGE_BREAK_SEGMENTS = 3;
        private const string SESSION_PHOTOS_FLAG = "SESSION_PHOTOS_FLAG";
        private const string TXT_TOGGLE_PHOTO_SETTING_ON = "TXT_TOGGLE_PHOTO_SETTING_ON";
        private const string TXT_TOGGLE_PHOTO_SETTING_OFF = "TXT_TOGGLE_PHOTO_SETTING_OFF";

        #endregion

        private string previousPageLink = null;
        private string nextPageLink = null;
        private string _vipPreviousPageLink = null;
        private string _vipNextPageLink = null;
        private string _vipSelectedPageSegment = null;
        private bool ignoreSelf = false;
        private string _MessageCountTitle;
        private EmailFolder _CurrentEmailFolder;
        private string _CurrentFolderName;
        private string _SelectedPageSegment;
        private int _CurrentMessageNumber = 0;
        private int _CurrentVIPMessageNumber = 0;
        private int _MessageCount = 0;
        private int _VIPMessageCount = 0;
        private bool photosFlag = false;
        private bool photosSetting = false;
        private bool _useBreadCrumb = false;
        protected System.Web.UI.WebControls.Table tableMailList;
        protected System.Web.UI.HtmlControls.HtmlImage imgDownArrow;

        #region IMailBox implementation

        public Matchnet.Web.Framework.Txt TxtReviseMessageAlertsSettings
        {
            get { return null; }
        }

        public System.Web.UI.WebControls.DropDownList DropDownListMove
        {
            get { return dropDownListMove; }
        }

        public System.Web.UI.WebControls.Label TxtMailBox
        {
            get { return txtMailBox; }
        }

        public Matchnet.Web.Framework.Ui.FormElements.FrameworkButton BtnEmptyFolder
        {
            get { return EmptyFolder; }
        }

        public Matchnet.Web.Framework.Ui.FormElements.FrameworkButton BtnDeleteMessage
        {
            get { return deleteMessage; }
        }

        public Matchnet.Web.Framework.Ui.FormElements.FrameworkButton BtnDeleteAllMessages
        {
            get { return deleteAllMessages; }
        }

        public Matchnet.Web.Framework.Ui.FormElements.FrameworkButton BtnIgnoreMessage
        {
            get { return ignoreMessage; }
        }

        public Matchnet.Web.Framework.Ui.FormElements.FrameworkButton BtnMarkUnreadMessage
        {
            get { return markUnreadMessage; }
        }

        public System.Web.UI.WebControls.Table TableMailList
        {
            get { return tableMailList; }
        }

        public System.Web.UI.WebControls.DataGrid MailDataGrid
        {
            get { return mailDataGrid; }
        }

        public System.Web.UI.WebControls.DataGrid VIPMailDataGrid
        {
            get { return dgVIPMail; }
        }

        public System.Web.UI.WebControls.DataGrid VIPMailHeaderDataGrid
        {
            get { return dgVIPMailHeader; }
        }

        public System.Web.UI.WebControls.DataGrid VIPMailFooterDataGrid
        {
            get { return dgVIPMailFooter; }
        }

        public System.Web.UI.HtmlControls.HtmlImage ImgDownArrow
        {
            get { return imgDownArrow; }
        }

        public Matchnet.Web.Framework.Ui.FormElements.FrameworkButton BtnMoveMessages
        {
            get { return moveMessages; }
        }

        public Matchnet.Web.Framework.Txt TxtEditfolders
        {
            get { return txtEditfolders; }
        }

        public Matchnet.Web.Framework.Image ImgNewMessages
        {
            get { return null; }
        }

        public System.Web.UI.WebControls.Panel MailBarPanel
        {
            get { return mailBarPanel; }
        }

        public System.Web.UI.WebControls.Panel EmptyFolderPanel
        {
            get { return emptyFolderPanel; }
        }

        public Matchnet.Web.Framework.Txt TxtEmptyFolder
        {
            get { return txtEmptyFolder; }
        }

        public Matchnet.Web.Framework.Image ImgTip
        {
            get { return null; }
        }

        public Matchnet.Web.Framework.Txt TxtTip
        {
            get { return null; }
        }

        public bool IgnoreSelf
        {
            get { return ignoreSelf; }
            set { ignoreSelf = value; }
        }

        public string MessageCountTitle
        {
            get { return _MessageCountTitle; }
            set { _MessageCountTitle = value; }
        }

        public EmailFolder CurrentEmailFolder
        {
            get { return _CurrentEmailFolder; }
            set { _CurrentEmailFolder = value; }
        }

        public string CurrentFolderName
        {
            get
            {
                return _CurrentFolderName;
                ;
            }
            set { _CurrentFolderName = value; }
        }

        public string SelectedPageSegment
        {
            get { return _SelectedPageSegment; }
            set { _SelectedPageSegment = value; }
        }

        public int CurrentMessageNumber
        {
            get { return _CurrentMessageNumber; }
            set { _CurrentMessageNumber = value; }
        }

        public int CurrentVIPMessageNumber
        {
            get { return _CurrentVIPMessageNumber; }
            set { _CurrentVIPMessageNumber = value; }
        }

        public int MessageCount
        {
            get { return _MessageCount; }
            set { _MessageCount = value; }
        }

        public int VIPMessageCount
        {
            get { return _VIPMessageCount; }
            set { _VIPMessageCount = value; }
        }

        public string VIPPreviousPageLink
        {
            get { return _vipPreviousPageLink; }
            set { _vipPreviousPageLink = value; }
        }

        public string VIPNextPageLink
        {
            get { return _vipNextPageLink; }
            set { _vipNextPageLink = value; }
        }

        public string VIPSelectedPageSegment
        {
            get { return _vipSelectedPageSegment; }
            set { _vipSelectedPageSegment = value; }
        }

        public Matchnet.Web.Framework.Txt TxtVIPEmpty
        {
            get { return txtVIPEmpty; }
        }

        //photos controls
        public System.Web.UI.WebControls.LinkButton LnkTogglePhotos
        {
            get { return lnkTogglePhotos; }
        }

        public Matchnet.Web.Framework.Txt TxtTogglePhotos
        {
            get { return txtTogglePhotos; }
        }

        public bool PhotosFlag
        {
            get { return photosFlag; }
            set { photosFlag = value; }

        }

        public bool PhotosSettingFlag
        {
            get { return photosSetting; }
            set { photosSetting = value; }

        }

        public string PreviousPageLink
        {
            get { return previousPageLink; }
            set { previousPageLink = value; }
        }

        public string NextPageLink
        {
            get { return nextPageLink; }
            set { nextPageLink = value; }
        }

        public FrameworkControl ResourceControl
        {
            get { return this; }
        }

        public bool UseBreadCrumb
        {
            get { return _useBreadCrumb; }
            set { _useBreadCrumb = value; }
        }

        private InboxShutdownHelper _inboxShutdownHelper = null;
        private InboxShutdownHelper P_InboxShutdownHelper
        {
            get
            {
                if (_inboxShutdownHelper == null)
                {
                    _inboxShutdownHelper = new InboxShutdownHelper(g);
                }
                return _inboxShutdownHelper;
            }
            set { _inboxShutdownHelper = value; }
        }


        public Label LnkTopPaging { get { return lnkTopPaging; } }
        public Label LnkBottomPaging { get { return lnkBottomPaging; } }
        public Label LnkVIPPaging { get { return lnkVIPPaging; } }
        MailBoxHandler _handler = null;
        #endregion
        private void Page_Load(object sender, System.EventArgs e)
        {
            string OrderBy = null;
            int currentPage = 1;
            string OrderByVIP = null;
            int currentPageVIP = 1;

            try
            {
                FrameworkGlobals.SaveBackToResultsURL(g);

                MailBoxHandler.ValidMemberAccess(g, true, (int)Matchnet.Content.ServiceAdapters.Links.PurchaseReasonType.AttemptToAccessInbox,WebConstants.MailAccessArea.mailBox);

                // OrderBy for both vip and regular datagrids
                OrderBy = _handler.ResolveSortRequest("OrderBy");
                OrderByVIP = _handler.ResolveSortRequest("VIPOrderBy");

                // Page number for regular grid
                if (Request.QueryString["msgpg"] != null && Request.QueryString["msgpg"].Trim().Length > 0)
                {
                    currentPage = int.Parse(Request.QueryString["msgpg"].Trim());
                }
                // Page number for VIP grid
                if (Request.QueryString["vipmsgpg"] != null && Request.QueryString["vipmsgpg"].Trim().Length > 0)
                {
                    currentPageVIP = int.Parse(Request.QueryString["vipmsgpg"].Trim());
                }

                _handler.LoadNavigation();
                _handler.InitializePhotosSetting();

                lnkTogglePhotos.Visible = PhotosSettingFlag;
                if (PhotosSettingFlag)
                {
                    txtTogglePhotos.ResourceConstant = _handler.GetPhotoSettingConstant();
                }
                if (IsPostBack)
                {
                    string ctrlname = Request["__EVENTTARGET"];
                    if (ctrlname != null && ctrlname != String.Empty)
                    {
                        if (ctrlname.ToLower().IndexOf("lnktoggle") > -1)
                        {
                            _handler.TogglePhotos();
                        }
                    }
                }
                if (Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ENABLE_MAILBOX_DELETE_ALL_MESSAGES", _g.Brand.Site.Community.CommunityID, _g.Brand.Site.SiteID).ToLower() == "true")
                {
                    deleteAllMessages.OnClientClick = "return window.confirm('" + g.GetResource("CONFIRM_DELETE_ALL_MESSAGES", this) + "');";
                    deleteAllMessages.Visible = true;
                }

                int folderid = _handler.GetCurrentFolderID();
                _handler.InitializeMessageList(folderid, OrderBy, currentPage, OrderByVIP, currentPageVIP, VIPMailUtils.IsVIPEnabledSite(_g.Brand), _g.Member.IsPayingMember(_g.Brand.Site.SiteID));
                SetupTabBar();
                if (VIPMailUtils.IsVIPEnabledSite(g.Brand) && _VIPMessageCount == 0)
                {
                    if (!VIPMailUtils.IsVIPMember(g, g.Member.MemberID) && MemberPrivilegeAttr.IsCureentSubscribedMember(g))
                    {
                        txtVIPEmpty.Visible = true;
                        emptyFolderPanel.Visible = false;
                    }

                }

                _handler.AddMailBoxURLToSession(Request.Url.AbsoluteUri.ToLower());

                if (g.Member.GetAttributeBool(g.Brand, "BannedEmailsRemoved"))
                {
                    g.Notification.AddMessage("TXT_FRAUD_EMAILS_REMOVED");
                    g.Member.SetAttributeInt(g.Brand, "BannedEmailsRemoved", 0);
                    Matchnet.Member.ServiceAdapters.MemberSA.Instance.SaveMember(g.Member);
                }

                //Add read receipt purchase link
                if (folderid == (int)SystemFolders.Sent)
                {
                    if (MessageManager.Instance.IsReadRecieptPremiumFeatureEnabled(g.Brand) && !MessageManager.Instance.DoesMemberHaveReadRecieptPrivilege(g.Member, g.Brand))
                    {
                        string readReceiptPurchaseLink = String.Empty;

                        if(g.Member.IsPayingMember(g.Brand.Site.SiteID)) //if the user 
                            readReceiptPurchaseLink = "/Applications/Subscription/PremiumServices.aspx?prtid=4176";
                        else
                            readReceiptPurchaseLink = "/Applications/Subscription/Subscribe.aspx?prtid=4176";

                        litReadReceiptPurchaseLink.Text = string.Format(g.GetResource("TXT_READ_RECEIPT_PURCHASE_LINK", this), readReceiptPurchaseLink);
                    }
                }

            }
            catch (Exception anException)
            {
                g.ProcessException(anException);
            }

            if (P_InboxShutdownHelper.IsInboxShutdownEnabled() && P_InboxShutdownHelper.IsCurrentMemberTargeted)
            {
                plcInboxShutdownBanner.Visible = true;
                imgInboxShutdownBanner.NavigateUrl = "/Applications/Subscription/Subscribe.aspx?prtid=1247";
            }
        }

        private void Page_PreRender(object sender, System.EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["composeoverlay"]))
            {
                g.AnalyticsOmniture.PageName = "Mail Draft overlay";
                g.AnalyticsOmniture.Evar2 = "Mail Draft overlay";
                if (!string.IsNullOrEmpty(Request.QueryString["ActionCallPage"]))
                {
                    g.AnalyticsOmniture.Evar27 = "Email Me(" + Request.QueryString["ActionCallPage"].Replace('+', ' ') + ")";
                }
            }
        }

        private void Page_Init(object sender, System.EventArgs e)
        {
            try
            {
                _handler = new MailBoxHandler(this, g);
                _handler.BuildOutMoveMailDropDownList(_handler.GetCurrentFolderID());
            }
            catch (Exception anException)
            {
                g.ProcessException(anException);
            }
        }

        #region message datagrid customizations

        public void vipMailBoxHeaderGrid_DataBound(object sender, DataGridItemEventArgs args)
        {
            HyperLink currentHyperLink = null;
            
            string MsgSubject = null;

            MsgSubject = g.GetResource("TXT_MSG_SUBJECT", this);

            // build out all of the table header sorts
            if (args.Item.ItemType == ListItemType.Header)
            {
                currentHyperLink = (HyperLink)args.Item.Cells[(int)VIPColumns.VIP_COL_STATUS].FindControl("itemUnreadSortLink");
                currentHyperLink.NavigateUrl = _handler.BuildHeaderSortLink("statusmask desc", "statusmask asc", GridType.VIPMail);

                currentHyperLink = (HyperLink)args.Item.Cells[(int)VIPColumns.VIP_COL_SUBJECT].FindControl("itemSubjectSortLink");
                currentHyperLink.NavigateUrl = _handler.BuildHeaderSortLink("subject desc", "subject asc", GridType.VIPMail);

                currentHyperLink = (HyperLink)args.Item.Cells[(int)VIPColumns.VIP_COL_INSERT_DATE].FindControl("itemFromSortLink");
                currentHyperLink.NavigateUrl = _handler.BuildHeaderSortLink("fromusername desc", "fromusername asc", GridType.VIPMail);

                currentHyperLink = (HyperLink)args.Item.Cells[(int)VIPColumns.VIP_COL_INSERT_DATE].FindControl("itemDateSortLink");
                currentHyperLink.NavigateUrl = _handler.BuildHeaderSortLink("insertdate des", "insertdate asc", GridType.VIPMail);
                if (Conversion.CInt(Request["MemberFolderID"]) == (int)SystemFolders.Sent)
                    currentHyperLink.Text = g.GetResource("TXT_SENT_DATE_LABEL", this);

                currentHyperLink = (HyperLink)args.Item.Cells[(int)VIPColumns.VIP_COL_STATUSMASK].FindControl("itemOpenedSortLink");
                currentHyperLink.NavigateUrl = _handler.BuildHeaderSortLink("opendate desc", "opendate asc", GridType.VIPMail);
                if (Conversion.CInt(Request["MemberFolderID"]) == (int)SystemFolders.Sent)
                    currentHyperLink.Visible = true;
                else
                    currentHyperLink.Visible = false;
            }


        }

        // handles buildout of the datagrid component
        public void vipMailBoxGrid_DataBound(object sender, DataGridItemEventArgs args)
        {
            HyperLink currentHyperLink = null;
            Label currentLabel = null;
            Label footerSelectAll = null;
            //photo controls
            HyperLink currentimgFrom = null;
            System.Web.UI.WebControls.Image imgPhoto = null;
            System.Web.UI.WebControls.Label lblFrom = null;

            int fromMemberID = 0;
            int toMemberID = 0;
            int currentMemberMailID = 0;
            int currentMemberFolderID = 0;
            MessageStatus statusMask = MessageStatus.Nothing;
            string openedDate = null;
            string currentSubject = null;
            string currentFromUser = null;
            string currentToUser = null;
            string currentInsertDate = null;
            string currentSize = null;

            string MsgSubject = null;

            MsgSubject = g.GetResource("TXT_MSG_SUBJECT", this);

            // build out all of the table header sorts
            if (args.Item.ItemType == ListItemType.Item || args.Item.ItemType == ListItemType.AlternatingItem)
            {

                EmailMessage emailMessage = (EmailMessage)args.Item.DataItem;
                // retrieve the known hidden parameters necessary to build the data grid
                statusMask = (MessageStatus)Enum.Parse(typeof(MessageStatus), args.Item.Cells[(int)VIPColumns.VIP_COL_STATUSMASK].Text);
                currentMemberMailID = int.Parse(args.Item.Cells[(int)VIPColumns.VIP_COL_MAILID].Text);
                currentMemberFolderID = int.Parse(args.Item.Cells[(int)VIPColumns.VIP_COL_FOLDERID].Text);

                #region  Message subject override
                // if the MSG subject has been defined in resource files use it otherwise use the default
                // This is place due to JDIL renaming of Ecards to J-card
                //20070807	RB
                currentSubject = ((MsgSubject != string.Empty && emailMessage.MailTypeID == MailType.ECard) ? MsgSubject : FrameworkGlobals.GetUnicodeText(args.Item.Cells[(int)VIPColumns.VIP_COL_SUBJECT1].Text));
                #endregion

                currentFromUser = FrameworkGlobals.Ellipsis(FrameworkGlobals.GetUnicodeText(args.Item.Cells[(int)VIPColumns.VIP_COL_FROMUSERNAME].Text), 10);
                currentInsertDate = args.Item.Cells[(int)VIPColumns.VIP_COL_INSERTDATE].Text;
                fromMemberID = int.Parse(args.Item.Cells[(int)VIPColumns.VIP_COL_FROMMEMBERID].Text);
                currentToUser = FrameworkGlobals.Ellipsis(args.Item.Cells[(int)VIPColumns.VIP_COL_TOMEMBERID].Text, 10);
                toMemberID = int.Parse(args.Item.Cells[(int)VIPColumns.VIP_COL_TOMEMBERID].Text);
                openedDate = args.Item.Cells[(int)VIPColumns.VIP_COL_OPENEDDATE].Text; // Used in Sent Box to see if it's been viewed by recipient.
                _CurrentVIPMessageNumber++;	// Maintained as we're binding the items in the drop-down list.

                // pull out and properly set the message sent date
                currentLabel = (Label)args.Item.Cells[(int)VIPColumns.VIP_COL_INSERT_DATE].FindControl("itemInsertDate");
                if (g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.JDateFR)
                {
                    currentLabel.Text = FrameworkGlobals.GetOffsetAdjustedDateDisplay(emailMessage.InsertDate, g.Brand);
                }
                else
                {
                    currentLabel.Text = FrameworkGlobals.GetOffsetAdjustedDateDisplay(Convert.ToDateTime(currentInsertDate, new CultureInfo("en-US")), g.Brand);
                }

                // pull out and properly link and format the From User Link
                currentHyperLink = (HyperLink)args.Item.Cells[(int)VIPColumns.VIP_COL_FROM_USERNAME].FindControl("itemFromLink");
                int photoMemberID = 0;

                if (currentMemberFolderID == (int)SystemFolders.Draft || currentMemberFolderID == (int)SystemFolders.Sent
                    || currentMemberFolderID == (int)SystemFolders.VIPSent || currentMemberFolderID == (int)SystemFolders.VIPDraft)
                {
                    currentHyperLink.NavigateUrl = BreadCrumbHelper.MakeViewProfileLink(BreadCrumbHelper.EntryPoint.Messages, toMemberID);
                    currentHyperLink.Text = currentToUser;
                    photoMemberID = toMemberID;
                }
                else
                {
                    currentHyperLink.NavigateUrl = BreadCrumbHelper.MakeViewProfileLink(BreadCrumbHelper.EntryPoint.Messages, fromMemberID);
                    currentHyperLink.Text = currentFromUser;
                    photoMemberID = fromMemberID;
                }
                currentimgFrom = (HyperLink)args.Item.Cells[(int)VIPColumns.VIP_COL_SUBJECT].FindControl("itemFromImageLink");
                imgPhoto = (System.Web.UI.WebControls.Image)args.Item.Cells[(int)VIPColumns.VIP_COL_SUBJECT].FindControl("imgPhoto");
                if (PhotosFlag)
                {
                    //MemberPhotoHelper photoHelper = new MemberPhotoHelper(RuntimeSettings.Instance);
                    Matchnet.Member.ServiceAdapters.Member member = Matchnet.Member.ServiceAdapters.MemberSA.Instance.GetMember(photoMemberID, MemberLoadFlags.None);
                    Photo photo = MemberPhotoDisplayManager.Instance.GetDefaultPhoto(g.Member, member, g.Brand);
                    string memberPhoto = MemberPhotoDisplayManager.Instance.GetPhotoDisplayURL(g.Member, member, g.Brand, photo,
                                                                              PhotoType.Thumbnail,
                                                                              PrivatePhotoImageType.TinyThumb,
                                                                              NoPhotoImageType.BackgroundTinyThumb);
                    ;
                    if (currentimgFrom != null)
                        currentimgFrom.NavigateUrl = currentHyperLink.NavigateUrl;
                    if (imgPhoto != null)
                        imgPhoto.ImageUrl = memberPhoto;

                }

                imgPhoto.Visible = PhotosFlag;
                currentimgFrom.Visible = PhotosFlag;

                // pull out and properly link and format the Subject message link
                currentHyperLink = (HyperLink)args.Item.Cells[(int)VIPColumns.VIP_COL_SUBJECT].FindControl("itemSubjectLink");

                // last parameter being false doesn't matter because we are processing vipgrid where the vip message subject is never masked
                makeMessageLink(currentHyperLink, emailMessage, GridType.VIPMail, false);

                if (MailBoxHandler.ApplyImailPermissions(g) && (fromMemberID != g.Member.MemberID) && !MemberPrivilegeAttr.IsPermitMember(g))
                {
                    switch (emailMessage.MailTypeID)
                    {
                        case MailType.Email:
                            currentHyperLink.Text = g.GetResource("EMAIL_SUBJECT_IMAIL_PERMISSIONS", this);
                            break;

                        default:
                            //currentHyperLink.Text = FrameworkGlobals.Ellipsis(emailMessage.Subject.Trim(), MESSAGE_SUBJECT_CROP_SIZE);
                            currentHyperLink.Text = FrameworkGlobals.Ellipsis(currentSubject.Trim(), MESSAGE_SUBJECT_CROP_SIZE);
                            break;
                    }
                }
                else
                {
                    //currentHyperLink.Text = FrameworkGlobals.Ellipsis(emailMessage.Subject.Trim(), MESSAGE_SUBJECT_CROP_SIZE);			
                    currentHyperLink.Text = FrameworkGlobals.Ellipsis(currentSubject.Trim(), MESSAGE_SUBJECT_CROP_SIZE);
                }

                if (currentMemberFolderID != (int)SystemFolders.VIPSent) // making the right side only avalible for folders other than sent 
                { 
                    // check to see which items are unread, replied or unopened
                    if ((statusMask & MessageStatus.Read) == MessageStatus.Read)
                    {
                        if ((statusMask & MessageStatus.Replied) == MessageStatus.Replied)
                        {
                            currentHyperLink = (HyperLink)args.Item.Cells[(int)VIPColumns.VIP_COL_STATUS].FindControl("itemImageRepliedLink");
                            args.Item.CssClass = "opened";
                        }
                        else
                        {
                            currentHyperLink = (HyperLink)args.Item.Cells[(int)VIPColumns.VIP_COL_STATUS].FindControl("itemImageOpenedLink");
                            args.Item.CssClass = "opened";
                        }
                    }
                    else
                    {
                        currentHyperLink = (HyperLink)args.Item.Cells[(int)VIPColumns.VIP_COL_STATUS].FindControl("itemImageUnopenedLink");

                        if (MailBoxHandler.ApplyImailPermissions(g) && (fromMemberID != g.Member.MemberID) &&
                            emailMessage.MailTypeID != MailType.Tease && emailMessage.MailTypeID != MailType.ECard
                            && !MemberPrivilegeAttr.IsPermitMember(g))
                        {
                            // IMailPermissions - unopened icon.
                            Matchnet.Web.Framework.Image icon = (Matchnet.Web.Framework.Image)args.Item.FindControl("Icon_Email_Unopened");
                            icon.FileName = "icon-email-unopened.gif";

                            // IMailPermissions - highlight the row dark.
                            args.Item.CssClass = "subscribetoview";
                        }
                        else
                        {
                            args.Item.CssClass = "unopened";
                        }

                    }

                    currentHyperLink.Visible = true;
                }

                // last parameter being false doesn't matter because we are processing vipgrid where the vip message subject is never masked
                makeMessageLink(currentHyperLink, emailMessage, GridType.VIPMail, false);

                // show opened icon in sent box if msg has been opened by recipient
                if (currentMemberFolderID == (int)SystemFolders.VIPSent)
                {
                    Matchnet.Web.Framework.Image image = (Matchnet.Web.Framework.Image)args.Item.Cells[(int)VIPColumns.VIP_COL_STATUSMASK].FindControl("OpenedImage");
                    if ((openedDate != null) && (Convert.ToDateTime(openedDate, new CultureInfo("en-US")) != DateTime.MinValue))
                    {
                        openedDate = FrameworkGlobals.GetOffsetAdjustedDateDisplay(Convert.ToDateTime(openedDate, new CultureInfo("en-US")), g.Brand);
                        image.Visible = false;

                        // show who & when sent
                        StringDictionary tokens = new StringDictionary();
                        tokens.Add("OPENED_BY", currentToUser);
                        tokens.Add("OPENED_DATETIME", openedDate.ToString());

                        image.AlternateText = g.GetResource("MAIL_OPENED_ALT_TEXT", this, tokens);

                        Matchnet.Web.Framework.Image imageOpened = (Matchnet.Web.Framework.Image)args.Item.Cells[(int)VIPColumns.VIP_COL_STATUSMASK].FindControl("aaSentImageOpened");
                        args.Item.CssClass = "opened";
                        imageOpened.AlternateText = g.GetResource("MAIL_OPENED_ALT_TEXT", this, tokens);
                        imageOpened.Visible = true;

                    }
                    else
                    {
                        Matchnet.Web.Framework.Image imageUnopened = (Matchnet.Web.Framework.Image)args.Item.Cells[(int)VIPColumns.VIP_COL_STATUSMASK].FindControl("aaSentItemImageUnopened");
                        args.Item.CssClass = "unopened";
                        imageUnopened.Visible = true;
                    }

                }

            }

        }

        public void vipMailBoxFooterGrid_DataBound(object sender, DataGridItemEventArgs args)
        {
            HyperLink currentHyperLink = null;
            Label currentLabel = null;
            Label footerSelectAll = null;
            //photo controls
            HyperLink currentimgFrom = null;
            System.Web.UI.WebControls.Image imgPhoto = null;
            System.Web.UI.WebControls.Label lblFrom = null;

            int fromMemberID = 0;
            int toMemberID = 0;
            int currentMemberMailID = 0;
            int currentMemberFolderID = 0;
            MessageStatus statusMask = MessageStatus.Nothing;
            string openedDate = null;
            string currentSubject = null;
            string currentFromUser = null;
            string currentToUser = null;
            string currentInsertDate = null;
            string currentSize = null;

            string MsgSubject = null;

            MsgSubject = g.GetResource("TXT_MSG_SUBJECT", this);

            // build out all of the table header sorts

            if (args.Item.ItemType == ListItemType.Footer)
            {
                footerSelectAll = new Label();
                footerSelectAll.Text = g.GetResource("TXT_SELECT_NBSP_ALL", this);

                args.Item.Cells[(int)VIPColumns.VIP_COL_STATUS].ColumnSpan = 3;
                args.Item.Cells[(int)VIPColumns.VIP_COL_STATUS].Wrap = false;
                args.Item.Cells[(int)VIPColumns.VIP_COL_STATUS].Controls.Add(footerSelectAll);

                if (Conversion.CInt(Request["MemberFolderID"]) == (int)SystemFolders.Sent)
                    args.Item.Cells[(int)VIPColumns.VIP_COL_FROM_USERNAME].ColumnSpan = 3;
                else
                    args.Item.Cells[(int)VIPColumns.VIP_COL_FROM_USERNAME].ColumnSpan = 2;

                currentLabel = new Label();
                currentLabel.Text = "";
                currentLabel.Visible = false;
                if (previousPageLink != null && nextPageLink != null)
                {
                    currentLabel.Visible = true;
                }
                args.Item.Cells[(int)VIPColumns.VIP_COL_FROM_USERNAME].Controls.Add(currentLabel);


                // remove all spanned columns not used
                for (int i = (args.Item.Cells.Count - 1); i >= 3; i--)
                {
                    args.Item.Cells.RemoveAt(i);
                }
            }
        }

        public void mailBoxGrid_DataBound(object sender, DataGridItemEventArgs args)
        {
            HyperLink currentHyperLink = null;
            Label currentLabel = null;
            Label footerSelectAll = null;
            //photo controls`
            HyperLink currentimgFrom = null;
            System.Web.UI.WebControls.Image imgPhoto = null;
            System.Web.UI.WebControls.Label lblFrom = null;

            int fromMemberID = 0;
            int toMemberID = 0;
            int currentMemberMailID = 0;
            int currentMemberFolderID = 0;
            MessageStatus statusMask = MessageStatus.Nothing;
            string openedDate = null;
            string currentSubject = null;
            string currentFromUser = null;
            string currentToUser = null;
            string currentInsertDate = null;
            string currentSize = null;

            string MsgSubject = null;

            bool readReciptPrivsEnabled = MessageManager.Instance.IsReadRecieptPremiumFeatureEnabled(g.Brand);
            bool readReciptPrivs = false;

            if (readReciptPrivsEnabled)
                readReciptPrivs = MessageManager.Instance.DoesMemberHaveReadRecieptPrivilege(g.Member, g.Brand);

            MsgSubject = g.GetResource("TXT_MSG_SUBJECT", this);

            // build out all of the table header sorts
            if (args.Item.ItemType == ListItemType.Header)
            {
                currentHyperLink = (HyperLink)args.Item.Cells[(int)Columns.COL_STATUS].FindControl("itemUnreadSortLink");
                currentHyperLink.NavigateUrl = _handler.BuildHeaderSortLink("statusmask desc", "statusmask asc", GridType.Mail);

                currentHyperLink = (HyperLink)args.Item.Cells[(int)Columns.COL_SUBJECT].FindControl("itemSubjectSortLink");
                currentHyperLink.NavigateUrl = _handler.BuildHeaderSortLink("subject desc", "subject asc", GridType.Mail);

                currentHyperLink = (HyperLink)args.Item.Cells[(int)Columns.COL_INSERT_DATE].FindControl("itemFromSortLink");
                currentHyperLink.NavigateUrl = _handler.BuildHeaderSortLink("fromusername desc", "fromusername asc", GridType.Mail);

                currentHyperLink = (HyperLink)args.Item.Cells[(int)Columns.COL_INSERT_DATE].FindControl("itemDateSortLink");
                currentHyperLink.NavigateUrl = _handler.BuildHeaderSortLink("insertdate des", "insertdate asc", GridType.Mail);

                if (Conversion.CInt(Request["MemberFolderID"]) == (int)SystemFolders.Sent)
                    currentHyperLink.Text = g.GetResource("TXT_SENT_DATE_LABEL", this);

                currentHyperLink = (HyperLink)args.Item.Cells[(int)Columns.COL_STATUSMASK].FindControl("itemOpenedSortLink");
                currentHyperLink.NavigateUrl = _handler.BuildHeaderSortLink("opendate desc", "opendate asc", GridType.Mail);
                if (Conversion.CInt(Request["MemberFolderID"]) == (int)SystemFolders.Sent)
                {
                    currentHyperLink.Visible = true;

                    if (readReciptPrivsEnabled && readReciptPrivs) //check for privs
                    { 
                        currentHyperLink = (HyperLink)args.Item.Cells[(int)Columns.COL_OPENED_DATE_FOR_DISPLAY].FindControl("itemOpenedDateSortLink");
                        currentHyperLink.NavigateUrl = _handler.BuildHeaderSortLink("opendate desc", "opendate asc", GridType.Mail);
                    }

                    if(!readReciptPrivsEnabled)
                    {
                        currentHyperLink = (HyperLink)args.Item.Cells[(int)Columns.COL_OPENED_DATE_FOR_DISPLAY].FindControl("itemOpenedDateSortLink");
                        currentHyperLink.NavigateUrl = _handler.BuildHeaderSortLink("opendate desc", "opendate asc", GridType.Mail);
                    }

                }
                else
                    currentHyperLink.Visible = false;
            }
            else if (args.Item.ItemType == ListItemType.Item || args.Item.ItemType == ListItemType.AlternatingItem)
            {

                EmailMessage emailMessage = (EmailMessage)args.Item.DataItem;

                // retrieve the known hidden parameters necessary to build the data grid
                statusMask = (MessageStatus)Enum.Parse(typeof(MessageStatus), args.Item.Cells[(int)Columns.COL_STATUSMASK].Text);
                currentMemberMailID = int.Parse(args.Item.Cells[(int)Columns.COL_MAILID].Text);
                currentMemberFolderID = int.Parse(args.Item.Cells[(int)Columns.COL_FOLDERID].Text);

                #region  Message subject override
                // if the MSG subject has been defined in resource files use it otherwise use the default
                // This is place due to JDIL renaming of Ecards to J-card
                //20070807	RB
                currentSubject = ((MsgSubject != string.Empty && emailMessage.MailTypeID == MailType.ECard) ? MsgSubject : FrameworkGlobals.GetUnicodeText(args.Item.Cells[(int)Columns.COL_SUBJECT1].Text));
                #endregion


                
                currentFromUser = FrameworkGlobals.Ellipsis(FrameworkGlobals.GetUnicodeText(args.Item.Cells[(int)Columns.COL_FROMUSERNAME].Text), 10);
                currentInsertDate = args.Item.Cells[(int)Columns.COL_INSERTDATE].Text;
                fromMemberID = int.Parse(args.Item.Cells[(int)Columns.COL_FROMMEMBERID].Text);
                currentToUser = FrameworkGlobals.Ellipsis(args.Item.Cells[(int)Columns.COL_TOUSERNAME].Text, 10);
                toMemberID = int.Parse(args.Item.Cells[(int)Columns.COL_TOMEMBERID].Text);
                openedDate = args.Item.Cells[(int)Columns.COL_OPENEDDATE].Text; // Used in Sent Box to see if it's been viewed by recipient.
                _CurrentMessageNumber++;	// Maintained as we're binding the items in the drop-down list.

                // pull out and properly set the message sent date
                currentLabel = (Label)args.Item.Cells[(int)Columns.COL_INSERT_DATE].FindControl("itemInsertDate");
                if (g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.JDateFR)
                {
                    currentLabel.Text = FrameworkGlobals.GetOffsetAdjustedDateDisplay(emailMessage.InsertDate, g.Brand);
                }
                else
                {
                    currentLabel.Text = FrameworkGlobals.GetOffsetAdjustedDateDisplay(Convert.ToDateTime(currentInsertDate, new CultureInfo("en-US")), g.Brand);
                }

                // pull out and properly link and format the From User Link
                currentHyperLink = (HyperLink)args.Item.Cells[(int)Columns.COL_FROM_USERNAME].FindControl("itemFromLink");
                int photoMemberID = 0;

                if (currentMemberFolderID == (int)SystemFolders.Draft || currentMemberFolderID == (int)SystemFolders.Sent
                    || currentMemberFolderID == (int)SystemFolders.VIPSent || currentMemberFolderID == (int)SystemFolders.VIPDraft)
                {
                    currentHyperLink.NavigateUrl = BreadCrumbHelper.MakeViewProfileLink(BreadCrumbHelper.EntryPoint.Messages, toMemberID);
                    currentHyperLink.Text = currentToUser;
                    photoMemberID = toMemberID;
                }
                else
                {
                    currentHyperLink.NavigateUrl = BreadCrumbHelper.MakeViewProfileLink(BreadCrumbHelper.EntryPoint.Messages, fromMemberID);
                    currentHyperLink.Text = currentFromUser;
                    photoMemberID = fromMemberID;
                }
                currentimgFrom = (HyperLink)args.Item.Cells[(int)Columns.COL_SUBJECT].FindControl("itemFromImageLink");
                imgPhoto = (System.Web.UI.WebControls.Image)args.Item.Cells[(int)Columns.COL_SUBJECT].FindControl("imgPhoto");
                if (PhotosFlag)
                {
                    if (currentimgFrom != null)
                        currentimgFrom.NavigateUrl = currentHyperLink.NavigateUrl;

                    Matchnet.Member.ServiceAdapters.Member member = Matchnet.Member.ServiceAdapters.MemberSA.Instance.GetMember(photoMemberID, MemberLoadFlags.None);

                    string memberPhoto = string.Empty;


                    if (member.HasApprovedPhoto(g.Brand.Site.Community.CommunityID))
                    {
                        Photo photo = MemberPhotoDisplayManager.Instance.GetDefaultPhoto(g.Member, member, g.Brand);
                        memberPhoto = MemberPhotoDisplayManager.Instance.GetPhotoDisplayURL(g.Member, member, g.Brand, photo,
                                                                                  PhotoType.Thumbnail,
                                                                                  PrivatePhotoImageType.TinyThumb,
                                                                                  NoPhotoImageType.BackgroundTinyThumb);
                    }
                    else
                    {
                        memberPhoto = MemberPhotoDisplayManager.Instance.GetNoPhotoFile(NoPhotoImageType.BackgroundTinyThumb, member, g.Brand);
                    }

                    if (imgPhoto != null)
                        imgPhoto.ImageUrl = memberPhoto;

                }
                bool isStandardMailMasked = false;
                if (VIPMailUtils.IsVIPEnabledSite(g.Brand))
                {
                    if (!MemberPrivilegeAttr.IsPermitMember(g))
                    {
                        if (SettingsManager.GetSettingBool(SettingConstants.VIP_MASK_STANDARD_MAIL, g.Brand))
                        {
                            isStandardMailMasked = true;
                        }
                        else
                        {
                            // if you get here, it means we shouldn't mask the standard email according to the db setting, but we will
                            // have to worry about site-specific rules
                            isStandardMailMasked = !FrameworkGlobals.MailAccessException(WebConstants.MailAccessArea.viewMessage, g, g.Member);

                            // In JDIL, we don't want to tie up the mailbox view with the viewmessage access rules
                            if (g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.JDateCoIL)
                            {
                                isStandardMailMasked = false;
                            }
                        }

                        if (isStandardMailMasked)
                        {
                            currentFromUser = g.GetResource("TXT_STANDARD_MAIL_FROMUSER_NONSUB", this);
                            currentSubject = g.GetResource("TXT_STANDARD_MAIL_SUBJ_NONSUB", this);

                            currentHyperLink = (HyperLink)args.Item.Cells[(int)Columns.COL_FROM_USERNAME].FindControl("itemFromLink");
                            if (currentHyperLink != null)
                            {
                                currentHyperLink.Text = currentFromUser;
                                currentHyperLink.NavigateUrl = "";

                            }
                            currentimgFrom = (HyperLink)args.Item.Cells[(int)Columns.COL_SUBJECT].FindControl("itemFromImageLink");
                            if (currentimgFrom != null)
                            {
                                currentimgFrom.NavigateUrl = string.Empty;
                            }
                            imgPhoto = (System.Web.UI.WebControls.Image)args.Item.Cells[(int)Columns.COL_SUBJECT].FindControl("imgPhoto");
                            bool isMale = FrameworkGlobals.IsMaleGender(fromMemberID, g);
                            imgPhoto.ImageUrl = MemberPhotoDisplayManager.Instance.GetNoPhotoFile(NoPhotoImageType.Thumb, isMale);
                        }
                    }

                }

                imgPhoto.Visible = PhotosFlag;
                currentimgFrom.Visible = PhotosFlag;

                // pull out and properly link and format the Subject message link
                currentHyperLink = (HyperLink)args.Item.Cells[(int)Columns.COL_SUBJECT].FindControl("itemSubjectLink");
                makeMessageLink(currentHyperLink, emailMessage, GridType.Mail, isStandardMailMasked);

                if (MailBoxHandler.ApplyImailPermissions(g) && (fromMemberID != g.Member.MemberID) && !MemberPrivilegeAttr.IsPermitMember(g))
                {
                    switch (emailMessage.MailTypeID)
                    {
                        case MailType.Email:
                            currentHyperLink.Text = g.GetResource("EMAIL_SUBJECT_IMAIL_PERMISSIONS", this);
                            break;

                        default:
                            //currentHyperLink.Text = FrameworkGlobals.Ellipsis(emailMessage.Subject.Trim(), MESSAGE_SUBJECT_CROP_SIZE);
                            currentHyperLink.Text = FrameworkGlobals.Ellipsis(currentSubject.Trim(), MESSAGE_SUBJECT_CROP_SIZE);
                            break;
                    }
                }
                else
                {
                    //currentHyperLink.Text = FrameworkGlobals.Ellipsis(emailMessage.Subject.Trim(), MESSAGE_SUBJECT_CROP_SIZE);			
                    currentHyperLink.Text = FrameworkGlobals.Ellipsis(currentSubject.Trim(), MESSAGE_SUBJECT_CROP_SIZE);
                }

                // check to see which items are unread, replied or unopened
                if ((statusMask & MessageStatus.Read) == MessageStatus.Read)
                {
                    if ((statusMask & MessageStatus.Replied) == MessageStatus.Replied)
                    {
                        currentHyperLink = (HyperLink)args.Item.Cells[(int)Columns.COL_OPENEDDATE].FindControl("itemImageRepliedLink");
                        args.Item.CssClass = "opened";
                    }
                    else
                    {
                        currentHyperLink = (HyperLink)args.Item.Cells[(int)Columns.COL_OPENEDDATE].FindControl("itemImageOpenedLink");
                        args.Item.CssClass = "opened";
                    }

                }
                else
                {
                    currentHyperLink = (HyperLink)args.Item.Cells[(int)Columns.COL_OPENEDDATE].FindControl("itemImageUnopenedLink");

                    if (MailBoxHandler.ApplyImailPermissions(g) && (fromMemberID != g.Member.MemberID) &&
                        emailMessage.MailTypeID != MailType.Tease && emailMessage.MailTypeID != MailType.ECard
                        && !MemberPrivilegeAttr.IsPermitMember(g))
                    {
                        // IMailPermissions - unopened icon.
                        Matchnet.Web.Framework.Image icon = (Matchnet.Web.Framework.Image)args.Item.FindControl("Icon_Email_Unopened");
                        icon.FileName = "icon-email-unopened.gif";

                        // IMailPermissions - highlight the row dark.
                        args.Item.CssClass = "subscribetoview";
                    }
                    else
                    {
                        args.Item.CssClass = "unopened";
                    }
                }

                currentHyperLink.Visible = true;
                makeMessageLink(currentHyperLink, emailMessage, GridType.Mail, isStandardMailMasked);

                // show opened icon in sent box if msg has been opened by recipient
                if (currentMemberFolderID == (int)SystemFolders.Sent)
                {
                    if (readReciptPrivsEnabled && !readReciptPrivs)
                    {
                        //Read Receipt is a premium feature on this site, we will show lock since user does not have privilege
                        HyperLink lnkReadReceiptLock = (HyperLink)args.Item.Cells[(int)Columns.COL_OPENEDDATE].FindControl("lnkReadReceiptLock");
                        lnkReadReceiptLock.Visible = true;

                        if (g.Member.IsPayingMember(g.Brand.Site.SiteID)) //if the user 
                            lnkReadReceiptLock.NavigateUrl = "/Applications/Subscription/PremiumServices.aspx?prtid=4176";
                        else
                            lnkReadReceiptLock.NavigateUrl = "/Applications/Subscription/Subscribe.aspx?prtid=4176";
                        
                        args.Item.CssClass = "unopened";
                    }
                    else
                    {
                        if ((openedDate != null) && openedDate != "01/01/0001 12:00 " && Convert.ToDateTime(openedDate, new CultureInfo("en-US")) != DateTime.MinValue)
                        {
                            openedDate = FrameworkGlobals.GetOffsetAdjustedDateDisplay(Convert.ToDateTime(openedDate, new CultureInfo("en-US")), g.Brand);

                            // show who & when sent
                            StringDictionary tokens = new StringDictionary();
                            tokens.Add("OPENED_BY", currentToUser);
                            tokens.Add("OPENED_DATETIME", openedDate.ToString());
                            args.Item.CssClass = "opened";

                            //show opened icon
                            Matchnet.Web.Framework.Image image1 = (Matchnet.Web.Framework.Image)args.Item.Cells[(int)Columns.COL_OPENEDDATE].FindControl("sentImageOpened");
                            image1.AlternateText = g.GetResource("MAIL_OPENED_ALT_TEXT", this, tokens);
                            image1.Visible = true;

                            ((Label)args.Item.Cells[(int)Columns.COL_OPENED_DATE_FOR_DISPLAY].FindControl("itemOpenedDate")).Text = openedDate;

                        }
                        else
                        {
                            args.Item.CssClass = "unopened";

                            //show unopened icon
                            Matchnet.Web.Framework.Image image2 = (Matchnet.Web.Framework.Image)args.Item.Cells[(int)Columns.COL_OPENEDDATE].FindControl("sentItemImageUnopened");
                            image2.Visible = true;
                        }
                    }
                }

                //inbox shutdown
                if (P_InboxShutdownHelper.IsInboxShutdownEnabled())
                {
                    if (P_InboxShutdownHelper.IsMessageBlocked(emailMessage, g.Member, fromMemberID,
                                                             currentMemberFolderID))
                    {
                        plcInboxShutdownOverlay.Visible = true;

                        Matchnet.Web.Framework.Image icon = currentHyperLink.Controls[0] as Matchnet.Web.Framework.Image;
                        if (icon != null)
                        {
                            icon.FileName = "icon-email-noaccess-1.png";
                            icon.TitleResourceConstant = "TXT_INBOX_SHUTDOWN_ICON_TITLE";
                            icon.NavigateUrl = "javascript:ShowInboxshutdownOverlay();";
                        }

                        currentHyperLink = (HyperLink)args.Item.Cells[(int)Columns.COL_SUBJECT].FindControl("itemSubjectLink");
                        currentHyperLink.Text = g.GetResource("TXT_INBOX_SHUTDOWN_MESSAGE_BLOCKED_SUBJECT", this);
                        currentHyperLink.NavigateUrl = "javascript:ShowInboxshutdownOverlay();";
                    }
                }
            }
            else if (args.Item.ItemType == ListItemType.Footer)
            {
                footerSelectAll = new Label();
                footerSelectAll.Text = g.GetResource("TXT_SELECT_NBSP_ALL", this);

                if (Conversion.CInt(Request["MemberFolderID"]) == (int)SystemFolders.Sent)
                {
                    //status column will not be rendered so we'll use next column 
                    args.Item.Cells[(int)Columns.COL_FROM_USERNAME].Wrap = false;
                    args.Item.Cells[(int)Columns.COL_FROM_USERNAME].Controls.Add(footerSelectAll);
                    args.Item.Cells[(int)Columns.COL_FROM_USERNAME].ColumnSpan = 5;
                }
                else
                {
                    args.Item.Cells[(int)Columns.COL_STATUS].ColumnSpan = 3;
                    args.Item.Cells[(int)Columns.COL_STATUS].Wrap = false;
                    args.Item.Cells[(int)Columns.COL_STATUS].Controls.Add(footerSelectAll);
                    args.Item.Cells[(int)Columns.COL_FROM_USERNAME].ColumnSpan = 2;
                }

                currentLabel = new Label();
                currentLabel.Text = "";
                currentLabel.Visible = false;
                if (previousPageLink != null && nextPageLink != null)
                {
                    currentLabel.Visible = true;
                }
                args.Item.Cells[(int)Columns.COL_FROM_USERNAME].Controls.Add(currentLabel);


                // remove all spanned columns not used
                for (int i = (args.Item.Cells.Count - 1); i >= 3; i--)
                {
                    args.Item.Cells.RemoveAt(i);
                }
            }
        }

        #endregion


        #region message functions

        private void EmptyFolder_Click(object sender, EventArgs e)
        {
            bool result = EmailMessageSA.Instance.EmptyFolder(g.Member.MemberID, g.Brand.Site.Community.CommunityID, _CurrentEmailFolder.MemberFolderID);
            bool result2 = EmailMessageSA.Instance.EmptyFolder(g.Member.MemberID, g.Brand.Site.Community.CommunityID, VIPMailUtils.GetMatchingVIPFolderId(_CurrentEmailFolder.MemberFolderID));
            string link = "";
            if (result && result2)
            {
                //link = "/Applications/Email/MailBox.aspx?MemberFolderID=" + _handler.GetCurrentFolderID() + "&rc=TXT_DELETE_MESSAGE_SUCCESS";

                g.Transfer("/Applications/Email/MailBox.aspx?MemberFolderID=" + _handler.GetCurrentFolderID() + "&rc=TXT_DELETE_MESSAGE_SUCCESS");
            }
            else
            {
                g.Transfer("/Applications/Email/MailBox.aspx?MemberFolderID=" + _handler.GetCurrentFolderID() + "&rc=TXT_DELETE_MESSAGE_FAILURE");
            }
        }

        // handles moving selected messages to a specified folder
        private void moveMessages_ServerClick(object sender, System.EventArgs e)
        {
            try
            {
                ArrayList selectedMessages = _handler.GetSelectedMessages(false);

                if (selectedMessages.Count > 0)
                {
                    if (dropDownListMove != null && dropDownListMove.SelectedIndex > 0)
                    {

                        if (EmailMessageSA.Instance.CanMoveEmailsToFolder(g.TargetMemberID, g.TargetCommunityID, selectedMessages, int.Parse(dropDownListMove.SelectedValue)))
                        {
                            if (EmailMessageSA.Instance.MoveToFolder(g.TargetMemberID, g.TargetCommunityID, selectedMessages, int.Parse(dropDownListMove.SelectedValue)))
                            {
                                g.Transfer("/Applications/Email/MailBox.aspx?MemberFolderID=" + _handler.GetCurrentFolderID() + "&rc=TXT_MOVE_MESSAGE_SUCCESS");
                            }
                            else
                            {
                                g.Transfer("/Applications/Email/MailBox.aspx?MemberFolderID=" + _handler.GetCurrentFolderID() + "&rc=TXT_MOVE_MESSAGE_FAILURE");
                            }
                        }
                        else
                        {
                            g.Transfer("/Applications/Email/MailBox.aspx?MemberFolderID=" + _handler.GetCurrentFolderID() + "&rc=TXT_MOVE_MESSAGE_VIP_NOT_ALLOWED");
                        }

                    }
                    else
                    {
                        g.Transfer("/Applications/Email/MailBox.aspx?MemberFolderID=" + _handler.GetCurrentFolderID() + "&rc=TXT_MOVE_MESSAGE_NO_FOLDER");
                    }
                }
                else
                {
                    g.Transfer("/Applications/Email/MailBox.aspx?MemberFolderID=" + _handler.GetCurrentFolderID() + "&rc=TXT_MOVE_MESSAGE_NO_SELECT");
                }

                // refresh the left Navigation to reflect the message deletion
                _handler.LoadNavigation();
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        // handles message deletion of selected item in datagrid
        private void deleteMessage_Click(object sender, System.EventArgs e)
        {
            try
            {
                ArrayList selectedMessages = _handler.GetSelectedMessages(false);
                bool result = false;
                string link = null;
                g.AnalyticsOmniture.PageName = _handler.GetOmniturePageName();

                if (selectedMessages.Count > 0)
                {

                    if (_CurrentEmailFolder.MemberFolderID == (Int32)SystemFolders.Trash)
                    {
                        result = EmailMessageSA.Instance.DeleteMessage(g.Member.MemberID, g.Brand.Site.Community.CommunityID, selectedMessages);
                    }
                    else
                    {
                        result = EmailMessageSA.Instance.MoveToFolder(g.Member.MemberID, g.Brand.Site.Community.CommunityID, selectedMessages, (Int32)SystemFolders.Trash);
                    }

                    string navURL = "/Applications/Email/MailBox.aspx?MemberFolderID=" + _handler.GetCurrentFolderID();

                    if (!string.IsNullOrEmpty(g.Session[MailBoxHandler.MESSAGE_ORIGIN_MAIL_BOX_URL_SESSION_KEY]))
                    {
                        navURL = g.Session[MailBoxHandler.MESSAGE_ORIGIN_MAIL_BOX_URL_SESSION_KEY];
                    }

                    navURL += navURL.Contains("?") ? "&" : "?";

                    if (result)
                    {
                        navURL += "rc=TXT_DELETE_MESSAGE_SUCCESS";
                        navURL = g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.MailBox, WebConstants.Action.MsgDeleteTop, navURL, "");
                        g.Transfer(navURL);
                    }
                    else
                    {
                        navURL += "rc=TXT_DELETE_MESSAGE_FAILURE";
                        navURL = g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.MailBox, WebConstants.Action.MsgDeleteTop, navURL, "");
                        g.Transfer(navURL);
                    }
                }
                else
                {
                    link = "/Applications/Email/MailBox.aspx?MemberFolderID=" + _handler.GetCurrentFolderID() + "&rc=TXT_DELETE_MESSAGE_NO_SELECT";
                    link = g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.MailBox, WebConstants.Action.MsgDeleteTop, link, "");
                    g.Transfer(link);
                }
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        // handles message deletion of all the messages in the folder
        private void deleteAllMessages_Click(object sender, System.EventArgs e)
        {
            try
            {
                EmailMessageCollection currentFolderMessages = EmailMessageSA.Instance.RetrieveMessages(g.Member.MemberID, g.Brand.Site.Community.CommunityID,
                                                             _CurrentEmailFolder.MemberFolderID);
                EmailMessageCollection currentVIPFolderMessages = EmailMessageSA.Instance.RetrieveMessages(g.Member.MemberID, g.Brand.Site.Community.CommunityID,
                                                             VIPMailUtils.GetMatchingVIPFolderId(_CurrentEmailFolder.MemberFolderID));

                ArrayList currentFolderMessagesList = new ArrayList();
                foreach (EmailMessage message in currentFolderMessages)
                {
                    currentFolderMessagesList.Add(message.MemberMailID);
                }
                foreach (EmailMessage message in currentVIPFolderMessages)
                {
                    currentFolderMessagesList.Add(message.MemberMailID);
                }

                EmailMessageSA.Instance.MoveToFolder(g.Member.MemberID, g.Brand.Site.Community.CommunityID, currentFolderMessagesList, (Int32)SystemFolders.Trash);

                string destURL = Request.Url.AbsoluteUri.ToLower();
                if (destURL.IndexOf("rc") != -1)
                {
                    destURL = g.RemoveParamFromURL(destURL, "rc", false);
                }
                if (destURL.IndexOf("msgpg") != -1)
                {
                    destURL = g.RemoveParamFromURL(destURL, "msgpg", false);
                }
                destURL += destURL.Contains("?") ? "&" : "?";
                destURL += "rc=" + "ALL_MESSAGES_WERE_DELETED_FROM_THE_FOLDER";
                g.Transfer(destURL);
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }


        // handles ignoring selected message(s), checks are in place to ensure the member is not ignoring himself
        private void ignoreMessage_Click(object sender, System.EventArgs e)
        {
            try
            {
                ArrayList memberIDs = null;
                g.AnalyticsOmniture.PageName = _handler.GetOmniturePageName();
                string link = null;
                ignoreSelf = false;
                ArrayList selectedMessages = _handler.GetSelectedMessages(true, ref memberIDs);

                if (selectedMessages.Count > 0)
                {
                    bool success = true;

                    foreach (int memberID in memberIDs)
                    {
                        ListSaveResult result = ListSA.Instance.AddListMember(HotListCategory.IgnoreList, g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Session.GetInt(WebConstants.SESSION_PROPERTY_NAME_MEMBERID), memberID, string.Empty, Constants.NULL_INT, false, true);
                        if (result.Status != ListActionStatus.Success)
                        {
                            success = false;
                        }
                    }

                    if (success)
                    {
                        if (ignoreSelf)
                        {
                            link = "/Applications/Email/MailBox.aspx?MemberFolderID=" + _handler.GetCurrentFolderID() + "&rc=TXT_YOU_CANT_IGNORE_YOURSELF";
                            link = g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.MailBox, WebConstants.Action.MsgBlockTop, link, "");
                            g.Transfer(link);
                        }
                        else
                        {
                            link = "/Applications/Email/MailBox.aspx?MemberFolderID=" + _handler.GetCurrentFolderID() + "&rc=TXT_IGNORE_MESSAGE_SUCCESS";
                            link = g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.MailBox, WebConstants.Action.MsgBlockTop, link, "");
                            g.Transfer(link);

                        }
                    }
                    else
                    {
                        if (ignoreSelf)
                        {
                            link = "/Applications/Email/MailBox.aspx?MemberFolderID=" + _handler.GetCurrentFolderID() + "&rc=TXT_YOU_CANT_IGNORE_YOURSELF";
                            link = g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.MailBox, WebConstants.Action.MsgBlockTop, link, "");
                            g.Transfer(link);

                        }
                        else
                        {
                            link = "/Applications/Email/MailBox.aspx?MemberFolderID=" + _handler.GetCurrentFolderID() + "&rc=TXT_IGNORE_MESSAGE_FAILURE";
                            link = g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.MailBox, WebConstants.Action.MsgBlockTop, link, "");
                            g.Transfer(link);

                        }
                    }
                }
                else
                {
                    if (ignoreSelf)
                    {
                        link = "/Applications/Email/MailBox.aspx?MemberFolderID=" + _handler.GetCurrentFolderID() + "&rc=TXT_YOU_CANT_IGNORE_YOURSELF";
                        link = g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.MailBox, WebConstants.Action.MsgBlockTop, link, "");
                        g.Transfer(link);

                    }
                    else
                    {
                        link = "/Applications/Email/MailBox.aspx?MemberFolderID=" + _handler.GetCurrentFolderID() + "&rc=TXT_UNREAD_MESSAGE_NO_SELECT";
                        link = g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.MailBox, WebConstants.Action.MsgBlockTop, link, "");
                        g.Transfer(link);

                    }
                }

                // refresh the left Navigation to reflect the message ignore
                _handler.LoadNavigation();
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        // handles marking selected message(s) as unread
        private void markUnreadMessage_Click(object sender, System.EventArgs e)
        {
            try
            {
                ArrayList selectedMessages = _handler.GetSelectedMessages(false);
                string link = null;
                g.AnalyticsOmniture.PageName = _handler.GetOmniturePageName();
                if (selectedMessages != null && selectedMessages.Count > 0)
                {
                    if (EmailMessageSA.Instance.MarkRead(g.TargetMemberID, g.TargetCommunityID, false, selectedMessages))
                    {
                        link = "/Applications/Email/MailBox.aspx?MemberFolderID=" + _handler.GetCurrentFolderID() + "&rc=TXT_UNREAD_MESSAGE_SUCCESS";
                        link = g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.MailBox, WebConstants.Action.MsgMarkTop, link, "");
                        g.Transfer(link);

                    }
                    else
                    {
                        link = "/Applications/Email/MailBox.aspx?MemberFolderID=" + _handler.GetCurrentFolderID() + "&rc=TXT_UNREAD_MESSAGE_FAILURE";
                        link = g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.MailBox, WebConstants.Action.MsgMarkTop, link, "");
                        g.Transfer(link);
                    }
                }
                else
                {
                    link = "/Applications/Email/MailBox.aspx?MemberFolderID=" + _handler.GetCurrentFolderID() + "&rc=TXT_UNREAD_MESSAGE_NO_SELECT";
                    link = g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.MailBox, WebConstants.Action.MsgMarkTop, link, "");
                    g.Transfer(link);

                }

                // refresh the left Navigation to reflect any message(s) marked unread
                _handler.LoadNavigation();
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        #endregion

        #region page component functions


        // retrieves and builds the 'Move to Folder' drop down list


        //The only resource that uses these tokens is no longer in use anywhere.
        // calculates and builds the account usage string
        /*private void buildAccountUsage() 
        {
            EmailFolder currentFolder = null;

            if((g.MemberEmailFolders != null) && (g.MemberEmailFolders.Count > 0))
            {
                currentFolder = g.MemberEmailFolders[0];
                g.ExpansionTokens.Add("USING", currentFolder.DisplayKBytes.ToString());
                g.ExpansionTokens.Add("LIMIT", MESSAGE_LIMIT_BYTES.ToString());
            }
        }*/
        #endregion

        #region utility functions
        private void makeMessageLink(HyperLink hyperLink, EmailMessage emailMessage, GridType pGridType, bool isStandardMsgSubjectMasked)
        {
            // This is to ignore missing email messages in the database which is happening often on stage due to bad syncs.
            if (hyperLink == null || emailMessage == null)
            {
                return;
            }


            if (MailBoxHandler.ApplyImailPermissions(g) && !MemberPrivilegeAttr.IsPermitMember(g)
                && emailMessage.MailTypeID == MailType.Email)
            {
                hyperLink.NavigateUrl = "/Applications/Subscription/Subscribe.aspx?prtid=30";
                return;
            }

            if (emailMessage.MemberFolderID == (int)SystemFolders.Draft || emailMessage.MemberFolderID == (int)SystemFolders.VIPDraft)
            {
                hyperLink.NavigateUrl = "/Applications/Email/Compose.aspx?MemberMailID=" + emailMessage.MemberMailID;
            }
            else
            {
                if (pGridType == GridType.VIPMail)
                {
                    hyperLink.NavigateUrl = "/Applications/Email/ViewMessage.aspx?MemberMailID=" + emailMessage.MemberMailID + "&MemberFolderID=" + emailMessage.MemberFolderID + "&OrderBy=" + _handler.ResolveSortRequest(MailBoxHandler.VIP_ORDER_BY)
                        + "&CurrentMessageNumber=" + _CurrentVIPMessageNumber + "&MessageCount=" + _VIPMessageCount;
                }
                else
                {
                    hyperLink.NavigateUrl = "/Applications/Email/ViewMessage.aspx?MemberMailID=" + emailMessage.MemberMailID + "&MemberFolderID=" + emailMessage.MemberFolderID + "&OrderBy=" + _handler.ResolveSortRequest("OrderBy")
                        + "&CurrentMessageNumber=" + _CurrentMessageNumber + "&MessageCount=" + _MessageCount + (isStandardMsgSubjectMasked ? "&" + WebConstants.URL_PARAMETER_NAME_VIP_STANDARD_MAIL_MASKED + "=1" : string.Empty);
                }
            }

            #region VIEW ECARD MESSAGE IMPLMENTATION:    5-24-06:  Steve C. / 7-10-06 Martin H.

            // Check if it is an ECard message.
            if (emailMessage.MailTypeID == MailType.ECard)
            {
                // View ECard messages page are hosted on Mingle site, so we have to assign the subject control with a link to Mingle site.
                // Currently the implementation is that Mingle will use our webservice to save a copy of ECard message record in our database,
                // and they will replace the body of the message with a link to their site when they make the webservice call.
                EmailMessage emailMessageWithBody = EmailMessageSA.Instance.RetrieveMessage(g.Member.MemberID, emailMessage.MessageList.GroupID, emailMessage.MessageList.MessageListID);

                string eCardUrl = emailMessageWithBody.Message.MessageBody;

                StringBuilder sbNavigateUrl = new StringBuilder(eCardUrl);

                if (eCardUrl.IndexOf('?') > 0)
                {
                    sbNavigateUrl.Append("&");
                }
                else
                {
                    sbNavigateUrl.Append("?");
                }

                sbNavigateUrl.Append("MessageID=" + emailMessageWithBody.Message.MessageID.ToString());
                sbNavigateUrl.Append("&toUserID=" + emailMessageWithBody.ToMemberID.ToString());
                hyperLink.NavigateUrl = FrameworkGlobals.BuildConnectFrameworkLink(sbNavigateUrl.ToString(), g, true);
            }

            #endregion
        }

        private void SetupTabBar()
        {
            FrameworkControl resourceControl = this.Page.LoadControl("/Applications/Email/MailboxResource.ascx") as FrameworkControl;
            MailBoxHandler.SetupTabBar(mailBoxTabBar, CurrentEmailFolder.SystemFolder, resourceControl, g, Constants.NULL_INT);
        }

        #endregion

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.EmptyFolder.Click += new EventHandler(EmptyFolder_Click);
            this.moveMessages.Click += new System.EventHandler(this.moveMessages_ServerClick);
            this.deleteMessage.Click += new System.EventHandler(this.deleteMessage_Click);
            this.ignoreMessage.Click += new System.EventHandler(this.ignoreMessage_Click);
            this.markUnreadMessage.Click += new System.EventHandler(this.markUnreadMessage_Click);
            this.deleteAllMessages.Click += new System.EventHandler(this.deleteAllMessages_Click);
            this.Load += new System.EventHandler(this.Page_Load);
            this.Init += new System.EventHandler(this.Page_Init);
            this.PreRender += new System.EventHandler(this.Page_PreRender);

        }
        #endregion

        protected override void OnPreRender(EventArgs e)
        {
            _g.AnalyticsOmniture.PageName = _handler.GetOmniturePageName();
            base.OnPreRender(e);

            int folderid = _handler.GetCurrentFolderID();
            if (folderid == (int)SystemFolders.Sent)
            {
                //hide left unread/read column
                if (mailDataGrid != null && mailDataGrid.Columns.Count > 0)
                {
                    mailDataGrid.Columns[(int)Columns.COL_STATUS].Visible = false;
                }
                if (VIPMailDataGrid != null && VIPMailDataGrid.Columns.Count > 0)
                {
                    VIPMailDataGrid.Columns[(int)VIPColumns.VIP_COL_STATUSMASK].Visible = false;
                }
                if (VIPMailHeaderDataGrid != null && VIPMailHeaderDataGrid.Columns.Count > 0)
                {
                    VIPMailHeaderDataGrid.Columns[(int)VIPColumns.VIP_COL_STATUS].Visible = false;
                }

                if (MessageManager.Instance.IsReadRecieptPremiumFeatureEnabled(g.Brand) && MessageManager.Instance.DoesMemberHaveReadRecieptPrivilege(g.Member, g.Brand))
                {
                    mailDataGrid.Columns[(int)Columns.COL_OPENED_DATE_FOR_DISPLAY].Visible = true;
                }
                else
                    mailDataGrid.Columns[(int)Columns.COL_OPENED_DATE_FOR_DISPLAY].Visible = false;

            }
            else
            {
                //lets hide the sent opened col
                mailDataGrid.Columns[(int)Columns.COL_OPENED_DATE_FOR_DISPLAY].Visible = false;
            }

            if (nextPageLink != null)
            {
                lnkNext.Href = g.AnalyticsOmniture.GetPagingURL(nextPageLink, "mailbox", _handler.GetMailBoxName(CurrentEmailFolder.MemberFolderID) + " - NextBottom");
                lnkNext.Visible = true;
            }

            if (previousPageLink != null)
            {
                lnkPrev.Href = g.AnalyticsOmniture.GetPagingURL(previousPageLink, "mailbox", _handler.GetMailBoxName(CurrentEmailFolder.MemberFolderID) + " - PrevBottom");
                lnkPrev.Visible = true;
            }

            if (dgVIPMail.DataSource == null)
            {
                lnkVIPPaging.Visible = false;
            }

            if (Request["overlay"] == "nothanks")
            {
                vipOverlay.ResourceControl = this;
                phVIPOverlay.Visible = vipOverlay.LoadOverlay();
            }
            else
            {
                EmailMessage firstunreadvipmsg = _handler.GetFirstUnreadVIPMessage();
                if (firstunreadvipmsg != null)
                {
                    vipOverlay.VIPEmail = firstunreadvipmsg;
                    vipOverlay.VIPMessageCount = _VIPMessageCount;
                    vipOverlay.CurrentVIPMessageNumber = _CurrentVIPMessageNumber;
                    vipOverlay.ResourceControl = this;
                    phVIPOverlay.Visible = vipOverlay.LoadOverlay();
                }
                else
                {
                    phVIPOverlay.Visible = false;
                }
            }

        }
    }
}
