﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Unsubscribe20.ascx.cs" Inherits="Matchnet.Web.Applications.Email.Unsubscribe20" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>

<h1><asp:Label ID="lblHeader" runat="server" Text=""></asp:Label></h1>
<div id="optout-email">
<div id="main-message"><asp:Label ID="lblOptedOut" runat="server" Text=""></asp:Label></div>
<p class="text-center margin-medium"><strong><asp:Label ID="lblOops" runat="server" Text=""></asp:Label></strong></p>
<p class="buttons">
    <asp:Button ID="btnRevise" runat="server" Text="" CssClass="btn secondary" PostBackUrl="/applications/email/messagesettings.aspx" />
</p>
<p><asp:Label ID="lblYouCanAlso" runat="server" Text=""></asp:Label></p>
<p><asp:Label ID="lbl48Hours" runat="server" Text=""></asp:Label></p>
</div>