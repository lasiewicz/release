﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="MessageSent20.ascx.cs" Inherits="Matchnet.Web.Applications.Email.MessageSent20" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn" TagName="AdUnit" Src="/Framework/UI/Advertising/AdUnit.ascx" %>
<%@ Register TagPrefix="uc1" TagName="HotlistProfileStrip" Src="/Applications/Hotlist/HotlistProfileStrip.ascx" %>

<div id="page-container" class="hotlist-non-home">
    <mn:Title runat="server" id="ttlMessageSent" ResourceConstant="TXT_MESSAGE"/>
	<div class="confirmation-message clearfix">
	    <asp:PlaceHolder runat="server" ID="plDefaultMessageSent">
	        <mn:Image id="GrnMessage" ResourceConstant="ALT_MESSAGE_SENT" titleResourceConstant="TTL_YOUR_MESSAGE_HAS_BEEN_SENT"
			    FileName="icon-email-confirmation.gif" runat="server" />
		    <div class="confirmation-message-content">
		        <h2><mn:txt id="txtYourMessageHasBeenSent" runat="server" ResourceConstant="TXT_YOUR_MESSAGE_HAS_BEEN_SENT" /></h2>
	            <p><mn:txt id="txtMemberMessage" Runat="server" ResourceConstant="TXT_MEMBER_MESSAGE" /></p>
	        </div>
	    </asp:PlaceHolder>
	    <asp:PlaceHolder runat="server" ID="plVIPMessageSent" Visible="false">
	        <mn:Image id="GrnVIPMessage" ResourceConstant="ALT_VIP_MESSAGE_SENT" titleResourceConstant="TTL_YOUR_VIP_MESSAGE_HAS_BEEN_SENT"
			    FileName="icon-allaccess-med.png" runat="server" />
		    <div class="confirmation-message-content">
		        <h2><mn:txt id="txtYourVIPMessageHasBeenSent" runat="server" ResourceConstant="TXT_YOUR_VIP_MESSAGE_HAS_BEEN_SENT" /></h2>
	            <p><mn:txt id="txtMemberVIPMessage" Runat="server" ResourceConstant="TXT_MEMBER_VIP_MESSAGE" /></p>
	        </div>
	    </asp:PlaceHolder>
	</div>
	
	<p><asp:HyperLink Runat="server" titleResourceConstant="TTL_BACK" ID="backToProfileLink">
	    <mn:txt id="txtBackToProfile" runat="server" ResourceConstant="TXT_BACK" /></asp:HyperLink></p>
    		
   <uc1:HotlistProfileStrip id="profileStrip" runat="server" HotlistDirection="1" NoResultsResource="MEMBER_WHO_HAVE_MARKETING" />
	<mn:AdUnit id="adSquare" Size="Square" runat="server" GAMAdSlot="emailconf_middle_300x250"/>
</div>
