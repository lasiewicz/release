﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="MessageSettingsNew20.ascx.cs"
    Inherits="Matchnet.Web.Applications.Email.MessageSettingsNew20" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="cc1" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<div id="page-container">
    
    <asp:Panel runat="server" ID="divLoggedIn">
    <h1>
        <asp:Label runat="server" ID="lblUserName"></asp:Label>
        <mn:Txt ID="txtMessageSetting" runat="server" ResourceConstant="MESSAGE_SETTING" />
    </h1>
    <%-- <h2><mn:txt id="txtMessageWithOtherTitle" runat="server" ResourceConstant="MESSAGE_WITH_OTHER_TITLE" /></h2> --%>
    <asp:Repeater runat="server" ID="rptMessageSetting">
        <ItemTemplate>
            <div class="message-setting-container clear-both">
                <asp:Label ID="lblMessageSettingTitle" runat="server" CssClass="message-settings-label" />
                <div class="float-inside">
                    <asp:CheckBox runat="server" ID="chkMessageSetting" Value='<%# DataBinder.Eval(Container.DataItem, "Value")%>'>
                    </asp:CheckBox>
                    <asp:Label ID="lblMessageSettingCaption" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Content")%>' />
                </div>
            </div>
        </ItemTemplate>
    </asp:Repeater>
    <div class="message-setting-container last clear-both">
        <label class="message-settings-label">
            <mn:Txt ID="txtTakeBreakTitle" runat="server" ResourceConstant="TAKE_A_BREAK_TITLE" />
        </label>
        <div class="float-inside">
            <asp:CheckBox runat="server" ID="chkVacation"></asp:CheckBox></div>
    </div>
     </asp:Panel>
    <asp:Panel runat="server" ID="divNotLoggedIn">
        <h1>
           <!-- place holder for anything that we need to display for a not logged in version -->
        </h1>
    </asp:Panel>
    <!-- begin PrefBorder External Email Alerts/Settings area -->
    <!-- <div id="prefBorder"> -->
    <asp:Panel ID="prefBorder" runat="server" CssClass="offsite-settings">
        <h2>
            <a name="OffSite"></a>
            <mn:Txt ID="txtMesageFromSiteTitle" runat="server" ResourceConstant="MESSAGE_FROM_SITE_TITLE" />
            <mn:Txt ID="txtMessageFromSiteTitle2" runat="server" ResourceConstant="MESSAGE_FROM_SITE_TITLE2" />
            <mn:Txt ID="txtChangeMessageFromSiteSetting" runat="server" ResourceConstant="MESSAGE_FROM_SITE_CHANGE" />
        </h2>
        <div id="msgPrefContainer">
            <h3 class="settings-header">
                <%-- Matches by Mail --%>
                <mn:Txt ID="txtMatchByMailTitle" runat="server" ResourceConstant="MATCH_BY_MAIL_TITLE" />
                <a rel="click" href="#" id="yourmatches-whatsthis" class="yourmatches whatsthis">
                    <mn:Txt ID="txt2" runat="server" ResourceConstant="WHAT_IS_THIS" />
                </a>
            </h3>
            <div id="yourmatches-background" class="message-options-sub-container clear-both">
                <mn:Txt ID="txtMatchByMailTitle2" runat="server" ResourceConstant="MATCH_BY_MAIL_TITLE2" />
                <div class="message-options-container">
                    <asp:RadioButtonList ID="rdoListMatchNewsLetterPeriod" runat="server" DataTextField="Content"
                        DataValueField="Value" DataMember="Content" DataTextFormatString="{0}" RepeatDirection="Horizontal"
                        RepeatLayout="Flow">
                    </asp:RadioButtonList>
                </div>
                <div id="yourmatches-answer" class="hide message-option-info">
                    <table border="0" cellspacing="8" cellpadding="0">
                        <tr>
                            <td>
                                <mn:Image ID="Image6" runat="server" FileName="scrn_email_yourmatches.gif"></mn:Image>
                            </td>
                            <td>
                                <p class="messageSettingsHelpParagraph">
                                    <mn:Txt ID="txtMatchByMailHelpText" runat="server" ResourceConstant="MATCH_BY_MAIL_HELP_TEXT" />
                                </p>
                            </td>
                        </tr>
                    </table>
                    <p class="messageSettingsHelpParagraphHide">
                        <a rel="click" href="#">
                            <mn:Txt ID="txtMatchByMailHideWhatIsThis" runat="server" ResourceConstant="HIDE_WHAT_IS_THIS" />
                        </a>
                    </p>
                </div>
                <!-- end yourmatches-answer -->
            </div>
            <!-- end yourmatches-background -->
            <asp:Panel ID="pnlNewMemberEmailSettings" runat="server" Visible="false">
                <h3 class="settings-header">
                    <%-- New Members by Mail --%>
                    <mn:Txt ID="txtNewMemberMailTitle" runat="server" ResourceConstant="NEW_MEMBER_MAIL_TITLE" />
                    <a rel="click" href="#" id="newmembermail-whatsthis" class="newmembermail whatsthis">
                        <mn:Txt ID="txt3" runat="server" ResourceConstant="WHAT_IS_THIS" />
                    </a>
                </h3>
                <div class="message-options-sub-container clear-both">
                    <mn:Txt ID="txtNewMemberMailTitle2" runat="server" ResourceConstant="NEW_MEMBER_TITLE2" />
                    <div class="message-options-container">
                        <asp:RadioButtonList ID="rdoListNewMemberEmailPeriod" runat="server" DataTextField="Content"
                            DataValueField="Value" DataMember="Content" DataTextFormatString="{0}" RepeatDirection="Horizontal"
                            RepeatLayout="Flow">
                        </asp:RadioButtonList>
                    </div>
                    <div id="newmembermail-answer" class="hide message-option-info">
                        <table border="0" cellspacing="8" cellpadding="0">
                            <tr>
                                <td>
                                    <mn:Image ID="Image10" runat="server" FileName="scrn-email-newmemmail.gif"></mn:Image>
                                </td>
                                <td>
                                    <p class="messageSettingsHelpParagraph">
                                        <mn:Txt ID="txtNewMemberEmailHelpText" runat="server" ResourceConstant="NEW_MEMBER_EMAIL_HELP_TEXT" />
                                    </p>
                                </td>
                            </tr>
                        </table>
                        <p class="messageSettingsHelpParagraphHide">
                            <a rel="click" href="#">
                                <mn:Txt ID="txtNewMemberEmailHideWhatIsThis" runat="server" ResourceConstant="HIDE_WHAT_IS_THIS" />
                            </a>
                        </p>
                    </div>
                </div>
            </asp:Panel>
            <asp:PlaceHolder runat="server" ID="plcYNM">
                <h3 class="settings-header">
                    <%-- Click --%>
                    <mn:Txt ID="txtClickTitle" runat="server" ResourceConstant="CLICK_TITLE" />
                </h3>
                <div class="message-options-sub-container clear-both">
                    <mn:Txt ID="txtClickMailTitle" runat="server" ResourceConstant="CLICK_MAIL_TITLE" />
                    <a rel="click" href="#" id="clickemail-whatsthis" class="clickemail whatsthis">
                        <mn:Txt ID="txtClickMailWhatIsThis" runat="server" ResourceConstant="WHAT_IS_THIS" />
                    </a>
                    <div id="clickemail-background">
                        <mn:Txt ID="txtClickMailTitle2" runat="server" ResourceConstant="CLICK_MAIL_TITLE2" />
                        <div class="message-options-container">
                            <asp:RadioButtonList ID="rdoListGotAClickEmailPeriod" DataTextField="Content" DataValueField="Value"
                                runat="server" RepeatLayout="Flow" RepeatDirection="Horizontal" DataTextFormatString="{0}&amp;nbsp;&amp;nbsp;&amp;nbsp;"
                                DataMember="Content" />
                        </div>
                        <div id="clickemail-answer" class="hide message-option-info">
                            <table border="0" cellspacing="8" cellpadding="0">
                                <tr>
                                    <td>
                                        <mn:Image ID="Image4" runat="server" FileName="scrn_email_clickemail.gif"></mn:Image>
                                    </td>
                                    <td>
                                        <p class="messageSettingsHelpParagraph">
                                            <mn:Txt ID="Txt1" runat="server" ResourceConstant="CLICK_MAIL_HELP_TEXT" />
                                        </p>
                                    </td>
                                </tr>
                            </table>
                            <p class="messageSettingsHelpParagraphHide">
                                <a rel="click" href="#">
                                    <mn:Txt ID="txtClickMailHideWhatIsThis" runat="server" ResourceConstant="HIDE_WHAT_IS_THIS" />
                                </a>
                            </p>
                        </div>
                        <!-- end clickemail-answer -->
                    </div>
                    <!-- end clickemail-background -->
                </div>
                <!-- end message-options-sub-container -->
            </asp:PlaceHolder>
            <asp:PlaceHolder runat="server" ID="phNewSettings">
                <asp:PlaceHolder runat="server" ID="plcYNM2">
                    <div class="message-options-sub-container clear-both">
                        <div class="options-title">
                            <mn:Txt ID="txtClickAlertTitle" runat="server" ResourceConstant="CLICK_ALERT_TITLE" />
                            <a rel="click" href="#" id="clickalert-whatsthis" class="clickalert whatsthis">
                                <mn:Txt ID="txtClickAlertWhatIsThis" runat="server" ResourceConstant="WHAT_IS_THIS" />
                            </a>
                        </div>
                        <div id="clickalert-background">
                            <asp:CheckBox runat="server" ID="chkClickAlert"></asp:CheckBox>
                            <div id="clickalert-answer" class="hide message-option-info">
                                <table border="0" cellspacing="8" cellpadding="0">
                                    <tr>
                                        <td>
                                            <mn:Image ID="Image8" runat="server" FileName="scrn_email_clickalert.gif"></mn:Image>
                                        </td>
                                        <td>
                                            <p class="messageSettingsHelpParagraph">
                                                <mn:Txt ID="txtClickAlertHelpText" runat="server" ResourceConstant="CLICK_ALERT_HELP_TEXT" />
                                            </p>
                                        </td>
                                    </tr>
                                </table>
                                <p class="messageSettingsHelpParagraphHide">
                                    <a rel="click" href="#">
                                        <mn:Txt ID="txtClickAlertHideWhatIsThis" runat="server" ResourceConstant="HIDE_WHAT_IS_THIS" />
                                    </a>
                                </p>
                            </div>
                            <!-- end clickalert-answer -->
                        </div>
                        <!-- end clickalert-background -->
                    </div>
                </asp:PlaceHolder>
                <!-- end message-options-sub-container -->
                <h3 class="settings-header">
                    <mn:Txt ID="txtAlertTitle" runat="server" ResourceConstant="ALERT_TITLE" />
                </h3>
                <asp:PlaceHolder runat="server" ID="phAlertHotList">
                    <div class="message-options-sub-container clear-both">
                        <div class="options-title">
                            <mn:Txt ID="txtHotListTitle" runat="server" ResourceConstant="HOT_LIST_TITLE" />
                            <a rel="click" href="#" id="hotlistalerts-whatsthis" class="hotlistalerts whatsthis">
                                <mn:Txt ID="txtHotListWhatIsThis" runat="server" ResourceConstant="WHAT_IS_THIS" />
                            </a>
                        </div>
                        <div id="hotlistalerts_background" class="options-text">
                            <asp:CheckBox runat="server" ID="chkHotListAlert"></asp:CheckBox>
                            <div id="hotlistalerts-answer" class="hide message-option-info">
                                <table border="0" cellspacing="8" cellpadding="0">
                                    <tr>
                                        <td>
                                            <mn:Image ID="Image9" runat="server" FileName="scrn_email_hotlistalerts.gif"></mn:Image>
                                        </td>
                                        <td>
                                            <p class="messageSettingsHelpParagraph">
                                                <mn:Txt ID="txtHotListHelpText" runat="server" ResourceConstant="HOT_LIST_HELP_TEXT" />
                                            </p>
                                        </td>
                                    </tr>
                                </table>
                                <p class="messageSettingsHelpParagraphHide">
                                    <a rel="click" href="#">
                                        <mn:Txt ID="txtHotListHideWhatIsThis" runat="server" ResourceConstant="HIDE_WHAT_IS_THIS" />
                                    </a>
                                </p>
                            </div>
                            <!-- end hotlistalerts-answer -->
                        </div>
                        <!-- end hotlistalerts_background -->
                    </div>
                    <!-- end message-options-sub-container -->
                </asp:PlaceHolder>
                <div class="message-options-sub-container clear-both">
                    <div class="options-title">
                        <mn:Txt ID="txtEmailAlertTitle" runat="server" ResourceConstant="EMAIL_ALERT_TITLE" />
                        <a rel="click" href="#" id="emailalerts-whatsthis" class="emailalerts whatsthis">
                            <mn:Txt ID="txtEmailAlertWhatIsThis" runat="server" ResourceConstant="WHAT_IS_THIS" />
                        </a>
                    </div>
                    <span id="emailalerts_background" class="options-text">
                        <asp:CheckBox runat="server" ID="chkEmailAlert"></asp:CheckBox>
                        <div id="emailalerts-answer" class="hide message-option-info">
                            <table border="0" cellspacing="8" cellpadding="0">
                                <tr>
                                    <td>
                                        <mn:Image ID="Image7" runat="server" FileName="scrn_email_emailalerts.gif"></mn:Image>
                                    </td>
                                    <td>
                                        <p class="messageSettingsHelpParagraph">
                                            <mn:Txt ID="txtEmailAlertHelpText" runat="server" ResourceConstant="EMAIL_ALERT_HELP_TEXT" />
                                        </p>
                                    </td>
                                </tr>
                            </table>
                            <p class="messageSettingsHelpParagraphHide">
                                <a rel="click" href="#">
                                    <mn:Txt ID="txtEmailAlertHideWhatIsThis" runat="server" ResourceConstant="HIDE_WHAT_IS_THIS" />
                                </a>
                            </p>
                        </div>
                        <!-- end emailalerts-answer -->
                    </span>
                    <!-- end emailalerts_background -->
                </div>
                <!-- end message-options-sub-container -->
                <asp:PlaceHolder runat="server" ID="phAlertEcard">
                    <div class="message-options-sub-container clear-both">
                        <div class="options-title">
                            <mn:Txt ID="txtECardAlertTitle" runat="server" ResourceConstant="ECARD_ALERT_TITLE" />
                            <a rel="click" href="#" id="ecardalerts-whatsthis" class="ecardalerts whatsthis">
                                <mn:Txt ID="txtECardWhatIsThis" runat="server" ResourceConstant="WHAT_IS_THIS" />
                            </a>
                        </div>
                        <span id="ecardalerts_background" class="options-text">
                            <asp:CheckBox runat="server" ID="chkECardAlert"></asp:CheckBox>
                            <div id="ecardalerts-answer" class="hide message-option-info">
                                <table border="0" cellspacing="8" cellpadding="0">
                                    <tr>
                                        <td>
                                            <mn:Image ID="Image1" runat="server" FileName="scrn_email_ecardalerts.gif"></mn:Image>
                                        </td>
                                        <td>
                                            <p class="messageSettingsHelpParagraph">
                                                <mn:Txt ID="txtEcardHelpText" runat="server" ResourceConstant="ECARD_ALERT_HELP_TEXT" />
                                            </p>
                                        </td>
                                    </tr>
                                </table>
                                <p class="messageSettingsHelpParagraphHide">
                                    <a rel="click" href="#">
                                        <mn:Txt ID="txtEcardAlertHideWhatIsThis" runat="server" ResourceConstant="HIDE_WHAT_IS_THIS" />
                                    </a>
                                </p>
                            </div>
                            <!-- end ecardalerts-answer -->
                        </span>
                        <!-- end ecardalerts_background -->
                    </div>
                    <!-- end message-options-sub-container -->
                </asp:PlaceHolder>
                <asp:PlaceHolder runat="server" ID="phAlertProfileViewed">
                    <div class="message-options-sub-container clear-both">
                        <div class="options-title">
                            <strong><mn:Txt ID="txtProfileViewedAlertTitle" runat="server" ResourceConstant="PROFILE_VIEWED_ALERT_TITLE" /></strong>
                            <a rel="click" href="#" id="profileviewedalerts-whatsthis" class="profileviewedalerts whatsthis">
                                <mn:Txt ID="txtProfileViewedWhatIsThis" runat="server" ResourceConstant="WHAT_IS_THIS" />
                            </a>
                        </div>
                        <span id="profileviewedalerts_background" class="options-text">
                            <asp:CheckBox runat="server" ID="chkProfileViewedAlert"></asp:CheckBox>
                            <div id="profileviewedalerts-answer" class="hide message-option-info">
                                <p class="messageSettingsHelpParagraph">
                                    <mn:Txt ID="txt6" runat="server" ResourceConstant="PROFILE_VIEWED_ALERT_HELP_TEXT" />
                                </p>

                                <p class="messageSettingsHelpParagraphHide">
                                    <a rel="click" href="#">
                                        <mn:Txt ID="txt7" runat="server" ResourceConstant="HIDE_WHAT_IS_THIS" />
                                    </a>
                                </p>
                            </div>
                            <!-- end profileviewed-answer -->
                        </span>
                        <!-- end profileviewed_background -->
                    </div>
                    <!-- end message-options-sub-container -->
                </asp:PlaceHolder>
            </asp:PlaceHolder>
            <h3 class="settings-header">
                <mn:Txt ID="txtNewsAlertTitle" runat="server" ResourceConstant="NEWS_EVENT_TITLE" />
            </h3>
            <div class="message-options-sub-container clear-both">
                <div class="options-title">
                    <mn:Txt ID="txtNewsLetterTitle" runat="server" ResourceConstant="NEWS_LETTERS_TITLE" />
                    <a rel="click" href="#" id="newsletters-whatsthis" class="newsletters whatsthis">
                        <mn:Txt ID="txtNewsLetterWhatIsThis" runat="server" ResourceConstant="WHAT_IS_THIS" />
                    </a>
                </div>
                <span id="newsletters_background" class="options-text">
                    <asp:CheckBox runat="server" ID="chkNewsLetter"></asp:CheckBox>
                    <div id="newsletters-answer" class="hide message-option-info">
                        <table border="0" cellspacing="8" cellpadding="0">
                            <tr>
                                <td>
                                    <p class="messageSettingsHelpParagraph">
                                        <mn:Txt ID="txtNewsLetterHelpText" runat="server" ResourceConstant="NEWS_LETTER_HELP_TEXT" />
                                    </p>
                                </td>
                            </tr>
                        </table>
                        <p class="messageSettingsHelpParagraphHide">
                            <a rel="click" href="#">
                                <mn:Txt ID="txtNewsLetterHideWhatIsThis" runat="server" ResourceConstant="HIDE_WHAT_IS_THIS" />
                            </a>
                        </p>
                    </div>
                    <!-- end newsletters-answer -->
                </span>
                <!-- end newsletters_background -->
            </div>
            <!-- end message-options-sub-container -->
            <div class="message-options-sub-container clear-both">
                <div class="options-title">
                    <mn:Txt ID="txtInvitationTitle" runat="server" ResourceConstant="INVITATION_TITLE" />
                    <a rel="click" href="#" id="invitations-whatsthis" class="invitations whatsthis">
                        <mn:Txt ID="txtInvitationWhatIsThis" runat="server" ResourceConstant="WHAT_IS_THIS" />
                    </a>
                </div>
                <span id="invitations_background" class="options-text">
                    <asp:CheckBox runat="server" ID="chkInvitation"></asp:CheckBox>
                    <div id="invitations-answer" class="hide message-option-info">
                        <table border="0" cellspacing="8" cellpadding="0">
                            <tr>
                                <td>
                                    <p class="messageSettingsHelpParagraph">
                                        <mn:Txt ID="txtInvitationHelpText" runat="server" ResourceConstant="INVITATION_HELP_TEXT" />
                                    </p>
                                </td>
                            </tr>
                        </table>
                        <p class="messageSettingsHelpParagraphHide">
                            <a rel="click" href="#">
                                <mn:Txt ID="txtInvitationHideWhatIsThis" runat="server" ResourceConstant="HIDE_WHAT_IS_THIS" />
                            </a>
                        </p>
                    </div>
                    <!-- end invitations-answer -->
                </span>
                <!-- end invitations_background -->
            </div>
            <!-- end message-options-sub-container -->
            <div class="message-options-sub-container clear-both">
                <div class="options-title">
                    <mn:Txt ID="txtEmailOfferTitle" runat="server" ResourceConstant="EMAIL_OFFER_TITLE" />
                    <a rel="click" href="#" id="emailoffers-whatsthis" class="emailoffers whatsthis">
                        <mn:Txt ID="txtEmailOfferWhatIsThis" runat="server" ResourceConstant="WHAT_IS_THIS" />
                    </a>
                </div>
                <span id="emailoffers_background" class="options-text">
                    <asp:CheckBox runat="server" ID="chkEmailOffer"></asp:CheckBox>
                    <div id="emailoffers-answer" class="hide message-option-info">
                        <table border="0" cellspacing="8" cellpadding="0">
                            <tr>
                                <td>
                                    <p class="messageSettingsHelpParagraph">
                                        <mn:Txt ID="txtEmailOfferHelpText" runat="server" ResourceConstant="EMAIL_OFFER_HELP_TEXT" />
                                    </p>
                                </td>
                            </tr>
                        </table>
                        <p class="messageSettingsHelpParagraphHide">
                            <a rel="click" href="#">
                                <mn:Txt ID="txtEmailOfferHideWhatIsThis" runat="server" ResourceConstant="HIDE_WHAT_IS_THIS" />
                            </a>
                        </p>
                    </div>
                    <!-- end emailoffers-answer -->
                </span>
                <!-- end emailoffers_background -->
            </div>
            <!-- end message-options-sub-container -->
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlDNE" runat="server" Visible="false">
        <h2 class="prefHeader">
            <a name="OffSite"></a>
            <mn:Txt ID="txtMesageFromSiteTitleDNE" runat="server" ResourceConstant="MESSAGE_FROM_SITE_TITLE" />
            <mn:Txt ID="txtMessageFromSiteTitle2DNE" runat="server" ResourceConstant="MESSAGE_FROM_SITE_TITLE2" />
        </h2>
        <div class="prefContainer">
            <mn:Txt ID="txtMemberOnDNE" runat="server" ResourceConstant="MEMBER_ON_DNE_MESSAGE" />
            <br />
        </div>
    </asp:Panel>
    <!--</div> -->
    <!-- End PrefBorder External Email Alerts/Settings area -->
    <p class="text-center">
        <cc1:FrameworkButton ID="btnSave" runat="server" ResourceConstant="TXT_SAVE_OFFSITE_MESSAGE_SETTINGS"
            class="activityButton btn primary"></cc1:FrameworkButton></p>
    <!--//-->
</div>
