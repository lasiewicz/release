﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="FlirtCollapsible.ascx.cs" Inherits="Matchnet.Web.Applications.Email.FlirtCollapsible" %>
<%@ Register TagPrefix="cc1" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>

<script type="text/javascript">

$j(document).ready(function(){
    $j('#smiles ul li:first-child a').addClass('open');
    $j("#smiles .category").click(function() {
       if($j(this).siblings().is(':hidden')){ 
            $j(this).addClass('open');
            $j(this).siblings().show();
       }else{
            $j(this).removeClass('open');
            $j(this).siblings().hide();
       }
        return false;
    })
});
</script>


<mn:Title runat="server" id="ttlMatches" ResourceConstant="TXT_SEND_A_TEASE" ImageName="hdr_message_sent.gif" CommunitiesForImage="CI;" />
<asp:PlaceHolder id="miniProfile" Runat="server" />

<div class="margin-medium collapsible" id="smiles">
    <h2><mn2:FrameworkLiteral ID="literalSelectTease" runat="server" ResourceConstant="TXT_SELECT_TEASE"></mn2:FrameworkLiteral></h2>
    <ul>
        <asp:Repeater ID="repeaterCategories" runat="server" OnItemDataBound="repeaterCategories_ItemDataBound">
            <ItemTemplate>
                <%--Categories--%>
                <li class="category-wrapper">
                    <a id="lnkCategoryName" href="#" runat="server" class="category"><asp:Literal ID="literalCategoryName" runat="server"></asp:Literal></a>
                    <%--<ul>--%>
                    <asp:Literal ID="literalULStart" runat="server"></asp:Literal>
                        <asp:Repeater ID="repeaterItems" runat="server" OnItemDataBound="repeaterItems_ItemDataBound">
                            <ItemTemplate>
                                <%--category items--%>
                                <li><asp:Literal ID="literalItemRadioButton" runat="server"></asp:Literal></li>
                            </ItemTemplate>
                        </asp:Repeater>
                    <asp:Literal ID="literalULEnd" runat="server" Text="</ul>"></asp:Literal>
                    <%--</ul>--%>
                </li>
                
                <%--this promo for category, not sure if we even use this, most likely will never render but we'll keep it here just in case as it currently exists in flirts20.ascx--%>
                <asp:PlaceHolder ID="phCategoryPromo" runat="server" Visible="false">
                    <div class="promo-insert">
			            <mn:Txt ID="txtPromo" runat="server"/>
		            </div>
		        </asp:PlaceHolder>
            </ItemTemplate>
        </asp:Repeater>
    </ul>
</div>

<div>
    <mn2:FrameworkButton runat="server" id="btnSubmit" CssClass="btn primary margin-medium" ResourceConstant="BTN_TEASE_AWAY" />
</div>

<mn:txt id="txtCallToAction" runat="server" ExpandImageTokens="true" ResourceConstant="FLIRT_CALL_TO_ACTION" Visible=false />
