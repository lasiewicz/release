﻿using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using Matchnet.Lib;
using Matchnet.Lib.Util;
using Matchnet.Web.Framework;
using Matchnet.Web.Applications.HotList;

namespace Matchnet.Web.Applications.Email
{
    public partial class TeaseSent20 : FrameworkControl
    {
        private void Page_Load(object sender, System.EventArgs e)
        {
            try
            {
                string teasesLeft = Request.Params.Get("teasesLeft");
                string memberId = Request.Params.Get("MemberId");

                if (teasesLeft != null && teasesLeft != "")
                {
                    int teaseRemaining = Matchnet.Conversion.CInt(teasesLeft, Constants.NULL_INT);

                    if (teaseRemaining != Constants.NULL_INT)
                    {
                        this.litTeasesLeft.Text = teasesLeft;
                    }
                }

              //  lblSubscribeNow.Text = string.Format(g.GetResource("TXT_SUBSCRIBE_MESSAGE", this), "<a href='/Applications/Subscription/Subscribe.aspx'>" + g.GetResource("TXT_SUBSCRIBE_NOW", this) + "</a>");

                if (memberId != null && memberId.Trim().Length > 0)
                {
                    if (g.Session[Tease.TEASE_ORIGIN_URL_SESSION_KEY] != null && !g.Session[Tease.TEASE_ORIGIN_URL_SESSION_KEY].Equals(String.Empty))
                    {
                        backToProfileLink.NavigateUrl = g.Session[Tease.TEASE_ORIGIN_URL_SESSION_KEY];
                        if (Matchnet.Content.ServiceAdapters.PageConfigSA.Instance.GetPageName(g.Session[Tease.TEASE_ORIGIN_URL_SESSION_KEY]).ToLower() != "viewprofile")
                        {
                            txtBackToProfile.ResourceConstant = "TXT_BACK";
                            txtBackToProfile.TitleResourceConstant = "TTL_BACK";
                        }
                    }
                    else
                    {
                        backToProfileLink.NavigateUrl = "/Applications/MemberProfile/ViewProfile.aspx?MemberId=" + memberId;
                    }
                }

                // Remove session key so it doesn't interfere with future calls
                if (g.Session[Tease.TEASE_ORIGIN_URL_SESSION_KEY] != null)
                {
                    g.Session.Remove(Tease.TEASE_ORIGIN_URL_SESSION_KEY);
                }

               

            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }


        protected override void OnPreRender(EventArgs e)
        {
            try
            {
                // Omniture Analytics - Event Tracking
                if (Convert.ToBoolean(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ANALYTICS_OMNITURE", _g.Brand.Site.Community.CommunityID, _g.Brand.Site.SiteID)))
                {
                    // Message Sent
                    _g.AnalyticsOmniture.AddEvent("event11");

                    // Message Type
                    _g.AnalyticsOmniture.Evar26 = "tease";
                }

                base.OnPreRender(e);
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);

        }
        #endregion
    }
}