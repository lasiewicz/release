﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="MessageSettingsNew30.ascx.cs" Inherits="Matchnet.Web.Applications.Email.MessageSettingsNew30" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="cc1" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<div id="page-container">
    
    <asp:Panel runat="server" ID="divLoggedIn">
    
    <h1>
        <asp:Label runat="server" ID="lblUserName"></asp:Label>
        <mn:Txt ID="txtMessageSetting" runat="server" ResourceConstant="MESSAGE_SETTING" />
    </h1>
    <asp:Repeater runat="server" ID="rptMessageSetting" OnItemDataBound="rptMessageSetting_ItemDataBound">
        <ItemTemplate>
            <div class="message-setting-container clear-both">
                <asp:Label ID="lblMessageSettingTitle" runat="server" CssClass="message-settings-label" />
                <div class="float-inside">
                    <asp:CheckBox runat="server" ID="chkMessageSetting" Value='<%# DataBinder.Eval(Container.DataItem, "Value")%>'>
                    </asp:CheckBox>
                    <asp:Label ID="lblMessageSettingCaption" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Content")%>' />
                </div>
            </div>
        </ItemTemplate>
    </asp:Repeater>
    <div class="message-setting-container last clear-both">
        <label class="message-settings-label">
            <mn:Txt ID="txtTakeBreakTitle" runat="server" ResourceConstant="TAKE_A_BREAK_TITLE" />
        </label>
        <div class="float-inside">
            <asp:CheckBox runat="server" ID="chkVacation"></asp:CheckBox></div>
    </div>
    </asp:Panel>
    <asp:Panel runat="server" ID="divNotLoggedIn">
        <h1>
           <!-- place holder for anything that we need to display for a not logged in version -->
        </h1>
    </asp:Panel>
    <%--This is used when SKIP_EMAIL_UNSUBSCRIBE_PAGE is enabled--%>
    <asp:Label ID="lblOptedOut" runat="server" Text="" CssClass="PageMessage notification msg-icon" Visible="False"/>
    <!-- begin PrefBorder External Email Alerts/Settings area -->
    <asp:Panel ID="prefBorder" runat="server" CssClass="offsite-settings">
        <h2><mn:Txt ID="txtMesageFromSiteTitle" runat="server" ResourceConstant="EMAIL_SETTINGS" /></h2>
        <small><asp:Literal ID="litEmail" runat="server"></asp:Literal>
        <a href="/Applications/MemberProfile/ChangeEmail.aspx"><mn:Txt ID="txtChange" runat="server" ResourceConstant="TXT_CHANGE" /></a>
        </small>
        <div id="msgPrefContainer">
            <asp:Panel ID="pnlNewMemberEmailSettings" runat="server" Visible="false">
                <h3 class="settings-header">
                    <%-- New Members by Mail --%>
                    <mn:Txt ID="txtNewMemberMailTitle" runat="server" ResourceConstant="NEW_MEMBER_MAIL_TITLE" />
                    <a rel="click" href="#" id="newmembermail-whatsthis" class="newmembermail whatsthis">
                        <mn:Txt ID="txt3" runat="server" ResourceConstant="WHAT_IS_THIS" />
                    </a>
                </h3>
                <div class="message-options-sub-container clear-both">
                    <mn:Txt ID="txtNewMemberMailTitle2" runat="server" ResourceConstant="NEW_MEMBER_TITLE2" />
                    <div class="message-options-container">
                        <asp:RadioButtonList ID="rdoListNewMemberEmailPeriod" runat="server" DataTextField="Content"
                            DataValueField="Value" DataMember="Content" DataTextFormatString="{0}" RepeatDirection="Horizontal"
                            RepeatLayout="Flow">
                        </asp:RadioButtonList>
                    </div>
                    <div id="newmembermail-answer" class="hide message-option-info">
                        <table border="0" cellspacing="8" cellpadding="0">
                            <tr>
                                <td>
                                    <mn:Image ID="Image10" runat="server" FileName="scrn-email-newmemmail.gif"></mn:Image>
                                </td>
                                <td>
                                    <p class="messageSettingsHelpParagraph">
                                        <mn:Txt ID="txtNewMemberEmailHelpText" runat="server" ResourceConstant="NEW_MEMBER_EMAIL_HELP_TEXT" />
                                    </p>
                                </td>
                            </tr>
                        </table>
                        <p class="messageSettingsHelpParagraphHide">
                            <a rel="click" href="#">
                                <mn:Txt ID="txtNewMemberEmailHideWhatIsThis" runat="server" ResourceConstant="HIDE_WHAT_IS_THIS" />
                            </a>
                        </p>
                    </div>
                </div>
            </asp:Panel>
            <h3 class="settings-header">
                <mn:Txt ID="txtAlertTitle" runat="server" ResourceConstant="ALERT_TITLE" />
            </h3>
            <%--Secret Admirer Email--%>
            <asp:PlaceHolder runat="server" ID="plcYNM">
                <div class="message-options-sub-container clear-both">
                    <h4 class="options-title">
                        <mn:Txt ID="txtClickMailTitle" runat="server" ResourceConstant="CLICK_MAIL_TITLE" />
                    </h4>
                    <div id="clickemail-background" class="options-text">
                        <mn:Txt ID="txtClickMailTitle2" runat="server" ResourceConstant="CLICK_MAIL_TITLE2" />
                        <span class="message-options-container">
                            <asp:RadioButtonList ID="rdoListGotAClickEmailPeriod" DataTextField="Content" DataValueField="Value"
                                runat="server" RepeatLayout="Flow" RepeatDirection="Horizontal" DataTextFormatString="{0}"
                                DataMember="Content" />
                        </span>                       
                    </div>
                </div>
            </asp:PlaceHolder>
            <%--Secret Admirer Alert--%>
            <asp:PlaceHolder runat="server" ID="plcYNM2">
                <div class="message-options-sub-container clear-both">
                    <h4 class="options-title">
                        <mn:Txt ID="txtClickAlertTitle" runat="server" ResourceConstant="CLICK_ALERT_TITLE" />
                    </h4>
                    <div id="clickalert-background" class="options-text">
                        <asp:CheckBox runat="server" ID="chkClickAlert"></asp:CheckBox>
                    </div>
                </div>
            </asp:PlaceHolder>
            <%--Matches by Mail --%>
            <div class="message-options-sub-container clear-both">
                <h4 class="options-title">
                    <mn:Txt ID="txtMatchMailTitle" runat="server" ResourceConstant="MATCH_BY_MAIL_TITLE" />
                </h4>
                <asp:PlaceHolder ID="phMatchesRadio" runat="server">
                <div id="yourmatches-background" class="options-text">
                    <mn:Txt ID="txtMatchByMailTitle2" runat="server" ResourceConstant="MATCH_BY_MAIL_TITLE2" />
                    <a href="/Applications/Search/SearchPreferences.aspx" runat="server" id="aMatchPreferncesRadio"><mn:Txt ID="txt2" runat="server" ResourceConstant="MATCH_PREFERENCES" /></a>
                    <div class="message-options-container">
                        <asp:RadioButtonList ID="rdoListMatchNewsLetterPeriod" runat="server" DataTextField="Content"
                            DataValueField="Value" DataMember="Content" DataTextFormatString="{0}" RepeatDirection="Horizontal"
                            RepeatLayout="Flow">
                        </asp:RadioButtonList>
                    </div>
                </div>
                </asp:PlaceHolder>
                <asp:PlaceHolder ID="phMatchesCheckbox" runat="server">
                <div id="matches_background" class="options-text">
                    <asp:CheckBox runat="server" ID="chkMatchesDaily"></asp:CheckBox>
                    <a href="/Applications/Search/SearchPreferences.aspx" runat="server" id="aMatchPrefernces"><mn:Txt ID="txt1" runat="server" ResourceConstant="MATCH_PREFERENCES" /></a>
                </div>
                </asp:PlaceHolder>
            </div>
            <%--Hotlist Alert--%>
            <asp:PlaceHolder runat="server" ID="phAlertHotList">
                <div class="message-options-sub-container clear-both">
                    <h4 class="options-title">
                        <mn:Txt ID="txtHotListTitle" runat="server" ResourceConstant="HOT_LIST_TITLE" />
                    </h4>
                    <div id="hotlistalerts_background" class="options-text">
                        <asp:CheckBox runat="server" ID="chkHotListAlert"></asp:CheckBox>
                    </div>
                </div>
            </asp:PlaceHolder>
            <%--Email Alert--%>
            <div class="message-options-sub-container clear-both">
                <h4 class="options-title">
                    <mn:Txt ID="txtEmailAlertTitle" runat="server" ResourceConstant="EMAIL_ALERT_TITLE" />
                </h4>
                <span id="emailalerts_background" class="options-text">
                    <asp:CheckBox runat="server" ID="chkEmailAlert"></asp:CheckBox>
                </span>
            </div>
            <%--Ecard Alert--%>
            <asp:PlaceHolder runat="server" ID="phAlertEcard">
                <div class="message-options-sub-container clear-both">
                    <h4 class="options-title">
                        <mn:Txt ID="txtECardAlertTitle" runat="server" ResourceConstant="ECARD_ALERT_TITLE" />
                    </h4>
                    <span id="ecardalerts_background" class="options-text">
                        <asp:CheckBox runat="server" ID="chkECardAlert"></asp:CheckBox>
                    </span>
                </div>
            </asp:PlaceHolder>
            <%--Profile Viewed Alert--%>
            <asp:PlaceHolder runat="server" ID="phAlertProfileViewed">
                <div class="message-options-sub-container clear-both">
                    <h4 class="options-title">
                        <mn:Txt ID="txtProfileViewedAlertTitle" runat="server" ResourceConstant="PROFILE_VIEWED_ALERT_TITLE" />
                    </h4>
                    <span id="profileviewedalerts_background" class="options-text">
                        <asp:CheckBox runat="server" ID="chkProfileViewedAlert"></asp:CheckBox>
                    </span>
                </div>
            </asp:PlaceHolder>
             <%--Essay Request Alert--%>
            <asp:PlaceHolder runat="server" ID="phAlertEssayRequest">
                <div class="message-options-sub-container clear-both">
                    <h4 class="options-title">
                        <mn:Txt ID="txtEssayRequestAlertTitle" runat="server" ResourceConstant="ESSAY_REQUEST_ALERT_TITLE" />
                    </h4>
                    <span id="essayrequestalerts_backgroud" class="options-text">
                        <asp:CheckBox runat="server" ID="chkEssayRequestAlert"></asp:CheckBox>
                    </span>
                </div>
            </asp:PlaceHolder>
            <h3 class="settings-header">
                <mn:Txt ID="txtNewsAlertTitle" runat="server" ResourceConstant="NEWS_EVENT_TITLE" />
            </h3>            
            <%--Invitations Events--%>
            <div class="message-options-sub-container clear-both">
                <h4 class="options-title">
                    <mn:Txt ID="txtInvitationTitle" runat="server" ResourceConstant="INVITATION_TITLE" />
                </h4>
                <span id="invitations_background" class="options-text">
                    <asp:CheckBox runat="server" ID="chkInvitation"></asp:CheckBox>
                </span>
            </div>
            <%--Newsletters--%>
            <div class="message-options-sub-container clear-both">
                <h4 class="options-title">
                    <mn:Txt ID="txtNewsletters" runat="server" ResourceConstant="NEWSLETTERS_TITLE" />
                </h4>
                <span id="newsletters_background" class="options-text">
                    <asp:CheckBox runat="server" ID="chkNewsletters"></asp:CheckBox>
                </span>
            </div>
            <%--Offers--%>
            <div class="message-options-sub-container clear-both">
                <h4 class="options-title">
                    <mn:Txt ID="txtOffers" runat="server" ResourceConstant="OFFERS_TITLE" />
                </h4>
                <span id="Offers_background" class="options-text">
                    <asp:CheckBox runat="server" ID="chkOffers"></asp:CheckBox>
                </span>
            </div>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlDNE" runat="server" Visible="false">
        <h2 class="prefHeader">
            <a name="OffSite"></a>
            <mn:Txt ID="txtMesageFromSiteTitleDNE" runat="server" ResourceConstant="MESSAGE_FROM_SITE_TITLE" />
            <mn:Txt ID="txtMessageFromSiteTitle2DNE" runat="server" ResourceConstant="MESSAGE_FROM_SITE_TITLE2" />
        </h2>
        <div class="prefContainer">
            <mn:Txt ID="txtMemberOnDNE" runat="server" ResourceConstant="MEMBER_ON_DNE_MESSAGE" />
            <br />
        </div>
    </asp:Panel>
    <!-- End PrefBorder External Email Alerts/Settings area -->
    <p class="text-center">
        <cc1:FrameworkButton ID="btnSave" runat="server" ResourceConstant="TXT_SAVE_OFFSITE_MESSAGE_SETTINGS"
            class="activityButton btn primary"></cc1:FrameworkButton></p>
    <!--//-->
</div>