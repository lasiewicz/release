﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ViewMessage20.ascx.cs"
    Inherits="Matchnet.Web.Applications.Email.ViewMessage20" %>
<%@ Register TagPrefix="mn" TagName="microProfile" Src="../../Framework/Ui/BasicElements/MicroProfile20.ascx" %>
<%@ Register TagPrefix="mn" TagName="microMicroProfile" Src="../../Framework/Ui/BasicElements/MicroMicroProfile20.ascx" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mnl" Namespace="Matchnet.Web.Framework.Ui.BasicElements.Links"
    Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="cc1" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn" TagName="TabBar" Src="../../Framework/Ui/BasicElements/TabBar.ascx" %>
<%@ Register TagPrefix="mn" TagName="InboxShutdownOverlay" Src="InboxShutdownOverlay.ascx" %>
<asp:PlaceHolder ID="plcInboxShutdownOverlay" runat="server" Visible="false">
    <mn:inboxshutdownoverlay id="InboxShutdownOverlay1" runat="server" />
</asp:PlaceHolder>
<div id="page-container">
    <script type="text/javascript">
        var MemberPhotoWindow;

        function launchPhotoWindow(strURL, Caption) {
            if (typeof (MemberPhotoWindow) == 'object') {
                MemberPhotoWindow.close();
            }
            MemberPhotoWindow = window.open(strURL, 'MemberPhotoWindow', 'status=yes,scrollbars=yes,location=no,resizable=no,menubar=no,width=400,height=400,title=Caption'); MemberPhotoWindow.opener = self;
            MemberPhotoWindow.name = 'MemberPhotoWindow';
            return 0;
        }
    </script>
    <div class="header-options clearfix">
        <div class="float-inside pagination-buttons image-text-pair">
            <asp:HyperLink runat="server" CssClass="btn link-secondary small" ID="viewMessageLink">
                <span class="direction">l<mn:txt id="Txt1" runat="server" resourceconstant="NAV_BACK_DIRECTION_ARROW" />
                </span><span class="prev">
                    <mn:txt id="Txt4" runat="server" resourceconstant="TXT_VIEW_MESSAGE" expandimagetokens="true" />
                </span>
            </asp:HyperLink>
        </div>
        <div class="float-outside pagination-buttons image-text-pair">
            <asp:HyperLink runat="server" CssClass="btn link-secondary small" ID="previousMessageLink">
                <span class="direction">
                    <mn:txt id="Txt11" runat="server" resourceconstant="NAV_BACK_DIRECTION_ARROW" />
                </span><span class="prev">
                    <mn:txt id="Txt3" runat="server" resourceconstant="TXT_PREVIOUS" />
                </span>
            </asp:HyperLink>
            <asp:HyperLink runat="server" CssClass="btn link-secondary small" ID="nextMessageLink">
                <span class="next">
                    <mn:txt id="Txt5" runat="server" resourceconstant="TXT_NEXT" />
                </span><span class="direction">
                    <mn:txt id="Txt9" runat="server" resourceconstant="DIRECTION_ARROW" />
                </span>
            </asp:HyperLink>
        </div>
    </div>
    <h1>
        <mn:txt runat="server" id="lblViewMessage" expandimagetokens="true" />
    </h1>
    <asp:Literal ID="litCSSMessage" runat="server" />
    <div id="message-view-container" class="<%=MyClass%>">
        <div id="message-control-header">
            <div class="message-header clearfix">
                <mn:micromicroprofile id="microMicroProfile" runat="server" visible="false" />
                <div class="message-header-head">
                    <p>
                        <b>
                            <mn:txt id="Txt7" runat="server" resourceconstant="TXT_SUBJECT" />
                            : </b>
                        <asp:Literal runat="server" ID="messageSubjectLiteral"></asp:Literal>
                    </p>
                    <p>
                        <b>
                            <mn:txt id="Txt6" runat="server" resourceconstant="TXT_DATE" />
                            : </b>
                        <asp:Literal runat="server" ID="messageDateLiteral"></asp:Literal>
                    </p>
                </div>
            </div>
        </div>
        <!--end carrot-profile-->
        <div id="message-view" class="clear-both clearfix">
            <span id="messageContentContainer">
                <asp:Literal runat="server" ID="messageContentLiteral"></asp:Literal>
            </span>
            <script type="text/javascript">
                //converts messages with custom markup (i.e. photos, emoticons, etc)
                //var vmMessageContent = 'check out my photo &lt;sparktag type="photo" alt="david1" src="&amp;#x2F;attachments&amp;#x2F;2a947e7bbd095248a819b612a52d916e&amp;#x2F;preview&amp;#x2F;120132038.jpg" id="1197" data-originalExt="jpg"/&gt; some emoticons too&lt;sparktag type="emoticon" class="spr icons-smiley-a-0002"/&gt; &lt;sparktag type="emoticon" class="spr icons-smiley-a-0000"/&gt;';
                var vmMessageContent = '<%=JSMessage%>';
                var doMessageConversion = <%=JSDoMessageConversion.ToString().ToLower()%>;
                if (doMessageConversion) {
                    var rebuiltHtml = spark.messageconversion.convertCustomTagToHtml(spark.messageconversion.htmlDecode(vmMessageContent));
                    $j('#messageContentContainer').html(rebuiltHtml);
                }
            </script>
            <asp:HyperLink runat="server" ID="lnkAppLink"></asp:HyperLink>
            <asp:PlaceHolder ID="phMessageAppend" runat="server" Visible="false" />
        </div>
        <asp:PlaceHolder runat="server" ID="plcPrivatePhoto" Visible="false">
            <mn:txt id="Txt13" runat="server" resourceconstant="PHOTO" />
            :
            <asp:PlaceHolder ID="imagePlaceHolder" runat="server" />
        </asp:PlaceHolder>
        <asp:PlaceHolder ID="phComposeEmail" runat="server" Visible="false">
            <!-- reply start -->
            <div class="reply-box compose-email<%=_AllAccessMsg %> clearfix">
                <asp:PlaceHolder ID="phVIPFreeReply" runat="server" Visible="false">
                    <mn:txt id="Txt2" runat="server" resourceconstant="TXT_FREE_REPLY_VIP" expandimagetokens="true" />
                </asp:PlaceHolder>
                <div class="compose-email-content">
                    <mn:txt runat="server" id="lblAllAccessReplyIcon" expandimagetokens="true" cssclass="all-access-reply-icon" />
                    <h3>
                        <mn:txt id="Txt8" runat="server" resourceconstant="TXT_REPLY_TO" />
                        &nbsp;<mn:txt runat="server" id="toUserName" />
                        &nbsp;<mn:txt id="Txt10" runat="server" resourceconstant="TXT_REPLY_TO_NOW" />
                    </h3>
                    <asp:PlaceHolder ID="plcMailToList" runat="server" Visible="false">
                        <asp:TextBox ID="toTextField" runat="server"></asp:TextBox>&nbsp;&nbsp;<a id="hrefMailToList"
                            runat="server" href="javascript:launchWindow('/Applications/Email/MailToList.aspx', 'View Profile',650,650,'')"><mn:txt
                                id="Txt12" runat="server" resourceconstant="TXT_CLICK_HERE_TO_CHOOSE_FROM_YOUR_HOTLIST" />
                        </a></asp:PlaceHolder>
                    <asp:TextBox runat="server" ID="subjectTextField" MaxLength="50" Style="display: none;"></asp:TextBox>
                    <label>
                        <mn:txt id="Txt15" runat="server" resourceconstant="TXT_MESSAGE" />
                        :</label>
                    <asp:TextBox runat="server" ID="messageBodyTextField" Rows="7" Columns="50" TextMode="MultiLine"
                        Width="95%"></asp:TextBox>
                        <div id="emailEmpty" style="display: none" class="comment-sent comment-error">
                            <mn:Image ID="Image2" runat="server" FileName="icon-page-message.gif" />
                            <%=g.GetResource("MESSAGE_EMPTY", this) %>
                        </div>
                    <div class="options">
                        <b>
                            <mn:txt id="Txt16" runat="server" resourceconstant="TXT_OPTIONS" />
                            :</b> <span class="item">
                                <asp:CheckBox runat="server" ID="saveCopyCheckBox"></asp:CheckBox></span>
                        <asp:PlaceHolder ID="plcSendPrivatePhotos" runat="server" Visible="false">
                            <div id="privatePhotoDiv">
                                <asp:CheckBox runat="server" ID="cbSendPrivatePhotos"></asp:CheckBox>
                            </div>
                        </asp:PlaceHolder>
                    </div>
                    <div class="text-outside buttons" style="padding-bottom: 1em;">
                        <asp:PlaceHolder ID="plcSendAsVIP" runat="server">
                            <div id="mail-send-as-vip">
                                <asp:CheckBox runat="server" ID="cbxSendAsVIP" Checked="false" />
                                <asp:Label runat="server" ID="lblVIPRemaining" CssClass="allaccess-remaining" />
                                <div id="divSendVIPOverlay">
                                    <asp:PlaceHolder ID="phSendVIPOverlay" runat="server" Visible="true">
                                        <mn:txt id="txtOverlay" runat="server" />
                                        <div class="cta">
                                            <cc1:frameworkbutton cssclass="btn primary" runat="server" id="btnYes" resourceconstant="BTN_YES"
                                                onclick="ProcessVIPMail" />
                                            <cc1:frameworkbutton cssclass="textlink" runat="server" id="btnNo" resourceconstant="BTN_NO"
                                                onclick="sendMessageClick" />
                                        </div>
                                    </asp:PlaceHolder>
                                </div>
                            </div>
                        </asp:PlaceHolder>
                        <cc1:frameworkbutton class="btn secondary" id="saveDraftButton" runat="server" resourceconstant="BTN_TXT_SAVE_AS_A_DRAFT"
                            onclick="saveDraftClick" />
                        &#160;
                        <cc1:frameworkbutton class="btn primary" runat="server" resourceconstant="BTN_TXT_SEND_MESSAGE"
                            id="sendMessageButton" onclick="sendMessageClick" />
                        <cc1:frameworkbutton class="btn primary vipbutton" runat="server" resourceconstant="BTN_TXT_SEND_MESSAGE"
                            id="sendVIPMessageButton" aonclientclick="javascript:showVIPOverlay();return !overlay;"
                            onclick="sendMessageClick" />
                    </div>
                </div>
            </div>
            <script type="text/javascript">
                var n = $j('#cc-compose-tips').length;
                if (n <= 0) {
                    $j('.compose-email-content', '#content-main').addClass('no-cc');
                } else { };

                var $vipoverlay = $j('#divSendVIPOverlay');
                var overlay = false;
                function showVIPOverlay() {

                    if (!$j('input:checkbox', '#mail-send-as-vip').is(":checked")) {
                        $vipoverlay.detach().appendTo('#aspnetForm').after('<div class="ui-widget-overlay" style="position:fixed;" />').show();

                        overlay = true;

                        $j.ajax({
                            type: "POST",
                            url: "/Applications/API/SessionUpdate.asmx/UpdateSessionIntWithIncrement",
                            data: "{key:'VIP_COMPOSE_OVERLAY_PER_SESSION',value:1,lifetime:0}",
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (msg) { },
                            error: function (msg) { }
                        })

                        return false;
                    }
                    else
                        return true;
                }
                $j(document).ready(function () {
                    var $allaccessTooltip = $j('#allaccess-tooltip');
                    $j('.allaccess-help', '#mail-send-as-vip').hover(function (e) {
                        var x = e.pageX;
                        var y = e.pageY - $j(window).scrollTop();
                        var tooltipWidth = $allaccessTooltip.outerWidth();
                        if ($j('html').attr('dir') == 'rtl') {
                            $allaccessTooltip.css({ 'position': 'fixed', 'left': x - tooltipWidth, 'top': y + 10 }).show();
                        } else {
                            $allaccessTooltip.css({ 'position': 'fixed', 'left': x + 10, 'top': y + 10 }).show();
                        };
                    },
                        function () {
                            setTimeout(function(){$allaccessTooltip.hide();}, 1000);
                        }
                    );
                });

                //autogrow for compose message boxes
                $j('textarea[id$="messageBodyTextField"]').TextAreaExpander(105, 600);
            </script>
            <!-- reply end -->
        </asp:PlaceHolder>
        <div class="message-options border-top-dotted clearfix">
            <cc1:frameworkbutton id="btnReply" class="btn primary wide" runat="server" resourceconstant="TXT_REPLY"
                onclick="replyClick" />
            <cc1:frameworkbutton id="FrameworkButton7" class="textlink" runat="server" resourceconstant="BTN_DELETE"
                onclick="deleteClick" />
            <cc1:frameworkbutton class="textlink" runat="server" resourceconstant="BTN_REPORT_MEMBER"
                onclick="RedirectToReportMember" id="btnReportMember2" />
            <cc1:frameworkbutton id="FrameworkButton8" class="textlink" runat="server" resourceconstant="BTN_IGNORE"
                onclick="ignoreClick" />
            <cc1:frameworkbutton id="FrameworkButton6" class="textlink last" runat="server" resourceconstant="BTN_MARK_AS_UNREAD"
                onclick="markUnreadClick" />
        </div>
    </div>
    <div class="header-options clearfix">
    </div>
    <!--tabbed profile-->
    <asp:PlaceHolder ID="phViewTabbedProfile" runat="server" Visible="False"></asp:PlaceHolder>
    
    <script type="text/javascript">
        //converts messages with custom markup (i.e. photos, emoticons, etc)
        var vmMessageContent = '<%=JSMessage%>';
        var doMessageConversion = <%=JSDoMessageConversion.ToString().ToLower()%>;
        $j(function () {
            if (doMessageConversion) {
                var rebuiltHtml = spark.messageconversion.convertCustomTagToHtml(spark.messageconversion.htmlDecode(vmMessageContent));
                $j('#messageContentContainer').html(rebuiltHtml);
            }
        });
    </script>
</div>
