﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Ui;

using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.Region;
using Matchnet.Email.ValueObjects;
using Matchnet.Email.ServiceAdapters;
using Matchnet.List.ServiceAdapters;
using Matchnet.List.ValueObjects;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Web.Framework.Globalization;

namespace Matchnet.Web.Applications.Email
{
    
    public partial class VIPMailOverlay : FrameworkControl
    {
        FrameworkControl _resourceControl = null;
        public EmailMessage VIPEmail { get; set; }
        public int CurrentVIPMessageNumber { get; set; }
        public int VIPMessageCount { get; set; }
        public FrameworkControl ResourceControl
        {
            get{
                if (_resourceControl == null)
                    return this;
                else
                { return _resourceControl; }
            }

            set{_resourceControl=value;}


        }
        protected void Page_Load(object sender, EventArgs e)
        {

        }


        public bool LoadOverlay()
        {
            try{
                
                string vipoverlayMode = "default";

                if (Request["overlay"] != null)
                    vipoverlayMode = Request["overlay"];
                
                if (VIPEmail == null && vipoverlayMode=="default") return false;
                

                if(vipoverlayMode=="default")
                {

                    Matchnet.Member.ServiceAdapters.Member member = Matchnet.Member.ServiceAdapters.MemberSA.Instance.GetMember(VIPEmail.FromMemberID, MemberLoadFlags.None);
                    if (member == null) return false;
                    int gendermask = member.GetAttributeInt(g.Brand, "gendermask");

                    string username=member.GetUserName(g.Brand);
                  

                    lnkReadMessage.NavigateUrl = "/Applications/Email/ViewMessage.aspx?MemberMailID=" + VIPEmail.MemberMailID + "&MemberFolderID=" + VIPEmail.MemberFolderID + "&OrderBy=insertDate desc"
                                                + "&CurrentMessageNumber=" + CurrentVIPMessageNumber + "&MessageCount=" + VIPMessageCount  + "&VIPOverlay=1";
                    txtLinkText.Text = g.GetResource("TXT_VIP_EMAIL_READ", ResourceControl);
                  
                    if (((int)VIPEmail.StatusMask & (int)MessageStatus.OneFreeReply) != (int)MessageStatus.OneFreeReply)
                    {
                        if (((int)VIPEmail.StatusMask & (int)MessageStatus.Replied) == (int)MessageStatus.Replied)
                        {
                            // if the message has been replied to already, hide the overlay otherwise we have no way of determining which overlay to show
                            txtMessage.Text = string.Empty;
                            txtTitle.Text = string.Empty;
                        }
                        else
                        {
                            txtMessage.Text = Personalize(g.GetResource("TXT_VIP_FREE_REPLY", ResourceControl, new string[] { username }), gendermask);
                            txtTitle.Text = Personalize(String.Format(g.GetResource("TXT_VIP_EMAIL_TITLE_FREE", ResourceControl), username), gendermask);
                        }
                    }
                    else
                    {
                        txtTitle.Text = Personalize(String.Format(g.GetResource("TXT_VIP_EMAIL_TITLE", ResourceControl), username), gendermask);
                        if (g.Member != null && g.Member.IsPayingMember(g.Brand.Site.SiteID))
                        {
                            txtMessage.Text = Personalize(String.Format(g.GetResource("TXT_READ_VIP_MESSAGE_SUB", ResourceControl), username), gendermask);
                        }
                        else
                        {
                            txtMessage.Text = Personalize(String.Format(g.GetResource("TXT_READ_VIP_MESSAGE", ResourceControl), username), gendermask);
                        }
                    }
                    photoProfile.ProfileMember = member;
                    photoProfile.ResourceControl = ResourceControl;
                }else if (vipoverlayMode=="nothanks")
                {

                    photoProfile.Visible=false;
                     txtLinkText.Text = g.GetResource("TXT_VIP_NOTHANKS", ResourceControl);
                     txtTitle.Text = g.GetResource("TXT_VIP_TITLE_NOTHANKS", ResourceControl);
                     txtMessage.Text = g.GetResource("TXT_VIP_TEXT_NOTHANKS", ResourceControl);
                    lnkReadMessage.Text=g.GetResource("TXT_VIP_CLOSE", ResourceControl);
                }

                return !(txtMessage.Text == string.Empty && txtTitle.Text == string.Empty);
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }

            return false;
        }
        public string Personalize(string text, int genderMask)
        { string output=text;
            try
            {
                string he_she="";
                string his_her = string.Empty;
               
                if(GenderUtils.GetGenderMaskSelf(genderMask)== (int)GenderMask.Male)
                {
                    he_she=g.GetResource("TXT_HE",ResourceControl);
                    his_her = g.GetResource("TXT_HIS", ResourceControl);
                }
                else
                {
                     he_she=g.GetResource("TXT_SHE",ResourceControl);
                     his_her = g.GetResource("TXT_HER", ResourceControl);
                }
                output = text.Replace("[he_she]", he_she).Replace("[his_her]", his_her);

                return Localizer.ExpandImageTokens(output, g.Brand);
            }
            catch (Exception ex)
            { return output; }


        }
    }
}