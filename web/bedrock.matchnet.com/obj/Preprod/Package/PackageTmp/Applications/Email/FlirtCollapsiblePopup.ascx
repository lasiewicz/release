﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="FlirtCollapsiblePopup.ascx.cs" Inherits="Matchnet.Web.Applications.Email.FlirtCollapsiblePopup" %>
<%@ Register TagPrefix="cc1" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="result" TagName="ResultList" Src="../../Framework/Ui/SearchElements/ResultList.ascx" %>

<form id="flirtForm" runat="server">
<asp:Panel ID="pnlflirtForm" runat="server">
    <input type="hidden" name="MemberID" value="<%=Member.MemberID %>" />
    <%--<mn:Title runat="server" id="ttlMatches" ResourceConstant="TXT_SEND_A_TEASE" ImageName="hdr_message_sent.gif" CommunitiesForImage="CI;" />--%>

    <div class="accordion" id="smiles">
        <%--<h2><mn2:FrameworkLiteral ID="literalSelectTease" runat="server" ResourceConstant="TXT_SELECT_TEASE"></mn2:FrameworkLiteral></h2>--%>
        <p class="editorial"><mn:txt runat="server" id="idHere" resourceconstant="TXT_FLIRT_MODAL_TAGLINE" expandimagetokens="true" /></p>
        <ul class="form-set">
            <asp:Repeater ID="repeaterCategories" runat="server" OnItemDataBound="repeaterCategories_ItemDataBound">
                <ItemTemplate>
                    <%--Categories--%>
                    <li class="category-wrapper">
                        <a id="lnkCategoryName" href="#" runat="server" class="category"><asp:Literal ID="literalCategoryName" runat="server"></asp:Literal></a>
                        <%--<ul>--%>
                        <asp:Literal ID="literalULStart" runat="server"></asp:Literal>
                            <asp:Repeater ID="repeaterItems" runat="server" OnItemDataBound="repeaterItems_ItemDataBound">
                                <ItemTemplate>
                                    <%--category items--%>
                                    <li><asp:Literal ID="literalItemRadioButton" runat="server"></asp:Literal></li>
                                </ItemTemplate>
                            </asp:Repeater>
                        <asp:Literal ID="literalULEnd" runat="server" Text="</ul>"></asp:Literal>
                        <%--</ul>--%>
                    </li>
                
                    <%--this promo for category, not sure if we even use this, most likely will never render but we'll keep it here just in case as it currently exists in flirts20.ascx--%>
                    <asp:PlaceHolder ID="phCategoryPromo" runat="server" Visible="false">
                        <div class="promo-insert">
			                <mn:Txt ID="txtPromo" runat="server"/>
		                </div>
		            </asp:PlaceHolder>
                </ItemTemplate>
            </asp:Repeater>
        </ul>
    </div>

    <div class="text-center margin-medium">
        <mn2:FrameworkButton runat="server" id="btnSubmit" CssClass="btn primary" ResourceConstant="BTN_TEASE_AWAY" />
    </div>

    <%--<div>
        <button id="sendFlirtButton" class="btn primary" type="button">Send Flirt</button>
    </div>--%>


    <mn:txt id="txtCallToAction" runat="server" ExpandImageTokens="true" ResourceConstant="FLIRT_CALL_TO_ACTION" Visible="false" />


    <script type="text/javascript">
        var ajax_load = '<div class="loading spinner-only" />';
        var targetMemberID = "<%=Member.MemberID %>";       

        $j('[id*=btnSubmit]').click(function (evt) {
            evt.preventDefault();
            
            var Radio_TeaseItem = $j("input[name='Radio_TeaseItem']:checked").val();
            $j.ajax({
                type: "POST",
                url: "/Applications/API/Flirts.asmx/ProcessFlirtOperation",
                data: "{ 'memberID': '" + targetMemberID + "', 'Radio_TeaseItem': '" + Radio_TeaseItem + "'}",
                contentType: "application/json; charset=ISO-8859-1",
                dataType: "json",
                beforeSend: function () {
                    $j("#flirtContent").html(ajax_load);
                },
                success: function (response) {
                    $j("#flirtContent").html(response.d);
                    //alert(response);
                    //$j(element).removeAttr("disabled");
                },
                error: function () {
                    //alert('error');
                    //$j(element).removeAttr("disabled");
                }
            });
        });

        var s = s_gi(s_account);
        s.linkTrackVars = 'events';
        s.linkTrackEvents = 'event2';
        s.events = 'event2';
        s.prop35 = targetMemberID;
        <%=GetAdditionalOmnitureData() %>
        s.t(true);
    </script>

</asp:Panel>

<asp:PlaceHolder ID="phFlirtResult" runat="server" Visible="false">
<%-- 
                    resource = "FLIRT_OPERATION_FAILED";
                    resource = "FLIRT_OPERATION_SUCCESSFUL";
                    resource = "FLIRT_OPERATION_TOO_MANY_FLIRTS";
                    resource = "FLIRT_OPERATION_SENT_ALREADY";
                    resource = "FLIRT_OPERATION_MEMBER_NOT_FOUND";
                    resource = "FLIRT_OPERATION_YOU_ARE_BLOCKED";
                    resource = "FLIRT_OPERATION_ALREADY_USED_UP_FREE_TEASES";
           --%>



    <mn:txt id="flirtHeader" runat="server" ExpandImageTokens="true" ResourceConstant="FLIRT_OPERATION_SUCCESSFUL" />

    <asp:PlaceHolder ID="phProfileSuggestion" runat="server" Visible="true">
        <result:ResultList ID="SearchResultList" runat="server" ResultListContextType="SearchResult" EnableMultipleSearchViews="false" PageSize="3" LoadControlAutomatically="false"></result:ResultList>

        <%-- closing divs for flirt messaging: opening found in FLIRT_OPERATION_XXXXX resources --%>
        <p class="text-center clear-both"><a href="/Applications/Search/SearchResults.aspx" class="btn link-primary"><mn:txt runat="server" id="txtBtnYourMatches" resourceconstant="BTN_YOUR_MATCHES" expandimagetokens="true" /></a></p>
        </div></div>
    </asp:PlaceHolder>

    <asp:PlaceHolder ID="phFlirtSuccessfulOmniture" runat="server" Visible="false">
        <script type="text/javascript">
            var s = s_gi(s_account);
            s.linkTrackVars = 'events';
            s.linkTrackEvents = 'event11,event2';
            s.events = 'event11,event2';
            s.eVar26 = "Tease";
            <%=GetAdditionalOmnitureDataSuccess() %>
            s.t(true);
        
        </script>
    </asp:PlaceHolder>

    <asp:PlaceHolder ID="phFlirtFailedOmniture" runat="server" Visible="false">
        <script type="text/javascript">
            var s = s_gi(s_account);
            s.linkTrackVars = 'events';
            s.linkTrackEvents = 'event2';
            s.events = 'event2';
            s.t(true);
        
        </script>
    </asp:PlaceHolder>
</asp:PlaceHolder>
</form>