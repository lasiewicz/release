using System;
using System.Web;
using System.Web.UI.WebControls;
using System.Text;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Ui.BasicElements;

using Matchnet.Email.ServiceAdapters;
using Matchnet.Email.ValueObjects;
using Matchnet.ExternalMail.ServiceAdapters;
using Matchnet.List.ServiceAdapters;
using Matchnet.List.ValueObjects;
using Matchnet.Session.ValueObjects;
using Matchnet.UserNotifications.ServiceAdapters;
using Matchnet.UserNotifications.ValueObjects;
using Matchnet.Web.Applications.UserNotifications;
using Matchnet.Content.ValueObjects.Quotas;
using Matchnet.Web.Framework.Managers;

namespace Matchnet.Web.Applications.Email
{
    public class FlirtHandler
    {
        public static string TEASE_ORIGIN_URL_SESSION_KEY = "TeaseOriginUrl";
        IFlirt _control = null;
        ContextGlobal g;
        private Matchnet.Member.ServiceAdapters.Member _member;

        public FlirtHandler(IFlirt control,ContextGlobal context)
        {
            g = context;
            _control = control;
        }

        public Matchnet.Member.ServiceAdapters.Member Member
        {
            get
            {
                return this._member;
            }
        }

        
        public void Page_Initialize(bool IsPostBack, bool isOverlay)
        {
            // TT #14412
            // this if statment was moved here from page_load because TEASE_ORIGIN_URL_SESSION_KEY wasn't getting
            // added if someone teases a member they've already teased.  That caused TeaseSentAlready to use the previous
            // value for TEASE_ORIGIN_URL_SESSION_KEY in the session if someone tries to tease the same user twice in a row.
            
            if (!IsPostBack)
            {
                // Tease origination determination, bypass if it's a flirt category switch
                if (HttpContext.Current.Request["teaseCategoryID"] == null)
                {
                    // Get referring url and store in session, but only do this upon loading tease.ascx the first time,
                    // you don't want this to happen if/when someone tabs thru the flirt messages, which postbacks.
                    GetRefURL();
                }
            }

            try
            {
                bool isHotListed = false;
                int memberID = 0;
                string startRow = string.Empty;

                if (HttpContext.Current.Request["MemberID"] != null
                    && HttpContext.Current.Request["MemberID"] != "")
                {
                    try
                    {
                        memberID = Convert.ToInt32(HttpContext.Current.Request.Params.Get("MemberID"));
                        startRow = HttpContext.Current.Request.Params.Get("StartRow");

                        if (memberID != 0 && memberID != int.MinValue)
                        {
                            this._member = MemberSA.Instance.GetMember(memberID, MemberLoadFlags.None);

                            if (this._member == null)
                            {
                                g.Notification.AddErrorString("[Member not found.]");
                                return;
                            }

                            if (!isOverlay)
                            {
                                //TT# 16688 check if sender member is blocked
                                isHotListed = CheckIgnoreList(this._member.MemberID);
                                if (isHotListed)
                                {
                                    g.Transfer("/Applications/Email/TeaseTooMany.aspx?erroroccurred=true&errortype=ignore&MemberID=" + this._member.MemberID);
                                }

                                //TT#16754 check if member already flirted with this member
                                isHotListed = CheckIfAlreadyTeased(g.Member.MemberID, this._member.MemberID);

                                if (isHotListed)
                                {
                                    if (startRow != null && startRow.Trim().Length > 0)
                                    {
                                        g.Transfer("/Applications/Email/TeaseSentAlready.aspx?MemberID=" + memberID + "&StartRow=" + startRow);
                                    }
                                    else
                                    {
                                        g.Transfer("/Applications/Email/TeaseSentAlready.aspx?MemberID=" + memberID);
                                    }
                                }

                                BindTeaseCategoryItems();
                            }
                        }
                        else
                        {
                            Exception ex = new Exception("MemberID was not found in the request.");
                            g.ProcessException(ex);
                            g.Transfer("/Default.aspx?rc=WE_WERE_UNABLE_TO_FIND_THE_PAGE_YOU_ARE_LOOKING_FOR_X");
                        }
                    }
                    catch (FormatException fe)
                    {
                        g.ProcessException(fe);
                        g.Transfer("/Default.aspx?rc=WE_WERE_UNABLE_TO_FIND_THE_PAGE_YOU_ARE_LOOKING_FOR_X");
                    }
                }
                else
                {
                    Exception ex = new Exception("MemberID was not found in the request.");
                    g.ProcessException(ex);
                    //g.Transfer("/Default.aspx?rc=WE_WERE_UNABLE_TO_FIND_THE_PAGE_YOU_ARE_LOOKING_FOR_X");
                }
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        public void SendTease(int CategoryID, int TeaseID)
        {
            int teasesLeft;
            TeaseCollection teases = TeaseSA.Instance.GetTeaseCollection(CategoryID, g.Brand.Site.SiteID);
            Matchnet.Email.ValueObjects.Tease selectedTease = null;
            foreach (Matchnet.Email.ValueObjects.Tease tease in teases)
            {
                if (tease.TeaseID == TeaseID)
                {
                    selectedTease = tease;
                    break;
                }
            }

            FlirtManager flirtManager = new FlirtManager(g);
            FlirtOperationResult fopResult = flirtManager.SendFlirt(CategoryID, TeaseID, g.GetResource("TXT_TEASE", _control.ResourceControl), g.GetResource(selectedTease.ResourceKey, _control.ResourceControl), _control.Member.MemberID, out teasesLeft);

            if (fopResult == FlirtOperationResult.AlreadyUsedUpAllFreeTeases)
            {
                g.Notification.AddError("YOU_VE_USED_UP_ALL_YOUR_FREE_TEASES__519718", null);
                g.Transfer("/Applications/Email/TeaseTooMany.aspx?MemberId=" + _control.Member.MemberID);
            }
            else if (fopResult == FlirtOperationResult.FlirtSent)
            {
                g.Transfer("/Applications/Email/TeaseSent.aspx?teasesLeft="
                    + teasesLeft.ToString() + "&MemberId=" + _control.Member.MemberID);
            }
            else
            {
                // sending of the message failed for some reason
                g.Transfer("/Applications/Email/TeaseTooMany.aspx?erroroccurred=true");
            }
        }
            

        

        public string BuildUrl(int teaseCategoryID)
        {
            StringBuilder sb = null;
            string url = null;

            url = HttpContext.Current.Request.RawUrl;

            if (HttpContext.Current.Request.Params.Get("teaseCategoryID") != null)
            {
                url = url.Replace("&teaseCategoryID="
                    + HttpContext.Current.Request.Params.Get("teaseCategoryID"), "");
            }

            sb = new StringBuilder();

            sb.Append(url);
            sb.Append("&");
            sb.Append("teaseCategoryID=");
            sb.Append(teaseCategoryID);

            return sb.ToString();
        }

        public void BindTeaseCategoryItems()
        {
            TeaseCategoryCollection teaseCategoryCollection = null;

            teaseCategoryCollection = TeaseSA.Instance.GetTeaseCategoryCollection(g.Brand.Site.SiteID);

           _control.RepTeaseCategories.DataSource = teaseCategoryCollection;
           _control.RepTeaseCategories.DataBind();
        }

        public void BindTeaseContentItems(int teaseCategoryID)
        {
            TeaseCollection teaseCollection = null;

            teaseCollection = TeaseSA.Instance.GetTeaseCollection(teaseCategoryID, g.Brand.Site.SiteID);

            foreach (Matchnet.Email.ValueObjects.Tease tease in teaseCollection)
            {
                ListItem item =
                    new ListItem(g.GetResource(tease.ResourceKey, _control.ResourceControl), Convert.ToString(tease.TeaseID));

                _control.RblTeaseContent.Items.Add(item);
            }

            _control.RblTeaseContent.SelectedIndex = 0;
        }

        //Removed functionality for MPR-1987
        public void GetRefURL()
        {
            string absoluteUri = string.Empty;
            if (HttpContext.Current.Request.UrlReferrer != null)
            {
                absoluteUri = HttpContext.Current.Request.UrlReferrer.AbsoluteUri;
            }

            // Remove any existing StartRow URL parameter which is not needed.
            // It causes problems when being included in the "TEASE_ORIGIN_URL_SESSION_KEY" saved to the session (see below)
            if (absoluteUri.IndexOf("StartRow") != -1)
            {
                absoluteUri = g.RemoveParamFromURL(absoluteUri, "StartRow", false);
            }

            // See if the navigatUrl has params already
            string qm = "?";
            if ((absoluteUri.IndexOf("?") != -1))
            {
                qm = "&";
            }

            string startRow = HttpContext.Current.Request.Params.Get("StartRow");
            if (startRow != null && startRow.Trim().Length > 0)
            {
                absoluteUri = absoluteUri + qm + "StartRow=" + startRow;
            }

            g.Session.Add(TEASE_ORIGIN_URL_SESSION_KEY, absoluteUri, SessionPropertyLifetime.Temporary);            
        }

        //TT# 16688 check if sender member is blocked
        public bool CheckIgnoreList(int recpMemberId)
        {
            bool isHotListed = false;
            try
            {
                int memberId = g.Member.MemberID;

                List.ServiceAdapters.List list = ListSA.Instance.GetList(recpMemberId);
                isHotListed = list.IsHotListed(HotListCategory.IgnoreList, g.Brand.Site.Community.CommunityID, memberId);
                return isHotListed;
            }
            catch (Exception ex)
            { return isHotListed; }
        }

        //TT#16754 check if member already flirted with this member
        public bool CheckIfAlreadyTeased(int sendingMemberID, int targetMemberID)
        {
            int communityID;
            bool isHotListed = false;
            try
            {
                communityID = g.Brand.Site.Community.CommunityID;
                //check if targetmember on sendingmember hotlist tease category
                isHotListed = g.List.IsHotListed(List.ValueObjects.HotListType.Tease, List.ValueObjects.HotListDirection.OnYourList, communityID, targetMemberID);

                if (isHotListed)
                { return isHotListed; }

                /* fix to bug#16754 :Deleting a member from 'Flirted with' list, makes it possible to flirt that member over and over again.
                Check if targetmember list has hotlist tease for sendingmemebr with direction flag=0
             */
                Matchnet.List.ServiceAdapters.List targetList = ListSA.Instance.GetList(targetMemberID);

                isHotListed = targetList.IsHotListed(List.ValueObjects.HotListType.Tease, List.ValueObjects.HotListDirection.OnTheirList, communityID, sendingMemberID);

                return isHotListed;
            }
            catch (Exception ex)
            { return isHotListed; }

        }
    }
}
