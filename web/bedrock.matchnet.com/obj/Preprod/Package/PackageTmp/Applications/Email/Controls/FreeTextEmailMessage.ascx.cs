﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Util;
using System.Web.Script.Serialization;
using Matchnet.PremiumServiceSearch.ValueObjects.QuestionAnswer;

namespace Matchnet.Web.Applications.Email.Controls
{
    public partial class FreeTextEmailMessage : Matchnet.Web.Applications.MemberProfile.ProfileTabs30.BaseProfile
    {
        public FreeTextMessage FTM { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public void LoadFreeTextMessage(string jsonString, ContextGlobal _g)
        {
            FTM = new JavaScriptSerializer().Deserialize<Email.Controls.FreeTextMessage>(jsonString);
            
            base.MemberProfile = _g.Member;

            string fullDisplayValue = "";

            if (_g.Member.MemberID != FTM.FromMemberId)
            {
                fullDisplayValue = GetMemberAttributeValue(FTM.SectionAttribute, this, false);

                if (String.IsNullOrEmpty(fullDisplayValue))
                {
                    fullDisplayValue = GetNoAnswer(this);
                }

                this.txtOrigMessageLabel.Text = string.Format(g.GetResource("USERNAME_IS_RESPONDING_TO_YOUR_ESSAY_TITLE", this), Uri.UnescapeDataString(FTM.FromMemberName), g.GetResource(Uri.UnescapeDataString(FTM.SectionAttribute.ToUpper()) + "_LABEL", this));
                this.commentText.Text = fullDisplayValue;
            }
            else
            {
                freeTextReplyText.Visible = false;
            }

        }
    }

    public class FreeTextMessage
    {
        public string FromMemberName { get; set; }
        public string SectionAttribute { get; set; }
        public int FromMemberId { get; set; }

    }
}