﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.Email.ValueObjects;

namespace Matchnet.Web.Applications.Email
{
    public class ComposeResult
    {
        public bool ComposeSuccess = false;
        public MessageSendResult messageSendResult = null;
        public bool VIPFreeReply = false;
        public bool VIPMail = false;
        public int membermailid = 0;

        public ComposeResult()
        {
            messageSendResult = new MessageSendResult(MessageSendError.Unspecified);
        }
    }
}
