﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="TeaseTooMany20.ascx.cs" Inherits="Matchnet.Web.Applications.Email.TeaseTooMany20" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="uc1" TagName="HotlistProfileStrip" Src="/Applications/Hotlist/HotlistProfileStrip.ascx" %>

<div id="page-container">
    <div class="confirmation-message clearfix editorial">
        <mn:Image id="GrnTease" ResourceConstant="ALT_TEASE_SENT" titleResourceConstant="TTL_YOUR_TEASE_HAS_BEEN_SENT" FileName="icon-flirt-confirmation.gif" runat="server" />
        <h2><mn:txt id="txtMessage" runat="server" ResourceConstant="TXT_YOUVE_USED_UP_ALL_YOUR_FREE_FLIRTS" ExpandImageTokens="true" /></h2>
    </div>
    <p><asp:HyperLink Runat="server" ID="backToProfileLink"><mn:txt id="txtBackToProfile" runat="server" titleResourceConstant="TTL_BACK_TO_PROFILE" ResourceConstant="TXT_BACK_TO_PROFILE" /></asp:HyperLink></p>
</div>