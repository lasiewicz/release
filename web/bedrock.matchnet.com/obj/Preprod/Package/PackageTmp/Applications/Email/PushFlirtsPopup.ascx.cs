﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Web.Framework;

using Matchnet.Search.ServiceAdapters;
using Matchnet.Search.ValueObjects;
using Matchnet.List.ServiceAdapters;
using Matchnet.List.ValueObjects;
using Matchnet.Web.Framework.Ui.ProfileElements;
using Matchnet.Web.Framework.Managers;

namespace Matchnet.Web.Applications.Email
{
    public partial class PushFlirtsPopup : FrameworkControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            loadMembers();
            if (g != null && g.Member != null)
            {
                bool isMale = FrameworkGlobals.IsMaleGender(_g.Member, _g);
                txtPushFlirtsHeader.Text = (isMale) ? g.GetResource("TXT_PUSH_FLIRTS_HEADER_MALE", this) : g.GetResource("TXT_PUSH_FLIRTS_HEADER", this);
                txtPushFlirtsSingleFlirt.Text = (isMale) ? g.GetResource("TXT_PUSH_FLIRTS_SINGLE_FLIRT_MALE", this) : g.GetResource("TXT_PUSH_FLIRTS_SINGLE_FLIRT", this);
            }
        }

        public void flirtMemberRepeater_OnItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            MiniProfile miniProfile = (MiniProfile)e.Item.FindControl("MiniProfile");
            miniProfile.DisplayHoverMenu = false;
            miniProfile.DisplayColorCode = false;
            miniProfile.DisplayMorePhotosLink = false;
            miniProfile.DisplayNewMemberImage = false;
            miniProfile.DisplayUpdatedImage = false;
            miniProfile.EnableProfileLinks = true;
            miniProfile.LoadMiniProfile();

            System.Web.UI.HtmlControls.HtmlInputCheckBox chkSendPushFlirt = (System.Web.UI.HtmlControls.HtmlInputCheckBox)e.Item.FindControl("chkSendPushFlirt");
            chkSendPushFlirt.Attributes.Add("data-memberid", miniProfile.Member.MemberID.ToString());
        }

        private void loadMembers()
        {
            try
            {
                FlirtManager flirtManager = new FlirtManager(g);
                MatchnetQueryResults searchResults = flirtManager.GetPushFlirtMatches();

                if (searchResults != null && searchResults.Items.Count > 0)
                {
                    ArrayList members = MemberSA.Instance.GetMembers(searchResults.ToArrayList(), MemberLoadFlags.None);
                    flirtMemberRepeater.DataSource = members;
                    flirtMemberRepeater.DataBind();
                    flirtMemberRepeater.Visible = true;

                    flirtManager.AddViewedPushFlirtMatchesToHotList(members);
                }
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
            
        }
    }
}