﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="TeaseSentAlready20.ascx.cs" Inherits="Matchnet.Web.Applications.Email.TeaseSentAlready20" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="uc1" TagName="HotlistProfileStrip" Src="/Applications/Hotlist/HotlistProfileStrip.ascx" %>

<div id="page-container" class="hotlist-non-home">
	<mn:Title runat="server" id="ttlTeasedAlready" ResourceConstant="TXT_TEASE"/>
	
    <div class="conf-message30 clearfix editorial">
        <div class="conf-message30-title"><mn:txt id="YouHaveAlreadySentATeaseToThisMember" runat="server" /></div>
        
        <div class="conf-message30-cont">
            <h2><mn:txt id="Txt1" runat="server" ResourceConstant="TXT_WHAT_IS_NEXT" /></h2>
            <h2 class="bullet"><mn:txt id="Txt2" runat="server" ResourceConstant="TXT_ALREADY_A_SUBSCRIBER" /></h2>
            <p><mn:txt id="txtKeepConversationBeginning" runat="server" ResourceConstant="TXT_KEEP_CONVERSATION_BEGINNING" /> <asp:HyperLink Runat="server" ID="keepTheConversationGoingLink"><mn:txt id="txtKeepTheConvGoing" runat="server" /></asp:HyperLink></p>
            <h2 class="bullet"><mn:txt id="Txt3" runat="server" ResourceConstant="TXT_NOT_A_SUBSCRIBER" /></h2>
            <p>
            <mn:txt id="Txt4" runat="server" ResourceConstant="TXT_AS_A_SUBSCRIBER_YOU_CAN_DO" /><mn:txt id="txtSubscribeNowLC" runat="server" titleResourceConstant="TTL_SUBSCRIBE_NOW" ResourceConstant="TXT_SUBSCRIBE_NOW" /><%--<mn:txt id="txtExclamationMark" runat="server" ResourceConstant="TXT_EXCLAMATION_MARK" />--%></p>
            <div class="back-link"><asp:HyperLink Runat="server" ID="backToProfileLink">
                    <mn:txt id="txtBackToProfile" runat="server" titleResourceConstant="TTL_BACK_TO_PROFILE" />
                </asp:HyperLink></div>
        </div>
    </div>
        
	<uc1:HotlistProfileStrip id="profileStrip" runat="server" NoResultsResource="MEMBER_WHO_HAVE_MARKETING" HotlistDirection="1" />
</div>