﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="MailBox20.ascx.cs"
    Inherits="Matchnet.Web.Applications.Email.MailBox20" %>
<%@ Register TagPrefix="cc1" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn" TagName="TabBar" Src="../../Framework/Ui/BasicElements/TabBar.ascx" %>
<%@ Register TagPrefix="mn" TagName="VIPOverlay" Src="VIPMailOverlay.ascx" %>
<%@ Register TagPrefix="mn" TagName="InboxShutdownOverlay" Src="InboxShutdownOverlay.ascx" %>
<%@ Register TagPrefix="mn" TagName="Sprite" Src="/Framework/Ui/PageElements/Sprite.ascx" %>

<script type="text/javascript">

    function checkAllMessages(callingClassName) {
        // console.log("checkAllMessages called");

        var caller = "." + callingClassName + " > input:checkbox";
        var checked = jQuery(caller).attr("checked");
        var theOtherCheckall;

        if (callingClassName == "mail-header-check-reg") {
            theOtherCheckall = jQuery(".mail-footer-check-reg > input:checkbox");
        }
        else {
            theOtherCheckall = jQuery(".mail-header-check-reg > input:checkbox");
        }

        jQuery(".mail-item-check-reg > input:checkbox").attr("checked", checked);
        jQuery(theOtherCheckall).attr("checked", checked);
    }

    function checkAllVIPMessages(callingClassName) {
        //  console.log("checkAllVIPMessages called");
        //debugger;
        var caller = "." + callingClassName + " > input:checkbox";
        var checked = jQuery(caller).attr("checked");
        var theOtherCheckall;

        if (callingClassName == "mail-header-check-vip") {
            theOtherCheckall = jQuery(".mail-footer-check-vip > input:checkbox");
        }
        else {
            theOtherCheckall = jQuery(".mail-header-check-vip > input:checkbox");
        }

        jQuery(".mail-item-check-vip > input:checkbox").attr("checked", checked);
        jQuery(theOtherCheckall).attr("checked", checked);

    }

    function togglePhotos() {

    }

    $j(document).ready(function () {
        $j('#moveToFolder').detach().appendTo('.nav-rounded-tabs', '#content-main').wrap('<li class="float-inside" />').show().css('display', 'inline');

        var $allaccessTooltip = $j('#allaccess-tooltip'),
            $allaccessHelp = $j('.allaccess-help'),
            hoverTimeout;

        $allaccessHelp.hover(function (e) {
            clearTimeout(hoverTimeout);
            if (!$allaccessHelp.hasClass('hovered')) {
                var x = e.pageX,
                y = e.pageY - $j(window).scrollTop(),
                tooltipWidth = $allaccessTooltip.outerWidth(),
                leftSet;

                if ($j('html').attr('dir') == 'rtl') {
                    $allaccessTooltip.css({ 'position': 'fixed', 'left': x - tooltipWidth, 'top': y - 10 }).show();
                } else {
                    if (x < 540) {
                        leftSet = x + 5;
                    } else {
                        leftSet = x + 2;
                    }

                    $allaccessTooltip.css({ 'position': 'fixed', 'left': leftSet, 'top': y - 10 }).show();
                };

                $allaccessHelp.addClass('hovered');
            }
        }, function () {
            hoverTimeout = setTimeout(function () {
                $allaccessTooltip.hide();
                $allaccessHelp.removeClass('hovered');
            }, 1000);
        });

    });
    

</script>
<asp:PlaceHolder ID="phVIPOverlay" runat="server">
    <mn:VIPOverlay runat="server" ID="vipOverlay" />
</asp:PlaceHolder>
<asp:PlaceHolder ID="plcInboxShutdownOverlay" runat="server" Visible="false">
    <mn:InboxShutdownOverlay runat="server" />
</asp:PlaceHolder>
<div class="header-options">
    <h1>
        <asp:Label ID="txtMailBox" runat="server"></asp:Label></h1>
    <div class="pagination-mail">
        <asp:Label runat="server" ID="lnkTopPaging" Visible="true" />
    </div>
</div>
<mn:TabBar ID="mailBoxTabBar" runat="server" ClassSelected="selected" />
<div class="nav-rounded-tabs-sub-menu">
    <asp:LinkButton runat="server" ID="lnkTogglePhotos" CssClass="toggle-photos">
        <mn:Txt ID="txtTogglePhotos" name="txtTogglePhotos" runat="server" ResourceConstant="TXT_TOGGLE_PHOTOS" />
    </asp:LinkButton>
    <input type="hidden" name="hdnToggle" />
</div>
<div id="mail-options" class="clearfix">
    <!-- begin mail system -->
    <asp:Panel ID="mailBarPanel" Visible="True" runat="server">
        <div id="moveToFolder">
            <mn:Image ID="imgNewMessages" runat="server" ResourceConstant="ALT_DOWN_ARROW" FileName="icon-select-all.gif"
                CssClass="mail-select-arrow" TitleResourceConstant="TTL_SELECT_MESSAGES" />
            <asp:DropDownList ID="dropDownListMove" runat="server" Enabled="True" AutoPostBack="False" />
            &nbsp;
            <cc1:FrameworkButton class="btn primary" ID="moveMessages" runat="server" ResourceConstant="BTN_MOVE" />&nbsp;
            <mn:Txt ID="txtEditfolders" runat="server" ResourceConstant="TXT_EDIT_FOLDERS_ARROWS"
                Href="/Applications/Email/FolderSettings.aspx" />
        </div>
        <div class="float-outside">
            <cc1:FrameworkButton class="btn secondary" ID="EmptyFolder" runat="server" ResourceConstant="BTN_EMPTY_FOLDER"
                Visible="False" />
            <cc1:FrameworkButton class="btn secondary" ID="deleteMessage" runat="server" ResourceConstant="BTN_DELETE" />
            <cc1:FrameworkButton class="btn secondary" ID="deleteAllMessages" runat="server"
                ResourceConstant="BTN_DELETE_ALL" Visible="false"></cc1:FrameworkButton>
            <cc1:FrameworkButton class="btn secondary" ID="ignoreMessage" runat="server" ResourceConstant="BTN_IGNORE" />
            <cc1:FrameworkButton class="btn secondary last" ID="markUnreadMessage" runat="server"
                ResourceConstant="BTN_MARK_AS_UNREAD" />
        </div>
    </asp:Panel>
</div>
<div id="mail-list"> 
    <!--ALL ACCESS MAIL-->
    <div id="allaccess-inbox-header">
        <mn:Txt runat="server" ResourceConstant="TXT_ALLACCESS_HEADER" ID="txtAllAccessHeader" />
    </div>
    <asp:Panel ID="emptyFolderPanel" runat="server" Visible="False">
        <mn:Txt ID="txtEmptyFolder" runat="server" ResourceConstant="TXT_THIS_FOLDER_IS_EMPTY"
            ExpandImageTokens="true" />
    </asp:Panel>
    <asp:DataGrid EnableViewState="False" PageSize="20" AllowCustomPaging="True" Visible="True"
        ID="dgVIPMailHeader" runat="server" CssClass="mail-table allaccess header" GridLines="None"
        AutoGenerateColumns="False" ShowFooter="False" ShowHeader="True" OnItemDataBound="vipMailBoxHeaderGrid_DataBound">
        <HeaderStyle Wrap="False" CssClass="mail-header" />
        <Columns>
            <asp:TemplateColumn HeaderStyle-CssClass="mail-header-check mail-header-check-vip"
                ItemStyle-CssClass="mail-item-check mail-item-check-vip" FooterStyle-CssClass="mail-footer-check mail-footer-check-vip">
                <HeaderTemplate>
                    <input type="checkbox" name="headerCheckAllVIP" onclick="checkAllVIPMessages('mail-header-check-vip')" />
                </HeaderTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderStyle-CssClass="mail-header-status" ItemStyle-CssClass="mail-item-status"
                FooterStyle-CssClass="mail-footer-status">
                <HeaderTemplate>
                    <asp:HyperLink runat="server" Visible="True" NavigateUrl="#" ID="itemUnreadSortLink">
                        <mn:Image ID="Image1" runat="server" App="Email" ResourceConstant="ALT_EMAIL_SORT"
                            FileName="icon-email-sort.gif" />
                    </asp:HyperLink>
                </HeaderTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderStyle-CssClass="mail-header-username" ItemStyle-CssClass="mail-item-username">
                <HeaderTemplate>
                    <asp:HyperLink runat="server" Visible="True" NavigateUrl="#" ID="itemFromSortLink">
                        <mn:Txt runat="server" ResourceConstant="TXT_FROM" ID="Txt1" NAME="Txt1" />
                    </asp:HyperLink>
                </HeaderTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderStyle-CssClass="mail-header-subject" ItemStyle-CssClass="mail-item-subject">
                <HeaderTemplate>
                    <asp:HyperLink runat="server" Visible="True" NavigateUrl="#" ID="itemSubjectSortLink">
                        <mn:Txt ID="Txt2" runat="server" ResourceConstant="TXT_SUBJECT" />
                    </asp:HyperLink>
                </HeaderTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderStyle-CssClass="mail-header-date" ItemStyle-CssClass="mail-item-date">
                <HeaderTemplate>
                    <asp:HyperLink runat="server" Visible="True" NavigateUrl="#" ID="itemDateSortLink">
                        <mn:Txt ID="Txt3" runat="server" ResourceConstant="TXT_DATE" />
                    </asp:HyperLink>
                </HeaderTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderStyle-CssClass="mail-header-indicator" ItemStyle-CssClass="mail-item-indicator">
                <HeaderTemplate>
                    <asp:HyperLink runat="server" NavigateUrl="#" ID="itemOpenedSortLink">
                        <mn:Txt ID="Txt4" runat="server" ResourceConstant="MAIL_OPENED" />
                    </asp:HyperLink>
                </HeaderTemplate>
            </asp:TemplateColumn>
            <asp:BoundColumn Visible="False" DataField="StatusMask" />
            <asp:BoundColumn Visible="False" DataField="MemberMailID" />
            <asp:BoundColumn Visible="False" DataField="MemberFolderID" />
            <asp:BoundColumn Visible="False" DataField="Subject" />
            <asp:BoundColumn Visible="False" DataField="FromUserName" />
            <asp:BoundColumn Visible="False" DataField="InsertDate" DataFormatString="{0:MM/dd/yyyy hh:mm tt}" />
            <asp:BoundColumn Visible="False" DataField="FromMemberID" />
            <asp:BoundColumn Visible="False" DataField="ToUserName" />
            <asp:BoundColumn Visible="False" DataField="ToMemberID" />
            <asp:BoundColumn Visible="False" DataField="OpenDate" />
        </Columns>
    </asp:DataGrid>
    <mn:Txt ID="txtVIPEmpty" runat="server" Visible="false" ResourceConstant="TXT_VIP_EMPTY"
        ExpandImageTokens="true" />
    <div id="allaccess-message-container">
        <asp:DataGrid EnableViewState="False" PageSize="20" AllowCustomPaging="True" Visible="True"
            ID="dgVIPMail" runat="server" CssClass="mail-table allaccess messages" GridLines="None"
            AutoGenerateColumns="False" ShowFooter="False" ShowHeader="False" OnItemDataBound="vipMailBoxGrid_DataBound">
            <Columns>
                <asp:TemplateColumn HeaderStyle-CssClass="mail-header-check mail-header-check-vip"
                    ItemStyle-CssClass="mail-item-check mail-item-check-vip" FooterStyle-CssClass="mail-footer-check mail-footer-check-vip">
                    <ItemTemplate>
                        <input type="checkbox" runat="server" name="itemCheckbox" id="itemCheckBox" />
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderStyle-CssClass="mail-header-status" ItemStyle-CssClass="mail-item-status"
                    FooterStyle-CssClass="mail-footer-status">
                    <ItemTemplate>
                        <asp:HyperLink runat="server" ID="itemImageUnopenedLink" Visible="False">
                            <mn:Image runat="server" App="Email" ResourceConstant="ALT_EMAIL_UNOPENED" FileName="icon-email-unopened.gif"
                                ID="Icon_Email_Unopened" />
                        </asp:HyperLink>
                        <asp:HyperLink runat="server" ID="itemImageOpenedLink" Visible="False">
                            <mn:Image ID="Image2" runat="server" App="Email" ResourceConstant="ALT_EMAIL_OPENED"
                                FileName="icon-email-opened.gif" />
                        </asp:HyperLink>
                        <asp:HyperLink runat="server" ID="itemImageRepliedLink" Visible="False">
                            <mn:Image ID="Image3" runat="server" App="Email" ResourceConstant="ALT_EMAIL_REPLIED"
                                FileName="icon-email-replied.gif" />
                        </asp:HyperLink>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderStyle-CssClass="mail-header-username" ItemStyle-CssClass="mail-item-username">
                    <ItemTemplate>
                        <asp:HyperLink runat="server" Visible="True" ID="itemFromImageLink">
                            <asp:Image runat="server" ID="imgPhoto" CssClass="mail-thumb" alt="" />
                        </asp:HyperLink>
                        <asp:HyperLink runat="server" Visible="True" ID="itemFromLink">
                            <asp:Label runat="server" Visible="True" ID="lblFrom"></asp:Label>
                        </asp:HyperLink>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderStyle-CssClass="mail-header-subject" ItemStyle-CssClass="mail-item-subject">
                    <ItemTemplate>
                        <asp:HyperLink runat="server" Visible="True" ID="itemSubjectLink"></asp:HyperLink>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn></asp:TemplateColumn>
                <asp:TemplateColumn HeaderStyle-CssClass="mail-header-date" ItemStyle-CssClass="mail-item-date">
                    <ItemTemplate>
                        <asp:Label runat="server" Visible="True" ID="itemInsertDate"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderStyle-CssClass="mail-header-indicator" ItemStyle-CssClass="mail-item-indicator">
                    <ItemTemplate>
                        <mn:Image runat="server" App="Email" FileName="icon-email-viewed.gif" ID="OpenedImage" Visible="False" />
                        <mn:Image runat="server" ID="aaSentItemImageUnopened" App="Email" FileName="icon-email-unopened.gif" Visible="False"  />
                        <mn:image runat="server" ID="aaSentImageOpened" app="Email"  filename="icon-email-opened.gif" Visible="False" />
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:BoundColumn Visible="False" DataField="StatusMask" />
                <asp:BoundColumn Visible="False" DataField="MemberMailID" />
                <asp:BoundColumn Visible="False" DataField="MemberFolderID" />
                <asp:BoundColumn Visible="False" DataField="Subject" />
                <asp:BoundColumn Visible="False" DataField="FromUserName" />
                <asp:BoundColumn Visible="False" DataField="InsertDate" DataFormatString="{0:MM/dd/yyyy hh:mm tt}" />
                <asp:BoundColumn Visible="False" DataField="FromMemberID" />
                <asp:BoundColumn Visible="False" DataField="ToUserName" />
                <asp:BoundColumn Visible="False" DataField="ToMemberID" />
                <asp:BoundColumn Visible="False" DataField="OpenDate" DataFormatString="{0:MM/dd/yyyy hh:mm tt}"/>
            </Columns>
        </asp:DataGrid>
    </div>
    <asp:DataGrid EnableViewState="False" PageSize="20" AllowCustomPaging="True" Visible="True"
        ID="dgVIPMailFooter" runat="server" CssClass="mail-table allaccess footer" GridLines="None"
        AutoGenerateColumns="False" ShowFooter="True" ShowHeader="False" OnItemDataBound="vipMailBoxFooterGrid_DataBound">
        <FooterStyle Wrap="False" CssClass="mail-footer" />
        <Columns>
            <asp:TemplateColumn FooterStyle-CssClass="mail-footer-check mail-footer-check-vip">
                <FooterTemplate>
                    <input type="checkbox" name="footerCheckAllVIP" onclick="checkAllVIPMessages('mail-footer-check-vip')" />
                </FooterTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn FooterStyle-CssClass="mail-footer-status"></asp:TemplateColumn>
            <asp:BoundColumn Visible="False" DataField="StatusMask" />
            <asp:BoundColumn Visible="False" DataField="MemberMailID" />
            <asp:BoundColumn Visible="False" DataField="MemberFolderID" />
            <asp:BoundColumn Visible="False" DataField="Subject" />
            <asp:BoundColumn Visible="False" DataField="FromUserName" />
            <asp:BoundColumn Visible="False" DataField="InsertDate" DataFormatString="{0:MM/dd/yyyy hh:mm tt}" />
            <asp:BoundColumn Visible="False" DataField="FromMemberID" />
            <asp:BoundColumn Visible="False" DataField="ToUserName" />
            <asp:BoundColumn Visible="False" DataField="ToMemberID" />
            <asp:BoundColumn Visible="False" DataField="OpenDate" DataFormatString="{0:MM/dd/yyyy hh:mm tt}"/>
        </Columns>
    </asp:DataGrid>
    <div class="pagination-mail">
        <asp:Label runat="server" ID="lnkVIPPaging" Visible="true" />
        <div class="pagination-buttons clearfix image-text-pair">
            <mn:Txt runat="server" CssClass="btn link-secondary" ID="lnkPrevVIP" Visible="false"
                ResourceConstant="TXT_PREVIOUS_PAGE" />
            <mn:Txt runat="server" CssClass="btn link-secondary" ID="lnkNextVIP" Visible="false"
                ResourceConstant="TXT_NEXT_PAGE" />
        </div>
    </div>
    
    <!--INBOX SHUTDOWN BANNER-->
    <asp:PlaceHolder runat="server" ID="plcInboxShutdownBanner" Visible="false">
        <mn:Image ID="imgInboxShutdownBanner" runat="server" ResourceConstant="רוצה לקרוא את כל ההודעות שלך? לחץ כאן"
            FileName="inbox-shutdown-banner.png" CssClass="inbox-shutdown-banner" TitleResourceConstant="רוצה לקרוא את כל ההודעות שלך? לחץ כאן" />
    </asp:PlaceHolder>

    <!--STANDARD MAIL-->
    <div id="standard-mail-header">
        <mn:Txt runat="server" ID="txtStandartMailHeader" ResourceConstant="TXT_STANDARD_MAIL" />
        <asp:Literal ID="litReadReceiptPurchaseLink" runat="server"></asp:Literal>
    </div>
    <asp:DataGrid EnableViewState="False" PageSize="20" AllowCustomPaging="True" Visible="True"
        ID="mailDataGrid" runat="server" CssClass="mail-table" GridLines="None" AutoGenerateColumns="False"
        ShowFooter="True" ShowHeader="True" OnItemDataBound="mailBoxGrid_DataBound">
        <HeaderStyle Wrap="False" CssClass="mail-header" />
        <FooterStyle Wrap="False" CssClass="mail-footer" />
        <Columns>
            <asp:TemplateColumn HeaderStyle-CssClass="mail-header-check mail-header-check-reg"
                ItemStyle-CssClass="mail-item-check mail-item-check-reg" FooterStyle-CssClass="mail-footer-check mail-footer-check-reg">
                <HeaderTemplate>
                    <input type="checkbox" name="headerCheckAll" onclick="checkAllMessages('mail-header-check-reg')" />
                </HeaderTemplate>
                <ItemTemplate>
                    <input type="checkbox" runat="server" name="itemCheckbox" id="itemCheckBox" />
                </ItemTemplate>
                <FooterTemplate>
                    <input type="checkbox" name="footerCheckAll" onclick="checkAllMessages('mail-footer-check-reg')" />
                </FooterTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderStyle-CssClass="mail-header-status" ItemStyle-CssClass="mail-item-status"
                FooterStyle-CssClass="mail-footer-status">
                <HeaderTemplate>
                    <asp:HyperLink runat="server" Visible="True" NavigateUrl="#" ID="itemUnreadSortLink" >
						<mn:image runat="server" app="Email" resourceconstant="ALT_EMAIL_SORT" filename="icon-email-sort.gif" />
                    </asp:HyperLink>
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:HyperLink runat="server" ID="itemImageUnopenedLink" Visible="False">
                        <mn:Image runat="server" App="Email" ResourceConstant="ALT_EMAIL_UNOPENED" FileName="icon-email-unopened.gif"
                            ID="Icon_Email_Unopened" />
                    </asp:HyperLink>
                    <asp:HyperLink runat="server" ID="itemImageOpenedLink" Visible="False">
						<mn:image runat="server" app="Email" resourceconstant="ALT_EMAIL_OPENED" filename="icon-email-opened.gif" />
                    </asp:HyperLink>
                    <asp:HyperLink runat="server" ID="itemImageRepliedLink" Visible="False">
						<mn:image runat="server" app="Email" resourceconstant="ALT_EMAIL_REPLIED" filename="icon-email-replied.gif" />
                    </asp:HyperLink>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderStyle-CssClass="mail-header-username" ItemStyle-CssClass="mail-item-username">
                <HeaderTemplate>
                    <asp:HyperLink runat="server" Visible="True" NavigateUrl="#" ID="itemFromSortLink">
                        <mn:Txt runat="server" ResourceConstant="TXT_FROM" ID="Txt1" NAME="Txt1" />
                    </asp:HyperLink>
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:HyperLink runat="server" Visible="True" ID="itemFromImageLink">
                        <asp:Image runat="server" ID="imgPhoto" CssClass="mail-thumb" alt="" />
                    </asp:HyperLink>
                    <asp:HyperLink runat="server" Visible="True" ID="itemFromLink">
                        <asp:Label runat="server" Visible="True" ID="lblFrom"></asp:Label>
                    </asp:HyperLink>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderStyle-CssClass="mail-header-subject" ItemStyle-CssClass="mail-item-subject">
                <HeaderTemplate>
                    <asp:HyperLink runat="server" Visible="True" NavigateUrl="#" ID="itemSubjectSortLink">
						<mn:txt runat="server" resourceconstant="TXT_SUBJECT" />
                    </asp:HyperLink>
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:HyperLink runat="server" Visible="True" ID="itemSubjectLink"></asp:HyperLink>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderStyle-CssClass="mail-header-date" ItemStyle-CssClass="mail-item-date">
                <HeaderTemplate>
                    <asp:HyperLink runat="server" Visible="True" NavigateUrl="#" ID="itemDateSortLink">
						<mn:txt runat="server" resourceconstant="TXT_DATE" />
                    </asp:HyperLink>
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Label runat="server" Visible="True" ID="itemInsertDate"></asp:Label>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderStyle-CssClass="mail-header-indicator" ItemStyle-CssClass="mail-item-indicator">
                <HeaderTemplate>
                    <asp:HyperLink runat="server" NavigateUrl="#" ID="itemOpenedSortLink">
						<mn:txt runat="server" ResourceConstant="MAIL_OPENED" />
                    </asp:HyperLink>
                </HeaderTemplate>
                <ItemTemplate>
                    <mn:Image runat="server" App="Email" FileName="icon-email-viewed.gif" ID="OpenedImage" Visible="False" />
                    <mn:Image runat="server" ID="sentItemImageUnopened" App="Email" FileName="icon-email-unopened.gif" Visible="False"  />
                    <mn:image runat="server" ID="sentImageOpened" app="Email"  filename="icon-email-opened.gif" Visible="False" />
                    <asp:HyperLink ID="lnkReadReceiptLock" runat="server" Visible="false">
                        <mn:Sprite ID="sprReadReceiptLock" TextResourceConstant="" TitleResourceConstant="" SpriteClass="spr-i ui-icons-locked" runat="server" />
                    </asp:HyperLink>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderStyle-CssClass="mail-header-date" ItemStyle-CssClass="mail-item-date">
                <HeaderTemplate>
                    <asp:HyperLink runat="server" Visible="True" NavigateUrl="#" ID="itemOpenedDateSortLink">
						<mn:txt runat="server" ResourceConstant="MAIL_OPENED_DATE" />
                    </asp:HyperLink>
                </HeaderTemplate>
                <ItemTemplate>
                     <ItemTemplate>
                    <asp:Label runat="server" Visible="True" ID="itemOpenedDate"></asp:Label>
                </ItemTemplate>
                </ItemTemplate>
            </asp:TemplateColumn>
            
        <asp:BoundColumn Visible="False" DataField="StatusMask" />
        <asp:BoundColumn Visible="False" DataField="MemberMailID" />
        <asp:BoundColumn Visible="False" DataField="MemberFolderID" />
        <asp:BoundColumn Visible="False" DataField="Subject" />
        <asp:BoundColumn Visible="False" DataField="FromUserName" />
        <asp:BoundColumn Visible="False" DataField="InsertDate" DataFormatString="{0:MM/dd/yyyy hh:mm tt}" />
        <asp:BoundColumn Visible="False" DataField="FromMemberID" />
        <asp:BoundColumn Visible="False" DataField="ToUserName" />
        <asp:BoundColumn Visible="False" DataField="ToMemberID" />
        <asp:BoundColumn Visible="False" DataField="OpenDate" DataFormatString="{0:MM/dd/yyyy hh:mm tt}"/>
        </Columns>
    </asp:DataGrid>
</div>
<div class="pagination-mail">
    <asp:Label runat="server" ID="lnkBottomPaging" Visible="true" />
    <div class="pagination-buttons clearfix image-text-pair">
        <mn:Txt runat="server" CssClass="btn link-secondary" ID="lnkPrev" Visible="false"
            ResourceConstant="TXT_PREVIOUS_PAGE" />
        <mn:Txt runat="server" CssClass="btn link-secondary" ID="lnkNext" Visible="false"
            ResourceConstant="TXT_NEXT_PAGE" />
    </div>
</div>
