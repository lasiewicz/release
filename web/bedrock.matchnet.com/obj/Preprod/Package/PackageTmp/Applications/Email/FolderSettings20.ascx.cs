﻿using Matchnet.Content.ServiceAdapters.Links;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Email.ValueObjects;
using Matchnet.Email.ServiceAdapters;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Util;

using System;
using System.Collections;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;


namespace Matchnet.Web.Applications.Email
{
    public partial class FolderSettings20 : FrameworkControl
    {
        private void Page_Load(object sender, System.EventArgs e)
        {
            try
            {
                validMemberAccess();

                g.BreadCrumbTrailHeader.SetThreeLinkCrumb(g.GetResource("TXT_MESSAGES", this), g.AppPage.App.DefaultPagePath
                    , g.GetResource("TXT_YOUR_MESSAGE_FOLDERS", this), string.Empty);
                g.BreadCrumbTrailFooter.SetThreeLinkCrumb(g.GetResource("TXT_MESSAGES", this), g.AppPage.App.DefaultPagePath
                    , g.GetResource("TXT_YOUR_MESSAGE_FOLDERS", this), string.Empty);
                initializeFolders();

                if (g.MemberEmailFolders != null && g.MemberEmailFolders.Count > 0)
                {
                    setMessageTotals();
                }
            }
            catch (Exception anException)
            {
                g.ProcessException(anException);
            }
        }

        // filter out to see if the current member has access
        private void validMemberAccess()
        {
            if (!applyImailPermissions())
            {
                if (!g.Brand.GetStatusMaskValue(StatusType.PayToSendEmail)
                    && g.Brand.IsPayToReplySite
                    && !g.ValidateMemberContact())
                {
                    Redirect.Subscription(g.Brand, (int)PurchaseReasonType.AttemptToAccessInbox, 0);
                }
            }
        }

        /// <summary>
        /// Refer to IMail Permissions project specs. Also check to see if member is a non subscriber.
        /// </summary>
        /// <returns>True if this feature is activated and member is not a subscriber.</returns>
        private bool applyImailPermissions()
        {
            if (Convert.ToBoolean(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("IMailPermissions", g.Brand.Site.Community.CommunityID,
                g.Brand.Site.SiteID, g.Brand.BrandID)))
            {
                if (g.TargetBrand.IsPaySite)
                {
                    if (MemberPrivilegeAttr.IsCureentSubscribedMember(g) || g.IsAdmin)
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
                else
                {
                    // free site.
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        // handles any custom repeater data binding modifications
        public void systemFolderListDataBound(object sender, RepeaterItemEventArgs args)
        {
            Literal currentLiteral = null;
            EmailFolder aFolder = null;
            Literal litFolderName = null;
            Button customFolderRenameSave = null;

            try
            {
                if (args.Item.ItemType == ListItemType.Item || args.Item.ItemType == ListItemType.AlternatingItem)
                {
                    // set up all the conditional table cells
                    // this has to be done first because they then need to be hidden.
                    HtmlTableCell tdCustomFolderRename = (HtmlTableCell)args.Item.FindControl("tdCustomFolderRename");
                    HtmlTableCell tdCustomFolderDelete = (HtmlTableCell)args.Item.FindControl("tdCustomFolderDelete");
                    HtmlTableCell tdCustomFolderNormal = (HtmlTableCell)args.Item.FindControl("tdCustomFolderNormal");
                   

                    tdCustomFolderRename.Visible = false;
                    tdCustomFolderDelete.Visible = false;
                    tdCustomFolderNormal.Visible = false;
                    

                    aFolder = (EmailFolder)args.Item.DataItem;

                    Literal litTotalMessages = (Literal)args.Item.FindControl("litTotalMessages");
                    Literal litUnreadMessages = (Literal)args.Item.FindControl("litUnreadMessages");
                    //Literal litDisplayKBytes = (Literal)args.Item.FindControl("litDisplayKBytes");

                    litTotalMessages.Text = EmailMessageSA.Instance.RetrieveMessages(g.Member.MemberID, g.Brand.Site.Community.CommunityID, aFolder.MemberFolderID).Count.ToString();
                    litUnreadMessages.Text = EmailMessageSA.Instance.RetrieveMessages(g.Member.MemberID, g.Brand.Site.Community.CommunityID, aFolder.MemberFolderID).UnreadCount.ToString();
                    //litDisplayKBytes.Text = EmailMessageSA.Instance.RetrieveMessages(g.Member.MemberID, g.Brand.Site.Community.CommunityID, aFolder.MemberFolderID).GetDisplayKBytes().ToString();

                    if (!aFolder.SystemFolder)
                    {
                        if (Request.QueryString["rid"] != null && Request.QueryString["rid"].Equals(aFolder.MemberFolderID.ToString()))
                        {
                            tdCustomFolderRename.Visible = true;
                            System.Web.UI.HtmlControls.HtmlInputText customFolderName = (HtmlInputText)args.Item.FindControl("customFolderName");
                            customFolderName.Value = FrameworkGlobals.GetUnicodeText(aFolder.Description);
                            customFolderRenameSave = (Button)args.Item.FindControl("customFolderRenameSave");
                            customFolderRenameSave.Text = g.GetResource("BTN_SAVE", this);
                        }
                        else if (Request.QueryString["did"] != null && Request.QueryString["did"].Equals(aFolder.MemberFolderID.ToString()))
                        {
                            litFolderName = (Literal)args.Item.FindControl("litDeleteFolderName");
                            litFolderName.Text = FrameworkGlobals.GetUnicodeText(aFolder.Description);
                            tdCustomFolderDelete.Visible = true;
                        }
                        else
                        {
                            tdCustomFolderNormal.Visible = true;
                            litFolderName = (Literal)args.Item.FindControl("litCustomFolderName");
                            litFolderName.Text = FrameworkGlobals.GetUnicodeText(aFolder.Description);
                        }
                    }
                 

                   
                }
            }
            catch (Exception anException)
            {
                throw anException;
            }
        }

        protected void confirmDeleteFolder(object sender, EventArgs e)
        {
            int folderToDelete = 0;

            try
            {
                folderToDelete = int.Parse(Request.QueryString["did"].Trim());

                EmailFolderSA.Instance.Delete(folderToDelete, g.TargetMemberID, g.Brand.Site.Community.CommunityID);

                g.Transfer("/Applications/Email/FolderSettings.aspx?rc=TXT_DELETE_FOLDER_SUCCESS");
            }
            catch (Exception anException)
            {
                g.ProcessException(anException);
            }
        }

        protected void cancelDeleteFolder(object sender, EventArgs e)
        {
            g.Transfer("/Applications/Email/FolderSettings.aspx");
        }

        // specific check for system folder duplicates for hebrew language as the 
        // member folder save stored procedure does not take this into account
        private bool hebrewIsSystemFolder(string newFolderName)
        {
            if (g.Brand.Site.LanguageID == (int)Matchnet.Language.Hebrew)
            {
                if (newFolderName.Trim().ToLower().Equals(
                    g.GetResource("TOK_INBOX", this).Trim().ToLower()))
                {
                    return true;
                }
                else if (newFolderName.Trim().ToLower().Equals(
                    g.GetResource("TXT_DRAFT", this).Trim().ToLower()))
                {
                    return true;
                }
                else if (newFolderName.Trim().ToLower().Equals(
                    g.GetResource("TXT_SENT", this).Trim().ToLower()))
                {
                    return true;
                }
                else if (newFolderName.Trim().ToLower().Equals(
                    g.GetResource("TXT_TRASH", this).Trim().ToLower()))
                {
                    return true;
                }
            }
            return false;
        }

        protected void renameFolder(object sender, EventArgs e)
        {
            string newFolderName = null;
            int folderToRename = 0;
            FolderSaveResultStatus folderRenameResult = FolderSaveResultStatus.Success;
            System.Web.UI.WebControls.Button saveButton;
            System.Web.UI.HtmlControls.HtmlInputText customFolderName;

            try
            {
                saveButton = (Button)sender;
                customFolderName = (HtmlInputText)saveButton.Parent.FindControl("customFolderName");
                newFolderName = customFolderName.Value;

                if (newFolderName != null && newFolderName.Trim().Length > 0)
                {
                    folderToRename = int.Parse(Request.QueryString["rid"].Trim().ToString());

                    if (hebrewIsSystemFolder(newFolderName))
                    {
                        folderRenameResult = FolderSaveResultStatus.FolderNameExists;
                    }
                    else
                    {
                        newFolderName = newFolderName;
                        folderRenameResult = EmailFolderSA.Instance.Save(g.TargetMemberID, g.TargetCommunityID, newFolderName, folderToRename).FolderSaveResultStatus;
                    }

                    switch (folderRenameResult)
                    {
                        case FolderSaveResultStatus.Success:
                            g.Transfer("/Applications/Email/FolderSettings.aspx?rc=TXT_RENAME_FOLDER_SUCCESS");
                            break;
                        case FolderSaveResultStatus.FolderNameExists:
                            g.Transfer("/Applications/Email/FolderSettings.aspx?rc=TXT_NEW_FOLDER_NAME_EXISTS_FAILURE");
                            break;
                        default:
                            g.Transfer("/Applications/Email/FolderSettings.aspx?rc=TXT_RENAME_FOLDER_FAILURE");
                            break;
                    }
                }
                else
                {
                    g.Transfer("/Applications/Email/FolderSettings.aspx?rc=TXT_RENAME_FOLDER_NONAME");
                }
            }
            catch (Exception anException)
            {
                g.ProcessException(anException);
            }
        }

        // handles binding the repeater with the current folders collection
        private void initializeFolders()
        {
           
            EmailFolderCollection coll = new EmailFolderCollection(g.Member.MemberID, g.Brand.Site.Community.CommunityID, true);
            IEnumerator enumerator = g.MemberEmailFolders.GetEnumerator();
            while (enumerator.MoveNext())
            {
                EmailFolder folder=(EmailFolder) enumerator.Current;
                if (!folder.SystemFolder)
                    coll.Add(folder);

            }
            systemFolderListRepeater.DataSource = coll;
            systemFolderListRepeater.DataBind();
        }

        // handles logic for creating a new folder
        private void newFolderClick(object sender, System.EventArgs e)
        {
            try
            {
                if (newFolderName != null && newFolderName.Text.Trim().Length > 0)
                {
                    string encodedFolderName = null;
                    FolderSaveResultStatus folderSaveResult = FolderSaveResultStatus.Success;

                    if (hebrewIsSystemFolder(newFolderName.Text))
                    {
                        folderSaveResult = FolderSaveResultStatus.FolderNameExists;
                    }
                    else
                    {
                        encodedFolderName = newFolderName.Text.Trim();
                        folderSaveResult = EmailFolderSA.Instance.Save(g.TargetMemberID, g.TargetCommunityID, encodedFolderName).FolderSaveResultStatus;
                    }
                    switch (folderSaveResult)
                    {
                        case FolderSaveResultStatus.Success:
                            g.Transfer("/Applications/Email/FolderSettings.aspx?rc=TXT_NEW_FOLDER_SUCCESS");
                            break;
                        case FolderSaveResultStatus.FolderNameExists:
                            g.Transfer("/Applications/Email/FolderSettings.aspx?rc=TXT_NEW_FOLDER_NAME_EXISTS_FAILURE");
                            break;
                        default:
                            g.Transfer("/Applications/Email/FolderSettings.aspx?rc=TXT_NEW_FOLDER_FAILURE");
                            break;
                    }
                }
                else
                {
                    g.Transfer("/Applications/Email/FolderSettings.aspx?rc=TXT_NEW_FOLDER_NO_NAME_FAILURE");
                }
                initializeFolders();
            }
            catch (Exception anException)
            {
                g.ProcessException(anException);
            }
        }

        // sets the lower table with the total message calculations
        private void setMessageTotals()
        {
            HtmlTableCell currentTableCell = new HtmlTableCell();
            Control currentControl = null;
            IEnumerator enumerator = null;

            enumerator = Controls.GetEnumerator();
            while (enumerator.MoveNext())
            {
                currentControl = (Control)enumerator.Current;
                if (currentControl is HtmlTableCell)
                {
                    currentTableCell = (HtmlTableCell)currentControl;
                    switch (currentTableCell.ID)
                    {
                        case "totalMessageCount":
                            currentTableCell.InnerText = getTotalMessages(g.MemberEmailFolders, false).ToString();
                            break;
                        case "totalUnreadMessageCount":
                            currentTableCell.InnerText = getTotalMessages(g.MemberEmailFolders, true).ToString();
                            break;
                        case "totalSizeCount":
                            // iterate through each folder to get the aggregate total of all folders.
                            int totalSize = 0;
                            foreach (EmailFolder folder in g.MemberEmailFolders)
                            {
                                totalSize += EmailMessageSA.Instance.RetrieveMessages(g.Member.MemberID, g.Brand.Site.Community.CommunityID, folder.MemberFolderID).GetDisplayKBytes();
                            }
                            currentTableCell.InnerText = totalSize + "k";
                            break;
                        default:
                            break;
                    }
                }
            }
        }

        // handles iterating and calculating folder message totals
        private int getTotalMessages(ICollection allFolders, bool unread)
        {
            EmailFolder currentFolder = null;
            IEnumerator enumerator = null;
            int totalMessages = 0;

            if (allFolders != null && allFolders.Count > 0)
            {
                enumerator = allFolders.GetEnumerator();
                while (enumerator.MoveNext())
                {
                    currentFolder = (EmailFolder)enumerator.Current;
                    if (unread)
                    {
                        totalMessages += EmailMessageSA.Instance.RetrieveMessages(g.Member.MemberID, g.Brand.Site.Community.CommunityID, currentFolder.MemberFolderID).UnreadCount;
                    }
                    else
                    {
                        totalMessages += EmailMessageSA.Instance.RetrieveMessages(g.Member.MemberID, g.Brand.Site.Community.CommunityID, currentFolder.MemberFolderID).Count;
                    }
                }
            }
            return totalMessages;
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.newFolderButton.Click += new System.EventHandler(this.newFolderClick);
            this.newFolderName.TextChanged += new EventHandler(newFolderName_TextChanged);
            this.Load += new System.EventHandler(this.Page_Load);
        }
        #endregion

        private void newFolderName_TextChanged(object sender, EventArgs e)
        {
            this.newFolderClick(sender, e);
        }
    }
}