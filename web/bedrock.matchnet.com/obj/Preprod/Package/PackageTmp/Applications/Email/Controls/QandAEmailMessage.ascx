﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="QandAEmailMessage.ascx.cs" Inherits="Matchnet.Web.Applications.Email.Controls.QandAEmailMessage" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>

<div class="original-answer"><%--
    <p><mn:Txt id="txtOrigMessageHeader" ResourceConstant="TXT_ORIG_MESSAGE_HEADER" runat="server" /></p>--%>
    <p><mn:Txt id="txtOrigMessageLabel" runat="server" /></p>
    <dl>
        <dt><asp:HyperLink runat="server" ID="questionLink"><span class="q"></span><asp:Literal ID="questionText" runat="server"/></asp:HyperLink></dt>
        <dd><span class="a"><mn:Txt id="Txt1" ResourceConstant="TXT_ANSWER_LABEL" runat="server" /></span><asp:Literal ID="answerText" runat="server"/></dd>
     </dl>
</div>
