﻿using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ServiceAdapters.Links;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Content.ValueObjects.Region;
using Matchnet.Email.ValueObjects;
using Matchnet.Email.ServiceAdapters;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Util;
using Matchnet.Web.Framework.Ui.BasicElements.Links;
using Matchnet.List.ServiceAdapters;
using Matchnet.List.ValueObjects;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects;
using Matchnet.Member.ValueObjects.Photos;
using Matchnet.Web.Applications.MemberProfile;
//using Matchnet.Web.Applications.MemberProfile.ViewProfileHelper;
using Matchnet.Web.Framework.Ui;
using Matchnet.Web.Framework.Ui.FormElements;

using System;
using System.Collections;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace Matchnet.Web.Applications.Email
{
    public interface IViewMessage
    {
        System.Web.UI.WebControls.Label LblMemberProfileUserName { get; }
        System.Web.UI.WebControls.Repeater RptMemberProfile { get; }
        Link LnkFromHeaderLink { get; }
        Matchnet.Web.Framework.Txt LblViewMessage { get; }
        PlaceHolder PhAboutMeViewPlaceHolder { get; }
        PlaceHolder PhPersonalInfoViewPlaceHolder { get; }
        PlaceHolder PhBasicsViewPlaceHolder { get; }
        PlaceHolder PhPersonalityViewPlaceHolder { get; }
        PlaceHolder PhInterestsViewPlaceHolder { get; }
        PlaceHolder PhActivityViewPlaceHolder { get; }
        PlaceHolder PhIdealMatchViewPlaceholder { get; }
        HyperLink LnkPreviousMessageLink { get; }
        HyperLink LnkNextMessageLink { get; }
        HyperLink LnkViewMessageLink { get; }

        Matchnet.Web.Framework.Ui.FormElements.FrameworkButton BtnReportMember2 { get; }
        HyperLink LnkAppLink { get; }
        Literal MessageDateLiteral { get; }
        Literal MessageSubjectLiteral { get; }
        Literal MessageContentLiteral { get; }


        int MemberMailID { get; set; }
        int MemberFolderID { get; set; }
        string OrderBy { get; set; }
        Direction CurrentDirection { get; set; }
        EmailMessage CurrentMessage { get; set; }
        int CurrentMessageNumber { get; set; }
        int MessageCount { get; set; }
        int FromMemberID { get; set; }
        void ProcessPrivatePhotos();
        FrameworkControl ResourceControl { get; }
        PlaceHolder ViewTabbedProfile { get; }
        System.Web.UI.Page Page { get; }
        PlaceHolder PhMessageAppend { get; }
        PlaceHolder PLCInboxShutdownOverlay { get; }

        string JSMessage { get; set; }
        bool JSDoMessageConversion { get; set; }
    }
}
