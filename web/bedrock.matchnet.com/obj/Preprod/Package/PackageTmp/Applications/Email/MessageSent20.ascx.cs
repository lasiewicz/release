﻿using Matchnet.Web.Framework;
using Matchnet.Content.ServiceAdapters;
using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Matchnet.Web.Applications.HotList;
namespace Matchnet.Web.Applications.Email
{
    public partial class MessageSent20 :FrameworkControl
    {
        private void Page_Load(object sender, System.EventArgs e)
        {
            if (g.Session[Compose20.MESSAGE_ORIGIN_URL_SESSION_KEY] != null && !g.Session[Compose20.MESSAGE_ORIGIN_URL_SESSION_KEY].Equals(String.Empty)
                && PageConfigSA.Instance.GetPageName(g.Session[Compose20.MESSAGE_ORIGIN_URL_SESSION_KEY]).ToLower() != "viewmessage")
            {
                string navURL = g.Session[Compose20.MESSAGE_ORIGIN_URL_SESSION_KEY].ToString();

                // The rc parameter will only appear in the MESSAGE_ORIGIN_URL_SESSION_KEY when an email draft has been previously saved by the user (TT15848)
                if (navURL.IndexOf("rc") != -1)
                {
                    backToProfileLink.NavigateUrl = g.RemoveParamFromURL(navURL, "rc", false);
                }
                else
                {
                    backToProfileLink.NavigateUrl = g.Session[Compose20.MESSAGE_ORIGIN_URL_SESSION_KEY];
                }

                // TT#16047 - set the correct text
                if (PageConfigSA.Instance.GetPageName(g.Session[Compose20.MESSAGE_ORIGIN_URL_SESSION_KEY]).ToString().ToLower() == "viewprofile")
                {
                    txtBackToProfile.ResourceConstant = "TXT_BACK_TO_PROFILE";
                    txtBackToProfile.TitleResourceConstant = "TTL_BACK_TO_PROFILE";
                }
            }
            else if (!String.IsNullOrEmpty(Request.QueryString["colorcodeSTF"]))
            {
                backToProfileLink.NavigateUrl = "/Applications/ColorCode/Analysis.aspx";
                txtBackToProfile.ResourceConstant = "TXT_BACK_TO_COLORCODE";
            }
            else if (!String.IsNullOrEmpty(Request.QueryString["qandaSTF"]))
            {
                backToProfileLink.NavigateUrl = "/Applications/QuestionAnswer/Question.aspx";
                if(!String.IsNullOrEmpty(Request.QueryString["questid"])) {
                    backToProfileLink.NavigateUrl += "?questid="+Request.QueryString["questid"];
                }
                txtBackToProfile.ResourceConstant = "TXT_BACK_TO_QANDA";
            }
            else if (!String.IsNullOrEmpty(Request.QueryString["MemberID"]) && g.GetReferrerUrl().IndexOf("SendToFriend.aspx") != -1)
            {
                backToProfileLink.NavigateUrl = "/Applications/MemberProfile/ViewProfile.aspx";
                backToProfileLink.NavigateUrl +="?"+ Request.QueryString;
                txtBackToProfile.ResourceConstant = "TXT_BACK_TO_PROFILE";
                txtBackToProfile.TitleResourceConstant = "TTL_BACK_TO_PROFILE";
            }
            else
            {
                backToProfileLink.NavigateUrl = "/Applications/Email/Mailbox.aspx";
                txtBackToProfile.ResourceConstant = "TXT_BACK_TO_INBOX";
            }
            //profileStrip.HotlistDirection = HotlistDirectionType.WhoList;

            if (Request["vipemail"] != null)
            {
                bool isVipEmail = Convert.ToBoolean(Request["vipemail"]);
                plVIPMessageSent.Visible = isVipEmail;
                plDefaultMessageSent.Visible = !isVipEmail;
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            try
            {
                // Omniture Analytics - Event Tracking
                if (Convert.ToBoolean(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ANALYTICS_OMNITURE", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID)))
                {
                    // Message Sent
                    _g.AnalyticsOmniture.AddEvent("event11");

                    // Message Type
                    g.AnalyticsOmniture.Evar26 = "imail";

                    // Color Code
                    if (!String.IsNullOrEmpty(Request.QueryString["RecipientCC"]))
                    {
                        ColorCode.Color colorCode = ColorCode.Color.none;
                        try
                        {
                            colorCode = (ColorCode.Color)Enum.Parse(typeof(ColorCode.Color), Request.QueryString["RecipientCC"], true);
                        }
                        catch (Exception eColor)
                        {
                            //invalid querystring value
                        }

                        if (colorCode != Matchnet.Web.Applications.ColorCode.Color.none)
                        {
                            g.AnalyticsOmniture.Evar34 = ColorCode.ColorCodeHelper.GetFormattedColorText(colorCode);
                        }
                    }


                    bool vipemail = Convert.ToBoolean(Request["vipemail"]);
                    bool vipfreereply = Convert.ToBoolean(Request["vipfreereply"]);
                    if (vipemail)
                    {
                        if (vipfreereply)
                            g.AnalyticsOmniture.Evar26 = "imail-Reply VIP mail";
                        else
                            g.AnalyticsOmniture.Evar26 = "imail-Send VIP mail";

                    }
                    else if (!string.IsNullOrEmpty(Request["Reply"]) && (Request["Reply"]).ToString().Equals("1"))
                    {
                        //g.AnalyticsOmniture.Evar26 = "imail-Reply Standard mail";
                        g.AnalyticsOmniture.AddEvent("event15");
                    }
                    else
                    {
                        //g.AnalyticsOmniture.Evar26 = "imail-Send Standard mail";
                    }

                    if (!string.IsNullOrEmpty(Request["freeimail"]) && (Request["freeimail"]).ToString().Equals("1"))
                    {
                        g.AnalyticsOmniture.Evar26 = "Free iMail";
                    }

                    string toMemberID = string.Empty;

                    if(!string.IsNullOrEmpty(HttpContext.Current.Request.Params.Get("MemberID")))
                    {
                        toMemberID = HttpContext.Current.Request.Params.Get("MemberID");
                    }
                    else if (!string.IsNullOrEmpty(HttpContext.Current.Request.QueryString[WebConstants.URL_MESSAGE_RECIPIENT_MEMBER_ID]))
                    {
                        toMemberID = HttpContext.Current.Request.QueryString[WebConstants.URL_MESSAGE_RECIPIENT_MEMBER_ID];
                    }

                    if (!string.IsNullOrEmpty(toMemberID))
                    {
                        g.AnalyticsOmniture.Prop35 = toMemberID;
                    }

                    if (!string.IsNullOrEmpty(Request.QueryString["composeoverlay"]))
                    {
                        g.AnalyticsOmniture.PageName = "Mail Confirmation overlay";
                        g.AnalyticsOmniture.Evar2 = "Mail Confirmation overlay";
                        g.AnalyticsOmniture.Evar27 = "Email Me(" + Request.QueryString["ActionCallPage"].Replace('+', ' ') + ")";
                    }

                }

                base.OnPreRender(e);
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);

        }
        #endregion
    }
}
