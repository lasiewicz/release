using System;
using System.Web.UI.WebControls;
using System.Text;
using Matchnet.Member.ValueObjects;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Ui.BasicElements;

using Matchnet.Email.ServiceAdapters;
using Matchnet.Email.ValueObjects;
using Matchnet.ExternalMail.ServiceAdapters;
using Matchnet.List.ServiceAdapters;
using Matchnet.List.ValueObjects;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Session.ValueObjects;

namespace Matchnet.Web.Applications.Email
{
	/// <summary>
	///		Summary description for Tease.
	/// </summary>
	public class Tease : FrameworkControl,IFlirt
	{
		public static string TEASE_ORIGIN_URL_SESSION_KEY = "TeaseOriginUrl";

		private Matchnet.Member.ServiceAdapters.Member _member;
		protected System.Web.UI.WebControls.Repeater repTeaseCategories;
		protected System.Web.UI.WebControls.RadioButtonList rblTeaseContent;
		protected System.Web.UI.WebControls.Button btnSubmit;
		protected Matchnet.Web.Framework.Txt txtSendATease;
		protected Matchnet.Web.Framework.Txt txtCallToAction;
		protected System.Web.UI.WebControls.PlaceHolder miniProfile;

        #region IFlirt implementation


        public Matchnet.Member.ServiceAdapters.Member Member { get { return _member; } }
        public System.Web.UI.WebControls.Repeater RepTeaseCategories { get { return repTeaseCategories; } }
        public System.Web.UI.WebControls.RadioButtonList RblTeaseContent { get { return rblTeaseContent; } }


        public FrameworkControl ResourceControl { get { return this; } }
        #endregion

        FlirtHandler _handler = null;
        private void Page_Init(object sender, EventArgs e)
		{
			// TT #14412
			// this if statment was moved here from page_load because TEASE_ORIGIN_URL_SESSION_KEY wasn't getting
			// added if someone teases a member they've already teased.  That caused TeaseSentAlready to use the previous
			// value for TEASE_ORIGIN_URL_SESSION_KEY in the session if someone tries to tease the same user twice in a row.
            _handler = new FlirtHandler(this, g);
            _handler.Page_Initialize(IsPostBack, false);
            this._member = _handler.Member;
		}
		
		private void Page_Load(object sender, System.EventArgs e)
		{
			try
			{

				// Moved to Page_Init - mroberts TT #14412
//				if(!IsPostBack)
//				{
//					// Tease origination determination, bypass if it's a flirt category switch
//					if(Request["teaseCategoryID"] == null)
//					{
//						string absoluteUri = string.Empty;
//						if(Request.UrlReferrer.AbsoluteUri != null)
//						{
//							absoluteUri = Request.UrlReferrer.AbsoluteUri;
//						}
//
//						g.Session.Add(TEASE_ORIGIN_URL_SESSION_KEY, absoluteUri, SessionPropertyLifetime.Temporary);
//					}
//				}

				string controlPath = "../../Framework/Ui/BasicElements/MiniProfile.ascx";
				MiniProfile control = (MiniProfile)LoadControl(controlPath);

				if (control != null)
				{
					control.Member = this._member;
					control.Transparency = true;

					this.miniProfile.Controls.Add(control);
				}
				
				if ( MemberPrivilegeAttr.IsCureentSubscribedMember(g))
				{
					txtCallToAction.Visible=false;
				}
				
			}
			catch (Exception ex)
			{
				g.ProcessException(ex);
			}
		}

		protected void repTeaseCategories_ItemDataBound(object sender, 
			RepeaterItemEventArgs e)
		{
			HyperLink link			= null;
			Label text				= null;
			TeaseCategory tc	= null;		
			int currentCategoryID	= 0;

			link	= (HyperLink) e.Item.FindControl("lnkLink");
			text	= (Label) e.Item.FindControl("lblText");
			tc		= (TeaseCategory) e.Item.DataItem;
			
			if (Request.Params.Get("teaseCategoryID") != null)
			{
				try
				{
					currentCategoryID 
						= Convert.ToInt32(Request.Params.Get("teaseCategoryID"));
				}
				catch (FormatException)
				{
				}
			}			

			if ((currentCategoryID == 0 && e.Item.ItemIndex == 0)
				|| (currentCategoryID == tc.TeaseCategoryID))
			{
				link.NavigateUrl	= "";
				text.CssClass		= "prefTabItemActive";

				_handler.BindTeaseContentItems(tc.TeaseCategoryID);
			}
			else
			{
				link.NavigateUrl = _handler.BuildUrl(tc.TeaseCategoryID);
			}
			
			string content = g.GetResource(tc.ResourceKey, this);
			text.Text = content;
		}

		protected void btnSubmit_Click(object sender, EventArgs e)
		{
			try
			{
                _handler.SendTease(Constants.NULL_INT, Convert.ToInt32(RblTeaseContent.SelectedValue));
			}
			catch (Exception ex)
			{
				g.ProcessException(ex);
			}
		}

	
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.repTeaseCategories.ItemDataBound += new System.Web.UI.WebControls.RepeaterItemEventHandler(this.repTeaseCategories_ItemDataBound);
			this.btnSubmit.Click += new System.EventHandler(this.btnSubmit_Click);
			this.Load += new System.EventHandler(this.Page_Load);
			this.Init += new System.EventHandler(this.Page_Init);

		}
		#endregion
	}
}
