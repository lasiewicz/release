﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="VIPMailOverlay.ascx.cs" Inherits="Matchnet.Web.Applications.Email.VIPMailOverlay" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mnl" Namespace="Matchnet.Web.Framework.Ui.BasicElements.Links" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn" TagName="PhotoProfile" Src="~/Framework/Ui/BasicElements/PhotoProfile.ascx" %>
<div id="vip-inbox-overlay" class="clearfix">

<h1><mn:Txt ID="txtTitle" runat="server" ExpandImageTokens="true" /></h1>

<mn:PhotoProfile runat="server"  id="photoProfile" EnableProfileLinks="false" />
<div class="message">
    <mn:Txt ID="txtMessage" runat="server" ExpandImageTokens="true"  />
</div>
<div class="cta">
    <asp:HyperLink ID="lnkReadMessage" CssClass="link-primary" runat="server" ><mn:Txt ID="txtLinkText" runat="server" /></asp:HyperLink>
</div>
</div>

<script type="text/javascript"> 
    $j(document).ready(function() { 
        var $vipinboxoverlay = $j('#vip-inbox-overlay');
        $j.blockUI({
            message: $vipinboxoverlay,
            css: {
                border: '',
                width: 'auto',
                cursor: '',
                color: '',
                backgroundColor: 'transparent'
            },
            overlayCSS: {
                cursor: null
            }
        }); 
        $vipinboxoverlay.find('.cta').find('a').click(function() {
            $j.unblockUI();
        });
        var overlaywidth = $vipinboxoverlay.width();
        var overlayoffset = overlaywidth / 2;
        $vipinboxoverlay.parent('.blockMsg').css({'left': '50%', 'top': '30%', 'margin-left' : '-' + overlayoffset + 'px'});
    }); 
</script> 