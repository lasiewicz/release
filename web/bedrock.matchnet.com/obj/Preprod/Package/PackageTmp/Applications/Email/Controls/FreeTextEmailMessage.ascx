﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FreeTextEmailMessage.ascx.cs" Inherits="Matchnet.Web.Applications.Email.Controls.FreeTextEmailMessage" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>

<asp:Panel ID="freeTextReplyText" runat="server">
<div class="message-view-essay-comment">
    <h2><mn:Txt id="txtOrigMessageLabel" runat="server" /></h2>
    <div class="essay-body">
    <asp:Literal ID="commentText" runat="server"/>

    </div>
</div>
</asp:Panel>