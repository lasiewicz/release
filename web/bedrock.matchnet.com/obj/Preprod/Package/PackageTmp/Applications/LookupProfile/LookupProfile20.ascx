﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="LookupProfile20.ascx.cs" Inherits="Matchnet.Web.Applications.LookupProfile.LookupProfile20" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="uc1" TagName="LookupByMemberID" Src="Controls/LookupByMemberID20.ascx" %>
<%@ Register TagPrefix="uc1" TagName="LookupByUsername" Src="Controls/LookupByUsername20.ascx" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>

<input type="hidden" name="IdentifierType" />
<input type="hidden" name="DrinkingHabits" value="-2147483647" />
<input type="hidden" name="EducationLevel" value="-2147483647" />
<input type="hidden" name="Ethnicity" value="-2147483647" />
<input type="hidden" name="HasPhotoFlag" value="1" />
<input type="hidden" name="LanguageMask" value="-2147483647" />
<input type="hidden" name="LastCountryRegionID" />
<input type="hidden" name="MaritalStatus" value="-2147483647" />
<input type="hidden" name="MaxHeight" value="-2147483647" />
<input type="hidden" name="MinHeight" value="-2147483647" />
<input type="hidden" name="Religion" value="-2147483647" />
<input type="hidden" name="SearchOrderBy" value="1" />
<input type="hidden" name="SmokingHabits" value="-2147483647" />

<mn:Title runat="server" id="ttlLookUpProfile" ResourceConstant="NAV_SUB_LOOK_UP_PROFILE"/>
	
<div id="lookup-profile" class="clearfix">
	<uc1:lookupbymemberid id="LookupByMemberID" runat="server" />
	<uc1:lookupbyusername id="LookupByUsername" runat="server" />
</div>

<div class="border-gen tips">
    <h2><mn:txt id="txtNeedHelpFindingMember" runat="server" resourceconstant="NEED_HELP_FINDING_A_MEMBER__520198" /></h2>
    <p><mn:txt id="Txt1" runat="server" ResourceConstant="TXT_CHECK_YOUR_LISTS"></mn:txt></p>
    <ul>
   <li> <a href="/Applications/HotList/View.aspx?CategoryID=-9">
	    <mn:txt id="Txt2" runat="server" ResourceConstant="MEMBERS_YOU_VE_VIEWED_519430"></mn:txt></a>
    </li>
    <li>
        <a href="/Applications/HotList/View.aspx?CategoryID=0">
	    <mn:txt id="Txt3" runat="server" ResourceConstant="TXT_MEMBERS_YOUVE_LISTED_AS_A_FAVORITE"></mn:txt></a>
	 </li>
	 <li>
        <a href="/Applications/HotList/View.aspx?CategoryID=-6">
	    <mn:txt id="Txt4" runat="server" ResourceConstant="TXT_MEMBERS_YOUVE_FLIRTED_WITH"></mn:txt></a>
	 </li>
	 <li>
        <a href="/Applications/HotList/View.aspx?CategoryID=-7">
	    <mn:txt id="Txt5" runat="server" ResourceConstant="MEMBERS_YOU_VE_EMAILED_520200"></mn:txt></a>
	 </li>
	 <li>
        <a href="/Applications/HotList/View.aspx?CategoryID=-10">
	    <mn:txt id="Txt7" runat="server" ResourceConstant="MATCHES_SENT_TO_YOU_VIA_EMAIL"></mn:txt></a>
	 </li>
</ul>
</div>

