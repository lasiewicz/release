﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="LookupByMemberID20.ascx.cs" Inherits="Matchnet.Web.Applications.LookupProfile.Controls.LookupByMemberID20" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<div id="member-id">
	<h3><mn:txt id="Txt1" runat="server" ResourceConstant="TXT_BY_MEMBER_NUMBER" expandImageTokens="True" /></h3>
	<p><mn:txt id="Txt6" runat="server" ResourceConstant="TXT_ENTER_MEMBER_NUMBER"></mn:txt></p>
	
	<input type="text" runat="server" id="LookupMemberID" name="LookupMemberID" />
	<mn2:frameworkButton runat="server" id="btnSearchByMemberNumber"  ResourceConstant="BTN_SEE_PROFILE" class="btn primary"/>
	
	<mn:MultiValidator id="LookupMemberIDValidator" tabIndex="-1" runat="server" MinimumLength="1" MaximumLength="25" RequiredType="IntegerType" ControlToValidate="LookupMemberID" FieldNameResourceConstant="MEMBER_ID" IsRequired="True" Display="Dynamic" />
</div>