﻿using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using Matchnet.Lib;
using Matchnet.Lib.Exceptions;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Ui;
using Matchnet.Member.ServiceAdapters;
namespace Matchnet.Web.Applications.LookupProfile.Controls
{
    public partial class LookupByUsername20 : FrameworkControl
    {
        private void Page_Load(object sender, System.EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                {
                    string userName = this.g.Member.GetUserName(g.Brand);

                    if (userName != "")
                    {
                        this.LookupUserName.Value = userName;
                    }

                    LookupUserName.Attributes.Add("onkeydown", "setAutoSubmitButton(" + btnLookupByUserName.ClientID + ",event)");

                    //Disable the UserName validator on load and when the UserName text box loses focus
                    LookupUserNameValidator.Enabled = false;
                    LookupUserName.Attributes.Add("onblur", LookupUserNameValidator.ClientID + ".enabled=false; ");
                    //Only enable it if the Go button next to the UserName box is clicked:
                    btnLookupByUserName.Attributes.Add("onclick", LookupUserNameValidator.ClientID + ".enabled=true; ");
                }
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        protected void btnLookupByUserName_Click(object sender, EventArgs e)
        {
            try
            {
                string userName = this.LookupUserName.Value.Trim();

                if (userName != String.Empty && userName.IndexOf(".") < 0 && userName.IndexOf("/") < 0)
                {
                    int memberID = MemberSA.Instance.GetMemberID(userName, g.Brand.Site.Community.CommunityID);

                    if (memberID != Constants.NULL_INT && memberID != 0 && memberID != int.MinValue)
                    {
                        Matchnet.Member.ServiceAdapters.Member member = null;
                        member = MemberSA.Instance.GetMember(memberID, MemberLoadFlags.None);

                        if (member != null)
                        {
                            // TT#15006, Somehow we were getting back a valid member that contained a null Username
                            // adding this check keeps the null reference from occuring when doing member.GetUserName(_g.Brand).ToLower()
                            if (member.GetUserName(_g.Brand) != null && member.GetUserName(_g.Brand).ToLower() == userName.ToLower())
                            {
                                bool isMemberOfCurrentCommunity = false;
                                int[] communityIDList = member.GetCommunityIDList();

                                // communityIDList is the only other possiblity for a null reference exception besides member.Usernamne
                                // this checks it just in case.
                                if (communityIDList != null)
                                {
                                    foreach (int communityID in communityIDList)
                                    {
                                        if (communityID == g.Brand.Site.Community.CommunityID)
                                        {
                                            isMemberOfCurrentCommunity = true;
                                            break;
                                        }
                                    }
                                }
                                else
                                {
                                    // Add the notification message and then throw a matchnet exception so we can log in sparkmon which
                                    // MemberID returned a null communityIDList
                                    g.Notification.AddMessage("SELECTED_MEMER_DOES_NOT_EXIST_IN_THIS_DOMAIN");
                                    throw new MatchnetException("GetCommunityIDList() returned NULL for MemberID: " + memberID, "LookupByUsername.btnLookupByUserName_Click");
                                }

                                // profile blocking feature check
                                bool isProfileBlockingEnabled = Convert.ToBoolean(Configuration.ServiceAdapters.RuntimeSettings.GetSetting("PROFILE_BLOCK_ENABLED", g.Brand.Site.Community.CommunityID));
                                bool isProfileBlocked = false;
                                if (isProfileBlockingEnabled)
                                {
                                    isProfileBlocked = g.List.IsHotListed(Matchnet.List.ValueObjects.HotListCategory.ExcludeListInternal, g.Brand.Site.Community.CommunityID, member.MemberID);
                                }

                                if (isMemberOfCurrentCommunity && !isProfileBlocked)
                                {
                                    g.Transfer("/Applications/MemberProfile/ViewProfile.aspx?MemberID=" + memberID.ToString()
                                        + "&EntryPoint=" + (int)BreadCrumbHelper.EntryPoint.LookUpMember);
                                }
                                else
                                {	// member isn't part of the current community
                                    g.Notification.AddMessage("SELECTED_MEMER_DOES_NOT_EXIST_IN_THIS_DOMAIN");
                                }
                            }
                            else
                            {
                                // Add the notification message and then throw a matchnet exception so we can log in sparkmon which
                                // MemberID was returning a member with a null username.
                                g.Notification.AddMessage("SELECTED_MEMER_DOES_NOT_EXIST_IN_THIS_DOMAIN");
                                if (member.GetUserName(_g.Brand) == null)
                                {
                                    throw new MatchnetException("Username was NULL in the member object returned for MemberID: " + memberID, "LookupByUsername.btnLookupByUserName_Click");
                                }
                            }
                        }
                        else
                        {	// member was null or username didn't match search string
                            g.Notification.AddMessage("SELECTED_MEMER_DOES_NOT_EXIST_IN_THIS_DOMAIN");
                        }
                    }
                    else
                    {
                        if (Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("USERNAME_APPROVAL_FLAG", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID).ToLower() == "true")
                        {
                            int memID;
                            bool memberFound = false;
                            if (int.TryParse(userName, out memID))
                            {
                                Matchnet.Member.ServiceAdapters.Member member = null;

                                member = MemberSA.Instance.GetMember(memID, MemberLoadFlags.IngoreSACache);

                                if (member != null && member.MemberID == memID && member.GetUserName(_g.Brand) != null && member.EmailAddress != null &&
                                    member.GetUserName(_g.Brand) == userName)
                                {
                                    bool isMemberOfCurrentCommunity = false;
                                    int[] communityIDList = member.GetCommunityIDList();
                                    foreach (int communityID in communityIDList)
                                    {
                                        if (communityID == g.Brand.Site.Community.CommunityID)
                                        {
                                            isMemberOfCurrentCommunity = true;
                                            break;
                                        }
                                    }
                                    if (isMemberOfCurrentCommunity)
                                    {
                                        memberFound = true;
                                        g.Transfer("/Applications/MemberProfile/ViewProfile.aspx?MemberID="
                                            + memID
                                            + "&EntryPoint=" + (int)BreadCrumbHelper.EntryPoint.LookUpMember);
                                    }
                                }
                            }
                            if (!memberFound)
                            {
                                // member id was null or zero, username doesn't exist
                                g.Notification.AddMessage("SELECTED_MEMER_DOES_NOT_EXIST_IN_THIS_DOMAIN");
                            }
                        }
                        else
                        {
                            // member id was null or zero, username doesn't exist
                            g.Notification.AddMessage("SELECTED_MEMER_DOES_NOT_EXIST_IN_THIS_DOMAIN");
                        }
                    }
                }
                else
                {	// bad search string.  
                    g.Notification.AddMessage("SELECTED_MEMER_DOES_NOT_EXIST_IN_THIS_DOMAIN");
                }
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
            this.btnLookupByUserName.Click += new EventHandler(this.btnLookupByUserName_Click);

        }
        #endregion
    }
}