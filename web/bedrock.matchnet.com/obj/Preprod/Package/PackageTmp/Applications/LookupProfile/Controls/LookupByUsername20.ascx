﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="LookupByUsername20.ascx.cs" Inherits="Matchnet.Web.Applications.LookupProfile.Controls.LookupByUsername20" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<div id="member-username">
    <h3><mn:txt id="Txt1" runat="server" ResourceConstant="TXT_BY_USERNAME" expandImageTokens="True" /></h3>
    <p><mn:txt id="Txt6" runat="server" ResourceConstant="TXT_ENTER_USERNAME" expandImageTokens="True" /></p>
    <input type="text" id="LookupUserName" runat="server" name="LookupUserName" />&nbsp; 
	<mn2:FrameworkButton ID="btnLookupByUserName" Runat="server" class="btn primary" ResourceConstant="BTN_SEE_PROFILE" />
	<br />
	<mn:MultiValidator id="LookupUserNameValidator" tabIndex="-1" runat="server" MinimumLength="1" MaximumLength="25" RequiredType="AlphaNumType" ControlToValidate="LookupUserName" FieldNameResourceConstant="USERNAME" IsRequired="True" Display="Dynamic" />
</div>