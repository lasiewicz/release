﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Lib;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Ui;
using Matchnet.Web.Framework.Ui.BasicElements;

using Matchnet.Member.ServiceAdapters;
using Matchnet.List.ServiceAdapters;
using Matchnet.List.ValueObjects;
using Matchnet.Session.ValueObjects;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Text;
using Matchnet.UserNotifications.ServiceAdapters;
using Matchnet.UserNotifications.ValueObjects;
using Matchnet.Web.Applications.UserNotifications;

namespace Matchnet.Web.Applications.HotList
{
    public partial class View20 : FrameworkControl, IHotListView
    {

        //protected string ChangeVoteMessage;
        //protected Matchnet.Web.Framework.Image icon_galleryView;
        //protected DropDownList ddlOptions;
        //protected System.Web.UI.WebControls.Literal ltlPageTitle;
        //protected System.Web.UI.WebControls.HyperLink lnkViewType;
        //protected Matchnet.Web.Framework.Image icon_ViewType;
        //protected Matchnet.Web.Framework.Ui.BasicElements.ResultList20 resultHotList;
        //protected Matchnet.Web.Framework.Txt txtEditCat;
        //protected Matchnet.Web.Framework.Txt txtViewAsHL;
        //protected HtmlInputHidden GalleryView;
        //protected Label lblListNavigationTop;
        //protected Label lblListNavigationBottom;

        private int _moveCount = 0;

        HotListDirection _listDirection = HotListDirection.None;
        HotListType _listType;
        List<Int32> _categoriesList;

        string categoryName = String.Empty;
        string _view = "";
        HotListViewHandler _hotlistViewHandler = null;

        private bool ynmlists = false;
        #region IHotListView  implementation
        public IResultList ResultHotList
        {
            get { return resultHotList; }
        }

        public int MoveCount
        {
            get { return _moveCount; }
            set { _moveCount = value; }
        }
        #endregion


        private void Page_Init(object sender, EventArgs e)
        {
            try
            {
                //	If the user just arrived here and has the right parameters, then add a user to their favorites
                //	list.  This is here to support non-logged in users who try to add someone to their
                //	favorites and are then sent through the registration process, ending up on this page.  See
                //	The Add2List control in Framework/Ui/BasicElements.
                if (!this.IsPostBack)
                {
                    if ("add".Equals(Request["a"]))
                    {
                        int toMemberID = Matchnet.Conversion.CInt(Request["ToMemberID"]);
                        if (toMemberID > 0)
                        {
                            Matchnet.List.ValueObjects.ListSaveResult result = ListSA.Instance.AddListMember(HotListCategory.Default,
                                g.Brand.Site.Community.CommunityID,
                                g.Brand.Site.SiteID,
                                g.Member.MemberID,
                                toMemberID,
                                string.Empty,
                                Constants.NULL_INT,
                                !((g.Member.GetAttributeInt(g.Brand, "HideMask", 0) & (Int32)WebConstants.AttributeOptionHideMask.HideHotLists) == (Int32)WebConstants.AttributeOptionHideMask.HideHotLists),
                                false);

                            if (UserNotificationFactory.IsUserNotificationsEnabled(g))
                            {
                                UserNotificationParams unParams = UserNotificationParams.GetParamsObject(toMemberID, g.Brand.Site.SiteID, g.Brand.Site.Community.CommunityID);
                                UserNotification notification = UserNotificationFactory.Instance.GetUserNotification(new AddedYouAsFavoriteUserNotification(), toMemberID, g.Member.MemberID, g);
                                if (notification.IsActivated())
                                {
                                    UserNotificationsServiceSA.Instance.AddUserNotification(unParams, notification.CreateViewObject());
                                }
                            }
	
                            if ("1".Equals(Request["RedirectToProfile"]))
                            {
                                string currentQueryString = Request.QueryString.ToString();
                                g.Transfer("/Applications/MemberProfile/ViewProfile.aspx?rc=SUCCESSFUL_HOTLIST_ADD&MemberID=" + toMemberID);
                            }
                        }
                    }
                }
                
                BindAndSetControls();


                // Wire up the List Navigation.
                g.ListNavigationTop = lblListNavigationTop;
                g.ListNavigationBottom = lblListNavigationBottom;
            }
            catch (Exception ex)
            {
                _g.ProcessException(ex);
            }
        }

        #region Bind and set the category id dropdown
        private void BindAndSetControls()
        {
            CustomCategoryCollection customCategories = null;

            List<Tab> categoriesList = new List<Tab>();
            string resourceConstant = "";
            hotListTabBar.HrefBase = "/Applications/HotList/View.aspx?CategoryID";
            hotListTabBar.HrefFormat = "{0}={1}";
            hotListTabBar.ClassSelected = "selected";
            hotListTabBar.MaxTextLen = 20;
            hotListTabBar.ResourceControl = this;
            hotListTabBar.SelectedTabID = (int)ResultHotList.HotListCategory;
            hotListTabBar.Clear();
            int communityid = g.Brand.Site.Community.CommunityID;
            int siteid = g.Brand.Site.SiteID;
            // get the name of a standard hotlist category
            if ((int)ResultHotList.HotListCategory <= 0)
            {

                if (ResultHotList.HotListCategory == HotListCategory.MutualYes)
                {
                    g.Member.SetAttributeInt(g.Brand, "HasNewMutualYes", 0);
                    MemberSA.Instance.SaveMember(g.Member);
                    categoryName = g.GetResource("TXT_YOU_EACH_SAID_YES", this);
                }
                else
                {
                    categoryName = ListLib.GetStandardCategoryName(ResultHotList.HotListCategory, g, this);
                }



                ListLib.GetHotListTypeAndDirection(resultHotList.HotListCategory, out _listType, out _listDirection);
                int count = 0;

                // HACK: They want to move the IgnoreList from OnYourList to OnTheirList. Instead of making the changes in the DB, we are overriding it here.
                if (resultHotList.HotListCategory == HotListCategory.IgnoreList)
                    _listDirection = HotListDirection.OnTheirList;

                if (_listDirection == HotListDirection.OnTheirList)
                {
                    resourceConstant = "LISTDIRECTION_WHO_HAVE_YOU";


                    count = g.List.GetCount(HotListCategory.WhoViewedYourProfile, communityid, siteid);
                    hotListTabBar.AddTab((int)HotListCategory.WhoViewedYourProfile, 1, count, false, "", "", ListLib.GetStandardCategoryName(HotListCategory.WhoViewedYourProfile, g, this));

                    count = g.List.GetCount(HotListCategory.WhoTeasedYou, communityid, siteid);
                    hotListTabBar.AddTab((int)HotListCategory.WhoTeasedYou, 2, count, false, "", "", ListLib.GetStandardCategoryName(HotListCategory.WhoTeasedYou, g, this));

                    count = g.List.GetCount(HotListCategory.WhoEmailedYou, communityid, siteid);
                    hotListTabBar.AddTab((int)HotListCategory.WhoEmailedYou, 3, count, false, "", "", ListLib.GetStandardCategoryName(HotListCategory.WhoEmailedYou, g, this));

                    count = g.List.GetCount(HotListCategory.WhoAddedYouToTheirFavorites, communityid, siteid);
                    hotListTabBar.AddTab((int)HotListCategory.WhoAddedYouToTheirFavorites, 4, count, false, "", "", ListLib.GetStandardCategoryName(HotListCategory.WhoAddedYouToTheirFavorites, g, this));

                    if (g.EcardsEnabled)
                    {

                        count = g.List.GetCount(HotListCategory.WhoSentYouECards, communityid, siteid);
                        hotListTabBar.AddTab((int)HotListCategory.WhoSentYouECards, 5, count, false, "", "LISTCATEGORY_WHOS_SENT_YOU_ECARDS", ListLib.GetStandardCategoryName(HotListCategory.WhoSentYouECards, g, this));

                    }


                    count = g.List.GetCount(HotListCategory.WhoIMedYou, communityid, siteid);
                    hotListTabBar.AddTab((int)HotListCategory.WhoIMedYou, 6, count, false, "", "", ListLib.GetStandardCategoryName(HotListCategory.WhoIMedYou, g, this));

                    if (Matchnet.Web.Applications.Email.VIPMailUtils.IsVIPEnabledSite(g.Brand))
                    {
                        count = g.List.GetCount(HotListCategory.MembersWhoSentYouAllAccessEmail, communityid, siteid);
                        hotListTabBar.AddTab((int)HotListCategory.MembersWhoSentYouAllAccessEmail, 7, count, false, "", "", ListLib.GetStandardCategoryName(HotListCategory.MembersWhoSentYouAllAccessEmail, g, this),80);
                    }

                    // MatchMeter (jmeter)
                    int mmsett = Int32.Parse(RuntimeSettings.GetSetting("MatchTest_App_Settings", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID));
                    int mmenumval = (Int32)WebConstants.MatchMeterFlags.EnableApp;
                    if ((mmsett & mmenumval) == mmenumval)
                    {
                        count = g.List.GetCount(HotListCategory.WhoJmeteredYou, communityid, siteid);
                        hotListTabBar.AddTab((int)HotListCategory.WhoJmeteredYou, 7, count, false, "", "LISTCATEGORY_WHOS_JMETERED_YOU", ListLib.GetStandardCategoryName(HotListCategory.WhoJmeteredYou, g, this), 24);
                    }

                    if (RuntimeSettings.GetSetting("ENABLE_OMNIDATE", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID) == "true")
                    {
                        count = g.List.GetCount(HotListCategory.WhoOmnidatedYou, communityid, siteid);
                        hotListTabBar.AddTab((int)HotListCategory.WhoOmnidatedYou, 7, count, false, "", "LISTCATEGORY_WHO_OMNIDATED_YOU", ListLib.GetStandardCategoryName(HotListCategory.WhoOmnidatedYou, g, this), 24);
                    }

                    count = g.List.GetCount(HotListCategory.IgnoreList, communityid, siteid);
                    hotListTabBar.AddTab((int)HotListCategory.IgnoreList, 8, count, false, "", "", ListLib.GetStandardCategoryName(HotListCategory.IgnoreList, g, this), 25);

                    if (Convert.ToBoolean(RuntimeSettings.GetSetting("PROFILE_BLOCK_UI_ENABLED", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID)))
                    {
                        count = g.List.GetCount(HotListCategory.ExcludeFromList, communityid, siteid);
                        hotListTabBar.AddTab((int)HotListCategory.ExcludeFromList, 9, count, false, "", "", ListLib.GetStandardCategoryName(HotListCategory.ExcludeFromList, g, this), 25);
                    }
                }
                else if (_listDirection == HotListDirection.OnYourList)
                {
                    ynmlists = (resultHotList.HotListCategory == HotListCategory.MutualYes) ||
                               (resultHotList.HotListCategory == HotListCategory.MembersClickedYes) ||
                               (resultHotList.HotListCategory == HotListCategory.MembersClickedNo) ||
                               (resultHotList.HotListCategory == HotListCategory.MembersClickedMaybe);

                    if (resultHotList.HotListCategory == HotListCategory.YourMatches)
                    {
                        resourceConstant = "LISTCATEGORY_YOUR_MATCHES";

                        count = g.List.GetCount(HotListCategory.YourMatches, communityid, siteid);
                        hotListTabBar.AddTab((int)HotListCategory.YourMatches, 1, count, false, "", resourceConstant, "");

                    }
                    else if (ynmlists)
                    {
                       
                        resourceConstant = "LISTCATEGORY_YOU_EACH_SAID_YES";

                        count = g.List.GetCount(HotListCategory.MutualYes, communityid, siteid);
                        hotListTabBar.AddTab((int)HotListCategory.MutualYes, 1, count, false, "", resourceConstant, "");

                        if (SettingsManager.GetSettingBool(SettingConstants.ENABLE_SECRET_ADMIRER_GAME, g.Brand))
                        {
                            resourceConstant = "LISTCATEGORY_YOU_SAID_YES";

                            count = g.List.GetCount(HotListCategory.MembersClickedYes, communityid, siteid);
                            hotListTabBar.AddTab((int)HotListCategory.MembersClickedYes, 2, count, false, "", resourceConstant, "");

                            resourceConstant = "LISTCATEGORY_YOU_SAID_NO";

                            count = g.List.GetCount(HotListCategory.MembersClickedNo, communityid, siteid);
                            hotListTabBar.AddTab((int)HotListCategory.MembersClickedNo, 3, count, false, "", resourceConstant, "");

                            resourceConstant = "LISTCATEGORY_YOU_SAID_MAYBE";

                            count = g.List.GetCount(HotListCategory.MembersClickedMaybe, communityid, siteid);
                            hotListTabBar.AddTab((int)HotListCategory.MembersClickedMaybe, 4, count, false, "", resourceConstant, "");
                            resourceConstant = "LISTCATEGORY_YOU_EACH_SAID_YES_TITLE";
                        }
                    }
                    else
                    {
                        resourceConstant = "LISTDIRECTION_YOU_HAVE";


                        count = g.List.GetCount(HotListCategory.MembersYouViewed, communityid, siteid);
                        hotListTabBar.AddTab((int)HotListCategory.MembersYouViewed, 1, count, false, "", "", ListLib.GetStandardCategoryName(HotListCategory.MembersYouViewed, g, this));

                        count = g.List.GetCount(HotListCategory.MembersYouTeased, communityid, siteid);
                        hotListTabBar.AddTab((int)HotListCategory.MembersYouTeased, 2, count, false, "", "", ListLib.GetStandardCategoryName(HotListCategory.MembersYouTeased, g, this));

                        count = g.List.GetCount(HotListCategory.MembersYouEmailed, communityid, siteid);
                        hotListTabBar.AddTab((int)HotListCategory.MembersYouEmailed, 3, count, false, "", "", ListLib.GetStandardCategoryName(HotListCategory.MembersYouEmailed, g, this));

                        count = g.List.GetCount(HotListCategory.Default, communityid, siteid);
                        hotListTabBar.AddTab((int)HotListCategory.Default, 4, count, false, "", "", ListLib.GetStandardCategoryName(HotListCategory.Default, g, this));
                        if (g.EcardsEnabled)
                        {
                            count = g.List.GetCount(HotListCategory.MembersYouSentECards, communityid, siteid);
                            hotListTabBar.AddTab((int)HotListCategory.MembersYouSentECards, 5, count, false, "", "LISTCATEGORY_MEMBERS_YOUVE_SENT_ECARDS", ListLib.GetStandardCategoryName(HotListCategory.MembersYouSentECards, g, this));
                        }
                        count = g.List.GetCount(HotListCategory.MembersYouIMed, communityid, siteid);
                        hotListTabBar.AddTab((int)HotListCategory.MembersYouIMed, 6, count, false, "", "", ListLib.GetStandardCategoryName(HotListCategory.MembersYouIMed, g, this));

                        // All Access
                        if (Matchnet.Web.Applications.Email.VIPMailUtils.IsVIPEnabledSite(g.Brand))
                        {
                            count = g.List.GetCount(HotListCategory.MembersYouSentAllAccessEmail, communityid, siteid);
                            hotListTabBar.AddTab((int)HotListCategory.MembersYouSentAllAccessEmail, 7, count, false, "", "", ListLib.GetStandardCategoryName(HotListCategory.MembersYouSentAllAccessEmail, g, this), 80);
                        }

                        // MatchMeter (jmeter)
                        int mmsett = Int32.Parse(RuntimeSettings.GetSetting("MatchTest_App_Settings", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID));
                        int mmenumval = (Int32)WebConstants.MatchMeterFlags.EnableApp;
                        if ((mmsett & mmenumval) == mmenumval)
                        {
                            count = g.List.GetCount(HotListCategory.MemberYouJmetered, communityid, siteid);
                            hotListTabBar.AddTab((int)HotListCategory.MemberYouJmetered, 7, count, false, "", "LISTCATEGORY_MEMBERS_YOU_JMETERED", ListLib.GetStandardCategoryName(HotListCategory.MemberYouJmetered, g, this), 26);
                        }

                        if (RuntimeSettings.GetSetting("ENABLE_OMNIDATE", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID) == "true")
                        {
                            count = g.List.GetCount(HotListCategory.MembersYouOmnidated, communityid, siteid);
                            hotListTabBar.AddTab((int)HotListCategory.MembersYouOmnidated, 7, count, false, "", "LISTCATEGORY_MEMBERS_YOUVE_OMNIDATED", ListLib.GetStandardCategoryName(HotListCategory.MembersYouOmnidated, g, this), 26);

                        }

                        if (Convert.ToBoolean(RuntimeSettings.GetSetting("PROFILE_BLOCK_UI_ENABLED", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID)))
                        {
                            count = g.List.GetCount(HotListCategory.ExcludeList, communityid, siteid);
                            hotListTabBar.AddTab((int)HotListCategory.ExcludeList, 8, count, false, "", "", ListLib.GetStandardCategoryName(HotListCategory.ExcludeList, g, this), 25);
                        }

                        int categoryID = Matchnet.Conversion.CInt(Request["CategoryID"]);
                        if (HotListCategory.Default.Equals((HotListCategory)categoryID))
                        {
                            phMoreMembers.Visible = true;
                            //phSearchLinks.Visible = true;
                            //ListItem defaultOpt = new ListItem(g.GetResource("OPT_DEFAULT", this), "");
                            //ListItem yourMatches = new ListItem(g.GetResource("OPT_YOUR_MATCHES", this), "/Applications/Search/SearchResults.aspx");
                            //ListItem quickSearch = new ListItem(g.GetResource("OPT_QUICK_SEARCH", this), "/Applications/QuickSearch/QuickSearch.aspx");
                            //ListItem membersOnline = new ListItem(g.GetResource("OPT_MEMBERS_ONLINE", this), "/Applications/MembersOnline/MembersOnline.aspx");

                            //if (ddlSearchLinks.Items.Count == 0)
                            //{
                            //    ddlSearchLinks.Items.Add(defaultOpt);
                            //    ddlSearchLinks.Items.Add(yourMatches);
                            //    ddlSearchLinks.Items.Add(quickSearch);
                            //    ddlSearchLinks.Items.Add(membersOnline);
                            //}
                        }
                        else
                        {
                            //phSearchLinks.Visible = false;
                            phMoreMembers.Visible = false;
                        }
                       
                        

                    }
                }

            }
            else
            {
                // resolve the name of the custom hotlist category
                customCategories = ListSA.Instance.GetCustomListCategories(g.Member.MemberID,
                    g.Brand.Site.Community.CommunityID);

                if (customCategories != null && customCategories.Count > 0)
                {
                    foreach (CustomCategory currentCategory in customCategories)
                    {
                        if (currentCategory.CategoryID == Matchnet.Conversion.CInt(Request["CategoryID"]))
                        {
                            categoryName = currentCategory.Description;
                            //break;
                        }


                        int count = g.List.GetCount((HotListCategory)currentCategory.CategoryID, communityid, siteid);
                        hotListTabBar.AddTab((int)currentCategory.CategoryID, 6, count, false, "", "", currentCategory.Description);
                    }

                }

                resourceConstant = "LISTCATEGORY_CUSTOM";
            }

            ltlPageTitle.Text = FrameworkGlobals.Ellipsis(g.GetResource(resourceConstant, this), 33);
        }
        # endregion

        private void RenderPage()
        {
            try
            {


                //g.BreadCrumbTrailHeader.SetLastElementAndRebind(FrameworkGlobals.Ellipsis(categoryName, 35), String.Empty);
                //g.BreadCrumbTrailFooter.SetLastElementAndRebind(FrameworkGlobals.Ellipsis(categoryName, 35), String.Empty);

                g.BreadCrumbTrailHeader.SetTwoLinkCrumb(categoryName, string.Empty);
                g.BreadCrumbTrailFooter.SetTwoLinkCrumb(categoryName, string.Empty);


                
                

                RenderAnalytics();

                idResultsViewType.GetSearchResultViewMode();
                ResultHotList.GalleryView = idResultsViewType.GalleryViewFlag; ;

                //	Set up the breadcrumbs
                int categoryID = Matchnet.Conversion.CInt(Request["CategoryID"]);

                if (categoryID != Constants.NULL_INT)
                {
                    ResultHotList.HotListCategory = (HotListCategory)categoryID;

                    setToolbarNotificationMask(g.Member.MemberID, (HotListCategory)categoryID);
                }
             
            }
            catch (Exception ex)
            {
                _g.ProcessException(ex);
            }
        }


        #region toolbarnotification
        private void setToolbarNotificationMask(int memberid, HotListCategory categoryid)
        {
            try
            {
                Constants.NotificationType notificationtype = Constants.NotificationType.NoNewNotification;

                if ((HotListCategory)categoryid == HotListCategory.WhoAddedYouToTheirFavorites)
                    notificationtype = Constants.NotificationType.HotList;
                else if ((HotListCategory)categoryid == HotListCategory.WhoViewedYourProfile)
                    notificationtype = Constants.NotificationType.ProfileViews;
                int brandid = g.Brand.BrandID;
                int communityid = g.Brand.Site.Community.CommunityID;
                int siteid = g.Brand.Site.SiteID;


                Matchnet.Member.ServiceAdapters.Member member = MemberSA.Instance.GetMember(memberid, MemberLoadFlags.None);
                int notificationMask = member.GetAttributeInt(communityid, siteid, brandid, "HasNewNotificationMask", 0);
                if ((notificationMask & (int)notificationtype) == (int)notificationtype)
                {
                    notificationMask = notificationMask - (int)notificationtype;
                    member.SetAttributeInt(communityid, siteid, brandid, "HasNewNotificationMask", notificationMask);
                    Member.ServiceAdapters.MemberSA.Instance.SaveMember(member);
                }

                if (HotList.HotListViewHandler.IsNotificationsEnabled(g))
                {
                    // If redirecting for subscription required page then do not clear notification
                    bool needsRedirect = ((categoryid == HotListCategory.WhoTeasedYou || categoryid == HotListCategory.WhoEmailedYou || categoryid == HotListCategory.WhoIMedYou)
                        && g.Brand.Site.Community.CommunityID != (int)WebConstants.COMMUNITY_ID.Cupid && g.Brand.Site.SiteID != (int)WebConstants.SITE_ID.JDateCoIL)
                        && !g.ValidatePageView();

                    if (!needsRedirect)
                    {
                        //if attribute is not set for site/brand then exception is thrown
                        try
                        {
                            string lastVisitAttributeId = "LastHotListPageVisit-" + categoryid.ToString();
                            member.SetAttributeDate(communityid, siteid, brandid, lastVisitAttributeId, DateTime.Now);
                            ////fix for jira ticket QAT-4674 
                            //if (HotListCategory.WhoEmailedYou == categoryid)
                            //{
                            //    member.SetAttributeDate(communityid, siteid, brandid, "LastHotListPageVisit-" + HotListCategory.WhoTeasedYou.ToString(), DateTime.Now);
                            //    member.SetAttributeDate(communityid, siteid, brandid, "LastHotListPageVisit-" + HotListCategory.WhoSentYouECards.ToString(), DateTime.Now);
                            //}
                            Member.ServiceAdapters.MemberSA.Instance.SaveMember(member);
                        }
                        catch (Exception exc) { exc = null; }
                    }
                }
            }
            catch (Exception ex)
            {
                ex = null;
            }
        }

        #endregion
        # region Page load and setup
        private void Page_Load(object sender, System.EventArgs e)
        {
            _hotlistViewHandler = new HotListViewHandler(g, this);
            RenderPage();
        }
        # endregion

        protected override void OnPreRender(System.EventArgs e)
        {
            _hotlistViewHandler.HandleAction();
        }

        #region Render site analytics
        private void RenderAnalytics()
        {
            int analyticsID = ConstantsTemp.PAGE_HOTLIST_DEFAULT - (int)ResultHotList.HotListCategory;
            g.Analytics.SetAttribute("p", analyticsID.ToString());

            int categoryID = Matchnet.Conversion.CInt(Request["CategoryID"]);
            string pageName = Enum.GetName(typeof(Matchnet.List.ValueObjects.HotListCategory), categoryID);
            // pageName = "Hot Lists - " + pageName + " " +  _view;
            // g.Session.Add(WebConstants.ACTION_CALL_PAGE, pageName, Matchnet.Session.ValueObjects.SessionPropertyLifetime.Temporary);


        }
        # endregion

        //private void ddlSearchLinks_OnSelect(object sender, System.EventArgs e)
        //{
        //    string url = ddlSearchLinks.SelectedValue;
        //    if (!string.IsNullOrEmpty(url))
        //    {
        //        Response.Redirect(url);
        //    }
        //}

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
           // this.ddlSearchLinks.SelectedIndexChanged += new System.EventHandler(this.ddlSearchLinks_OnSelect);
            this.Load += new System.EventHandler(this.Page_Load);
            this.Init += new System.EventHandler(this.Page_Init);
        }
        #endregion



    }
}
