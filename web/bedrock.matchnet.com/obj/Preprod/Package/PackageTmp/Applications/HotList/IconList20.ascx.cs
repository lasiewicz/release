﻿using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Web.Framework;
namespace Matchnet.Web.Applications.HotList
{
    public partial class IconList20 : FrameworkControl
    {
        private void Page_Load(object sender, System.EventArgs e)
        {
            try
            {
                g.BreadCrumbTrailHeader.SetTwoLinkCrumb(g.GetResource("TXT_ICON_LIST", this), String.Empty);
                g.BreadCrumbTrailFooter.SetTwoLinkCrumb(g.GetResource("TXT_ICON_LIST", this), String.Empty);

                //tt # 20151 Martin Hristoforov 
                if (g.EcardsEnabled)
                {
                    pnlEcards.Visible = true;
                }

                if (RuntimeSettings.GetSetting("ENABLE_OMNIDATE", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID) == "true")
                {
                    plcOmnidate.Visible = true;
                }

                plcYNM.Visible = g.IsYNMEnabled;
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);

        }
        #endregion
    }
}