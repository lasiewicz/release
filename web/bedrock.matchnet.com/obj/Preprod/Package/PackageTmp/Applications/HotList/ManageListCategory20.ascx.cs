﻿#region System References
using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text.RegularExpressions;
#endregion

#region Matchnet Web App References
using Matchnet.Lib.Util;
using Matchnet.Web.Applications.HotList;
using Matchnet.Web.Framework;
#endregion

#region Matchnet Service References
using Matchnet.List.ServiceAdapters;
using Matchnet.List.ValueObjects;
#endregion

namespace Matchnet.Web.Applications.HotList
{
    public partial class ManageListCategory20 : FrameworkControl
    {
      

        private int _ItemCount = 0;

        private enum EditMode { Normal, Rename, Delete };

        private void Page_Init(object sender, EventArgs e)
        {
            try
            {
                SetActionIDs();

                btnSubmit.Value = g.GetResource("CREATE", this);

                CustomCategoryCollection customLists = List.ServiceAdapters.ListSA.Instance.GetCustomListCategories(g.Session.GetInt(WebConstants.SESSION_PROPERTY_NAME_MEMBERID), g.Brand.Site.Community.CommunityID);

                CategoryRepeater.DataSource = customLists;
                CategoryRepeater.DataBind();

                DetermineShowCreateNew();
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        private void DetermineShowCreateNew()
        {
            CustomCategoryCollection customLists = List.ServiceAdapters.ListSA.Instance.GetCustomListCategories(g.Session.GetInt(WebConstants.SESSION_PROPERTY_NAME_MEMBERID), g.Brand.Site.Community.CommunityID);
            if (customLists.Count >= 5)
            {
                phCreateNew.Visible = false;
                phMaximumLists.Visible = true;
            }
            else
            {
                phCreateNew.Visible = true;
                phMaximumLists.Visible = false;
            }
        }

        private void SetActionIDs()
        {
            string sDeleteID = Request["DeleteID"];
            DeleteID = Matchnet.Conversion.CInt(sDeleteID);

            string sRenameID = Request["RenameID"];
            RenameID = Matchnet.Conversion.CInt(sRenameID);
        }

        private int _DeleteID;
        protected int DeleteID
        {
            get { return (_DeleteID); }
            set { _DeleteID = value; }
        }

        private int _RenameID;
        protected int RenameID
        {
            get { return (_RenameID); }
            set { _RenameID = value; }
        }

        private void CategoryRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                List.ValueObjects.CustomCategory customCategory = (CustomCategory)e.Item.DataItem;

                if (customCategory != null)
                {
                    _ItemCount++;
                    int currentCategoryId = customCategory.CategoryID;

                    string Description = FrameworkGlobals.GetUnicodeText(customCategory.Description.ToString());

                    Literal ltlDescription = (Literal)e.Item.FindControl("ltlDescription");

                    ltlDescription.Text = g.InsertSpaces(Description, 50);

                    Literal ltlDeleteDescription = (Literal)e.Item.FindControl("ltlDeleteDescription");

                    ltlDeleteDescription.Text = g.InsertSpaces(Description.ToString(), 50);

                    TextBox HotListCategoryRename = (TextBox)e.Item.FindControl("HotListCategoryRename");
                    HotListCategoryRename.Text = Description;

                    SetPanels(e.Item, EditMode.Normal);
                }
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        private void CategoryRepeater_ItemCommand(object Sender, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "Rename")
            {
                SetPanels(e.Item, EditMode.Rename);
            }
            else if (e.CommandName == "Delete")
            {
                SetPanels(e.Item, EditMode.Delete);
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            WebControl ParentControl = (WebControl)((WebControl)sender).Parent;
            TextBox HotListCategoryRename = (TextBox)ParentControl.FindControl("HotListCategoryRename");
            string rawNewName = HotListCategoryRename.Text;

            // if not blank, add a cleaned up copy
            string cleanCategoryDescription = Regex.Replace(rawNewName, @"<(.|\n)*?>", string.Empty);

            double categoryID;

            // make the new category.  4 ('friend') is the default hotlisttypeid for user created hot lists
            if ((double.TryParse(Request["RenameID"], System.Globalization.NumberStyles.Integer, null, out categoryID) &&
                cleanCategoryDescription != null
                && cleanCategoryDescription.Trim().Length > 0
                && cleanCategoryDescription != String.Empty))
            {
                CustomCategoryResult result = List.ServiceAdapters.ListSA.Instance.SaveListCategory(
                    (Int32)categoryID,
                    g.Brand.Site.Community.CommunityID
                    , g.Session.GetInt(WebConstants.SESSION_PROPERTY_NAME_MEMBERID)
                    , cleanCategoryDescription.Trim());

                CategoryRepeater.DataSource = List.ServiceAdapters.ListSA.Instance.GetCustomListCategories(g.Session.GetInt(WebConstants.SESSION_PROPERTY_NAME_MEMBERID), g.Brand.Site.Community.CommunityID);
                CategoryRepeater.DataBind();

                switch (result.Status)
                {
                    case CustomCategoryResultStatus.Success:
                        g.Notification.AddMessage("SUCCESSFULLY_SAVED_CATEGORY");
                        break;
                    case CustomCategoryResultStatus.ExistingCategoryName:
                        g.Notification.AddError("TXT_YOU_ALREADY_HAVE_A_CATEGORY_WITH_THAT_NAME");
                        break;
                    default:
                        break;
                }
            }
            else
            {
                g.Notification.AddError("ERR_INVALID_CATEGORY_DESCRIPTION");
            }

            DetermineShowCreateNew();
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            g.Transfer("/Applications/Hotlist/ManageListCategory.aspx");
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            Button btnDelete = (Button)sender;
            int deleteID = Matchnet.Conversion.CInt(btnDelete.CommandArgument);

            ListSA.Instance.DeleteListCategory(deleteID, g.Brand.Site.Community.CommunityID, g.Session.GetInt(WebConstants.SESSION_PROPERTY_NAME_MEMBERID));
            CategoryRepeater.DataSource = List.ServiceAdapters.ListSA.Instance.GetCustomListCategories(g.Session.GetInt(WebConstants.SESSION_PROPERTY_NAME_MEMBERID), g.Brand.Site.Community.CommunityID);
            CategoryRepeater.DataBind();

            g.Notification.AddMessage("SUCCESSFULLY_DELETED_CATEGORY");

            DetermineShowCreateNew();
        }


        private void btnSubmit_ServerClick(object sender, EventArgs e)
        {
            // check for blank string
            if (CategoryName.Value.Trim() == "")
            {
                g.Notification.AddError("ERR_INVALID_CATEGORY_DESCRIPTION");
            }
            else
            {
                // if not blank, add a cleaned up copy
                string cleanCategoryDescription = Regex.Replace(CategoryName.Value, @"<(.|\n)*?>", string.Empty);

                DataTable categories = ListLib.GetCategories(g.Session.GetInt(WebConstants.SESSION_PROPERTY_NAME_MEMBERID), g.Brand.Site.Community.CommunityID, g, this);

                // make the new category
                CustomCategoryResult result = ListSA.Instance.SaveListCategory(
                    Constants.NULL_INT,
                    g.Brand.Site.Community.CommunityID,
                    g.Session.GetInt(WebConstants.SESSION_PROPERTY_NAME_MEMBERID)
                    , cleanCategoryDescription);

                switch (result.Status)
                {
                    case CustomCategoryResultStatus.Success:
                        // Category create success
                        g.Notification.AddMessage("SUCCESSFULLY_SAVED_CATEGORY");
                        CategoryName.Value = "";
                        CategoryRepeater.DataSource = List.ServiceAdapters.ListSA.Instance.GetCustomListCategories(g.Session.GetInt(WebConstants.SESSION_PROPERTY_NAME_MEMBERID), g.Brand.Site.Community.CommunityID);
                        CategoryRepeater.DataBind();
                        break;
                    case CustomCategoryResultStatus.MaximumCategoriesReached:
                        // Maximum number of custom categories reached
                        g.Notification.AddMessage("TXT_YOU_HAVE_EXCEEDED_MAXIMUM_NUMBER_OF_CATEGORIES");
                        break;
                    case CustomCategoryResultStatus.ExistingCategoryName:
                        // Category exists
                        g.Notification.AddError("TXT_YOU_ALREADY_HAVE_A_CATEGORY_WITH_THAT_NAME");
                        break;
                    default:
                        break;
                }
            }

            DetermineShowCreateNew();
        }

        /// <summary>
        /// Displays the appropriate controls for the Category based on whether the user is viewing, renaming, or deleting.
        /// </summary>
        /// <param name="repeaterItem">The repeaterItem corresponding to the category for which you are changing the controls.</param>
        /// <param name="editMode">Normal, Rename, or Delete</param>
        private void SetPanels(RepeaterItem repeaterItem, EditMode editMode)
        {
            Panel panelNormal = (Panel)repeaterItem.FindControl("PanelNormal");
            Panel panelRename = (Panel)repeaterItem.FindControl("PanelRename");
            Panel panelDelete = (Panel)repeaterItem.FindControl("PanelDelete");

            switch (editMode)
            {
                case EditMode.Normal:
                    panelNormal.Visible = true;
                    panelRename.Visible = false;
                    panelDelete.Visible = false;
                    break;
                case EditMode.Rename:
                    panelNormal.Visible = false;
                    panelRename.Visible = true;
                    panelDelete.Visible = false;
                    break;
                case EditMode.Delete:
                    panelNormal.Visible = false;
                    panelRename.Visible = false;
                    panelDelete.Visible = true;
                    break;
            }
        }


        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSubmit.ServerClick += new System.EventHandler(this.btnSubmit_ServerClick);
            this.Init += new System.EventHandler(this.Page_Init);
            this.CategoryRepeater.ItemDataBound += new RepeaterItemEventHandler(CategoryRepeater_ItemDataBound);
            this.CategoryRepeater.ItemCommand += new RepeaterCommandEventHandler(CategoryRepeater_ItemCommand);
        }
        #endregion
    }
}
