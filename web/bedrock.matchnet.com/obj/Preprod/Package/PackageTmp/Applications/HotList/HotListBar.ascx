﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="HotListBar.ascx.cs" Inherits="Matchnet.Web.Applications.HotList.HotListBar" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>

<asp:PlaceHolder ID="phTitle" runat="server">
    <h2 class="component-header"><mn2:FrameworkLiteral ID="txtTitle" runat="server" ></mn2:FrameworkLiteral></h2>
</asp:PlaceHolder>
<ul class="<asp:Literal id='litClass' runat='server'/>">
<asp:Repeater ID="rptHotListCategory" runat="server" OnItemDataBound="rptHotListCategory_OnItemDataBound" >
   <ItemTemplate>
        <li runat=server id="liTab" class="clearfix"><a runat=server id="liIconLink" class="hotlist-stat-icon"><span class="<asp:Literal id='litImageClass' runat='server'/>"><span><asp:Literal id="litNotifications" Visible="true" runat="server"/></span></span></a><mn:Txt id="txtHotListCategory" runat="server" CssClass="hotlist-stat-category" /><asp:Literal id="litNotificationsRight" Visible="true" runat="server"/></li>
   </ItemTemplate>
</asp:Repeater>
</ul>