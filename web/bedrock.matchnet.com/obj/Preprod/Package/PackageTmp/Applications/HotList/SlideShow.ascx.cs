﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Member.ValueObjects.Interfaces;
using Matchnet.Member.ValueObjects.MemberDTO;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Managers;
using Matchnet.Web.Framework.Ui;
using Matchnet.Web.Framework.Ui.BasicElements;
using Matchnet.List.ServiceAdapters;
using Matchnet.List.ValueObjects;
using Matchnet.Web.Applications.Home;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects;
namespace Matchnet.Web.Applications.HotList
{
    public partial class SlideShow : FrameworkControl
    {
        public string ViewProfileLinkFormat
        {
            get { return ucSlideshowProfile.ViewProfileLinkFormat; }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            ucSlideshowProfile.EntryPoint = BreadCrumbHelper.EntryPoint.SlideShowPage;
            ucSlideshowProfile.ReloadAds = true;
            ucSlideshowProfile.OmniturePageName = "Slideshow";
            ucSlideshowProfile.PopulateYNMBuckets = true;
            ucSlideshowProfile.SlideshowOuterContainerDivID = "secretAdmirer";
            ucSlideshowProfile.Load(true);

            bool enableConfiguration = Conversion.CBool(RuntimeSettings.GetSetting("ENABLE_CONFIGURABLE_SLIDESHOW", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID));
            if (enableConfiguration)
            {
                ucSlideshowProfile.LoadPreferences();
            }
            
            int yesCount=0;
            ArrayList  yesList= g.List.GetListMembers(HotListCategory.MembersClickedYes, g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, 1, 4, out yesCount);
            int noCount=0;
            ArrayList  noList= g.List.GetListMembers(HotListCategory.MembersClickedNo, g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, 1, 4, out noCount);
            int maybeCount = 0;
            ArrayList maybeList = g.List.GetListMembers(HotListCategory.MembersClickedMaybe, g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, 1, 4, out maybeCount);

            listYes.MemberList = GetProfiles(yesList, HotListCategory.MembersClickedYes);
            listYes.DisplayContext = DisplayContextType.YNMList;
            listNo.MemberList = GetProfiles(noList, HotListCategory.MembersClickedNo);
            listMaybe.MemberList = GetProfiles(maybeList, HotListCategory.MembersClickedMaybe);
        }


        private List<ProfileHolder> GetProfiles(ArrayList memberids, HotListCategory category)
        {
            List<ProfileHolder> profileList = new List<ProfileHolder>();
            try
            {
                ArrayList MemberList = MemberDTOManager.Instance.GetIMemberDTOs(g, memberids, MemberType.Search, MemberLoadFlags.None);

                for (int i = 0; i < MemberList.Count; i++)
                {
                    ProfileHolder ph = new ProfileHolder();
                    ph.Member = MemberList[i] as IMemberDTO;
                    ph.Ordinal = i + 1;
                    ph.HotlistCategory = category;
                    ph.MyEntryPoint = BreadCrumbHelper.EntryPoint.SlideShowPage;
                    profileList.Add(ph);
                }
                return profileList;
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
                return profileList;
            }

        }

        public string GetAgeFormat()
        {
            return ucSlideshowProfile.GetAgeFormat();
        }
     
    }
}
