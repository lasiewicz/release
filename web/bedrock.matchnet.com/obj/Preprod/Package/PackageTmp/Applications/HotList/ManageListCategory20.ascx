﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ManageListCategory20.ascx.cs" Inherits="Matchnet.Web.Applications.HotList.ManageListCategory20" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mnl" Namespace="Matchnet.Web.Framework.Ui.BasicElements.Links" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="cc1" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>

<h1><mn:txt id="txtYourHotLists" runat="server" ResourceConstant="TXT_YOUR_HOT_LISTS" /></h1>

	<asp:PlaceHolder ID="phCreateNew" Runat="server">
	    <div class="border-gen">
            <h2><mn:txt id="txtCreateNewCategory" runat="server" resourceconstant="CREATE_NEW_CATEGORY" /></h2>
            <b><mn:txt id="txtName" runat="server" resourceconstant="NAME" /></b>&nbsp;
            <input type="text" id="CategoryName" runat="server" name="CategoryName" /> &nbsp;<input type="button" id="btnSubmit" runat="server" name="btnSubmit" class="btn primary" />
        </div>
	</asp:PlaceHolder>

<div id="folder-settings-hot-lists" class="margin-medium">	
	<asp:PlaceHolder Runat="server" ID="phMaximumLists" Visible="False" EnableViewState="False">
		<p><mn:txt runat="server" id="txtListsMax" ResourceConstant="MAXIMUM_LISTS_REACHED" /></p>
	</asp:PlaceHolder>
 
    <h2><mn:txt id="txtManageHotLists" runat="server" resourceconstant="MANAGE_HOT_LISTS" /></h2>
    <asp:repeater id="CategoryRepeater" runat="server">
	    <ItemTemplate>
	    <div class="item">
		    <asp:Panel ID="PanelNormal" Runat="server">
				
			    <asp:Literal Runat="server" ID="ltlDescription" />&nbsp;
			    <asp:LinkButton ID="LinkButton1" Runat="server" CommandName="Rename" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "CategoryID") %>'>
				    <mn:txt ID="Txt1" runat="server" resourceconstant="RENAME" />
			    </asp:LinkButton>&nbsp;
			    <asp:LinkButton ID="LinkButton2" Runat="server" CommandName="Delete" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "CategoryID") %>'>
				    <mn:txt ID="Txt2" runat="server" resourceconstant="DELETE" />
			    </asp:LinkButton>
		
		    </asp:Panel>
		    <asp:Panel ID="PanelRename" Runat="server">
		    <asp:TextBox id="HotListCategoryRename" runat="server" />
			    <input type="hidden" name="RenameID" value="<%# DataBinder.Eval(Container.DataItem, "CategoryID") %>">
			    &nbsp;
			    <cc1:FrameworkButton class="btn primary" id="btnSave" runat="server" OnClick="btnSave_Click" ResourceConstant="BTN_SAVE" />
			    &nbsp;
			    <cc1:FrameworkButton class="btn secondary" id="btnCancel" runat="server" OnClick="btnCancel_Click" ResourceConstant="BTN_CANCEL"/>
			
		    </asp:Panel>
		    <asp:Panel ID="PanelDelete" Runat="server">
			    <asp:Literal Runat="server" ID="ltlDeleteDescription" />:&nbsp;<mn:txt ID="Txt3" runat="server" ResourceConstant="TXT_DELETE_CONFIRMATION" />
			    &nbsp;
			    <cc1:FrameworkButton class="btn secondary" id="btnDelete" runat="server" OnClick="btnDelete_Click" ResourceConstant="BTN_YES"  CommandArgument='<%# DataBinder.Eval(Container.DataItem, "CategoryID") %>'/>
				    &nbsp;
				<cc1:FrameworkButton class="btn secondary" id="btnNo" runat="server" OnClick="btnCancel_Click" ResourceConstant="BTN_NO" />
		    </asp:Panel>
	    </div>
	    </itemtemplate>
        <AlternatingItemTemplate>
        <div class="item odd">
		    <asp:Panel ID="PanelNormal" Runat="server">
				
			    <asp:Literal Runat="server" ID="ltlDescription" />&nbsp;
			    <asp:LinkButton ID="LinkButton1" Runat="server" CommandName="Rename" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "CategoryID") %>'>
				    <mn:txt ID="Txt1" runat="server" resourceconstant="RENAME" />
			    </asp:LinkButton>&nbsp;
			    <asp:LinkButton ID="LinkButton2" Runat="server" CommandName="Delete" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "CategoryID") %>'>
				    <mn:txt ID="Txt2" runat="server" resourceconstant="DELETE" />
			    </asp:LinkButton>
		
		    </asp:Panel>
		    <asp:Panel ID="PanelRename" Runat="server">
		    <asp:TextBox id="HotListCategoryRename" runat="server" />
			    <input type="hidden" name="RenameID" value="<%# DataBinder.Eval(Container.DataItem, "CategoryID") %>">
			    &nbsp;
			    <cc1:FrameworkButton class="btn primary" id="btnSave" runat="server" OnClick="btnSave_Click" ResourceConstant="BTN_SAVE" />
			    &nbsp;
			    <cc1:FrameworkButton class="btn secondary" id="btnCancel" runat="server" OnClick="btnCancel_Click" ResourceConstant="BTN_CANCEL"/>
			
		    </asp:Panel>
		    <asp:Panel ID="PanelDelete" Runat="server">
			    <asp:Literal Runat="server" ID="ltlDeleteDescription" />:&nbsp;<mn:txt ID="Txt3" runat="server" ResourceConstant="TXT_DELETE_CONFIRMATION" />
			    &nbsp;
			    <cc1:FrameworkButton class="btn secondary" id="btnDelete" runat="server" OnClick="btnDelete_Click" ResourceConstant="BTN_YES"  CommandArgument='<%# DataBinder.Eval(Container.DataItem, "CategoryID") %>'/>
				    &nbsp;
			    <cc1:FrameworkButton class="btn secondary" id="btnNo" runat="server" OnClick="btnCancel_Click" ResourceConstant="BTN_NO" />
		    </asp:Panel>
	    </div>
        </AlternatingItemTemplate>
    </asp:repeater>
    

    
    <div class="pagination-buttons clearfix image-text-pair">
		<mn:Txt CssClass="btn link-secondary small" ID="txtHotList" RunAt="server" ResourceConstant="TXT_RETURN_TO_HOTLIST" Href="/Applications/HotList/View.aspx" />
    </div>
</div>