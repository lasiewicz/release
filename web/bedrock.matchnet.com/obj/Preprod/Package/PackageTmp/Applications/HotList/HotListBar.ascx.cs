﻿using System;
using System.Collections;
using System.Collections.ObjectModel;
using System.Collections.Generic;

using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using Matchnet.Lib;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Ui.BasicElements;

using Matchnet.Member.ServiceAdapters;
using Matchnet.List.ServiceAdapters;
using Matchnet.List.ValueObjects;
using Matchnet.Session.ValueObjects;

using Matchnet.InstantMessenger.ServiceAdapters;
using Matchnet.InstantMessenger.ValueObjects;
using Matchnet.Web.Framework.Managers;
using Matchnet.Content.ServiceAdapters.Links;


namespace Matchnet.Web.Applications.HotList
{
    public partial class HotListBar : FrameworkControl
    {
        const string HOTLIST_FORMAT = "{0}: {1}";
        const string HOTLIST_FORMAT_NO_COUNT = "{0}";
        const string IMAGE_RESX_FORMAT = "IMAGE_CAT_{0}";
        const string IMAGE_DISABLED_RESX_FORMAT = "IMAGE_CAT_DISABLED_{0}";
        HotListCategory _hotListCategorySelected;
        List<int> _categories;
        String _classSelected = "";
        List<int> _notificationCategories;
        FrameworkControl _resourceControl;
        bool _customListFlag;
        private int messageNotificationsCount = 0;
        private int messageCount = 0;
        bool _hasHotListActivity = false;
        bool _saveMember = false;
        string _hotlistFormat=HOTLIST_FORMAT;

        int _totalcount = 0;
        public bool HasHotListActivity
        {
            get { return _hasHotListActivity; }
            set { _hasHotListActivity = value; }
        }

         public FrameworkControl ResourceControl
         {
             get { return _resourceControl; }
             set { _resourceControl = value; }
         }
         public HotListCategory HotListCategorySelected
         {
             get { return _hotListCategorySelected; }
             set { _hotListCategorySelected = value; }

         }
         public String Class
         {
             get;
             set;

         }
         public String ListItemClass
         {
             get;
             set;

         }
         public String ClassSelected
         {
             get { return _classSelected; }
             set { _classSelected = value; }

         }
         public List<int> Categories
         {
             get { return _categories; }
             set { _categories = value; }

         }

         public bool IsCustomList
         {
             get { return _customListFlag; }
             set { _customListFlag = value; }
         }
         public bool EnableNotifications
         {
             get;
             set;

         }

         public HotListDirection Direction
         {
             get;
             set;
         }

         public bool DisplayTitle
         {
             get { return phTitle.Visible; }
             set { phTitle.Visible = value; }
         }

        
        public bool HideNotificationIcon { get; set; }
        public bool HideEmptyLists { get; set; }
        public bool HideIfAllListsEmpty { get; set; }
        public int TotalListsCount { get { return _totalcount; } }


        public string HotlistFormat { get { return _hotlistFormat; } set { _hotlistFormat = value; } }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (_categories == null || _categories.Count == 0)
            {
                if (!IsCustomList)
                { 
                    GetCategories();
                    GetNotificationCategories();
                }

            }
            if (!IsCustomList)
            {
                rptHotListCategory.DataSource = ListLib.GetStandardCategories(g.Brand.Site.SiteID, _categories);
                if (Direction == HotListDirection.OnTheirList)
                {
                    txtTitle.ResourceConstant = "LISTDIRECTION_WHO_HAVE_YOU";
                }
                else
                {
                    txtTitle.ResourceConstant = "LISTDIRECTION_YOU_HAVE";
                }
            }
            else
                if (g.Member != null)
                {
                    rptHotListCategory.DataSource = ListLib.GetCustomCategories(g.Member.MemberID, g.Brand.Site.Community.CommunityID, _categories);
                }

            for (int i = 0; i < _categories.Count; i++)
            {
                _totalcount+=g.List.GetCount((HotListCategory)_categories[i], g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID);
            }

            if (_totalcount == 0 && HideIfAllListsEmpty)
                return;
             if(String.IsNullOrEmpty(Class))
                litClass.Text = "nav-rounded-tabs no-click clearfix";
             else
                 litClass.Text=Class;
             rptHotListCategory.DataBind();

             //if last page visits for hot lists were set then save member
             if (_saveMember)
             {
                 try
                 {
                     Member.ServiceAdapters.MemberSA.Instance.SaveMember(g.Member);
                 }
                 catch (Exception exc) { exc = null; }
                 _saveMember = false;
             }
        }

         public void rptHotListCategory_OnItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
         {
             Txt txtList =(Txt) e.Item.FindControl("txtHotListCategory");
           
             HtmlGenericControl liTab = (HtmlGenericControl)e.Item.FindControl("liTab");
             int categoryid = 0;
             int count = 0;
             int notificationCount = 0;
             if (txtList == null || liTab==null)
                 return;

             Literal litTab = (Literal)e.Item.FindControl("litItemClass");
             if (litTab != null)
             {
                 if (String.IsNullOrEmpty(ListItemClass))
                 { litTab.Text = "tab"; }
                 else
                 {
                     litTab.Text = ListItemClass;
                 }

             }

             txtList.Text = GetHotListTabText(e, ref count, ref notificationCount);
             if (count == 0 && HideEmptyLists)
             {
                 liTab.Style.Add("display", "none");
             }

             if (IsCustomList)
             {
                 List.ValueObjects.CustomCategory hotListRow = ((CustomCategory)e.Item.DataItem);
                 categoryid= hotListRow.CategoryID;
             }
             else
             {
                 Literal litImageClass = (Literal)e.Item.FindControl("litImageClass");
                 List.ValueObjects.StandardCategory hotListRow = ((StandardCategory)e.Item.DataItem);
                 categoryid= hotListRow.ListCategoryID;
                 string imageformat="";

                 if (HotList.HotListViewHandler.IsNotificationsEnabled(g))
                 {
                     if (notificationCount > 0 && !HideNotificationIcon)
                     {
                         imageformat = IMAGE_RESX_FORMAT;
                         string imageSpriteClass = g.GetResource(String.Format(imageformat, categoryid * -1), this);
                         string imageSuperscriptClass = g.GetResource("IMG_SUPERSCRIPT", this);
                         litImageClass.Text = imageSpriteClass + imageSuperscriptClass;
                     }
                     else
                     {
                         litImageClass.Text = g.GetResource("IMG_BLANK", this);
                     }


                     //if (categoryid == (int)HotListCategory.WhoTeasedYou
                     //    || categoryid == (int)HotListCategory.WhoSentYouECards)
                     //{
                     //    liTab.Visible = false;
                     //    messageNotificationsCount += notificationCount;
                     //    messageCount += count;
                     //}
                 }
                 else
                 {
                     if (CategoryHasNotification(categoryid))
                         imageformat = IMAGE_RESX_FORMAT;
                     else
                         imageformat = IMAGE_RESX_FORMAT;

                     string imageSpriteClass = g.GetResource(String.Format(imageformat, categoryid * -1), this);
                     litImageClass.Text = imageSpriteClass;
                 }
             }

             txtList.Href = "/Applications/HotList/View.aspx?CategoryID=" + categoryid;
             if (categoryid == (int)HotListCategory.WhoViewedYourProfile)
             {
                 ListManager listManager = new ListManager();
                 if (!listManager.IsViewedYourProfileListAvailableToMember(g.Member, g.Brand))
                 {
                     txtList.Href = FrameworkGlobals.GetSubscriptionLink(false, (int)PurchaseReasonType.HotlistViewedYourProfile);
                 }
             }

             HtmlAnchor liIconLink = e.Item.FindControl("liIconLink") as HtmlAnchor;
             if (notificationCount > 0)
             {
                 liIconLink.HRef = txtList.Href;
             }
             else
             {
                 liIconLink.Name = "noclick";
             }

             if (!String.IsNullOrEmpty(ListItemClass))
             {
                 liTab.Attributes["class"] = ListItemClass;
                 if (categoryid == (int)_hotListCategorySelected || CategoryHasNotification(categoryid))
                 {
                     // txtList.Attributes.Add("class", _classSelected);
                     liTab.Attributes["class"] = liTab.Attributes["class"] + " " + ClassSelected;
                 }
             }

             if (count > 0)
             {
                 //omniture stuff
                 HasHotListActivity = true;              
             }
         }

         private string GetHotListTabText(System.Web.UI.WebControls.RepeaterItemEventArgs e, ref int count, ref int notificationCount)
         {
             string resource = String.Empty;
             string resTxt = String.Empty;
             HotListCategory categoryid = HotListCategory.Default;
             string resTxtKey = String.Empty;
             if (IsCustomList)
             {
                 List.ValueObjects.CustomCategory hotListRow = ((CustomCategory)e.Item.DataItem);
                 resTxt = hotListRow.Description;
                 categoryid=(HotListCategory)hotListRow.CategoryID;
                 count = g.List.GetCount(categoryid, g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID);
               }
             else
             {
                 List.ValueObjects.StandardCategory hotListRow = ((StandardCategory)e.Item.DataItem);
                 resTxtKey = hotListRow.ResourceKey;
                 resTxt = getResource(resTxtKey);
                 categoryid = (HotListCategory)hotListRow.ListCategoryID;
                 count = g.List.GetCount(categoryid, g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID);
             }
             if (Direction == HotListDirection.OnTheirList && HotList.HotListViewHandler.IsNotificationsEnabled(g))
             {
                 //get new notifications
                 int brandid = g.Brand.BrandID;
                 int communityid = g.Brand.Site.Community.CommunityID;
                 int siteid = g.Brand.Site.SiteID;
                 string lastVisitAttributeId = "LastHotListPageVisit-" + categoryid.ToString();
                 bool attributeIsSet = true;
                 DateTime today = DateTime.Now;
                 DateTime lastVisit = today;
                 try
                 {
                     lastVisit = g.Member.GetAttributeDate(communityid, siteid, brandid, lastVisitAttributeId, lastVisit);
                 }
                 catch (Exception ex)
                 {
                     //exception is thrown if attribute not set for site/brand
                     attributeIsSet = false;
                     ex = null;
                 }

                 if (attributeIsSet)
                 {
                     //if no last visit then set last visit == today. This is only true if the
                     //pages have never been visited for the life of the account (only really true 
                     //for new members)
                     if (lastVisit.Equals(today))
                     {
                        try
                        {
                         g.Member.SetAttributeDate(communityid, siteid, brandid, lastVisitAttributeId, DateTime.Now);
                         _saveMember = true;
                        }
                        catch (Exception exc) { exc = null; }
                     }
                     else if (count > 0)
                     {
                         //get count based on last date of page visit
                         int rowcount;

                         ArrayList memberIDs = g.List.GetListMembers(categoryid,
                                             communityid,
                                             siteid,
                                             1,
                                             count,
                                             out rowcount);
                         for (int i = 0; i < memberIDs.Count; i++)
                         {
                             ListItemDetail listDetail = g.List.GetListItemDetail(categoryid,
                                             communityid,
                                             siteid,
                                             (int)memberIDs[i]);

                             if (listDetail.ActionDate > lastVisit)
                             {
                                 notificationCount++;
                             }
                         }
                     }

                     if (notificationCount > 0)
                     {
                         Literal notifications = e.Item.FindControl("litNotifications") as Literal;
                         notifications.Text = (notificationCount < 100) ? notificationCount + "" : "99+";

                         if (HideNotificationIcon)
                         {
                             Literal rightnotifications = e.Item.FindControl("litNotificationsRight") as Literal;
                             string notificationresx = g.GetResource("HOTLIST_NOTIFICATIONS_FORMAT_", this);
                             //string notificationsnum= (notificationCount < 100) ?notificationCount.ToString() : "99+";
                             rightnotifications.Text = String.Format(notificationresx, notificationCount);
                         }                         
                     }
                     string listText = (count == 0) ? HOTLIST_FORMAT_NO_COUNT : _hotlistFormat;
                     return String.Format(listText, resTxt, count,notificationCount);
                 }
             }

             resTxt = FrameworkGlobals.Ellipsis(resTxt, 20);
             return String.Format(HOTLIST_FORMAT, resTxt, count);
         }

         private string getResource(string key)
         {
             string res = "";
           
             res = g.GetResource(key, this);

             return res;
         }

         private void GetCategories()
         {
             try{
                 if(_categories== null)
                     _categories=new List<int>();
             
                 if (Direction == HotListDirection.OnTheirList)
                 {
                     //if (HotListViewHandler.IsNotificationsEnabled(g))
                     //{
                     //    _categories.Add(-1);
                     //    _categories.Add(-3);
                     //    _categories.Add(-25);
                     //    _categories.Add(-4);
                     //    _categories.Add(-2);
                     //    _categories.Add(-5);
                     //}
                     //else
                     //{
                         _categories.Add(-1);
                         _categories.Add(-4);
                         _categories.Add(-2);
                         _categories.Add(-3);
                         _categories.Add(-25);
                         _categories.Add(-5);
                         _categories.Add(-15);
                     //}
                    
                 }
                 else if (Direction == HotListDirection.OnYourList)
                 {
                     _categories.Add(-9);
                     _categories.Add(-6);
                     _categories.Add(-7);
                     _categories.Add(0);
                     _categories.Add(-24);
                     _categories.Add(-8);
               
                 }

             }
             catch (Exception ex)
             { g.ProcessException(ex); }
         }

         private void GetNotificationCategories()
         {
             try
             {
                 if (g.Member == null)
                     return;

                 _notificationCategories = new List<int>();

                 int newNotificationMask = g.Member.GetAttributeInt(g.Brand, WebConstants.ATTRIBUTE_NAME_HASNEWNOTIFICATION, 0);

                 ConversationInvites invites = Matchnet.InstantMessenger.ServiceAdapters.InstantMessengerSA.Instance.GetInvites(g.Member.MemberID, g.Brand.Site.Community.CommunityID);
                 bool foundInvitations = ((invites != null) && (invites.Count > 0));
                 if (foundInvitations)
                 {
                     _notificationCategories.Add(-5);
                 }

                 if ((newNotificationMask & (int)Matchnet.Constants.NotificationType.HotList) == (int)Matchnet.Constants.NotificationType.HotList)
                     _notificationCategories.Add(-2);
                 if (g.Member.GetAttributeBool(g.Brand, WebConstants.ATTRIBUTE_NAME_HASNEWMAIL))
                     _notificationCategories.Add(-4);
                 if((newNotificationMask & (int)Matchnet.Constants.NotificationType.ProfileViews)==(int)Matchnet.Constants.NotificationType.ProfileViews)
                     _notificationCategories.Add(-1);
             }
             catch (Exception ex) { g.ProcessException(ex); }

         }

         private bool CategoryHasNotification(int category)
         {
             int cat=0;
             cat    =_notificationCategories.Find(delegate(int c) { return c == category; });
             if (cat >= 0)
                 return false;


             return true;
                 
                 
         }
        

         
    }
}
