﻿#region system
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
#endregion

#region Matchnet Web App References
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Util;
using Matchnet.Lib;
using Matchnet.Web.Framework.Ui.FormElements;
#endregion

namespace Matchnet.Web.Applications.MembersOnline.Controls
{
    public interface IMembersOnlineFilter
    {
        System.Web.UI.HtmlControls.HtmlSelect SELGenderMask { get; }
        System.Web.UI.HtmlControls.HtmlSelect SELRegionID { get;  }
        System.Web.UI.HtmlControls.HtmlSelect SELLanguageMask { get; }
        DropDownList DDLDistance { get; }
        System.Web.UI.HtmlControls.HtmlInputText TXTAgeMin { get;  }
        System.Web.UI.HtmlControls.HtmlInputText TXTAgeMax { get; }
        PlaceHolder PLCanguage { get; }

        System.Web.UI.WebControls.RangeValidator VALAgeMinRange { get;  }
        System.Web.UI.WebControls.RangeValidator VALAgeMaxRange { get; }
        System.Web.UI.WebControls.RequiredFieldValidator VALAgeMinReq { get; }
        System.Web.UI.WebControls.RequiredFieldValidator VALAgeMaxReq { get;  }
        System.Web.UI.WebControls.ValidationSummary VALSummary { get; }
        Matchnet.Web.Framework.Txt TXTOnlineNow { get;  }
        Matchnet.Web.Framework.Txt TXTShow { get;  }
        Matchnet.Web.Framework.Txt TXTFrom { get;  }
        Matchnet.Web.Framework.Txt TXTAges { get; }
        Matchnet.Web.Framework.Txt TXTTo { get;  }
        Matchnet.Web.Framework.Txt TXTWhoSpeak { get;  }
        System.Web.UI.WebControls.CompareValidator VALCompare { get;  }
        Matchnet.Web.Framework.Ui.FormElements.FrameworkButton BTNSearch { get;  }

        PickRegionNew PrSearchRegion { get; }
        Anthem.HyperLink LnkLocation { get; }
        Anthem.PlaceHolder PlcDistance { get; }
        Anthem.PlaceHolder PlcAreaCodes { get; }
        Anthem.PlaceHolder PlcCollege { get; }

        FrameworkControl ThisControl { get;  }

        int intRegionID { get; }
        int intAgeMin { get; }
        int intAgeMax{ get; }
        int intLanguageMask { get; }
        int intDistance { get; }
        int GetGenderMask();
        
    }
}
