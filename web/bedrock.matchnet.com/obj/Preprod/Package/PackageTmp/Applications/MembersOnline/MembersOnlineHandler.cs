﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


using Matchnet.Lib;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Ui;
using Matchnet.Web.Framework.Ui.BasicElements;

using Matchnet.MembersOnline.ValueObjects;

using Matchnet.Session.ValueObjects;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Web.Framework.Managers;
using Matchnet.Search.ValueObjects;
using Matchnet.Search.Interfaces;

namespace Matchnet.Web.Applications.MembersOnline
{
    public class MembersOnlineHandler:FrameworkControl
    {
        IMembersOnline _molControl;
        ContextGlobal g;

        public MembersOnlineHandler(ContextGlobal context,IMembersOnline mol)
        {
            _molControl=mol;
            g=context;
        }

        public void SetSortDropDown(bool isPostBack)
        {
            if (MembersOnlineManager.Instance.IsE2MOLEnabled(g.Brand))
            {
                if (string.IsNullOrEmpty(g.MembersOnlineSearchPreferences["searchorderby"]))
                {
                    g.MembersOnlineSearchPreferences["searchorderby"] = ((int)QuerySorting.LastLogonDate).ToString();
                }

                string e2SearchOrderBy = "";
                if (isPostBack)
                {
                    if (HttpContext.Current.Request[_molControl.MemberSortDropDown.UniqueID] != null)
                    {
                        e2SearchOrderBy = HttpContext.Current.Request[_molControl.MemberSortDropDown.UniqueID];

                        try
                        {
                            QuerySorting e2Sorting = (QuerySorting)Enum.Parse(typeof(QuerySorting), e2SearchOrderBy);
                            g.MembersOnlineSearchPreferences["searchorderby"] = ((int)e2Sorting).ToString();
                        }
                        catch (Exception e)
                        {
                            //ignore
                        }
                    }
                }

                g.Session.Add(WebConstants.SESSION_MOL_SEARCH, g.MembersOnlineSearchPreferences, SessionPropertyLifetime.Temporary);
                BindSortDropDownE2();
            }
            else
            {
                String strSearchOrderBy = SortFieldType.HasPhoto.ToString();

                if (isPostBack)
                {
                    if (HttpContext.Current.Request[_molControl.MemberSortDropDown.UniqueID] != null)
                    {
                        strSearchOrderBy = HttpContext.Current.Request[_molControl.MemberSortDropDown.UniqueID];
                        g.Session.Add(WebConstants.SESSION_PROPERTY_NAME_MOL_SORT, strSearchOrderBy, SessionPropertyLifetime.Temporary);
                    }
                }
                else
                {
                    strSearchOrderBy = g.Session[WebConstants.SESSION_PROPERTY_NAME_MOL_SORT];

                    if (strSearchOrderBy == null || strSearchOrderBy == string.Empty)
                    {
                        // 18083 making Photos default for all sites on MOL sort
                        strSearchOrderBy = SortFieldType.HasPhoto.ToString();
                    }
                }

                BindSortDropDown(strSearchOrderBy);
            }

        }

        public void BindSortDropDown(String searchOrderBy)
        {
            // if this is a photo required site, 
            if (Convert.ToBoolean(RuntimeSettings.GetSetting("PHOTO_REQUIRED_SITE", g.Brand.Site.Community.CommunityID,
                    g.Brand.Site.SiteID, g.Brand.BrandID)))
            {
                if (_molControl.PhSortBy != null)
                {
                    _molControl.PhSortBy.Visible = false;
                }
            }
            else
            {
                _molControl.MemberSortDropDown.Items.Clear();
                _molControl.MemberSortDropDown.Items.Add(new ListItem(g.GetResource("MOL_NEW_MEMBERS", _molControl.ThisControl), SortFieldType.InsertDate.ToString()));
                _molControl.MemberSortDropDown.Items.Add(new ListItem(g.GetResource("MOL_MEMBERS_WITH_PHOTOS", _molControl.ThisControl), SortFieldType.HasPhoto.ToString()));
                _molControl.MemberSortDropDown.Items.Add(new ListItem(g.GetResource("MOL_USERNAME", _molControl.ThisControl), SortFieldType.UserName.ToString()));
                _molControl.MemberSortDropDown.Items.Add(new ListItem(g.GetResource("MOL_AGE", _molControl.ThisControl), SortFieldType.Age.ToString()));
                _molControl.MemberSortDropDown.Items.Add(new ListItem(g.GetResource("MOL_GENDER", _molControl.ThisControl), SortFieldType.Gender.ToString()));

                _molControl.MemberSortDropDown.SelectedValue = searchOrderBy;
            }
        }

        private void BindSortDropDownE2()
        {
            _molControl.MemberSortDropDown.Items.Clear();
            _molControl.MemberSortDropDown.Items.Add(new ListItem(g.GetResource("TXT_SEARCH_SORT_JOINDATE_CONTENT", _molControl.ThisControl), ((int)QuerySorting.JoinDate).ToString()));
            _molControl.MemberSortDropDown.Items.Add(new ListItem(g.GetResource("TXT_SEARCH_SORT_PROXIMITY_CONTENT", _molControl.ThisControl), ((int)QuerySorting.Proximity).ToString()));
            _molControl.MemberSortDropDown.Items.Add(new ListItem(g.GetResource("TXT_SEARCH_SORT_LASTLOGONDATE_CONTENT", _molControl.ThisControl), ((int)QuerySorting.LastLogonDate).ToString()));

            _molControl.MemberSortDropDown.SelectedValue = g.MembersOnlineSearchPreferences["SearchOrderBy"];
            
        }

        public void HandleView(bool isPostBack)
        {
            string defaultView = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("IS_MOL_DEFAULT_VIEW_GALLERY", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID);
            string galleryView = String.Empty; // TT #13433 - make galleryview the default for search results

            string ViewMode = HttpContext.Current.Request["ViewMode"];
            if (ViewMode != null)
            {
                if (ViewMode.ToLower() == "gallery")
                {
                    galleryView = "true";
                }
                else if (ViewMode.ToLower() == "list")
                {
                    galleryView = "false";
                }
            }
            else
            {
                galleryView = defaultView;

                if (!isPostBack)
                {
                    if (g.Member != null)
                    {
                        //get list view type from member attribute
                        Matchnet.Web.Framework.Ui.BasicElements.ResultsViewType.ListViewType lvType = ResultsViewType.GetMemberListType(g);
                        if (lvType != ResultsViewType.ListViewType.none)
                        {
                            if (lvType == Matchnet.Web.Framework.Ui.BasicElements.ResultsViewType.ListViewType.gallery)
                                galleryView = "true";
                            else
                                galleryView = "false";
                        }
                        else
                        {
                            lvType = galleryView == "true" ? Matchnet.Web.Framework.Ui.BasicElements.ResultsViewType.ListViewType.gallery : Matchnet.Web.Framework.Ui.BasicElements.ResultsViewType.ListViewType.list;
                        }

                        Matchnet.Web.Framework.Ui.BasicElements.ResultsViewType.SaveMemberListType(g, lvType);
                    }
                    else if (g.Session[ResultListHandler.SESSIONOBJ_GALLERYVIEW] != null)
                    {
                        galleryView = g.Session[ResultListHandler.SESSIONOBJ_GALLERYVIEW].ToString();
                        if (galleryView == String.Empty)
                        {
                            // TT #13433 & #13466 - make galleryview the default for search result based on setting
                            galleryView = defaultView;
                        }
                    }
                    _molControl.HIDGalleryView.Value = galleryView;
                }
                else
                {
                    galleryView = HttpContext.Current.Request.Params[_molControl.HIDGalleryView.UniqueID];

                    if (galleryView != null)
                    {
                        if (g.Member != null)
                        {
                            Matchnet.Web.Framework.Ui.BasicElements.ResultsViewType.SaveMemberListType(g, galleryView == "true" ? Matchnet.Web.Framework.Ui.BasicElements.ResultsViewType.ListViewType.gallery : Matchnet.Web.Framework.Ui.BasicElements.ResultsViewType.ListViewType.list);
                        }

                        if (g.Session[ResultListHandler.SESSIONOBJ_GALLERYVIEW] != null && g.Session[ResultListHandler.SESSIONOBJ_GALLERYVIEW].ToString() != galleryView)
                            g.Session.Add(ResultListHandler.SESSIONOBJ_GALLERYVIEW, galleryView, SessionPropertyLifetime.Temporary);

                        // TT #15790 - set a session flag to specify whether the list view changed, so we don't reset the VisitorLimit inappropriately (VisitorLimitHelper.cs)
                        g.Session.Add(VisitorLimitHelper.SEARCHRESULTS_VIEW_CHANGED_SESSION_KEY, "ViewChanged", SessionPropertyLifetime.Temporary);
                    }
                }
            }

            if (galleryView == "true")
            {
                if (_molControl.IconViewType != null)
                {
                    _molControl.IconViewType.FileName = "icon_listView.gif";
                    _molControl.IconViewType.ResourceConstant = "TXT_VIEW_AS_LIST";
                    
                }
                if (_molControl.TXTListOff != null)
                    { _molControl.TXTListOff.Visible = false; }

                _molControl.TXTViewAsMOL.ResourceConstant = "TXT_VIEW_AS_LIST";

                _molControl.LNKViewType.NavigateUrl = "javascript:document.getElementById('" + _molControl.HIDResultViewChanged.ClientID + "').value='true';";  // TT #15790 - set value to hidden field so we know the view has changed
                _molControl.LNKViewType.NavigateUrl += "document.getElementById('" + _molControl.HIDGalleryView.ClientID + "').value='false';document." + WebConstants.HTML_FORM_ID + ".submit();";

            }
            else
            {
                if (_molControl.IconViewType != null)
                {
                    _molControl.IconViewType.FileName = "icon_galleryView.gif";
                    _molControl.IconViewType.ResourceConstant = "TXT_VIEW_AS_GALLERY";
                }

                if (_molControl.TXTGalleryOff != null)
                { _molControl.TXTGalleryOff.Visible = false; }

                _molControl.TXTViewAsMOL.ResourceConstant = "TXT_VIEW_AS_GALLERY";

                _molControl.LNKViewType.NavigateUrl = "javascript:document.getElementById('" + _molControl.HIDResultViewChanged.ClientID + "').value='true';";  // TT #15790 - set value to hidden field so we know the view has changed
                _molControl.LNKViewType.NavigateUrl += "document.getElementById('" + _molControl.HIDGalleryView.ClientID + "').value='true';document." + WebConstants.HTML_FORM_ID + ".submit();";

            }


            _molControl.MOLResultList.GalleryView = galleryView;
        }

        public void BindResultList()
        {
            int genderMask;
            int regionID;
            int ageMin;
            int ageMax;
            int languageMask;

            genderMask = _molControl.MOLFilter.GetGenderMask();
            regionID = _molControl.MOLFilter.intRegionID;
            ageMin = _molControl.MOLFilter.intAgeMin;
            ageMax = _molControl.MOLFilter.intAgeMax;
            languageMask = _molControl.MOLFilter.intLanguageMask;

            MOCollection collection = null;

            // if this is a photo required site, override the sort pref to be HasPhoto always.
            if (Convert.ToBoolean(RuntimeSettings.GetSetting("PHOTO_REQUIRED_SITE", g.Brand.Site.Community.CommunityID,
                    g.Brand.Site.SiteID, g.Brand.BrandID)))
            {
                collection = new MOCollection(SortFieldType.HasPhoto.ToString(), genderMask, regionID, ageMin, ageMax, languageMask);
            }
            else
            {
                collection = new MOCollection(_molControl.MemberSortDropDown.SelectedValue, genderMask, regionID, ageMin, ageMax, languageMask);
            }
                
            _molControl.MOLResultList.MyMOCollection = collection;
        }


        public static  int GetGenderMaskOption(int genderMask)
        {
            if ((genderMask & ConstantsTemp.GENDERID_MALE) == ConstantsTemp.GENDERID_MALE)
            {
                if ((genderMask & ConstantsTemp.GENDERID_SEEKING_FEMALE) == ConstantsTemp.GENDERID_SEEKING_FEMALE)
                {
                    return 0;
                }
                else
                {
                    return 3;
                }
            }
            else if ((genderMask & ConstantsTemp.GENDERID_FEMALE) == ConstantsTemp.GENDERID_FEMALE)
            {
                if ((genderMask & ConstantsTemp.GENDERID_SEEKING_MALE) == ConstantsTemp.GENDERID_SEEKING_MALE)
                {
                    return 1;
                }
                else
                {
                    return 2;
                }
            }
            return -1;
        }

        public void HandlerVisitorLimit()
        {

            // TT #15790 - set a session flag to specify whether the list view changed, so we don't reset the VisitorLimit inappropriately (VisitorLimitHelper.cs)
            if (HttpContext.Current.Request.Params[_molControl.HIDResultViewChanged.UniqueID] != null)
            {
                if (bool.Parse(HttpContext.Current.Request.Params[_molControl.HIDResultViewChanged.UniqueID]))
                {
                    g.Session.Add(VisitorLimitHelper.SEARCHRESULTS_VIEW_CHANGED_SESSION_KEY, "ViewChanged", SessionPropertyLifetime.Temporary);
                }
                else
                {
                    g.Session.Remove(VisitorLimitHelper.SEARCHRESULTS_VIEW_CHANGED_SESSION_KEY);
                }
            }
        }

    }
}
