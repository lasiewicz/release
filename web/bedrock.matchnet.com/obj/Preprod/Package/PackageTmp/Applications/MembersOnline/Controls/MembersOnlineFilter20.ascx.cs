﻿#region System References
using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
#endregion

#region Matchnet Web App References
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Util;
using Matchnet.Lib;
using Matchnet.Web.Framework.Ui.FormElements;
#endregion

#region Matchnet Service References
using Matchnet.Session.ValueObjects;
using Matchnet.Web.Framework.Managers;
using System.Collections;
using Spark.SAL;
#endregion

namespace Matchnet.Web.Applications.MembersOnline.Controls
{
    public partial class MembersOnlineFilter20 : FrameworkControl, IMembersOnlineFilter
    {
        protected string changeVoteMessage;
        #region IMembersOnlineFilget implementation

        public System.Web.UI.HtmlControls.HtmlSelect SELGenderMask { get { return GenderMask; } }
        public System.Web.UI.HtmlControls.HtmlSelect SELRegionID { get { return RegionID; } }
        public System.Web.UI.HtmlControls.HtmlSelect SELLanguageMask { get { return LanguageMask; } }
        public DropDownList DDLDistance { get { return ddlDistance; } }
        public System.Web.UI.HtmlControls.HtmlInputText TXTAgeMin { get { return AgeMin; } }
        public System.Web.UI.HtmlControls.HtmlInputText TXTAgeMax { get { return AgeMax; } }
        public PlaceHolder PLCanguage { get { return plcLanguage; } }

        public System.Web.UI.WebControls.RangeValidator VALAgeMinRange { get { return AgeMinRange; } }
        public System.Web.UI.WebControls.RangeValidator VALAgeMaxRange { get { return AgeMaxRange; } }
        public System.Web.UI.WebControls.RequiredFieldValidator VALAgeMinReq { get { return AgeMinReq; } }
        public System.Web.UI.WebControls.RequiredFieldValidator VALAgeMaxReq { get { return AgeMaxReq; } }
        public System.Web.UI.WebControls.ValidationSummary VALSummary { get { return valSummary; } }
        public Matchnet.Web.Framework.Txt TXTOnlineNow { get { return null; } }
        public Matchnet.Web.Framework.Txt TXTShow { get { return txtShow; } }
        public Matchnet.Web.Framework.Txt TXTFrom { get { return txtFrom; } }
        public Matchnet.Web.Framework.Txt TXTAges { get { return txtAges; } }
        public Matchnet.Web.Framework.Txt TXTTo { get { return txtTo; } }
        public Matchnet.Web.Framework.Txt TXTWhoSpeak { get { return txtWhoSpeak; } }
        public System.Web.UI.WebControls.CompareValidator VALCompare { get { return valCompare; } }
        public Matchnet.Web.Framework.Ui.FormElements.FrameworkButton BTNSearch { get { return btnSearch; } }

        public PickRegionNew PrSearchRegion { get { return prSearchRegion; } }
        public Anthem.HyperLink LnkLocation { get { return lnkLocation; } }
        public Anthem.PlaceHolder PlcDistance { get { return plcDistance; } }
        public Anthem.PlaceHolder PlcAreaCodes { get { return plcAreaCodes; } }
        public Anthem.PlaceHolder PlcCollege { get { return plcCollege; } }

        public FrameworkControl ThisControl { get { return this; } }

        MembersOnlineFilterHandler _handler = null;
        #endregion


        public int intRegionID
        {
            get
            {

                return _handler.intRegionID;
            }
        }

        public int GetGenderMask()
        {
            return _handler.GetGenderMask();
        }

        public int intAgeMin
        {
            get { return Matchnet.Conversion.CInt(AgeMin.Value); }
        }

        public int intAgeMax
        {
            get { return Matchnet.Conversion.CInt(AgeMax.Value); }
        }

        public int intLanguageMask
        {
            get { return Matchnet.Conversion.CInt(LanguageMask.Value); }
        }

        public int intDistance
        {
            get { return Conversion.CInt(ddlDistance.SelectedValue); }
        }

        private void Page_Init(object sender, System.EventArgs e)
        {
            try
            {
                int memberID = (g.Member != null && g.Member.MemberID > 0) ? g.Member.MemberID : Constants.NULL_INT;
                _handler = new MembersOnlineFilterHandler(g, this);
                _handler.BindGender();
                _handler.BindLanguage();
                _handler.SetAge();

                if (MembersOnlineManager.Instance.IsE2MOLEnabled(g.Brand))
                {
                    plcRegion.Visible = false;
                    plcRegionE2.Visible = true;
                    _handler.BindDistance();
                    _handler.BindLocation(IsPostBack);
                }
                else
                {
                    plcRegion.Visible = true;
                    plcRegionE2.Visible = false;
                    _handler.BindRegion();
                }

                if (Request["Postback"] == null)
                {
                    if (!IsPostBack)
                    {
                        if ((int)WebConstants.COMMUNITY_ID.Cupid == g.Brand.Site.Community.CommunityID)
                        {	// Default the filter criteria differently for cupid.co.il
                            GenderMask.SelectedIndex = 0;	// Anyone

                            // Anywhere
                            if (this.RegionID.Value != null && this.RegionID.Value != string.Empty)
                            {
                                RegionID.SelectedIndex = -1;
                                RegionID.Items.FindByValue(Constants.NULL_INT.ToString()).Selected = true;
                            }
                        }
                    }
                }
            }

            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        private void Page_Load(object sender, System.EventArgs e)
        {
            try
            {
                _handler.DoValidation();
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }


        private void btnSearch_Click(object sender, System.EventArgs e)
        {
            try
            {
                _handler.SetSession();

                string url = Request.RawUrl;
                if (url.IndexOf("Postback") < 0)
                {
                    url += ((Request.RawUrl.IndexOf("?") >= 0) ? "&" : "?") + "Postback=1";
                }
                g.Transfer(url);
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        private void prSearchRegion_RegionSaved(EventArgs e)
        {
            IEnumerator areaCodeEnumerator = prSearchRegion.GetAreaCodes();

            bool done = false;

            for (Int32 i = 1; i <= 6; i++)
            {
                if (!done)
                {
                    done = !areaCodeEnumerator.MoveNext();
                }

                g.MembersOnlineSearchPreferences["AreaCode" + i] = done ? Constants.NULL_INT.ToString() : (Conversion.CInt(areaCodeEnumerator.Current)).ToString();
            }

            g.MembersOnlineSearchPreferences["RegionID"] = prSearchRegion.RegionID.ToString();
            g.MembersOnlineSearchPreferences["SchoolID"] = prSearchRegion.SchoolID.ToString();
            g.MembersOnlineSearchPreferences["SearchTypeID"] = ((int)prSearchRegion.SearchType).ToString();

            _handler.SetSession();
            _handler.BindLocation(IsPostBack);
        }

        private void prSearchRegion_Closed(EventArgs e)
        {
            prSearchRegion.RegionID = Conversion.CInt(g.MembersOnlineSearchPreferences["RegionID"]);
            prSearchRegion.SearchType = (SearchType)Enum.Parse(typeof(SearchType), g.MembersOnlineSearchPreferences["SearchTypeID"].ToString());
        }

        private void prSearchRegion_RegionReset(EventArgs e)
        {
            prSearchRegion.RegionID = g.Member.GetAttributeInt(g.Brand, "RegionID");
        }

        public void SetFilterSession()
        {
            _handler.SetSession();
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            this.Load += new System.EventHandler(this.Page_Load);
            this.Init += new System.EventHandler(this.Page_Init);
            prSearchRegion.RegionSaved += new Matchnet.Web.Framework.Ui.FormElements.PickRegionNew.RegionSavedEventHandler(prSearchRegion_RegionSaved);
            prSearchRegion.Closed += new Matchnet.Web.Framework.Ui.FormElements.PickRegionNew.ClosedEventHandler(prSearchRegion_Closed);
            prSearchRegion.RegionReset += new Matchnet.Web.Framework.Ui.FormElements.PickRegionNew.RegionResetEventHandler(prSearchRegion_RegionReset);
        }
        #endregion

    }
}