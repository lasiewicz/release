﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Ui.FormElements;
using Matchnet.Web.Applications.UserNotifications.Controls;
using Matchnet.UserNotifications.ValueObjects;

namespace Matchnet.Web.Applications.UserNotifications
{
    public partial class Notifications : UserNotificationsControl
    {

        private DateTime date = DateTime.Now.Date;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsNotificationsEnabled)

            {
                if (IsLoggedIn)
                {
                    if (Page.IsPostBack)
                    {
                    }
                    else
                    {
                    }

                    this.repeaterNotifications.DataSource = this.GetUserNotficationViewObjects(g.Member.MemberID);
                    this.repeaterNotifications.DataBind();
                }
                else
                {
                    g.Transfer("/Applications/Logon/Logon.aspx?DestinationURL=" + HttpUtility.UrlEncode(Request.RawUrl));
                }
            }
            else
            { 
                g.Transfer("/Applications/Home/default.aspx");
            }
        }

        public void rptNotifications_OnItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                UserNotificationViewObject un = e.Item.DataItem as UserNotificationViewObject;
                FrameworkLiteral thumbnail = e.Item.FindControl("imgThumbnail") as FrameworkLiteral;
                FrameworkLiteral literalActivityAction = e.Item.FindControl("literalUserNotification") as FrameworkLiteral;
                FrameworkLiteral lastTimeUserNotification = e.Item.FindControl("lastTimeUserNotification") as FrameworkLiteral;
                Literal omnitureTag = e.Item.FindControl("omnitureTag") as Literal;

                if (e.Item.ItemIndex == 0 || !date.Equals(un.Created.Date))
                {
                    date = un.Created.Date;
                    string dateStr = date.Date.ToString("MMMM dd");
                    if (date.Date == DateTime.Today)
                    {
                        dateStr = g.GetResource("TXT_TODAY", this);
                    }
                    else if (date.Date == DateTime.Today.AddDays(-1))
                    {
                        dateStr = g.GetResource("TXT_YESTERDAY",this);
                    }
                    FrameworkLiteral dateHeader=e.Item.FindControl("dateHeader") as FrameworkLiteral;
                    dateHeader.Visible = true;
                    dateHeader.Text = "<li class=\"\">"+dateStr+"</li>";
                }

                thumbnail.Text = un.Thumbnail;
                literalActivityAction.Text = un.Text;
                lastTimeUserNotification.Text = un.Timestamp;
                omnitureTag.Text = un.OmnitureTag;   
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            //omniture
            base.OnPreRender(e);
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
        }

        #region Private Methods
        
        #endregion
    }
}
