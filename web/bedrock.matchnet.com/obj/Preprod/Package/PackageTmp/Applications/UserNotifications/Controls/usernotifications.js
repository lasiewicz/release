﻿__addToNamespace__('spark.notifications', {
    configs: [],
    getConfig: function(idKey, idValue) {
        var config = null;
        for (var idx = 0; idx < spark.notifications.configs.length; idx++) {
            if (spark.notifications.configs[idx][idKey] && spark.notifications.configs[idx][idKey] == idValue) {
                config = spark.notifications.configs[idx];
            }
        }
        return config;
    },
    addConfig: function(config) {
        if (null != config) spark.util.addItem(spark.notifications.configs, config);
        return spark.notifications.configs;
    },
    totalNotifications: function(config) {
        return config.newNotifications.length + config.oldNotifications.length;
    },
    onlineIndicatorIds: [],
    __loadOnlineIndicator__: function() {
        jQuery.ajax({
            type: "POST",
            url: "/Applications/API/UserNotifications.asmx/GetNewNotificationsNumber",
            data: "{brandID:" + spark.notifications.BrandId + "," +
                "memberID:" + spark.notifications.MemberId + "}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            timeout:10000,
              success: function(msg, status, onlineIndicatorId) {
                //TODO:Upgrade jQuery so we can use parseJSON
                //eval the jason response
                var newNotifications = eval("(" + msg.d + ")");
                jQuery.each(spark.notifications.onlineIndicatorIds, function(key, value) {
                    var onlineElement = jQuery("#" + value);
                    if (onlineElement.length > 0) {
                        var spans = onlineElement.children("a").children(".s-icon-news");
                        if (spans.length > 0) {
                            spans.remove();
                        }
                        if (newNotifications.newNotifications != "0") {
                            onlineElement.children("a").prepend('<span class="spr s-icon-news spr-superscript"><span>' + newNotifications.newNotifications + '</span></span>');
                        } else {
                            onlineElement.children("a").prepend('<span class="spr s-icon-news"><span></span></span>');
                        }
                    }
                });
                window.setTimeout("if (!isOnlineTimeLimitReached) spark.notifications.__loadOnlineIndicator__()", spark.notifications.getTimeInterval()); //update every 60 seconds                
            }
        });
    },
    loadOnlineIndicator: function() {
        spark.notifications.__loadOnlineIndicator__();
    },
    __loadNotificationsWithConfig__: function(evt, pageSize, startIdx, setAsViewed, realtime) {
        var startIndex = (startIdx && startIdx > 0) ? startIdx : 0;
        var config = spark.notifications.getConfig('ClickId', evt.currentTarget.id);
        jQuery.ajax({
            type: "POST",
            url: "/Applications/API/UserNotifications.asmx/GetUserNotifications",
            data: "{brandID:" + spark.notifications.BrandId + "," +
                "memberID:" + spark.notifications.MemberId + "," +
                "startIdx:" + startIndex + "," +
                "pageSize:" + pageSize + "," +
                "setAsViewed:" + setAsViewed + "}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            timeout: 10000,
            success: function(msg, status) {
                spark.notifications.parseNotifications(msg, status, config);
                if (realtime) {
                    window.setTimeout("if (!isOnlineTimeLimitReached) spark.notifications.loadNotificationsWithConfig({currentTarget:{id:\"" + evt.currentTarget.id + "\"}},spark.notifications.totalNotifications(spark.notifications.getConfig(\"ClickId\",\"" + evt.currentTarget.id + "\"))+10,false,true)", spark.notifications.getTimeInterval()); //update every 60 seconds
                }
            }
        });
    },
    loadNotificationsWithConfig: function(evt, pageSize, setAsViewed, realtime) {
        spark.notifications.__loadNotificationsWithConfig__(evt, pageSize, null, setAsViewed, realtime);
    },
    parseNotifications: function(msg, status, config) {
        //TODO:Upgrade jQuery so we can use parseJSON
        //eval the jason response
        var notifications = eval("(" + msg.d + ")");
        var currentTotal = notifications[0].length;
        var total = notifications[1];
        var showViewMore = (total > currentTotal || total == 0);
        if (total == 0) {
            jQuery("#seeNotifications").removeAttr("href").attr("name", "seeNotifications");
        }
        //separate json into maps by status
        config.newNotifications = [];
        config.oldNotifications = [];
        jQuery.each(notifications[0], function(idx, value) {
            if (!value.Viewed) {
                spark.util.addItem(config.newNotifications, value);
            } else {
                spark.util.addItem(config.oldNotifications, value);
            }
        });

        //this function will write the html for each notification
        config.innerText = '';
        jQuery.each(config.newNotifications, function(i, v) {
            spark.notifications.makeList(v, config);
        });
        jQuery.each(config.oldNotifications, function(i, v) {
            spark.notifications.makeList(v, config);
        });

        var lastItemInnerText = '<li id="' + config.ListId + '-lastli" class="item view-more">';
        lastItemInnerText += (spark.notifications.totalNotifications(config) > 0) ? config.ViewMoreText : spark.notifications.BrowseMoreText;
        lastItemInnerText += '</li>';
        if (showViewMore) { config.innerText += lastItemInnerText; }

        var list = jQuery("#" + config.ListId);
        list.empty().append(config.innerText);
        delete config['innerText'];

        if (spark.notifications.totalNotifications(config) > 0 && config.Carousel) {
            config.Carousel();
        } else {
            list.parent().css("visibility", "visible");
        }
    },
    makeList: function(val, config) {
        var notification = val;
        var innerText = '';
        innerText += '<li ';
        innerText += ' class="' + config.ListClass;
        if (config.ListStyle) {
            innerText += '" style="' + config.ListStyle;
        }
        var suppressMember = jQuery(notification.Text).attr("suppress")+'';
        if (suppressMember != 'true') {
            innerText += '" title="' + notification.MemberName;
        }
        innerText += '" ot="' + notification.OmnitureTag + '">';
        innerText += notification.Thumbnail;
        innerText += '<span class="' + config.MessageClass + '">' + notification.Text + '</span>';
        if (notification.Timestamp) { innerText += '<span class="' + config.TimestampClass + '">' + notification.Timestamp + '</span>'; }
        innerText += '</li>';
        config.innerText += innerText;
    },
    getTimeInterval: function() { return 60000; },
    __init__: function() {}
});
