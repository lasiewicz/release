﻿using System.Text;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Configuration.ValueObjects;
using Matchnet.List.ValueObjects;
using Matchnet.Member.ValueObjects.Interfaces;
using Matchnet.UserNotifications.ValueObjects;
using Matchnet.Web.Applications.UserNotifications.Controls;
using Matchnet.Web.Framework.Managers;
using Matchnet.Web.Framework.Ui.BasicElements;
using Matchnet.Web.Framework;
using Matchnet.Search.ServiceAdapters;
using Matchnet.Search.ValueObjects;
using System.Web.UI;
using Matchnet.Web.Framework.Util;
using Matchnet.Purchase.ServiceAdapters;
using System.Reflection;
using System;
using System.Collections.Generic;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.List.ServiceAdapters;
using Matchnet.List.ValueObjects;
using Spark.CloudStorage;
using NotificationType = Matchnet.UserNotifications.ValueObjects.NotificationType;
using Matchnet.Web.Analytics;
using System.Diagnostics;
using Matchnet.Web.Interfaces;
using Matchnet.Content.ServiceAdapters.Links;

namespace Matchnet.Web.Applications.UserNotifications
{
    public class UserNotificationManager : AbstractManagerBaseClass
    {
        private string _OmnitureSourceName = "Newsfeed";
        private string _OmniturePageName = "Home - Default";
        private ListManager _listManager = new ListManager();

        public string OmniturePageName
        {
            get { return _OmniturePageName; }
            set { _OmniturePageName = value; }
        }

        public string OmnitureSourceName
        {
            get { return _OmnitureSourceName; }
            set { _OmnitureSourceName = value; }
        }

        public void MarkNewsfeedAsViewed(int memberId, Brand brand)
        {
            try
            {
                UserNotificationParams uParams = new UserNotificationParams();
                uParams.MemberId = memberId;
                uParams.SiteId = brand.Site.SiteID;
                uParams.CommunityId = brand.Site.Community.CommunityID;
                uParams.SetAsViewed = true;

                Matchnet.UserNotifications.ServiceAdapters.UserNotificationsServiceSA.Instance.GetUserNotifications(uParams, 0, 1);
            }
            catch (Exception ex)
            {
                //log error, this should not effect the caller
                this.LoggingManager.LogException(ex, null, brand, "UserNotificationManager.MarkNewsfeedAsViewed()");
            }
        }

        public List<UserNotificationViewObject> GetUserNotficationViewObjects(int memberId, Brand brand, List.ServiceAdapters.List list, int startNum, int pageSize, bool setAsViewed, ContextGlobal g, bool useDataPointCache, bool refreshDataPointCache)
        {
            List<UserNotificationViewObject> sortedViewObjects = new List<UserNotificationViewObject>();
            try
            {
                List<UserNotificationViewObject> viewObjects = new List<UserNotificationViewObject>();

                UserNotificationParams uParams = new UserNotificationParams();
                uParams.MemberId = memberId;
                uParams.SiteId = brand.Site.SiteID;
                uParams.CommunityId = brand.Site.Community.CommunityID;
                uParams.SetAsViewed = setAsViewed;

                if (useDataPointCache)
                {
                    //this uses a different cache to support pagination
                    viewObjects = Matchnet.UserNotifications.ServiceAdapters.UserNotificationsServiceSA.Instance.GetUserNotificationsWithDataPoints(uParams, startNum, pageSize, refreshDataPointCache);
                }
                else
                {
                    viewObjects = Matchnet.UserNotifications.ServiceAdapters.UserNotificationsServiceSA.Instance.GetUserNotifications(uParams, startNum, pageSize);
                }

                if (viewObjects != null)
                {
                    foreach (UserNotificationViewObject viewObject in viewObjects)
                    {
                        UserNotificationViewObject unvo = viewObject;
                        if (list != null)
                        {
                            Int32 temp = 0;
                            System.Collections.ArrayList favs = list.GetListMembers(HotListCategory.Default, brand.Site.Community.CommunityID, brand.Site.SiteID, 1, 100, out temp);
                            if (favs.Contains(unvo.CreatorId))
                            {
                                unvo = UserNotificationFactory.Instance.ConvertToFavoritesNotification(unvo, g);
                            }
                        }
                        unvo.Timestamp = DateTime.Now.ToString();
                        sortedViewObjects.Add(unvo);
                    }
                    sortedViewObjects.Sort();
                }
            }
            catch (Exception ex)
            {
                //log error, exception should not be thrown to caller, will just return null
                this.LoggingManager.LogException(ex, null, brand, "UserNotificationManager.GetUserNotficationViewObjects()");
            }
            return sortedViewObjects;
        }

        public string GetThumbnailLink(object resourceControl, UserNotificationViewObject unvo, Member.ServiceAdapters.Member owner, Member.ServiceAdapters.Member creator, Brand brand)
        {
            var resourceManager = new ResourceManager(brand);
            var thumbnailFile = unvo.ThumbnailFile;
            var memberName = unvo.MemberName;
            var profileLink = Matchnet.Web.Framework.Ui.BreadCrumbHelper.MakeViewProfileLink(Matchnet.Web.Framework.Ui.BreadCrumbHelper.EntryPoint.NotificationCreate, unvo.CreatorId, 0, null, 0, false, (int)Matchnet.Web.Framework.Ui.BreadCrumbHelper.PremiumEntryPoint.NoPremium, "", owner.MemberID);
            profileLink = FrameworkGlobals.AppendParamToURL(profileLink, Omniture.GetActionURLParams(WebConstants.PageIDs.ViewProfile, WebConstants.Action.ProfilePhoto, _OmniturePageName, _OmnitureSourceName));

            var baseText = resourceManager.GetResource("IMAGE_HTML", resourceControl);

            /*if (thumbnailFile.IndexOf("img") < 0 && SettingsManager.GetSettingBool(SettingConstants.ENABLE_MEMBER_PHOTOS_DISPLAY_FROM_CLOUD, brand))
            {
                var sparkCloudStorageClient = new Spark.CloudStorage.Client(brand.Site.Community.CommunityID,
                                                                            brand.Site.SiteID, RuntimeSettings.Instance);

                thumbnailFile = sparkCloudStorageClient.GetFullCloudImagePath(thumbnailFile, FileType.MemberPhoto, false);
            }*/
            
            bool notificationRequiresSubscriptionAndNonSub = NotificationRequiresSubscriptionForDetails(unvo.NotificationType, brand) && !owner.IsPayingMember(brand.Site.SiteID);

            if (notificationRequiresSubscriptionAndNonSub || !MemberPhotoDisplayManager.Instance.IsPhotosAvailable(owner, creator, brand))
            {
                bool isMale = FrameworkGlobals.IsMaleGender(creator, brand);
                thumbnailFile = MemberPhotoDisplayManager.Instance.GetNoPhotoFile(NoPhotoImageType.ThumbV2, isMale);
                memberName = "";
            }

            if (notificationRequiresSubscriptionAndNonSub)
            {
                string link = GetNotificationLink(unvo, owner, brand, WebConstants.Action.ProfilePhoto);
                return string.Format(baseText, link, thumbnailFile, memberName);
            }
            else
            {
                return string.Format(baseText, profileLink, thumbnailFile, memberName);
            }
        }

        public bool NotificationRequiresSubscriptionForDetails(Matchnet.UserNotifications.ValueObjects.NotificationType notificationType, Brand brand)
        {
            if(notificationType == NotificationType.ECardedYou || notificationType == NotificationType.FlirtedYou || 
                notificationType == NotificationType.EmailedYou || notificationType == NotificationType.IMedYou)
            {
                return true;
            }
            else if (notificationType == NotificationType.ViewedYourProfile && _listManager.IsViewedYourProfileListASubscriberFeature(brand))
            {
                return true;
            }

            return false;
        }

        public string GetNotificationLink(UserNotificationViewObject unvo, Member.ServiceAdapters.Member owner, Brand brand, WebConstants.Action actionSource)
        {
            var link = string.Empty;
            var profileLink = getProfileLink(unvo);
            
            switch(unvo.NotificationType)
            {
                case NotificationType.IMedYou:
                case NotificationType.ECardedYou:
                    link = "/Applications/Email/MailBox.aspx";
                    link = FrameworkGlobals.AppendParamToURL(link, Omniture.GetActionURLParams(WebConstants.PageIDs.MailBox, actionSource, _OmniturePageName, _OmnitureSourceName));
                    break;
                case NotificationType.NewMatchesAdded:
                    link = "/Applications/Search/SearchResults.aspx?SearchOrderBy=1";
                    link = FrameworkGlobals.AppendParamToURL(link, Omniture.GetActionURLParams(WebConstants.PageIDs.SearchResults, actionSource, _OmniturePageName, _OmnitureSourceName));
                    break;
                case NotificationType.NewMembersInLocation:
                    link = "/Applications/QuickSearch/QuickSearchResults.aspx?SearchOrderBy=1";
                    link = FrameworkGlobals.AppendParamToURL(link, Omniture.GetActionURLParams(WebConstants.PageIDs.QuickSearchResults, actionSource, _OmniturePageName, _OmnitureSourceName));
                    break;
                case NotificationType.AnswerTodaySparksNudge:
                    link = "/Applications/QuestionAnswer/Home.aspx";
                    link = FrameworkGlobals.AppendParamToURL(link, Omniture.GetActionURLParams(WebConstants.PageIDs.QuestionAnswerHome, actionSource, _OmniturePageName, _OmnitureSourceName));
                    break;
                case NotificationType.UploadPhotoNudge:
                    link = "/Applications/MemberProfile/MemberPhotoEdit.aspx";
                    link = FrameworkGlobals.AppendParamToURL(link, Omniture.GetActionURLParams(WebConstants.PageIDs.MemberPhotoEdit, actionSource, _OmniturePageName, _OmnitureSourceName));
                    break;
                case NotificationType.SubscriptionExpired:
                    link = "/Applications/Subscription/Subscribe.aspx?prtid=835";
                    break;
                case NotificationType.SubscriptionRenewal:
                    link = "/Applications/MemberServices/AutoRenewalSettings.aspx";
                    link = FrameworkGlobals.AppendParamToURL(link, Omniture.GetActionURLParams(WebConstants.PageIDs.AutoRenewalSettings, actionSource, _OmniturePageName, _OmnitureSourceName));
                    break;
                case NotificationType.FavoriteUploadedPhoto:
                    link = Matchnet.Web.Framework.Ui.BreadCrumbHelper.AppendParamToProfileLink(profileLink, WebConstants.URL_PARAMETER_NAME_PROFILETAB + "=Photo");
                    link = FrameworkGlobals.AppendParamToURL(link, Omniture.GetActionURLParams(WebConstants.PageIDs.ViewProfile, actionSource, _OmniturePageName, _OmnitureSourceName));
                    break;
                case NotificationType.FavoriteAnsweredQandA:
                    link = Matchnet.Web.Framework.Ui.BreadCrumbHelper.AppendParamToProfileLink(profileLink, WebConstants.URL_PARAMETER_NAME_PROFILETAB + "=Questions");
                    link = FrameworkGlobals.AppendParamToURL(link, Omniture.GetActionURLParams(WebConstants.PageIDs.ViewProfile, actionSource, _OmniturePageName, _OmnitureSourceName));
                    break;
                case NotificationType.EmailedYou:
                case NotificationType.FlirtedYou:
                    if (owner.IsPayingMember(brand.Site.SiteID) && unvo.DataPoints.ContainsKey(UserNotificationDataPointConstants.MAIL_MESSAGE_ID) && unvo.DataPoints.ContainsKey(UserNotificationDataPointConstants.MAIL_MESSAGE_FOLDER_ID))
                    {
                        link = string.Format("/Applications/Email/ViewMessage.aspx?MemberMailID={0}&MemberFolderID={1}",
                            unvo.DataPoints[UserNotificationDataPointConstants.MAIL_MESSAGE_ID],
                            unvo.DataPoints[UserNotificationDataPointConstants.MAIL_MESSAGE_FOLDER_ID]);
                        link = FrameworkGlobals.AppendParamToURL(link, Omniture.GetActionURLParams(WebConstants.PageIDs.ViewMessage, actionSource, _OmniturePageName, _OmnitureSourceName));
                    }
                    else
                    {
                        link = "/Applications/Email/MailBox.aspx";
                        link = FrameworkGlobals.AppendParamToURL(link, Omniture.GetActionURLParams(WebConstants.PageIDs.MailBox, actionSource, _OmniturePageName, _OmnitureSourceName));
                    }
                    break;
                case NotificationType.AllAccessEmailedYou:
                    if(unvo.DataPoints.ContainsKey(UserNotificationDataPointConstants.MAIL_MESSAGE_ID) && unvo.DataPoints.ContainsKey(UserNotificationDataPointConstants.MAIL_MESSAGE_FOLDER_ID))
                    {
                        link = string.Format("/Applications/Email/ViewMessage.aspx?MemberMailID={0}&MemberFolderID={1}",
                            unvo.DataPoints[UserNotificationDataPointConstants.MAIL_MESSAGE_ID],
                            unvo.DataPoints[UserNotificationDataPointConstants.MAIL_MESSAGE_FOLDER_ID]);
                        link = FrameworkGlobals.AppendParamToURL(link, Omniture.GetActionURLParams(WebConstants.PageIDs.ViewMessage, actionSource, _OmniturePageName, _OmnitureSourceName));
                    }
                    else
                    {
                        link = profileLink;
                        link = FrameworkGlobals.AppendParamToURL(link, Omniture.GetActionURLParams(WebConstants.PageIDs.ViewProfile, actionSource, _OmniturePageName, _OmnitureSourceName));
                    }
                    break;
                case NotificationType.ViewedYourProfile:
                    if (!_listManager.IsViewedYourProfileListAvailableToMember(owner, brand))
                    {
                        link = FrameworkGlobals.GetSubscriptionLink(false, (int)PurchaseReasonType.HotlistViewedYourProfile);
                    }
                    else
                    {
                        link = profileLink;
                        link = FrameworkGlobals.AppendParamToURL(link, Omniture.GetActionURLParams(WebConstants.PageIDs.ViewProfile, actionSource, _OmniturePageName, _OmnitureSourceName));
                    }
                    break;
                default:
                    link = profileLink;
                    link = FrameworkGlobals.AppendParamToURL(link, Omniture.GetActionURLParams(WebConstants.PageIDs.ViewProfile, actionSource, _OmniturePageName, _OmnitureSourceName));
                    break;
            }

            return link;
        }

        public string GetNotificationButtonText(object resourceControl, UserNotificationViewObject unvo, Member.ServiceAdapters.Member owner, Brand brand)
        {
            var resourceConstant = string.Empty;
            var buttonText = string.Empty;

            switch(unvo.NotificationType)
            {
                case NotificationType.ViewedYourProfile:
                case NotificationType.AddedYouAsFavorite:
                case NotificationType.MutualYes:
                case NotificationType.LikedFreeText:
                case NotificationType.LikedAnswer:
                case NotificationType.LikedPhoto:
                case NotificationType.FavoriteUpdatedProfile:
                    resourceConstant = "BUTTON_TEXT_VIEW_PROFILE";
                    break;
                case NotificationType.AllAccessEmailedYou:
                    resourceConstant = "BUTTON_TEXT_REPLY_NOW";
                    break;
                case NotificationType.EmailedYou:
                case NotificationType.FlirtedYou:
                case NotificationType.IMedYou:
                case NotificationType.ECardedYou:
                    resourceConstant = "BUTTON_TEXT_READ_MESSAGE";
                    break;
                case NotificationType.FavoriteUploadedPhoto:
                    resourceConstant = "BUTTON_TEXT_VIEW_PHOTOS";
                    break;
                case NotificationType.AnswerTodaySparksNudge:
                case NotificationType.FavoriteAnsweredQandA:
                    resourceConstant = "BUTTON_TEXT_ANSWER_QNA";
                    break;
                case NotificationType.NewMatchesAdded:
                    resourceConstant = "BUTTON_TEXT_VIEW_MATCHES";
                    break;
                case NotificationType.NewMembersInLocation:
                    resourceConstant = "BUTTON_TEXT_VIEW_MEMBERS";
                    break;
                case NotificationType.UpdateProfileNudge:
                    resourceConstant = "BUTTON_TEXT_UPDATE_PROFILE";
                    break;
                case NotificationType.UploadPhotoNudge:
                    resourceConstant = "BUTTON_TEXT_ADD_PHOTOS";
                    break;
                case NotificationType.SubscriptionExpired:
                case NotificationType.SubscriptionRenewal:
                    resourceConstant = "BUTTON_TEXT_RENEW_NOW";
                    break;
            }

            if(!string.IsNullOrEmpty(resourceConstant))
            {
                buttonText = new ResourceManager(brand).GetResource(resourceConstant, resourceControl);
            }
            
            return buttonText;
        }
       
        public string GetNotificationText(object resourceControl, UserNotificationViewObject unvo, Member.ServiceAdapters.Member owner, Brand brand)
        {
            var notificationText = string.Empty;

            try
            {
                var resourceManager = new ResourceManager(brand);
                var resourceKey = unvo.ResourceKey;
                var profileLink = getProfileLink(unvo);
                profileLink = FrameworkGlobals.AppendParamToURL(profileLink, Omniture.GetActionURLParams(WebConstants.PageIDs.ViewProfile, WebConstants.Action.ViewPageLink, _OmniturePageName, _OmnitureSourceName));
                bool notificationRequiresSubscriptionForDetails = NotificationRequiresSubscriptionForDetails(unvo.NotificationType, brand);
                if (notificationRequiresSubscriptionForDetails)
                {
                    //reset the resource key, because it might be that it was set to the non-sub version but the member is now a sub or reversed
                    if (owner.IsPayingMember(brand.Site.SiteID))
                    {
                        switch (unvo.NotificationType)
                        {
                            case NotificationType.EmailedYou:
                                resourceKey = unvo.DataPoints.ContainsKey(UserNotificationDataPointConstants.NOTIFICATION_FROM_FAVORITE) ? "USER_NOTIFICATION_FAVORITE_EMAILED_YOU_MESSAGE_TXT" : "USER_NOTIFICATION_EMAILED_YOU_MESSAGE_TXT";
                                break;
                            case NotificationType.ECardedYou:
                                resourceKey = unvo.DataPoints.ContainsKey(UserNotificationDataPointConstants.NOTIFICATION_FROM_FAVORITE) ? "USER_NOTIFICATION_FAVORITE_ECARDED_YOU_MESSAGE_TXT" : "USER_NOTIFICATION_ECARDED_YOU_MESSAGE_TXT";
                                break;
                            case NotificationType.FlirtedYou:
                                resourceKey = unvo.DataPoints.ContainsKey(UserNotificationDataPointConstants.NOTIFICATION_FROM_FAVORITE) ? "USER_NOTIFICATION_FAVORITE_FLIRTED_YOU_MESSAGE_TXT" : "USER_NOTIFICATION_FLIRTED_YOU_MESSAGE_TXT";
                                break;
                            case NotificationType.IMedYou:
                                resourceKey = unvo.DataPoints.ContainsKey(UserNotificationDataPointConstants.NOTIFICATION_FROM_FAVORITE) ? "USER_NOTIFICATION_FAVORITE_IMED_YOU_MESSAGE_TXT" : "USER_NOTIFICATION_IMED_YOU_MESSAGE_TXT";
                                break;
                            case NotificationType.ViewedYourProfile:
                                resourceKey = unvo.DataPoints.ContainsKey(UserNotificationDataPointConstants.NOTIFICATION_FROM_FAVORITE) ? "USER_NOTIFICATION_FAVORITE_VIEWED_YOU_MESSAGE_TXT" : "USER_NOTIFICATION_VIEWED_YOU_MESSAGE_TXT";
                                break;
                        }
                    }
                    else
                    {
                        switch (unvo.NotificationType)
                        {
                            case NotificationType.EmailedYou:
                                resourceKey = "NON_SUB_USER_NOTIFICATION_EMAILED_YOU_MESSAGE_TXT";
                                break;
                            case NotificationType.ECardedYou:
                                resourceKey = "NON_SUB_USER_NOTIFICATION_ECARDED_YOU_MESSAGE_TXT";
                                break;
                            case NotificationType.FlirtedYou:
                                resourceKey = "NON_SUB_USER_NOTIFICATION_FLIRTED_YOU_MESSAGE_TXT";
                                break;
                            case NotificationType.IMedYou:
                                resourceKey = "NON_SUB_USER_NOTIFICATION_IMED_YOU_MESSAGE_TXT";
                                break;
                            case NotificationType.ViewedYourProfile:
                                resourceKey = "NON_SUB_USER_NOTIFICATION_VIEWED_YOU_MESSAGE_TXT";
                                break;
                        }
                    }
                }
                
                var baseText = resourceManager.GetResource(resourceKey, resourceControl);
                
                switch (unvo.NotificationType)
                {
                    case NotificationType.NewMatchesAdded:
                        var newMatchesLink = GetNotificationLink(unvo, owner, brand, WebConstants.Action.ViewPageLink);
                        if(unvo.DataPoints.ContainsKey(UserNotificationDataPointConstants.NUMBER_NEW_MATCHES))
                        {
                            notificationText = string.Format(baseText, unvo.DataPoints[UserNotificationDataPointConstants.NUMBER_NEW_MATCHES], newMatchesLink);
                        }
                        else
                        {
                            notificationText = string.Format(resourceManager.GetResource("USER_NOTIFICATION_NEW_MATCHES_ADDED_MESSAGE_TXT", resourceControl), newMatchesLink);
                        }
                        break;
                    case NotificationType.NewMembersInLocation:
                        var newMembersLink = GetNotificationLink(unvo, owner, brand, WebConstants.Action.ViewPageLink);
                        if (unvo.DataPoints.ContainsKey(UserNotificationDataPointConstants.NUMBER_NEW_MATCHES))
                        {
                            notificationText = string.Format(baseText, unvo.DataPoints[UserNotificationDataPointConstants.NUMBER_NEW_MATCHES], newMembersLink);
                        }
                        else
                        {
                            notificationText = string.Format(resourceManager.GetResource("USER_NOTIFICATION_NEW_MEMBERS_IN_LOCATION_MESSAGE_TXT", resourceControl), newMembersLink);
                        }
                        break;
                    case NotificationType.FavoriteUpdatedProfile:
                        var pronoun = getGenderPronoun(resourceControl, brand, unvo);
                        notificationText = string.Format(baseText, profileLink, unvo.MemberName, pronoun);
                        break;
                    case NotificationType.FavoriteUploadedPhoto:
                        var photolink = GetNotificationLink(unvo, owner, brand, WebConstants.Action.ViewPageLink);
                        notificationText = string.Format(baseText, profileLink, unvo.MemberName, photolink);
                        break;
                    case NotificationType.FavoriteAnsweredQandA:
                        var questionsLink = GetNotificationLink(unvo, owner, brand, WebConstants.Action.ViewPageLink);
                        notificationText = string.Format(baseText, profileLink, unvo.MemberName, questionsLink);
                        break;
                    case NotificationType.AddedYouAsFavorite:
                        var emailLink = "/Applications/Email/Compose.aspx?PrtId=834&MemberId=" + unvo.CreatorId.ToString();
                        emailLink = FrameworkGlobals.AppendParamToURL(emailLink, Omniture.GetActionURLParams(WebConstants.PageIDs.Compose, WebConstants.Action.ViewPageLink, _OmniturePageName, _OmnitureSourceName));
                        notificationText = string.Format(baseText, profileLink, unvo.MemberName, emailLink);
                        break;
                    case NotificationType.LikedAnswer:
                        if (unvo.DataPoints.ContainsKey(UserNotificationDataPointConstants.QNA_QUESTION_ID))
                        {
                            var questionLink = "/Applications/QuestionAnswer/Question.aspx?questid=" + unvo.DataPoints[UserNotificationDataPointConstants.QNA_QUESTION_ID];
                            questionLink = FrameworkGlobals.AppendParamToURL(questionLink, Omniture.GetActionURLParams(WebConstants.PageIDs.Question, WebConstants.Action.ViewPageLink, _OmniturePageName, _OmnitureSourceName));
                            notificationText = string.Format(baseText, profileLink, unvo.MemberName, questionLink);
                        }
                        break;
                    case NotificationType.EmailedYou:
                    case NotificationType.FlirtedYou:
                        var mailLink = GetNotificationLink(unvo, owner, brand, WebConstants.Action.ViewPageLink);

                        if (owner.IsPayingMember(brand.Site.SiteID))
                        {
                            notificationText = string.Format(baseText, profileLink, unvo.MemberName, mailLink);
                        }
                        else
                        {
                            notificationText = string.Format(baseText, mailLink);
                        }
                        
                        break;
                    case NotificationType.AllAccessEmailedYou:
                        var aaMailLink = GetNotificationLink(unvo, owner, brand, WebConstants.Action.ViewPageLink);
                        notificationText = string.Format(baseText, profileLink, unvo.MemberName, aaMailLink);
                        break;
                    case NotificationType.AnswerTodaySparksNudge:
                        string qnaLink = GetNotificationLink(unvo, owner, brand, WebConstants.Action.ViewPageLink);
                        notificationText = string.Format(baseText, qnaLink);
                        break;
                    case NotificationType.UpdateProfileNudge:
                        notificationText = string.Format(baseText, profileLink, profileLink);
                        break;
                    case NotificationType.UploadPhotoNudge:
                        string editPhotoLink = GetNotificationLink(unvo, owner, brand, WebConstants.Action.ViewPageLink);
                        notificationText = string.Format(baseText, editPhotoLink, profileLink);
                        break;
                    case NotificationType.SubscriptionRenewal:
                        DateTime expiration = owner.GetAttributeDate(brand, WebConstants.ATTRIBUTE_NAME_SUBSCRIPTIONEXPIRATIONDATE, DateTime.MinValue);
                        double days = Math.Ceiling((expiration - DateTime.Now).TotalDays);
                        string daysTxt = (days > 1) ? resourceManager.GetResource("PLURAL_DAYS", resourceControl) : resourceManager.GetResource("SINGLE_DAY", resourceControl);
                        string autoRenewalLink = GetNotificationLink(unvo, owner, brand, WebConstants.Action.ViewPageLink);
                        notificationText = string.Format(baseText, days, daysTxt, autoRenewalLink);
                        break;
                    case NotificationType.SubscriptionExpired:
                        string subscribeLink = GetNotificationLink(unvo, owner, brand, WebConstants.Action.ViewPageLink);
                        notificationText = string.Format(baseText, subscribeLink);
                        break;
                    case NotificationType.ECardedYou:
                    case NotificationType.IMedYou:
                        var mailLinkEcard = GetNotificationLink(unvo, owner, brand, WebConstants.Action.ViewPageLink);
                        if (owner.IsPayingMember(brand.Site.SiteID))
                        {
                            notificationText = string.Format(baseText, profileLink, unvo.MemberName, mailLinkEcard);
                        }
                        else
                        {
                            notificationText = string.Format(baseText, mailLinkEcard);
                        }
                        break;
                    case NotificationType.ViewedYourProfile:
                        if (notificationRequiresSubscriptionForDetails && !owner.IsPayingMember(brand.Site.SiteID))
                        {
                            string subscribeLinkViewYou = GetNotificationLink(unvo, owner, brand, WebConstants.Action.ViewPageLink);
                            notificationText = string.Format(baseText, subscribeLinkViewYou);
                        }
                        else
                        {
                            notificationText = string.Format(baseText, profileLink, unvo.MemberName);
                        }
                        break;
                    default:
                        notificationText = string.Format(baseText, profileLink, unvo.MemberName);
                        break;
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            //omniture for hotlist links specified directly in resources
            string omnitureHotlistPage = Omniture.GetActionURLParams(WebConstants.PageIDs.View, WebConstants.Action.ViewPageLink, _OmniturePageName, _OmnitureSourceName);
            notificationText = notificationText.Replace("{{omnitureViewLink}}", omnitureHotlistPage);
            notificationText = notificationText.Replace("{omnitureViewLink}", omnitureHotlistPage);


            return notificationText;
        }
        
        private string getProfileLink(UserNotificationViewObject unvo)
        {
            return Framework.Ui.BreadCrumbHelper.MakeViewProfileLink(Framework.Ui.BreadCrumbHelper.EntryPoint.NotificationCreate, unvo.CreatorId, 0, null, 0, false, (int)Matchnet.Web.Framework.Ui.BreadCrumbHelper.PremiumEntryPoint.NoPremium, "", unvo.OwnerId);
        }

        private string getGenderPronoun(object resourceControl, Brand brand, UserNotificationViewObject unvo)
        {
            int gendermask;
            var pronounConstant = "TXT_THEIR";

            if (unvo.DataPoints.ContainsKey(UserNotificationDataPointConstants.CREATOR_GENDERMASK) && int.TryParse(unvo.DataPoints[UserNotificationDataPointConstants.CREATOR_GENDERMASK], out gendermask))
            {
                pronounConstant = ((gendermask & Matchnet.Lib.ConstantsTemp.GENDERID_FEMALE) == Matchnet.Lib.ConstantsTemp.GENDERID_FEMALE) ? "TXT_HER" : "TXT_HIS";
            }

            return new ResourceManager(brand).GetResource(pronounConstant, resourceControl);
        }

        public string GetNotificationSubheading(object resourceControl, UserNotificationViewObject unvo, Member.ServiceAdapters.Member owner, Brand brand)
        {
            if (NotificationRequiresSubscriptionForDetails(unvo.NotificationType, brand) && !owner.IsPayingMember(brand.Site.SiteID))
            {
                return string.Empty;
            }
            
            var text = string.Empty;
            var stringBuilder = new StringBuilder();

            if(unvo.DataPoints.Count > 0)
            {
                if(unvo.DataPoints.ContainsKey(UserNotificationDataPointConstants.CREATOR_AGE))
                {
                    stringBuilder.Append(unvo.DataPoints[UserNotificationDataPointConstants.CREATOR_AGE]);
                    stringBuilder.Append(", ");
                }
                if (unvo.DataPoints.ContainsKey(UserNotificationDataPointConstants.CREATOR_MARITAL_STATUS))
                {
                    stringBuilder.Append(unvo.DataPoints[UserNotificationDataPointConstants.CREATOR_MARITAL_STATUS]);
                    stringBuilder.Append(", ");
                }
                if (unvo.DataPoints.ContainsKey(UserNotificationDataPointConstants.CREATOR_GENDERMASK))
                {
                    stringBuilder.Append(unvo.DataPoints[UserNotificationDataPointConstants.CREATOR_GENDERMASK]);
                    stringBuilder.Append(", ");
                }
                if (unvo.DataPoints.ContainsKey(UserNotificationDataPointConstants.CREATOR_LOCATION))
                {
                    stringBuilder.Append(unvo.DataPoints[UserNotificationDataPointConstants.CREATOR_LOCATION]);
                }
                if(stringBuilder.Length >0)
                {
                    text = stringBuilder.ToString();
                }
            }

            return text;
        }
    }
}
