﻿using Matchnet.Configuration.ValueObjects;
using Spark.SAL;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Web.Framework;
using Matchnet.List.ServiceAdapters;
using Matchnet.List.ValueObjects;
using Matchnet.Lib;
using Matchnet.Web.Framework.Ui.BasicElements;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.UserNotifications.ValueObjects;
using Matchnet.Web.Applications.QuickSearch;

namespace Matchnet.Web.Applications.UserNotifications.Controls
{
    public partial class UserNotificationsControl : FrameworkControl
    {
        Stack<UserNotification> unStack = new Stack<UserNotification>();
        public bool IsLoggedIn
        {
            get { return null != g.Member && g.Member.MemberID > 0; }
        }

        public string ClickableElementId { get; set; }

        public string OnlineIndicatorElementId { get; set; }

        public int BrandId
        {
            get { return g.Brand.BrandID; }
        }

        public string Direction
        {
            get
            {
                return g.Brand.Site.Direction.ToString();
            }
        }

        public int MemberId
        {
            get { return g.Member.MemberID; }
        }

        public int TimeInterval
        {
            get
            {
                int interval = SettingsManager.GetSettingInt(SettingConstants.USER_NOTIFICATIONS_TIME_INTERVAL, g.Brand);
                return interval;
            }
        }

        public bool IsNotificationsEnabled
        {
            get
            {
                return UserNotificationFactory.IsUserNotificationsEnabled(_g);
            }
        }

        public string GetText(string key, bool useParent, bool javascriptEncode)
        {
            string resText = g.GetResource(key, ((useParent) ? this.LoadControl("/Applications/UserNotifications/Controls/UserNotificationsControl.ascx") : this));
            if (javascriptEncode)
            {
                resText = EncodeForJS(resText);
            }
            return resText;
        }

        public string EncodeForJS(string s)
        {
            return FrameworkGlobals.JavaScriptEncode(s);
        }

        public int GetCountOfNewUserNotficationViewObjects(int memberId)
        {
            UserNotificationParams uParams = new UserNotificationParams();
            uParams.MemberId = memberId;
            uParams.SiteId = g.Brand.Site.SiteID;
            uParams.CommunityId = g.Brand.Site.Community.CommunityID;
            int count = Matchnet.UserNotifications.ServiceAdapters.UserNotificationsServiceSA.Instance.GetCountOfNewUserNotifications(uParams);
            return count;
        }

        public int GetTotalUserNotificationViewObjectsNumber(int memberId)
        {
            UserNotificationParams uParams = new UserNotificationParams();
            uParams.MemberId = memberId;
            uParams.SiteId = g.Brand.Site.SiteID;
            uParams.CommunityId = g.Brand.Site.Community.CommunityID;
            uParams.SetAsViewed = false;
            int count = Matchnet.UserNotifications.ServiceAdapters.UserNotificationsServiceSA.Instance.GetTotalNotificationsNumber(uParams);
            return count;
        }

        public List<UserNotificationViewObject> GetUserNotficationViewObjects(int memberId)
        {
            return GetUserNotficationViewObjects(memberId, -1, -1, false);
        }

        public List<UserNotificationViewObject> GetUserNotficationViewObjects(int memberId, int startNum, int pageSize, bool setAsViewed)
        {
            List<UserNotificationViewObject> sortedViewObjects = new List<UserNotificationViewObject>();
            try
            {
                Matchnet.Web.Framework.Ui.BasicElements.LastLoginDate lDate = this.LoadControl("/Framework/Ui/BasicElements/LastLoginDate.ascx") as Matchnet.Web.Framework.Ui.BasicElements.LastLoginDate;
                UserNotificationsControl ctrl = this.LoadControl("/Applications/UserNotifications/Controls/UserNotificationsControl.ascx") as UserNotificationsControl;
                List<UserNotificationViewObject> viewObjects = new List<UserNotificationViewObject>();

                //Member.ServiceAdapters.Member member = Member.ServiceAdapters.MemberSA.Instance.GetMember(memberId, Matchnet.Member.ServiceAdapters.MemberLoadFlags.None);
                //Matchnet.List.ServiceAdapters.List list = ListSA.Instance.GetList(memberId);
                //Int32 temp = 0;
                //System.Collections.ArrayList favs = list.GetListMembers(HotListCategory.Default, g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, 1, 100, out temp);

                ////populate notifications
                //Random r = getTotallyRandom(memberId);
                //for (int i = startNum; i < 12; i++)
                //{
                //    Member.ServiceAdapters.Member mem = Member.ServiceAdapters.MemberSA.Instance.GetMember((int)favs[r.Next(favs.Count)], Matchnet.Member.ServiceAdapters.MemberLoadFlags.None);
                //    UserNotification un = getNextNotification(member);
                //    un.NotificationCreator = mem;
                //    un.TimeStamp = lDate;
                //    int days = r.Next(28) + 1;
                //    un.Viewed = (r.Next(1) == 1);
                //    un.Created = new DateTime(2010, 4, days);
                //    un.ctrl = ctrl;
                //    un._g = g;
                //    viewObjects.Add(un.CreateViewObject());
                //}

                UserNotificationParams uParams = new UserNotificationParams();
                uParams.MemberId = memberId;
                uParams.SiteId = g.Brand.Site.SiteID;
                uParams.CommunityId = g.Brand.Site.Community.CommunityID;
                uParams.SetAsViewed = setAsViewed;

                viewObjects = Matchnet.UserNotifications.ServiceAdapters.UserNotificationsServiceSA.Instance.GetUserNotifications(uParams, startNum, pageSize);
                if (null != viewObjects)
                {
                    foreach (UserNotificationViewObject viewObject in viewObjects)
                    {
                        UserNotificationViewObject unvo = viewObject;
                        if (null != g.List)
                        {
                            Int32 temp = 0;
                            System.Collections.ArrayList favs = g.List.GetListMembers(HotListCategory.Default, g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, 1, 100, out temp);
                            if (favs.Contains(unvo.CreatorId))
                            {
                                unvo=UserNotificationFactory.Instance.ConvertToFavoritesNotification(unvo, g);
                            }
                        }
                        lDate.LastLoginDateValue = unvo.Created;
                        unvo.Timestamp = lDate.BuildLastLoginDate();
                        sortedViewObjects.Add(unvo);
                    }
                    sortedViewObjects.Sort();
                }
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
            return sortedViewObjects;
        }

        private UserNotification getNextNotification(Member.ServiceAdapters.Member m)
        {
            if (unStack.Count == 0)
            {
                unStack.Push(new ViewedYourProfileUserNotification());
                unStack.Push(new FlirtedYouUserNotification());
                unStack.Push(new EmailedYouUserNotification());
                unStack.Push(new AddedYouAsFavoriteUserNotification());
                unStack.Push(new ECardedYouUserNotification());
                unStack.Push(new IMedYouUserNotification());
                unStack.Push(new MutualYesUserNotification());
                unStack.Push(new FavoriteViewedYourProfileUserNotification());
                unStack.Push(new FavoriteFlirtedYouUserNotification());
                unStack.Push(new FavoriteEmailedYouUserNotification());
                unStack.Push(new FavoriteAddedYouAsFavoriteUserNotification());
                unStack.Push(new FavoriteECardedYouUserNotification());
                unStack.Push(new FavoriteIMedYouUserNotification());
                unStack.Push(new FavoriteMutualYesUserNotification());
                unStack.Push(new FavoriteUpdatedProfileUserNotification());
                unStack.Push(new FavoriteUploadedPhotoUserNotification());
                unStack.Push(new FavoriteAnsweredQandAUserNotification());
                unStack.Push(new NewMatchesAddedUserNotification());
                unStack.Push(new NewMembersInLocationUserNotification());
                unStack.Push(new UpdateProfileNudgeUserNotification());
                unStack.Push(new UploadPhotosNudgeUserNotification());
                unStack.Push(new AnswerTodaySparksNudgeUserNotification());
                unStack.Push(new SubscriptionRenewalUserNotification());
            }
            UserNotification un = unStack.Pop();
            un.NotificationOwner = m;
            return un;
        }

        private Random getTotallyRandom(int memberID)
        {
            int d = Math.Abs((memberID * _g.RandomNumber) / (int)DateTime.Now.Ticks);
            return new Random(d);
        }
    }
}
