﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.UserNotifications.ValueObjects;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Ui.BasicElements;
using Matchnet.Web.Framework.Ui.FormElements;
using NotificationType = Matchnet.UserNotifications.ValueObjects.NotificationType;
using Matchnet.Member.ServiceAdapters;

namespace Matchnet.Web.Applications.UserNotifications.Controls
{
    public partial class Newsfeed : FrameworkControl
    {
        private DateTime date = DateTime.Now.Date;
        private UserNotificationManager userNotificationManager = new UserNotificationManager();
        private LastLoginDate lDate;

        public int NumberOfLoadedNotifications { get; set; }
        public int NumberOfLoadedNonSystemNotifications { get; set; }
        public string OmniturePageName { get; set; }

        protected override void OnInit(EventArgs e)
        {            
            base.OnInit(e);
        }
        
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        public void LoadNewsfeed(int startNum, int pageSize, bool refreshDataPointCache)
        {
            //initialize user notification manager with omniture for tracking
            userNotificationManager.OmniturePageName = string.IsNullOrEmpty(OmniturePageName) ? "Home - Default" : OmniturePageName;
            userNotificationManager.OmnitureSourceName = "Newsfeed";

            lDate = LoadControl("/Framework/Ui/BasicElements/LastLoginDate.ascx") as LastLoginDate;
            
            if (g.Member != null)
            {
                var notifications = userNotificationManager.GetUserNotficationViewObjects(g.Member.MemberID, g.Brand,
                                                                                          g.List, startNum, pageSize, true, g, true, refreshDataPointCache);

                NumberOfLoadedNotifications = notifications.Count;
                NumberOfLoadedNonSystemNotifications = (from n in notifications where !n.SystemNotification select n).Count();
                repeaterNotifications.DataSource = notifications;
                repeaterNotifications.DataBind();
            }

        }

        public void rptNotifications_OnItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var un = e.Item.DataItem as UserNotificationViewObject;
                var notificationText = userNotificationManager.GetNotificationText(this, un, g.Member, g.Brand);
                var plcItemContainer = e.Item.FindControl("plcItemContainer") as PlaceHolder;

                if (notificationText != string.Empty)
                {
                    plcItemContainer.Visible = true;

                    var thumbnail = e.Item.FindControl("imgThumbnail") as FrameworkLiteral;
                    var literalActivityAction = e.Item.FindControl("literalUserNotification") as FrameworkLiteral;
                    var lastTimeUserNotification = e.Item.FindControl("lastTimeUserNotification") as FrameworkLiteral;
                    var omnitureTag = e.Item.FindControl("omnitureTag") as Literal;
                    var literalUserNotificationSubheading = e.Item.FindControl("literalUserNotificationSubheading") as FrameworkLiteral;
                    var lnkButton = e.Item.FindControl("lnkButton") as HyperLink;
                    var plcSpriteNewMember = e.Item.FindControl("plcSpriteNewMember") as PlaceHolder;
                    var plcSpriteUpdatedProfile = e.Item.FindControl("plcSpriteUpdatedProfile") as PlaceHolder;
                    var plcSpriteAllAccess = e.Item.FindControl("plcSpriteAllAccess") as PlaceHolder;
                    var plcSpriteNewPhoto = e.Item.FindControl("plcSpriteNewPhoto") as PlaceHolder;
                    var plcSystemIcon = e.Item.FindControl("plcSystemIcon") as PlaceHolder;
                    Member.ServiceAdapters.Member creator = un.CreatorId > 0 ? MemberSA.Instance.GetMember(un.CreatorId, MemberLoadFlags.None) : null;
                    
                    plcSpriteNewMember.Visible = (un.NotificationType == NotificationType.NewMatchesAdded || un.NotificationType == NotificationType.NewMembersInLocation || un.NotificationType == NotificationType.NewMemberPromo);
                    plcSpriteUpdatedProfile.Visible = (un.NotificationType == NotificationType.FavoriteUpdatedProfile);
                    plcSpriteAllAccess.Visible = (un.NotificationType == NotificationType.AllAccessEmailedYou);
                    plcSpriteNewPhoto.Visible = (un.NotificationType == NotificationType.FavoriteUploadedPhoto); 

                    if (un.NotificationType == NotificationType.NewMatchesAdded || un.NotificationType == NotificationType.NewMembersInLocation || un.NotificationType == NotificationType.NewMemberPromo)
                    {
                        plcSpriteNewMember.Visible = true;
                    }
                    if (un.NotificationType == NotificationType.FavoriteUpdatedProfile)
                    {
                        plcSpriteUpdatedProfile.Visible = true;
                    }

                    lnkButton.NavigateUrl = userNotificationManager.GetNotificationLink(un, g.Member, g.Brand, WebConstants.Action.ViewPageButton);
                    lnkButton.Text = userNotificationManager.GetNotificationButtonText(this, un, g.Member, g.Brand);

                    thumbnail.Visible = !un.SystemNotification;
                    plcSystemIcon.Visible = un.SystemNotification;
                    
                    if (thumbnail.Visible && !string.IsNullOrEmpty(un.ThumbnailFile))
                    {
                        thumbnail.Text = userNotificationManager.GetThumbnailLink(this, un, g.Member, creator, g.Brand);
                    }
                    
                    omnitureTag.Text = un.OmnitureTag;
                    literalActivityAction.Text = notificationText;
                    literalUserNotificationSubheading.Text = userNotificationManager.GetNotificationSubheading(this, un, g.Member, g.Brand);

                    lDate.LastLoginDateValue = un.Created;
                    lastTimeUserNotification.Text = lDate.BuildLastLoginDate();
                }
                else
                {
                    plcItemContainer.Visible = false;
                }
            }
        }

        
    }
}