﻿using System;
using System.Collections;
using System.Data;
using System.Web.UI.WebControls;
using System.Text;
using System.Xml;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Search.Interfaces;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Ui.FormElements;
using Matchnet.Web.Framework.Util;
using Matchnet.Lib;
using Spark.SAL;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.MemberSlideshow.ServiceAdapters;
using SearchType = Spark.SAL.SearchType;

namespace Matchnet.Web.Applications.Search
{
    public partial class SearchPreferencesNew20 : FrameworkControl
    {
        protected Matchnet.Web.Framework.Title ttlPreferences;
        protected DropDownList ddlGender;
        protected DropDownList ddlSeekingGender;
        protected TextBox tbAgeMin;
        protected TextBox tbAgeMax;
        protected Anthem.PlaceHolder plcDistance;
        protected Anthem.PlaceHolder plcAreaCodes;
        protected Anthem.PlaceHolder plcCollege;
        protected DropDownList ddlDistance;
        protected Anthem.HyperLink lnkLocation;
        protected CheckBox cbHasPhoto;
        protected CheckBox cbRamahAlum;
        protected SearchPreferenceRepeater rptBasics;
        protected Repeater rptAdditionalSections;
        protected Button btnSave;
        protected Anthem.PlaceHolder plcPickRegion;
        protected PickRegionNew prSearchRegion;
        protected Matchnet.Web.Framework.MultiValidator AgeMinValidator;
        protected Matchnet.Web.Framework.MultiValidator AgeMaxValidator;
        protected MemberSearch _memberSearch;

        private void Page_Load(object sender, System.EventArgs e)
        {
            try
            {
                //Load data for search being edited
                if (g.Member == null)
                {
                    _memberSearch = MemberSearchCollection.Load(Constants.NULL_INT, g.Brand).PrimarySearch;
                }
                else
                {
                    _memberSearch = MemberSearchCollection.Load(g.Member.MemberID, g.Brand).PrimarySearch;
                }

                //Bind data to controls
                bindGender();
                bindAge();
                bindDistance();
                bindLocation();
                bindHasPhoto();
                bindRamahAlum();
                bindXMLPreferences();

                //keyword
                if (SearchUtil.IsKeywordMatchesSearchPrefEnabled(g.Brand.BrandID, g.Brand.Site.SiteID, g.Brand.Site.Community.CommunityID))
                {
                    phKeyword.Visible = true;
                    if (!string.IsNullOrEmpty(_memberSearch.KeywordSearch) && _memberSearch.KeywordSearch.Trim() != "")
                    {
                        txtKeyword.Text = _memberSearch.KeywordSearch;
                    }
                }

                btnSave.Text = g.GetResource("BTN_SAVE_PREFERENCES", this);

                if (g.BreadCrumbTrailHeader != null)
                {
                    g.BreadCrumbTrailHeader.SetTwoLinkCrumb(g.GetResource("NAV_PREFERENCES", this),
                                                            g.AppPage.App.DefaultPagePath);

                }
                if (g.BreadCrumbTrailFooter != null)
                {
                    g.BreadCrumbTrailFooter.SetTwoLinkCrumb(g.GetResource("NAV_PREFERENCES", this),
                                                           g.AppPage.App.DefaultPagePath);
                }

                //g.SetPageTitle(g.GetResource("TXT_PAGE_TITLE", this));
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            Page.Validate();

            if (Page.IsValid)
            {
                if (g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.BlackSingles)
                {
                    int genderMask = (int)Enum.Parse(typeof(Gender), DropDownListGenderSingleSelect.SelectedValue);
                    if ((genderMask & (int)ConstantsTemp.GENDERID_SEEKING_MALE) == (int)ConstantsTemp.GENDERID_SEEKING_MALE)
                    {
                        _memberSearch.Gender = Gender.Female;
                    }
                    else
                    {
                        _memberSearch.Gender = Gender.Male;
                    }

                    if ((genderMask & (int)ConstantsTemp.GENDERID_SEEKING_FEMALE) == (int)ConstantsTemp.GENDERID_SEEKING_FEMALE)
                    {
                        _memberSearch.SeekingGender = Gender.Female;
                    }
                    else
                    {
                        _memberSearch.SeekingGender = Gender.Male;
                    }
                }
                else
                {
                    _memberSearch.Gender = (Gender)Enum.Parse(typeof(Gender), ddlGender.SelectedValue);
                    _memberSearch.SeekingGender = (Gender)Enum.Parse(typeof(Gender), ddlSeekingGender.SelectedValue);
                }

                Int32 minAge = Conversion.CInt(tbAgeMin.Text);
                Int32 maxAge = Conversion.CInt(tbAgeMax.Text);

                if (minAge > maxAge)
                {
                    tbAgeMin.Text = tbAgeMax.Text;
                    tbAgeMax.Text = minAge.ToString();

                    minAge = maxAge;
                    maxAge = Conversion.CInt(tbAgeMax.Text);
                }

                _memberSearch.Age.MinValue = minAge;
                _memberSearch.Age.MaxValue = maxAge;

                _memberSearch.Distance = Conversion.CInt(ddlDistance.SelectedValue);
                
                // if this is a photo required site, we want to set this search pref manually
                if (Convert.ToBoolean(RuntimeSettings.GetSetting("PHOTO_REQUIRED_SITE", g.Brand.Site.Community.CommunityID,
                    g.Brand.Site.SiteID, g.Brand.BrandID)))
                {
                    _memberSearch.HasPhoto = true;
                }
                else
                {
                    _memberSearch.HasPhoto = cbHasPhoto.Checked;
                }

                string oldKeyword = _memberSearch.KeywordSearch;
                _memberSearch.KeywordSearch = "";
                if (SearchUtil.IsKeywordMatchesSearchPrefEnabled(g.Brand.BrandID, g.Brand.Site.SiteID, g.Brand.Site.Community.CommunityID))
                {
                    if (g.GetResource("TXT_KEYWORD_EXAMPLE", this.txtKeyword).ToLower() != txtKeyword.Text.Trim())
                    {
                        _memberSearch.KeywordSearch = txtKeyword.Text.Trim();
                        if (_memberSearch.KeywordSearch.Length > SearchUtil.KEYWORD_SEARCH_MAXCHAR_LENGTH)
                        {
                            _memberSearch.KeywordSearch = _memberSearch.KeywordSearch.Substring(0, SearchUtil.KEYWORD_SEARCH_MAXCHAR_LENGTH);
                        }
                        
                        //if removed keywords, change sort to newest
                        if (string.IsNullOrEmpty(_memberSearch.KeywordSearch))
                        {
                            g.SearchPreferences["searchorderby"] = Convert.ToInt32(QuerySorting.JoinDate).ToString();
                        }
                        //if changing keyword values, default sort to keyword relevance
                        else if (oldKeyword != _memberSearch.KeywordSearch)
                        {
                            g.SearchPreferences["searchorderby"] = Convert.ToInt32(QuerySorting.KeywordRelevance).ToString();
                        }
                    }
                }


                if (g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.JDate)
                {

                    if (cbxRamahDate.Checked)
                        (_memberSearch[Preference.GetInstance("RamahAlum")] as MemberSearchPreferenceInt).Value = 1;
                    else
                        (_memberSearch[Preference.GetInstance("RamahAlum")] as MemberSearchPreferenceInt).Value = Constants.NULL_INT;

                }


                _memberSearch.Save();
                try
                {
                    bool enableMemberSlideshow = Conversion.CBool(RuntimeSettings.GetSetting("ENABLE_SLIDESHOW", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID));
                    bool enableConfigurableSlideshow = Conversion.CBool(RuntimeSettings.GetSetting("ENABLE_CONFIGURABLE_SLIDESHOW", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID));
                    if (enableMemberSlideshow && !enableConfigurableSlideshow)
                    {
                        MemberSlideshowSA.Instance.RemoveFromCache(g.Member.MemberID, g.Brand.Site.Community.CommunityID);
                    }
                }
                catch (Exception ex)
                { g.ProcessException(ex); }
                g.Transfer("/Applications/Search/SearchResults.aspx");
            }
        }


        private void bindGender()
        {
            if (g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.BlackSingles)
            {
                PlaceHolderGenderMultiSelects.Visible = false;
                PlaceHolderGenderSingleSelect.Visible = true;

                ListItem itemMaleFemale = new ListItem(g.GetResource("MALE_SEEK_FEMALE"), (ConstantsTemp.GENDERID_MALE + ConstantsTemp.GENDERID_SEEKING_FEMALE).ToString());
                ListItem itemFemaleMale = new ListItem(g.GetResource("FEMALE_SEEK_MALE"), (ConstantsTemp.GENDERID_FEMALE + ConstantsTemp.GENDERID_SEEKING_MALE).ToString());

                DropDownListGenderSingleSelect.Items.Add(itemMaleFemale);
                DropDownListGenderSingleSelect.Items.Add(itemFemaleMale);

                int gender, seekingGender;
                if (_memberSearch.Gender == Gender.Male)
                {
                    gender = ConstantsTemp.GENDERID_MALE;
                }
                else
                {
                    gender = ConstantsTemp.GENDERID_FEMALE;
                }
                if (_memberSearch.SeekingGender == Gender.Male)
                {
                    seekingGender = ConstantsTemp.GENDERID_SEEKING_MALE;
                }
                else
                {
                    seekingGender = ConstantsTemp.GENDERID_SEEKING_FEMALE;
                }

                FrameworkGlobals.SelectItem(DropDownListGenderSingleSelect, 
                    (gender | seekingGender).ToString());
            }
            else
            {
                PlaceHolderGenderMultiSelects.Visible = true;
                PlaceHolderGenderSingleSelect.Visible = false;

                ListItem itemMale = new ListItem(g.GetResource("MALE"), ((Int32)Gender.Male).ToString());
                ListItem itemFemale = new ListItem(g.GetResource("FEMALE"), ((Int32)Gender.Female).ToString());

                ddlGender.Items.Add(itemMale);
                ddlGender.Items.Add(itemFemale);

                itemMale = new ListItem(g.GetResource("MALE"), ((Int32)Gender.Male).ToString());
                itemFemale = new ListItem(g.GetResource("FEMALE"), ((Int32)Gender.Female).ToString());

                ddlSeekingGender.Items.Add(itemMale);
                ddlSeekingGender.Items.Add(itemFemale);


                /*fixing possibility of wrong selectvalue passed 
                 * ddlGender.SelectedValue = ((Int32) _memberSearch.Gender).ToString();
                 *ddlSeekingGender.SelectedValue = ((Int32) _memberSearch.SeekingGender).ToString();
                */
                FrameworkGlobals.SelectItem(ddlGender, ((Int32)_memberSearch.Gender).ToString());
                FrameworkGlobals.SelectItem(ddlSeekingGender, ((Int32)_memberSearch.SeekingGender).ToString());
            }
        }

        private void bindAge()
        {
            tbAgeMin.Text = _memberSearch.Age.MinValue.ToString();
            tbAgeMax.Text = _memberSearch.Age.MaxValue.ToString();
        }

        private void bindDistance()
        {
            DataTable distanceOptions = Option.GetOptions(SearchUtil.GetDistanceAttribute(g), g);
            ddlDistance.DataSource = distanceOptions;
            ddlDistance.DataTextField = "Content";
            ddlDistance.DataValueField = "Value";
            ddlDistance.DataBind();

            FrameworkGlobals.SelectItem(ddlDistance, _memberSearch.Distance.ToString(), SettingsManager.GetSettingInt(SettingConstants.DEFAULT_DISTANCE_LIST_ORDER, g.Brand) - 1);
            //ddlDistance.SelectedValue = _memberSearch.Distance.ToString();
        }

        private void bindLocation()
        {
            #region Bind PickRegion Control
            if (!IsPostBack)
            {
                if (g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.JDateFR ||
                    g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.JDateUK)
                {
                    // Initially select the Region tab when opening the Search
                    // Region popup in JDate.fr and JDate.co.uk
                    prSearchRegion.SearchType = SearchType.Region;
                }
                else
                {
                    prSearchRegion.SearchType = _memberSearch.SearchType;
                }
            }

            prSearchRegion.RegionID = _memberSearch.RegionID;
            prSearchRegion.SchoolID = _memberSearch.SchoolID;

            Int32 countryRegionID = 0;

            //TODO: abstract area code stuff into the SAL.MemberSearch object
            StringBuilder sbAreaCodes = new StringBuilder();
            for (Int32 i = 1; i <= 6; i++)
            {
                MemberSearchPreferenceInt areaCode = _memberSearch[Preference.GetInstance("AreaCode" + i)] as MemberSearchPreferenceInt;
                if (areaCode != null && Conversion.CInt(areaCode.Value) > 0)
                {
                    countryRegionID = Matchnet.Content.ServiceAdapters.RegionSA.Instance.RetrieveAreaCodes()[areaCode.Value];
                    prSearchRegion.AddAreaCode(areaCode.Value.ToString(), countryRegionID);
                    sbAreaCodes.Append(areaCode.Value.ToString() + ", ");
                }
            }
            if (sbAreaCodes.Length > 0)
            {
                sbAreaCodes.Remove(sbAreaCodes.Length - 2, 2);
            }

            prSearchRegion.AreaCodeCountryRegionID = countryRegionID;
            #endregion

            lnkLocation.UpdateAfterCallBack = true;
            lnkLocation.Attributes.Add("onclick", "popUpDiv(\"searchPopupDiv\",\"16.67em\",\"-3px\");return false");
            lnkLocation.Attributes.Add("href", "#");

            plcDistance.Visible = true;
            plcDistance.UpdateAfterCallBack = true;
            plcAreaCodes.Visible = false;
            plcAreaCodes.UpdateAfterCallBack = true;
            plcCollege.Visible = false;
            plcCollege.UpdateAfterCallBack = true;

            switch (_memberSearch.SearchType)
            {
                case SearchType.PostalCode:
                case SearchType.Region:
                    lnkLocation.Text = FrameworkGlobals.GetRegionString(_memberSearch.RegionID, g.Brand.Site.LanguageID);
                    break;
                case SearchType.AreaCode:
                    plcDistance.Visible = false;
                    plcAreaCodes.Visible = true;
                    lnkLocation.Text = sbAreaCodes.ToString();
                    break;
                case SearchType.College:
                    plcDistance.Visible = false;
                    plcCollege.Visible = true;
                    MemberSearchPreferenceInt schoolIDPreference = _memberSearch[Preference.GetInstance("SchoolID")] as MemberSearchPreferenceInt;
                    lnkLocation.Text = Matchnet.Content.ServiceAdapters.RegionSA.Instance.RetrieveSchoolName(schoolIDPreference.Value).SchoolName;
                    break;
            }

            prSearchRegion.RegionName = lnkLocation.Text;
            lnkLocation.Text += g.GetResource("EDIT", this);
        }

        private void bindHasPhoto()
        {
            // if this is a photo required site, it doesn't make sense to offer this option
            if (Convert.ToBoolean(RuntimeSettings.GetSetting("PHOTO_REQUIRED_SITE", g.Brand.Site.Community.CommunityID,
                    g.Brand.Site.SiteID, g.Brand.BrandID)))
            {
                cbHasPhoto.Visible = false;
            }
            else
            {
                cbHasPhoto.Text = g.GetResource("TXT_SHOW_PHOTO_PROFILES_ONLY", this);
                cbHasPhoto.Checked = _memberSearch.HasPhoto;
            }
        }


        private void bindRamahAlum()
        {
            
            if (g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.JDate)
            {
                cbxRamahDate.Visible = true;
                cbxRamahDate.Text = g.GetResource("TXT_SHOW_RAMAH_ONLY", this);
                cbxRamahDate.Checked = ((_memberSearch[Preference.GetInstance("RamahAlum")] as MemberSearchPreferenceInt).Value == 1);
            }
        }

        private void bindXMLPreferences()
        {
            XmlDocument document = new XmlDocument();
            document.Load(MapPathSecure(TemplateSourceDirectory + @"\Search." + g.Brand.Site.SiteID + ".xml"));

            foreach (XmlNode node in document.ChildNodes[0].ChildNodes)
            {
                if (node.Name.ToLower() != "section")
                {
                    throw new Exception("Invalid node name: " + node.Name + ".  Expected 'Section'.");
                }

                switch (node.Attributes["Name"].Value.ToLower())
                {
                    case "basics":
                        rptBasics.XmlNode = node;
                        rptBasics.MemberSearch = _memberSearch;
                        rptBasics.DataBind();
                        break;
                    case "additional":
                        rptAdditionalSections.DataSource = node.ChildNodes;
                        rptAdditionalSections.DataBind();
                        break;
                }
            }
        }

        private void rptAdditionalSections_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            XmlNode node = (XmlNode)e.Item.DataItem;

            SearchPreferenceRepeater rptSection = (SearchPreferenceRepeater)e.Item.FindControl("rptSection");
            rptSection.XmlNode = node;
            rptSection.MemberSearch = _memberSearch;
            rptSection.DataBind();

            Txt txtSectionTitle = (Txt)e.Item.FindControl("txtSectionTitle");
            txtSectionTitle.ResourceConstant = node.Attributes["ResourceConstant"].Value;
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
            btnSave.Click += new EventHandler(btnSave_Click);
            rptAdditionalSections.ItemDataBound += new RepeaterItemEventHandler(rptAdditionalSections_ItemDataBound);
            prSearchRegion.RegionSaved += new Matchnet.Web.Framework.Ui.FormElements.PickRegionNew.RegionSavedEventHandler(prSearchRegion_RegionSaved);
            prSearchRegion.Closed += new Matchnet.Web.Framework.Ui.FormElements.PickRegionNew.ClosedEventHandler(prSearchRegion_Closed);
            prSearchRegion.RegionReset += new Matchnet.Web.Framework.Ui.FormElements.PickRegionNew.RegionResetEventHandler(prSearchRegion_RegionReset);
        }
        #endregion

        private void prSearchRegion_RegionSaved(EventArgs e)
        {
            //TODO: abstract area code stuff into the SAL.MemberSearch object

            IEnumerator areaCodeEnumerator = prSearchRegion.GetAreaCodes();

            bool done = false;

            for (Int32 i = 1; i <= 6; i++)
            {
                MemberSearchPreferenceInt areaCodePreference = _memberSearch[Preference.GetInstance("AreaCode" + i)] as MemberSearchPreferenceInt;

                if (!done)
                {
                    done = !areaCodeEnumerator.MoveNext();
                }

                areaCodePreference.Value = done ? Constants.NULL_INT : Conversion.CInt(areaCodeEnumerator.Current);
            }

            _memberSearch.RegionID = prSearchRegion.RegionID;
            _memberSearch.SchoolID = prSearchRegion.SchoolID;
            _memberSearch.SearchType = prSearchRegion.SearchType;
            _memberSearch.Save();

            bindLocation();
        }

        private void prSearchRegion_Closed(EventArgs e)
        {
            prSearchRegion.RegionID = _memberSearch.RegionID;
            prSearchRegion.SearchType = _memberSearch.SearchType;
        }

        private void prSearchRegion_RegionReset(EventArgs e)
        {
            prSearchRegion.RegionID = g.Member.GetAttributeInt(g.Brand, "RegionID");
        }
    }
}