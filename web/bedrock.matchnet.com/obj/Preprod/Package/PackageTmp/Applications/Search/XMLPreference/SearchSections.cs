﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace Matchnet.Web.Applications.Search.XMLPreference
{
    [Serializable]
    [XmlRoot("Sections")]
    public class SearchSections
    {
        [XmlElement("Section")]
        public List<SearchSection> SearchSectionList { get; set; }

        public SearchSection GetSection(string sectionName)
        {
            SearchSection ss = null;
            if (SearchSectionList != null)
            {
                foreach (SearchSection section in SearchSectionList)
                {
                    if (section.Name.ToLower() == sectionName.ToLower())
                        return section;
                }
            }
            return ss;
        }

        public List<SearchSectionPreference> GetPreferences(string[] sectionNames)
        {
            //TODO: Later we can add ordering and other filter conditions here to get back the right list of preferences
            List<SearchSectionPreference> preferenceList = new List<SearchSectionPreference>();
            foreach (string sectionName in sectionNames)
            {
                SearchSection ss = GetSection(sectionName);
                if (ss != null)
                {
                    foreach (SearchSectionPreference ssp in ss.SearchSectionPreferenceList)
                    {
                        preferenceList.Add(ssp);
                    }
                }
            }

            return preferenceList;
        }
    }
}
