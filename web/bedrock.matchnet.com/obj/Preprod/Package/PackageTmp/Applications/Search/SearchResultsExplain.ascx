<%@ Control Language="c#" AutoEventWireup="false" Codebehind="SearchResultsExplain.ascx.cs" Inherits="Matchnet.Web.Applications.Search.SearchResultsExplain" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>

<style type="text/css">
		#mem1 table { border:1 solid; width:650px; }
		#mem1 .m_label { font-size: 9px; text-color: gray; text-align: right; padding-left: 4px; }
		#mem1 .m_value { font-size: normal; text-color: black; text-align: left; font-weight: bold; }
		#mem1 .m_blend { width:100%; }
		#mem1 .m_bllab { width:4%; font-size: 9px;  color: #888888; }
		#mem1 .m_blval { width:7%; font-size: normal; color: black; font-weight: bold; }
		#mem1 .m_attr  { font-size: 9px; border: red 1px solid; background-color: #ffece9; padding: 1px; }
		#mem1 .m_thumb { width: 80px; height: 104px; overflow: hidden;}
</style>

<div id="mem1" class="mem1">
	<asp:Repeater ID="MemberRepeater" Runat="server">
		<ItemTemplate>

			<table border="1">
				<tr>
					<td class="m_thumb">
						<a target="_blank" href="/Applications/MemberProfile/ViewProfile.aspx?MemberID=<%# DataBinder.Eval(Container.DataItem, "MemberID")%>"><img class="m_thumb" alt="Thumb goes here" src="<%# DataBinder.Eval(Container.DataItem, "ThumbPath")%>"></a>
					</td>
					<td>
						<table>
							<tr>
								<td>
									<span>
										<b><%# DataBinder.Eval(Container.DataItem, "Username")%>, <%# DataBinder.Eval(Container.DataItem, "MemberID")%></b>
										 - <a target="_blank" href="http://lasvcsengine01:6969/command/show?type=mem&memID=<%# DataBinder.Eval(Container.DataItem, "MemberID")%>&domID=<%# DataBinder.Eval(Container.DataItem, "CommunityID")%>&mode=7">e2 Data</a>
										 - <a target="_blank" href="/Applications/Search/SearchStoreView.aspx?MemberID=<%# DataBinder.Eval(Container.DataItem, "MemberID")%>&BrandID=<%# DataBinder.Eval(Container.DataItem, "BrandID")%>">Search Data</a>
									</span>
									<table>
										<tr>
										</tr>
										<tr>
											<td class="m_label"> Active:</td>
											<td class="m_value"><%# ((DateTime) DataBinder.Eval(Container.DataItem, "ActiveDate")).ToString("yyyy-MM-dd HH:mm") %></td>
											<td class="m_label"> Reg:</td>
											<td class="m_value"><%# ((DateTime) DataBinder.Eval(Container.DataItem, "RegisterDate")).ToString("yyyy-MM-dd")%></td>
											<td class="m_label"> Photo:</td>
											<td class="m_value"><%# DataBinder.Eval(Container.DataItem, "HasPhoto")%></td>
											<td class="m_label"> Online:</td>
											<td class="m_value"><%# DataBinder.Eval(Container.DataItem, "IsOnline")%></td>
											<td class="m_label"> ColorCode:</td>
											<td class="m_value"><%# DataBinder.Eval(Container.DataItem, "ColorCode")%></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td>
									<table class="m_blend">
										<tr>
											<td class="m_bllab">Total:</td>
											<td class="m_blval"><%# DataBinder.Eval(Container.DataItem, "TotalScore")%></td>
											<td class="m_bllab">Cscz: </td>
											<td class="m_blval"><%# DataBinder.Eval(Container.DataItem, "CsczScore")%></td>
											<td class="m_bllab">Ldg:  </td>
											<td class="m_blval"><%# DataBinder.Eval(Container.DataItem, "LdgScore")%></td>
											<td class="m_bllab">Age:  </td>
											<td class="m_blval"><%# DataBinder.Eval(Container.DataItem, "AgeScore")%></td>
											<td class="m_bllab">Hgt:  </td>
											<td class="m_blval"><%# DataBinder.Eval(Container.DataItem, "HeightScore")%></td>
											<td class="m_bllab">Match:</td>
											<td class="m_blval"><%# DataBinder.Eval(Container.DataItem, "MatchScore")%></td>
											<td class="m_bllab">Act:  </td>
											<td class="m_blval"><%# DataBinder.Eval(Container.DataItem, "ActivityScore")%></td>
											<td class="m_bllab">New:  </td>
											<td class="m_blval"><%# DataBinder.Eval(Container.DataItem, "NewScore")%></td>
											<td class="m_bllab">Pop:  </td>
											<td class="m_blval"><%# DataBinder.Eval(Container.DataItem, "PopularityScore")%></td>
											<td class="m_bllab">Online:  </td>
											<td class="m_blval">TBD</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td>
									<asp:Repeater ID="TagRepeater" Runat="server">
										<ItemTemplate>
											<span class="m_attr"><%# DataBinder.Eval(Container.DataItem, "Key") %>:<%# DataBinder.Eval(Container.DataItem, "Value") %></span>
										</ItemTemplate>
									</asp:Repeater>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</ItemTemplate>
	</asp:Repeater>
</div>