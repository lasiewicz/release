namespace Matchnet.Web.Applications.Search
{
	using System;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;
	using System.Text;
	using System.Collections;

	using Matchnet.Web.Framework;
	using Matchnet.Content.ValueObjects.PageConfig;
	using Matchnet.Configuration.ServiceAdapters;
	using Matchnet.Search.ServiceAdapters;
	using Matchnet.Search.ValueObjects;
	using Matchnet.Member.ServiceAdapters;

	/// <summary>
	///		Summary description for GalleryExitPopup.
	/// </summary>
	public class GalleryExitPopup : FrameworkControl
	{
		private const Int32 PopupWidth = 639;
		private const Int32 PopupHeight = 660;
		private const String PopupProperties = "location=0,menubar=0,resizable=0,status=0,titlebar=0";
		private const Int32 ProfilesPerGallery = 9;

		private int count = 0;
		protected Repeater GalleryMemberRepeater;

		private void Page_Init(object sender, System.EventArgs e)
		{
			try
			{
				// Try a search.  Photos required.
				// NOTE:  Not enough profiles with photos in DEV, thus, don't require it in DEV.
				if (!g.IsDevMode)
					g.SearchPreferences.Add("HasPhotoFlag", "1");

				MatchnetQueryResults matchnetQueryResults = MemberSearchSA.Instance.Search(g.SearchPreferences
					, g.Brand.Site.Community.CommunityID
					, g.Brand.Site.SiteID
					, 0
					, ProfilesPerGallery);

				// Make sure we have at the required number of profiles (ProfilesPerGallery).
				if (matchnetQueryResults.Items.Count < ProfilesPerGallery)
				{
					CloseWindow();
				}
				else
				{
					ArrayList members = MemberSA.Instance.GetMembers(matchnetQueryResults.ToArrayList()
						, MemberLoadFlags.None);

					GalleryMemberRepeater.DataSource = members;
					GalleryMemberRepeater.DataBind();
				}
			}
			catch(Exception ex)
			{
				g.ProcessException(ex);
				
				CloseWindow();
			}
		}

		private void CloseWindow()
		{
			StringBuilder script = new StringBuilder();
			string scriptKey = typeof(GalleryExitPopup).FullName;

			script.Append("<script language='JavaScript'>");
			script.Append(Environment.NewLine);
			script.Append("self.close();");
			script.Append(Environment.NewLine);
			script.Append("</script>");
			script.Append(Environment.NewLine);

			g.Page.RegisterClientScriptBlock(scriptKey, script.ToString());
		}

		public void GalleryMemberRepeater_OnItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{
			count++;

			Matchnet.Web.Framework.Ui.BasicElements.GalleryMiniProfile GalleryMiniProfile = (Matchnet.Web.Framework.Ui.BasicElements.GalleryMiniProfile)e.Item.FindControl("GalleryMiniProfile");
				
			Literal rptrNewRow = (Literal)e.Item.FindControl("rptrNewRow");
			Literal rptrNewCell = (Literal)e.Item.FindControl("rptrNewCell");
			Literal rptrEndCell = (Literal)e.Item.FindControl("rptrEndCell");
			Literal rptrEndRow = (Literal)e.Item.FindControl("rptrEndRow");

			if(count % 3 == 1)
			{
				rptrNewRow.Visible = true;
				rptrNewCell.Visible = true;

				rptrEndCell.Visible = true;
			}
			else if(count % 3 == 2)
			{
				rptrNewCell.Visible = true;

				rptrEndCell.Visible = true;					
			}
			else 
			{
				rptrNewCell.Visible = true;

				rptrEndCell.Visible = true;
				rptrEndRow.Visible = true;
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Init += new System.EventHandler(this.Page_Init);
		}
		#endregion

		/// <summary>
		/// We do preliminary checking here to see if an exit popup is necessary.  
		/// </summary>
		/// <param name="g"></param>
		/// <returns></returns>
		public static Popup CreatePopup(ContextGlobal g)
		{
			// Check to see if a Gallery Exit Popup should be displayed for this site.
			// The exit popup should only appear for a Registered member who is not a Subscribed member.
			// The ExitPopup will only show on non-secure (http) pages.
			if (RuntimeSettings.GetSetting("ENABLE_GALLERY_EXIT_POPUP", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID) != "true" ||
				g.Member == null || g.Member.MemberID <= 0 || MemberPrivilegeAttr.IsCureentSubscribedMember(g) == true ||
				g.IsSecureRequest())
				return null;

			Popup popup = new Popup();

			popup.Enabled = true;
			popup.Name = "Gallery Exit Popup";
			popup.LayoutTemplate = LayoutTemplate.Popup;
			popup.Width = PopupWidth;
			popup.Height = PopupHeight;
			popup.Url = FrameworkGlobals.LinkHref("/Applications/Search/GalleryExitPopup.aspx", false);
			popup.PopupType = Matchnet.Web.Framework.Popup.PopupTypeEnum.ExitPopupType;
			popup.Properties = PopupProperties;
			//ResetOnUnloadEvent();	// Don't believe this is necessary.
			
			return popup;
		}
	}
}
