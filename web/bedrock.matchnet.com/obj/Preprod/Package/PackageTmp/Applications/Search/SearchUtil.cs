using System;
using System.Collections;
using System.Xml;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Search.Interfaces;
using Matchnet.Search.ServiceAdapters;
using Matchnet.Search.ValueObjects;
using Matchnet.Web.Framework;
using Matchnet.Web.Applications.Search.XMLPreference;
using System.IO;
using System.Xml.Serialization;
using Matchnet.Web.Framework.Managers;
using Matchnet.Web.Framework.Ui;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Web.Framework.Util;

namespace Matchnet.Web.Applications.Search
{
	/// <summary>
	/// Summary description for SearchUtil.
	/// 
	/// This utility class has been developed to assist in the front-end
	/// prototype for working with search results from the FAST service
	/// in XML format	K.Landrus 1/4/05
	/// </summary>
	public class SearchUtil
	{
        public const int KEYWORD_SEARCH_MAXCHAR_LENGTH = 200;

		public SearchUtil()
		{
		}

		public static SearchPreferenceCollection ConvertRequestToSearchPreferences(System.Collections.Specialized.NameValueCollection pRequestParams)
		{
			SearchPreferenceCollection searchPreferences = new SearchPreferenceCollection();
			if (pRequestParams != null)
			{	// It would be nice to just iterate the collection of request params adding them to the collection
				// but there are too many keys in Request.Params that do not pertain to the current query string.

				// Community
				if (pRequestParams["CommunityID"] != null)
				{
					searchPreferences.Add("DomainID", pRequestParams["CommunityID"]);
				}

				if ((pRequestParams["Gender"] != null) && (pRequestParams["Seeking"] != null))
				{
					int maskValue = 0;
					string gender = pRequestParams["Gender"].ToLower();
					string seeking = pRequestParams["Seeking"].ToLower();

					if ("male".Equals(gender) && "female".Equals(seeking))
					{	// male seeking female
						maskValue = 6;
					}
					else
					{	// female seeking male
						maskValue = 9;
					}
					searchPreferences.Add("GenderMask", maskValue.ToString());
				}

				if (pRequestParams["ZipCode"] != null)
				{
					searchPreferences.Add("ZipCode", pRequestParams["ZipCode"]);
				}

				if (pRequestParams["Distance"] != null)
				{
					searchPreferences.Add("Distance", pRequestParams["Distance"]);
				}

				if (pRequestParams["AgeMin"] != null)
				{
					searchPreferences.Add("AgeMin", pRequestParams["AgeMin"]);
				}

				if (pRequestParams["AgeMax"] != null)
				{
					searchPreferences.Add("AgeMax", pRequestParams["AgeMax"]);
				}

				if (pRequestParams["HasPhotoFlag"] != null)
				{
					searchPreferences.Add("HasPhotoFlag", pRequestParams["HasPhotoFlag"]);
				}

			}
			return searchPreferences;
		}

		/// <summary>
		/// This method transforms the search results for a Mode 1 Search (MemberIDs only)
		/// </summary>
		/// <param name="queryResults"></param>
		/// <returns></returns>
		public static XmlDocument TransformResultsToXml(MatchnetQueryResults queryResults)
		{
			XmlDocument xmlDoc = new XmlDocument();
			xmlDoc.LoadXml("<?xml version=\"1.0\"?><results></results>");

			XmlElement questionElement = xmlDoc.CreateElement("question");
			xmlDoc.DocumentElement.AppendChild(questionElement);

			XmlElement answerElement = xmlDoc.CreateElement("answer");
			xmlDoc.DocumentElement.AppendChild(answerElement);

			XmlElement element;
			foreach (MatchnetResultItem resultItem in queryResults.Items.List)
			{
				// Create the Member node
				element = xmlDoc.CreateElement("Member"); 
				answerElement.AppendChild(element);
				element.SetAttribute("id", resultItem.MemberID.ToString());
			}
			return xmlDoc;
		}

        public static int GetSearch30PageSize(ContextGlobal g)
        {
            int pageSize = 0;
            try
            {
                pageSize = Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("SEARCH30_PAGE_SIZE", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID));
            }
            catch (Exception ex)
            {
                //setting missing
            }

            if (pageSize <= 0)
            {
                pageSize = 16;
            }
            return pageSize;

        }

        public static SearchSections GetSearchPreferenceSections(int siteID, ContextGlobal g)
        {
            SearchSections sections = null;
            string sectionsCacheKey = "SearchPreferenceSections_" + siteID.ToString();

            try
            {
                //check cache
                sections = g.Cache[sectionsCacheKey] as SearchSections;

                if (sections == null)
                {
                    //load from xml
                    string xmlPath = System.Web.HttpContext.Current.Server.MapPath("/Applications/Search/Search." + siteID.ToString() + ".xml");
                    TextReader textReader = new StreamReader(xmlPath);

                    XmlSerializer deserializer = new XmlSerializer(typeof(SearchSections));
                    sections = deserializer.Deserialize(textReader) as SearchSections;

                    textReader.Close();

                    //save to cache
                    if (sections != null && sections.SearchSectionList != null)
                    {
                        g.Cache.Insert(sectionsCacheKey, sections);
                    }

                }

            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }

            return sections;
        }

        public static bool IsSearchSortByPopularityEnabled(int brandID, int siteID, int communityID)
        {
            string isEnabled = "true";
            try
            {
                isEnabled = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ENABLE_SEARCH_SORT_BY_POPULARITY", communityID, siteID, brandID);
            }
            catch (Exception ex)
            {
                //setting missing
                isEnabled = "true";
            }

            if (isEnabled.ToLower().Trim() == "true")
                return true;
            else
                return false;

        }

        //gets the distance attribute (depends on site and setting)
        public static string GetDistanceAttribute(ContextGlobal g)
        {
            //distance
            SettingsManager settingsManager = new SettingsManager();
            string distanceAttribute = settingsManager.GetSettingBool(SettingConstants.USE_E2_DISTANCE, g.Brand) ? "E2Distance" : "Distance";
            if (settingsManager.GetSettingBool(SettingConstants.USE_MINGLE_DISTANCE, g.Brand))
            {
                distanceAttribute = "MingleDistance";
            }
            return distanceAttribute;
        }

        public static bool IsSearch30ListViewEnabled(int brandID, int siteID, int communityID)
        {
            string isEnabled = "false";
            try
            {
                isEnabled = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ENABLE_SEARCH30_LISTVIEW", communityID, siteID, brandID);
            }
            catch (Exception ex)
            {
                //setting missing
                isEnabled = "false";
            }

            if (isEnabled.ToLower().Trim() == "true")
                return true;
            else
                return false;

        }

        public static bool IsReverseSearchRedesignEnabled(int brandID, int siteID, int communityID)
        {
            string isEnabled = "false";
            try
            {
                isEnabled = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ENABLE_REVERSE_SEARCH_REDESIGN", communityID, siteID, brandID);
            }
            catch (Exception ex)
            {
                //setting missing
                isEnabled = "false";
            }

            if (isEnabled.ToLower().Trim() == "true")
                return true;
            else
                return false;

        }

        /// <summary>
        /// Indicates whether Reverse Search 2.0 redesigned page will be displayed
        /// </summary>
        /// <returns></returns>
        public static bool IsDisplayingReverseSearchRedesign20(ContextGlobal g)
        {
            bool result = false;

            try
            {
                if (g != null && g.Member != null && g.AppPage.ControlName.ToLower() == "reversesearch")
                {
                    result = IsReverseSearchRedesignEnabled(g.Brand.BrandID, g.Brand.Site.SiteID, g.Brand.Site.Community.CommunityID);
                }
            }
            catch (Exception ex)
            {
                //missing setting
                result = false;
            }

            return result;
        }

        public static bool IsKeywordMatchesSearchPrefEnabled(int brandID, int siteID, int communityID)
        {
            string isEnabled = "false";
            try
            {
                isEnabled = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ENABLE_KEYWORD_MATCHES_SEARCHPREF", communityID, siteID, brandID);
            }
            catch (Exception ex)
            {
                //setting missing
                isEnabled = "false";
            }

            if (isEnabled.ToLower().Trim() == "true")
                return true;
            else
                return false;

        }

        public static bool IsKeywordHighlightEnabled(ContextGlobal g)
        {
            bool result = false;

            //TODO - do we need separate setting from keyword feature
            result = IsKeywordMatchesSearchPrefEnabled(g.Brand.BrandID, g.Brand.Site.SiteID, g.Brand.Site.Community.CommunityID);

            return result;
        }

        public static Matchnet.Search.Interfaces.SearchType GetSearchTypeForActivityLogging(Matchnet.Web.Framework.Ui.BreadCrumbHelper.EntryPoint entryPoint)
        {
            Matchnet.Search.Interfaces.SearchType searchType = Matchnet.Search.Interfaces.SearchType.None;
            switch (entryPoint)
            {
                case BreadCrumbHelper.EntryPoint.SearchResults:
                    searchType = Matchnet.Search.Interfaces.SearchType.YourMatchesWebSearch;
                    break;
                case BreadCrumbHelper.EntryPoint.HomeHeroProfileMatches:
                case BreadCrumbHelper.EntryPoint.HomeHeroProfileFilmStripMatches:
                case BreadCrumbHelper.EntryPoint.HomeProfileFilmstripWithVoting:
                    searchType = Matchnet.Search.Interfaces.SearchType.ProfileFilmStripWebSearch;
                    break;
                case BreadCrumbHelper.EntryPoint.QuickSearchResults:
                    searchType = Matchnet.Search.Interfaces.SearchType.QuickSearchWebSearch;
                    break;
                case BreadCrumbHelper.EntryPoint.KeywordSearchResults:
                    searchType = Matchnet.Search.Interfaces.SearchType.KeywordSearchWebSearch;
                    break;
                case BreadCrumbHelper.EntryPoint.ReverseSearchResults:
                    searchType = Matchnet.Search.Interfaces.SearchType.ReverseSearchWebSearch;
                    break;
                case BreadCrumbHelper.EntryPoint.AdvancedSearchResults:
                    searchType = Matchnet.Search.Interfaces.SearchType.AdvancedSearchWebSearch;
                    break;
                case BreadCrumbHelper.EntryPoint.HotLists:
                case BreadCrumbHelper.EntryPoint.PhotoGallery:
                case BreadCrumbHelper.EntryPoint.MembersOnline:
                case BreadCrumbHelper.EntryPoint.HomeHeroProfileMOL:
                case BreadCrumbHelper.EntryPoint.HomeHeroProfileFilmStripMOL:
                case BreadCrumbHelper.EntryPoint.HomeProfileFilmstripWithVotingMOL:
                case BreadCrumbHelper.EntryPoint.SlideShowPage:
                case BreadCrumbHelper.EntryPoint.MemberSlideshow:
                    searchType = Matchnet.Search.Interfaces.SearchType.None;
                    break;
                default:
                    searchType = Matchnet.Search.Interfaces.SearchType.YourMatchesWebSearch;
                    break;
            }

            return searchType;
        }
	}

    public enum EditPreferenceType
    {
        None = 0,
        AddMore = 1,
        Gender = 2,
        Dropdown = 3
    }
}
