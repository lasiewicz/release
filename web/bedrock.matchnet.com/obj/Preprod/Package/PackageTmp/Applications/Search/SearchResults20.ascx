﻿<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="result" TagName="ResultList" Src="../../Framework/Ui/BasicElements/ResultList20.ascx" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn" TagName="miniProfile" Src="/Framework/Ui/BasicElements/MiniProfile.ascx" %>
<%@ Register TagPrefix="mn1" TagName="ResultsViewType" Src="/Framework/Ui/BasicElements/ResultsViewType.ascx" %>
<%@ Register TagPrefix="cc1" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn1" TagName="ColorCodeSelect" Src="/Framework/Ui/BasicElements/ColorCodeSelect.ascx" %>
<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="SearchResults20.ascx.cs" Inherits="Matchnet.Web.Applications.Search.SearchResults20" %>

<div id="divSearchResultsPageContainer">
<%--SearchRedesign 3.0 Beta Toggler --%>
<asp:PlaceHolder ID="phSearchRedesign30BetaToggler" runat="server" Visible="false">
    <div id="beta-toggler" class="matches">
         <asp:HyperLink CssClass="beta-button-what" ID="lnkSearchRedesign30BetaToggler" runat="server">
            <span class="betawhatsthis" title="<mn:Txt ID='txt1' runat='server' ResourceConstant='TXT_WHATS_THIS' />"><mn:Txt ID="txtWhatIsThis" runat="server" ResourceConstant="TXT_WHATS_THIS" /></span>
         </asp:HyperLink>
         <div id="betalayer" class="hide editorial">
            <mn:Txt ID="htmlBetaLayerContent" runat="server" ResourceConstant="HTML_BETA_LAYER_CONTENT" ExpandImageTokens="true" />
            <p><mn:txt ID="betaLinkTag" ResourceConstant="TXT_BETA_LINK_TAG" runat="server" />
            <%--<a href="#" class="margin-light" id="lnkSearchRedesign30BetaToggler2" runat="server"><mn:txt ID="betaLink" ResourceConstant="TXT_BETA_LINK" runat="server" /></a>--%>
                <mn2:FrameworkLiteral ID="litTourLink" runat="server" ResourceConstant="LNK_BETA_TOUR"></mn2:FrameworkLiteral>
            </p>
            <mn:Txt ID="htmlBetaLayerLegal" runat="server" ResourceConstant="HTML_BETA_LAYER_LEGAL" ExpandImageTokens="true" /> 
        </div>
        
		<script type="text/javascript">
            $j(document).ready(function () {
                var $link = $j(".betawhatsthis"),
                    $layer = $j("#betalayer");

                MenuToggler($link, $layer);
                $link.click(function (e) {
                    $layer.css({ left: e.pageX + 'px', top: (e.pageY + 12) + 'px', zIndex: '1003' }).appendTo('body');
                });
            });
        </script>
    </div>
    
</asp:PlaceHolder>

<div class="header-options clearfix">
    <mn:Title runat="server" id="txtMatches" ResourceConstant="NAV_SUB_MATCHES"/>
	<div class="links">
		<mn:txt id="SearchPreferences" runat="server" href="/Applications/Search/SearchPreferences.aspx" resourceconstant="REVISE_YOUR_SEARCH_PREFERENCES" />
	</div>
	<div class="pagination">
		<asp:label id="lblListNavigationTop" Runat="server" />
	</div>
</div>

<asp:PlaceHolder runat="server"  ID="plcSector" Visible="false">
    <div id="sector-header">
        <div id="sector-text">
            <mn:Txt runat="server" ID="txtSectorText" />
        </div>
        <mn:Image FileName="btn-post-profile.gif" runat="server" ID="btnJoinNow" Visible="false" TitleResourceConstant="ALT_JOINNOW"/>
    </div>
</asp:PlaceHolder>

<asp:PlaceHolder id="plcPromotionalProfile" Runat="server" Visible="false"></asp:PlaceHolder>

	<div class="sort-display" style="border:none">
		
		<ul class="nav-rounded-tabs no-click clearfix">
		    <li class="no-tab search-type"><mn:txt runat="server" id="txtView" resourceconstant="TXT_VIEW_BY" /></li>
            <asp:Repeater id="rptSearchOption" Runat="server">
		        <ItemTemplate>
		            <li class="<asp:Literal id=litSortSpan runat=server /> tab">
			            <asp:HyperLink id="lnkSort" Runat="server" />
			            <span class="x"><asp:Literal id="litSortTitle" Runat="server" Visible="False" /></span>
		            
                
                </li>
            </ItemTemplate>
        </asp:Repeater>
        <li class="no-tab view-type image-text-pair">
            <mn1:ResultsViewType id="idResultsViewType" runat="server" />
        </li>
    </ul>
    <mn1:ColorCodeSelect id="colorCodeSelect" runat="server" />
    <input type="hidden" name="hidSearchOrderBy" runat="server" id="hidSearchOrderBy" />
</div>
<div id="results-container" class="clearfix clear-both">
    <asp:PlaceHolder ID="Results" runat="server" Visible="true">
        <result:ResultList ID="SearchResultList" runat="server" ResultListContextType="SearchResult">
        </result:ResultList>
        <%-- <mn:Txt ID="promoDiv" runat="server" ResourceConstant="TXT_PROMO_DIV" Visible="false" /> --%>
        <input type="hidden" value="<% = ChangeVoteMessage %>" name="ChangeVoteMessage" />
        <iframe tabindex="-1" name="FrameYNMVote" frameborder="0" width="1" scrolling="no"
            height="1">
            <layer name="FrameYNMVote" frameborder="0" scrolling="no" width="1" height="1"></layer>
        </iframe>
    </asp:PlaceHolder>
</div>
<div class="pagination">
    <asp:Label ID="lblListNavigationBottom" runat="server"></asp:Label>
</div>
</div>
