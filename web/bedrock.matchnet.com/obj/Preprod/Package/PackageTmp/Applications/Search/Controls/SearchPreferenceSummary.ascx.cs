﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Search.Interfaces;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Web.Framework.Managers;
using Spark.SAL;
using Matchnet.Web.Framework;
using Matchnet.Web.Applications.Search.XMLPreference;
using System.Text;
using Matchnet.Web.Framework.Util;
using System.Data;
using Matchnet.Configuration.ServiceAdapters;
using System.Web.UI.HtmlControls;
using Matchnet.Lib;
using Matchnet.MemberSlideshow.ServiceAdapters;
using SearchType = Spark.SAL.SearchType;

namespace Matchnet.Web.Applications.Search.Controls
{
    public partial class SearchPreferenceSummary : FrameworkControl
    {
        private SearchSections _SearchPreferenceSections;
        private bool _PreferenceEdited = false;

        //Preferences ready event
        public delegate void PreferencesReadyEventHandler();
        public event PreferencesReadyEventHandler PreferencesReady;

        #region Properties
        public MemberSearch MemberSearch { get; set; }
        public bool IsRegOverlayRefresh { get; set; }
        public bool IsPartialRender { get; set; }
        public bool IgnoreFilteredSearchResultsCache { get; set; }
        public bool IsSectorPage { get; set; }
        public bool PostbackRequiresPRG { get; set; }
        #endregion

        #region Event Handlers
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.MemberSearch == null || IsSectorPage)
            {
                if (IsSectorPage)
                {
                    this.MemberSearch = Spark.SAL.MemberSearch.GetMemberSearch(SectorsManager.GetSearchpreferences(SectorsManager.GetSector(), g), g.Member != null ? g.Member.MemberID : Constants.NULL_INT, g.Brand.Site.Community.CommunityID, "Default");
                }
                else
                {
                    this.MemberSearch = Spark.SAL.MemberSearch.GetMemberSearch(g.SearchPreferences, g.Member != null ? g.Member.MemberID : Constants.NULL_INT, g.Brand.Site.Community.CommunityID, "Default");
                }
            }

            //get available pref list from xml
            _SearchPreferenceSections = SearchUtil.GetSearchPreferenceSections(g.Brand.Site.SiteID, g);

            if (!IsRegOverlayRefresh)
            {
                //load Edit Preferences
                LoadEditPreferences();
            }
            else
            {
                editPrefLayer.Visible = false;
                phEditSearchPrefJS.Visible = false;
            }

            //determine if we need to update preferences before raising PreferencesReady event
            if (!Page.IsPostBack
                || (Page.IsPostBack
                    && String.IsNullOrEmpty(Request.Form[btnSaveSearchPreferences1.UniqueID])
                    && String.IsNullOrEmpty(Request.Form[btnSaveSearchPreferences2.UniqueID])))
            {
                LoadPreferencesReady();
            }

        }

        protected override void OnPreRender(EventArgs e)
        {
            if (Convert.ToBoolean(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ANALYTICS_OMNITURE", _g.Brand.Site.Community.CommunityID, _g.Brand.Site.SiteID)))
            {
                if (_g.Session.Get("PreferenceEdited") != null)
                {
                    _g.AnalyticsOmniture.Evar50 = _g.Session.Get("PreferenceEdited").ToString();
                    _g.Session.Remove("PreferenceEdited");
                    hidAdvancePref.Value = "";
                }
            }

            base.OnPreRender(e);
        }

        protected void btnSaveSearchPreferences_Click(object sender, EventArgs e)
        {
            string sortOrderBy = string.Empty;
            try
            {
                _PreferenceEdited = true;
                _g.Session.Add("PreferenceEdited", (hidAdvancePref.Value == "true") ? "Advanced" : "Simple", Matchnet.Session.ValueObjects.SessionPropertyLifetime.Temporary);

                Page.Validate();

                if (Page.IsValid)
                {
                    g.ResetSearchStartRow = true;

                    //GenderMask when saved as search pref is the reverse to indicate the gendermask of the people you're looking for
                    //so if you're a man seeking women, it needs to be saved as women seeking men.
                    int genderMask = Convert.ToInt32(ddlBoxGenderMask.SelectedValue);
                    this.MemberSearch.Gender = ((genderMask & ConstantsTemp.GENDERID_MALE) == ConstantsTemp.GENDERID_MALE) ? Gender.Male : Gender.Female;
                    this.MemberSearch.SeekingGender = ((genderMask & ConstantsTemp.GENDERID_SEEKING_MALE) == ConstantsTemp.GENDERID_SEEKING_MALE) ? Gender.Male : Gender.Female;

                    Int32 minAge = Conversion.CInt(tbAgeMin.Text);
                    Int32 maxAge = Conversion.CInt(tbAgeMax.Text);

                    if (minAge > maxAge)
                    {
                        tbAgeMin.Text = tbAgeMax.Text;
                        tbAgeMax.Text = minAge.ToString();

                        minAge = maxAge;
                        maxAge = Conversion.CInt(tbAgeMax.Text);
                    }

                    this.MemberSearch.Age.MinValue = minAge;
                    this.MemberSearch.Age.MaxValue = maxAge;

                    //this.MemberSearch.Distance = 0;// Conversion.CInt(ddlDistance.SelectedValue);

                    // if this is a photo required site, we want to set this search pref manually
                    if (Convert.ToBoolean(RuntimeSettings.GetSetting("PHOTO_REQUIRED_SITE", g.Brand.Site.Community.CommunityID,
                        g.Brand.Site.SiteID, g.Brand.BrandID)))
                    {
                        this.MemberSearch.HasPhoto = true;
                    }
                    else
                    {
                        this.MemberSearch.HasPhoto = cbHasPhoto.Checked;
                    }

                    if (g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.JDate)
                    {

                        if (cbxRamahDate.Checked)
                            (this.MemberSearch[Preference.GetInstance("RamahAlum")] as MemberSearchPreferenceInt).Value = 1;
                        else
                            (this.MemberSearch[Preference.GetInstance("RamahAlum")] as MemberSearchPreferenceInt).Value = Constants.NULL_INT;

                    }



                    string oldKeyword = this.MemberSearch.KeywordSearch;
                    this.MemberSearch.KeywordSearch = "";
                    if (SearchUtil.IsKeywordMatchesSearchPrefEnabled(g.Brand.BrandID, g.Brand.Site.SiteID, g.Brand.Site.Community.CommunityID))
                    {
                        if (g.GetResource("TXT_KEYWORD_EXAMPLE", this.txtKeyword).ToLower() != txtKeyword.Text.Trim())
                        {
                            this.MemberSearch.KeywordSearch = txtKeyword.Text.Trim();
                            if (this.MemberSearch.KeywordSearch.Length > SearchUtil.KEYWORD_SEARCH_MAXCHAR_LENGTH)
                            {
                                this.MemberSearch.KeywordSearch = this.MemberSearch.KeywordSearch.Substring(0, SearchUtil.KEYWORD_SEARCH_MAXCHAR_LENGTH);
                            }

                            //if removed keywords, change sort to newest
                            if (string.IsNullOrEmpty(this.MemberSearch.KeywordSearch))
                            {
                                //only change to newest of current sort by is keyword
                                if (String.IsNullOrEmpty(g.SearchPreferences["SearchOrderBy"]) || (!String.IsNullOrEmpty(g.SearchPreferences["SearchOrderBy"]) && g.SearchPreferences["SearchOrderBy"] == Convert.ToInt32(QuerySorting.KeywordRelevance).ToString()))
                                {
                                    g.SearchPreferences["searchorderby"] = Convert.ToInt32(QuerySorting.JoinDate).ToString();
                                }
                            }
                            //if changing keyword values, default sort to keyword relevance
                            else if (oldKeyword != this.MemberSearch.KeywordSearch)
                            {
                                g.SearchPreferences["searchorderby"] = Convert.ToInt32(QuerySorting.KeywordRelevance).ToString();
                            }
                            //saving the sort order so it is remembered after the reset
                            sortOrderBy = g.SearchPreferences["searchorderby"];
                        }
                    }

                    this.MemberSearch.Save();

                    try
                    {
                        if (g.Member != null)
                        {
                            bool enableMemberSlideshow = Conversion.CBool(RuntimeSettings.GetSetting("ENABLE_SLIDESHOW", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID));
                            bool enableConfigurableSlideshow = Conversion.CBool(RuntimeSettings.GetSetting("ENABLE_CONFIGURABLE_SLIDESHOW", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID));
                            if (enableMemberSlideshow && !enableConfigurableSlideshow)
                            {
                                MemberSlideshowSA.Instance.RemoveFromCache(g.Member.MemberID, g.Brand.Site.Community.CommunityID);
                            }
                        }
                    }
                    catch (Exception ex)
                    { g.ProcessException(ex); }

                    g.ResetSearchPreferences();
                }
                else
                {
                    editPrefLayer.Attributes["style"] = "";
                    if (!AgeMinValidator.IsValid && !AgeMaxValidator.IsValid)
                    {
                        AgeMinValidator.Text = g.GetResource("AGE_INVALID", this);
                        AgeMaxValidator.Text = "";
                    }

                }
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
            finally
            {
                IgnoreFilteredSearchResultsCache = true;
                //set the sort order by if it was set for keyword search
                if (!string.IsNullOrEmpty(sortOrderBy))
                {
                    g.SearchPreferences["searchorderby"] = sortOrderBy;
                }
                PostbackRequiresPRG = true;
                LoadPreferencesReady();
            }
        }

        #endregion

        #region Public Methods
        #endregion

        #region Private Methods
        private void LoadPreferencesReady()
        {
            //load preferences summary
            LoadPreferenceSummary();

            //notify clients that preferences are updated and ready
            PreferencesReady();
        }

        private void LoadPreferenceSummary()
        {
            //Build preference summary string based on member's current search preferences
            StringBuilder sb = new StringBuilder();
            if (_SearchPreferenceSections != null && _SearchPreferenceSections.SearchSectionList.Count > 0)
            {
                foreach (SearchSection s in _SearchPreferenceSections.SearchSectionList)
                {
                    foreach (SearchSectionPreference ssp in s.SearchSectionPreferenceList)
                    {
                        string displayValue = string.Empty;
                        bool addSeparator = false;
                        bool addLabel = false;

                        switch (ssp.Name.ToLower())
                        {
                            //required prefs and custom attributes
                            case "gendermask":
                                displayValue = GetGenderPreferenceDisplay();
                                displayValue += " ";
                                break;
                            case "age":
                                displayValue = GetAgePreferenceDisplay();
                                displayValue += " ";
                                break;
                            case "distancelocation":
                                displayValue = GetDistanceLocationPreferenceDisplay();
                                break;
                            case "hasphoto":
                                displayValue = GetHasPhotoPreferenceDisplay();
                                addSeparator = true;
                                break;
                            default:
                                //standard pref attributes
                                Preference p = Preference.GetInstance(ssp.Name);
                                addSeparator = true;
                                addLabel = true;
                                if (p != null)
                                {
                                    switch (p.PreferenceType)
                                    {
                                        case PreferenceType.Int:
                                            displayValue = GetPreferenceDisplayInt(p);
                                            break;
                                        case PreferenceType.Mask:
                                            displayValue = GetPreferenceDisplayMask(p);
                                            break;
                                        case PreferenceType.Range:
                                            displayValue = GetPreferenceDisplayRange(p);
                                            break;
                                        case PreferenceType.Text:
                                            displayValue = GetPreferenceDisplayText(p);
                                            break;
                                    }
                                }
                                break;
                        }

                        if (!string.IsNullOrEmpty(displayValue))
                        {
                            if (addSeparator)
                                sb.Append("; ");

                            if (addLabel)
                                sb.Append("<span class=\"summary-label\">" + g.GetResource(ssp.ResourceConstant, this) + ": </span>");

                            sb.Append(displayValue);

                        }
                    }
                }

                //handle ramah because its displayed as a standard search pref 
                var ramahForDisplay = GetOnlyRamahPrefDisplay();
                if(ramahForDisplay != "")
                    sb.Append(ramahForDisplay);
                
                //keyword
                if (SearchUtil.IsKeywordMatchesSearchPrefEnabled(g.Brand.BrandID, g.Brand.Site.SiteID, g.Brand.Site.Community.CommunityID))
                {
                    if (!string.IsNullOrEmpty(this.MemberSearch.KeywordSearch) && this.MemberSearch.KeywordSearch.Trim() != "")
                    {
                        sb.Append("; ");
                        sb.Append("<span class=\"summary-label\">" + g.GetResource("TXT_KEYWORDS", this) + ": </span>");
                        sb.Append(Server.HtmlEncode(this.MemberSearch.KeywordSearch));
                    }
                }

            }

            literalCurrentPreferences.Text = sb.ToString();
        }

        private void LoadEditPreferences()
        {
            if (_SearchPreferenceSections != null && _SearchPreferenceSections.SearchSectionList.Count > 0)
            {
                //default prefs
                SearchSection sectionRequired = _SearchPreferenceSections.GetSection("required");
                if (sectionRequired != null)
                {
                    foreach (SearchSectionPreference ssp in sectionRequired.SearchSectionPreferenceList)
                    {
                        switch (ssp.Name.ToLower())
                        {
                            case "gendermask":
                                //get current gendermask pref (stored as reverse)
                                int currentGenderMask = 0;
                                currentGenderMask += (this.MemberSearch.Gender == Gender.Male) ? ConstantsTemp.GENDERID_MALE : ConstantsTemp.GENDERID_FEMALE;
                                currentGenderMask += (this.MemberSearch.SeekingGender == Gender.Male) ? ConstantsTemp.GENDERID_SEEKING_MALE : ConstantsTemp.GENDERID_SEEKING_FEMALE;

                                //load gendermask options
                                string[] maskValues = ssp.MaskValues.Split(new char[] { ',' });
                                string[] maskResources = ssp.MaskResources.Split(new char[] { ',' });
                                ddlBoxGenderMask.Items.Clear();
                                if (maskValues != null && maskResources != null)
                                {
                                    for (int i = 0; i < maskValues.Length; i++)
                                    {
                                        ddlBoxGenderMask.Items.Add(new ListItem(g.GetResource(maskResources[i], this), maskValues[i]));
                                    }
                                    ddlBoxGenderMask.SelectedValue = currentGenderMask.ToString();
                                }
                                break;
                            case "age":
                                tbAgeMin.Text = this.MemberSearch.Age.MinValue.ToString();
                                tbAgeMax.Text = this.MemberSearch.Age.MaxValue.ToString();
                                break;
                            case "distancelocation":
                                EditLocationPreference1.LoadLocationPreference(this.MemberSearch);
                                break;
                            case "hasphoto":
                                // if this is a photo required site, it doesn't make sense to offer this option
                                if (!Convert.ToBoolean(RuntimeSettings.GetSetting("PHOTO_REQUIRED_SITE", g.Brand.Site.Community.CommunityID,
                                        g.Brand.Site.SiteID, g.Brand.BrandID)))
                                {
                                    cbHasPhoto.Visible = true;
                                    cbHasPhoto.Text = g.GetResource("TXT_SHOW_PHOTO_PROFILES_ONLY", this);
                                    cbHasPhoto.Checked = this.MemberSearch.HasPhoto;
                                }

                                break;
                            
                        }

                    }
                }

                //keyword
                if (SearchUtil.IsKeywordMatchesSearchPrefEnabled(g.Brand.BrandID, g.Brand.Site.SiteID, g.Brand.Site.Community.CommunityID))
                {
                    phKeyword.Visible = true;
                    if (!string.IsNullOrEmpty(this.MemberSearch.KeywordSearch) && this.MemberSearch.KeywordSearch.Trim() != "")
                    {
                        txtKeyword.Text = this.MemberSearch.KeywordSearch;
                    }
                }


                //ramah alum
                if (g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.JDate)
                {
                    cbxRamahDate.Visible = true;
                    cbxRamahDate.Text = g.GetResource("TXT_SHOW_RAMAH_ONLY", this);
                    cbxRamahDate.Checked = ((this.MemberSearch[Preference.GetInstance("RamahAlum")] as MemberSearchPreferenceInt).Value == 1);
                }


                //advance prefs
                rptSection.MemberSearch = this.MemberSearch;
                string[] advancePrefSections = new string[] { "basics", "additional" };
                rptSection.SearchSectionPreferenceList = _SearchPreferenceSections.GetPreferences(advancePrefSections);
                rptSection.DataBind();

            }

        }

        private string GetPreferenceDisplayInt(Preference p)
        {
            StringBuilder sb = new StringBuilder();
            MemberSearchPreferenceInt prefInt = (MemberSearchPreferenceInt)this.MemberSearch[p];

            if (prefInt != null && prefInt.Value >= 0)
            {
                sb.Append(Option.GetContent(p.PreferenceName, prefInt.Value, g));
            }

            return sb.ToString();
        }

        private string GetPreferenceDisplayMask(Preference p)
        {
            StringBuilder sb = new StringBuilder();
            MemberSearchPreferenceInt prefMask = (MemberSearchPreferenceInt)this.MemberSearch[p];

            if (prefMask != null && prefMask.Value > 0)
            {
                if (p.PreferenceName.ToLower() == "childrencount")
                {
                    sb.Append(Option.GetChildrenCountMaskContent(prefMask.Value, g));
                }
                else if (p.PreferenceName.ToLower() == "jdatereligion" && prefMask.Value == FrameworkGlobals.GetJewishReligionOnly(g))
                {
                    sb.Append(g.GetResource("TXT_JEWISH", this));
                }
                else
                {
                    sb.Append(Option.GetMaskContent(p.PreferenceName, prefMask.Value, g));
                }
            }

            return sb.ToString();
        }

        private string GetPreferenceDisplayRange(Preference p)
        {
            StringBuilder sb = new StringBuilder();
            MemberSearchPreferenceRange prefRange = (MemberSearchPreferenceRange)this.MemberSearch[p];

            if (prefRange != null && (prefRange.MinValue >= 0 || prefRange.MaxValue >= 0))
            {
                if (p.PreferenceName.ToLower() == "height")
                {
                    sb.Append(GetHeightPreferenceDisplay(p));
                }
                else if (p.PreferenceName.ToLower() == "age")
                {
                    sb.Append(GetAgePreferenceDisplay());
                }
                else
                {
                    //min
                    if (prefRange.MinValue < 0)
                    {
                        sb.Append(g.GetResource("ANY_"));
                    }
                    else
                    {
                        sb.Append(Option.GetContent(p.PreferenceName, prefRange.MinValue, g));
                    }

                    sb.Append(" - ");

                    //max
                    if (prefRange.MaxValue < 0)
                    {
                        sb.Append(g.GetResource("ANY_"));
                    }
                    else
                    {
                        sb.Append(Option.GetContent(p.PreferenceName, prefRange.MaxValue, g));
                    }
                }
            }

            return sb.ToString();
        }

        private string GetPreferenceDisplayText(Preference p)
        {
            StringBuilder sb = new StringBuilder();
            MemberSearchPreferenceText prefText = (MemberSearchPreferenceText)this.MemberSearch[p];

            //TODO

            return sb.ToString();
        }

        private string GetGenderPreferenceDisplay()
        {
            if (MemberSearch.SeekingGender == Gender.Male)
                return g.GetResource("TXT_MEN", this);
            else
                return g.GetResource("TXT_WOMEN", this);
        }

        private string GetAgePreferenceDisplay()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(g.GetResource("TXT_AGE", this) + " ");
            sb.Append(MemberSearch.Age.MinValue.ToString() + "-" + MemberSearch.Age.MaxValue.ToString());
            return sb.ToString();
        }

        private string GetHeightPreferenceDisplay(Preference p)
        {
            StringBuilder sb = new StringBuilder();

            MemberSearchPreferenceRange prefRange = (MemberSearchPreferenceRange)this.MemberSearch[p];
            if (prefRange != null && (prefRange.MinValue > 0 || prefRange.MaxValue > 0))
            {
                DropDownList ddlHeight = FrameworkGlobals.CreateHeightList(g, p.PreferenceName, Constants.NULL_INT, true);

                //sb.Append(g.GetResource("ANY_"));
                if (prefRange.MinValue > 0 && prefRange.MaxValue < 0)
                {
                    //min height only
                    sb.Append(g.GetResource("TXT_HEIGHT_AT_LEAST", this) + " ");
                    foreach (ListItem li in ddlHeight.Items)
                    {
                        if (Convert.ToInt32(li.Value) == prefRange.MinValue)
                        {
                            sb.Append(li.Text);
                            break;
                        }
                    }
                }
                else if (prefRange.MinValue < 0 && prefRange.MaxValue > 0)
                {
                    //max height only
                    sb.Append(g.GetResource("TXT_HEIGHT_UNDER", this) + " ");
                    foreach (ListItem li in ddlHeight.Items)
                    {
                        if (Convert.ToInt32(li.Value) == prefRange.MaxValue)
                        {
                            sb.Append(li.Text);
                            break;
                        }
                    }
                }
                else if (prefRange.MinValue == prefRange.MaxValue)
                {
                    //min and max height are the same
                    sb.Append(g.GetResource("TXT_HEIGHT_EXACTLY", this) + " ");
                    foreach (ListItem li in ddlHeight.Items)
                    {
                        if (Convert.ToInt32(li.Value) == prefRange.MaxValue)
                        {
                            sb.Append(li.Text);
                            break;
                        }
                    }
                }
                else
                {
                    //min and max height are different
                    sb.Append(g.GetResource("TXT_HEIGHT_BETWEEN", this) + " ");
                    foreach (ListItem li in ddlHeight.Items)
                    {
                        if (Convert.ToInt32(li.Value) == prefRange.MinValue)
                        {
                            sb.Append(li.Text);
                            break;
                        }
                    }
                    sb.Append(" - ");
                    foreach (ListItem li in ddlHeight.Items)
                    {
                        if (Convert.ToInt32(li.Value) == prefRange.MaxValue)
                        {
                            sb.Append(li.Text);
                            break;
                        }
                    }
                }
            }

            return sb.ToString();
        }

        private string GetDistanceLocationPreferenceDisplay()
        {
            StringBuilder sb = new StringBuilder();

            if (MemberSearch.SearchType == SearchType.PostalCode ||
                MemberSearch.SearchType == SearchType.Region)
            {
                sb.Append(g.GetResource("TXT_WITHIN", this) + " ");
                string distanceValue = Option.GetContent(SearchUtil.GetDistanceAttribute(g), MemberSearch.Distance, SettingsManager.GetSettingInt(SettingConstants.DEFAULT_DISTANCE_LIST_ORDER, g.Brand), g);

                if (distanceValue == g.GetResource("ATTOPT_730_ANY_MILES", null))
                {
                    distanceValue = g.GetResource("TXT_ANY_DISTANCE", this);
                }
                sb.Append(distanceValue + " ");
                sb.Append(g.GetResource("TXT_OF", this) + " ");
                sb.Append(FrameworkGlobals.GetRegionString(MemberSearch.RegionID, g.Brand.Site.LanguageID));
            }
            else if (MemberSearch.SearchType == SearchType.AreaCode)
            {
                sb.Append(g.GetResource("TXT_AREACODE", this) + " ");
                sb.Append(GetAreaCodePreferenceList());
            }

            return sb.ToString();
        }

        private string GetAreaCodePreferenceList()
        {
            StringBuilder sbAreaCodes = new StringBuilder();
            for (Int32 i = 1; i <= 6; i++)
            {
                MemberSearchPreferenceInt areaCode = MemberSearch[Preference.GetInstance("AreaCode" + i)] as MemberSearchPreferenceInt;
                if (areaCode != null && Conversion.CInt(areaCode.Value) > 0)
                {
                    sbAreaCodes.Append(areaCode.Value.ToString() + ", ");
                }
            }
            if (sbAreaCodes.Length > 0)
            {
                sbAreaCodes.Remove(sbAreaCodes.Length - 2, 2);
            }

            return sbAreaCodes.ToString();
        }

        private string GetHasPhotoPreferenceDisplay()
        {
            StringBuilder sb = new StringBuilder();

            // if this is a photo required site, it doesn't make sense to offer this option
            if (!Convert.ToBoolean(RuntimeSettings.GetSetting("PHOTO_REQUIRED_SITE", g.Brand.Site.Community.CommunityID,
                    g.Brand.Site.SiteID, g.Brand.BrandID)) && MemberSearch.HasPhoto)
            {
                sb.Append(" " + g.GetResource("TXT_HAS_PHOTOS", this));
            }

            return sb.ToString();
        }


        private string GetOnlyRamahPrefDisplay()
        {
            MemberSearchPreferenceInt rAlum = MemberSearch[Preference.GetInstance("RamahAlum")] as MemberSearchPreferenceInt;

            if(rAlum.Value == 1)
            {
                return " " + g.GetResource("TXT_SHOW_RAMAH_ONLY", this);
            }
            else
                return "";
        }

        #endregion


        public string GetStyle()
        {
            if (IsSectorPage)
            {
                return "style=\"display: none !important;\"";
            }
            return string.Empty;
        }
    }



}
