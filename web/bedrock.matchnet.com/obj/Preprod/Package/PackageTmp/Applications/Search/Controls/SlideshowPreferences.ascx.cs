﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Web.Framework.Ui.BasicElements;
using Spark.SAL;
using Matchnet.Web.Framework;
using Matchnet.Web.Applications.Search.XMLPreference;
using System.Text;
using Matchnet.Web.Framework.Util;
using System.Data;
using Matchnet.Configuration.ServiceAdapters;
using System.Web.UI.HtmlControls;
using Matchnet.Lib;
using Matchnet.MemberSlideshow.ServiceAdapters;
using Matchnet.Web.Framework.Managers;
using Matchnet.Web.Framework.Util;

namespace Matchnet.Web.Applications.Search.Controls
{
    public partial class SlideshowPreferences : FrameworkControl
    {
        SlideshowDisplayType _slideshowDisplayType = SlideshowDisplayType.Full;

        public SlideShowModeType SlideShowMode { get; private set; }
        public string SlideshowOuterContainerDivID { get; private set; }
        public bool ShowUpdatedWideStyle { get; set; }
        
        protected void Page_Load(object sender, EventArgs e)
        {
            divSlideshowPreferencesData.Attributes.Add("data-ageminclientid", tbAgeMin.ClientID);
            divSlideshowPreferencesData.Attributes.Add("data-agemaxclientid", tbAgeMax.ClientID);
            divSlideshowPreferencesData.Attributes.Add("data-regionclientid", EditLocationPreferences.RegionClientID);
            divSlideshowPreferencesData.Attributes.Add("data-searchtypeclientid", EditLocationPreferences.SearchTypeClientID);
            divSlideshowPreferencesData.Attributes.Add("data-areacodelistclientid", EditLocationPreferences.AreaCodeListClientID);
            divSlideshowPreferencesData.Attributes.Add("data-displaytype", _slideshowDisplayType.ToString("d"));
            divSlideshowPreferencesData.Attributes.Add("data-urlparamslideshowdisplaytype", WebConstants.URL_PARAMETER_SLIDESHOW_DISPLAY_TYPE);
            divSlideshowPreferencesData.Attributes.Add("data-urlparamslideshowminage", WebConstants.URL_PARAMETER_MINISLIDESHOW_MINAGE);
            divSlideshowPreferencesData.Attributes.Add("data-urlparamslideshowmaxage", WebConstants.URL_PARAMETER_MINISLIDESHOW_MAXAGE);
            divSlideshowPreferencesData.Attributes.Add("data-urlparamslideshowregionid", WebConstants.URL_PARAMETER_MINISLIDESHOW_REGIONID);
            divSlideshowPreferencesData.Attributes.Add("data-urlparamslideshowsearchtype", WebConstants.URL_PARAMETER_MINISLIDESHOW_SEARCHTYPE);
            divSlideshowPreferencesData.Attributes.Add("data-urlparamslideshowareacodelist", WebConstants.URL_PARAMETER_MINISLIDESHOW_AREACODE_LIST);
            divSlideshowPreferencesData.Attributes.Add("data-urlparamslideshowmode", WebConstants.URL_PARAMETER_SLIDESHOW_MODE);
            divSlideshowPreferencesData.Attributes.Add("data-urlparamslideshowoutercontainerdiv", WebConstants.URL_PARAMETER_SLIDESHOW_OUTER_CONTAINER_DIV);
            divSlideshowPreferencesData.Attributes.Add("data-urlparamslideshowshowupdatedwidestyle", WebConstants.URL_PARAMETER_SLIDESHOW_SHOW_UPDATED_WIDE_STYLE);
            divSlideshowPreferencesData.Attributes.Add("data-slideshowmode", SlideShowMode.ToString("d"));
            divSlideshowPreferencesData.Attributes.Add("data-outercontainerdivid", SlideshowOuterContainerDivID);
            divSlideshowPreferencesData.Attributes.Add("data-showupdatedwidestyle", ShowUpdatedWideStyle ? "true" : "false");
        }

        public void LoadPreferences(SlideshowDisplayType slideshowDisplayType, SlideShowModeType slideShowMode, string slideshowOuterContainerDivID)
        {
            MemberSlideshowManager manager = new MemberSlideshowManager(g.Member.MemberID, g.Brand);
            MemberSlideshowSettings settings = manager.GetConfigurableSlideshowSettings();
            tbAgeMin.Text = settings.MinAge.ToString();
            tbAgeMax.Text = settings.MaxAge.ToString();

            EditLocationPreferences.LoadLocationPreference(settings);
            _slideshowDisplayType = slideshowDisplayType;
            SlideShowMode = slideShowMode;
            SlideshowOuterContainerDivID = slideshowOuterContainerDivID;
        }

        protected string GetDisplayType()
        {
            return _slideshowDisplayType.ToString("d");
        }
    }
}