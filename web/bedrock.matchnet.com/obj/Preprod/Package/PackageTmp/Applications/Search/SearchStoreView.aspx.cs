using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.AttributeOption;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Content.ValueObjects.Region;
//using Matchnet.e1Loader.ServiceAdapters;

namespace Matchnet.Web.Applications.Search
{
	/// <summary>
	/// Summary description for SearchStoreView.
	/// </summary>
	public class SearchStoreView : System.Web.UI.Page
	{
		private void Page_Load(object sender, System.EventArgs e)
		{
			Int32 memberID = Conversion.CInt(Request["MemberID"]);
			Int32 brandID = Conversion.CInt(Request["BrandID"]);
			Brand brand = BrandConfigSA.Instance.GetBrandByID(brandID);
			Int32 communityID = brand.Site.Community.CommunityID;

			Hashtable memberAttributes = null;//LoaderSA.Instance.GetDBMember(memberID, communityID);

			foreach (DictionaryEntry de in memberAttributes)
			{
				string key = de.Key.ToString();
				string val = de.Value.ToString();

				if (key.IndexOf("RegionID") > -1)
				{
					val = RegionSA.Instance.RetrieveRegionByID(Conversion.CInt(val), (Int32) Matchnet.Language.English).Description;
				}
				else
				{
					StringBuilder values = new StringBuilder();

					AttributeOptionCollection aoc = AttributeOptionSA.Instance.GetAttributeOptionCollection(key, brandID);
					if (aoc != null)
					{
						foreach (AttributeOption ao in aoc)
						{
							if (ao.MaskContains(Conversion.CInt(val)))
							{
								values.Append(ao.Description);
							}
						}
						val = values.ToString();
					}
				}

				Response.Write(key + ": " + val + "<br />");
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
