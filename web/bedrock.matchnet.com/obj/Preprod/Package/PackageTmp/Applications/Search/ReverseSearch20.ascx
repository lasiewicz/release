﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="ReverseSearch20.ascx.cs" Inherits="Matchnet.Web.Applications.Search.ReverseSearch20" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="result" TagName="ResultList" Src="../../Framework/Ui/SearchElements/ResultList.ascx" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="cc1" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn1" TagName="ResultsViewType" Src="/Framework/Ui/BasicElements/ResultsViewType.ascx" %>

<div id="divReverseSearchPageContainer">
    <div class="header-options results30 clearfix">
        <mn:Title runat="server" id="txtMatches" ResourceConstant="TXT_REVERSE_SEARCH_TITLE"/>
        <asp:Panel ID="pnlOnlyPhotos" runat="server" CssClass="links">
			<input type="checkbox" id="onlyProfilesWithPhotos" runat="server" class="onlyPhotos" onchange="OnOnlyPhotoChange();" />
			<label>
				<mn:Txt ID="showProfilesWithPhotosOnlyTxt" runat="server" ResourceConstant="showProfilesWithPhotosOnly" />
			</label>
		</asp:Panel>
        <h3 class="tag-line clear-both"><mn:txt runat="server" id="txtDesc" ResourceConstant="TXT_DESC" /></h3>
    </div>

    <%--Sort and top navigation--%>
    <div class="sort-display clearfix">
        <div class="view-by">
            <mn:txt runat="server" id="txtView" resourceconstant="TXT_VIEW_BY" />
            <select id="ddlSearchOption" onchange="OnSearchOrderByChange();">
                <asp:Repeater id="rptSearchOption" Runat="server">
	                <ItemTemplate>
	                    <li class="<asp:Literal id=litSortSpan runat=server /> no-tab">
		                    <asp:HyperLink id="lnkSort" Runat="server" />
		                    <span class="x"><asp:Literal id="litSortTitle" Runat="server" Visible="False" /></span>
                        </li>
                        <asp:Literal ID="litOption" runat="server"></asp:Literal>
                    </ItemTemplate>
                </asp:Repeater>
            </select>
        </div>
    
        <div class="pagination">
		    <asp:label id="lblListNavigationTop" Runat="server" />
	    </div>

        <asp:PlaceHolder ID="phViewMode" runat="server">
        <div class="view-as">
            <%--gallery/list--%>
            <mn1:ResultsViewType id="idResultsViewType" runat="server" 
                ListResourceConstant="TXT_LIST30"
                ListSelectedResourceConstant="TXT_LIST30_SELECTED"
                GalleryResourceConstant="TXT_GALLERY30"
                GallerySelectedResourceConstant="TXT_GALLERY30_SELECTED"
            />
        </div>
        </asp:PlaceHolder>
        <input type="hidden" name="hidPhotoOnly" runat="server" id="hidPhotoOnly" />
        <input type="hidden" name="hidSearchOrderBy" runat="server" id="hidSearchOrderBy" />
    </div>

    <%--Search results--%>
    <div id="results30" class="results30 clearfix clear-both">
        <asp:PlaceHolder id="plcPromotionalProfile" Runat="server" Visible="false"></asp:PlaceHolder>

        <asp:PlaceHolder ID="Results" runat="server" Visible="true">
            <result:ResultList ID="SearchResultList" runat="server" ResultListContextType="ReverseSearchResult" EnableMultipleSearchViews="false" PageSize="16" LoadControlAutomatically="false"></result:ResultList>
            <input type="hidden" value="<% = ChangeVoteMessage %>" name="ChangeVoteMessage" />
            <iframe tabindex="-1" name="FrameYNMVote" frameborder="0" width="1" scrolling="no" height="1">
                <layer name="FrameYNMVote" frameborder="0" scrolling="no" width="1" height="1"></layer>
            </iframe>
        </asp:PlaceHolder>
    </div>
    <div class="pagination">
        <asp:Label ID="lblListNavigationBottom" runat="server"></asp:Label>
    </div>

    <script type="text/javascript">
        function OnSearchOrderByChange() {
            window.location = "/Applications/Search/ReverseSearch.aspx?SearchOrderBy=" + $j('#ddlSearchOption').val() + "&op=" + $j('#<%=hidPhotoOnly.ClientID %>').val();
        }

        function OnOnlyPhotoChange() {
            if ($j('#<%=hidPhotoOnly.ClientID %>').val() == 'true')
                window.location = "/Applications/Search/ReverseSearch.aspx";
            else
                window.location = "/Applications/Search/ReverseSearch.aspx?op=true";
        }
    </script>
</div>