﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EditSlideshowLocationPreference.ascx.cs" Inherits="Matchnet.Web.Applications.Search.Controls.EditSlideshowLocationPreference" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>


<%--Label for region by city or zip code, includes distance dropdown option--%>
<%--Label for area code--%>
<div id="divAreaCodeLabel" runat="server" style="display:none;" class="region-label">
    <label><mn:txt id="txtAreaCodeLabel" ResourceConstant="TXT_LOCATED_IN_AREACODES" runat="server"></mn:txt></label>
</div>

<%--Location Text and edit link--%>
<a id="lnkLocationEdit" href="#" class="region-edit-link">
    <span class="spr s-icon-arrow-right"><span></span></span>
    <span id="spanLocation">
        <asp:Literal ID="literalLocation" runat="server"></asp:Literal>
    </span>
    <mn2:FrameworkLiteral ID="literalLocationEdit" runat="server" ResourceConstant="TXT_EDIT"></mn2:FrameworkLiteral>
</a>

<div id="locationLayer" style="display:none;" class="region-layer">
    <h2><span class="spr s-icon-arrow-down"><span></span></span><a href="#" id="locationTitle"><asp:Literal ID="literalLocationTitle" runat="server"></asp:Literal></a></h2>
    <a href="#" class="close" title="<mn:txt id='txtClose2' ResourceConstant='TXT_CLOSE' runat='server'></mn:txt>">
        <span class="spr s-icon-closethick-color"><span>X</span></span>
    </a>
    <p id="errorMessage" class="error"></p>
    <div class="option">
        <strong><mn2:FrameworkLiteral ID="literalSearchBy" runat="server" ResourceConstant="TXT_SEARCHBY"></mn2:FrameworkLiteral></strong>
        <asp:RadioButton ID="rbZipCode" runat="server" GroupName="rbgSearchBy" />
        <asp:RadioButton ID="rbRegion" runat="server" GroupName="rbgSearchBy" />
        <asp:RadioButton ID="rbAreaCode" runat="server" GroupName="rbgSearchBy" />
    </div>

    <%--Zip Code--%>
    <fieldset id="divZipCode" runat="server" style="display:none;" class="zip">
        <label><mn2:FrameworkLiteral ID="literalZipCountry" runat="server" ResourceConstant="TXT_COUNTRY"></mn2:FrameworkLiteral></label>
        <asp:DropDownList ID="ddlZipCountry" runat="server"></asp:DropDownList>
        <br />
        <label><mn2:FrameworkLiteral ID="literalZipCode" runat="server" ResourceConstant="TXT_ZIPCODE"></mn2:FrameworkLiteral></label>
        <asp:TextBox ID="txtZipCode" runat="server"></asp:TextBox>
        
        <div class="cta">
            <asp:HyperLink ID="lnkZipCodeSave" runat="server" NavigateUrl="#" CssClass="link-primary">
                <mn2:FrameworkLiteral ID="literalZipCodeSave" runat="server" ResourceConstant="TXT_SAVE"></mn2:FrameworkLiteral>
            </asp:HyperLink>
        </div>
    </fieldset>

    <%--Region--%>
    <fieldset id="divRegion" runat="server" style="display:none;" class="region">
        <label><mn2:FrameworkLiteral ID="literalRegionCountry" runat="server" ResourceConstant="TXT_COUNTRY"></mn2:FrameworkLiteral></label>
        <asp:DropDownList ID="ddlRegionCountry" runat="server"></asp:DropDownList>
        
        <div id="divState" runat="server">
            <label><mn2:FrameworkLiteral ID="literalState" runat="server" ResourceConstant="TXT_STATE"></mn2:FrameworkLiteral></label>
            <asp:DropDownList ID="ddlState" runat="server"></asp:DropDownList>
        </div>
        
        <label class="label-city-lh"><mn2:FrameworkLiteral ID="literalCity" runat="server" ResourceConstant="TXT_CITY"></mn2:FrameworkLiteral></label>
        <asp:TextBox ID="txtCity" runat="server" class="input-city-lh"></asp:TextBox>
        <asp:HyperLink ID="lnkCityLookup" runat="server" style="display:none;" NavigateUrl="#" CssClass="city-lookup">
            <span id="lookuplink" name="lookuplink"><mn2:FrameworkLiteral ID="literalCityLookup" runat="server" ResourceConstant="TXT_LOOK_UP_CITY"></mn2:FrameworkLiteral></span>
        </asp:HyperLink>
        
        <div class="cta">
            <asp:HyperLink ID="lnkRegionSave" runat="server" NavigateUrl="#" CssClass="link-primary">
                <mn2:FrameworkLiteral ID="literalRegionSave" runat="server" ResourceConstant="TXT_SAVE"></mn2:FrameworkLiteral>
            </asp:HyperLink>
            <asp:HyperLink ID="lnkResetRegion" runat="server" NavigateUrl="#">
                <mn2:FrameworkLiteral ID="literalResetRegion" runat="server" ResourceConstant="TXT_RESET_REGION"></mn2:FrameworkLiteral>
            </asp:HyperLink>
        </div>
    </fieldset>

    <%--Area Code--%>
    <fieldset id="divAreaCode" runat="server" style="display:none;" class="area">
        <asp:DropDownList ID="ddlAreaCodeCountry" runat="server"></asp:DropDownList>
        <div class="area-list">
            <asp:TextBox ID="txtAreaCode1" runat="server"></asp:TextBox>
            <asp:TextBox ID="txtAreaCode2" runat="server"></asp:TextBox>
            <asp:TextBox ID="txtAreaCode3" runat="server"></asp:TextBox>
            <asp:TextBox ID="txtAreaCode4" runat="server"></asp:TextBox>
            <asp:TextBox ID="txtAreaCode5" runat="server"></asp:TextBox>
            <asp:TextBox ID="txtAreaCode6" runat="server"></asp:TextBox>
        </div>
        <div class="cta">
            <asp:HyperLink ID="lnkAreaCodeSave" runat="server" NavigateUrl="#" CssClass="link-primary">
                <mn2:FrameworkLiteral ID="FrameworkLiteral1" runat="server" ResourceConstant="TXT_SAVE"></mn2:FrameworkLiteral>
            </asp:HyperLink>
        </div>
    </fieldset>
    
    <asp:HiddenField ID="hidRegionID" runat="server" />
    <asp:HiddenField ID="hidAreaCodes" runat="server" />
    <asp:HiddenField ID="hidSearchType" runat="server" />
</div>

<script type="text/javascript">
    var SearchTypeEnum = { PostalCode: "<%=Spark.SAL.SearchType.PostalCode.ToString("d") %>", AreaCode: "<%=Spark.SAL.SearchType.AreaCode.ToString("d") %>", Region: "<%=Spark.SAL.SearchType.Region.ToString("d")%>" };
    var EditLocationPrefObj =
    {
        IsVerbose: false,
        BrandID: "<%=_g.Brand.BrandID.ToString()%>",
        AccountRegionID: "<%=_AccountRegionID %>",
        AccountCountryRegionID: "<%=_AccountCountryRegionID %>",
        AccountStateRegionID: "<%=_AccountStateRegionID %>",
        AccountCityName: "<%=_AccountCityName %>",
        InitialCityParentRegionID: "<%=_InitialCityParentRegionID %>",
        IsRegOverlay: <%=IsRegOverlay.ToString().ToLower() %>
    }
    var $locationLayer = $j('#locationLayer');
    var $lnkLocationEdit = $j('#lnkLocationEdit');
    var $divZipCode = $j('#<%=divZipCode.ClientID %>');
    var $divRegion = $j('#<%=divRegion.ClientID %>');
    var $divAreaCode = $j('#<%=divAreaCode.ClientID %>');
    var $hidRegionID = $j('#<%=hidRegionID.ClientID %>');
    var $hidSearchType = $j('#<%=hidSearchType.ClientID %>');
    var $hidAreaCodes = $j('#<%=hidAreaCodes.ClientID %>');

    $lnkLocationEdit.click(function() {
        var position = $j(this).position();
        $locationLayer.css({ left: position.left - 10, top: position.top - 10 }).toggle();

        $j('body').one('click', function() {
            $locationLayer.hide()
        });
        $locationLayer.click(function(e) {
            e.stopPropagation();
        });
        return false;
    });

    $j('#locationTitle, #locationLayer .close').click(function() {
        $locationLayer.hide();
        return false;
    });
    
    $j('#<%=rbZipCode.ClientID%>').click(function() {
        EditLocationPreference_ResetVisibility();
        $divZipCode.show();
        $hidSearchType.val(SearchTypeEnum.PostalCode);
        $j('#errorMessage').hide();
    });

    $j('#<%=rbRegion.ClientID%>').click(function() {
        EditLocationPreference_ResetVisibility();
        $divRegion.show();
        $hidSearchType.val(SearchTypeEnum.Region);
        $j('#errorMessage').hide();
    });

    $j('#<%=rbAreaCode.ClientID%>').click(function() {
        EditLocationPreference_ResetVisibility();
        $divAreaCode.show();
        $hidSearchType.val(SearchTypeEnum.AreaCode);
        $j('#errorMessage').hide();
    });

    $j('#<%=lnkZipCodeSave.ClientID%>').click(function() {
        //validate and get regionID
        var newZipCode = $j('#<%=txtZipCode.ClientID%>').val();
        var newZipCountryRegionID = $j('#<%=ddlZipCountry.ClientID %>').val();
        try {
            $j.ajax({
                type: "POST",
                url: "/Applications/API/RegionService.asmx/GetRegionInfoByZipCode",
                data: "{ zipCode: \"" + newZipCode + "\", countryRegionId: " + newZipCountryRegionID + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(result) {
                    var regionResult = (typeof result.d == 'undefined') ? result : result.d;
                    if (regionResult.Status == 2) {
                        $j('#<%=divAreaCodeLabel.ClientID %>').hide();
                        $j('#spanLocation').html(regionResult.LocationDisplayText);
                        $j('#locationTitle').html(regionResult.LocationDisplayText);
                        $hidRegionID.val(regionResult.RegionID);
                        $hidSearchType.val(SearchTypeEnum.PostalCode);
                        $j('#errorMessage').hide();
                        $locationLayer.hide();
                    }
                    else {
                        //show error
                        $j('#errorMessage').show().html(regionResult.StatusMessage);
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    if (EditLocationPrefObj.IsVerbose) { alert(textStatus); }
                }
            });
        }
        catch (e) {
            if (EditLocationPrefObj.IsVerbose) { alert(e); }
        }

        return false;
    });

    $j('#<%=lnkRegionSave.ClientID%>').click(function() {
        //validate and get regionID
        var newCountryRegionID = $j('#<%=ddlRegionCountry.ClientID%>').val();
        var newStateRegionID = $j('#<%=ddlState.ClientID %>').val();
        var newCityText = $j('#<%=txtCity.ClientID %>').val();

        if (newStateRegionID == null || newStateRegionID == "") {
            newStateRegionID = "0";
        }
        
        try {
            $j.ajax({
                type: "POST",
                url: "/Applications/API/RegionService.asmx/GetRegionInfoByCity",
                data: "{ cityName: \"" + newCityText + "\", stateRegionId: " + newStateRegionID + ", countryRegionId: " + newCountryRegionID + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(result) {
                    var regionResult = (typeof result.d == 'undefined') ? result : result.d;
                    if (regionResult.Status == 2) {
                        $j('#<%=divAreaCodeLabel.ClientID %>').hide();
                        $j('#spanLocation').html(regionResult.LocationDisplayText);
                        $j('#locationTitle').html(regionResult.LocationDisplayText);
                        $hidRegionID.val(regionResult.RegionID);
                        $hidSearchType.val(SearchTypeEnum.Region);
                        $j('#errorMessage').hide();
                        $locationLayer.hide();
                    }
                    else {
                        //show error
                        $j('#errorMessage').show().html(regionResult.StatusMessage);
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    if (EditLocationPrefObj.IsVerbose) { alert(textStatus); }
                }
            });
        }
        catch (e) {
            if (EditLocationPrefObj.IsVerbose) { alert(e); }
        }

        return false;
    });

    $j('#<%=lnkResetRegion.ClientID%>').click(function() {
        try {
            if (EditLocationPrefObj.AccountRegionID != "") {
                $j('#<%=ddlRegionCountry.ClientID%>').val(EditLocationPrefObj.AccountCountryRegionID);
                EditLocationPreference_CountryChanged(EditLocationPrefObj.AccountStateRegionID, EditLocationPrefObj.AccountCityName);
                $hidRegionID.val(EditLocationPrefObj.AccountRegionID);
                $hidSearchType.val(SearchTypeEnum.Region);
            }
        }
        catch (e) {
            if (EditLocationPrefObj.IsVerbose) { alert(e); }
        }

        return false;
    });

    $j('#<%=lnkAreaCodeSave.ClientID%>').click(function() {
        //validate area codes
        var newCountryRegionID = $j('#<%=ddlAreaCodeCountry.ClientID%>').val();
        var areaCodes = "";
        $j("#<%=divAreaCode.ClientID%> input[type='text']").each(function() {
            if ($j(this).val() != "") {
                areaCodes += $j(this).val() + ",";
            }
        });

        if (areaCodes.length > 0) {
            areaCodes = areaCodes.substring(0, areaCodes.length - 1);
        }
        
        try {
            $j.ajax({
                type: "POST",
                url: "/Applications/API/RegionService.asmx/GetAreaCodeInfo",
                data: "{ areaCodes: \"" + areaCodes + "\", countryRegionId: " + newCountryRegionID + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(result) {
                    var regionResult = (typeof result.d == 'undefined') ? result : result.d;
                    if (regionResult.Status == 2) {
                        $j('#<%=divAreaCodeLabel.ClientID %>').show();
                        $j('#spanLocation').html(regionResult.LocationDisplayText);
                        $j('#locationTitle').html(regionResult.LocationDisplayText);
                        $hidAreaCodes.val(regionResult.ValidAreaCodes);
                        $hidSearchType.val(SearchTypeEnum.AreaCode);
                        $j('#errorMessage').hide();
                        $locationLayer.hide();
                    }
                    else {
                        //show error
                        $j('#errorMessage').show().html(regionResult.StatusMessage);
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    if (EditLocationPrefObj.IsVerbose) { alert(textStatus); }
                }
            });
        }
        catch (e) {
            if (EditLocationPrefObj.IsVerbose) { alert(e); }
        }

        return false;
    });

    $j('#<%=ddlRegionCountry.ClientID%>').change(function() {
        //alert('hi from region country change');
        EditLocationPreference_CountryChanged("", "");
        return false;
    });

    $j('#<%=ddlState.ClientID%>').change(function() {
        var newStateRegionID = $j('#<%=ddlState.ClientID %>').val();

        $j('#<%=txtCity.ClientID %>').val('');
        EditLocationPreference_LoadCityAutoComplete(newStateRegionID, "");
        
        return false;
    });

    $j('#<%=lnkCityLookup.ClientID%>').click(function() {
        try {
            var parentRegionID = $j('#<%=ddlRegionCountry.ClientID%>').val();
            if ($j('#<%=ddlState.ClientID%>').is(':visible')) {
                parentRegionID = $j('#<%=ddlState.ClientID%>').val();
            }
            window.open('/Applications/MemberProfile/CityList.aspx?plid=' + EditLocationPrefObj.BrandID 
                + '&CityControlName=<%=txtCity.ClientID %>&ParentRegionID=' + parentRegionID, '', 'height=415px,width=500px,scrollbars=yes');
        }
        catch (e) {
            if (EditLocationPrefObj.IsVerbose) { alert(e); }
        }
        return false;
    });

    function EditLocationPreference_CountryChanged(preSelectedStateRegionID, preSelectedCityName) {
        var newCountryRegionID = $j('#<%=ddlRegionCountry.ClientID%>').val();
        
        //get new state list, if applicable
        $j.ajax({
            type: "POST",
            url: "/Applications/API/RegionService.asmx/GetRegionStates",
            data: "{countryRegionId:" + newCountryRegionID + "}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function(result) {
                var regionResult = (typeof result.d == 'undefined') ? result : result.d;
                if (regionResult.Status == 2) {
                    var divState = $j('#<%=divState.ClientID%>');
                    var ddlState = $j('#<%=ddlState.ClientID%>');
                    ddlState.empty();

                    $j.each(regionResult.StatesList, function(key, value) {
                        $j("<option>").attr("value", key).text(value).appendTo(ddlState); 
                    });

                    if (regionResult.hasStates) {
                        divState.show();
                        if (preSelectedStateRegionID != "") {
                            ddlState.val(preSelectedStateRegionID);
                        }
                        EditLocationPreference_LoadCityAutoComplete(ddlState.val(), "");
                    } else {
                        divState.hide();
                        EditLocationPreference_LoadCityAutoComplete(newCountryRegionID, "");
                    }

                    if (preSelectedCityName != "") {
                        $j('#<%=txtCity.ClientID %>').val(preSelectedCityName);
                    }
                    else {
                        $j('#<%=txtCity.ClientID %>').val('');
                    }
                    
                    $j('#errorMessage').hide();
                }
                else {
                    $j('#errorMessage').show().html(regionResult.StatusMessage);
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                if (EditLocationPrefObj.IsVerbose) { alert(textStatus); }
            }
        });
    }

    function EditLocationPreference_LoadCityAutoComplete(parentRegionID, description) {
        //get new city list
        $j.ajax({
            type: "POST",
            url: "/Applications/API/RegionService.asmx/GetRegionCities",
            data: "{parentRegionID:" + parentRegionID + ", description: \"" + description + "\"}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function(result) {
                var regionResult = (typeof result.d == 'undefined') ? result : result.d;
                if (regionResult.Status == 2) {
                    if (regionResult.CityList != null) {
                        $j('#<%=txtCity.ClientID %>').autocomplete({
                            source: regionResult.CityList,
                            minLength: 3
                        });
                        
                        $j('.ui-autocomplete').click(function(e) {
                            e.stopPropagation();
                        });
                    }

                    $j('#errorMessage').hide();
                }
                else {
                    $j('#errorMessage').show().html(regionResult.StatusMessage);
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                if (EditLocationPrefObj.IsVerbose) { alert(textStatus); }
            }
        });
    }

    function EditLocationPreference_ResetVisibility() {
        $divZipCode.hide();
        $divRegion.hide();
        $divAreaCode.hide();
    }

    $j(document).ready(function () {
        if (EditLocationPrefObj.InitialCityParentRegionID != "" && !EditLocationPrefObj.IsRegOverlay) {
            EditLocationPreference_LoadCityAutoComplete(EditLocationPrefObj.InitialCityParentRegionID, "");
        }
    })

</script>
