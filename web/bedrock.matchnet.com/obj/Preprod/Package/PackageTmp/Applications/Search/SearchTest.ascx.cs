using System;
using System.Data;
using System.Drawing;
using System.IO;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Xml;
using Matchnet.Web.Framework;
using Matchnet.Search.Interfaces;
using Matchnet.Search.ServiceAdapters;
using Matchnet.Search.ValueObjects;

namespace Matchnet.Web.Applications.Search
{

	/// <summary>
	///		Summary description for SearchTest.
	/// </summary>
	public class SearchTest : FrameworkControl
	{

		private void Page_Load(object sender, System.EventArgs e)
		{
			SearchPreferenceCollection searchPreferences = SearchUtil.ConvertRequestToSearchPreferences(Request.Params);

			// Perform the search with a page of results (0-49)
			MatchnetQueryResults searchResults = MemberSearchSA.Instance.Search(searchPreferences
				, g.Brand.Site.Community.CommunityID
				, g.Brand.Site.SiteID
				, 0
				, 49);

			XmlDocument xmlDoc = SearchUtil.TransformResultsToXml(searchResults);

			StringWriter sw = new StringWriter();
			XmlTextWriter xw = new XmlTextWriter(sw);
			xmlDoc.WriteTo(xw);

			// Here is the XML structure in string format.
			string xmlText = sw.ToString();

			Response.Write(xmlText);
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
	}
}
