﻿using System;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using Matchnet.Content.ServiceAdapters;

using Matchnet.Web.Applications.Article.Util;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Globalization;
using Matchnet.Content.ValueObjects.Article;
namespace Matchnet.Web.Applications.Article.Controls
{
    public partial class ArticleContent20 : FrameworkControl
    {
        public const string SUBSCRIPTION_COST = "SUBSCRIPTIONCOST";

		public const string SUBSCRIPTION_COST_TOKEN = "((" + SUBSCRIPTION_COST + "))";

	

		private int _ArticleID = Constants.NULL_INT;
		public int ArticleID
		{
			get { return (_ArticleID); }
			set { _ArticleID = value; }
		}

		private int _CategoryID = Constants.NULL_INT;
		public int CategoryID
		{
			get { return (_CategoryID); }
			set { _CategoryID = value; }
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			try
			{
				if(ArticleID == Constants.NULL_INT)
				{
					if(CategoryID != Constants.NULL_INT)
					{
						ArticleID = ArticleUtils.DetermineArticleIDFromCategoryID(CategoryID, g.Brand.Site.SiteID);
					}
					else
					{
						ArticleID = ArticleUtils.DetermineArticleIDFromRequest(Request, g.Brand.Site.SiteID);
					}
				}

               	string articleText = string.Empty;
				
				// If the supplied articleID doesn't exist for the site, an Exception is thrown.  
				// Log the exception and gracefully redirect to home page with error message.
				try
				{
				    SiteArticle siteArticle =
				        ((SiteArticle) (ArticleSA.Instance.RetrieveSiteArticle(ArticleID, g.Brand.Site.SiteID)));
                    articleText = siteArticle.Content;

                    if (!string.IsNullOrEmpty(siteArticle.PageTitle))
                    {
                        g.SetPageTitle(siteArticle.PageTitle);
                    }
                    if (!string.IsNullOrEmpty(siteArticle.MetaDescription))
                    {
                        g.SetMetaDescriptionWithTag(siteArticle.MetaDescription);
                    }
				}
				catch( Exception ex )
				{
					g.ProcessException( new Exception("ArticleContent.Page_Load, Line 62: Article not found, ArticleID: " + ArticleID + " SiteID: " + g.Brand.Site.SiteID,ex) );
					g.Transfer( "/Default.aspx?rc=WE_WERE_UNABLE_TO_FIND_THE_PAGE_YOU_ARE_LOOKING_FOR_X" );
				}

				CategoryID = ArticleUtils.DetermineCategoryIDFromRequest(Request);

				if( CategoryID == 2006 ) // TT# 12397 only use the nav if Category is HappyEndings
				{
					SetupNav();

				}

				//Per discussions with Rob Young, Erik Phipps, and Don Lee, I am adding the ExpandImageTokens() functionality to articles:
				articleText = Localizer.ExpandImageTokens(articleText, g.Brand);

				//	Sometimes in articles there is a need for a ((SubscriptionPlan)) grid
				//	that shows the various subscription plan options.  This is only needed
				//	in certain cases, so we don't want to go through the hassle/load of doing
				//	this all the time, but if the SubscriptionPlan token is present then
				//	we need to render the plan DataGrid and stuff it into the translator.

				int articleSubPlanCount = articleText.IndexOf(SUBSCRIPTION_COST_TOKEN);
				if(articleSubPlanCount >= 0)
				{
					//	Token is present.  Need to set up the substitution.
					StringWriter stringWriter = new StringWriter();
					HtmlTextWriter htmlWriter = new HtmlTextWriter(stringWriter);

					Matchnet.Web.Framework.SubscriptionPlanDataGrid SubPlan = new SubscriptionPlanDataGrid();
					DataGrid subscriptionGrid = SubPlan.createGrid();
					subscriptionGrid.RenderControl(htmlWriter);
					string subGridHtml = stringWriter.ToString();
					g.ExpansionTokens.Add(SUBSCRIPTION_COST, subGridHtml);
				}

				//	Run all substitutions for Domain, etc.

				articleContent.Text = Localizer.ExpandTokens(articleText, g.ExpansionTokens);
				
				
				//if the site is american singles then we need to display the third party artciles
				if(g.Brand.Site.SiteID == 101)
				{
					if (CategoryID == 2308)
					{
						plcArticleContent.Visible = false;
						plcThirdPartyArticleMan.Visible = true;
					}
					
					if (CategoryID == 2010)
					{
						plcArticleContent.Visible = false;
						plcThirdPartyArticleWoman.Visible = true;
					}
				}

                if (g.Brand.Site.SiteID == 101
                    || g.Brand.Site.SiteID == 103)
                {
                    if (CategoryID == 1000649)
                    {
                        plcArticleContent.Visible = false;
                        phlTotalBeauty.Visible = true;
                    }
                }									
			}
			catch (Exception ex)
			{
				g.ProcessException(ex);
			}
		}

		private void SetupNav()
		{
			// for prev/next 10 links
			NavStartIndex.Value = Request[NavStartIndex.UniqueID];

			// top nav
            //plcTopArticleNav.Visible = true;
            topArticleNav.SiteID = g.Brand.Site.SiteID;
            topArticleNav.CategoryID = CategoryID;
            topArticleNav.ArticleID = ArticleID;

			// bottom nav
			plcBottomArticleNav.Visible = true;
			bottomArticleNav.SiteID = g.Brand.Site.SiteID;
			bottomArticleNav.CategoryID = CategoryID;
			bottomArticleNav.ArticleID = ArticleID;
           
		}

		/// <summary>
		/// called by either nav's event handlers to update the two
		/// when next 10 or previous 10 is selected.
		/// </summary>
		/// <param name="newIndex"></param>
		public void UpdateNavs( int newIndex )
		{
            topArticleNav.JumpTen( newIndex );
			bottomArticleNav.JumpTen( newIndex );
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
	}
    
}