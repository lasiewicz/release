﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Lobby.ascx.cs" Inherits="Matchnet.Web.Applications.Article.Lobby" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn" TagName="galleryMiniProfile" Src="/Framework/Ui/BasicElements/GalleryMiniProfile20.ascx" %>
<%@ Register TagPrefix="mn" TagName="miniProfile" Src="/Framework/Ui/BasicElements/MiniProfile20.ascx" %>
<%@ Register TagPrefix="result" TagName="ResultList" Src="../../Framework/Ui/SearchElements/ResultList.ascx" %>
<%@ Register TagPrefix="uc2" TagName="ProfileFilmStrip" Src="~/Framework/Ui/BasicElements/ProfileFilmStrip.ascx" %>
<%@ Register TagPrefix="uc1" TagName="MicroProfileList" Src="../../Framework/Ui/BasicElements/MicroProfileList.ascx" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="uc2" TagName="HeroProfileFilmStrip" Src="~/Framework/Ui/BasicElements/HeroProfileFilmStrip.ascx" %>
<div id="lobby-page" <%=GetLobbyPageClass() %> >
    <asp:PlaceHolder runat="server" ID="plcAcademicVisitor" Visible="False">
        <div id="lobby-header-academic">
            <h1>
                <mn:Txt ID="txtMainTitleAcademicVisitor" runat="server" ResourceConstant="TXT_MAIN_TITLE" />
            </h1>
        </div>
        <div id="lobby-main-article">
            <div id="main-article-txt">
                <mn:Txt runat="server" ResourceConstant="TXT_MAIN_ARTICLE" ID="txtMainArticleAcademicVisitor" />
            </div>
            <div id="main-article-image">
                <mn:Image runat="server" ID="imgMainArticleAcademicVisitor" />
            </div>
        </div>
        <hr />
        <h2 class="articles-title">
            <mn:Txt runat="server" ID="txtMemberForVisitors" ResourceConstant="TXT_MEMBERS" />
        </h2>
        <hr />
        <result:ResultList ID="SearchResultListVisitorsMen" runat="server" ResultListContextType="AcademicVisitorSearchResultMen"
            EnableMultipleSearchViews="false" PageSize="4" LoadControlAutomatically="false"
            GalleryView="true"></result:ResultList>
        <div class="button-div">
            <asp:HyperLink runat="server" ID="lnkMoreMembersVisitorMen" CssClass="purple button">
                <mn:Txt runat="server" ID="txtMoreMembersVisitorMen" ResourceConstant="TXT_MORE_MEMBERS_ACADEMIC" />
            </asp:HyperLink>
        </div>
        <result:ResultList ID="SearchResultListVisitorsWomen" runat="server" ResultListContextType="AcademicVisitorSearchResultWomen"
            EnableMultipleSearchViews="false" PageSize="4" LoadControlAutomatically="false"
            GalleryView="true"></result:ResultList>
        <div class="button-div">
            <asp:HyperLink runat="server" ID="lnkMoreMembersVisitorWomen" CssClass="purple button">
                <mn:Txt runat="server" ID="txtMoreMembersVisitorWomen" ResourceConstant="TXT_MORE_MEMBERS_ACADEMIC" />
            </asp:HyperLink>
        </div>
        <hr />
        <h2 class="articles-title">
            <mn:Txt ID="txtArticlesTitleVisitor" runat="server" ResourceConstant="TXT_ARTICLES_TITLE" />
        </h2>
        <hr />
        <div id="articlesVisitor">
            <mn:Txt ID="txtArticlesVisitor" runat="server" ResourceConstant="TXT_ARTICLES" />
            <div class="button-div">
                <asp:HyperLink runat="server" ID="lnkMoreArticlesVisitor" CssClass="purple button"
                    Target="_blank">
                    <mn:Txt runat="server" ID="txtMoreArticlesVisitor" ResourceConstant="TXT_MORE_ARTICLES" />
                </asp:HyperLink>
            </div>
        </div>
        <hr />
    </asp:PlaceHolder>
    <asp:PlaceHolder runat="server" ID="plcAcademicMember" Visible="False">
        <div id="hp-hero">
            <uc2:HeroProfileFilmStrip ID="HeroProfileFilmStripMatches" runat="server" TrackingParam="ClickFrom=Homepage - Your Matches" />
        </div>
        <hr />
        <h2 class="articles-title">
            <mn:Txt runat="server" ID="txtMemberForMembers" ResourceConstant="TXT_MEMBERS" />
        </h2>
        <hr />
        <result:ResultList ID="SearchResultListMembers" runat="server" ResultListContextType="SectorSearchResult"
            EnableMultipleSearchViews="false" PageSize="8" LoadControlAutomatically="false"
            GalleryView="true"></result:ResultList>
        <div class="button-div">
            <asp:HyperLink runat="server" ID="lnkMoreMembersAcademic" CssClass="purple button">
                <mn:Txt runat="server" ID="txtMoreMembersAcademic" ResourceConstant="TXT_MORE_MEMBERS_ACADEMIC" />
            </asp:HyperLink>
        </div>
    </asp:PlaceHolder>
    <asp:PlaceHolder runat="server" ID="plcGeneric" Visible="True">
        <div id="lobby-header">
            <h1>
                <mn:Txt ID="txtMainTitle" runat="server" ResourceConstant="TXT_MAIN_TITLE" />
            </h1>
        </div>
        <div id="lobby-main-article">
            <div id="main-article-txt">
                <mn:Txt runat="server" ResourceConstant="TXT_MAIN_ARTICLE" ID="txtMainArticle" />
            </div>
            <div id="main-article-image">
                <mn:Image runat="server" ID="imgMainArticle" ResourceConstant="TXT_SECTION_NAME_ALT" />
            </div>
        </div>
        <h2 class="articles-title">
            <mn:Txt ID="txtArticlesTitle" runat="server" ResourceConstant="TXT_ARTICLES_TITLE" />
        </h2>
        <hr />
        <div id="articles">
            <mn:Txt ID="txtArticles" runat="server" ResourceConstant="TXT_ARTICLES" />
            <div class="button-div">
                <asp:HyperLink runat="server" ID="lnkMoreArticles" CssClass="purple button" Target="_blank">
                    <mn:Txt runat="server" ID="txtMoreArticles" ResourceConstant="TXT_MORE_ARTICLES" />
                </asp:HyperLink>
            </div>
        </div>
        <hr />
        <h2 class="articles-title">
            <mn:Txt runat="server" ID="txtMembers" ResourceConstant="TXT_MEMBERS" />
        </h2>
        <hr />
        <div id="lobby-profiles">
            <div>
                <a href="/Applications/Search/SearchPreferences.aspx">
                    <mn:Txt ID="Txt1" runat="server" ResourceConstant="TXT_SEARCH_PREFERENCES" />
                </a>
            </div>
            <asp:Repeater runat="server" ID="rptMiniProfiles">
                <ItemTemplate>
                    <mn:miniProfile runat="server" ID="miniProfile" Member="<%# Container.DataItem %>"
                        DisplayContext="SearchResult" OnlineImageName="icon-status-online.gif" OfflineImageName="icon-status-offline.gif"
                        IsMatchMeterEnabled="true" />
                </ItemTemplate>
            </asp:Repeater>
            <asp:Repeater runat="server" ID="rptGalleryMiniProfiles">
                <ItemTemplate>
                    <mn:galleryMiniProfile runat="server" ID="galleryMiniProfile" Member="<%# Container.DataItem %>"
                        DisplayContext="SearchResult" OnlineImageName="icon-status-online.gif" OfflineImageName="icon-status-offline.gif"
                        IsMatchMeterEnabled="true" />
                </ItemTemplate>
            </asp:Repeater>
            <div class="button-div">
                <asp:HyperLink runat="server" ID="lnkMoreMembers" CssClass="purple button">
                    <mn:Txt runat="server" ID="txtMoreMembers" ResourceConstant="TXT_MORE_MEMBERS" />
                </asp:HyperLink>
            </div>
        </div>
        <hr />
    </asp:PlaceHolder>
</div>
<mn:txt id="txtGoogleRemarketingCode" runat="server" resourceconstant="JAVASCRIPT_GOOGLE_REMARKETING_TAG" expandimagetokens="true" />
