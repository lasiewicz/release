﻿using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections;

using Matchnet.Web.Framework;

using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.Article;

namespace Matchnet.Web.Applications.Article.Controls
{
    public partial class ArticleNav20 : FrameworkControl
    {
        protected HtmlInputHidden NavStartIndex;
        private int _CategoryID = Constants.NULL_INT;
        private int _SiteID = Constants.NULL_INT;
        private int _ArticleID = Constants.NULL_INT;
        private string ArticleTitle = string.Empty;
        private int CurArticleBound;
        private const string ARTICLEVIEW_URL = "/Applications/Article/ArticleView.aspx?";

        #region public properties
        public int CategoryID
        {
            set
            {
                _CategoryID = value;
            }
        }

        public int SiteID
        {
            set
            {
                _SiteID = value;
            }
        }

        public int ArticleID
        {
            set
            {
                _ArticleID = value;
            }
        }
        #endregion

        private void Page_Load(object sender, System.EventArgs e)
        {
            // Make sure we have at least a category and site to work with
            if (_CategoryID == Constants.NULL_INT || _SiteID == Constants.NULL_INT)
            {
                return;
            }

            SiteArticleCollection siteArticles = ArticleSA.Instance.RetrieveCategorySiteArticles(_CategoryID, _SiteID);
            // no need for nav if no articles exist
            if (siteArticles == null || siteArticles.Count == 0)
            {
                return;
            }

            // if not supplied with ArticleID, use first article
            if (_ArticleID == Constants.NULL_INT)
            {
                _ArticleID = siteArticles[0].ArticleID;
            }

            // NavStartIndex is shared among all navs 
            NavStartIndex = (HtmlInputHidden)NamingContainer.FindControl("NavStartIndex");

            int articleIndex = getArticleIndex(siteArticles);
            SetupPreviousLink(articleIndex, siteArticles);
            SetupNextLink(articleIndex, siteArticles);
            idArticleNavButtons.CategoryID = _CategoryID;
            idArticleNavButtons.ArticleID = _ArticleID;

            // if its initial load, load like normal (centering on current article).  
            // Postbacks will be handled in the click event of the lnkNextTen and lnkPreviousTen linkbuttons.
            //if (!IsPostBack)
            if (string.IsNullOrEmpty(Request.QueryString["nextten"]) && string.IsNullOrEmpty(Request.QueryString["prevten"]))
            {
                // using articleIndex - 4 makes the nav bar always center on the current article
                BindData(articleIndex - 4, siteArticles);
                SetupNextPreviousTenLinks(articleIndex - 4, siteArticles.Count, siteArticles);
            }
        }

        private void Page_PreRender(object sender, System.EventArgs e)
        {
            NavStartIndex.Value = Request.QueryString["NavStartIndex"];

            if (!string.IsNullOrEmpty(Request.QueryString["nextten"]))
            {
                int startIndex = Conversion.CInt(NavStartIndex.Value, 0);
                ArticleContent20 ac = (ArticleContent20)NamingContainer;
                ac.UpdateNavs(startIndex + 10);
            }
            else
            {
                if (!string.IsNullOrEmpty(Request.QueryString["prevten"]))
                {
                    int startIndex = Conversion.CInt(NavStartIndex.Value, 0);
                    ArticleContent20 ac = (ArticleContent20)NamingContainer;
                    ac.UpdateNavs(startIndex - 10);
                }
            }
        }

        private void BindData(int startIndex, SiteArticleCollection siteArticles)
        {
            ArrayList displayArticles = SetupDisplayRange(startIndex, siteArticles);
            rptIndexLinks.DataSource = displayArticles;
            rptIndexLinks.DataBind();
        }

        private ArrayList SetupDisplayRange(int startIndex, SiteArticleCollection siteArticles)
        {
            ArrayList retList = new ArrayList();
            if (NavStartIndex == null)
                NavStartIndex = (HtmlInputHidden)NamingContainer.FindControl("NavStartIndex");
            if (startIndex < 1)
            {
                CurArticleBound = 1;
                NavStartIndex.Value = "0";

                // add 1st ten articles
                for (int i = 0; (i < 10) && (i < siteArticles.Count); i++)
                {
                    retList.Add(siteArticles[i].ArticleID);
                }
            }
            else
            {
                CurArticleBound = startIndex + 1;
                NavStartIndex.Value = startIndex.ToString();

                // add the 4 before ArticleIndex
                // add ArticleIndex
                // add up to next 5.
                for (int i = startIndex; (i < siteArticles.Count) && (i - startIndex < 10); i++)
                {
                    retList.Add(siteArticles[i].ArticleID);
                }
            }

            return retList;
        }

        private int getArticleIndex(SiteArticleCollection siteArticles)
        {
            int index;
            for (index = 0; index < siteArticles.Count; index++)
            {
                if (siteArticles[index].ArticleID == _ArticleID)
                {
                    ArticleTitle = siteArticles[index].Title;
                    break;
                }
            }
            return index;
        }

        private void SetupPreviousLink(int articleIndex, SiteArticleCollection siteArticles)
        {
            // if not the first article
            if (articleIndex > 0)
            {
                lnkPreviousArticle.Visible = true;
                lnkPreviousArticle.NavigateUrl = ARTICLEVIEW_URL +
                    "CategoryID=" + _CategoryID +
                    "&ArticleID=" + siteArticles[articleIndex - 1].ArticleID;
            }
        }

        private void SetupNextLink(int articleIndex, SiteArticleCollection siteArticles)
        {
            // if not the last article
            if (articleIndex < siteArticles.Count - 1)
            {
                lnkNextArticle.Visible = true;
                lnkNextArticle.NavigateUrl = ARTICLEVIEW_URL +
                    "CategoryID=" + _CategoryID +
                    "&ArticleID=" + siteArticles[articleIndex + 1].ArticleID;
            }
        }

        private void SetupNextPreviousTenLinks(int startIndex, int maxIndex, SiteArticleCollection siteArticles)
        {
            if (startIndex < 0)
            {
                startIndex = 0;
            }

            if (startIndex >= 10)
            {
                lnkPreviousTen.Visible = true;
            }

            if (startIndex < maxIndex - 10)
            {
                lnkNextTen.Visible = true;
            }

            string currentURL = ARTICLEVIEW_URL +
                    "CategoryID=" + _CategoryID +
                    "&ArticleID=" + _ArticleID +
                    "&NavStartIndex=" + NavStartIndex.Value;
            lnkPreviousTen.NavigateUrl = currentURL + "&prevten=1";
            lnkNextTen.NavigateUrl = currentURL + "&nextten=1";
        }

        private void rptIndexLinks_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem == null)
            {
                return;
            }

            int curArticleID = (int)e.Item.DataItem;
            HyperLink curLink = (HyperLink)e.Item.FindControl("lnkIndexLink");
            Txt curLinkText = (Txt)e.Item.FindControl("txtCurrentLinkText");
            if (_ArticleID == curArticleID) // current article being displayed
            {
                curLink.Visible = false;
                curLinkText.Text = CurArticleBound.ToString() + ". " + ArticleTitle;
                curLinkText.Visible = true;
            }
            else // any other article
            {
                curLink.Text = CurArticleBound.ToString();
                curLink.NavigateUrl = ARTICLEVIEW_URL +
                    "CategoryID=" + _CategoryID +
                    "&ArticleID=" + curArticleID;
            }
            CurArticleBound++;
        }

        /// <summary>
        /// called by the container control to update the nav after
        /// next 10 or previous 10 is selected
        /// </summary>
        /// <param name="startIndex"></param>
        public void JumpTen(int startIndex)
        {
            SiteArticleCollection siteArticles = ArticleSA.Instance.RetrieveCategorySiteArticles(_CategoryID, _SiteID);
            
            BindData(startIndex, siteArticles);
            SetupNextPreviousTenLinks(startIndex, siteArticles.Count, siteArticles);
        }

        // These event handlers tell the container control to update all
        // of the navs with the new start index.
        private void lnkNextTen_Click(object sender, EventArgs e)
        {
            int startIndex = Conversion.CInt(NavStartIndex.Value, 0);
            ArticleContent20 ac = (ArticleContent20)NamingContainer;
            ac.UpdateNavs(startIndex + 10);
        }
        private void lnkPreviousTen_Click(object sender, EventArgs e)
        {
            int startIndex = Conversion.CInt(NavStartIndex.Value, 0);
            ArticleContent20 ac = (ArticleContent20)NamingContainer;
            ac.UpdateNavs(startIndex - 10);
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
            this.PreRender += new System.EventHandler(this.Page_PreRender);
            this.rptIndexLinks.ItemDataBound += new RepeaterItemEventHandler(rptIndexLinks_ItemDataBound);
        }
        #endregion

    }
}