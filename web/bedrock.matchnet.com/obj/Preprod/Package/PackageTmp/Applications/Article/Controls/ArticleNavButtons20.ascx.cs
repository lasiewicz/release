﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Web.Framework;

using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.Article;
namespace Matchnet.Web.Applications.Article.Controls
{
    public partial class ArticleNavButtons20 : FrameworkControl
    {


        private string _prevArticleLink = "";
        private string _nextArticleLink = "";
        private int _categoryID = Constants.NULL_INT;
        private int _articleID = Constants.NULL_INT;
       
        private const string ARTICLEVIEW_URL = "/Applications/Article/ArticleView.aspx?";

        #region public properties
        public int CategoryID
        {
            set
            {   _categoryID = value; }
        }

     

        public int ArticleID
        {
            set
            {   _articleID = value; }
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                SiteArticleCollection siteArticles = ArticleSA.Instance.RetrieveCategorySiteArticles(_categoryID, g.Brand.Site.SiteID);

                int index = getArticleIndex(siteArticles);
                _prevArticleLink = SetupPreviousLink(index, siteArticles);
                _nextArticleLink = SetupNextLink(index, siteArticles);
                if (!String.IsNullOrEmpty(_prevArticleLink))
                {
                    lnkPrevious.HRef = _prevArticleLink;
                }
                else
                {
                    lnkPrevious.Visible = false;
                }

                if (!String.IsNullOrEmpty(_nextArticleLink))
                {
                    lnkNext.HRef = _nextArticleLink;
                }
                else
                {
                    lnkNext.Visible = false;
                }


            }
            catch (Exception ex)
            { }
        }


        private string SetupPreviousLink(int articleIndex, SiteArticleCollection siteArticles)
        {
            // if not the first article
            if (articleIndex > 0)
            {
                return ARTICLEVIEW_URL +
                     "CategoryID=" + _categoryID +
                     "&ArticleID=" + siteArticles[articleIndex - 1].ArticleID;
            }
            else
            {
                return null;
            }
        }

        private string SetupNextLink(int articleIndex, SiteArticleCollection siteArticles)
        {
            // if not the last article
            if (articleIndex < siteArticles.Count - 1)
            {
                return ARTICLEVIEW_URL +
                "CategoryID=" + _categoryID +
                "&ArticleID=" + siteArticles[articleIndex + 1].ArticleID;
            }
            else
            {
                return null;
            }
        }

        private int getArticleIndex(SiteArticleCollection siteArticles)
        {
            int index;
            for (index = 0; index < siteArticles.Count; index++)
            {
                if (siteArticles[index].ArticleID == _articleID)
                {

                    break;
                }
            }
            return index;
        }
    }
}