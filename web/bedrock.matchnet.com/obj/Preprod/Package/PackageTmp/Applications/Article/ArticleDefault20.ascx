﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="ArticleDefault20.ascx.cs" Inherits="Matchnet.Web.Applications.Article.ArticleDefault20" %>
<%@ Register TagPrefix="uc1" TagName="ArticleContent"     Src="Controls/ArticleContent20.ascx" %>
<div class="page-container editorial">
	<asp:PlaceHolder ID="plcTitle" Runat="server" Visible=False>
		<h1><asp:Label id="lblTitle" runat="server"></asp:Label></h1>
	</asp:PlaceHolder>
	<uc1:articlecontent id="ArticleContent1" runat="server" />
</div>
