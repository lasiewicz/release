namespace Matchnet.Web.Applications.Article
{
	using System;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;
    using Matchnet.Web.Framework;
	

	/// <summary>
	///		Summary description for FAQMain.
	/// </summary>
	public class FAQMain : FrameworkControl
	{
		protected Matchnet.Web.Framework.Title ttlFAQ;
		//protected System.Web.UI.WebControls.Repeater SubCategoryRepeater;

		private void Page_Load(object sender, System.EventArgs e)
		{
			try
			{
				//	Set the custom Breadcrumbs for this page			
				g.BreadCrumbTrailHeader.SetTwoLinkCrumb(g.GetResource("TXT_FAQ", this), null);
				g.BreadCrumbTrailFooter.SetTwoLinkCrumb(g.GetResource("TXT_FAQ", this), null);

                g.SetPageTitle(g.GetResource("TXT_PAGE_TITLE", this));

                if (g.BreadCrumbTrailHeader != null)
                {
                    g.BreadCrumbTrailHeader.SetTwoLinkCrumb(g.GetResource("NAV_FREQUENTLY_ASKED_QUESTIONS", this),
                                                            g.AppPage.App.DefaultPagePath);

                }
                if (g.BreadCrumbTrailFooter != null)
                {
                    g.BreadCrumbTrailFooter.SetTwoLinkCrumb(g.GetResource("NAV_FREQUENTLY_ASKED_QUESTIONS", this),
                                                           g.AppPage.App.DefaultPagePath);
                }			
			}
			catch (Exception ex)
			{
				g.ProcessException(ex);
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
	}
}
