﻿using System;
using System.Web.UI.HtmlControls;

using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.Article;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Content.ValueObjects.PageConfig;
using Matchnet.Session.ValueObjects;
using Matchnet.Web.Framework;


namespace Matchnet.Web.Applications.Article
{
    public partial class ArticleView20 : FrameworkControl
    {
        private const string ARTICLEVIEW_URL = "/Applications/Article/ArticleView.aspx?";

        protected System.Web.UI.WebControls.Label lblTitle;
        protected System.Web.UI.WebControls.Literal lnkMoreTopics;
        protected System.Web.UI.WebControls.Panel panelEndNav;
        protected Matchnet.Web.Framework.Txt moreTopics;
        protected Matchnet.Web.Framework.Txt backLink;

        protected HtmlGenericControl AlignOverrideDiv;

        private int _categoryID;
        private int _articleID;
        public int CategoryID
        {
            get { return (_categoryID); }
            set { _categoryID = value; }
        }

        public int ArticleID
        {
            get { return (_articleID); }
            set { _articleID = value; }
        }

        private void Page_Load(object sender, System.EventArgs e)
        {
            try
            {
                // If this article is being displayed in a popup window, display a left margin:
                if (g.LayoutTemplate.Equals(LayoutTemplate.Popup) || g.LayoutTemplate.Equals(LayoutTemplate.PopupOnce))
                {
                    //Issue #11854: This is a hack to keep the "rightNew" div from being right-aligned when the content is displayed in a popup window.
                    //Ultimately, the rightNew div should be moved out of the invidual controls into the LayoutTemplates so that they can better control the page layout.

                    //Issue #13252: This messed up the hebrew pages so I added the appropirate css to hebcommonYNM4 for rightNewAlignOverride -mroberts
                    //if (g.Brand.Site.Direction != DirectionType.rtl)
                    //{
                    AlignOverrideDiv.Attributes.Add("class", "rightNewAlignOverride");
                    //}


                }
                else
                {

                }



                // TT15768 - set up back link and show when the referrer is viewprofile or a result list page (MOL, Search Results, etc.)
                // Either pass in a refUrl parm with the constructed referring URL or enter the article page from any page other than itself.
                string refUri = string.Empty;
                if (Request.Url.AbsoluteUri != null && g.GetReferrerUrl() != null)
                {
                    refUri = g.GetReferrerUrl();
                    if (Request.Params.Get("refUrl") != null && Request.Params.Get("refUrl").IndexOfAny(new char[] { '<', '>', '\'', '\"', ')', '(' }) == -1)
                    {
                        refUri = Server.UrlDecode(Request.Params.Get("refUrl"));
                        g.Session.Add("ArticleRefUrl", refUri, SessionPropertyLifetime.TemporaryDurable);
                    }
                    else if (refUri.ToLower().IndexOf("articleview") == -1)
                    {
                        // ref is not the same as the current page so add it 
                        g.Session.Add("ArticleRefUrl", refUri, SessionPropertyLifetime.TemporaryDurable);
                    }
                }

                if (g.Session["ArticleRefUrl"] != null && !g.Session["ArticleRefUrl"].Equals(String.Empty))
                {
                    // should always be the case
                    backLink.Href = g.Session.Get("ArticleRefUrl").ToString();
                }
                else
                {
                    backLink.Visible = false;
                }



                //	Set up the heading stuff.
                string sCategoryID = Request["CategoryID"];
                CategoryID = Matchnet.Conversion.CInt(sCategoryID);

                string sArticleID = Request["ArticleID"];

                ArticleID = Matchnet.Conversion.CInt(sArticleID);

                if (ArticleID < 0)
                {
                    //to replace articledefault
                    ArticleID = Matchnet.Web.Applications.Article.Util.ArticleUtils.DetermineArticleIDFromRequest(Request, g.Brand.Site.SiteID);

                }
                if (CategoryID == 2010		//	Make It Happen
                    || CategoryID == 2308	//	Singles Talk
                    || CategoryID == 2006)	//	Happy Endings
                {
                    //	This is special handling for Make It Happen (2010), Singles Talk (2308)
                    //	and Happy Endings (2006).  Obviously this isn't the best way to handle
                    //	this but we have a deadline and it will work for now.  In the future
                    //	when some time frees up cleaning this up is a nice TOFIX item

                    //	Always hide the nav for these pages
                    moreTopics.Visible = false;
                    panelEndNav.Visible = false;
                    backLink.Visible = false;

                    if (CategoryID == 2006)
                    {
                        getSuccessStoryExpansionTokens();
                        phSuccessStoryTitle.Visible = true;
                        idArticleNavButtons.Visible = true;
                    }
                    // DataTable siteCategories = categories.RetrieveParentInfo(WebConstants.TEMP_TRANSLATION_ID, CategoryID);
                    SiteCategory siteCategory = ArticleSA.Instance.RetrieveSiteCategory(g.Brand.Site.SiteID, CategoryID);
                    if (siteCategory != null)
                    {
                        string categoryName = siteCategory.Content;

                        //	Kind of a hack to keep these out of actual display
                        categoryName = CleanYNM4(categoryName);

                        string articleName;

                        SiteArticle siteArticle = ArticleSA.Instance.RetrieveSiteArticle(ArticleID, g.Brand.Site.SiteID);

                        idArticleNavButtons.CategoryID = _categoryID;
                        idArticleNavButtons.ArticleID = _articleID;
                        if (siteArticle != null)
                        {
                            articleName = siteArticle.Title;

                            if (!string.IsNullOrEmpty(siteArticle.PageTitle))
                            {
                                g.SetPageTitle(siteArticle.PageTitle);
                            }
                            if (!string.IsNullOrEmpty(siteArticle.MetaDescription))
                            {
                                g.SetMetaDescription(siteArticle.MetaDescription);
                            }
                        }
                        else
                        {
                            articleName = "Can't find article title";
                            phArticleTitle.Visible = false;
                        }

                        articleName = FrameworkGlobals.GetUnicodeText(articleName);

                        //	It looks like these articles have the titles inside the content
                        lblTitle.Text = articleName;

                        //	Set the custom Breadcrumbs for this page			
                        //g.BreadCrumbTrailHeader.SetTwoLinkCrumb(articleName, null);
                        //g.BreadCrumbTrailFooter.SetTwoLinkCrumb(articleName, null);

                        Matchnet.Content.ValueObjects.Article.Article article = Matchnet.Content.ServiceAdapters.ArticleSA.Instance.RetrieveArticle(ArticleID);
                        if (article != null)
                        {
                            SiteCategory siteCategoryFromSiteArticle =
                           Matchnet.Content.ServiceAdapters.ArticleSA.Instance.RetrieveSiteCategory(
                               g.Brand.Site.SiteID, article.CategoryID);

                            if (siteCategoryFromSiteArticle != null && !string.IsNullOrEmpty(siteCategoryFromSiteArticle.Content))
                            {
                                categoryName = siteCategoryFromSiteArticle.Content;
                            }
                        }


                        if (g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.JDateCoIL && ArticleID == 6228 && CategoryID == 2010)
                        {
                            g.Transfer("/Applications/Article/ArticleView.aspx?CategoryID=2308&ArticleID=100001560", "301");
                        }
                        g.BreadCrumbTrailHeader.SetThreeLinkCrumb(categoryName, "/Applications/Article/ArticleDefault.aspx?CategoryID=" + CategoryID, articleName, null);
                        g.BreadCrumbTrailFooter.SetThreeLinkCrumb(categoryName, "/Applications/Article/ArticleDefault.aspx?CategoryID=" + CategoryID, articleName, null);
                    }
                }
                else
                {
                    // DataTable siteCategories = categories.RetrieveParentInfo(WebConstants.TEMP_TRANSLATION_ID, CategoryID);
                    SiteCategory siteCategory = ArticleSA.Instance.RetrieveSiteCategory(g.Brand.Site.SiteID, CategoryID);
                    SiteArticleCollection categoryArticles = ArticleSA.Instance.RetrieveCategorySiteArticles(CategoryID, g.Brand.Site.SiteID);

                    if (siteCategory != null)
                    {
                        string content = siteCategory.Content;

                        //	Kind of a hack to keep these out of actual display
                        content = FrameworkGlobals.GetUnicodeText(CleanYNM4(content));

                        lblTitle.Text = content;

                        // Set the custom Breadcrumbs for this page
                        switch (CategoryID)
                        {
                            //legal exception
                            case 1948:
                                g.BreadCrumbTrailFooter.SetTwoLinkCrumb(g.GetResource("TXT_LEGAL", this), String.Empty);
                                g.BreadCrumbTrailHeader.SetTwoLinkCrumb(g.GetResource("TXT_LEGAL", this), String.Empty);
                                break;

                            //our mission exception
                            case 2060:
                                g.BreadCrumbTrailFooter.SetTwoLinkCrumb(g.GetResource("OUR_MISSION_BREADCRUMB", null), String.Empty);
                                g.BreadCrumbTrailHeader.SetTwoLinkCrumb(g.GetResource("OUR_MISSION_BREADCRUMB", null), String.Empty);

                                break;

                            //about us exception
                            /*THIS MAY NOT GET USED...WILL IS CHECKING.  IF IT DOES, NEED TO ADD ENTRY INTO GLOBAL RESX
                            case 1998:
                            g.BreadCrumbTrailFooter.SetTwoLinkCrumb(g.GetResource("ABOUT_US_BREADCRUMB", null), String.Empty);
                            g.BreadCrumbTrailHeader.SetTwoLinkCrumb(g.GetResource("ABOUT_US_BREADCRUMB", null), String.Empty);*/

                            default:

                                g.BreadCrumbTrailHeader.SetTwoLinkCrumb(content, null);
                                g.BreadCrumbTrailFooter.SetTwoLinkCrumb(content, null);

                                int articleID = Conversion.CInt(Request["ArticleID"]);

                                if (articleID != Constants.NULL_INT)
                                {
                                    SiteArticle siteArticle = ArticleSA.Instance.RetrieveSiteArticle(articleID, g.Brand.Site.SiteID);

                                    if (siteArticle != null)
                                    {
                                        g.BreadCrumbTrailHeader.SetThreeLinkCrumb(content, "/Applications/Article/ArticleDefault.aspx?CategoryID=" + CategoryID, siteArticle.Title, null);
                                        g.BreadCrumbTrailFooter.SetThreeLinkCrumb(content, "/Applications/Article/ArticleDefault.aspx?CategoryID=" + CategoryID, siteArticle.Title, null);
                                    }
                                }
                                break;
                        }
                    }

                    string sHideNav = Request["HideNav"];
                    if (sHideNav != null)
                    {
                        bool hideNav = bool.Parse(sHideNav);
                        if (hideNav)
                        {
                            moreTopics.Visible = false;
                            panelEndNav.Visible = false;
                            backLink.Visible = false;
                        }
                    }

                    if (categoryArticles == null || categoryArticles.Count <= 1) // No other articles in this category.  Hide the moreTopics.
                    {
                        moreTopics.Visible = false;
                        panelEndNav.Visible = false;
                        backLink.Visible = false;
                    }
                }


            }
            catch (Exception ex) { g.ProcessException(ex); }
        }


        /// <summary>
        /// Removes a "(YNM4) " prefix that has been added to articles for the YNM4
        ///	release.  This is a hacky utility method that should probably ultimately
        ///	go away.
        /// </summary>
        /// <param name="myString">string to potentially clean</param>
        /// <returns>
        /// original input or original input with (YNM4) removed from the front (if present)
        /// </returns>
        private string CleanYNM4(string myString)
        {
            string retVal = myString;

            string badPrefix = "(YNM4) ";
            if (myString.StartsWith(badPrefix))
            {
                retVal = myString.Substring(badPrefix.Length);
            }

            return (retVal);
        }


        private void getSuccessStoryExpansionTokens()
        {
            try
            {
                if (CategoryID != 2006)
                    return;

                SiteArticleCollection siteArticles = ArticleSA.Instance.RetrieveCategorySiteArticles(_categoryID, g.Brand.Site.SiteID);
                int index;
                for (index = 0; index < siteArticles.Count; index++)
                {
                    if (siteArticles[index].ArticleID == _articleID)
                    {
                        break;
                    }
                }
                index += 1;
                g.AddExpansionToken("ARTICLE_INDEX", index.ToString());
                g.AddExpansionToken("ARTICLES_COUNT", siteArticles.Count.ToString());
                txtArticleCount.ResourceConstant = "TXT_ARTICLE_AVAILABLE_COUNT";
                txtArticleCount.Visible = true;


            }
            catch (Exception ex)
            { }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);

        }
        #endregion
    }
}