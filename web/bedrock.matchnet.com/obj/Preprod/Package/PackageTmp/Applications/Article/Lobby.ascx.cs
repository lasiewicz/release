﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Configuration.ServiceAdapters.Analitics;
using Matchnet.List.ValueObjects;
using Matchnet.Member.ServiceAdapters;
using Matchnet.MembersOnline.ServiceAdapters;
using Matchnet.MembersOnline.ValueObjects;
using Matchnet.Search.ServiceAdapters;
using Matchnet.Search.ValueObjects;
using Matchnet.Web.Applications.Home;
using Matchnet.Web.Framework;
using Matchnet.Session.ValueObjects;
using Matchnet.Web.Framework.Managers;
using Matchnet.Web.Framework.Ui;

namespace Matchnet.Web.Applications.Article
{

    public partial class Lobby : FrameworkControl
    {
        private const string DOMAINID = "DomainID";
        private BreadCrumbHelper.EntryPoint _heroProfileEntry = BreadCrumbHelper.EntryPoint.HomeHeroProfileMatches;
        private bool _hasMatches = false;

        protected Sector MySector
        {
            get
            {
                Sector result = Sector.Default;
                int sectionID;

                string fullURL = Context.Request.Url.AbsoluteUri;

                if (fullURL.Contains("religious-dating"))
                    result = Sector.Religious;
                else if (fullURL.Contains("academic-dating"))
                    result = Sector.Academic;
                else if (fullURL.Contains("Divorced-dating"))
                    result = Sector.Divorced;
                else if (fullURL.Contains("Senior-dating"))
                    result = Sector.Elderly;
                else if (int.TryParse(Request.QueryString["Section"], out sectionID))
                    result = (Sector)sectionID;

                return result;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                int sectionID;

                // Wire up the List Navigation. (Pagination uses these labels in ResultListHandler.cs)
                g.ListNavigationTop = new System.Web.UI.WebControls.Label();
                g.ListNavigationBottom = new System.Web.UI.WebControls.Label();

                if (MySector != Sector.Default)
                {
                    string sectionString = "_" + MySector.ToString().ToUpper();

                    // set BreadCrumbTrail
                    string sectionName = g.GetResource("TXT_SECTION_NAME" + sectionString, this);
                    string breadCrumbLink = g.GetResource("TXT_BREADCRUMB_LINK", this);
                    g.BreadCrumbTrailHeader.SetTwoLinkCrumb(sectionName, breadCrumbLink);
                    g.BreadCrumbTrailFooter.SetTwoLinkCrumb(sectionName, breadCrumbLink);

                    // set resources constants
                    txtMainTitle.ResourceConstant += sectionString;
                    imgMainArticle.ResourceConstant += sectionString;
                    txtMoreArticles.ResourceConstant += sectionString;
                    txtMembers.ResourceConstant += sectionString;
                    txtMoreMembers.ResourceConstant += sectionString;
                    txtMainArticle.ResourceConstant += sectionString;
                    txtArticlesTitle.ResourceConstant += sectionString;
                    txtArticles.ResourceConstant += sectionString;
                    imgMainArticle.FileName = "main-article-image" + sectionString + ".jpg";
                    imgMainArticle.AlternateText = g.GetResource("TXT_SECTION_NAME_ALT" + sectionString, this);


                    g.SetPageTitle(g.GetResource("PAGE_TITLE" + sectionString, this));
                    g.SetMetaDescriptionWithTag(g.GetResource("META_DESCRIPTION" + sectionString, this));
                    g.SetMeta(g.GetResource("META" + sectionString, this));

                    lnkMoreArticles.NavigateUrl = g.GetResource("TXT_MORE_ARTICLES_LINK" + sectionString, this);

                    lnkMoreMembers.NavigateUrl = "http://" + Request.Url.Host +
                                                 g.GetResource("TXT_MORE_PROFILE_PAGE" + sectionString, this);

                    txtGoogleRemarketingCode.ResourceConstant += sectionString;

                    // set profiles
                    if (IsPostBack || !string.IsNullOrEmpty(Request.QueryString["rc"]))
                    {
                        SetProfiles();
                    }
                    else
                    {
                        SetRandomProfiles(MySector);
                    }

                    if (MySector == Sector.Academic)
                    {

                        if (g.Member == null)
                        {
                            // Academic visitor
                            plcAcademicVisitor.Visible = true;
                            plcGeneric.Visible = false;
                            txtMemberForVisitors.ResourceConstant = txtMembers.ResourceConstant;
                            txtMoreArticlesVisitor.ResourceConstant = txtMoreArticles.ResourceConstant;
                            txtArticlesVisitor.ResourceConstant = txtArticles.ResourceConstant;
                            lnkMoreArticlesVisitor.NavigateUrl = lnkMoreArticles.NavigateUrl;
                            txtArticlesTitleVisitor.ResourceConstant = txtArticlesTitle.ResourceConstant;

                            lnkMoreMembersVisitorMen.NavigateUrl = "http://" + Request.Url.Host +
                                                 g.GetResource("TXT_MORE_PROFILE_PAGE_MEN" + sectionString, this);
                            txtMoreMembersVisitorMen.ResourceConstant = txtMoreMembers.ResourceConstant + "_MEN";
                            lnkMoreMembersVisitorWomen.NavigateUrl = "http://" + Request.Url.Host +
                                                 g.GetResource("TXT_MORE_PROFILE_PAGE_WOMEN" + sectionString, this);
                            txtMoreMembersVisitorWomen.ResourceConstant = txtMoreMembers.ResourceConstant + "_WOMEN";

                            txtMainTitleAcademicVisitor.ResourceConstant = txtMainTitle.ResourceConstant;
                            txtMainArticleAcademicVisitor.ResourceConstant = txtMainArticle.ResourceConstant;
                            imgMainArticleAcademicVisitor.FileName = imgMainArticle.FileName;
                            imgMainArticleAcademicVisitor.AlternateText = g.GetResource("TXT_SECTION_NAME_ALT" + sectionString, this);
                            SearchResultListVisitorsMen.LoadResultList();
                            SearchResultListVisitorsWomen.LoadResultList();
                        }
                        else
                        {
                            // Academic member
                            plcAcademicMember.Visible = true;
                            plcGeneric.Visible = false;
                            txtMemberForMembers.ResourceConstant = txtMembers.ResourceConstant;
                            lnkMoreMembersAcademic.NavigateUrl = lnkMoreMembers.NavigateUrl;
                            txtMoreMembersAcademic.ResourceConstant = "TXT_MORE_MEMBERS_ACADEMIC_MEMBER";
                            SearchResultListMembers.LoadResultList();
                            LoadProfileFilmStrip(MySector);
                        }
                    }

                    switch (MySector)
                    {
                        case Sector.Religious: _g.AnalyticsOmniture.PageName = "Religious Lobby"; break;
                        case Sector.Academic: _g.AnalyticsOmniture.PageName = "Academic Lobby"; break;
                        case Sector.Divorced: _g.AnalyticsOmniture.PageName = "Divorced Lobby"; break;
                        case Sector.Elderly: _g.AnalyticsOmniture.PageName = "Senior Lobby"; break;
                    }
                }
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }

        }

        private void LoadProfileFilmStrip(Sector sector)
        {
            try
            {
                MOCollection mocollection = null;
                HeroProfileFilmStripMatches.MemberList = GetHeroFilmStripProfiles(SectorsManager.GetSearchpreferences(sector, g), 17, out mocollection);
                if (HeroProfileFilmStripMatches.MemberList != null && HeroProfileFilmStripMatches.MemberList.Count > 0)
                {
                    HeroProfileFilmStripMatches.member = HeroProfileFilmStripMatches.MemberList[0].Member;
                    HeroProfileFilmStripMatches.Ordinal = HeroProfileFilmStripMatches.MemberList[0].Ordinal;
                    HeroProfileFilmStripMatches.MyEntryPoint = _heroProfileEntry;
                    HeroProfileFilmStripMatches.HotListCategoryID = (int)HeroProfileFilmStripMatches.MemberList[0].HotlistCategory;
                    HeroProfileFilmStripMatches.MyMOCollection = mocollection;
                }
                else
                {
                    HeroProfileFilmStripMatches.member = g.Member;
                    HeroProfileFilmStripMatches.Ordinal = 0;
                    HeroProfileFilmStripMatches.MyEntryPoint = BreadCrumbHelper.EntryPoint.HomeHeroProfileFilmStripYourProfile;
                    //HeroProfileFilmStripMatches.HotListCategoryID = (int)HeroProfileFilmStripMatches.MemberList[0].HotlistCategory;
                    HeroProfileFilmStripMatches.MyMOCollection = mocollection;

                }
            }
            catch (Exception ex)
            { g.ProcessException(ex); }
        }


        private List<ProfileHolder> GetHeroFilmStripProfiles(SearchPreferenceCollection prefs, int maxProfileDisplay, out MOCollection mocollection)
        {
            // declare local varialbes for resuse and clarity
            const string HASPHOTOFLAG = "HasPhotoFlag";
            int startRow = 0;
            int pageSize = maxProfileDisplay * 5;
            MatchnetQueryResults searchResults = null;

            mocollection = null;
            //get current photo preference before we modify it
            string photoPreference = prefs.Value(HASPHOTOFLAG);
            prefs.Add(HASPHOTOFLAG, "1");

            //Ensure that domainID is set on search preferences for the correct Community.
            prefs.Add(DOMAINID, g.Brand.Site.Community.CommunityID.ToString());
            prefs.Add("ColorCode", "");
            try
            {
                // Potential for exception due to invalid search preferences on this member's stored set of preferences.
                searchResults = MemberSearchSA.Instance.Search(prefs
                     , g.Brand.Site.Community.CommunityID
                     , g.Brand.Site.SiteID
                     , startRow
                     , pageSize
                     , g.Member == null ? Constants.NULL_INT : g.Member.MemberID
                     , Matchnet.Search.Interfaces.SearchEngineType.FAST
                     , Matchnet.Search.Interfaces.SearchType.ProfileFilmStripWebSearch
                     , Matchnet.Search.Interfaces.SearchEntryPoint.ProfileFilmStripMatches
                     , false
                     , false
                     , false
                     );
            }
            catch (Exception)
            {
                //invalid search preferences, initialize an empty results list so the home page will render correctly.
                searchResults = new MatchnetQueryResults(0);
            }

            //set photo preference back to old value
            prefs.Add(HASPHOTOFLAG, photoPreference);

            List<ProfileHolder> profileList = new List<ProfileHolder>();
            ArrayList results = null;
            ArrayList members = new ArrayList();
            ArrayList randomizedmembers = null;
            int resultsCount = searchResults.ToArrayList().Count;
            HotListCategory cat = HotListCategory.YourMatches;
            BreadCrumbHelper.EntryPoint entrypoint = BreadCrumbHelper.EntryPoint.HomeHeroProfileFilmStripMatches;

            if (searchResults == null || resultsCount <= 1)
            {
                mocollection = GetMembersOnlineCriteria(g.Member.GetAttributeInt(g.Brand, "RegionID"));
                results = GetMembersOnline(mocollection, pageSize);
                resultsCount = results.Count;
                if (results == null || results.Count <= 1)
                {
                    MOCollection mocollectiondefaultregion = GetMembersOnlineCriteria(g.Brand.Site.DefaultRegionID);
                    results = GetMembersOnline(mocollectiondefaultregion, pageSize);
                    resultsCount = results.Count;
                }
                entrypoint = BreadCrumbHelper.EntryPoint.HomeHeroProfileFilmStripMOL;
                _heroProfileEntry = BreadCrumbHelper.EntryPoint.HomeHeroProfileMOL;
            }
            else
            {
                results = searchResults.ToArrayList();
            }

            List<int> ordinals = null;
            if (resultsCount > 7)
            {
                if (Request.QueryString["random"] == "false")
                    randomizedmembers = results;
                else
                    randomizedmembers = RandomizeMemberIDs(results, 17, out ordinals);

                members = MemberSA.Instance.GetMembers(randomizedmembers, MemberLoadFlags.None);
            }
            else
            {
                members = MemberSA.Instance.GetMembers(results, MemberLoadFlags.None);
            }
            startRow = 0;
            foreach (Member.ServiceAdapters.Member member in members)
            {
                //add member
                ProfileHolder ph = new ProfileHolder();
                ph.Member = member;
                if (ordinals != null && ordinals.Count > startRow)
                    ph.Ordinal = ordinals[startRow];
                else
                    ph.Ordinal = startRow + 1;
                ph.HotlistCategory = HotListCategory.YourMatches;
                ph.MyEntryPoint = entrypoint;

                profileList.Add(ph);

                startRow++;

            }
            if (profileList.Count > 0 && _heroProfileEntry == BreadCrumbHelper.EntryPoint.HomeHeroProfileMatches)
            {
                _hasMatches = true;
            }

            return profileList;

        }

        private MOCollection GetMembersOnlineCriteria(int regionid)
        {
            try
            {

                // old - get gender mask from online members if or from member
                //int gendermask = g.Session.GetInt("MembersOnlineGender", g.Member.GetAttributeInt(g.Brand, "gendermask"));

                //fix MRP-2533 - get gendermask from search preferences
                int gendermask = Conversion.CInt(g.SearchPreferences["gendermask"]);

                int agemin = g.Session.GetInt("MembersOnlineAgeMin", g.Brand.DefaultAgeMin);
                int agemax = g.Session.GetInt("MembersOnlineAgeMax", g.Brand.DefaultAgeMax);
                int languagemask = g.Session.GetInt("MembersOnlineLanguage", 0);

                MOCollection mocollection = new MOCollection(SortFieldType.HasPhoto.ToString(), gendermask, regionid, agemin, agemax, languagemask);

                return mocollection;
            }
            catch (Exception ex)
            {
                g.ProcessException(ex); return null;
            }

        }

        private ArrayList GetMembersOnline(MOCollection molcollection, int maxProfileDisplay)
        {
            try
            {

                Matchnet.MembersOnline.ValueObjects.MOLQueryResult MOLResults = MembersOnlineSA.Instance.GetMemberIDs(g.Session.Key.ToString(),
                 g.Brand.Site.Community.CommunityID,
                 (short)molcollection.GenderMask,
                 molcollection.RegionID,
                 (Int16)molcollection.AgeMin,
                 (Int16)((Int16)molcollection.AgeMax + 1),	// See TT 15298.
                 molcollection.LanguageMask,
                 SortFieldType.HasPhoto,
                 SortDirectionType.Asc,
                 1,
                 maxProfileDisplay,
                 g.Member == null ? Constants.NULL_INT : g.Member.MemberID);

                return MOLResults.ToArrayList();
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
                return null;
            }


        }



        private void SetProfiles()
        {
            ArrayList miniProfiles = new ArrayList();
            ArrayList galleryMiniProfiles = new ArrayList();

            miniProfiles.Add(MemberSA.Instance.GetMember(int.Parse(g.Session["LOBBY_PAGE_MEMID_1"]), MemberLoadFlags.None));

            rptMiniProfiles.DataSource = miniProfiles;
            rptMiniProfiles.DataBind();

            galleryMiniProfiles.Add(MemberSA.Instance.GetMember(int.Parse(g.Session["LOBBY_PAGE_MEMID_2"]), MemberLoadFlags.None));
            galleryMiniProfiles.Add(MemberSA.Instance.GetMember(int.Parse(g.Session["LOBBY_PAGE_MEMID_3"]), MemberLoadFlags.None));

            rptGalleryMiniProfiles.DataSource = galleryMiniProfiles;
            rptGalleryMiniProfiles.DataBind();
        }

        private void SetRandomProfiles(Sector sector)
        {
            SearchPreferenceCollection preferences = SectorsManager.GetSearchpreferences(sector, g);
            int startRow = 0;
            int pageSize = 50;
            MatchnetQueryResults searchResults = null;

            try
            {
                searchResults = MemberSearchSA.Instance.Search(
                      preferences,
                      g.Brand.Site.Community.CommunityID,
                      g.Brand.Site.SiteID,
                      startRow,
                      pageSize);
            }
            catch (Exception)
            {
                //invalid search preferences, initialize an empty results list so the lobby page will render correctly.
                searchResults = new MatchnetQueryResults(0);
            }
            if (searchResults.Items.Count > 2)
            {
                ArrayList results = searchResults.ToArrayList();

                ArrayList randomizedmembers = RandomizeMemberIDs(results, 3);

                g.Session.Add("LOBBY_PAGE_MEMID_1", randomizedmembers[0].ToString(), SessionPropertyLifetime.Temporary);
                g.Session.Add("LOBBY_PAGE_MEMID_2", randomizedmembers[1].ToString(), SessionPropertyLifetime.Temporary);
                g.Session.Add("LOBBY_PAGE_MEMID_3", randomizedmembers[2].ToString(), SessionPropertyLifetime.Temporary);

                SetProfiles();
            }
        }

        public ArrayList RandomizeMemberIDs(ArrayList memberIDs, int resultSize, out List<int> ordinals)
        {
            ArrayList members = new ArrayList();
            int count = 0;
            ordinals = new List<int>();
            try
            {

                int size = memberIDs.Count;
                int iternum = size < resultSize ? size : resultSize;
                while (count < iternum)
                {
                    Guid uniqueID = Guid.NewGuid();
                    byte[] bArr = uniqueID.ToByteArray();
                    int autonum = BitConverter.ToInt32(bArr, 0);
                    int mynum = Math.Abs(autonum) % size;
                    if (!members.Contains(memberIDs[mynum]))
                    {
                        members.Add(memberIDs[mynum]);
                        ordinals.Add(mynum);
                        count += 1;
                    }
                }
                return members;
            }
            catch (Exception ex)
            { g.ProcessException(ex); return null; }



        }



        private ArrayList RandomizeMemberIDs(ArrayList memberIDs, int resultSize)
        {
            ArrayList members = new ArrayList();
            int count = 0;

            try
            {
                int size = memberIDs.Count;
                int iternum = size < resultSize ? size : resultSize;

                while (count < iternum)
                {
                    Guid uniqueID = Guid.NewGuid();
                    byte[] bArr = uniqueID.ToByteArray();
                    int autonum = BitConverter.ToInt32(bArr, 0);
                    int mynum = Math.Abs(autonum) % size;
                    if (!members.Contains(memberIDs[mynum]))
                    {
                        members.Add(memberIDs[mynum]);
                        count += 1;
                    }
                }
                return members;
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
                return null;
            }
        }


        protected string GetLobbyPageClass()
        {
            return "class=lobby-" + MySector.ToString().ToLower();
        }
    }
}
