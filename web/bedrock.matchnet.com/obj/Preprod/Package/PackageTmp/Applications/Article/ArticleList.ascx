<%@ Control Language="c#" AutoEventWireup="false" Codebehind="ArticleList.ascx.cs" Inherits="Matchnet.Web.Applications.Article.ArticleList" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<div id="rightNew">
	<asp:Label id="CategoryLabel" runat="server" />
	<br />
	<asp:Repeater id=LinkRepeater runat="server">
		<ItemTemplate>
			[link]
			<asp:HyperLink ID="MyLink" Runat="server" />
			<br />
		</ItemTemplate>
	</asp:Repeater>
</div> <!-- end Right Div area -->