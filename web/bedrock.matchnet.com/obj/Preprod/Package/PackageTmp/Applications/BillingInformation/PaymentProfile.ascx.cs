using System;
using Matchnet.Web.Framework;
using Matchnet.Purchase.ValueObjects;
using Matchnet.Web.Applications.Subscription.UnifiedPaymentSystem;

namespace Matchnet.Web.Applications.BillingInformation
{
	/// <summary>
	///		Summary description for PaymentProfile.
	/// </summary>
	public class PaymentProfile : FrameworkControl //: System.Web.UI.UserControl
	{
		private void Page_Load(object sender, System.EventArgs e)
		{
            
            try
            {
                PaymentUIConnector connector = new PaymentUIConnector();
                PaymentProfilePage page = new PaymentProfilePage();

                ((IPaymentUIJSON)page).PaymentType = PaymentType.CreditCard;
                ((IPaymentUIJSON)page).SaveLegacyData = false;

                connector.RedirectToPaymentUI(page);
                return;
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
                return;
            }
		}
        
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			//this.Button1.Click += new System.EventHandler(this.Button1_Click);
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
