using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Matchnet.List.ValueObjects;
using Matchnet.List.ServiceAdapters;

namespace Matchnet.Web.Framework
{
	/// <summary>
	/// Library class to assemble lists and help with List management
	/// </summary>
	public sealed class ListLib
	{
		private ListLib() {}

		/// <summary>
		/// Retrieves all categories for a particular user, both standard and custom.
		/// </summary>
		/// <param name="memberID"></param>
		/// <param name="domainID"></param>
		/// <param name="contextGlobal"></param>
		/// <param name="caller"></param>
		/// <returns>A data table of the ID and Description for each standard and custom category.</returns>
		public static DataTable GetCategories(int memberID, int domainID, ContextGlobal contextGlobal, object caller)
		{
			DataTable dtResult = new DataTable();
			dtResult.Columns.Add("HotListID", typeof(int));
			dtResult.Columns.Add("Description", typeof(string));
			
			dtResult.Rows.Add(new object[2]{Constants.NULL_INT, contextGlobal.GetResource("YOUR_HOT_LIST", caller) + contextGlobal.GetResource("YOUR_FAVORITES", caller)});

			CustomCategoryCollection categories = ListSA.Instance.GetCustomListCategories(memberID, domainID);
			foreach (CustomCategory thisCategory in categories)
			{
				dtResult.Rows.Add(new object[2]{thisCategory.CategoryID, contextGlobal.GetResource("YOUR_HOT_LIST", caller) + thisCategory.Description});
			}

			StandardCategoryCollection standardCategories = ListSA.Instance.GetStandardListCategories(contextGlobal.Brand.Site.SiteID);
			foreach (StandardCategory standardCategory in standardCategories)
			{
				dtResult.Rows.Add(new object[2]{standardCategory.ListCategoryID, contextGlobal.GetResource(standardCategory.ResourceKey)});	
			}

			return dtResult;
		}	

		/// <summary>
		/// Returns a standard category name based on the resourceid of the provieded category ID.
		/// </summary>
		/// <param name="category"></param>
		/// <param name="contextGlobal"></param>
		/// <returns></returns>
		public static string GetStandardCategoryName(HotListCategory category, ContextGlobal contextGlobal)
		{
			if ((int)category == 0)
			{
				return contextGlobal.GetResource("YOUR_FAVORITES");
			}
			else
			{
				StandardCategoryCollection standardCategories = ListSA.Instance.GetStandardListCategories(contextGlobal.Brand.Site.SiteID);

				foreach(StandardCategory standardCategory in standardCategories)
				{
					if(standardCategory.ListCategoryID == (int)category)
					{
						return contextGlobal.GetResource(standardCategory.ResourceKey);
					}
				}
			}

			return string.Empty;
		}

        public static string GetStandardCategoryName(HotListCategory category, ContextGlobal contextGlobal, FrameworkControl resourceControl)
        {
            if ((int)category == 0)
            {
                return contextGlobal.GetResource("YOUR_FAVORITES", resourceControl);
            }
            else
            {
                StandardCategoryCollection standardCategories = ListSA.Instance.GetStandardListCategories(contextGlobal.Brand.Site.SiteID);

                foreach (StandardCategory standardCategory in standardCategories)
                {
                    if (standardCategory.ListCategoryID == (int)category)
                    {
                        return contextGlobal.GetResource(standardCategory.ResourceKey, null,true,resourceControl);
                    }
                }
            }

            return string.Empty;
        }


       

        public static void GetHotListTypeAndDirection(HotListCategory category, out HotListType hotlistType, out HotListDirection listDirection)
        {

            ListInternal.MapListTypeDirection(category, out hotlistType, out listDirection);

        }


        public static StandardCategoryCollection GetStandardCategories(int siteid, List<int> categoriesList)
        {
            if (categoriesList == null || categoriesList.Count == 0)
                return null;
            List<StandardCategory> list = new List<StandardCategory>();

            StandardCategoryCollection standardCategories = ListSA.Instance.GetStandardListCategories(siteid);
            StandardCategoryCollection collection =new StandardCategoryCollection(siteid);
            //needed to do it to keep categoriesList so we would need to loop thru categoriesList
            foreach (StandardCategory standardCategory in standardCategories)
            {
                list.Add(standardCategory);
            }

            foreach (int category in categoriesList)
            {                
                StandardCategory c=list.Find(delegate(StandardCategory cat){return cat.ListCategoryID==category;});
                if (c != null)
                    collection.Add(c);
                else
                {
                    if (category == (int)HotListCategory.Default)
                    {
                        collection.Add(new StandardCategory((int)HotListCategory.Default, "LISTCATEGORY_DEFAULT"));
                    }
                    else if(category==(int)HotListCategory.MutualYes)
                        collection.Add(new StandardCategory((int)HotListCategory.MutualYes, "LISTCATEGORY_YOU_EACH_SAID_YES"));
                }
            }
            //somehow default category is not in the database, I guess because it's default..
           
            return collection;
                     
        }

        public static CustomCategoryCollection GetCustomCategories(int memberid, int communityid,   List<int> categoriesList)
        {
            if (categoriesList == null || categoriesList.Count == 0)
                return null;

            List<CustomCategory> list = new List<CustomCategory>();
            CustomCategoryCollection collection = new CustomCategoryCollection(memberid, communityid);
            CustomCategoryCollection categories = ListSA.Instance.GetCustomListCategories(memberid, communityid);
            foreach (CustomCategory thisCategory in categories)
            {
                list.Add(thisCategory);
            }

            foreach (int category in categoriesList)
            {
                CustomCategory c = list.Find(delegate(CustomCategory cat) { return cat.CategoryID == category; });
                if (c != null)
                    collection.Add(c);
            }

            return collection;
        }

        

	}
}
