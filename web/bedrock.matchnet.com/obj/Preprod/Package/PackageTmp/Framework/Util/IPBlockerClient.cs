﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Xml;
using System.Runtime.Serialization.Json;
using System.Text.RegularExpressions;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Member.ValueObjects;
using Spark.Common;
using Spark.Common.Adapter;

namespace Matchnet.Web.Framework.Util
{

    
    public class IPBlockerClient
    {
       

        public IPBlockerClient()
        {

        }

        public bool CheckAccess(string ipAddress, IPBlockerAccessType accessType, ContextGlobal g)
        {
            bool allowed = IPBlockerServiceAdapter.Instance.CheckIPAccess(ipAddress, g.Brand.Site.SiteID, accessType);
            return allowed;
        }

        public void CheckAccessAndRedirectIfBlocked(string ipAddress, IPBlockerAccessType accessType, ContextGlobal g)
        {
            bool canAccess = CheckAccess(ipAddress, accessType, g);
            if (!canAccess)
            {
                FrameworkGlobals.RedirectToErrorPageForIPBlock();
            }
        }
    }
}
