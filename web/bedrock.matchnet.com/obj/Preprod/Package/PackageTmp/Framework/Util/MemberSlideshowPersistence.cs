﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.Web.Interfaces;
using Spark.SAL;

namespace Matchnet.Web.Framework.Util
{
    public class MemberSlideshowPersistence: ISlideshowPersistence
    {
        private int _memberID;
        private string _cookieName = "MS_{0}";
        private HttpCookie _persistCookie;
        
        public int MinAge { get; set; }
        public int MaxAge { get; set; }
        public int RegionID { get; set; }
        public SearchType SearchType { get; set; }
        public int AreaCode1 { get; set; }
        public int AreaCode2 { get; set; }
        public int AreaCode3 { get; set; }
        public int AreaCode4 { get; set; }
        public int AreaCode5 { get; set; }
        public int AreaCode6 { get; set; }

        public int MemberID
        {
            get
            {
                return _memberID;
            }
        }

        public MemberSlideshowPersistence(int memberID)
        {
            _memberID = memberID;
            string _name = string.Format(_cookieName, memberID.ToString());

            _persistCookie = HttpContext.Current.Request.Cookies[_name];

            if (_persistCookie == null)
            {
                _persistCookie = new HttpCookie(_name, _name);
                InitializeValues();
            }
            else
            {
                LoadValues();
            }
        }

        public void PersistValues()
        {
            _persistCookie["MinAge"] = MinAge.ToString();
            _persistCookie["MaxAge"] = MaxAge.ToString();
            _persistCookie["RegionID"] = RegionID.ToString();
            _persistCookie["SearchType"] = SearchType.ToString("d");
            _persistCookie["AreaCode1"] = AreaCode1.ToString();
            _persistCookie["AreaCode2"] = AreaCode2.ToString();
            _persistCookie["AreaCode3"] = AreaCode3.ToString();
            _persistCookie["AreaCode4"] = AreaCode4.ToString();
            _persistCookie["AreaCode5"] = AreaCode5.ToString();
            _persistCookie["AreaCode6"] = AreaCode6.ToString();
            HttpContext.Current.Response.Cookies.Add(_persistCookie);
        }

        public string GetPersistedValue(string key)
        {
            return _persistCookie[key].ToString();
        }

        public void Clear()
        {
            if (_persistCookie != null)
                _persistCookie.Expires = DateTime.Now.AddYears(-30);
            HttpContext.Current.Response.Cookies.Add(_persistCookie);
        }

        private void InitializeValues()
        {
            MinAge = Constants.NULL_INT;
            MaxAge = Constants.NULL_INT;
            RegionID = Constants.NULL_INT;
            SearchType = SearchType.PostalCode;
            AreaCode1 = Constants.NULL_INT;
            AreaCode2 = Constants.NULL_INT;
            AreaCode3 = Constants.NULL_INT;
            AreaCode4 = Constants.NULL_INT;
            AreaCode5 = Constants.NULL_INT;
            AreaCode6 = Constants.NULL_INT;
        }

        private void LoadValues()
        {
            MinAge = Convert.ToInt32(_persistCookie["MinAge"]);
            MaxAge = Convert.ToInt32(_persistCookie["MaxAge"]);
            
            RegionID = _persistCookie["RegionID"] != null ? 
                Convert.ToInt32(_persistCookie["RegionID"]) : Constants.NULL_INT;

            SearchType = SearchType.Region;
            if (_persistCookie["SearchType"] != null)
            {
                int searchTypeInt;
                if(int.TryParse(_persistCookie["SearchType"], out searchTypeInt))
                {
                    SearchType = (SearchType)Convert.ToInt32(_persistCookie["SearchType"]);
                }
            }

            AreaCode1 = _persistCookie["AreaCode1"] != null ?
                Convert.ToInt32(_persistCookie["AreaCode1"]) : Constants.NULL_INT;
            AreaCode2 = _persistCookie["AreaCode2"] != null ?
                Convert.ToInt32(_persistCookie["AreaCode2"]) : Constants.NULL_INT;
            AreaCode3 = _persistCookie["AreaCode3"] != null ?
                Convert.ToInt32(_persistCookie["AreaCode3"]) : Constants.NULL_INT;
            AreaCode4 = _persistCookie["AreaCode4"] != null ?
                Convert.ToInt32(_persistCookie["AreaCode4"]) : Constants.NULL_INT;
            AreaCode5 = _persistCookie["AreaCode5"] != null ?
                Convert.ToInt32(_persistCookie["AreaCode5"]) : Constants.NULL_INT;
            AreaCode6 = _persistCookie["AreaCode6"] != null ?
                Convert.ToInt32(_persistCookie["AreaCode6"]) : Constants.NULL_INT;
        }
       
    }
}


