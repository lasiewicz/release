﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.Web.Framework.Managers;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Member.ServiceAdapters.Interfaces;

namespace Matchnet.Web.Framework.Util
{
    public enum ThrottleMode
    {
        OddEven=1,
        LastDigit=2,
        StringCompare=3
    }

    public enum ControlVersion
    {
        Current = 0,
        Version20,
        Version30,
        Version40a,
        Version40b,
        Version50,
        Version50b
    }

    public class FeatureThrottler
    {
        private ThrottleMode _mode;
        private string _filterSettingConstant;
        private SettingsManager _settingsManager;
        private Brand _brand;
        private IMember _member;
        private string _stringCompareItem;

        public string FilterSetting
        {
            get { return _filterSettingConstant; }
            set { _filterSettingConstant = value; }
        }

        public string StringCompareItem
        {
            get { return _stringCompareItem; }
            set { _stringCompareItem = value; }
        }

        private SettingsManager SettingsManager
        {
            get
            {
                if (_settingsManager == null)
                {
                    _settingsManager = new SettingsManager();
                }
                return _settingsManager;
            }
        }

        public FeatureThrottler(ThrottleMode mode, string filterSetting, Brand brand, IMember member)
        {
            _mode = mode;
            _filterSettingConstant = filterSetting;
            _brand = brand;
            _member = member;
        }

        public FeatureThrottler(ThrottleMode mode, string filterSetting, Brand brand, string stringCompareItem)
        {
            _mode = mode;
            _filterSettingConstant = filterSetting;
            _brand = brand;
            _stringCompareItem = stringCompareItem;
        }

        /// <summary>
        /// Checks if feature is enabled based on current filter setting and member
        /// </summary>
        /// <returns></returns>
        public bool IsFeatureEnabled()
        {            
            return IsFeatureEnabled(null, null);
        }

        /// <summary>
        /// Checks if feature is enabled using new filter setting and existing member
        /// </summary>
        /// <param name="filterSetting">new setting to use</param>
        /// <returns></returns>
        public bool IsFeatureEnabled(string filterSetting)
        {
            return IsFeatureEnabled(filterSetting, null);
        }

        /// <summary>
        /// Checks if feature is 
        /// </summary>
        /// <param name="filterSetting">optional new setting to use</param>
        /// <param name="compareItem">optional string to compare, only applicable for ThrottleMode.StringCompare</param>
        /// <returns></returns>
        public bool IsFeatureEnabled(string filterSetting, string stringCompareItem)
        {
            bool enabled = false;
            if (!string.IsNullOrEmpty(filterSetting))
            {
                _filterSettingConstant = filterSetting; 
            }

            if (!string.IsNullOrEmpty(stringCompareItem))
            {
                _stringCompareItem = stringCompareItem;
            }
            
            int mod = 0;
            switch(_mode)
            {
                case ThrottleMode.OddEven:
                    //Throttles based on an even/odd memberID split
                    //Setting should be one of these values:
                    // -1 - No member
                    // 0 - Even member id
                    // 1 - Odd member id
                    // 2 - All members
                    int threshold = SettingsManager.GetSettingInt(_filterSettingConstant, _brand);
                    mod = 2;
                    enabled = (mod == threshold) || (_member != null && _member.MemberID > 0 && _member.MemberID % mod == threshold);
                    break;
                case ThrottleMode.LastDigit:
                    //Throttles based on matching last digit of memberID
                    //Setting should be a comma-delimited list of digits
                    string lastDigitList = SettingsManager.GetSettingString(_filterSettingConstant, _brand);
                    if (!string.IsNullOrEmpty(lastDigitList))
                    {
                        string[] lastDigitsArray = lastDigitList.Split(new char[] { ',' });
                        mod = 10;
                        string lastDigit = (_member.MemberID % mod).ToString();
                        
                        if (lastDigitsArray.Length > 0)
                        {
                            if (_member != null)
                            {
                                foreach (var digit in lastDigitsArray)
                                {
                                    if (lastDigit == digit)
                                    {
                                        enabled = true;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    break;
                case ThrottleMode.StringCompare:
                    //Throttles based on matching string
                    //Setting should be a comma-delimited list of strings
                    string stringCompareList = SettingsManager.GetSettingString(_filterSettingConstant, _brand);
                    if (!string.IsNullOrEmpty(stringCompareList))
                    {
                        string[] stringCompareArray = stringCompareList.Split(new char[] { ',' });

                        if (stringCompareArray.Length > 0)
                        {
                            foreach (var stringCompare in stringCompareArray)
                            {
                                if (_stringCompareItem == stringCompare)
                                {
                                    enabled = true;
                                    break;
                                }
                            }
                        }
                    }
                    break;
            }

            return enabled;
        }

    }
}