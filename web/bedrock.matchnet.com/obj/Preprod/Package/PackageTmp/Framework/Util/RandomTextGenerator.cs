using System;

namespace Matchnet.Web.Framework.Util
{
	/// <summary>
	/// Static class that can generated randomn text.
	/// </summary>
	public abstract class RandomTextGenerator
	{
		private static char[] characters = "ABCDEFGHJKLMNPQRSTUVWXYZ23456789".ToCharArray();
		private static Random random = new Random();
		
		public static string Generate()
		{
			string generatedString = Generate(6);
			
			return generatedString;
		}
		
		/// <summary>
		/// generates a random string given a lenght
		/// </summary>
		/// <param name="lenght">how many characters should the output be</param>
		/// <returns>a randomly generated string</returns>
		public static string Generate(int lenght)
		{
			string generatedString = "";
			
			for (int i = 0;i<lenght; i++)
			{
					generatedString += characters[random.Next(0, characters.Length - 1)];
			}
			
			return generatedString;
		}
		
		/// <summary>
		/// generates a random string with a random leght given leght boundries
		/// </summary>
		/// <param name="minLenght">the mimimum number of chars that the text should be</param>
		/// <param name="maxLenght">the maximum number of chars that the text should be</param>
		/// <returns>a randomly generated text</returns>
		public static string Generate(int minLenght, int maxLenght)
		{
			return Generate(random.Next(minLenght, maxLenght));
		}
		
		
	}
}
