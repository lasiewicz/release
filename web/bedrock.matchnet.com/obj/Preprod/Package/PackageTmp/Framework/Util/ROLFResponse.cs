﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Matchnet.Web.Framework.Util
{
    public class ROLFResponse
    {
        public bool Passed { get; set; }
        public bool Error { get; set; }
        public string ErrorDescription { get; set; }
        public int Code { get; set; }
        public string Description { get; set; }
    }
}
