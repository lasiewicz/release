﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Web;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Web.Framework.Managers;
using Matchnet.Web.Interfaces;

namespace Matchnet.Web.Framework.TemplateControls
{
    public class RegistrationPersistence : ITemporaryPersistence
    {
        #region ITemporaryPersistence Members

        private HttpCookie _persistCookie;
        private SettingsManager _settingsManager = null;
        public const string COOKIE_NAME = "REG091202";

        public SettingsManager SettingsManager
        {
            get { return _settingsManager ?? new SettingsManager(); }
        }

        public RegistrationPersistence()
        {
            EnsureCookieExists();
        }

        public string Name
        {
            get { return COOKIE_NAME; }
        }

        public string this[string index]
        {
            get
            {
                return _persistCookie[index];
            }
            set
            {
                _persistCookie[index] = value;
            }
        }

        public DateTime Expires
        {
            get
            {
                return _persistCookie.Expires;
            }
            set { _persistCookie.Expires = value; }
        }

        public void Persist(Brand brand)
        {
            int expirationDays = SettingsManager.GetSettingInt(SettingConstants.REGISTRATION_COOKIE_EXPIRE, brand);
            _persistCookie.Expires = DateTime.Now.AddDays(expirationDays);
            HttpContext.Current.Response.Cookies.Add(_persistCookie);
        }

        public void Clear()
        {
            if (_persistCookie != null)
                _persistCookie.Expires = DateTime.Now.AddYears(-30);
            HttpContext.Current.Response.Cookies.Add(_persistCookie);
        }

        public bool HasValues
        {
            get
            {
                return _persistCookie.Values.Count > 0;    
            }
            
        }

        public NameValueCollection Values
        {
            get { return _persistCookie.Values; }
        }

        private void EnsureCookieExists()
        {
            if (_persistCookie == null)
                _persistCookie = HttpContext.Current.Request.Cookies[COOKIE_NAME];

            if (_persistCookie == null)
            {
                _persistCookie = new HttpCookie(COOKIE_NAME, COOKIE_NAME);
            }
        }
        #endregion

        public override string ToString()
        {
            StringBuilder cookieValues = new StringBuilder();
            foreach(string key in _persistCookie.Values.AllKeys)
            {
                cookieValues.AppendLine(key + ": " + _persistCookie[key]);
            }

            return cookieValues.ToString();
        }
    }
}