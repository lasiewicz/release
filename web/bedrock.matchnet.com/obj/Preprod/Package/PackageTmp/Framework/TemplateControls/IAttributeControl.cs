﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Collections.Specialized;
using Matchnet.Web.Applications.Registration;
using Matchnet.Web.Interfaces;

namespace Matchnet.Web.Framework.TemplateControls
{
    public interface IAttributeControl
    {
        void Save();
        void SetValue(string value);
        string GetValue();
        void PersistToCookie(HttpCookie cookie);
        void Persist(ITemporaryPersistence persistence);
        bool Validate();
        string ScriptBlock();
        FrameworkControl ResourceControl { get; set; }
        void SetVisible(bool visible);
        Applications.Registration.Controls.ValidationMessage ValidationMessage { get; }
        string FocusControlClientID { get; }
        string ContainerDivClientID { get; }
        bool IsOverlayReg { get; set; }
        NameValueCollection GetSearchPreferenceParamaterNVC();
        AttributeControl AttrControl { get; set; }
    }


    public interface IJQValidation
    {
        string InputFieldID { get; }
        List<ValidationDictionaryEntry> GetValidationRules { get; }
    }
}
