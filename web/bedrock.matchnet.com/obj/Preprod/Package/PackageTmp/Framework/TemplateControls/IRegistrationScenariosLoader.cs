﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Web.Framework.Enumerations;

namespace Matchnet.Web.Framework.TemplateControls
{
    public interface IRegistrationScenariosLoader
    {
        Scenarios LoadScenarios(Brand brand, RegistrationFlowType flowType, RegistrationScenarioType scenarioType);
    }
}
