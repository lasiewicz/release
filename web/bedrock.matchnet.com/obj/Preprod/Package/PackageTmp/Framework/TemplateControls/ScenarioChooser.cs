﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Web.Framework.Enumerations;
using Matchnet.Web.Framework.Managers;
using Matchnet.Web.Framework.HTTPContextWrappers;
using Matchnet.Web.Interfaces;

namespace Matchnet.Web.Framework.TemplateControls
{
    public class ScenarioChooser
    {
        private IRegistrationScenariosLoader _scenariosLoader;
        private const string CACHE_KEY_BASE = "REG_SCENARIO_CACHE";
        private ICurrentRequest _currentRequest = null;
        private SettingsManager _settingsManager = null;
        private ICaching _caching = null;
        private Brand _brand = null;

        public ICurrentRequest CurrentRequest
        {
            get { return _currentRequest ?? new CurrentRequest(); }
            set { _currentRequest = value; }
        }

        public SettingsManager SettingsManager
        {
            get { return _settingsManager ?? new SettingsManager(); }
        }

        public ICaching Cache
        {
            get { return _caching ?? Managers.CacheManager.Instance.GetCacheInstance(_brand); }
            set { _caching = value;}
        }

        public ScenarioChooser()
        {
            _scenariosLoader = new XMLRegistrationScenariosLoader();
        }

        public Scenario GetRandomScenario(Brand brand, RegistrationFlowType flowType, bool overrideCache, List<string> excludeList)
        {
            _brand = brand;
            string cacheKey = GetCacheKey(brand, flowType, false);

            Scenarios scenarios = GetScenarios(brand, flowType, overrideCache,cacheKey);

            foreach (var scenarioName in excludeList)
            {
                var namedScenario = scenarios.GetScenario(scenarioName);
                if (namedScenario != null)
                {
                    scenarios.ScenarioList.Remove(namedScenario);
                }
            }

            if (scenarios != null && scenarios.ScenarioList.Count > 0)
            {
                int count = scenarios.ScenarioList.Count;
                Random random = new System.Random();
                int randomInt = random.Next(0, count);
                return scenarios.ScenarioList[randomInt];
            }

            return null;
        }

        public Scenario GetNamedScenario(Brand brand, RegistrationFlowType flowType, string scenarioName, bool overrideCache, List<string> excludeList)
        {
            _brand = brand;
            string cacheKey = GetCacheKey(brand, flowType, false);

            Scenarios scenarios = GetScenarios(brand, flowType, overrideCache,cacheKey);
            Scenario scen = null;
            if (scenarios != null && scenarios.ScenarioList.Count > 0)
            {
                scen = scenarios.GetScenario(scenarioName);
            }
            return scen;
        }

        private Scenarios GetScenarios(Brand brand, RegistrationFlowType flowType, bool overrideCache, string cacheKey)
        {
            Scenarios scenarios = GetStaticScenario(brand, flowType);
            if (scenarios == null)
            {
                scenarios = (Scenarios)Cache.Get(cacheKey);
            }

            if (scenarios == null || overrideCache)
            {
                scenarios = _scenariosLoader.LoadScenarios(brand, flowType, RegistrationScenarioType.Dynamic);
                Cache.Insert(cacheKey, scenarios);
            }
            return scenarios;
        }

        private string GetCacheKey(Brand brand, RegistrationFlowType flowType, bool isStatic)
        {
            string cacheKey = CACHE_KEY_BASE + "~" + brand.BrandID.ToString() + "~"+flowType.ToString();
            if (isStatic) cacheKey +="Static";
            return cacheKey;
        }

        private Scenarios GetStaticScenario(Brand brand, RegistrationFlowType flowType)
        {
            Scenarios scenarios = null;  // The returned value, should always contain 1 scenario only.
            string cacheKey = GetCacheKey(brand, flowType, true);
            
            Scenarios staticScenarios = (Scenarios)Cache.Get(cacheKey);

            if (staticScenarios == null)
            {
                staticScenarios = _scenariosLoader.LoadScenarios(brand, flowType, RegistrationScenarioType.Static);
                
                if(staticScenarios != null)
                {
                    Cache.Insert(cacheKey, staticScenarios);
                }
            }

            if (staticScenarios != null)
            {
                foreach (Scenario staticScenario in staticScenarios.ScenarioList)
                {
                    if (CheckStaticScenarioConditions(staticScenario.StaticConditions, brand))
                    {
                        scenarios = new Scenarios();
                        scenarios.Condition = staticScenarios.Condition;
                        scenarios.ScenarioList = new List<Scenario>(1) { staticScenario };
                        break;
                    }
                }
            }

            return scenarios;
        }


        private bool CheckStaticScenarioConditions(List<StaticCondition> staticConditions, Brand brand)
        {
            bool isMatch = true;
            foreach (StaticCondition condition in staticConditions)
            {
                switch (condition.ConditionType)
                {
                    case ConditionTypeEnum.BrandID:
                        if (brand.BrandID.ToString() != condition.ConditionValue)
                        {
                            isMatch = false;
                        }
                        break;
                    case ConditionTypeEnum.URLParam:
                        {
                            string key = condition.ConditionValue.Split('=')[0];
                            string value = condition.ConditionValue.Split('=')[1];
                            if (!string.IsNullOrEmpty(CurrentRequest.QueryString[key]))
                            {
                                if (CurrentRequest.QueryString[key] != value)
                                {
                                    isMatch = false;
                                }
                            }
                            else
                            {
                                isMatch = false;
                            }
                        }
                        break;
                    case ConditionTypeEnum.FullURLContains:
                        string fullURL = CurrentRequest.Url.AbsoluteUri;
                        if (!fullURL.Contains(condition.ConditionValue))
                        {
                            isMatch = false;
                        }
                        break;
                    case ConditionTypeEnum.DBForcedScenarioName:
                        string forcedScenarioName = SettingsManager.GetSettingString(SettingConstants.FORCE_SPLASH_REG_SCENARIO_NAME, brand);
                        if (!string.IsNullOrEmpty(forcedScenarioName))
                        {
                            if (forcedScenarioName != condition.ConditionValue)
                            {
                                isMatch = false;
                            }
                        }
                        else
                        {
                            isMatch = false;
                        }
                        break;
                }

                if (!isMatch) break;
            }

            return isMatch;
        }
    }
}