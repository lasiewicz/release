﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Web.Framework.Enumerations;
using Matchnet.Web.Framework.HTTPContextWrappers;
using Matchnet.Web.Framework.Util;
using Matchnet.Web.Interfaces;

namespace Matchnet.Web.Framework.TemplateControls
{
    public class XMLRegistrationScenariosLoader : IRegistrationScenariosLoader
    {
        private const string FULL_TEMPLATE_PATH = "/Applications/Registration/XML/Registration.xml";
        private const string SPLASH_TEMPLATE_PATH = "/Applications/Registration/XML/SplashRegistration.xml";
        private const string OVERLAY_TEMPLATE_PATH = "/Applications/Registration/XML/RegistrationOverlay.xml";
        private ICurrentServer _currentServer = null;

        public ICurrentServer CurrentServer
        {
            get
            {
                if (_currentServer == null)
                {
                    _currentServer = new CurrentServer();
                }
                return _currentServer;
            }
            set { _currentServer = value; }
        }
        
        #region IRegistrationScenariosLoader Members

        public Scenarios LoadScenarios(Brand brand, RegistrationFlowType flowType, RegistrationScenarioType scenarioType)
        {
            Scenarios scenarios = null;
            XMLObjectSerializer objectSerializer = new XMLObjectSerializer();

            string filePath = string.Empty;
            
            switch(flowType)
            {
                case RegistrationFlowType.FullRegistration:
                    filePath = BrandScenarioFile(FULL_TEMPLATE_PATH, brand);
                    break;
                case RegistrationFlowType.Splash:
                    filePath = BrandScenarioFile(SPLASH_TEMPLATE_PATH, brand);
                    break;
                case RegistrationFlowType.Overlay:
                    filePath = BrandScenarioFile(OVERLAY_TEMPLATE_PATH, brand);
                    break;
            }

            if(scenarioType == RegistrationScenarioType.Static)
            {
                filePath = filePath.Replace("Registration_", "RegistrationStatic_").Replace("RegistrationOverlay_", "RegistrationOverlayStatic_");
            }

            string xmlFile = CurrentServer.MapPath(filePath);

            FileInfo fi = new FileInfo((xmlFile));
            if (fi.Exists)
            {
                scenarios = objectSerializer.FromXmlFile<Scenarios>(xmlFile);
            }

            return scenarios;
        }

        #endregion

        private string BrandScenarioFile(string filename, Brand brand)
        {
            string brandedFileName = filename;
            try
            {
                string fileBrandExt = "_" + brand.Site.SiteID.ToString();
                int ext = filename.IndexOf(".");
                if (ext < 0)
                { brandedFileName += fileBrandExt; }
                else
                {
                    string[] fileapart = filename.Split('.');
                    brandedFileName = fileapart[0] + fileBrandExt + "." + fileapart[1];
                }

                return brandedFileName;
            }
            catch (Exception ex)
            {
                return brandedFileName;
            }
        }
    }
}