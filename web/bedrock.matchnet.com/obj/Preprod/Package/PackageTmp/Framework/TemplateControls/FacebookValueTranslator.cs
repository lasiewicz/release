﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Matchnet.Web.Framework.TemplateControls
{
    public class FacebookValueTranslator
    {
        public string TranslateFirstName(string name)
        {
            string firstName = name;
            int spaceIndex = name.IndexOf(' ');
            if(spaceIndex > 0)
            {
                firstName = name.Substring(0, spaceIndex);
            }
            return firstName;
        }

        public DateTime TranslateBirthdate(string birthdate)
        {
            DateTime translatedBirthdate = DateTime.MinValue;
            DateTime.TryParse(birthdate, out translatedBirthdate);
            return translatedBirthdate;
        }
    }
}