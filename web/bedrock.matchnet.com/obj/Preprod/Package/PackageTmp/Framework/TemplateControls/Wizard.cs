﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Session.ValueObjects;
using Matchnet.Web;
using Matchnet.Web.Framework;
using System.Xml.Serialization;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Web.Framework.Managers;
using Matchnet.Web.Interfaces;

namespace Matchnet.Web.Framework.TemplateControls
{
    public class Wizard
    {
        private string _scenarioCacheKey;
        private string _scenarioName;
        private int _stepNum = 1;
        private int _completedSteps;
        private List<int> _conditionalSkipSteps;
        private String _sessionID;
        private int _startStep;
        private string _registrationGUID;
        private ITemporaryPersistence _temporaryPersistence = null;
        protected Brand _brand;
        private SessionManager _sessionManager;

        public string Name { get { return Persistence.Name; } }
        public string ScenarioName { get { return _scenarioName; } set { _scenarioName = value; } }
        public string CacheKey { get { return _scenarioCacheKey; } set { _scenarioCacheKey = value; } }
        public string RegistrationgUID { get { return _registrationGUID; } set { _registrationGUID = value; } }
        public List<int> ConditionalSkipSteps { get { return _conditionalSkipSteps; } }
        public int CompletedSteps { get { return _completedSteps; } set { _completedSteps = value; } }
        public int StepID { get { return _stepNum; } set { _stepNum = value; } }

        public ITemporaryPersistence Persistence
        {
            get
            {
                if (_temporaryPersistence == null)
                {
                    _temporaryPersistence = new RegistrationPersistence();
                }
                return _temporaryPersistence;
            }
            set { _temporaryPersistence = value; }
        }

        public String SessionID
        {
            get
            {
                if (String.IsNullOrEmpty(_sessionID))
                {
                    UserSession session = _sessionManager.GetCurrentSession(_brand);
                    _sessionID = session.Key.ToString();
                }
                return _sessionID;
            }
        }
        
        public int StartStepID
        {
            get
            {
                if (_startStep <= 0)
                { return 1; }
                else
                { return _startStep; }
            }
            set { _startStep = value; }
        }

        public Wizard(Brand brand)
        {
            _sessionManager = new SessionManager();
            _brand = brand;
        }

        public bool SkipStep(int stepid)
        {
            bool ret = false;
            try
            {
                if (_conditionalSkipSteps == null || _conditionalSkipSteps.Count == 0)
                    return false;

                IEnumerable<int> query = from l in _conditionalSkipSteps
                                         where l == stepid
                                         select l;

                if (query.ToList<int>().Count > 0)
                {
                    ret = true;
                }
                return ret;
            }
            catch (Exception ex)
            { return ret; }
        }
        
        public virtual void Persist()
        {
            Persist(false, string.Empty);
        }

        public virtual void Persist(bool excludeScenarioName, string originalScenarioName)
        {
            Persistence["LAST_COMPLETED_STEP"] = _completedSteps.ToString();
            Persistence["CURRENT_STEP"] = _stepNum.ToString();
            Persistence["SESSION_ID"] = SessionID;
            Persistence["START_STEP_ID"] = StartStepID.ToString();

            if (!excludeScenarioName)
            {
                Persistence["ScenarioName"] = _scenarioName;
            }
            else
            {
                Persistence["ScenarioName"] = originalScenarioName;
            }
            
            if (_conditionalSkipSteps != null && _conditionalSkipSteps.Count > 0)
            {
                string skip = "";
                for (int i = 0; i < _conditionalSkipSteps.Count; i++)
                {
                    skip += _conditionalSkipSteps[i].ToString();
                    if (i < _conditionalSkipSteps.Count - 1)
                        skip += ",";
                }
                Persistence["SKIP_STEPS"] = skip;
            }

            Persistence.Persist(_brand);
        }

        public virtual void ClearPersistence()
        {
            Persistence.Clear();
        }
        
        public virtual void Load(bool loadNewWizard)
        {
            if (!loadNewWizard)
            {
                if(!Persistence.HasValues)
                {
                    _stepNum = 1;
                    return;
                }

                UserSession session = _sessionManager.GetCurrentSession(_brand);

                _completedSteps = Conversion.CInt(Persistence["LAST_COMPLETED_STEP"]) == Constants.NULL_INT ? 0 : Conversion.CInt(Persistence["LAST_COMPLETED_STEP"]);
                _stepNum = Conversion.CInt(Persistence["CURRENT_STEP"]);
                _sessionID = Persistence["SESSION_ID"];
                _startStep = Conversion.CInt(Persistence["START_STEP_ID"]);
                //if user started and abandoned registration and session expired
                //they will start from step 1
                //if session is still alive - from next not completed step  as it written in the cookies
                if (_completedSteps <= 0 || SessionID != session.Key.ToString())
                {
                    _stepNum = 1;
                    _sessionID = session.Key.ToString();
                    _startStep = 0;

                }
                _scenarioName = Persistence["ScenarioName"];
                _registrationGUID = Persistence["RegistrationGUID"];
                string skipSteps = Persistence["SKIP_STEPS"];
                _conditionalSkipSteps = new List<int>();
                if (!String.IsNullOrEmpty(skipSteps))
                {
                    string[] skip = skipSteps.Split(',');
                    for (int i = 0; i < skip.Length; i++)
                    {
                        _conditionalSkipSteps.Add(Conversion.CInt(skip[i]));
                    }

                }
            }
            else
            {
                _completedSteps = 0;
                _stepNum = 1;
                _sessionID = "";
                {
                    _scenarioName = Persistence["ScenarioName"];
                    _registrationGUID = Persistence["RegistrationGUID"];
                }
            }
        }
    }
}
