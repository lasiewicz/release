﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Matchnet.Web.Framework.ConstantClasses
{
    public static class ExpansionTokens
    {
        public const string SiteURL = "SITEURL";
        public const string SiteURLSSL = "SITEURLSSL";
        public const string PLDomain = "PLDOMAIN";
        public const string PLServerName = "PLSERVERNAME";
        public const string Year = "YEAR";
        public const string PLPhoneNumber = "PLPHONENUMBER";
        public const string PLSecureLink = "PLSECURELINK";
        public const string PLSecureCreditLink = "PLSECURECREDITLINK";
        public const string PLSecureCheckLink = "PLSECURECHECKLINK";
        public const string CSSupportPhoneNumber = "CSSUPPORTPHONENUMBER";
        public const string HurryDateLandingPageURL = "HURRYDATELANDINGPAGEURL";
        public const string MemeberIDURLParam = "MEMBERIDURLPARAM";
        public const string BrandName = "BRANDNAME";
    }
}