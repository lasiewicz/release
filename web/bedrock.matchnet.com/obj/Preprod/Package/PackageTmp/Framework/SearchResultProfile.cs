using System;
using System.Web.UI.WebControls;

using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Member.ServiceAdapters.Interfaces;
using Matchnet.Member.ValueObjects.Interfaces;
using Matchnet.Member.ValueObjects.Photos;
using Matchnet.MembersOnline.ServiceAdapters;
using Matchnet.Security;
using Matchnet.Web.Framework.Managers;

namespace Matchnet.Web.Framework
{
	/// <summary>
	/// Summary description for SearchResultProfile.
	/// </summary>
	public class SearchResultProfile : FrameworkControl
	{
        public IMemberDTO _Member;
		private int _ordinal;
        private bool _NoPhotoVisible = true;

        public SearchResultProfile(IMemberDTO newMember, int ordinal)
		{
			_Member = newMember;
			_ordinal = ordinal;
		}

		public int Ordinal
		{
			get { return(_ordinal); }
		}

        public bool NoPhotoVisible
        {
            get { return _NoPhotoVisible; }
        }
	
		/// <summary>
		/// Per Test Track Issue #11945, jdate.co.il (like all sites) will display GenderMask as: 
		/// [Gender][seeking][Gender being sought]
		/// </summary>
		/// <param name="DomainID"></param>
		/// <param name="TextGender"></param>
		public void SetGenderMask(int DomainID, Literal TextGender)
		{
			string seekingText = FrameworkGlobals.GetGenderMaskString(_Member.GetAttributeInt(g.Brand, "GenderMask"));
			if(seekingText != String.Empty)
			{
                TextGender.Text = ", " + seekingText;
			}
			else
			{
				TextGender.Text = String.Empty;
			}
			
		}

        public void SetGender(int DomainID, Literal TextGender)
        {
            string seekingText = FrameworkGlobals.GetGenderString(_Member.GetAttributeInt(g.Brand, "GenderMask"));
            if (seekingText != String.Empty)
            {
                TextGender.Text = " " + seekingText + "<br>";
            }
            else
            {
                TextGender.Text = String.Empty;
            }

        }
		
		
		public string showOnClickJavascript(int _counter)
		{
			return "swapState(this, 'hotlistShow" 
				+ _counter.ToString() 
				+ "', 'hotlistSelectText" 
				+ _counter.ToString() 
				+ "', 'hotlistDeselectText" 
				+ _counter.ToString() +"', 'hotListHide', 'hotListShow', 'hideSelect', 'showSelect')";
		}

		public void SetHeadline(Literal txtHeadline, int MemberID, int MAXSTRINGLENGTH, int MAXWORDLENGTH)
		{
			string Headline = FrameworkGlobals.Ellipsis(_Member.GetAttributeTextApproved(g.Brand, "AboutMe", MemberID, g.GetResource("FREETEXT_NOT_APPROVED")), MAXSTRINGLENGTH);
			
			if (Headline != String.Empty && Headline != null)
			{
				char[] delimiters = {' '};
				string[] contents = Headline.Split(delimiters);

				bool longWordFound = false;
			
				for(int i = 0; i < contents.Length; i++)
				{
					if(contents[i].Length > MAXWORDLENGTH)
					{
						for(int j = 1; j <= contents[i].Length / MAXWORDLENGTH; j++)
						{
							if(contents[i].Length * j > MAXWORDLENGTH)
							{
								contents[i] = contents[i].Insert(MAXWORDLENGTH * j , " ");
							}
						}
						longWordFound = true;
					}
				}

				if(longWordFound)
				{
					Headline = String.Join(" ", contents);
				}
			}

			txtHeadline.Text = Headline;

		}


        public void SetHeadline(Literal txtHeadline, string HeadlineText, int MemberID, int MAXSTRINGLENGTH, int MAXWORDLENGTH)
        {
            string Headline = FrameworkGlobals.Ellipsis(HeadlineText, MAXSTRINGLENGTH);

            if (Headline != String.Empty && Headline != null)
            {
                char[] delimiters = { ' ' };
                string[] contents = Headline.Split(delimiters);

                bool longWordFound = false;

                for (int i = 0; i < contents.Length; i++)
                {
                    if (contents[i].Length > MAXWORDLENGTH)
                    {
                        for (int j = 1; j <= contents[i].Length / MAXWORDLENGTH; j++)
                        {
                            if (contents[i].Length * j > MAXWORDLENGTH)
                            {
                                contents[i] = contents[i].Insert(MAXWORDLENGTH * j, " ");
                            }
                        }
                        longWordFound = true;
                    }
                }

                if (longWordFound)
                {
                    Headline = String.Join(" ", contents);
                }
            }

            txtHeadline.Text = Headline;

        }

		public void SetAge(System.Web.UI.WebControls.Literal txtAge, object caller)
		{
			DateTime birthDate = _Member.GetAttributeDate(g.Brand, "BirthDate", DateTime.MinValue);
			txtAge.Text = FrameworkGlobals.GetAgeCaption(birthDate,g,caller,"AGE_UNSPECIFIED");
		}

		public void SetNotes(System.Web.UI.HtmlControls.HtmlInputText txtFavoriteNote)
		{	
			//TODO:gp
			/*
			if(_Member.ViewerHotList.Comments != null && _Member.ViewerHotList.Comments != String.Empty)
			{
				txtFavoriteNote.Value = StringUtil.MaxWordLength(_Member.ViewerHotList.Comments, 35);
			}
			*/
			txtFavoriteNote.Visible = false;
		}


		public void SetUserName(HyperLink lnkUserName, string userNameUrl)
		{	
			lnkUserName.NavigateUrl = userNameUrl;
			lnkUserName.Text = FrameworkGlobals.Ellipsis(_Member.GetUserName(_g.Brand), 12);
		}

        public void SetUserName(Literal txtUserName)
        {
            txtUserName.Text = FrameworkGlobals.Ellipsis(_Member.GetUserName(_g.Brand), 12);
        }

        public void SetAboutMeEssay(Literal txtAboutMeEssay)
        {
            txtAboutMeEssay.Text = FrameworkGlobals.Ellipsis(_Member.GetAttributeText(_g.Brand, "AboutMe"), 100);
        }

		public void SetRegion(int LanguageID, Literal txtRegion, int MAXSIZE)
		{
			//region
			int regionID = _Member.GetAttributeInt(g.Brand, "RegionID");
			if (regionID != Constants.NULL_INT)
			{
				txtRegion.Text = FrameworkGlobals.Ellipsis(FrameworkGlobals.GetRegionString(regionID, LanguageID,false,true,false), MAXSIZE);
			}
		}

		public void SetThumb(Matchnet.Web.Framework.Image imgThumb, 
			Matchnet.Web.Framework.Ui.PageElements.NoPhoto noPhoto, string thumbDestinationUrl)
		{
            //MemberPhotoHelper photoHelper = new MemberPhotoHelper(RuntimeSettings.Instance);
            imgThumb.NavigateUrl = thumbDestinationUrl;
			
			// Instantiate by default the typical no photo setup
			noPhoto.Visible = true;
            _NoPhotoVisible = true;
			imgThumb.Visible = false;
			
			noPhoto.Mode = Matchnet.Web.Framework.Ui.PageElements.NoPhoto.PhotoMode.NoPhoto;
			noPhoto.MemberID = _Member.MemberID;
			noPhoto.DestURL = thumbDestinationUrl;

            Photo photo = MemberPhotoDisplayManager.Instance.GetDefaultPhoto(g.Member, _Member, g.Brand);
			
			if (photo != null && photo.IsApproved)
			{
                if (MemberPhotoDisplayManager.Instance.IsPrivatePhoto(photo, g.Brand)) 
				{
					// Private photo and Cupid domain
					noPhoto.Mode = Matchnet.Web.Framework.Ui.PageElements.NoPhoto.PhotoMode.NoPrivatePhoto;
					return;
				}

                if (!MemberPhotoDisplayManager.Instance.IsPhotosAvailableToGuests(g.Member, _Member, g.Brand)) 
				{
					// Only show photo to guests and not logged in
					noPhoto.DestURL = imgThumb.NavigateUrl;//Applications/Logon/Logon.aspx";
					noPhoto.Mode = Matchnet.Web.Framework.Ui.PageElements.NoPhoto.PhotoMode.OnlyMembers;
					return;
				}

                if (!MemberPhotoDisplayManager.Instance.PhotoIsEmpty(photo, PhotoType.Thumbnail, g.Brand))
				{
                    imgThumb.ImageUrl = MemberPhotoDisplayManager.Instance.GetPhotoDisplayURL(g.Member, _Member, g.Brand,
                                                                       photo, PhotoType.Thumbnail,
                                                                       PrivatePhotoImageType.Thumb,
                                                                        NoPhotoImageType.Thumb);

					noPhoto.Visible = false;
                    _NoPhotoVisible = false;
					imgThumb.Visible = true;
				}
			}
		}

        public void SetThumb(Matchnet.Web.Framework.Image imgThumb, Matchnet.Web.Framework.Ui.PageElements.NoPhoto20 noPhoto, string thumbDestinationUrl)
        {           
            // Instantiate by default the typical no photo setup
            noPhoto.Visible = true;
            _NoPhotoVisible = true;
            imgThumb.Visible = false;

            noPhoto.Mode = Matchnet.Web.Framework.Ui.PageElements.NoPhoto20.PhotoMode.NoPhoto;
            noPhoto.MemberID = _Member.MemberID;
            noPhoto.Member = _Member;
            noPhoto.DestURL = thumbDestinationUrl;
            imgThumb.NavigateUrl = thumbDestinationUrl;

            //MemberPhotoHelper photoHelper = new MemberPhotoHelper(RuntimeSettings.Instance);
            Photo photo = MemberPhotoDisplayManager.Instance.GetDefaultPhoto(g.Member, _Member, g.Brand);

            if (photo != null && photo.IsApproved)
            {
                if (MemberPhotoDisplayManager.Instance.IsPrivatePhoto(photo, g.Brand))
                {
                    // Private photo and Cupid domain
                    noPhoto.Mode = Matchnet.Web.Framework.Ui.PageElements.NoPhoto20.PhotoMode.NoPrivatePhoto;
                    return;
                }

                if (!MemberPhotoDisplayManager.Instance.IsPhotosAvailableToGuests(g.Member, _Member, g.Brand)) //g.Member == null --> not logged in
                {
                    // Only show photo to guests and not logged in
                    noPhoto.DestURL = imgThumb.NavigateUrl;//Applications/Logon/Logon.aspx";
                    noPhoto.Mode = Matchnet.Web.Framework.Ui.PageElements.NoPhoto20.PhotoMode.OnlyMembers;
                    return;
                }

                if (!MemberPhotoDisplayManager.Instance.PhotoIsEmpty(photo, PhotoType.Thumbnail, g.Brand))
                {
                    imgThumb.ImageUrl = MemberPhotoDisplayManager.Instance.GetPhotoDisplayURL(g.Member, _Member, g.Brand,
                                                                                      photo, PhotoType.Thumbnail,
                                                                                      PrivatePhotoImageType.Thumb,
                                                                                      NoPhotoImageType.Thumb);
                    
                    noPhoto.Visible = false;
                    _NoPhotoVisible = false;
                    imgThumb.Visible = true;
                }
            }
        }

        public void SetThumbNoTextNoLink(Matchnet.Web.Framework.Image imgThumb, Matchnet.Web.Framework.Ui.PageElements.NoPhoto20 noPhoto)
        {
            // Instantiate by default the typical no photo setup
            noPhoto.Visible = true;
            _NoPhotoVisible = true;
            imgThumb.Visible = false;

            noPhoto.Mode = Matchnet.Web.Framework.Ui.PageElements.NoPhoto20.PhotoMode.NoPhotoNoTextNoURL;
            noPhoto.MemberID = _Member.MemberID;
            noPhoto.DestURL = string.Empty;
            imgThumb.NavigateUrl = string.Empty;
            //MemberPhotoHelper photoHelper = new MemberPhotoHelper(RuntimeSettings.Instance);
            Photo photo = MemberPhotoDisplayManager.Instance.GetDefaultPhoto(g.Member, _Member, g.Brand);

            if (photo != null && photo.IsApproved)
            {
                if (MemberPhotoDisplayManager.Instance.IsPrivatePhoto(photo, g.Brand))
                {
                    // Private photo and Cupid domain
                    noPhoto.Mode = Matchnet.Web.Framework.Ui.PageElements.NoPhoto20.PhotoMode.NoPrivatePhoto;
                    return;
                }

                if (!MemberPhotoDisplayManager.Instance.IsPhotosAvailableToGuests(g.Member, _Member, g.Brand)) //g.Member == null --> not logged in
                {
                    // Only show photo to guests and not logged in
                    noPhoto.DestURL = imgThumb.NavigateUrl;//Applications/Logon/Logon.aspx";
                    noPhoto.Mode = Matchnet.Web.Framework.Ui.PageElements.NoPhoto20.PhotoMode.OnlyMembers;
                    return;
                }

                if (!MemberPhotoDisplayManager.Instance.PhotoIsEmpty(photo, PhotoType.Thumbnail, g.Brand))
                {
                    imgThumb.ImageUrl = MemberPhotoDisplayManager.Instance.GetPhotoDisplayURL(g.Member, _Member, g.Brand,
                                                                                      photo, PhotoType.Thumbnail,
                                                                                      PrivatePhotoImageType.Thumb,
                                                                                      NoPhotoImageType.Thumb);

                    noPhoto.Visible = false;
                    _NoPhotoVisible = false;
                    imgThumb.Visible = true;
                }
            }
        }
		
		private string EncryptMemberID()
		{
			return Crypto.Encrypt(WebConstants.HTTPIM_ENCRYPT_KEY, _Member.MemberID.ToString() + ",1," + DateTime.Now);
		}
	}
}