using System;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Matchnet.Web.Framework
{
    /// <summary>
    /// Summary description for Txt.
    /// </summary>
    [DefaultProperty("Resource")]
    public class Txt : WebControl
    {
        private ContextGlobal _g;
        private string _target = string.Empty;
        private string _titleResourceConstant = string.Empty;
        private string _resourceConstant = string.Empty;
        private string _defaultResourceConstant = string.Empty;
        private string[] _args;
        private string _href = string.Empty;
        private string _hrefName = string.Empty;
        private string _text = string.Empty;
        private bool _expandImageTokens;
        private FrameworkControl _resourceControl;
        public Txt()
        {
            if (Context != null)
            {
                if (Context.Items["g"] != null)
                {
                    _g = (ContextGlobal)Context.Items["g"];
                }
            }
            else
            {
                _g = new ContextGlobal(null);
            }
        }


        [Bindable(true), Category("Appearance"), DefaultValue("")]
        public string ResourceConstant
        {
            get { return _resourceConstant; }
            set { _resourceConstant = value; }
        }


        [Bindable(true), Category("Appearance"), DefaultValue("")]
        public string TitleResourceConstant
        {
            get { return _titleResourceConstant; }
            set { _titleResourceConstant = value; }
        }

        [Bindable(true), Category("Appearance"), DefaultValue("")]
        public string Target
        {
            get { return _target; }
            set { _target = value; }
        }


        [Bindable(true), Category("Appearance"), DefaultValue("")]
        public string[] Args
        {
            get { return _args; }
            set { _args = value; }
        }


        [Bindable(true), Category("Appearance"), DefaultValue("")]
        public string Href
        {
            get { return FrameworkGlobals.LinkHref(_href, true); }
            set { _href = value; }
        }

        [Bindable(true), Category("Appearance"), DefaultValue("")]
        public string HrefName
        {
            get { return _hrefName; }
            set { _hrefName = value; }
        }


        [Bindable(true), Category("Appearance"), DefaultValue("")]
        public bool ExpandImageTokens
        {
            get { return _expandImageTokens; }
            set { _expandImageTokens = value; }
        }


        [Bindable(true), Category("Appearance"), DefaultValue("")]
        protected string DefaultResourceConstant
        {
            get { return _defaultResourceConstant; }
            set { _defaultResourceConstant = value; }
        }

        [Bindable(true), Category("Appearance"), DefaultValue("")]
        public string Text
        {
            get { return _text; }
            set { _text = value; }
        }


        public FrameworkControl ResourceControl
        {
            get { return _resourceControl; }
            set { _resourceControl = value; }
        }



        /// <summary> 
        /// Render this control to the output parameter specified.
        /// </summary>
        /// <param name="output"> The HTML writer to write out to </param>
        protected override void Render(HtmlTextWriter output)
        {
            if (_href != string.Empty)
            {
                output.WriteBeginTag(HtmlTextWriterTag.A.ToString().ToLower());
                output.WriteAttribute(HtmlTextWriterAttribute.Href.ToString().ToLower(), _href);

                if(_hrefName != string.Empty)
                {
                    output.WriteAttribute(HtmlTextWriterAttribute.Name.ToString().ToLower(), _hrefName);    
                }

                foreach (string key in Attributes.Keys)
                {
                    output.WriteAttribute(key, Attributes[key]);
                }
                if (Target != string.Empty)
                {
                    output.WriteAttribute(HtmlTextWriterAttribute.Target.ToString(), Target);
                }
                if (CssClass != string.Empty)
                {
                    output.WriteAttribute(HtmlTextWriterAttribute.Class.ToString().ToLower(), CssClass);
                }
                if (TitleResourceConstant != string.Empty)
                {
                    if (_resourceControl == null)
                        output.WriteAttribute(HtmlTextWriterAttribute.Title.ToString().ToLower(), _g.GetResource(TitleResourceConstant, this), true);
                    else
                        output.WriteAttribute(HtmlTextWriterAttribute.Title.ToString().ToLower(), _g.GetResource(TitleResourceConstant, _resourceControl), true);
                }

                output.Write(HtmlTextWriter.TagRightChar);
                RenderInnerContent(output);
                output.WriteEndTag(HtmlTextWriterTag.A.ToString().ToLower());
            }
            else if (CssClass != String.Empty || TitleResourceConstant != string.Empty)
            {
                output.WriteBeginTag(HtmlTextWriterTag.Span.ToString().ToLower());
                if (CssClass != string.Empty)
                {
                    output.WriteAttribute(HtmlTextWriterAttribute.Class.ToString().ToLower(), CssClass);
                }
                if (TitleResourceConstant != string.Empty)
                {
                    if (_resourceControl == null)
                        output.WriteAttribute(HtmlTextWriterAttribute.Title.ToString().ToLower(), _g.GetResource(TitleResourceConstant, this), true);
                    else
                        output.WriteAttribute(HtmlTextWriterAttribute.Title.ToString().ToLower(), _g.GetResource(TitleResourceConstant, _resourceControl), true);
                }
                output.Write(HtmlTextWriter.TagRightChar);
                RenderInnerContent(output);
                output.WriteEndTag(HtmlTextWriterTag.Span.ToString().ToLower());
            }
            else
            {
                RenderInnerContent(output);
            }
        }

        private void RenderInnerContent(HtmlTextWriter output)
        {
            // if resource constant is not specified, lookup the default (globally)
            if (ResourceConstant == string.Empty)
            {
                if (DefaultResourceConstant != string.Empty)
                {
                    output.Write(_g.GetResource(DefaultResourceConstant, _args, _expandImageTokens, null));
                }
                else if (Text != string.Empty)
                {
                    output.Write(Text);
                }
            }
            else
            {
                if (_resourceControl == null)
                    output.Write(_g.GetResource(ResourceConstant, _args, _expandImageTokens, this));
                else
                    output.Write(_g.GetResource(ResourceConstant, _args, _expandImageTokens, _resourceControl));
            }
        }
    }
}
