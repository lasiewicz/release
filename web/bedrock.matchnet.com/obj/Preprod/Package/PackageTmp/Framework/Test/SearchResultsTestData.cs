﻿using System;
using System.Collections.Generic;

namespace Matchnet.Web.Framework.Test
{
	public static class SearchResultsTestData
	{
		public const Int32 Total = 50;

		private static readonly Dictionary<Int32, IEnumerable<Int32>> Ranges =
			new Dictionary<Int32, IEnumerable<Int32>>
		{
			{ 1, new[]
			     {
						100042948,
						988269718,
	       			100003352,
	       			100000373,
	       			100020055,
	       			100019971,
	       			100000984,
	       			100000401,
	       			987656154,
	       			100000645,
	       			100042378,
						100047079,
			     }
			},
			{ 2, new[]
			     {
						100042378,
	       			100003352,
	       			100267111,
	       			100000373,
	       			100020055,
	       			100019971,
	       			100000984,
	       			100000401,
	       			987656154,
	       			100000645,
	       			100042378,
						100000645,
			     }
			},
			{ 3, new[]
			     {
	       			100003352,
	       			100267111,
	       			100000373,
	       			100020055,
	       			100019971,
	       			100000984,
	       			100000401,
	       			987656154,
	       			100000645,
	       			100042378,
						100000645,
	       			100042378,
			     }
			},
			{ 4, new[]
			     {
	       			100000308,
	       			100267111,
	       			100000373,
	       			100020055,
	       			100019971,
	       			100000984,
	       			100000401,
	       			987656154,
	       			100000645,
	       			100042378,
						100000645,
	       			100042378,
			     }
			},
			{ 5, new[]
			     {
	       			988272102,
	       			100020478,
			     }
			},
		};

		public static IEnumerable<int> GetTestMemberIds(Int32 range)
		{
			return Ranges.ContainsKey(range) ? Ranges[range] : Ranges[1];
		}
	}
}