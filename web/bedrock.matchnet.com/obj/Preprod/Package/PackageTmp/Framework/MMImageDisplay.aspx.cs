﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using System.Drawing.Imaging;
using System.Net;
using System.IO;

using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects;
using Matchnet.Member.ValueObjects.Photos;
using Matchnet;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Web.Framework.Managers;

namespace Matchnet.Web.Framework
{
    public partial class MMImageDisplay : System.Web.UI.Page
    {
        private const string MAX_WIDTH_SETTING = "MM_LARGE_IMAGE_MAX_WIDTH";
        private const string MAX_HEIGHT_SETTING = "MM_LARGE_IMAGE_MAX_HEIGHT";
        private bool _WriteToWeb = false;
        private bool _WriteToEventLog = false;
        
        
        protected void Page_Load(object sender, EventArgs e)
        {
            Bitmap newImage = null;
            System.Drawing.Image image = null;
            Stream stream = null;
            WebClient client = null;

            try
            {
                int maxWidth = Conversion.CInt(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting(MAX_WIDTH_SETTING));
                int maxHeight = Conversion.CInt(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting(MAX_HEIGHT_SETTING));
                Dictionary<string, int> querystringValues = GetQuerystringValues(Request.Url.Query.Substring(1));

                int memberID = querystringValues["MID"];
                int brandID = querystringValues["BID"];

                if (memberID != Constants.NULL_INT && brandID != Constants.NULL_INT)
                {
                    Member.ServiceAdapters.Member member = MemberSA.Instance.GetMember(memberID, MemberLoadFlags.None);
                    Brand brand = BrandConfigSA.Instance.GetBrandByID(brandID);

                    var photo = MemberPhotoDisplayManager.Instance.GetDefaultPhoto(member, member,
                                                                                     brand);

                    if (photo != null)
                    {
                        Response.ContentType = "image/jpeg";
                        //MemberPhotoHelper photoHelper = new MemberPhotoHelper(RuntimeSettings.Instance);
                        string httpPath = MemberPhotoDisplayManager.Instance.GetPhotoDisplayURL(member, member, brand, photo, 
                            PhotoType.Thumbnail, PrivatePhotoImageType.Thumb, NoPhotoImageType.Matchmail, true);
                        client = new WebClient();
                        stream = client.OpenRead(httpPath);

                        image = System.Drawing.Image.FromStream(stream);
                        if (image.Width > maxWidth || image.Height > maxHeight)
                        {
                            newImage = ResizeImage(image, maxHeight, maxWidth);
                            newImage.Save(Response.OutputStream, ImageFormat.Jpeg);
                            Response.End();
                        }
                        else
                        {
                            image.Save(Response.OutputStream, ImageFormat.Jpeg);
                            Response.End();
                        }
                    }
                    else
                    {
                        //MemberPhotoHelper photoHelper = new MemberPhotoHelper(RuntimeSettings.Instance);
                        image = System.Drawing.Image.FromFile(Server.MapPath(MemberPhotoDisplayManager.Instance.GetNoPhotoFile(NoPhotoImageType.Matchmail, member, brand)));
                        image.Save(Response.OutputStream, ImageFormat.Jpeg);
                        Response.End();
                    }
                }
            }
            catch (System.Threading.ThreadAbortException thEx)
            {
                thEx = null;
            }
            catch (Exception ex)
            {
                Matchnet.Web.Framework.Diagnostics.ContextExceptions.WriteExceptionToEventLogNoG(ex);
                Response.End();
            }
            finally{
                if (newImage != null)
                {
                    newImage.Dispose();
                }
                if (image != null)
                {
                    image.Dispose();
                }
                
            }
        }

        private Dictionary<string, int> GetQuerystringValues(string encryptedString)
        {
            Dictionary<string, int> querystringValues = new Dictionary<string, int>();

            string urlDecodedString = System.Text.ASCIIEncoding.ASCII.GetString(HttpServerUtility.UrlTokenDecode(encryptedString));
            string decrpytedString = Matchnet.Security.Crypto.Decrypt(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("KEY"), urlDecodedString);
            string[] nameValuePairs = decrpytedString.Split(new char[] {'&'});
            string[] individualPair;
            
            foreach (string nameValuePair in nameValuePairs)
            {
                individualPair = nameValuePair.Split(new char[] {'='});
                querystringValues.Add(individualPair[0], Conversion.CInt(individualPair[1]));
            }
            
            return querystringValues;
        }

        private Bitmap ResizeImage(System.Drawing.Image image, int heightLimit, int widthLimit)
        {
            int newWidth = image.Width;
            int newHeight = image.Height;

            if(image.Width > widthLimit)
            {
                newWidth = widthLimit;
                newHeight = image.Height * widthLimit / image.Width;
            }
            else if(image.Height > heightLimit)
            {
                newWidth = image.Width * widthLimit / image.Height;
                newHeight = heightLimit;
            }

            Bitmap bmp = new Bitmap(newWidth, newHeight);
            Graphics g = Graphics.FromImage((System.Drawing.Image)bmp);
            g.DrawImage(image, 0, 0, newWidth, newHeight);
            return bmp;
        }

    }
}
