﻿using System;
using System.Collections.Generic;
using System.Linq;
using System;
using System.Collections;
using System.Data;
using System.Text.RegularExpressions;
using System.Web.Caching;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Text;
using System.Net;
using System.IO;
using System.Web.Script.Serialization;
using System.Runtime.Serialization.Json;
using System.Xml;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Configuration.ValueObjects;
using System.Web;
using System.Web.UI;

namespace Matchnet.Web.Framework
{
    public class MingleFraudScoreClient
    {
        private string _fraudScoreURL;
        
        public static readonly MingleFraudScoreClient Instance = new MingleFraudScoreClient();

        private MingleFraudScoreClient()
        {
            _fraudScoreURL = RuntimeSettings.GetSetting("MINGLE_FRAUD_SCORE_URL");
        }

        public string GetFraudScore(int siteID, int memberID)
        {
            string formattedUri = String.Format(_fraudScoreURL, memberID.ToString(), siteID.ToString());
            string fraudScore = string.Empty;

            Uri serviceUri = new Uri(formattedUri, UriKind.Absolute);
            HttpWebRequest webRequest = (HttpWebRequest)System.Net.WebRequest.Create(serviceUri);
            webRequest.Timeout = 3000;
            try
            {
                HttpWebResponse response = (HttpWebResponse)webRequest.GetResponse();

                XmlDictionaryReader xmlReader = JsonReaderWriterFactory.CreateJsonReader(response.GetResponseStream(), new System.Xml.XmlDictionaryReaderQuotas());

                while (xmlReader.Read())
                {
                    if (xmlReader.NodeType == XmlNodeType.Element && xmlReader.Name.ToLower() == "score")
                    {
                        fraudScore = xmlReader.ReadElementContentAsString();
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                fraudScore = "Error";
                // A little extra protection to ensure compatiblity with .NET designer...
                if (HttpContext.Current != null && HttpContext.Current.Items["g"] != null)
                {
                    ContextGlobal g = (ContextGlobal)HttpContext.Current.Items["g"];
                    g.ProcessException(ex);
                }
            }

            return fraudScore;
        }

    }
}
