﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Matchnet.Web.Framework.Enumerations
{
    public enum RegistrationScenarioType
    {
        Static=0,
        Dynamic=1
    }
}