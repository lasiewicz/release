﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Matchnet.Web.Framework
{
    public enum PhotoType
    {
        Full = 1,
        Thumbnail
    }

    public enum PrivatePhotoImageType
    {
        Full = 0,
        BackgroundThumb = 1,
        Thumb = 2,
        TinyThumb = 3,
        None = 4
    }

    public enum NoPhotoImageType
    {
        BackgroundThumb = 0,
        Thumb = 1,
        BackgroundTinyThumb = 2,
        ThumbV2 = 3,
        TinyThumbV2 = 4,
        Matchmail = 5,
        None = 6
    }
}