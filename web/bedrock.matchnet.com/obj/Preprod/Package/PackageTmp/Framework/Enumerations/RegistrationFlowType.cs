﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Matchnet.Web.Framework.Enumerations
{
    public enum RegistrationFlowType
    {
        Splash=0,
        FullRegistration=1,
        Overlay=2
    }
}