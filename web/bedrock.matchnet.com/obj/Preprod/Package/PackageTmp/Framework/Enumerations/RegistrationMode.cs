using System;

namespace Matchnet.Web.Framework.Enumerations
{
	/// <summary>
	/// Summary description for RegistrationMode.
	/// </summary>
	public enum RegistrationMode : int
	{
		Edit = 0
		,InitialRegistration = 1
		,LoginWithIncompleteSteps = 2
	}

	public enum PlansDisplayMode :int
	{
		AdminMode = 0,
		DefaultSubscription =1 ,
		PromoSubscriptionURL = 2,
		PromoSubscriptionEngine = 3,
		RestoreFromSession = 4
	}
}
