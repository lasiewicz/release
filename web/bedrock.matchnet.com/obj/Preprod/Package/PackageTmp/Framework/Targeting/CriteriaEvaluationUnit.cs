﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Matchnet.Web.Framework.Targeting
{
    public abstract class CriteriaEvaluationUnit
    {
        public abstract bool PassesEvaluationCriteria();

        public abstract void AddEvaluationUnit(CriteriaEvaluationUnit unit, BooleanEvaluationOperator evaluationOperator);
    }
}