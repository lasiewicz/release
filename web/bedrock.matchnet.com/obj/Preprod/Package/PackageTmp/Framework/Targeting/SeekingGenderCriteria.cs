﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Member.ServiceAdapters.Interfaces;

namespace Matchnet.Web.Framework.Targeting
{
    public class SeekingGenderCriteria: MemberTargetingCriteria
    {
        protected IMember _member;
        private GenderCriteriaOperator _criteriaOperator;

        public SeekingGenderCriteria(IMember member, Brand brand, GenderCriteriaOperator criteriaOperator)
            : base(brand)
        {
            _member = member;
            _criteriaOperator = criteriaOperator;
        }

        public override bool PassesCriteria()
        {
            bool passes = false;
            
            bool isMale = FrameworkGlobals.IsSeekingMaleGender(_member, _brand);

            if (isMale && _criteriaOperator == GenderCriteriaOperator.Male) passes = true;
            if (!isMale && _criteriaOperator == GenderCriteriaOperator.Female) passes = true;

            return passes;
        }

        public override string ToString()
        {
            return "SeekingGenderCriteria: " + _criteriaOperator.ToString();
        }
       
    }
}