﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Member.ServiceAdapters.Interfaces;

namespace Matchnet.Web.Framework.Targeting
{
    public class MemberSubscriberStatusCriteria: MemberTargetingCriteria
    {
        private IMember _member;
        private MemberSubscriberStatusCriteriaOperator _criteriaOperator;

        public MemberSubscriberStatusCriteria(IMember member, Brand brand, MemberSubscriberStatusCriteriaOperator criteriaOperator)
            : base(brand)
        {
            _member = member;
            _criteriaOperator = criteriaOperator;
        }

        public override bool PassesCriteria()
        {
            bool passes = false;
            bool isSub = _member.IsPayingMember(_brand.Site.SiteID);

            if (isSub && _criteriaOperator == MemberSubscriberStatusCriteriaOperator.Subscriber) passes = true;
            if (!isSub && _criteriaOperator == MemberSubscriberStatusCriteriaOperator.NonSubscriber) passes = true;

            return passes;
        }

        public override string ToString()
        {
            return "MemberSubscriberStatusCriteria: " + _criteriaOperator.ToString();
        }
    }
}