﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Member.ServiceAdapters.Interfaces;

namespace Matchnet.Web.Framework.Targeting
{
    public class MemberIDLastDigitCriteria: MemberTargetingCriteria
    {
        private IMember _member = null;
        private List<int> _comparativeValueList = null;


        public MemberIDLastDigitCriteria(IMember member, Brand brand, List<int> comparativeValueList)
            : base(brand)
        {
            if (comparativeValueList == null || comparativeValueList.Count == 0)
            {
                throw new ApplicationException("The comparative value list must not be null and must contain at least one value");
            }
            
            _comparativeValueList = comparativeValueList;
            _member = member;

        }

        public override bool PassesCriteria()
        {
            bool passes = false;

            int memberIDLastDigit = _member.MemberID%10;

            if(_comparativeValueList.Contains(memberIDLastDigit)) passes = true;

            return passes;
        }

        public override string ToString()
        {
            return "MemberIDLastDigitCriteria: " + _comparativeValueList.ToString();
        }
    }
}