﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Member.ServiceAdapters.Interfaces;

namespace Matchnet.Web.Framework.Targeting
{
    public class IntAttributeCriteria : AttributeCriteria
    {
        private IntAttributeCriteriaOperator _criteriaOperator;
        private int _comparativeValue = Matchnet.Constants.NULL_INT;

        public IntAttributeCriteria(IMember member, Brand brand, string attributeName, IntAttributeCriteriaOperator criteriaOperator, int comparativeValue)
            : base(member, brand, attributeName)
        {
            _comparativeValue = comparativeValue;
            _criteriaOperator = criteriaOperator;
        }

        public override bool PassesCriteria()
        {
            bool passes = false;

            int attributeValue = _member.GetAttributeInt(_brand, _attributeName, Constants.NULL_INT);
            if (attributeValue == Constants.NULL_INT) return false;

            switch (_criteriaOperator)
            {
                case IntAttributeCriteriaOperator.Equals:
                    if (attributeValue.Equals(_comparativeValue)) passes = true;
                    break;
                case IntAttributeCriteriaOperator.ContainsMaskValue:
                    if ((attributeValue & _comparativeValue) == _comparativeValue) passes = true;
                    break;
            }

            return passes;
        }

        public override string ToString()
        {
            return "IntAttributeCriteria: " + _attributeName + "  " + _criteriaOperator.ToString() + " " + _comparativeValue.ToString();
        }
    }
}