﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Matchnet.Web.Framework.Targeting
{
    public enum BooleanEvaluationOperator
    {
        And=0,
        Or=1, 
        None=2, 
        AndNot=3
    }
}