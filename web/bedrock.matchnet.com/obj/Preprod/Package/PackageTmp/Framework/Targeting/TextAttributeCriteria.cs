﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Member.ServiceAdapters.Interfaces;

namespace Matchnet.Web.Framework.Targeting
{
    public class TextAttributeCriteria : AttributeCriteria
    {
        private TextAttributeCriteriaOperator _criteriaOperator;
        private string _comparativeValue = string.Empty;

        public TextAttributeCriteria(IMember member, Brand brand, string attributeName, TextAttributeCriteriaOperator criteriaOperator, string comparativeValue)
            : base(member, brand, attributeName)
        {
            _comparativeValue = comparativeValue;
            _criteriaOperator = criteriaOperator;
        }

        public override bool PassesCriteria()
        {
            bool passes = false;

            string attributeValue = _member.GetAttributeText(_brand, _attributeName);
            
            switch (_criteriaOperator)
            {
                case TextAttributeCriteriaOperator.Equals:
                    if (attributeValue.Equals(_comparativeValue)) passes = true;
                    break;
                case TextAttributeCriteriaOperator.Contains:
                    if (attributeValue.IndexOf(_comparativeValue) >= 0) passes = true;
                    break;
            }

            return passes;
        }

        public override string ToString()
        {
            return "TextAttributeCriteria: " + _attributeName + "  " + _criteriaOperator.ToString() + " " + _comparativeValue;
        }
    }
}