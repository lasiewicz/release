﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Member.ServiceAdapters.Interfaces;
using Matchnet.Web.Framework.Managers;

namespace Matchnet.Web.Framework.Targeting
{
    public class TargetingEngine
    {
        private IMember _member;
        private Brand _brand;

        private LoggingManager _loggingManager = null;

        public TargetingEngine(IMember member, Brand brand)
        {
            _member = member;
            _brand = brand;
            _loggingManager = new LoggingManager();
        }

        public bool EnableFeature(string feature)
        {
            bool featureEnabled = false;

            ITargetingCriteriaLoader criteriaLoader = new XMLTargetingCriteriaLoader();
            CriteriaEvaluationUnit criteriaEvaluationUnit = null;

            try
            {
                criteriaEvaluationUnit = criteriaLoader.GetCriteria(feature, _member, _brand);
            }
            catch (Exception ex)
            {
                Exception outerEx = new Exception("Error loading criteria.", ex);
                _loggingManager.LogException(outerEx, _member, _brand, "TargetingEngine");
            }

            if (criteriaEvaluationUnit != null)
            {
                try
                {
                    featureEnabled = criteriaEvaluationUnit.PassesEvaluationCriteria();
                }
                catch (Exception ex)
                {
                    Exception outerEx = new Exception("Error evaluating criteria.", ex);
                    _loggingManager.LogException(outerEx, _member, _brand, "TargetingEngine");
                }
            }

            return featureEnabled;
        }
    }
}