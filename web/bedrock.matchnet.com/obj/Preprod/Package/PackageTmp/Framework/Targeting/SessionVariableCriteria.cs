﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Session.ValueObjects;
using Matchnet.Web.Framework.Managers;

namespace Matchnet.Web.Framework.Targeting
{
    public class SessionVariableCriteria : MemberTargetingCriteria
    {
        private string _sessionVariableName;
        public SessionManager _sessionManager;
        public string _comparativeValue;

        public SessionVariableCriteria(Brand brand, SessionManager sessionManager, string sessionVariableName, string comparativeValue)
            : base(brand)
        {
            _sessionVariableName = sessionVariableName;
            _sessionManager = sessionManager;
            _comparativeValue = comparativeValue;
        }

        public override bool PassesCriteria()
        {
            UserSession session = _sessionManager.GetCurrentSession(_brand);
            string variableValue = session.GetString(_sessionVariableName, string.Empty);
            
            return (variableValue == _comparativeValue);
        }
    }
}