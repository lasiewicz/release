﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Matchnet.Web.Framework.Targeting
{
    public enum CriteriaType
    {
        Gender=0,
        SeekingGender=1,
        DateAttributeEquality=2,
        DateAttributeRange=3,
        IntAttribute=4,
        IntAttributeList=5,
        TextAttribute=6,
        MemberID=7,
        MemberIDLastDigit=8, 
        SubscriptionLapseDate=9,
        ApprovedPhotoRequired=10,
        SessionMaxCount=11, 
        BooleanSetting=12
    }
}