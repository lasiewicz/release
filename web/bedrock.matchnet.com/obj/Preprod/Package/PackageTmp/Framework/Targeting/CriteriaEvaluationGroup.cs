﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace Matchnet.Web.Framework.Targeting
{
    public class CriteriaEvaluationGroup : CriteriaEvaluationUnit
    {
        
        private List<CriteriaEvaluationPair> _evaluationUnits = new List<CriteriaEvaluationPair>();


        public CriteriaEvaluationGroup()
        {
            
        }

        public override void AddEvaluationUnit(CriteriaEvaluationUnit unit, BooleanEvaluationOperator evaluationOperator)
        {
            CriteriaEvaluationPair pair = new CriteriaEvaluationPair(unit, evaluationOperator);
            _evaluationUnits.Add(pair);
        }

        public override bool PassesEvaluationCriteria()
        {
            bool passes = true;
            
            if(_evaluationUnits.Count == 1)
            {
                passes = _evaluationUnits[0].EvaluationUnit.PassesEvaluationCriteria();
            }
            else
            {
                CriteriaEvaluationPair previousPair = null;

                foreach (CriteriaEvaluationPair pair in _evaluationUnits)
                {
                    if (previousPair == null)
                    {
                        previousPair = pair;
                    }
                    else
                    {
                        BooleanEvaluationOperator currentOperator = (pair.EvaluationOperator == BooleanEvaluationOperator.None ? previousPair.EvaluationOperator : pair.EvaluationOperator);

                        if (currentOperator == BooleanEvaluationOperator.And && (!previousPair.EvaluationUnit.PassesEvaluationCriteria() || !pair.EvaluationUnit.PassesEvaluationCriteria()))
                            passes = false;
                        if (currentOperator == BooleanEvaluationOperator.Or && (!previousPair.EvaluationUnit.PassesEvaluationCriteria() && !pair.EvaluationUnit.PassesEvaluationCriteria()))
                            passes = false;
                    }

                    if (!passes) break;
                }
            }

            return passes;
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("(");

            foreach (CriteriaEvaluationPair pair in _evaluationUnits)
            {
                sb.Append(pair.EvaluationUnit.ToString());
                if(pair.EvaluationOperator != BooleanEvaluationOperator.None)
                {
                    sb.Append(" " + pair.EvaluationOperator.ToString() + " ");
                }
            }

            sb.Append(")");
            return sb.ToString();
        }

        private class CriteriaEvaluationPair
        {
            public CriteriaEvaluationUnit EvaluationUnit { get; private set; }
            public BooleanEvaluationOperator EvaluationOperator { get; private set; }
            
            public CriteriaEvaluationPair(CriteriaEvaluationUnit evaluationUnit, BooleanEvaluationOperator evaluationOperator)
            {
                EvaluationUnit = evaluationUnit;
                EvaluationOperator = evaluationOperator;
            }
        }
    }
}