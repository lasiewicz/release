﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Xml;
using System.Xml.Serialization;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Member.ServiceAdapters.Interfaces;
using Matchnet.Web.Framework.HTTPContextWrappers;
using Matchnet.Web.Framework.Managers;
using Matchnet.Web.Framework.TemplateControls;
using Matchnet.Web.Framework.Util;
using Matchnet.Web.Interfaces;

namespace Matchnet.Web.Framework.Targeting
{
    public class XMLTargetingCriteriaLoader : ITargetingCriteriaLoader
    {
        private const string XML_FILE_BASE_PATH = "/Framework/Targeting/XML/{0}_{1}.xml";

        private IMember _member;
        private Brand _brand;
        private ICurrentServer _currentServer = null;
        private SessionManager _sessionManager = null;
        private SettingsManager _settingsManager = null;

        public ICurrentServer CurrentServer
        {
            get
            {
                if (_currentServer == null)
                {
                    _currentServer = new CurrentServer();
                }
                return _currentServer;
            }
            set { _currentServer = value; }
        }

        public SessionManager SessionManager
        {
            get
            {
                if (_sessionManager == null)
                {
                    _sessionManager = new SessionManager();
                }
                return _sessionManager;
            }
            set { _sessionManager = value; }
        }

        public SettingsManager SettingsManager
        {
            get
            {
                if (_settingsManager == null)
                {
                    _settingsManager = new SettingsManager();
                }
                return _settingsManager;
            }
            set { _settingsManager = value; }
        }
        

        public CriteriaEvaluationUnit GetCriteria(string feature, IMember member, Brand brand)
        {
            _member = member;
            _brand = brand;

            CriteriaEvaluationUnit memberTargetingCriteria = null;

            string relativefilePath = string.Format(XML_FILE_BASE_PATH, feature, brand.Site.SiteID.ToString());
            string xmlFile = CurrentServer.MapPath(relativefilePath);
            MemberTargetingCriteria currentCriteria = null;

            FileInfo fi = new FileInfo((xmlFile));
            if (fi.Exists)
            {
                memberTargetingCriteria = ParseXMLFile(xmlFile);
            }

            return memberTargetingCriteria;
        }

        private CriteriaEvaluationUnit ParseXMLFile(string path)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(path);

            XmlNodeList nodelist = doc.SelectNodes("/CriteriaList");
            string expression = string.Empty;

            if(nodelist == null)
            {
                throw new Exception("No nodes inside criterialist element or criterlist element not present");
            }

            CriteriaEvaluationUnit unit = ParseNodeList(nodelist[0].ChildNodes);

            return unit;
        }

        private CriteriaEvaluationUnit ParseNodeList(XmlNodeList nodeList)
        {
            CriteriaEvaluationGroup outerEvaluationGroup = new CriteriaEvaluationGroup();
            List<EvaluationPair> pairs = new List<EvaluationPair>();

            NodeType previousNodeType = NodeType.None;
            CriteriaEvaluationUnit currentUnit = null;
            BooleanEvaluationOperator currentOperator = BooleanEvaluationOperator.None;

            foreach (XmlNode childNode in nodeList)
            {
                if (childNode.Name.ToLower() == "evaluationgroup")
                {
                    if (previousNodeType == NodeType.Criteria)
                    {
                        throw new Exception("Operator must follow criteria");
                    }

                    if (childNode.ChildNodes.Count == 0)
                    {
                        throw new Exception("Group must have child nodes");
                    }

                    currentUnit = ParseNodeList(childNode.ChildNodes);
                    previousNodeType = NodeType.Group;
                }
                else if (childNode.Name.ToLower() == "criteriaoperator")
                {
                    if (previousNodeType == NodeType.Operator)
                    {
                        throw new Exception("Operator can not follow operator");
                    }

                    currentOperator = parseOperatorNode(childNode);
                    previousNodeType = NodeType.Operator;
                }
                else if (childNode.Name.ToLower() == "criteria")
                {
                    if (previousNodeType == NodeType.Criteria || previousNodeType == NodeType.Group)
                    {
                        throw new Exception("Criteria ");
                    }
                    currentUnit = ParseCriteriaNode(childNode);
                    previousNodeType = NodeType.Criteria;
                }
                else
                {
                    //unrecognized element type, throw exception 
                    throw new Exception("Element type not recognized: " + childNode.Name);
                }

                if (currentUnit != null && currentOperator != BooleanEvaluationOperator.None)
                {
                    pairs.Add(new EvaluationPair(currentUnit, currentOperator));
                    currentUnit = null;
                    currentOperator = BooleanEvaluationOperator.None;
                }
            }


            foreach (EvaluationPair pair in pairs)
            {
                outerEvaluationGroup.AddEvaluationUnit(pair.EvaluationUnit, pair.EvaluationOperator);
            }

            if (currentUnit != null && currentOperator == BooleanEvaluationOperator.None)
            {
                outerEvaluationGroup.AddEvaluationUnit(currentUnit, currentOperator);
            }

            return outerEvaluationGroup;
        }


        private CriteriaEvaluationUnit ParseEvaluationGroupNode(XmlNode node)
        {
            CriteriaEvaluationGroup outerEvaluationGroup = new CriteriaEvaluationGroup();
            List<EvaluationPair> pairs = new List<EvaluationPair>();

            NodeType previousNodeType = NodeType.None;
            CriteriaEvaluationElement currentElement = null;
            BooleanEvaluationOperator currentOperator = BooleanEvaluationOperator.None;
            
            foreach (XmlNode childNode in node.ChildNodes)
            {
                if (currentElement != null && currentOperator != BooleanEvaluationOperator.None)
                {
                    pairs.Add(new EvaluationPair(currentElement, currentOperator));
                    currentElement = null;
                    currentOperator = BooleanEvaluationOperator.None;
                }
                
                if (childNode.Name.ToLower() == "evaluationgroup")
                {
                    if (previousNodeType == NodeType.Criteria)
                    {
                        throw new Exception("Operator must follow criteria");
                    }
                    previousNodeType = NodeType.Group;
                }
                else if (childNode.Name.ToLower() == "criteriaoperator")
                {
                    if (previousNodeType == NodeType.Operator)
                    {
                        throw new Exception("Operator can not follow operator");
                    }

                    currentOperator = parseOperatorNode(childNode);
                    previousNodeType = NodeType.Operator;
                }
                else if (childNode.Name.ToLower() == "criteria")
                {
                    if (previousNodeType == NodeType.Criteria || previousNodeType == NodeType.Group)
                    {
                        throw new Exception("Criteria ");
                    }
                    currentElement = ParseCriteriaNode(childNode);
                    previousNodeType = NodeType.Criteria;
                }
                else
                {
                    //unrecognized element type, throw exception 
                    throw new Exception("Element type not recognized: " + childNode.Name);
                }
                
                foreach(EvaluationPair pair in pairs)
                {
                    outerEvaluationGroup.AddEvaluationUnit(pair.EvaluationUnit, pair.EvaluationOperator);
                }

            }

            return outerEvaluationGroup;
        }

        private BooleanEvaluationOperator parseOperatorNode(XmlNode node)
        {
            BooleanEvaluationOperator evaluationOperator = BooleanEvaluationOperator.None;
            
            if (node.Attributes == null || node.Attributes[0].Name.ToLower() != "type")
            {
                throw new Exception("Operator node missing type attribute");
            }
            
            try
            {
                evaluationOperator = (BooleanEvaluationOperator)Enum.Parse(typeof(BooleanEvaluationOperator), node.Attributes[0].Value);
            }
            catch (Exception ex)
            {
                throw new Exception("EvaluationOperator not recognized", ex);
            }

            return evaluationOperator;
        }

        private CriteriaEvaluationElement ParseCriteriaNode(XmlNode node)
        {
            string attributes = string.Empty;

            if (node.Name.ToLower() != "criteria")
            {
                throw new Exception("Element type was not criteria:" + node.Name);
            }

            if (node.Attributes == null)
            {
                throw new Exception("No attributes found in criteria node:" + node.Name);
            }

            MemberTargetingCriteria currentCriteria = null;

            string criteriaTypeString = node.Attributes["type"].Value;
            CriteriaType type = (CriteriaType)Enum.Parse(typeof(CriteriaType), criteriaTypeString);

            switch (type)
            {
                case CriteriaType.DateAttributeEquality:
                    currentCriteria = GetDateAttributeEqualityCriteria(node.Attributes);
                    break;
                case CriteriaType.Gender:
                    currentCriteria = GetGenderCritera(node.Attributes);
                    break;
                case CriteriaType.SeekingGender:
                    currentCriteria = GetSeekingGenderCriteria(node.Attributes);
                    break;
                case CriteriaType.DateAttributeRange:
                    currentCriteria = GetDateAttributeRangeCriteria(node.Attributes);
                    break;
                case CriteriaType.IntAttribute:
                    currentCriteria = GetIntAttributeCriteria(node.Attributes);
                    break;
                case CriteriaType.MemberID:
                    currentCriteria = GetMemberIDCriteria(node.Attributes);
                    break;
                case CriteriaType.MemberIDLastDigit:
                    currentCriteria = GetMemberIDLastDigitCriteria(node.Attributes);
                    break;
                case CriteriaType.TextAttribute:
                    currentCriteria = GetTextAttributeCriteria(node.Attributes);
                    break;
                case CriteriaType.SubscriptionLapseDate:
                    currentCriteria = GetSubscriptionLapseDateCriteria(node.Attributes);
                    break;
                case CriteriaType.ApprovedPhotoRequired:
                    currentCriteria = GetApprovedPhotoRequiredCriteria(node.Attributes);
                    break;
                case CriteriaType.SessionMaxCount:
                    currentCriteria = GetSessionMaxCountCriteria(node.Attributes);
                    break;
                case CriteriaType.BooleanSetting:
                    currentCriteria = GetBooleanSettingCriteriaCriteria(node.Attributes);
                    break;
            }

            return new CriteriaEvaluationElement(currentCriteria);
        }

        private DateAttributeEqualityCriteria GetDateAttributeEqualityCriteria(XmlAttributeCollection xmlAttributes)
        {
            try
            {
                string dateEqualityAttributeName = xmlAttributes["attributename"].Value;
                DateTime dateComparativeValue = DateTime.Parse(xmlAttributes["comparativevalue"].Value);
                return new DateAttributeEqualityCriteria(_member, _brand, dateEqualityAttributeName, dateComparativeValue);
            }
            catch (Exception ex)
            {
                throw new Exception("Error parsing date attribute equality criteria", ex);
            }
        }

        private GenderCriteria GetGenderCritera(XmlAttributeCollection xmlAttributes)
        {
            try
            {
                GenderCriteriaOperator genderCriteriaOperator = (GenderCriteriaOperator)Enum.Parse(typeof(GenderCriteriaOperator), xmlAttributes["criteriaoperator"].Value);
                return new GenderCriteria(_member, _brand, genderCriteriaOperator);
            }
            catch (Exception ex)
            {
                throw new Exception("Error parsing gender criteria", ex);
            }
        }

        private SeekingGenderCriteria GetSeekingGenderCriteria(XmlAttributeCollection xmlAttributes)
        {
            try
            {
                GenderCriteriaOperator seekingGenderCriteriaOperator = (GenderCriteriaOperator)Enum.Parse(typeof(GenderCriteriaOperator), xmlAttributes["criteriaoperator"].Value);
                return new SeekingGenderCriteria(_member, _brand, seekingGenderCriteriaOperator);
            }
            catch (Exception ex)
            {
                throw new Exception("Error parsing seeking gender criteria", ex);
            }
        }

        private DateAttributeRangeCriteria GetDateAttributeRangeCriteria(XmlAttributeCollection xmlAttributes)
        {
            try
            {
                string dateRangeAttributeName = xmlAttributes["attributename"].Value;
                DateAttributeRangeCriteriaOperator dateAttributeRangeCriteriaOperator = (DateAttributeRangeCriteriaOperator)Enum.Parse(typeof(DateAttributeRangeCriteriaOperator), xmlAttributes["criteriaoperator"].Value);
                int factor = Convert.ToInt32(xmlAttributes["factor"].Value);
                return new DateAttributeRangeCriteria(_member, _brand, dateRangeAttributeName, dateAttributeRangeCriteriaOperator, factor);
            }
            catch (Exception ex)
            {
                throw new Exception("Error parsing date attribute range criteria", ex);
            }
        }

        private IntAttributeCriteria GetIntAttributeCriteria(XmlAttributeCollection xmlAttributes)
        {
            try
            {
                string intAttributeName = xmlAttributes["attributename"].Value;
                IntAttributeCriteriaOperator intAttributeCriteriaOperator = (IntAttributeCriteriaOperator)Enum.Parse(typeof(IntAttributeCriteriaOperator), xmlAttributes["criteriaoperator"].Value);
                int intComparitiveValue = Convert.ToInt32(xmlAttributes["comparativevalue"].Value);
                return new IntAttributeCriteria(_member, _brand, intAttributeName, intAttributeCriteriaOperator,intComparitiveValue);
            }
            catch (Exception ex)
            {
                throw new Exception("Error parsing int attribute criteria", ex);
            }
        }

        private MemberIDCriteria GetMemberIDCriteria(XmlAttributeCollection xmlAttributes)
        {
            try
            {
                MemberIDCriteriaOperator memberIDCriteriaOperator = (MemberIDCriteriaOperator)Enum.Parse(typeof(MemberIDCriteriaOperator), xmlAttributes["criteriaoperator"].Value);
                return new MemberIDCriteria(_member, _brand, memberIDCriteriaOperator);
            }
            catch (Exception ex)
            {
                throw new Exception("Error parsing memberid criteria", ex);
            }
        }

        private MemberIDLastDigitCriteria GetMemberIDLastDigitCriteria(XmlAttributeCollection xmlAttributes)
        {
            try
            {
                string comparativeValueList = xmlAttributes["comparativevaluelist"].Value;

                List<int> intComparativeValueList = null;
                if (comparativeValueList.Contains(","))
                {
                    intComparativeValueList = new List<int>(comparativeValueList.Split(',').Select(x => int.Parse(x)).ToList());
                }
                else
                {
                    intComparativeValueList = new List<int>() { Convert.ToInt32(comparativeValueList) };
                }
                return new MemberIDLastDigitCriteria(_member, _brand, intComparativeValueList);
            }
            catch (Exception ex)
            {
                throw new Exception("Error parsing memberid last digit criteria", ex);
            }
            
        }

        private TextAttributeCriteria GetTextAttributeCriteria(XmlAttributeCollection xmlAttributes)
        {
            try
            {
                string textAttributeName = xmlAttributes["attributename"].Value;
                TextAttributeCriteriaOperator textAttributeCriteriaOperator = (TextAttributeCriteriaOperator)Enum.Parse(typeof(TextAttributeCriteriaOperator), xmlAttributes["criteriaoperator"].Value);
                string textComparitiveValue = xmlAttributes["comparativevalue"].Value;
                return new TextAttributeCriteria(_member, _brand, textAttributeName, textAttributeCriteriaOperator, textComparitiveValue);
            }
            catch (Exception ex)
            {
                throw new Exception("Error parsing text attribute criteria", ex);
            }

        }

        private SubscriptionLapseDateCriteria GetSubscriptionLapseDateCriteria(XmlAttributeCollection xmlAttributes)
        {
            try
            {
                DateAttributeRangeCriteriaOperator dateAttributeRangeCriteriaOperator = (DateAttributeRangeCriteriaOperator)Enum.Parse(typeof(DateAttributeRangeCriteriaOperator), xmlAttributes["criteriaoperator"].Value);
                int factor = Convert.ToInt32(xmlAttributes["factor"].Value);
                return new SubscriptionLapseDateCriteria(_member, _brand, dateAttributeRangeCriteriaOperator, factor);
            }
            catch (Exception ex)
            {
                throw new Exception("Error parsing subscription lapse date criteria", ex);
            }

        }

        private BooleanSettingCriteria GetBooleanSettingCriteriaCriteria(XmlAttributeCollection xmlAttributes)
        {
            try
            {
                string constant= xmlAttributes["settingconstant"].Value;
                return new BooleanSettingCriteria(_brand, constant, SettingsManager);
            }
            catch (Exception ex)
            {
                throw new Exception("Error parsing subscription lapse date criteria", ex);
            }

        }
        

        private ApprovedPhotoRequiredCriteria GetApprovedPhotoRequiredCriteria(XmlAttributeCollection xmlAttributes)
        {
            try
            {
                return new ApprovedPhotoRequiredCriteria(_member, _brand);
            }
            catch (Exception ex)
            {
                throw new Exception("Error parsing Approved Photo Required criteria", ex);
            }
        }

        private SessionMaxCountCriteria GetSessionMaxCountCriteria(XmlAttributeCollection xmlAttributes)
        {
            try
            {
                string sessionVariableName = xmlAttributes["sessionvariablename"].Value;
                int count = Convert.ToInt32(xmlAttributes["count"].Value);
                return new SessionMaxCountCriteria(_brand, SessionManager, sessionVariableName, count);
            }
            catch (Exception ex)
            {
                throw new Exception("Error parsing Session Max Count criteria", ex);
            }
        }
        
        private enum NodeType
        {
            None=0,
            Group=1,
            Operator=2,
            Criteria=3
        }

        private class EvaluationPair
        {
            public  CriteriaEvaluationUnit EvaluationUnit { get; set; }
            public BooleanEvaluationOperator EvaluationOperator { get; set; }

            public EvaluationPair (CriteriaEvaluationUnit evaluationUnit, BooleanEvaluationOperator evaluationOperator)
            {
                EvaluationUnit = evaluationUnit;
                EvaluationOperator = evaluationOperator;
            }
        }
    }

}