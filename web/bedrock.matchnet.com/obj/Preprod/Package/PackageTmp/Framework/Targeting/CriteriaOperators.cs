﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Matchnet.Web.Framework.Targeting
{
    public enum DateAttributeRangeCriteriaOperator
    {
        WithinNumberOfDaysInFuture = 0,
        WithinNumberOfDaysInFutureInclusive=1,
        WithinNumberOfDaysInPast=2,
        WithinNumberOfDaysInPastInclusive=3,
        GreaterThanNumberOfDaysInFuture = 4,
        GreaterThanNumberOfDaysInFutureInclusive = 5,
        GreaterThanNumberOfDaysInPast = 6,
        GreaterThanNumberOfDaysInPastInclusive = 7
    }

    public enum IntAttributeCriteriaOperator
    {
        Equals = 0,
        ContainsMaskValue=1
    }

    public enum TextAttributeCriteriaOperator
    {
        Equals = 0,
        Contains = 1
    }

    public enum GenderCriteriaOperator
    {
        Male=0,
        Female=1
    }

    public enum MemberIDCriteriaOperator
    {
        Odd=0,
        Even=1
    }

    public enum MemberSubscriberStatusCriteriaOperator
    {
        Subscriber = 0,
        NonSubscriber = 1
    }

}