﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.Member.ServiceAdapters.Interfaces;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Content.ServiceAdapters;
namespace Matchnet.Web.Framework.Targeting
{
    public class DateAttributeRangeCriteria : AttributeCriteria
    {
        private DateAttributeRangeCriteriaOperator _criteriaOperator;
        private int _factor = Constants.NULL_INT;
        
        public DateAttributeRangeCriteria(IMember member, Brand brand, string attributeName, DateAttributeRangeCriteriaOperator criteriaOperator, int factor)
            : base(member, brand, attributeName)
        {
            _factor = factor;
            _criteriaOperator = criteriaOperator;
        }

        public override bool PassesCriteria()
        {
            bool passes = false;

            DateTime attributeValue = _member.GetAttributeDate(_brand, _attributeName, DateTime.MinValue);
            if (attributeValue == DateTime.MinValue) return false;

            switch (_criteriaOperator)
            {
                case DateAttributeRangeCriteriaOperator.WithinNumberOfDaysInFuture:
                    if (attributeValue.Date >= DateTime.Now.Date && attributeValue.Date < DateTime.Now.Date.AddDays(_factor)) passes = true;
                    break;
                case DateAttributeRangeCriteriaOperator.WithinNumberOfDaysInFutureInclusive:
                    if (attributeValue.Date >= DateTime.Now.Date && attributeValue.Date <= DateTime.Now.Date.AddDays(_factor)) passes = true;
                    break;
                case DateAttributeRangeCriteriaOperator.WithinNumberOfDaysInPast:
                    if (attributeValue.Date <= DateTime.Now.Date && attributeValue.Date > DateTime.Now.Date.AddDays(-_factor)) passes = true;
                    break;
                case DateAttributeRangeCriteriaOperator.WithinNumberOfDaysInPastInclusive:
                    if (attributeValue.Date <= DateTime.Now.Date && attributeValue.Date >= DateTime.Now.Date.AddDays(-_factor)) passes = true;
                    break;
                case DateAttributeRangeCriteriaOperator.GreaterThanNumberOfDaysInFuture:
                    if (attributeValue.Date > DateTime.Now.Date && attributeValue.Date > DateTime.Now.Date.AddDays(_factor)) passes = true;
                    break;
                case DateAttributeRangeCriteriaOperator.GreaterThanNumberOfDaysInFutureInclusive:
                    if (attributeValue.Date > DateTime.Now.Date && attributeValue.Date >= DateTime.Now.Date.AddDays(_factor)) passes = true;
                    break;
                case DateAttributeRangeCriteriaOperator.GreaterThanNumberOfDaysInPast:
                    if (attributeValue.Date < DateTime.Now.Date && attributeValue.Date < DateTime.Now.Date.AddDays(_factor)) passes = true;
                    break;
                case DateAttributeRangeCriteriaOperator.GreaterThanNumberOfDaysInPastInclusive:
                    if (attributeValue.Date < DateTime.Now.Date && attributeValue.Date <= DateTime.Now.Date.AddDays(_factor)) passes = true;
                    break;
            }
            
            return passes;
        }

        public override string ToString()
        {
            return "DateAttributeRangeCriteria: " + _attributeName + "  " + _factor.ToString() + " " + _criteriaOperator.ToString();
        }
    }
}