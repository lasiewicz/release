﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Matchnet.Lib;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Ui.BasicElements;

namespace Matchnet.Web.Framework.Validation
{
    public partial class JQValidator : FrameworkControl
    {
        Dictionary<string, string> _rules;
        Dictionary<string, string> _messages;

        string FORMAT_RULE = "{0} :{{\r\n{1}\r\n}}";

        string FORMAT_MESSAGE = "{0} :{{{1}}}";
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public void AddRule(string id, string rule)
        {
            try
            {
                if (_rules == null)
                { _rules = new Dictionary<string, string>(); }

                _rules.Add(id, rule);
            }
            catch (Exception ex) { ex = null; }

        }

        public void AddMessage(string id, string rule)
        {
            try
            {
                if (_messages == null)
                { _messages = new Dictionary<string, string>(); }

                _messages.Add(id, rule);
            }
            catch (Exception ex) { ex = null; }

        }

        private void Page_PreRender(object sender, System.EventArgs e)
        {
            int count = 0;
            if (_rules != null)
            {
                foreach(string key in _rules.Keys)
                {
                    count += 1;
                    litRules.Text += String.Format(FORMAT_RULE, key,_rules[key].ToString() );
                    if (count < _rules.Count)
                        litRules.Text += ",";

                }
            }


            if (_messages != null)
            {
                foreach (string key in _messages.Keys)
                {
                    count += 1;
                    litMessages.Text += String.Format(FORMAT_MESSAGE, key, _messages[key].ToString());
                    if (count < _messages.Count)
                        litMessages.Text += ",";

                }
            }
        }
    }
}