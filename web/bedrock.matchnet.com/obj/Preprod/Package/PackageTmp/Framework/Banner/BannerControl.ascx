<%@ Control Language="c#" AutoEventWireup="false" CodeBehind="BannerControl.ascx.cs"
    Inherits="Matchnet.Web.Framework.Banner.BannerControl" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<%@ Register TagPrefix="uc1" TagName="AdUnit" Src="../Ui/Advertising/AdUnit.ascx" %>
<asp:PlaceHolder ID="plcBanner" runat="server" Visible="True">
    <table cellpadding="0" cellspacing="0" border="0">
        <tr>
            <td>
                <asp:HyperLink runat="server" ID="lnkBanner" />
            </td>
        </tr>
    </table>
</asp:PlaceHolder>
<asp:PlaceHolder ID="plcAdUnit" runat="server" Visible="False">
    <div id="topSubscriptionBanner" style="margin-bottom: 8px;" class="floatInside">
        <div class="adunit-sm-leaderboard">
            <uc1:AdUnit ID="AdUnitSmallLeaderBoard" runat="server" Size="SmallLeaderboardHeaderBelowMenu"
                ExpandImageTokens="true" GAMAdSlot="subnow_top_592x60" />
            <uc1:AdUnit ID="AdUnitSmallLeaderBoardIL" runat="server" Visible="false" ExpandImageTokens="true"
                GAMAdSlot="subnow_top_590x60" />
        </div>
    </div>
</asp:PlaceHolder>
