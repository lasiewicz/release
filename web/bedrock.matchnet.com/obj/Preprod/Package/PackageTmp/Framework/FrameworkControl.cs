using System;
using System.Globalization;
using System.IO;
using System.Web;
using System.Web.UI;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Globalization;
using Matchnet.Web.Framework.Managers;

namespace Matchnet.Web.Framework
{
	public class FrameworkControl : System.Web.UI.UserControl
	{
		#region Properties
		
		private bool _saveSession = true;
        private SettingsManager _settingsManager = null;
	    private ResourceManager _resourceManager = null;

		public bool SaveSession 
		{
			get { return _saveSession; }
			set { _saveSession = value; }
		}
		
		public new bool IsPostBack
		{
			get 
			{ 
				if ( Context.Items["Redirect"] != null ) 
				{
					return false; 
				}
				else 
				{
					return base.IsPostBack; 
				}
			}
		}

        public string ClientIP
        {
            get
            {
                string remoteClientHostIP = string.Empty;
                remoteClientHostIP=  HttpContext.Current.Request.Headers["client-ip"];
                if (remoteClientHostIP == null)
                    remoteClientHostIP = HttpContext.Current.Request.UserHostAddress;
                return remoteClientHostIP;
            }
        }

		protected ContextGlobal _g;
		public ContextGlobal g
		{
			get
			{
				if (_g == null)
					SetupG();
				return _g;
			}
		}

        public SettingsManager SettingsManager
        {
            get
            {
                if (_settingsManager == null)
                {
                    _settingsManager = new SettingsManager();
                }
                return _settingsManager;
            }
        }
        
	    public ResourceManager ResourceManager
	    {
	        get
	        {
                if (_resourceManager == null)
                {
                    _resourceManager = new ResourceManager(g.Member, g.Brand);
                }
	            return _resourceManager;
	        }
	    }

		#endregion

		#region Public Methods
		public FrameworkControl()
		{
			SetupG();
		}

		private void SetupG()
		{
			// A little extra protection to ensure compatiblity with .NET designer...
			if ( Context != null )
			{
				if ( Context.Items["g"] != null )
					_g = (ContextGlobal) Context.Items["g"];
			}
		}

		public virtual void AppInit() 
		{
		}

		public string Image(int AppID, string fileName)
		{
			return Image((WebConstants.APP)AppID,	fileName);
		}

		public string Image(WebConstants.APP app, string fileName)
		{
			Matchnet.Web.Framework.Image I = new Image();
			I.FileName = fileName;
			I.App = app;
			return I.ImageUrl;
		}

		#endregion

		protected override void Render(System.Web.UI.HtmlTextWriter writer)
		{
#if DEBUG
            //if ((g.Session.GetString(WebConstants.SESSIONKEY_RESXHINT_CTRL_TOGGLE  ,"0") == "1"))
            //{
            //    Guid imgButtonId = Guid.NewGuid();
            //    string baseResourcePath = Localizer.ConvertTypeToResourcePath(this.GetType());

            //    // only show a resource hint if there exists a resource folder containing at least one resource file for this control!
            //    string localFilePath = Server.MapPath("~/" + baseResourcePath);
            //    // anchor for the image tag
            //    writer.AddAttribute("href", "//" + Request.Url.Host + ":8080/EditResources.aspx?basepath=" + baseResourcePath + "&site=" + g.Brand.Site.SiteID + "&culture=" + CultureInfo.CurrentCulture.Name);
            //    writer.AddAttribute("target", "_blank");
            //    writer.RenderBeginTag(HtmlTextWriterTag.A);

            //    // edit resource image
            //    writer.AddAttribute("id", imgButtonId.ToString());
            //    //				writer.AddAttribute("class", "resourceHint");
            //    writer.AddAttribute(HtmlTextWriterAttribute.Src, "http://static.matchnet.com/misc/dev/c.gif");
            //    writer.AddAttribute(HtmlTextWriterAttribute.Alt, "Control File: " + baseResourcePath);
            //    writer.AddStyleAttribute("position", "absolute");
            //    writer.AddStyleAttribute("z-index", "100");
            //    //writer.AddStyleAttribute("top", "1px");
            //    //writer.AddStyleAttribute("left", "1px");
            //    writer.AddStyleAttribute(HtmlTextWriterStyle.BorderWidth, "0");
            //    writer.RenderBeginTag("img");
            //    writer.RenderEndTag();

            //    // end anchor tag
            //    writer.RenderEndTag();
            //}
#endif
			// render control content
			base.Render (writer);
		}
	}
}
