using System;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Text.RegularExpressions;

using Matchnet.CachingTemp;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.PagePixel;
using Matchnet.Content.ValueObjects.Region;
using Matchnet.Lib;
using Matchnet.Lib.Xsl;
using System.Collections.Generic;
using Matchnet.Web.Framework;

namespace Matchnet.Web.Framework.PagePixels
{
    [DefaultProperty("PageID"), ToolboxData("<{0}:PagePixel runat=server />")]
    public class PagePixelControl : WebControl
    {
        private ContextGlobal g;
        private int pageID = Constants.NULL_INT;
        private int siteID = Constants.NULL_INT;
        RegionLanguage region;

        public PagePixelControl()
        {
            // There should always be a Context.  If there isn't then something is wrong.
            try
            {
                g = (ContextGlobal)Context.Items["g"];
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        public int PageID
        {
            get { return pageID; }
            set { pageID = value; }
        }

        public string WritePixel()
        {
            if (HttpContext.Current.Request["purchasedUpsale"] != null)
            {
                if (HttpContext.Current.Request["purchasedUpsale"] == "true")
                {
                    // Affiliate tracking pixel has already been rendered in the upsale page 
                    // so do not render another affiliate tracking pixel 
                    return String.Empty;
                }
            }

            StringBuilder sb = new StringBuilder();

            siteID = g.Brand.Site.SiteID;
            if (pageID == Constants.NULL_INT)
            {
                pageID = Conversion.CInt(g.Analytics.GetAttribute("p"));
            }

            PagePixelCollection pixels = null;

            // There is a time out set in content service to prevent pixels from taking the site down.
            try
            {
                pixels = PagePixelSA.Instance.RetrievePagePixels(pageID, siteID);
            }
            catch (System.Exception ex)
            {
                g.ProcessException(new Exception("Page pixel error retrieving page pixels.", ex));
                return String.Empty;
            }

            if (pixels.Count > 0)
            {
                sb.Append(System.Environment.NewLine);
                sb.Append(@"<!-- Affiliate Pixels -->");
                sb.Append(System.Environment.NewLine);
            }

            foreach (PagePixel pixel in pixels)
            {
                try
                {
                    if (pixel != null)
                    {
                        PagePixel p = new PagePixel();
                        p.Code = pixel.Code;
                        p.PageID = pixel.PageID;
                        p.SiteID = pixel.SiteID;
                        p.TargetingConditionCollection = pixel.TargetingConditionCollection;
                        sb.Append(EvaluatePixel(p));
                    }
                }
                catch (Exception ex)
                {
                    g.ProcessException(new Exception("Error processing a pixel.", ex));
                }
            }

            if (pixels.Count > 0)
            {
                sb.Append(@"<!-- End Affiliate Pixels -->");
                sb.Append(System.Environment.NewLine);
            }

            return sb.ToString();
        }

        /// <summary> 
        /// Render this control to the output parameter specified.
        /// </summary>
        /// <param name="output"> The HTML writer to write out to </param>
        protected override void Render(HtmlTextWriter output)
        {
            if (HttpContext.Current.Request["purchasedUpsale"] != null)
            {
                if (HttpContext.Current.Request["purchasedUpsale"] == "true")
                {
                    // Affiliate tracking pixel has already been rendered in the upsale page 
                    // so do not render another affiliate tracking pixel 
                    return;
                }
            }

            siteID = g.Brand.Site.SiteID;
            pageID = Conversion.CInt(g.Analytics.GetAttribute("p"));

            // If reg overlay is shown, force the pageID to be reg page
            if (g.Member == null && FrameworkGlobals.IsRegOverlay())
            {
                if (
                    Convert.ToBoolean(
                        Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting(
                            "REGISTRATION_OVERLAY_ENABLED", g.Brand.Site.Community.CommunityID,
                            g.Brand.Site.SiteID)))
                {
                    pageID = (int)WebConstants.PageIDs.Registration;
                }
            }

            PagePixelCollection pixels = null;

            if (pageID == (int)WebConstants.PageIDs.RegistrationWelcome)
            {
                if (string.IsNullOrEmpty(g.Session["RegCompletePixelNotFired"]))
                {
                    return;
                }
                else
                {
                    g.Session.Remove("RegCompletePixelNotFired");
                }
            }

            // There is a time out set in content service to prevent pixels from taking the site down.
            try
            {
                pixels = PagePixelSA.Instance.RetrievePagePixels(pageID, siteID);
            }
            catch (System.Exception ex)
            {
                g.ProcessException(new Exception("Page pixel error retrieving page pixels.", ex));
                return;
            }

            if (pixels.Count > 0)
            {
                output.Write(System.Environment.NewLine);
                output.Write(@"<!-- Affiliate Pixels -->");
                output.Write(System.Environment.NewLine);
            }

            foreach (PagePixel pixel in pixels)
            {
                try
                {
                    if (pixel != null)
                    {
                        PagePixel p = new PagePixel();
                        p.Code = pixel.Code;
                        p.PageID = pixel.PageID;
                        p.SiteID = pixel.SiteID;
                        p.TargetingConditionCollection = pixel.TargetingConditionCollection;
                        output.Write(EvaluatePixel(p));
                    }
                }
                catch (Exception ex)
                {
                    g.ProcessException(new Exception("Error processing a pixel.", ex));
                }
            }

            if (pixels.Count > 0)
            {
                output.Write(@"<!-- End Affiliate Pixels -->");
                output.Write(System.Environment.NewLine);
            }
        }

        /// <summary>
        /// Apply filtering and adding arguments.
        /// </summary>
        /// <param name="pixel"></param>
        /// <returns></returns>
        private string EvaluatePixel(PagePixel pixel)
        {
            Nullable<Boolean> showPixel = true;

            Nullable<Boolean> showcountryid = null, showlgid = false, showgenderid = false,
                showpromoid = null, showage = null, showpageid = null;

            Dictionary<String, Nullable<Boolean>> checkList = new Dictionary<string, Nullable<Boolean>>();

            #region Targeting

            TargetingConditionCollection conditions = pixel.TargetingConditionCollection;
            bool temp = false;

            foreach (TargetingCondition condition in conditions)
            {
                switch (condition.Name)
                {
                    case "countryid":
                        InitializeRegion();

                        temp = FilterCompare(condition.ConditionValue, region.CountryRegionID.ToString(), condition.Comparison);

                        UpdateDictionaryItem(checkList, "countryid", temp);

                        //if (!checkList.ContainsKey("countryid"))
                        //{
                        //    checkList.Add("countryid", temp);
                        //}
                        //else
                        //{
                        //    temp |= checkList["countryid"].Value;
                        //    checkList.Remove("countryid");
                        //    checkList.Add("countryid", temp);
                        //}
                        break;
                    case "lgid":
                        string lgid = Constants.NULL_STRING;

                        if (g.Member != null && g.Member.GetAttributeText(g.Brand, WebConstants.ATTRIBUTE_NAME_BRANDLUGGAGE) != Constants.NULL_STRING && g.Member.GetAttributeText(g.Brand, WebConstants.ATTRIBUTE_NAME_BRANDLUGGAGE) != "")
                        {
                            lgid = g.Member.GetAttributeText(g.Brand, WebConstants.ATTRIBUTE_NAME_BRANDLUGGAGE);
                        }
                        else
                        {
                            lgid = g.Session["Luggage"];
                        }

                        temp = FilterCompare(condition.ConditionValue, lgid, condition.Comparison);

                        UpdateDictionaryItem(checkList, "lgid", temp);

                        break;
                    case "genderid":
                        if (g.Member != null)
                        {
                            temp = FilterCompare(condition.ConditionValue, g.Member.GetAttributeInt(g.Brand, "GenderMask").ToString(), condition.Comparison);

                            UpdateDictionaryItem(checkList, "genderid", temp);
                        }
                        break;
                    case "promoid":
                        string promoID = Constants.NULL_STRING;

                        if (g.Member != null && g.Member.GetAttributeInt(g.Brand, WebConstants.ATTRIBUTE_NAME_BRANDPROMOTIONID, Constants.NULL_INT) != Constants.NULL_INT)
                        {
                            promoID = Convert.ToString(g.Member.GetAttributeInt(g.Brand, WebConstants.ATTRIBUTE_NAME_BRANDPROMOTIONID));
                        }
                        else
                        {
                            promoID = g.Session["PromotionID"];
                        }

                        temp = FilterCompare(condition.ConditionValue, promoID, condition.Comparison);

                        UpdateDictionaryItem(checkList, "promoid", temp);
                        break;
                    case "age":
                        if (g.Member != null)
                        {
                            temp = FilterCompare(condition.ConditionValue, FrameworkGlobals.GetAge(g.Member.GetAttributeDate(g.Brand, "BirthDate")).ToString(), condition.Comparison);

                            UpdateDictionaryItem(checkList, "age", temp);
                        }
                        break;
                    case "pageid":
                        temp = FilterCompare(condition.ConditionValue, g.AppPage.ID.ToString(), condition.Comparison);

                        UpdateDictionaryItem(checkList, "pageid", temp);

                        break;
                    default:
                        g.ProcessException(new Exception("Page pixel unknown targeting " + condition.Name));
                        break;
                }
            }

            if (conditions.Count > 0)
            {
                foreach (KeyValuePair<string, Nullable<Boolean>> hmm in checkList)
                {
                    if (hmm.Value == null)
                        continue;

                    showPixel &= hmm.Value;
                }
            }
            else
            {
                showPixel = true;
            }

            // None of the filters met, do not render anything for this pixel.
            if (showPixel == false)
            {
                return String.Empty;
            }

            #endregion // Targeting

            #region Arguments

            MatchCollection matchCollection = Regex.Matches(pixel.Code, @"{(.*?)}");

            string[] fields = new string[matchCollection.Count];

            for (int i = 0; i < fields.Length; i++)
            {
                string argumentName = matchCollection[i].Groups[1].Value.ToString();

                switch (argumentName.ToLower())
                {
                    case "birthdate":
                        try
                        {
                            pixel.Code = ReplaceString(pixel.Code, "{birthdate}", g.Member.GetAttributeDate(g.Brand, "Birthdate").ToString("yyyyMMdd"));
                        }
                        catch
                        {
                            pixel.Code = ReplaceString(pixel.Code, "{birthdate}", string.Empty);
                        }
                        break;
                    case "gender":
                        try
                        {
                            pixel.Code = ReplaceString(pixel.Code, "{gender}", g.Member.GetAttributeInt(g.Brand, "GenderMask").ToString());
                        }
                        catch
                        {
                            pixel.Code = ReplaceString(pixel.Code, "{gender}", String.Empty);
                        }
                        break;
                    case "memberid":
                        pixel.Code = ReplaceString(pixel.Code, "{memberid}", g.Session.GetInt(WebConstants.SESSION_PROPERTY_NAME_MEMBERID).ToString());
                        break;
                    case "country":
                        try
                        {
                            InitializeRegion();

                            pixel.Code = ReplaceString(pixel.Code, "{country}", region.CountryRegionID.ToString());
                        }
                        catch
                        {
                            pixel.Code = ReplaceString(pixel.Code, "{country}", string.Empty);
                        }
                        break;
                    case "referring_url":
                        pixel.Code = ReplaceString(pixel.Code, "{referring_url}", HttpUtility.UrlEncode(Convert.ToString(HttpContext.Current.Request.ServerVariables["HTTP_REFERER"])));
                        break;
                    case "state":
                        try
                        {
                            InitializeRegion();

                            pixel.Code = ReplaceString(pixel.Code, "{state}", region.StateRegionID.ToString());
                        }
                        catch
                        {
                            pixel.Code = ReplaceString(pixel.Code, "{state}", string.Empty);
                        }
                        break;
                    case "dma":
                        try
                        {
                            int rid = g.Member.GetAttributeInt(g.Brand, "RegionID", 0);

                            DMA dma = RegionSA.Instance.GetDMAByZipRegionID(rid);

                            pixel.Code = ReplaceString(pixel.Code, "{dma}", dma.DMAID.ToString());
                        }
                        catch
                        {
                            pixel.Code = ReplaceString(pixel.Code, "{dma}", string.Empty);
                        }
                        break;
                    case "age":
                        try
                        {
                            pixel.Code = ReplaceString(pixel.Code, "{age}", FrameworkGlobals.GetAge(g.Member.GetAttributeDate(g.Brand, "BirthDate")).ToString());
                        }
                        catch
                        {
                            pixel.Code = ReplaceString(pixel.Code, "{age}", string.Empty);
                        }
                        break;
                    case "age_group":
                        try
                        {
                            pixel.Code = ReplaceString(pixel.Code, "{age_group}", GetAgeGroup(g.Member.GetAttributeDate(g.Brand, "BirthDate")).ToString());
                        }
                        catch
                        {
                            pixel.Code = ReplaceString(pixel.Code, "{age_group}", string.Empty);
                        }
                        break;
                    case "prm":
                        try
                        {
                            //09252008 TL MPR-457, Updating to ensure the promotion ID rendered for page pixel is based on member's registered promotionID
                            if (g.Member != null && g.Member.GetAttributeInt(g.Brand, WebConstants.ATTRIBUTE_NAME_BRANDPROMOTIONID) > 0 && g.Member.GetAttributeInt(g.Brand, WebConstants.ATTRIBUTE_NAME_BRANDPROMOTIONID) != Constants.NULL_INT)
                            {
                                pixel.Code = ReplaceString(pixel.Code, "{prm}", g.Member.GetAttributeInt(g.Brand, WebConstants.ATTRIBUTE_NAME_BRANDPROMOTIONID).ToString());
                            }
                            else
                            {
                                pixel.Code = ReplaceString(pixel.Code, "{prm}", g.Session["PromotionID"]);
                            }
                        }
                        catch
                        {
                            pixel.Code = ReplaceString(pixel.Code, "{prm}", string.Empty);
                        }
                        break;
                    case "lgid":
                        if (g.Member != null && g.Member.GetAttributeText(g.Brand, WebConstants.ATTRIBUTE_NAME_BRANDLUGGAGE) != Constants.NULL_STRING && g.Member.GetAttributeText(g.Brand, WebConstants.ATTRIBUTE_NAME_BRANDLUGGAGE) != "")
                        {
                            pixel.Code = ReplaceString(pixel.Code, "{lgid}", g.Member.GetAttributeText(g.Brand, WebConstants.ATTRIBUTE_NAME_BRANDLUGGAGE));
                        }
                        else
                        {
                            pixel.Code = ReplaceString(pixel.Code, "{lgid}", g.Session["Luggage"]); ;
                        }
                        break;
                    case "subscription_amount":
                        pixel.Code = ReplaceString(pixel.Code, "{subscription_amount}", HttpUtility.UrlEncode(g.Session["SubAmt"]));
                        break;
                    case "subscription_duration":
                        pixel.Code = ReplaceString(pixel.Code, "{subscription_duration}", HttpUtility.UrlEncode(g.Session["SubDuration"]));
                        break;
                    case "timestamp":
                        pixel.Code = ReplaceString(pixel.Code, "{timestamp}", DateTime.Now.ToString("yyyyMMddHHmmss"));
                        break;
                    default:
                        // This line was throwing massive exceptions due to wrong arg names being passed in. 12/28/09
                        //g.ProcessException(new Exception("Page Pixel argument " + argumentName + " not found."));
                        break;
                }

            #endregion // Populate Arguments

            }

            return pixel.Code + System.Environment.NewLine;
        }

        #region Helper Functions

        private void InitializeRegion()
        {
            if (region == null)
            {
                region = new RegionLanguage();

                region = RegionSA.Instance.RetrievePopulatedHierarchy(g.Member.GetAttributeInt(g.Brand, "RegionID"), g.Brand.Site.LanguageID);
            }
            // return string.Format("<img src=\"{0}{1}\" width=\"1\" height=\"1\" alt=\"\" />\r\n", protocol, href);
        }

        private int GetAgeGroup(DateTime DOB)
        {
            int age = FrameworkGlobals.GetAge(DOB);

            return GetAgeGroup(age);
        }

        private int GetAgeGroup(int Age)
        {
            if (Age >= 18 && Age <= 25)
                return ConstantsTemp.AGE_GROUP_18_TO_25;
            else if (Age >= 26 && Age <= 34)
                return ConstantsTemp.AGE_GROUP_26_TO_34;
            else if (Age >= 35 && Age <= 45)
                return ConstantsTemp.AGE_GROUP_35_TO_45;
            else if (Age >= 46 && Age <= 60)
                return ConstantsTemp.AGE_GROUP_46_TO_60;
            else if (Age >= 60)
                return ConstantsTemp.AGE_GROUP_60_TO_99;
            else
                return 0;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="checkList"></param>
        /// <param name="key"></param>
        /// <param name="temp"></param>
        /// <returns></returns>
        private void UpdateDictionaryItem(Dictionary<String, Nullable<Boolean>> checkList, string key, bool temp)
        {
            if (!checkList.ContainsKey(key))
            {
                checkList.Add(key, temp);
            }
            else
            {
                temp |= checkList[key].Value;
                checkList.Remove(key);
                checkList.Add(key, temp);
            }
        }

        private bool FilterCompare(string value1, string value2, string op)
        {
            try
            {
                int value1int = Convert.ToInt32(value1);
                int value2int = Convert.ToInt32(value2);

                switch (op)
                {
                    case "1":
                        return (value1int == value2int);
                    case "2":
                        return (value1int != value2int);
                    case "3":
                        return (value1int < value2int);
                    case "4":
                        return (value1int <= value2int);
                    case "5":
                        return (value1int > value2int);
                    case "6":
                        return (value1int >= value2int);
                    case "7":
                        return (value2.IndexOf(value1) != -1);
                    default:
                        g.ProcessException(new Exception("Page pixel unknown filter operation. " + op.ToString()));
                        return false;
                }
            }
            catch (System.Exception ex)
            {
                g.ProcessException(new Exception("Page pixel error running comparison.", ex));
                return false;
            }
        }

        /// <summary>
        /// Case insensitive string replacer.
        /// </summary>
        /// <param name="source"></param>
        /// <param name="find"></param>
        /// <param name="replace"></param>
        /// <returns></returns>
        private string ReplaceString(string source, string find, string replace)
        {
            return System.Text.RegularExpressions.Regex.Replace(source, find, replace, System.Text.RegularExpressions.RegexOptions.IgnoreCase);
        }

        #endregion
    }
}
