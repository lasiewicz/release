using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.ComponentModel;

using Matchnet.Content;

namespace Matchnet.Web.Framework
{
    /// <summary>
    /// Summary description for Validator.
    /// </summary>
    public class RequiredFieldValidator : System.Web.UI.WebControls.RequiredFieldValidator
    {
        string _ErrorResourceConstant = "RESOURCE_NOT_FOUND";
        string _FieldNameResourceConstant = "RESOURCE_NOT_FOUND";
        Control _ResourceControl;

        [Bindable(true), 
        Category("Appearance"), 
        DefaultValue("RESOURCE_NOT_FOUND")]
            // public Translator.Resources ErrorResource
        public string ErrorResourceConstant
        {
            get
            {
                return _ErrorResourceConstant;
            }
            set
            {
                _ErrorResourceConstant = value;
            }
        }

        [Bindable(true), 
        Category("Appearance"), 
        DefaultValue("RESOURCE_NOT_FOUND")]
        public string FieldNameResourceConstant
        {
            get
            {
                return _FieldNameResourceConstant;
            }
            set
            {
                _FieldNameResourceConstant = value;
            }
        }

        public Control ResourceControl
        {
            get {
                if (null == _ResourceControl)
                {
                    _ResourceControl = this;
                }
                return _ResourceControl; 
            }
            set { _ResourceControl = value; }
        }

        /// Render this control to the output parameter specified.
        /// </summary>
        /// <param name="output"> The HTML writer to write out to </param>
        protected override void Render(HtmlTextWriter output)
        {
            if ( CssClass.Length == 0 )
            {
                CssClass = "error";
            }

            ContextGlobal g=null;
            if  ( HttpContext.Current != null )
            {
                g = (ContextGlobal)HttpContext.Current.Items["g"];
            }

            if ( g != null )
            {
                if ( _FieldNameResourceConstant != "RESOURCE_NOT_FOUND" )
                {
                    base.ErrorMessage = g.GetResource(_ErrorResourceConstant, ResourceControl, new string[1] { g.GetResource(_FieldNameResourceConstant, ResourceControl) }
                                                     );
                }
                else
                {
                    base.ErrorMessage = g.GetResource(_ErrorResourceConstant, ResourceControl);
                }
            }
            else
            {
                base.ErrorMessage = "["+ _FieldNameResourceConstant + ":" + _ErrorResourceConstant + "]";
            }

            base.Render(output);
        }

    }
}
