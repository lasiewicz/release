using System;
using System.Web;
using System.Web.UI.WebControls;
using System.Text;
using Matchnet.Content.ValueObjects.PageConfig;

namespace Matchnet.Web.Framework
{
	/// <summary>
	/// Summary description for Popup.
	/// </summary>
	public class Popup : Literal
	{
		protected ContextGlobal _g;

		private LayoutTemplate _layoutTemplate = LayoutTemplate.Popup;

		private string _url = "";
		private string _name = "";
		private string _properties = "";
		
		private bool _enabled = false;
		private bool _renderClientScript = false;
		
		private int _width;
		private int _height;

		/// <summary>
		/// Determines the behavior of the Popup.
		/// </summary>
		/// <remarks>
		/// The Popup may need to show whenver the user navigates to another page or closes the window (<c>GenericPopupType</c>),
		/// only when the user closes the window (<c>ExitPopupType</c>).  Other types may be added in the future for different
		/// behaviors.
		/// </remarks>
		public enum PopupTypeEnum
		{
			GenericPopupType
			, ExitPopupType
		}
		/// <remarks>Default to GenericPopupType.</remarks>
		private PopupTypeEnum _PopupType = PopupTypeEnum.GenericPopupType;

		public Popup()
		{
			if ( Context != null )
			{
				if ( Context.Items["g"] != null )
					_g = (ContextGlobal) Context.Items["g"];
			}		
		}

		public string Url
		{
			get
			{
				return _url;
			}
			set
			{
				_url = value;
			}
		}

		public LayoutTemplate LayoutTemplate
		{
			get
			{
				return _layoutTemplate;
			}
			set
			{
				_layoutTemplate = value;
			}
		}

		public bool Enabled
		{
			get
			{
				return _enabled;
			}
			set
			{
				_enabled = value;
			}
		}

		public string Name
		{
			get
			{
				return _name;
			}
			set
			{
				_name = value;
			}
		}


		public int Width
		{
			get
			{
				return _width;
			}
			set
			{
				_width = value;
			}
		}

		public int Height
		{
			get
			{
				return _height;
			}
			set
			{
				_height = value;
			}
		}

		public string Properties
		{
			get
			{
				return _properties;
			}
			set
			{
				_properties = value;
			}
		}

		public PopupTypeEnum PopupType
		{
			get
			{
				return _PopupType;
			}
			set
			{
				_PopupType = value;
			}
		}

		private void DetermineRenderClientScript()
		{
			if (_g.Page != null && _g.Page.Request != null)
			{
				HttpBrowserCapabilities bc = _g.Page.Request.Browser;
				bool hasEcmaScript = 
					(bc.EcmaScriptVersion.CompareTo(new Version(1, 2)) >= 0);
				bool hasDOM = 
					(bc.MSDomVersion.Major >= 4);
				bool hasBehaviors = 
					(bc.MajorVersion > 5) ||
					((bc.MajorVersion == 5) &&
					(bc.MinorVersion >= .0));

				_renderClientScript = 
					hasEcmaScript && hasDOM && hasBehaviors;
			}
		}

		protected override void OnPreRender(EventArgs e)
		{
			string PopupScriptName = "";

			base.OnPreRender(e);

			if (_enabled)
			{
				if ( _g != null )
				{
					if (_url != null)
					{
						DetermineRenderClientScript();
						if (_renderClientScript)
						{
							StringBuilder script = new StringBuilder();
						
							PopupScriptName = DefinePopupScriptName(_PopupType);

							string layoutTemplateID = ((int) _layoutTemplate).ToString();
							string scriptKey = typeof(Popup).FullName;
							if (_url.ToLower().IndexOf("LayoutTemplateID") == -1)
								_url += 
									(_url.IndexOf("?") == -1 ? 
									"?LayoutTemplateID=" + layoutTemplateID : 
									"&LayoutTemplateID=" + layoutTemplateID);

							script.Append("<script language='javascript'>");
							script.Append(Environment.NewLine);
							script.Append("if (typeof(" + PopupScriptName + ") != \"undefined\")");
							script.Append(Environment.NewLine);
							script.Append("window.onunload=" + PopupScriptName + ";");
							script.Append(Environment.NewLine);
							script.Append("function CheckForVarAndReset() {");
							script.Append(Environment.NewLine);
							script.Append("		if ( typeof(Page_BlockSubmit) == \"undefined\") return;" );
							script.Append(Environment.NewLine);
							script.Append("if ( Page_BlockSubmit == false ) ResetOnUnloadEvent();");
							script.Append(Environment.NewLine);
							script.Append("}");
							script.Append(Environment.NewLine);
							script.Append("</script>");
							script.Append(Environment.NewLine);

							_g.Page.RegisterStartupScript(scriptKey, script.ToString());

							_g.Page.RegisterArrayDeclaration("popupInitValues", 
								"'" + _url + "'");
							_g.Page.RegisterArrayDeclaration("popupInitValues", 
								"'" + _name + "'");
							_g.Page.RegisterArrayDeclaration("popupInitValues", 
								"'" + _width.ToString() + "'");
							_g.Page.RegisterArrayDeclaration("popupInitValues", 
								"'" + _height.ToString() + "'");
							_g.Page.RegisterArrayDeclaration("popupInitValues", 
								"'" + _properties + "'");

							_g.Page.RegisterOnSubmitStatement(scriptKey, "javascript:CheckForVarAndReset();");
							//_g.Page.RegisterOnSubmitStatement(scriptKey, "if ( Page_BlockSubmit == false ) ResetOnUnloadEvent();");
						}
					}
				}
			}
		
		}

		private string DefinePopupScriptName(PopupTypeEnum PopupType)
		{
			switch (PopupType)
			{
				case PopupTypeEnum.ExitPopupType:
					return "ExitPopupInit";
				case PopupTypeEnum.GenericPopupType:
					return "PopupInit";
				default:
					return "PopupInit";
			}
		}
	}
}
