<%@ Control Language="c#" AutoEventWireup="false" Codebehind="FirstElementFocus.ascx.cs" Inherits="Matchnet.Web.Framework.JavaScript.FirstElementFocus" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<script type="text/javascript">
<!--
function FirstElementFocus_giveElementFocus()
{
	var elementId	= '<asp:literal id="elementId" runat="server" />';
	var element		= document.getElementById(elementId);
	
	if (element == null)
	{
		return;
	}
		
	element.focus();
}

$j(document).ready(function(){FirstElementFocus_giveElementFocus()});
//-->
</script>