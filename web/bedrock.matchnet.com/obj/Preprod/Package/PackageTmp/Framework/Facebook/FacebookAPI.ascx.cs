﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Configuration.ValueObjects;

namespace Matchnet.Web.Framework.Facebook
{
    public partial class FacebookAPI : FrameworkControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected string GetFacebookAppID()
        {
            return SettingsManager.GetSettingString(SettingConstants.FACEBOOK_APP_ID, g.Brand);
        }
    }
}