﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FacebookAPI.ascx.cs" Inherits="Matchnet.Web.Framework.Facebook.FacebookAPI" %>
<script type="text/javascript">
    //Facebook
    __addToNamespace__('spark.facebook', {
        appId: "<%=GetFacebookAppID() %>",
        authenticationMessage: '<%=g.GetResource("TXT_MSG_MUST_LOG_IN",this) %>',
        connectionError: '<%=g.GetResource("TXT_MSG_UNABLE_TO_CONNECT",this) %>',
        fbConnectSave: function (fbAuthResponse) {
            $j.ajax({
                type: "POST",
                url: "/Applications/API/Facebook.asmx/UpdateFacebookConnect",
                data: "{fbUserID:\"" + fbAuthResponse.userID + "\", accessToken:\"" + fbAuthResponse.accessToken + "\", expiresInSeconds:" + fbAuthResponse.expiresIn + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(result) {
                    var updateResult = (typeof result.d == 'undefined') ? result : result.d;
                    if (updateResult.Status != 2) {
                        alert(updateResult.StatusMessage);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert(textStatus);
                }
            });
        },
        fbInitialized: false
    });

    window.fbAsyncInit = function () {
        FB.init({
            appId: spark.facebook.appId, // App ID
            version: 'v2.0', // version of the API
            // removing the channel file since it is not needed for our implementation and it causes a bug in IE
            // the IE bug: if the user cancels the popup, the popup does not close
            //channelUrl: '//' + document.domain + '/fbchannel/channel.html', // Channel File
            channelUrl: 'http://' + window.location.hostname + '/fbchannel/channel.aspx', // Channel File
            status: true, // check login status
            cookie: true, // enable cookies to allow the server to access the session
            xfbml: true  // parse XFBML
        });

        //alert('http://' + window.location.hostname + '/fbchannel/channel.aspx');

        __addToNamespace__('spark.facebook', {
            fbAuth: function (userAuth, userCanceled, scopes) {
                FB.login(function (response) {
                    if (response.authResponse) {
                        //save user connection
                        spark.facebook.fbConnectSave(response.authResponse);
                        if (userAuth) { userAuth() };
                    } else {
                        if (userCanceled) { userCanceled() };
                    }
                }, {
                    scope: scopes
                });
            }
        });

        spark.facebook.fbInitialized = true;
    };

    // Load the SDK Asynchronously
    (function (d) {
        var js, id = 'facebook-jssdk'; if (d.getElementById(id)) { return; }
        js = d.createElement('script'); js.id = id; js.async = true;
        js.src = "//connect.facebook.net/en_US/sdk.js"; // update source location after v2.0
        d.getElementsByTagName('head')[0].appendChild(js);
    } (document));
    </script>
    <div id="fb-root"></div>