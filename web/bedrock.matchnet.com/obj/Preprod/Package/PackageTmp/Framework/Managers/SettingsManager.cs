﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Web.Interfaces;

namespace Matchnet.Web.Framework.Managers
{
    public class SettingsManager
    {
        private ISettingsSA _settingsService;
        private ILoggingManager _loggingManager = null;

        public ISettingsSA SettingsService
        {
            get{ return _settingsService ?? RuntimeSettings.Instance; }
            set { _settingsService = value; }
        }

        public ILoggingManager LoggingManager
        {
            get
            {
                if (_loggingManager == null)
                {
                    _loggingManager = new LoggingManager();
                }
                return _loggingManager;
            }

            set { _loggingManager = value; }
        }

        public int GetSettingInt(string settingConstant, Brand brand)
        {
            return GetSettingInt(settingConstant, brand, Constants.NULL_INT);
        }

        public int GetSettingInt(string settingConstant, Brand brand, int defaultValue)
        {

            if (brand == null || brand.Site == null || brand.Site.Community == null)
            {
                throw new ArgumentNullException("brand");
            }

            return GetSettingInt(settingConstant, brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID, defaultValue);

        }

        public int GetSettingInt(string settingConstant, int communityID, int siteID, int brandID)
        {
            return GetSettingInt(settingConstant, communityID, siteID, brandID, Constants.NULL_INT);
        }

        public int GetSettingInt(string settingConstant, int communityID, int siteID, int brandID, int defaultValue)
        {
            int settingValue = defaultValue;
            try
            {
                settingValue = Conversion.CInt(SettingsService.GetSettingFromSingleton(settingConstant, communityID, siteID, brandID));
            }
            catch (Exception ex)
            {
                settingValue = defaultValue;
                LoggingManager.LogException(ex, null, communityID, siteID, brandID, "SettingsManager");
            }

            return settingValue;
        }

        public bool GetSettingBool(string settingConstant, Brand brand)
        {
            return GetSettingBool(settingConstant, brand, false);
        }

        public bool GetSettingBool(string settingConstant, Brand brand, bool defaultValue)
        {            
            if (brand == null || brand.Site == null || brand.Site.Community == null)
            {
                throw new ArgumentNullException("brand");
            }

            return GetSettingBool(settingConstant, brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID, defaultValue);
        }

        public bool GetSettingBool(string settingConstant, int communityID, int siteID, int brandID)
        {
            return GetSettingBool(settingConstant, communityID, siteID, brandID, false);
        }

        public bool GetSettingBool(string settingConstant, int communityID, int siteID, int brandID, bool defaultValue)
        {
            bool settingValue = defaultValue;
            try
            {
                settingValue = Conversion.CBool(SettingsService.GetSettingFromSingleton(settingConstant, communityID, siteID, brandID));
            }
            catch (Exception ex)
            {
                settingValue = defaultValue;
                LoggingManager.LogException(ex, null, communityID, siteID, brandID, "SettingsManager");
            }

            return settingValue;
        }

        public string GetSettingString(string settingConstant, Brand brand)
        {
            return GetSettingString(settingConstant, brand, string.Empty);
        }

        public string GetSettingString(string settingConstant, Brand brand, string defaultValue)
        {
            string settingValue = string.Empty;

            if (brand == null || brand.Site == null || brand.Site.Community == null)
            {
                throw new ArgumentNullException("brand");
            }

            return GetSettingString(settingConstant, brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID, defaultValue);
        }

        public string GetSettingString(string settingConstant, int communityID, int siteID, int brandID)
        {
            return GetSettingString(settingConstant, communityID, siteID, brandID, string.Empty);
        }

        public string GetSettingString(string settingConstant, int communityID, int siteID, int brandID, string defaultValue)
        {
            string settingValue = defaultValue;
            try
            {
                settingValue = SettingsService.GetSettingFromSingleton(settingConstant, communityID, siteID, brandID);
            }
            catch (Exception ex)
            {
                settingValue = defaultValue;
                LoggingManager.LogException(ex, null, communityID, siteID, brandID, "SettingsManager");
            }

            return settingValue;
        }

        public string GetTokenEnvironmentForCookie()
        {
            string _tokenCookieEnvironmentValue = ConfigurationManager.AppSettings["TokenCookieEnvironment"] ?? String.Empty;
            if (string.IsNullOrEmpty(_tokenCookieEnvironmentValue)) return _tokenCookieEnvironmentValue;
            var environmentHost = string.Empty;
            switch (_tokenCookieEnvironmentValue)
            {
                case "stgv3":
                case "newstgv3":
                    environmentHost = "stgv3";
                    break;
                default:
                    environmentHost = _tokenCookieEnvironmentValue;
                    break;
            }

            return environmentHost;
        }
    }
}