﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.Search.ValueObjects;

namespace Matchnet.Web.Framework.Managers
{
    public enum Sector
    {
        Default = 0,
        Religious = 1,
        Academic = 2,
        Divorced = 3,
        Elderly = 4,
        HommesJuifs = 5,
        FemmesJuives = 6
    }

    public class SectorsManager
    {
        public static Sector GetSector()
        {
            Sector result = Sector.Default;
            int sectionID;

            string fullURL = HttpContext.Current.Request.Url.AbsoluteUri;

            if (fullURL.Contains("religious-dating"))
                result = Sector.Religious;
            else if (fullURL.Contains("academic-dating"))
                result = Sector.Academic;
            else if (fullURL.Contains("Divorced-dating"))
                result = Sector.Divorced;
            else if (fullURL.Contains("Senior-dating"))
                result = Sector.Elderly;
            else if (fullURL.Contains("Les-hommes-juifs"))
                result = Sector.HommesJuifs;
            else if (fullURL.Contains("Femmes-juives"))
                result = Sector.FemmesJuives;
            else if (int.TryParse(HttpContext.Current.Request.QueryString["Section"], out sectionID))
                result = (Sector)sectionID;
            else if (int.TryParse(HttpContext.Current.Request.QueryString["sector"], out sectionID))
                result = (Sector)sectionID;

            return result;
        }

        public static SearchPreferenceCollection GetSearchpreferences(Sector sector, ContextGlobal g, bool showFemaleSeekingMale = false)
        {
            string fullURL = HttpContext.Current.Request.Url.AbsoluteUri;


            SearchPreferenceCollection preferences = new SearchPreferenceCollection();

            foreach (string key in g.SearchPreferences.Keys())
            {
                preferences.Add(key, g.SearchPreferences.Value(key));
            }

            ResetPreferences(preferences);

            // Shared preferences
            preferences.Add("SearchTypeID", "2");
            preferences.Add("SearchOrderBy", "1");
            preferences.Add("CountryRegionID", "105");
            preferences.Add("HasPhotoFlag", "1");
            preferences.Add("AreaCode1", "03");

            // Unique preferences
            switch (sector)
            {
                case Sector.Academic:
                    preferences.Add("GenderMask", "9");
                    if (showFemaleSeekingMale)
                    {
                        preferences.Add("GenderMask", "6");
                    }
                    if (fullURL.Contains("gender=women"))
                    {
                        preferences.Add("GenderMask", "6");
                    }
                    preferences.Add("MinAge", "21");
                    preferences.Add("MaxAge", "30");
                    preferences.Add("EducationLevel", "120");
                    break;
                case Sector.Religious:
                    preferences.Add("GenderMask", "9");
                    preferences.Add("MinAge", "18");
                    preferences.Add("MaxAge", "40");
                    preferences.Add("JDateReligion", "1087");
                    preferences.Add("KeepKosher", "2");
                    preferences.Add("SynagogueAttendance", "1");
                    break;
                case Sector.Divorced:
                    preferences.Add("GenderMask", "6");
                    preferences.Add("MinAge", "18");
                    preferences.Add("MaxAge", "99");
                    preferences.Add("MaritalStatus", "8");
                    break;
                case Sector.Elderly:
                    preferences.Add("GenderMask", "6");
                    preferences.Add("MinAge", "50");
                    preferences.Add("MaxAge", "99");
                    break;
                case Sector.HommesJuifs:
                    preferences.Add("SearchTypeID", "4");
                    preferences.Add("SearchOrderBy", "1");
                    preferences.Add("GenderMask", "9");
                    preferences.Add("HasPhotoFlag", "1");
                    preferences.Add("MinAge", "18");
                    preferences.Add("MaxAge", "90");

                    preferences.Add("CountryRegionID", "76");
                    preferences.Add("regionid", "9795409");
                    preferences.Add("geodistance", "7");
                    preferences.Add("regionidcity", "9795409");
                    preferences.Add("distance", "160");
                    preferences.Add("jdatereligion", "24575");
                    break;
                case Sector.FemmesJuives:
                    preferences.Add("SearchTypeID", "4");
                    preferences.Add("SearchOrderBy", "1");
                    preferences.Add("GenderMask", "6");
                    preferences.Add("HasPhotoFlag", "1");
                    preferences.Add("MinAge", "18");
                    preferences.Add("MaxAge", "90");

                    preferences.Add("CountryRegionID", "76");
                    preferences.Add("regionid", "9795409");
                    preferences.Add("geodistance", "7");
                    preferences.Add("regionidcity", "9795409");
                    preferences.Add("distance", "160");
                    preferences.Add("jdatereligion", "24575");
                    break;
            }

            // g.SaveSearchPreferences();

            return preferences;
        }



        private static void ResetPreferences(SearchPreferenceCollection preferences)
        {
            preferences.Add("SchoolID", HttpContext.Current.Request["SchoolID"]);
            preferences.Add("HasPhotoFlag", HttpContext.Current.Request["HasPhotoFlag"]);
            preferences.Add("EducationLevel", HttpContext.Current.Request["EducationLevel"]);
            preferences.Add("Religion", HttpContext.Current.Request["Religion"]);
            preferences.Add("LanguageMask", HttpContext.Current.Request["LanguageMask"]);
            preferences.Add("Ethnicity", HttpContext.Current.Request["Ethnicity"]);
            preferences.Add("SmokingHabits", HttpContext.Current.Request["SmokingHabits"]);
            preferences.Add("DrinkingHabits", HttpContext.Current.Request["DrinkingHabits"]);
            preferences.Add("MinHeight", HttpContext.Current.Request["MinHeight"]);
            preferences.Add("MaxHeight", HttpContext.Current.Request["MaxHeight"]);
            preferences.Add("JDateReligion", HttpContext.Current.Request["JDateReligion"]);
            preferences.Add("JDateEthnicity", HttpContext.Current.Request["JDateEthnicity"]);
            preferences.Add("SynagogueAttendance", HttpContext.Current.Request["SynagogueAttendance"]);
            preferences.Add("KeepKosher", HttpContext.Current.Request["KeepKosher"]);
            preferences.Add("MajorType", HttpContext.Current.Request["MajorType"]);
            preferences.Add("RelationshipMask", HttpContext.Current.Request["RelationshipType"]);		//RelationshipType from collegeclub.com
            preferences.Add("RelationshipStatus", HttpContext.Current.Request["RelationshipStatus"]);
            preferences.Add("AreaCode1", HttpContext.Current.Request["AreaCode1"]);
        }

    }
}