﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Member.ServiceAdapters.Interfaces;
using Matchnet.Web.Framework.Util;
using Matchnet.MembersOnline.ServiceAdapters;
using Matchnet.Session.ServiceAdapters;

namespace Matchnet.Web.Framework.Managers
{
    /// <summary>
    /// Manages Members online related processes
    /// </summary>
    public class MembersOnlineManager : AbstractManagerBaseClass
    {
         #region Singleton manager
        private static readonly MembersOnlineManager _Instance = new MembersOnlineManager();
        private MembersOnlineManager()
        {
        }
        public static MembersOnlineManager Instance
        {
            get { return _Instance; }
        }

        #endregion

        public bool IsMOLRedesign30Enabled(IMember member, Brand brand)
        {
            if (member != null)
            {
                    FeatureThrottler ft = new FeatureThrottler(ThrottleMode.LastDigit, "MEMBERS_ONLINE_30_REDESIGN",
                        brand, member);

                    return ft.IsFeatureEnabled();
            }

            return false;
        }

        public bool IsConsolidatedWebChatEnabled(Brand brand)
        {
            if (brand != null)
            {
                return SettingsManager.GetSettingBool("USE_MOL_CONSOLIDATION_UI", brand, false);
            }
            else
            {
                return false;
            }
        }

        public bool IsMOLRefresherEnabled(Brand brand)
        {
            if (brand != null)
            {
                return SettingsManager.GetSettingBool("USE_MOL_REFRESHER", brand, false);
            }
            else
            {
                return false;
            }
        }

        public bool IsConsolidatedMOLCountEnabled(Brand brand)
        {
            if (brand != null)
            {
                return SettingsManager.GetSettingBool("USE_MOL_CONSOLIDATION_COUNT", brand, false);
            }
            else
            {
                return false;
            }
        }

        public bool IsE2MOLEnabled(Brand brand)
        {
            if (brand != null)
            {
                return SettingsManager.GetSettingBool("USE_MOL_E2", brand, false);
            }
            else
            {
                return false;
            }
        }

        public int GetMOLCommunityCount(Brand brand, bool ignoreCache)
        {
            if (IsConsolidatedMOLCountEnabled(brand))
            {
                return MembersOnlineSA.Instance.GetMOLCount(brand.Site.Community.CommunityID, ignoreCache);
            }
            else
            {
                return SessionSA.Instance.GetSessionCount(brand.Site.Community.CommunityID);
            }
        }
    }
}