﻿using Matchnet.Content.ValueObjects.BrandConfig;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Matchnet.Web.Framework.Managers
{
    public class RenewalManager : AbstractManagerBaseClass
    {
        #region Singleton manager
        private static readonly RenewalManager _Instance = new RenewalManager();
        private RenewalManager()
        {
        }
        public static RenewalManager Instance
        {
            get { return _Instance; }
        }

        #endregion

        /// <summary>
        /// This gets the user's renewal subscription from UPS, but stores it as part of the HttpContext so we can re-use it during a single web request processing
        /// Caching not yet implemented until we have a way to trigger cache expiration
        /// </summary>
        /// <param name="memberID"></param>
        /// <param name="brand"></param>
        /// <returns></returns>
        public Spark.Common.RenewalService.RenewalSubscription GetRenewalSubscription(int memberID, Brand brand)
        {
            Spark.Common.RenewalService.RenewalSubscription renewalSub = null;
            string cacheKey = GetRenewalSubscriptionCacheKey(memberID, brand.Site.SiteID);

            if (HttpContext.Current.Items[cacheKey] != null)
            {
                renewalSub = HttpContext.Current.Items[cacheKey] as Spark.Common.RenewalService.RenewalSubscription;
            }

            if (renewalSub == null)
            {
                renewalSub = Spark.Common.Adapter.RenewalServiceWebAdapter.GetProxyInstanceForBedrock().GetCurrentRenewalSubscription(memberID, brand.Site.SiteID);
                if (renewalSub != null)
                {
                    HttpContext.Current.Items[cacheKey] = renewalSub;
                }
            }

            return renewalSub;
        }

        public string GetRenewalSubscriptionCacheKey(int memberID, int siteID)
        {
            return string.Format("UPSRenewalSubscription_{0}_{1}", memberID, siteID);
        }
    }
}