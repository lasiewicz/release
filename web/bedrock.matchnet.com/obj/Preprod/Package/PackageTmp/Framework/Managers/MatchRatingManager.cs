﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.Search.ValueObjects;
using Matchnet.Web.Interfaces;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Web.Framework.Util;
using Matchnet.Security;
using Matchnet.Search.Interfaces;
using Matchnet.Search.ServiceAdapters;

namespace Matchnet.Web.Framework.Managers
{
    public class MatchRatingManager : AbstractManagerBaseClass
    {
        private const string MATCHRATINGENCRYPTKEY = "666ecfa4";

        #region Singleton manager
        private static readonly MatchRatingManager _Instance = new MatchRatingManager();
        private MatchRatingManager()
        {
        }
        public static MatchRatingManager Instance
        {
            get { return _Instance; }
        }

        #endregion

        #region Public Methods
        public bool IsSearchSortByMutualMatchEnabled(int brandID, int siteID, int communityID)
        {
            return SettingsManager.GetSettingBool("ENABLE_MUTUALMATCH_SORT", communityID, siteID, brandID);
        }

        public bool IsMutualMatchPOCEnabled(int brandID, int siteID, int communityID)
        {
            return SettingsManager.GetSettingBool("ENABLE_MUTUAL_MATCH_POC", communityID, siteID, brandID);
        }

        public bool IsMatchRatingCalculationEnabled(int memberID, int communityID)
        {
            bool isEnabled = false;
            if (memberID > 0)
            {
                if (SettingsManager.GetSettingBool("ENABLE_MATCH_RATING_CALCULATION", communityID, Constants.NULL_INT, Constants.NULL_INT))
                {
                    isEnabled = true;
                }
            }

            return isEnabled;
        }

        public bool DisplayMatchRatingOnMatches(int memberID, Brand brand)
        {
            bool isEnabled = false;

            if (IsMatchRatingCalculationEnabled(memberID, brand.Site.Community.CommunityID))
            {
                if (memberID > 0)
                {
                    isEnabled = SettingsManager.GetSettingBool("DISPLAY_MATCH_RATING_ON_MATCHES", brand);
                }
            }

            return isEnabled;
        }

        public bool DisplayMatchRatingOnProfile(int memberID, Brand brand)
        {
            bool isEnabled = false;

            if (IsMatchRatingCalculationEnabled(memberID, brand.Site.Community.CommunityID))
            {
                if (memberID > 0)
                {
                    isEnabled = SettingsManager.GetSettingBool("DISPLAY_MATCH_RATING_ON_PROFILE", brand);
                }
            }

            return isEnabled;
        }

        public bool DisplayMatchRatingOnFilmstrip(int memberID, Brand brand)
        {
            bool isEnabled = false;

            if (IsMatchRatingCalculationEnabled(memberID, brand.Site.Community.CommunityID))
            {
                if (memberID > 0)
                {
                    isEnabled = SettingsManager.GetSettingBool("DISPLAY_MATCH_RATING_ON_FILMSTRIP", brand);
                }
            }

            return isEnabled;
        }

        public bool IsSliderWeightingEnabled(int brandID, int siteID, int communityID)
        {
            return false;
        }

        public string EncryptRating(int rating, bool urlEncode)
        {
            string encrypted = Crypto.Encrypt(MATCHRATINGENCRYPTKEY, rating.ToString());

            if (urlEncode)
                return HttpContext.Current.Server.UrlEncode(encrypted);
            else
                return encrypted;
        }

        public int DecryptRating(string encryptedRating, bool urlEncoded)
        {
            int rating = Constants.NULL_INT;
            if (urlEncoded)
                int.TryParse(Crypto.Decrypt(MATCHRATINGENCRYPTKEY, HttpContext.Current.Server.UrlDecode(encryptedRating)), out rating);
            else
                int.TryParse(Crypto.Decrypt(MATCHRATINGENCRYPTKEY, encryptedRating), out rating);

            return rating;
        }

        public MatchRatingResult CalculateMatchPercentage(int memberID1, int memberID2, int communityID, int siteID, SearchType searchType)
        {
            return MatchRatingUtil.CalculateMatchPercentage(memberID1, memberID2, communityID, siteID, searchType);
        }

        public bool IsMatchRatingCaptureMismatchDataOnProfileEnabled(int communityID)
        {
            return MatchRatingUtil.IsMatchRatingCaptureMismatchDataOnProfileEnabled(communityID);
        }

        #endregion
    }
}