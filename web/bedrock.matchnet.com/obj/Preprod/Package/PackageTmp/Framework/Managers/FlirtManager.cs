﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Member.ValueObjects;
using Matchnet.Web.Framework;
using Matchnet.Member.ServiceAdapters;

using Matchnet.Email.ServiceAdapters;
using Matchnet.Email.ValueObjects;
using Matchnet.ExternalMail.ServiceAdapters;
using Matchnet.List.ServiceAdapters;
using Matchnet.List.ValueObjects;
using Matchnet.Session.ValueObjects;
using Matchnet.UserNotifications.ServiceAdapters;
using Matchnet.UserNotifications.ValueObjects;
using Matchnet.Web.Applications.UserNotifications;
using Matchnet.Content.ValueObjects.Quotas;
using Matchnet.Search.ValueObjects;
using Matchnet.Search.ServiceAdapters;
using Matchnet.Web.Framework.Util;

namespace Matchnet.Web.Framework.Managers
{
    public class FlirtManager
    {
        private ContextGlobal _g;

        public static int NUMBER_OF_MATCHES_TO_DISPLAY = 5;
        public const string PUSH_FLIRTS_THROTTLE_SETTING_FEMALE = "SHOW_PUSH_FLIRTS_THROTTLE_SETTING";
        public const string PUSH_FLIRTS_THROTTLE_SETTING_MALE = "SHOW_PUSH_FLIRTS_THROTTLE_SETTING_MALE";

        //non-existant flirts
        public static int NO_PHOTO_FLIRTID = -10000;

        public FlirtManager(ContextGlobal g)
        {
            _g = g;
        }

        public bool EnoughResultsToShowPushFlirtsToMember()
        {
            bool show = true;

            MatchnetQueryResults searchResults = GetPushFlirtMatches();

            if (searchResults == null || searchResults.Items.Count < NUMBER_OF_MATCHES_TO_DISPLAY)
            {
                show = false;
            }

            return show;
        }

        public bool ShowPushFlirtsToMember(PushFlirtViewMode viewMode)
        {
            bool show = PushFlirtsEnabled();

            if (show)
            {
                if (viewMode == PushFlirtViewMode.ForcedOverlay)
                {
                    //we only need to look at the session if the view mode is overlay
                    string sessiontimes =
                        _g.Session.GetString(WebConstants.SESSION_PROPERTY_PUSH_FLIRTS_OVERLAY_SHOWN_COUNT);

                    if (!String.IsNullOrEmpty(sessiontimes))
                    {
                        int timesalreadyshown = Conversion.CInt(sessiontimes, 0);
                        if (timesalreadyshown > 0)
                        {
                            // hack for dev
                            if (!string.IsNullOrEmpty(HttpContext.Current.Request["ShowPushFlirt"]))
                            {
                                show = true;
                            }
                            else
                            {
                                //if we've already show this once, don't show it again.
                                show = false;
                            }
                        }
                    }
                }

                if (show && !_g.Member.HasApprovedPhoto(_g.Brand.Site.Community.CommunityID))
                {
                    show = false;
                }

                int daysForSubExpirationFactor =
                    Convert.ToInt32(RuntimeSettings.GetSetting("SHOW_PUSH_FLIRTS_BY_SUBEXPIRATIONDATE_FACTOR",
                                                               _g.Brand.Site.Community.CommunityID, _g.Brand.Site.SiteID,
                                                               _g.Brand.BrandID));
                int daysForRegistrationFactor =
                    Convert.ToInt32(RuntimeSettings.GetSetting("SHOW_PUSH_FLIRTS_BY_REGDATE_FACTOR",
                                                               _g.Brand.Site.Community.CommunityID, _g.Brand.Site.SiteID,
                                                               _g.Brand.BrandID));
                bool showBySubExpiration =
                    Convert.ToBoolean(RuntimeSettings.GetSetting("SHOW_PUSH_FLIRTS_BY_SUBEXPIRATIONDATE",
                                                                 _g.Brand.Site.Community.CommunityID,
                                                                 _g.Brand.Site.SiteID, _g.Brand.BrandID));
                bool showByRegistration =
                    Convert.ToBoolean(RuntimeSettings.GetSetting("SHOW_PUSH_FLIRTS_BY_REGDATE",
                                                                 _g.Brand.Site.Community.CommunityID,
                                                                 _g.Brand.Site.SiteID, _g.Brand.BrandID));

                if (show && showBySubExpiration)
                {
                    if (_g.Member.IsPayingMember(_g.Brand.Site.SiteID))
                    {
                        DateTime subExpirationDate = _g.Member.GetAttributeDate(_g.Brand, "SubscriptionExpirationDate");
                        if (subExpirationDate != DateTime.MinValue && subExpirationDate >= DateTime.Now &&
                            subExpirationDate <= DateTime.Now.AddDays(daysForSubExpirationFactor))
                        {
                            show = true;
                        }
                        else
                        {
                            show = false;
                        }
                    }
                }

                if (show && showByRegistration)
                {
                    DateTime registrationDate = _g.Member.GetAttributeDate(_g.Brand, "BrandInsertDate");
                    if (registrationDate >= DateTime.Now.AddDays(-daysForRegistrationFactor))
                    {
                        show = true;
                    }
                    else
                    {
                        show = false;
                    }
                }

                if (show && viewMode == PushFlirtViewMode.ForcedOverlay)
                {
                    //we only care about the number of results if this is the overlay
                    if (!EnoughResultsToShowPushFlirtsToMember())
                    {
                        show = false;
                    }
                }
            }

            return show;
        }

        private bool PushFlirtsEnabled()
        {
            bool pushFlirtsSettingEnabled = Convert.ToBoolean(RuntimeSettings.GetSetting("ENABLE_PUSH_FLIRTS", _g.Brand.Site.Community.CommunityID, _g.Brand.Site.SiteID, _g.Brand.BrandID));

            return pushFlirtsSettingEnabled && ShowPushFlirts();
        }

        private bool ShowPushFlirts()
        {
            //Throttle setting determines who can see the feature
            // -1 - No member
            // 0 - Even member id
            // 1 - Odd member id
            // 2 - All members

            ThrottleMode throttleMode = ThrottleMode.OddEven;
            try
            {
                throttleMode = (ThrottleMode)Enum.Parse(typeof(ThrottleMode),
                    RuntimeSettings.GetSetting("PUSH_FLIRTS_THROTTLE_MODE",
                   _g.Brand.Site.Community.CommunityID,
                   _g.Brand.Site.SiteID, _g.Brand.BrandID));
            }
            catch (Exception ex)
            {
                _g.ProcessException(ex);
            }

            //Show for all females
            var throttler = new FeatureThrottler(throttleMode, PUSH_FLIRTS_THROTTLE_SETTING_FEMALE, _g.Brand, _g.Member);
            if (!FrameworkGlobals.IsMaleGender(_g.Member, _g) && throttler.IsFeatureEnabled())
                return true;
            //Show for men with odd MemberId
            throttler = new FeatureThrottler(throttleMode, PUSH_FLIRTS_THROTTLE_SETTING_MALE, _g.Brand, _g.Member);
            if (FrameworkGlobals.IsMaleGender(_g.Member, _g) && throttler.IsFeatureEnabled())
                return true;
            return false;
        }

        public void IncrementPushFlirtsShownCount()
        {
            _g.UpdateSessionInt(WebConstants.SESSION_PROPERTY_PUSH_FLIRTS_OVERLAY_SHOWN_COUNT, 1, 0);
        }

        public FlirtOperationResult SendPushFlirt(int targetMemberID)
        {
            int categoryID = Convert.ToInt32(RuntimeSettings.GetSetting("PUSH_FLIRTS_TEASE_CATEGORYID", _g.Brand.Site.Community.CommunityID, _g.Brand.Site.SiteID, _g.Brand.BrandID));
            int teaseID = Convert.ToInt32(RuntimeSettings.GetSetting("PUSH_FLIRTS_TEASE_TEASEID", _g.Brand.Site.Community.CommunityID, _g.Brand.Site.SiteID, _g.Brand.BrandID));
            string teaseHeader = _g.GetResource(_g.Brand, "TXT_TEASE");
            string teaseResourceKey = RuntimeSettings.GetSetting("PUSH_FLIRTS_TEASE_RESOURCE_KEY",
                                                                 _g.Brand.Site.Community.CommunityID,
                                                                 _g.Brand.Site.SiteID, _g.Brand.BrandID);
            string teaseMessage = _g.GetResource(_g.Brand, teaseResourceKey);

            int teasesLeft = 0;
            return SendFlirt(categoryID, teaseID, teaseHeader, teaseMessage, targetMemberID, out teasesLeft);
        }


        public MatchnetQueryResults GetPushFlirtMatches()
        {
            try
            {
                SearchPreferenceCollection prefs = SearchPreferencesSA.Instance.GetSearchPreferences(_g.Member.MemberID, _g.Brand.Site.Community.CommunityID);
                return MemberSearchSA.Instance.SearchForPushFlirtMatches(prefs, _g.Member.MemberID, _g.Brand.Site.Community.CommunityID,
                    _g.Brand.Site.SiteID, _g.Brand.BrandID, NUMBER_OF_MATCHES_TO_DISPLAY);
            }
            catch (Exception ex)
            {
                //Search service throws 'Lucene.NET dll not found' exception while showing pushflirts popup on homepage for member 119237352.
                //Wrapping the call in try catch so that it doesn't blow up the home page.
                System.Diagnostics.EventLog.WriteEntry("WWW", ex.Message + ex.StackTrace,
                                                       System.Diagnostics.EventLogEntryType.Error);
                return null;
            }

        }

        public void AddViewedPushFlirtMatchesToHotList(ArrayList memberMatches)
        {
            foreach (Matchnet.Member.ServiceAdapters.Member member in memberMatches)
            {
                ListSA.Instance.AddListMember(HotListCategory.MembersYouViewedInPushFlirts,
                _g.Brand.Site.Community.CommunityID, _g.Brand.Site.SiteID, _g.Member.MemberID, member.MemberID,
                              string.Empty, Matchnet.Constants.NULL_INT, false, false, false);
            }
        }

        //a return value of 1 indicates flirt can be sent
        public int CanFlirtBeSent(int sendingMemberId, int targetMemberId, Brand brand)
        {
            var targetMember = MemberSA.Instance.GetMember(targetMemberId, MemberLoadFlags.None);

            if (targetMember == null)
            {
                return Convert.ToInt32(FlirtOperationResult.MemberNotFound);
            }

            List.ServiceAdapters.List targetList = ListSA.Instance.GetList(targetMemberId);
            var isHotListed = targetList.IsHotListed(HotListCategory.IgnoreList, brand.Site.Community.CommunityID, sendingMemberId);

            if (isHotListed)
            {
                return Convert.ToInt32(FlirtOperationResult.YouAreBlocked);
            }

            var youAlreadyTeased = _g.List.IsHotListed(HotListType.Tease, HotListDirection.OnYourList, brand.Site.Community.CommunityID, targetMemberId);
            var alreadyTeasedYou = targetList.IsHotListed(HotListType.Tease, HotListDirection.OnTheirList, brand.Site.Community.CommunityID, sendingMemberId);

            if (youAlreadyTeased || alreadyTeasedYou)
            {
                return Convert.ToInt32(FlirtOperationResult.FlirtSentAlready);
            }

            return 1;
        }

        /// <summary>
        /// This is an automatic no photo request message (ask me for my photo) sent as a flirt for non-subscribers
        /// The flirtID doesn't exist and is not available in the regular flirt page
        /// </summary>
        /// <param name="targetMemberID"></param>
        /// <returns></returns>
        public FlirtOperationResult SendNoPhotoRequestMessageAsFlirt(int targetMemberID)
        {
            //verify this request has not been sent to this member before; limit to once per target member
            bool allowed = true;
            string memberList = _g.Member.GetAttributeText(_g.Brand, "NoPhotoRequestMemberList", "");
            if (!string.IsNullOrEmpty(memberList))
            {
                string[] memberListArray = memberList.Split(new char[] { ',' });
                foreach (string s in memberListArray)
                {
                    if (s == targetMemberID.ToString())
                    {
                        allowed = false;
                        return FlirtOperationResult.FlirtSentAlready;
                    }
                }
            }

            if (allowed)
            {
                string teaseHeader = _g.GetResource("TXT_NO_PHOTO_FLIRT_SUBJECT");
                string teaseMessage = _g.GetResource("TXT_NO_PHOTO_FLIRT_MESSAGE");

                int teasesLeft = 0;
                FlirtOperationResult operationResult = SendFlirt(Constants.NULL_INT, NO_PHOTO_FLIRTID, teaseHeader, teaseMessage, targetMemberID, out teasesLeft);
                if (operationResult == FlirtOperationResult.FlirtSent)
                {
                    //update list of members sent
                    if (string.IsNullOrEmpty(memberList))
                    {
                        memberList = targetMemberID.ToString();
                    }
                    else
                    {
                        memberList = memberList + "," + targetMemberID.ToString();
                    }

                    _g.Member.SetAttributeText(_g.Brand, "NoPhotoRequestMemberList", memberList, Matchnet.Member.ValueObjects.TextStatusType.Auto);
                    MemberSA.Instance.SaveMember(_g.Member);
                }

                return operationResult;
            }
            else
            {
                return FlirtOperationResult.FlirtSentAlready;
            }
        }

        public FlirtOperationResult SendFlirt(int categoryID, int teaseID, string teaseHeader, string teaseMessage, int targetMemberID, out int teasesLeft)
        {
            MessageSave messageSave = null;
            ListSaveResult hotListSaveResult = null;
            teasesLeft = 0;

            if (_g.Member.MemberID != 0 && _g.Member.MemberID != int.MinValue && targetMemberID != Constants.NULL_INT)
            {
                // check Tease quota
                int teaseMaxCount;
                int quotaCounter;
                if (QuotaSA.Instance.IsTeaseAtQuota(_g.Brand.Site.Community.CommunityID, _g.Brand.Site.SiteID, _g.Member.MemberID, QuotaType.Tease, out quotaCounter, out teaseMaxCount))
                {
                    // at max quota here
                    return FlirtOperationResult.AlreadyUsedUpAllFreeTeases;
                }

                // let's send the actual tease and do some post processing
                int selectedTeaseID = teaseID;

                TeaseCategoryCollection categories = TeaseSA.Instance.GetTeaseCategoryCollection(_g.Brand.Site.SiteID);
                TeaseCategory selectedCategory = null;

                TeaseCollection teases = TeaseSA.Instance.GetTeaseCollection(categoryID, _g.Brand.Site.SiteID);
                Matchnet.Email.ValueObjects.Tease selectedTease = null;

                bool isNonExistantFlirtID = false;
                if (teaseID == NO_PHOTO_FLIRTID)
                {
                    isNonExistantFlirtID = true;
                }

                if (!isNonExistantFlirtID)
                {
                    foreach (TeaseCategory category in categories)
                    {
                        if (category.TeaseCategoryID == categoryID)
                        {
                            selectedCategory = category;
                            break;
                        }
                    }

                    foreach (Matchnet.Email.ValueObjects.Tease tease in teases)
                    {
                        if (tease.TeaseID == selectedTeaseID)
                        {
                            selectedTease = tease;
                            break;
                        }
                    }
                }

                if (selectedCategory == null || string.IsNullOrEmpty(selectedCategory.PromoResourceKey))
                {
                    messageSave = new MessageSave(_g.Member.MemberID,
                        targetMemberID,
                       _g.Brand.Site.Community.CommunityID,
                        _g.Brand.Site.SiteID,
                        MailType.Tease,
                        teaseHeader,
                        teaseMessage);
                }
                else
                {
                    messageSave = new MessageSave(_g.Member.MemberID,
                   targetMemberID,
                   _g.Brand.Site.Community.CommunityID,
                   _g.Brand.Site.SiteID,
                   MailType.Tease,
                   teaseHeader,
                   teaseMessage);
                }

                // _control was copied from Compose.ascx.cs to see whether to save copy of the flirt
                // ??? _control needs to verified with product management per Josh Viney
                bool saveCopy = false;
                AttributeOptionMailboxPreference mailboxMask = (AttributeOptionMailboxPreference)_g.Member.GetAttributeInt(_g.Brand, WebConstants.ATTRIBUTE_NAME_MAILBOXPREFERENCE);
                if ((mailboxMask & AttributeOptionMailboxPreference.SaveCopiesOfSentMessages) == AttributeOptionMailboxPreference.SaveCopiesOfSentMessages)
                {
                    saveCopy = true;
                }

                MessageSendResult messageSendResult = EmailMessageSA.Instance.SendMessage(messageSave, saveCopy, Constants.NULL_INT, false);
                bool result = (messageSendResult.Status == MessageSendStatus.Success);

                if (result)
                {
                    bool isFraudHold = (messageSendResult.MailOptions & MailOption.FraudHold) == MailOption.FraudHold;
                    if (isFraudHold)
                    {
                        // just update the quota and do nothing else
                        QuotaSA.Instance.AddQuotaItem(_g.Brand.Site.Community.CommunityID, _g.Member.MemberID, QuotaType.Tease);
                    }
                    else
                    {
                        // send out various notifications
                        if (teaseID == NO_PHOTO_FLIRTID)
                        {
                            //no photo flirt should not be added to tease hotlist because the that list is used
                            //to restrict users from only sending one flirt per member and we want the no photo flirt to be independent; 
                            //i.e. if you've already sent a flirt, you can still send a no photo flirt
                            //for now we will add it to your emailed hotlist
                            ListSA.Instance.AddListMember(HotListCategory.MembersYouEmailed,
                                    _g.Brand.Site.Community.CommunityID,
                                    _g.Brand.Site.SiteID,
                                    _g.Member.MemberID,
                                    targetMemberID,
                                    null,
                                    Constants.NULL_INT,
                                    true,
                                    false);
                        }
                        else
                        {
                            ListSA.Instance.AddListMember(HotListCategory.MembersYouTeased,
                               _g.Brand.Site.Community.CommunityID,
                               _g.Brand.Site.SiteID,
                               _g.Member.MemberID,
                               targetMemberID,
                               null,
                               teaseID,
                               true,
                               false);
                        }

                        if (UserNotificationFactory.IsUserNotificationsEnabled(_g))
                        {
                            var additionalParams = new Hashtable
                                                       {
                                                           {"messageid",messageSendResult.RecipientMessageListID.ToString() },
                                                           {"messagefolderid", SystemFolders.Inbox.ToString("d")}
                                                       };

                            UserNotificationParams unParams = UserNotificationParams.GetParamsObject(targetMemberID, _g.Brand.Site.SiteID, _g.Brand.Site.Community.CommunityID);
                            UserNotification notification = UserNotificationFactory.Instance.GetUserNotification(new FlirtedYouUserNotification(), targetMemberID, _g.Member.MemberID, _g, additionalParams);
                            if (notification.IsActivated())
                            {
                                UserNotificationsServiceSA.Instance.AddUserNotification(unParams, notification.CreateViewObject());
                            }
                        }

                        ExternalMailSA.Instance.SendInternalMailNotification(messageSendResult.EmailMessage, _g.Brand.BrandID, messageSendResult.RecipientUnreadCount);
                    }

                    teasesLeft = teaseMaxCount - (quotaCounter + 1);

                    return FlirtOperationResult.FlirtSent;
                }
                else
                {
                    // sending of the message failed for some reason
                    return FlirtOperationResult.OperationFailed;
                }

            }
            else
            {
                return FlirtOperationResult.MemberNotFound;
            }

            return FlirtOperationResult.OperationFailed;
        }
    }

    public enum PushFlirtConfirmationMode
    {

        None = 0,
        Successfull = 1,
        QuotaReached = 2,
        NotEnoughResults = 3
    }

    public enum PushFlirtViewMode
    {
        ForcedOverlay = 0,
        Link = 1
    }

    public enum FlirtOperationResult
    {
        OperationFailed = 0,
        FlirtSent = 1,
        TooManyFlirts = 2,
        FlirtSentAlready = 4,
        MemberNotFound = 8,
        YouAreBlocked = 16,
        AlreadyUsedUpAllFreeTeases = 32,
        SessionExpired = 64
    }
}