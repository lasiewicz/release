﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.ExternalMail.ServiceAdapters;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ServiceAdapters.Interfaces;
using Matchnet.Member.ValueObjects;
using Matchnet.Web.Interfaces;

namespace Matchnet.Web.Framework.Managers
{
    public class MemberPasswordManager : AbstractManagerBaseClass, IMemberPasswordManager
    {
        private static readonly MemberPasswordManager _instance = new MemberPasswordManager();
        private IGetMember _getMember;
        private ISaveMember _saveMember;
        private IExternalMailSA _externalMail;

        private MemberPasswordManager()
        {
        }

        public static MemberPasswordManager Instance
        {
            get { return _instance; }
        }

        public IGetMember GetMember
        {
            get { return _getMember ?? (_getMember = MemberSA.Instance); }
            set { _getMember = value; }
        }

        public ISaveMember SaveMember
        {
            get { return _saveMember ?? (_saveMember = MemberSA.Instance); }
            set { _saveMember = value; }
        }

        public IExternalMailSA ExternalMail
        {
            get { return _externalMail ?? (_externalMail = ExternalMailSA.Instance); }
            set { _externalMail = value; }
        }

        /// <summary>
        /// Creates a reset password token and send out an email to the member with the link to reset the password.
        /// </summary>
        /// <param name="emailAddress">Email address of the member</param>
        /// <returns>False if email is not found</returns>
        public bool ResetPassword(int brandId, string emailAddress)
        {
            var memberId = GetMember.GetMemberIDByEmail(emailAddress);
            if (memberId == Constants.NULL_INT)
                return false;

            // generate the token
            string tokenGuid = SaveMember.ResetPassword(memberId);
            // send off an externalmail with this value
            string resetUrl = string.Format("http://{0}/applications/memberservices/forgotpasswordsetpassword.aspx?resettoken={1}&mid={2}",
                                       HttpContext.Current.Request.Url.Host, HttpUtility.UrlEncode(tokenGuid), memberId.ToString());
            var success = ExternalMail.ResetPassword(brandId, memberId, resetUrl);

            this.LoggingManager.LogInfoMessage("MemberPasswordManager",
                                      string.Format(
                                          "Reset password request completed with BrandId:{0}, MemberId:{1}, ResetURL:{2}, Call to ExternalMail success?:{3}",
                                          brandId.ToString(),
                                          memberId.ToString(), resetUrl, success.ToString()));

            return success;
        }

        /// <summary>
        /// Checks to see if the token value is still active. If so the memberID is returned.
        /// </summary>
        /// <param name="tokenValue">URL decoded token value</param>
        /// <returns>MemberID if the token is valid</returns>
        public int ValidateResetPasswordToken(string tokenGuidString)
        {
            return GetMember.ValidateResetPasswordToken(tokenGuidString);
        }

        /// <summary>
        /// Changes the member's password and sends the changed profile external mail to the member. This method doesn't validate the password,
        /// so make sure the password is valid before calling this.
        /// </summary>
        /// <param name="member"></param>
        /// <param name="brand"></param>
        /// <param name="password"></param>
        /// <param name="changedFieldPassword">Resource to used to identify password field has been changed.</param>
        /// <returns></returns>
        public bool ChangePassword(Matchnet.Member.ServiceAdapters.Member member, Brand brand, string password, string changedFieldPassword)
        {
            member.Password = password;
            var result = SaveMember.SaveMember(member, brand.Site.Community.CommunityID);

            if(result.SaveStatus == MemberSaveStatusType.Success)
            {
                var changedFields = new List<string>();
                changedFields.Add(changedFieldPassword);
                ExternalMail.SendProfileChangedConfirmationEmail(member.MemberID,
                    brand.BrandID, member.EmailAddress, member.GetAttributeText(brand, "sitefirstname"), member.GetUserName(brand), changedFields);
                return true;
            }

            return false;
        }

        /// <summary>
        /// Our current password strength requirement is pretty weak. Only between 4 to 16 characters. I'm trying to centralize
        /// the logic here so when we make the requirements stronger, we can do it here.
        /// </summary>
        /// <param name="password">Password to test for strength</param>
        /// <returns>True if it meets our current standard for strength of password</returns>
        public bool ValidatePasswordStrength(string password)
        {
            if (string.IsNullOrEmpty(password))
                return false;

            return (password.Any(char.IsDigit) && password.Any(char.IsLetter) && 
                password.Length <= 16 && password.Length >= 8);
        }
    }
}