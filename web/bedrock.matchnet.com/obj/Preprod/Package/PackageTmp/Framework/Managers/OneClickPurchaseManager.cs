﻿using System;
using System.Collections.Generic;
using System.Data.Linq.SqlClient;
using System.Diagnostics;
using System.Runtime.Serialization;
using System.Web;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ServiceAdapters.Links;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Content.ValueObjects.Region;
using Matchnet.Lib;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ServiceAdapters.Interfaces;
using Matchnet.Purchase.ServiceAdapters;
using Matchnet.Web.Applications.Subscription.UnifiedPaymentSystem;
using Matchnet.Web.Framework.Util;
using Matchnet.Web.Interfaces;
using Newtonsoft.Json;
using Spark.Common.AccessService;
using Spark.Common.Adapter;
using Spark.Common.PaymentProfileService;
using Spark.Common.PurchaseService;
using Spark.Common.RenewalService;
using Spark.Common.UPS;
using Item = Spark.Common.CatalogService.Item;
using MPVO = Matchnet.Purchase.ValueObjects;
using Package = Spark.Common.CatalogService.Package;
using PackageType = Spark.Common.CatalogService.PackageType;
using PrivilegeType = Spark.Common.AccessService.PrivilegeType;

namespace Matchnet.Web.Framework.Managers
{
    public class OneClickPurchaseManager : AbstractManagerBaseClass, IOneClickPurchaseManager
    {
        public bool IsOneClickAllAccessEnabled(Brand brand, IMember member)
        {
            if (member != null)
            {
                var ft = new FeatureThrottler(ThrottleMode.LastDigit, "ALL_ACCESS_ONE_CLICK_PURCHASE",
                    brand, member);

                return ft.IsFeatureEnabled();
            }

            return false;
        }

        public bool MemberHaveValidOneClickPaymentProfile(int memberID, int callingSystemID)
        {
            bool memberHaveValidOneClickPaymentProfile = false;

            try
            {
                if (!string.IsNullOrEmpty(
                    PaymentProfileServiceWebAdapter.GetProxyInstanceForBedrock()
                        .GetMemberOneClickPaymentProfileID(memberID, callingSystemID)))
                    memberHaveValidOneClickPaymentProfile = true;
            }
            finally
            {
                PaymentProfileServiceWebAdapter.CloseProxyInstance();
            }

            return memberHaveValidOneClickPaymentProfile;
        }

        public purchaseResponse DoOneClickPurchase(ContextGlobal g, int prtid,
            string oneClickPackages)
        {
            int customerID = g.Member.MemberID;
            int callingSystemID = g.Brand.Site.SiteID;
            int callingSystemTypeID = 1;
            DateTime requestedPurchaseDate = DateTime.Now;
            int regionID = g.Member.GetAttributeInt(g.Brand, "RegionID");
            string orderAttributes = string.Empty;
            string transactionType = "AdditionalSubscriptionPurchase";
            var settingsManager = new SettingsManager();
            bool isSubscriptionConfirmationEmailEnabled =
                settingsManager.GetSettingBool(SettingConstants.ENABLE_SUBSCRIPTION_CONFIRMATION_EMAIL, g.Brand);

            var OrderAttributes = new Dictionary<string, string>();
            OrderAttributes.Add("PRTID", prtid.ToString());

            foreach (string key in OrderAttributes.Keys)
            {
                if (OrderAttributes[key] != string.Empty)
                    orderAttributes += key + "=" + OrderAttributes[key] + ",";
            }

            if (orderAttributes.Length > 0)
                orderAttributes = orderAttributes.TrimEnd(',');

            string purchaseResponse = string.Empty;
            try
            {
                purchaseResponse = PurchaseServiceWebAdapter.GetProxyInstanceForBedrock()
                    .DoOneClickPurchase(oneClickPackages, customerID, callingSystemID, callingSystemTypeID,
                        requestedPurchaseDate, regionID, orderAttributes, transactionType,
                        isSubscriptionConfirmationEmailEnabled);
            }
            finally
            {
                PurchaseServiceWebAdapter.CloseProxyInstance();
            }

            return JsonConvert.DeserializeObject<purchaseResponse>(purchaseResponse);
        }

        public string GetCreditCardTypeText(int orderID, int CustomerID, int CallingSystemID, ContextGlobal g)
        {
            ObsfucatedPaymentProfileResponse paymentProfileResponse =
                PaymentProfileServiceWebAdapter.GetProxyInstance()
                    .GetObsfucatedPaymentProfileByOrderID(orderID, CustomerID, CallingSystemID);
            var creditCardPaymentProfile =
                paymentProfileResponse.PaymentProfile as ObsfucatedCreditCardPaymentProfile;
            string creditCardType = creditCardPaymentProfile.CardType;
            try
            {
                MPVO.CreditCardCollection ccc =
                    PurchaseSA.Instance.GetCreditCardTypes(g.TargetBrand.Site.SiteID);

                if (ccc != null)
                {
                    MPVO.CreditCardType cardType = UPSGetMatchnetCreditCardType(creditCardType);

                    if (cardType != MPVO.CreditCardType.None)
                    {
                        creditCardType = g.GetResource(ccc.FindByID(
                            Convert.ToInt32(cardType)).ResourceConstant, null);
                    }
                }
            }
            catch (Exception ex)
            {
                creditCardType = creditCardPaymentProfile.CardType;
            }
            finally
            {
                PaymentProfileServiceWebAdapter.CloseProxyInstance();
            }

            return creditCardType;
        }

        public decimal GetPackageOverrideTotalAmount(ContextGlobal g, MPVO.PremiumType premiumType)
        {
            return GetUpsalePackage(g, premiumType).PackageOverrideTotalAmount;
        }

        public string SerializeOneClickPackage(OneClickPackage package)
        {
            string serializedPackages = string.Empty;

            //try
            //{
            var oneClickPackages = new OneClickPackages();
            oneClickPackages.Packages.Add(package);
            serializedPackages = JsonConvert.SerializeObject(oneClickPackages, typeof(OneClickPackages), null);
            //}
            //catch (SerializationException serex)
            //{

            //}

            return serializedPackages;
        }

        public OneClickPackage GetUpsalePackage(ContextGlobal g, MPVO.PremiumType premiumType)
        {
            RenewalSubscription renewalSub =
                RenewalManager.Instance.GetRenewalSubscription(g.TargetMemberID, g.TargetBrand);
            if (renewalSub == null || renewalSub.RenewalSubscriptionID <= 0 ||
                (renewalSub.RenewalDatePST < DateTime.Now))
            {
                Trace.WriteLine("Cannot find RenewalSubscription for Member ID [" +
                                Convert.ToString(g.Member.MemberID) + "]");
                Trace.WriteLine(
                    "Redirect to the subscription page since the member does not have a basic subscription privilege");

                // Basic subscription privilege is required
                // If the member does not have basic subscription privilege, redirect the user to the subscription
                // page to purchase a basic subscription 
                Redirect.Subscription(g.TargetBrand
                    ,
                    (int)
                        PurchaseReasonType
                            .AttemptToAccessUpsalePackagesWithNoSubscription
                    , Constants.NULL_INT
                    , false
                    ,
                    (HttpContext.Current.Request["DestinationURL"] != null)
                        ? HttpContext.Current.Server.UrlEncode(
                            Convert.ToString(HttpContext.Current.Request["DestinationURL"]))
                        : string.Empty);
                //g.Notification.AddErrorString("Member does not have a subscription. MemberID:" + g.TargetMember.MemberID);
                //throw new Exception("Membersub cannot be null. MemberID:" + g.TargetMember.MemberID);
            }
            int activePlanID = renewalSub.PrimaryPackageID;
            // Get the details of the most recent plan that the member is on  
            Package activeSubscriptionPackage =
                CatalogServiceWebAdapter.GetProxyInstance().GetPackageDetails(activePlanID);
            CatalogServiceWebAdapter.CloseProxyInstance();
            // Get the packages allowed for upsale for this member 
            Item activeSubscriptionPackageBasicItem = null;
            //Matchnet.Web.CatalogServiceReference.Item activeSubscriptionPackageBasicItem = null;
            foreach (Item item in activeSubscriptionPackage.Items)
            {
                var itemPrivileges =
                    new List<Spark.Common.CatalogService.PrivilegeType>();
                itemPrivileges.AddRange(item.PrivilegeType);
                if (itemPrivileges.Contains(Spark.Common.CatalogService.PrivilegeType.BasicSubscription))
                {
                    // Use the basic subscription item in the package to determine the duration and duration type of the package  
                    activeSubscriptionPackageBasicItem = item;
                    break;
                }
            }
            string upsaleFromType = "Standard";
            if (activeSubscriptionPackage.PackageType == PackageType.Bundled)
            {
                upsaleFromType = "BundledWithPremiumServices";
            }
            int[] arrUpsalePackages =
                CatalogServiceWebAdapter.GetProxyInstance()
                    .GetUpsalePackages(activeSubscriptionPackageBasicItem.Duration,
                        Enum.GetName(typeof(DurationType), activeSubscriptionPackageBasicItem.DurationType),
                        g.Brand.Site.SiteID, upsaleFromType, activeSubscriptionPackageBasicItem.RenewalDuration,
                        Enum.GetName(typeof(DurationType), activeSubscriptionPackageBasicItem.RenewalDurationType));
            CatalogServiceWebAdapter.CloseProxyInstance();
            var upsalePackages = new List<int>();
            upsalePackages.AddRange(arrUpsalePackages);
            var colPlansAvailableForUpsale = new List<MPVO.Plan>();
            foreach (int planID in upsalePackages)
            {
                MPVO.Plan upsalePlan = PlanSA.Instance.GetPlan(planID, g.TargetBrand.BrandID);

                if (upsalePlan != null)
                {
                    colPlansAvailableForUpsale.Add(upsalePlan);
                }
            }

            AccessPrivilege[] accessPrivileges =
                AccessServiceWebAdapter.GetProxyInstance()
                    .GetCustomerPrivilegeListAllWithIgnoreCache(g.Member.MemberID);
            var activePrivileges =
                new List<AccessPrivilege>();
            foreach (AccessPrivilege checkAccessPrivilege in accessPrivileges)
            {
                if (checkAccessPrivilege.CallingSystemID == g.TargetBrand.Site.SiteID)
                {
                    if (checkAccessPrivilege.UnifiedPrivilegeType ==
                        PrivilegeType.BasicSubscription
                        ||
                        checkAccessPrivilege.UnifiedPrivilegeType ==
                        PrivilegeType.HighlightedProfile
                        ||
                        checkAccessPrivilege.UnifiedPrivilegeType ==
                        PrivilegeType.SpotlightMember
                        || checkAccessPrivilege.UnifiedPrivilegeType == PrivilegeType.JMeter
                        ||
                        checkAccessPrivilege.UnifiedPrivilegeType == PrivilegeType.AllAccess
                        ||
                        checkAccessPrivilege.UnifiedPrivilegeType ==
                        PrivilegeType.ReadReceipt)
                    {
                        if (checkAccessPrivilege.EndDateUTC > DateTime.Now)
                        {
                            activePrivileges.Add(checkAccessPrivilege);
                        }
                    }
                }
            }
            var allowedUpsalePlanCollection = new List<MPVO.Plan>();
            foreach (MPVO.Plan plan in colPlansAvailableForUpsale)
            {
                if ((plan.PremiumTypeMask & premiumType) == premiumType)
                {
                    PrivilegeType privilegeType = GetPrivilegeTypeFromPremiumType(premiumType);
                    if (IsAllowedToPurchaseUpsalePlan(activePrivileges,
                        privilegeType, renewalSub.RenewalDatePST))
                    {
                        allowedUpsalePlanCollection.Add(plan);
                    }
                }
            }
            var oneClickPackage = new OneClickPackage();
            // A La Carte Plans
            // Should only be two. One of each premium service.
            foreach (MPVO.Plan plan in allowedUpsalePlanCollection)
            {
                if (plan.EndDate != DateTime.MinValue)
                    continue;

                //package.ID = plan.PlanID;
                oneClickPackage.ID = plan.PlanID;
                decimal proratedAmount = GetMemberProratedAmount(plan.InitialCost, renewalSub.RenewalDatePST,
                    GetStartDateForUpsalePlan(plan, activePrivileges), g);
                oneClickPackage.PackageOverrideTotalAmount = proratedAmount;
            }

            return oneClickPackage;
        }

        public OneClickPackage GetFixedPricingUpslaePackage(ContextGlobal g)
        {
            Dictionary<string, string> colDataAttributes = new Dictionary<string, string>();


            if (g.TargetMember == null)
                throw new Exception("Member cannot null.");

            int memberID = g.TargetMember.MemberID;

            PaymentJson payment = new PaymentJson();

            // Get all the privileges that have not expired for this member for this site  
            Spark.Common.AccessService.AccessPrivilege[] accessPrivileges =
                Spark.Common.Adapter.AccessServiceWebAdapter.GetProxyInstance()
                    .GetCustomerPrivilegeListAll(memberID);
            List<Spark.Common.AccessService.AccessPrivilege> activePrivileges =
                new List<Spark.Common.AccessService.AccessPrivilege>();
            foreach (Spark.Common.AccessService.AccessPrivilege checkAccessPrivilege in accessPrivileges)
            {
                if (checkAccessPrivilege.CallingSystemID == g.TargetBrand.Site.SiteID)
                {
                    if (checkAccessPrivilege.UnifiedPrivilegeType ==
                        Spark.Common.AccessService.PrivilegeType.BasicSubscription
                        ||
                        checkAccessPrivilege.UnifiedPrivilegeType ==
                        Spark.Common.AccessService.PrivilegeType.HighlightedProfile
                        ||
                        checkAccessPrivilege.UnifiedPrivilegeType ==
                        Spark.Common.AccessService.PrivilegeType.SpotlightMember
                        ||
                        checkAccessPrivilege.UnifiedPrivilegeType == Spark.Common.AccessService.PrivilegeType.JMeter
                        ||
                        checkAccessPrivilege.UnifiedPrivilegeType ==
                        Spark.Common.AccessService.PrivilegeType.AllAccess)
                    {
                        if (checkAccessPrivilege.EndDateUTC > DateTime.Now)
                        {
                            activePrivileges.Add(checkAccessPrivilege);
                        }
                    }
                }
            }



            List<int> upsalePackages = GetFixedPricingUpsalePackage(g.TargetBrand.Site.SiteID);

            List<MPVO.Plan> allowedUpsalePlanCollection = new List<MPVO.Plan>();


            foreach (int planID in upsalePackages)
            {
                MPVO.Plan upsalePlan = PlanSA.Instance.GetPlan(planID, g.TargetBrand.BrandID);

                if (upsalePlan != null)
                {
                    // The fixed pricing upsale package just contains all access emails that do not renew
                    // But in order to purchase all access emails, the member must have all access that has not yet expired 
                    // Also check the maximum number of all access emails that the member can purchase 
                    if ((upsalePlan.PremiumTypeMask & MPVO.PremiumType.AllAccessEmail) ==
                        MPVO.PremiumType.AllAccessEmail)
                    {
                        bool isAllowedToPurchaseAllAccessEmails = false;

                        Spark.Common.AccessService.AccessPrivilege checkPrivilege =
                            activePrivileges.Find(
                                delegate(Spark.Common.AccessService.AccessPrivilege privilege)
                                {
                                    return privilege.UnifiedPrivilegeType ==
                                           Spark.Common.AccessService.PrivilegeType.AllAccess;
                                });
                        if (checkPrivilege == null)
                        {
                            // Member can purchase this privilege since he does not have this privilege yet
                            isAllowedToPurchaseAllAccessEmails = false;
                        }
                        else
                        {
                            if (checkPrivilege.EndDatePST == DateTime.MinValue
                                || checkPrivilege.EndDatePST < DateTime.Now)
                            {
                                // All access expired   
                                isAllowedToPurchaseAllAccessEmails = false;
                            }
                            else
                            {
                                isAllowedToPurchaseAllAccessEmails = true;
                            }
                        }

                        if (isAllowedToPurchaseAllAccessEmails)
                        {
                            allowedUpsalePlanCollection.Add(upsalePlan);
                        }
                    }
                }
            }

            var oneClickPackage = new OneClickPackage();
            // A La Carte Plans
            // Should only be two. One of each premium service.
            foreach (MPVO.Plan plan in allowedUpsalePlanCollection)
            {
                if (plan.EndDate != DateTime.MinValue)
                    continue;

                //package.ID = plan.PlanID;
                oneClickPackage.ID = plan.PlanID;
                oneClickPackage.PackageOverrideTotalAmount = plan.InitialCost;
            }

            return oneClickPackage;
        }

        private PrivilegeType GetPrivilegeTypeFromPremiumType(MPVO.PremiumType premiumType)
        {
            PrivilegeType privilegeType;
            switch (premiumType)
            {
                case MPVO.PremiumType.AllAccess:
                    privilegeType = PrivilegeType.AllAccess;
                    break;
                case MPVO.PremiumType.AllAccessEmail:
                    privilegeType = PrivilegeType.AllAccessEmails;
                    break;
                default:
                    privilegeType = PrivilegeType.HighlightedProfile;
                    break;
            }

            return privilegeType;
        }

        private MPVO.CreditCardType UPSGetMatchnetCreditCardType(string upsCreditCardType)
        {
            var cardType = MPVO.CreditCardType.None;
            //this is the UPS credit card type text
            switch (upsCreditCardType)
            {
                case "Visa":
                    cardType = MPVO.CreditCardType.Visa;
                    break;
                case "Mastercard":
                    cardType = MPVO.CreditCardType.MasterCard;
                    break;
                case "AmericanExpress":
                    cardType = MPVO.CreditCardType.AmericanExpress;
                    break;
                case "Discover":
                    cardType = MPVO.CreditCardType.Discover;
                    break;
                case "Switch":
                    cardType = MPVO.CreditCardType.Switch;
                    break;
                case "CarteBlanche":
                    cardType = MPVO.CreditCardType.CarteBlanche;
                    break;
                case "Solo":
                    cardType = MPVO.CreditCardType.Solo;
                    break;
                case "DirectDebit":
                    cardType = MPVO.CreditCardType.DirectDebit;
                    break;
                case "Delta":
                    cardType = MPVO.CreditCardType.Delta;
                    break;
                case "Electron":
                    cardType = MPVO.CreditCardType.Electron;
                    break;
                case "DinersClub":
                    cardType = MPVO.CreditCardType.DinersClub;
                    break;
                case "YoterClubLeumiCard":
                    cardType = MPVO.CreditCardType.YoterClubLeumiCard;
                    break;
                case "LeumiCardStudentCard":
                    cardType = MPVO.CreditCardType.LeumiCardStudentCard;
                    break;
                case "YoterClubIsraCard":
                    cardType = MPVO.CreditCardType.YoterClubIsraCard;
                    break;
                case "IsraCardCampusCardStudent":
                    cardType = MPVO.CreditCardType.IsraCardCampusCardStudent;
                    break;
                case "VisaCalSoldiersClub":
                    cardType = MPVO.CreditCardType.VisaCalSoldiersClub;
                    break;
                case "IsraCard":
                    cardType = MPVO.CreditCardType.IsraCard;
                    break;
            }

            return cardType;
        }

        private bool IsAllowedToPurchaseUpsalePlan(List<AccessPrivilege> activePrivileges,
            PrivilegeType checkPrivilegeType, DateTime renewalDate)
        {
            bool isAllowedToPurchaseUpsalePlan = false;

            AccessPrivilege checkPrivilege =
                activePrivileges.Find(
                    delegate(AccessPrivilege privilege) { return privilege.UnifiedPrivilegeType == checkPrivilegeType; });
            if (checkPrivilege == null)
            {
                // Member can purchase this privilege since he does not have this privilege yet
                isAllowedToPurchaseUpsalePlan = true;
            }
            else
            {
                if (checkPrivilege.EndDatePST == DateTime.MinValue
                    || checkPrivilege.EndDatePST < DateTime.Now)
                {
                    // Member can purchase this privilege since he had this privilege but it expired  
                    isAllowedToPurchaseUpsalePlan = true;
                }
                else
                {
                    // The start date of the premium service should be either earlier than the renewal date 
                    // or the same as the renewal date. If the start date of the premium service is past the
                    // renewal date, then set the start date of the premium service to the renewal date. 
                    if (checkPrivilege.EndDatePST > renewalDate)
                    {
                        isAllowedToPurchaseUpsalePlan = false;
                    }
                    else
                    {
                        TimeSpan dateDifference = checkPrivilege.EndDatePST.Subtract(renewalDate);
                        if (Math.Abs(dateDifference.TotalMinutes) > 20)
                        {
                            // There is still additional time that member can purchase for this privilege
                            isAllowedToPurchaseUpsalePlan = true;
                        }
                        else
                        {
                            // The end date of the privilege is the same as the end date of the basic subscription  
                            // so do not allow the member to purchase this privilege  
                            isAllowedToPurchaseUpsalePlan = false;
                        }
                    }
                }
            }

            return isAllowedToPurchaseUpsalePlan;
        }

        /// <summary>
        ///     Copied from Purchase
        /// </summary>
        /// <param name="initialCost"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        private decimal GetMemberProratedAmount(decimal initialCost, DateTime endDate, DateTime startDate,
            ContextGlobal g)
        {
            if (endDate < DateTime.Now)
            {
                g.Notification.AddErrorString("Subscription end date must be a future date. Enddate:" +
                                              endDate);

                throw new Exception("Subscription end date must be a future date. Enddate:" +
                                    endDate);
            }

            decimal amount = Constants.NULL_DECIMAL;
            decimal pricePerDay = Constants.NULL_DECIMAL;

            // The start date of the premium service should be either earlier than the renewal date 
            // or the same as the renewal date. If the start date of the premium service is past the
            // renewal date, then set the start date of the premium service to the renewal date. 
            if (startDate > endDate)
            {
                amount = 0.0m;
            }
            else
            {
                int monthsUntilRenewalDate = GetExactMonthsUntilRenewal(startDate, endDate);
                decimal subTotalFromMonths = (monthsUntilRenewalDate) * (initialCost);
                DateTime dteAfterMonthsAdded = startDate.AddMonths(monthsUntilRenewalDate);

                // Calculate cost per day
                DateTime dteEndDateOfRemainingDaysWithMonthAdded = dteAfterMonthsAdded.AddMonths(1);
                TimeSpan span = dteEndDateOfRemainingDaysWithMonthAdded.Subtract(dteAfterMonthsAdded);
                pricePerDay = (initialCost) / Convert.ToDecimal(span.Days);

                decimal subTotalFromRemainingDays = (pricePerDay) *
                                                    (Convert.ToDecimal(GetDuration(endDate, dteAfterMonthsAdded)));

                amount = subTotalFromMonths + subTotalFromRemainingDays;
            }

            if (amount < 0)
                throw new Exception("Prorated Amount cannot be less then 0.");

            Trace.WriteLine("InitialCost:" + initialCost +
                            "EndDate:" + endDate + "PricePerDay:" + pricePerDay + "Amount:" + amount);

            return amount;
        }

        private int GetExactMonthsUntilRenewal(DateTime startDate, DateTime endDate)
        {
            int monthDifference = SqlMethods.DateDiffMonth(startDate, endDate);

            if (monthDifference > 0)
            {
                // Check to see if the number of months in between these two dates should be one less
                DateTime dteMonthDifferenceAdded = startDate.AddMonths(monthDifference);

                if (endDate < dteMonthDifferenceAdded)
                {
                    monthDifference = (monthDifference - 1);
                }
            }

            return monthDifference;
        }

        private int GetDuration(DateTime endDate, DateTime startDate)
        {
            // Get remaining days left on this plan
            // Whenever there is any remaining time in a day, give that full day of remaining credit to the 
            // member.  
            // Round 2.1 to 3 days
            // Round 2.9 to 3 days
            TimeSpan span = endDate.Subtract(startDate);
            Int32 remainingDays = Convert.ToInt32(Math.Ceiling(span.TotalDays));

            Trace.WriteLine("RemainingDays:" + remainingDays);

            return remainingDays;
        }

        private DateTime GetStartDateForUpsalePlan(MPVO.Plan plan,
            List<AccessPrivilege> activePrivileges)
        {
            var checkPrivilegeType = PrivilegeType.None;

            if ((plan.PremiumTypeMask & MPVO.PremiumType.HighlightedProfile) == MPVO.PremiumType.HighlightedProfile)
            {
                checkPrivilegeType = PrivilegeType.HighlightedProfile;
            }
            else if ((plan.PremiumTypeMask & MPVO.PremiumType.SpotlightMember) == MPVO.PremiumType.SpotlightMember)
            {
                checkPrivilegeType = PrivilegeType.SpotlightMember;
            }
            else if ((plan.PremiumTypeMask & MPVO.PremiumType.JMeter) == MPVO.PremiumType.JMeter)
            {
                checkPrivilegeType = PrivilegeType.JMeter;
            }
            else if ((plan.PremiumTypeMask & MPVO.PremiumType.AllAccess) == MPVO.PremiumType.AllAccess)
            {
                checkPrivilegeType = PrivilegeType.AllAccess;
            }
            else if ((plan.PremiumTypeMask & MPVO.PremiumType.ReadReceipt) == MPVO.PremiumType.ReadReceipt)
            {
                checkPrivilegeType = PrivilegeType.ReadReceipt;
            }
            else
            {
                checkPrivilegeType = PrivilegeType.None;
            }

            AccessPrivilege checkPrivilege =
                activePrivileges.Find(
                    delegate(AccessPrivilege privilege) { return privilege.UnifiedPrivilegeType == checkPrivilegeType; });

            if (checkPrivilege == null)
            {
                return DateTime.Now.AddMinutes(60);
            }
            if (checkPrivilege.EndDatePST <= DateTime.Now)
            {
                return DateTime.Now.AddMinutes(60);
            }
            return checkPrivilege.EndDatePST;
        }

        private List<int> GetFixedPricingUpsalePackage(int siteID)
        {
            List<int> upsalePackages = new List<int>();

            if (siteID == (int)WebConstants.SITE_ID.JDate)
            {
                upsalePackages.Add(4500);
                upsalePackages.Add(4501);
            }
            else if (siteID == (int)WebConstants.SITE_ID.JDateUK)
            {
                upsalePackages.Add(4045);
                upsalePackages.Add(4046);
            }
            else if (siteID == (int)WebConstants.SITE_ID.JDateFR)
            {
                upsalePackages.Add(4050);
                upsalePackages.Add(4051);
            }
            else if (siteID == (int)WebConstants.SITE_ID.JDateCoIL)
            {
                upsalePackages.Add(4503);
            }

            return upsalePackages;
        }

        #region One click packages serializ objects

        public class OneClickPackage
        {
            private int _id = Constants.NULL_INT;
            private decimal _packageOverrideTotalAmount = Constants.NULL_DECIMAL;

            [DataMember]
            public int ID
            {
                get { return _id; }
                set { _id = value; }
            }

            [DataMember]
            public decimal PackageOverrideTotalAmount
            {
                get { return _packageOverrideTotalAmount; }
                set { _packageOverrideTotalAmount = value; }
            }
        }

        public class OneClickPackages
        {
            private List<OneClickPackage> _packages = new List<OneClickPackage>();

            public List<OneClickPackage> Packages
            {
                get { return _packages; }
                set { _packages = value; }
            }
        }

        #endregion
    }
}