﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Web;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.List.ServiceAdapters;
using Matchnet.List.ValueObjects;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ServiceAdapters.Interfaces;
using Matchnet.Member.ValueObjects.Photos;
using Matchnet.UserNotifications.ServiceAdapters;
using Matchnet.UserNotifications.ValueObjects;
using Matchnet.Web.Applications.UserNotifications;
using Matchnet.Web.Framework.Exceptions;
using Matchnet.Web.Interfaces;

namespace Matchnet.Web.Framework.Managers
{
    public enum PhotoUpdateResult
    {
        Success=0,
        Failure=1,
        TooManyPhotos=2
    }
    
    
    public class MemberPhotoUpdateManager:AbstractManagerBaseClass
    {
        
        private List<PhotoUpdate> _photoUpdates;
        private List<Byte[]> _remotePhotoUpdates;
        private List<Photo> _photos;
        private IMember _member;
        private Brand _brand;
        private ISaveMember _memberSaveService;
        private ISavePhotos _photosSaveService;
        private IPhotoRules _photoRules;
        public int _newPhotos = 0;
        public int _deletedPhotos = 0;

        #region Public Properties
        public String DefaultCaption { get; set; }

        public ISaveMember MemberSaveService
        {
            get { return _memberSaveService ?? MemberSA.Instance; }
            set { _memberSaveService = value; }
        }

        public ISavePhotos PhotosSaveService
        {
            get { return _photosSaveService ?? MemberSA.Instance; }
            set { _photosSaveService = value; }
        }

        public IPhotoRules PhotoRulesManager
        {
            get
            {
                if(_photoRules == null)
                {
                    _photoRules = new PhotoRulesManager();
                }
                return _photoRules;
            }
            set { _photoRules = value; }
        }
        #endregion

        public MemberPhotoUpdateManager(IMember member, Brand brand, bool loadExistingPhotos)
        {
            _member = member;
            _brand = brand;
            _photoUpdates = new List<PhotoUpdate>();
            _remotePhotoUpdates = new List<Byte[]>();
            _photoRules = new PhotoRulesManager();

            if(loadExistingPhotos)
            {
                LoadExistingPhotos();
            }
        }

        public List<Photo> GetPhotos()
        {
            if (_photos == null) LoadExistingPhotos();
            return _photos;
        }

        public List<Photo> GetPaddedToMaxPhotos()
        {
            if (_photos == null) LoadExistingPhotos();

            List<Photo> paddedPhotos = new List<Photo>();
            
            if(_photos != null)
            {
                paddedPhotos.AddRange(_photos);
            }

            for (byte i = Byte.Parse(paddedPhotos.Count.ToString()); i < PhotoRulesManager.GetMaxPhotoCount(_brand); i++)
            {
                paddedPhotos.Add(new Photo(Constants.NULL_INT, Constants.NULL_INT, null, Byte.Parse((i + 1).ToString())));
            }

            return paddedPhotos;
        }
        
        public void AddUpdate(int memberPhotoID, string caption, bool isPrivate, byte listOrder, Byte[] fileBytes, bool isMain)
        {
            if (_photos == null) LoadExistingPhotos();
            
            if(memberPhotoID < 0 && !PhotoRulesManager.IsAcceptedImageType(fileBytes))
            {
                throw new ImageTypeNotSupportedException();
            }
            
            Regex regex = new Regex("<(.|\n)+?>", RegexOptions.IgnoreCase);
            caption = regex.Replace(caption, "");
            caption = caption.Replace("\r\n", " ");
            if (caption.ToLower().Trim() == DefaultCaption)
            {
                caption = string.Empty;
            }

            if (memberPhotoID > 0)
            {
                Photo photo = _photos.Find(delegate(Photo p) { return p.MemberPhotoID == memberPhotoID; });
                if (photo != null)
                {
                    if (photo.ListOrder == listOrder && photo.Caption == caption && photo.IsPrivate == isPrivate &&
                        photo.IsMain == isMain)
                    {
                        return;
                    }

                    bool isCaptionApproved = false;

                    if (photo.Caption == caption)
                    {
                        isCaptionApproved = photo.IsCaptionApproved;
                    }

                    _photoUpdates.Add(new PhotoUpdate(fileBytes,
                                                      false,
                                                      memberPhotoID,
                                                      photo.FileID,
                                                      photo.FileWebPath,
                                                      photo.ThumbFileID,
                                                      photo.ThumbFileWebPath,
                                                      listOrder,
                                                      photo.IsApproved,
                                                      isPrivate,
                                                      photo.AlbumID,
                                                      photo.AdminMemberID, caption, isCaptionApproved,
                                                      photo.FileCloudPath, photo.ThumbFileCloudPath,
                                                      photo.IsApprovedForMain, isMain, true));
                }
            }
            else
            {
                _newPhotos++;
                _photoUpdates.Add(new PhotoUpdate(fileBytes,
                                                  false,
                                                  memberPhotoID,
                                                  Constants.NULL_INT,
                                                  Constants.NULL_STRING,
                                                  Constants.NULL_INT,
                                                  Constants.NULL_STRING,
                                                  listOrder,
                                                  false,
                                                  isPrivate,
                                                  Constants.NULL_INT,
                                                  Constants.NULL_INT, caption, false,
                                                  Constants.NULL_STRING, Constants.NULL_STRING, false, isMain, true));
            }
        }

        public void AddDelete(int memberPhotoID, byte listOrder)
        {
            _deletedPhotos++;
            _photoUpdates.Add(new PhotoUpdate(null,
                                            true,
                                            memberPhotoID,
                                            Constants.NULL_INT,
                                            Constants.NULL_STRING,
                                            Constants.NULL_INT,
                                            Constants.NULL_STRING,
                                            listOrder,
                                            false,
                                            false,
                                            Constants.NULL_INT,
                                            Constants.NULL_INT));
        }

        public void AddRemotePhoto(string url)
        {
            WebClient webClient = new WebClient();
            Stream stream = webClient.OpenRead(url);

            MemoryStream ms = stream.CopyStream();
            Byte[] fileBytes = new byte[ms.Length];
            fileBytes = ms.GetBuffer();

            _remotePhotoUpdates.Add(fileBytes);
        }

        public PhotoUpdateResult SaveUpdates()
        {
            if(!NumberOfPhotoUpdatesAllowed())
            {
                return PhotoUpdateResult.TooManyPhotos;
            }
            
            PhotoUpdateResult result = PhotoUpdateResult.Success;

            if (_photoUpdates.Count > 0 || _remotePhotoUpdates.Count > 0)
            {
                if (_remotePhotoUpdates.Count > 0)
                {
                    int existingPhotoCount = 0;
                    if (_photos != null)
                    {
                        existingPhotoCount = _photos.Count;
                    }

                    int maxListOrder =  existingPhotoCount + _newPhotos + 1;
                    foreach(Byte[] fileBytes in _remotePhotoUpdates)
                    {
                        _photoUpdates.Add(new PhotoUpdate(fileBytes,
                                    false,
                                    Constants.NULL_INT,
                                    Constants.NULL_INT,
                                    Constants.NULL_STRING,
                                    Constants.NULL_INT,
                                    Constants.NULL_STRING,
                                    (Byte)maxListOrder++,
                                    false,
                                    false,
                                    Constants.NULL_INT,
                                    Constants.NULL_INT, Constants.NULL_STRING, false,
                                    Constants.NULL_STRING, Constants.NULL_STRING, false, false, true));
                    }
                }

                bool successfulSave = PhotosSaveService.SavePhotos(_brand.BrandID,
                                                              _brand.Site.SiteID, _brand.Site.Community.CommunityID,
                                                              _member.MemberID,
                                                              _photoUpdates.ToArray());
                if (successfulSave)
                {
                    _member.SetAttributeDate(_brand, "LastUpdated", DateTime.Now);
                    MemberSaveService.SaveMember(_member);
                }
                else
                {
                    result = PhotoUpdateResult.Failure;
                }
            }

            return result;
        }

        private bool NumberOfPhotoUpdatesAllowed()
        {
            var existingPhotosCount = _photos == null ? 0 : _photos.Count;
            return (existingPhotosCount + _newPhotos + _remotePhotoUpdates.Count - _deletedPhotos) <=
                   PhotoRulesManager.GetMaxPhotoCount(_brand);
        }

        private void LoadExistingPhotos()
        {
            _photos = MemberPhotoDisplayManager.Instance.GetAllPhotos(_member.GetIMemberDTO(), _brand.Site.Community.CommunityID);
        }
      
    }
}
