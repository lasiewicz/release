﻿using Matchnet.Configuration.ValueObjects;
using Matchnet.Content.ValueObjects.ServiceDefinitions;
using Matchnet.Member.ServiceAdapters.Interfaces;
using Matchnet.MemberSlideshow.ValueObjects.ServiceDefinitions;
using Matchnet.Search.Interfaces;
using Matchnet.Search.ValueObjects.Exceptions;
using Matchnet.Search.ValueObjects.ServiceDefinitions;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.Region;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.MemberSlideshow.ServiceAdapters;
using Matchnet.MemberSlideshow.ValueObjects;
using Matchnet.Search.ServiceAdapters;
using Matchnet.Search.ValueObjects;
using Matchnet.Web.Framework.Util;
using Matchnet.Web.Interfaces;
using SearchType = Spark.SAL.SearchType;

namespace Matchnet.Web.Framework.Managers
{
    public enum SlideshowDisplayType
    {
        Full=0,
        Mini=1, 
        ModalPopup=2
    }

    public class MemberSlideshowManager : AbstractManagerBaseClass
    {
        private int _memberID;
        private Brand _brand;
        private bool _useConfiguration;
        private ISlideshowPersistence _slideshowPersistence = null;
        private ISearchPreferencesSA _searchPreferencesService = null;
        private IMemberSlideshowSA _memberSlideshowService = null;
        private IRegionSA _regionService;

        public static int MIN_AGE_LIMIT = 18;
        public static int MAX_AGE_LIMIT = 99;
        
        public ISearchPreferencesSA SearchPreferencesService
        {
            get { return _searchPreferencesService ?? SearchPreferencesSA.Instance; }
            set { _searchPreferencesService = value; }
        }

        public IMemberSlideshowSA MemberSlideshowService
        {
            get { return _memberSlideshowService ?? MemberSlideshowSA.Instance; }
            set { _memberSlideshowService = value; }
        }

        public IRegionSA RegionService
        {
            get { return _regionService ?? (IRegionSA)RegionSA.Instance; }
            set { _regionService = value; }
        }

        public MemberSlideshowManager(int memberID, Brand brand)
        {
            _memberID = memberID;
            _brand = brand;
        }

        public void SetPersistence(ISlideshowPersistence persistence)
        {
            _slideshowPersistence = persistence;
        }

        public ISlideshowPersistence GetPersistence(int memberID)
        {
            return _slideshowPersistence ?? new MemberSlideshowPersistence(memberID); 
        }

        
        public MemberSlideshowSettings GetConfigurableSlideshowSettings()
        {
            ISlideshowPersistence persistence = GetPersistence(_memberID);
            if (persistence.MinAge == Constants.NULL_INT)
            {
                GetInitialConfigurationSettings(persistence);
            }

            MemberSlideshowSettings settings = new MemberSlideshowSettings(persistence.MinAge, persistence.MaxAge, persistence.RegionID, 
                persistence.SearchType, persistence.AreaCode1, persistence.AreaCode2, persistence.AreaCode3,
                persistence.AreaCode4, persistence.AreaCode5, persistence.AreaCode6);
            return settings;
        }

        public Slideshow GetSlideshow()
        {
            _useConfiguration = SettingsManager.GetSettingBool(SettingConstants.MEMBER_SLIDESHOW_USE_CONFIGURATION, _brand);
            if (_useConfiguration)
            {
                return GetConfigurableSlideshow();
            }
            else
            {
                return GetFixedSlideshow();
            }
        }

        public void SaveConfigurableSlideshowSettings(MemberSlideshowSettings settings)
        {
            ISlideshowPersistence persistence = GetPersistence(_memberID);
            persistence.MinAge = settings.MinAge;
            persistence.MaxAge = settings.MaxAge;
            persistence.RegionID = settings.RegionID;
            persistence.SearchType = settings.SearchType;
            persistence.AreaCode1 = settings.AreaCode1;
            persistence.AreaCode2 = settings.AreaCode2;
            persistence.AreaCode3 = settings.AreaCode3;
            persistence.AreaCode4 = settings.AreaCode4;
            persistence.AreaCode5 = settings.AreaCode5;
            persistence.AreaCode6 = settings.AreaCode6;
            persistence.PersistValues();
        }

        public void SaveConfigurableSlideshowSettings(int minAge, int maxAge, int regionID, SearchType searchType,
            int areaCode1, int areaCode2, int areaCode3, int areaCode4, int areaCode5, int areaCode6)
        {
            ISlideshowPersistence persistence = GetPersistence(_memberID);
            persistence.MinAge = minAge;
            persistence.MaxAge = maxAge;
            persistence.RegionID = regionID;
            persistence.SearchType = searchType;
            persistence.AreaCode1 = areaCode1;
            persistence.AreaCode2 = areaCode2;
            persistence.AreaCode3 = areaCode3;
            persistence.AreaCode4 = areaCode4;
            persistence.AreaCode5 = areaCode5;
            persistence.AreaCode6 = areaCode6;
            persistence.PersistValues();
        }

        public void RemoveSlideshowFromCache(int memberID, int communityID)
        {
            MemberSlideshowService.RemoveFromCache(memberID, communityID);
        }

        private Slideshow GetFixedSlideshow()
        {
            return MemberSlideshowService.GetSlideshow(_memberID, _brand);
        }
        
        public bool IsConfigurableSlideshowEnabled()
        {
            return SettingsManager.GetSettingBool(SettingConstants.ENABLE_CONFIGURABLE_SLIDESHOW, _brand);
        }

        public bool SlideshowUsesConfiguration()
        {
            return SettingsManager.GetSettingBool(SettingConstants.MEMBER_SLIDESHOW_USE_CONFIGURATION, _brand);
        }

        public IMember GetValidMemberFromSlideshow(bool updatePosition)
        {
            Slideshow slideshow = GetSlideshow();
            return MemberSlideshowService.GetValidMemberFromSlideshow(slideshow, _brand, updatePosition);
        }

        private Slideshow GetConfigurableSlideshow()
        {
            ISlideshowPersistence persistence = GetPersistence(_memberID); 
            if (persistence.MinAge == Constants.NULL_INT)
            {
                GetInitialConfigurationSettings(persistence);
            }

            SearchPreferenceCollection prefs = GetPreferencesForSlideshow(persistence);
            bool prioritizeExpiringMembers = SettingsManager.GetSettingBool(SettingConstants.MEMBER_SLIDESHOW_PRIORITIZE_EXPIRING_MEMBERS, _brand);
            return MemberSlideshowService.GetSlideshow(_memberID, _brand, prefs, prioritizeExpiringMembers);
        }

        private SearchPreferenceCollection GetPreferencesForSlideshow(ISlideshowPersistence persistence)
        {
            SearchPreferenceCollection slideshowPrefs = new SearchPreferenceCollection();
            SearchPreferenceCollection memberPreferences = SearchPreferencesService.GetSearchPreferences(_memberID, _brand.Site.Community.CommunityID);
            RegionLanguage region = RegionService.RetrievePopulatedHierarchy(persistence.RegionID, _brand.Site.LanguageID);

            slideshowPrefs.Add("GenderMask", memberPreferences.Value("GenderMask"));
            slideshowPrefs.Add("DomainID", _brand.Site.Community.CommunityID.ToString());
            slideshowPrefs.Add("SearchTypeID", persistence.SearchType.ToString("d")); 
            slideshowPrefs.Add("SearchOrderBy", ((int)QuerySorting.LastLogonDate).ToString()); 
            slideshowPrefs.Add("Radius", "160");
            slideshowPrefs.Add("HasPhotoFlag", "1");
            slideshowPrefs.Add("MinAge", persistence.MinAge.ToString());
            slideshowPrefs.Add("MaxAge", persistence.MaxAge.ToString());
            slideshowPrefs.Add("RegionID", persistence.RegionID.ToString());
            slideshowPrefs.Add("CountryRegionID", region.CountryRegionID.ToString());
            
            if(persistence.SearchType == SearchType.AreaCode)
            {
                if(persistence.AreaCode1 != Constants.NULL_INT)
                {
                    slideshowPrefs.Add("AreaCode1", persistence.AreaCode1.ToString());
                }
                if (persistence.AreaCode2 != Constants.NULL_INT)
                {
                    slideshowPrefs.Add("AreaCode2", persistence.AreaCode2.ToString());
                }
                if (persistence.AreaCode3 != Constants.NULL_INT)
                {
                    slideshowPrefs.Add("AreaCode3", persistence.AreaCode3.ToString());
                }
                if (persistence.AreaCode4 != Constants.NULL_INT)
                {
                    slideshowPrefs.Add("AreaCode4", persistence.AreaCode4.ToString());
                }
                if (persistence.AreaCode5 != Constants.NULL_INT)
                {
                    slideshowPrefs.Add("AreaCode5", persistence.AreaCode5.ToString());
                }
                if (persistence.AreaCode6 != Constants.NULL_INT)
                {
                    slideshowPrefs.Add("AreaCode6", persistence.AreaCode6.ToString());
                }
            }

            ValidationResult validationResult = slideshowPrefs.Validate();
            if (validationResult.Status == PreferencesValidationStatus.Failed)
            {
                throw new InvalidSearchPreferencesException(validationResult);
            }

            return slideshowPrefs;
        }

        private void GetInitialConfigurationSettings(ISlideshowPersistence persistence)
        {
            int ageFactor = SettingsManager.GetSettingInt(SettingConstants.MEMBER_SLIDESHOW_AGE_FACTOR, _brand);
            SearchPreferenceCollection memberPreferences = SearchPreferencesService.GetSearchPreferences(_memberID, _brand.Site.Community.CommunityID);

            ValidationResult validationResult = memberPreferences.Validate();
            if(validationResult.Status == PreferencesValidationStatus.Failed)
            {
                throw new InvalidSearchPreferencesException(validationResult);
            }
            
            int perfMinAge = Conversion.CInt(memberPreferences.Value("MinAge"));
            int perfMaxAge = Conversion.CInt(memberPreferences.Value("MaxAge"));
            persistence.MinAge = (perfMinAge - ageFactor >= MIN_AGE_LIMIT) ? (perfMinAge - ageFactor) : MIN_AGE_LIMIT;
            persistence.MaxAge = (perfMaxAge + ageFactor <= MAX_AGE_LIMIT) ? (perfMaxAge + ageFactor) : MAX_AGE_LIMIT;
            persistence.SearchType = (SearchType)Conversion.CInt(memberPreferences.Value("SearchTypeID"));
            persistence.RegionID = Conversion.CInt(memberPreferences.Value("RegionID"));
            
            if(persistence.SearchType == SearchType.AreaCode)
            {
                if (memberPreferences.ContainsKey("AreaCode1"))
                {
                    persistence.AreaCode1 = Conversion.CInt(memberPreferences.Value("AreaCode1"));
                }
                if (memberPreferences.ContainsKey("AreaCode2"))
                {
                    persistence.AreaCode2 = Conversion.CInt(memberPreferences.Value("AreaCode2"));
                }
                if (memberPreferences.ContainsKey("AreaCode3"))
                {
                    persistence.AreaCode3 = Conversion.CInt(memberPreferences.Value("AreaCode3"));
                }
                if (memberPreferences.ContainsKey("AreaCode4"))
                {
                    persistence.AreaCode4 = Conversion.CInt(memberPreferences.Value("AreaCode4"));
                }
                if (memberPreferences.ContainsKey("AreaCode5"))
                {
                    persistence.AreaCode5 = Conversion.CInt(memberPreferences.Value("AreaCode5"));
                }
                if (memberPreferences.ContainsKey("AreaCode6"))
                {
                    persistence.AreaCode6 = Conversion.CInt(memberPreferences.Value("AreaCode6"));
                }
            }
        }
    }
}
