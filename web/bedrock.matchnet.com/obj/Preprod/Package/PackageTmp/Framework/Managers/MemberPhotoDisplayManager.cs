﻿using System;
using System.Collections.Generic;
using System.Linq;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Member.ServiceAdapters.Interfaces;
using Matchnet.Member.ValueObjects.Interfaces;
using Matchnet.Member.ValueObjects.Photos;
using Matchnet.Web.Interfaces;
using Spark.CloudStorage;
using Matchnet.Member.ValueObjects;

namespace Matchnet.Web.Framework.Managers
{
    public sealed class MemberPhotoDisplayManager : AbstractManagerBaseClass, IMemberPhotoDisplayManager

    {
        #region Image Constants
        //composite of background and text, large size for use in member profile
        private const string IMAGE_PRIVATE_PHOTO_FULL = "privatephoto200x250_ynm4.gif";
        private const string IMAGE_PRIVATE_PHOTO_FULL_FEMALE = "privatephoto200x250_ynm4.gif";

        //background only, does not contain text, for use as thumbnail in searches and profiles (text appears on top).
        private const string IMAGE_PRIVATE_PHOTO_BACKGROUND_THUMB = "privatephoto80x104_ynm4.gif";
        private const string IMAGE_PRIVATE_PHOTO_BACKGROUND_THUMB_FEMALE = "privatephoto80x104_ynm4.gif";

        //composite of background and text, medium size and needs to be a non-interlaced jpg/jpeg for use in Userplane flash movie (and emails)
        private const string IMAGE_PRIVATE_PHOTO_THUMB = "privatephoto80x104_ynm4.jpg";
        private const string IMAGE_PRIVATE_PHOTO_THUMB_FEMALE = "privatephoto80x104_ynm4.jpg";

        //composite of background and text, small size for use in IM notifier
        private const string IMAGE_PRIVATE_PHOTO_TINY_THUMB = "privatephoto43x56_ym4.gif";
        private const string IMAGE_PRIVATE_PHOTO_TINY_THUMB_FEMALE = "privatephoto43x56_ym4.gif";

        //background only, does not contain text, for use as thumbnail in searches and profiles (text appears on top).
        private const string IMAGE_NOPHOTO_BACKGROUND_THUMB = "no-photo-m-m.png"; //"noPhoto.gif"; //current 80x103
        private const string IMAGE_NOPHOTO_BACKGROUND_THUMB_FEMALE = "no-photo-m-f.png"; //"noPhoto.gif";

        //composite of background and text, medium size and needs to be a non-interlaced jpg/jpeg for use in Userplane flash movie (and emails)
        private const string IMAGE_NOPHOTO_THUMB = "no-photo-m-m.png";//"noPhoto.jpg"; //current 80x103
        private const string IMAGE_NOPHOTO_THUMB_FEMALE = "no-photo-m-f.png";//"noPhoto.jpg";

        //background only, does not contain text, small size for use in IM notifier
        private const string IMAGE_NOPHOTO_BACKGROUND_TINY_THUMB = "no-photo-s-m.png";//"noPhoto_small.gif"; //current 43x56
        private const string IMAGE_NOPHOTO_BACKGROUND_TINY_THUMB_FEMALE = "no-photo-s-f.png";//"noPhoto_small.gif";

        //for use as thumbnail (new version 2.0)
        private const string IMAGE_NOPHOTO_THUMB_V2 = "no-photo-s-m.png"; //"no-photo-sm.gif"; //current 43x56
        private const string IMAGE_NOPHOTO_THUMB_V2_FEMALE = "no-photo-s-f.png";//"no-photo-sm.gif";

        //for use as tiny thumbnail (new version 2.0)
        private const string IMAGE_NOPHOTO_TINY_THUMB_V2 = "no-photo-s-m.png";//"no-photo-sm-54x70.gif";
        private const string IMAGE_NOPHOTO_TINY_THUMB_V2_FEMALE = "no-photo-s-f.png";//"no-photo-sm-54x70.gif";

        //Used by MMImageDisplay.aspx (email related), must stay as jpg
        private const string IMAGE_NOPHOTO_MM = "no-photo-l-m.jpg"; //nophotoLargeMM.jpg 246x328
        private const string IMAGE_NOPHOTO_MM_FEMALE = "no-photo-l-f.jpg"; //nophotoLargeMM.jpg
        #endregion

        private static readonly MemberPhotoDisplayManager _instance = new MemberPhotoDisplayManager();
        private string _imgDirectory = string.Empty;

        private MemberPhotoDisplayManager() {}

        public static MemberPhotoDisplayManager Instance
        {
            get { return _instance; }   
        }

        public string ImageDirectory
        {
            get
            {
                if(_imgDirectory == null || _imgDirectory == string.Empty)
                {
                    _imgDirectory = Matchnet.Web.Framework.Image.ImageUrlRoot + "/";
                }
                return _imgDirectory;
            }
            set { _imgDirectory = value; }
        }

        #region Public Methods

        /// <summary>
        /// The meaning of "private" photos isn't really private.  Private photos means they shouldn't be visible to visitors, but we
        /// still display them to members.
        /// </summary>
        /// <param name="photo"></param>
        /// <param name="brand"></param>
        /// <returns>True if the Photo is private and the Brand allows private photos.</returns>
        public bool IsPrivatePhoto(Photo photo, Brand brand)
        {
            return (photo.IsPrivate && brand.GetStatusMaskValue(StatusType.PrivatePhotos));
        }

        public bool PhotoIsEmpty(Photo photo, PhotoType photoType, Brand brand)
        {
            bool showPhotosFromCloud =
                SettingsManager.GetSettingBool(SettingConstants.ENABLE_MEMBER_PHOTOS_DISPLAY_FROM_CLOUD, brand);

            if (photoType == PhotoType.Full)
            {
                return showPhotosFromCloud
                           ? String.IsNullOrEmpty(photo.FileCloudPath)
                           : String.IsNullOrEmpty(photo.FileWebPath);
            }
            else
            {
                return showPhotosFromCloud
                      ? String.IsNullOrEmpty(photo.ThumbFileCloudPath)
                      : String.IsNullOrEmpty(photo.ThumbFileWebPath);
            }
        }

        /// <summary>
        /// Determines if only public photos should return.
        /// </summary>
        /// <param name="brand">Brand of the site the code is running on.</param>
        /// <param name="member">IMemberDTO who owns the photos to be returned.</param>
        /// <returns>True if only public photos should be returned.</returns>
        public bool PublicPhotosOnly(Brand brand, IMemberDTO viewingMember, IMemberDTO member)
        {
            // if the site doesn't support it we can return right away with false here
            if(!brand.GetStatusMaskValue(StatusType.PrivatePhotos))
                return false;

            // at this point, we just have to make sure the viewing member is not a guest
            if (viewingMember == null)
                return true;

            return false;
        }

        /// <summary>
        /// Selects a "default" photo object to use as a thumbnail (or whatever), returning a private photo if there are no approved,
        /// non-private photos, and null if there are no approved photos.
        /// </summary>
        /// <returns>The member's Photo or null if there is no photo.</returns>
        public Photo GetDefaultPhoto(IMemberDTO viewingMember, IMemberDTO member, Brand brand)
        {
            return member.GetDefaultPhoto(brand.Site.Community.CommunityID, PublicPhotosOnly(brand, viewingMember, member));
        }

        /// <summary>
        /// Retrieves one random photo out of all main-eligible photos.
        /// </summary>
        /// <returns>A random main approved photo or null if none found.</returns>
        public Photo GetRandomApprovedForMainPhoto(IMemberDTO viewingMember, IMemberDTO member, Brand brand)
        {
            return member.GetRandomApprovedForMainPhoto(brand.Site.Community.CommunityID, PublicPhotosOnly(brand, viewingMember, member));
        }

        /// <summary>
        /// Gets all photos that approved for main.
        /// </summary>
        /// <returns>List of Photo objects or null if none found.</returns>
        public List<Photo> GetApprovedForMainPhotos(IMemberDTO viewingMember, IMemberDTO member, Brand brand)
        {
            return member.GetApprovedForMainPhotos(brand.Site.Community.CommunityID, PublicPhotosOnly(brand, viewingMember, member));
        }

        /// <summary>
        /// Gets all photos that approved for main.  Checks PhotoIsEmpy before adding the photo to the returned list.
        /// </summary>
        /// <param name="photoType">PhotoType to check for when checking for empty photo.</param>
        /// <returns>List of Photo objecdts or null if none found.</returns>
        public List<Photo> GetApprovedForMainPhotos(IMemberDTO viewingMember, IMemberDTO member, Brand brand, PhotoType photoType)
        {
            var photos = GetApprovedForMainPhotos(viewingMember, member, brand);

            if (photos == null)
                return null;

            return (from p in photos
                    where !PhotoIsEmpty(p, photoType, brand)
                    select p).ToList();

            //var photos = member.GetPhotos(communityId);
            //if (photos == null || photos.Count == 0)
            //    return null;

            //var approvedPhotos = new List<Photo>();
            //for (byte i = 0; i < photos.Count; i++)
            //{
            //    if (photos[i].IsApproved && photos[i].IsApprovedForMain && !PhotoIsEmpty(photos[i], photoType))
            //    {
            //        approvedPhotos.Add(photos[i]);
            //    }
            //}

            //approvedPhotos.Sort((p1, p2) => p1.ListOrder.CompareTo(p2.ListOrder));

            //return approvedPhotos;
        }

        /// <summary>
        /// Gets any approved photos exluding the main photo.
        /// </summary>
        /// <param name="publicOnly">Pass true if private photos shouldn't return.</param>
        /// <returns>List of Photo objects or null if none found.</returns>
        public List<Photo> GetApprovedPhotosExcludingMain(IMemberDTO viewingMember, IMemberDTO member, Brand brand, bool publicOnly)
        {
            return member.GetApprovedPhotosExcludingMain(brand.Site.Community.CommunityID, publicOnly);
        }

        /// <summary>
        /// Gets any approved photos; including approved photos not suitable for main.
        /// </summary>
        /// <returns>List of Photo objects or null if none found.</returns>
        public List<Photo> GetApprovedPhotos(IMemberDTO viewingMember, IMemberDTO member, Brand brand)
        {
            return member.GetApprovedPhotos(brand.Site.Community.CommunityID, PublicPhotosOnly(brand, viewingMember, member));
        }

        /// <summary>
        /// Gets any approved photos; including approved photos not suitable for main.  Checks to see if PhotoIsEmpty before adding the photo
        /// to the returned list.
        /// </summary>
        /// <param name="member"></param>
        /// <param name="communityId"></param>
        /// <returns></returns>
        public List<Photo> GetApprovedPhotos(IMemberDTO viewingMember, IMemberDTO member, Brand brand, PhotoType photoType)
        {
            var photos = GetApprovedPhotos(viewingMember, member, brand);

            if (photos == null)
                return null;

            return (from p in photos
                    where !PhotoIsEmpty(p, photoType, brand)
                    select p).ToList();

            //var photos = member.GetPhotos(communityId);
            //if (photos == null || photos.Count == 0)
            //    return null;

            //var approvedPhotos = new List<Photo>();
            //for (byte i = 0; i < photos.Count; i++)
            //{
            //    if (photos[i].IsApproved && !PhotoIsEmpty(photos[i], photoType))
            //    {
            //        approvedPhotos.Add(photos[i]);
            //    }
            //}

            //approvedPhotos.Sort((p1, p2) => p1.ListOrder.CompareTo(p2.ListOrder));

            //return approvedPhotos;
        }

        /// <summary>
        /// Count can be obtained by calling GetApprovedPhotos too, but by making this into a separate method, it's easier to tell the caller's intention.
        /// This will help us during future refactoring.  I see too many places where GetApprovedPhotos is called just for count.
        /// </summary>
        /// <returns>Number of approved photos.</returns>
        public int GetApprovedPhotosCount(IMemberDTO viewingMember, IMemberDTO member, Brand brand)
        {
            var photos = GetApprovedPhotos(viewingMember, member, brand);

            if (photos == null)
                return 0;

            return photos.Count;
        }

        /// <summary>
        /// Count can be obtained by calling GetApprovedPhotos too, but by making this into a separate method, it's easier to tell the caller's intention.
        /// This will help us during future refactoring.  I see too many places where GetApprovedPhotos is called just for count.  Checks PhotoIsEmpty
        /// before counting that photo as the returned count.
        /// </summary>
        /// <returns>Number of approved photos excluding the number of empty photos for a given PhotoType.</returns>
        public int GetApprovedPhotosCount(IMemberDTO viewingMember, IMemberDTO member, Brand brand, PhotoType photoType)
        {
            var photos = GetApprovedPhotos(viewingMember, member, brand, photoType);

            if (photos == null)
                return 0;

            return photos.Count;
        }

        /// <summary>
        /// Retrieve all member photos not caring for any sort of approval status.
        /// </summary>
        /// <returns>List of Photo objects or null if member has none.</returns>
        public List<Photo> GetAllPhotos(IMemberDTO member, int communityId)
        {
            var photos = member.GetPhotos(communityId);
            if (photos == null || photos.Count == 0)
                return null;

            var allPhotos = new List<Photo>();
            for (byte i = 0; i < photos.Count; i++)
            {
                allPhotos.Add(photos[i]);
            }

            allPhotos.Sort((p1, p2) => p1.ListOrder.CompareTo(p2.ListOrder));

            return allPhotos;
        }

        /// <summary>
        /// Retrieve all member photos not caring for any sort of approval status. This method checks for PhotoIsEmpty before including
        /// the photo in the return list.
        /// </summary>
        /// <param name="photoType">PhotoType to use when checking for photo is empty.</param>
        /// <returns>List of Photo objects or null if member has none.</returns>
        public List<Photo> GetAllPhotos(IMemberDTO member, Brand brand, PhotoType photoType)
        {
            var photos = member.GetPhotos(brand.Site.Community.CommunityID);
            if (photos == null || photos.Count == 0)
                return null;

            var allPhotos = new List<Photo>();
            for (byte i = 0; i < photos.Count; i++)
            {
                if (!PhotoIsEmpty(photos[i], photoType, brand))
                {
                    allPhotos.Add(photos[i]);
                }
            }

            allPhotos.Sort((p1, p2) => p1.ListOrder.CompareTo(p2.ListOrder));

            return allPhotos;
        }

        /// <summary>
        /// Returns the count of all photos the member has not caring for any sort of approval status.
        /// </summary>
        /// <returns>Number of all photos - no filter applied.</returns>
        public int GetAllPhotosCount(IMemberDTO member, int communityId)
        {
            var photos = GetAllPhotos(member, communityId);

            return photos == null ? 0 : photos.Count;
        }
        
        public string GetPhotoDisplayURL(IMemberDTO memberViewing, IMemberDTO memberBeingViewed, Brand brand,
            Photo photo, PhotoType photoType, PrivatePhotoImageType privatePhotoType, bool useFullURL)
        {
            //there is no NO PHOTO image for full size photos
            return GetPhotoDisplayURL(memberViewing, memberBeingViewed, brand, photo, photoType, privatePhotoType,
                               NoPhotoImageType.None, useFullURL);
        }
        public string GetPhotoDisplayURL(IMemberDTO memberViewing, IMemberDTO memberBeingViewed, Brand brand,
            Photo photo, PhotoType photoType, PrivatePhotoImageType privatePhotoType)
        {
            //there is no NO PHOTO image for full size photos
            return GetPhotoDisplayURL(memberViewing, memberBeingViewed, brand, photo, photoType, privatePhotoType,
                               NoPhotoImageType.None, false);
        }

        public string GetPhotoDisplayURL(IMemberDTO memberViewing, IMemberDTO memberBeingViewed, Brand brand,
            Photo photo, PhotoType photoType, PrivatePhotoImageType privatePhotoType, NoPhotoImageType noPhotoType)
        {
            //there is no NO PHOTO image for full size photos
            return GetPhotoDisplayURL(memberViewing, memberBeingViewed, brand, photo, photoType, privatePhotoType,
                               noPhotoType, false);
        }

        public string GetPhotoDisplayURL(IMemberDTO memberViewing, IMemberDTO memberBeingViewed, Brand brand,
            Photo photo, PhotoType photoType, PrivatePhotoImageType privatePhotoType, NoPhotoImageType noPhotoType, bool useFullURL)
        {
            return GetPhotoDisplayURL(memberViewing, memberBeingViewed, brand, photo, photoType, privatePhotoType,
                               noPhotoType, useFullURL, false);
        }

        public string GetPhotoDisplayURL(IMemberDTO memberViewing, IMemberDTO memberBeingViewed, Brand brand,
            Photo photo, PhotoType photoType, PrivatePhotoImageType privatePhotoType, NoPhotoImageType noPhotoType, bool useFullURL, bool displayPrivatePhotos)
        {
            bool isMale = FrameworkGlobals.IsMaleGender(memberBeingViewed, brand);
            string privatePhotoFile = (privatePhotoType == PrivatePhotoImageType.None) ? string.Empty : GetPrivatePhotoFile(privatePhotoType, isMale);
            string noPhotoFile = (noPhotoType == NoPhotoImageType.None) ? string.Empty : GetNoPhotoFile(noPhotoType, isMale);

            if (photo == null)
            {
                return noPhotoFile;
            }

            if (!displayPrivatePhotos && !IsPhotosAvailable(memberViewing, memberBeingViewed, brand))
            {
                return noPhotoFile;
            }

            return getPhotoURL(photo, brand, privatePhotoFile, noPhotoFile, photoType, useFullURL, displayPrivatePhotos);
        }

        public string GetPrivatePhotoFile(PrivatePhotoImageType imageType, IMemberDTO member, Brand brand)
        {
            bool isMale = FrameworkGlobals.IsMaleGender(member, brand);
            return GetPrivatePhotoFile(imageType, isMale);
        }

        public string GetPrivatePhotoFile(PrivatePhotoImageType imageType, bool isMale)
        {
            string imageFile = string.Empty;

            switch (imageType)
            {
                case PrivatePhotoImageType.BackgroundThumb:
                    imageFile = isMale
                                    ? IMAGE_PRIVATE_PHOTO_BACKGROUND_THUMB
                                    : IMAGE_PRIVATE_PHOTO_BACKGROUND_THUMB_FEMALE;
                    break;
                case PrivatePhotoImageType.Full:
                    imageFile = isMale
                                    ? IMAGE_PRIVATE_PHOTO_FULL
                                    : IMAGE_PRIVATE_PHOTO_FULL_FEMALE;
                    break;
                case PrivatePhotoImageType.Thumb:
                    imageFile = isMale
                                    ? IMAGE_PRIVATE_PHOTO_THUMB
                                    : IMAGE_PRIVATE_PHOTO_THUMB_FEMALE;
                    break;
                case PrivatePhotoImageType.TinyThumb:
                    imageFile = isMale
                                    ? IMAGE_PRIVATE_PHOTO_TINY_THUMB
                                    : IMAGE_PRIVATE_PHOTO_TINY_THUMB_FEMALE;
                    break;
            }

            return ImageDirectory + imageFile;
        }

        public string GetNoPhotoFile(NoPhotoImageType imageType, IMemberDTO member, Brand brand)
        {
            bool isMale = FrameworkGlobals.IsMaleGender(member, brand);
            return GetNoPhotoFile(imageType, isMale);
        }

        public string GetNoPhotoFile(NoPhotoImageType imageType, bool isMale)
        {
            string imageFile = string.Empty;

            switch (imageType)
            {
                case NoPhotoImageType.BackgroundThumb:
                    imageFile = isMale
                                    ? IMAGE_NOPHOTO_BACKGROUND_THUMB
                                    : IMAGE_NOPHOTO_BACKGROUND_THUMB_FEMALE;
                    break;
                case NoPhotoImageType.BackgroundTinyThumb:
                    imageFile = isMale
                                    ? IMAGE_NOPHOTO_BACKGROUND_TINY_THUMB
                                    : IMAGE_NOPHOTO_BACKGROUND_TINY_THUMB_FEMALE;
                    break;
                case NoPhotoImageType.Matchmail:
                    imageFile = isMale ? IMAGE_NOPHOTO_MM : IMAGE_NOPHOTO_MM_FEMALE;
                    break;
                case NoPhotoImageType.Thumb:
                    imageFile = isMale ? IMAGE_NOPHOTO_THUMB : IMAGE_NOPHOTO_THUMB_FEMALE;
                    break;
                case NoPhotoImageType.ThumbV2:
                    imageFile = isMale
                                    ? IMAGE_NOPHOTO_THUMB_V2
                                    : IMAGE_NOPHOTO_THUMB_V2_FEMALE;
                    break;
                case NoPhotoImageType.TinyThumbV2:
                    imageFile = isMale
                        ? IMAGE_NOPHOTO_TINY_THUMB_V2
                        : IMAGE_NOPHOTO_TINY_THUMB_V2_FEMALE;
                    break;
            }

            return ImageDirectory + imageFile;
        }

        /// <summary>
        /// This checks if photos are available to guests
        /// </summary>
        public bool IsPhotosAvailableToGuests(IMemberDTO memberViewing, IMemberDTO memberBeingViewed, Brand brand)
        {
            if (memberViewing == null || memberViewing.MemberID <= 0)
                return memberBeingViewed.GetAttributeBool(brand, "DisplayPhotosToGuests");
            else
            {
                return true;
            }
        }

        /// <summary>
        /// This checks if photos are available at all - checks for guests, suspension, etc
        /// </summary>
        public bool IsPhotosAvailable(IMemberDTO memberViewing, IMemberDTO memberBeingViewed, Brand brand)
        {
            bool avail = true;

            if (avail)
            {
                //suspended members have photos hidden (moved to another bucket )and are not available, resulting in broken images
                int globalStatusMask = memberBeingViewed.GetAttributeInt(brand, "GlobalStatusMask");
                if ((globalStatusMask & CachedMember.ATTRIBUTEOPTION_SUSPEND_ADMIN) ==
                    CachedMember.ATTRIBUTEOPTION_SUSPEND_ADMIN)
                {
                    avail = false;
                }
            }
            
            if (avail)
            {
                //self-suspended members have photos (moved to another bucket )and are not available, resulting in broken images
                int selfSuspendedFlag = memberBeingViewed.GetAttributeInt(brand, "SelfSuspendedFlag");
                if (selfSuspendedFlag == 1)
                {
                    avail = false;
                }
            }

            if (avail)
            {
                avail = IsPhotosAvailableToGuests(memberViewing, memberBeingViewed, brand);
            }

            return avail;
        }

        #endregion Public Methods

        #region Private Methods

        private string getPhotoURL(Photo photo, Brand brand, string privatePhotoFile, string noPhotoFile, PhotoType photoType, bool useFullURL, bool displayPrivatePhotos)
        {
            string memberPhotoUrl = string.Empty;

            bool showPhotosFromCloud =
                SettingsManager.GetSettingBool(SettingConstants.ENABLE_MEMBER_PHOTOS_DISPLAY_FROM_CLOUD, brand);

            if (photo != null)
            {
                if (!displayPrivatePhotos && MemberPhotoDisplayManager.Instance.IsPrivatePhoto(photo, brand))
                {
                    memberPhotoUrl = privatePhotoFile;
                    return memberPhotoUrl;//no need to continue. Return relative to site root path to private photo file. We don't want to use proxy on non-absolute private photo URL. see 17305.
                }
                else
                {
                    switch (photoType)
                    {
                        case PhotoType.Full:
                            if (photo.FileWebPath != null) //17243 - some members have null FileWebPath but shouldn't 
                            {
                                memberPhotoUrl = showPhotosFromCloud ? photo.FileCloudPath : photo.FileWebPath;
                            }
                            break;
                        case PhotoType.Thumbnail:
                            if (photo.ThumbFileWebPath != null) // some members have null ThumbFileWebPath (before approval for example)
                            {
                                memberPhotoUrl = showPhotosFromCloud ? photo.ThumbFileCloudPath : photo.ThumbFileWebPath;
                            }
                            break;
                    }
                }
            }

            //if photo URL is still empty, use the default noPhoto file
            if (memberPhotoUrl == string.Empty)
            {
                memberPhotoUrl = noPhotoFile;
            }
            else
            {
                if (showPhotosFromCloud)
                {
                    Client cloudClient = new Client(brand.Site.Community.CommunityID, brand.Site.SiteID, SettingsManager.SettingsService);
                    bool isSecure = FrameworkGlobals.IsSecureRequest();
                    memberPhotoUrl = cloudClient.GetFullCloudImagePath(memberPhotoUrl, FileType.MemberPhoto, isSecure);
                }
                else
                {
                    if (FrameworkGlobals.IsSecureRequest())
                    {
                        //use a proxy that mimics https site to avoid "this page contains nonsecure items"
                        memberPhotoUrl = useSecureProxyImage(memberPhotoUrl);
                    }
                    else if (useFullURL)
                    {
                        memberPhotoUrl = prependFullURL(brand, memberPhotoUrl);
                    }
                }
            }

            return memberPhotoUrl;
        }

        /// <summary>
        /// This is to defeat the "this page contains nonsecure items" dialog that appears when you open a https page.
        /// Instead of getting image from http url, we get image from aspx page on https site that acts as proxy for that image.
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        private string useSecureProxyImage(string url)
        {
            return "/Applications/MemberProfile/MemberPhotoFile.aspx?Path=" + System.Web.HttpUtility.UrlEncode(url);
        }

        private string prependFullURL(Brand brand, string relativePath)
        {
            return "http://" + brand.Site.DefaultHost + "." + brand.Uri + relativePath;
        }


        #endregion Private Methods
    }
}