﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Security;

namespace Matchnet.Web.Framework.Managers
{
    public class MessageManager : AbstractManagerBaseClass
    {
        private const string URL_PARM_SITEID = "sid";
        private const string URL_PARM_ENCRYPTED_MEMBERID = "emid";

        #region Singleton manager
        private static readonly MessageManager _Instance = new MessageManager();
        private MessageManager()
        {
            
        }
        public static MessageManager Instance
        {
            get { return _Instance; }
        }
        #endregion

        public int DecryptMemberID(string encMemberID)
        {
            var memberID = Constants.NULL_INT;

            // Because the Request.QueryString object interprets the '+' sign as a space, we must use the replace function
            var decMemberID = Crypto.Decrypt(WebConstants.OUTBOUND_EMAIL_CRYPT_KEY,
                                                encMemberID.Replace(" ", "+"));

            if (!int.TryParse(decMemberID, out memberID))
            {
                throw new ApplicationException("Invalid memberID");
            }

            return memberID;
        }

        public int GetMemberIDFromURL(HttpRequest request)
        {
            var memberID = Constants.NULL_INT;

            var siteID = String.IsNullOrEmpty(request[URL_PARM_SITEID]) ? Constants.NULL_STRING : request[URL_PARM_SITEID];
            var encryptedMemberID = String.IsNullOrEmpty(request[URL_PARM_ENCRYPTED_MEMBERID]) ? Constants.NULL_STRING : request[URL_PARM_ENCRYPTED_MEMBERID];

            if (!string.IsNullOrEmpty(siteID) && !string.IsNullOrEmpty(encryptedMemberID))
            {
                var member = MemberSA.Instance.GetMember(DecryptMemberID(encryptedMemberID), MemberLoadFlags.None);
                if (member != null && member.GetSiteIDList().Contains<int>(int.Parse(siteID)))
                {
                    memberID = member.MemberID;
                }
            }

            return memberID;
        }

        public void MarkMaskValues(Member.ServiceAdapters.Member member, Brand brand, string maskAttrName, int[] values)
        {
            var oldValue = member.GetAttributeInt(brand, maskAttrName);

            oldValue = values.Aggregate(oldValue, (current, val) => current | val);

            member.SetAttributeInt(brand, maskAttrName, oldValue);
        }

        public void UnmarkMaskValues(Member.ServiceAdapters.Member member, Brand brand, string message, int[] values)
        {
            var oldValue = member.GetAttributeInt(brand, message);

            foreach (var val in values)
            {
                if ((oldValue & val) == val)
                {
                    oldValue -= val;
                }
            }

            member.SetAttributeInt(brand, message, oldValue);
        }

        public bool IsNoPhotoRequestMessageEnabled(Brand brand)
        {
            bool isEnabled = SettingsManager.GetSettingBool("ENABLE_AUTO_NO_PHOTO_REQUEST_MESSAGE", brand, false);

            return isEnabled;
        }

        public bool IsReadRecieptPremiumFeatureEnabled(Brand brand)
        {
            bool isEnabled = SettingsManager.GetSettingBool("ENABLE_READ_RECEIPTS_PREMIUM_SERVICE_FEATURE", brand, false);

            return isEnabled;
        }

        public bool DoesMemberHaveReadRecieptPrivilege(Member.ServiceAdapters.Member member, Brand brand)
        {
            bool hasPrivilege = false;

            //temp: use url parameter until this privilege is a part of access and available for purchase
            if (member != null && brand != null)
            {
                Spark.Common.AccessService.AccessPrivilege readReceiptPrivilege = member.GetUnifiedAccessPrivilege(Spark.Common.AccessService.PrivilegeType.ReadReceipt, brand.BrandID, brand.Site.SiteID, brand.Site.Community.CommunityID);
                if (readReceiptPrivilege != null && readReceiptPrivilege.EndDatePST > DateTime.Now)
                {
                    hasPrivilege = true;
                }
            }
            return hasPrivilege;
        }
    }
}