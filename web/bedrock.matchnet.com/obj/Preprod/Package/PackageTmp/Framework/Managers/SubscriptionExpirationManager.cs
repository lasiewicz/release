﻿using System;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Member.ServiceAdapters.Interfaces;
using Matchnet.Session.ValueObjects;
using Spark.Common.Adapter;

namespace Matchnet.Web.Framework.Managers
{
    public class SubscriptionExpirationManager : AbstractManagerBaseClass
    {
        private const string SUBSCRIPTION_EXPIRATION_ALERT_DISPLAYED = "SubscriptionExpirationAlertDisplayed";

        #region Singleton manager
        private static readonly SubscriptionExpirationManager _Instance = new SubscriptionExpirationManager();
        private SubscriptionExpirationManager()
        {
        }
        public static SubscriptionExpirationManager Instance
        {
            get { return _Instance; }
        }

        #endregion 

        private Matchnet.Purchase.ValueObjects.SubscriptionStatus GetMemberSubscriptionStatus(ContextGlobal _g)
        {
            int memberSubscriptionStatus = (_g.Member != null &&
                              _g.Member.MemberID == _g.TargetMemberID) ?
                                                FrameworkGlobals.GetMemberSubcriptionStatus(_g.Member, _g.TargetBrand) :
                                                     FrameworkGlobals.GetMemberSubcriptionStatus(_g.TargetMemberID, _g.TargetBrand);
            Matchnet.Purchase.ValueObjects.SubscriptionStatus enumSubscriptionStatus = (Matchnet.Purchase.ValueObjects.SubscriptionStatus)memberSubscriptionStatus;
            return enumSubscriptionStatus;

        }

        public bool DisplaySubscriptionExpirationAlert(Brand brand, IMember member, ContextGlobal _g)
        {
            // hack for dev
            if (!string.IsNullOrEmpty(CurrentRequest["SubExAlert"]))
                return true;

            bool isEnabled = false;
            try
            {
                if (member != null && member.MemberID > 0) 
                {
                    // Display every 12 hours or per session
                    if (ShowAlert(_g))
                    {
                        // AB test
                        string lastDigits = SettingsManager.GetSettingString("SUBSCRIPTION_EXPIRATION_ALERT_VISIBILITY", brand);
                        var lastDigitsArray = lastDigits.Split(new char[] { ',' });
                        bool found = false;
                        if (lastDigitsArray.Length > 0)
                        {
                            foreach (var digit in lastDigitsArray)
                            {
                                if ((member.MemberID % 10).ToString() == digit)
                                {
                                    found = true;
                                    break;
                                }
                            }
                        }
                        if (!found)
                            return false;

                        // Only for subscribers
                        var enumSubscriptionStatus = GetMemberSubscriptionStatus(_g);

                        if (enumSubscriptionStatus == Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberExSubscriber ||
                          enumSubscriptionStatus == Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberFreeTrial ||
                            enumSubscriptionStatus == Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberNeverSubscribed ||
                            enumSubscriptionStatus == Matchnet.Purchase.ValueObjects.SubscriptionStatus.PausedSubscriber)
                            return false;

                        Spark.Common.RenewalService.RenewalSubscription renewalSub = RenewalManager.Instance.GetRenewalSubscription(_g.TargetMemberID, _g.TargetBrand);
                        int numOfDaysToDisplayBeforeSubX = SettingsManager.GetSettingInt("SUBSCRIPTION_EXPIRATION_ALERT_X_DAYS_BEFORE", brand);

                        if (renewalSub == null || renewalSub.RenewalSubscriptionID <= 0)
                        {
                            if (AuthorizationServiceWebAdapter.GetProxyInstance().HasActiveFreeTrialSubscription(_g.TargetMemberID, _g.TargetBrand.Site.SiteID))
                            {
                                //UPDATING NOT SHOWING ALERT FOR FREE TRIAL
                                // Only if # of days DB setting <= days till renewal
                                //if (GetDaysTillRenewal(_g) > numOfDaysToDisplayBeforeSubX)
                                    return false;
                            }
                            //else
                            //{
                            //    return false;
                            //}
                        }
                        else
                        {
                            if ((renewalSub.RenewalDatePST - DateTime.Now.Date).Days > numOfDaysToDisplayBeforeSubX)
                                return false;
                        }

                        // Only for Auto renewal turned off
                        if (!FrameworkGlobals.IsAutorenewOn(_g.Member, _g))
                        {
                            isEnabled = true;
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                _g.ProcessException(ex);
            }

            return isEnabled;
        }

        public int GetDaysTillRenewal(ContextGlobal _g)
        {
            if (_g.Member.GetAttributeDate(_g.Brand, "SubscriptionExpirationDate") != DateTime.MinValue)
            {
                return (_g.Member.GetAttributeDate(_g.Brand, "SubscriptionExpirationDate") - DateTime.Now.Date).Days;
            }

            return 0;
        }

        public bool ShowUpgradeLink(ContextGlobal _g)
        {
            return !FrameworkGlobals.IsMemberPremiumAuthenticated(_g.Member.MemberID, _g.Brand);
        }

        public bool ShowAlert(ContextGlobal _g)
        {
            int hours = SettingsManager.GetSettingInt("SUBSCRIPTION_EXPIRATION_ALERT_SHOW_EVERY_X_HOURS", _g.Brand);
            if(hours == 0) //then show alert per session. Otherwise show alert every x hours from the setting.
            {
                if (string.IsNullOrEmpty(_g.Session[SUBSCRIPTION_EXPIRATION_ALERT_DISPLAYED]))
                {
                    _g.Session.Add(SUBSCRIPTION_EXPIRATION_ALERT_DISPLAYED, "true", SessionPropertyLifetime.Temporary);
                    return true;
                }
                return false;
            }
            //If hours > 0
            //Show alert to members every x hours if they had closed the alert. 
            //When member closes the alert, a cookie is created that will expire in x hours. Show the alert only when this cookie has expired.
            string cookieVal = FrameworkGlobals.GetCookie("subexpalert", "", false);
            if (string.IsNullOrEmpty(cookieVal)) //No cookie, show alert
            {
                return true;
            }
            return false;    
        }

    }
}
