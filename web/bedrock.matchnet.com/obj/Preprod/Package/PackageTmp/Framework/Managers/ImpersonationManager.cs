﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ServiceAdapters.Interfaces;
using Matchnet.Member.ValueObjects.Admin;
using Matchnet.Web.Interfaces;

namespace Matchnet.Web.Framework.Managers
{
    public class ImpersonationManager : AbstractManagerBaseClass, IImpersonationManager
    {
        #region Singleton implementation
        private static readonly ImpersonationManager _instance = new ImpersonationManager();
        private ImpersonationManager()
        {}
        public static ImpersonationManager Instance
        {
            get { return _instance; }
        }
        #endregion

        private IGetMember _getMember;
        private ISaveMember _saveMember;

        public IGetMember GetMember
        {
            get { return _getMember ?? (_getMember = MemberSA.Instance); }
            set { _getMember = value; }
        }

        public ISaveMember SaveMember
        {
            get { return _saveMember ?? (_saveMember = MemberSA.Instance); }
            set { _saveMember = value; }
        }

        #region Implementation of IImpersonationManager

        public string GenerateImpersonateToken(int adminMemberId, int impersonateMemberId, int impersonateBrandId)
        {
            return SaveMember.GenerateImpersonateToken(adminMemberId, impersonateMemberId, impersonateBrandId);
        }

        public ImpersonateToken ValidateImpersonateToken(string impersonateTokenGUID)
        {
            return GetMember.ValidateImpersonateToken(impersonateTokenGUID);
        }

        public void ExpireImpersonateToken(int adminMemberId)
        {
            SaveMember.ExpireImpersonationToken(adminMemberId);
        }

        #endregion
    }
}