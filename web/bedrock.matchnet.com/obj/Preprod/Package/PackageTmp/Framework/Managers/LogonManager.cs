﻿#region

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web;
using System.Windows.Forms;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects;
using Matchnet.Web.Applications.API;
using Matchnet.Web.Applications.Chat.XMLResponses;
using Spark.Common.Adapter;
using Spark.Common.RenewalService;
using Spark.Authentication.OAuth.V2;
using Spark.Common.RestConsumer.V2.Models.OAuth2;
using Matchnet.Configuration.ValueObjects;
using Spark.LoginFraud;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Web.Framework.SparkAPI;

#endregion

namespace Matchnet.Web.Framework.Managers
{
    public class LogonManager : AbstractManagerBaseClass, IPostLogonProcessor
    {
        private readonly ContextGlobal _g;
        private int appId = Constants.NULL_INT;
        private string clientSecret = Constants.NULL_STRING;
        private bool _isAutoLogon = false;
        private static ISettingsSA _settingsService = null;

        public LogonManager(ContextGlobal g)
        {
            _g = g;
        }

        public static ISettingsSA SettingsService
        {
            get { return _settingsService ?? RuntimeSettings.Instance; }
            set { _settingsService = value; }
        }

        public static bool IsTopLevelDomainCookiesEnabled(ContextGlobal g)
        {
            return SettingsService.SettingExistsFromSingleton("SUA_TOP_DOMAIN_COOKIE_ENABLED",
                g.Brand.Site.Community.CommunityID,
                g.Brand.Site.SiteID) &&
                SettingsService.GetSettingFromSingleton("SUA_TOP_DOMAIN_COOKIE_ENABLED",
                    g.Brand.Site.Community.CommunityID,
                    g.Brand.Site.SiteID) == "true";
        }

        public static bool IsSuaLogon(ContextGlobal g)
        {
            return SettingsService.SettingExistsFromSingleton("SUA_OAUTH_ENABLED",
                g.Brand.Site.Community.CommunityID,
                g.Brand.Site.SiteID) &&
                SettingsService.GetSettingFromSingleton("SUA_OAUTH_ENABLED",
                    g.Brand.Site.Community.CommunityID,
                    g.Brand.Site.SiteID) == "true";
        }

        public static bool IsBetaRedesignEnabled(ContextGlobal g)
        {
            return SettingsService.GetSettingFromSingleton("BETA_SITE_REDESIGN_ENABLED", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID) == "true";
        }

        public static int GetBetaRedesignPercent(ContextGlobal g)
        {
            int defaultVal = 0;
            try
            {
                defaultVal = Int32.Parse(SettingsService.GetSettingFromSingleton("BETA_SITE_REDESIGN_PERCENT",
                    g.Brand.Site.Community.CommunityID,
                    g.Brand.Site.SiteID));
            }
            catch (Exception e)
            {
                //TODO: Spark.Logger here
                defaultVal = 0;
            }
            return defaultVal;
        }

        public static string GetBetaRedesignUrl(ContextGlobal g, string pageName)
        {
            string defaultVal = "http://beta."+g.Brand.Site.DefaultHost;
            try
            {
                defaultVal = SettingsService.GetSettingFromSingleton("BETA_SITE_REDESIGN_URL",
                    g.Brand.Site.Community.CommunityID,
                    g.Brand.Site.SiteID) + "/" + pageName;
            }
            catch (Exception e)
            {
                //TODO: Spark.Logger here
                defaultVal = "http://beta." + g.Brand.Site.DefaultHost;
            }
            return defaultVal;
        }

        #region IPostLogonProcessor Members
        public string Environment
        {
            get
            {
                return ConfigurationManager.AppSettings["TokenCookieEnvironment"] ?? String.Empty;
            }
        }

        public bool IsTopLevelDomainCookiesEnabled()
        {
            return LogonManager.IsTopLevelDomainCookiesEnabled(_g);
        }

        public bool IsSuaLogon()
        {
            return LogonManager.IsSuaLogon(_g);
        }

        public string SiteName
        {
            get { return _g.Brand.Site.Name.Replace(".", "").ToLower(); }
        }

        public int ApplicationID
        {
            get
            {
                if (Constants.NULL_INT == appId)
                {
                    try
                    {
                        string appIdStr = ConfigurationManager.AppSettings["ApplicationId"];
                        appId = Convert.ToInt32(appIdStr);
                    }
                    catch (Exception e)
                    {
                        _g.ProcessException(e);
                        appId = 1003; //default to jdate id
                    }
                }
                return appId;
            }
        }

        public string ClientSecret
        {
            get
            {
                if (Constants.NULL_STRING == clientSecret)
                {
                    try
                    {
                        clientSecret = ConfigurationManager.AppSettings["ClientSecret"];
                    }
                    catch (Exception e)
                    {
                        _g.ProcessException(e);
                    }
                }
                return clientSecret;
            }
        }

        public string RedirectUrl
        {
            get
            {
                var redirectUrl = "/applications/logon/logon.aspx";
                string destinationUrl = _g.GetDestinationURL();
                if (destinationUrl.Length > 0)
                {
                    redirectUrl += String.Format("?DestinationUrl={0}", HttpContext.Current.Server.UrlEncode(destinationUrl));
                }
                redirectUrl = FrameworkGlobals.LinkHrefSSL(redirectUrl);
                return redirectUrl;
            }
            set
            {
                //do nothing
            }
        }

        public RedirectUrlSet RedirectSet
        {
            get
            {
                var redirectUrlSet = new RedirectUrlSet();
                redirectUrlSet.SuaParams = new Dictionary<string, string>();
                bool shouldRedirectToBetaRedesign = false;
                // if beta turned on, compute which site to send member
                if (LogonManager.IsBetaRedesignEnabled(_g))
                {
                    // if cookie exists use cookie value
                    if (CookieUtils.CookieExists(SuaLogonManager.CookieBetaRedesign))
                    {
                        shouldRedirectToBetaRedesign =
                            (CookieUtils.GetCookieFromRequestOrResponse(SuaLogonManager.CookieBetaRedesign)
                                .Value.ToLower() ==
                             Boolean.TrueString.ToLower());
                    }
                    else
                    {
                        // else use percentage based algorithm
                        int betaRedesignPercent = GetBetaRedesignPercent(_g);
                        if (betaRedesignPercent > 0)
                        {
                            var rand = new Random();
                            var val = 0.00001 + rand.NextDouble()*100.0;

                            shouldRedirectToBetaRedesign = (val <= betaRedesignPercent);
                        }
                    }

                    // setup redirect urls. RedirectUrl == beta url.  DefaultRUrl == bedrock url.
                    redirectUrlSet.RedirectUrl = string.Empty;
                    redirectUrlSet.RedirectUrl = GetBetaRedesignUrl(_g, "verify");
                    string destinationUrl = _g.GetDestinationURL();
                    if (destinationUrl.Length > 0)
                    {
                        redirectUrlSet.RedirectUrl += String.Format("?redirecturl={0}",
                            HttpContext.Current.Server.UrlEncode(destinationUrl));
                    }

                    // send bedrock url in case member has opted out of beta so we may redirect back to bedrock
                    redirectUrlSet.SuaParams["defaultRUrl"] = HttpUtility.UrlEncode(RedirectUrl);

                    // set beta cookie so next time, member will be beta-ed automatically
                    if (shouldRedirectToBetaRedesign)
                    {
                        CreateAndStoreBetaFlag(shouldRedirectToBetaRedesign);
                    }

                    // send flags to SUA to set/update beta attributes on member
                    redirectUrlSet.SuaParams[SuaLogonManager.CookieBetaRedesign] = shouldRedirectToBetaRedesign.ToString();
                    redirectUrlSet.SuaParams[SuaLogonManager.CookieBetaRedesignOffered] = Boolean.FalseString;
                }
                else // if beta is off, always send to bedrock
                {
                    redirectUrlSet.RedirectUrl = RedirectUrl;
                }
                
                return redirectUrlSet;
            }
            set
            {
                //do nothing
            }
        }

        /// <summary>
        ///     Cookie to hold beta value
        /// </summary>
        /// <param name="isBeta"></param>
        public void CreateAndStoreBetaFlag(bool isBeta)
        {
            var domain = (IsTopLevelDomainCookiesEnabled())
                ? "."+_g.Brand.Uri.ToLower()
                : CookieUtils.GetCurrentDomain();
            CookieUtils.AddCookie(SuaLogonManager.CookieBetaRedesign, isBeta.ToString(), (7*24), false, domain, SuaLogonManager.RootPath);
        }

        /// <summary>
        /// this property is only for internal code use. On the round trip from SUA the redirect url was getting lost. 
        /// </summary>
        public string PreserverRedirectURLForAutoLogin
        {
            get;
            set;
        }

        public void RedirectToSua(string url)
        {
            _g.Transfer(url);
        }

        public void ProcessSuccessfulLogon(int memberId, bool startLogonSession, bool isAutoLogon) 
        {
            Member.ServiceAdapters.Member member = MemberSA.Instance.GetMember(memberId, MemberLoadFlags.None);

            if (isAutoLogon)
            {
                Lib.Util util = new Lib.Util();
                util.BetaRedirect("");
            }

            if (startLogonSession)
            {
                if (member == null)
                {
                    var exception = new Exception(string.Format("SUA response. Null member from API"));
                    LoggingManager.LogInfoMessage("LogonManager.ProcessSuaResponse()", exception.ToString());
                    throw exception;
                }
                _g.StartLogonSession(member.MemberID, _g.Brand.Site.Community.CommunityID, _g.Brand.BrandID,
                                     member.EmailAddress, member.GetUserName(_g.Brand), false, Constants.NULL_INT);
            }

            //create cookies for IM if top level cookies are not enabled and sua logon not enabled
            if (!IsTopLevelDomainCookiesEnabled() && IsSuaLogon())
            {
                var scope = HttpContext.Current.Request.QueryString["scope"];
                var cookieExpirationHours = (scope == "long") ? SuaLogonManager.ScopeLong * 24 : SuaLogonManager.ScopeShort * 24;
                OAuthTokens authTokens = SuaLogonManager.GetTokensFromCookies();
                ImOAuthLogin(cookieExpirationHours, authTokens.AccessToken, scope, member);
            }

            // Auto renewal info. We need this info for TopAuxMenu, so we don't want to call Renewal service each time a page is loaded,
            // so we need to load the member attribute with it here.
            try
            {
                if (_g != null && _g.Member != null && _g.Brand != null)
                {
                    LoggingManager.LogInfoMessage("LogonManager.ProcessSuccesfulLogon", string.Format("MemberID:{0} SiteID:{1} startLogonSession:{2} isAutoLogon:{3}", _g.Member.MemberID, _g.Brand.Site.SiteID, startLogonSession, isAutoLogon));
                }
                else
                {
                    LoggingManager.LogInfoMessage("LogonManager.ProcessSuccesfulLogon", string.Format("_g is null:{0} Member is null:{1} Brand is null:{2}",
                        _g == null ? "yes" : "no",
                        _g == null || _g.Member == null ? "yes" : "no",
                        _g == null || _g.Brand == null ? "yes" : "no"));
                }

                RenewalSubscription renewalSub = RenewalManager.Instance.GetRenewalSubscription(_g.Member.MemberID, _g.Brand);

                if (renewalSub != null)
                {
                    LoggingManager.LogInfoMessage("LogonManager.ProcessSuccesfulLogon", string.Format("RenewalSub.IsRenewalEnabled:{0}", renewalSub.IsRenewalEnabled));

                    _g.Member.SetAttributeInt(_g.Brand, "AutoRenewalStatus", renewalSub.IsRenewalEnabled ? 1 : 0);
                    MemberSA.Instance.SaveMember(_g.Member);
                }
                else
                {
                    LoggingManager.LogInfoMessage("LogonManager.ProcessSuccesfulLogon", "RenewalSub came back NULL.");
                }
            }
            catch (Exception ex)
            {
                // we don't want a failure calling Renewal service to discontinue the member's successful login process
                LoggingManager.LogInfoMessage("LogonManager.ProcessSuccesfulLogon", string.Format("Failure retrieving the member's auto renewal status. ExceptionMessage:{0}", ex.Message));
            }

            // Omniture Analytics - Event Tracking
            if (Convert.ToBoolean(SettingsService.GetSettingFromSingleton("ANALYTICS_OMNITURE", _g.Brand.Site.Community.CommunityID, _g.Brand.Site.SiteID)))
            {
                // Tracking Omniture Traffic Variables on successful login.
                _g.Session.Add("OmnitureLoggedIn", "true",
                    Session.ValueObjects.SessionPropertyLifetime.Temporary);
                // Tracking using Google Analytics on successful login.
                _g.Session.Add("GoogleAnalyticsLoggedIn", "true",
                   Session.ValueObjects.SessionPropertyLifetime.Temporary);

                if (isAutoLogon)
                {
                    // SUA auto login, this will make event20 fire to Omniture.
                    _g.Session.Add("OmnitureAutoLoggedIn", "true", Matchnet.Session.ValueObjects.SessionPropertyLifetime.Temporary);
                }
            }

            //for forced page on logon
            _g.Session.Add("LoggedIn", "true", Session.ValueObjects.SessionPropertyLifetime.Temporary);
            MigrateMingleCookies();
            var hasJdateReligion = true;
            if (_g.Brand.Site.Community.CommunityID == (int)WebConstants.COMMUNITY_ID.JDate)
            {
                if (_g.Member.GetAttributeInt(_g.Brand, "JdateReligion", Constants.NULL_INT) == Constants.NULL_INT)
                {
                    hasJdateReligion = false;
                }
            }

            //record auto-login for logon history
            if (isAutoLogon && _g != null && _g.Member != null)
            {
                try
                {
                    RecordAutoLogonAction(_g.Brand, _g.Member.EmailAddress, _g.ClientIP, _g.GetUserAgent(), _g.GetHeaders(), _g.Member.MemberID, ApplicationID, false, true);
                }
                catch (Exception ex)
                {
                    LoggingManager.LogInfoMessage("LogonManager.ProcessSuccesfulLogon", string.Format("Failure recording auto-login action with Mingle for logon history. ExceptionMessage:{0}", ex.Message));
                }
            }

            FrameworkGlobals.SetConnectCookie(_g);


            ///////////////////////////////////////////////
            //Potential redirects/transfers below

            if (!hasJdateReligion)
            {
                var alreadyOffered = _g.Session.GetString("ATTR_EDIT_JDATERELIGION");
                if (String.IsNullOrEmpty(alreadyOffered))
                    _g.Transfer(
                        "/Applications/MemberProfile/AttributeEdit.aspx?Attribute=JDatereligion&DestinationURL=" +
                        HttpUtility.UrlEncode(_g.GetDestinationURL()));
            }


            //redirect to destination URL if set
            if (_g.GetDestinationURL().Length > 0)
            {
                // if the logon contains a destination to the IM page and the current member is valid, proceed to transfer 
                // the user to viewProfile, otherwise send them to the IM control for any redirection considerations.
                if (_g.DestinationPage == ContextGlobal.Pages.PAGE_INSTANT_MESSENGER_MAIN &&
                    _g.ValidateMemberContact(_g.RecipientMemberID))
                {
                    _g.Transfer("/Applications/MemberProfile/ViewProfile.aspx?memberID=" + _g.RecipientMemberID);
                }
                else
                {
                    _g.DestinationURLRedirect();
                }
            }
            else if(this.PreserverRedirectURLForAutoLogin != null)
            {
                _g.Transfer(this.PreserverRedirectURLForAutoLogin);
            }

            // if we are here, either no destination URL exists or the transfer to it was not allowed
            member = MemberSA.Instance.GetMember(_g.Session.GetInt(WebConstants.SESSION_PROPERTY_NAME_MEMBERID), MemberLoadFlags.None);
            var globalStatusMask = member.GetAttributeInt(_g.Brand, "GlobalStatusMask", 0);

            if (Matchnet.Lib.Util.Util.CBool(globalStatusMask & WebConstants.EMAIL_VERIFIED, true) || (_g.EmailVerify.EmailVerificationBlocking == WebConstants.ForcedBlockingMask.NotBlocked))
            {
                if (PhotoGalleryManager.Instance.IsPhotoGalleryAsHomePageEnabled(member, _g.Brand))
                {
                    _g.Transfer("/Applications/PhotoGallery/PhotoGallery.aspx");
                }
                else
                {
                    _g.Transfer("/Applications/Home/Default.aspx");
                }
                
            }
            else
            {
                _g.Transfer("/Applications/MemberServices/VerifyEmailNag.aspx");
            }
        }

        private void ImOAuthLogin(int cookieExpirationHours, string accessToken, string scope, Member.ServiceAdapters.Member member)
        {
            bool isPayingMember = false;
            try
            {
                isPayingMember = member.IsPayingMember(_g.Brand.Site.SiteID);
            }
            catch (Exception e)
            {
                LoggingManager.LogException(e, member, _g.Brand, "LogonManager.ImOAuthLogin()");
            }
            finally
            {
                var oauthTokens = new Spark.Common.RestConsumer.V2.Models.OAuth2.OAuthTokens()
                {
                    AccessExpiresTime = DateTime.Now.AddHours(cookieExpirationHours),
                    AccessToken = accessToken,
                    MemberId = member.MemberID,
                    ExpiresIn = cookieExpirationHours * 60,
                    IsPayingMember = isPayingMember
                };
                string tokenEnvironmentForCookie = SettingsManager.GetTokenEnvironmentForCookie();
                LoginManager.Instance.SaveTokensToCookies(null, oauthTokens, true, member.EmailAddress, tokenEnvironmentForCookie, _g.Brand.Site.Name);
            }
        }
        /// <summary>
        ///     If admin imerpsonate, ignore SUA.
        /// </summary>
        public void AdminImpersonate()
        {
            // check to see if this is a start of an impersonation session
            if (HttpContext.Current.Request.Params["imptoken"] != null)
            {
                var impersonateToken =
                    ImpersonationManager.Instance.ValidateImpersonateToken(
                        HttpContext.Current.Request.Params["imptoken"]);

                if (impersonateToken != null)
                {
                    //create bedrock session
                    var member = MemberSA.Instance.GetMember(impersonateToken.ImpersonateMemberId,
                                                             MemberLoadFlags.None);
                    var brand = Content.ServiceAdapters.BrandConfigSA.Instance.GetBrandByID(
                        impersonateToken.ImperonateBrandId);

                    _g.StartLogonSession(member.MemberID, brand.Site.Community.CommunityID, brand.BrandID,
                                         member.EmailAddress, member.GetUserName(brand), false,
                                         impersonateToken.AdminMemberId);

                    ImpersonationManager.Instance.ExpireImpersonateToken(impersonateToken.AdminMemberId);

                    MemberSA.Instance.SaveMemberLastLogon(member.MemberID, brand.BrandID, impersonateToken.AdminMemberId);

                    //create SUA token
                    //1. needed by bedrock for chat and IM history
                    //2. needed for redirects to beta for norbert users
                    Spark.Common.RestConsumer.V2.Models.OAuth2.OAuthTokens oAuthToken = InternalApiAdapter.Instance.CreateApiAccessToken(_g, member, impersonateToken.AdminMemberId);

                    if (oAuthToken != null)
                    {
                        Spark.Common.RestConsumer.V2.Models.Content.BrandConfig.Community restCommunity = new Spark.Common.RestConsumer.V2.Models.Content.BrandConfig.Community();
                        restCommunity.Id = brand.Site.Community.CommunityID;
                        restCommunity.Name = brand.Site.Community.Name;

                        Spark.Common.RestConsumer.V2.Models.Content.BrandConfig.Site restSite = new Spark.Common.RestConsumer.V2.Models.Content.BrandConfig.Site();
                        restSite.Community = restCommunity;
                        restSite.Id = brand.Site.SiteID;
                        restSite.Name = brand.Site.Name;

                        Spark.Common.RestConsumer.V2.Models.Content.BrandConfig.Brand restBrand = new Spark.Common.RestConsumer.V2.Models.Content.BrandConfig.Brand();
                        restBrand.Site = restSite;
                        restBrand.Id = brand.BrandID;
                        restBrand.Uri = brand.Uri;

                        SuaLogonManager.SaveTokensToCookies(null, oAuthToken, restBrand, "short", IsTopLevelDomainCookiesEnabled(_g));
                    }
                    else
                    {
                        SuaLogonManager.RemoveTokenCookiesSua();
                    }

                    _g.Transfer("/Applications/Home/Default.aspx");
                }
            }
        }

        #endregion


        //this is migration routine to migrate mingle cookies, has a lot of hardcoding
        //should be hopefully called once per mingle user
        //and then can be deleted with time.....which probably means never
        private void MigrateMingleCookies()
        {
            try
            {
                if (_g.Member == null)
                    return;

                var communityid = _g.Brand.Site.Community.CommunityID;
                if (!FrameworkGlobals.IsMingleSite(communityid))
                {
                    return;
                }
                var cookiesmigrated = _g.Member.GetAttributeInt(_g.Brand, "MingleCookieMigrationFlag",
                                                                Constants.NULL_INT);

                if (cookiesmigrated == 1)
                    return;

                var prm = Conversion.CInt(FrameworkGlobals.GetCookie("camp_src", "", false), Constants.NULL_INT);
                var luggage = FrameworkGlobals.GetCookie("camp_opt", "", false);
                var banner = Conversion.CInt(FrameworkGlobals.GetCookie("camp_adid", "", false), Constants.NULL_INT);
                if (prm > 0)
                {
                    _g.Member.SetAttributeInt(_g.Brand, WebConstants.SESSION_PROPERTY_NAME_PROMOTIONID, prm);
                }
                if (!String.IsNullOrEmpty(luggage))
                {
                    _g.Member.SetAttributeText(_g.Brand, WebConstants.ATTRIBUTE_NAME_BRANDLUGGAGE, luggage,
                                               TextStatusType.Auto);
                }
                if (banner > 0)
                {
                    _g.Member.SetAttributeInt(_g.Brand, WebConstants.ATTRIBUTE_NAME_BRANDBANNERID, banner);
                }

                _g.Member.SetAttributeInt(_g.Brand, "MingleCookieMigrationFlag", 1);

                MemberSA.Instance.SaveMember(_g.Member);
                //
                FrameworkGlobals.RemoveCookie("camp_src");
                FrameworkGlobals.RemoveCookie("camp_opt");
                FrameworkGlobals.RemoveCookie("camp_adid");
                FrameworkGlobals.RemoveCookie("singlesession");
                FrameworkGlobals.RemoveCookie("msession");
                FrameworkGlobals.RemoveCookie("encrPassword");
                FrameworkGlobals.RemoveCookie("loginusername");
                FrameworkGlobals.RemoveCookie("mbox");
                FrameworkGlobals.RemoveCookie("s_cc");
                FrameworkGlobals.RemoveCookie("s_sq");
                FrameworkGlobals.RemoveCookie("s_vi");
                FrameworkGlobals.RemoveCookie("camp_cat");
                FrameworkGlobals.RemoveCookie("camp_opt2");
                FrameworkGlobals.RemoveCookie("camp_omni");
                FrameworkGlobals.RemoveCookie("_gads");
                FrameworkGlobals.RemoveCookie("loginemail");
            }
            catch (Exception ex)
            {
                _g.ProcessException(ex);
            }
        }

        public OAuthTokens GetTokensFromCookies()
        {
            OAuthTokens tokens;
            bool isSuaLogon = IsSuaLogon();
            if (isSuaLogon)
            {
                tokens = SuaLogonManager.GetTokensFromCookies();
            }
            else
            {
                tokens = LoginManager.Instance.GetTokensFromCookies();
            }
            return tokens;
        }

        public LogActionResponse RecordAutoLogonAction(Brand brand, string emailAddress, string ipAddress, string userAgent,
                                           string httpHeaders, int memberId, int applicationId, bool throttled, bool loginSuccessful)
        {
            LoggingManager.LogInfoMessage("LogonManager.RecordAutoLogonAction", string.Format("MemberID:{0} SiteID:{1} emailAddress:{2} ipAddress:{3}", memberId, brand.Site.SiteID, emailAddress, ipAddress));
            if (IsLoginFraudEnabled(ipAddress, brand))
            {
                LoginFraudClient loginFraudClient = new LoginFraudClient(brand, brand.Site.SiteID);

                LogActionResponse response = loginFraudClient.LogAction(emailAddress, LogActionType.AutoLogin, ipAddress, userAgent ?? string.Empty,
                                                   httpHeaders ?? string.Empty, memberId, applicationId, throttled,
                                                   loginSuccessful);

                return response;
            }
            else
                return null;
        }

        public bool IsLoginFraudEnabled(string ipAddress, Brand brand)
        {
            return SettingsManager.GetSettingBool(SettingConstants.MINGLE_LOGIN_FRAUD_ENABLED, brand) && !string.IsNullOrEmpty(ipAddress);
        }
    }

}