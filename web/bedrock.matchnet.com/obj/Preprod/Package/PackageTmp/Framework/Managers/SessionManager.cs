﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Session.ServiceAdapters;
using Matchnet.Session.ValueObjects;
using Matchnet.Session.ValueObjects.ServiceDefinitions;
using Matchnet.Web.Framework.HTTPContextWrappers;
using Matchnet.Web.Interfaces;

namespace Matchnet.Web.Framework.Managers
{
    public class SessionManager:AbstractManagerBaseClass
    {
        private bool _isDevMode = false;
        private ISessionSA _sessionService = null;
        
        public ISessionSA SessionService
        {
            get
            {
                return _sessionService ?? (ISessionSA)SessionSA.Instance;
            }
            set { _sessionService = value; }
        }
        
        public UserSession GetCurrentSession(Brand brand)
        {
            _isDevMode = SettingsManager.GetSettingBool(SettingConstants.IS_DEVELOPMENT_MODE, brand);

            if (IsApiSession())
            {
                return GetSessionFromID();
            }
            else
            {
                return SessionService.GetSession(_isDevMode);
            }
        }

        private bool IsApiSession()
        {
            string session = CurrentRequest["SPWSCYPHER"];
            return !String.IsNullOrEmpty(session) ? true : false;
        }

        public UserSession GetSessionFromID()
        {
            UserSession session = null;
            if (CurrentRequest["SPWSCYPHER"] != null)
            {
                string key = HttpUtility.UrlDecode(CurrentRequest["SPWSCYPHER"]).Replace(" ", "+");
                if (!String.IsNullOrEmpty(key))
                {
                    string keyret = Matchnet.Security.Crypto.Decrypt(WebConstants.SPARK_WS_CRYPT_KEY, key);
                    session = SessionService.GetSession(keyret);
                }

            }
            if (session == null)
                session =SessionService.GetSession(_isDevMode);

            return session;
        }
    }
}