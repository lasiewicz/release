﻿using System;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;

namespace Matchnet.Web.Framework.Search
{

	public class XmlSerializerSectionHandler :
			  IConfigurationSectionHandler
	{
		public Object Create(
			  Object parent,
			  Object configContext,
			  XmlNode section
		){
			return new XmlSerializer(
					Type.GetType(
						(String)section.CreateNavigator().Evaluate("string(@type)")
					)
				).Deserialize(new XmlNodeReader(section));
		}
	}
}