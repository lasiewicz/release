namespace Matchnet.Web.Framework.Search
{
	public enum SortType
	{
		Newest,
		MostActive,
		ClosestToYou,
		BestMatch,
		None,
	}
}