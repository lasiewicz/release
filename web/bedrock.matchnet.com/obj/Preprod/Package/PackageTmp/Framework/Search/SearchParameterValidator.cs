﻿using System;
using System.Collections.Generic;

namespace Matchnet.Web.Framework.Search
{
	/// <summary>
	/// This class is used for validating parameters, and returning error info when
	/// parameters go bad.
	/// </summary>
	public static class SearchParameterValidator
	{
		private static readonly Dictionary<String, Boolean> AcceptablePreferenceValues
			= new Dictionary<String, Boolean>
		{
			{Constants.Literals.FCharacter.ToString(), true},	 
 			{Constants.Literals.MCharacter.ToString(), true},
		};

		/// <summary>
		/// Determines whether the query is valid. A query can still be
		/// valid if it's a null or empty string, provided the location is not
		/// a null or empty string.
		/// </summary>
		/// <param name="query">The query.</param>
		/// <param name="location">The location.</param>
		/// <param name="error">The error.</param>
		/// <returns>
		/// 	<c>true</c> if the query is valid; otherwise, <c>false</c>.
		/// </returns>
		public static Boolean IsQueryValid(String query, String location, out String error)
		{
			if (String.IsNullOrEmpty(location) && String.IsNullOrEmpty(query))
			{
				error = Constants.ResourceKeys.NoQueryError;
				return false;
			}

			if (AreSearchTermsTooLong(query))
			{
				error = Constants.ResourceKeys.SearchTermsTooLong;
				return false;
			}

			error = String.Empty;
			return true;
		}

		/// <summary>
		/// Determines whether an age range is valid. The acceptable range of values are
		/// between <see cref="Constants.Thresholds.MinimumAgeRangeValue"/> and
		/// <see cref="Constants.Thresholds.MaximumAgeRangeValue"/>, inclusive.
		/// </summary>
		/// <param name="ageRange">The age range.</param>
		/// <param name="error">The error.</param>
		/// <returns>
		/// 	<c>true</c> if the age range is valid; otherwise, <c>false</c>.
		/// </returns>
		public static Boolean IsAgeRangeValid(String ageRange, out String error)
		{
			if (String.IsNullOrEmpty(ageRange))
			{
				error = Constants.ResourceKeys.NoAgeRangeError;
				return false;
			}

			Int32 ageRangeValue;

			if (Int32.TryParse(ageRange, out ageRangeValue) == false)
			{
				error = Constants.ResourceKeys.InvalidAgeRange;
				return false;
			}

			if (ageRangeValue < Constants.Thresholds.MinimumAgeRangeValue ||
				 ageRangeValue > Constants.Thresholds.MaximumAgeRangeValue)
			{
				error = Constants.ResourceKeys.InvalidAgeRange;
				return false;
			}

			error = String.Empty;
			return true;
		}

		/// <summary>
		/// Determines whether a location is valid. A location can still be
		/// valid if it's a null or empty string, provided the query parameter is not
		/// a null or empty string.
		/// </summary>
		/// <param name="location">The location.</param>
		/// <param name="query">The query.</param>
		/// <param name="error">The error.</param>
		/// <returns>
		/// 	<c>true</c> if [is location valid] [the specified location]; otherwise, <c>false</c>.
		/// </returns>
		public static Boolean IsLocationValid(String location, String query, out String error)
		{
			if(
					String.IsNullOrEmpty(query) &&
					(String.IsNullOrEmpty(location) || location == Constants.Literals.InitialLocationText)
			)
			{
				error = Constants.ResourceKeys.NoLocationError;
				return false;
			}

			if (IsLocationTooLong(location))
			{
				error = Constants.ResourceKeys.LocationTooLongError;
				return false;
			}

			error = String.Empty;
			return true;
		}

		/// <summary>
		/// Determines whether gender preference is valid..
		/// </summary>
		/// <param name="genderPreference">The gender preference.</param>
		/// <param name="preferenceMappings">The preference mappings.</param>
		/// <param name="error">The error.</param>
		/// <returns>
		/// 	<c>true</c> if the gender preference is valid; otherwise, <c>false</c>.
		/// </returns>
		public static Boolean IsGenderPreferenceValid(String genderPreference, 
			Dictionary<String,String> preferenceMappings, out String error)
		{

			if (String.IsNullOrEmpty(genderPreference))
			{
				error = Constants.ResourceKeys.MissingPreferences;
				return false;
			}

			if (preferenceMappings.ContainsKey(genderPreference) == false)
			{
				error = Constants.ResourceKeys.UnidentifiedPreferences;
				return false;
			}

			error = String.Empty;
			return true;
		}

		/// <summary>
		/// Determines whether seeker is valid. There are two acceptable values: "m", and "f".
		/// </summary>
		/// <param name="seeker">The seeker.</param>
		/// <param name="error">The error.</param>
		/// <returns>
		/// 	<c>true</c> if seeker is valid; otherwise, <c>false</c>.
		/// </returns>
		public static Boolean IsSeekerValid(String seeker, out String error)
		{
			if (String.IsNullOrEmpty(seeker) || seeker == Constants.Literals.ChooseOption)
			{
				error = Constants.ResourceKeys.NoSeekerError;
				return false;
			}

			if (AcceptablePreferenceValues.ContainsKey(seeker) == false)
			{
				error = Constants.ResourceKeys.InvalidPreferenceError;
				return false;
			}

			error = String.Empty;
			return true;
		}

		/// <summary>
		/// Determines whether seeking is valid. There are two acceptable values: "m", and "f".
		/// </summary>
		/// <param name="seeking">The seeking.</param>
		/// <param name="error">The error.</param>
		/// <returns>
		/// 	<c>true</c> if seeking is valid; otherwise, <c>false</c>.
		/// </returns>
		public static Boolean IsSeekingValid(String seeking, out String error)
		{
			if (String.IsNullOrEmpty(seeking) || seeking == Constants.Literals.ChooseOption)
			{
				error = Constants.ResourceKeys.NoSeekerError;
				return false;
			}

			if (AcceptablePreferenceValues.ContainsKey(seeking) == false)
			{
				error = Constants.ResourceKeys.InvalidPreferenceError;
				return false;
			}

			error = String.Empty;
			return true;
		}

		private static Boolean AreSearchTermsTooLong(String searchTerms)
		{
			return searchTerms.Length > Constants.Thresholds.MaximumSearchTermLength ||
				searchTerms.Split(Constants.Literals.Space).Length >
					Constants.Thresholds.MaximumNumberOfKeywords;
		}

		private static Boolean IsLocationTooLong(String location)
		{
			return String.IsNullOrEmpty(location) == false &&
					 location.Length > Constants.Thresholds.MaximumLocationLength;
		}
	}
}