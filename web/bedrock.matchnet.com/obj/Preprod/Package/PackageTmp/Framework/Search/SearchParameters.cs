﻿using System;

namespace Matchnet.Web.Framework.Search
{
	/// <summary>
	/// SearchParameters represents a set of parameters used in
	/// the keyword search namespace. This is a glorified parameter object.
	/// </summary>
	[Serializable]
	public sealed class SearchParameters : IEquatable<SearchParameters>
	{
		#region Null

		public static SearchParameters Null
		{
			get { return new SearchParameters(true); }
		}

		#endregion Null

		#region Constants

		/// <summary>
		/// This is the separator used to serialize a SearchParameters
		/// instance into a string, and parse back from a string.
		/// </summary>
		public const String Separator = "~";

		#endregion Constants

		#region Properties

		/// <summary>
		/// Gets or sets the query.
		/// </summary>
		/// <value>The query.</value>
		public String Query { get; set; }

		/// <summary>
		/// Gets or sets the seeker parameter.
		/// </summary>
		/// <value>The seeker parameter.</value>
		public String Seeker { get; set; }

		/// <summary>
		/// Gets or sets the seeking parameter.
		/// </summary>
		/// <value>The seeking parameter.</value>
		public String Seeking { get; set; }

		/// <summary>
		/// Gets or sets the age range parameter.
		/// </summary>
		/// <value>The age range parameter.</value>
		public String AgeRange { get; set; }

		/// <summary>
		/// Gets or sets the location parameter.
		/// </summary>
		/// <value>The location parameter.</value>
		public String Location { get; set; }

		/// <summary>
		/// Gets or sets the page.
		/// </summary>
		/// <value>The page.</value>
		public String Page { get; set; }

        /// <summary>
        /// Gets or sets the community id.
        /// </summary>
        /// <value>The community id.</value>
        public int CommunityID { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether this instance is the null instance.
		/// </summary>
		/// <value><c>true</c> if this instance is null; otherwise, <c>false</c>.</value>
		public Boolean IsNull { get; private set; }

		#endregion Properties

		#region Constructors

		private SearchParameters(Boolean isNull)
		{
			IsNull = isNull;
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="SearchParameters"/> class.
		/// </summary>
		public SearchParameters()
			: this(String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, Matchnet.Constants.NULL_INT)
		{

		}

		/// <summary>
		/// Initializes a new instance of the <see cref="SearchParameters"/> class.
		/// </summary>
		/// <param name="query">The query.</param>
		/// <param name="seeker">The seeker parameter.</param>
		/// <param name="seeking">The seeking parameter.</param>
		/// <param name="ageRange">The age range parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <param name="page">The page.</param>
		public SearchParameters(String query, String seeker, String seeking, String ageRange, String location, String page, int communityID)
			: this(false)
		{
			Query = query;
			Seeker = seeker;
			Seeking = seeking;
			AgeRange = ageRange;
			Location = location;
			Page = page;
            CommunityID = communityID;
		}

		#endregion Constructors

		#region Instance Methods

		/// <summary>
		/// Serves as a hash function for a particular type.
		/// </summary>
		/// <returns>
		/// A hash code for the current <see cref="T:System.Object"/>.
		/// </returns>
		public override Int32 GetHashCode()
		{
			return (Query ?? String.Empty).GetHashCode() ^ 
					 (AgeRange ?? String.Empty).GetHashCode() ^
					 (Location ?? String.Empty).GetHashCode() ^
					 (Seeker ?? String.Empty).GetHashCode() ^
					 (Seeking ?? String.Empty).GetHashCode() ^
                     CommunityID.GetHashCode();
		}

		/// <summary>
		/// Determines whether the specified <see cref="T:System.Object"/> is equal to the current <see cref="T:System.Object"/>.
		/// </summary>
		/// <param name="obj">The <see cref="T:System.Object"/> to compare with the current <see cref="T:System.Object"/>.</param>
		/// <returns>
		/// true if the specified <see cref="T:System.Object"/> is equal to the current <see cref="T:System.Object"/>; otherwise, false.
		/// </returns>
		/// <exception cref="T:System.NullReferenceException">
		/// The <paramref name="obj"/> parameter is null.
		/// </exception>
		public override Boolean Equals(object obj)
		{
			return Equals(obj as SearchParameters);
		}

		/// <summary>
		/// Indicates whether the current object is equal to another object of the same type.
		/// </summary>
		/// <param name="other">An object to compare with this object.</param>
		/// <returns>
		/// true if the current object is equal to the <paramref name="other"/> parameter; otherwise, false.
		/// </returns>
		public Boolean Equals(SearchParameters other)
		{
			if (other == null) return false;

			return other.Query == Query &&
					 other.AgeRange == AgeRange &&
					 other.Location == Location &&
					 other.Seeker == Seeker &&
					 other.Seeking == Seeking &&
					 other.Page == Page &&
                     other.CommunityID == CommunityID;
                    
		}

		/// <summary>
		/// Returns a <see cref="T:System.String"/> that represents the current <see cref="T:System.Object"/>.
		/// </summary>
		/// <returns>
		/// A <see cref="T:System.String"/> that represents the current <see cref="T:System.Object"/>.
		/// </returns>
		public override String ToString()
		{
			return String.Join(
				Separator,
				new[]
				{
					Query,
					Seeker,
					Seeking,
					AgeRange,
					Location,
					Page, 
                    CommunityID.ToString()
				}
			);
		}

		#endregion Instance Methods

		#region Static Methods

		/// <summary>
		/// Performs an implicit conversion from <see cref="System.String"/> to <see cref="Matchnet.Web.Framework.Search.SearchParameters"/>.
		/// </summary>
		/// <param name="searchParameters">The search parameters.</param>
		/// <returns>The result of the conversion.</returns>
		public static implicit operator SearchParameters(String searchParameters)
		{
			try
			{
				return Parse(searchParameters);
			}
			catch
			{
				return Null;
			}
		}

		/// <summary>
		/// Tries the parse a <see cref="Matchnet.Web.Framework.Search.SearchParameters"/>
		/// instance from a string.
		/// </summary>
		/// <param name="parameterString">The parameter string to parse.</param>
		/// <param name="searchParameters">The search parameters object that gets created.</param>
		public static void TryParse(
			String parameterString, out SearchParameters searchParameters)
		{
			try
			{
				searchParameters = Parse(parameterString);
			}
			catch
			{
				searchParameters = Null;
			}
		}

		/// <summary>
		/// Parses the specified search parameters.
		/// </summary>
		/// <param name="searchParameters">The search parameters.</param>
		/// <returns></returns>
		/// <exception cref="ArgumentOutOfRangeException">
		/// Thrown if the <paramref name="searchParameters"/> parameter
		/// isn't formatted correctly.
		/// </exception>
		public static SearchParameters Parse(String searchParameters)
		{
			try
			{
				var properties = searchParameters.Split(Separator[0]);

				return new SearchParameters
				{
					Query = properties[0],
					Seeker = properties[1],
					Seeking = properties[2],
					AgeRange = properties[3],
					Location = properties[4],
					Page = properties[5],
                    CommunityID = Conversion.CInt(properties[6])
				};
			}
			catch (Exception exc)
			{
				throw new ArgumentOutOfRangeException("Could not make conversion", exc);
			}
		}

		#endregion Static Methods
	}
}