﻿using System;
using System.Web;

namespace Matchnet.Web.Framework.Search
{
	/// <summary>
	/// Contains methods to help get parameter values using partial parameter
	/// names, store search preferences in a cookie, etc. It's a general helper
	/// class. for interaction with the Request and Response objects.
	/// </summary>
	public static class SearchPageHelper
	{
		/// <summary>
		/// Gets a parameter value from a HttpRequest, using the partial
		/// name of the parameter. This is to get around the bizarre
		/// name mangling that .NET uses for controls in user controls.
		/// </summary>
		/// <param name="partialParamName">Partial name of the param.</param>
		/// <param name="request">The request.</param>
		/// <returns>
		/// <see cref="string.Empty"/> if the parameter isn't found; the value of
		/// the parameter otherwise.
		/// </returns>
		public static String GetParamValue(String partialParamName, HttpRequest request)
		{
			if (String.IsNullOrEmpty(partialParamName)) return String.Empty;

			if (request == null) return String.Empty;

			if (request.Params == null || request.Params.Count < 1) return String.Empty;

			foreach (var param in request.Params)
			{
				var paramValue = (String)param;
				if (paramValue.Contains(partialParamName)) return request.Params[paramValue];
			}

			return String.Empty;
		}

		/// <summary>
		/// Handles omniture page tracking.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <param name="globalContext">The global context.</param>
		public static void HandleOmniture(HttpRequest request, ContextGlobal globalContext)
		{
			if (request.Params["kc"] == null) return;

			switch (request.Params["kc"])
			{
				case "gam":
					globalContext.AnalyticsOmniture.Prop14 = "Keyword Banner - Keyword";
					break;
				case "overlay":
					globalContext.AnalyticsOmniture.Prop14 = "Homepage Overlay: Keyword Search";
					break;
				case "all":
					globalContext.AnalyticsOmniture.Prop14 = "Keyword Banner - All Keywords";
					break;
				case "tag":
					globalContext.AnalyticsOmniture.Prop14 = "Keyword Tag Cloud";
					break;
				default:
					break;
			}
		}

		/// <summary>
		/// Stores a user's search preferences in a cookie.
		/// </summary>
		/// <param name="searchParameters">The search parameters.</param>
		/// <param name="response">The response.</param>
		public static void StorePreferences(SearchParameters searchParameters, HttpResponse response)
		{
			if (searchParameters == null || searchParameters.IsNull) return;

			var cookie = new HttpCookie(Constants.Session.CookieKeyName)
			{
				Expires = DateTime.UtcNow.AddDays(Constants.Session.CookieExpiration),
			};

			cookie.Values[Constants.Session.SearchPreferencesKey] = searchParameters.ToString();

			response.Cookies.Add(cookie);
		}

		/// <summary>
		/// Gets keyword search preferences from a http request by pulling them from the cookie.
		/// </summary>
		/// <param name="request">The request to pull the prefs from.</param>
		/// <returns>An instance of <see cref="SearchParameters"/>.</returns>
		public static SearchParameters GetKeywordSearchPreferences(HttpRequest request)
		{
			var cookie = request.Cookies[Constants.Session.CookieKeyName];

			return (cookie == null || cookie.Values[Constants.Session.SearchPreferencesKey] == null) ?
						String.Empty : cookie.Values[Constants.Session.SearchPreferencesKey];
		}

		/// <summary>
		/// Gets the keyword search results URL based on cookie values.
		/// </summary>
		/// <param name="request">The request to build the url from.</param>
		/// <returns>A relative url pointing to keyword results.</returns>
		public static String GetKeywordSearchResultsUrl(HttpRequest request)
		{
			var cookie = request.Cookies[Constants.Session.CookieKeyName];

			SearchParameters preference =
				(cookie == null || cookie.Values[Constants.Session.SearchPreferencesKey] == null) ?
				String.Empty : cookie.Values[Constants.Session.SearchPreferencesKey];

			return
				String.Join(
					 String.Empty,
					 new[]
					 {
						 "/Applications/Search/KeywordSearchResults.aspx?st=",
						 preference.Query,
                   Constants.Parameters.SeekerKey,
						 preference.Seeker,
                   Constants.Parameters.SeekingKey,
						 preference.Seeking,
                   Constants.Parameters.AgeKey,
						 preference.AgeRange,
                   Constants.Parameters.LocationKey,
						 preference.Location,
                   "&StartRow=",
						 ConvertPageToStartRow(preference.Page),
                   Constants.Parameters.GalleryKeyValuePair
					 }
				);
		}

		private static String ConvertPageToStartRow(String page)
		{
			Int32 pageNumber;

			if (Int32.TryParse(page, out pageNumber) == false) return Constants.Paging.DefaultPage.ToString();

			if (pageNumber < 1) return 1.ToString();

			return ( (pageNumber - 1) * 12 + 1).ToString();
		}

		public static Int32 GetMemberIdFromOrdinal(Int32 callingMemberId, Int32 ordinal, HttpRequest request, int communityID)
		{
			var searchParameters = GetKeywordSearchPreferences(request);

			var results = KeywordSearcher.GetKeywordSearchResults(
                callingMemberId,
				Int32.Parse(searchParameters.AgeRange),
				searchParameters.Location,
				String.Join(
					Constants.Literals.Two,
					new[]
					{
						searchParameters.Seeker, 
						searchParameters.Seeking
					}
				),
				searchParameters.Query,
				ordinal - 1,
				1,
                communityID
			);

			if (results == null || results.total < 1 || results.getSearchResult.Length < 1) return 0;

			Int32 memberId;

			Int32.TryParse(results.getSearchResult[0].memberId, out memberId);

			return memberId;
		}
	}
}