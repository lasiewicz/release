using System.Collections;

namespace Matchnet.Web.Framework
{
	/// <summary>
	/// Summary description for Notification.
	/// </summary>
	public class Notifications
	{
		public ArrayList NotificationList;

		public Notifications()
		{
			NotificationList = new ArrayList();
		}


        // Free form non-resource driven error message
		public void AddErrorString(string message)
		{
            Notification notification = new Notification(NotificationType.Error);
            notification.Message = message;
			AddNotification(notification);
		}


        // Free form non-resource driven message
		public void AddMessageString(string message)
		{
            Notification notification = new Notification(NotificationType.Normal);
            notification.Message = message;
			AddNotification(notification);
        }

        // 
        // ResourceConstant notification methods

        // Add Error by ResourceConstant
		public void AddError(string resourceConstant)
		{
			AddNotification(new Notification(NotificationType.Error, resourceConstant, null));		
		}

        // Add Error by ResourceConstant and args
        public void AddError(string resourceConstant,  string[] args)
		{
			AddNotification(new Notification(NotificationType.Error, resourceConstant, args));		
		}

        // Add Message by ResourceConstant
		public void AddMessage(string resourceConstant)
		{
			AddNotification(new Notification(NotificationType.Normal, resourceConstant, null));		
		}

        // Add Message by ResourceConstant and args
		public void AddMessage(string resourceConstant, string[] args)
		{
			AddNotification(new Notification(NotificationType.Normal, resourceConstant, args));		
		}

		private void AddNotification(Notification notification) 
		{
			if (!NotificationList.Contains(notification)) 
			{
				NotificationList.Add(notification);
			}
		}

        // Clear utility method
		public void Clear()
		{
			NotificationList.Clear();
		}


	}
}
