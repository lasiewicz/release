﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace Matchnet.Web.Framework.SparkAPI
{
    public class ApiClient
    {
        private static readonly ApiClient _instance = new ApiClient();

        private string _accessToken;

        public static ApiClient GetInstance(string accessToken) 
        {
            _instance._accessToken = accessToken;
            return _instance;
        }

        private ApiClient()
        {
        }

        public string Execute(string url, Dictionary<string, string> urlParams, Dictionary<string, string> data, Method method)
        {
            var client = new RestClient(ConfigurationManager.AppSettings["RESTAuthority"]);
            var request = new RestRequest(url, method);
            request.AddHeader("Accept", "version=V2.1");

            AddParameters(request, data);
            AddUrlSegments(request, urlParams);
            AddAccessToken(request, method);

            var response = client.Execute(request);
            return response.Content;
        }

        public T Execute<T>(string url, Dictionary<string, string> urlParams, Dictionary<string, string> data, Method method) where T : new()
        {
            var client = new RestClient(ConfigurationManager.AppSettings["RESTAuthority"]);
            var request = new RestRequest(url, method);
            request.AddHeader("Accept", "version=V2.1");

            client.AddHandler("application/json", new RestDeserializer());

            AddParameters(request, data);
            AddUrlSegments(request, urlParams);
            AddAccessToken(request, method);

            var response = client.Execute<T>(request);
            return response.Data;
        }

        private void AddParameters(RestRequest request, Dictionary<string, string> data) 
        {
            if (data == null)
                return;

            foreach (var param in data)
                request.AddParameter(param.Key, param.Value);
        }

        private void AddUrlSegments(RestRequest request, Dictionary<string, string> urlParams)
        {
            if (urlParams == null)
                return;
              
            foreach (var param in urlParams)
                request.AddUrlSegment(param.Key, param.Value);
        }

        private void AddAccessToken(RestRequest request, Method method) 
        {
            Parameter parameter = request.Parameters.Find(delegate(Parameter param) {
                return (String.Compare(param.Name, "access_token", true) == 0 || 
                    String.Compare(param.Name, "accesstoken", true) == 0);
            });

            if (parameter != null)
                return;

            request.AddParameter("access_token", _accessToken);
            if (method == Method.POST)
            {
                var url = request.Resource + "?access_token="+_accessToken;

                request.Parameters.RemoveAt(request.Parameters.Count - 1);
                request.Resource = url;
                request.AddUrlSegment("token", _accessToken);
            }
        }

    
    }
}