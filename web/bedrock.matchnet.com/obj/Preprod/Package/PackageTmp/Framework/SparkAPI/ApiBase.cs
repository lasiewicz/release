﻿using RestSharp;
using Spark.Common.RestConsumer.V2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using Matchnet.Web.Framework.Managers;
using Spark.Common.RestConsumer.V2.Models.OAuth2;
using Spark.Authentication.OAuth.V2;

namespace Matchnet.Web.Framework.SparkAPI
{
    public abstract class ApiBase
    {

        protected string AccessToken
        {
            get;
            set; 
        }

        protected ResponseWrapper<T> Get<T>(string accessToken, string url, Dictionary<string, string> urlParams, Dictionary<string, string> data) where T : new()
        {
            AccessToken = accessToken;
            return Execute<T>(url, urlParams, data, Method.GET);
        }

        protected ResponseWrapper<T> Post<T>(string accessToken, string url, Dictionary<string, string> urlParams, Dictionary<string, string> data) where T : new()
        {
            AccessToken = accessToken;
            return Execute<T>(url, urlParams, data, Method.POST);
        }

        private ResponseWrapper<T> Execute<T>(string url, Dictionary<string, string> urlParams, Dictionary<string, string> data, Method method)
        {

            if (this.AccessToken == null)
                throw new Exception("Please set AccessToken");

            var client = ApiClient.GetInstance(this.AccessToken);
            var response = client.Execute<ResponseWrapper<T>>(url, urlParams, data, method);

            // Check to see if unauthorize call to API is attempted 
            // (could be access token required, expired, etc.)
            if (response.Code == (int)HttpStatusCode.Unauthorized)
                throw new AuthorizationException(response.Error.Code, ParseError(response.Error));
            if (response.Code != 0 && response.Code != (int)HttpStatusCode.OK)
                throw new ApiException(ParseError(response.Error))
                {
                    ErrorCode = response.Error.Code,
                    HttpStatusCode = response.Code,
                    Type = this.GetType()
                };

            return response;
        }

        private string ParseError(Error error)
        {
            return String.Format("{0} - {1}", error.Code, error.Message);
        }


        public class AuthorizationException : Exception
        {
            private int _errorCode;
            public int ErrorCode
            {
                get
                {
                    return _errorCode;
                }
                set
                {
                    _errorCode = value;
                }

            }

            public AuthorizationException(string message)
                : base(message)
            {

            }

            public AuthorizationException(int errorCode, string message)
                : base(message)
            {
                _errorCode = errorCode;
            }
        }

        public class ApiException : Exception
        {
            public int HttpStatusCode { get; set; }
            public int ErrorCode { get; set; }
            public Type Type { get; set; }

            public ApiException(string message)
                : base(message)
            {

            }
        }
    }
}