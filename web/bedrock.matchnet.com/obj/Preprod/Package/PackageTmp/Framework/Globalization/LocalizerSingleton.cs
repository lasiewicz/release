﻿using System;
using System.Collections;
using System.Collections.Specialized;
using System.Globalization;
using System.IO;
using System.Text;
using System.Web;
using System.Web.UI;

using Matchnet.Web.Framework;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Web.Interfaces;

namespace Matchnet.Web.Framework.Globalization
{
    public class LocalizerSingleton:ILocalizer
    {
        private LocalizerSingleton(){}
        
        public static readonly LocalizerSingleton Instance = new LocalizerSingleton();
        
        #region ILocalizer Members

        public string GetFormattedStringResource(string resourceName, object caller, Brand brand, StringDictionary tokenMap, string[] args, bool showResourceHintsBySetting)
        {
            return Localizer.GetFormattedStringResource(resourceName, caller, brand, tokenMap, args, showResourceHintsBySetting);
        }

        public string GetStringResource(string key, object caller, Brand brand)
        {
            return Localizer.GetStringResource(key, caller, brand);
        }

        public string GetStringResource(string key, object caller, Brand brand, StringDictionary tokenMap)
        {
            return Localizer.GetStringResource(key, caller, brand, tokenMap);
        }

        public string GetStringResourceWithNoTokeMap(string key, object caller, Brand brand)
        {
            return Localizer.GetStringResourceWithNoTokeMap(key, caller, brand);
        }

        public string GetFormattedStringResource(string resourceName, object caller, Brand brand, StringDictionary tokenMap, string[] args)
        {
            return Localizer.GetFormattedStringResource(resourceName, caller, brand, tokenMap, args);
        }

        public string ConvertTypeToResourcePath(Type type)
        {
            return Localizer.ConvertTypeToResourcePath(type);
        }

        public string ExpandTokens(string source, StringDictionary tokenMap)
        {
            return Localizer.ExpandTokens(source, tokenMap);
        }

        public string ExpandImageTokens(string source, Brand brand)
        {
            return Localizer.ExpandImageTokens(source, brand);
        }

        #endregion
    }
}