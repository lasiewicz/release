using System;
using System.Collections;
using System.Collections.Specialized;
using System.Globalization;
using System.IO;
using System.Text;
using System.Web;
using System.Web.UI;

using Matchnet.Web.Framework;
using Matchnet.Content.ValueObjects.BrandConfig;

namespace Matchnet.Web.Framework.Globalization
{
	/// <summary>
	///		Localizer class manager the retrieval of resources at runtime.
	/// </summary>
	public sealed class Localizer
	{
		/// TODO: Rip this apart. Better a singleteon? Better with no string catenations and funky collections remaining from fallback resources.
		
		private const string RESOURCES_SUBFOLDER		= @"_Resources";
		private const string ROOT_NAMESPACE_FOLDER		= @"matchnet\web";
		private const string GLOBAL_RESX_NAME			= @"_Resources\Global";

		/// <summary>
		///		Prevent instantiation
		/// </summary>
		private Localizer() {}

		/// <summary>
		///		Retrieve a string resource.
		/// </summary>
		/// <param name="key">Resource key.</param>
		/// <param name="caller">The class that the resource belongs to.</param>
		/// <param name="brand">Site brand name.</param>
		/// <returns>
		///		Returns a localized string or null if resource is not defined.
		/// </returns>
		public static string GetStringResource(string key, object caller, Brand brand)
		{
			return GetStringResource(key, caller, brand, null);
		}


		/// <summary>
		///		Retrieve a string resource.
		/// </summary>
		/// <param name="key">Resource key.</param>
		/// <param name="caller">The class that the resource belongs to.</param>
		/// <param name="brand">Site brand name.</param>
		/// <param name="tokenMap">String expansion tokens (name/value pairs).</param>
		/// <returns>
		///		Returns a localized string or null if resource is not defined.
		/// </returns>
		public static string GetStringResource(string key, object caller, Brand brand, StringDictionary tokenMap)
		{
			return GetFormattedStringResource(key, caller, brand, tokenMap, null);
		}

        /// <summary>
        /// There are cases where we dont' need the TokenMap. If this is null GetFormattedStringResource method just returns nothing.
        /// This works for things within our framework because ContextGlobal initializes the TokenMap, but we need the ability to grab
        /// the resources without ContextGlobal.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="caller"></param>
        /// <param name="brand"></param>
        /// <returns></returns>
        public static string GetStringResourceWithNoTokeMap(string key, object caller, Brand brand)
        {
            StringDictionary noTokenMap = new StringDictionary();
            return GetFormattedStringResource(key, caller, brand, noTokenMap, null);
        }

        internal static string GetFormattedStringResource(string resourceName, object caller, Brand brand, StringDictionary tokenMap, string[] args)
        {
            return GetFormattedStringResource(resourceName, caller, brand, tokenMap, args, false);
        }

		/// <summary>
		///		Retrieve a string resource, use it as format template, and populate with values from the supplied argument array.
		/// </summary>
		/// <param name="resourceName">Resource key.</param>
		/// <param name="caller">The class that the resource belongs to.</param>
		/// <param name="brand">Site brand name.</param>
		/// <param name="tokenMap">String expansion tokens (name/value pairs).</param>
		/// <param name="args">String replacement arguments.</param>
		/// <returns>
		///		Returns a localized string or null if resource is not defined.
		/// </returns>
		internal static string GetFormattedStringResource(string resourceName, object caller, Brand brand, StringDictionary tokenMap, string[] args, bool showResourceHintsBySetting)
		{	
			Control callingControl = GetCallingPageOrUserControl(caller);

            string resourceSetBaseName = string.Empty;
            bool showResourceHints = GetShowResourceHints();

            if (showResourceHintsBySetting)
            {
                if (null != callingControl)
                {
                    Type type = callingControl.GetType();
                    resourceSetBaseName = ConvertTypeToResourcePath(type);
                }
                else
                {
                    resourceSetBaseName = GLOBAL_RESX_NAME;
                }
            }
			LocalizedResourceSet.ResourceValue resourceValue = LocalizedResourceSet.GetResource(callingControl, brand, resourceName);
			
			// tokenMap not found:
			if (tokenMap == null)
			{
                if (showResourceHintsBySetting)
                {
                    System.Diagnostics.Trace.WriteLine("Localizer.GetFormattedStringResource() no tokenMap for " + resourceSetBaseName + " : " + resourceName);
                    return (showResourceHints) ? ShowResourceHint(resourceName, resourceName, resourceSetBaseName, brand) : resourceSetBaseName + ":" + resourceName;
                }
                else
                {
                    return string.Empty;

                }
			}

			// resource not found:
			if (resourceValue == null)
			{
                if (showResourceHintsBySetting)
                {
                    System.Diagnostics.Trace.WriteLine("Localizer.GetFormattedStringResource() no resource for " + resourceSetBaseName + " : " + resourceName);
                    return (showResourceHints) ? ShowResourceHint(resourceName, resourceName, resourceSetBaseName, brand) : resourceSetBaseName + ":" + resourceName;
                }
                else
                {
                    return string.Empty;
                }
			}

			// Resource found, value is empty.
			if (resourceValue.Value == string.Empty)
			{
                if (showResourceHintsBySetting)
                {
                    System.Diagnostics.Trace.WriteLine("Localizer.GetFormattedStringResource() resource value empty " + resourceSetBaseName + " : " + resourceName);
                    return (showResourceHints) ? ShowResourceHint(resourceName, resourceName, resourceSetBaseName, brand) : string.Empty;
                }
                else
                {
                    return string.Empty;
                }

			}

			string formattedString = resourceValue.Value;

			// perform  replace "((NAME1))" with tokenMap["NAME1"].Value 
			if (resourceValue.HasNamedTokens)
			{ 
				formattedString = ExpandTokens(formattedString, tokenMap);
			}


			if (args == null || args.Length == 0 || !resourceValue.HasPositionalTokens)
			{
                if (showResourceHintsBySetting)
                {
                    return (showResourceHints) ? ShowResourceHint(formattedString, resourceName, resourceSetBaseName, brand) : formattedString;
                }
                else
                {
                    return formattedString;
                }
			}
			else 
			{
				// replace occurrences of "%s" with values from the agrs array
				formattedString = Format(formattedString, args);

                if (showResourceHintsBySetting)
                {
                    return (showResourceHints) ? ShowResourceHint(formattedString, resourceName, resourceSetBaseName, brand) : formattedString;
                }
                else
                {

                    return formattedString;
                }
			}
		}


		internal static string ConvertTypeToResourcePath(Type type)
		{
			string typeName = type.Name.ToLower();

			string folderName = type.BaseType.Namespace.ToLower().Replace('.', '\\');

			// strip the root namespace from the folder name, since the file structure ignores it
			if (folderName.StartsWith(ROOT_NAMESPACE_FOLDER))
			{
				folderName = folderName.Substring(ROOT_NAMESPACE_FOLDER.Length + 1);
			}

			#region For 1.1/2.0 compatibility
			//In 1.1, typeName is of the form "searchareacode_ascx"
			//In 2.0, it's of the form "applications_home_controls_searchareacode_ascx"
			//The code below strips the extra junk from the 2.0 version of the type name
			if (typeName.StartsWith(folderName.Replace("\\", "_")))
			{
				typeName = typeName.Substring(folderName.Length + 1);
			}
			#endregion

			if (typeName.EndsWith("_aspx") || typeName.EndsWith("_ascx") || typeName.EndsWith("_asmx"))
			{
				typeName = typeName.Substring(0, typeName.Length - 5) + "." + typeName.Substring(typeName.Length - 4);
			}
			else
			{
				System.Diagnostics.Trace.WriteLine("Funny Type " + type);
			}

			return folderName + @"\" + RESOURCES_SUBFOLDER + @"\" + typeName;
		}


		private static bool GetShowResourceHints()
		{	
#if DEBUG
			ContextGlobal g = (ContextGlobal)HttpContext.Current.Items["g"];

            if (g == null)
                return false;
            else
                return (g.Session.GetString(WebConstants.SESSIONKEY_RESXHINT_KEY_TOGGLE ,"0") == "1");
#else
			return false;
#endif
		}

															
		private static string ShowResourceHint(string ResourceValue, string ResourceKeyName, string RelativeControlPath, Brand brand)
		{
			if (ResourceKeyName.StartsWith("ALT_") || ResourceKeyName.StartsWith("NAV_") || ResourceKeyName.StartsWith("BTN_")|| ResourceKeyName.StartsWith("TTL_"))
			{
				return ResourceValue;
			}
			else 
			{
				return string.Format(
					"<div style=\"display:inline;\" onmouseover=\"document.getElementById('popup_{1}').style.visibility='visible'; return true;\"" +
					" onmouseout=\"document.getElementById('popup_{1}').style.visibility='hidden'; return true;\" style=\"font-weight: normal;\">" +
					"{0}<div id=\"popup_{1}\" class=\"resourceHint\">{2}</div></div>", 
					ResourceValue, 
					Guid.NewGuid(), 
					ResourceAdminURL(HttpContext.Current.Request.Url.Host,
					RelativeControlPath,
					brand.Site.SiteID,
					ResourceKeyName, 
					string.Format("<B>{0}</B><br />{1}",ResourceKeyName,RelativeControlPath)
					)
					);
			}
		}


		/// <summary>
		///		Try to determine the Page or UserControl that this resource request ultimately
		///		comes from. Recurse up the Control hierarchy if necessary.
		/// </summary>
		/// <param name="caller"></param>
		/// <returns></returns>
		private static Control GetCallingPageOrUserControl(object caller)
		{
			if (null == caller || !(caller is Control))
			{
				return null;
			}

			if (! (caller is UserControl || caller is Page))
			{
				return GetCallingPageOrUserControl(((Control) caller).Parent);
			}

			return (Control) caller;
		}


		/// <summary>
		///		Substitute occurrences of "%s" in the format string with values from the arguments array.
		/// </summary>
		/// <param name="source">Format string.</param>
		/// <param name="args">Arguments array.</param>
		/// <returns></returns>
		private static string Format(string source, string[] args)
		{
			if (args == null || 0 == args.Length ) 
			{
				return source;
			}

			int pos = source.IndexOf("%s");
			int lastPos = 0;
			int counter = 0;

			if (pos < 0) 
			{
				return source;
			}
			
			// Preallocate buffer to strring length + some. StringBuilder re-allocates mem and initializes to an arbitrary length initially.
			// Why 32? Why Not?
			StringBuilder builder = new StringBuilder(source.Length + 32);
	
			while (pos > -1) 
			{
				builder.Append(source.Substring(lastPos, pos - lastPos));
				if (counter < args.Length) 
				{
					builder.Append(args[counter]);
					counter++;
				} 
				else 
				{
					break;
				}
				lastPos = pos + 2;
				if (lastPos > source.Length) 
				{
					break;
				}
				pos = source.IndexOf("%s", lastPos);
			}
			if (lastPos < source.Length) 
			{
				builder.Append(source.Substring(lastPos));
			}

			return builder.ToString();
		}


		/// <summary>
		///		For every token (key) value found in the source string, replace it with the corresponding
		///		expansion (value).
		/// </summary>
		/// <param name="source"></param>
		/// <param name="tokenMap"></param>
		/// <returns></returns>
		public static string ExpandTokens(string source, StringDictionary tokenMap)
		{
			if (tokenMap == null) 
			{	
#if DEBUG
				System.Diagnostics.Trace.WriteLine("__Globalization.Localizer.ExpandTokens() empty tokenmap for " + source);
#endif
			return source;
			}

			string result = source;
			foreach(DictionaryEntry entry in tokenMap)
			{
				string key = "((" + entry.Key.ToString().ToUpper(CultureInfo.InvariantCulture) + "))";

				if (entry.Value == null)
				{
#if DEBUG
					System.Diagnostics.Trace.WriteLine("__Globalization.Localizer.ExpandTokens() null value for key " + entry.Key);
#endif
                    continue;
				}

				result = result.Replace(key, entry.Value.ToString());
			}
			return result;
		}


		/// <summary>
		/// Returns a path the the resource editor web page
		/// </summary>
		/// <param name="RelativeControlPath">The relative path to the control ex: _Resources\Global.ascx</param>
		/// <param name="ResourceKeyName">The resource key name. this will be used to jump to the correct resource that this dive shows a hint for</param>
		/// <returns>A link that will pop </returns>
		private static string ResourceAdminURL(string BasePath, string RelativeControlPath, int SiteID, string ResourceKeyName , string Value)
		{
			return String.Format("<a href=\"//{0}:8080/EditResources.aspx?basepath={2}&site={3}&culture={4}&resourcename={1}#{1}\" target=\"_Blank\">{5}</a>", 
				BasePath,
				ResourceKeyName,
				RelativeControlPath,
				SiteID,
				CultureInfo.CurrentCulture.Name,
				Value);
		}


		public static string ExpandImageTokens(string source, Brand brand)
		{
			int pos = source.IndexOf("((image:");
			int lastPos = 0;
			int endPos = 0;
			string fileName = string.Empty;
			string token = string.Empty;
			
			if (pos < 0) 
			{
				return source;
			}

			StringBuilder builder = new StringBuilder();
	
			while (pos > -1) 
			{
				
				builder.Append(source.Substring(lastPos, pos - lastPos));
				
				endPos = source.IndexOf("))", pos);
				
				if (endPos > -1)
				{
					token = source.Substring(pos + 2, endPos - pos - 2);

					int colon1 = token.IndexOf(":");
					int colon2 = token.LastIndexOf(":");
					
					if (colon1 != -1 && colon2 != -1)
					{
						fileName = token.Substring(colon2 + 1, token.Length - colon2 - 1);
						builder.Append(Image.GetURLFromFilename(fileName));
					}

					lastPos = endPos + 2;
					if (lastPos > source.Length) 
					{
						break;
					}
					pos = source.IndexOf("((image:", lastPos);
				}
				else
					break;

			}
			if (lastPos < source.Length) 
			{
				builder.Append(source.Substring(lastPos));
			}

			return builder.ToString();
		}
	}
}
