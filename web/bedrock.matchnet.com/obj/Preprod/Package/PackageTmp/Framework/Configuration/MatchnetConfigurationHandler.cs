using System.Configuration;
using System.Xml;

namespace Matchnet.Web.Framework.Configuration
{
	/// <summary>
	///     Strongly-typed configuration settings
	/// </summary>
	internal sealed class MatchnetConfigurationHandler : IConfigurationSectionHandler
	{
		public object Create(object parent, object configContext, XmlNode section)
		{
			return new MatchnetConfiguration(section.ChildNodes);
		}
	}
}
