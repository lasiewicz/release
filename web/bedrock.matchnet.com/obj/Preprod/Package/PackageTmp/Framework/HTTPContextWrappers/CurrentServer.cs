﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.Web.Interfaces;

namespace Matchnet.Web.Framework.HTTPContextWrappers
{
    public class CurrentServer:ICurrentServer
    {
        #region ICurrentServer Members

        public string MapPath(string path)
        {
            return HttpContext.Current.Server.MapPath(path);
        }

        #endregion
    }
}