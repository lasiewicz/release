using System;
using System.Web;
using System.Diagnostics;
using System.Configuration;
using System.Collections.Generic;

using Matchnet.Content;
using Matchnet.Web.Framework;

namespace Matchnet.Web.Framework.Diagnostics
{
	/// <summary>
	/// Summary description for ContextExceptions.
	/// </summary>
	public class ContextExceptions
	{
		private ContextGlobal _g;
		private bool _WriteToWeb = false;
		private bool _WriteToEventLog = false;
		public const string Source = "WWW";
		public const string Log = "MatchNet";
        private bool redirect;
		public ContextExceptions()
		{
		}

        public int LogException(Exception exception)
        {
            return LogException(exception, EventLogEntryType.Error);
        }

		public int LogException(Exception exception, EventLogEntryType eventLogEntryType)
        {
            try
            {
                if (FilterViewStateException(exception))
                {
                    return -1;
                }
                //randy's fix to avoid logging threadabortexception
                if (exception.GetType().ToString().ToLower() == "system.threading.threadabortexception")
                {
                    return 600072;
                }

                _g = (ContextGlobal) HttpContext.Current.Items["g"];

                ReadConfig();

                if (_g != null)
                {
                    if (_WriteToWeb)
                    {
                        WriteToWeb(exception);
                    }

                    if (_WriteToEventLog)
                    {
                        WriteToEventLog(exception, eventLogEntryType);
                    }
                }
                else
                {
                    WriteExceptionToEventLogNoG(exception, eventLogEntryType);
                }

                SendExceptionToRaygun(exception);

                // return Translator.Resources.OK;
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message);
            }
            return 600072;
		}

        public int LogException(Exception exception, bool redirectToErrorPage)
        {
            return LogException(exception, redirectToErrorPage, EventLogEntryType.Error);
        }

        public int LogException(Exception exception, bool redirectToErrorPage, EventLogEntryType eventLogEntryType)
        {
            if (FilterViewStateException(exception))
            {
                return 600072;
            }
            //randy's fix to avoid logging threadabortexception
            System.Diagnostics.Trace.Write("LogException enter, redirect: " + redirectToErrorPage.ToString());
            redirect = false;
            redirect = redirectToErrorPage;
            //WriteToEventLog(exception);
            
            if (exception.GetType().ToString().ToLower() == "system.threading.threadabortexception")
            {
                return 600072;
            }
            

            _g = (ContextGlobal)HttpContext.Current.Items["g"];
            System.Diagnostics.Trace.Write("LogException getting _g");
            if(exception != null)
                System.Diagnostics.Trace.Write(exception.ToString());
            ReadConfig();

            if (_g != null)
            {
                if (_WriteToWeb)
                {
                    WriteToWeb(exception);
                }

                if (_WriteToEventLog)
                {
                    System.Diagnostics.Trace.Write("LogException _WriteEventLog");
                    WriteToEventLog(exception, eventLogEntryType);
                }
            }
            else
            {
                System.Diagnostics.Trace.Write("LogException WriteExceptionToEventLog");
                WriteExceptionToEventLogNoG(exception, eventLogEntryType);
            }

            SendExceptionToRaygun(exception);
            // return Translator.Resources.OK;
            return 600072;
        }

        /// <summary>
        /// Logs an exception to the event log
        /// </summary>
        /// <param name="exception">The exception to log</param>
        public static void WriteExceptionToEventLogNoG(Exception exception)
        {
            WriteExceptionToEventLogNoG(exception, EventLogEntryType.Error);
        }

		/// <summary>
		/// Logs an exception based on the context of execution, to the web page
		/// </summary>
		/// <param name="exception">The exception to log</param>
		private void WriteToWeb(Exception exception)
		{
			_g.Notification.AddErrorString(exception.ToString());
		}

		/// <summary>
		/// Logs an exception to the event log
		/// </summary>
		/// <param name="exception">The exception to log</param>
        /// <param name="eventLogEntryType">The event log type</param>
        public static void WriteExceptionToEventLogNoG(Exception exception, EventLogEntryType eventLogEntryType)
		{
            // handle errors coming from toolbarnotification.
            string additionInfo="";
           
			try 
			{
                additionInfo = getAdditionalExInfo(exception);
                
				// g is null here so we have to do some hacks to garner some information, normally in g.ClientIP
				// since g is null the only info we can get is the user ip address and the request url
				string _clientIP = HttpContext.Current.Request.Headers["client-ip"];
				
				if (_clientIP == null)
				{
					_clientIP = HttpContext.Current.Request.ServerVariables["REMOTE_HOST"];
				}
                
				string desc = "Unable to retrieve g from HttpContext. " 
					+ "\r\nClientIP: " + _clientIP
					+ "\r\nURL: " + HttpContext.Current.Request.Url.ToString()
                    + "\r\nException: " + exception.ToString() + additionInfo;


                EventLog.WriteEntry(Source, desc, eventLogEntryType);			
                Trace.WriteLine(desc);
			} 
			catch 
			{
				// if HttpContext is null the above try might fail
				try 
				{
					// just throw the error, g and httpcontext are null
					string desc = "Unable to retrieve g from HttpContext. HttpContext is null.  " 
						+ "\r\nHttpContext is null.  " 
						+ "\r\nException: " + exception.ToString() + additionInfo;
					EventLog.WriteEntry(Source, desc, EventLogEntryType.Error);	
		            Trace.WriteLine(desc);
				} 
				catch 
				{
					// if we cant log an exception something really unforeseen has happened
				}
			}
		}

        private void WriteToEventLog(Exception exception, EventLogEntryType eventLogEntryType)
        {
            string desc = "";
            try
            { 
                if(redirect)
                    desc+="Redirecting to error page\r\n";
               
                if (_g == null)
                {
                    
                    _g = (ContextGlobal)HttpContext.Current.Items["g"]; 
                }
                desc += exception.Source + "-" + exception.Message;
                if (_g != null)
                {
                    try
                    {
                        desc += "\r\nMemberID:" + _g.Session.GetInt(WebConstants.SESSION_PROPERTY_NAME_MEMBERID).ToString()
                        + "\r\nBrandID:" + _g.Brand.BrandID.ToString()
                        + "\r\nURL:" + HttpContext.Current.Request.Url.ToString()
                        + "\r\nDestPage:" + _g.DestinationPage.ToString()
                        + "\r\nHost:" + _g.ClientIP;


                        if (_g.AppPage != null)
                        {

                            desc += "\r\nAppPage=:" + _g.AppPage.ControlName;
                        }
                    }
                    catch (Exception ee) { desc += "\r\nWere unable to get all exception Member info."; }
                }
                

                desc += "\r\nURL:" + HttpContext.Current.Request.Url.ToString();
                   
                
                desc +="\r\n" +  exception.ToString() + "\r\n";
                string additionalInfo=getAdditionalExInfo(exception);
                desc+="\r\n" + additionalInfo;

                EventLog.WriteEntry(Source, desc, eventLogEntryType);
                Trace.WriteLine(desc);
            }
            catch(Exception e)
            {
                // probably a security exception. we shouldn't blow up the app because of
                // this type of misconfig.
                string s = e.ToString();
                 System.Diagnostics.Trace.Write("LogException  Error    \r\n :" + s);
                 s += getAdditionalExInfo(e);
                 desc += "\r\n Exception while logging : \r\n" + s;
                 EventLog.WriteEntry(Source, desc, EventLogEntryType.Error);
                Trace.WriteLine(desc);
            }
        }


        private static string getCookies()
        {
            string cookies = "";
            try
            {
                foreach (string key in HttpContext.Current.Request.Cookies.Keys)
                {
                    cookies += "key: " + key + " value: " + HttpUtility.UrlDecode(HttpContext.Current.Request.Cookies[key].Value) + "\r\n";

                }
                return cookies;
            }
            catch(Exception ex)
            {
                return cookies;
            }

        }

		private void ReadConfig()
		{
			// else ,let's read our config data...
            try
            {
                string val = System.Configuration.ConfigurationSettings.AppSettings.Get("LogExceptions");

                if (val == null)
                {
                    _WriteToWeb = false;
                    _WriteToEventLog = true;	//default to event log so we don't have to configure production boxes
                    return;
                }
                if (val.ToLower() == "web")
                {
                    _WriteToWeb = true;
                    _WriteToEventLog = false;
                    return;
                }

                if (val.ToLower() == "eventlog")
                {
                    _WriteToWeb = false;
                    _WriteToEventLog = true;
                    return;
                }

                if (val.ToLower() == "both")
                {
                    _WriteToWeb = true;
                    _WriteToEventLog = true;
                    return;
                }
            }
            catch (Exception ex)
            { 
                System.Diagnostics.Trace.Write("ReadConfig  Error    \r\n :" + ex.ToString());
                _WriteToEventLog = true; 
            }
		}

        //hack not to log our toolbar viewstate exception
        private static bool FilterViewStateException(Exception ex)
        {
            bool ret = false;
            try
            {
                string msg = ex.ToString();
                string url = HttpContext.Current.Request.Url.ToString().ToLower();
                string rawurl = HttpContext.Current.Request.RawUrl.ToString().ToLower();
                if (url.IndexOf("anthem_callback=true") >= 0 && rawurl.IndexOf("registration") < 0)
                    ret = true;
                return ret;
            }
            catch (Exception)
            { return ret; }
        }

        public static string getAdditionalExInfo(Exception exception)
        {
            string url="";
            string inner="";
            string cookie="";
            string addExc = "";
            string refurl = "";
            string excstring = "";
			try
                {
                    url = HttpContext.Current.Request.RawUrl;
                    refurl = HttpContext.Current.Request.UrlReferrer.ToString();
                    if (exception != null)
                    {
                        excstring = exception.ToString();
                        if (exception.InnerException != null)
                            inner = exception.InnerException.ToString();
                    }
                    else
                    {
                        inner="\r\nException is null";
                    }
                    cookie = getCookies();
                    addExc += "\r\n" + excstring;
                    addExc += "\r\nInnerException=" + inner;
                    addExc += "\r\nRawURL=" + url;
                    addExc += "\r\nRefURL=" + refurl;
                    addExc += "\r\nCookies=" + cookie;
                    addExc += "\r\nStack=" + exception.StackTrace;
                    addExc += "\r\nStackInner=" + exception.InnerException.StackTrace;
                    
                    return addExc;
                }
                catch 
                {
                    return addExc;
                }
        }

        private void SendExceptionToRaygun(Exception exception)
        {
            string memberId = (_g != null && _g.Session != null) ? _g.Session.GetInt(WebConstants.SESSION_PROPERTY_NAME_MEMBERID).ToString() : "";
            string brandId = (_g != null && _g.Brand!= null) ? _g.Brand.BrandID.ToString() : "";
            string appPage = (_g != null && _g.AppPage != null) ? _g.AppPage.ControlName : "";
            string destinationPage = (_g != null && _g.DestinationPage != null) ? _g.DestinationPage.ToString() : "";
            var tags = new List<string>() { memberId, brandId, appPage, destinationPage };

            var rayGunClient = new Mindscape.Raygun4Net.RaygunClient();
            rayGunClient.User = memberId;
            
            var customData = new Dictionary<string, string>();
            customData.Add("MemberID", memberId);
            customData.Add("BrandID", brandId);
            customData.Add("AppPage", appPage);
            customData.Add("DestinationPage", destinationPage);
            
            rayGunClient.Send(exception, tags, customData);
        }

	}
}
