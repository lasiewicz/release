﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WatermarkTextbox.ascx.cs" Inherits="Matchnet.Web.Framework.Ui.FormElements.WatermarkTextbox" %>

<asp:TextBox ID="WatermarkTextBox1" runat="server" ></asp:TextBox>

<script type="text/javascript">
    //<![CDATA[                    
        $j("#<%=WatermarkTextBox1.ClientID %>").watermark("<%=WatermarkText%>", { useNative: false } );
        
        //if a custom class is added that is not "watermark"
        if("<%=WatermarkCssClass%>" != "watermark"){
            $j('#<%=WatermarkTextBox1.ClientID %>').focus(function() {
                $j(this).removeClass("<%=WatermarkCssClass%>");
            });

            $j('#<%=WatermarkTextBox1.ClientID %>').blur(function() {
                if ($j(this).val() == '<%=WatermarkText%>') {
                    $j(this).addClass("<%=WatermarkCssClass%>");
                }
            });
        }
    //]]>
</script>