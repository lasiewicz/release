using System;
using Matchnet.List.ValueObjects;
using Matchnet.Member.ServiceAdapters.Interfaces;
using Matchnet.Member.ValueObjects.Interfaces;
using Matchnet.Web.Framework;
using Matchnet.Configuration.ServiceAdapters;

namespace Matchnet.Web.Framework.Ui.BasicElements
{
	/// <summary>
	///		Summary description for MiniProfileHotListBar.
	/// </summary>
	public class MiniProfileHotListBar : FrameworkControl
	{
        private bool _PopulatedHotlistActivity = false;
		private IMemberDTO	_member;

		protected Image	FriendIconImage;
		protected Image	ContactedIconImage;
		protected Image	TeaseIconImage;
		protected Image	IMIconImage;
		protected Image	ViewProfileIconImage;
        protected Image OmnidateIconImage;

        private string friendIconFileName = "icon_gry_{0}_hotlist.gif";
        private string contactedIconFileName = "icon_gry_{0}_email.gif";
        private string teaseIconImageFileName="icon_gry_{0}_tease.gif";
        private string imIconFileName="icon_gry_{0}_im.gif";
        private string viewProfileIconFileName = "icon_gry_{0}_viewed.gif";
        private string omnidateIconFileName = "icon_gry_{0}_omnidate.gif";

		public IMemberDTO Member
		{
			get { return _member; }
			set { _member = value; }
		}

        public string FriendIconFileName
        {
            get {return friendIconFileName;}
            set{friendIconFileName=value;}
        }
        public string ContactedIconFileName
        {
            get { return contactedIconFileName; }
            set { contactedIconFileName = value; }
        }
        public string TeaseIconImageFileName
        {
            get { return teaseIconImageFileName; }
            set { teaseIconImageFileName = value; }
        }

        public string IMIconFileName
        {
            get { return imIconFileName; }
            set { imIconFileName = value; }
        }
        public string ViewProfileIconFileName
        {
            get { return viewProfileIconFileName; }
            set { viewProfileIconFileName = value; }
        }
        public string OmnidateIconFileName
        {
            get { return omnidateIconFileName; }
            set { omnidateIconFileName = value; }
        }

		private void Page_Load(object sender, System.EventArgs e)
		{
			if (null == _member)
			{
				//throw new ArgumentException("Member", "This control requires the use of the current Member.ServiceAdapters.Member object.");
                return;
			}

			DetermineHotListActivity();
		}

        public void LoadHotlistActivity(IMemberDTO pMember)
        {
            _member = pMember;
            DetermineHotListActivity();
        }

		private void DetermineHotListActivity()
		{
            if (!_PopulatedHotlistActivity)
            {
                FriendIconImage.FileName = FriendIconFileName;
                ContactedIconImage.FileName = ContactedIconFileName;
                TeaseIconImage.FileName = TeaseIconImageFileName;
                IMIconImage.FileName = IMIconFileName;
                ViewProfileIconImage.FileName = ViewProfileIconFileName;
                OmnidateIconImage.FileName = OmnidateIconFileName;
                DetermineHotListActivityForType(FriendIconImage, HotListType.Friend);
                DetermineHotListActivityForType(ContactedIconImage, HotListType.Email);
                DetermineHotListActivityForType(TeaseIconImage, HotListType.Tease);
                DetermineHotListActivityForType(IMIconImage, HotListType.IM);
                DetermineHotListActivityForType(ViewProfileIconImage, HotListType.ViewProfile);
                if (RuntimeSettings.GetSetting("ENABLE_OMNIDATE", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID) == "true")
                {
                    DetermineHotListActivityForType(OmnidateIconImage, HotListType.Omnidate);
                }
                _PopulatedHotlistActivity = true;
            }
		}

		private void DetermineHotListActivityForType(Image iconImage, HotListType hotListType)
		{
            if (g.Member == null || _member == null)
                return;

			HotListCategory listCategorySource = ListInternal.MapListCategory(hotListType, HotListDirection.OnYourList);
			HotListCategory listCategoryTarget = ListInternal.MapListCategory(hotListType, HotListDirection.OnTheirList);

			bool atSource = _g.List.IsHotListed(listCategorySource, _g.Brand.Site.Community.CommunityID, _member.MemberID);
			bool atTarget = _g.List.IsHotListed(listCategoryTarget, _g.Brand.Site.Community.CommunityID, _member.MemberID);

            if (hotListType == HotListType.Email)
            {
                // for Email type, we have to check the VIP hotlists too and OR the results
                listCategorySource = ListInternal.MapListCategory(HotListType.AllAccessEmail, HotListDirection.OnYourList);
                listCategoryTarget = ListInternal.MapListCategory(HotListType.AllAccessEmail, HotListDirection.OnTheirList);

                atSource |= _g.List.IsHotListed(listCategorySource, _g.Brand.Site.Community.CommunityID, _member.MemberID);
                atTarget |= _g.List.IsHotListed(listCategoryTarget, _g.Brand.Site.Community.CommunityID, _member.MemberID);
            }

			ConfigureHotListIcon(iconImage, hotListType, atSource, atTarget);
		}

		public void ConfigureHotListIcon(Image iconImage, HotListType hotListType, bool atSource, bool atTarget)
		{
			iconImage.Visible = true;

			string imageKey = "";
			if (atSource && atTarget)
			{
				imageKey = "bth";
			}
			else if (atSource)
			{
				imageKey = "you";
			}
			else if (atTarget)
			{
				imageKey = "mem";
			}

			if (imageKey.Length > 0)
			{
				iconImage.FileName = string.Format(iconImage.FileName, imageKey);
				iconImage.AlternateText = GetResourceValue(hotListType, atSource, atTarget);
			}
			else
			{
				iconImage.App = WebConstants.APP.None;
				iconImage.Visible = false;
			}
		}

		private string GetResourceValue(HotListType hotListType, bool atSource, bool atTarget)
		{
			string altResourceConstant = "";
			switch (hotListType)
			{
				case HotListType.Friend:
					if (atSource && atTarget)
					{
						altResourceConstant = "ALT_ONLIST_BOTH";
					}
					else if (atSource)
					{
						altResourceConstant = "ALT_ONLIST_YOU";
					}
					else if (atTarget)
					{
						altResourceConstant = "ALT_ONLIST_THEY";
					}
					break;

				case HotListType.Email:
					if (atSource && atTarget)
					{
						altResourceConstant = "ALT_CONTACTED_BOTH";
					}
					else if (atSource)
					{
						altResourceConstant = "ALT_CONTACTED_YOU";
					}
					else if (atTarget)
					{
						altResourceConstant = "ALT_CONTACTED_THEY";
					}
					break;

				case HotListType.Tease:
					if (atSource && atTarget)
					{
						altResourceConstant = "ALT_TEASED_BOTH";
					}
					else if (atSource)
					{
						altResourceConstant = "ALT_TEASED_YOU";
					}
					else if (atTarget)
					{
						altResourceConstant = "ALT_TEASED_THEY";
					}
					break;

				case HotListType.IM:
					if (atSource && atTarget)
					{
						altResourceConstant = "ALT_IMD_BOTH";
					}
					else if (atSource)
					{
						altResourceConstant = "ALT_IMD_YOU";
					}
					else if (atTarget)
					{
						altResourceConstant = "ALT_IMD_THEY";
					}
					break;

				case HotListType.ViewProfile:
					if (atSource && atTarget)
					{
						altResourceConstant = "ALT_VIEWED_BOTH";
					}
					else if (atSource)
					{
						altResourceConstant = "ALT_VIEWED_YOU";
					}
					else if (atTarget)
					{
						altResourceConstant = "ALT_VIEWED_THEY";
					}
					break;
                case HotListType.Omnidate:
                    if (atSource && atTarget)
                    {
                        altResourceConstant = "ALT_OMNIDATE_BOTH";
                    }
                    else if (atSource)
                    {
                        altResourceConstant = "ALT_OMNIDATE_YOU";
                    }
                    else if (atTarget)
                    {
                        altResourceConstant = "ALT_OMNIDATE_THEY";
                    }
                    break;
				default:
					throw new Exception("Unknown HotListType: " + hotListType);
			}

			if (null == altResourceConstant || altResourceConstant.Length == 0)
			{
				throw new Exception("Application encountered an unexpected state: the HotListType " + hotListType + " was acted on neither by you not the other person.");
			}

			return _g.GetResource(altResourceConstant, this, new string[]{_member.GetUserName(_g.Brand)});
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
