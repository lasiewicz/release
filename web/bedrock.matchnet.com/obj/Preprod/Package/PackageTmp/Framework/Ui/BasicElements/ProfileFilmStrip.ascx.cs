﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Web.Applications.ColorCode;
using Matchnet.Search.ValueObjects;
using Matchnet.Web.Applications.Home;
using System.Web.UI.HtmlControls;
using Matchnet.List.ValueObjects;
using System.Collections;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Search.ServiceAdapters;

namespace Matchnet.Web.Framework.Ui.BasicElements
{
    public partial class ProfileFilmStrip : FrameworkControl
    {
        #region Fields
        protected string _ColorText = "";
        private List<ProfileHolder> _memberList;
        private bool _hasMatches = false;
        private const string DOMAINID = "DomainID";
        private int _MaxDisplay = 17;
        #endregion

        #region Properties
        public bool HasMatches
        {
            get { return _hasMatches; }
            set { _hasMatches = value; }
        }

        public int MaxDisplay
        {
            get { return _MaxDisplay;}
            set { _MaxDisplay = value;}
        }

        public string TrackingParam { get; set; }

        #endregion

        #region Event Handlers
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void repeaterProfiles_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                ProfileHolder profileHolder = e.Item.DataItem as ProfileHolder;

                FilmStripMicroProfile FilmStripMicroProfile1 = e.Item.FindControl("FilmStripMicroProfile1") as FilmStripMicroProfile;
                FilmStripMicroProfile1.member = profileHolder.Member;
                FilmStripMicroProfile1.MyEntryPoint = profileHolder.MyEntryPoint;
                FilmStripMicroProfile1.Ordinal = profileHolder.Ordinal;
                FilmStripMicroProfile1.HotListCategoryID = (int)profileHolder.HotlistCategory;
                FilmStripMicroProfile1.TrackingParam = TrackingParam;
                FilmStripMicroProfile1.LoadMemberProfile();

                if (FrameworkGlobals.memberHighlighted(profileHolder.Member.MemberID, g.Brand))
                {
                    //CSS
                    HtmlGenericControl liBegin = e.Item.FindControl("liBegin") as HtmlGenericControl;
                    liBegin.Attributes["class"] += " highlighted";
                }
            }
        }

        #endregion

        #region Public Methods
        public void LoadYourMatches(BreadCrumbHelper.EntryPoint entryPoint)
        {
            _memberList = GetYourMatches(g.SearchPreferences, _MaxDisplay, entryPoint);

            if (_memberList != null && _memberList.Count > 0)
            {
                phHasProfiles.Visible = true;
                phMissingProfilesCopy.Visible = false;

                _hasMatches = true;

                if (_memberList.Count < _MaxDisplay)
                {
                    phViewMore.Visible = false;
                }
                repeaterProfiles.DataSource = _memberList;
                repeaterProfiles.DataBind();
            }
            else
            {
                //show marketing copy
                phHasProfiles.Visible = false;
                phMissingProfilesCopy.Visible = true;

                //show copy 3 or 4 depending on if user has taken color code
                if (ColorCodeHelper.HasMemberCompletedQuiz(g.Member, g.Brand))
                    phMissingProfileCopyItem3.Visible = true;
                else
                    phMissingProfileCopyItem4.Visible = true;
                   
            }
        }
        #endregion

        #region Private Methods
        private List<ProfileHolder> GetYourMatches(SearchPreferenceCollection prefs, int maxProfileDisplay, BreadCrumbHelper.EntryPoint entryPoint)
        {
            // declare local varialbes for resuse and clarity
            const string HASPHOTOFLAG = "HasPhotoFlag";
            int startRow = 0;
            int pageSize = maxProfileDisplay;
            MatchnetQueryResults searchResults = null;

            //get current photo preference before we modify it
            string photoPreference = prefs.Value(HASPHOTOFLAG);
            prefs.Add(HASPHOTOFLAG, "1");

            //Ensure that domainID is set on search preferences for the correct Community.
            prefs.Add(DOMAINID, g.Brand.Site.Community.CommunityID.ToString());
            prefs.Add("ColorCode", "");            
            try
            {
                // Potential for exception due to invalid search preferences on this member's stored set of preferences.
                searchResults = MemberSearchSA.Instance.Search(prefs
                     , g.Brand.Site.Community.CommunityID
                     , g.Brand.Site.SiteID
                     , startRow
                     , pageSize
                     , g.Member == null ? Constants.NULL_INT : g.Member.MemberID
                     , Matchnet.Search.Interfaces.SearchEngineType.FAST
                     , Matchnet.Search.Interfaces.SearchType.ProfileFilmStripWebSearch
                     , Matchnet.Search.Interfaces.SearchEntryPoint.ProfileFilmStripMatches
                     , false
                     , false
                     , false
                     );      
            }
            catch (Exception)
            {
                //invalid search preferences, initialize an empty results list so the home page will render correctly.
                searchResults = new MatchnetQueryResults(0);
            }

            //set photo preference back to old value
            prefs.Add(HASPHOTOFLAG, photoPreference);

            List<ProfileHolder> profileList = new List<ProfileHolder>();
            ArrayList members = new ArrayList();
            if (searchResults != null && searchResults.ToArrayList().Count > 0)
            {
                members = MemberSA.Instance.GetMembers(searchResults.ToArrayList(),
                         MemberLoadFlags.None);

                foreach (Member.ServiceAdapters.Member member in members)
                {
                    //add member
                    ProfileHolder ph = new ProfileHolder();
                    ph.Member = member;
                    ph.Ordinal = startRow + 1;
                    ph.HotlistCategory = HotListCategory.YourMatches;
                    ph.MyEntryPoint = entryPoint;

                    profileList.Add(ph);

                    startRow++;
                }
            }

            if (profileList.Count > 0)
            {
                _hasMatches = true;
            }

            return profileList;

        }
        #endregion
    }
}