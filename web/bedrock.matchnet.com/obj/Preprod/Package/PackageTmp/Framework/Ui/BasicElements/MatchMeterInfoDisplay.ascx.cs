﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Matchnet.Web.Applications.CompatibilityMeter;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Util;
using Matchnet.Content.ServiceAdapters.Links;
using Matchnet.Content.ValueObjects.PageConfig;

using Matchnet.MatchTest.ValueObjects.MatchMeter;
using Matchnet.MatchTest.ServiceAdapters;
using Matchnet.Member.ServiceAdapters;


namespace Matchnet.Web.Framework.Ui.BasicElements
{
    public partial class MatchMeterInfoDisplay : FrameworkControl
    {

        #region Fields (6)

        private int _DispalyType;
        private bool _IsMatchesPage;
        private bool _IsMatchMeterEnabled;
        private bool _IsMemberHighlighted;
        private decimal _MatchScore;
        // 1 - minprofile. 2 - galleryminiprofile. 3 - full profile. 4 - micro profile. 5- full profile tab 30. 6 -  miniprofilelistview. 7- miniprofile
        private int _MemberID;

        #endregion Fields

        #region Properties (6)

        public int DispalyType
        {
            set { _DispalyType = value; }
        }

        public bool IsMatchesPage
        {
            set { _IsMatchesPage = value; }
        }

        public bool IsMatchMeterEnabled
        {
            set { _IsMatchMeterEnabled = value; }
        }

        public bool IsMemberHighlighted
        {
            set { _IsMemberHighlighted = value; }
        }

        public decimal MatchScore
        {
            set { _MatchScore = value; }
        }

        public int MemberID
        {
            set { _MemberID = value; }
        }

        public int Ordinal { get; set; }

        #endregion Properties

        #region Methods (1)


        // Protected Methods (1) 

        //protected void Page_Load(object sender, EventArgs e)
        protected void Page_PreRender(object sender, EventArgs e)
        {
            try
            {
                if (_IsMatchMeterEnabled)
                {
                    int subPRTID = 0;
                    switch (_DispalyType)
                    {
                        case 1: // minprofile
                            {
                                plcMatchMeterLogo.Visible = !_IsMatchesPage;
                                plcMathMeterScore.Visible = _IsMatchesPage;
                                if (plcMathMeterScore.Visible)
                                {
                                    divScoreBar.Attributes.Add("title", g.GetResource("SCORE_BAR_TOOLTIP", this));

                                    MatchMeterScoreBar.IsMemberHighlighted = _IsMemberHighlighted;
                                }
                                if (_IsMatchesPage)
                                {
                                    MatchMeterScoreBar.MatchScore = _MatchScore;
                                }
                                else
                                {
                                    //txtJmeter.ResourceConstant = "TXT_JMETER";
                                    imgJmeter.ResourceConstant = "ALT_JMETER_VIEW"; // Alt tag for JMeter logo (same for rest of cases)
                                    imgJmeter.TitleResourceConstant = "TTL_JMETER_VIEW";    // Title tag for JMeter logo (same for rest of cases)
                                    if (_IsMemberHighlighted)
                                    {
                                        imgJmeter.FileName = g.GetResource("LONG_LOGO_LIST_YELLOW", this);
                                    }
                                    else
                                    {
                                        imgJmeter.FileName = g.GetResource("LONG_LOGO_LIST_BLUE", this);

                                    }
                                }

                                txtJmeter.Visible = true;
                                subPRTID = 661;
                            } break;
                        case 2: // galleryminiprofile
                            {

                                //txtJmeter.Visible = false;
                                imgJmeter.ResourceConstant = "ALT_JMETER_VIEW";
                                imgJmeter.TitleResourceConstant = "TTL_JMETER_VIEW";
                                if (_IsMemberHighlighted)
                                {
                                    imgJmeter.FileName = g.GetResource("SHORT_LOGO_GALLERY_YELLOW", this);
                                }
                                else
                                {
                                    imgJmeter.FileName = g.GetResource("SHORT_LOGO_GALLERY_BLUE", this);
                                }
                                subPRTID = 660;
                            }
                            break;
                        case 3: // full profile
                            {
                                //txtJmeter.ResourceConstant = "TXT_JMETER_FULL_PROFILE";
                                if (_IsMemberHighlighted)
                                {
                                    imgJmeter.FileName = g.GetResource("LOGO_PROFILE_VIEW_YELLOW", this);
                                }
                                else
                                {
                                    imgJmeter.FileName = g.GetResource("LOGO_PROFILE_VIEW_BLUE", this);
                                }

                                imgJmeter.ResourceConstant = "ALT_JMETER_VIEW";
                                imgJmeter.TitleResourceConstant = "TTL_JMETER_VIEW";
                                subPRTID = 659;
                            } break;
                        case 4: // micro profile
                            {
                                plcMatchMeterLogo.Visible = false;
                                plcMathMeterScore.Visible = true;
                                txtViewMatch.Visible = false;
                                divScoreBar.Attributes.Add("title", g.GetResource("SCORE_BAR_TOOLTIP", this));
                                MatchMeterScoreBar.IsMemberHighlighted = _IsMemberHighlighted;
                                MatchMeterScoreBar.MatchScore = _MatchScore;
                                MatchMeterScoreBar.DisplayHearts = true;
                                divLogoInfoPopup.Attributes["class"] = "jmeterLogoInfoPopupMicroProfile";
                                subPRTID = 662;
                            } break;
                        case 5: // full profile tab 30
                            {
                                //txtJmeter.ResourceConstant = "TXT_JMETER_FULL_PROFILE";
                                if (_IsMemberHighlighted)
                                {
                                    imgJmeter.FileName = g.GetResource("LOGO_PROFILE_VIEW_30_YELLOW", this);
                                }
                                else
                                {
                                    imgJmeter.FileName = g.GetResource("LOGO_PROFILE_VIEW_30_BLUE", this);
                                }

                                imgJmeter.ResourceConstant = "ALT_JMETER_VIEW";
                                imgJmeter.TitleResourceConstant = "TTL_JMETER_VIEW";
                                subPRTID = 659;
                            } break;
                        case 6: // miniprofilelistview
                            {
                                //txtJmeter.ResourceConstant = "TXT_JMETER_FULL_PROFILE";
                                if (_IsMemberHighlighted)
                                {
                                    imgJmeter.FileName = g.GetResource("MINI_PROFILE_LIST_VIEW_YELLOW", this);
                                }
                                else
                                {
                                    imgJmeter.FileName = g.GetResource("MINI_PROFILE_LIST_VIEW_BLUE", this);
                                }

                                imgJmeter.ResourceConstant = "ALT_JMETER_VIEW";
                                imgJmeter.TitleResourceConstant = "TTL_JMETER_VIEW";
                                subPRTID = 659;
                            } break;
                        case 7: // miniprofile
                            {
                                //txtJmeter.ResourceConstant = "TXT_JMETER_FULL_PROFILE";
                                if (_IsMemberHighlighted)
                                {
                                    imgJmeter.FileName = g.GetResource("MINI_PROFILE_YELLOW", this);
                                }
                                else
                                {
                                    imgJmeter.FileName = g.GetResource("MINI_PROFILE_BLUE", this);
                                }

                                imgJmeter.ResourceConstant = "ALT_JMETER_VIEW";
                                imgJmeter.TitleResourceConstant = "TTL_JMETER_VIEW";
                                subPRTID = 659;
                            } break;
                    }

                    MatchTestStatus targetMTS = MatchTestStatus.MinimumCompleted;
                    Member.ServiceAdapters.Member aMember = MemberSA.Instance.GetMember(_MemberID, MemberLoadFlags.None);
                    if (aMember != null)
                    {
                        targetMTS = MatchMeterSA.Instance.GetMatchTestStatus(_MemberID, aMember.IsPayingMember(g.Brand.Site.SiteID), g.Brand);
                    }
                    bool memberCompleted = false;
                    MatchTestStatus mts = MatchTestStatus.NotTaken;
                    string url = FrameworkGlobals.LinkHref("/Applications/CompatibilityMeter/OneOnOne.aspx", true);

                    if (g.Member != null)
                    {
                        mts = MatchMeterSA.Instance.GetMatchTestStatus(g.Member.MemberID, g.Member.IsPayingMember(g.Brand.Site.SiteID), g.Brand);
                        if (mts == MatchTestStatus.MinimumCompleted || mts == MatchTestStatus.Completed)
                        {
                            memberCompleted = true;
                        }
                        if (g.Member.MemberID != _MemberID) // Make sure user doesn't watch his own profile
                        {
                            url += "?MemberID=" + _MemberID.ToString();
                            if (Ordinal > 0)
                            {
                                url += "&Ordinal=" + Ordinal;
                            }
                        }
                        else
                        {
                            url = FrameworkGlobals.LinkHref("/Applications/CompatibilityMeter/Welcome.aspx", true);
                        }
                    }
                    else
                    {
                        // for visitors
                        url += "?MemberID=" + _MemberID.ToString();
                    }

                    CompatibilityMeterHandler cmh = new CompatibilityMeterHandler(g);

                    // 3 - full profile.
                    if (_DispalyType == 3)
                    {
                        lnkMatchMeter.NavigateUrl = url;
                        if (g.LayoutTemplate == LayoutTemplate.Popup || g.LayoutTemplate == LayoutTemplate.PopupProfile)
                        {
                            if (!memberCompleted)
                            {
                                lnkMatchMeter.Attributes.Add("onclick", string.Format("javascript:window.open('{0}', null, 'height=664,width=780,status=yes,toolbar=yes,menubar=yes,location=yes, resizable=yes, scrollbars=1');", url));
                                lnkMatchMeter.NavigateUrl = string.Empty;
                            }
                        }
                    }
                    else
                    {
                        lnkMatchMeter.NavigateUrl = url;
                    }
                    if ((targetMTS == MatchTestStatus.NotCompleted || targetMTS == MatchTestStatus.NotTaken) && memberCompleted)
                    {
                        string notification = cmh.CheckSendInvitationNotification(aMember);
                        if (string.IsNullOrEmpty(notification))
                        {
                            if (CompatibilityMeterHandler.IsJMeterV3(g.Brand))
                            {
                                lnkMatchMeter.NavigateUrl = "/Applications/MemberProfile/ViewProfile.aspx?ProfileTab=JMeter&MemberID=" +
                                                            _MemberID.ToString();
                            }
                            else
                            {
                                string inviteURL =
                            FrameworkGlobals.LinkHref(
                                "/Applications/CompatibilityMeter/SendInvitation.aspx?LayoutTemplateID=8&PersistLayoutTemplate=1&MemberID=" + _MemberID.ToString(),
                                    //+ "&opener=" + Server.UrlEncode(Request.Url.AbsoluteUri.ToLower()),
                                true);
                                lnkMatchMeter.Attributes.Add("onclick", string.Format("javascript:window.open('{0}', null, 'height=600,width=620,status=no,toolbar=no,menubar=no,location=no, resizable=no, scrollbars=0');", inviteURL));
                                lnkMatchMeter.NavigateUrl = string.Empty;
                            }
                        }
                        else
                        {
                            string currentURL = Request.Url.AbsoluteUri.ToLower();
                            if (currentURL.IndexOf("rc") != -1)
                            {
                                currentURL = g.RemoveParamFromURL(currentURL, "rc", false);
                            }

                            currentURL += currentURL.Contains('?') ? "&" : "?";
                            currentURL += "rc=" + notification;

                            lnkMatchMeter.NavigateUrl = currentURL;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                lnkMatchMeter.NavigateUrl = FrameworkGlobals.LinkHref("/Applications/CompatibilityMeter/Error.aspx", true);
                g.ProcessException(ex);
                /*
                if (!g.IsDevMode && !(ex is System.Threading.ThreadAbortException))
                {
                    g.Transfer("/Applications/CompatibilityMeter/Error.aspx");
                }
                */
            }
        }
        #endregion Methods
    }
}
