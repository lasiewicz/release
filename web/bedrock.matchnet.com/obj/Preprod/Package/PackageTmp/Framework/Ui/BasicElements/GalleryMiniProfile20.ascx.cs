﻿using System;
using System.Collections.Specialized;
using System.Text;
using System.Web;
using System.Web.UI;
using Matchnet.Member.ValueObjects.Interfaces;
using Matchnet.Member.ValueObjects.MemberDTO;
using Matchnet.Web.Framework.Managers;
using Matchnet.Web.Framework.Ui.PageElements;
using Matchnet.Content.ServiceAdapters.Links;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Configuration.ServiceAdapters.Analitics;
using Matchnet.Web.Applications.ColorCode;
using Matchnet.Web.Applications.MemberProfile;

namespace Matchnet.Web.Framework.Ui.BasicElements
{
    /// <summary>
    /// GalleryMiniProfile20 is the 2.0 version of the gallery mini profile control,
    /// which displays mini profiles in gallery format.
    /// </summary>
    partial class GalleryMiniProfile20 : FrameworkControl, IPostBackDataHandler
    {
        private IMemberDTO member;
        private int memberId;
        private string offlineImageName;
        private string onlineImageName;
        //private List<Photo> _approvedPhotos;
        private int _approvedPhotosCount = -1;
        protected NoPhoto20 noPhoto;
        protected Color _Color = Color.none;
        protected string _ColorText = "";

        public string OfflineImageName
        {
            get { return offlineImageName; }
            set { offlineImageName = value; }
        }

        public string OnlineImageName
        {
            get { return onlineImageName; }
            set { onlineImageName = value; }
        }

        protected HighlightInfoDisplay20 ucHighlightProfileInfoDisplay;

        public int ApprovedPhotosCount
        {
            get
            {
                try
                {
                    if(_approvedPhotosCount == -1)
                    {
                        _approvedPhotosCount = MemberPhotoDisplayManager.Instance.GetApprovedPhotosCount(g.Member, Member, g.Brand);
                    }

                    return _approvedPhotosCount;

                }
                catch (Exception ex)
                { return 0; }

            }

        }

        // constants
        protected int MAX_STRING_LENGTH = 12;

        public SearchResultProfile searchResult;

        public IMemberDTO Member
        {
            get { return member; }
            set
            {
                member = value;
                memberId = member.MemberID;

                IsHighlighted = FrameworkGlobals.memberHighlighted(member, g.Brand);
            }
        }

        public int MemberID
        {
            get { return (memberId); }
            set
            {
                memberId = value;
                if (value != 0)
                {
                    member = MemberDTOManager.Instance.GetIMemberDTO(memberId, g, MemberType.Search, MemberLoadFlags.None);
                    // MemberSA.Instance.GetMember(memberId,MemberLoadFlags.None);
                }
                else
                {
                    member = null;
                }
            }
        }

        public int Ordinal { get; set; }

        public BreadCrumbHelper.EntryPoint MyEntryPoint { get; set; }

        public MOCollection MyMOCollection { get; set; }

        public int HotListCategoryID { get; set; }

        public bool IsHighlighted { get; set; }

        public bool IsMatchMeterInfoDisaplay { get; set; }

        public bool IsMatchMeterEnabled { get; set; }

        public Int32 Counter { get; set; }

        # region Context Type public property and enumerator

        //protected Txt TxtSelectThisProfile;
        //protected Txt TxtDeselectThisProfile;
        protected YNMVoteBarSmall VoteBar;

        public GalleryMiniProfile20()
        {
            DisplayContext = ResultContextType.SearchResult;
        }

        public ResultContextType DisplayContext { get; set; }

        #endregion Context Type public property and enumerator

        private void Page_Load(object sender, EventArgs e)
        {
            try
            {
                int brandId = 0;
                member.GetLastLogonDate(g.Brand.Site.Community.CommunityID, out brandId);

                add2List.Orientation = Add2ListOrientation.IconOnly;
                add2List.MemberID = MemberID;

                txtFavoriteNote.Visible = false;
                searchResult = new SearchResultProfile(member, Ordinal);
                chkSelected.Value = member.MemberID.ToString();

                SetData();
                ToggleDisplayType();
                // Alter the z-index here
                boxContainerGallery.Style.Add("z-index", Convert.ToString(100 - Counter));

                //Set jmeter controls
                SetJmeter();
                HotListBar.Member = member;
                SetEmailButtonABTest();
                SetQuickMessage();

                //color code
                if (ColorCodeHelper.IsColorCodeEnabled(g.Brand) &&
                    ColorCodeHelper.HasMemberCompletedQuiz(member, g.Brand) &&
                    !ColorCodeHelper.IsMemberColorCodeHidden(member, g.Brand))
                {
                    phColorCode.Visible = true;
                    _Color = ColorCodeHelper.GetPrimaryColor(member, g.Brand);
                    _ColorText = ColorCodeHelper.GetFormattedColorText(_Color);
                }

                btnOmnidate.TargetMember = member;

                plcClick.Visible = g.IsYNMEnabled;

                if (
                    Boolean.Parse(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("MINGLE_SEARCH",
                                                                                                    _g.Brand.Site.Community.CommunityID,
                                                                                                    _g.Brand.Site.SiteID)))
                {
                    PlaceHolderMingleSearch.Visible = false;
                    PlaceHolderAboutMeEssay.Visible = true;
                    searchResult.SetAboutMeEssay(txtAboutMeEssay);
                    txtReadMore.Href = g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.ViewProfile,
                                                                        WebConstants.Action.ViewMemberLink, txtMore.Href,
                                                                        "List");
                    if (txtAboutMeEssay.Text.Length == 0)
                    {
                        txtReadMore.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                _g.ProcessException(ex);
            }
        }

        private void SetQuickMessage()
        {
            if (Boolean.Parse(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ENABLE_SEARCH_RESULTS_QUICK_MESSAGE", _g.Brand.Site.Community.CommunityID, _g.Brand.Site.SiteID)) &&
                (DisplayContext == ResultContextType.SearchResult ||
                DisplayContext == ResultContextType.MembersOnline ||
                DisplayContext == ResultContextType.QuickSearchResult ||
                DisplayContext == ResultContextType.HotList ||
                DisplayContext == ResultContextType.ReverseSearchResult)
                )
            {

                PlaceHolderEmailMe.Visible = false;
                imgBtnQuickMessage.Visible = true;
                imgBtnQuickMessage.Attributes.Add("MemberID", MemberID.ToString());
                plcQuickMessage.Visible = true;
                txtQuickMessageSubject.Text = g.GetResource("MESSAGE_SUBJECT", this);
                txtQuickMessageBody.Text = g.GetResource("MESSAGE_BODY", this);

                StringBuilder validator = new StringBuilder();
                validator.AppendLine("var quickMessageValidator = { \"quick-message-subject\":[");
                validator.AppendLine("{ \"errorKey\": \"required\", \"message\": \"" + g.GetResource("TXT_CHANGE_SUBJECT", this) + "\" },");
                validator.AppendLine("{ \"errorKey\": \"noChange\", \"message\": \"" + g.GetResource("TXT_CHANGE_SUBJECT", this) + "\" }],");
                validator.AppendLine("\"quick-message-body\":[");
                validator.AppendLine("{ \"errorKey\": \"required\", \"message\": \"" + g.GetResource("TXT_MESSAGE_BODY_BLANK") + "\" },");
                validator.AppendLine("{ \"errorKey\": \"noChange\", \"message\": \"" + g.GetResource("TXT_CHANGE_BODY", this) + "\" }]};");
                validator.AppendLine("var quickMessageGeneralError =\"" + g.GetResource("TXT_QUICK_MESSAGE_GENERAL_ERROR") + "\";");
                validator.AppendLine("var quickMessageProfileType=\"gallery\";");
                this.Page.ClientScript.RegisterStartupScript(this.GetType(), "QuickMessageValidator", validator.ToString(), true);
            }
        }
        protected string GetMemberIDAttr()
        {
            return "MemberID=\"" + MemberID + "\"";
        }

        private void SetEmailButtonABTest()
        {
            Scenario scenario = g.GetABScenario("MINIPROFILE_BUTTON");
            if (scenario == Scenario.B)
            {
                imgEmail.FileName = "btn-view-member.gif";
                imgEmail.ResourceConstant = "VIEW_MEMBER";
                txtMore.Visible = false;
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            try
            {
                base.OnPreRender(e);
                string additionalSource = String.Empty;

                switch (DisplayContext)
                {
                    case ResultContextType.HotList:
                    case ResultContextType.MembersOnline:
                        additionalSource = "Gallery";
                        break;
                }

                if (DisplayContext != ResultContextType.RegistrationCarrot && g.AnalyticsOmniture != null)
                {
                    string viewProfileURL = IsHighlighted ?
                            BreadCrumbHelper.MakeViewProfileLink(
                                MyEntryPoint,
                                MemberID,
                                Ordinal,
                                MyMOCollection,
                                HotListCategoryID,
                                false,
                                (int)BreadCrumbHelper.PremiumEntryPoint.Highlight
                            )
                                :
                            BreadCrumbHelper.MakeViewProfileLink(
                                MyEntryPoint,
                                MemberID,
                                Ordinal,
                                MyMOCollection,
                                HotListCategoryID,
                                false,
                                (int)BreadCrumbHelper.PremiumEntryPoint.NoPremium
                            );
                    
                    if (_g.Member != null)
                    {
                        //set flirt/tease link
                        lnkTeaseMe.HRef = FrameworkGlobals.LinkHref(ProfileDisplayHelper.GetFlirtLink(g, member.MemberID, GetStartRow()), true);
                        ProfileDisplayHelper.AddFlirtOnClick(g, ref lnkTeaseMe);
                    }
                    else
                    {
                        lnkTeaseMe.HRef = g.GetLogonRedirect(viewProfileURL, null);
                    }
                    
                    lnkTeaseMe.HRef = g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.Tease, WebConstants.Action.Flirt, lnkTeaseMe.HRef, additionalSource);
                    lnkUserName.NavigateUrl = g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.ViewProfile, WebConstants.Action.ProfileName, lnkUserName.NavigateUrl, additionalSource);
                    imgThumb.NavigateUrl = g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.ViewProfile, WebConstants.Action.ProfilePhoto, imgThumb.NavigateUrl, additionalSource);
                    noPhoto.DestURL = imgThumb.NavigateUrl;
                    imgOnline.Attributes.Add("onclick", g.AnalyticsOmniture.GetOnClickCustomLinkTracking(WebConstants.Action.IM, g.AnalyticsOmniture.PageName, "", false, false));
                    lnkECard.NavigateUrl = g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.ECard, WebConstants.Action.ECard, lnkECard.NavigateUrl, additionalSource);
                    add2List.ActionCallPage = g.AnalyticsOmniture.PageName;
                    add2List.ActionCallPageDetail = "";

                    Scenario scenario = g.GetABScenario("MINIPROFILE_BUTTON");
                    if (scenario == Scenario.B)
                    {
                        lnkEmail.NavigateUrl = g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.ViewProfile, WebConstants.Action.ViewMemberButton, txtMore.Href, additionalSource);
                    }
                    else
                    {
                        lnkEmail.NavigateUrl = g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.Compose, WebConstants.Action.EmailMeNowButton, lnkEmail.NavigateUrl, additionalSource);
                        //txtMore.Href = g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.ViewProfile, WebConstants.Action.ViewProfile, txtMore.Href, additionalSource);
                        txtMore.Text = FrameworkGlobals.GetMorePhotoLink(ApprovedPhotosCount, g, this);
                        if (ApprovedPhotosCount > 0)
                        {
                            txtMore.Href = g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.ViewProfile, WebConstants.Action.ViewMorePhotos, txtMore.Href, additionalSource);
                        }
                        else
                        {
                            txtMore.Href = g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.ViewProfile, WebConstants.Action.ViewMemberLink, txtMore.Href, additionalSource);
                        }
                    }
                }
                else
                {
                    lnkEmail.Visible = false;
                    lnkUserName.NavigateUrl = "";
                    txtMore.Visible = false;
                    imgThumb.NavigateUrl = "";
                    noPhoto.disableLink();
                    searchResult.SetHeadline(TextHeadline, MemberID, 80, 20);
                    TextHeadline.Visible = true;
                    divHistory.Style.Add("display", "none");

                }
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        private void SetJmeter()
        {
            if (IsMatchMeterEnabled)
            {
                ucMatchMeterInfoDisplay.Visible = true;
                ucMatchMeterInfoDisplay.IsMatchMeterEnabled = true;
                ucMatchMeterInfoDisplay.MemberID = MemberID;
                ucMatchMeterInfoDisplay.IsMemberHighlighted = IsHighlighted;
            }
            else
            {
                ucMatchMeterInfoDisplay.IsMatchMeterEnabled = false;
                ucMatchMeterInfoDisplay.Visible = false;
            }
        }

        bool IPostBackDataHandler.LoadPostData(string postDataKey, NameValueCollection values)
        {
            return false;
        }

        void IPostBackDataHandler.RaisePostDataChangedEvent()
        {
        }

        private void ToggleDisplayType()
        {
            switch (DisplayContext)
            {
                case ResultContextType.HotList:
                    ShowHotListMember();
                    break;
                case ResultContextType.SearchResult:
                    ShowSearchResultMember();
                    break;
                case ResultContextType.MembersOnline:
                    ShowOnlineMember();
                    break;
                case ResultContextType.RegistrationCarrot:
                    ShowRegistrationCarrot();
                    break;
            }
        }

        private void ShowOnlineMember()
        {
            plcHotListMove.Visible = false;
            txtFavoriteNote.Visible = false;
        }

        private void ShowSearchResultMember()
        {
            plcHotListMove.Visible = false;
            txtFavoriteNote.Visible = false;
        }
        private void ShowRegistrationCarrot()
        {
            plcHotListMove.Visible = false;
            txtFavoriteNote.Visible = false;
            divCommunications.Style.Add("display", "none");

        }
        private void ShowHotListMember()
        {
            plcHotListMove.Visible = false;
            chkSelected.Visible = false;
            chkSelected.Attributes.Add("OnClick", searchResult.showOnClickJavascript(Counter));
            searchResult.SetNotes(txtFavoriteNote);

            // When in any of the hot list categories page, a Delete from list displays.
            // When this displays, do not display Learn about Highlighted Profiles.
            ucHighlightProfileInfoDisplay.Visible = false;
        }


        private void SetData()
        {
            // TPJ-94
            imgIsNew.Visible = member.IsNewMember(g.Brand.Site.Community.CommunityID);
            imgIsUpdate.Visible = !imgIsNew.Visible && member.IsUpdatedMember(g.Brand.Site.Community.CommunityID);

            SetPremiumImageAuthIsVisible();

            SetHighlighting();

            VoteBar.MemberID = member.MemberID;
            if (g.Member != null)
            {
                if (g.Member.MemberID == member.MemberID)
                {
                    txtDoWeClick.Visible = false;
                    VoteBar.Visible = false;
                }
            }
            else
            {
                txtDoWeClick.Visible = false;
                VoteBar.Visible = false;
            }

            string userDestinationUrl = IsHighlighted ?
                BreadCrumbHelper.MakeViewProfileLink(
                    MyEntryPoint,
                    MemberID,
                    Ordinal,
                    MyMOCollection,
                    HotListCategoryID,
                    false,
                    (int)BreadCrumbHelper.PremiumEntryPoint.Highlight
                )
                    :
                BreadCrumbHelper.MakeViewProfileLink(
                    MyEntryPoint,
                    MemberID,
                    Ordinal,
                    MyMOCollection,
                    HotListCategoryID,
                    false,
                    (int)BreadCrumbHelper.PremiumEntryPoint.NoPremium
                );

            if (IsReverseSearchRequest())
            {
                userDestinationUrl = AppendReverseSearchParameters(userDestinationUrl);
            }

            string emailUrl =
                String.Join(
                    String.Empty,
                    new[]
					{
						"/Applications/Email/Compose.aspx?MemberId=",
						member.MemberID.ToString(),
						"&LinkParent=",
						((int)LinkParent.GalleryMiniProfile).ToString(),
						"&StartRow=",
						GetStartRow().ToString()
					}
                );

            // Render the appropriate image and URL for the IM link
            OnlineLinkHelper.SetOnlineLink(
                DisplayContext,
                searchResult._Member,
                Member,
                imgOnline,
                LinkParent.GalleryMiniProfile,
                onlineImageName,
                offlineImageName
            );

            txtMore.Href = userDestinationUrl;
            lnkEmail.NavigateUrl = emailUrl;
            lnkViewProfile.NavigateUrl = userDestinationUrl;// FrameworkGlobals.LinkHref(string.Format("/Applications/MemberProfile/ViewProfile.aspx?Ordinal=1&PersistLayoutTemplate=1&MemberID={0}", searchResult._Member.MemberID), false);

            if (Boolean.Parse(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("MINGLE_SEARCH", _g.Brand.Site.Community.CommunityID, _g.Brand.Site.SiteID)))
            {
                txtMore.Visible = false;
                ImageMorePhotos.Visible = true;
                PlaceHolderViewProfile.Visible = true;
                PlaceHolderEmailMe.Visible = false;
            }
            else
            {
                txtMore.Visible = true;
                ImageMorePhotos.Visible = false;
                PlaceHolderViewProfile.Visible = false;
                PlaceHolderEmailMe.Visible = true;
            }

            //MemberPhotoHelper photoHelper = new MemberPhotoHelper(RuntimeSettings.Instance);
            searchResult.SetUserName(lnkUserName, userDestinationUrl);
            noPhoto.MemberID = searchResult._Member.MemberID;
            noPhoto.NoPhotoFileName = MemberPhotoDisplayManager.Instance.GetNoPhotoFile(NoPhotoImageType.BackgroundThumb, searchResult._Member, g.Brand);
            noPhoto.PrivatePhotoFileName = MemberPhotoDisplayManager.Instance.GetNoPhotoFile(NoPhotoImageType.BackgroundThumb, searchResult._Member, g.Brand);
            searchResult.SetThumb(imgThumb, noPhoto, userDestinationUrl);
            searchResult.SetAge(txtAge, g.GetResource("AGE_UNSPECIFIED", this));
            if (g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.BlackSingles)
            {
                searchResult.SetGender(g.Brand.Site.Community.CommunityID, LiteralGender);
            }
            searchResult.SetRegion(g.Brand.Site.LanguageID, txtRegion, 25);

            ImageMorePhotos.NavigateUrl = BreadCrumbHelper.AppendParamToProfileLink(userDestinationUrl, "ProfileTab=Photo");// FrameworkGlobals.LinkHref(string.Format("/Applications/MemberProfile/ViewProfile.aspx?ProfileTab=Photo&Ordinal=1&PersistLayoutTemplate=1&MemberID={0}", searchResult._Member.MemberID), false);


            #region ECARD IMPLEMENTATION  5-22-06: Steve C.

            // Show hide send ECard link based on setting from database.  (ON or OFF)
            if (g.EcardsEnabled)
            {

                // Generate mingle ecard page url. (to parameter is the username of the reciever)
                string destPageUrl = FrameworkGlobals.BuildConnectFrameworkLink("cards/categories.html?to=" + member.GetUserName(_g.Brand) + "&MemberID=" + member.MemberID + "&return=1", g, true);

                // Assign url to ECard link.
                lnkECard.NavigateUrl = destPageUrl;
            }
            else
            {
                lnkECard.Visible = false;
            }

            #endregion
        }

        private String AppendReverseSearchParameters(String originalUrl)
        {
            StringBuilder sb = new StringBuilder();
            var parameters = HttpUtility.ParseQueryString(Request.Url.Query);

            var foundPage = false;

            var page = 0;

            foreach (var parameter in parameters)
            {
                if (parameter.ToString() == "pg")
                {
                    foundPage = true;
                    page = Int32.Parse(parameters[(String)parameter]);
                }
                sb.Append(String.Format("{0}={1}", parameter, parameters[(String)parameter]));
            }

            if (foundPage == false)
            {
                sb.Append("pg=1");
            }

            return BreadCrumbHelper.AppendParamToProfileLink(originalUrl, sb.ToString() + "&rp=" + page);

        }

        private Boolean IsReverseSearchRequest()
        {
            return Request.Url.LocalPath.ToLower().Contains("reversesearch.aspx");
        }

        private void SetHighlighting()
        {
            if (IsHighlighted)
            {
                boxContainerGallery.Attributes.Remove("class");
                boxContainerGallery.Attributes.Add("class", "results gallery-view boxGallery highlighted");

                ucHighlightProfileInfoDisplay.MemberHighlighted = true;
                ucHighlightProfileInfoDisplay.ViewedMemberID = member.MemberID;
            }
            else
            {
                ucHighlightProfileInfoDisplay.Visible = false;
            }
        }

        private void SetPremiumImageAuthIsVisible()
        {
            if (g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.JDateCoIL)
            {
                imgPremiumAuthenticated.Visible = FrameworkGlobals.IsMemberPremiumAuthenticated(member.MemberID, g.Brand);
            }
            else
            {
                imgPremiumAuthenticated.Visible = false;
            }
        }

        /// <summary>
        /// Get startRow value for persisting result index.
        /// </summary>
        /// <returns>The starting row; a null value if none are found.</returns>
        private int GetStartRow()
        {
            // Recurse through the parent controls.  
            // If one of the controls is "ResultList" retrieve the StartRow value.
            var parentCtrl = Parent;
            while (parentCtrl != null)
            {
                if (parentCtrl.GetType().Name == "framework_ui_basicelements_resultlist20_ascx")
                {
                    return ((ResultList20)parentCtrl).StartRow;
                }

                parentCtrl = parentCtrl.Parent;
            }

            return Constants.NULL_INT;
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }
        #endregion
    }
}
