﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Web.Framework.Managers;

namespace Matchnet.Web.Framework.Ui.PageAnnouncements
{
    public class PageAnnouncementHelper
    {
        public enum PageAnnouncementMask
        {
            None = 0,
            MutualMatch = 1,
            IMEnhancements = 2,
            FooterMobile = 4,
            FooterMobileV2 = 8
        }

        public static int GetPageAnnouncementViewedMask(Matchnet.Member.ServiceAdapters.Member member, int siteID)
        {
            int maskValue = 0;
            if (member != null)
            {
                maskValue = member.GetAttributeInt(Constants.NULL_INT, siteID, Constants.NULL_INT, "PageAnnouncementViewedMask", 0);
            }

            return maskValue;
        }

        public static bool HasViewedPageAnnouncement(Matchnet.Member.ServiceAdapters.Member member, int siteID, PageAnnouncementMask pageAnnouncement)
        {
            bool hasViewed = false;
            int maskValue = 0;
            if (member != null)
            {
                maskValue = GetPageAnnouncementViewedMask(member, siteID);
                if ((maskValue & (int)pageAnnouncement) == (int)pageAnnouncement)
                {
                    hasViewed = true;
                }
            }

            return hasViewed;
        }

        public static int AddPageAnnouncementViewedMask(Matchnet.Member.ServiceAdapters.Member member, int siteID, PageAnnouncementMask pageAnnouncement)
        {
            int maskValue = 0;
            if (member != null)
            {
                maskValue = GetPageAnnouncementViewedMask(member, siteID);
                if ((maskValue & (int)pageAnnouncement) <= 0)
                {
                    maskValue = maskValue + (int)pageAnnouncement;
                    member.SetAttributeInt(Constants.NULL_INT, siteID, Constants.NULL_INT, "PageAnnouncementViewedMask", maskValue);
                    Matchnet.Member.ServiceAdapters.MemberSA.Instance.SaveMember(member);
                }
            }

            return maskValue;
        }

    }
}