﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FilmStripNanoProfile.ascx.cs" Inherits="Matchnet.Web.Framework.Ui.BasicElements.FilmStripNanoProfile" %>
<%@ Register TagPrefix="uc1" TagName="NoPhoto" Src="../PageElements/NoPhoto20.ascx" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mnl" Namespace="Matchnet.Web.Framework.Ui.BasicElements.Links" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mnbe" Namespace="Matchnet.Web.Framework.Ui.BasicElements" Assembly="Matchnet.Web" %>

<%--photo--%>
<div class="picture" onmouseover="GetMember(<%= MemberID %>,<%= Ordinal %>)">
    <mn:Image runat="server" ID="imgThumb" class="profileImageHover" ResourceConstant="ALT_PROFILE_PICTURE" TitleResourceConstant="TTL_VIEW_MY_PROFILE"  />
    <uc1:NoPhoto runat="server" ID="noPhoto" class="no-photo" Visible="False" />
    <asp:PlaceHolder ID="phColorCode" runat="server" Visible="false">
        <div class="cc-pic-tag cc-pic-tag-sm cc-pic-<%=_ColorText.ToLower() %>">
            <span><%=_ColorText %></span>
        </div>
    </asp:PlaceHolder>
</div>
