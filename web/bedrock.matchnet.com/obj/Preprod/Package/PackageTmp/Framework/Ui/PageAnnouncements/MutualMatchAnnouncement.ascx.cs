﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Matchnet.Web.Framework.Ui.PageAnnouncements
{
    public partial class MutualMatchAnnouncement : FrameworkControl
    {
        protected int MemberID = Constants.NULL_INT;

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        #region Public Methods
        public void LoadAnnouncement()
        {
            if (g.Member != null)
            {
                MemberID = g.Member.MemberID;
                MemberID.ToString();
            }
        }
        #endregion
    }
}