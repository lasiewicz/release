﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.TemplateControls;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Web.Framework.Menus;
using Matchnet.Session.ServiceAdapters;
using Matchnet.Web.Framework.Ui.Advertising;
using Matchnet.Web.Framework.Managers;

namespace Matchnet.Web.Framework.Ui.HeaderNavigation20
{
    public partial class HeaderMenu : FrameworkControl
    {
        private Matchnet.Web.Framework.Menus.Menu _menus;

        public bool ApiMenu { get; set; }

        protected string GetMenuOrientation
        {
            get
            {
                if (RuntimeSettings.GetSetting("ENABLE_VERTICAL_TOP_NAV", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID).ToLower() == "true")
                {
                    return "sf-menu-vertical" + (g.Member == null ? " visitor" : string.Empty);
                }
                return "sf-menu";
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //announcement message
            // phHeaderAnnouncement.Visible = HeaderAnnouncement1.LoadAnnouncementMessage();

            //Social nav indicators
            if (MenuUtils.IsSocialNavEnabled(g))
            {
                LoadMOL();

                if (g.Member != null)
                {
                    //these are only available if member is logged in
                    if (!ApiMenu)
                    {//for connect menu cross-domain ajax calls will not work
                        LoadFavoritesOnline();
                        LoadNewsNotifications();

                    }
                    else
                    {
                        phUserNotifications.Visible = false;
                        phFavourites.Visible = false;
                    }
                }

            }

        }

        public void BindMenuItem(object sender, RepeaterItemEventArgs e)
        {
            Matchnet.Web.Framework.Menus.MenuItem i = (Matchnet.Web.Framework.Menus.MenuItem)e.Item.DataItem;

            i.HtmlEncodeLinks = !ApiMenu;
            if (e.Item.ItemType == ListItemType.Footer || e.Item.ItemType == ListItemType.Header)
            { return; }

            Txt itm = (Txt)e.Item.FindControl("txtMenuItem");
            PlaceHolder plcMenuItem = (PlaceHolder)e.Item.FindControl("plcMenuItem");

            if (!string.IsNullOrEmpty(i.ConfigurationCondition))
            {
                bool visible = MenuUtils.IsSettingConfiguredVisible(i.ConfigurationCondition, g);
                plcMenuItem.Visible = visible;
            }

            if (!string.IsNullOrEmpty(i.ShowConditionFunction))
            {
                bool visible = MenuUtils.IsConditionallyVisible(i.ShowConditionFunction, g);
                plcMenuItem.Visible = visible;
            }
            HtmlGenericControl liControl = (HtmlGenericControl)e.Item.FindControl("liMenu");
            itm.ResourceConstant = i.ResourceConstant;

            if (!String.IsNullOrEmpty(i.Link))
            {
                itm.Href = i.GetExpandedLink(g);
                if (!String.IsNullOrEmpty(i.Target))
                    itm.Target = i.Target;
            }
            else
                itm.Href = MenuUtils.HandleCustomLink(i, g);

            string linkfunction = "";
            if (i.LinkFunction != null)
                linkfunction = i.LinkFunction.ToLower();

            if (ApiMenu && !String.IsNullOrEmpty(itm.Href))
            {
                bool connectitm = linkfunction == "getecardlink" || linkfunction == "getboardlink" || linkfunction == "getchatlink" || linkfunction == "getfriendinvitelink";
                if (!connectitm && !itm.Href.Contains("http"))
                {
                    string apiimagehost = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("API_CALL_IMAGE_HOST", _g.Brand.Site.Community.CommunityID, _g.Brand.Site.SiteID);
                    string href = "http://" + apiimagehost + "." + _g.Brand.Uri + HttpUtility.HtmlEncode(itm.Href);
                    itm.Href = href;
                }
                else
                {
                    itm.Href = HttpUtility.HtmlEncode(itm.Href);
                }
            }

            if (g.Member !=null && !ApiMenu && !string.IsNullOrEmpty(i.GAMAdSlotName) && RuntimeSettings.GetSetting("ENABLE_SUB_TAB_GAM", _g.Brand.Site.Community.CommunityID, _g.Brand.Site.SiteID) == "true")
            {
                AdUnit adUnitMenuItem = e.Item.FindControl("AdUnitMenuItem") as AdUnit;
                if(adUnitMenuItem!=null)
                {
                     int subStatus = FrameworkGlobals.GetMemberSubcriptionStatus(g.Member, g.Brand);
                    // Show only for never sub and ex sub
                    if (subStatus == 1 || subStatus == 2)
                    {
                        itm.HrefName = "lnkSubTab";
                        adUnitMenuItem.GAMAdSlot = i.GAMAdSlotName;
                        adUnitMenuItem.IsSubTab = true;
                    }
                }
            }

            if (i.MenuItems != null && i.MenuItems.Count > 0)
            {
                Repeater rptsubItems = (Repeater)e.Item.FindControl("rptSubMenu");
                rptsubItems.DataSource = i.MenuItems;
                rptsubItems.DataBind();
            }
            else
            {
                PlaceHolder phUl1 = (PlaceHolder)e.Item.FindControl("phSubItemUl1");
                if (phUl1 != null)
                    phUl1.Visible = false;

                PlaceHolder phUl2 = (PlaceHolder)e.Item.FindControl("phSubItemUl2");
                if (phUl2 != null)
                    phUl2.Visible = false;

            }


            if (!string.IsNullOrEmpty(i.LIClass))
            {
                liControl.Attributes.Add("class", i.LIClass);
            }
            else
            {
                if (MenuUtils.IsMenuSelected(i, g))
                {
                    itm.Attributes.Add("class", "selected");
                    liControl.Attributes.Add("class", "menu-item current");
                }
            }

            if (RuntimeSettings.GetSetting("ENABLE_VERTICAL_TOP_NAV", _g.Brand.Site.Community.CommunityID, _g.Brand.Site.SiteID).ToLower() == "true")
            {
                bool addSpace = false;
                if (!string.IsNullOrEmpty(liControl.Attributes["class"]))
                {
                    if (liControl.Attributes["class"].Trim() != string.Empty)
                    {
                        addSpace = true;
                    }
                }
                liControl.Attributes["class"] += (addSpace ? " " : string.Empty) + i.Desc.ToLower().Replace(" ", "-");
            }

            if (!string.IsNullOrEmpty(i.RenderFunction))
            {
                RenderFunction(i.RenderFunction, liControl, itm);
            }

            if (!string.IsNullOrEmpty(i.MBoxName))
            {
                PlaceHolder plcMenuItemMBoxDefaultStart = (PlaceHolder)e.Item.FindControl("plcMenuItemMBoxDefaultStart");
                PlaceHolder plcMenuItemMBoxDefaultEndCreate = (PlaceHolder)e.Item.FindControl("plcMenuItemMBoxDefaultEndCreate");
                Literal litMBoxName = (Literal)e.Item.FindControl("litMBoxName");
                Literal litMBoxNameParams = (Literal)e.Item.FindControl("litMBoxNameParams");

                if (plcMenuItemMBoxDefaultStart != null && plcMenuItemMBoxDefaultEndCreate != null && litMBoxName != null)
                {
                    plcMenuItemMBoxDefaultStart.Visible = true;
                    plcMenuItemMBoxDefaultEndCreate.Visible = true;
                    litMBoxName.Text = i.MBoxName;

                    if ((!string.IsNullOrEmpty(itm.Href) || !string.IsNullOrEmpty(itm.Target)) && litMBoxNameParams != null)
                    {
                        litMBoxNameParams.Text += string.IsNullOrEmpty(itm.Href)
                                                       ? string.Empty
                                                       : string.Format(",'Href={0}'", itm.Href);
                        litMBoxNameParams.Text += string.IsNullOrEmpty(itm.Target)
                                                       ? string.Empty
                                                       : string.Format(",'Target={0}'", itm.Target);
                    }
                }
            }


        }

        private void Page_Init(object sender, System.EventArgs e)
        {
            try
            {
                string menufile = String.Format("/Framework/Ui/HeaderNavigation20/XML/TopMenu.{0}.xml", g.Brand.Site.SiteID);
                _menus = MenuUtils.GetMenuItems(g, menufile, String.Format("TOP_MENU_{0}", g.Brand.Site.SiteID));

                rptMenu.DataSource = _menus.MenuItems;
                rptMenu.DataBind();
            }
            catch (Exception ex)
            { g.ProcessException(ex); }
        }

        private void RenderFunction(string function, HtmlGenericControl liControl, Txt txtControl)
        {

            try
            {
                switch (function)
                {
                    case "ShowNewMessages":
                        if (g.hasNewMessage)
                        {
                            txtControl.TitleResourceConstant = "TTL_YOU_HAVE_NEW_MESSAGES";
                            liControl.Attributes.Add("class", "menu-item new-messages");
                        }
                        else
                        {
                            txtControl.TitleResourceConstant = "TTL_YOUR_MESSAGE_CENTER";
                        }
                        break;
                    case "ShowUpgradeAccount":
                         {
                            if (!Matchnet.Web.Framework.Managers.SubscriberManager.Instance.IsIAPSubscriber(g.Member, g.Brand))
                            {
                                if (MemberPrivilegeAttr.IsCureentSubscribedMember(g))
                                {
                                    bool premiumServices = FrameworkGlobals.IfHasPremiumService(g);
                                    if (!premiumServices)
                                    {
                                        txtControl.ResourceConstant = "NAV_SUB_UPGRADE";
                                    }
                                    else
                                    {
                                        liControl.Attributes["class"] += " upgraded";
                                    }
                                }
                            }
                            
                        }
                        break;
                    case "BillingInfo":
                        {
                            if (Matchnet.Web.Framework.Managers.SubscriberManager.Instance.IsIAPSubscriber(g.Member, g.Brand))
                            {
                                if(g != null)
                                {
                                    if (g.Brand.Site.SiteID == (int)Matchnet.Web.Framework.WebConstants.SITE_ID.JDateCoIL)
                                        txtControl.Href = "http://static.jdate.com/Components/IAP/IL";
                                    else if (g.Brand.Site.SiteID == (int)Matchnet.Web.Framework.WebConstants.SITE_ID.JDateFR)
                                        txtControl.Href = "http://static.jdate.com/Components/IAP/FR/";
                                    else
                                        txtControl.Href = "http://static.jdate.com/Components/IAP/";

                                    txtControl.Target = "_blank";
                                }
                                
                            }

                        }
                        break;
                    }

            }
            catch
                (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        private void LoadMOL()
        {
            // If this is cupid, hide the MOL count
            //07/27/2009 SK changed to be a setting
            phMemberOnline.Visible = SettingsManager.GetSettingBool(SettingConstants.MOL_DISPLAY_FLAG, g.Brand);
            if (phMemberOnline.Visible)
            {
                lnkMembersOnline.Visible = true;
                litMembersOnlineCount.Text = MembersOnlineManager.Instance.GetMOLCommunityCount(g.Brand, false).ToString();
                string href = "";
                if (ApiMenu)
                {
                    string apiimagehost = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("API_CALL_IMAGE_HOST", _g.Brand.Site.Community.CommunityID, _g.Brand.Site.SiteID);
                    href = "http://" + apiimagehost + "." + _g.Brand.Uri;
                }
                lnkMembersOnline.NavigateUrl = href + "/Applications/MembersOnline/MembersOnline.aspx";
                lnkMembersOnline.ToolTip = g.GetResource("NAV_MEMBERS_ONLINE");
                imgMembersOnline.ToolTip = lnkMembersOnline.ToolTip;
            }
        }

        private void LoadFavoritesOnline()
        {
            phFavoritesOnline.Visible = Matchnet.Web.Applications.Favorites.FavoritesControl.IsFavoritesOnlineEnabled(g);
        }

        private void LoadNewsNotifications()
        {
            phNews.Visible = Matchnet.Web.Applications.UserNotifications.UserNotificationFactory.IsUserNotificationsEnabled(g);
        }

    }
}