﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ImageRotator.ascx.cs" Inherits="Matchnet.Web.Framework.Ui.BasicElements.ImageRotator" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<script type="text/javascript" src="/javascript20/cycle.js"></script>
<script type="text/javascript">var url = '<asp:Literal id="litData" runat="server"/>';</script>
<script type="text/javascript" src="/javascript20/readXML.js"></script>
<div id="wrapper">

    <div id="img_controls"><!-- control goes here -->

    </div>

    <div id="img_contents"><!-- content goes here -->
	    <mn:Txt runat="server" ResourceConstant="HTM_DEFAULT_IMAGE"/>
    </div>

</div><!-- end wrapper //-->