﻿using System;
using System.Collections;
using System.Collections.ObjectModel;
using System.Collections.Generic;

using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using Matchnet.Lib;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Ui.BasicElements;

namespace Matchnet.Web.Framework.Ui.BasicElements
{
    public partial class TabBar : FrameworkControl
    {
        //format text in tab( if needed to add count for example, can be replaced with {0} if no counts present
        string _format = "{0}: {1}";
        //href format for all tabs, most of hrefs are include categoryid or folderid, Href on Tab object will override
        string _hrefBase;
        string _hrefFormat = "{0}={1}";
        // max len of tab text, rest is ellipsis
        int _maxTextLen;
        //class of selected tab
        string _cssSelected;
        //selectedf tab id
        int _selectedTabID;
        FrameworkControl _resourceControl;
        List<Tab> _tabs;

        public String TabFormat
        {
            get { return _format; }
            set { _format = value; }

        }

        public String HrefFormat
        {
            get { return _hrefFormat; }
            set { _hrefFormat = value; }

        }
        public String HrefBase
        {
            get { return _hrefBase; }
            set { _hrefBase = value; }

        }

        public int MaxTextLen
        {

            get { return _maxTextLen; }
            set { _maxTextLen = value; }
        }

        public int SelectedTabID
        {

            get { return _selectedTabID; }
            set { _selectedTabID = value; }
        }
        public String ClassSelected
        {
            get { return _cssSelected; }
            set { _cssSelected = value; }

        }

        public FrameworkControl ResourceControl
        {
            get
            {
                if (_resourceControl == null)
                    _resourceControl = this;
                return _resourceControl;
            }

            set { _resourceControl = value; }

        }
        public List<Tab> Tabs
        {
            get { return _tabs; }
            set { _tabs = value; }
        }

        public void AddTab(Tab tab)
        {
            if (_tabs == null)
                _tabs = new List<Tab>();

            _tabs.Add(tab);
        }

        public void AddTab(int id, int order, int count, bool selected, string href, string textresource, string text)
        {
            Tab tab = new Tab(id, order, count, selected, href, textresource, text);
            if (_tabs == null)
                _tabs = new List<Tab>();

            _tabs.Add(tab);
        }

        public void AddTab(int id, int order, int count, bool selected, string href, string textresource, string text, int maxTextLen)
        {
            Tab tab = new Tab(id, order, count, selected, href, textresource, text, maxTextLen);
            if (_tabs == null)
                _tabs = new List<Tab>();

            _tabs.Add(tab);
        }

        public void Clear()
        {
            if (_tabs != null)
            {
                _tabs.Clear();
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            rpTabBar.DataSource = _tabs;
            rpTabBar.DataBind();
        }

        public void rptTabBar_OnItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
        {
            Txt txtTab = (Txt)e.Item.FindControl("txtTabBarCategory");
            HtmlGenericControl liTab = (HtmlGenericControl)e.Item.FindControl("liTab");

            if (txtTab == null || liTab == null)
                return;

            Tab tab = (Tab)e.Item.DataItem;
            string href = "";
            if (!String.IsNullOrEmpty(tab.Href))
                href = tab.Href;
            else if (!String.IsNullOrEmpty(_hrefBase))
            {
                href = string.Format(_hrefFormat, _hrefBase, tab.TabID);

            }

            txtTab.Href = href;

            txtTab.Text = GetTabText(tab);
            if (tab.Selected || tab.TabID == _selectedTabID)
            {
                // txtList.Attributes.Add("class", _classSelected);
                liTab.Attributes["class"] = liTab.Attributes["class"] + " " + _cssSelected;
            }
        }

        private string GetTabText(Tab tab)
        {
            string resTxt = "";

            if (String.IsNullOrEmpty(tab.Text))
                resTxt = getResource(tab.TextResource);
            else
                resTxt = tab.Text;

            if (tab.MaxTextLen > 0)
            {
                resTxt = FrameworkGlobals.Ellipsis(resTxt, tab.MaxTextLen);
            }
            else
            {
                resTxt = FrameworkGlobals.Ellipsis(resTxt, _maxTextLen);
            }

            if (tab.Count != Constants.NULL_INT)
            {
                return String.Format(_format, resTxt, tab.Count);
            }
            else
            {
                return resTxt;
            }

        }

        private string getResource(string key)
        {
            string res = "";
            if (_resourceControl == null)
                res = g.GetResource(key);
            else
                res = g.GetResource(key, _resourceControl);

            return res;
        }



    }
    public class Tab : IComparable
    {
        int _id;
        int _displayOrder;
        bool _selected;
        //resource key where to get text
        string _textResource;
        // actual text, if not empty _textResource is ignored
        string _text;
        //actual href for tab, if not empty overrides HrefFormat of tabcontrol
        string _href;
        //tabs can include :count
        int _count;
        // Enables specific max len for each tab. Not a mandatory property
        public int MaxTextLen { get; set; }

        public int DisplayOrder
        {
            get { return _displayOrder; }
            set { _displayOrder = value; }
        }

        public int TabID
        {
            get { return _id; }
            set { _id = value; }
        }

        public int Count
        {
            get { return _count; }
            set { _count = value; }
        }
        public bool Selected
        {
            get { return _selected; }
            set { _selected = value; }
        }

        public String Href
        {
            get { return _href; }
            set { _href = value; }
        }

        public String TextResource
        {
            get { return _textResource; }
            set { _textResource = value; }
        }

        public String Text
        {
            get { return _text; }
            set { _text = value; }
        }
        public Tab()
        { }


        public Tab(int id, int order, int count, bool selected, string href, string textresource, string text)
        {
            _id = id;
            _displayOrder = order;
            _count = count;
            _selected = selected;
            _href = href;
            _textResource = textresource;
            _text = text;

        }
        public Tab(int id, int order, int count, bool selected, string href, string textresource, string text, int maxTextLen)
        {
            _id = id;
            _displayOrder = order;
            _count = count;
            _selected = selected;
            _href = href;
            _textResource = textresource;
            _text = text;
            MaxTextLen = maxTextLen;
        }
        public int CompareTo(object obj)
        {
            if (obj is Tab)
            {
                Tab t = (Tab)obj;
                return _displayOrder.CompareTo(t.DisplayOrder);
            }
            else
                throw new ArgumentException("Object is not a Tab.");
        }


    }
}