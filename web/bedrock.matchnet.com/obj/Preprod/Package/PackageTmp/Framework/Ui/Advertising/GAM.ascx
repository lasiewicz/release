﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GAM.ascx.cs" Inherits="Matchnet.Web.Framework.Ui.Advertising.GAM" %>
<asp:PlaceHolder ID="PlaceHolderGAM" runat="server">

<script type="text/javascript">
    var googletag = googletag || {};
    googletag.cmd = googletag.cmd || [];
    (function () {
        var gads = document.createElement('script');
        gads.async = true;
        gads.type = 'text/javascript';
        var useSSL = 'https:' == document.location.protocol;
        gads.src = (useSSL ? 'https:' : 'http:') +
            '//www.googletagservices.com/tag/js/gpt.js';
        var node = document.getElementsByTagName('script')[0];
        node.parentNode.insertBefore(gads, node);
    })();
</script>

<script type="text/javascript">
    googletag.cmd.push(function() {
    <asp:PlaceHolder ID="PlaceHolderAdSlots" runat="server"></asp:PlaceHolder>
    <asp:PlaceHolder ID="PlaceHolderAdAttributes" runat="server"></asp:PlaceHolder>
    googletag.pubads().enableSingleRequest();
    googletag.pubads().collapseEmptyDivs();
    googletag.enableServices();
    });
</script>

<script type="text/javascript">
    <asp:PlaceHolder ID="PlaceHolderPlaceCast" runat="server"></asp:PlaceHolder>
</script>
</asp:PlaceHolder>