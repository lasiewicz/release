#region System References
using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.UI;
#endregion

#region Matchnet Web App References
using Matchnet.Lib;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Util;
using Matchnet.Lib.Util;
#endregion

namespace Matchnet.Web.Applications.Admin.EditProfile.Controls
{
    /// <summary>
    ///		Pick Gender : and Seeking Gender: 
    ///		public property GenderMask gives back the parent control what options were picked.
    /// </summary>
    public class PickGenderMask : FrameworkControl
    {
        protected System.Web.UI.WebControls.PlaceHolder placeMemberGender;
        protected System.Web.UI.WebControls.PlaceHolder placeSeekingGender;

        #region private variables
        private int _genderMask = Constants.NULL_INT;
        private RadioButtonList memberGender = new RadioButtonList();
        private RadioButtonList seekingGender = new RadioButtonList();
        #endregion

        #region public properties
        public int GenderMask
        {
            get
            {
                return GetGenderMask();
            }
            set
            {
                _genderMask = value;
            }
        }
        #endregion

        public string CssClass { get; set; }

        private void Page_Load(object sender, System.EventArgs e)
        {
            try
            {
                DataTable dt = Option.GetOptions("GenderMask", g.TargetCommunityID, g.TargetBrand.Site.SiteID, g.TargetBrandID, g);

                memberGender.ID = "memberGender";
                memberGender.RepeatDirection = RepeatDirection.Horizontal;

                seekingGender.ID = "seekingGender";
                seekingGender.RepeatDirection = RepeatDirection.Horizontal;

                foreach (DataRow row in dt.Rows)
                {
                    ListItem item;
                    item = PopulateItem(row);
                    if (item.Value != "")
                    {
                        switch (Matchnet.Conversion.CInt(item.Value))
                        {
                            case ConstantsTemp.GENDERID_MALE:
                                memberGender.Items.Add(item);
                                break;
                            case ConstantsTemp.GENDERID_FEMALE:
                                memberGender.Items.Add(item);
                                break;
                            case ConstantsTemp.GENDERID_MTF:
                                item.Text = "MTF"; //These options do not have resources on non-Glimpse sites.  I used this quick hack because creating resources takes forever.
                                memberGender.Items.Add(item);
                                break;
                            case ConstantsTemp.GENDERID_FTM:
                                item.Text = "FTM"; //See above.
                                memberGender.Items.Add(item);
                                break;
                            case ConstantsTemp.GENDERID_SEEKING_MALE:
                                seekingGender.Items.Add(item);
                                break;
                            case ConstantsTemp.GENDERID_SEEKING_FEMALE:
                                seekingGender.Items.Add(item);
                                break;
                            case ConstantsTemp.GENDERID_SEEKING_MTF:
                                item.Text = "MTF"; //See above.
                                seekingGender.Items.Add(item);
                                break;
                            case ConstantsTemp.GENDERID_SEEKING_FTM:
                                item.Text = "FTM"; //See above.
                                seekingGender.Items.Add(item);
                                break;
                        }
                    }
                }
                placeMemberGender.Controls.Add(memberGender);
                placeSeekingGender.Controls.Add(seekingGender);
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        private ListItem PopulateItem(DataRow row)
        {
            string itemContent = Matchnet.Lib.Util.Util.CString(row["Content"]);
            string itemValue = Matchnet.Lib.Util.Util.CString(row["Value"]);
            ListItem item = new ListItem(itemContent, itemValue);
            if ((item.Value != "") && (item.Value != Matchnet.Constants.NULL_STRING))
                if ((Matchnet.Conversion.CInt(item.Value) & _genderMask) == Matchnet.Conversion.CInt(item.Value))
                {
                    item.Selected = true;
                }
            return item;
        }


        private int GetGenderMask()
        {
            _genderMask = 0;
            ListItemCollection items = new ListItemCollection();
            foreach (ListItem item in memberGender.Items)
            {
                items.Add(item);
            }
            foreach (ListItem item in seekingGender.Items)
            {
                items.Add(item);
            }
            foreach (ListItem item in items)
            {
                if (item.Selected)
                {
                    _genderMask = _genderMask | Matchnet.Conversion.CInt(item.Value);
                }
            }
            return _genderMask;
        }

        public int GetGenderMaskFromPost()
        {
            int memberGenderValue;
            int seekingGenderValue;

            int.TryParse(Request[memberGender.UniqueID], out memberGenderValue);
            int.TryParse(Request[seekingGender.UniqueID], out seekingGenderValue);

            return (memberGenderValue | seekingGenderValue);
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);

        }
        #endregion
    }
}
