﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HeaderMenu.ascx.cs"
    Inherits="Matchnet.Web.Framework.Ui.HeaderNavigation20.HeaderMenu" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="ft2" TagName="FavoritesNav" Src="~/Applications/Favorites/FavoritesClientNav.ascx" %>
<%@ Register TagPrefix="unn" TagName="UserNotificationsNav" Src="~/Applications/UserNotifications/Controls/UserNotificationsClientNav.ascx" %>
<%@ Register TagPrefix="mn" TagName="AdUnit" Src="/Framework/UI/Advertising/AdUnit.ascx" %>
<div id="nav">
    <%--Navigation menu--%>
    <ul class="sf-navbar <%=GetMenuOrientation %>">
        <asp:Repeater ID="rptMenu" runat="server" OnItemDataBound="BindMenuItem">
            <ItemTemplate>
                <asp:PlaceHolder ID="plcMenuItem" runat="server" Visible="true">
                    <li runat="server" id="liMenu" class="menu-item">
                        <asp:PlaceHolder ID="plcMenuItemMBoxDefaultStart" runat="server" Visible="false">
                            <div class="mboxDefault">
                        </asp:PlaceHolder>
                        <mn:Txt ID="txtMenuItem" runat="server" />
                        <asp:PlaceHolder ID="plcMenuItemMBoxDefaultEndCreate" runat="server" Visible="false">
                            </div><script language="javascript">    
                                                              mboxCreate('<asp:Literal ID="litMBoxName" runat="server" />'<asp:Literal ID="litMBoxNameParams" runat="server" />);</script></asp:PlaceHolder>
                        <mn:AdUnit ID="AdUnitMenuItem" runat="server" GAMIframe="false" GAMPageMode="None"
                            Size="SubTab" />
                        <asp:PlaceHolder ID="phSubItemUl1" runat="server" Visible="true">
                            <ul class="menu-item-sub">
                        </asp:PlaceHolder>
                        <asp:PlaceHolder ID="phSubItems" runat="server">
                            <asp:Repeater ID="rptSubMenu" runat="server" OnItemDataBound="BindMenuItem">
                                <ItemTemplate>
                                    <asp:PlaceHolder ID="plcMenuItem" runat="server" Visible="true">
                                        <li runat="server" id="liMenu">
                                            <mn:Txt ID="txtMenuItem" runat="server" />
                                            <asp:PlaceHolder ID="phSubItemUl1" runat="server" Visible="true">
                                                <ul class="menu-item-sub">
                                            </asp:PlaceHolder>
                                            <asp:PlaceHolder ID="phSubItems" runat="server">
                                                <asp:Repeater ID="rptSubMenu" runat="server" OnItemDataBound="BindMenuItem">
                                                    <ItemTemplate>
                                                        <asp:PlaceHolder ID="plcMenuItem" runat="server" Visible="true">
                                                            <li runat="server" id="liMenu">
                                                                <mn:Txt ID="txtMenuItem" runat="server" />
                                                            </li>
                                                        </asp:PlaceHolder>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </asp:PlaceHolder>
                                            <asp:PlaceHolder ID="phSubItemUl2" runat="server" Visible="true"></ul> </asp:PlaceHolder>
                                        </li>
                                    </asp:PlaceHolder>
                                </ItemTemplate>
                            </asp:Repeater>
                        </asp:PlaceHolder>
                        <asp:PlaceHolder ID="phSubItemUl2" runat="server" Visible="true"></ul> </asp:PlaceHolder>
                    </li>
                </asp:PlaceHolder>
            </ItemTemplate>
        </asp:Repeater>
        <li id="newsNav" class="socialnav-menu news">
            <%--Indicator: News/Notifications--%>
            <asp:PlaceHolder ID="phNews" runat="server" Visible="false"><a href="#" class="spr-parent">
                <span class="spr s-icon-news spr-superscript"></span><span class="cate news">
                    <mn:Txt ID="txtNewsfeed" runat="server" ResourceConstant="NAV_NEWSFEED" />
                </span></a></asp:PlaceHolder>
        </li>
        <li id="favNav" class="socialnav-menu favorites">
            <%--Indicator: Favorites Online--%>
            <asp:PlaceHolder ID="phFavoritesOnline" runat="server" Visible="false"><a href="#"
                class="spr-parent"><span class="spr s-icon-favorites spr-superscript"></span><span
                    class="cate favs">
                    <mn:Txt ID="txtFavorites" runat="server" ResourceConstant="NAV_FAVORITES" />
                </span></a></asp:PlaceHolder>
        </li>
        <%--Social Nav Indicators--%>
        <li id="molNav" class="socialnav-menu mol">
            <%--Indicator: Members Online--%>
            <asp:PlaceHolder ID="phMemberOnline" runat="server" Visible="false"><span id="divMembersOnline"
                class="activity-item online"><span class="activity-icon online-icon">
                    <mn:Image App="Home" runat="server" FileName="icon-status-online-nav.gif" ID="imgMembersOnline"
                        alt="" /></span><asp:HyperLink ID="lnkMembersOnline" runat="Server">
                            <span class="mol-number">
                                <asp:Literal runat="server" ID="litMembersOnlineCount" /></span>
                            <mn:Txt ID="Txt2" runat="server" ResourceConstant="TXT_ONLINE" />
                        </asp:HyperLink>
            </span></asp:PlaceHolder>
        </li>
    </ul>
    <asp:PlaceHolder ID="phFavourites" runat="server">
        <ft2:FavoritesNav ID="FavoritesNav" ClickableElementId="favNav" OnlineIndicatorElementId="favNav"
            runat="server" />
    </asp:PlaceHolder>
    <asp:PlaceHolder ID="phUserNotifications" runat="server">
        <unn:UserNotificationsNav ID="UserNotificationsNav" ClickableElementId="newsNav"
            OnlineIndicatorElementId="newsNav" runat="server" />
    </asp:PlaceHolder>
</div>
<mn:Txt ID="mntxt7573" runat="server" ResourceConstant="JS_GLOBAL_MENU" ExpandImageTokens="true" />
