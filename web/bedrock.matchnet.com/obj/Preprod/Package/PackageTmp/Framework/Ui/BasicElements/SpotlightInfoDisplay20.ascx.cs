﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Util;
using Matchnet.Content.ServiceAdapters.Links;

namespace Matchnet.Web.Framework.Ui.BasicElements
{
    /// <summary>
    /// This control renders the Spotlight Information content layer; also renders icon/text to show or hide the layer
    /// </summary>
    public partial class SpotlightInfoDisplay20 : FrameworkControl
    {
        private bool _Spotlight = false;
        private int _viewedMemberID = Constants.NULL_INT;
        private bool _showImageNoText = false;

        public bool Spotlight
        {
            set
            {
                this._Spotlight = value;
            }
        }

        public int ViewedMemberID
        {
            set
            {
                this._viewedMemberID = value;
            }
        }

        public bool ShowImageNoText
        {
            set
            {
                this._showImageNoText = value;
            }
        }

        public string TextResourceConstant { get; set; }

        #region Event Handlers
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected override void OnPreRender(EventArgs e)
        {
            try
            {

                if (this._Spotlight)
                {
                    // Show Learn about Highlighted Profiles only for profiles that are highlighted
                    System.Text.StringBuilder strShowPopup = new System.Text.StringBuilder();
                    bool showDifferentInfoDisplayToSubscriber = false;
                    if (MemberPrivilegeAttr.IsCureentSubscribedMember(g) && showDifferentInfoDisplayToSubscriber)
                    {
                        // Viewing member that is actively subscribed see feature information
                        // but cannot upgrade to get highlighted profile
                        // Renders html and does not take up space
                        strShowPopup.Append("javascript: document.getElementById('" + pnlSpotlightInfoForMembersWithoutSubscription.ClientID + "').style.display = 'none';");
                        strShowPopup.Append("TogglePopupDivDisplay('" + pnlSpotlightInfoForMembersWithSubscription.ClientID + "');");
                        
                        // Renders html and also takes up space
                        lnkSpotlightInfoForMembersWithSubscriptionCloseText.Text = g.GetResource("TXT_CLOSE", this);
                        lnkSpotlightInfoForMembersWithSubscriptionCloseImage.Text = g.GetResource("IMAGE_CLOSE", null, true, this);

                        lnkSpotlightInfoForMembersWithSubscriptionCloseText.Attributes.Add("href", strShowPopup.ToString());
                        lnkSpotlightInfoForMembersWithSubscriptionCloseImage.Attributes.Add("href", strShowPopup.ToString());
                    }
                    else
                    {
                        // Need to set to PurchaseReasonType.ViewedSpotlightInfo
                        lnkSpotlightInfoSubscribe.NavigateUrl = LinkFactory.Instance.GetLink("/Applications/Subscription/Subscribe.aspx?prtid=" + ((int)PurchaseReasonType.SubscriptionPurchaseFromSpotlightedProfileUpgrade).ToString(), g.Brand, System.Web.HttpContext.Current.Request.Url.ToString()) + "&srid=" + this._viewedMemberID.ToString();
                        if (g.Member.IsPayingMember(g.Brand.Site.SiteID) && Convert.ToBoolean(RuntimeSettings.GetSetting("SPOTLIGHT_INFO_DISPLAY_SEND_SUB_TO_UPSALE", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID)))
                        {
                            lnkSpotlightInfoSubscribe.NavigateUrl = LinkFactory.Instance.GetLink("/Applications/Subscription/Upsale.aspx?PackagesToDisplay=SM&prtid=" + ((int)PurchaseReasonType.SubscriptionPurchaseFromSpotlightedProfileUpgrade).ToString(), g.Brand, System.Web.HttpContext.Current.Request.Url.ToString()) + "&srid=" + this._viewedMemberID.ToString();
                        }

                        // Viewing member that is not actively subscribed will see feature
                        // explanation and link to upgrade to get highlighted profile
                        strShowPopup.Append("javascript: document.getElementById('" + pnlSpotlightInfoForMembersWithSubscription.ClientID + "').style.display = 'none';");
                        strShowPopup.Append("TogglePopupDivDisplay('" + pnlSpotlightInfoForMembersWithoutSubscription.ClientID + "');");

                        lnkSpotlightInfoForMembersWithoutSubscriptionCloseText.Text = g.GetResource("TXT_CLOSE", this);
                        lnkSpotlightInfoForMembersWithoutSubscriptionCloseImage.Text = g.GetResource("IMAGE_CLOSE", null, true, this);

                        lnkSpotlightInfoForMembersWithoutSubscriptionCloseText.Attributes.Add("href", strShowPopup.ToString());
                        lnkSpotlightInfoForMembersWithoutSubscriptionCloseImage.Attributes.Add("href", strShowPopup.ToString());
                    }
                    lnkSpotlightInfo.Attributes.Add("href", strShowPopup.ToString());

                    if (this._showImageNoText)
                    {
                        // Show image without text
                        string resKey = "IMG_LEARN_ABOUT_SPOTLIGHT_PROFILES";
                        lnkSpotlightInfo.Text = g.GetResource(resKey, null, true, this);
                    }
                    else
                    {
                        // Show the text without image
                        if (String.IsNullOrEmpty(TextResourceConstant))
                            lnkSpotlightInfo.Text = g.GetResource("TXT_LEARN_ABOUT_SPOTLIGHT_PROFILES", null, true, this);
                        else
                            lnkSpotlightInfo.Text = g.GetResource(TextResourceConstant, null, true, this);
                    }
                    lnkSpotlightInfo.Visible = true;
                    pnlSpotlightInfoForMembersWithSubscription.Style.Add("display", "none");
                    pnlSpotlightInfoForMembersWithSubscription.Style.Remove("visibility");
                    pnlSpotlightInfoForMembersWithoutSubscription.Style.Add("display", "none");
                    pnlSpotlightInfoForMembersWithoutSubscription.Style.Remove("visibility");
                }
                else
                {
                    lnkSpotlightInfo.Visible = false;
                    // Do not render html
                    pnlSpotlightInfoForMembersWithSubscription.Visible = false;
                    pnlSpotlightInfoForMembersWithoutSubscription.Visible = false;
                }
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        #endregion

    }
}