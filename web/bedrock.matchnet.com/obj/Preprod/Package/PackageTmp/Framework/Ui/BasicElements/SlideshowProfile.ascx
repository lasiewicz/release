﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SlideshowProfile.ascx.cs" Inherits="Matchnet.Web.Framework.Ui.BasicElements.SlideshowProfile" %>
<%@ Register TagPrefix="uc1" TagName="SlideshowPreferences" Src="/Applications/Search/Controls/SlideshowPreferences.ascx" %>
<%@ Register TagPrefix="mnl" Namespace="Matchnet.Web.Framework.Ui.BasicElements.Links"   Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>

<div id="slideshowContainer" runat="server">
    <div id="slideshow-inner" class="clearfix">
	    <h2><mn:Txt ID="txtHeader" runat="server" /></h2>
	    
	    <asp:Panel ID="prefWrapper" runat="server" CssClass="pref-wrapper" Visible="false">
	        <a title="open form" href="#" class="edit-pref"><mn:txt id="txtChangePreferences2" runat="server" resourceconstant="TXT_CHANGE_PREFERENCES" expandimagetokens="false" /></a>
	    </asp:Panel>
	    
	    <asp:Panel ID="theFormDiv" runat="server" CssClass="pref-form" Visible="false">
	        <div id="closeButton">
			        <a href="#" title="Close" class="pref-close"><mn:txt id="txtChangePreferences" runat="server" resourceconstant="TXT_CHANGE_PREFERENCES" expandimagetokens="false" /></a>
		    </div>
	        <div class="pref-form-cont">
                <uc1:SlideshowPreferences runat="server" id="slideshowPreferences" Visible="false" />
		        <%--<mn:Image id="imgSave" runat="server" FileName="btn-save-sm.gif" onclick="SaveSettings();" />--%>
	        </div>
	    </asp:Panel>
        
	    <asp:Panel ID="pnlSlideshowMainContent" runat="server">
	        <div id="slideshow-member">
	            <table cellspacing="0" cellpadding="0" width="100%">
		            <tbody><tr><td>
			            <asp:HyperLink ID="lnkPhoto" runat="server"><mn:Image ID="imgProfile" runat="server" Border="0" CssClass="centered" style="width:246px;height:328px;" /></asp:HyperLink>
		            </td></tr></tbody>
	            </table>
	        </div>
        	
	        <div id="slideshow-details">
		        <div id="slideshow-info">
			        <h3><asp:HyperLink ID="lnkUserName" runat="server" /></h3>
			        <p><asp:Literal ID="litAge" runat="server"></asp:Literal><br />
			            <asp:Literal ID="litLocation" runat="server"></asp:Literal></p>
			        <p><asp:Literal ID="litGender" runat="server"></asp:Literal></p>
			        <p><mn:Txt runat="server" ResourceConstant="TXT_FOR" />&nbsp;<asp:Literal ID="litRelationshipMask" runat="server"/><asp:Literal ID="litSeekingFor" runat="server"></asp:Literal></p>
			        <p><mnl:Link runat="server" ID="lnkAboutMe" TitleResourceConstant="TTL_VIEW_MY_PROFILE" /></p>
		        </div>
		        <div class="click20 click20-vertical">

                    <asp:PlaceHolder ID="phDoWeClickTitle" runat="server">
			            <h3><mn:Txt ID="txtDoWeClick" runat="server"  /></h3>
			        </asp:PlaceHolder>
			        <div class="click20-ynm">
				        <div class="click20-option">
					        <span class="spr-btn sbtn-click20-yes" runat="server" id="spanYes"><span><mn:txt id="mntxt1383" runat="server" resourceconstant="YNM_YES" expandimagetokens="true" /></span></span>
					        <%--<p><mn:Txt ID="TxtYes" runat="server" ResourceConstant="TXT_YES"/></p>--%>
				        </div>

				        <div class="click20-option">
					        <span class="spr-btn sbtn-click20-no" runat="server" id="spanNo"><span><mn:txt id="mntxt1384" runat="server" resourceconstant="YNM_NO" expandimagetokens="true" /></span></span>
					        <%--<p><mn:Txt ID="txtNo" runat="server" ResourceConstant="TXT_NO" /></p>--%>
				        </div>
				        <div class="click20-option">
					        <span class="spr-btn sbtn-click20-maybe" runat="server" id="spanMaybe"><span><mn:txt id="mntxt1385" runat="server" resourceconstant="YNM_MAYBE" expandimagetokens="true" /></span></span>
					        <%--<p><mn:Txt ID="txtMaybe" runat="server" ResourceConstant="TXT_MAYBE" /></p>--%>

				        </div>
			        </div>
		        </div>
	        </div>
   	    
        </asp:Panel>
        
        <div class="slideshow-waiting" style="display:none;">
            <h2 class="text-center"><mn:txt id="mntxt3616" runat="server" resourceconstant="TXT_LOADING_MSG" expandimagetokens="true" /></h2>
            <mn:image id="mnimage6491" runat="server" filename="ajax-loader.gif" alt="" CssClass="centered" />
        </div>
        
        <asp:Panel ID="pnlSlideshowEnd" runat="server" Visible="false">
            <div id="Div1">
                <div id="slideshow-end">
                    <h2><mn:txt id="mntxt1293" runat="server" resourceconstant="TXT_SLIDESHOW_END_MSG_HEADER" expandimagetokens="true" /></h2>
                    <p><asp:HyperLink ID="lnkSearchPreffrences" NavigateUrl="/Applications/Search/SearchPreferences.aspx" runat="server"><mn:txt id="mntxt1294" runat="server" resourceconstant="TXT_SLIDESHOW_END_MSG_BODY_LNK" expandimagetokens="true" /></asp:HyperLink> <mn:txt id="Txt1" runat="server" resourceconstant="TXT_SLIDESHOW_END_MSG_BODY_TXT" expandimagetokens="true" /></p>
                </div>
            </div>
        </asp:Panel>
        
        <asp:Panel ID="pnlConfigurableSlideshowEnd" runat="server" Visible="false">
            <div id="Div2">
                <div id="slideshow-end">
                    <h2><mn:txt id="txtConfigurableSlideshowEndHeader" runat="server" resourceconstant="TXT_CONFIGURABLE_SLIDESHOW_END_MSG_HEADER" expandimagetokens="true" /></h2>
                    <p><mn:txt id="txtConfigurableSlideshowBody" runat="server" resourceconstant="TXT_CONFIGURABLE_SLIDESHOW_END_MSG_BODY_TXT" expandimagetokens="true" /></p>
                </div>
            </div>
        </asp:Panel>
    </div>
</div>