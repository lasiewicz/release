﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HeroProfileFilmStrip.ascx.cs"
    Inherits="Matchnet.Web.Framework.Ui.BasicElements.HeroProfileFilmStrip" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mnl" Namespace="Matchnet.Web.Framework.Ui.BasicElements.Links"
    Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mnbe" Namespace="Matchnet.Web.Framework.Ui.BasicElements"
    Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="uc1" TagName="NanoProfileFilmStrip" Src="NanoProfileFilmStrip.ascx" %>
<script type="text/javascript" src="/javascript20/library/jquery.tmpl.js"></script>
<script type="text/javascript" src="/webservices/member/membersvc.js"></script>
<script type="text/javascript">
//<![CDATA[
var ageformat = "<%= GetAgeFormat()%>";
var viewprofilelinkformat = "<% =ViewProfileLinkFormat%>";
var emaillinkformat = "<% =EmailLinkFormat%>";
var moreprofileformat="<%=g.GetResource("TXT_VIEW_MEMBER_ARROWS",this)  %>";
var morephotosformat="<%=g.GetResource("TXT_VIEW_MEMBER_PHOTOS_FORMAT",this)  %>";
var morephotoformat="<%=g.GetResource("TXT_VIEW_MEMBER_PHOTO_FORMAT",this)  %>";
var emailmebuttontext="<%=EmailButtonText%>";
var requestphototext="<%=g.GetResource("TXT_CLICK_HERE_TO_REQUEST",this)  %>";
var viewmore="test";
//var moreresultslink="<% =MoreResultsLink %>";
var moreresultstext='<% =MoreResultsText %>';

var cache={};
 function GetMember(memberid, ordinal) {
        if(cache[memberid])
        {// console.log("found in cache"); 
         ShowFromCache(memberid) }
        else
        {//console.log("not ound in cache");  
          GetMemberByID(memberid,ordinal,AjaxSucceeded, AjaxFailed);  }
        return false;
    }
    function AjaxSucceeded(result) {
        if (result.d.MemberID >= 0) {
            var heroTpl = $j("#HeroTemplate").render(result.d);
            $j("#hp-hero-profile #hero-tpl").replaceWith(heroTpl);
            cache[result.d.MemberID]=result.d;
                
        }
        else {
          //  alert("An error occurred and the update was not made.");
        }
    }
    function ShowFromCache(memberid) {
        
           var heroTpl = $j("#HeroTemplate").render(cache[memberid]);
            $j("#hp-hero-profile #hero-tpl").replaceWith(heroTpl);     
        
    }
    function AjaxFailed(result) {
       // console.log(result);
    }
//]]>
</script>
<%--<input type="button" id="ajaxtest" onclick="GetMember(987655800)" value="Test" />--%>
<asp:PlaceHolder ID="Content1" runat="server">
    <script id="HeroTemplate" type="text/html">
<div id="hero-tpl">
   {% if(ApprovedPhotosCount - 1 > 1){ 
       viewmore=morephotosformat.replace('{0}',ApprovedPhotosCount - 1);
        }; %}
        
   {% if(ApprovedPhotosCount - 1 == 1){ 
       viewmore=morephotoformat.replace('{0}',ApprovedPhotosCount - 1);
        }; %}
     {% if(ApprovedPhotosCount - 1  == 0){ 
            viewmore=moreprofileformat;
    }; %}

{% 
    viewprofilelink=viewprofilelinkformat.replace('{0}',MemberID);
    viewprofilelink=viewprofilelink.replace('{1}',Ordinal); 
   %}
<div id="hp-hero-profile-pic" class="picture">
    
<table cellspacing="0" cellpadding="0">
    <tbody>
        <tr>
            <td class="content-centered">
        {% if(LargePhotos.length > 0){ %}
            <a href="{%=viewprofilelink %}"><div style="width:185px;"><img class="centered hero-link" src="{%= LargePhotos[0]%}" /></div></a>
        {% }; %}
      
        {% if(ColorCodeText){ %}
             <div class="cc-pic-tag cc-pic-tag-lrg cc-{%=ColorCodeText %}-bg">
                    Color: <strong>
                        {%=ColorCodeText %}</strong>
                    <div class="cc-pic-tag-help">
                        <b>What does this mean?</b><br />
                        Click the bar to find out!</div>
                </div>
        {% }; %}
        </td>
    </tr>
</table>
</div>
<div id="hp-hero-profile-cont">
    {%=moreresultstext %}
    <h3 class="username"><a href="{%=viewprofilelink %}">{%= UserName %}</a></h3>
    <div class="info-basics">
    <ul class="overview">
        <li>{%= ageformat.replace('{0}',Age) %}</li>
        <li>{%= DisplayLocation %}</li>
        <%=GetEducationLevelTemplate() %>
        <li><a href="{%=viewprofilelink %}" class="emphasized">{%=viewmore %}</a></li>
    </ul>
    </div>
    <div class="action-email"><a href="{%=emaillinkformat.replace('{0}',MemberID) %}" class="btn link-primary large">{%=emailmebuttontext%}</a></div>
</div>
</div>

    </script>
</asp:PlaceHolder>
<div id="hp-hero-profile">
    <div id="hero-tpl">
        <div id="hp-hero-profile-pic" class="picture">
            <!--large photo-->
            <table cellspacing="0" cellpadding="0">
                <tbody>
                    <tr>
                        <td class="content-centered">
                            <div style="width: 185px;">
                                <mn:Image ID="imgProfile" runat="server" Border="0" CssClass="centered hero-link" /></div>
                            <!--no photo with link to upload-->
                            <%--  <asp:Panel ID="pnlNoPhoto" runat="server" CssClass="no-photo" Visible="false">
                            <asp:HyperLink ID="lnkRequestPhotoUpload" CssClass="link-primary" runat="server" NavigateUrl="/Applications/Email/Compose.aspx?MemberId=">
                                <mn:txt id="mntxt6171" runat="server" resourceconstant="TXT_CLICK_HERE_TO_REQUEST" expandimagetokens="true" />
                            </asp:HyperLink>
                        </asp:Panel>--%>
                            <asp:PlaceHolder ID="phColorCode" runat="server" Visible="false">
                                <div class="cc-pic-tag cc-pic-tag-lrg cc-<%=_ColorText.ToLower() %>-bg">
                                    Color: <strong>
                                        <%=_ColorText %></strong>
                                    <div class="cc-pic-tag-help">
                                        <b>What does this mean?</b><br />
                                        Click the bar to find out!</div>
                                </div>
                            </asp:PlaceHolder>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <asp:Panel ID="panelHeroProfileContent" runat="server">
            <%--<asp:Panel ID="panelHeroProfileContent" runat="server" CssClass="hero-profile-content-div hero-profile-spotlight">--%>
            <div id="hp-hero-profile-cont">
                <mn:Txt runat="server" ID="txtTitle" />
                <h3 class="username">
                    <asp:HyperLink runat="server" ID="lnkUserName1" /></h3>
                <div class="info-basics">
                    <!--basic info-->
                    <ul class="overview">
                        <li>
                            <asp:Literal ID="literalAge" runat="server"></asp:Literal>&nbsp;<%--<mn2:FrameworkLiteral id="literalAgeDisplay" ResourceConstant="TXT_YEARS_OLD" runat="server"></mn2:FrameworkLiteral>--%></li>
                        <li>
                            <asp:Literal ID="literalLocation" runat="server"></asp:Literal></li>
                        <asp:PlaceHolder runat="server" ID="plcEducationLevel">
                            <asp:Label runat="server" ID="txtEducationLevel" class="education-level"></asp:Label>
                        </asp:PlaceHolder>
                        <li>
                            <asp:HyperLink ID="lnkViewMorePhotos" runat="server">
                                <asp:Literal ID="litViewMorePhotos" runat="server"></asp:Literal></asp:HyperLink></li>
                    </ul>
                </div>
                <!--email link-->
                <div class="action-email">
                    <asp:HyperLink ID="lnkEmail" runat="server" CssClass="btn link-primary large">
                        <mn:Txt ID="mntxt9707" runat="server" ResourceConstant="TXT_EMAIL_ME_BUTTON" ExpandImageTokens="true" />
                    </asp:HyperLink>
                </div>
            </div>
        </asp:Panel>
    </div>
</div>
<asp:PlaceHolder ID="phFilmStrip" runat="server" Visible="true">
    <div id="hp-hero-strip" class="results">
        <uc1:NanoProfileFilmStrip ID="NanoProfileFilmStripYourMatches" runat="server" />
    </div>
</asp:PlaceHolder>
