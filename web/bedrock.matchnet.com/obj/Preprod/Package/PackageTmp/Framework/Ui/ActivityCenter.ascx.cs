﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Session.ServiceAdapters;
using System.Text;
using Matchnet.Session.ValueObjects;
using Matchnet.Member.ValueObjects.Photos;
using Matchnet.Web.Framework.Managers;
using Matchnet.Web.Framework.Menus;

namespace Matchnet.Web.Framework.Ui
{
    public partial class ActivityCenter : FrameworkControl
    {
        #region Member Variables

        #endregion

        protected string H2TagOpen
        {
            get
            {
                string tag = "<h2>";
                if (g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.Cupid)
                {
                    tag = string.Empty;
                }
                return tag;
            }
        }

        protected string H2TagClose
        {
            get
            {
                string tag = "</h2>";
                if (g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.Cupid)
                {
                    tag = string.Empty;
                }
                return tag;
            }
        }

        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.JDateCoIL)
                {
                    LoadFullActivityCenter();
                }
                else if (g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.BlackSingles && g.AppPage.ControlName.ToLower() == "attributeedit")
                {
                    LoadSimpleActivityCenter();
                }
                else if (g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.BBW && g.AppPage.ControlName.ToLower() == "attributeedit")
                {
                    LoadSimpleActivityCenter();
                }
                else if (g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.Cupid && g.AppPage.ControlName.ToLower() == "attributeedit")
                {
                    LoadSimpleActivityCenter();
                }
                else
                {
                    if (MenuUtils.IsSocialNavEnabled(g) && (!g.IsSiteMingle || Matchnet.Web.Applications.MemberProfile.ProfileTabs30.ProfileUtility.IsProfile30Enabled(g)))
                    {
                        LoadSimpleActivityCenter();
                    }
                    else
                    {
                        LoadFullActivityCenter();
                    }
                }
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            if (!MenuUtils.IsSocialNavEnabled(g) || (g.IsSiteMingle && !Matchnet.Web.Applications.MemberProfile.ProfileTabs30.ProfileUtility.IsProfile30Enabled(g)))
            {
                // check for online status onprerender (no page_load) so that it fires AFTER saving of displaysettings 16766 to get most recent hidemask
                // hide Instant Messenger checkIM if this page does not support checking IM
                // Don't check IM on page where we don't want interruptions
                // such as during subscription, initial registration and admin pages (related bug 15463)
                // Don't check IM if user has chosen to be hidden.
                // Also, if the page is Unsuspend, don't load Checkim because FrameBuster will hijack the page
                // (this might not be the best place to do this, however time is of the essence - this note borrowed from elsewhere)
                if (!_g.SilentCheckIM
                    && g.Member != null
                    && g.AppPage.PageName != "Unsuspend"
                    && g.Member.GetAttributeInt(g.Brand, "SelfSuspendedFlag") != 1
                    && !Matchnet.Web.Lib.LayoutTemplateBase.IsAdminPage(g)
                    )
                {
                    plcCheckIM.Visible = true;
                    //show online status as hidden if user wants to be hidden (should not check IM's or add to MOL. That is handled elsewhere by javascript)
                    plcIMHidden.Visible = (g.Member.GetAttributeInt(g.Brand, "HideMask") & (Int32)WebConstants.AttributeOptionHideMask.HideMembersOnline) == (Int32)WebConstants.AttributeOptionHideMask.HideMembersOnline;
                    plcIMActive.Visible = !plcIMHidden.Visible;

                    //im status
                    if (Request.QueryString["showimstatus"] != null) { this.plcIMOnlineDebug.Visible = true; }

                }

                // If this is cupid, hide the MOL count
                //07/27/2009 SK changed to be a setting
                plcMemberOnline.Visible = SettingsManager.GetSettingBool(SettingConstants.MOL_DISPLAY_FLAG, g.Brand);


                //hide Active IM section if member is blocked
                if (_g.EmailVerify.IsMemberBlocked(_g.Member))
                {
                    plcIMActive.Visible = false;
                }
            }

            if (!String.IsNullOrEmpty(TrackingParam))
            {
                hplUsername.NavigateUrl += "&" + TrackingParam;
                hplUsername2.NavigateUrl += "&" + TrackingParam;
            }

            if (g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.JDateCoIL)
            {
                divActivity.Visible = plcNewEmailMessages.Visible || plcCheckIM.Visible ||
                plcNewClicks.Visible || plcMatchMeter.Visible || plcSMSAlerts.Visible;
            }

            base.OnPreRender(e);
        }

        #endregion

        #region Methods
        private void LoadFullActivityCenter()
        {
            //tt # 19623
            string destinationUrl = g.GetDestinationURL(false);

            phActivityCenter.Visible = false;
            plcActivityCenter.Visible = false;

            //set links for login and logoff
            if (destinationUrl.Length == 0)
            {
                destinationUrl = Request.RawUrl.ToString();
            }

            txtLogin.Href = FrameworkGlobals.LinkHref(
                "/Applications/Logon/Logon.aspx?DestinationURL=" + destinationUrl, true);
            txtLogout.Href = FrameworkGlobals.LinkHref("/Applications/Logon/Logoff.aspx", true);

            //set members online count - always visible by default
            lnkMembersOnline.Visible = true;
            if (g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.Cupid)
            {
                litMembersOnlineCount.Visible = false;
            }
            else
            {
                litMembersOnlineCount.Text = Convert.ToString(MembersOnlineManager.Instance.GetMOLCommunityCount(g.Brand, false));

            }

            lnkMembersOnline.NavigateUrl = FrameworkGlobals.LinkHref("/Applications/MembersOnline/MembersOnline.aspx",
                                                                     true);
            lnkMembersOnline.ToolTip = g.GetResource("NAV_MEMBERS_ONLINE", true);
            imgMembersOnline.ToolTip = lnkMembersOnline.ToolTip;

            if (g.Member != null)
            {
                if (g.AppPage.App.ID == (int)WebConstants.APP.Home && g.AppPage.ID == (int)WebConstants.PageIDs.Default || g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.Cupid || g.IsSiteMingle)
                {
                    phActivityCenter.Visible = true;
                    plcActivityCenter.Visible = true;
                    //If member is logged in show Hi Username and logoff link
                    plcLogoff.Visible = true;
                    hplUsername.Text = g.Member.GetUserName(_g.Brand);
                    //FrameworkGlobals.Ellipsis(g.Member.GetUserName(_g.Brand), 9);
                    hplUsername.ToolTip = g.Member.GetUserName(_g.Brand);
                    string viewProfileUrl =
                        FrameworkGlobals.LinkHref("/Applications/MemberProfile/ViewProfile.aspx?EntryPoint=99999", true);
                    hplUsername.NavigateUrl = viewProfileUrl;


                    //show additional profile information on homepage
                    if (((g.AppPage.App.ID == (int)WebConstants.APP.Home) &&
                        g.AppPage.ID == (int)WebConstants.PageIDs.Default) || g.IsSiteMingle)
                    {
                        //03102009 TL - PM-143 MiniSearch project has changed so thumbnail in activity center only displays on homepage
                        //display thumbnail
                        //MemberPhotoHelper photoHelper = new MemberPhotoHelper(RuntimeSettings.Instance);
                        noPhoto.NoPhotoFileName = MemberPhotoDisplayManager.Instance.GetNoPhotoFile(NoPhotoImageType.TinyThumbV2, true);
                        noPhoto.MemberID = g.Member.MemberID;
                        noPhoto.Mode = Matchnet.Web.Framework.Ui.PageElements.NoPhoto20.PhotoMode.UploadPhotoNow;
                        noPhoto.DestURL = "/Applications/MemberProfile/MemberPhotoEdit.aspx";
                        imgThumb.NavigateUrl = "/Applications/MemberProfile/MemberPhotoEdit.aspx";
                        Photo photo = MemberPhotoDisplayManager.Instance.GetDefaultPhoto(g.Member, g.Member, g.Brand);
                        if (photo != null && photo.IsApproved)
                        {
                            if (!MemberPhotoDisplayManager.Instance.PhotoIsEmpty(photo, PhotoType.Thumbnail, g.Brand))
                            {
                                imgThumb.ImageUrl = MemberPhotoDisplayManager.Instance.GetPhotoDisplayURL(g.Member, g.Member, g.Brand,
                                                                       photo, PhotoType.Thumbnail,
                                                                       PrivatePhotoImageType.Thumb,
                                                                        NoPhotoImageType.Thumb);

                                noPhoto.Visible = false;
                                imgThumb.Visible = true;
                            }
                        }

                        phEditProfileInfo.Visible = true;
                        panelProfileIMageLargeVersion.Visible = true;
                    }


                    //show new email messages if any
                    if (g.hasNewMessage)
                    {
                        plcNewEmailMessages.Visible = false;
                        lnkNewMessages.NavigateUrl = FrameworkGlobals.LinkHref("/Applications/Email/MailBox.aspx", true);
                        lnkNewMessages.ToolTip = g.GetResource("TTL_YOU_HAVE_NEW_MESSAGES_EXCLAMATION", this);
                        imgNewMessages.ToolTip = lnkNewMessages.ToolTip;

                    }
                    //show new clicks if any
                    if (g.Member.GetAttributeBool(g.Brand, "HasNewMutualYes") && g.IsYNMEnabled)
                    {
                        plcNewClicks.Visible = false;
                        lnkNewClicks.NavigateUrl =
                            FrameworkGlobals.LinkHref("/Applications/HotList/View.aspx?CategoryID=-15", true);
                        lnkNewClicks.ToolTip = g.GetResource("TTL_YOU_HAVE_NEW_CLICKS_EXCLAMATION", this);
                        imgMatches.ToolTip = lnkNewClicks.ToolTip;
                    }

                    if (g.AppPage.ID != (int)WebConstants.PageIDs.Default)
                    {
                        int mmsett = Int32.Parse(RuntimeSettings.GetSetting("MatchTest_App_Settings", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID));
                        int mmenumval = (Int32)WebConstants.MatchMeterFlags.EnableApp;

                        if ((mmsett & mmenumval) == mmenumval)
                        {
                            plcMatchMeter.Visible = true;
                            lnkMatchMeter.NavigateUrl = FrameworkGlobals.LinkHref("/Applications/CompatibilityMeter/Welcome.aspx", true);
                        }
                        else
                        {
                            plcMatchMeter.Visible = false;
                        }


                        if (RuntimeSettings.GetSetting("SMS_ALERTS_ENABLED", g.Brand.Site.Community.CommunityID,
                                                   g.Brand.Site.SiteID, g.Brand.BrandID).ToLower() == "true")
                        {
                            plcSMSAlerts.Visible = true;
                            lnkSMSAlerts.NavigateUrl = FrameworkGlobals.LinkHref("/Applications/MemberServices/SMSAlert.aspx",
                                                                                 true);
                        }
                    }
                }
            }

            else
            {
                //If member is NOT logged in show Join Now Welcome and logon link
                // per issue 14034
                //15909 join now links should maintain destination url
                string url = FrameworkGlobals.GetRegistrationPageURL(g);
                txtJoinNow.Href = FrameworkGlobals.LinkHref(url, true);
                plcJoinNow.Visible = true;

            }
        }

        private void LoadSimpleActivityCenter()
        {
            phActivityCenter.Visible = false;
            plcActivityCenter.Visible = false;
            if (g.Member != null)
            {
                if (g.AppPage.App.ID == (int)WebConstants.APP.Home && g.AppPage.ID == (int)WebConstants.PageIDs.Default)
                {
                    phActivityCenter.Visible = true;
                    plcSimpleActivityCenter.Visible = true;

                    bool useFirstnameOverUsername = Convert.ToBoolean(RuntimeSettings.GetSetting("USE_FIRSTNAME_OVER_USERNAME", _g.Brand.Site.Community.CommunityID, _g.Brand.Site.SiteID));
                    if (useFirstnameOverUsername)
                    {
                        hplUsername2.Text = g.Member.GetAttributeText(_g.Brand, "SiteFirstName", g.Member.GetUserName(g.Brand)); //FrameworkGlobals.Ellipsis(g.Member.Username, 9);
                        hplUsername2.ToolTip = g.GetResource("FIRSTNAME_OVER_USERNAME_TOOLTIP", this);
                    }
                    else
                    {
                        hplUsername2.Text = g.Member.GetUserName(g.Brand); //FrameworkGlobals.Ellipsis(g.Member.Username, 9);
                        hplUsername2.ToolTip = g.Member.GetUserName(g.Brand);
                    }

                    //MemberPhotoHelper photoHelper = new MemberPhotoHelper(RuntimeSettings.Instance);
                    string viewProfileUrl = FrameworkGlobals.LinkHref("/Applications/MemberProfile/ViewProfile.aspx?EntryPoint=99999", true);
                    hplUsername2.NavigateUrl = viewProfileUrl;

                    noPhoto2.NoPhotoFileName = MemberPhotoDisplayManager.Instance.GetNoPhotoFile(NoPhotoImageType.TinyThumbV2, true);
                    noPhoto2.MemberID = g.Member.MemberID;
                    noPhoto2.Mode = Matchnet.Web.Framework.Ui.PageElements.NoPhoto20.PhotoMode.UploadPhotoNow;
                    noPhoto2.DestURL = "/Applications/MemberProfile/MemberPhotoEdit.aspx";
                    imgThumb2.NavigateUrl = "/Applications/MemberProfile/MemberPhotoEdit.aspx";
                    Photo photo = MemberPhotoDisplayManager.Instance.GetDefaultPhoto(g.Member, g.Member, g.Brand);
                    if (photo != null && photo.IsApproved)
                    {
                        if (!MemberPhotoDisplayManager.Instance.PhotoIsEmpty(photo, PhotoType.Thumbnail, g.Brand))
                        {
                            imgThumb2.ImageUrl = MemberPhotoDisplayManager.Instance.GetPhotoDisplayURL(g.Member, g.Member, g.Brand,
                                                                       photo, PhotoType.Thumbnail,
                                                                       PrivatePhotoImageType.Thumb,
                                                                        NoPhotoImageType.Thumb);

                            noPhoto2.Visible = false;
                            imgThumb2.Visible = true;
                        }
                    }

                    phEditProfileInfo2.Visible = true;
                    txtEditProfile2.Visible = true;
                    
                    panelProfileIMageLargeVersion2.Visible = true;
                }
            }
        }

        public static bool HideProfileCompletionMeter(ContextGlobal g)
        {
            if (Matchnet.Web.Applications.MemberProfile.ProfileTabs30.ProfileUtility.IsProfile30Enabled(g) && Matchnet.Web.Applications.MemberProfile.ProfileTabs30.ProfileUtility.IsProfile30EditEnabled(g))
            {
                //check profile completion meter setting
                string isHidden = "false";
                try
                {
                    isHidden = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("HIDE_PROFILE30_CURRENT_COMPLETION_METER", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID);
                }
                catch (Exception ex)
                {
                    //setting missing
                    isHidden = "false";
                }

                if (isHidden.ToLower().Trim() == "true")
                    return true;
                else
                    return false;
            }
            else
            {
                return false;
            }

        }
        #endregion

        #region Properties
        public PlaceHolder JoinNowSection
        {
            get { return this.plcJoinNow; }
        }

        public PlaceHolder ActivityCenterSection
        {
            get { return this.plcActivityCenter; }
        }

        public string TrackingParam { get; set; }

        #endregion

    }
}
