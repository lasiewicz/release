using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Matchnet.Content.ServiceAdapters.Links;
using Matchnet.List.ServiceAdapters;
using Matchnet.List.ValueObjects;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ServiceAdapters.Interfaces;
using Matchnet.Member.ValueObjects.Interfaces;
using Matchnet.Member.ValueObjects.MemberDTO;
using Matchnet.Member.ValueObjects.Photos;
using Matchnet.Web.Framework.Managers;
using Matchnet.Web.Applications.MemberProfile;

namespace Matchnet.Web.Framework.Ui.BasicElements
{
    public class MiniProfile : FrameworkControl, IMiniProfile
    {
        public delegate void SaveNoteEventHandler(object sender, SaveNoteEventArgs e);
        public event SaveNoteEventHandler SaveNote;
        protected string strDefaultNote = String.Empty;



        #region Members

        protected Add2List add2List;
        protected Add2List add2List1;
        protected PlaceHolder plcBreaks;
        private IMemberDTO _Member;
        private ResultContextType _DisplayContext = Matchnet.Web.Framework.Ui.BasicElements.ResultContextType.SearchResult;
        private string _categoryID = String.Empty;
        private int _counter = 0;
        private int _memberID;
        private int _ordinal;
        private BreadCrumbHelper.EntryPoint _myEntryPoint;
        private MOCollection _myMOCollection;
        private HotListCategory _hotListCategory;
        private bool _isHotListFriend = false;
        private bool _bEnableSingleSelect = false;
        private string ViewProfileUrl;
        private bool _currentlyOnline = false;
        private bool _isHighlighted = false;
        private bool _isSpotlight = false;
        private bool _overrideSelfSpotlight = false;
        protected Matchnet.Web.Framework.Image ImageEmailMe;
        protected Matchnet.Web.Framework.Image ImageList;
        protected Matchnet.Web.Framework.Image ImageIsUpdate;
        protected Matchnet.Web.Framework.Image ImageIsNew;
        protected Matchnet.Web.Framework.Image imgPremiumAuthenticated;
        protected Matchnet.Web.Framework.Image ImageThumb;
        protected Matchnet.Web.Framework.Image ImageYes;
        protected Matchnet.Web.Framework.Image ImageMaybe;
        protected Matchnet.Web.Framework.Image ImageNo;
        protected Matchnet.Web.Framework.Image ImagePhone;
        protected Matchnet.Web.Framework.Txt TxtYes;
        protected Matchnet.Web.Framework.Txt TxtMaybe;
        protected Matchnet.Web.Framework.Txt TxtNo;
        protected Matchnet.Web.Framework.Txt TxtPhone;
        protected HyperLink LinkUserName;
        protected System.Web.UI.HtmlControls.HtmlAnchor lnkEmailMe;
        protected System.Web.UI.HtmlControls.HtmlAnchor lnkTeaseMe;
        protected System.Web.UI.HtmlControls.HtmlAnchor lnkTeaseMe1;
        protected Literal TextGender;
        protected Literal TextAge;
        protected Literal TextRegion;
        protected Literal TextHeadline;
        protected HtmlInputHidden YNMVoteStatus;
        protected HtmlTable TablePhone;
        protected LastLoginDate lastLoginDate;
        protected Matchnet.Web.Framework.Txt txtPixel;
        protected Matchnet.Web.Framework.Image imgPixel;
        protected Literal plcPixel;
        // hot list bar
        protected MiniProfileHotListBar HotListBar;
        protected PlaceHolder plcHotListBar;

        protected Matchnet.Web.Framework.Image imgChat;
        protected Matchnet.Web.Framework.Image imgChat1;
        protected Matchnet.Web.Framework.Image icon_tease;
        protected Matchnet.Web.Framework.Ui.PageElements.NoPhoto noPhoto;
        protected System.Web.UI.HtmlControls.HtmlAnchor lnkHotList;
        protected System.Web.UI.HtmlControls.HtmlAnchor lnkECard;
        protected System.Web.UI.HtmlControls.HtmlAnchor lnkECard1;
        protected Matchnet.Web.Framework.Txt txtHotList;
        protected Matchnet.Web.Framework.Image imgBothSaidYes;

        const int MAX_STRING_LENGTH = 31;
        protected System.Web.UI.HtmlControls.HtmlInputText txtFavoriteNote;
        protected Matchnet.Web.Framework.Ui.FormElements.FrameworkButton btnSave;
        protected System.Web.UI.WebControls.HyperLink lnkOnline;
        protected System.Web.UI.WebControls.HyperLink lnkOnline1;
      
        protected Txt txtOnline;
        protected Txt txtOnline1;
        // protected PlaceHolder plcHotListMove;

        protected Matchnet.Web.Framework.SearchResultProfile searchResult;
        protected Panel pnlComments;
        protected bool _transparency = false;

        protected Matchnet.Web.Framework.Image ImageOnline;
        protected System.Web.UI.HtmlControls.HtmlTableRow TableRowComments;
        protected Matchnet.Web.Framework.Txt TxtThinkYoud;
        protected Matchnet.Web.Framework.Txt txtClick;
        protected System.Web.UI.HtmlControls.HtmlAnchor lnkClick;
        protected Matchnet.Web.Framework.Txt txtMore;
        protected Matchnet.Web.Framework.Txt txtEmail;
        protected Matchnet.Web.Framework.Image trans;
        protected Matchnet.Web.Framework.Txt txtTease;
        protected PlaceHolder plcTransparent;
        protected Matchnet.Web.Framework.Txt txtSelect;
        protected Matchnet.Web.Framework.Txt txtDeselect;
        protected System.Web.UI.WebControls.LinkButton lbRemoveProfile;

        protected Matchnet.Web.Framework.Ui.BasicElements.HighlightProfileInfoDisplay ucHighlightProfileInfoDisplay;
        protected Matchnet.Web.Framework.Ui.BasicElements.SpotlightInfoDisplay ucSpotlightInfoDisplay;

        protected Matchnet.Web.Framework.Image icon_ecard;
        protected Matchnet.Web.Framework.Txt txtECard;
        protected HtmlTableCell TableCellECard;

        protected System.Web.UI.HtmlControls.HtmlTableCell tdAnimBG;
        protected System.Web.UI.WebControls.Literal litContainer;
        protected PlaceHolder plcNormalView;
        protected PlaceHolder plcSpotlightView;
        protected PlaceHolder plcSpotlight;
        protected Literal TextInfo;
        protected Repeater rptPhotos;
        protected ProfileEnlarge ucProfileEnlarge;

        protected Txt txtBothLike;
        protected Txt txtSpotlight;
        protected Txt txtSpotlightPromo;
        protected Txt txtSelfSpotlight;
        protected Matchnet.Web.Framework.Image memberSpotlight_curveBottom;
        protected PlaceHolder plcSpotlightSettings;
        bool promotionMemberFlag = false;

        private string _yesFileNameSelected = "icon-click-y-on.gif";
        private string _noFileNameSelected = "icon-click-n-on.gif";
        private string _maybeFileNameSelected = "icon-click-m-on.gif";

        protected Matchnet.Web.Framework.Ui.BasicElements.MatchMeterInfoDisplay ucMatchMeterInfoDisplay;
        private bool _IsMatchMeterEnabled;
        private decimal _MatchScore;

        protected PlaceHolder plcYNM;
        protected PlaceHolder plcEmailMe;
        protected HyperLink lnkEmail;
        protected PlaceHolder plcUpperComm;
        protected PlaceHolder plcLowerComm;

        MiniProfileHandler _handler = null;
        #endregion
        #region IMiniProfile implementation

        public FrameworkControl ThisControl
        {
            get { return this; }
        }

        public Literal TXTGender
        {
            get { return TextGender; }
        }
        public Literal TXTAge
        {
            get { return TextAge; }
        }
        public Literal TXTRegion
        {
            get { return TextRegion; }
        }
        public Literal TXTHeadline
        {
            get { return TextHeadline; }
        }

        public Matchnet.Web.Framework.SearchResultProfile SearchResult
        {
            get { return searchResult; }
        }

        public HyperLink LNKUserName { get { return LinkUserName; } }
        public Matchnet.Web.Framework.Image IMGIsNew { get { return ImageIsNew; } }
        public Matchnet.Web.Framework.Image IMGThumb { get { return ImageThumb; } }
        public Matchnet.Web.Framework.Image IMGIsUpdate { get { return ImageIsUpdate; } }
        public System.Web.UI.WebControls.Literal LITContainer { get { return litContainer; } }
        public Matchnet.Web.Framework.Image IMGPremiumAuthenticated { get { return imgPremiumAuthenticated; } }
        public Matchnet.Web.Framework.Ui.BasicElements.HighlightProfileInfoDisplay UCHighlightProfileInfoDisplay { get { return ucHighlightProfileInfoDisplay; } }
        public LastLoginDate LastLoginDate { get { return lastLoginDate; } }
        public Panel PNLComments { get { return pnlComments; } }
        public PlaceHolder PLCBreaks { get { return plcBreaks; } }
        public HtmlInputText TXTFavoriteNote { get { return txtFavoriteNote; } }
        public FormElements.FrameworkButton BTNSave { get { return btnSave; } }
        public LinkButton LBRemoveProfile { get { return lbRemoveProfile; } }
        public Txt TXTMore { get { return txtMore; } }
        public Txt TXTOnline { get { return txtOnline; } }
        public Matchnet.Web.Framework.Image IMGChat { get { return imgChat; } }
        public System.Web.UI.WebControls.HyperLink LNKOnline { get { return lnkOnline; } }
        public Matchnet.Web.Framework.Image IMGYes { get { return ImageYes; } }
        public Matchnet.Web.Framework.Image IMGMaybe { get { return ImageMaybe; } }
        public Matchnet.Web.Framework.Image IMGNo { get { return ImageNo; } }
        public Matchnet.Web.Framework.Image IMGBothSaidYes { get { return imgBothSaidYes; } }
        public Matchnet.Web.Framework.Txt TXTYes { get { return TxtYes; } }
        public Matchnet.Web.Framework.Txt TXTMaybe { get { return TxtMaybe; } }
        public Matchnet.Web.Framework.Txt TXTNo { get { return TxtNo; } }
        public HtmlInputHidden YNMVOTEStatus { get { return YNMVoteStatus; } }
        public HtmlTableCell YNMBackgroundAnimation { get { return tdAnimBG; } }

        #endregion


        #region Properties
        public string YesFileNameSelected
        {
            get { return _yesFileNameSelected; }
            set { _yesFileNameSelected = value; }
        }

        public string NoFileNameSelected
        {
            get { return _noFileNameSelected; }
            set { _noFileNameSelected = value; }
        }

        public string MaybeFileNameSelected
        {
            get { return _maybeFileNameSelected; }
            set { _maybeFileNameSelected = value; }
        }

        public bool IsHotListFriend
        {
            get { return _isHotListFriend; }
            set { _isHotListFriend = value; }
        }
        public string DefaultNote
        {
            get
            {
                return strDefaultNote;
            }
        }

        public bool Transparency
        {
            get { return (_transparency); }
            set { _transparency = value; }
        }

        public string CategoryID
        {
            get { return (_categoryID); }
            set { _categoryID = value; }
        }

        public int Counter
        {
            get { return (_counter); }
            set { _counter = value; }
        }

        public IMemberDTO Member
        {
            get { return _Member; }
            set
            {
                _Member = value;
                _memberID = _Member.MemberID;
                if (add2List != null)
                {
                    add2List.MemberID = _Member.MemberID;
                }
                if (add2List1 != null)
                {
                    add2List1.MemberID = _Member.MemberID;
                }


                //if (FrameworkGlobals.memberHighlighted(this._memberID, g.Brand))
                if (FrameworkGlobals.memberHighlighted(this._Member, g.Brand))
                {
                    this._isHighlighted = true;
                }
                else
                {
                    this._isHighlighted = false;
                }
            }
        }

        public int MemberID
        {
            get { return (_memberID); }
            set
            {
                _memberID = value;
                if (value != 0)
                {
                    _Member = MemberDTOManager.Instance.GetIMemberDTO(_memberID, g, MemberType.Search, MemberLoadFlags.None);
                    //MemberSA.Instance.GetMember(_memberID, MemberLoadFlags.None);
                    if (add2List != null)
                    {
                        add2List.MemberID = _Member.MemberID;
                    }
                    if (add2List1 != null)
                    {
                        add2List1.MemberID = _Member.MemberID;
                    }
                }
                else
                {
                    _Member = null;
                }
            }
        }

        public int Ordinal
        {
            get { return (_ordinal); }
            set { _ordinal = value; }
        }

        public BreadCrumbHelper.EntryPoint MyEntryPoint
        {
            get { return (_myEntryPoint); }
            set { _myEntryPoint = value; }
        }

        public MOCollection MyMOCollection
        {
            get { return (_myMOCollection); }
            set { _myMOCollection = value; }
        }

        public HotListCategory HotListCategory
        {
            get { return (_hotListCategory); }
            set { _hotListCategory = value; }
        }

        public bool EnableSingleSelect
        {
            get { return _bEnableSingleSelect; }
            set { _bEnableSingleSelect = value; }
        }

        public string Note
        {
            get
            {
                return txtFavoriteNote.Value;
            }
        }

        public bool CurrentlyOnline
        {
            get { return (_currentlyOnline); }
            set { _currentlyOnline = value; }
        }

        public bool IsHighlighted
        {
            get { return (this._isHighlighted); }
            set { this._isHighlighted = value; }
        }

        public bool IsSpotlight
        {
            get { return (this._isSpotlight); }
            set { this._isSpotlight = value; }
        }

        public bool OverrideSelfSpotlight
        {
            get { return (this._overrideSelfSpotlight); }
            set { this._overrideSelfSpotlight = value; }
        }
        public bool IsPromotionalMember
        {
            get { return (this.promotionMemberFlag); }
            set { this.promotionMemberFlag = value; }
        }
        public bool IsMatchMeterEnabled
        {
            get { return _IsMatchMeterEnabled; }
            set { _IsMatchMeterEnabled = value; }
        }
        public decimal MatchesScore
        {
            set { _MatchScore = value; }
        }
        public String ViewProfileURL
        {
            get { return (ViewProfileUrl); }
            set { ViewProfileUrl = value; }
        }
        #endregion

        private void Page_Load(object sender, System.EventArgs e)
        {
            int brandId = 0;
            if (_Member == null)
            {
                return;
            }
            try
            {
                _handler = new MiniProfileHandler(this);
                strDefaultNote = g.GetResource("HOT_MAKE_A_NOTE", this);

                if (LinkUserName.Text == null || LinkUserName.Text.Trim().Length == 0)
                {
                    plcTransparent.Visible = Transparency;

                    searchResult = new SearchResultProfile(_Member, Ordinal);

                    if (_Member == null)
                    {
                        ThrowNullMemberError();
                        return;
                    }

                    // Configure HotListBar
                    //HotListBar.Member = _Member;
                    // only add the HotListBar if someone's logged in
                    if (_g.Member != null)
                    {
                        if (_g.Member.MemberID != _Member.MemberID)
                        {
                            HotListBar = (MiniProfileHotListBar)LoadControl("/Framework/Ui/BasicElements/MiniProfileHotListBar.ascx");
                            HotListBar.Member = _Member;
                            plcHotListBar.Controls.Add(HotListBar);
                        }
                    }

                    //photo
                    _handler.SetMemberPhotoAndName(g);

                    //online link
                    _handler.SetOnlineLink();
                    imgChat1.FileName = imgChat.FileName;
                    lnkOnline1.NavigateUrl = lnkOnline.NavigateUrl;
                    txtOnline1.ResourceConstant = txtOnline.ResourceConstant;
                    imgChat1.NavigateUrl = imgChat.NavigateUrl;
                    imgChat1.TitleResourceConstant = imgChat.TitleResourceConstant;

                    _handler.SetLinks();

                    _handler.SetMemberDetails(g, MAX_STRING_LENGTH);
                    _handler.SetThumb(noPhoto);
                    if (_DisplayContext == ResultContextType.MatchMeterMatches)
                    {
                        noPhoto.IsMatchMeterMatchesPage = true;
                    }
                    //member subclass-specific stuff
                    _handler.ToggleDisplayType(g);

                    //	Set up contact links (email & ecard & tease)
                    SetContactLinks();

                    // Set up Jmeter controls
                    SetJmeter();

                    // Set up YNM visibility
                    if (!g.IsYNMEnabled)
                    {
                        plcYNM.Visible = false;
                        plcEmailMe.Visible = true;
                        string emailURL = "/Applications/Email/Compose.aspx?MemberId=" + _Member.MemberID + "&LinkParent=" +
                                  (int)LinkParent.MiniProfileTop;
                        lnkEmail.NavigateUrl = emailURL;
                    }

                }

                // TT15768 - prep href for back link in article page
                PrepClickLink();
                insertPixel();
                SetSpotlightDetails();

            }
            catch (Exception ex)
            {
                _g.ProcessException(ex);
            }
        }

        private void SetJmeter()
        {
            if (IsMatchMeterEnabled)
            {
                // ucMatchMeterInfoDisplay.IsMatchMeterInfoDisaplay = IsMatchMeterInfoDisaplay;
                ucMatchMeterInfoDisplay.Visible = true;
                ucMatchMeterInfoDisplay.IsMatchMeterEnabled = true;
                ucMatchMeterInfoDisplay.IsMemberHighlighted = IsHighlighted;
                ucMatchMeterInfoDisplay.MemberID = MemberID;
                if (_DisplayContext == ResultContextType.MatchMeterMatches)
                {
                    ucMatchMeterInfoDisplay.IsMatchesPage = true;
                    ucMatchMeterInfoDisplay.MatchScore = _MatchScore;
                }
            }
            else
            {
                ucMatchMeterInfoDisplay.IsMatchMeterEnabled = false;
                ucMatchMeterInfoDisplay.Visible = false;
            }
        }

        private void PrepClickLink()
        {
            string refUrl = string.Empty;
            if (Request.Url.AbsoluteUri != null)
            {
                refUrl = Request.Url.AbsoluteUri;
            }

            if (refUrl.IndexOf("StartRow") != -1)
            {
                refUrl = g.RemoveParamFromURL(refUrl, "StartRow", false);
            }

            string qm = "?";
            if ((refUrl.IndexOf("?") != -1))
            {
                qm = "&";
            }

            refUrl = refUrl + qm + "StartRow=" + GetStartRow();
            refUrl = Server.UrlEncode(refUrl);

            //lnkClick.HRef = FrameworkGlobals.LinkHref("/Applications/Article/ArticleView.aspx?CategoryID=1980&ArticleID=6296&refUrl=" + refUrl, true);
            lnkClick.HRef = FrameworkGlobals.LinkHref("/Applications/Article/ArticleView.aspx?CategoryID=1980&ArticleID=6296&refUrl=" + refUrl, true);
            if (g.Session["ArticleRefUrl"] != null && !g.Session["ArticleRefUrl"].Equals(String.Empty))
            {
                // remove existing value
                g.Session.Remove("ArticleRefUrl");
            }
        }


        private void hideYNMComponents()
        {
            TxtYes.Visible = false;
            ImageYes.Visible = false;
            TxtNo.Visible = false;
            ImageNo.Visible = false;
            TxtMaybe.Visible = false;
            ImageMaybe.Visible = false;
            txtClick.Visible = false;
            TxtThinkYoud.Visible = false;
        }

        //private void ToggleDisplayType()
        //{
        //    btnSave.Visible = false;
        //    txtFavoriteNote.Visible = false;

        //    if (g.Member != null)
        //    {
        //        _isHotListFriend = g.List.IsHotListed(HotListCategory.Default,
        //            g.Brand.Site.Community.CommunityID,
        //            _memberID);
        //    }

        //    switch (_DisplayContext)
        //    {
        //        case Matchnet.Web.Framework.Ui.BasicElements.ResultList.ResultContextType.MembersOnline:
        //            lastLoginDate.LastLoginDateLabel = g.GetResource("TXT_ONLINE_NOW", this);
        //            break;
        //        case Matchnet.Web.Framework.Ui.BasicElements.ResultList.ResultContextType.HotList:
        //            _handler.SetLoginUpdateDate(g);
        //            lastLoginDate.ShowCurrentlyOnline = false;
        //            break;
        //        default:
        //            // TT16550 - set the online label appropriately if the user is in fact online
        //            if (CurrentlyOnline)
        //            {
        //                lastLoginDate.LastLoginDateLabel = g.GetResource("TXT_ONLINE_NOW", this);
        //            }
        //            else
        //            {
        //                //lastLoginDate.Visible = false;
        //                _handler.SetLoginUpdateDate(g);
        //            }
        //            _handler.ShowNonHotListMember();
        //            break;
        //    }
        //}

        //private void ShowNonHotListMember()
        //{
        //    txtFavoriteNote.Visible = false;
        //    btnSave.Visible = false;
        //}

        //private void ShowHotListMember()
        //{

        //    if (_hotListCategory == HotListCategory.Default || _hotListCategory > 0)
        //    {
        //        pnlComments.Visible = true;
        //        plcBreaks.Visible = true;
        //        txtFavoriteNote.Visible = true;
        //        btnSave.Visible = true;

        //        pnlComments.Attributes["style"] = "z-index:" + (50 - Counter).ToString() + "; position: absolute;";
        //        txtFavoriteNote.Attributes["style"] = "z-index:" + (50 - Counter).ToString() + ";";

        //        ListItemDetail listItemDetail = g.List.GetListItemDetail(_hotListCategory,
        //            g.Brand.Site.Community.CommunityID,
        //            g.Brand.Site.SiteID,
        //            _Member.MemberID);

        //        if (listItemDetail.Comment != strDefaultNote && (listItemDetail.Comment != null && listItemDetail.Comment != ""))
        //        {
        //            txtFavoriteNote.Value = StringUtil.MaxWordLength(listItemDetail.Comment, 35);
        //        }
        //        else
        //        {
        //            string strJavaScript = "if(this.value=='" + strDefaultNote + "')this.value='';";

        //            txtFavoriteNote.Value = strDefaultNote;

        //            txtFavoriteNote.Attributes.Remove("onFocus");
        //            txtFavoriteNote.Attributes.Add("onFocus", strJavaScript);
        //        }
        //    }
        //    else if (_hotListCategory != HotListCategory.MutualYes && _hotListCategory != HotListCategory.YourMatches
        //        && _hotListCategory != HotListCategory.IgnoreList)
        //    {
        //        lbRemoveProfile.Visible = true;
        //        lbRemoveProfile.Text = g.GetResource("REMOVE_THIS_PROFILE", this);
        //    }

        //    // When in any of the hot list categories page, a Delete from list displays.
        //    // When this displays, do not display Learn about Highlighted Profiles.
        //    ucHighlightProfileInfoDisplay.Visible = false;
        //}

        //private void SetMemberDetails()
        //{
        //    //gender
        //    searchResult.SetGender(g.Brand.Site.Community.CommunityID, TextGender);

        //    //age
        //    DateTime birthDate = _Member.GetAttributeDate(g.Brand, "BirthDate", DateTime.MinValue);
        //    TextAge.Text = FrameworkGlobals.GetAgeCaption(birthDate, g, this, "AGE_UNSPECIFIED");

        //    searchResult.SetRegion(g.Brand.Site.LanguageID, TextRegion, MAX_STRING_LENGTH);

        //    //headline

        //    searchResult.SetHeadline(TextHeadline, g.Session.GetInt(WebConstants.SESSION_PROPERTY_NAME_MEMBERID), 60, 20);
        //}

        //private void SetLinks()
        //{
        //    txtMore.Href = ViewProfileUrl;
        //}

        //private void SetOnlineLink()
        //{
        //    bool isOnline;
        //    OnlineLinkHelper.SetOnlineLink(_DisplayContext,searchResult._Member, this.Member, imgChat, lnkOnline, LinkParent.MiniProfile, out isOnline);
        //    if (! isOnline)
        //    {
        //        txtOnline.ResourceConstant = "SPACES";
        //        CurrentlyOnline = false;
        //    }
        //    else
        //        CurrentlyOnline = true;

        //}

        private void SetContactLinks()
        {
            lnkEmailMe.HRef = FrameworkGlobals.LinkHref(string.Format("/Applications/Email/Compose.aspx?MemberID={0}&LinkParent={1}&StartRow={2}", _Member.MemberID, (int)LinkParent.MiniProfile, GetStartRow()), true);
            lnkTeaseMe.HRef = FrameworkGlobals.LinkHref("/Applications/Email/Tease.aspx?MemberID=" + _Member.MemberID + "&StartRow=" + GetStartRow(), true);
            lnkTeaseMe1.HRef = FrameworkGlobals.LinkHref("/Applications/Email/Tease.aspx?MemberID=" + _Member.MemberID + "&StartRow=" + GetStartRow(), true);

            #region ECARD IMPLEMENTATION	5-22-06: Steve C.

            // Show hide send ECard link based on setting from database.  (ON or OFF)
            if (g.EcardsEnabled)
            {
                // Generate mingle ecard page url. (to parameter is the username of the reciever)
                string destPageUrl = FrameworkGlobals.BuildConnectFrameworkLink("cards/categories.html?to=" + _Member.GetUserName(_g.Brand) + "&return=1", g, true);

                // Assign url to ECard link.
                lnkECard.HRef = destPageUrl;
                lnkECard1.HRef = destPageUrl;

            }
            else
            {
                TableCellECard.Visible = false;
            }

            #endregion
        }

        //private void SetMemberPhotoAndName()
        //{
        //    if (this._isHighlighted)
        //    {
        //        ViewProfileUrl = BreadCrumbHelper.MakeViewProfileLink(MyEntryPoint, MemberID, Ordinal, MyMOCollection, (int)_hotListCategory, false, (int) BreadCrumbHelper.PremiumEntryPoint.Highlight);
        //    }
        //    else if (this._isSpotlight)
        //    {
        //        ViewProfileUrl = BreadCrumbHelper.MakeViewProfileLink(MyEntryPoint, MemberID, Ordinal, MyMOCollection, (int)_hotListCategory, false, (int)BreadCrumbHelper.PremiumEntryPoint.Spotlight);
        //    }
        //    else
        //    {
        //        ViewProfileUrl = BreadCrumbHelper.MakeViewProfileLink(MyEntryPoint, MemberID, Ordinal, MyMOCollection, (int)_hotListCategory, false, (int) BreadCrumbHelper.PremiumEntryPoint.NoPremium);
        //        //ViewProfileUrl = BreadCrumbHelper.MakeViewProfileLink(MyEntryPoint, MemberID, Ordinal, MyMOCollection, (int)_hotListCategory);
        //    }
        //    LinkUserName.NavigateUrl = ViewProfileUrl;
        //    LinkUserName.Attributes.Add("onmouseover","this.title='" + _Member.GetUserName(_g.Brand) +"'");
        //    LinkUserName.Text = FrameworkGlobals.Ellipsis(_Member.GetUserName(_g.Brand), 17);
        //    // TPJ-94
        //    ImageIsNew.Visible = _Member.IsNewMember(g.Brand.Site.Community.CommunityID);
        //    ImageIsUpdate.Visible = !ImageIsNew.Visible && _Member.IsUpdatedMember(g.Brand.Site.Community.CommunityID);

        //    litContainer.Text = "<div id='boxContainer' class='box' style='z-index: " + Convert.ToString(100 - this.Counter) + ";'>";
        //    if (g.Brand.Site.SiteID == (int) WebConstants.SITE_ID.JDateCoIL)
        //    {
        //        imgPremiumAuthenticated.Visible = FrameworkGlobals.IsMemberPremiumAuthenticated(_Member.MemberID, g.Brand);        
        //    }
        //    else
        //    {
        //        imgPremiumAuthenticated.Visible = false;
        //    }

        //    //if (FrameworkGlobals.memberHighlighted(_Member.MemberID, g.Brand))     
        //    if (this._isHighlighted)     
        //    {
        //        litContainer.Text = "<div id='boxContainer' class='box highlightedProfile' style='z-index: " + Convert.ToString(100 - this.Counter) + ";'>";
        //        //boxContainer.Attributes.Remove("class");                
        //        //boxContainer.Attributes.Add("class", "box highlightedProfile");

        //        ucHighlightProfileInfoDisplay.MemberHighlighted = true;
        //        ucHighlightProfileInfoDisplay.ViewedMemberID = _Member.MemberID;
        //    }
        //    else
        //    {
        //        ucHighlightProfileInfoDisplay.Visible = false;
        //    }



        //    searchResult.SetThumb(ImageThumb, noPhoto, ViewProfileUrl);
        //}



        // Get startRow value for persisting result index
        private int GetStartRow()
        {
            // Recurse through the parent controls.  If one of the controls is "ResultList" retrieve the StartRow value.
            Control parentCtrl = ((Control)(this)).Parent;
            while (parentCtrl != null)
            {
                if (parentCtrl.GetType().Name == "ResultList_ascx")
                {
                    return ((ResultList)parentCtrl)._StartRow;
                }

                parentCtrl = parentCtrl.Parent;
            }

            return Constants.NULL_INT;
        }


        private void SetYNMControls()
        {
            ClickMask clickMask = g.List.GetClickMask(g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, _Member.MemberID);

            YNMVoteStatus.Value = ((int)clickMask).ToString();

            if ((clickMask & ClickMask.MemberYes) == ClickMask.MemberYes)
            {
                ImageYes.FileName = "icon-click-y-on.gif";
            }
            else if ((clickMask & ClickMask.MemberNo) == ClickMask.MemberNo)
            {
                ImageNo.FileName = "icon-click-n-on.gif";
            }
            else if ((clickMask & ClickMask.MemberMaybe) == ClickMask.MemberMaybe)
            {
                ImageMaybe.FileName = "icon-click-m-on.gif";
            }

            if ((clickMask & ClickMask.MemberYes) == ClickMask.MemberYes && (clickMask & ClickMask.TargetMemberYes) == ClickMask.TargetMemberYes)
            {
                imgBothSaidYes.Visible = true;
            }

            YNMVoteParameters parameters = new YNMVoteParameters();

            parameters.SiteID = g.Brand.Site.SiteID;
            parameters.CommunityID = g.Brand.Site.Community.CommunityID;
            parameters.FromMemberID = g.Session.GetInt(WebConstants.SESSION_PROPERTY_NAME_MEMBERID);
            parameters.ToMemberID = _Member.MemberID;

            string encodedParameters = parameters.EncodeParams();

            ImageYes.NavigateUrl = string.Format("javascript:YNMVote('{0}','1','{1}');",
                YNMVoteStatus.Parent.ClientID, encodedParameters);
            ImageYes.Attributes.Add("onMouseOver", string.Format("YNMMouseOver(this, '{0}', '1')", YNMVoteStatus.Parent.ClientID));
            ImageYes.Attributes.Add("onMouseOut", string.Format("YNMMouseOut(this, '{0}', '1')", YNMVoteStatus.Parent.ClientID));

            ImageNo.NavigateUrl = string.Format("javascript:YNMVote('{0}','2','{1}');",
                YNMVoteStatus.Parent.ClientID, encodedParameters);
            ImageNo.Attributes.Add("onMouseOver", string.Format("YNMMouseOver(this, '{0}', '2')", YNMVoteStatus.Parent.ClientID));
            ImageNo.Attributes.Add("onMouseOut", string.Format("YNMMouseOut(this, '{0}', '2')", YNMVoteStatus.Parent.ClientID));

            ImageMaybe.NavigateUrl = string.Format("javascript:YNMVote('{0}','3','{1}');",
                YNMVoteStatus.Parent.ClientID, encodedParameters);
            ImageMaybe.Attributes.Add("onMouseOver", string.Format("YNMMouseOver(this, '{0}', '4')", YNMVoteStatus.Parent.ClientID));
            ImageMaybe.Attributes.Add("onMouseOut", string.Format("YNMMouseOut(this, '{0}', '4')", YNMVoteStatus.Parent.ClientID));

            TxtYes.Href = ImageYes.NavigateUrl;
            TxtYes.Attributes.Add("onmouseover", string.Format("document." + WebConstants.HTML_FORM_ID + ".{0}.onmouseover();", ImageYes.ClientID));
            TxtYes.Attributes.Add("onmouseout", string.Format("document." + WebConstants.HTML_FORM_ID + ".{0}.onmouseout();", ImageYes.ClientID));

            TxtNo.Href = ImageNo.NavigateUrl;
            TxtNo.Attributes.Add("onmouseover", string.Format("document." + WebConstants.HTML_FORM_ID + ".{0}.onmouseover();", ImageNo.ClientID));
            TxtNo.Attributes.Add("onmouseout", string.Format("document." + WebConstants.HTML_FORM_ID + ".{0}.onmouseout();", ImageNo.ClientID));

            TxtMaybe.Href = ImageMaybe.NavigateUrl;
            TxtMaybe.Attributes.Add("onmouseover", string.Format("document." + WebConstants.HTML_FORM_ID + ".{0}.onmouseover();", ImageMaybe.ClientID));
            TxtMaybe.Attributes.Add("onmouseout", string.Format("document." + WebConstants.HTML_FORM_ID + ".{0}.onmouseout();", ImageMaybe.ClientID));

            tdAnimBG.Attributes.Add("onmouseover", "this.style.backgroundImage='URL(" + Framework.Image.GetURLFromFilename("bknd_vote_anim.gif") + ")';");
            tdAnimBG.Attributes.Add("onmouseout", "this.style.backgroundImage='URL(" + Framework.Image.GetURLFromFilename("bknd_vote.gif") + ")';");

            if (g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.JDateCoIL)
            {
                imgPremiumAuthenticated.Visible = FrameworkGlobals.IsMemberPremiumAuthenticated(_Member.MemberID, g.Brand);
            }

            ////if (FrameworkGlobals.memberHighlighted(_Member.MemberID, g.Brand))     
            //if (this._isHighlighted)     
            //{
            //    tdAnimBG.Attributes.Remove("onmouseover");                
            //    tdAnimBG.Attributes.Remove("onmouseout");                                    

            //    tdAnimBG.Attributes.Add("onmouseover", "this.style.backgroundImage='URL(" + Framework.Image.GetURLFromFilename("bknd_vote_anim_highlighted.gif") + ")';");
            //    tdAnimBG.Attributes.Add("onmouseout", "this.style.backgroundImage='URL(" + Framework.Image.GetURLFromFilename("bknd_vote_highlighted.gif") + ")';");                
            //}
        }

        private void ThrowNullMemberError()
        {
            g.Notification.AddError("PAGE_MEMBER_PROFILE_TEMPORARILY_UNAVAILABLE");
            TextGender.Text = g.GetResource("MEMBER_PROFILE_TEMPORARILY_UNAVAILABLE", this);
            ImageEmailMe.Visible = false;
            ImageList.Visible = false;
            ImageThumb.ImageUrl = "/img/trans.gif";
        }

        //public void SetLoginUpdateDate()
        //{
        //    string resourceValue = "";
        //    DateTime dateValue = DateTime.MinValue;

        //    switch (_DisplayContext)
        //    {
        //        case Matchnet.Web.Framework.Ui.BasicElements.ResultList.ResultContextType.HotList:

        //            HotListType hotListType;
        //            HotListDirection hotListDirection;

        //            ListInternal.MapListTypeDirection(_hotListCategory, out hotListType, out hotListDirection);

        //            ListItemDetail listItemDetail = g.List.GetListItemDetail(_hotListCategory,
        //                g.Brand.Site.Community.CommunityID,
        //                g.Brand.Site.SiteID,
        //                _Member.MemberID);

        //            try
        //            {
        //                dateValue = listItemDetail.ActionDate;
        //            }
        //            catch
        //            {
        //                dateValue = DateTime.Now;
        //            }

        //            switch (_hotListCategory)
        //            {
        //                case HotListCategory.WhosOnline:
        //                    resourceValue = g.GetResource("LOGGED_IN", this);
        //                    dateValue = _Member.GetLastLogonDate(g.Brand.Site.Community.CommunityID);
        //                    break;

        //                case HotListCategory.WhoViewedYourProfile:
        //                case HotListCategory.MembersYouViewed:
        //                    resourceValue = g.GetResource("VIEWED_ON", this);
        //                    break;

        //                case HotListCategory.WhoEmailedYou:
        //                case HotListCategory.WhoIMedYou:
        //                case HotListCategory.MembersYouIMed:
        //                case HotListCategory.MembersYouEmailed:
        //                    resourceValue = g.GetResource("DATE_SENT", this);
        //                    break;

        //                case HotListCategory.WhoAddedYouToTheirFavorites:
        //                    resourceValue = g.GetResource("ADDED_ON", this);
        //                    break;

        //                case HotListCategory.WhoTeasedYou:
        //                case HotListCategory.MembersYouTeased:
        //                    resourceValue = g.GetResource("TEASE_SENT__519989", this);
        //                    break;

        //                default:
        //                    resourceValue = g.GetResource("LAST_CHANGED", this);
        //                    break;
        //            }
        //            break;

        //        default:
        //            resourceValue = g.GetResource("LOGGED_IN", this);
        //            dateValue = _Member.GetLastLogonDate(g.Brand.Site.Community.CommunityID);
        //            break;
        //    }

        //    lastLoginDate.LastLoginDateLabel = resourceValue;
        //    lastLoginDate.LastLoginDateValue = dateValue;
        //}

        public ResultContextType DisplayContext
        {
            set
            {
                _DisplayContext = value;
            }

            get
            {
                return _DisplayContext;
            }
        }

        private void lbRemoveProfile_Click(object sender, System.EventArgs e)
        {
            try
            {
                if ((_hotListCategory != HotListCategory.Default || _hotListCategory > 0) ||
                    (_hotListCategory != HotListCategory.MutualYes && _hotListCategory != HotListCategory.YourMatches))
                {
                    ListSA.Instance.RemoveListMember(_hotListCategory, g.Brand.Site.Community.CommunityID, g.Member.MemberID, this.Member.MemberID);
                }

                int intval = Convert.ToInt32(_hotListCategory);
                g.Transfer("/Applications/HotList/View.aspx?CategoryID=" + intval.ToString() + "&StartRow=" + this.GetStartRow() + "&rc=YOUR_LIST_HAS_BEEN_UPDATED");
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            try
            {
                base.OnPreRender(e);
                //This line was removed to make miniprofile work since it no longer implements IPostBackDataHandler:
                //Page.RegisterRequiresPostBack(this);
                if (_DisplayContext == Matchnet.Web.Framework.Ui.BasicElements.ResultContextType.HotList)
                {
                    _handler.ShowHotListMember(g);
                }

            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }


        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbRemoveProfile.Click += new System.EventHandler(this.lbRemoveProfile_Click);
            this.btnSave.Click += new System.EventHandler(this.btnSave_ServerClick);
            this.Load += new System.EventHandler(this.Page_Load);
            rptPhotos.ItemDataBound += new RepeaterItemEventHandler(rptPhotos_ItemDataBound);
            this.PreRender += new EventHandler(MiniProfile_PreRender);
            if (add2List != null)
            {
                add2List.MemberID = this.MemberID;
            }
            if (add2List1 != null)
            {
                add2List1.MemberID = this.MemberID;
            }
        }
        #endregion

        private void btnSave_ServerClick(object sender, System.EventArgs e)
        {
            SaveNoteEventArgs args = new SaveNoteEventArgs();
            args.Member = _Member;
            args.Note = txtFavoriteNote.Value;
            SaveNote(sender, args);

            g.Notification.AddMessage("NOT_YOUR_HOT_LIST_HAS_BEEN_UPDATED");
        }

        private void MiniProfile_PreRender(object sender, EventArgs e)
        {
            if (g.Member != null && _Member != null && g.Member.MemberID != _Member.MemberID)
            {
                //TableYNM.Visible = true;
                _handler.SetYNMControls(g);
                SetHighlightDetail();
            }
            else
            {
                hideYNMComponents();
            }
        }
        private void insertPixel()
        {
            try
            {
                if (g.PromotionalProfileEnabled() && g.PromotionalProfileMember() == _Member.MemberID)
                {
                    string pixelResx = g.GetResource("PROMOTION_MINIPROFILE_PIXEL");
                    if (pixelResx != null && pixelResx != string.Empty)
                    { plcPixel.Text = pixelResx; }
                }
            }
            catch (Exception ex) { }
        }


        private void SetSpotlightDetails()
        {

            if (g.Member == null)
                return;
            bool selfSpotlight = (g.Member.MemberID == _Member.MemberID);
            //headline
            if (!_isSpotlight)
            { return; }

            plcSpotlightSettings.Visible = selfSpotlight && !_overrideSelfSpotlight;
            string matches = ProfileDisplayHelper.GetSpotlightMatchString(g, _Member);
            if (!String.IsNullOrEmpty(matches) && !selfSpotlight)
            {
                txtBothLike.Visible = true;
                searchResult.SetHeadline(TextHeadline, matches, _memberID, 80, 20);
            }
            else
            {
                searchResult.SetHeadline(TextHeadline, g.Session.GetInt(WebConstants.SESSION_PROPERTY_NAME_MEMBERID), 60, 20);
            }

            txtSpotlight.Visible = (!promotionMemberFlag && (!selfSpotlight || _overrideSelfSpotlight));
            txtSpotlightPromo.Visible = (promotionMemberFlag && !selfSpotlight);
            txtSelfSpotlight.Visible = selfSpotlight && !_overrideSelfSpotlight;


            litContainer.Text = "<div class=\"results list-view spotlightedProfile\" style=\"z-index: " + Convert.ToString(100 - this.Counter) + ";\">";
            plcSpotlight.Visible = true;
            plcNormalView.Visible = false;
            DateTime birthDate = _Member.GetAttributeDate(g.Brand, "BirthDate", DateTime.MinValue);
            string age = FrameworkGlobals.GetAgeCaption(birthDate, g, this, "AGE_UNSPECIFIED");
            string regionInfo = "";
            int regionid = _Member.GetAttributeInt(g.Brand, "RegionID");


            regionInfo = FrameworkGlobals.GetRegionString(regionid, g.Brand.Site.LanguageID);


            TextInfo.Text = String.Format("{0}, {1}", age, regionInfo);
            plcNormalView.Visible = false;
            plcSpotlightView.Visible = true;

            var ph1 = MemberPhotoDisplayManager.Instance.GetApprovedPhotos(g.Member, _Member, g.Brand);

            if (ph1 != null && ph1.Count > 0)
            {
                rptPhotos.DataSource = ph1;
                rptPhotos.DataBind();

            }

            if (!selfSpotlight || _overrideSelfSpotlight)
            {
                ucSpotlightInfoDisplay.Spotlight = true;
                ucSpotlightInfoDisplay.Visible = true;
            }
            bool showEnlarge = false;

            showEnlarge = Conversion.CBool(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("SPOTLIGHT_ENLARGE_PROFILE", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID));
            if (showEnlarge && _Member.HasApprovedPhoto(g.Brand.Site.Community.CommunityID))
            {
                ucProfileEnlarge.Member = _Member;
                ucProfileEnlarge.Visible = true;
            }
            memberSpotlight_curveBottom.Visible = true;
        }

        private void SetHighlightDetail()
        {
            if (this._isHighlighted)
            {
                tdAnimBG.Attributes.Remove("onmouseover");
                tdAnimBG.Attributes.Remove("onmouseout");

                tdAnimBG.Attributes.Add("onmouseover", "this.style.backgroundImage='URL(" + Framework.Image.GetURLFromFilename("bknd_vote_anim_highlighted.gif") + ")';");
                tdAnimBG.Attributes.Add("onmouseout", "this.style.backgroundImage='URL(" + Framework.Image.GetURLFromFilename("bknd_vote_highlighted.gif") + ")';");
            }
        }

        //private void setMember()
        //{
        //    try
        //    {
        //        strDefaultNote = g.GetResource("HOT_MAKE_A_NOTE", this);

        //        if (LinkUserName.Text == null || LinkUserName.Text.Trim().Length == 0)
        //        {
        //            plcTransparent.Visible = Transparency;

        //            searchResult = new SearchResultProfile(_Member, Ordinal);

        //            if (_Member == null)
        //            {
        //                ThrowNullMemberError();
        //                return;
        //            }

        //            // Configure HotListBar
        //            //HotListBar.Member = _Member;
        //            // only add the HotListBar if someone's logged in
        //            if (_g.Member != null)
        //            {
        //                HotListBar = (MiniProfileHotListBar)LoadControl("/Framework/Ui/BasicElements/MiniProfileHotListBar.ascx");
        //                HotListBar.Member = _Member;
        //                plcHotListBar.Controls.Add(HotListBar);
        //            }

        //            //photo
        //            _handler.SetMemberPhotoAndName(g);
        //            _handler.SetThumb(noPhoto);

        //            //online link
        //            _handler.SetOnlineLink();

        //            _handler.SetLinks();

        //            _handler.SetMemberDetails(g, MAX_STRING_LENGTH);

        //            //member subclass-specific stuff
        //            _handler.ToggleDisplayType(g);

        //            //	Set up contact links (email & ecard & tease)
        //            SetContactLinks();
        //        }

        //        // TT15768 - prep href for back link in article page
        //        PrepClickLink();
        //        insertPixel();
        //        SetSpotlightDetails();

        //    }
        //    catch (Exception ex)
        //    {
        //       throw(ex);
        //    }

        //}
        protected void rptPhotos_ItemDataBound(Object sender, RepeaterItemEventArgs e)
        {
            try
            {
                //MemberPhotoHelper photoHelper = new MemberPhotoHelper(RuntimeSettings.Instance);
                Photo photo = (Photo)e.Item.DataItem;
                //PremiumService s = Matchnet.PremiumServices.ServiceAdapter.ServiceSA.Instance.GetPremiumService(m.ServiceInstanceID);

                // Matchnet.Web.Framework.Image img = new Image();
                Image img = ((Image)e.Item.FindControl("imgTinyThumb"));
                if (MemberPhotoDisplayManager.Instance.IsPrivatePhoto(photo, g.Brand))
                    return;

                if (!MemberPhotoDisplayManager.Instance.PhotoIsEmpty(photo, PhotoType.Thumbnail, g.Brand))
                {
                    img.ImageUrl = MemberPhotoDisplayManager.Instance.GetPhotoDisplayURL(g.Member, _Member, g.Brand, photo, PhotoType.Thumbnail,
                                                         PrivatePhotoImageType.Thumb, NoPhotoImageType.Thumb);
                }
                else
                {
                    img.ImageUrl = MemberPhotoDisplayManager.Instance.GetPhotoDisplayURL(g.Member, _Member, g.Brand, photo, PhotoType.Full,
                                                         PrivatePhotoImageType.Full, NoPhotoImageType.None);
                }


                // e.Item.Controls.Add(img);

            }
            catch (Exception ex)
            {

            }

        }

    }


}
