<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mnbe" Namespace="Matchnet.Web.Framework.Ui.BasicElements" Assembly="Matchnet.Web" %>
<%@ Control Language="c#" AutoEventWireup="false" Codebehind="YNMVoteBarSmall.ascx.cs" Inherits="Matchnet.Web.Framework.Ui.YNMVoteBarSmall" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>

<div id="divMutualYes" style="display: none" runat="server">
<mn:Image id="imgMutualYes" filename="icon-click-yy.gif" runat="server" hspace="2" />
</div>

<div id="divVoteButtons" style="display: inline" runat="server">
<input type="hidden" id="YNMVoteStatus" runat="server" NAME="YNMVoteStatus">
<mnbe:YNMVoteButton type="Yes" id="ImageYes" filename="icon-click-y-off.gif" selectedFilename="icon-click-y-on.gif"
	runat="server" resourceconstant="YNM_Y_IMG_ALT_TEXT" EncodeResource="false" align="absmiddle" Source="miniprofile"/>
<mnbe:YNMVoteButton type="No" id="ImageNo" filename="icon-click-n-off.gif" selectedFilename="icon-click-n-on.gif"
	runat="server" resourceconstant="YNM_N_IMG_ALT_TEXT" EncodeResource="false" align="absmiddle" Source="miniprofile" />
<mnbe:YNMVoteButton type="Maybe" id="ImageMaybe" filename="icon-click-m-off.gif" selectedFilename="icon-click-m-on.gif"
	runat="server" resourceconstant="YNM_M_IMG_ALT_TEXT" EncodeResource="false" align="absmiddle" Source="miniprofile"/>
</div>