﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProgressBar.ascx.cs" Inherits="Matchnet.Web.Framework.Ui.BasicElements.ProgressBar" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
 <h2><mn:Txt ID="txtProgress" runat="server" ResourceConstant="TXT_PROGRESS" /></h2>
 <div class="progress-bar">
 <span>
   <em id="progress-position" <asp:Literal ID="litStyle" runat="server" />><asp:Literal ID="litProgress" runat="server" /></em>
 </span>
 </div>