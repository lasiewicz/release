namespace Matchnet.Web.Framework.Ui.Advertising
{
    using System;
    using System.Web;
    using System.Web.UI.WebControls;
    using Matchnet.Web.Framework;
    using Matchnet.Content.ServiceAdapters;
    using Matchnet.Content.ValueObjects.Region;
    using Matchnet.Content.ValueObjects.PageConfig;
    using Matchnet.Purchase.ServiceAdapters;
    using Matchnet.Purchase.ValueObjects;
    using Matchnet.Web.Applications.MemberProfile;
    using Matchnet.Web.Applications.MemberProfile.ProfileTabs30;
    using Matchnet.Web.Applications.MemberProfile.ProfileTabs30.XMLProfileTabConfig;
    using Matchnet.Content.ServiceAdapters.Links;


    /// <summary>
    ///		The AdUnit (not AdSlot) is the Ad control used to display all types
    ///		of banner ads on certain pages in the websolution. The technical and other
    ///		specifications for this control can be found on sharepoint in the 
    ///		AdUnit_TechnicalSpec.doc.
    /// </summary>
    public class AdUnit : FrameworkControl
    {

        private void Page_Load(object sender, System.EventArgs e)
        {
            //block GAM for ssl pages because it doesn't have https
            bool ssl = (Request.ServerVariables["SERVER_PORT"] == "443");
            if (ssl || _Disabled)
            {
                this.Visible = false;
                return;
            }
            isGAMEnabled = Convert.ToBoolean(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("GOOGLE_AD_MANAGER", _g.Brand.Site.Community.CommunityID, _g.Brand.Site.SiteID));

            //decide whether to show the ad or not base on size, page in site
            switch (_size)
            {
                //deal with the case that this is a Square size ad
                case AdUnit.AdSize.Square:
                    SetResourceConstant("SQUARE");
                    SetSquareVisibility();
                    break;
                //deal with the case that this is a Leaderboard size ad
                case AdUnit.AdSize.Leaderboard:
                    SetResourceConstant("LEADERBOARD");
                    SetLeaderboardVisibility();
                    break;
                //deal with the case that this is a Leaderboard size ad for the Chat Header
                case AdUnit.AdSize.LeaderboardHeader:
                    SetResourceConstant("LEADERBOARDHEADER");
                    pnlAd.CssClass = "chatHeaderLeaderboardAd";
                    break;
                //deal with the case that this a VerticalBanner size ad
                case AdUnit.AdSize.VerticalBanner:
                    SetResourceConstant("VERTICALBANNER");
                    SetVerticalBannerVisibility();
                    break;
                //deal with the case that this a WideSkyscraper size ad
                case AdUnit.AdSize.WideSkyscraper:
                    SetResourceConstant("WIDESKYSCRAPER");
                    SetWideskyscraperVisibility();
                    if (g.AppPage.ID != (int)WebConstants.PageIDs.Subscribe && g.AppPage.ID != (int)WebConstants.PageIDs.SubscribeByCheck && txtAdvertiseWithUs != null)
                        txtAdvertiseWithUs.Visible = true;
                    break;
                //deal with the case that this a WideSkyscraper size ad on one of the special article pages
                case AdUnit.AdSize.WideSkyscraperArticle:
                    SetResourceConstant("WIDESKYSCRAPER");
                    SetWideskyscraperArticleVisibility();
                    break;
                case AdUnit.AdSize.FullBanner:
                    SetResourceConstant("FULLBANNER");
                    SetFullBannerVisibility();
                    break;
                case AdUnit.AdSize.HalfBanner:
                    SetResourceConstant("HALFBANNER");
                    SetHalfBannerVisibility();
                    break;
                case AdUnit.AdSize.LargeRectangle:
                    SetResourceConstant("LARGERECTANGLE");
                    this.pnlAd.HorizontalAlign = HorizontalAlign.NotSet;
                    break;
                case AdUnit.AdSize.Rectangle:
                    SetResourceConstant("RECTANGLE");
                    SetRectangleVisibility();
                    break;
                case AdUnit.AdSize.LeaderboardHeaderAboveMenu:
                    SetResourceConstant("LEADERBOARDHEADERABOVEMENU");
                    SetLeaderBoardMenuAdUnitsVisibility();
                    if (this.Visible)
                    {
                        pnlAd.CssClass = "adspace_header";
                        if (this.HideAd)
                        {
                            pnlAd.CssClass += " hide";
                        }
                    }

                    break;
                case AdUnit.AdSize.LeaderboardHeaderAboveAll:
                    SetResourceConstant("LEADERBOARDHEADERABOVEMENU");
                    SetLeaderBoardMenuAdUnitsVisibility();
                    break;
                case AdUnit.AdSize.LeaderboardHeaderBelowMenu:
                    SetResourceConstant("LEADERBOARDHEADERBELOWMENU");
                    SetLeaderBoardMenuAdUnitsVisibility();
                    break;
                case AdUnit.AdSize.SmallLeaderboardHeaderBelowMenu:

                    if (g.LayoutVersion == WebConstants.LayoutVersions.versionWide)
                        SetResourceConstant("SMALLLEADERBOARDHEADERBELOWMENU", "20");
                    else
                        SetResourceConstant("SMALLLEADERBOARDHEADERBELOWMENU");

                    break;
                case AdUnit.AdSize.ShortFullBanner:
                    SetResourceConstant("SHORTFULLBANNER");
                    SetShortFullBannerVisibility();
                    break;
                case AdUnit.AdSize.LeaderboardMemberProfileA:
                    SetResourceConstant("LEADERBOARDMEMBERPROFILEA");
                    SetLeaderboardMemberProfileVisibility();
                    break;
                case AdUnit.AdSize.LeaderboardMemberProfileB:
                    SetResourceConstant("LEADERBOARDMEMBERPROFILEB");
                    SetLeaderboardMemberProfileVisibility();
                    break;
                case AdUnit.AdSize.LeaderboardMemberProfileC:
                    SetResourceConstant("LEADERBOARDMEMBERPROFILEC");
                    SetLeaderboardMemberProfileVisibility();
                    break;
                case AdUnit.AdSize.LeaderboardMemberProfileD:
                    SetResourceConstant("LEADERBOARDMEMBERPROFILED");
                    SetLeaderboardMemberProfileVisibility();
                    break;
                case AdUnit.AdSize.ForcedPage:
                    SetResourceConstant("FORCEDPAGE");
                    SetForcedPageVisibility();
                    break;
                case AdUnit.AdSize.TextLinkLeft:
                    SetResourceConstant("TEXTLINKLEFT");
                    SetTextLinkLeftVisibility();
                    break;
                case AdUnit.AdSize.TextLinkFooter:
                    SetResourceConstant("TEXTLINKFOOTER");
                    SetTextLinkFooterVisibility();
                    break;
                case AdUnit.AdSize.BlockUI:
                    SetResourceConstant("BLOCKUI");
                    SetBlockUIVisibility();
                    break;
                case AdUnit.AdSize.SmallSquare:
                    SetResourceConstant("SMALLSQUARE");
                    SetSmallSquareVisibiliy();
                    break;
                case AdUnit.AdSize.SmallSquareChat:
                    SetResourceConstant("SMALLSQUARECHAT");
                    SetSmallSquareChatVisibiliy();
                    break;
                case AdUnit.AdSize.Games:
                    SetResourceConstant("GAMES");
                    break;
                case AdUnit.AdSize.SplashMiddle:
                    SetResourceConstant("SPLASHMIDDLE");
                    this.Visible = true;
                    break;
                case AdUnit.AdSize.FlirtSent:
                    this.Visible = true;
                    break;
                case AdUnit.AdSize.RegTop:
                    this.Visible = true;
                    break;
                case AdUnit.AdSize.LeftNavDefault:
                    SetResourceConstant("LEFTNAVDEFAULT");
                    this.Visible = true;
                    break;
                case AdUnit.AdSize.SplashTop:
                    this.Visible = true;
                    break;
                case AdUnit.AdSize.LargeTopSubBanner:
                    this.Visible = true;
                    break;
                case AdUnit.AdSize.SubTab:
                    SetSubTabVisibility();
                    break;
                case AdUnit.AdSize.Overlay:
                    this.Visible = true;
                    break;
                case AdUnit.AdSize.Wallpaper:
                    SetWallpaperVisibility();
                    break;
                default:
                    SetResourceConstant("HALFBANNER");
                    break;
            }

            // GAM - redirect the request to Google Ad Manager when the site setting is on.
            if (isGAMEnabled && _size != AdUnit.AdSize.SmallSquareChat)
            {
                if (this.Visible == false || _gamAdSlot == string.Empty)
                {
                    return;
                }

                string gamAdSlot = string.Empty;
                if (this._gamPageMode == GAMPageModeType.Auto)
                {
                    gamAdSlot = g.GAM.RegisterAdSlot(_gamAdSlot, true);
                }
                else
                {
                    gamAdSlot = g.GAM.RegisterAdSlot(_gamAdSlot);
                }

                if (this._GAMIframe)
                {
                    litAd.Visible = false;
                    phGAMIframe.Visible = true;

                    iframeGAM.Attributes.Add("src", GetGAMIFrameSource(this.GAMAdSlot));
                    iframeGAM.Attributes.Add("width", _GAMIframeWidth.ToString());
                    iframeGAM.Attributes.Add("height", _GAMIframeHeight.ToString());
                    iframeGAM.Attributes.Add("name", iframeGAM.ClientID);
                    _AdIFrameID = iframeGAM.ClientID;

                }
                else
                {
                    litAd.Visible = true;
                    phGAMIframe.Visible = false;

                    litAd.Text = Framework.Ui.Advertising.GAM.GetFillSlot(this.ID, gamAdSlot, g);
                }

                // To track down any missing ads..
                //if (_gamAdSlot == string.Empty)
                //{
                //    g.ProcessException(new Exception("GAMAdSlot property value is required. ID=" + this.ID + ", Size=" + this.Size.ToString()));
                //}

                return;
            }
            else
            {
                //Previous code for non-GAM Ad Support
                try
                {
                    //output the contents of the resource 
                    //TODO: expand image tokens should be removed when 3rd party starts serving ads.
                    this.litAd.Text = g.GetResource(_resourceConstant, null, _expandImageTokens, this);

                    //04092008 TL - Exposing IFrameID hosting the ad
                    this._AdIFrameID = GetAttributeValueFromElement(this.litAd.Text, "iframe", "id");

                    //this is for the doubleclick project
                    this.litAd.Text = ReplaceLinkKeywords(this.litAd.Text);

                    //new logic per last minute addition to spec: turn off ad control if resource is blank
                    //(or if resource text contains the resource constant - which is a sign that
                    //we're on dev and since there is a blank resource, it displays the name
                    if (litAd.Text == string.Empty
                        ||
                        litAd.Text.IndexOf(_resourceConstant) > -1)
                    {

                        this.Visible = false;
                    }
                }
                catch (Exception ex)
                {
                    g.ProcessException(ex);
                }

                //save resource value to property for access by container, as applicable
                if (litAd.Text != string.Empty && litAd.Text.IndexOf(_resourceConstant) < 0)
                    this._resourceValue = litAd.Text;
            }

        }

        public bool HideAd { get; set; }

        private void SetSmallSquareChatVisibiliy()
        {
            this.Visible = false;
            try
            {
                if (g.Member != null)
                {
                    string pageIDsList = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ONLINE_CHAT_SUPPORT_PAGE_IDS",
                                                                                                                     _g.Brand.Site.
                                                                                                                         Community.
                                                                                                                         CommunityID,
                                                                                                                     _g.Brand.Site.SiteID);
                    string[] pagesIdsListArray = pageIDsList.Split(new char[] { ',' });
                    foreach (var pageID in pagesIdsListArray)
                    {
                        if (g.AppPage.ID.ToString() == pageID.Trim())
                        {
                            this.Visible = true;
                            break;
                        }
                    }
                }
            }
            catch (Exception)
            {
                this.Visible = false;
            }

        }

        private void SetSmallSquareVisibiliy()
        {
            string res = g.GetResource(_resourceConstant, this);
            if (String.IsNullOrEmpty(res) || res.ToLower().IndexOf("_resources") >= 0)
            {
                this.Visible = true;
            }
        }

        private void SetRectangleVisibility()
        {
            string res = g.GetResource(_resourceConstant, this);
            if (String.IsNullOrEmpty(res) || res.ToLower().IndexOf("_resources") >= 0)
            {
                if ((WebConstants.PageIDs)g.AppPage.ID == WebConstants.PageIDs.Default)
                {
                    this.Visible = true;
                }
            }
            if (
                Convert.ToBoolean(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("IS_IL_GAM",
                                                                                                    _g.Brand.Site.
                                                                                                        Community.
                                                                                                        CommunityID,
                                                                                                    _g.Brand.Site.SiteID)))
            {
                this.Visible = true;
            }
        }

        private void SetBlockUIVisibility()
        {
            try
            {
                this.Visible = false;

                if (g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.JDateCoIL)
                {
                    switch ((WebConstants.PageIDs)g.AppPage.ID)
                    {
                        case WebConstants.PageIDs.SMSAlert:
                        case WebConstants.PageIDs.SMSAlertsSettings:
                            this.Visible = true;
                            break;

                        default:
                            this.Visible = false;
                            break;
                    }
                }
            }
            catch (Exception ex)
            {//do not break the home page :)
            }
        }

        private void SetTextLinkFooterVisibility()
        {
            bool enabled = Conversion.CBool(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("TEXT_LINK_LEFT_ENABLED", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID));
            if (enabled)
            {

                switch ((WebConstants.APP)g.AppPage.App.ID)
                {
                    case WebConstants.APP.HotList:
                    case WebConstants.APP.MembersOnline:
                    case WebConstants.APP.Search:
                    case WebConstants.APP.PhotoGallery:
                    case WebConstants.APP.QuickSearch:
                        enabled = true;
                        break;
                    default:
                        enabled = false;
                        break;
                }
                if (!enabled)
                {
                    switch ((WebConstants.PageIDs)g.AppPage.ID)
                    {
                        case WebConstants.PageIDs.MatchMeterMatches:
                        case WebConstants.PageIDs.ViewProfile:
                        case WebConstants.PageIDs.Default:
                            enabled = true;
                            break;
                        default:
                            enabled = false;
                            break;
                    }
                }
            }
            this.Visible = enabled;
        }

        private void SetTextLinkLeftVisibility()
        {
            this.Visible = Conversion.CBool(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("TEXT_LINK_LEFT_ENABLED", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID));
        }

        public string ReplaceLinkKeywords(string link)
        {
            /*  try
              {*/
            //do a site check i had one before my code was deleted by Build_mt
            if (Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("IS_DoubleCLick_Site", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID).ToUpper() == "true".ToUpper())
            {
                if (g.Member != null)
                {
                    //Replace GENDER
                    int genderMask = g.Member.GetAttributeInt(g.Brand, "gendermask");
                    string strGender = FrameworkGlobals.GetGenderString(genderMask);

                    try
                    {
                        if (FrameworkGlobals.isHebrewSite(g.Brand))
                        {
                            if ((genderMask & Matchnet.Lib.ConstantsTemp.GENDERID_MALE) == Matchnet.Lib.ConstantsTemp.GENDERID_MALE)
                            {
                                strGender = g.GetResource("ADVERTISING_GENDER_MAN", this);
                            }
                            else
                            {
                                if ((genderMask & Matchnet.Lib.ConstantsTemp.GENDERID_FEMALE) == Matchnet.Lib.ConstantsTemp.GENDERID_FEMALE)
                                    strGender = g.GetResource("ADVERTISING_GENDER_WOMAN", this);
                            }
                        }
                    }
                    catch (Exception ex2)
                    {
                        strGender = FrameworkGlobals.GetGenderString(genderMask);
                    }

                    link = link.Replace("GENDER", strGender);
                    //Setting Region attributes 
                    int rid = g.Member.GetAttributeInt(g.Brand, "RegionID", 0);
                    RegionLanguage region = RegionSA.Instance.RetrievePopulatedHierarchy(rid, Constants.NULL_INT);
                    if (region != null)
                    {
                        link = link.Replace("COUNTRY", region.CountryName);
                        link = link.Replace("STATE", region.StateDescription);
                        link = link.Replace("ZIP", region.PostalCode);
                    }
                    // DMA information
                    link = link.Replace("DMAC", GetDMA(rid));

                    //replace AGE
                    link = link.Replace("AGE", FrameworkGlobals.GetAge(g.Member, g.Brand).ToString());
                    // Age range
                    link = link.Replace("AER", GetAgeRange(FrameworkGlobals.GetAge(g.Member, g.Brand)));
                    // days Since registration
                    link = link.Replace("REGD", GetDaysSinceReg());
                    DateTime dtSubendDate = g.Member.GetAttributeDate(g.Brand, WebConstants.ATTRIBUTE_NAME_SUBSCRIPTIONEXPIRATIONDATE, DateTime.MinValue);
                    if (dtSubendDate != null)
                    {
                        // member subscription status
                        link = link.Replace("SUBST", GetSubscriptionStatus(dtSubendDate));
                        // Renewal date is in. Subscription privillages ends in xx days or subscription ended at this date (for non-active subscribers)(SUBEND)
                        link = link.Replace("SUBEND", dtSubendDate != DateTime.MinValue ? GetDaysToRenewal(dtSubendDate) : "");
                    }

                    // Days since subscription (DAYSSUB)					
                    link = link.Replace("DAYSUB", GetDaysSinceSub());
                    // Ethnicity					
                    link = link.Replace("ETH", GetEthnicity());
                    // Religion
                    link = link.Replace("REL", GetReligion());
                    // Education
                    link = link.Replace("EDU", GetAttributeValueByAttributeName("EducationLevel"));
                    //city/region
                    link = link.Replace("CTRG", GetRegion());
                    // Pet holder
                    link = link.Replace("PET", GetAttributeValueByAttributeName("Pets"));
                    // Profession
                    link = link.Replace("PROF", GetAttributeValueByAttributeName("IndustryType"));
                    // Smoking Habits
                    link = link.Replace("SMO", GetAttributeValueByAttributeName("SmokingHabits"));
                    // Body style
                    link = link.Replace("BOD", GetAttributeValueByAttributeName("BodyType"));
                    // Current relationship status
                    if (g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.CollegeLuv)
                    {
                        link = link.Replace("MAR", GetAttributeValueByAttributeName("RelationshipStatus"));
                    }
                    else
                    {
                        link = link.Replace("MAR", GetAttributeValueByAttributeName("MaritalStatus"));
                    }
                    if (g.Session.GetString(WebConstants.SESSION_PROPERTY_NAME_PROMOTIONID, null) != null)
                    {
                        link = link.Replace("PRM", g.Session.GetString(WebConstants.SESSION_PROPERTY_NAME_PROMOTIONID, null));
                    }
                }
                //Repalce RANDOMNUMBER
                link = link.Replace("DEV", (g.IsDevMode ? "dev" : ""));

                //Repalce RANDOMNUMBER
                link = link.Replace("INSERT_RANDOM_NUMBER_HERE", g.RandomNumber.ToString());
                link = link.Replace("RANDOMNUMBER", g.RandomNumber.ToString());

                //Replace member ID
                try
                {
                    link = link.Replace("MEMID", g.Member.MemberID.ToString());
                }
                catch
                {
                    link = link.Replace("MEMID", "0");
                }
                //Replace URL
                link = link.Replace("URL", HttpUtility.UrlEncode(Request.Url.AbsoluteUri.IndexOf('?') > 0 ? Request.Url.AbsoluteUri.Remove(Request.Url.AbsoluteUri.IndexOf('?')) : Request.Url.AbsoluteUri));
                if ((WebConstants.APP)g.AppPage.App.ID == WebConstants.APP.MemberProfile)
                {
                    string targetMemberID = Request["MemberID"];
                    int iTargetMemId = Conversion.CInt(targetMemberID, Constants.NULL_INT);

                    if (iTargetMemId != Constants.NULL_INT && g.PromotionalProfileEnabled())
                    {
                        int iPromoMemId = g.PromotionalProfileMember();
                        if (iPromoMemId != Constants.NULL_INT && iPromoMemId == iTargetMemId)
                        {
                            link = link.Replace("PROMOMEMBERID", iPromoMemId.ToString());
                        }
                    }
                }

            }
            /*  }
            catch (Exception ex4)
            {
                
                link = ex4.Message;
              }*/
            return link;
        }
        private string GetAgeRange(int age)
        {
            string strResult = string.Empty;
            if (age >= 18 && age <= 24)
            {
                strResult = "a";
            }
            if (age >= 25 && age <= 29)
            {
                strResult = "b";
            }
            if (age >= 30 && age <= 39)
            {
                strResult = "c";
            }
            if (age >= 40 && age <= 49)
            {
                strResult = "d";
            }
            if (age >= 50 && age <= 59)
            {
                strResult = "e";
            }
            if (age >= 60 && age <= 69)
            {
                strResult = "f";
            }
            if (age >= 70)
            {
                strResult = "g";
            }

            return strResult;
        }

        private string GetDaysSinceReg()
        {
            string strResult = string.Empty;

            DateTime memberRegDate = g.Member.GetAttributeDate(g.Brand, "BrandInsertDate", System.DateTime.MinValue);
            TimeSpan ts = DateTime.Now.Subtract(memberRegDate);
            int days = ts.Days;


            if (days >= 0 && days <= 15)
            {
                strResult = "a";
            }
            if (days >= 16 && days <= 30)
            {
                strResult = "b";
            }
            if (days >= 31 && days <= 60)
            {
                strResult = "c";
            }
            if (days >= 61 && days <= 90)
            {
                strResult = "d";
            }
            if (days >= 91 && days <= 180)
            {
                strResult = "e";
            }
            if (days >= 181)
            {
                strResult = "f";
            }

            return strResult;
        }

        private string GetSubscriptionStatus(DateTime dtSubEndDate)
        {
            string strResult = string.Empty; // Initializing to NonSubscriber NverSubscribed

            try
            {
                MemberSubscriptionStatus memsubstatus = PurchaseSA.Instance.GetMemberSubStatus(g.Member.MemberID, g.Brand.Site.SiteID, dtSubEndDate);
                if (memsubstatus != null)
                {
                    switch (memsubstatus.Status)
                    {
                        case SubscriptionStatus.NonSubscriberNeverSubscribed:
                            strResult = "a";
                            break;
                        case SubscriptionStatus.NonSubscriberExSubscriber:
                            strResult = "b";
                            break;
                        case SubscriptionStatus.SubscribedFTS:
                            strResult = "c";
                            break;
                        case SubscriptionStatus.SubscribedWinBack:
                            strResult = "d";
                            break;
                        case SubscriptionStatus.RenewalSubscribed:
                            strResult = "e";
                            break;
                        case SubscriptionStatus.RenewalWinBack:
                            strResult = "f";
                            break;
                        case SubscriptionStatus.PremiumSubscriber:
                            strResult = "g";
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
                System.Diagnostics.Trace.Write("GetSubscriptionStatus exception. (MemberID=" + g.Member.MemberID.ToString() + ")" + ex.Message);
            }

            return strResult;

        }

        private string GetDaysSinceSub()
        {
            string strResult = string.Empty;

            try
            {
                if (g.Member.GetAttributeDate(g.Brand, "SubscriptionLastInitialPurchaseDate") != DateTime.MinValue)
                {
                    int daysSinceLastTransactionDate = (DateTime.Now.Date - g.Member.GetAttributeDate(g.Brand, "SubscriptionLastInitialPurchaseDate")).Days;
                    if (daysSinceLastTransactionDate >= 0 && daysSinceLastTransactionDate <= 30)
                    {
                        strResult = "a";
                    }
                    else if (daysSinceLastTransactionDate >= 31 && daysSinceLastTransactionDate <= 60)
                    {
                        strResult = "b";
                    }
                    else if (daysSinceLastTransactionDate >= 61 && daysSinceLastTransactionDate <= 90)
                    {
                        strResult = "c";
                    }
                    else if (daysSinceLastTransactionDate >= 91 && daysSinceLastTransactionDate <= 180)
                    {
                        strResult = "d";
                    }
                    else if (daysSinceLastTransactionDate >= 181)
                    {
                        strResult = "e";
                    }
                }
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
                System.Diagnostics.Trace.Write("GetDaysSinceSub exception (MemberID=" + g.Member.MemberID.ToString() + ")" + ex.Message);
            }
            return strResult;
        }
        private string GetDaysToRenewal(DateTime dtSubEndDate)
        {
            string strResult = string.Empty;
            int intDaysToRenewal = 0;

            try
            {

                intDaysToRenewal = ((TimeSpan)dtSubEndDate.Subtract(DateTime.Now)).Days; ////You have subscription privileges until:

                if (intDaysToRenewal >= 0 && intDaysToRenewal <= 14)
                {
                    strResult = "a";
                }
                if (intDaysToRenewal > 14 && intDaysToRenewal <= 30)
                {
                    strResult = "b";
                }
                if (intDaysToRenewal > 30 && intDaysToRenewal <= 60)
                {
                    strResult = "c";
                }
                if (intDaysToRenewal > 60 && intDaysToRenewal <= 90)
                {
                    strResult = "d";
                }
                if (intDaysToRenewal > 90 && intDaysToRenewal <= 180)
                {
                    strResult = "e";
                }
                if (intDaysToRenewal > 180 && intDaysToRenewal <= 365)
                {
                    strResult = "f";
                }
                if (intDaysToRenewal > 365)
                {
                    strResult = "g";
                }
                if (intDaysToRenewal < 0 && intDaysToRenewal >= -14)
                {
                    strResult = "h";
                }
                if (intDaysToRenewal < -14 && intDaysToRenewal >= -30)
                {
                    strResult = "i";
                }
                if (intDaysToRenewal < -30 && intDaysToRenewal >= -60)
                {
                    strResult = "j";
                }
                if (intDaysToRenewal < -60 && intDaysToRenewal >= -90)
                {
                    strResult = "k";
                }
                if (intDaysToRenewal < -90 && intDaysToRenewal >= -180)
                {
                    strResult = "l";
                }
                if (intDaysToRenewal < -180 && intDaysToRenewal >= -365)
                {
                    strResult = "m";
                }
                if (intDaysToRenewal < -365)
                {
                    strResult = "n";
                }

            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
                System.Diagnostics.Trace.Write("GetDaysToRenewal exception (MemberID=" + g.Member.MemberID.ToString() + ")" + ex.Message);
            }
            return strResult;
        }
        /// <summary>
        /// This method returns a DMA code based on passed RegionID parameter. 
        /// </summary>
        /// <remarks>This is important to note that the regionID parameter is a ZipCode RegionID (depth 4)</remarks>
        /// <param name="regionID"></param>
        /// <returns></returns>
        private string GetDMA(int regionID)
        {
            string strResult = string.Empty;

            try
            {
                DMA memDma = RegionSA.Instance.GetDMAByZipRegionID(regionID);
                if (memDma != null)
                {
                    strResult = memDma.DMAID.ToString();
                }
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
            return strResult;
        }
        private string GetEthnicity()
        {
            string strResult = string.Empty;
            string strEthnicity = string.Empty;

            // encompasses JD and JDIL
            if (((int)WebConstants.COMMUNITY_ID.JDate == g.Brand.Site.Community.CommunityID))
            {
                //strResult=Option.GetOptions.GetContent("JDateEthnicity", g.Member.GetAttributeInt(g.Brand, "JDateEthnicity"), g);//g.Member.GetAttributeInt(g.Brand, "JDateEthnicity").ToString();
                //  strEthnicity = ProfileDisplayHelper.GetOptionValue(g.Member, g, "JDateEthnicity", "JDateEthnicity");
                Matchnet.Content.ValueObjects.AttributeOption.AttributeOptionCollection aoc = AttributeOptionSA.Instance.GetAttributeOptionCollection("JDateEthnicity", 3);
                if (aoc != null)
                {
                    string s = GetAttributeValueByAttributeName("JDateEthnicity");
                    if (!string.IsNullOrEmpty(s))
                    {
                        int attID = 0;
                        if (int.TryParse(s, out attID))
                        {
                            Matchnet.Content.ValueObjects.AttributeOption.AttributeOption ao = aoc[attID];
                            if (ao != null)
                            {
                                strEthnicity = ao.Description;
                            }
                        }
                    }
                }
            }
            else
            {
                //strResult=g.Member.GetAttributeInt(g.Brand, "Ethnicity").ToString();
                strEthnicity = ProfileDisplayHelper.GetOptionValue(g.Member, g, "Ethnicity", "Ethnicity");
            }


            if (strEthnicity.IndexOf("European") >= 0)
            {
                strResult = "a";
            }
            if (strEthnicity.IndexOf(@"African Descent/Black") >= 0)
            {
                strResult = "b";
            }
            if (strEthnicity.IndexOf("Asian") >= 0)
            {
                strResult = "c";
            }
            if (strEthnicity.IndexOf("Caucasian") >= 0)
            {
                strResult = "d";
            }
            if (strEthnicity.IndexOf(@"South Asian/Eat India") >= 0)
            {
                strResult = "e";
            }
            if (strEthnicity.IndexOf("Hispanic") >= 0)
            {
                strResult = "f";
            }
            if (strEthnicity.IndexOf("Other") >= 0)
            {
                strResult = "g";
            }
            if (strEthnicity.IndexOf("Native American") >= 0)
            {
                strResult = "h";
            }
            if (strEthnicity.IndexOf("Mixed Race") >= 0)
            {
                strResult = "i";
            }
            if ((strEthnicity.Trim()).Length == 0)
            {
                strResult = "j";
            }
            if (strEthnicity.IndexOf("Pacific Islander") >= 0)
            {
                strResult = "k";
            }
            if (strEthnicity.IndexOf("Middle Eastern") >= 0)
            {
                strResult = "l";
            }
            if (strEthnicity.IndexOf("Ashkenazi") >= 0)
            {
                strResult = "m";
            }
            if (strEthnicity.IndexOf("Mixed Ethnic") >= 0)
            {
                strResult = "n";
            }
            if (strEthnicity.IndexOf("Another Ethnic") >= 0)
            {
                strResult = "o";
            }
            if (strEthnicity.IndexOf("Sephardic") >= 0)
            {
                strResult = "p";
            }
            if (strEthnicity.IndexOf("Will tell you later") >= 0)
            {
                strResult = "q";
            }

            return strResult;
        }

        private string GetReligion()
        {
            string strResult = string.Empty;
            string strReligion = string.Empty;

            /* 
               Matchnet.Content.ValueObjects.AttributeOption.AttributeOptionCollection aoc = AttributeOptionSA.Instance.GetAttributeOptionCollection("JDateEthnicity", 3);
                if (aoc != null)
                {
                    string s = GetAttributeValueByAttributeName("JDateEthnicity");
                    if (!string.IsNullOrEmpty(s))
                    {
                        int attID = 0;
                        if (int.TryParse(s, out attID))
                        {
                            Matchnet.Content.ValueObjects.AttributeOption.AttributeOption ao = aoc[attID];
                            if (ao != null)
                            {
                                strEthnicity = ao.Description;
                            }
                        }
                    }
                }
             */
            // encompasses JD and JDIL
            if (((int)WebConstants.COMMUNITY_ID.JDate == g.Brand.Site.Community.CommunityID))
            {
                Matchnet.Content.ValueObjects.AttributeOption.AttributeOptionCollection aoc = AttributeOptionSA.Instance.GetAttributeOptionCollection("JdateReligion", 3);
                if (aoc != null)
                {
                    string s = GetAttributeValueByAttributeName("JdateReligion");
                    if (!string.IsNullOrEmpty(s))
                    {
                        int attID = 0;
                        if (int.TryParse(s, out attID))
                        {
                            Matchnet.Content.ValueObjects.AttributeOption.AttributeOption ao = aoc[attID];
                            if (ao != null)
                            {
                                strReligion = ao.Description;
                            }
                        }
                    }
                }
            }
            else
            {
                if (((int)WebConstants.COMMUNITY_ID.Cupid == g.Brand.Site.Community.CommunityID))
                {
                    Matchnet.Content.ValueObjects.AttributeOption.AttributeOptionCollection aoc = AttributeOptionSA.Instance.GetAttributeOptionCollection("JdateReligion", 10);
                    if (aoc != null)
                    {
                        string s = GetAttributeValueByAttributeName("JdateReligion");
                        if (!string.IsNullOrEmpty(s))
                        {
                            int attID = 0;
                            if (int.TryParse(s, out attID))
                            {
                                Matchnet.Content.ValueObjects.AttributeOption.AttributeOption ao = aoc[attID];
                                if (ao != null)
                                {
                                    strReligion = ao.Description;
                                }
                            }
                        }
                    }
                }
                else
                {
                    strReligion = ProfileDisplayHelper.GetOptionValue(g.Member, g, "Religion", "Religion");
                }

            }


            if (strReligion.IndexOf(@"Church of England") >= 0)
            {
                strResult = "a";
            }
            if (strReligion.Trim().Length == 0)
            {
                strResult = "b";
            }
            if (strReligion.IndexOf(@"Atheist") >= 0)
            {
                strResult = "c";
            }
            if (strReligion.IndexOf(@"Catholic") >= 0)
            {
                strResult = "d";
            }
            if (strReligion.IndexOf(@"Protestant") >= 0)
            {
                strResult = "e";
            }
            if (strReligion.IndexOf(@"Other Christian") >= 0)
            {
                strResult = "f";
                return strResult; // This is because of conflicts in Other and Christian
            }
            if (strReligion.IndexOf(@"Hindu") >= 0)
            {
                strResult = "g";
            }
            if (strReligion.IndexOf(@"Muslim") >= 0)
            {
                strResult = "h";
            }
            if (strReligion.IndexOf(@"Jewish") >= 0)
            {
                strResult = "i";
            }
            if (strReligion.IndexOf(@"Mormon") >= 0)
            {
                strResult = "j";
            }
            if (strReligion.IndexOf(@"Non Religious") >= 0 || strReligion.IndexOf(@"Not Religious") >= 0)
            {
                strResult = "k";
                return strResult; // // This is because of a conflict in Religious
            }
            if (strReligion.IndexOf(@"Other") >= 0)
            {
                strResult = "l";
            }
            if (strReligion.IndexOf(@"Unaffiliated") >= 0)
            {
                strResult = "m";
            }
            if (strReligion.IndexOf(@"Anglican") >= 0)
            {
                strResult = "n";
            }
            if (strReligion.IndexOf(@"Baptist") >= 0)
            {
                strResult = "o";
            }
            if (strReligion.IndexOf(@"Lutheran") >= 0)
            {
                strResult = "p";
            }
            if (strReligion.IndexOf(@"Methodist") >= 0)
            {
                strResult = "q";
            }
            if (strReligion.IndexOf(@"Presbyterian") >= 0)
            {
                strResult = "r";
            }
            if (strReligion.IndexOf(@"Sikh") >= 0)
            {
                strResult = "s";
            }
            if (strReligion.IndexOf(@"Christian") >= 0)
            {
                strResult = "t";
            }
            if (strReligion.IndexOf(@"Buddhist") >= 0)
            {
                strResult = "u";
            }
            if (strReligion.IndexOf(@"Agnostic") >= 0)
            {
                strResult = "v";
            }
            if (strReligion.IndexOf(@"Pagan") >= 0)
            {
                strResult = "w";
            }
            if (strReligion.IndexOf(@"Quaker") >= 0)
            {
                strResult = "x";
            }
            if (strReligion.IndexOf(@"Will tell you later") >= 0)
            {
                strResult = "y";
            }
            if (strReligion.IndexOf(@"Religious") >= 0)
            {
                strResult = "z";
            }
            if (strReligion.IndexOf(@"Orthodox (Baal Teshuva)") >= 0 || strReligion.IndexOf(@"Orthodox(Baal Teshuva)") >= 0)
            {
                strResult = "01";
            }
            if (strReligion.IndexOf(@"Conservative") >= 0)
            {
                strResult = "02";
            }
            if (strReligion.IndexOf(@"Conservadox") >= 0)
            {
                strResult = "03";
            }
            if (strReligion.IndexOf(@"Hassidic") >= 0)
            {
                strResult = "04";
            }
            if (strReligion.IndexOf(@"Modern Orthodox") >= 0)
            {
                strResult = "05";
            }
            if (strReligion.IndexOf(@"Orthodox (frum from bi)") >= 0 || strReligion.IndexOf(@"Orthodox (frum from birth)") >= 0)
            {
                strResult = "06";
            }
            if (strReligion.IndexOf(@"Another Stream") >= 0)
            {
                strResult = "07";
            }
            if (strReligion.IndexOf(@"Reconstructionist") >= 0)
            {
                strResult = "08";
            }
            if (strReligion.IndexOf(@"Reform") >= 0)
            {
                strResult = "09";
            }
            if (strReligion.IndexOf(@"Secular") >= 0)
            {
                strResult = "10";
            }
            if (strReligion.IndexOf(@"Traditional") >= 0)
            {
                strResult = "11";
            }
            if (strReligion.IndexOf(@"Willing to convert") >= 0 || strReligion.IndexOf(@"Willing to Convert") >= 0)
            {
                strResult = "12";
            }

            return strResult;
        }
        // Gets the attribute name and return the attrinute value of the current member, returns null if empty or on error
        private string GetAttributeValueByAttributeName(string attributeName)
        {
            string strResult = string.Empty;
            try
            {
                strResult = g.Member.GetAttributeInt(g.Brand, attributeName).ToString();

            }
            catch (Exception ex)
            {
                strResult = string.Empty;
            }
            return strResult;
        }
        private string GetRegion()
        {
            string strResult = string.Empty;
            try
            {
                int regionID = Int32.Parse(GetAttributeValueByAttributeName("RegionID"));
                Matchnet.Content.ValueObjects.Region.RegionLanguage regionLanguage = RegionSA.Instance.RetrievePopulatedHierarchy(regionID, g.TargetLanguageID);
                strResult = regionLanguage.StateRegionID.ToString();
            }
            catch (Exception ex)
            {
                strResult = string.Empty;
            }
            return strResult;
        }

        #region Enumerations

        public enum AdSize
        {
            /// <summary>
            /// 728 x 90 Leaderboard ad (for footer of page)
            /// </summary>
            Leaderboard = 1,
            /// <summary>
            /// 728 x 90 Leaderboard ad (for header of chat page for example)
            /// </summary>
            LeaderboardHeader,
            /// <summary>
            /// 160 x 600 Wide Skyscraper ad
            /// </summary>
            WideSkyscraper,
            /// <summary>
            /// 160 x 600 Wide Skyscraper ad for special placement in special article category pages inside margin
            /// </summary>
            WideSkyscraperArticle,
            /// <summary>
            /// 468 x 60 Full Banner ad
            /// </summary>
            FullBanner,
            /// <summary>
            /// 234 x 60 Half Banner ad
            /// </summary>
            HalfBanner,
            /// <summary>
            /// 400 x 600 Large Rectangle ad (*this size is non-standard)
            /// </summary>
            LargeRectangle,
            /// <summary>LeaderboardHeaderAboveMenu
            /// 180 x 150 Rectangle
            /// </summary>
            Rectangle,
            /// <summary>
            /// 120 x 240 Vertical Banner
            /// </summary>
            VerticalBanner,
            /// <summary>
            /// 300 x 250 Square
            /// </summary>
            Square,
            /// <summary>
            /// 728 x 90 Leaderboard ad (for above the menu)
            /// </summary>
            LeaderboardHeaderAboveMenu,
            /// <summary>
            /// 728 x 90 Leaderboard ad (for above the menu and everything)
            /// </summary>
            LeaderboardHeaderAboveAll,
            /// <summary>
            /// 728 x 90 Leaderboard ad (for below the menu)
            /// </summary>
            LeaderboardHeaderBelowMenu,
            /// <summary>
            /// 592 X 60 Leaderboard ad (for below the menu)
            /// </summary>
            SmallLeaderboardHeaderBelowMenu,
            /// <summary>
            /// 468 x 48 Shorter version of Full Banner Ad
            /// </summary>
            ShortFullBanner,
            /// <summary>
            /// 600 x 90 Leaderboard ad above the redesigned member profile page with tab navigation
            /// </summary>
            LeaderboardMemberProfileA,
            /// <summary>
            /// 600 x 90 Leaderboard ad above the redesigned member profile page with tab navigation
            /// </summary>
            LeaderboardMemberProfileB,
            /// <summary>
            /// 600 x 90 Leaderboard ad above the redesigned member profile page with tab navigation
            /// </summary>
            LeaderboardMemberProfileC,
            /// <summary>
            /// 600 x 90 Leaderboard ad above the redesigned member profile page with tab navigation
            /// </summary>
            LeaderboardMemberProfileD,

            LeaderboardMemberProfileE,
            //forced page
            ForcedPage,
            /// <summary>
            /// 250 X 250 Text Link ad for left nav 
            /// </summary>
            TextLinkLeft,
            /// <summary>
            /// 728 X 90 Text Link ad for the footer
            /// </summary>
            TextLinkFooter,
            /// <summary>
            /// Block UI - Will be used for SMS Alerts PM-78, but I guess it can be used for other features as well
            /// </summary>
            BlockUI,
            /// <summary>
            /// 160 X 130 - SmallSquare for MPR-994 - Cupid - Creating commercial zone for Kanguru search engine 
            /// </summary>
            SmallSquare,
            /// <summary>
            /// 160 X 160 - SmallSquareChat - MPR-907 - Open new space in JDFR website for "Online Chat Support" banner placement
            /// </summary>
            SmallSquareChat,
            /// <summary>
            /// 590 X 60 - Games banner
            /// </summary>
            Games,
            SplashMiddle,
            FlirtSent,
            RegTop,
            LeftNavDefault,
            /// <summary>
            /// 165 x 86 on Splash page
            /// </summary>
            SplashTop,
            /// <summary>
            /// 860 x 40 top banner
            /// </summary>
            LargeTopSubBanner,
            /// <summary>
            /// 105 x 32 Sub Tab
            /// </summary>
            SubTab,
            /// <summary>
            /// 1x1 wallpaper ad
            /// </summary>
            Wallpaper,
            /// <summary>
            /// 1x1 overlay
            /// </summary>
            Overlay
        }


        #endregion

        #region Member Variables

        //default size
        private AdUnit.AdSize _size = AdUnit.AdSize.HalfBanner;
        protected System.Web.UI.WebControls.Literal litAd;
        protected Matchnet.Web.Framework.Txt txtAdvertiseWithUs;
        protected System.Web.UI.WebControls.Panel pnlAd;
        //default resource constant
        private string _resourceConstant = "AD_HALFBANNER_VISITOR";
        private bool _expandImageTokens = false;
        private string _AdIFrameID = String.Empty;
        private string _resourceValue = String.Empty;
        private bool isGAMEnabled = false;

        protected System.Web.UI.WebControls.Panel pnlAdvertiseWithUs;
        protected System.Web.UI.WebControls.PlaceHolder phGAMIframe;
        protected System.Web.UI.WebControls.PlaceHolder phAdMessage;
        protected System.Web.UI.WebControls.Literal litAdMessage;
        protected System.Web.UI.HtmlControls.HtmlIframe iframeGAM;

        public enum GAMPageModeType
        {
            None,
            Auto
        }

        private string _gamAdSlot = String.Empty;
        private GAMPageModeType _gamPageMode = GAMPageModeType.None;
        private bool _GAMIframe = false;
        private int _GAMIframeWidth = 0;
        private int _GAMIframeHeight = 0;

        private bool _Disabled = false;  //this allows us to automatically disable an adunit so that it does not go through any of the normal processing

        #endregion

        #region Properties

        /// <summary>
        /// The size of the ad unit (Leaderboard, HalfBanner etc.)
        /// </summary>
        public AdUnit.AdSize Size
        {

            get { return _size; }
            set { _size = value; }

        }

        /// <summary>
        /// Code behind determines the page name and prepends to the GAMAdSlot name including the first underscore.
        /// 
        /// For example, 
        /// 
        /// GAMAdSlot = right_120x240
        /// GAMPageMode = Auto
        /// 
        /// This on mol page will generate full GAMAdSlot name value of mol_left_120x240
        /// 
        /// Default value is None.
        /// </summary>
        public GAMPageModeType GAMPageMode
        {
            get
            {
                return _gamPageMode;
            }
            set
            {
                _gamPageMode = value;
            }
        }

        public string GAMAdSlot
        {
            get
            {
                return _gamAdSlot;
            }
            set
            {
                _gamAdSlot = value;
            }
        }

        /// <summary>
        /// Boolean to indicate whether to render out the GAM Ad in an IFrame
        /// Note: Iframe Client ID is exposed by AdIframeID.
        /// </summary>
        public bool GAMIframe
        {
            get
            {
                return _GAMIframe;
            }
            set
            {
                _GAMIframe = value;
            }
        }

        /// <summary>
        /// Specifies the width for GAM Iframe, if GAMIframe is true
        /// </summary>
        public int GAMIframeWidth
        {
            get
            {
                return _GAMIframeWidth;
            }
            set
            {
                _GAMIframeWidth = value;
            }
        }

        /// <summary>
        /// Specifies the height for GAM Iframe, if GAMIframe is true
        /// </summary>
        public int GAMIframeHeight
        {
            get
            {
                return _GAMIframeHeight;
            }
            set
            {
                _GAMIframeHeight = value;
            }
        }

        /// <summary>
        /// Whether or not to expand image tokens in the ad .
        /// TODO: this property should be removed when a 3rd party starts serving ads because token
        /// replacement causes unnecessary processing.
        /// </summary>
        public bool ExpandImageTokens
        {
            get { return _expandImageTokens; }
            set { _expandImageTokens = value; }
        }

        /// <summary>
        /// The ad's clientside IFrame ID, if any
        /// </summary>
        public string AdIFrameID
        {
            get { return this._AdIFrameID; }
        }

        /// <summary>
        /// Gets the ResourceValue for the AdUnit Control from the resource file
        /// </summary>
        public string ResourceValue
        {
            get { return this._resourceValue; }
        }

        /// <summary>
        /// Gets the AdUnit's Client PanelID
        /// </summary>
        public string AdPanelID
        {
            get { return this.pnlAd.ClientID; }
        }

        /// <summary>
        /// Allows us to automatically disable an adunit so that it does not go through any of the normal processing
        /// </summary>
        public bool Disabled
        {
            get { return _Disabled; }
            set { _Disabled = value; }
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Concatenates AD_ with ad size plus underscore and the user status to get correct resource constant
        /// </summary>
        /// <param name="adSize"></param>
        private void SetResourceConstant(string adSize)
        {
            _resourceConstant = "AD_" + adSize + "_";
            //VISITOR
            if (g.Member == null)
            {
                _resourceConstant += "VISITOR";
            }
            //SUBSCRIBER
            else if (MemberPrivilegeAttr.IsCureentSubscribedMember(g))
            {
                _resourceConstant += "SUBSCRIBER";
            }
            //REGISTERED USER
            else
            {
                _resourceConstant += "REGISTRANT";
            }

        }

        private void SetResourceConstant(string adSize, string versionPrefix)
        {
            _resourceConstant = "AD_" + adSize + "_";
            //VISITOR
            if (g.Member == null)
            {
                _resourceConstant += "VISITOR";
            }
            //SUBSCRIBER
            else if (MemberPrivilegeAttr.IsCureentSubscribedMember(g))
            {
                _resourceConstant += "SUBSCRIBER";
            }
            //REGISTERED USER
            else
            {
                _resourceConstant += "REGISTRANT";
                if (g.LayoutVersion == WebConstants.LayoutVersions.versionWide)
                    _resourceConstant = _resourceConstant + "_" + versionPrefix;
            }

        }

        private void SetSquareVisibility()
        {
            this.Visible = true;
            switch ((WebConstants.APP)g.AppPage.App.ID)
            {
                case WebConstants.APP.Email:
                    _resourceConstant += "_EMAIL";
                    break;
                case WebConstants.APP.Home:
                    _resourceConstant += "_HOME";
                    break;
                case WebConstants.APP.QuickSearch:
                    _resourceConstant += "_QUICKSEARCH";
                    break;
                case WebConstants.APP.Video:
                    _resourceConstant += "_VIDEO";
                    break;
                default:
                    if (!this.isGAMEnabled)
                        this.Visible = false;
                    break;
            }
        }
        private void SetLeaderboardVisibility()
        {
            this.Visible = true;
            this.pnlAd.CssClass = "mediumPadding";

            switch ((WebConstants.APP)g.AppPage.App.ID)
            {
                case WebConstants.APP.Article:
                    DetermineIfGeneralArticle();
                    break;
                case WebConstants.APP.HotList:
                    _resourceConstant += "_HOTLIST";
                    break;
                case WebConstants.APP.MembersOnline:
                    _resourceConstant += "_MOL";
                    break;
                case WebConstants.APP.Search:
                    _resourceConstant += "_SEARCH";
                    break;
                case WebConstants.APP.Email:
                    _resourceConstant += "_INBOX";
                    break;
                case WebConstants.APP.Home:
                    _resourceConstant += "_HOME";
                    break;
                case WebConstants.APP.PhotoGallery:
                    _resourceConstant += "_PHOTOGAL";
                    break;
                case WebConstants.APP.Logon:
                    _resourceConstant += "_LOGON";
                    break;
                default:
                    if (!this.isGAMEnabled)
                        this.Visible = false;
                    break;
            }
        }

        private void SetVerticalBannerVisibility()
        {
            this.Visible = true;
            switch ((WebConstants.APP)g.AppPage.App.ID)
            {
                case WebConstants.APP.MembersOnline:
                    if (g.AppPage.ControlName == "MembersOnline")
                    {
                        _resourceConstant += "_MOL";
                    }
                    break;
                case WebConstants.APP.HotList:
                    if (g.AppPage.ControlName == "View")
                    {
                        _resourceConstant += "_HOTLIST";
                    }
                    break;
                case WebConstants.APP.Search:

                    if (g.AppPage.ControlName == "SearchResults")
                    {
                        _resourceConstant += "_SEARCH";
                    }
                    break;
                case WebConstants.APP.Article:
                    DetermineIfGeneralArticle();
                    break;
                case WebConstants.APP.Email:
                    _resourceConstant += "_INBOX";
                    break;
                case WebConstants.APP.Home:
                    _resourceConstant += "_HOME";
                    break;
                case WebConstants.APP.PhotoGallery:
                    _resourceConstant += "_PHOTOGAL";
                    break;
                case WebConstants.APP.Video:
                    _resourceConstant += "_VIDEO";
                    break;
                default:
                    if (!this.isGAMEnabled)
                        this.Visible = false;
                    break;
            }

            //Need a margin on top (to separate it from what's above it)
            //set width to no more than the left nav, set overflow so that you don't push
            //main content down if text is longer. It's very sensitive.
            this.pnlAd.CssClass = "leftNavElement";
            //this.pnlAd.Style.Add("MARGIN-TOP","10px");
            //this.pnlAd.Style.Add("OVERFLOW","hidden");
            //this.pnlAd.Style.Add("POSITION","static");
            //this.pnlAd.Style.Add("WIDTH","160px");
            //this.pnlAd.Style.Add("TEXT-ALIGN","center");

        }

        private void SetWideskyscraperVisibility()
        {
            this.Visible = true;
            this.pnlAd.CssClass = "wideSkyscraperBannerAd";
            switch ((WebConstants.APP)g.AppPage.App.ID)
            {
                case WebConstants.APP.HotList:
                    _resourceConstant += "_HOTLIST";
                    break;
                case WebConstants.APP.MembersOnline:
                    _resourceConstant += "_MOL";
                    break;
                case WebConstants.APP.Search:
                    _resourceConstant += "_SEARCH";
                    //sksycraper on these pages needs to be positioned outside margins on the 
                    //right hand side (or left if Hebrew)
                    //this.pnlAd.Style.Add("position","absolute");
                    //this.pnlAd.Style.Add("top","150px");
                    //this.pnlAd.Style.Add((g.Brand.Site.LanguageID == (int)Matchnet.Language.Hebrew ? "right":"left"), "780px");
                    //this.pnlAd.CssClass = "wideSkyscraperBannerAd";
                    break;
                case WebConstants.APP.Email:
                    _resourceConstant += "_INBOX";
                    break;
                case WebConstants.APP.PhotoGallery:
                    _resourceConstant += "_PHOTOGAL";
                    break;
                case WebConstants.APP.Home:
                    _resourceConstant += "_HOME";
                    break;
                case WebConstants.APP.Article:
                    _resourceConstant += "_FAQ";
                    this.pnlAd.CssClass = "wideSkyscraperBannerAd";
                    if (IsSpecialArticleCategory())
                    {
                        this.Visible = false;
                    }
                    break;
                case WebConstants.APP.Video:
                    this.Visible = VideoPageWidescrapperVisible();
                    _resourceConstant += "_VIDEO";
                    break;
                default:
                    if (!this.isGAMEnabled)
                    {
                        this.Visible = false;
                    }
                    break;
            }

        }

        private void SetWideskyscraperArticleVisibility()
        {
            this.Visible = true;
            switch ((WebConstants.APP)g.AppPage.App.ID)
            {
                case WebConstants.APP.Article:
                    if (IsSpecialArticleCategory())
                    {
                        _resourceConstant += "_GENERAL";
                        //sksycraper on "special" article pages needs to be positioned INSIDE margins
                        //and floated to the right side of content (or left if Hebrew)
                        //this.pnlAd.Style.Add("float",(g.Brand.Site.LanguageID == (int)Matchnet.Language.Hebrew ? "left":"right"));
                        this.pnlAd.CssClass = "wideSkyscraperArticleBannerAd";
                        this.Visible = true;
                    }
                    else
                    {
                        this.Visible = false;
                    }
                    break;
                default:
                    if (!this.isGAMEnabled)
                        this.Visible = false;
                    break;

            }

        }

        private void SetFullBannerVisibility()
        {
            this.Visible = true;
            this.pnlAd.CssClass = "fullBannerBox";

            switch ((WebConstants.APP)g.AppPage.App.ID)
            {
                case WebConstants.APP.IM:
                    _resourceConstant += "_IM";
                    break;

                default:
                    break;
            }
        }

        private void SetHalfBannerVisibility()
        {
            this.Visible = true;
            //this.pnlAd.CssClass = "wideSkyscraperBannerAd";
            switch ((WebConstants.APP)g.AppPage.App.ID)
            {
                case WebConstants.APP.QuickSearch:
                    _resourceConstant += "_QUICKSEARCH";
                    break;
                case WebConstants.APP.IM:
                    _resourceConstant += "_IM";
                    break;
                default:
                    if (!this.isGAMEnabled)
                        this.Visible = false;
                    break;
            }

        }

        private void SetShortFullBannerVisibility()
        {
            this.Visible = true;

            switch ((WebConstants.APP)g.AppPage.App.ID)
            {
                case WebConstants.APP.IM:
                    _resourceConstant += "_IM";
                    break;

                default:
                    break;
            }
        }

        private void SetForcedPageVisibility()
        {
            try
            {
                this.Visible = false;

                switch ((WebConstants.APP)g.AppPage.App.ID)
                {

                    case WebConstants.APP.Home:
                        bool setting = Conversion.CBool(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("FORCEDPAGE_FLAG_HOME", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID));

                        string vis = g.Session.GetString("FORCEDPAGE_HOME_DISPLAYED");
                        if (vis != "true" && setting)
                        {
                            _resourceConstant += "_HOME";
                            this.Visible = true;
                            g.Session.Add("FORCEDPAGE_HOME_DISPLAYED", "true", Matchnet.Session.ValueObjects.SessionPropertyLifetime.Temporary);
                        }
                        _expandImageTokens = true;
                        break;

                    default:
                        if (!this.isGAMEnabled)
                            this.Visible = false;
                        break;
                }
            }
            catch (Exception ex)
            {//do not break the home page :)
            }
        }

        private void SetLeaderboardMemberProfileVisibility()
        {
            try
            {
                this.Visible = false;
         
                string gAMAdSlot = this.GAMAdSlot;

                //Checking GAMAdSlot because g.AppPage.ID = 60200 which is PageName 'GAMIframe' and is not unique to the page
                switch (gAMAdSlot)
                {
                    case "photogallery_modal_top_728x90":
                        bool setting = Conversion.CBool(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ENABLE_GALLERYMODAL_ADSLOT", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID));
                        this.Visible = setting;
                        break;

                    default:
                            this.Visible = true;
                        break;
                }
            }
            catch (Exception ex)
            {//do not break the home page :)
            }
        }

        private void SetSubTabVisibility()
        {
            try
            {
                this.Visible = IsSubTab;
            }
            catch (Exception ex)
            {//do not break the home page :)
            }
        }

        public bool IsSubTab { get; set; }


        private void DetermineIfGeneralArticle()
        {

            if (IsSpecialArticleCategory())
            {
                _resourceConstant += "_GENERAL";

            }
            else
            {
                _resourceConstant += "_FAQ";
            }

        }


        /// <summary>
        /// Returns true if the current page is in categoryID 2010, 2308, or 2006.
        /// This is important for determining if the Wide Skyscraper or Vertical
        /// Banner should appear.
        /// </summary>
        /// <returns></returns>
        private bool IsSpecialArticleCategory()
        {
            string categoryID = Request.QueryString[WebConstants.URL_PARAMETER_NAME_CATEGORYID];

            if (categoryID == "2010"		//	Make It Happen
                || categoryID == "2308"	//	Singles Talk
                || categoryID == "2006")	//	Happy Endings
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        private void SetLeaderBoardMenuAdUnitsVisibility()
        {
            try
            {
                // By default, let's show unless there is a runtime setting specific to this app page
                bool show = true;

                string settingconst = g.AppPage.ControlName + "_" + _size;
                int communitiyID = g.Brand.Site.Community.CommunityID;
                int siteID = g.Brand.Site.SiteID;

                if (Matchnet.Configuration.ServiceAdapters.RuntimeSettings.SettingExists(settingconst.ToUpper(), communitiyID, siteID))
                {
                    string settingVal = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting(settingconst.ToUpper(), communitiyID, siteID);
                    show = Conversion.CBool(settingVal);
                }

                if (show)
                {
                    this.Visible = true;
                }

                switch ((WebConstants.APP)g.AppPage.App.ID)
                {
                    case WebConstants.APP.Article:
                        DetermineIfGeneralArticle();
                        break;
                    case WebConstants.APP.HotList:
                        _resourceConstant += "_HOTLIST";
                        break;
                    case WebConstants.APP.MembersOnline:
                        _resourceConstant += "_MOL";
                        break;
                    case WebConstants.APP.Search:
                        _resourceConstant += "_SEARCH";
                        break;
                    case WebConstants.APP.Email:
                        _resourceConstant += "_INBOX";
                        break;
                    case WebConstants.APP.Home:
                        _resourceConstant += "_HOME";
                        break;
                    case WebConstants.APP.PhotoGallery:
                        _resourceConstant += "_PHOTOGAL";
                        break;
                    case WebConstants.APP.Video:
                        _resourceConstant += "_VIDEO";
                        break;
                    case WebConstants.APP.MemberProfile:
                        _resourceConstant += "_PROFILE";
                        break;
                    default:
                        if (!this.isGAMEnabled)
                            this.Visible = false;
                        break;
                }
            }
            catch (Exception ex)
            { System.Diagnostics.Trace.Write("SetLeaderBoardAdUnits exception " + ex.Message); }

        }

        /// <summary>
        /// Gets an attribute value of an html element contained in a string
        /// </summary>
        /// <param name="text">Text containing html element</param>
        /// <param name="elementName"></param>
        /// <param name="attributeName"></param>
        /// <returns></returns>
        private string GetAttributeValueFromElement(string text, string elementName, string attributeName)
        {
            string attributeValue = String.Empty;

            if (text.Trim() != "" && elementName != "" && attributeName != "")
            {
                int startingElementIndex = -1;
                int endingElementIndex = -1;
                int attributeIndex = -1;

                startingElementIndex = text.ToLower().IndexOf("<" + elementName.ToLower());
                if (startingElementIndex >= 0)
                {
                    endingElementIndex = text.ToLower().IndexOf(">", startingElementIndex);

                    if (endingElementIndex > startingElementIndex)
                    {
                        //look for attribute and get value
                        string elementText = text.ToLower().Substring(startingElementIndex, endingElementIndex + 1);

                        string attributeKey = " " + attributeName + "=";
                        attributeIndex = elementText.IndexOf(attributeKey);

                        if (attributeIndex > 0)
                        {
                            string attributeText = elementText.Substring(attributeIndex + attributeKey.Length);
                            int firstApostropheIndex = -1;
                            int secondApostropheIndex = -1;
                            char[] attributeArray = attributeText.ToCharArray();

                            for (int i = 0; i < attributeArray.Length; i++)
                            {
                                if (attributeArray[i] == Convert.ToChar("\"") || attributeArray[i] == Convert.ToChar("'"))
                                {
                                    if (firstApostropheIndex == -1)
                                        firstApostropheIndex = i;
                                    else
                                    {
                                        secondApostropheIndex = i;
                                        break;
                                    }

                                }
                            }

                            if (firstApostropheIndex > -1 && secondApostropheIndex > -1)
                                attributeValue = attributeText.Substring(firstApostropheIndex + 1, secondApostropheIndex - firstApostropheIndex - 1);

                        }
                    }
                }

            }

            return attributeValue;
        }

        private bool VideoPageWidescrapperVisible()
        {
            bool visible = true;
            try
            {
                string PlayerMode = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("VIDEO_MODE", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID);
                if (PlayerMode.ToLower() == Web.Applications.Video.VideoCenter20.VIDEO_MODE_CHANNEL)
                    visible = false;

                return visible;

            }
            catch (Exception ex)
            { return visible; }
        }

        private void SetWallpaperVisibility()
        {
            if (!this.isGAMEnabled)
            {
                this.Visible = false;
            }
            else
            {
                bool setting = Conversion.CBool(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ENABLE_WALLPAPER_ADSLOT", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID));
                this.Visible = setting;
            }
        }
        #endregion

        #region Public Methods
        public bool SetTopLeaderBoardMenuIFrameSupport()
        {
            bool IFrameRequired = false;
            try
            {
                switch ((WebConstants.APP)g.AppPage.App.ID)
                {
                    case WebConstants.APP.MemberProfile:
                        if (g.AppPage.ControlName.ToLower() == "viewprofile" || g.AppPage.ControlName.ToLower() == "viewtabbedprofile20")
                        {
                            this.GAMAdSlot = "profile_basics_top_728x90";
                            this.GAMPageMode = Matchnet.Web.Framework.Ui.Advertising.AdUnit.GAMPageModeType.None;
                            this.GAMIframe = true;
                            this.GAMIframeHeight = 90;
                            this.GAMIframeWidth = 728;
                            IFrameRequired = true;

                            if (ProfileUtility.IsDisplayingProfile30(g))
                            {
                                ProfileTabConfigs tabConfigs = ProfileUtility.GetProfileTabConfigs(g.Brand.Site.SiteID, g);
                                if (tabConfigs != null && tabConfigs.ProfileTabConfigList != null && tabConfigs.ProfileTabConfigList.Count > 0)
                                {
                                    ProfileTabEnum activeTab = ViewProfileTabUtility.DetermineActiveTab(tabConfigs.ProfileTabConfigList[0].ProfileTabType);
                                    if (activeTab != ProfileTabEnum.None)
                                    {
                                        ProfileTabConfig tabConfig = ProfileUtility.GetProfileTabConfig(g.Brand.Site.SiteID, g, activeTab);
                                        if (tabConfig != null && !String.IsNullOrEmpty(tabConfig.GAMAdSlotTop))
                                        {
                                            this.GAMAdSlot = tabConfig.GAMAdSlotTop;
                                        }
                                    }
                                }
                            }
                        }
                        break;
                    case WebConstants.APP.Termination:
                        if (g.AppPage.ControlName.ToLower() == "terminatefinal")
                        {
                            this.GAMAdSlot = "terminateconfirm_top_728x90";
                            this.GAMPageMode = Matchnet.Web.Framework.Ui.Advertising.AdUnit.GAMPageModeType.None;
                            this.GAMIframe = true;
                            this.GAMIframeHeight = 90;
                            this.GAMIframeWidth = 728;
                            IFrameRequired = true;
                        }
                        break;
                    case WebConstants.APP.Home:
                        if (g.AppPage.ControlName.ToLower() == "default")
                        {
                            this.GAMAdSlot = "home_top_728x90";
                            this.GAMPageMode = Matchnet.Web.Framework.Ui.Advertising.AdUnit.GAMPageModeType.None;
                            this.GAMIframe = true;
                            this.GAMIframeHeight = 90;
                            this.GAMIframeWidth = 728;
                            IFrameRequired = true;
                        }
                        break;

                }
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }

            if (IFrameRequired)
            {
                Page.ClientScript.RegisterClientScriptBlock(typeof(System.Web.UI.Page), "TopAuxNavJSAd",
                           "<script type=\"text/javascript\">"
                           + "var topAd728x90IFrameID = \"" + iframeGAM.ClientID + "\";"
                           + "</script>");
            }

            return IFrameRequired;
        }

        public void ShowRightAdMessageForNonSubs()
        {
            phAdMessage.Visible = true;
            string subscribeHook = g.GetResource("TXT_AD_MESSAGE_SUBSCRIBE_TO_DISABLE_ADS", this);

            subscribeHook = String.Format(subscribeHook,
                    FrameworkGlobals.LinkHref("/Applications/Subscription/Subscribe.aspx?" + WebConstants.URL_PARAMETER_PURCHASEREASONTYPEID + "=" + PurchaseReasonType.HideRightAdProfilePage.ToString("d") + "&DestinationURL=" + HttpUtility.UrlEncode(Request.RawUrl), false));

            litAdMessage.Text = subscribeHook;
        }

        public static string GetGAMIFrameSource(string pGAMAdSlot)
        {
            return "/framework/ui/advertising/gamiframe.aspx?LayoutTemplateID=" + LayoutTemplate.IFrame.ToString("d") + "&GAMAdSlot=" + pGAMAdSlot;
        }
        #endregion

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);

        }
        #endregion
    }
}
