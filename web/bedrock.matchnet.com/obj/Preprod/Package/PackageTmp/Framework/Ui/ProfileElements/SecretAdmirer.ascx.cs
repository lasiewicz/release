﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.List.ValueObjects;
using Matchnet.Member.ValueObjects.Interfaces;
using Matchnet.Web.Framework.Ui.BasicElements;

namespace Matchnet.Web.Framework.Ui.ProfileElements
{
    public partial class SecretAdmirer : FrameworkControl
    {
        private static bool LoadedSharedJS = false;
        protected IMemberDTO Member;

        //For JS only
        protected string _EncodedParams = "";

        #region Properties
        public string ExternalYNMIndicatorID { get; set; }
        public string ExternalYYIndicatorID { get; set; }
        public string IconHasYNMCSS { get; set; }
        public string IconNoYNMCSS { get; set; }
        public string LinkResourceConstant { get; set; }
        public string LinkTitleResourceConstant { get; set; }
        public bool HideLinkText { get; set; }
        public bool HideLinkIcon { get; set; }
        public bool HideYNMInPopup { get; set; }

        #endregion

        #region Event Handlers
        protected override void  OnInit(EventArgs e)
        {
            if (String.IsNullOrEmpty(ExternalYNMIndicatorID)) { ExternalYNMIndicatorID = ""; }
            if (String.IsNullOrEmpty(IconHasYNMCSS)) { IconHasYNMCSS = "spr s-icon-click-y-on"; }
            if (String.IsNullOrEmpty(IconNoYNMCSS)) { IconNoYNMCSS = "spr s-icon-click-y-off"; }
 	        base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected override void OnPreRender(EventArgs e)
        {
            //register shared JS Data
            if (!LoadedSharedJS)
            {
                LoadedSharedJS = true;
                Page.ClientScript.RegisterClientScriptBlock(typeof(System.Web.UI.Page), "SecretAdmirerSharedParams", "<script type=\"text/javascript\">" +
                "SecretAdmirer_SharedYNMParams.externalYAlt = '" + g.GetResource("YNM_Y", this) + "';" +
                "SecretAdmirer_SharedYNMParams.externalMAlt = '" + g.GetResource("YNM_M", this) + "';" +
                "SecretAdmirer_SharedYNMParams.externalNAlt = '" + g.GetResource("YNM_N", this) + "';" +
                "SecretAdmirer_SharedYNMParams.externalYYAlt = '" + g.GetResource("YNM_YY", this) + "';" +
                "</script>");
            }

            base.OnPreRender(e);
        }
        #endregion

        #region Public Methods
        public void LoadSecretAdmirer(IMemberDTO member)
        {
            this.Member = member;

            if (g.IsYNMEnabled)
            {
                spanSecretAdmirer.Visible = !HideLinkIcon;
                txtSeccretAdmirer.Visible = !HideLinkText;
                phYNMInline.Visible = HideYNMInPopup;
                phYNMInPopup.Visible = !HideYNMInPopup;

                if (!String.IsNullOrEmpty(LinkResourceConstant))
                    txtSeccretAdmirer.ResourceConstant = LinkResourceConstant;
                else
                    txtSeccretAdmirer.Text = g.GetResource("PRO_SECRETADMIRER", this);

                if (!String.IsNullOrEmpty(LinkTitleResourceConstant))
                    lnkSecretAdmirer.ToolTip = g.GetResource(LinkTitleResourceConstant, this);
                else
                    lnkSecretAdmirer.ToolTip = g.GetResource("ALT_SECRETADMIRER", this);

                try
                {
                    ClickMask clickMask = g.List.GetClickMask(g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, this.Member.MemberID);

                    //Put the clickMask in a hidden input for use by JavaScript (YNMVote)
                    YNMVoteStatus.Value = ((int)clickMask).ToString();

                    bool hasYMM = true;
                    if ((((clickMask & ClickMask.TargetMemberYes) == ClickMask.TargetMemberYes) && ((clickMask & ClickMask.MemberYes) == ClickMask.MemberYes)))
                    {
                        secretAdmirerPopup_descYY.Style["display"] = "block";
                        spanYesInPopup.Attributes["class"] = "spr s-icon-click-y-on";
                        spanYesInline.Attributes["class"] = "spr s-icon-click-y-on";
                    }
                    else if ((clickMask & ClickMask.MemberYes) == ClickMask.MemberYes)
                    {
                        secretAdmirerPopup_descY.Style["display"] = "block";
                        spanYesInPopup.Attributes["class"] = "spr s-icon-click-y-on";
                        spanYesInline.Attributes["class"] = "spr s-icon-click-y-on";
                    }
                    else if ((clickMask & ClickMask.MemberNo) == ClickMask.MemberNo)
                    {
                        secretAdmirerPopup_descN.Style["display"] = "block";
                        spanNoInPopup.Attributes["class"] = "spr s-icon-click-n-on";
                        spanNoInline.Attributes["class"] = "spr s-icon-click-n-on";
                    }
                    else if ((clickMask & ClickMask.MemberMaybe) == ClickMask.MemberMaybe)
                    {
                        secretAdmirerPopup_descM.Style["display"] = "block";
                        spanMaybeInPopup.Attributes["class"] = "spr s-icon-click-m-on";
                        spanMaybeInline.Attributes["class"] = "spr s-icon-click-m-on";
                    }
                    else
                    {
                        secretAdmirerPopup_desc.Style["display"] = "block";
                        hasYMM = false;
                    }

                    if (hasYMM)
                    {
                        if (String.IsNullOrEmpty(IconHasYNMCSS))
                            spanSecretAdmirer.Attributes.Add("class", "spr s-icon-click-y-on");
                        else
                            spanSecretAdmirer.Attributes.Add("class", IconHasYNMCSS);
                    }
                    else
                    {
                        if (String.IsNullOrEmpty(IconNoYNMCSS))
                            spanSecretAdmirer.Attributes.Add("class", "spr s-icon-click-y-off");
                        else
                            spanSecretAdmirer.Attributes.Add("class", IconNoYNMCSS);
                    }

                    YNMVoteParameters parameters = new YNMVoteParameters();

                    parameters.SiteID = _g.Brand.Site.SiteID;
                    parameters.CommunityID = _g.Brand.Site.Community.CommunityID;
                    parameters.FromMemberID = _g.Member.MemberID;
                    parameters.ToMemberID = this.Member.MemberID;
                    _EncodedParams = parameters.EncodeParams();

                    //Yes Vote button
                    lnkYesInPopup.ToolTip = g.GetResource("YNM_Y_IMG_ALT_TEXT", this);
                    lnkYesInline.ToolTip = g.GetResource("YNM_Y_IMG_ALT_TEXT", this);

                    //No Vote button
                    lnkNoInPopup.ToolTip = g.GetResource("YNM_N_IMG_ALT_TEXT", this);
                    lnkNoInline.ToolTip = g.GetResource("YNM_N_IMG_ALT_TEXT", this);

                    //Maybe Vote button
                    lnkMaybeInPopup.ToolTip = g.GetResource("YNM_M_IMG_ALT_TEXT", this);
                    lnkMaybeInline.ToolTip = g.GetResource("YNM_M_IMG_ALT_TEXT", this);

                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine(ex.Message);

                    //error here is likely in GetClickMask, because there is no member (e.g. Visitor)
                    //this.divMutualYes.Style.Add("display", "none");
                    YNMVoteStatus.Value = Convert.ToInt32(ClickMask.None).ToString();
                }


            }
            else
            {
                this.Visible = false;
            }
        }

        #endregion

        #region Private Methods

        #endregion
    }
}