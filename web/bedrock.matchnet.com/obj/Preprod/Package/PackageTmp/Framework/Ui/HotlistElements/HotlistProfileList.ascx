﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HotlistProfileList.ascx.cs" Inherits="Matchnet.Web.Framework.Ui.HotlistElements.HotlistProfileList" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mnl" Namespace="Matchnet.Web.Framework.Ui.BasicElements.Links"
    Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="uc2" TagName="Sprite" Src="../PageElements/Sprite.ascx" %>
<%@ Register TagPrefix="uc2" TagName="LastTime" Src="../BasicElements/LastTime.ascx" %>
<%@ Register TagPrefix="uc1" TagName="NoPhoto" Src="../PageElements/NoPhoto20.ascx" %>

<div class="viewed-your-profile clearfix grad-base">
    <h2 class="component-header"><mn:Txt ID="txtTitle" ResourceConstant="TXT_TITLE_VIEWED_YOUR_PROFILE" runat="server" /></h2>
    <asp:PlaceHolder ID="phHasProfiles" runat="server">
    <asp:Repeater ID="rpProfiles" runat="server" OnItemDataBound="rpProfiles_ItemDataBound">
        <ItemTemplate>
        <div class="mini-profile list-view-30">
            <div class="profile-wrapper">
                <div class="profile-body clearfix">
                    <ul class="indicator">
                        <li class="item status">
                            <%--online/offline icon goes here--%>
                            <asp:PlaceHolder ID="phOnline" runat="server" Visible="false">
                                <div class="online">
                                    <asp:HyperLink runat="server" ID="lnkOnline">
                                        <uc2:Sprite ID="sprOnline" TitleResourceConstant="ALT_IM_ONLINE" TextResourceConstant="ALT_IM_ONLINE"
                                            SpriteClass="spr s-icon-status-online-sm" runat="server" />
                                    </asp:HyperLink>
                                </div>
                            </asp:PlaceHolder>
                            <asp:PlaceHolder ID="phOffline" runat="server" Visible="false">
                                <div class="offline">
                                    <uc2:Sprite ID="sprOffline" TextResourceConstant="ALT_IM_OFFLINE" SpriteClass="spr s-icon-status-offline-sm"
                                        runat="server" />
                                    <uc2:LastTime ID="lastTimeLogon" runat="server" Visible="false" />
                                </div>
                            </asp:PlaceHolder>
                        </li>
                    </ul>
 
                    <%--picture--%>
                    <div class="picture">
                        <%--New/Updated - '.s-icon-word-new/.s-icon-word-update--%>
                        <asp:PlaceHolder ID="phIsNewProfile" runat="server" Visible="false">
                            <div class="ribbon small"><span class="spr s-icon-word-new"><span>New</span></span></div>
                        </asp:PlaceHolder>
                        <asp:PlaceHolder ID="phIsUpdatedProfile" runat="server" Visible="false">
                            <div class="ribbon small"><span class="spr s-icon-word-updated"><span>Updated</span></span></div>
                        </asp:PlaceHolder>
                        <mn:Image runat="server" ID="imgThumb" Width="70" Height="90" Border="0" ResourceConstant="" />
                        <uc1:NoPhoto runat="server" ID="noPhoto" DivCSSClass="ask" Mode="NoPhoto" Visible="False" />
                    </div>
                    <div class="info">
                        <h4><asp:HyperLink ID="lnkUserName" runat="server" /></h4>
                        <ul class="details">
                            <li>
                                <asp:Label ID="txtAge" runat="server" />
                                -
                                <asp:Label ID="txtLocation" runat="server" />
                            </li>
                            <li><asp:Literal ID="literalMaritalAndGender" runat="server"></asp:Literal></li>
                            <li>
                                <mn:Txt ID="txtHotlistDate" ResourceConstant="TXT_VIEWED_ON" runat="server" />
                                <uc2:LastTime ID="lastTimeHotlistDate" runat="server" />
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        </ItemTemplate>
    </asp:Repeater>
    <asp:HyperLink ID="lnkViewMore" runat="server" CssClass="more">
        <mn:Txt ID="txtViewMore" ResourceConstant="TXT_VIEW_MORE" runat="server" />
    </asp:HyperLink>
    </asp:PlaceHolder>
    <asp:PlaceHolder ID="phNoProfiles" runat="server" Visible="false">
        <div class="no-profile">
            <p><mn:Txt ID="txtNoProfile" ResourceConstant="TXT_NO_PROFILE_VIEWED_YOUR_PROFILE" runat="server" /></p>
        </div>
    </asp:PlaceHolder>
    <asp:PlaceHolder ID="phNonSubViewForSubscriberOnlyLists" Visible="false" runat="server">
        <div class="clear-both text-center">
            <mn2:FrameworkLiteral ID="litViewedYourProfileForSubscribersOnly" runat="server" ResourceConstant="TXT_VIEWED_YOUR_PROFILE_FOR_SUBSCRIBERS_ONLY_DEFAULT_COPY"></mn2:FrameworkLiteral>
        </div>
    </asp:PlaceHolder>
</div>