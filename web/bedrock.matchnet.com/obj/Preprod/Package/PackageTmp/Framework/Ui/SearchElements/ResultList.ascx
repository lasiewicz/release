﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="ResultList.ascx.cs" Inherits="Matchnet.Web.Framework.Ui.SearchElements.ResultList" %>
<%@ Register TagPrefix="mn" TagName="MiniProfile" Src="../ProfileElements/MiniProfile.ascx" %>
<%@ Register TagPrefix="mn" TagName="MiniProfileListView" Src="../ProfileElements/MiniProfileListView.ascx" %>
<%@ Register TagPrefix="mn" TagName="noResults" Src="../BasicElements/NoResults.ascx" %>

<input id="hidStartRow" type="hidden" name="hidStartRow" runat="server" />
<asp:repeater id="memberGalleryRepeater" runat="server" Visible="True" OnItemDataBound="MemberRepeater_OnItemDataBound">
	<itemtemplate>		 
		<mn:miniprofile runat="server" id="miniProfile" EnableProfileLinks="true" Visible="false" LoadControlAutomatically="false"/>
	</itemtemplate>
</asp:repeater>

<asp:repeater id="memberListRepeater" runat="server" Visible="True" OnItemDataBound="MemberRepeater_OnItemDataBound">
	<itemtemplate>		 
        <mn:MiniProfileListView runat="server" id="MiniProfileListView1" EnableProfileLinks="true" Visible="false" LoadControlAutomatically="false"/>
	</itemtemplate>
</asp:repeater>

<mn:noResults runat="server" id="noSearchResults" visible="False" />
