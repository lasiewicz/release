using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Web.Framework.Managers;

namespace Matchnet.Web.Framework.Ui.PageElements
{
    using System;
    using System.Data;
    using System.Drawing;
    using System.Web;
    using System.Web.UI.WebControls;
    using System.Web.UI.HtmlControls;

    using Matchnet.Web.Framework;

    /// <summary>
    ///		Summary description for NoPhoto.
    /// </summary>
    public class NoPhoto : FrameworkControl
    {
        public enum PhotoMode { UploadPhotoNow, NoPhoto, NoPhotoNoText, OnlyMembers, NoPrivatePhoto };

        private bool _enableLink = true;
        private int _memberID;
        private string _destUrl;
        private PhotoMode _Mode = PhotoMode.NoPhoto;

        protected Matchnet.Web.Framework.Txt txtAskForPhoto;
        protected System.Web.UI.HtmlControls.HtmlAnchor lnkNoPhoto;
        protected System.Web.UI.HtmlControls.HtmlTable tblNoPhoto;

        public bool EnableLink
        {
            get { return _enableLink; }
            set { _enableLink = value; }
        }
        public bool IsMatchMeterMatchesPage { get; set; } // Jmeter patch QAT-872 QAT-871

        protected override void OnPreRender(EventArgs e)
        {
            if (this.Visible)
            {
                //MemberPhotoHelper photoHelper = new MemberPhotoHelper(RuntimeSettings.Instance);
                bool isMale = FrameworkGlobals.IsMaleGender(MemberID, g);
                string photoFileName = string.Empty;

                switch (_Mode)
                {
                    case PhotoMode.UploadPhotoNow:
                        txtAskForPhoto.ResourceConstant = "TXT_UPLOAD_A_PHOTO_NOW";
                        photoFileName = MemberPhotoDisplayManager.Instance.GetNoPhotoFile(NoPhotoImageType.BackgroundThumb, isMale);
                        break;

                    case PhotoMode.NoPhoto:
                        txtAskForPhoto.ResourceConstant = "TXT_ASK_ME_FOR_MY_PHOTO";
                        photoFileName = MemberPhotoDisplayManager.Instance.GetNoPhotoFile(NoPhotoImageType.BackgroundThumb, isMale);
                        break;

                    case PhotoMode.NoPhotoNoText:
                        txtAskForPhoto.Visible = false;
                        photoFileName = MemberPhotoDisplayManager.Instance.GetNoPhotoFile(NoPhotoImageType.BackgroundThumb, isMale);
                        break;

                    case PhotoMode.OnlyMembers:
                        txtAskForPhoto.ResourceConstant = "TXT_ONLY_MEMBERS_CAN_SEE_MY_PHOTOS";
                        photoFileName = MemberPhotoDisplayManager.Instance.GetNoPhotoFile(NoPhotoImageType.BackgroundThumb, isMale);
                        break;

                    case PhotoMode.NoPrivatePhoto:
                        txtAskForPhoto.ResourceConstant = "TXT_CUPID_PRIVATE_PHOTO";
                        photoFileName = MemberPhotoDisplayManager.Instance.GetNoPhotoFile(NoPhotoImageType.BackgroundThumb, isMale);
                        break;
                }

                tblNoPhoto.Attributes.Add("background", photoFileName);
            }

            if (EnableLink == true)
            {
                if (!string.IsNullOrEmpty(DestURL) && !lnkNoPhoto.Disabled)
                {
                    lnkNoPhoto.HRef = DestURL;
                    if (IsMatchMeterMatchesPage)
                    {
                        tblNoPhoto.Attributes.Add("onclick", DestURL);
                    }
                    else
                    {
                        tblNoPhoto.Attributes.Add("onclick", "matchnetYNM4_checkWindowOpener('" + lnkNoPhoto.ClientID + "');");
                    }
                }
                else
                {
                    tblNoPhoto.Attributes.Add("style", "cursor: default");
                }
            }

            base.OnPreRender(e);
        }

        public PhotoMode Mode
        {
            get { return _Mode; }
            set { _Mode = value; }
        }

        public int MemberID
        {
            get { return (_memberID); }
            set { _memberID = value; }
        }

        public void AddAttribute(string Attribute, string AttributeValue)
        {
            lnkNoPhoto.Attributes.Add(Attribute, AttributeValue);
        }

        public string DestURL
        {
            get { return (_destUrl); }
            set
            {
                _destUrl = value;
                lnkNoPhoto.HRef = _destUrl;
                //if( value == String.Empty )
                //	lnkNoPhoto.Disabled = true;
            }
        }

        public void disableLink()
        {
            //TT 18639. Need to "disable" link for FireFox UI compatibility (so underline and outline don't appear onmouseover)
            // Must cause it not to render an underline at all.  Only way I found was to remove the hyperlink and add the table back.
            // HACK alert. This control is a mess. But what else is new.
            this.Controls.Remove(lnkNoPhoto);
            this.Controls.Add(tblNoPhoto);

            //The following Hacks did not produce desired results.
            //lnkNoPhoto.Style.Add("text-decoration","none");  //this made the underline go away, but not the table border
            //tblNoPhoto.Style.Add("border-width","0px"); //this plain just didn't work, cuz it got overridden by class perhaps?
            //lnkNoPhoto.Disabled = true; // this causes text to be grayed out in IE, so unless that's desired, don't use it.
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            //this.lnkNoPhoto.ServerClick += new EventHandler(lnkNoPhoto_ServerClick);
        }
        #endregion

        /*private void lnkNoPhoto_ServerClick(object sender, EventArgs e)
		{
			switch (this.Mode)
			{
				case PhotoMode.OnlyMembers:
					g.LogonRedirect(this.DestURL);
					break;
				case PhotoMode.NoPhoto:
					g.Transfer(this.DestURL);
					break;
			}
		}*/
    }
}
