﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MicroProfile20.ascx.cs"
    Inherits="Matchnet.Web.Framework.Ui.BasicElements.MicroProfile20" %>
<%@ Register TagPrefix="uc1" TagName="NoPhoto" Src="../PageElements/NoPhoto20.ascx" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mnl" Namespace="Matchnet.Web.Framework.Ui.BasicElements.Links"
    Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mnbe" Namespace="Matchnet.Web.Framework.Ui.BasicElements"
    Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn" TagName="YNMVoteBarSmall" Src="../YNMVoteBarSmall.ascx" %>
<%@ Register Src="LastTime.ascx" TagName="LastTime" TagPrefix="uc2" %>
<%@ Register TagPrefix="uc2" TagName="HighlightProfileInfoDisplay" Src="HighlightInfoDisplay20.ascx" %>
<asp:Panel ID="panelMicroProfile20" runat="server" CssClass="highlight-profile micro-profile clearfix">
    <!--MicroProfile20-->
  
    <div class="member-pic">
        <mn:Image runat="server" ID="imgThumb" class="profileImageHover" Border="0" ResourceConstant="ALT_PROFILE_PICTURE"
            TitleResourceConstant="TTL_VIEW_MY_PROFILE" />
        <uc1:NoPhoto runat="server" ID="noPhoto" class="no-photo" Visible="False" />
        <asp:PlaceHolder ID="phColorCode" runat="server" Visible="false">
            <div class="cc-pic-tag cc-pic-tag-sm cc-pic-<%=_ColorText.ToLower() %>">
                <span>
                    <%=_ColorText %></span></div>
        </asp:PlaceHolder>
    </div>
    <div class="member-info">
        <!--mutual yes-->
        <asp:Panel ID="panelYY" runat="server" Visible="false">
            <mn:Image ID="imgYY" runat="server" Border="0" Hspace="2" FileName="icon-click-yy.gif"
                AlternateText="" />
        </asp:Panel>
        <!--username-->
        <asp:HyperLink runat="server" ID="lnkUserName1" />
        <!--highlight icon-->
        <asp:PlaceHolder ID="phHighlightedInfoLink" runat="server" Visible="false">
            <!--highlight info display (also renders icon)-->
            <uc2:HighlightProfileInfoDisplay runat="server" ID="ucHighlightProfileInfoDisplay"
                ShowImageNoText="true" />
        </asp:PlaceHolder>
        <!--other basic info-->
        <p>
            <asp:Literal ID="literalAge" runat="server"></asp:Literal>&nbsp;
            <%--<mn2:FrameworkLiteral ID="literalAgeDisplay" ResourceConstant="TXT_YEARS_OLD" runat="server"></mn2:FrameworkLiteral>--%>
            <br />
            <asp:Literal ID="literalLocation" runat="server"></asp:Literal>
        </p>
        <asp:Panel ID="panelCommunication" runat="server" CssClass="ymn-wrapper">
            <!--email-->
            <asp:PlaceHolder ID="phCommunication" runat="server" >
            <asp:HyperLink ID="lnkEmail" runat="server">
                <mn:Image ID="imgEmail" runat="server" FileName="btn-email-sm.png" ResourceConstant="ALT_EMAIL"
                    TitleResourceConstant="ALT_EMAIL" />
            </asp:HyperLink>
           
            <asp:PlaceHolder runat="server" ID="plcClick">
                <!--ymn-->
                <span class="ymn-box">
                    <mn:YNMVoteBarSmall ID="VoteBar" runat="server" />
                </span></asp:PlaceHolder>
          
            </asp:PlaceHolder>
        </asp:Panel>
    </div>
    <asp:PlaceHolder ID="phHotlistAction" runat="server" Visible="false">
        <div class="communicated-info">
            <asp:HyperLink ID="lnkHotlistAction" runat="server">
                <mn2:FrameworkLiteral ID="literalHotlistAction" runat="server"></mn2:FrameworkLiteral>
            </asp:HyperLink>
            <span class="dim-text">
                <uc2:LastTime ID="lastTimeAction" runat="server" />
            </span>
        </div>
    </asp:PlaceHolder>
</asp:Panel>
