<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Control Language="c#" AutoEventWireup="false" Codebehind="MaskControl20.ascx.cs"
Inherits="Matchnet.Web.Framework.Ui.FormElements.MaskControl20" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>

<div class="search-pref" id="preferenceControl" runat="server"><!-- begin maskControl upper div -->
	<asp:PlaceHolder id="plcBasicPreference" Runat="server">
		<div class="search-pref-header <%if (IsCollapsed){%>search-pref-closed<%}else{ %>search-pref-open<%} %>">
            <span class="spr <%if (IsCollapsed){%>s-icon-arrow-right<%}else{ %>s-icon-arrow-down<%} %>"></span>
            <a href="#" class="title"><mn:txt id="txtPreferenceName" Runat="server" /></a>
			<span id="divSelectedItems" class="search-pref-selected-option" runat="server">
				<asp:Literal id="litSelectedItems" Runat="server" />
			</span>
            <%if (IsSliderWeightEnabled)
              { %>
            <em class="value-wrapper importance"><span class="value"></span></em>
            <%} %>
		</div>
	</asp:PlaceHolder>

	<asp:PlaceHolder id="plcAdditionalPreference" Runat="server">
		<div class="prefSearchPrefHeader">
			<mn:txt id="txtAdditionalPreferenceName" Runat="server" />
		</div>
	</asp:PlaceHolder>
		
	<asp:Repeater id="rptOptions" Runat="server">
		<HeaderTemplate>
			<div <% if (Collapsible && IsCollapsed){ %> style="display:none;"<% }%> class="search-pref-container">
                 <%if (IsSliderWeightEnabled)
                   { %>
                <div class="importance clearfix">
                    <h4 class="title">Importance</h4>
                    <div class="slider"></div>
                    <em class="value-wrapper"><span class="value"></span></em>
                </div>
                <%} %>
                <ul class="search-pref-option-container">
		</HeaderTemplate>
			    <ItemTemplate>
				    <li><asp:CheckBox id="cbOption" Runat="server" /></li>
			    </ItemTemplate>
		<FooterTemplate>
				</ul>
            </div>
		</FooterTemplate>
	</asp:Repeater>
		
    <asp:PlaceHolder ID="PlaceHolderJdateReligion" runat="server" Visible="false">
		
		<script type="text/javascript">

		    function CheckBoxListSelect(caller, control, selectedItemsTextControlID, text) {

		        var state = (caller.checked) ? "true" : "";

		        if (caller.checked) {
		            UpdateSelectedItemsText(selectedItemsTextControlID, text);
		        }
		        
		        var checkBoxes = document.getElementsByTagName("input");
		        for (var i = 0; i < checkBoxes.length; i++) {
		            var name = checkBoxes[i].getAttribute('name');
		            if (name.indexOf(control) != -1)
		                checkBoxes[i].checked = state;

		            if ((name.indexOf('RepeaterNonJdateReligion') != -1) &&
		                (name.indexOf('CheckBoxGroupNonJdateReligion') == -1)) {
		                if (state == "true")
		                    checkBoxes[i].checked = "";
		            }
		            
		            if (name.indexOf('CheckBoxGroupNonJdateReligion') != -1)
		            {
		                if (state == "true")
		                    checkBoxes[i].checked = "";
		            }
		        }
		    }

		    function ToggleGroupCheckBox(caller, groupCheckBoxID) {

		        var state = (caller.checked) ? "true" : "";
		        var groupCheckBox = document.getElementById(groupCheckBoxID)
		        if (state == "") {
		            groupCheckBox.checked = state;
		        }
		    }

		    function UpdateSelectedItemsText(controlID, text) {
		        document.getElementById(controlID).innerHTML = text;
		    }
        </script>

		<div <% if (Collapsible){ if (IsCollapsed){%> style="display:none; z-index:2;" class="search-pref-container clearfix" <%}else{%>style="z-index:2;" class="search-pref-container clearfix" <%}}%>>
             <%if (IsSliderWeightEnabled)
               { %>
            <div class="importance clearfix">
                <h4 class="title">Importance</h4>
                <div class="slider"></div>
                <em class="value-wrapper"><span class="value"></span></em>
            </div>
            <%} %>

		    <ul class="check-lists search-pref-option-container clearfix">
		        <asp:Repeater id="RepeaterJdateReligion" Runat="server">
			        <HeaderTemplate>
			            <li class="check-list-02-col">
			            <asp:CheckBox id="CheckBoxGroupJdateReligion" runat="server" />
			            <strong><mn:Txt ResourceConstant="TXT_JEWISH" runat="server" id="txtJewish" /></strong>
				         <ul class="clearfix">
			        </HeaderTemplate>
			        <ItemTemplate>
				        <li><asp:CheckBox id="cbOption" Runat="server" /></li>
			        </ItemTemplate>
			        <FooterTemplate>
				        </ul>
				        </li>
			        </FooterTemplate>
		        </asp:Repeater>
                <asp:Repeater id="RepeaterNonJdateReligion" Runat="server">
			        <HeaderTemplate>
			            <li class="check-list-01-col">
			            <asp:CheckBox id="CheckBoxGroupNonJdateReligion" runat="server" />
			            <strong class="not-strong"><mn:Txt ID="Txt1" ResourceConstant="TXT_ANY_RELIGION" runat="server" /></strong>
				         <ul class="clearfix">
			        </HeaderTemplate>
			        <ItemTemplate>
				        <li><asp:CheckBox id="cbOption" Runat="server" /></li>
			        </ItemTemplate>
			        <FooterTemplate>
				        </ul>
				        </li>
			        </FooterTemplate>
		        </asp:Repeater>
		    </ul>
		</div>
    </asp:PlaceHolder>

    <asp:HiddenField ID="sliderValue" runat="server" />

    <script type="text/javascript">
        if (<%=IsSliderWeightEnabled.ToString().ToLower()%>){
            preferenceSliderControl($j('#<%=preferenceControl.ClientID %>'), '<%=sliderValue.ClientID %>', <%=SearchWeightValue.ToString()%>);
        }
        // preferenceToggler($clicker, $container, arrowOpenClassName, arrowCloseClassName)
        preferenceToggler($j('#<%=preferenceControl.ClientID %> .search-pref-header'), $j('#<%=preferenceControl.ClientID %> .search-pref-container'), 'search-pref-open', 'search-pref-closed', 'spr spr s-icon-arrow-right', 'spr spr s-icon-arrow-down');
    </script>
</div><!-- end maskControl upper div -->
