﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ContactHistory.ascx.cs" Inherits="Matchnet.Web.Framework.Ui.ProfileElements.ContactHistory" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>

<a id="lnkContactHistory" href="#" runat="server" tabindex="10" class="menu">
    <span id="spanIcon" runat="server"><span></span></span>
    <span class="title" ID="spanLinkDesc" runat="server">
        <mn2:FrameworkLiteral ID="literalContactHistoryLink" runat="server"></mn2:FrameworkLiteral>
    </span>
</a>
<div id="divContactHistory" runat="server" style="display:none;" class="history-list items">
    <h2 class="title header" ID="spanHeaderDesc" runat="server">
        <mn2:FrameworkLiteral ID="literalContactHistoryHeader" runat="server"></mn2:FrameworkLiteral>
    </h2>
    
    <asp:PlaceHolder ID="phContactHistoryUITemplateContent" runat="server" Visible="false">
        <%--UI Templated generated html--%>
    </asp:PlaceHolder>
    
    <asp:PlaceHolder ID="phContactHistoryContent" runat="server" Visible="false">
        <%--Pre-generated html--%>
        <asp:Repeater ID="repeaterContactHistoryMatched" runat="server" OnItemDataBound="repeaterContactHistoryMatched_ItemDataBound">
            <HeaderTemplate><ul class="default"></HeaderTemplate>
            <ItemTemplate>
                <li>
                    <asp:Literal ID="literalMatchedItemText" runat="server"></asp:Literal>
                </li>
            </ItemTemplate>
            <FooterTemplate></ul></FooterTemplate>
        </asp:Repeater> 
        <asp:Repeater ID="repeaterContactHistoryDetailed" runat="server" OnItemDataBound="repeaterContactHistoryDetailed_ItemDataBound">
            <HeaderTemplate>
                <ul class="details">
            </HeaderTemplate>
            <ItemTemplate>
                <li>
                    <asp:Literal ID="literalContactHistoryItemText" runat="server"></asp:Literal>
                    <span class="timestamp"><asp:Literal ID="literalContactHistoryItemTimestampText" runat="server"></asp:Literal></span>
                </li>
            </ItemTemplate>
            <FooterTemplate></ul></FooterTemplate>
        </asp:Repeater> 
    </asp:PlaceHolder>
    
    <asp:PlaceHolder ID="phContactHistoryEmptyContent" runat="server" Visible="false">
        <%--No contact history--%>
        <p class="margin-light"><mn2:FrameworkLiteral ID="literalEmptyContactHistory" runat="server" ResourceConstant="TXT_EMPTY_CONTACTHISTORY"></mn2:FrameworkLiteral></p>
    </asp:PlaceHolder>
</div>

<script type="text/javascript">

    MenuToggler($j('#<%=lnkContactHistory.ClientID %>'), $j('#<%=divContactHistory.ClientID %>'));
    
</script>