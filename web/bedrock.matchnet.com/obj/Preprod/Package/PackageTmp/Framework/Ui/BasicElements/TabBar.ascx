﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TabBar.ascx.cs" Inherits="Matchnet.Web.Framework.Ui.BasicElements.TabBar" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>

<ul class="nav-rounded-tabs no-click clearfix">
<asp:Repeater ID="rpTabBar" runat="server" OnItemDataBound="rptTabBar_OnItemDataBound" >
   <ItemTemplate>
        <li class="tab" runat="server" id="liTab"><mn:Txt id="txtTabBarCategory" runat="server" /></li>
   </ItemTemplate>
</asp:Repeater>    
</ul>