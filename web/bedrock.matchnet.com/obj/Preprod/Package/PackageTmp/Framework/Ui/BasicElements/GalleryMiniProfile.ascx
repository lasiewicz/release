<%@ Register TagPrefix="mn" TagName="YNMVoteBarSmall" Src="../YNMVoteBarSmall.ascx" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mnl" Namespace="Matchnet.Web.Framework.Ui.BasicElements.Links"
    Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="uc1" TagName="NoPhoto" Src="../PageElements/NoPhoto.ascx" %>
<%@ Control Language="c#" AutoEventWireup="false" CodeBehind="GalleryMiniProfile.ascx.cs"
    Inherits="Matchnet.Web.Framework.Ui.BasicElements.GalleryMiniProfile" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<%@ Register TagPrefix="uc1" TagName="Add2List" Src="Add2List.ascx" %>
<%@ Register TagPrefix="uc2" TagName="HighlightProfileInfoDisplay" Src="HighlightProfileInfoDisplay.ascx" %>
<%@ Register TagPrefix="uc2" TagName="MatchMeterInfoDisplay" Src="MatchMeterInfoDisplay.ascx" %>
<asp:PlaceHolder runat="server" ID="plcHotListMove" Visible="False">
    <div class="profileButtonAlign2">
        <div class="showSelectedDiv" id="hotlistShow<%= _counter.ToString() %>" style="margin-bottom: 5px;
            width: 188px">
            <input id="chkSelected" type="checkbox" name="chkSelected" runat="server"><span class="showSelect"
                id="hotlistSelectText<%= _counter.ToString() %>">&nbsp;
                <mn:Txt ID="TxtSelectThisProfile" runat="server" ResourceConstant="TXT_SELECT_THIS_PROFILE">
                </mn:Txt>
            </span><span class="hideSelect" id="hotlistDeselectText<%= _counter.ToString() %>">
                <mn:Txt ID="TxtDeselectThisProfile" runat="server" ResourceConstant="TXT_DESELECT_THIS_PROFILE">
                </mn:Txt>
            </span>
        </div>
    </div>
</asp:PlaceHolder>
<div class="boxGallery" id="boxContainerGallery" runat="server">
    <table id="boxContentGallery" class="boxGallery" border="0" cellpadding="0" cellspacing="0"
        width="192">
        <tr>
            <td colspan="2" id="cProfileTop" runat="server" class="gp1" valign="middle">
                <span class="miniProfileTitle">&nbsp;<mnl:Link runat="server" ID="lnkUserName" TitleResourceConstant="TTL_VIEW_MY_PROFILE" /></span>
                <mn:Image ID="imgIsNew" runat="server" FileName="icon_newMember.gif" Visible="false"
                    Hspace="2" CssClass="imageAlignInline" TitleResourceConstant="ALT_NEW_MEMBER" ResourceConstant="ALT_NEW_MEMBER"/>
                <mn:Image ID="imgIsUpdate" runat="server" FileName="icon-updated.gif" Visible="false"
                    Hspace="2" CssClass="imageAlignInline"/>
                <mn:Image ID="imgPremiumAuthenticated" runat="server" FileName="icon_premiumAuthenticated.gif"
                    Visible="false" TitleResourceConstant="ALT_PREMIUM_AUTHENTICATED" ResourceConstant="ALT_PREMIUM_AUTHENTICATED"
                    CssClass="imageAlignInline" />
                <uc2:HighlightProfileInfoDisplay runat="server" ID="ucHighlightProfileInfoDisplay"
                    ShowImageNoText="true" />
            </td>
        </tr>
        <tr>
            <td class="gp2" valign="top">
                <mn:Image runat="server" ID="imgThumb" Width="80" Height="104" BorderWidth="1" CssClass="profileImageHoverGallery" />
                <uc1:NoPhoto runat="server" ID="noPhoto" class="noPhoto" Visible="False" />
            </td>
            <td class="gp3" valign="top" width="100%">
                <span class="gpYNM">
                    <mn:YNMVoteBarSmall ID="VoteBar" runat="server" />
                    <asp:PlaceHolder runat="server" ID="plcEmailMe" Visible="false">
                        <asp:HyperLink runat="server" ID="lnkEmailMeTop">
                            <mn:Image ID="ImageEmailMe" runat="server" FileName="icon_email.gif" align="absMiddle"
                                Hspace="3" Vspace="0" ResourceConstant="ALT_EMAIL_ME" />
                            <mn:Txt runat="server" ID="txtEmail" ResourceConstant="PRO_EMAIL" />
                        </asp:HyperLink></asp:PlaceHolder>
                </span>
                <div class="gpInfo">
                    <asp:Literal ID="txtAge" runat="server" /><br />
                    <asp:Literal ID="txtRegion" runat="server" /><br />
                    <span class="galleryLink">
                        <mn:Txt runat="server" ID="txtMore" ResourceConstant="VIEW_MEMBER" />
                    </span>
                </div>
            </td>
        </tr>
        <tr>
            <td width="100%" colspan="2" id="cProfileBottom" runat="server" class="gpbottom"
                style="padding: 0px;">
                <table width="100%" cellpadding="3" cellspacing="0" border="0" style="whitespace: nowrap;">
                    <tr>
                        <td class="gp4 alignLTR alignCTR" width="100%">
                            <mn:Image align="absmiddle" ID="imgOnline" CssClass="imgOnline" runat="server" FileName="icon_chat.gif"
                                Visible="true" ResourceConstant="ONLINE" />
                            &nbsp;
                              <uc1:Add2List ID="add2List" cssClass="hebListMenu" runat="server" />
                            &nbsp;
                            <mn:Image align="absmiddle" Border="0" FileName="smileIcon.gif" Hspace="3" Vspace="0"
                                ResourceConstant="ALT_SMILE" runat="server" ID="imgFlirt" CssClass="imgFlirt" Visible="false" />
                            <asp:HyperLink runat="server" ID="lnkEmail" CssClass="lnkEmail">
                                <mn:Image align="absmiddle" Border="0" FileName="icon-profile-email.gif" Hspace="3" Vspace="0"
                                    ResourceConstant="ALT_EMAIL_ME" runat="server" ID="imgEmail" />
                            </asp:HyperLink>&nbsp;                            
                            <asp:HyperLink runat="server" ID="lnkECard" CssClass="lnkECard">
                                <mn:Image align="absmiddle" Border="0" FileName="icon-ecard.gif" Hspace="3" Vspace="3"
                                    ResourceConstant="ALT_ECARD" runat="server" ID="imgECard" />
                            </asp:HyperLink>&nbsp;
                            <uc1:Add2List ID="add2List1" cssClass="hebListMenu" runat="server" Visible="false" />
                            <uc2:MatchMeterInfoDisplay runat="server" ID="ucMatchMeterInfoDisplay" DispalyType="2" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <input type="text" name="edit" runat="server" id="txtFavoriteNote" />
</div>
<asp:PlaceHolder runat="server" ID="plcTransparent" Visible="False">
    <div id="transContainer">
        <div id="trans1">
        </div>
        <div id="trans2">
        </div>
        <div id="trans3">
        </div>
    </div>
</asp:PlaceHolder>
