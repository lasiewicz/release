﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProfileFilmStrip.ascx.cs" Inherits="Matchnet.Web.Framework.Ui.BasicElements.ProfileFilmStrip" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mnl" Namespace="Matchnet.Web.Framework.Ui.BasicElements.Links" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mnbe" Namespace="Matchnet.Web.Framework.Ui.BasicElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="uc1" TagName="FilmStripMicroProfile" Src="FilmStripMicroProfile.ascx" %>

<div id="member-filmstrip" class="grid-block-full">
    <asp:PlaceHolder ID="phHasProfiles" runat="server">
        <div class="header-options clearfix">
	        <h2 class="component-header"><mn2:FrameworkLiteral ID="literalYourMatches" runat="server" ResourceConstant="TXT_YOUR_MATCHES"></mn2:FrameworkLiteral></h2>
	        <div class="links">
		        <a href="/Applications/Search/SearchPreferences.aspx"><mn2:FrameworkLiteral ID="FrameworkLiteral1" runat="server" ResourceConstant="TXT_EDIT"></mn2:FrameworkLiteral></a>
		         | <a href="/Applications/Search/SearchResults.aspx"><mn2:FrameworkLiteral ID="FrameworkLiteral2" runat="server" ResourceConstant="TXT_VIEW_MORE"></mn2:FrameworkLiteral></a>
	        </div>
        </div>

        <div id="filmstrip-yourmatches">
            <div class="ajax-loading">
                <mn:image id="mnimage4704" runat="server" filename="ajax-loader.gif" titleresourceconstant="" resourceconstant="" />
                <p><mn:txt id="mntxt7220" runat="server" resourceconstant="TXT_LOADING" expandimagetokens="true" /></p>
            </div>
            <button class="filmstrip-prev disabled">&#9668;</button>
            <button class="filmstrip-next">&#9658;</button>
            <div class="filmstrip results">
	            <ul>            		
		            <asp:Repeater ID="repeaterProfiles" runat="server" OnItemDataBound="repeaterProfiles_ItemDataBound">
		                <ItemTemplate><%--note: highlighted class added in code-behind --%>
		                    <li class="item style-basic" id="liBegin" runat="server">
		                        <uc1:FilmStripMicroProfile ID="FilmStripMicroProfile1" runat="server"></uc1:FilmStripMicroProfile>
		                    </li>
		                </ItemTemplate>
		            </asp:Repeater>
		            <asp:PlaceHolder ID="phViewMore" runat="server">
		                <li class="item style-basic view-more"><a href="/Applications/Search/SearchResults.aspx" class="memberpic"><mn2:FrameworkLiteral ID="FrameworkLiteral3" runat="server" ResourceConstant="TXT_VIEW_MORE"></mn2:FrameworkLiteral></a></li>
		            </asp:PlaceHolder>
	            </ul>
            </div>
        </div>
    </asp:PlaceHolder>
    
    <asp:PlaceHolder ID="phMissingProfilesCopy" runat="server">
        <div class="header-options clearfix">
	        <h2 class="component-header">Start Finding Your Match!</h2>
        </div>
        <div id="filmstrip-no-matches" class="clearfix">
	        <div class="item"><%--Change Match Preferences--%>
                <mn:txt id="mntxt1262" runat="server" resourceconstant="TXT_NO_MATCHES_ITEM_01" expandimagetokens="true" />
	        </div>
	        <div class="item"><%--Create a Standout Profile--%>
                <mn:txt id="Txt1" runat="server" resourceconstant="TXT_NO_MATCHES_ITEM_02" expandimagetokens="true" />
	        </div>
	        
	        <asp:PlaceHolder ID="phMissingProfileCopyItem3" runat="server" Visible="false">
	            <div class="item"><%--Search Members Online--%>
                    <mn:txt id="Txt2" runat="server" resourceconstant="TXT_NO_MATCHES_ITEM_03" expandimagetokens="true" />
	            </div>
	        </asp:PlaceHolder>
	        
	        <asp:PlaceHolder ID="phMissingProfileCopyItem4" runat="server" Visible="false">
	            <div class="item"><%--Learn Your Color--%>
                    <mn:txt id="Txt3" runat="server" resourceconstant="TXT_NO_MATCHES_ITEM_04" expandimagetokens="true" />
	            </div>
	        </asp:PlaceHolder>
        </div>
    </asp:PlaceHolder>
</div>




