﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MiniProfileListView.ascx.cs"
    Inherits="Matchnet.Web.Framework.Ui.ProfileElements.MiniProfileListView" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mnl" Namespace="Matchnet.Web.Framework.Ui.BasicElements.Links"
    Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="uc2" TagName="HighlightProfileInfoDisplay" Src="HighlightInfo.ascx" %>
<%@ Register TagPrefix="uc2" TagName="Sprite" Src="../PageElements/Sprite.ascx" %>
<%@ Register TagPrefix="uc2" TagName="LastTime" Src="../BasicElements/LastTime.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Add2List" Src="/Framework/Ui/BasicElements/Add2List.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Block" Src="/Framework/Ui/ProfileElements/Block.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ContactHistory" Src="/Framework/Ui/ProfileElements/ContactHistory.ascx" %>
<%@ Register TagPrefix="uc1" TagName="SecretAdmirer" Src="/Framework/Ui/ProfileElements/SecretAdmirer.ascx" %>
<%@ Register TagPrefix="uc2" TagName="MiniProfileHotListBar" Src="/Framework/Ui/BasicElements/MiniProfileHotListBar.ascx" %>
<%@ Register TagPrefix="uc2" TagName="MatchMeterInfoDisplay" Src="~/Framework/Ui/BasicElements/MatchMeterInfoDisplay.ascx" %>
<%@ Register TagPrefix="uc2" TagName="OmnidateInvitationButton" Src="/Applications/Omnidate/Controls/OmnidateInvitationButton.ascx" %>
<%@ Register TagPrefix="uc1" TagName="NoPhoto" Src="../PageElements/NoPhoto20.ascx" %>

<asp:Panel ID="panelMiniProfile" runat="server" CssClass="results results30-profile list-view-30 mini-profile">
    <div class="profile-wrapper">
        <div class="profile-body clearfix">
            <uc2:HighlightProfileInfoDisplay runat="server" ID="ucHighlightProfileInfoDisplay"
                OuterLinkDisplayText="false" OuterLinkImageResourceConstant="IMAGE_SPRITE_LEARN_ABOUT_HIGHLIGHTED_PROFILES" />
            <uc2:OmnidateInvitationButton runat="server" ID="btnOmnidate" DisplayContext="MiniProfileListView" />
            <ul class="indicator">
                <li class="item status">
                    <%--online/offline icon goes here--%>
                    <asp:PlaceHolder ID="phOnline" runat="server" Visible="false">
                        <div class="online">
                            <asp:HyperLink runat="server" ID="lnkOnline">
                                <uc2:Sprite ID="sprOnline" TitleResourceConstant="ALT_IM_ONLINE" TextResourceConstant="ALT_IM_ONLINE"
                                    SpriteClass="spr s-icon-status-online-sm" runat="server" />
                            </asp:HyperLink>
                        </div>
                    </asp:PlaceHolder>
                    <asp:PlaceHolder ID="phOffline" runat="server" Visible="false">
                        <div class="offline">
                            <uc2:Sprite ID="sprOffline" TextResourceConstant="ALT_IM_OFFLINE" SpriteClass="spr s-icon-status-offline-sm"
                                runat="server" />
                            <uc2:LastTime ID="lastTimeLogon" runat="server" Visible="false" />
                        </div>
                    </asp:PlaceHolder>
                </li>
            </ul>
 
            <%--picture--%>
            <div class="picture">
                <%--New/Updated - '.s-icon-word-new/.s-icon-word-update--%>
                <asp:PlaceHolder ID="phIsNewProfile" runat="server" Visible="false">
                    <div class="ribbon small"><span class="spr s-icon-word-new"><span>New</span></span></div>
                </asp:PlaceHolder>
                <asp:PlaceHolder ID="phIsUpdatedProfile" runat="server" Visible="false">
                    <div class="ribbon small"><span class="spr s-icon-word-updated"><span>Updated</span></span></div>
                </asp:PlaceHolder>
                <mn:Image runat="server" ID="imgThumb" Width="112" Height="144" Border="0" ResourceConstant="" />
                <uc1:NoPhoto runat="server" ID="noPhoto" DivCSSClass="ask" Mode="NoPhoto" Visible="False" />
                <asp:PlaceHolder ID="phColorCode" runat="server" Visible="false">
                    <div class="cc-pic-tag cc-pic-tag-sm cc-pic-<%=_ColorText.ToLower() %>">
                        <span>
                            <%=_ColorText %>
                        </span>
                    </div>
                </asp:PlaceHolder>
            </div>
            <div class="info">
                <h4>
                    <asp:HyperLink ID="lnkUserName" runat="server" />
                    <asp:PlaceHolder ID="phMutualMatchRate" runat="server" Visible="false">
                        <%--Mutualmatch rating goes here--%>
                        <span class="mutual-rate"><asp:Literal ID="litMutualMatchRate" runat="server"></asp:Literal>%</span>
                    </asp:PlaceHolder>
                </h4>
                <ul class="details">
                    <li>
                        <asp:Label ID="txtAge" runat="server" />
                        -
                        <asp:Label ID="txtLocation" runat="server" /></li>
                    <li>
                        <asp:Literal ID="literalMaritalAndGender" runat="server"></asp:Literal></li>
                    <li>
                        <asp:PlaceHolder ID="phLastLogon" runat="server" Visible="false">
                            <mn:Txt ID="txtLoggedIn" ResourceConstant="LOGGED_IN" runat="server" />
                            <uc2:LastTime ID="lastTimeLogon2" runat="server" />
                        </asp:PlaceHolder>
                    </li>
                </ul>
                <div class="history">
                    <mn:Image ID="imgBothSaidYes" runat="server" TitleResourceConstant="TTL_CLICK_YOU_BOTH_SAID_YES_EXCLAMATION"
                        FileName="icon-click-yy.gif" Style="display: none;" />
                    <mn:Image ID="imgPremiumAuthenticated" runat="server" FileName="icon_premiumAuthenticated_l.gif"
                        Visible="false" TitleResourceConstant="ALT_PREMIUM_AUTHENTICATED" ResourceConstant="ALT_PREMIUM_AUTHENTICATED"
                        class="verifiedIcon" />
                    <asp:PlaceHolder ID="plcHotListBar" runat="server">
                        <uc2:MiniProfileHotListBar ID="HotListBar" runat="server" FriendIconFileName="icon-members-hotlisted-{0}.gif"
                            ContactedIconFileName="icon-members-emailed-{0}.gif" TeaseIconImageFileName="icon-members-flirted-{0}.gif"
                            IMIconFileName="icon-members-IM-{0}.gif" ViewProfileIconFileName="icon-members-viewed-{0}.gif"
                            OmnidateIconFileName="icon-members-omnidated-{0}.gif" />
                    </asp:PlaceHolder>
                </div>
            </div>
            <p class="excerpt">
                <asp:PlaceHolder ID="phAboutMe" runat="server" Visible="false"><span class="highlight-text">
                    <asp:Literal ID="litAboutMe" runat="server"></asp:Literal></span> </asp:PlaceHolder>
                <asp:HyperLink ID="lnkMorePhotos" runat="server" Visible="true" />
            </p>
            <ul class="action-items tab-items cearfix">
                <li class="comm item tabbed">
                    <div class="tabbed-wrapper">
                        <%--Say Hi--%>
                        <asp:HyperLink ID="lnkSayHi" runat="server" TabIndex="10" CssClass="menu">
                            <span class="spr s-icon-A-hover-comm"><span></span></span>
                            <mn:Txt ID="txtSayHi" CssClass="title" ResourceConstant="TXT_SAY_HI" TitleResourceConstant="ALT_SAY_HI"
                                runat="server" />
                        </asp:HyperLink>
                        <div id="divSayHi" runat="server" style="display: none" class="profile30-comm items">
                            <ul class="clearfix">
                                <li class="email">
                                    <asp:HyperLink ID="lnkEmail" runat="server">
                                        <mn:Txt ID="txtEmail" ResourceConstant="PRO_EMAIL" TitleResourceConstant="ALT_EMAIL_ME"
                                            runat="server" />
                                    </asp:HyperLink>
                                </li>
                                <li class="online">
                                    <!--online-->
                                    <asp:HyperLink ID="lnkIMOnline" runat="server">
                                        <mn:Txt ID="txtIMOnline" ResourceConstant="PRO_IM" TitleResourceConstant="ALT_IM_ONLINE"
                                            runat="server" />
                                    </asp:HyperLink>
                                    <!--offline-->
                                    <mn:Txt ID="txtIMOffline" ResourceConstant="PRO_IM" TitleResourceConstant="ALT_IM_OFFLINE"
                                        runat="server" CssClass="offline" />
                                </li>
                                <uc2:OmnidateInvitationButton runat="server" ID="btnOmnidateComm" DisplayContext="MiniProfileListViewComm" />
                                <asp:PlaceHolder runat="server" ID="plcFlirt" >
                                    <li class="flirt">
                                        <asp:HyperLink ID="lnkFlirt" runat="server" Visible="True">
                                            <mn:Txt ID="txtFlirt" ResourceConstant="PRO_TEASE" TitleResourceConstant="ALT_TEASE_ME"
                                                runat="server" />
                                            <mn:Txt ID="txtFlirtFree" ResourceConstant="PRO_TEASE_FREE" TitleResourceConstant="ALT_TEASE_ME"
                                                runat="server" />
                                        </asp:HyperLink>
                                    </li>
                                </asp:PlaceHolder>
                                <asp:PlaceHolder ID="phEcardLink" runat="server">
                                    <li class="ecard">
                                        <asp:HyperLink ID="lnkEcard" runat="server">
                                            <mn:Txt ID="txtEcard" ResourceConstant="PRO_ECARD" TitleResourceConstant="ALT_ECARD"
                                                runat="server" />
                                            <mn:Txt ID="txtEcardFree" ResourceConstant="PRO_ECARD_FREE" TitleResourceConstant="ALT_ECARD"
                                                runat="server" />
                                        </asp:HyperLink>
                                    </li>
                                </asp:PlaceHolder>
                            </ul>
                        </div>
                    </div>
                    <%--tabbed-wrapper--%>
                </li>
                <asp:PlaceHolder runat="server" ID="plcFlirt2" Visible="False" >
                <li class="comm item flirt">
                    <asp:HyperLink ID="lnkFlirt2" runat="server" Visible="True">
                        <mn:Txt ID="txtFlirt2" ResourceConstant="PRO_TEASE" TitleResourceConstant="ALT_TEASE_ME"
                            runat="server" />
                        <mn:Txt ID="txtFlirtFree2" ResourceConstant="PRO_TEASE_FREE" TitleResourceConstant="ALT_TEASE_ME"
                            runat="server" />
                    </asp:HyperLink>
                </li>
                </asp:PlaceHolder>
                <li class="admirer item menu">
                    <%--Secret Admirer--%>
                    <uc1:SecretAdmirer ID="SecretAdmirer1" runat="server" IconHasYNMCSS="spr s-icon-hover-y-on-sm"
                        IconNoYNMCSS="spr s-icon-hover-y-off-sm s-icon-A-hover-y-sm" HideLinkIcon="true" HideYNMInPopup="true">
                    </uc1:SecretAdmirer>
                    <%--tabbed-wrapper--%>
                </li>
                <asp:PlaceHolder ID="phCustomMatchMeter" Visible="false" runat="server">
                    <li class="jmeter item tabbed">
                        <uc2:MatchMeterInfoDisplay runat="server" ID="ucMatchMeterInfoDisplay" 
                            Visible="false" />
                    </li>
                </asp:PlaceHolder>
                <li class="favorites item tabbed">
                    <div class="tabbed-wrapper">
                        <%--Favorites--%>
                        <uc1:Add2List ID="add2List" runat="server" ClearIconInlineAlignments="true" Orientation="Horizontal"
                            HotlistResourceConstant="TXT_FAVORITE" HotlistTitleResourceConstant="ALT_ADD_TO_FAVORITES"
                            UnHotlistResourceConstant="TXT_UNFAVORITE" UnHotlistTitleResourceConstant="ALT_REMOVE_FROM_FAVORITES"
                            IncludeSpanAroundDescriptionText="true" ExcludeBlockAsFavorited="true" DisplayLargeIcon="false"
                            EnableSprite="true" SpriteHotlistCSS="spr s-icon-A-favorites-none" SprintUnHotlistCSS="spr s-icon-favorites-added s-icon-favorites-performed">
                        </uc1:Add2List>
                    </div>
                    <%--tabbed-wrapper--%>
                </li>
                <li class="block item tabbed">
                    <div class="tabbed-wrapper">
                        <%--Block--%>
                        <uc1:Block ID="blockProfile" runat="server" HideLinkText="true" LinkResourceConstant="TXT_BLOCK_FROM"
                            LinkTitleResourceConstant="ALT_BLOCK" IconCSS="spr s-icon-A-hover-block" IconBlockedCSS="spr s-icon-hover-block-active s-icon-hover-block-performed"
                            AutoRefreshOnSearchResultsBlock="true"></uc1:Block>
                    </div>
                    <%--tabbed-wrapper--%>
                </li>
            </ul>
        </div>
    </div>
</asp:Panel>
<script type="text/javascript">
    MenuToggler($j('#<%=lnkSayHi.ClientID %>'), $j('#<%=divSayHi.ClientID %>'), '<%=GetEventType() %>'); //$link, $layer, eventType
    TabbedMenuToggler($j('#<%=panelMiniProfile.ClientID %>'), undefined, $j('#<%=lnkSayHi.ClientID %>'), '<%=GetEventType() %>'); //$menu, isHover, $link, eventType
</script>
