﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MicroProfileList.ascx.cs" Inherits="Matchnet.Web.Framework.Ui.BasicElements.MicroProfileList" %>
<%@ Register Src="/Framework/Ui/BasicElements/MicroProfile20.ascx" TagName="MicroProfile20" TagPrefix="uc2" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>

     <asp:PlaceHolder ID="phNoResultDisplay" runat="server" Visible="false">
       <mn2:FrameworkLiteral ID="noResultDisplay" runat="server" ResourceConstant=""/>
         <ul>
             <asp:Literal ID="litYNMBucket" runat="server" />
         </ul>
    </asp:PlaceHolder>
    
    <asp:PlaceHolder ID="phYourMatches" runat="server" Visible="true">
        <asp:Repeater ID="repeaterYourMatches" runat="server" >
        <HeaderTemplate>
        <ul>
             <asp:Literal ID="litYNMBucket" runat="server" />
        </HeaderTemplate>
        <ItemTemplate>
            <asp:PlaceHolder ID="phHighlight" runat="server" Visible="false">
            <li class="highlight-profile home-your-matches clearfix">
            </asp:PlaceHolder>
		    
            <asp:PlaceHolder ID="phRegular" runat="server" Visible="false">
            <li class="home-your-matches clearfix">
            </asp:PlaceHolder>
		    
            <!--microprofile20-->
                <uc2:MicroProfile20 ID="microProfileMatches" runat="server" DisplayContext="Default" NoResultContentResource="NO_MATCHES_MARKETING"/>
            </li>
        </ItemTemplate>
        <FooterTemplate>
        </ul>
        </FooterTemplate>
        </asp:Repeater>
        
    </asp:PlaceHolder>
    
  <div class="your-matches-view-more-btm">
        <div class="pagination-buttons image-text-pair">
        <asp:HyperLink NavigateUrl="/Applications/Search/SearchResults.aspx" class="btn link-secondary" ID="lnkViewMore" runat="server">
            <span class="next"><mn2:FrameworkLiteral ID="FrameworkLiteral4" runat="server" ResourceConstant="TXT_VIEW_MORE"></mn2:FrameworkLiteral></span><span class="direction"><mn:Txt runat="server" ResourceConstant="NAV_NEXT_ARROW" /></span>
        </asp:HyperLink>	
        </div>
    </div>