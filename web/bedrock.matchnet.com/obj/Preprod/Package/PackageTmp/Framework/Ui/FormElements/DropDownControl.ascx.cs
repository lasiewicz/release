using System;
using System.Data;
using System.Text;
using System.Web.UI.WebControls;

using Spark.SAL;

namespace Matchnet.Web.Framework.Ui.FormElements
{
	/// <summary>
	///		Summary description for HeightRangeControl.
	/// </summary>
	public class DropDownControl : FrameworkControl
	{
		protected Txt txtPreferenceName;
		protected Literal litCurrentValue;
		protected System.Web.UI.HtmlControls.HtmlGenericControl divCurrentValue;
		protected DropDownList ddlOptions;

        public bool IsCollapsed = true;

		private MemberSearchPreferenceInt _memberSearchPreferenceInt;

		private void Page_Load(object sender, System.EventArgs e)
		{
            IsCollapsed = (_memberSearchPreferenceInt.Value <= 0);
			bindData();
			setupClientScript();
		}

		private void DropDownControl_PreRender(object sender, EventArgs e)
		{
			litCurrentValue.Text = ddlOptions.SelectedItem.Text;
		}

		private void ddlOptions_SelectedIndexChanged(object sender, EventArgs e)
		{
			_memberSearchPreferenceInt.Value = Conversion.CInt(ddlOptions.SelectedValue);
            IsCollapsed = (_memberSearchPreferenceInt.Value <= 0);
		}

		private void bindData()
		{
			
			DataTable dtOptions = Util.Option.GetOptions(_memberSearchPreferenceInt.Preference.Attribute.Name, g);
			dtOptions.Rows[0]["Content"] = g.GetResource("ANY_");

			ddlOptions.DataSource = dtOptions;
			ddlOptions.DataValueField = "Value";
			ddlOptions.DataTextField = "Content";
			ddlOptions.DataBind();
			FrameworkGlobals.SelectItem(ddlOptions, _memberSearchPreferenceInt.Value.ToString());
		}

		private void setupClientScript()
		{
			ddlOptions.Attributes.Add("onchange", "showSelectedValue();");

			StringBuilder sb = new StringBuilder();
			sb.Append("<script language='javascript'>");
			sb.Append("function showSelectedValue()");
			sb.Append("{");
			sb.Append("var list = document.getElementById('" + ddlOptions.ClientID + "');");
			sb.Append("var textDiv = document.getElementById('" + divCurrentValue.ClientID + "');");
			sb.Append("textDiv.innerHTML = list.options[list.selectedIndex].text;");
			sb.Append("}");
			sb.Append("</script>");
			Page.RegisterClientScriptBlock("DropDownControlScript", sb.ToString());
		}

		public string ResourceConstant
		{
			get
			{
				return txtPreferenceName.ResourceConstant;
			}
			set
			{
				txtPreferenceName.ResourceConstant = value;
			}
		}

		public MemberSearchPreferenceInt MemberSearchPreferenceInt
		{
			get
			{
				return _memberSearchPreferenceInt;
			}
			set
			{
				_memberSearchPreferenceInt = value;
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
			this.PreRender += new EventHandler(DropDownControl_PreRender);
			ddlOptions.SelectedIndexChanged += new EventHandler(ddlOptions_SelectedIndexChanged);
		}
		#endregion
	}
}
