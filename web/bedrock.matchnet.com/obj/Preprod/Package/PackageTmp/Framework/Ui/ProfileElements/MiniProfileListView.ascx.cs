﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Member.ValueObjects.Interfaces;
using Matchnet.Member.ValueObjects.Photos;
using Matchnet.Web.Applications.ColorCode;
using Matchnet.Web.Applications.MemberProfile;
using Matchnet.Web.Framework.Managers;
using Spark.Common.AccessService;
using Matchnet.Web.Framework.Ui.BasicElements;
using Matchnet.List.ValueObjects;
using Matchnet.Content.ServiceAdapters.Links;
using Matchnet.Configuration.ServiceAdapters;

namespace Matchnet.Web.Framework.Ui.ProfileElements
{
    public partial class MiniProfileListView : FrameworkControl
    {
        protected int _brandID;
        protected int _communityiID;
        protected int _siteID;
        protected Color _Color = Color.none;
        protected string _ColorText = "";
        protected string HighlightedClass = "";
        protected FrameworkControl _resourceControl;
        protected string _viewProfileUrl = "";
        protected bool _isHighlighted;
        protected bool _isSpotlight;
        protected ResultContextType _DisplayContext = ResultContextType.SearchResult;
        protected HotListCategory _hotListCategory = HotListCategory.Default;
        //private List<Photo> _approvedPhotos;
        private int _approvedPhotosCount = -1;
        private int _matchRating = Constants.NULL_INT;
        private bool _displayMatchRating;

        #region Properties
        public int ApprovedPhotosCount
        {
            get
            {
                try
                {
                    if (_approvedPhotosCount == -1)
                    {
                        _approvedPhotosCount = MemberPhotoDisplayManager.Instance.GetApprovedPhotosCount(g.Member, Member,
                                                                                  g.Brand);
                    }

                    return _approvedPhotosCount;

                }
                catch (Exception ex)
                { return 0; }

            }

        }

        public FrameworkControl ResourceControl
        {
            get
            {
                if (_resourceControl == null)
                    return this;
                else
                { return _resourceControl; }
            }

            set { _resourceControl = value; }
        }
        public IMemberDTO Member { get; set; }
        public bool EnableProfileLinks { get; set; }
        public bool CurrentlyOnline { get; set; }
        public int Ordinal { get; set; }
        public bool IsHighlighted
        {
            get { return (this._isHighlighted); }
            set { this._isHighlighted = value; }
        }

        public bool IsSpotlight
        {
            get { return (this._isSpotlight); }
            set { this._isSpotlight = value; }
        }
        public ResultContextType DisplayContext
        {
            set { _DisplayContext = value; }
            get { return _DisplayContext; }

        }
        public BreadCrumbHelper.EntryPoint MyEntryPoint { get; set; }
        public bool IsHotListFriend { get; set; }
        public int Counter { get; set; }
        public HotListCategory HotListCategory
        {
            get { return (_hotListCategory); }
            set { _hotListCategory = value; }
        }

        /// <summary>
        /// This indicates whether control will load automatically during page load,
        /// set this to false if you need to orchestrate when control loads
        /// </summary>
        public bool LoadControlAutomatically { get; set; }

        public bool PhotoOnly { get; set; }
        public int MatchRating
        {
            get { return _matchRating; }
            set { _matchRating = value; }
        }
        public bool DisplayMatchRating
        {
            get { return _displayMatchRating; }
            set { _displayMatchRating = value; }
        }
        #endregion

        public MiniProfileListView()
        {
            MyEntryPoint = BreadCrumbHelper.EntryPoint.Unknown;
            LoadControlAutomatically = true;
        }

        #region Event Handlers
        protected void Page_Load(object sender, EventArgs e)
        {
            if (LoadControlAutomatically)
            {
                LoadMiniProfile();
            }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            if (FrameworkGlobals.SearchResultsM2MUpgradeEnabled(g))
            {
                SecretAdmirer1.LinkResourceConstant = "PRO_SECRETADMIRER_M2M_UPGRADE";
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            try
            {
                base.OnPreRender(e);

                if (g.AnalyticsOmniture != null)
                {
                    //Omniture
                    string additionalSource = "";
                    WebConstants.Action action = WebConstants.Action.EmailMeNowButton;
                    if (_DisplayContext == ResultContextType.HotList)
                    {
                        if (_isSpotlight)
                            additionalSource = "List";
                    }
                    else if (_DisplayContext == ResultContextType.MembersOnline)
                    {
                        additionalSource = "List";
                    }
                    else if (FrameworkGlobals.SearchResultsM2MUpgradeEnabled(g))
                    {
                        additionalSource = "List View";
                        action = WebConstants.Action.EmailMe;
                    }

                    lnkFlirt.NavigateUrl = g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.Tease, WebConstants.Action.Flirt, lnkFlirt.NavigateUrl, additionalSource);
                    lnkFlirt2.NavigateUrl = lnkFlirt.NavigateUrl;
                    lnkUserName.NavigateUrl = g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.ViewProfile, WebConstants.Action.ProfileName, lnkUserName.NavigateUrl, additionalSource);
                    imgThumb.NavigateUrl = g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.ViewProfile, WebConstants.Action.ProfilePhoto, imgThumb.NavigateUrl, additionalSource);
                    lnkOnline.Attributes.Add("onclick", g.AnalyticsOmniture.GetOnClickCustomLinkTracking(WebConstants.Action.IM, g.AnalyticsOmniture.PageName, "", false, false));
                    lnkEcard.NavigateUrl = g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.ECard, WebConstants.Action.ECard, lnkEcard.NavigateUrl, additionalSource);
                    add2List.ActionCallPage = g.AnalyticsOmniture.PageName;
                    add2List.ActionCallPageDetail = "";

                    lnkEmail.NavigateUrl = g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.Compose, action, lnkEmail.NavigateUrl, additionalSource);
                    if (ApprovedPhotosCount > 0)
                    {
                        lnkMorePhotos.NavigateUrl = g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.ViewProfile, WebConstants.Action.ViewMorePhotos, lnkMorePhotos.NavigateUrl, additionalSource);
                    }
                    else
                    {
                        lnkMorePhotos.NavigateUrl = g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.ViewProfile, WebConstants.Action.ViewMemberLink, lnkMorePhotos.NavigateUrl, additionalSource);
                    }
                }
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }

        }

        #endregion

        #region Public Methods
        public void LoadMiniProfile()
        {
            try
            {
                if (Member == null)
                    return;

                _viewProfileUrl = BreadCrumbHelper.MakeViewProfileLink(MyEntryPoint, Member.MemberID, Ordinal, null,
                                                                       (int)_hotListCategory);
                if (PhotoOnly)
                {
                    _viewProfileUrl = BreadCrumbHelper.AppendParamToProfileLink(_viewProfileUrl,
                                                                                Search.Constants.ReverseSearch.Parameter
                                                                                      .OnlyPhotos + "=true");
                }

                _siteID = g.Brand.Site.SiteID;
                _brandID = g.Brand.BrandID;
                _communityiID = g.Brand.Site.Community.CommunityID;

                //_isHighlighted = FrameworkGlobals.memberHighlighted(Member.MemberID, g.Brand);
                _isHighlighted = FrameworkGlobals.memberHighlighted(Member, g.Brand);
                if (_isHighlighted)
                {
                    HighlightedClass = " highlighted";
                    panelMiniProfile.CssClass += HighlightedClass;
                    ucHighlightProfileInfoDisplay.Visible = true;
                    ucHighlightProfileInfoDisplay.HighlightedMemberID = Member.MemberID;
                    ucHighlightProfileInfoDisplay.Highlighted = true;

                    if (g.Member != null)
                    {
                        AccessPrivilege apHighlight =
                            g.Member.GetUnifiedAccessPrivilege(PrivilegeType.HighlightedProfile, g.Brand.BrandID,
                                                               g.Brand.Site.SiteID, g.Brand.Site.Community.CommunityID);
                        if (apHighlight != null && apHighlight.EndDatePST > DateTime.Now)
                        {
                            ucHighlightProfileInfoDisplay.Visible = false;
                        }
                    }
                }

                DateTime birthDate = Member.GetAttributeDate(g.Brand, "BirthDate", DateTime.MinValue);
                txtAge.Text = FrameworkGlobals.GetAge(birthDate).ToString();
                if (this.txtAge.Text.Trim() == "")
                {
                    this.txtAge.Text = "&nbsp;";
                }
                else
                {
                    if (FrameworkGlobals.isHebrewSite(g.Brand))
                    {
                        txtAge.Text = g.GetResource("TXT_YEARS_OLD", ResourceControl) + " " + txtAge.Text;
                    }
                    else
                    {
                        txtAge.Text += " " + g.GetResource("TXT_YEARS_OLD", ResourceControl);
                    }
                }

                bool isNewProfile = Member.IsNewMember(g.Brand.Site.Community.CommunityID);
                bool isUpdatedProfile = Member.IsUpdatedMember(g.Brand.Site.Community.CommunityID);

                phIsNewProfile.Visible = isNewProfile;
                phIsUpdatedProfile.Visible = !isNewProfile && isUpdatedProfile;

                string userName = Member.GetUserName(g.Brand);
                if (_displayMatchRating && g.Member != null && g.Member.MemberID != Member.MemberID && _matchRating > 0)
                {
                    phMutualMatchRate.Visible = true;
                    litMutualMatchRate.Text = _matchRating.ToString();
                    lnkUserName.Text = FrameworkGlobals.Ellipsis(userName, 13, "&hellip;");
                }
                else
                {
                    lnkUserName.Text = FrameworkGlobals.Ellipsis(userName, 15, "&hellip;");
                }

                lnkUserName.ToolTip = userName;

                string location = ProfileDisplayHelper.GetRegionDisplay(Member, g);
                txtLocation.Text = FrameworkGlobals.Ellipsis(location, 22, "&hellip;");
                txtLocation.ToolTip = location;

                literalMaritalAndGender.Text = ProfileDisplayHelper.GetMaritalStatusSeekingGenderDisplay(Member, this.g);
                litAboutMe.Text = GetAboutMeEssay();
                if (!string.IsNullOrEmpty(litAboutMe.Text))
                {
                    phAboutMe.Visible = true;
                }

                if (EnableProfileLinks)
                {
                    lnkUserName.NavigateUrl = _viewProfileUrl;
                    imgThumb.NavigateUrl = _viewProfileUrl;
                }

                DisplayMemberPhoto();
                SetOnlineLink();

                //color code
                if (ColorCodeHelper.IsColorCodeEnabled(g.Brand) &&
                    ColorCodeHelper.HasMemberCompletedQuiz(Member, g.Brand) &&
                    !ColorCodeHelper.IsMemberColorCodeHidden(Member, g.Brand))
                {
                    phColorCode.Visible = true;
                    _Color = ColorCodeHelper.GetPrimaryColor(Member, g.Brand);
                    _ColorText = ColorCodeHelper.GetFormattedColorText(_Color);
                }

                //hotlist related icons
                SetHotlistIcons();

                

                //Say Hi
                SetSayHi();

                //favorites
                add2List.MemberID = this.Member.MemberID;

                //block
                blockProfile.IsAvailableToSubscribersOnly =
                    SettingsManager.GetSettingBool(SettingConstants.BLOCK_PROFILE_AVAILABLE_TO_SUBS_ONLY, g.Brand);
                blockProfile.LoadBlockControl(Member);

                //YNM
                SetYNM();

                // JMeter
                int mmsett =
                    Int32.Parse(RuntimeSettings.GetSetting("MatchTest_App_Settings", g.Brand.Site.Community.CommunityID,
                                                           g.Brand.Site.SiteID, g.Brand.BrandID));
                int mmenumval = (Int32)WebConstants.MatchMeterFlags.EnableApp;
                if ((mmsett & mmenumval) == mmenumval)
                {
                    phCustomMatchMeter.Visible = true;
                    ucMatchMeterInfoDisplay.Visible = true;
                    ucMatchMeterInfoDisplay.IsMatchMeterEnabled = true;
                    ucMatchMeterInfoDisplay.MemberID = Member.MemberID;
                    ucMatchMeterInfoDisplay.DispalyType = 6;
                }

                // Omnidate
                btnOmnidate.TargetMember = Member;
                btnOmnidateComm.TargetMember = Member;


                if (FrameworkGlobals.SearchResultsM2MUpgradeEnabled(g))
                {
                    panelMiniProfile.CssClass += " comm-overlay";
                    plcFlirt.Visible = false;
                    plcFlirt2.Visible = true;
                    if (g.Member != null && g.Member.IsPayingMember(g.Brand.Site.SiteID))
                        panelMiniProfile.CssClass += " mini-profile-sub";
                }

                if (g.AppPage.ControlName.ToLower() == "membersonline20" &&
                    MembersOnlineManager.Instance.IsMOLRedesign30Enabled(g.Member, g.Brand))
                {
                    phLastLogon.Visible = false;
                }
            }
            catch (Exception ex)
            { g.ProcessException(ex); }
        }
        #endregion

        #region Private Methods
        private void DisplayMemberPhoto()
        {
            lnkMorePhotos.Text = g.GetResource("TXT_VIEW_MEMBER_ARROWS", this);
            lnkMorePhotos.NavigateUrl = _viewProfileUrl;
            imgThumb.NavigateUrl = _viewProfileUrl;

            bool isMale = FrameworkGlobals.IsMaleGender(Member, g.Brand);
            imgThumb.ImageUrl = MemberPhotoDisplayManager.Instance.GetNoPhotoFile(NoPhotoImageType.Thumb, isMale);

            var photo = MemberPhotoDisplayManager.Instance.GetDefaultPhoto(g.Member, Member, g.Brand);
            if (photo == null)
            {
                imgThumb.Visible = false;
                noPhoto.Visible = true;
                noPhoto.MemberID = Member.MemberID;
                noPhoto.Member = Member;
                noPhoto.DestURL = _viewProfileUrl;

                noPhoto.NoPhotoFileName = MemberPhotoDisplayManager.Instance.GetNoPhotoFile(NoPhotoImageType.BackgroundThumb, isMale);
                noPhoto.PrivatePhotoFileName = MemberPhotoDisplayManager.Instance.GetNoPhotoFile(NoPhotoImageType.BackgroundThumb, isMale);
            }
            else
            {
                imgThumb.ImageUrl = MemberPhotoDisplayManager.Instance.GetPhotoDisplayURL(g.Member, Member, g.Brand,
                                                                       photo, PhotoType.Thumbnail,
                                                                       PrivatePhotoImageType.Thumb,
                                                                        NoPhotoImageType.Thumb);
                if (ApprovedPhotosCount > 1)
                {
                    lnkMorePhotos.Text = FrameworkGlobals.GetMorePhotoLink(ApprovedPhotosCount, g, ResourceControl, "TXT_VIEW_MEMBER_ARROWS");
                }
            }
        }

        protected void SetOnlineLink()
        {
            bool isOnline;
            string evar27ForIM = GetEvar27ForIM();
            
            OnlineLinkHelper.SetOnlineLink(DisplayContext, Member, g.Member, lnkOnline, LinkParent.MiniProfile, out isOnline, evar27ForIM);    
            
            DateTime loginDate = Member.GetLastLogonDate(g.Brand.Site.Community.CommunityID);
            lastTimeLogon2.LoadLastTime(loginDate);
            phLastLogon.Visible = !String.IsNullOrEmpty(lastTimeLogon2.LiteralTimeControl.Text);

            if (!isOnline)
            {
                CurrentlyOnline = false;
                phOffline.Visible = true;

                lastTimeLogon.LoadLastTime(loginDate);
                string lastLoginText = String.IsNullOrEmpty(lastTimeLogon.LiteralTimeControl.Text) ? g.GetResource("N_A", this) : lastTimeLogon.LiteralTimeControl.Text;
                sprOffline.ResourceControl = this;
                sprOffline.Title = g.GetResource("LAST_ONLINE", this) + lastLoginText;
            }
            else
            {
                CurrentlyOnline = true;
                phOnline.Visible = true;
                sprOnline.ResourceControl = this;
            }
        }

        private string GetEvar27ForIM()
        {
            string result = string.Empty;
            if (FrameworkGlobals.SearchResultsM2MUpgradeEnabled(g))
            {
                result = "IM (Matches - List View)";
                if (g.AppPage.ControlName.ToLower() == "membersonline20" &&
                    MembersOnlineManager.Instance.IsMOLRedesign30Enabled(g.Member, g.Brand))
                {
                    result = "IM (MOL - List View)";
                }
            }

            return result;
        }

        protected void SetHotlistIcons()
        {
            if (g.Member != null)
            {
                if (g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.JDateCoIL)
                {
                    imgPremiumAuthenticated.Visible = FrameworkGlobals.IsMemberPremiumAuthenticated(Member.MemberID, g.Brand);
                }

                HotListBar.LoadHotlistActivity(Member);
            }

        }

        protected void SetSayHi()
        {
            bool isSubscriber = false;
            if (g.Member != null && g.Member.IsPayingMember(g.Brand.Site.SiteID))
            {
                isSubscriber = true;
            }

            //set IM
            bool isOnline = false;
            string evar27ForIM = GetEvar27ForIM();
            
            OnlineLinkHelper onlineLinkHelper = OnlineLinkHelper.getOnlineLinkHelper(Matchnet.Web.Framework.Ui.BasicElements.ResultContextType.None, this.Member, g.Member, LinkParent.FullProfile, (int)PurchaseReasonType.AttemptToIMMiniProfileListView, out isOnline, evar27ForIM);
            if (!isOnline)
            {
                this.lnkIMOnline.Visible = false;
                this.txtIMOffline.Visible = true;
            }
            else
            {
                this.lnkIMOnline.NavigateUrl = onlineLinkHelper.OnlineLink;
                this.lnkIMOnline.Visible = true;
                this.txtIMOffline.Visible = false;
            }

            //set send e-card link
            if (g.EcardsEnabled)
            {
                // Generate mingle ecard page url. (to parameter is the username of the reciever)
                string destPageUrl = FrameworkGlobals.BuildConnectFrameworkLink("cards/categories.html?to=" + Member.GetUserName(_g.Brand) + "&MemberID=" + Member.MemberID.ToString() + "&return=1", g, true);

                // Assign url to ECard link.
                this.lnkEcard.NavigateUrl = destPageUrl;

                txtEcard.Visible = isSubscriber;
                txtEcardFree.Visible = !isSubscriber;
            }
            else
            {
                this.phEcardLink.Visible = false;
            }

            //set flirt/tease link
            if (_g.Member != null)
            {
                //set flirt/tease link
                this.lnkFlirt.NavigateUrl = ProfileDisplayHelper.GetFlirtLink(_g, Member.MemberID);
                ProfileDisplayHelper.AddFlirtOnClick(_g, ref this.lnkFlirt);
                ProfileDisplayHelper.AddFlirtOnClick(_g, ref this.lnkFlirt2);
            }
            else
            {
                this.lnkFlirt.NavigateUrl = g.GetLogonRedirect(_viewProfileUrl, null);
            }

            txtFlirt2.Visible = txtFlirt.Visible = isSubscriber;
            txtFlirtFree2.Visible = txtFlirtFree.Visible = !isSubscriber;

            //email link
            lnkEmail.NavigateUrl = ProfileDisplayHelper.GetEmailLink(Member.MemberID) + "&" + WebConstants.URL_PARAMETER_PURCHASEREASONTYPEID + "=" + PurchaseReasonType.AttemptToEmailMiniProfileListView.ToString("d");
        }

        protected void SetYNM()
        {
            if (g.IsYNMEnabled)
            {
                try
                {
                    //SecretAdmirer1.ExternalYNMIndicatorID = spanSecretAdmirerIndicator.ClientID;
                    SecretAdmirer1.ExternalYYIndicatorID = imgBothSaidYes.ClientID;
                    SecretAdmirer1.LoadSecretAdmirer(Member);
                    

                    ClickMask clickMask = g.List.GetClickMask(g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, this.Member.MemberID);

                    if ((((clickMask & ClickMask.TargetMemberYes) == ClickMask.TargetMemberYes) && ((clickMask & ClickMask.MemberYes) == ClickMask.MemberYes)))
                    {
                        imgBothSaidYes.Attributes["style"] = "";
                    }

                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine(ex.Message);
                }


            }
        }

        public string GetAboutMeEssay()
        {
            int MAXSTRINGLENGTH = 80;
            int MAXWORDLENGTH = 20;

            int viewerMemberID = (g.Member != null) ? g.Member.MemberID : Constants.NULL_INT;
            string Headline = FrameworkGlobals.Ellipsis(Member.GetAttributeTextApproved(g.Brand, "AboutMe", viewerMemberID, ""), MAXSTRINGLENGTH);

            if (Headline != String.Empty && Headline != null)
            {
                char[] delimiters = { ' ' };
                string[] contents = Headline.Split(delimiters);

                bool longWordFound = false;

                for (int i = 0; i < contents.Length; i++)
                {
                    if (contents[i].Length > MAXWORDLENGTH)
                    {
                        for (int j = 1; j <= contents[i].Length / MAXWORDLENGTH; j++)
                        {
                            if (contents[i].Length * j > MAXWORDLENGTH)
                            {
                                contents[i] = contents[i].Insert(MAXWORDLENGTH * j, " ");
                            }
                        }
                        longWordFound = true;
                    }
                }

                if (longWordFound)
                {
                    Headline = String.Join(" ", contents);
                }
            }
            else
            {
                Headline = "";
            }

            return Headline;

        }
        protected string GetEventType()
        {
            return FrameworkGlobals.SearchResultsM2MUpgradeEnabled(g) ? "hover" : "click";
        }
        #endregion
    }
}
