﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MiniSearch.ascx.cs" Inherits="Matchnet.Web.Framework.Ui.BasicElements.MiniSearch" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mnl" Namespace="Matchnet.Web.Framework.Ui.BasicElements.Links" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mnbe" Namespace="Matchnet.Web.Framework.Ui.BasicElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="uc1" TagName="NoPhoto" Src="../PageElements/NoPhoto20.ascx" %>

<asp:PlaceHolder ID="phMiniSearch" runat="server" Visible="false">
    <asp:PlaceHolder ID="phDivMiniSearchOpen" runat="server">
    <div id="divMiniSearch">
    </asp:PlaceHolder>
        <div id="photos-header">
            <%=H3TagOpen%><mn2:FrameworkLiteral ID="literalTitle" runat="server" ResourceConstant="TXT_TITLE_DEFAULT"></mn2:FrameworkLiteral><%=H3TagClose%>&nbsp;<mnl:Link ID="linkBack" runat="server" ResourceConstant="TXT_BACK_LINK_DEFAULT" NavigateUrl="#"></mnl:Link>
        </div>
        
        <div id="mini-search-block">   
        <table cellpadding="0" cellspacing="0">
            <tr>
                <asp:PlaceHolder ID="phPreviousPage" runat="server" Visible="false">
    	        <td class="photos-arrows image-text-pair" ID="tdPrevious" runat="server">
	                <span><mn2:FrameworkLiteral ID="literalPreviousPage" runat="server" ResourceConstant="TXT_PREVIOUS_PAGE"></mn2:FrameworkLiteral></span>
	                <mn:Image ID="imgPrevious" runat="server" FileName="btn-previous-minisearch.png" />
                </td>
                </asp:PlaceHolder>
                <td>
<%--                    <div id="photos-header">
                        <h3><mn2:FrameworkLiteral ID="literalTitle" runat="server" ResourceConstant="TXT_TITLE_DEFAULT"></mn2:FrameworkLiteral></h3>&nbsp;<mnl:Link ID="linkBack" runat="server" ResourceConstant="TXT_BACK_LINK_DEFAULT" NavigateUrl="#"></mnl:Link>
                    </div>--%>
                    <div id="photos-min">
                        <asp:Repeater ID="repeaterProfiles" runat="server">
                        <ItemTemplate>
                            <asp:Panel ID="panelImgThumb" runat="server"><mn:Image runat="server" alt="" id="imgThumb" /></asp:Panel>
	                        <uc1:NoPhoto runat="server" id="noPhoto" class="no-photo" Width="" Height="" visible="False"  />
                        </ItemTemplate>
                        </asp:Repeater>
                    </div>
                    <asp:PlaceHolder ID="phAjaxLoading" runat="server">
                        <!--ajax loading-->
                        <div id="divMiniSearchAjaxLoading" style="display:none;">
                            <p class="loading">
                                <mn2:FrameworkLiteral ID="literalLoading" runat="server" ResourceConstant="TXT_LOADING"></mn2:FrameworkLiteral>
                                <br/>
                                <mn2:FrameworkLiteral ID="literalLoadingTitle" runat="server" ResourceConstant="TXT_TITLE_DEFAULT"></mn2:FrameworkLiteral>
                                &hellip;
                            </p>
                            <mn:Image ID="Image1" runat="server" FileName="ajax-loader.gif" alt="" />
                        </div>
                    </asp:PlaceHolder> 		                    
                </td>
                <asp:PlaceHolder ID="phNextPage" runat="server" Visible="false">
                <td class="photos-arrows image-text-pair" ID="tdNext" runat="server">
	                <span><mn2:FrameworkLiteral ID="literalNextPage" runat="server" ResourceConstant="TXT_NEXT_PAGE"></mn2:FrameworkLiteral></span>
	                <mn:Image ID="imgNext" runat="server" FileName="btn-next-minisearch.png" />
                </td>
                </asp:PlaceHolder>
            </tr>
        </table> 
        </div>
    <asp:PlaceHolder ID="phMarketingCopy" runat="server">
        <!--marketing copy-->
        <div id="divMiniSearchMarketingCopy">
            <mn2:FrameworkLiteral ID="literalMarketingClose" runat="server" ResourceConstant="TXT_CLOSE"></mn2:FrameworkLiteral>
            <mn2:FrameworkLiteral ID="literalMarketingHeader" runat="server" ResourceConstant="TXT_HEADER"></mn2:FrameworkLiteral>        
            <mn2:FrameworkLiteral ID="literalMarketingCopy" runat="server" ResourceConstant="TXT_MARKETING_COPY"></mn2:FrameworkLiteral>
        </div>
    </asp:PlaceHolder>

   
    <asp:PlaceHolder ID="phDivMiniSearchClose" runat="server">
    </div>
    </asp:PlaceHolder>       
    
</asp:PlaceHolder>