﻿(function ($) {
    // create secretAdmirer space in spark namespace
    __addToNamespace__('spark.secretAdmirer');

    //function variables
    var slideData = {},
        prefData = {},
        beautyBeastPromotionOmniture = '';


    function recordYNMVoteForSlideshowHeaderOnProfilePage(headerData, button) {
        //console.log(headerData);
        var url = "/Applications/Home/SlideshowProfileAPI.aspx?";
        url = url + "ynmp=" + headerData.ynmparams;
        url = url + "&YNMLT=" + headerData.clicktype;
        url = url + "&asl=false";
        url = url + "&" + headerData.urlparamslideshowdisplaytype + "=" + headerData.displaytype;
        url = url + "&displayedRight=false";

        jQuery.get(url, function (data) {
        }).always(function () {
            window.location.href = $(button).attr("href");
        });
    }

    //function recordYNMVoteAndGetNewSlideshowProfile(params, type, memberID, displayedInRightControl, reloadContent, parentDivId) {
    function recordYNMVoteAndGetNewSlideshowProfile(containerDivId, clickType) {

        slideData = jQuery('#' + containerDivId).data();

        //console.log(slideData);

        //block with loading div
        spark_blockSAProfile('#' + slideData.outercontainerdivid);

        var omniturePageName = '';
        if (s != null) {
            omniturePageName = s.pageName;
        }

        var url = "/Applications/Home/SlideshowProfileAPI.aspx?";
        url = url + "ynmp=" + slideData.ynmparams;
        url = url + "&YNMLT=" + clickType;
        url = url + "&asl=" + slideData.reloadcontent;
        url = url + "&" + slideData.urlparamslideshowdisplaytype + "=" + slideData.displaytype;
        url = url + "&" + slideData.urlparamslideshowoutercontainerdiv + "=" + slideData.outercontainerdivid;
        url = url + "&" + slideData.urlparamslideshowmode + "=" + slideData.slideshowmode;
        url = url + "&" + slideData.urlparamslideshowomniturepagename + "=" + slideData.omniturepagename;
        url = url + "&displayedRight=" + slideData.displayedinrightcontrol;
        url = url + "&omniturepage=" + omniturePageName.toString();

        if (slideData.showupdatedwidestyle) {
            url = url + "&" + slideData.urlparamslideshowshowupdatedwidestyle + "=true";
        }

        jQuery.get(url, function (data) {
            if (slideData.reloadcontent) {
                jQuery('#' + slideData.outercontainerdivid).find('.slideshow').html(data);
                $('#' + slideData.outercontainerdivid).find('.block-on-load').unblock();

                if (slideData.reloadads) {
                    refreshTop728by90SR();
                    refreshRight300by250SR();
                }

                if (slideData.populateynmbuckets) {
                    var ynmbucket = "";

                    switch (clickType) {
                        case 1:
                            ynmbucket = 'Yes';
                            break;
                        case 2:
                            ynmbucket = 'No';
                            break;
                        case 3:
                            ynmbucket = 'Maybe';
                            break;
                    }

                    GetYNMMember(slideData.slideshowmemberid, ynmbucket, YNMSucceededGetMember, YNMFailedGetMember);
                }

                if (s != null && s != undefined) {
                    var originalPageName = s.pageName;
                    var typeString;
                    PopulateS(true); //clear existing values in omniture "s" object

                    switch (clickType) {
                        case 1:
                            typeString = 'Y';
                            break;
                        case 2:
                            typeString = 'N';
                            break;
                        case 3:
                            typeString = 'M';
                            break;
                    }

                    var omniturePageNameToUse = (slideData.omniturepagename == undefined) ? originalPageName : slideData.omniturepagename;

                    s.pageName = originalPageName + " - Secret Admirer";
                    s.prop30 = typeString + ' - ' + omniturePageNameToUse;
                    if (typeof (slideData.beautybeastpromotionomniture) != 'undefined' && slideData.beautybeastpromotionomniture != '') {
                        s.prop30 = s.prop30 + ' - ' + slideData.beautybeastpromotionomniture;
                    }
                    s.prop31 = slideData.slideshowmemberid;
                    s.t(); //send omniture updated values as page load
                    s.pageName = originalPageName;
                }
            }
        });

    }

    function YNMSucceededGetMember(result) {
        if (result.d.MemberID >= 0) {
            //var microPrf = $j("#MictoProfileTemplate").render(result.d);
            // console.log("v 1");
            // console.log(result.d.YNMVote);
            var $yesBucket = $j('#yesBucket');
            var $noBucket = $j('#noBucket');
            var $maybeBucket = $j('#maybeBucket');
            switch (result.d.YNMVote) {
                /* case "Yes":
                $j("#MictoProfileTemplate").render(result.d).prependTo("#divMicroProfileInjectYesBucket");
                break;
                case "No":
                $j("#MictoProfileTemplate").render(result.d).prependTo("#divMicroProfileInjectNoBucket");
                break;
                case "Maybe":
                $j("#MictoProfileTemplate").render(result.d).prependTo("#divMicroProfileInjectMaybeBucket");
                break;*/ 
                case "Yes":
                    $j("#MictoProfileTemplate").render(result.d).prependTo($j('.carousel>ul', $yesBucket)).show('blind');
                    /*$yesBucket.find('.carousel').jCarouselLite({
                    btnNext: "#yesBucket>.next",
                    btnPrev: "#yesBucket>.prev",
                    circular: false,
                    vertical: true,
                    visible: 4
                    });*/
                    //$yesBucket.find('.home-your-matches:gt(3)').remove();
                    if ($yesBucket.find('.home-your-matches').length <= 4) {
                        $yesBucket.find('.your-matches-view-more-btm').hide();
                    } else {
                        $yesBucket.find('.home-your-matches:gt(3)').remove().end().find('.your-matches-view-more-btm').appendTo($yesBucket).fadeIn('fast');
                    };
                    break;
                case "No":
                    $j("#MictoProfileTemplate").render(result.d).prependTo($j('.carousel>ul', $noBucket)).show('blind');
                    /*$noBucket.find('.carousel').jCarouselLite({
                    btnNext: "#noBucket>.next",
                    btnPrev: "#noBucket>.prev",
                    circular: false,
                    vertical: true,
                    visible: 4
                    });*/
                    //$noBucket.find('.home-your-matches:gt(3)').remove();
                    if ($noBucket.find('.home-your-matches').length <= 4) {
                        $noBucket.find('.your-matches-view-more-btm').hide();
                    } else {
                        $noBucket.find('.home-your-matches:gt(3)').remove().end().find('.your-matches-view-more-btm').appendTo($noBucket).fadeIn('fast');
                    };
                    break;
                case "Maybe":
                    $j("#MictoProfileTemplate").render(result.d).prependTo($j('.carousel>ul', $maybeBucket)).show('blind');
                    /*$maybeBucket.find('.carousel').jCarouselLite({
                    btnNext: "#maybeBucket>.next",
                    btnPrev: "#maybeBucket>.prev",
                    circular: false,
                    vertical: true,
                    visible: 4
                    });*/
                    //$maybeBucket.find('.home-your-matches:gt(3)').remove();     
                    if ($maybeBucket.find('.home-your-matches').length <= 4) {
                        $maybeBucket.find('.your-matches-view-more-btm').hide();
                    } else {
                        $maybeBucket.find('.home-your-matches:gt(3)').remove().end().find('.your-matches-view-more-btm').appendTo($maybeBucket).fadeIn('fast');
                    };
                    break;
            }
        }
    }

    function SaveSettings() {
        //NOTE: BeginSaveSettings must be defined in the control/page that contains this control 

        BeginSaveSettings();

        var omniturePageName = '';
        if (s != null) {
            omniturePageName = s.pageName;
        }

        //var prefData = jQuery('#' + containerDivId).data();

        var minAge = jQuery('#' + prefData.ageminclientid).val();

        if (minAge < 18) {
            jQuery('#' + prefData.ageminclientid).text(18);
        }

        var maxAge = jQuery('#' + prefData.agemaxclientid).val();

        if (maxAge > 99) {
            jQuery('#' + prefData.agemaxclientid).text(99);
        }

        var tempMin = minAge;
        var tempMax = maxAge;

        if (minAge > maxAge) {
            maxAge = tempMin;
            minAge = tempMax;
        }

        var regionID = jQuery('#' + prefData.regionclientid).val();
        var searchType = jQuery('#' + prefData.searchtypeclientid).val();
        var areaCodes = jQuery('#' + prefData.areacodelistclientid).val();

        var url = "/Applications/Home/SlideshowProfileAPI.aspx?";
        url = url + "&" + prefData.urlparamslideshowdisplaytype + "=" + prefData.displaytype;
        url = url + "&" + prefData.urlparamslideshowmode + "=" + prefData.slideshowmode;
        url = url + "&" + prefData.urlparamslideshowoutercontainerdiv + "=" + prefData.outercontainerdivid;
        url = url + "&" + prefData.urlparamslideshowminage + "=" + minAge;
        url = url + "&" + prefData.urlparamslideshowmaxage + "=" + maxAge;
        url = url + "&" + prefData.urlparamslideshowregionid + "=" + regionID;
        url = url + "&" + prefData.urlparamslideshowsearchtype + "=" + searchType;
        url = url + "&" + prefData.urlparamslideshowareacodelist + "=" + areaCodes;
        url = url + "&omniturepage=" + omniturePageName.toString();

        if (prefData.showupdatedwidestyle) {
            url = url + "&" + prefData.urlparamslideshowshowupdatedwidestyle + "=true";
        }

        jQuery.get(encodeURI(url), function (data) {
            //NOTE: EndSaveSettings must be defined in the control/page that contains this control            
            EndSaveSettings(data);

        });
    }

    function YNMFailedGetMember(result) {
        console.log(result);
    }

    function BeginSaveSettings() {
        //console.log('BeginSaveSettings');
        //$('#' + dataObject.outercontainerdivid).find(".pref-form").slideUp();
        $(".pref-form").slideUp();
        spark_blockSAProfile('#' + slideData.outercontainerdivid);
    }

    function EndSaveSettings(data) {
        //console.log('EndSaveSettings');
        $('#' + slideData.outercontainerdivid).find('.slideshow').html(data);
        //$('#slideshow').html(data);
        $('#' + slideData.outercontainerdivid).find('.block-on-load').unblock();
        refreshTop728by90SR();
        refreshRight300by250SR();
    }

    function spark_blockSAProfile(element) {
        $j(element).find('.block-on-load').block({
            message: '<div class="loading spinner-only" />',
            overlayCSS: { backgroundColor: 'white', opacity: '0.5', cursor: 'auto', borderRadius: '8px' },
            css: {
                backgroundColor: null,
                border: null,
                width: '100%',
                height: '100%',
                top: '0',
                left: '0'
            }
        });
    }

    // bind modal event to namespace
    $(spark.secretAdmirer).bind('secretAdmirerModal', function () {
        $('#slideshowSecretAdmirerPopup').dialog({
            width: 650,
            modal: true,
            dialogClass: 'secret-admirer-modal ui-dialog-plain',
            open: function () {
                var $this = $(this),
                closeTxt = $this.find('[id*=slideshowSecretAdmirerPopupData]').data('modal-close'),
                closeHtml = $('<div class="ui-dialog-plain-close link-style">' + closeTxt + '<span class="spr s-icon-closethick-color"></span></div>').bind('click', function () {
                    $this.dialog('close');
                });
                $this.parent().find('.ui-dialog-plain-close').remove();
                $this.parent().append(closeHtml);

                var omniturePageName = $this.find('[id*=slideshowSecretAdmirerPopupData]').data('omniturepagename');

                if (s != null && s != undefined) {
                    PopulateS(true);
                    s.pageName = omniturePageName;
                    s.t();
                }

            }
        });
    });

    // add jQuery dom-ready stuff
    $(function () {
        $('#slideshow-inner').find('.spr-btn').live('click', function () {
            var buttonData = $(this).data();
            //console.log('clicked: ' + $(this).attr('class'));
            //console.log(buttonData);
            recordYNMVoteAndGetNewSlideshowProfile(buttonData.containerdivid, buttonData.clicktype);
        });

        $('#admirerWrapper').find('.click-btn').live('click', function () {
            var buttonData = $(this).data();
            //console.log('clicked: ' + $(this).attr('class'));
            //console.log(buttonData);
            recordYNMVoteAndGetNewSlideshowProfile(buttonData.containerdivid, buttonData.clicktype);
        });

        $('#fp-click20').find('.click-btn').live('click', function () {
            //event.preventDefault();
            var buttonData = $(this).data();
            //console.log('clicked: ' + $(this).attr('class'));
            //console.log(buttonData);
            recordYNMVoteForSlideshowHeaderOnProfilePage(buttonData, this);
            return false;
        });

        //        $('#secretAdmirerModalBtn').click(function (event) {
        //            $(spark.secretAdmirer).trigger("secretAdmirerModal");
        //        });

        if ($('#slideshowSecretAdmirerPopup').find('[id*=slideshowSecretAdmirerPopupData]').data('launch-modal')) {
            $(spark.secretAdmirer).trigger("secretAdmirerModal");

        }

        //bind preferences toggler
        $(".pref-wrapper").live('click', function (event) {
            event.preventDefault();
            $(this).next(".pref-form").slideToggle();
        });

        $(".pref-form a.pref-close").live('click', function (event) {
            event.preventDefault();
            $(this).closest(".pref-form").slideUp();
        });

        //bind preferences button
        $(".seeking .btn").live('click', function (event) {
            event.preventDefault();
            slideData = $(this).closest('[data-slideshowmode]').data();
            prefData = $(this).closest('[id*=divSlideshowPreferencesData]').data();

            //console.log(slideData);
            //console.log(prefData);

            SaveSettings();
        });

    });

} (jQuery));