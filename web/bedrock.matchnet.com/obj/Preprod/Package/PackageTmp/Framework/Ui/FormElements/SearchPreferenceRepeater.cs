using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;

using Spark.SAL;
using System.Collections.Generic;
using Matchnet.Web.Applications.Search.XMLPreference;

namespace Matchnet.Web.Framework.Ui.FormElements
{
	/// <summary>
	/// Summary description for SearchPreferenceRepeater.
	/// </summary>
	public class SearchPreferenceRepeater : Repeater
	{
		private MemberSearch _memberSearch;
        private bool isXMLNode = true;

		public SearchPreferenceRepeater()
		{
			ItemTemplate = new SearchPreferenceItemTemplate();
			this.ItemDataBound += new RepeaterItemEventHandler(SearchPreferenceRepeater_ItemDataBound);
		}

        /// <summary>
        /// XML representation of preferences list from xml file
        /// </summary>
		public XmlNode XmlNode
		{
			set
			{
                isXMLNode = true;
				DataSource = value.ChildNodes;
			}
		}

        /// <summary>
        /// Object representation of preferences list from xml file
        /// </summary>
        public List<SearchSectionPreference> SearchSectionPreferenceList
        {
            set
            {
                isXMLNode = false;
                DataSource = value;
            }
        }

		public MemberSearch MemberSearch
		{
			get
			{
				return _memberSearch;
			}
			set
			{
				_memberSearch = value;
			}
		}

		private void SearchPreferenceRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
		{
            if (isXMLNode)
                XML_ItemDataBound(sender, e);
            else
                SearchSectionPreferenceList_ItemDataBound(sender, e);
		}

        private void XML_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            XmlNode preferenceNode = (XmlNode)e.Item.DataItem;

            if (preferenceNode.Name.ToLower() != "preference")
            {
                throw new Exception("Invalid node name: " + preferenceNode.Name + ".  Expected 'Preference'.");
            }

            string preferenceName = preferenceNode.Attributes["Name"].Value;
            string resourceConstant = preferenceNode.Attributes["ResourceConstant"].Value;

            SearchPreferenceControl preference = (SearchPreferenceControl)e.Item.FindControl("Preference");

            preference.ResourceConstant = resourceConstant;
            preference.MemberSearchPreference = _memberSearch[Preference.GetInstance(preferenceName)];
            preference.Collapsible = Matchnet.Conversion.CBool(preferenceNode.Attributes["Collapsible"].Value);

            if (preferenceNode.Attributes["AutoSelectHigherValues"] != null)
            {
                preference.AutoSelectHigherValues = Matchnet.Conversion.CBool(preferenceNode.Attributes["AutoSelectHigherValues"].Value);
            }
        }

        private void SearchSectionPreferenceList_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            SearchSectionPreference ssp = e.Item.DataItem as SearchSectionPreference;
            if (ssp != null)
            {
                SearchPreferenceControl preference = (SearchPreferenceControl)e.Item.FindControl("Preference");
                preference.ResourceConstant = ssp.ResourceConstant;
                preference.MemberSearchPreference = _memberSearch[Preference.GetInstance(ssp.Name)];
                preference.Collapsible = ssp.Collapsible;
                preference.AutoSelectHigherValues = ssp.AutoSelectHigherValues;
            }
        }
	}

	public class SearchPreferenceItemTemplate : ITemplate
	{
		#region ITemplate Members

		public void InstantiateIn(Control container)
		{
			SearchPreferenceControl preference = new SearchPreferenceControl();
			preference.ID = "Preference";
			container.Controls.Add(preference);
		}

		#endregion
	}
}
