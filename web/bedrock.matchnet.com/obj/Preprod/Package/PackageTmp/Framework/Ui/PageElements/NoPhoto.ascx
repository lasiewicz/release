<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Control Language="c#" AutoEventWireup="false" Codebehind="NoPhoto.ascx.cs" Inherits="Matchnet.Web.Framework.Ui.PageElements.NoPhoto" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<a runat="server" id="lnkNoPhoto">
	<table runat="server" id="tblNoPhoto" cellpadding="0" cellspacing="0" align="center" style="width:80px; height: 104px;" class="noPhoto" >
		<tr>
			<td align="center">	
				<mn:txt runat="server" id="txtAskForPhoto" ResourceConstant="TXT_ASK_ME_FOR_MY_PHOTO" />
			</td>
		</tr>
	</table>
</a>

