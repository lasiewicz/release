<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="uc1" TagName="ExternalFeedback" Src="~/Framework/Ui/PageElements/ExternalFeedback.ascx" %>
<%@ Register TagPrefix="uc2" TagName="OmnidateHeadCode" Src="~/Applications/Omnidate/Controls/HeadCode.ascx" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Head20.ascx.cs" Inherits="Matchnet.Web.Framework.Ui.Head20" %>
<head>
    <mn:Txt ID="txtDynamicYieldHeadCode" runat="server" ResourceConstant="TXT_DYNAMIC_YIELD_HEAD_CODE" Visible="False" />
    <meta charset="utf-8">
    <title><asp:Literal runat="server" ID="litTitle" /></title>
    <!-- 
	DevelopmentMode: <%= g.IsDevMode %>
	Host: <%= System.Environment.MachineName %>
	URL: <%= g.Brand.Site.DefaultHost + "." + g.Brand.Uri %> 
	CommunityID: <%= g.Brand.Site.Community.CommunityID %>
	PromotionID: <%= g.Session["PromotionID"] %> 
	Luggage: <%= g.Session["Luggage"]%>
	BannerID: <%= g.Session["BannerID_Tracking"]%>
	LandingPageID: <%= g.Session["LandingPageID"]%>
	MOL: <%=GetMOLCountHelper()%>
	Template: <%= g.LayoutTemplate.ToString() %>
	PageID: <%= g.AppPage.ID %>
    <%=GetSecondsSinceReg() %>
	-->
    <asp:Literal runat="server" ID="litMeta" />
    <asp:Literal runat="server" ID="litMetaDescription" />
    <asp:Literal runat="server" ID="litGoogleWebmaster" />
    <asp:Literal runat="server" ID="litBingWebmaster" />
    <link rel="shortcut icon" href="<%=GetShortcutIconURL()%>" />
    <asp:PlaceHolder ID="phCSSStructIncludes" runat="server">
        <link rel="stylesheet" type="text/css" href="" runat="server" id="structStyleSheet" />
    </asp:PlaceHolder>
    <link rel="image_src" type="image/jpeg" href="http://www.<%= g.Brand.Uri + GetPreviewImgURL()%>" />
    <meta property="og:image" content="http://www.<%= g.Brand.Uri + GetPreviewImgURL()%>" />
    <asp:PlaceHolder ID="phCSSIncludes" runat="server">
        <link rel="stylesheet" type="text/css" href="" runat="server" id="styleStyleSheet" />
    </asp:PlaceHolder>
    <asp:PlaceHolder ID="PlaceHolderSplashCSS" runat="server" visible="false">
        <link rel="stylesheet" type="text/css" href="css/splash.<%=g.Brand.Site.SiteID%>.css" media="all" />
    </asp:PlaceHolder>
    <asp:PlaceHolder ID="plcOnePageReg" runat="server" visible="false">
        <link rel="stylesheet" type="text/css" href="css/splash-one-page-reg.<%=g.Brand.Site.SiteID%>.css" media="all" />
    </asp:PlaceHolder>
     <asp:PlaceHolder ID="plcCollageStyle" runat="server" visible="false">
	    <link rel="stylesheet" href="/css/splash.collageWithControls.103.css"  type="text/css" media="all"/>
	</asp:PlaceHolder>
    <asp:PlaceHolder ID="plcMediaPrintCSS" runat="server" visible="false">
        <!--[if lte IE 8]>
        <link rel="stylesheet" type="text/css" href="css/style.<%=g.Brand.Site.SiteID%>.print.css" media="print" />
        <![endif]-->
    </asp:PlaceHolder>
    <%=GetCanonicalTag()%>
    <asp:PlaceHolder ID="PlaceHolderGAM" runat="server"></asp:PlaceHolder>
    <asp:PlaceHolder id="PlaceHolderBlank" runat="Server" visible="true">
        <script src="/javascript20/library/modernizr2-custom.min.js"></script>
        <%--third party js--%><script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script><%--must be in noConflict mode--%>
        <%--jQuery CDN fallback--%><script>window.jQuery || document.write('<script src="/javascript20/library/jquery-1.7.1.min.js">\x3C/script>')</script>
        <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/jquery-ui.min.js"></script>
        <%--jQueryUI CND fallback--%><script>
                                         window.jQuery.ui || document.write('<script src="/javascript20/library/jquery-ui-1.8.16.min.js">\x3C/script>');
                                         Modernizr.load({
                                             test : Modernizr.canvas,
                                             nope : '/javascript20/library/excanvas.compiled.js'
                                         });
                                     </script>
        <script src="/javascript20/library/jquery-plugins.js"></script>
        <script src="/javascript20/library/qTransform.js"></script>
        <script src="/javascript20/library/strophe/strophe.min.js"></script>
        <script src="/javascript20/library/strophe/strophe.xdomainrequest.js"></script>
        <%--Put jQuery in no conflict mode and assign '$j' shortcut--%><script>var $j = jQuery.noConflict();</script>
        <script src="<asp:Literal runat='server' id='litJavascriptMain'/>"></script>
        <asp:PlaceHolder ID="phJavascriptLibrary" runat="server">
            <script src="<asp:Literal runat='server' id='litJavascript' text='/javascript20/library/javascript.js' />"></script>
        </asp:PlaceHolder>
        <script src="/javascript20/library/mustache.js"></script>
        <asp:PlaceHolder Runat="server" ID="phAslMainScript"></asp:PlaceHolder>
        <asp:Literal runat="server" ID="litAdditionalContent" />
        <mn:Txt ID="TxtPreloadImages" runat="server" ResourceConstant="TXT_PRELOAD_IMAGES_HEAD">
        </mn:Txt>
        <uc1:ExternalFeedback ID="ExternalFeedback1" runat="server" OutputTag="Head" />
        <uc2:OmnidateHeadCode ID="OmnidateHeadCode" runat="server" />
    </asp:PlaceHolder>
    <asp:PlaceHolder ID="plcPassCallWAPRedirect" runat="server" Visible="false">
        <script src="/javascript20/jdate_pcmdt.js"></script>
    </asp:PlaceHolder>
    <asp:PlaceHolder ID="plcSlideShowJS" runat="server">
        <script src="/Framework/UI/BasicElements/Slideshow.js"></script>
     </asp:PlaceHolder>
     <asp:PlaceHolder runat="server" ID="phFacebookLikes" visible="false">
        <script src="/javascript20/facebooklikes.js"></script>
     </asp:PlaceHolder>
     <script src="/javascript20/spark.messageconversion.js"></script>
     <script type="text/javascript">
         spark.messageconversion.messageAttachmentURLPath = '<%=JSMessageAttachmentURLPath%>';
         (function() {
             __addToNamespace__('spark.util', {
                 BrandId: '<%=g.Brand.BrandID%>',
                 //the following was added to support Google Tag Manager custom variables
                 MemberID: '<%=(g.Member!=null ? g.Member.MemberID.ToString() : string.Empty)%>',
                 Gender: '<%=GetGenderText() %>',
                 PRM: '<%= g.Session["PromotionID"] %>',
                 LGID: '<%= g.Session["Luggage"]%>',
                 MemberStatus: '<%=GetMemberStatus()%>',
                 SeekingGender: '<%=GetSeekingGenderText() %>',
                 Age: '<%=GetAge() %>'
                 }
             );
         })();
     </script>
     <mn:Txt ID="TxtFbRemarketingPixel" runat="server" ResourceConstant="FB_REMARKETING_PIXEL" />
     <mn:Txt ID="txtYoyoRemarketingCode" runat="server" ResourceConstant="JAVASCRIPT_FB_REMARKETING_PIXEL"/>
     <mn:Txt ID="txtAdNginCode" runat="server" ResourceConstant="JAVASCRIPT_ADNGIN_PIXEL"/>

    <asp:PlaceHolder runat="server" ID="phCORS" visible="false">
        <script src="/javascript20/library/flXHR/flXHR.js"></script>
        <script src="/javascript20/library/flXHR/jquery.flXHRproxy.js"></script>
        <script type="text/javascript">
            var spark = spark || {};
            spark.cors = spark.cors || {
                enabled: false,
                restAuthority: '<%=_RESTAuthority%>',
                log: function (message) {
                    if (typeof (console) !== "undefined") {
                        console.log(message);
                    }
                },
                useFlXHRIfNeeded: function () {
                    if (!this.isCorsSupported() && !this.enabled) {
                        this.log("Cors is not supported, registering flXHRproxy");
                        this.enabled = true;
                        //register included urls
                        $j.flXHRproxy.registerOptions(this.restAuthority, { xmlResponseText: false });
                        //register excluded url 
                        //$.flXHRproxy.registerExclusionPaths('/mail/uploadattachmentsform');
                        // set flXHR as the default XHR object used in jQuery AJAX requests
                        //$.ajaxSetup({transport:'flXHRproxy'});
                    }
                },
                isCorsSupported: function () {
                    return !!(window.XMLHttpRequest && 'withCredentials' in new XMLHttpRequest());
                },
            };

            $j(function () {
                //set up cors
                spark.cors.useFlXHRIfNeeded();
            });

        </script>
    </asp:PlaceHolder>

    <%--CONSOLIDATED PRESENCE/IM SUPPORT--%>
    <%--Note: Refactor out to config or db setting for things we find will be changing often--%>
    <asp:PlaceHolder runat="server" ID="phConsolidatedPresenceDevStage" Visible="false">
        <script type="text/javascript" src="//connect.stgv3.<%=g.Brand.Uri%>/js/jquery_noty_min-1.js"></script>
        <script type="text/javascript" src="//connect.stgv3.<%=g.Brand.Uri%>/js/jquery_cookie-1.js"></script>
        <script type="text/javascript" src="//connect.stgv3.<%=g.Brand.Uri%>/js/strophe_113-1.js"></script>
        <script type="text/javascript" src="//connect.stgv3.<%=g.Brand.Uri%>/js/presenceclient-19.js"></script>

        <script type="text/javascript">

            (function ($) {
                $(document).ready(function () {
                    $(document).trigger('connect_ejabberd', {
                        memberid: '<%=(g.Member!=null ? g.Member.MemberID.ToString() : string.Empty)%>',
                        communityid: '<%=g.Brand.Site.Community.CommunityID%>',
                        siteid: '<%=g.Brand.Site.SiteID%>',
                        brandid: '<%=g.Brand.BrandID%>',
                        api_url: 'http://api.bhstage.securemingle.dev',
                        ejabberd_server: 'lastgejabberd301u.stgv3.spark.net',
                        ejabberd_server_port: '80',
                        ejabberd_server_protocol: 'http://',
                        paid_status: <%=(g.Member!=null ? g.Member.IsPayingMember(g.Brand.Site.SiteID).ToString().ToLower() : "false")%>,
                        site_domain: 'stgv3.<%=g.Brand.Uri%>',
                        online_menu: 6,
                        language: '<%=_g.Brand.Site.CultureInfo.TwoLetterISOLanguageName%>',
                        upgradeUrl: 'http://stage3.<%=g.Brand.Uri%>/Applications/Subscription/Subscribe.aspx?prtid=<%=_IMPrtId%>&SparkWS=true&DestinationURL=http%3A%2F%2Fconnect.stgv3.<%=g.Brand.Uri%>%2Fchat%2F',
                        site_type: 'la'
                    });
                });
            })(jQuery);

            <%--Spark.js var to Control mol/Im behavior--%>
            isSparkMOLConsolidationEnabled = <%=IsSparkMOLConsolidationEnabled.ToString().ToLower()%>;
            isSparkWebchatConsolidationEnabled = <%=IsSparkWebchatConsolidationEnabled.ToString().ToLower()%>;

        </script>

    </asp:PlaceHolder>

    <asp:PlaceHolder runat="server" ID="phConsolidatedPresenceProd" Visible="false">
        <script type="text/javascript" src="//connect.<%=g.Brand.Uri%>/js/jquery_noty_min-1.js"></script>
        <script type="text/javascript" src="//connect.<%=g.Brand.Uri%>/js/jquery_cookie-1.js"></script>
        <script type="text/javascript" src="//connect.<%=g.Brand.Uri%>/js/strophe_113-1.js"></script>
        <script type="text/javascript" src="//connect.<%=g.Brand.Uri%>/js/presenceclient-19.js"></script>

        <script type="text/javascript">

            (function ($) {
                $(document).ready(function () {
                    $(document).trigger('connect_ejabberd', {
                        memberid: '<%=(g.Member!=null ? g.Member.MemberID.ToString() : string.Empty)%>',
                        communityid: '<%=g.Brand.Site.Community.CommunityID%>',
                        siteid: '<%=g.Brand.Site.SiteID%>',
                        brandid: '<%=g.Brand.BrandID%>',
                        api_url: 'https://api.securemingle.com',
                        ejabberd_server: 'im.spark.net',
                        ejabberd_server_port: '443',
                        ejabberd_server_protocol: 'https://',
                        paid_status: <%=(g.Member!=null ? g.Member.IsPayingMember(g.Brand.Site.SiteID).ToString().ToLower() : "false")%>,
                        site_domain: '<%=g.Brand.Uri%>',
                        online_menu: 6,
                        language: '<%=_g.Brand.Site.CultureInfo.TwoLetterISOLanguageName%>',
                        upgradeUrl: 'http://www.<%=g.Brand.Uri%>/Applications/Subscription/Subscribe.aspx?prtid=<%=_IMPrtId%>&SparkWS=true&DestinationURL=http%3A%2F%2Fconnect.<%=g.Brand.Uri%>%2Fchat%2F',
                        site_type: 'la'
                    });
                });
            })(jQuery);

            <%--Spark.js var to Control mol/Im behavior--%>
            isSparkMOLConsolidationEnabled = <%=IsSparkMOLConsolidationEnabled.ToString().ToLower()%>;
            isSparkWebchatConsolidationEnabled = <%=IsSparkWebchatConsolidationEnabled.ToString().ToLower()%>;
        </script>

    </asp:PlaceHolder>
</head>

