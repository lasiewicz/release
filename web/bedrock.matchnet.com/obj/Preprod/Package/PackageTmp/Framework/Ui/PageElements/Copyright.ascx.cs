namespace Matchnet.Web.Framework.Ui.PageElements
{
	using System;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;

	/// <summary>
	///		Summary description for Copyright.
	/// </summary>
	public class Copyright : System.Web.UI.UserControl
	{
		#region Static Form Elements
		protected Matchnet.Web.Framework.Txt txtTrademark;
		#endregion
		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			string[] args = new string[1];
			DateTime dt=DateTime.Now;
			string year=String.Format("{0:yyyy}",dt);
			args[0] = year;
			txtTrademark.Args=args;
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
