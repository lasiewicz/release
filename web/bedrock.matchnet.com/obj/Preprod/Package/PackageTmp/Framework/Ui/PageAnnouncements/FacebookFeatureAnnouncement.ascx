﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FacebookFeatureAnnouncement.ascx.cs" Inherits="Matchnet.Web.Framework.Ui.PageAnnouncements.FacebookFeatureAnnouncement" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>

<%--Facebook feature prompt--%>
<div id="FBFeatureAnnouncement" class="feature-announcement plain">
    <h2><mn:Txt ID="txtAnnouncementTitle" ResourceConstant="TXT_ANNOUNCEMENT_TITLE" runat="server" /></h2>
    <a class="click-close" href="#"><mn:Txt ID="txtClose" ResourceConstant="TXT_CLOSE" runat="server" /></a>
    <blockquote><mn:Txt ID="txtAnnouncementIntro" ResourceConstant="TXT_ANNOUNCEMENT_INTRO" runat="server" /></blockquote>
    <a href="#tabBookmark" class="btn-fb likes" id="lnkLikes" runat="server"><span class="spr s-icon-fb-sq-sm"></span><mn:Txt ID="btnFBLikes" ResourceConstant="TXT_BTN_FB_LIKES" runat="server" /></a>
    <a href="/Applications/MemberProfile/MemberPhotoEdit.aspx" class="btn-fb" id="lnkFBPhoto" runat="server"><span class="spr s-icon-fb-sq-sm"></span><mn:Txt ID="btnFBPhotos" ResourceConstant="TXT_BTN_FB_PHOTOS" runat="server" /></a>
    <%--<p class="desc"><mn:Txt ID="txtAnnouncementDesc" ResourceConstant="TXT_ANNOUNCEMENT_DESC_LIKES_ONLY" runat="server" /></p>--%><%-- after fb photo turned on => 'TXT_ANNOUNCEMENT_DESC' --%>
</div>

<script type="text/javascript">
    var fbFeatureAnnouncement = $j("#FBFeatureAnnouncement");

    $j(".click-close", fbFeatureAnnouncement).click(function(e){
        e.preventDefault();
        FBClosePrompt();
    });

    $j(".btn-fb.likes", fbFeatureAnnouncement).click(function(e){
        e.preventDefault();
        FBPrepBumpToLikesInterests();
        goToByScroll(".thumbs-area");
        FBLikesConnect();
    });
    
    function goToByScroll(target){
        $j('html,body').animate({scrollTop: $j(target).offset().top},'slow');
    }

    //close Facebook prompt
    function FBClosePrompt() {
        //hide prompt
        fbFeatureAnnouncement.slideUp();

        //update member info that prompt was displayed and closed
        FBUpdatePromptSeen();

        //update omniture
        if (s != null) {
            PopulateS(true); //clear existing values in omniture "s" object
            s.eVar57 = "FB_Connect_Close";
            s.t(); //send omniture updated values as page load
        }
        return false;
    }

    function FBUpdatePromptSeen() {
        $j.ajax({
            type: "POST",
            url: "/Applications/API/Member.asmx/UpdateAttributeDate",
            data: "{memberID:" + <%=MemberID.ToString() %> + ", attributeName: \"FacebookFeaturePromptDate\"}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function(result) {
                var updateResult = (typeof result.d == 'undefined') ? result : result.d;
                if (updateResult.Status != 2) {
                    alert(updateResult.StatusMessage);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert(textStatus);
            }
        });
    }

    function FBPrepBumpToLikesInterests() {
        if (tabGroup != null) {
            tabGroup.goToTab(1);
        }
        return true;
    }
</script>
