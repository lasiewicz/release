﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.List.ValueObjects;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Content.ServiceAdapters.Links;
using Matchnet.Member.ValueObjects.Interfaces;

namespace Matchnet.Web.Framework.Ui.ProfileElements
{
    public partial class Block : FrameworkControl
    {
        private bool isControlLoaded = false;
        private bool isControlValid = false;

        public IMemberDTO Member { get; set; }
        public string LinkResourceConstant { get; set; }
        public string LinkTitleResourceConstant { get; set; }
        public string HeaderResourceConstant { get; set; }
        public string IconBlockedCSS { get; set; }
        public string IconCSS { get; set; }
        public bool HideLinkText { get; set; }
        public bool HidePopupHeaderText { get; set; }
        public bool IsAvailableToSubscribersOnly { get; set; }
        public bool AutoRefreshOnSearchResultsBlock { get; set; }

        #region Event Handlers
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            cbRemoveFromMySearch.CheckedChanged += new EventHandler(cbRemoveFromMySearch_CheckedChanged);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Member != null && !isControlLoaded)
            {
                LoadBlockControl(Member);
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            this.spanHeaderDesc.Visible = !HidePopupHeaderText;
            this.spanLinkDesc.Visible = !HideLinkText;
            this.Visible = isControlValid;

            base.OnPreRender(e);
        }

        protected void cbRemoveFromMySearch_CheckedChanged(object sender, EventArgs e)
        {
            //prevent removed block from search profile from auto-filling profile (due to auto LoadPostData) that happens to take same spot by just refreshing it with current value
            cbRemoveFromMySearch.Checked = g.List.IsHotListed(HotListCategory.ExcludeList, g.Brand.Site.Community.CommunityID, Member.MemberID);
        }

        #endregion

        public void LoadBlockControl(IMemberDTO member)
        {
            Member = member;
            isControlLoaded = true;

            if (Member != null && Convert.ToBoolean(RuntimeSettings.GetSetting("PROFILE_BLOCK_ENABLED", g.Brand.Site.Community.CommunityID)) &&
                g.Member != null)
            {
                isControlValid = true;
                bool isSubscriber = g.Member.IsPayingMember(g.Brand.Site.SiteID);

                if (String.IsNullOrEmpty(IconCSS))
                    IconCSS = "spr s-icon-A-hover-block";
                spanIcon.Attributes.Add("class", IconCSS);

                if (String.IsNullOrEmpty(IconBlockedCSS))
                    IconBlockedCSS = IconCSS;

                if (!String.IsNullOrEmpty(LinkResourceConstant))
                {
                    literalBlockLink.ResourceConstant = LinkResourceConstant;
                }

                if (!String.IsNullOrEmpty(HeaderResourceConstant))
                {
                    literalBlockHeader.ResourceConstant = HeaderResourceConstant;
                }

                if (!String.IsNullOrEmpty(LinkTitleResourceConstant))
                {
                    btnBlock.Title = g.GetResource(LinkTitleResourceConstant, this);
                }
                else
                {
                    btnBlock.Title = g.GetResource("ALT_BLOCK", this);
                }

                btnSubmitBlocks.OnClientClick = "return BlockAPI_SaveProfileBlocks('" + blockDiv.ClientID + "'," + Member.MemberID.ToString() + ",'" + blockConfirmation.ClientID + "','" + spanIcon.ClientID + "','" + IconCSS + "','" + IconBlockedCSS + "'," + AutoRefreshOnSearchResultsBlock.ToString().ToLower() + ");";

                cbBlockContact.Text = g.GetResource("BLOCK_CONTACT", this);
                cbBlockFromSearchingMe.Text = g.GetResource("BLOCK_FROM_SEARCH", this);
                cbRemoveFromMySearch.Text = g.GetResource("REMOVE_FROM_SEARCH", this);

                cbBlockContact.Checked = g.List.IsHotListed(HotListCategory.IgnoreList, g.Brand.Site.Community.CommunityID, Member.MemberID);
                cbBlockFromSearchingMe.Checked = g.List.IsHotListed(HotListCategory.ExcludeFromList, g.Brand.Site.Community.CommunityID, Member.MemberID);
                cbRemoveFromMySearch.Checked = g.List.IsHotListed(HotListCategory.ExcludeList, g.Brand.Site.Community.CommunityID, Member.MemberID);

                if (cbBlockContact.Checked || cbBlockFromSearchingMe.Checked || cbRemoveFromMySearch.Checked)
                {
                    spanIcon.Attributes["class"] = IconBlockedCSS;
                }

                if (!isSubscriber && IsAvailableToSubscribersOnly)
                {
                    //we will show the subscribe now copy to non-subscribers
                    phBlockFeature.Visible = false;
                    phBlockNonSubscribers.Visible = true;

                    literalBlockHeader.ResourceConstant = "TXT_BLOCK_NON_SUB_TITLE";
                    literalBlockLink.ResourceConstant = "TXT_BLOCK_NON_SUB_TITLE";
                    try
                    {
                        lnkSubscribeNow.NavigateUrl = FrameworkGlobals.LinkHref("/Applications/Subscription/Subscribe.aspx?" + WebConstants.URL_PARAMETER_PURCHASEREASONTYPEID + "=" + PurchaseReasonType.AttemptToBlockSearchResultsPage.ToString("d") + "&DestinationURL=" + HttpUtility.UrlEncode(Request.RawUrl), false);
                    }
                    catch (Exception ex) 
                    { 
                        ;
                    }
                }

                if (g.Member.MemberID == Member.MemberID)
                {
                    btnSubmitBlocks.Visible = false;
                }
            }
        }

    }

}
