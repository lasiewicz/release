<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Control Language="c#" AutoEventWireup="false" Codebehind="Add2List.ascx.cs" Inherits="Matchnet.Web.Framework.Ui.BasicElements.Add2List" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<%@ Import Namespace="Matchnet.Web.Framework.Managers" %>
<div class="listMenuContainer">
	<asp:placeholder id="nonLoggedInUserDisplay" visible="false" runat="server">
		<a id="lnkNonLoggedInUserDest" runat="server">
			<mn:image id="ImageListNonLoggedIn" runat="server" resourceconstant="ALT_HOT_LIST_ME" titleresourceconstant="ALT_HOT_LIST_ME" filename="icon_hotlist.gif" />
			<asp:PlaceHolder ID="phSpriteNonLoggedIn" runat="server">
			    <span id="spanSpriteNonLoggedIn" runat="server" class="spr s-icon-favorites-added"><span><mn:Txt ID="txtSpriteNonLogin" runat="server" ResourceConstant="ALT_HOT_LIST_ME" /></span></span>
			</asp:PlaceHolder>
			<% 
if (Orientation == Matchnet.Web.Framework.Ui.BasicElements.Add2ListOrientation.Vertical)
{ 
			%>
			<% 
}

if (IncludeSpanAroundDescriptionText)
{
			%>
			<span class="title header">
			<%	}

if (Orientation != Matchnet.Web.Framework.Ui.BasicElements.Add2ListOrientation.IconOnly)
{ 
			%>
			<mn:txt id="txtHotListNonLoggedIn" runat="server" resourceconstant="TXT_ADD2LIST" />
			<% 
}

if (IncludeSpanAroundDescriptionText)
{
			%>
			</span>
			<%		}
			%>
		</a>
	</asp:placeholder>
	<asp:placeholder id="loggedInUserDisplay" visible="False" runat="server">
	<!--Note: onblur in Safari will only work on INPUT, TEXTAREA and SELECT elements.-->
    <div id="Favorite1Div" style="<%= DisplayFavoriteOrigHTML %>">
		<a onclick="ToggleDisplay('<%= MemberID %><%= CleanUniqueID %>', true);<%= ExtraJavaScriptToRun_HotlistLoggedInOnClick %>" 
			onblur="ToggleDisplay('<%= MemberID %><%= CleanUniqueID %>', false);" 
			href="#" title="<%=_HotlistTitle %>" tabindex="0" class="menu">
			<mn:image id="ImageListLoggedIn" runat="server" resourceconstant="ALT_HOT_LIST_ME"  filename="icon_hotlist.gif" />
			<asp:PlaceHolder ID="phSpriteLoggedIn" runat="server">
			    <span id="spanSpriteLoggedIn" runat="server" class="spr s-icon-favorites-added"><span><mn:Txt ID="txtSpriteLoggedIn" runat="server" ResourceConstant="ALT_HOT_LIST_ME" /></span></span>
			</asp:PlaceHolder>
			<% 
if ((Orientation == Matchnet.Web.Framework.Ui.BasicElements.Add2ListOrientation.Vertical))
{ 
			%>
            <asp:PlaceHolder runat="server" ID="placeHolderBRTag" ><br /></asp:PlaceHolder>
				
			<% 
}

if (IncludeSpanAroundDescriptionText)
{
			%>
			<span class="title header">
			<%	}

if (Orientation != Matchnet.Web.Framework.Ui.BasicElements.Add2ListOrientation.IconOnly)
{ 
			%>
			<mn:txt id="txtHotListLoggedIn" runat="server" resourceconstant="TXT_ADD2LIST" />
			<% 
}

if (IncludeSpanAroundDescriptionText)
{
			%>
			</span>
			<%		}
			%>
			</a>
 		    <ul class="ListMenu items" style="display: none; top:<%= GetDropDownMenuOffset() %> " id="smenu<%= MemberID %><%= CleanUniqueID %>" onmouseover="setHighlight(true);" onmouseout="setHighlight(false)">
			    <asp:repeater id="rptActionOptions" runat="server" onitemdatabound="rptActionOptions_ItemDataBound">
				    <itemtemplate>
					    <li class="ListElementItem">
						    <asp:linkbutton cssclass="listElementPadding" id="lnkTakeAction" onclick="lnkTakeAction_Click" runat="server">
						    <%# DataBinder.Eval(Container.DataItem, "DisplayResource")%> /
						    <%# DataBinder.Eval(Container.DataItem, "CommandName")%> /
						    <%# DataBinder.Eval(Container.DataItem, "CommandArgument")%>
						    </asp:linkbutton>
					    </li>
				    </itemtemplate>
			    </asp:repeater>
		    </ul>
        </div>
        <div id="FavoriteAjaxDiv" style="<%= DisplayFavoriteAjaxHTML %>">
            <div id="Favorite2Div" style="<%= DisplayNewFavoriteBtnHTML %>"> <%--profile/new--%>
                <a href="#" id="ajaxFavorites<%= MemberID %>" class="menu-style-only ajaxFavorites" title="<%=_HotlistTitle %>" onclick="onFavoritesClick(<%= MemberID %>); return false;">
                    <span id="spanProfileAddedIcon<%= MemberID %>" class="spr s-icon-favorites-added"  style="<%= DisplayAsFavoriteHTML %>"></span> <span><mn:txt ID="Txt2" runat="server" resourceconstant="TXT_FAVORITE" /></span>
                </a>
            </div>

             <div id="Favorite3Div" style="<%= DisplayFavoriteSpriteHTML %>"> <%--list/new--%>
               <a href="#" id="ajaxFavorites<%= MemberID %>" class="menu-style-only ajaxFavorites<%= MemberID %>" title="<%=_HotlistTitle %>" onclick="onFavoritesClick(<%= MemberID %>); return false;">
                    <span class="<%= spanSpriteLoggedIn.Attributes["class"] %>"id="spanSpriteLoggedIn<%= MemberID %>"></span>
                    <%--<span id= "SpriteText<%= MemberID %>"><%= ImageListLoggedIn.TitleResourceConstant%></span>--%>
               </a>
            </div>

            <div id="Favorite4Div" style="<%= DisplayFavoriteImageHTML %>"> <%--list/old style--%>
                <a href="#" id="ajaxFavorites<%= MemberID %>" class="menu-style-only ajaxFavorites<%= MemberID %>" title="<%=_HotlistTitle %>" onclick="onFavoritesClick(<%= MemberID %>); return false;">
                    <img vspace="0" hspace="2" border="0" align="absmiddle" alt="<%=_HotlistTitle %>" src= "<%= ImageListLoggedIn.ImageUrl %>" id="ImageListLoggedIn<%= MemberID %>" title="<%=_HotlistTitle %>"/><span id= "ImageText<%= MemberID %>"><%= ImageListLoggedIn.TitleResourceConstant%></span>
                    <%--<img vspace="0" hspace="2" border="0" align="absmiddle" alt="<%=_HotlistTitle %>" src= "<%= ImageListLoggedIn.ImageUrl %>" id="ImageListLoggedIn<%= MemberID %>" title="<%=_HotlistTitle %>"/><span id= "ImageText<%= MemberID %>"><mn:txt id="txt1" runat="server" resourceconstant="TXT_ADD2LIST" /></span>--%>
                </a>
            </div>
        </div>
	</asp:placeholder>
	
	<asp:PlaceHolder ID="phOmnitureActionVars" runat="server" Visible="true">
        <input id="actionCallPage" type="hidden" runat="server" value="" />
        <input id="actionCallPageDetail" type="hidden" runat="server" value="" />
	</asp:PlaceHolder>

        <script type="text/javascript">

            function add2List_manager() {
                this.instanceRef = null;
                this.updateInProgress = false;
                this.favoriteTitle = '';
                this.unFavoriteTitle = '';
                this.iconFavoriteImage = '';
                this.iconUnFavoriteImage = '';
            }

            this.onFavoritesClick = function (targetMemberID) {

               var memberIDloggedin = <%=MemberIDloggedIn%>;
 
                this.toggleFavorites(memberIDloggedin, targetMemberID);
            }

            this.toggleFavorites = function (memberid, targetmemberid) {
                var  
                    add2ListManagerRef = this.instanceRef,
                    $favorite = $j('#ajaxFavorites' + targetmemberid.toString()),
                    $favoriteSpan = $j('#spanSpriteLoggedIn' + targetmemberid.toString()),
//                    $favoriteIcon = $favorite.find('.s-icon-favorites-added');
                    $favoriteIcon = $j('#spanProfileAddedIcon' + targetmemberid.toString()),
                    $favoriteImage = $j('#ImageListLoggedIn' + targetmemberid.toString()),
                    $favoriteImageText = $j('#ImageText' + targetmemberid.toString()),
                    $favoriteSpriteText = $j('#SpriteText' + targetmemberid.toString());

                var  favoriteImageSrc = $favoriteImage.attr('src').toString();

                if (!this.updateInProgress) {

                    $j.ajax({
                        type: "POST",
                        url: "/Applications/API/Favorites.asmx/AddOrRemoveFavorite",
                        data: "{memberID:'" + memberid + "', favoriteMemberID: '" + targetmemberid + "'}",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        beforeSend: function () {
                            this.updateInProgress = true;
                        },
                        complete: function () {
                            this.updateInProgress = false;
                        },
                        success: function (result) {
                            if (result.d == 'add') {
                                //ajaxFavoritesAdded
                                //console.log("favorited!");
                                $favoriteIcon.css({ display: 'inline-block' });
                                $favorite.prop('title', add2ListManager.unFavoriteTitle.toString());
                                $j('.ajaxFavorites' + targetmemberid.toString()).prop('title', add2ListManager.unFavoriteTitle.toString());
                                $favoriteSpan.attr('class', 'spr s-icon-favorites-added s-icon-favorites-performed').css('background-position','');

                                favoriteImageSrc = favoriteImageSrc.replace(add2ListManager.iconFavoriteImage, add2ListManager.iconUnFavoriteImage);
                                $favoriteImage.attr('src', favoriteImageSrc);

                                $favoriteImage.prop('title', add2ListManager.unFavoriteTitle.toString());
                                $favoriteImage.prop('alt', add2ListManager.unFavoriteTitle.toString());
                                $favoriteSpriteText.text(add2ListManager.unFavoriteTitle.toString());

                                var imageTextlen = $favoriteImageText.text().length;
                                if (imageTextlen > 0) {
                                  $favoriteImageText.text(add2ListManager.unFavoriteTitle.toString());
                                }

      //                          galleryManager.submitOmnitureFavorite(true);
                            }
                            else if (result.d == 'del') {
                                //ajaxFavoritesRemoved
                                //console.log("UNfavorited!");
                                $favoriteIcon.css({ display: 'none' });
                          
                                $favorite.prop('title', add2ListManager.favoriteTitle.toString());

                                $favoriteSpan.attr('class', 'spr s-icon-A-favorites-none');

                               favoriteImageSrc = favoriteImageSrc.replace(add2ListManager.iconUnFavoriteImage, add2ListManager.iconFavoriteImage);
//                             $favoriteImage.attr('src', add2ListManager.iconFavoriteImage);
                               $favoriteImage.attr('src', favoriteImageSrc);

                                $favoriteImage.prop('title', add2ListManager.favoriteTitle.toString());
                                $favoriteImage.prop('alt', add2ListManager.favoriteTitle.toString());
                                $j('.ajaxFavorites' + targetmemberid.toString()).prop('title', add2ListManager.favoriteTitle.toString());
                                $favoriteSpriteText.text(add2ListManager.favoriteTitle.toString());
                                
                                var imageTextlen = $favoriteImageText.text().length;
                                if (imageTextlen > 0) {
                                 $favoriteImageText.text(add2ListManager.favoriteTitle.toString());
                                }
                            }
                            else {
                                console.log("Add2List.ascx:  Applications/API/Favorites.asmx/AddOrRemoveFavorite result value invalid: " + result.d.toString());
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            //ajaxFavoritesError
//                            alert('error');
                            console.log(errorThrown);
                        }

                    });
                }
            } //this.toggleFavorites

              //initialize objects
            var add2ListManager = new add2List_manager();

            $j(function () {
                add2ListManager.instanceRef = add2ListManager;

                add2ListManager.favoriteTitle = '<%=TextFavoriteHover%>'; //TXT_FAVORITE_HOVER
                add2ListManager.unFavoriteTitle = '<%=TextUnfavoriteHover%>'; //TXT_UNFAVORITE_HOVER

                add2ListManager.iconFavoriteImage = '<%=IconHotList%>'; 
                add2ListManager.iconUnFavoriteImage = '<%=IconHotListRemove%>'; 

          });

        </script>
</div>
