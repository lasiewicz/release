﻿using System;
using System.Web.UI.WebControls;
using Matchnet.Member.ServiceAdapters.Interfaces;
using Matchnet.Member.ValueObjects.Interfaces;
using Matchnet.Web.Framework.Managers;
using Matchnet.Member.ValueObjects.Photos;
using Matchnet.Web.Applications.MemberProfile;

namespace Matchnet.Web.Framework.Ui.BasicElements
{
    public partial class ProfileEnlarge : FrameworkControl
    {
        IMemberDTO _member = null;
        protected Matchnet.Web.Framework.SearchResultProfile searchResult;

        public IMemberDTO Member
        {
            get { return _member; }
            set { _member = value; }

        }

        
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (_member == null)
                    return;
                rptPhotos.ItemDataBound += new RepeaterItemEventHandler(rptPhotos_ItemDataBound);
               // searchResult = new SearchResultProfile(_member, 0);
                TextUserName.Text = _member.GetUserName(_g.Brand);
                TextGender.Text = ProfileDisplayHelper.GetMaritalStatusSeekingGenderDisplay(_member, g);
                TextRelationship.Text = ProfileDisplayHelper.GetMaskContent(_member, g, "RelationshipMask");
              // searchResult.SetRegion(g.Brand.Site.LanguageID, TextRegion, 50);
                TextRegion.Text = ProfileDisplayHelper.GetRegionDisplay(_member, g);

                TextAge.Text=FrameworkGlobals.GetAgeCaption(_member.GetAttributeDate(g.Brand, "Birthdate"), g, this, "TXT_NOT_VALID");

                //MemberPhotoHelper photoHelper = new MemberPhotoHelper(RuntimeSettings.Instance);
                Photo photo = MemberPhotoDisplayManager.Instance.GetDefaultPhoto(g.Member, _member, g.Brand);
                string imageURL = MemberPhotoDisplayManager.Instance.GetPhotoDisplayURL(g.Member, _member, g.Brand, photo, PhotoType.Full, PrivatePhotoImageType.Full, NoPhotoImageType.BackgroundThumb); 

                ImageThumb.ImageUrl = imageURL;
                var photos = MemberPhotoDisplayManager.Instance.GetApprovedPhotosExcludingMain(g.Member, _member, g.Brand, true);
                ImageIsNew.Visible = _member.IsNewMember(g.Brand.Site.Community.CommunityID);
                ImageIsUpdate.Visible = !ImageIsNew.Visible && _member.IsUpdatedMember(g.Brand.Site.Community.CommunityID);
                if (photos != null && photos.Count > 0)
                {
                    rptPhotos.DataSource = photos;
                    rptPhotos.DataBind();

                }

                searchResult=new SearchResultProfile(_member,0);
                string matches = ProfileDisplayHelper.GetSpotlightMatchString(g, _member);
                bool selfSpotlight = (g.Member.MemberID == _member.MemberID);
                if (!String.IsNullOrEmpty(matches) && ! selfSpotlight)
                {
                    TextBothLike.Visible = true;
                    bothLike.Visible = true;
                    searchResult.SetHeadline(TextHeadline, matches, _member.MemberID, matches.Length, 20);
                }
                else
                {
                    TextBothLike.Visible = false;
                    bothLike.Visible = false;
                    searchResult.SetHeadline(TextHeadline, g.Session.GetInt(WebConstants.SESSION_PROPERTY_NAME_MEMBERID), 300, 20);
                }


            }
            catch (Exception ex)
            { }


        }

        protected void rptPhotos_ItemDataBound(Object sender, RepeaterItemEventArgs e)
        {
            try
            {

                Photo photo = (Photo)e.Item.DataItem;
                //PremiumService s = Matchnet.PremiumServices.ServiceAdapter.ServiceSA.Instance.GetPremiumService(m.ServiceInstanceID);

                // Matchnet.Web.Framework.Image img = new Image();
                Image img = ((Image)e.Item.FindControl("imgTinyThumb"));
                if (MemberPhotoDisplayManager.Instance.IsPrivatePhoto(photo, g.Brand) || !photo.IsApproved)
                    return;

                //MemberPhotoHelper photoHelper = new MemberPhotoHelper(RuntimeSettings.Instance);
                img.ImageUrl = MemberPhotoDisplayManager.Instance.GetPhotoDisplayURL(g.Member, _member, g.Brand,
                                                                       photo, PhotoType.Thumbnail,
                                                                       PrivatePhotoImageType.Thumb, NoPhotoImageType.Thumb); ;
                // e.Item.Controls.Add(img);

            }
            catch (Exception ex)
            {

            }

        }
    }
}