﻿using System;
using System.ComponentModel;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace Matchnet.Web.Framework.Ui.FormElements
{
    public class FrameworkLinkButton : System.Web.UI.WebControls.LinkButton
    {
        string _ResourceConstant = String.Empty;

        #region Properties
        /// <summary>
        /// Resource constant representing the key to a value in a resource file
        /// </summary>
        [Category("Appearance")]
        [DefaultValue("")]
        [Description("Resource key for value in resource file")]
        public string ResourceConstant
        {
            get { return this._ResourceConstant; }
            set { this._ResourceConstant = value; }
        }
        #endregion

        protected override void Render(System.Web.UI.HtmlTextWriter writer)
        {
            ContextGlobal g = System.Web.HttpContext.Current.Items["g"] as ContextGlobal;
            
            try
            {
                //assign resource value to text
                
                if (g != null && this._ResourceConstant.Trim() != "")
                    this.Text += g.GetResource(this._ResourceConstant, this);

            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
            finally
            {
                base.Render(writer);
            }
        }
    }
}
