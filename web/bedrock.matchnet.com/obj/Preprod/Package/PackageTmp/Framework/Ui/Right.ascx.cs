﻿

using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Session.ServiceAdapters;
using System.Text;
using Matchnet.Session.ValueObjects;
using Matchnet.Web.Framework.Ui.BasicElements;
using Matchnet.Web.Framework.Ui.ProfileElements;
using Matchnet.Web.Applications.MemberProfile.ProfileTabs30.Controls;
using Matchnet.Web.Applications.MemberProfile;
using Matchnet.Web.Applications.MemberProfile.ProfileTabs30.XMLProfileTabConfig;
using Matchnet.Web.Applications.MemberProfile.ProfileTabs30;
using Matchnet.Web.Applications.Home;
using Matchnet.Web.Framework.Managers;
namespace Matchnet.Web.Framework.Ui
{
    public partial class Right : FrameworkControl, IRight
    {
        #region Member Variables
        private bool renderAdHeaderTopIFrameIDToJS = false;
        protected PlaceHolder PlaceHolderGAMChannels;
        #endregion

        #region Events
        protected override void OnInit(EventArgs e)
        {
            //update AdUnit for pages that needs to use IFrame.
            switch ((WebConstants.APP)g.AppPage.App.ID)
            {
                case WebConstants.APP.MemberProfile:
                    if (g.AppPage.ControlName.ToLower() == "viewprofile" || g.AppPage.ControlName.ToLower() == "viewtabbedprofile20")
                    {
                        if (ProfileUtility.HideProfile30RightAd(g))
                        {
                            this.adSquareRight.Disabled = true;
                        }
                        else
                        {
                            this.adSquareRight.GAMAdSlot = "profile_basics_right_300x250";
                            this.adSquareRight.GAMPageMode = Matchnet.Web.Framework.Ui.Advertising.AdUnit.GAMPageModeType.None;
                            this.adSquareRight.GAMIframe = true;
                            this.adSquareRight.GAMIframeHeight = 250;
                            this.adSquareRight.GAMIframeWidth = 300;
                            this.renderAdHeaderTopIFrameIDToJS = true;

                            if (ProfileUtility.IsDisplayingProfile30(g))
                            {
                                ProfileTabConfigs tabConfigs = ProfileUtility.GetProfileTabConfigs(g.Brand.Site.SiteID, g);
                                if (tabConfigs != null && tabConfigs.ProfileTabConfigList != null && tabConfigs.ProfileTabConfigList.Count > 0)
                                {
                                    ProfileTabEnum activeTab = ViewProfileTabUtility.DetermineActiveTab(tabConfigs.ProfileTabConfigList[0].ProfileTabType);
                                    if (activeTab != ProfileTabEnum.None)
                                    {
                                        ProfileTabConfig tabConfig = ProfileUtility.GetProfileTabConfig(g.Brand.Site.SiteID, g, activeTab);
                                        if (tabConfig != null && !String.IsNullOrEmpty(tabConfig.GAMAdSlotRight))
                                        {
                                            this.adSquareRight.GAMAdSlot = tabConfig.GAMAdSlotRight;
                                        }
                                    }
                                }

                                if (ProfileUtility.IsHideProfile30RightAdSlotEnabled(g))
                                {
                                    this.adSquareRight.ShowRightAdMessageForNonSubs();
                                }
                            }
                        }

                    }
                    break;
                case WebConstants.APP.Home:
                    if (g.AppPage.ControlName.ToLower() == "default")
                    {
                        this.adSquareRight.GAMAdSlot = "home_right_300x250";
                        this.adSquareRight.GAMPageMode = Matchnet.Web.Framework.Ui.Advertising.AdUnit.GAMPageModeType.None;
                        this.adSquareRight.GAMIframe = true;
                        this.adSquareRight.GAMIframeHeight = 250;
                        this.adSquareRight.GAMIframeWidth = 300;
                        this.renderAdHeaderTopIFrameIDToJS = true;
                    }
                    break;
                case WebConstants.APP.Article:
                    ShowRegistrationIFrame();
                    break;
            }

            //update color code mailto link
            if (g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.AmericanSingles)
            {
                lnkColorCodeMailTo.HRef = "mailto:colorcode@spark.com?subject=Color Code Feedback";
            }
            else
            {
                lnkColorCodeMailTo.HRef = "mailto:colorcode@jdate.com?subject=Color Code Feedback";
            }

            if (HomeUtil.IsDisplayingHome40(g.Member, g.AppPage, g.Brand))
            {
                //Viewed Your Profile hotlist for new homepage design
                hotlistProfileList1.Visible = true;
                hotlistProfileList1.HotlistCategory = List.ValueObjects.HotListCategory.WhoViewedYourProfile;
                hotlistProfileList1.LoadHotlistProfileList();

                ActivityCenter.Visible = false;
            }

            base.OnInit(e);

        }

        private void ShowRegistrationIFrame()
        {
            if (g.Member == null)
            {
                var sector = SectorsManager.GetSector();
                string prm = "";
                bool showIFrame = false;
                switch (sector)
                {
                    case Sector.Academic:
                        prm = "88539";
                        showIFrame = true;
                        break;
                    case Sector.Religious:
                        prm = "96413";
                        showIFrame = true;
                        break;
                    case Sector.Elderly:
                        prm = "96415";
                        showIFrame = true;
                        break;
                    case Sector.Divorced:
                        prm = "96414";
                        showIFrame = true;
                        break;
                }

                string fullURL = Context.Request.Url.AbsoluteUri.ToLower();
                if (showIFrame)
                {
                    plcRegistration.Visible = true;
                    string src = "/?iframeopr=academic&ShowSplashPageForPostLoginUsers=1";
                    ifrmReg.Attributes["src"] = "http://" + Request.ServerVariables.Get("HTTP_HOST") + src;
                    if (fullURL.Contains("prm="))
                    {
                        ifrmReg.Attributes["src"] += "&prm=" +
                                                     fullURL.Split(new string[] { "prm=" },
                                                                   StringSplitOptions.RemoveEmptyEntries)[1];
                    }
                    else
                    {
                        ifrmReg.Attributes["src"] += "&prm=" + prm;
                    }
                }
            }
        }

        private void Page_Load(object sender, EventArgs e)
        {
            try
            {
                string controlName = g.AppPage.ControlName.ToLower();
                // Hide Jmessage unless querystring has override
                if (Request.QueryString["ShowJMessage"] == "1")
                {
                    plcJMessage.Visible = true;
                }

                /*AdServerTest.Visible = false;
                if(RuntimeSettings.GetSetting("ADSERVER") != null)
                {
                    if(RuntimeSettings.GetSetting("ADSERVER") =="true")
                    {
                        AdServerTest.Visible = true;
                    }
                }*/

                if (Request["ClickedHomepage"] != null)
                {
                    ClickedMakeHomepage = Request["ClickedHomepage"].ToLower() == "true" ? true : false;
                }

                PlaceHolderGAMChannels.Visible = true;
                rightContent.Visible = false;

                if (((g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.JDateCoIL && g.Member == null) ||
                    g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.Cupid) && controlName != "jexperts")
                {
                    //if (g.AppPage.ID == (int)WebConstants.PageIDs.ArticleView ||
                    //    g.AppPage.ID == (int)WebConstants.PageIDs.FAQMain)
                    //{
                    txtTips.Visible = true;
                    //}
                }
                if (g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.Cupid && g.AppPage.ControlName.ToLower() == "attributeedit")
                {
                    txtTips.Visible = false;
                }
                if (g.AppPage.App.ID == (int)WebConstants.APP.ColorCode && g.AppPage.ControlName.ToLower() == "analysis")
                {
                    if (g.Member != null)
                    {
                        int ccpurchase = g.Member.GetAttributeInt(g.Brand, "ColorAnalysis");
                        bool purchaseEnabled = SettingsManager.GetSettingBool(SettingConstants.ENABLE_COLORCODE_PURCHASE, g.Brand);
                        phColorCodePurchase.Visible = ((ccpurchase != 1) && purchaseEnabled);
                    }
                }
                else
                {
                    phColorCodePurchase.Visible = false;
                }

                //hide channel ads based on pages or settings
                //note: we can call "HideGAMChannels()" from most pages because it happens after adslots already process
                if (g.AppPage.App.ID == (int)WebConstants.APP.MemberProfile && g.AppPage.ID == (int)WebConstants.PageIDs.ViewProfile)
                {
                    PlaceHolderGAMChannels.Visible = false;
                }
                else if (HomeUtil.IsDisplayingHome40(g.Member, g.AppPage, g.Brand))
                {
                    PlaceHolderGAMChannels.Visible = false;
                }


                //Enable channel adslots if container is still visible
                if (PlaceHolderGAMChannels.Visible == true)
                {
                    AdUnitGAMChannel1.Disabled = false;
                    AdUnitGAMChannel2.Disabled = false;
                    AdUnitGAMChannel3.Disabled = false;
                    AdUnitGAMChannel4.Disabled = false;
                    AdUnitGAMChannel5.Disabled = false;
                    AdUnitGAMChannel6.Disabled = false;
                    AdUnitGAMChannel7.Disabled = false;
                    AdUnitGAMChannel8.Disabled = false;
                }

                //Disable some channel adslots that will display on homepage instead of Right control of new JDate homepage version
                if (g.AppPage.App.ID == (int)WebConstants.APP.Home && g.AppPage.ID == (int)WebConstants.PageIDs.Default)
                {
                    Matchnet.Configuration.ServiceAdapters.Analitics.Scenario scenario = g.GetABScenario("HOMEPAGE");
                    if (scenario == Matchnet.Configuration.ServiceAdapters.Analitics.Scenario.B)
                    {
                        if (Matchnet.Web.Applications.Home.HomeUtil.IsHomepageChannelAdsOverrideEnabled(g.Brand))
                        {
                            AdUnitGAMChannel3.Disabled = true;
                            AdUnitGAMChannel5.Disabled = true;
                            AdUnitGAMChannel6.Disabled = true;
                            AdUnitGAMChannel7.Disabled = true;
                        }
                    }
                }

                //These ads shouldn't be rendered on non-IL site, seems they were meant for IL initiatives
                if (g.Brand.Site.SiteID != (int)WebConstants.SITE_ID.JDateCoIL)
                {
                    AdUnitGAMBottom2.Disabled = true;
                }

                if (g.AppPage.App.ID == (int)WebConstants.APP.Home && g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.AmericanSingles
                    || g.AppPage.App.ID == (int)WebConstants.APP.Home && g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.JDate
                    || g.AppPage.App.ID == (int)WebConstants.APP.Home && g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.JDateFR
                    || g.AppPage.App.ID == (int)WebConstants.APP.Home && g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.JDateUK
                    || g.AppPage.App.ID == (int)WebConstants.APP.Home && g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.JDateCoIL
                    || g.AppPage.App.ID == (int)WebConstants.APP.Home && g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.BBW)
                {
                    phHeaderAnnouncement.Visible = HeaderAnnouncement1.LoadAnnouncementMessage();
                }

                if (Convert.ToBoolean(RuntimeSettings.GetSetting("ENABLE_JEXPERTS",
                                                                             g.Brand.Site.Community.CommunityID,
                                                                             g.Brand.Site.SiteID, g.Brand.BrandID)) && controlName == "jexperts")
                {
                    txtJExperts.Visible = true;
                }
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            if (this.renderAdHeaderTopIFrameIDToJS)
            {
                Page.ClientScript.RegisterClientScriptBlock(typeof(System.Web.UI.Page), "RightJSAd",
                       "<script type=\"text/javascript\">"
                       + "var rightAd300x250IFrameID = \"" + this.adSquareRight.AdIFrameID + "\";"
                       + "var rightAdProfilePopup = \"false\";"
                       + "</script>");
            }

            base.OnPreRender(e);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Gets desired attributes from clients browser passed in http header.
        /// At some point this should be completed and moved to some common utility class.
        /// </summary>
        /// <param name="requestedAttribute">examples: BrowserType, HasFlash, MonitorRes, Bandwidth, CookiesEnabled</param>
        /// <returns>result from http header</returns>
        private string BrowserAttribute(string requestedAttribute)
        {
            string browserAttribute = string.Empty;
            HttpContext context = HttpContext.Current;
            HttpBrowserCapabilities httpbrowsercapabilitiesBrowser = context.Request.Browser;

            switch (requestedAttribute)
            {
                case "BrowserType":
                    browserAttribute = httpbrowsercapabilitiesBrowser.Browser;
                    return browserAttribute;
                case "HasFlash":
                    return browserAttribute;
                case "MonitorRes":
                    return browserAttribute;
                case "Bandwidth":
                    return browserAttribute;
                case "CookiesEnabled":
                    return browserAttribute;
                default:
                    return browserAttribute;
            }
        }

        private bool IsInvalidRequest()
        {
            //this is a HUGE HACK. There's a problem with some gam ads calling to images and stylesheets that
            //no longer exist, and the resulting 404 causes default.aspx to be reloaded and the slideshow to advance. 
            //This code prevents that, and can be removed once the gam issue is resolved. 
            bool invalid = false;
            string controlname = g.AppPage.ControlName;
            string uri = HttpContext.Current.Request.RawUrl;
            string referrer = HttpContext.Current.Request.UrlReferrer != null ? HttpContext.Current.Request.UrlReferrer.AbsoluteUri : string.Empty;


            if (controlname.ToLower() == "default" &&
                (uri.ToLower().IndexOf("/?") >= 0 ||
                uri.ToLower().IndexOf("default.aspx?siteid=") >= 0 ||
                uri.ToLower().IndexOf("default.aspx?v=") >= 0) ||
                referrer.ToLower().IndexOf("red01.css") >= 0)
            {
                Trace.Write("is: true");
                invalid = true;
            }

            return invalid;
        }


        //Methods below should be renamed, and ultimately superceded by using different templates for Login, Registration, and subscription
        //instead of trying to hard code this logic.

        /// <summary>
        /// Subscription Mode. Redundant method called from subscription common pages to hide left nav area if Member is not a paying member
        /// Rationale: not to distract users during subscription. Join Now will be hidden, because they are a member.
        /// </summary>
        public void HideUserNameLogout()
        {
            HideLoginLink();
        }

        /// <summary>
        /// Registration Mode. Redundant Method called from the registration control that is mostly redundant with g.Member = null.
        /// Hide Join Now cuz you're already registering. (we also now hide the login link because there are two other login links
        /// We should do the same on other sites.
        /// </summary>
        public void ShowWelcomeText()
        {
            this.ActivityCenter1.JoinNowSection.Visible = false;
            HideLoginLink();
        }

        /// <summary>
        /// Login Mode. Redundant method called from the logon control. It is mostly redundant with g.Member = null
        /// It asks that we hide the MembersOnline count (because we're logging in and don't need the tease
        /// and show the Join Now (which is redundant  because if wer're logged out it'll be shown anyway).
        /// Logon should use different template that does not include the left nav!
        /// </summary>
        public void HideLoginLink()
        {
            this.ActivityCenter1.ActivityCenterSection.Visible = false;
        }

        /// <summary>
        /// Shows the color code section for profile examples
        /// </summary>
        public void ShowColorCodeProfileExamples()
        {
            this.phColorCodeProfileExamples.Visible = true;
        }

        /// <summary>
        /// Shows the color code section for test help
        /// </summary>
        public void ShowColorCodeTestHelp()
        {
            this.phColorCodeTestHelp.Visible = true;
        }

        /// <summary>
        /// Shows the color code section for the about color code and meet dr. hartman info
        /// </summary>
        public void ShowColorCodeAbout()
        {
            this.phColorCodeAbout.Visible = true;
        }

        /// <summary>
        /// Shows the color code section for member feedback
        /// </summary>
        public void ShowColorCodeFeedback()
        {
            this.phColorCodeMemberFeedback.Visible = true;
        }

        public void HideGamChannels()
        {
            this.PlaceHolderGAMChannels.Visible = false;
        }

        public void ShowMiniMemberSlideShow()
        {
            phMiniMemberSlideshow.Visible = true;
            ucMiniSlideshowProfile.Load(true);

            bool enableConfiguration = Conversion.CBool(RuntimeSettings.GetSetting("ENABLE_CONFIGURABLE_SLIDESHOW", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID));
            if (enableConfiguration)
            {
                ucMiniSlideshowProfile.LoadPreferences();
            }
        }

        public void ShowMiniMemberSlideShowBelowAd()
        {
            phMiniMemberSlideshowBelowAd.Visible = true;
            ucMiniSlideshowProfileBelowAd.Load(true);

            bool enableConfiguration = Conversion.CBool(RuntimeSettings.GetSetting("ENABLE_CONFIGURABLE_SLIDESHOW", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID));
            if (enableConfiguration)
            {
                ucMiniSlideshowProfileBelowAd.LoadPreferences();
            }
        }
        #endregion

        #region Properties
        /// <summary>
        /// gets/sets whether the user clicked on the set as homepage
        /// </summary>
        private bool ClickedMakeHomepage
        {
            get
            {
                bool clickedHomepage = false;

                try
                {
                    clickedHomepage = bool.Parse(g.Session["ClickedHomepage"]);
                }
                catch
                {

                }

                return clickedHomepage;
            }
            set
            {
                g.Session.Add("ClickedHomepage", value.ToString(), SessionPropertyLifetime.Persistent);
            }
        }

        public string MovieUrl
        {
            get
            {
                StringBuilder sb = new StringBuilder(200);
                if (g.Member != null)
                {
                    sb.Append("http://strategy.matchnet.com/jmessage/jmessage.swf?Happen_URL=http://strategy.matchnet.com/m_online/Happen.xml&amp;Inbox_URL=http://strategy.matchnet.com/m_online/Inbox.xml&amp;IM_URL=http://strategy.matchnet.com/m_online/IM.xml");

                    /*
					
                    sb.Append(Server.UrlEncode("http://dv-wlybrand.jdate.com/Applications/API/FlashListAPI.xml"));
                    sb.Append(Server.UrlEncode("?MemberID=" + g.Member.MemberID));
                    sb.Append(Server.UrlEncode("&BrandID=" + g.Brand.BrandID));

                    sb.Append("&Inbox_URL=http://strategy.matchnet.com/m_online/inbox.xml");
					
                    sb.Append(Server.UrlEncode("http://dv-wlybrand.jdate.com/Applications/API/FlashEmailAPI.aspx"));
                    sb.Append(Server.UrlEncode("?MemberID=" + g.Member.MemberID));
                    sb.Append(Server.UrlEncode("&BrandID=" + g.Brand.BrandID));
                    sb.Append(Server.UrlEncode("&MemberFolderID=1&StartRow=1&PageSize=4&OrderBy=InsertDate desc"));
					
                    //sb.Append("");

                    sb.Append("&IM_URL=");
                    sb.Append(Server.UrlEncode("http://strategy.matchnet.com/m_online/IM.xml"));
                    */
                }
                return sb.ToString();
            }
        }



        /// <summary>
        /// Gets reference to the MiniSearch control
        /// </summary>
        public MiniSearch MiniSearchControl
        {
            get { return this.MiniSearch1; }
        }

        public ActivityCenter ActivityCenter
        {
            get { return this.ActivityCenter1; }
        }

        public ProfileToolBar ProfileToolBar
        {
            get { return this.profileToolBar1; }
        }

        public ProfileDetails ProfileDetails
        {
            get { return this.profileDetails1; }
        }

        public bool IsVisible
        {
            get
            {
                return this.Visible;
            }
            set
            {
                this.Visible = value;
            }
        }

        public FrameworkControl Control
        {
            get
            {
                return this;
            }
        }

        #endregion
    }
}
