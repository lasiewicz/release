﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.List.ValueObjects;

namespace Matchnet.Web.Framework.Ui.ProfileElements
{
    [Serializable]
    public class ContactHistoryItem : IComparable
    {
        public DateTime ActionDate { get; set; }
        public HotListCategory HotlistCategory {get; set;}
        public HotListType HotlistType { get; set; }
        public bool IsMatched { get; set; }
        public string YNM { get; set; }
        public bool MutualYNM { get; set; }
        public string ItemText { get; set; }
        public string ItemTimeStampText { get; set; }

        #region Constructors
        public ContactHistoryItem()
        {
            this.ActionDate = DateTime.MinValue;
            this.IsMatched = false;
            this.ItemText = "";
            this.ItemTimeStampText = "";
        }

        #endregion

        public int CompareTo(object obj)
        {
            if (obj is ContactHistoryItem)
            {
                ContactHistoryItem l2 = (ContactHistoryItem)obj;
                return ActionDate.CompareTo(l2.ActionDate) * -1;
            }
            else
                throw new ArgumentException("Object is not a ContactHistoryItem.");
        }
    }
}
