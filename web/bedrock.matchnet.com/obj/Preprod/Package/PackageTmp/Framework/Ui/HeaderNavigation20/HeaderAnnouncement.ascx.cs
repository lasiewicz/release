﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Web.Framework.Menus;

namespace Matchnet.Web.Framework.Ui.HeaderNavigation20
{
    public partial class HeaderAnnouncement : FrameworkControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        public bool LoadAnnouncementMessage()
        {
            bool messageLoaded = false;
            try
            {
                /*
                 * Announcements in resource files are currently page specific and supports
                 * multiple announcement messages, separated by double pipes (e.g. message1||message2), and will
                 * be displayed randomly.
                 * 
                 * Future: Likely move announcements to database with admin tool to manage, and possibly add global message
                 * that exists on all pages.
                 */
                if (!MemberPrivilegeAttr.IsCureentSubscribedMember(g) && g.AppPage.App.ID == (int)WebConstants.APP.Home)
                {
  
                    bool enabled = false;
                    try
                    {
                        enabled =
                            Convert.ToBoolean(
                                (RuntimeSettings.GetSetting("ENABLE_SUBNOW_AD",
                                                            g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID,
                                                            g.Brand.BrandID)));
                    }
                    catch (Exception ex)
                    {
                        enabled = false;
                        g.ProcessException(ex);
                    }

                    //If the SubNow GAM Ad is enabled for the site, then enable the GAM Ad
                    if (enabled)
                    {
                        phSubNowAd.Visible = true;
                        AdUnitGAMSubnowRight.Disabled = false;
                        return true;
                    }
                    else  //If the SubNow GAM Ad is not enabled for the site, then display the announcement with the Subscribe text
                    {
                        string subannouncement = g.GetResource("TXT_SUBSCRIBE", this);

                        if (!String.IsNullOrEmpty(subannouncement) && subannouncement.IndexOf("\\_Resources\\") <= 0)
                        {
                            literalAnnouncement.ResourceConstant = "TXT_SUBSCRIBE";
                            //literalAnnouncement.Text = subannouncement;
                            phAnnouncement.Visible = true;
                            messageLoaded = true;
                            return true;
                        }
                    }

            }

                if (MenuUtils.IsAnnouncementMessageEnabled(g))
                {
                    string announcements = g.GetResource("TXT_ANNOUNCEMENTS_PAGE_" + g.AppPage.ID.ToString(), this);
                    string selectedAnnouncement = "";
                    if (!String.IsNullOrEmpty(announcements) && announcements.IndexOf("\\_Resources\\") <= 0)
                    {
                        string[] announcementList = announcements.Split(new string[] { "||" }, StringSplitOptions.None);
                        if (announcementList.Length > 0)
                        {
                            if (announcementList.Length == 1)
                            {
                                //there's only one announcement message
                                selectedAnnouncement = announcementList[0];
                            }
                            else
                            {
                                //randomly pick an announcement message
                                int r = new Random().Next(announcementList.Length);
                                selectedAnnouncement = announcementList[r];
                            }
                        }

                        if (!String.IsNullOrEmpty(selectedAnnouncement))
                        {
                            messageLoaded = true;
                            phAnnouncement.Visible = true;
                            literalAnnouncement.Text = selectedAnnouncement;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }

            return messageLoaded;
        }
    }
}