using System;
using System.Text;
using System.Web.UI.WebControls;

using Spark.SAL;
using Matchnet.Web.Applications.Search;
using Matchnet.Search.Interfaces;
using Matchnet.Web.Framework.Managers;

namespace Matchnet.Web.Framework.Ui.FormElements
{
	/// <summary>
	///		Summary description for HeightRangeControl.
	/// </summary>
	public class HeightRangeControl : FrameworkControl
	{
		protected Txt txtPreferenceName;
		protected Literal litCurrentRange;
		protected System.Web.UI.HtmlControls.HtmlGenericControl divSelectedItems;
		protected PlaceHolder phMinHeight;
		protected PlaceHolder phMaxHeight;
		protected DropDownList _ddlMinHeight;
		protected DropDownList _ddlMaxHeight;
        public bool IsCollapsed = true;
        public int SearchWeightValue = (int)SearchWeight.VeryImportant;

        public bool IsSliderWeightEnabled
        {
            get
            {
                return MatchRatingManager.Instance.IsSliderWeightingEnabled(g.Brand.BrandID, g.Brand.Site.SiteID, g.Brand.Site.Community.CommunityID);
            }
        }

		private MemberSearchPreferenceRange _memberSearchPreferenceRange;

		private void Page_Load(object sender, System.EventArgs e)
		{
			_ddlMinHeight = FrameworkGlobals.CreateHeightList(g, "MinHeight", _memberSearchPreferenceRange.MinValue, true);
			_ddlMaxHeight = FrameworkGlobals.CreateHeightList(g, "MaxHeight", _memberSearchPreferenceRange.MaxValue, true);

			phMinHeight.Controls.Add(_ddlMinHeight);
			phMaxHeight.Controls.Add(_ddlMaxHeight);

			_ddlMinHeight.SelectedIndexChanged += new EventHandler(_ddlMinHeight_SelectedIndexChanged);
			_ddlMaxHeight.SelectedIndexChanged += new EventHandler(_ddlMaxHeight_SelectedIndexChanged);

			setupClientScript(_ddlMinHeight, _ddlMaxHeight);
		}

		private void HeightRangeControl_PreRender(object sender, EventArgs e)
		{
			litCurrentRange.Text = _ddlMinHeight.SelectedItem.Text + " - " + _ddlMaxHeight.SelectedItem.Text;
            if (_memberSearchPreferenceRange.MinValue > 0 || _memberSearchPreferenceRange.MaxValue > 0)
            {
                IsCollapsed = false;
            }
		}

		private void _ddlMinHeight_SelectedIndexChanged(object sender, EventArgs e)
		{
			_memberSearchPreferenceRange.MinValue = Conversion.CInt(_ddlMinHeight.SelectedValue);
			validate();
		}

		private void _ddlMaxHeight_SelectedIndexChanged(object sender, EventArgs e)
		{
			_memberSearchPreferenceRange.MaxValue = Conversion.CInt(_ddlMaxHeight.SelectedValue);
			validate();
		}

		private void validate()
		{
			if (_memberSearchPreferenceRange.MinValue > _memberSearchPreferenceRange.MaxValue  &&  _memberSearchPreferenceRange.MaxValue != Constants.NULL_INT)
			{
				Int32 temp = _memberSearchPreferenceRange.MinValue;
				_memberSearchPreferenceRange.MinValue = _memberSearchPreferenceRange.MaxValue;
				_memberSearchPreferenceRange.MaxValue = temp;
			}
		}

		private void setupClientScript(DropDownList ddlMinHeight, DropDownList ddlMaxHeight)
		{
			ddlMinHeight.Attributes.Add("onchange", "showSelectedValues();");
			ddlMaxHeight.Attributes.Add("onchange", "showSelectedValues();");

			StringBuilder sb = new StringBuilder();
			sb.Append("<script type=\"text/javascript\">");
			sb.Append("function showSelectedValues()");
			sb.Append("{");
			sb.Append("var minList = document.getElementById('" + ddlMinHeight.ClientID + "');");
			sb.Append("var maxList = document.getElementById('" + ddlMaxHeight.ClientID + "');");
			sb.Append("var textDiv = document.getElementById('" + divSelectedItems.ClientID + "');");
			sb.Append("textDiv.innerHTML = minList.options[minList.selectedIndex].text + ' - ' + maxList.options[maxList.selectedIndex].text;");
			sb.Append("}");
			sb.Append("</script>");
			Page.RegisterClientScriptBlock("HeightControlScript", sb.ToString());
		}

		public string ResourceConstant
		{
			get
			{
				return txtPreferenceName.ResourceConstant;
			}
			set
			{
				txtPreferenceName.ResourceConstant = value;
			}
		}

		public MemberSearchPreferenceRange MemberSearchPreferenceRange
		{
			get
			{
				return _memberSearchPreferenceRange;
			}
			set
			{
				_memberSearchPreferenceRange = value;
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
			this.PreRender += new EventHandler(HeightRangeControl_PreRender);
		}
		#endregion
	}
}
