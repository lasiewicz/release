<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Control Language="c#" AutoEventWireup="false" Codebehind="YNMVoteBar.ascx.cs" Inherits="Matchnet.Web.Framework.Ui.YNMVoteBar" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>

<script type="text/javascript">
	var ImageOver = 'URL(<asp:Literal ID="litImageOver" Runat="server" />)';
	var ImageOut = 'URL(<asp:Literal ID="litImageOut" Runat="server" />)';
</script>

<asp:PlaceHolder Runat="server" ID="plcAnimated">
<div id="voteBar" onmouseover="this.style.backgroundImage=ImageOver;" onmouseout="this.style.backgroundImage=ImageOut;">
</asp:PlaceHolder>
<asp:PlaceHolder Runat="server" ID="plcStatic" Visible="False">
<div id="voteBar">
</asp:PlaceHolder>
<div class="clearFloats" style="width:575px">
	<div class="floatInside">
		<input type="hidden" runat="server" id="YNMVoteStatus" name="YNMVoteStatus">
		<mn:image id="ImageYes" runat="server" resourceconstant="YNM_Y_IMG_ALT_TEXT" hspace="3" filename="icon-click-y-off.gif"></mn:image>
		<mn:txt id="TxtYes" runat="server" ResourceConstant="YNM_YES"></mn:txt>&nbsp; 
		<mn:image id="ImageMaybe" runat="server" resourceconstant="YNM_M_IMG_ALT_TEXT" hspace="3" filename="icon-click-m-off.gif"></mn:image>
		<mn:txt id="TxtMaybe" runat="server" ResourceConstant="YNM_MAYBE"></mn:txt>&nbsp; 
		<mn:image id="ImageNo" runat="server" resourceconstant="YNM_N_IMG_ALT_TEXT" hspace="3" filename="icon-click-n-off.gif"></mn:image>
		<mn:txt id="TxtNo" runat="server" ResourceConstant="YNM_NO"></mn:txt>&nbsp;	
	</div>
	
	<div class="floatInside voteBarBothSaidYess">
		<div id="divBothSaidYes" runat="server" style="visibility: hidden">
			<mn:txt id="TxtBothSaidYes" runat="server" ResourceConstant="TXT_YOU_BOTH_SAID"></mn:txt>
			<mn:image id="imgBothSaidYes" runat="server" FileName="icon-click-yy.gif" hspace="2" border="0" style="visibility: hidden" />
		</div>
	</div>
	
	<div class="floatOutside" style="margin-top: 4px;">
		<mn:txt runat="server" id="txtNextLink" ResourceConstant="TXT_MORE_PROFILES" />
	</div>


</div>
</td>
<script type="text/javascript">
	SetBothSaidYes('<%= this.ClientID %>', '<asp:Literal id="litBothSaidYesVisible" runat="server" />');
</script>
