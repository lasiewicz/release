using System;
using System.Web.UI.WebControls;

using Matchnet.Web.Framework;

namespace Matchnet.Web.Framework.Ui.PageElements
{
    /// <summary>
    ///		Summary description for BreadCrumbTrail.
    /// </summary>
    public class BreadCrumbTrail : FrameworkControl
    {
        protected System.Web.UI.WebControls.Literal HeaderContents;
        protected System.Web.UI.WebControls.Literal FooterContents;
        protected System.Web.UI.WebControls.Label lblCurrentPage;
        protected HyperLink lnkNav4;
        protected HyperLink lnkNav3;
        protected HyperLink lnkNav2;
        protected HyperLink lnkNav1;
        protected Literal ltlArrow2;
        protected Literal ltlArrow3;
        protected Literal ltlArrow4;
        protected PlaceHolder plcBreadNav;
        protected Label lblNav;
        protected HyperLink lnkMore;
        protected Literal ltlName;
        protected Literal ltlNameArrows;
        protected System.Web.UI.HtmlControls.HtmlGenericControl divBreadNav;

        private ContextTypeEnum _contextType = ContextTypeEnum.Header;

        string linkClass = String.Empty;
        protected System.Web.UI.WebControls.Label lblMiddlePages;
        protected System.Web.UI.WebControls.PlaceHolder pnlBreadNav;
        protected System.Web.UI.WebControls.Label lblEndNavigation;

        public Label LblMiddlePages
        {
            get
            {
                return lblMiddlePages;
            }
        }

        public enum ContextTypeEnum
        {
            Header, Footer
        }

        public ContextTypeEnum ContextType
        {
            get { return _contextType; }
            set { _contextType = value; }
        }

        private void Page_Init(object sender, System.EventArgs e)
        {
            try
            {
                if (ContextType == ContextTypeEnum.Header)
                {
                    lblNav.CssClass = "breadCrumbUpper";
                    divBreadNav.Attributes["class"] = "subNavTop";
                    linkClass = "breadCrumbPagesLink";
                    lblMiddlePages.CssClass = "breadCrumbPagesUpper";
                    lblEndNavigation.CssClass = "breadCrumbUpperRight";
                }
                else
                {
                    lblNav.CssClass = "breadCrumbLower";
                    divBreadNav.Attributes["class"] = "subNavBottom";
                    linkClass = "";
                    lblMiddlePages.CssClass = "breadCrumbPagesLower";
                    lblEndNavigation.CssClass = "breadCrumbLowerRight";
                }

                SetupLinks();
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        private void SetupLinks()
        {
            string hplink = GetHomepageLink();

            //added for Site redesign
            if (g.AppPage.App.Path.ToLower() == "/applications/home" || g.LayoutVersion == WebConstants.LayoutVersions.versionWide)
            {
                SetNavLink(g.GetResource("HOME", this), hplink, 1);
                lnkNav1.Visible = false;
                lnkNav2.Visible = false;
                ltlArrow2.Visible = false;
                ltlArrow3.Visible = false;
                lnkNav4.Visible = false;
                ltlArrow4.Visible = false;
            }
            else
            {
                if (g.AppPage.ResourceConstant != string.Empty && g.AppPage.ResourceConstant != Constants.NULL_STRING)
                {
                    string appResourceValue = g.GetResource(g.AppPage.App.ResourceConstant, null);
                    string pageResourceValue = g.GetResource(g.AppPage.ResourceConstant, null);

                    // SetNavLink(g.GetResource("HOME", this), GetHomepageLink(), 3);

                    SetNavLink(g.GetResource("HOME", this), hplink, 3);
                    lnkNav3.NavigateUrl = hplink;

                    SetNavLink(appResourceValue, g.AppPage.App.DefaultPagePath, 2);

                    SetNavLink(pageResourceValue, String.Empty, 1);
                }
                else
                {
                    // SetNavLink(g.GetResource("HOME", this), GetHomepageLink(), 2);
                    SetNavLink(g.GetResource("HOME", this), hplink, 2);
                    lnkNav2.NavigateUrl = hplink;

                    string text = String.Empty;

                    if (g.IsDevMode)
                    {
                        text = "Contact Dev. Missing Entry in DB Page Table For Control:" + g.AppPage.GetFullControlPath();
                    }

                    if (g.AppPage.App.ResourceConstant != null && g.AppPage.App.ResourceConstant != "")
                    {
                        text = g.GetResource(g.AppPage.App.ResourceConstant, null);
                    }
                    SetNavLink(text, g.AppPage.GetFullControlPath(), 1);
                }
            }
            
        }

        public void SetLink(string Text, string Href)
        {
            HyperLink lnkNewLink = new HyperLink();
            lnkNewLink.Text = Text;
            lnkNewLink.NavigateUrl = Href;
            lnkNewLink.CssClass = linkClass;
            lblEndNavigation.Controls.Add(lnkNewLink);
        }

        public void SetTwoLinkCrumb(string Text, string Href)
        {
            SetNavLink(g.GetResource("HOME", this), GetHomepageLink(), 2);
            SetNavLink(Text, Href, 1);
        }


        public void SetThreeLinkCrumb(string SecondText, string SecondHref, string ThirdText, string ThirdHref)
        {
            SetNavLink(g.GetResource("HOME", this), GetHomepageLink(), 3);
            SetNavLink(SecondText, SecondHref, 2);
            SetNavLink(ThirdText, ThirdHref, 1);
        }

        // TT#16469 - bold and link the appropriate breadcrumb items when viewing a message.
        public void SetFourLinkCrumb(string SecondText, string SecondHref, string FourthText, string FourthHref)
        {
            SetNavLink(g.GetResource("HOME", this), GetHomepageLink(), 3);
            SetNavLink(SecondText, SecondHref, 2);
            SetNavLink(FourthText, FourthHref, 4);

            // view message should not show the current page label
            lblCurrentPage.Visible = false;
        }

        public void SetLastElementAndRebind(string Text, string Href)
        {
            SetNavLink(Text, Href, 1);
        }

        public void SetViewProfileCrumbs(string SecondText, string SecondHref, string ThirdText, string ThirdHref, string UserName, string NextText, string NextLink, string FarRightText, string FarRightHref)
        {
            lnkNav1.Visible = true;
            // lnkNav2.Visible = true;
            lnkNav3.Visible = true;
            ltlArrow3.Visible = true;
            // ltlArrow2.Visible = true;
            ltlNameArrows.Visible = true;
            ltlName.Visible = true;
            lnkMore.Visible = true;
            lnkNav4.Visible = false;
            ltlArrow4.Visible = false;

            lblCurrentPage.Visible = false;

            if (ThirdText == null || ThirdHref == null)
            {
                lnkNav1.Visible = false;
                ltlArrow2.Visible = false;
            }
            else
            {
                lnkNav1.Text = ThirdText;
                lnkNav1.NavigateUrl = ThirdHref;
                lnkNav1.CssClass = linkClass;
            }

            lnkNav2.Text = SecondText;
            lnkNav2.NavigateUrl = SecondHref;
            lnkNav2.CssClass = linkClass;

            lnkNav3.Text = g.GetResource("HOME", this);
            lnkNav3.NavigateUrl = GetHomepageLink();

            if (UserName != null)
            {
                ltlName.Visible = true;
                ltlName.Text = UserName;

                ltlNameArrows.Visible = true;
            }

            if (NextLink != null && g.Member != null)
            {
                lnkMore.Visible = true;
                lnkMore.NavigateUrl = NextLink;
                lnkMore.Text = NextText;
                lnkMore.CssClass = linkClass;
            }
            else
            {
                lnkMore.Visible = false;
            }

            if (Request.QueryString["mmvid"] == null)
            {
                // SetLink(FarRightText, FarRightHref);
            }
        }

        public void SetAstroScopeCrumbs(string SecondText,
            string SecondHref,
            string UserName,
            string UserNameHref,
            string ThirdText,
            string ThirdHref,
            string FourthText,
            string NextText,
            string NextLink,
            string FarRightText,
            string FarRightHref)
        {
            lnkNav1.Visible = true;
            lnkNav2.Visible = true;
            lnkNav3.Visible = true;
            ltlArrow3.Visible = true;
            ltlArrow2.Visible = true;
            ltlNameArrows.Visible = true;
            ltlName.Visible = true;
            lnkMore.Visible = true;
            lnkNav4.Visible = true;
            ltlArrow4.Visible = true;

            lblCurrentPage.Visible = false;

            if (ThirdText == null || ThirdHref == null)
            {
                lnkNav1.Visible = false;
                ltlArrow2.Visible = false;
            }
            else
            {
                lnkNav1.Text = ThirdText;
                lnkNav1.NavigateUrl = ThirdHref;
                lnkNav1.CssClass = linkClass;
            }

            lnkNav2.Text = SecondText;
            lnkNav2.NavigateUrl = SecondHref;
            lnkNav2.CssClass = linkClass;

            lnkNav3.Text = g.GetResource("HOME", this);
            lnkNav3.NavigateUrl = GetHomepageLink();

            if (UserName != null)
            {
                lnkNav4.Text = UserName;

                if (UserNameHref != null)
                    lnkNav4.NavigateUrl = UserNameHref;
            }

            if (FourthText != null)
            {
                ltlName.Visible = true;
                ltlName.Text = FourthText;

                ltlNameArrows.Visible = true;
            }

            if (NextLink != null && g.Member != null)
            {
                lnkMore.Visible = true;
                lnkMore.NavigateUrl = NextLink;
                lnkMore.Text = NextText;
                lnkMore.CssClass = linkClass;
            }
            else
            {
                lnkMore.Visible = false;
            }

            SetLink(FarRightText, FarRightHref);
        }

        public void SetNavLink(string Text, string Href, int level)
        {
            if (Href == null)
            {
                Href = "";
            }

            if (level == 1)
            {
                if (Text != null && !Text.Equals(String.Empty))
                {
                    lblCurrentPage.Text = "<strong>" + Text + "</strong>";
                    lblCurrentPage.Visible = true;
                }
                else
                {
                    lblCurrentPage.Text = String.Empty;
                    lblCurrentPage.Visible = false;
                }
                lblCurrentPage.CssClass = linkClass;
            }
            else if (level == 2)
            {
                lnkNav2.Visible = true;
                lnkNav2.Text = Text;

                // if a link is going to subscribe, make it https
                // this should only be occuring at level 2
                if (Href.ToLower().EndsWith("subscribe.aspx"))
                {
                    lnkNav2.NavigateUrl = FrameworkGlobals.LinkHrefSSL(Href);
                }
                else
                {
                    lnkNav2.NavigateUrl = FrameworkGlobals.LinkHref(Href, true);
                }

                if (Text == g.GetResource("HOME", this))
                {
                    lnkNav2.CssClass = "Breadhome";
                }
                else
                {
                    lnkNav2.CssClass = linkClass;
                }
                ltlArrow2.Visible = true;
            }
            else if (level == 3)
            {
                lnkNav3.Visible = true;
                lnkNav3.Text = Text;
                lnkNav3.NavigateUrl = FrameworkGlobals.LinkHref(Href, true);
                //				lnkNav3.CssClass = linkClass;
                ltlArrow3.Visible = true;
            }
            else if (level == 4)
            {
                lnkNav4.Visible = true;
                lnkNav4.Text = Text;
                lnkNav4.NavigateUrl = FrameworkGlobals.LinkHref(Href, true);
                lnkNav4.CssClass = linkClass;
                ltlArrow4.Visible = true;
            }
        }

        // Returns absolute URL for better SEO MPR-698
        private string GetHomepageLink()
        {
            return FrameworkGlobals.GetHomepageAbsURL(g, Request.ServerVariables.Get("HTTP_HOST"));
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Init += new System.EventHandler(this.Page_Init);

        }
        #endregion
    }
}
