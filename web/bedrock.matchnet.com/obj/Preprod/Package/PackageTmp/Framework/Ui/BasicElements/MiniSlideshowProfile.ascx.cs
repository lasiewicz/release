﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Member.ServiceAdapters.Interfaces;
using Matchnet.Member.ValueObjects.Interfaces;
using Matchnet.Web.Applications.MemberProfile;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects.Photos;
using Matchnet.Member.ValueObjects;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Content.ValueObjects.Region;
using Matchnet.MemberSlideshow.ServiceAdapters;
using Matchnet.MemberSlideshow.ValueObjects;
using Matchnet.List.ServiceAdapters;
using Matchnet.List.ValueObjects;
using Matchnet.Web.Framework.Managers;
using Matchnet.Web.Framework.Util;
using Matchnet.Search.ServiceAdapters;
using Matchnet.Search.ValueObjects;
using Matchnet.Web.Analytics;

namespace Matchnet.Web.Framework.Ui.BasicElements
{
    public partial class MiniSlideshowProfile : FrameworkControl
    {
        private bool _showUpdatedWideStyle;
        
        private IMemberDTO _slideshowMember;
        private BreadCrumbHelper.EntryPoint _entryPoint = BreadCrumbHelper.EntryPoint.MemberSlideshow;
        public string ViewProfileLinkFormat { get; set; }
        public bool ReloadAds { get; set; }
        public string OmniturePageName { get; set; }
        public bool PopulateYNMBuckets { get; set; }
        public string SlideshowOuterContainerDivID { get; set; }
        public bool ShowUpdatedWideStyle
        {
            get { return _showUpdatedWideStyle; }
            set 
            { 
                _showUpdatedWideStyle = value;
                slideshowPreferences.ShowUpdatedWideStyle = value;
            }
        }

        public void Load(bool updatePosition)
        {
            try
            {
                bool loadSuccess = LoadSlideshowMember(updatePosition);
                if (loadSuccess)
                {
                    InitializePage();
                    DisplayBeautyAndBeastPromotion();
                }
                else
                {
                    DisplaySlideshowEnd();
                }
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        public void LoadPreferences()
        {
            slideshowPreferences.LoadPreferences(SlideshowDisplayType.Mini, SlideShowModeType.HomePage, SlideshowOuterContainerDivID);
        }

        private bool LoadSlideshowMember(bool updatePosition)
        {
            try
            {
                if (ShowUpdatedWideStyle)
                {
                    UpdatedWideStyle();
                }

                MemberSlideshowManager manager = new MemberSlideshowManager(g.Member.MemberID, g.Brand);
                IMember validMemberFromSlideshow = manager.GetValidMemberFromSlideshow(updatePosition);
                _slideshowMember = (null != validMemberFromSlideshow) ? validMemberFromSlideshow.GetIMemberDTO() : null;
                return _slideshowMember != null;
            }
            catch (Exception ex)
            { g.ProcessException(ex); return false; }
        }

        private string GetYNMParams()
        {
            YNMVoteParameters parameters = new YNMVoteParameters();

            parameters.SiteID = _g.Brand.Site.SiteID;
            parameters.CommunityID = _g.Brand.Site.Community.CommunityID;
            parameters.FromMemberID = _g.Member.MemberID;
            parameters.ToMemberID = _slideshowMember.MemberID;
            
            return parameters.EncodeParams();

        }

        private void UpdatedWideStyle()
        {
            plcRegularTitle.Visible = false;
            plcShiftedTitle.Visible = true;

            ulButtons.Attributes["class"]  += " wide";
        }

        private void InitializePage()
        {
            pnlSlideshowEnd.Visible = false;
            pnlAdmirer.Visible = true;
            
            WebConstants constants = new WebConstants();

            miniSlideshowContainer.Attributes.Add("data-displaytype", SlideshowDisplayType.Mini.ToString("d"));
            miniSlideshowContainer.Attributes.Add("data-urlparamslideshowdisplaytype", WebConstants.URL_PARAMETER_SLIDESHOW_DISPLAY_TYPE);
            miniSlideshowContainer.Attributes.Add("data-urlparamslideshowoutercontainerdiv", WebConstants.URL_PARAMETER_SLIDESHOW_OUTER_CONTAINER_DIV);
            miniSlideshowContainer.Attributes.Add("data-urlparamslideshowshowupdatedwidestyle", WebConstants.URL_PARAMETER_SLIDESHOW_SHOW_UPDATED_WIDE_STYLE);
            miniSlideshowContainer.Attributes.Add("data-ynmparams", GetYNMParams());
            miniSlideshowContainer.Attributes.Add("data-slideshowmemberid", _slideshowMember.MemberID.ToString());
            miniSlideshowContainer.Attributes.Add("data-reloadcontent", "true");
            miniSlideshowContainer.Attributes.Add("data-displayedinrightcontrol", "true");
            miniSlideshowContainer.Attributes.Add("data-outercontainerdivid", SlideshowOuterContainerDivID);
            miniSlideshowContainer.Attributes.Add("data-reloadads", ReloadAds.ToString().ToLower());
            miniSlideshowContainer.Attributes.Add("data-populateynmbuckets", PopulateYNMBuckets.ToString().ToLower());
            miniSlideshowContainer.Attributes.Add("data-omniturepagename", OmniturePageName);
            miniSlideshowContainer.Attributes.Add("data-slideshowmode", SlideShowModeType.HomePage.ToString("d"));
            miniSlideshowContainer.Attributes.Add("data-showupdatedwidestyle", ShowUpdatedWideStyle ? "true" : "false");

            spanYes.Attributes.Add("data-clicktype", ((int)ClickListType.Yes).ToString());
            spanYes.Attributes.Add("data-containerdivid", miniSlideshowContainer.ClientID);
            spanNo.Attributes.Add("data-clicktype", ((int)ClickListType.No).ToString());
            spanNo.Attributes.Add("data-containerdivid", miniSlideshowContainer.ClientID);
            spanMaybe.Attributes.Add("data-clicktype", ((int)ClickListType.Maybe).ToString());
            spanMaybe.Attributes.Add("data-containerdivid", miniSlideshowContainer.ClientID);

            spanYes.Attributes.Add("title", g.GetResource("TTL_YNM_BUTTON_YES", this));
            spanNo.Attributes.Add("title", g.GetResource("TTL_YNM_BUTTON_NO", this));
            spanMaybe.Attributes.Add("title", g.GetResource("TTL_YNM_BUTTON_MAYBE", this));

            /*
            spanYes.Attributes.Add("onclick", string.Format(constants.JSCRIPT_MINISLIDESHOW_VOTE_NO_SUBMIT, GetYNMParams(), (int)ClickListType.Yes, _slideshowMember.MemberID, "true"));
            spanYes.Attributes.Add("title", g.GetResource("TTL_YNM_BUTTON_YES", this));
            spanNo.Attributes.Add("onclick", string.Format(constants.JSCRIPT_MINISLIDESHOW_VOTE_NO_SUBMIT, GetYNMParams(), (int)ClickListType.No, _slideshowMember.MemberID, "true"));
            spanNo.Attributes.Add("title", g.GetResource("TTL_YNM_BUTTON_NO", this));
            spanMaybe.Attributes.Add("onclick", string.Format(constants.JSCRIPT_MINISLIDESHOW_VOTE_NO_SUBMIT, GetYNMParams(), (int)ClickListType.Maybe, _slideshowMember.MemberID, "true"));
            spanMaybe.Attributes.Add("title", g.GetResource("TTL_YNM_BUTTON_MAYBE", this));*/

            string ViewProfileUrl = BreadCrumbHelper.MakeViewProfileLink(_entryPoint, _slideshowMember.MemberID);

            ViewProfileLinkFormat = ViewProfileUrl.Replace(_slideshowMember.MemberID.ToString(), "{0}");

            lnkUserName.NavigateUrl = g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.ViewProfile, WebConstants.Action.ProfileName, ViewProfileUrl, "Slideshow");
            lnkUserName.Attributes.Add("title", _slideshowMember.GetUserName(_g.Brand));
            lnkUserName.Text = FrameworkGlobals.Ellipsis(_slideshowMember.GetUserName(_g.Brand), 17);
            lnkPhoto.NavigateUrl = g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.ViewProfile, WebConstants.Action.ProfilePhoto, ViewProfileUrl, "Slideshow");

            litAge.Text = FrameworkGlobals.GetAge(_slideshowMember, g.Brand).ToString();
            if (litAge.Text.Trim() == "")
            {
                litAge.Text = "&nbsp;";
            }
            else
            {
                if (FrameworkGlobals.isHebrewSite(g.Brand))
                {
                    litAge.Text = g.GetResource("TXT_YEARS_OLD", this) + " " + litAge.Text;
                }
                else
                {
                    litAge.Text += " " + g.GetResource("TXT_YEARS_OLD", this);
                }
            }

            litLocation.Text = ProfileDisplayHelper.GetRegionDisplay(_slideshowMember, g);
            if (this.litLocation.Text.Trim() == "") this.litLocation.Text = "&nbsp;";

            DisplayMemberPhoto();
        }

        private void DisplaySlideshowEnd()
        {
            pnlSlideshowEnd.Visible = true;
            pnlAdmirer.Visible = false;
        }

        private void DisplayMemberPhoto()
        {
            var photo = MemberPhotoDisplayManager.Instance.GetRandomApprovedForMainPhoto(g.Member, _slideshowMember,
                                                                                         g.Brand);

            if (photo != null)
            {
                var imageUrl = MemberPhotoDisplayManager.Instance.GetPhotoDisplayURL(g.Member, _slideshowMember, g.Brand,
                                                                                     photo, PhotoType.Full,
                                                                                     PrivatePhotoImageType.Full);
                imgProfile.ImageUrl = imageUrl;
            }

            imgProfile.Visible = (photo != null);
        }

        private void DisplayBeautyAndBeastPromotion()
        {
            bool isEnabled = PromotionHelper.IsBeautyAndBeastPromotionEnabled(g);
            if (isEnabled)
            {
                phBeautyBeastImage.Visible = true;
                phBeautyBeastTrackingPixel.Visible = true;
                miniSlideshowContainer.Attributes.Add("data-beautybeastpromotionomniture", PromotionHelper.BeautyBeastPromotionOmniture);
            }
        }

        public string GetAgeFormat()
        {
            string age = "{0}";

            if (FrameworkGlobals.isHebrewSite(g.Brand))
            {
                age = g.GetResource("TXT_YEARS_OLD", this) + " " + age;
            }
            else
            {
                age += " " + g.GetResource("TXT_YEARS_OLD", this);
            }

            return age;
        }
        
        protected void Page_Load(object sender, EventArgs e)
        {
            SlideshowOuterContainerDivID = "miniSlideshow";
            ReloadAds  = true;
            OmniturePageName = "Home Page Slideshow";
            PopulateYNMBuckets = false;
        }

        protected override void OnPreRender(EventArgs e)
        {
            if (g.AnalyticsOmniture != null)
            {
                if (phBeautyBeastImage.Visible)
                {
                    g.AnalyticsOmniture.Prop30 = PromotionHelper.BeautyBeastPromotionOmniture;
                }
            }
            base.OnPreRender(e);
        }
    }
}