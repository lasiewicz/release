namespace Matchnet.Web.Framework.Ui.BasicElements
{
	using System;
	using System.Collections;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.UI;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;

	using Matchnet.Lib.Util;
	using Matchnet.List.ServiceAdapters;
	using Matchnet.List.ValueObjects;
	using Matchnet.Web.Framework;
    using Matchnet.UserNotifications.ServiceAdapters;
    using Matchnet.UserNotifications.ValueObjects;
    using Matchnet.Web.Applications.UserNotifications;
    using Matchnet.Web.Applications.MemberProfile;

	/// <summary>
	///		Summary description for Add2List.
	/// </summary>
	public class Add2List : FrameworkControl
	{
		#region Class members

        private int _memberIDloggedIn = 0;
        private bool _blockedMember = false;
        private bool _displayAsFavorite = false;

        private bool _displayNewFavoriteBtn= true;
        private string _displayFavoriteAjaxHTML = "display:none";

        private string _displayAsFavoriteHTML = "display:inline-block";

        private string  _displayFavoriteOrigHTML = "display:none";
        private string _displayNewFavoriteBtnHTML = "display:none";

        private string _displayFavoriteSpriteHTML = "display:none";
        private string _displayFavoriteImageHTML = "display:none";

        protected string TextFavoriteHover { get; set; }
        protected string TextUnfavoriteHover { get; set; }

		private string _cleanUniqueID;
		private bool _isMemberOfVoluntaryList;
		private bool _showBlock;
		private int _memberID;
		private Add2ListOrientation _orientation = Add2ListOrientation.Horizontal;
		private Matchnet.List.ValueObjects.HotListCategory _hotListID;
		protected System.Web.UI.WebControls.Repeater rptActionOptions;
		private ArrayList _options = new ArrayList();
		protected System.Web.UI.WebControls.PlaceHolder nonLoggedInUserDisplay;
		protected System.Web.UI.WebControls.PlaceHolder loggedInUserDisplay;
        protected Matchnet.Web.Framework.Image ImageListNonLoggedIn;
		protected Matchnet.Web.Framework.Image ImageListLoggedIn;
		protected Matchnet.Web.Framework.Txt txtHotListNonLoggedIn;
		protected Matchnet.Web.Framework.Txt txtHotListLoggedIn;
		protected System.Web.UI.HtmlControls.HtmlAnchor lnkNonLoggedInUserDest;
		private bool _IncludeSpanAroundDescriptionText = false;
        private bool _ClearIconInlineAlignment = false;
        private bool _DisplayLargeIcon = false;
        private string _ExtraJavaScriptToRun_HotlistLoggedInOnClick = "";
        protected PlaceHolder placeHolderBRTag;
        protected System.Web.UI.HtmlControls.HtmlGenericControl spanSpriteNonLoggedIn;
        protected System.Web.UI.HtmlControls.HtmlGenericControl spanSpriteLoggedIn;
        protected Matchnet.Web.Framework.Txt txtSpriteLoggedIn;
        protected Matchnet.Web.Framework.Txt txtSpriteNonLogged;
        protected PlaceHolder phSpriteNonLoggedIn;
        protected PlaceHolder phSpriteLoggedIn;
        protected string _HotlistTitle;

        protected System.Web.UI.HtmlControls.HtmlInputHidden actionCallPage;
        protected System.Web.UI.HtmlControls.HtmlInputHidden actionCallPageDetail;

		#endregion

        private string _iconHotListFileName = "icon-hotlist.gif";
        private string _iconHotListRemoveFileName = "icon_hotlist_remove.gif";

    
        
		/// <summary>
		/// Returns the number of pixels below the Hotlist link that the drop down menu should appear
		/// when you click to Hotlist a member profile. When the hotlist link is "Icon Only"
		/// or "Horizontal" then the dropdown menu should appear approx 18 pixels below the icon. 
		/// When the hotlist link is "Vertical" (meaning it has an icon and text below it) then 
		/// the dropdown menu should appear further down from the icon, so it doesn't overlap it.
		/// TT 18050. It goes without saying that this control has a flawed UI, and must be rewritten.
		/// </summary>
		/// <returns></returns>
		protected string GetDropDownMenuOffset() 
		{
			switch (Orientation) 
			{
				case Add2ListOrientation.Vertical:
					return "32px";//a little more space so that dropdown menu won't cover hotlist icon.
				case Add2ListOrientation.Horizontal:
                case Add2ListOrientation.TextOnly:
				case Add2ListOrientation.IconOnly:
                    default: return "18px";
            }
		}

        public bool IsJMeterPage { get; set; }


		private void Page_Load(object sender, System.EventArgs e)
		{

			DetermineShowBlock();

            TextFavoriteHover = ResourceManager.GetResource("ALT_ADD_TO_FAVORITES", this);
            TextUnfavoriteHover = ResourceManager.GetResource("ALT_REMOVE_FROM_FAVORITES", this);
     
            //set alt resource
            if (String.IsNullOrEmpty(HotlistTitleResourceConstant))
                HotlistTitleResourceConstant = "ALT_ADD_TO_FAVORITES";

            if (String.IsNullOrEmpty(UnHotlistTitleResourceConstant))
                UnHotlistTitleResourceConstant = "ALT_REMOVE_FROM_FAVORITES";

			if(g.Member != null)
			{
                _memberIDloggedIn = g.Member.MemberID;

                 if (IsMemberInBlockedListOnly())
                {
                    _blockedMember = true;
                }

                CustomCategoryCollection customCategories = ListSA.Instance.GetCustomListCategories(g.Session.GetInt(WebConstants.SESSION_PROPERTY_NAME_MEMBERID), g.Brand.Site.Community.CommunityID);
                int customCategoriesCount = customCategories.Count;

                //Get Favorite Onclick setting for site:
                bool site_setting = Conversion.CBool(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ENABLE_FAVORITE_ONECLICK", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID));


                //If the member has custom lists or the displayed member is blocked or not enabled for Site, use old favorite button:
                if (customCategoriesCount >= 1 || _blockedMember == true || site_setting == false)
                {
                    _displayNewFavoriteBtn = false;
                    _displayFavoriteAjaxHTML = "display:none";
                    _displayFavoriteOrigHTML = "display:inline-block";
                    _displayNewFavoriteBtnHTML = "display:none";
                }
                else
                {
                    _displayFavoriteAjaxHTML = "display:inline-block";
                    _displayFavoriteOrigHTML = "display:none";
                    _displayNewFavoriteBtnHTML = "display:inline-block";
                }
               
				_isMemberOfVoluntaryList = g.List.IsMemberOfVoluntaryList(MemberID, g.Brand.Site.Community.CommunityID);

                if (_isMemberOfVoluntaryList == true)  {
                    _displayAsFavorite = true;
                    _displayAsFavoriteHTML = "display:inline-block";
                }
                else
                {
                    _displayAsFavorite = false;
                    _displayAsFavoriteHTML = "display:none";
                }

                bool DisplayAsFavorited = _isMemberOfVoluntaryList;
                if (DisplayAsFavorited && ExcludeBlockAsFavorited)
                {
                    // If member is part of blocked list, don't show indicator that member is favorited
                    DisplayAsFavorited = !IsMemberInBlockedListOnly();
                }

                //	Logged in user
				//	Have to rebuild dynamic objects so that link button clicks have a place to land
				
				loggedInUserDisplay.Visible = true;
				nonLoggedInUserDisplay.Visible = false;


                if (!String.IsNullOrEmpty(HotlistResourceConstant))
                {
                    txtHotListLoggedIn.ResourceConstant = HotlistResourceConstant;
                }
                else
                {
                    if (IsJMeterPage)
                    {
                        txtHotListLoggedIn.ResourceConstant = "TXT_ADD2LIST_JMETER";
                    }
                }

				//	If the member being displayed is a member of a voluntary list, we need to change
				//	the display text to MOVE from ADD2LIST.
                _HotlistTitle = g.GetResource(HotlistTitleResourceConstant, this);
                if (EnableSprite)
                {
                    ImageListLoggedIn.Visible = false;
                    if (!String.IsNullOrEmpty(SpriteHotlistCSS))
                    {
                        _displayFavoriteSpriteHTML = "display:inline-block";
                        _displayNewFavoriteBtnHTML = "display:none";
                        _displayFavoriteImageHTML =  "display:none";
                        phSpriteLoggedIn.Visible = true;
                        spanSpriteLoggedIn.Attributes["class"] = SpriteHotlistCSS;
                    }
                    else
                        phSpriteLoggedIn.Visible = false;
                }
                else
                {
                    _displayFavoriteSpriteHTML = "display:none";
                    _displayNewFavoriteBtnHTML = "display:none";
                    _displayFavoriteImageHTML = "display:inline-block";
                    phSpriteLoggedIn.Visible = false;
                    ImageListLoggedIn.FileName = _iconHotListFileName;

                    if (Orientation == Matchnet.Web.Framework.Ui.BasicElements.Add2ListOrientation.IconOnly)
                    {
                        ImageListLoggedIn.TitleResourceConstant = "";
                    }
                    else
                    {
                        ImageListLoggedIn.TitleResourceConstant = g.GetResource(HotlistTitleResourceConstant, this);
                    }

                    if (true)
                    {
                        
                    }
                }

				if(DisplayAsFavorited)
				{
					txtHotListLoggedIn.ResourceConstant = "TXT_MOVE";
                    _HotlistTitle = g.GetResource(UnHotlistTitleResourceConstant, this);
                    if (EnableSprite)
                    {
                        txtSpriteLoggedIn.ResourceConstant = "ALT_MOVE_ME";
                        if (!string.IsNullOrEmpty(SprintUnHotlistCSS))
                        {
                            phSpriteLoggedIn.Visible = true;
                            spanSpriteLoggedIn.Attributes["class"] = SprintUnHotlistCSS;
                        }
                        else
                            phSpriteLoggedIn.Visible = false;

                    }
                    else
                    {
                        ImageListLoggedIn.FileName = IconHotListRemove;
                        ImageListLoggedIn.ResourceConstant = "ALT_MOVE_ME";
                        if (Orientation == Matchnet.Web.Framework.Ui.BasicElements.Add2ListOrientation.IconOnly)
                        {
                            ImageListLoggedIn.TitleResourceConstant = "";
                        }
                        else
                        {
                            ImageListLoggedIn.TitleResourceConstant = g.GetResource(UnHotlistTitleResourceConstant, this);
                        }
                    }

                    if (!String.IsNullOrEmpty(UnHotlistResourceConstant))
                    {
                        txtHotListLoggedIn.ResourceConstant = UnHotlistResourceConstant;
                    }
				}
                if (_orientation == Add2ListOrientation.TextOnly)
                {
                    ImageListLoggedIn.Visible = false;
                    phSpriteNonLoggedIn.Visible = false;
                }                

				BuildAndBind();
			} 
			else 
			{
				//	Non-logged in user
				loggedInUserDisplay.Visible = false;
				nonLoggedInUserDisplay.Visible = true;

                if (!String.IsNullOrEmpty(HotlistResourceConstant))
                {
                    txtHotListNonLoggedIn.ResourceConstant = HotlistResourceConstant;
                }

                if (EnableSprite)
                {
                    ImageListNonLoggedIn.Visible = false;
                    if (!String.IsNullOrEmpty(SpriteHotlistCSS))
                    {
                        spanSpriteNonLoggedIn.Attributes["class"] = SpriteHotlistCSS;
                    }
                    else
                        phSpriteNonLoggedIn.Visible = false;
                }
                else
                {
                    phSpriteNonLoggedIn.Visible = false;
                }

				//	If the user isn't logged in, then the framework should trap them trying to get
				//	to the View.aspx page and send them through the login or registration process.
				//	When they get dumped out at the end they should then be redirected to the View.aspx
				//	page where (on non-postbacks) these parameters will be noticed and the ToMember
				//	member will be added to the newly-logged-in user's Favorites list.
				//	This is a little convoluted, but is being done to maintain the pre-existing (YNM4)
				//	behavior.
                ImageListNonLoggedIn.FileName = _iconHotListFileName;
                if (_orientation == Add2ListOrientation.TextOnly)
                {
                    ImageListNonLoggedIn.Visible = false;
                    phSpriteLoggedIn.Visible = false;
                }
				lnkNonLoggedInUserDest.HRef = "/Applications/HotList/View.aspx?RedirectToProfile=1&a=add&ToMemberID=" + MemberID;
			}

			if( FrameworkGlobals.isHebrewSite(g.Brand) )
			{
				ImageListLoggedIn.ResourceConstant = "";
                ImageListNonLoggedIn.ResourceConstant = "";
			}

            if (Convert.ToBoolean(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("MINGLE_PROFILE_VIEW", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID)))
            {
                placeHolderBRTag.Visible = false;
            }

            if (_g.AppPage != null && _g.AppPage.ControlName.ToLower() == "viewprofilefbtab")
            {
                string host = HttpContext.Current.Request.Url.Host;
                lnkNonLoggedInUserDest.HRef = "http://" + host +
                            "/Applications/MemberProfile/ViewProfile.aspx?prm=80633&mailtypeid=1&MemberID=" +
                            _memberID;

                loggedInUserDisplay.Visible = false;
                nonLoggedInUserDisplay.Visible = true;
                ImageListNonLoggedIn.FileName = "icon-hotlist-add.gif";

            }
		}

		protected override void OnPreRender(EventArgs e)
		{
			if (!this._ClearIconInlineAlignment && !EnableSprite)
			{
				//add default inline styles for the icons
                this.ImageListNonLoggedIn.Attributes.Add("hspace", "2");
                this.ImageListNonLoggedIn.Attributes.Add("vspace", "0");
                this.ImageListNonLoggedIn.Attributes.Add("align", "absmiddle");

				this.ImageListLoggedIn.Attributes.Add("hspace", "2");
				this.ImageListLoggedIn.Attributes.Add("vspace", "0");
				this.ImageListLoggedIn.Attributes.Add("align", "absmiddle");
			}

            if (this._DisplayLargeIcon && !EnableSprite)
            {
                this.ImageListLoggedIn.FileName = this.ImageListLoggedIn.FileName.Replace(".gif", "_lrg.gif");// = "icon_hotlist_lrg.gif";
                this.ImageListNonLoggedIn.FileName = this.ImageListNonLoggedIn.FileName.Replace(".gif", "_lrg.gif");// = "icon_hotlist_lrg.gif";
            }

            //omniture action
            if (!Page.IsPostBack)
            {
                if (g.AnalyticsOmniture != null)
                {
                    if (!String.IsNullOrEmpty(g.Session.GetString(WebConstants.SESSION_PROPERTY_NAME_HOTLIST_ACTION)))
                    {
                        g.AnalyticsOmniture.Evar27 = g.Session[WebConstants.SESSION_PROPERTY_NAME_HOTLIST_ACTION];
                        g.AnalyticsOmniture.AddEvent("event27");
                    }
                }
                g.Session.Remove(WebConstants.SESSION_PROPERTY_NAME_HOTLIST_ACTION);
            }

			base.OnPreRender (e);
		}


		#region Page set up methods

		private void BuildAndBind()
		{
            try
            {
                int appID = g.AppPage.App.ID;
                if (appID == 26) //	HotList
                {
                    this.HotListID = (Matchnet.List.ValueObjects.HotListCategory)Matchnet.Conversion.CInt(Request["CategoryID"]);
                }
            }
            catch (Exception ex) 
            { ;}
			SetUpOptions();
			
			rptActionOptions.DataSource = _options;
			rptActionOptions.DataBind();
		}

		private void DetermineShowBlock()
		{
			_showBlock = false;

            if (g.AppPage != null)
            {
                int appID = g.AppPage.App.ID;
                int pageID = g.AppPage.ID;

                //	Only show the block option if we are in the Instant Messenger application or if
                //	we are on the view email page.

                if (appID == 19 || pageID == 27020)
                {
                    _showBlock = true;
                }
            }
		}

		private void SetUpOptions()
		{
			//	If the user is not logged in then they shouldn't see any options - just
			//	the Add2List link.
            try
            {
                if (g.Member == null)
                {
                    return;
                }

                //	UNLIST from a Custom List or Favorites (voluntaryList) option
                CustomCategoryCollection customCategories = ListSA.Instance.GetCustomListCategories(g.Session.GetInt(WebConstants.SESSION_PROPERTY_NAME_MEMBERID), g.Brand.Site.Community.CommunityID);

                bool inCustomList = false;
                foreach (CustomCategory category in customCategories)
                {
                    if (g.List.IsHotListed((HotListCategory)category.CategoryID, g.Brand.Site.Community.CommunityID, MemberID))
                    {
                        _options.Add(new HotListDestinationOption(
                            "UNLIST " + category.CategoryID.ToString(),
                            ActionType.UNLIST,
                            category.CategoryID,
                            category.CategoryID));

                        inCustomList = true;
                    }
                }

                if (!inCustomList)
                {
                    if (_isMemberOfVoluntaryList)
                    {
                        // Fix this logic here. DetermineVoluntaryList method actually catches the first list that contains the memberid, which means
                        // if the same member is on multiple lists, we can get the wrong list. With that said, it looks like all the existing code depends on
                        // this logic, so I just made exception cases for the new ExcludeList and ExcludeFromList.
                        int voluntaryList = g.List.DetermineVoluntaryList(MemberID, g.Brand.Site.Community.CommunityID);

                        if (HotListID == HotListCategory.ExcludeFromList || HotListID == HotListCategory.ExcludeList)
                            voluntaryList = (int)HotListID;

                        _options.Add(new HotListDestinationOption("UNLIST", ActionType.UNLIST, voluntaryList, Constants.NULL_INT));
                    }
                }

                //	Move to Favorites option
                if (!g.List.IsHotListed(Matchnet.List.ValueObjects.HotListCategory.Default, g.Brand.Site.Community.CommunityID, MemberID))
                {
                    _options.Add(new HotListDestinationOption(
                        "MOVE " + Matchnet.List.ValueObjects.HotListCategory.Default.ToString(),
                        ActionType.ADD,
                        (int)_hotListID,
                        (int)Matchnet.List.ValueObjects.HotListCategory.Default));
                }

                //	Move to other custom lists option
                foreach (CustomCategory category in customCategories)
                {
                    if (!g.List.IsHotListed((HotListCategory)category.CategoryID, g.Brand.Site.Community.CommunityID, MemberID))
                    {
                        _options.Add(new HotListDestinationOption(
                            "MOVE " + category.CategoryID.ToString(),
                            ActionType.ADD,
                            (int)_hotListID,
                            category.CategoryID));
                    }
                }

                // Allow for moving back to Favorites if the viewed member is in an existing custom list  (TT15322)
                inCustomList = false;
                foreach (CustomCategory category in customCategories)
                {
                    if (g.List.IsHotListed((HotListCategory)category.CategoryID, g.Brand.Site.Community.CommunityID, MemberID))
                    {
                        inCustomList = true;
                    }
                }
                if (inCustomList)
                {
                    _options.Add(new HotListDestinationOption(
                        "MOVE " + Matchnet.List.ValueObjects.HotListCategory.Default.ToString(),
                        ActionType.ADD,
                        (int)_hotListID,
                        (int)Matchnet.List.ValueObjects.HotListCategory.Default));
                }


                //	Move to blocked list option
                if (ShowBlock)
                {
                    if (!g.List.IsHotListed(Matchnet.List.ValueObjects.HotListCategory.IgnoreList, g.Brand.Site.Community.CommunityID, MemberID))
                    {
                        _options.Add(new HotListDestinationOption(
                            "BLOCK " + Matchnet.List.ValueObjects.HotListCategory.Default.ToString(),
                            ActionType.BLOCK,
                            (int)_hotListID,
                            (int)Matchnet.List.ValueObjects.HotListCategory.IgnoreList));
                    }
                }
            }
            catch (Exception ex)
            { g.ProcessException(ex); }
		}

        private bool IsMemberInBlockedListOnly()
        {
            int firstVoluntaryListID = g.List.DetermineVoluntaryList(MemberID, g.Brand.Site.Community.CommunityID);
            if (firstVoluntaryListID == (int)Matchnet.List.ValueObjects.HotListCategory.IgnoreList
                || firstVoluntaryListID == (int)Matchnet.List.ValueObjects.HotListCategory.ExcludeList
                || firstVoluntaryListID == (int)Matchnet.List.ValueObjects.HotListCategory.ExcludeFromList)
            {
                return true;
            }

            return false;
        }

		#endregion

		/// <summary>
		/// Hate to do this with a redirect, but the changes performed after a button click
		/// could alter the left nav stuff.
		/// </summary>
		private void RedirectAfterAction(string resourceConstant)
		{
			string url = Request.RawUrl;
            if (url.Contains("404"))
            {
                try
                {
                    // string tempURL = Server.UrlDecode(Request.RawUrl).Split(new string[] { ":80" }, StringSplitOptions.None)[1];
                    string tempURL = Request.Path;
                    url = tempURL;
                    if (Request.RawUrl.Contains("religious-dating"))
                    {
                        url += "?section=1";
                    }
                    else if (Request.RawUrl.Contains("academic-dating"))
                    {
                        url += "?section=2";
                    }

                }
                catch (Exception)
                {
                }
            }
			Int32 qsStart = url.IndexOf('?');

			if (qsStart != -1)
				url = url.Substring(0, qsStart) + BuildRedirectionQueryString(url.Substring(qsStart + 1), resourceConstant, GetStartRow());
			else
				url = url + BuildRedirectionQueryString(String.Empty, resourceConstant, GetStartRow());

			g.Transfer(url);
		}

		private string BuildRedirectionQueryString(String queryString, String resourceConstant, Int32 startRow)
		{
			const String QSPARAM_RESOURCECONSTANT = "rc";
			Boolean rcAdded = false;
			Boolean srAdded = false;

			String[] aryQS = queryString.Split('&');
			String qsNew = "?";

			if (queryString != String.Empty)
			{
				// Iterate through the queryString values and replace/add values as necessary.
				for (int i = 0; i <= aryQS.GetUpperBound(0); i++)
				{
					Boolean addValue = true;

					// ResourceConstant.
					if ((aryQS[i].Length >= QSPARAM_RESOURCECONSTANT.Length) && (aryQS[i].Substring(0, QSPARAM_RESOURCECONSTANT.Length) == QSPARAM_RESOURCECONSTANT))
					{
						if (resourceConstant == null)
						{
							// Remove existing value, if one exists.
							addValue = false;
						}
						else
						{
							// Replace the existing value.
							aryQS[i] = QSPARAM_RESOURCECONSTANT + "=" + resourceConstant;
							rcAdded = true;
						}
					}

					// StartRow.
                    if ((aryQS[i].Length >= ResultListHandler.QSPARAM_STARTROW.Length) && (aryQS[i].Substring(0, ResultListHandler.QSPARAM_STARTROW.Length) == ResultListHandler.QSPARAM_STARTROW))
					{
						if (startRow == Constants.NULL_INT)
						{
							// Remove existing value, if one exists.
                            addValue = true;//false;
						}
						else
						{
							// Replace the existing value.
							aryQS[i] = ResultListHandler.QSPARAM_STARTROW + "=" + startRow.ToString();
							srAdded = true;
						}
					}

					// Add the qs name and value to the new queryString.
					if (addValue && aryQS[i] != String.Empty)
						qsNew += aryQS[i] + "&";
				}
			}

			// If the values weren't replaced then they need to be added.
			if (resourceConstant != null && rcAdded == false)
				qsNew += QSPARAM_RESOURCECONSTANT + "=" + resourceConstant + "&";

			if (startRow != Constants.NULL_INT && srAdded == false)
				qsNew += ResultListHandler.QSPARAM_STARTROW + "=" + startRow.ToString() + "&";

			if (qsNew.Length <= 2)
				return String.Empty; // "?&".
			else
				return qsNew.Substring(0, qsNew.Length - 1);	// Remove the trailing "&".
		}

		private int GetStartRow()
		{
			// Recurse through the parent controls.  If one of the controls is "ResultList" retrieve the StartRow value.
			Control parentCtrl = ((Control)(this)).Parent;
			while (parentCtrl != null)
			{
				if (parentCtrl.GetType().Name == "ResultList_ascx")
				{
					return ((ResultList)parentCtrl)._StartRow;
				}

				parentCtrl = parentCtrl.Parent;
			}

			return Constants.NULL_INT;
		}

		private void RedirectAfterAction()
		{
			RedirectAfterAction(null);
		}

		/// <summary>
		/// Turn the int listID into a user-viewable string
		/// </summary>
		/// <param name="listID">listID</param>
		/// <returns>user-viewable string</returns>
		private string DetermineListName(int listID)
		{
			string retVal = "Unknown";

			if((HotListCategory)listID == Matchnet.List.ValueObjects.HotListCategory.Default)
			{
				retVal = g.GetResource("LIST_NAME_FAVORITES", this);
			}
			else if((HotListCategory)listID == Matchnet.List.ValueObjects.HotListCategory.IgnoreList)
			{
				retVal = g.GetResource("LIST_NAME_BLOCKED", this);
			}
            else if ((HotListCategory)listID == Matchnet.List.ValueObjects.HotListCategory.ExcludeList)
            {
                retVal = g.GetResource("LIST_NAME_EXCLUDE", this);
            }
            else if ((HotListCategory)listID == Matchnet.List.ValueObjects.HotListCategory.ExcludeFromList)
            {
                retVal = g.GetResource("LIST_NAME_EXCLUDE_FROM", this);
            }
			else if(listID > 0 && g.Member != null)	//	Custom list (for logged in members)
			{
				CustomCategoryCollection customCategories;
				CustomCategory matchingCategory = null;

				customCategories = ListSA.Instance.GetCustomListCategories(g.Member.MemberID, g.Brand.Site.Community.CommunityID);
				foreach(CustomCategory myCategory in customCategories)
				{
					if(myCategory.CategoryID == listID)
					{
						matchingCategory = myCategory;
						break;
					}
				}
				if(matchingCategory != null)
				{
					retVal = matchingCategory.Description;
				}
			}

			return(retVal);
		}

		#region Event callbacks

		protected void rptActionOptions_ItemDataBound(object sender, RepeaterItemEventArgs e)
		{
			if(e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{
				LinkButton lnkTakeAction = (LinkButton)e.Item.FindControl("lnkTakeAction");
				HotListDestinationOption action = (HotListDestinationOption)e.Item.DataItem;

				lnkTakeAction.CommandName = action.CommandName;
				lnkTakeAction.CommandArgument = "MemberID=" + this.MemberID + "," + action.CommandArgument;
				
				if(action.ActionType == ActionType.ADD)
				{
					if(g.Member != null && _isMemberOfVoluntaryList)
					{
						lnkTakeAction.Text = g.GetResource("TXT_MOVE_TO", this) + DetermineListName(action.ToList);
					} 
					else
					{
						lnkTakeAction.Text = g.GetResource("TXT_ADD_TO", this) + DetermineListName(action.ToList);
					}
				} 
				else if (action.ActionType == ActionType.BLOCK)
				{
					lnkTakeAction.Text = g.GetResource("BLOCK_MEMBER", this);
				}
				else if(action.ActionType == ActionType.UNLIST)
				{
					lnkTakeAction.Text = g.GetResource("UNLIST_FROM", this) + DetermineListName(action.FromList);
				}
			}
		}

		protected void lnkTakeAction_Click(object sender, EventArgs e)
		{
			try
			{
				string resultResourceConstant = null;

				LinkButton lnkTakeAction = (LinkButton)sender;
				string commandName = lnkTakeAction.CommandName;
				string commandArguments = lnkTakeAction.CommandArgument;
				string[] aCommandArguments = commandArguments.Split(',');
				Hashtable myParams = new Hashtable();
				foreach(string arg in aCommandArguments)
				{
					string[] vals = arg.Split('=');
					if(vals.Length == 2)
					{
						myParams.Add(vals[0], vals[1]);
					}
					else
					{
						//	Invalid format for args (which should never happen)  Ignore for now.
						//	TODO - at least log this event - dcornell
					}
				}

				int fromList = Matchnet.Conversion.CInt(myParams["FromList"]);
				int toList = Matchnet.Conversion.CInt(myParams["ToList"]);
				int memberID = Matchnet.Conversion.CInt(myParams["MemberID"]);

				Matchnet.List.ValueObjects.ListSaveResult result = null;

				if(ActionType.UNLIST.ToString().Equals(commandName))
				{
					HotListCategory listCategory = (HotListCategory)fromList;
					ListSA.Instance.RemoveListMember(listCategory, g.Brand.Site.Community.CommunityID, g.Member.MemberID, memberID);

                    // If this is an UNLIST action for ExcludeFromList, target member's ExcludeListInternal needs to remove the current memberID
                    if (listCategory == HotListCategory.ExcludeFromList)
                    {
                        ListSA.Instance.RemoveListMember(HotListCategory.ExcludeListInternal, g.Brand.Site.Community.CommunityID, memberID, g.Member.MemberID);
                    }
                    else if (listCategory == HotListCategory.ExcludeList)
                    {
                        // If this is an UNLIST action for ExcludeList, ExcludeListInternal for the current member needs to remove the target memberID
                        ListSA.Instance.RemoveListMember(HotListCategory.ExcludeListInternal, g.Brand.Site.Community.CommunityID, g.Member.MemberID, memberID);
                    }
                    
					// set the result to success
					result = new ListSaveResult();
					result.Status = ListActionStatus.Success;
				} 
				else if(ActionType.ADD.ToString().Equals(commandName))
				{
					HotListCategory fromListCategory = (HotListCategory)fromList;
					HotListCategory toListCategory = (HotListCategory)toList;
					
					// if either memberid is 0 do not insert into the db
					// result will stay null, so the final error message applies correctly
					if(memberID != 0 && memberID != int.MinValue && g.Member.MemberID != 0 && g.Member.MemberID != int.MinValue)
					{
						string comment = String.Empty;

						ListItemDetail detail = g.List.GetListItemDetail(fromListCategory, g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, memberID);
						if (detail != null && detail.Comment != null)
						{
							comment = detail.Comment;
						}

						result = ListSA.Instance.AddListMember(toListCategory,
							g.Brand.Site.Community.CommunityID,
							g.Brand.Site.SiteID,
							g.Member.MemberID,
							memberID,
							comment,
							Constants.NULL_INT,
							!((g.Member.GetAttributeInt(g.Brand, "HideMask", 0) & (Int32)WebConstants.AttributeOptionHideMask.HideHotLists) == (Int32)WebConstants.AttributeOptionHideMask.HideHotLists),
							false);
                        if (toListCategory == (int)Matchnet.List.ValueObjects.HotListCategory.Default)
                        {
                            if (UserNotificationFactory.IsUserNotificationsEnabled(g))
                            {
                                UserNotificationParams unParams = UserNotificationParams.GetParamsObject(memberID, g.Brand.Site.SiteID, g.Brand.Site.Community.CommunityID);
                                UserNotification notification = UserNotificationFactory.Instance.GetUserNotification(new AddedYouAsFavoriteUserNotification(), memberID, g.Member.MemberID, g);
                                if (notification.IsActivated())
                                {
                                    UserNotificationsServiceSA.Instance.AddUserNotification(unParams, notification.CreateViewObject());
                                }
                            }
                        }

					}

                    //11242008 TL - Adding omniture action click event for hotlist to evar27
                    string hotlistAction = "Hot List";
                    if (this.actionCallPage.Value != "")
                    {
                        
                        if (this.actionCallPageDetail.Value == "")
                        {
                            hotlistAction = " (" + this.actionCallPage.Value + ")";
				        }
                        else
                        {
                            hotlistAction = " (" + this.actionCallPage.Value + " - " + this.actionCallPageDetail.Value + ")";
                        }
                    }
                    //save to session to write during GET
                    g.Session.Add(WebConstants.SESSION_PROPERTY_NAME_HOTLIST_ACTION, hotlistAction, Matchnet.Session.ValueObjects.SessionPropertyLifetime.Temporary);
				}
				else if(ActionType.BLOCK.ToString().Equals(commandName))
				{
					HotListCategory fromListCategory = (HotListCategory)fromList;
					HotListCategory toListCategory = HotListCategory.IgnoreList;

					// if either memberid is 0 do not insert into the db
					// result will stay null, so the final error message applies correctly
					if(memberID != 0 && memberID != int.MinValue && g.Member.MemberID != 0 && g.Member.MemberID != int.MinValue)
					{
						result = ListSA.Instance.AddListMember(toListCategory,
							g.Brand.Site.Community.CommunityID,
							g.Brand.Site.SiteID,
							g.Member.MemberID,
							memberID,
							string.Empty,
							Constants.NULL_INT,
							!((g.Member.GetAttributeInt(g.Brand, "HideMask", 0) & (Int32)WebConstants.AttributeOptionHideMask.HideHotLists) == (Int32)WebConstants.AttributeOptionHideMask.HideHotLists),
							false);
					}
				}
				else
				{
					//	Invalid command (which should never happen)  Ignore for now.
					//	TODO - at least log this event - dcornell
				}

				if(result == null)
				{
					// Failure occurred
					resultResourceConstant = "ERROR_YOUR_LIST_HAS_NOT_BEEN_UPDATED";
				} 
				else if(result.Status == ListActionStatus.Success)
				{
					//	Successful.  Add a successmessage.
                    resultResourceConstant = "YOUR_LIST_HAS_BEEN_UPDATED_" + commandName.ToUpper();
				}
				
				//	Make sure we re-render the page
				RedirectAfterAction(resultResourceConstant);
			} 
			catch(Exception ex)
			{
				g.ProcessException(ex);
			}
		}

		#endregion

		/// <summary>
		/// MemberID that this Add2List link is for
		/// </summary>
		public int MemberID
		{
			get { return(_memberID); }
			set { _memberID = value; }
		}

        /// <summary>
        /// MemberID that this Add2List link is for
        /// </summary>
        public int MemberIDloggedIn
        {
            get { return (_memberIDloggedIn); }
            set { _memberIDloggedIn = value; }
        }

         /// <summary>
        /// If the member is blocked or not
        /// </summary>
        public bool BlockedMember
        {
            get { return (_blockedMember); }
            set { _blockedMember = value; }
        }

        /// <summary>
        /// If the member is favorited or not
        /// </summary>
        public bool DisplayAsFavorite
        {
            get { return (_displayAsFavorite); }
            set { _displayAsFavorite = value; }
        }

        /// <summary>
        /// If the new favorite btn should be displayed.
        /// </summary>
        public bool DisplayNewFavoriteBtn
        {
            get { return (_displayNewFavoriteBtn); }
            set { _displayNewFavoriteBtn = value; }
        }

        /// <summary>
        /// HTML to display as favorite
        /// </summary>
        public string DisplayAsFavoriteHTML
        {
            get { return (_displayAsFavoriteHTML); }
            set { _displayAsFavoriteHTML = value; }
        }

        /// <summary>
        /// HTML to display Original Favorite HTML
        /// </summary>
        public string DisplayFavoriteOrigHTML
        {
            get { return (_displayFavoriteOrigHTML); }
            set { _displayFavoriteOrigHTML = value; }
        }

        /// <summary>
        /// HTML to display Ajax Favorite HTML
        /// </summary>
        public string DisplayFavoriteAjaxHTML
        {
            get { return (_displayFavoriteAjaxHTML); }
            set { _displayFavoriteAjaxHTML = value; }
        }

        /// <summary>
        /// HTML to display New Favorite button
        /// </summary>
        public string DisplayNewFavoriteBtnHTML
        {
            get { return (_displayNewFavoriteBtnHTML); }
            set { _displayNewFavoriteBtnHTML = value; }
        }

         /// <summary>
        /// HTML to display New Favorite sprite
        /// </summary>
        public string DisplayFavoriteSpriteHTML
        {
            get { return (_displayFavoriteSpriteHTML); }
            set { _displayFavoriteSpriteHTML = value; }
        }

        /// <summary>
        /// HTML to display New Favorite image
        /// </summary>
        public string DisplayFavoriteImageHTML
        {
            get { return (_displayFavoriteImageHTML); }
            set { _displayFavoriteImageHTML = value; }
        }
        
		/// <summary>
		/// How should this display be rendered (ie vertical or horizontal)
		/// </summary>
		public Add2ListOrientation Orientation
		{
			get { return(_orientation); }
			set { _orientation = value; }
		}

		/// <summary>
		/// If this Add2List is being displayed in a HotList display, which hot list is being displayed?
		/// </summary>
		public Matchnet.List.ValueObjects.HotListCategory HotListID
		{
			get { return(_hotListID); }
			set { _hotListID = value; }
		}

		/// <summary>
		/// Should the option be available to block the user?
		/// </summary>
		public bool ShowBlock
		{
			get { return(_showBlock); }
		}

        
        public string IconHotList
        {
            get { return _iconHotListFileName; }
            set { _iconHotListFileName = value; }
        }

        public string IconHotListRemove
        {
            get { return _iconHotListRemoveFileName; }
            set { _iconHotListRemoveFileName = value; }
        }

		/// <summary>
		/// A cleaned up version of the UniqueID parameter that replaces : characters (bad for JavaScript)
		/// with _ characters (better for JavaScript)  This will allow multiple Add2List controls to be
		/// used on the same page.
		/// </summary>
		public string CleanUniqueID
		{
			get
			{
				if(_cleanUniqueID == null)
				{
					_cleanUniqueID = this.UniqueID.Replace(":", "_");
				}

				return(_cleanUniqueID);
			}
		}

		/// <summary>
		/// Specifies whether to include span tags around the description text for vertical or horizontal orientation.
		/// </summary>
		public bool IncludeSpanAroundDescriptionText
		{
			get { return _IncludeSpanAroundDescriptionText;}
			set { _IncludeSpanAroundDescriptionText = value;}
		}

		/// <summary>
		/// Specifies whether to remove inline style attributes for alignment (e.g. hspace, vspace, align)
		/// </summary>
		public bool ClearIconInlineAlignments
		{
			get { return this._ClearIconInlineAlignment;}
			set { this._ClearIconInlineAlignment = value;}
		}

        /// <summary>
        /// Specifies whether to display the large icon; default is false
        /// </summary>
        public bool DisplayLargeIcon
        {
            get { return this._DisplayLargeIcon; }
            set { this._DisplayLargeIcon = value; }
        }

        /// <summary>
        /// Specifies additional javascript to execute for hotlist onclick event when user is logged in
        /// </summary>
        public string ExtraJavaScriptToRun_HotlistLoggedInOnClick
        {
            get
            {
                return _ExtraJavaScriptToRun_HotlistLoggedInOnClick;
            }
            set
            {
                _ExtraJavaScriptToRun_HotlistLoggedInOnClick = value;
            }
        }

        /// <summary>
        /// The page name where the hotlist action was taken; used for omniture evar27
        /// </summary>
        public string ActionCallPage
        {
            get { return actionCallPage.Value; }
            set { actionCallPage.Value = value; }
        }

        /// <summary>
        /// The subpage or detail, appended to the ActionCallPage; used for omniture evar27
        /// </summary>
        public string ActionCallPageDetail
        {
            get { return actionCallPageDetail.Value; }
            set { actionCallPageDetail.Value = value; }
        }

        /// <summary>
        /// Retrieves the clientID of the actionCallPageDetail form field
        /// </summary>
        public string ActionCallPageDetailClientID
        {
            get { return actionCallPageDetail.ClientID; }
        }

        public string HotlistResourceConstant { get; set; }
        public string HotlistTitleResourceConstant { get; set; }
        public string UnHotlistResourceConstant { get; set; }
        public string UnHotlistTitleResourceConstant { get; set; }
        public string SpriteHotlistCSS { get; set; }
        public string SprintUnHotlistCSS { get; set; }
        public bool EnableSprite { get; set; }
        public bool ExcludeBlockAsFavorited { get; set; }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
	}

     
	#region Control-specific enums

	public enum Add2ListOrientation
	{
		Horizontal = 1,
		Vertical = 2,
		IconOnly = 3,
        TextOnly = 4
	}

	public enum ActionType
	{
		UNLIST = 1,
		ADD = 2,
		BLOCK = 3
	}

	#endregion

	#region Control-specific utility classes

	internal class HotListDestinationOption
	{
		private string _displayResource;
		private ActionType _actionType;
		private int _fromList;
		private int _toList;

		public HotListDestinationOption(string displayResource, ActionType actionType, int fromList, int toList)
		{
			this._displayResource = displayResource;
			this._actionType = actionType;
			this._fromList = fromList;
			this._toList = toList;
		}

		/// <summary>
		/// Resource name that should be used to display the description of the action
		/// </summary>
		public string DisplayResource
		{
			get { return(_displayResource); }
		}

		/// <summary>
		/// Command to be used for the button this will help render
		/// </summary>
		public string CommandName
		{
			get { return(_actionType.ToString()); }
		}

		/// <summary>
		/// CommandArgument to be used for the button this will help render
		/// </summary>
		public string CommandArgument
		{
			get
			{
				string retVal;

				retVal = "FromList=" + _fromList + ",ToList=" + _toList;

				return(retVal);
			}
		}

		/// <summary>
		/// ActionType
		/// </summary>
		public ActionType ActionType
		{
			get { return(_actionType); }
		}

		/// <summary>
		/// ToList
		/// </summary>
		public int ToList
		{
			get { return(_toList); }
		}

		/// <summary>
		/// FromList
		/// </summary>
		public int FromList
		{
			get { return(_fromList); }
		}
	}

	#endregion
}
