﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Block.ascx.cs" Inherits="Matchnet.Web.Framework.Ui.ProfileElements.Block" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>

<a id="btnBlock" href="#" runat="server" tabindex="10" class="menu">
    <span id="spanIcon" runat="server"><span></span></span>
    <span class="title" ID="spanLinkDesc" runat="server">
        <mn2:FrameworkLiteral ID="literalBlockLink" ResourceConstant="TXT_BLOCK" runat="server"></mn2:FrameworkLiteral>
    </span>
</a>
    
<div id="blockDiv" runat="server" style="display:none;" class="items">
    
    <%--Block feature--%>
    <asp:PlaceHolder ID="phBlockFeature" runat="server">
        <h2 class="title header" ID="spanHeaderDesc" runat="server">
            <mn2:FrameworkLiteral ID="literalBlockHeader" ResourceConstant="TXT_BLOCK_FROM" runat="server"></mn2:FrameworkLiteral>
        </h2>
        <ul>
            <li><asp:CheckBox ID="cbBlockContact" runat="server" /></li>
            <li><asp:CheckBox ID="cbRemoveFromMySearch" runat="server" /></li>
            <li><asp:CheckBox ID="cbBlockFromSearchingMe" runat="server" /></li>
        </ul>
        <div class="cta">
            <mn2:FrameworkButton ID="btnSubmitBlocks" runat="server" ResourceConstant="BTN_SUBMIT_BLOCKS" 
                UseSubmitBehavior="false" CausesValidation="false" CssClass="link-primary" />
        </div>
        <span id="blockConfirmation" style="display:none;" runat="server"></span>
    </asp:PlaceHolder>
    
    <%--Marketing copy for non-subscribers--%>
    <asp:PlaceHolder ID="phBlockNonSubscribers" runat="server" Visible="false">
        <div class="non-sub">
            <h2 class="title header"><mn:txt ID="txtNonSubTitle" ResourceConstant="TXT_BLOCK_NON_SUB" runat="server" /></h2>
            <p class="description"><mn2:FrameworkLiteral ID="literalNonSubscriberCopy" runat="server" ResourceConstant="TXT_NONSUBSCRIBER_BLOCK_COPY"></mn2:FrameworkLiteral></p>
            <div class="cta">
                <asp:HyperLink ID="lnkSubscribeNow" runat="server" CssClass="link-primary">
                    <mn2:FrameworkLiteral ID="literalSubscribeNow" runat="server" ResourceConstant="TXT_SUBSCRIBE_NOW"></mn2:FrameworkLiteral>
                </asp:HyperLink>
            </div>
        </div>
    </asp:PlaceHolder>
    
</div>

<script type="text/javascript">

    MenuToggler($j('#<%=btnBlock.ClientID %>'), $j('#<%=blockDiv.ClientID %>'));
 
</script>
