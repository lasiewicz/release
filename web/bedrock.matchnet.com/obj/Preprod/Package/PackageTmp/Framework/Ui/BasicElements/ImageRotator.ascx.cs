﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Matchnet.Lib;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Ui.BasicElements;

namespace Matchnet.Web.Framework.Ui.BasicElements
{
    public partial class ImageRotator : FrameworkControl
    {
        private string _resourceConstant = "";
        private FrameworkControl _resourceControl;
        public string ResourceConstant
        {
            get { return _resourceConstant; }
            set { _resourceConstant = value; }
        }

        public FrameworkControl ResourceControl
        {
            get
            {
                if (_resourceControl == null)
                    return this;
                else
                    return _resourceControl;
            
            }
            set { _resourceControl = value; }

        }
        protected void Page_Load(object sender, EventArgs e)
        {

            litData.Text = g.GetResource(_resourceConstant, ResourceControl);

        }
    }
}