﻿using System;
using System.Collections.Generic;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Member.ValueObjects.Interfaces;
using Matchnet.Web.Applications.MemberProfile.ProfileTabs30.Controls.ProfileDataGroup;
using Matchnet.Web.Framework.Managers;
using Matchnet.Web.Applications.MemberProfile;
using System.Collections;
using Matchnet.Content.ServiceAdapters.Links;
using Matchnet.Web.Applications.ColorCode;
using Matchnet.Web.Applications.Home;


namespace Matchnet.Web.Framework.Ui.BasicElements
{
    public partial class HeroProfileFilmStrip : FrameworkControl
    {
        #region Fields
        private int _ordinal;
        private int _memberID;
        private BreadCrumbHelper.EntryPoint _myEntryPoint;
        private MOCollection _myMOCollection;
        private int _hotListCategoryID;
        private IMemberDTO _Member;
        private bool _isSpotlighted = false;
        protected string viewProfileUrl;
        private DateTime _lastViewedDate = DateTime.MinValue;
        private DisplayContextType _DisplayContext = DisplayContextType.Default;
        //private List<Photo> _approvedPhotos;
        private int _approvedPhotosCount = -1;
        protected Color _Color = Color.none;
        protected string _ColorText = "";

        const int MAX_WORD_LENGTH = 10;
        const int MAX_STRING_LENGTH = 40;
        const int MAX_USERNAME_LENGTH = 16;
        const int MAX_ESSAY_LENTH = 300;
        private List<ProfileHolder> _memberList;
        #endregion

        #region Properties

        public DisplayContextType DisplayContext
        {
            get { return _DisplayContext; }
            set { _DisplayContext = value; }
        }

        public IMemberDTO member
        {
            get { return _Member; }
            set
            {
                _Member = value;
                _memberID = _Member.MemberID;
            }
        }

        public int MemberID
        {
            get { return (_memberID); }
        }

        public int Ordinal
        {
            get { return (_ordinal); }
            set { _ordinal = value; }
        }


        public BreadCrumbHelper.EntryPoint MyEntryPoint
        {
            get { return (_myEntryPoint); }
            set { _myEntryPoint = value; }
        }

        public MOCollection MyMOCollection
        {
            get { return (_myMOCollection); }
            set { _myMOCollection = value; }
        }

        public int HotListCategoryID
        {
            get { return (_hotListCategoryID); }
            set { _hotListCategoryID = value; }
        }

        public bool IsSpotlighted
        {
            get { return (this._isSpotlighted); }
            set { this._isSpotlighted = value; }
        }

        public DateTime LastViewedDate
        {
            get { return this._lastViewedDate; }
            set { this._lastViewedDate = value; }
        }

        public string TrackingParam { get; set; }

        public List<ProfileHolder> MemberList
        {
            get { return _memberList; }
            set { _memberList = value; }
        }
        public int ApprovedPhotosCount
        {
            get
            {
                try
                {
                    if (_approvedPhotosCount == -1)
                    {
                        _approvedPhotosCount = MemberPhotoDisplayManager.Instance.GetApprovedPhotosCount(g.Member, member, g.Brand);
                    }

                    return _approvedPhotosCount;

                }
                catch (Exception ex)
                { return 0; }

            }

        }

        public string ViewProfileLinkFormat { get; set; }
        public string EmailLinkFormat { get; set; }
        public string MoreResultsLink { get; set; }
        public string MoreResultsText { get; set; }
        public string EmailButtonText { get; set; }
        #endregion

        #region Event Handler
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            NanoProfileFilmStripYourMatches.MemberList = _memberList;
            LoadMemberProfile();
            NanoProfileFilmStripYourMatches.MyMOCollection = MyMOCollection;
            NanoProfileFilmStripYourMatches.BindMemberList();
            if (MyEntryPoint == BreadCrumbHelper.EntryPoint.HomeHeroProfileMatches)
            {
                MoreResultsText = g.GetResource("TXT_HERO_PROFILE_TITLE_MATCH", this);
                // MoreResultsLink = "/Applications/Search/SearchResults.aspx";
            }
            else if (MyEntryPoint == BreadCrumbHelper.EntryPoint.HomeHeroProfileMOL)
            {
                MoreResultsText = g.GetResource("TXT_HERO_PROFILE_TITLE_MOL", this);
                //MoreResultsLink = "/Applications/MembersOnline/MembersOnline.aspx";
            }
            else if (MyEntryPoint == BreadCrumbHelper.EntryPoint.HomeHeroProfileFilmStripYourProfile)
            {
                MoreResultsText = g.GetResource("TXT_HERO_PROFILE_YOUR_PROFILE", this);

            }
            txtTitle.Text = MoreResultsText;
            // lnkViewMore.Visible = MyEntryPoint != BreadCrumbHelper.EntryPoint.HomeHeroProfileMOL;
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
        }
        #endregion


        #region Methods
        /// <summary>
        /// Loads Member Profile based on properties already set into the control, including the Member object
        /// </summary>
        public void LoadMemberProfile()
        {
            try
            {
                SearchResultProfile searchResult = new SearchResultProfile(_Member, Ordinal);

                if (this.IsSpotlighted)
                {
                    viewProfileUrl = BreadCrumbHelper.MakeViewProfileLink(this.MyEntryPoint, this.MemberID, this.Ordinal, this.MyMOCollection, this.HotListCategoryID, false, (int)BreadCrumbHelper.PremiumEntryPoint.Spotlight);
                    viewProfileUrl = BreadCrumbHelper.AppendParamToProfileLink(viewProfileUrl, WebConstants.URL_PARAMETER_NAME_HERO_PROFILE + "=" + "Hero+Spotlight");
                }
                else
                {
                    viewProfileUrl = BreadCrumbHelper.MakeViewProfileLink(this.MyEntryPoint, this.MemberID, this.Ordinal, this.MyMOCollection, this.HotListCategoryID, false, (int)BreadCrumbHelper.PremiumEntryPoint.NoPremium);
                    viewProfileUrl = BreadCrumbHelper.AppendParamToProfileLink(viewProfileUrl, WebConstants.URL_PARAMETER_NAME_HERO_PROFILE + "=" + "Hero+Match");
                    //this.panelHeroProfileContent.CssClass = "hero-profile-content-div";
                }

                ViewProfileLinkFormat = viewProfileUrl.Replace(this.MemberID.ToString(), "{0}");
                ViewProfileLinkFormat = ViewProfileLinkFormat.Replace("Ordinal=" + this.Ordinal.ToString(), "Ordinal={1}");

                imgProfile.NavigateUrl = viewProfileUrl;
                //set basic info
                lnkUserName1.NavigateUrl = viewProfileUrl;
                lnkUserName1.Text = FrameworkGlobals.Ellipsis(_Member.GetUserName(g.Brand), 12);
                this.literalAge.Text = FrameworkGlobals.GetAge(this.member, g.Brand).ToString();
                if (this.literalAge.Text.Trim() == "")
                {
                    this.literalAge.Text = "&nbsp;";
                }
                else
                {
                    if (FrameworkGlobals.isHebrewSite(g.Brand))
                    {
                        literalAge.Text = g.GetResource("TXT_YEARS_OLD", this) + " " + literalAge.Text;
                    }
                    else
                    {
                        literalAge.Text += " " + g.GetResource("TXT_YEARS_OLD", this);
                    }
                }

                this.literalLocation.Text = ProfileDisplayHelper.GetRegionDisplay(this.member, g);
                if (this.literalLocation.Text.Trim() == "") this.literalLocation.Text = "&nbsp;";
                string fullURL = Context.Request.Url.AbsoluteUri;

                if (fullURL.Contains("academic-dating"))
                {
                    plcEducationLevel.Visible = true;
                    txtEducationLevel.Text = new BaseDataGroup().GetDataGroupAttributeDisplayValue(this.member, "EducationLevel", this, false);
                }

                litViewMorePhotos.Text = g.GetResource("TXT_VIEW_MEMBER_ARROWS", this);
                lnkViewMorePhotos.NavigateUrl = viewProfileUrl;
                DisplayMemberPhoto();

                LinkParent parent = LinkParent.HomePageHeroProfile;

                switch (_DisplayContext)
                {
                    case DisplayContextType.Default:
                        parent = LinkParent.HomePageHeroProfile;
                        break;
                }
                if (MyEntryPoint == BreadCrumbHelper.EntryPoint.HomeHeroProfileFilmStripYourProfile)
                {
                    lnkEmail.Visible = false;
                    phFilmStrip.Visible = false;
                }
                else
                {
                    if (SettingsManager.GetSettingBool(SettingConstants.ENABLE_FLIRT_BUTTON_ON_HERO_PROFILE, g.Brand))
                    {
                        if (g.Member.IsPayingMember(g.Brand.Site.SiteID))
                        {
                            lnkEmail.NavigateUrl = String.Format(FrameworkGlobals.GetEmailLink(this.MemberID, false) + "&LinkParent={0}", parent.ToString("d"));
                            EmailLinkFormat = FrameworkGlobals.GetEmailLinkTemplate(false) + "&LinkParent=" + parent.ToString("d");
                            EmailButtonText = g.GetResource("TXT_EMAIL_ME_BUTTON", this);
                        }
                        else
                        {
                            string linkBase = FrameworkGlobals.LinkHref("/Applications/Email/Tease.aspx?" + WebConstants.ACTION_CALL + "=" + WebConstants.PageIDs.Tease.ToString() + "&MemberID={0}", true);
                            lnkEmail.NavigateUrl = String.Format(linkBase + "&LinkParent={1}", _Member.MemberID, parent.ToString("d"));
                            this.mntxt9707.ResourceConstant = "TXT_FREE_FLIRT_BUTTON";
                            EmailLinkFormat = linkBase + "&LinkParent=" + parent.ToString("d");
                            EmailButtonText = g.GetResource("TXT_FREE_FLIRT_BUTTON", this);
                        }
                    }
                    else
                    {
                        lnkEmail.NavigateUrl = String.Format(FrameworkGlobals.GetEmailLink(this.MemberID, false) + "&LinkParent={0}", parent.ToString("d"));
                        EmailLinkFormat = FrameworkGlobals.GetEmailLinkTemplate(false) + "&LinkParent=" + parent.ToString("d");
                        EmailButtonText = g.GetResource("TXT_EMAIL_ME_BUTTON", this);
                    }
                }
                // SetEmailButtonABTest();

                //color code
                if (ColorCodeHelper.IsColorCodeEnabled(g.Brand) && ColorCodeHelper.HasMemberCompletedQuiz(_Member, g.Brand) && !ColorCodeHelper.IsMemberColorCodeHidden(_Member, g.Brand))
                {
                    phColorCode.Visible = true;
                    _Color = ColorCodeHelper.GetPrimaryColor(_Member, g.Brand);
                    _ColorText = ColorCodeHelper.GetFormattedColorText(_Color);

                }

            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }

        }

        //private void SetEmailButtonABTest()
        //{
        //    Scenario scenario = g.GetABScenario("MINIPROFILE_BUTTON");
        //    if (scenario == Scenario.B)
        //    {
        //        imgEmail.FileName = "btn-view-fullprofile.gif";
        //        imgEmail.ResourceConstant = "VIEW_MEMBER";
        //        imgEmail.TitleResourceConstant = "VIEW_MEMBER";
        //        lnkEmail.NavigateUrl = g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.ViewProfile, WebConstants.Action.ViewMemberButton, lnkUserName1.NavigateUrl, string.Empty);
        //    }
        //    else
        //    {
        //        lnkEmail.NavigateUrl = g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.Compose, WebConstants.Action.EmailMeNowButton, lnkEmail.NavigateUrl, string.Empty);
        //    }
        //}

        private void DisplayMemberPhoto()
        {
            ArrayList approvedPhotos = new ArrayList();
            // This must be appended to all photos that hit the file servers in order for the site-specific
            // "No Photo" images to appear.  Appending this to any other photos should not cause errors.
            string siteIDParam = "?" + WebConstants.URL_PARAMETER_NAME_SITEID + "=" + g.Brand.Site.SiteID.ToString();

            // get the main approved photo that's marked as main by the member
            var photo = MemberPhotoDisplayManager.Instance.GetDefaultPhoto(g.Member, _Member, g.Brand);

            if (photo != null)
            {
                //display large photo
                var imageURL = MemberPhotoDisplayManager.Instance.GetPhotoDisplayURL(g.Member, _Member, g.Brand, photo, PhotoType.Full, PrivatePhotoImageType.Full);
                imgProfile.ImageUrl = imageURL;

                // this count includes photos that are approved; it doesn't care for ApprovedForMain flag value
                var approvedPhotosCount = MemberPhotoDisplayManager.Instance.GetApprovedPhotosCount(g.Member, _Member, g.Brand);

                if (approvedPhotosCount - 1 > 1)
                {
                    litViewMorePhotos.Text = String.Format(g.GetResource("TXT_VIEW_MEMBER_PHOTOS_FORMAT", this), approvedPhotosCount - 1);
                }
                else if (approvedPhotosCount - 1 == 1)
                {
                    litViewMorePhotos.Text = String.Format(g.GetResource("TXT_VIEW_MEMBER_PHOTO_FORMAT", this), approvedPhotosCount - 1);
                }

            }
            else
            {
                //no approved photos, so hide photo
                imgProfile.Visible = false;
            }

        }

        public string GetAgeFormat()
        {
            string age = "{0}";

            if (FrameworkGlobals.isHebrewSite(g.Brand))
            {
                age = g.GetResource("TXT_YEARS_OLD", this) + " " + age;
            }
            else
            {
                age += " " + g.GetResource("TXT_YEARS_OLD", this);
            }

            return age;
        }




        #endregion

        protected string GetEducationLevelTemplate()
        {
            string fullURL = Context.Request.Url.AbsoluteUri;

            if (fullURL.Contains("academic-dating"))
            {
                return "\n<li><span class=\"education-level\" >{%= EducationLevel %}</span></li>\n";
            }

            return string.Empty;
        }
    }
}
