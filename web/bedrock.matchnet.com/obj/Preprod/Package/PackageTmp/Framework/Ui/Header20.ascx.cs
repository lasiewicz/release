﻿using System;
using System.Collections;
using System.Web;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

using Matchnet.Content.ServiceAdapters.Links;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Web.Applications.MemberProfile.ProfileTabs30.Controls;
using Matchnet.Web.Framework.Ui.SearchElements;
using Matchnet.Web.Framework.Ui.Advertising;


namespace Matchnet.Web.Framework.Ui
{
    public partial class Header20 : FrameworkControl, IHeader
    {

        public bool Visible
        { set { DefaultHeader.Visible = value; noNavHeader.Visible = false; } }

        public MiniSearchBar MiniSearchBar
        {
            get { return miniSearchBar1; }
        }

        public SecretAdmirerGame SecretAdmirerGame
        {
            get { return secretAdmirerGame0; }
        }

        public bool ShowEditProfileMessage
        {
            get { return phYourProfileMessage.Visible; }
            set { phYourProfileMessage.Visible = value; }
        }

        private void Page_Load(object sender, EventArgs e)
        {
            if (_g.Member == null && RuntimeSettings.GetSetting("BREADCRUMBS_ON_REDESIGN_FLAG", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID).ToLower() == "true")
            {
                /// Add the bread crumb trail header links
                BreadCrumbPH.Controls.Add(g.BreadCrumbTrailHeader);
            }

            if (g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.NRGDating)
            {
                DefaultHeader.Controls.Add(LoadControl("HeaderNavigation/Cupid.ascx"));
            }
            else
            {
                // DefaultHeader.Controls.Add(LoadControl("HeaderNavigation20/Default.ascx"));
                DefaultHeader.Controls.Add(LoadControl("HeaderNavigation20/HeaderMenu.ascx"));
            }

            if (lnkDefault2 != null)
            {
                //lnkDefault2.HRef = FrameworkGlobals.LinkHref("/Default.aspx", true);
                lnkDefault2.HRef = FrameworkGlobals.GetHomepageAbsURL(g, Request.ServerVariables.Get("HTTP_HOST"));
            }


        }

        public void ShowBlankHeader()
        {
            DefaultHeader.Visible = false;
            //TopAuxMenu.Visible = false;
            noNavHeader.Visible = true;
            SwitchHeaderImage(HeaderImageType.Default);
        }

        public void ShowBlankHeader(HeaderImageType headerType)
        {
            DefaultHeader.Visible = false;
            // TopAuxMenu.Visible = false;
            noNavHeader.Visible = true;
            //NoNavTopHeader.Visible = true;
            // SwitchHeaderImage(headerType);
        }

        public void SetTopNavTitle(string title)
        {
            txtNoNavTitle.Text = title;
        }

        public void SwitchHeaderImage(HeaderImageType headerType)
        {
            BrandAlias brandAlias = g.BrandAlias;
            tblHeaderAlternate.Attributes["background"] = Framework.Image.GetBrandAliasImagePath("logo.gif", brandAlias);

            imgDefault.Visible = false;
            if (headerType == HeaderImageType.Login)
            {
                tblHeaderAlternate.Attributes["background"] = Framework.Image.GetBrandAliasImagePath("login_background.jpg", brandAlias);

                imgLogin.Visible = true;
            }
            else if (headerType == HeaderImageType.Registration)
            {
                tblHeaderAlternate.Attributes["background"] = Framework.Image.GetBrandAliasImagePath("reg_background.jpg", brandAlias);
                imgRegistration.Visible = true;
            }
            else
            {
                imgDefault.Visible = true;
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }
        #endregion


        #region IHeader Members

        #endregion
    }
}
