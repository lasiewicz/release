﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SecretAdmirer.ascx.cs" Inherits="Matchnet.Web.Framework.Ui.ProfileElements.SecretAdmirer" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>

<asp:HyperLink ID="lnkSecretAdmirer" runat="server" NavigateUrl="#" TabIndex="10" CssClass="menu">
    <%--Icon link--%>
    <span id="spanSecretAdmirer" runat="server"><span><mn:Txt ID="txt1" runat="server" ResourceConstant="YNM_Y" /></span></span>
    <%--Text link--%>
    <mn:Txt ID="txtSeccretAdmirer" ResourceConstant="PRO_SECRETADMIRER" TitleResourceConstant="ALT_SECRETADMIRER" runat="server" Visible="false" />
</asp:HyperLink>

<%--YNM buttons in-line next to link--%>
<asp:PlaceHolder ID="phYNMInline" runat="server" Visible="false">
    <div class="click20-horizontal">
        <div class="click20-option">
            <asp:HyperLink ID="lnkYesInline" runat="server">
                <span id="spanYesInline" runat="server" class="spr s-icon-click-y-off"><span>
                    <mn:Txt ID="txt2" runat="server" ResourceConstant="YNM_Y" />
                </span></span>
            </asp:HyperLink>
        </div>
        <div class="click20-option">
            <asp:HyperLink ID="lnkNoInline" runat="server">
                <span id="spanNoInline" runat="server" class="spr s-icon-click-n-off"><span>
                    <mn:Txt ID="txt3" runat="server" ResourceConstant="YNM_N" />
                </span></span>
            </asp:HyperLink>
        </div>
        <div class="click20-option">
            <asp:HyperLink ID="lnkMaybeInline" runat="server">
                <span id="spanMaybeInline" runat="server" class="spr s-icon-click-m-off"><span>
                    <mn:Txt ID="txt4" runat="server" ResourceConstant="YNM_M" />
                </span></span>
            </asp:HyperLink>
        </div>
    </div>
</asp:PlaceHolder>

<%--popup--%>
<div id="secretAdmirerPopup" runat="server" style="display:none" class="secret-admirer items">
    <div class="title header"><h2><mn2:FrameworkLiteral ID="literalSecretAdmirerTitle" ResourceConstant="TXT_SECRETADMIRER_TITLE" runat="server"></mn2:FrameworkLiteral></h2>
        <%--YNM buttons in popup--%>
        <asp:PlaceHolder ID="phYNMInPopup" runat="server" Visible="false">
            <asp:HyperLink ID="lnkYesInPopup" runat="server">
                <span id="spanYesInPopup" runat="server" class="spr s-icon-click-y-off"><span><mn:Txt ID="txt5" runat="server" ResourceConstant="YNM_Y" /></span></span>
            </asp:HyperLink>
            <asp:HyperLink ID="lnkNoInPopup" runat="server">
                <span id="spanNoInPopup" runat="server" class="spr s-icon-click-n-off"><span><mn:Txt ID="txt6" runat="server" ResourceConstant="YNM_N" /></span></span>
            </asp:HyperLink>
            <asp:HyperLink ID="lnkMaybeInPopup" runat="server">
                <span id="spanMaybeInPopup" runat="server" class="spr s-icon-click-m-off"><span><mn:Txt ID="txt7" runat="server" ResourceConstant="YNM_M" /></span></span>
            </asp:HyperLink>
        </asp:PlaceHolder>
    </div>
    <%--input to hold current vote status, used by js--%>
    <input id="YNMVoteStatus" type="hidden" name="YNMVoteStatus" runat="server"/>
    
    <p id="secretAdmirerPopup_desc" style="display:none" runat="server" class="description"><mn2:FrameworkLiteral ID="literalSecretAdmirerDesc" ResourceConstant="TXT_SECRETADMIRER_DESC" runat="server"></mn2:FrameworkLiteral></p>
    <p id="secretAdmirerPopup_descY" style="display:none" runat="server" class="description"><mn2:FrameworkLiteral ID="literalSecretAdmirerDescY" ResourceConstant="TXT_SECRETADMIRER_DESC_YES" runat="server"></mn2:FrameworkLiteral></p>
    <p id="secretAdmirerPopup_descN" style="display:none" runat="server" class="description"><mn2:FrameworkLiteral ID="literalSecretAdmirerDescN" ResourceConstant="TXT_SECRETADMIRER_DESC_NO" runat="server"></mn2:FrameworkLiteral></p>
    <p id="secretAdmirerPopup_descM" style="display:none" runat="server" class="description"><mn2:FrameworkLiteral ID="literalSecretAdmirerDescM" ResourceConstant="TXT_SECRETADMIRER_DESC_MAYBE" runat="server"></mn2:FrameworkLiteral></p>
    <p id="secretAdmirerPopup_descYY" style="display:none" runat="server" class="description"><mn2:FrameworkLiteral ID="literalSecretAdmirerDescYY" ResourceConstant="TXT_SECRETADMIRER_DESC_MUTUALYES" runat="server"></mn2:FrameworkLiteral></p>
</div>

<script type="text/javascript">

    $j(function () {
        // control SecretAdmirer
        var lnkSecretAdmirer = $j('#<%=lnkSecretAdmirer.ClientID %>'),
            secretAdmirerPopup = $j('#<%=secretAdmirerPopup.ClientID %>');

        MenuToggler(lnkSecretAdmirer, secretAdmirerPopup);

        if ($j('#results30 > div').hasClass('list-view-30')) {
            $j(secretAdmirerPopup).append('<span class="spr s-icon-close"><span></span></span>');

            $j(secretAdmirerPopup).find('.s-icon-close').click(function () {
                $j(secretAdmirerPopup).hide().parent().removeClass('active');
            });
        }
    });


    $j('#<%=HideYNMInPopup ? lnkYesInline.ClientID : lnkYesInPopup.ClientID %>').click(function () {
        var params = new SecretAdmirer_YNMParams();
        params.parentClientID = '<%=HideYNMInPopup ? lnkYesInline.NamingContainer.ClientID : lnkYesInPopup.NamingContainer.ClientID %>';
        params.type = '1';
        params.encryptedParams = '<%=_EncodedParams %>';
        params.memberID = '<%=Member.MemberID%>';
        params.externalYNMIndicatorID = '<%=ExternalYNMIndicatorID %>';
        params.externalYYIndicatorID = '<%=ExternalYYIndicatorID %>';
        params.secretAdmirerPopupID = '<%=secretAdmirerPopup.ClientID%>';
        params.secretAdmirerPopupID_desc = '<%=secretAdmirerPopup_desc.ClientID%>';
        params.secretAdmirerPopupID_descY = '<%=secretAdmirerPopup_descY.ClientID%>';
        params.secretAdmirerPopupID_descN = '<%=secretAdmirerPopup_descN.ClientID%>';
        params.secretAdmirerPopupID_descM = '<%=secretAdmirerPopup_descM.ClientID%>';
        params.secretAdmirerPopupID_descYY = '<%=secretAdmirerPopup_descYY.ClientID%>';
        params.spanYesID = '<%=HideYNMInPopup ? spanYesInline.ClientID : spanYesInPopup.ClientID%>';
        params.spanNoID = '<%=HideYNMInPopup ? spanNoInline.ClientID : spanNoInPopup.ClientID%>';
        params.spanMaybeID = '<%=HideYNMInPopup ? spanMaybeInline.ClientID : spanMaybeInPopup.ClientID%>';
        params.YNMVoteStatusID = '<%=YNMVoteStatus.ClientID%>';
        params.OmnitureName = 'Mini-Profile';
        params.YNMIconCSS = '<%=IconHasYNMCSS%>';
        params.spanSecretAdmirerIconID = '<%=spanSecretAdmirer.ClientID%>';

        SecretAdmirer_ProcessYNM(params);
        return false;
    });

    $j('#<%=HideYNMInPopup ? lnkNoInline.ClientID : lnkNoInPopup.ClientID %>').click(function () {
        var params = new SecretAdmirer_YNMParams();
        params.parentClientID = '<%=HideYNMInPopup ? lnkNoInline.NamingContainer.ClientID : lnkNoInPopup.NamingContainer.ClientID %>';
        params.type = '2';
        params.encryptedParams = '<%=_EncodedParams %>';
        params.memberID = '<%=Member.MemberID%>';
        params.externalYNMIndicatorID = '<%=ExternalYNMIndicatorID %>';
        params.externalYYIndicatorID = '<%=ExternalYYIndicatorID %>';
        params.secretAdmirerPopupID = '<%=secretAdmirerPopup.ClientID%>';
        params.secretAdmirerPopupID_desc = '<%=secretAdmirerPopup_desc.ClientID%>';
        params.secretAdmirerPopupID_descY = '<%=secretAdmirerPopup_descY.ClientID%>';
        params.secretAdmirerPopupID_descN = '<%=secretAdmirerPopup_descN.ClientID%>';
        params.secretAdmirerPopupID_descM = '<%=secretAdmirerPopup_descM.ClientID%>';
        params.secretAdmirerPopupID_descYY = '<%=secretAdmirerPopup_descYY.ClientID%>';
        params.spanYesID = '<%=HideYNMInPopup ? spanYesInline.ClientID : spanYesInPopup.ClientID%>';
        params.spanNoID = '<%=HideYNMInPopup ? spanNoInline.ClientID : spanNoInPopup.ClientID%>';
        params.spanMaybeID = '<%=HideYNMInPopup ? spanMaybeInline.ClientID : spanMaybeInPopup.ClientID%>';
        params.YNMVoteStatusID = '<%=YNMVoteStatus.ClientID%>';
        params.OmnitureName = 'Mini-Profile';
        params.YNMIconCSS = '<%=IconHasYNMCSS%>';
        params.spanSecretAdmirerIconID = '<%=spanSecretAdmirer.ClientID%>';

        SecretAdmirer_ProcessYNM(params);
        return false;
    });

    $j('#<%=HideYNMInPopup ? lnkMaybeInline.ClientID : lnkMaybeInPopup.ClientID %>').click(function () {
        var params = new SecretAdmirer_YNMParams();
        params.parentClientID = '<%=HideYNMInPopup ? lnkMaybeInline.NamingContainer.ClientID : lnkMaybeInPopup.NamingContainer.ClientID %>';
        params.type = '3';
        params.encryptedParams = '<%=_EncodedParams %>';
        params.memberID = '<%=Member.MemberID%>';
        params.externalYNMIndicatorID = '<%=ExternalYNMIndicatorID %>';
        params.externalYYIndicatorID = '<%=ExternalYYIndicatorID %>';
        params.secretAdmirerPopupID = '<%=secretAdmirerPopup.ClientID%>';
        params.secretAdmirerPopupID_desc = '<%=secretAdmirerPopup_desc.ClientID%>';
        params.secretAdmirerPopupID_descY = '<%=secretAdmirerPopup_descY.ClientID%>';
        params.secretAdmirerPopupID_descN = '<%=secretAdmirerPopup_descN.ClientID%>';
        params.secretAdmirerPopupID_descM = '<%=secretAdmirerPopup_descM.ClientID%>';
        params.secretAdmirerPopupID_descYY = '<%=secretAdmirerPopup_descYY.ClientID%>';
        params.spanYesID = '<%=HideYNMInPopup ? spanYesInline.ClientID : spanYesInPopup.ClientID%>';
        params.spanNoID = '<%=HideYNMInPopup ? spanNoInline.ClientID : spanNoInPopup.ClientID%>';
        params.spanMaybeID = '<%=HideYNMInPopup ? spanMaybeInline.ClientID : spanMaybeInPopup.ClientID%>';
        params.YNMVoteStatusID = '<%=YNMVoteStatus.ClientID%>';
        params.OmnitureName = 'Mini-Profile';
        params.YNMIconCSS = '<%=IconHasYNMCSS%>';
        params.spanSecretAdmirerIconID = '<%=spanSecretAdmirer.ClientID%>';

        SecretAdmirer_ProcessYNM(params);
        return false;
    });
</script>