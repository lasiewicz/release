<%@ Control Language="c#" AutoEventWireup="false" CodeBehind="AdUnit.ascx.cs" Inherits="Matchnet.Web.Framework.Ui.Advertising.AdUnit"
    TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<asp:Panel ID="pnlAd" runat="server" CssClass="adunit">
    <%--either render litAd for single ad, or GamIframe (mainly necessary if GAM needs to be refreshed on client side)--%>
    <asp:Literal ID="litAd" runat="server"></asp:Literal>
    <asp:PlaceHolder ID="phGAMIframe" runat="server" Visible="false">
        <iframe id="iframeGAM" runat="server" frameborder="0" scrolling="no" marginheight="0"
            marginwidth="0"></iframe>
    </asp:PlaceHolder>
    
    <%--Turn off ad message that displays below ad--%>
    <asp:PlaceHolder ID="phAdMessage" runat="server" Visible="false">
    <div class="marketing">
        <asp:Literal ID="litAdMessage" runat="server"></asp:Literal>
    </div>
    </asp:PlaceHolder>
</asp:Panel>
