using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using Matchnet.Web.Framework;

using Matchnet.Content.ValueObjects.BrandConfig;

namespace Matchnet.Web.Framework.Ui.BasicElements
{
	/// <summary>
	///		Summary description for LastUpdateDate.
	/// </summary>
	public class LastUpdateDate : FrameworkControl
	{
		protected Label LastUpdated;
		protected Label lblLastUpdateDate;
		private string _date;
		private bool _showLabel = true;

		private void Page_Load(object sender, System.EventArgs e)
		{
			LastUpdated.Text = _date;
		

			if( _showLabel )
			{
				lblLastUpdateDate.Text = g.GetResource("LAST_UPDATE_DATE", this);
			}
			else
			{
				lblLastUpdateDate.Visible = false;
			}
		}
				
		public string Date
		{
			set
			{
				_date = value;
			}
		}

		public bool ShowLabel
		{
			set
			{
				_showLabel = value;
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
