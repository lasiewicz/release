﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Util;
namespace Matchnet.Web.Framework.Ui.BasicElements
{
    public partial class ColorCodeSelect : FrameworkControl
    {
        public const int ColorCodeAll=15;
        public enum Colors:int
        {
            none=0,
            white=1,
            blue=2,
            yellow=4,
            red=8

        }
        public int ColorCodeMask { get; set; }

        public string NavigateURL { get; set; }

        public bool ShowLink { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                StringBuilder strShowPopup = new StringBuilder();

               // strShowPopup.Append("javascript: document.getElementById('" + pnlColorCodeExpand.ClientID + "').style.display = 'none';");
               // strShowPopup.Append("TogglePopupDivDisplay('" + pnlColorCodeExpand.ClientID + "');");

                lnkCloseText.Text = g.GetResource("TXT_CLOSE", this);
                //lnkCloseImage.Text = g.GetResource("IMAGE_CLOSE", null, true, this);

                lnkCloseText.Attributes.Add("href", strShowPopup.ToString());
                //lnkCloseImage.Attributes.Add("href", strShowPopup.ToString());

                pnlColorCodeExpand.Style.Add("display", "none");
                pnlColorCodeExpand.Style.Remove("visibility");

                lnkColorCodeCollapse.Attributes.Add("href", "#");
                lnkColorCodeCollapse.Attributes.Add("onClick", "return false");
                lnkColorCodeCollapse.Attributes.Add("rel", "click");

                ColorCodeMask = GetColorCode();
                if (ColorCodeMask <= 0)
                {
                   ColorCodeMask = Conversion.CInt(g.Session.Get("COLORCODE_SEARCH_MASK"), ColorCodeAll);
                 }
                if (ShowLink)
                {
                    pnlColorCodeContainer.Style.Add("display", "block");
                    lnkColorCodeCollapse.Text = GetColorCodeString();
                    lnkColorCodeCollapse.Attributes.Add("class", "cc-selection");
                }
                DisplayColorCode();
               
            }
            catch (Exception ex)
            { g.ProcessException(ex); }
        }


        protected void showResults(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(NavigateURL))
            { 
                int colorcode = GetColorCode();
                if (colorcode > 0)
                {
                    g.SearchPreferences.Add("ColorCode", colorcode.ToString());
                    g.Session.Add("COLORCODE_SEARCH_MASK", colorcode.ToString(), Matchnet.Session.ValueObjects.SessionPropertyLifetime.Temporary);
                }
                else
                {
                    g.SearchPreferences.Add("ColorCode", "");
                    g.Session.Remove("COLORCODE_SEARCH_MASK");
                }
            
                g.Transfer(NavigateURL);
            }

        }

        public void DisplayColorCode()
        {
            if((ColorCodeMask & (int)Colors.yellow)==(int)Colors.yellow)
            {chkYellow.Checked=true;}

            if((ColorCodeMask & (int)Colors.white)==(int)Colors.white)
            {chkWhite.Checked=true;}
             if((ColorCodeMask & (int)Colors.blue)==(int)Colors.blue)
            {chkBlue.Checked=true;}

             if ((ColorCodeMask & (int)Colors.red) == (int)Colors.red)
             { chkRed.Checked = true; }
        }


        public int GetColorCode()
        {
            int mask=0;
            if (chkYellow.Checked)
            { mask = mask | (int)Colors.yellow; }

            if (chkWhite.Checked)
            { mask = mask | (int)Colors.white; }

            if (chkBlue.Checked)
            { mask = mask | (int)Colors.blue; }

            if (chkRed.Checked)
            { mask = mask | (int)Colors.red; }

            return mask;
        }


        public string GetColorCodeString()
        {
            StringBuilder str = new StringBuilder();
            bool addComma = false;
            if ((ColorCodeMask & (int)Colors.yellow) == (int)Colors.yellow)
            { str.Append(g.GetResource("TXT_YELLOW",this));
            addComma = true;
            }

            if ((ColorCodeMask & (int)Colors.white) == (int)Colors.white)
            {
                if (addComma)
                    str.Append(",");
                str.Append(g.GetResource("TXT_WHITE", this));
                addComma = true;
            }
            if ((ColorCodeMask & (int)Colors.blue) == (int)Colors.blue)
            {
                if (addComma)
                    str.Append(",");
                str.Append(g.GetResource("TXT_BLUE", this));
                addComma = true;
            }

            if ((ColorCodeMask & (int)Colors.red) == (int)Colors.red)
            {
                if (addComma)
                    str.Append(",");

                str.Append(g.GetResource("TXT_RED", this));
                addComma = true;
            }
            str.Append(g.GetResource("TXT_EDIT", this));
            return str.ToString();
        }
    }
}
