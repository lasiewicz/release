﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Lib;
using Matchnet.Configuration.ServiceAdapters.Analitics;
using Matchnet.Search.Interfaces;
using Matchnet.Web.Applications.Home;
using Matchnet.Search.ValueObjects;
using Matchnet.Search.ServiceAdapters;
using System.Collections;
using Matchnet.Member.ServiceAdapters;
using Matchnet.List.ValueObjects;
using Matchnet.PhotoSearch.ServiceAdapters;
using Matchnet.MembersOnline.ValueObjects;
using Matchnet.MembersOnline.ServiceAdapters;
using System.Collections.Specialized;
using System.Text;
using Matchnet.List.ServiceAdapters;
using Matchnet.Web.Analytics;
using Matchnet.Web.Facelink.Search;
using Matchnet.Web.Framework.Managers;
using Matchnet.Web.Framework.Search;
using Matchnet.Web.Framework.Test;
using Matchnet.Web.Framework.Ui.SearchElements;

namespace Matchnet.Web.Framework.Ui.BasicElements
{
	/// <summary>
	/// This control displays a mini search results with page navigation via ajax.
	/// </summary>
	public partial class MiniSearch : MiniSearchBase
	{
        protected string H3TagOpen
        {
            get
            {
                string tag = "<h3>";
                if (g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.Cupid)
                {
                    tag = string.Empty;
                }
                return tag;
            }
        }

        protected string H3TagClose
        {
            get
            {
                string tag = "</h3>";
                if (g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.Cupid)
                {
                    tag = string.Empty;
                }
                return tag;
            }
        }

		public MiniSearch()
		{
			DisplayMiniSearchMarketingDiv = true;
			DisplayMiniSearchAjaxLoadingDiv = true;
			DisplayMiniSearchContainingDiv = true;
		}

		#region Event Handlers

		protected override void OnInit(EventArgs e)
		{
			base.OnInit(e);

			repeaterProfiles.ItemDataBound += repeaterProfiles_ItemDataBound;

			omnitureEnabled = Convert.ToBoolean(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting(
				"ANALYTICS_OMNITURE", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID));
		}

		protected override void OnPreRender(EventArgs e)
		{
			if (displayMiniSearchProfile == false) return;

			try
			{
				ToggleElementVisibility();

				HandleMarketingCopy();

				HandleOmnitureAnalytics();
			}
			catch (Exception ex)
			{
				g.ProcessException(ex);
			}

			base.OnPreRender(e);
		}

		private void ToggleElementVisibility()
		{
			phMiniSearch.Visible = displayMiniSearchProfile;
			phAjaxLoading.Visible = DisplayMiniSearchAjaxLoadingDiv;
			phDivMiniSearchClose.Visible = DisplayMiniSearchContainingDiv;
			phDivMiniSearchOpen.Visible = DisplayMiniSearchContainingDiv;
		}

		private void HandleMarketingCopy()
		{
			phMarketingCopy.Visible = true;
		    bool enablecopy = SettingsManager.GetSettingBool(SettingConstants.ENABLE_MINISEARCH_MARKETTING_COPY, g.Brand);
            if (!enablecopy)
            {
                phMarketingCopy.Visible = false;
                return;
            }
			if (g.Member == null) return;

			//determine whether to display marketing copy; update if necessary
			var memberMarketingCopy = g.Member.GetAttributeInt(
				Constants.NULL_INT, g.Brand.Site.SiteID,
				Constants.NULL_INT,
				MarketingFlagAttribute,
				0
			);

			if (memberMarketingCopy == 0 && DisplayMiniSearchMarketingDiv)
			{
				phMarketingCopy.Visible = true;
				g.Member.SetAttributeInt(Constants.NULL_INT, g.Brand.Site.SiteID, Constants.NULL_INT, MarketingFlagAttribute, 1);
				MemberSA.Instance.SaveMember(g.Member);
			}
			else
			{
				phMarketingCopy.Visible = false;
			}
		}

		private void HandleOmnitureAnalytics()
		{
			if (!omnitureEnabled) return;

			if (_g.AnalyticsOmniture != null)
			{
				_g.AnalyticsOmniture.AddEvent("event24");
			}

			AddClickToPageForOmniture();
		}

		private void AddClickToPageForOmniture()
		{
			if (linkBack.NavigateUrl.IndexOf("?") > 0)
			{
				linkBack.NavigateUrl += Omniture.GetOmnitureClickToPageURLParams(
					"Mini Search", WebConstants.PageIDs.MiniSearch, "Back to Search",
					WebConstants.Action.None, false);
			}
			else
			{
				linkBack.NavigateUrl += "?" + Omniture.GetOmnitureClickToPageURLParams(
															"Mini Search", WebConstants.PageIDs.MiniSearch, "Back to Search",
															WebConstants.Action.None, false).Substring(1);
			}
		}

		protected void repeaterProfiles_ItemDataBound(object sender, RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.Item && e.Item.ItemType != ListItemType.AlternatingItem) return;

			ProfileHolder profile = e.Item.DataItem as ProfileHolder;

			if (profile == null) return;

			SearchResultProfile searchResult = new SearchResultProfile(profile.Member, profile.Ordinal);

			String viewProfileUrl = BreadCrumbHelper.MakeViewProfileLink(
				profile.MyEntryPoint,
				profile.Member.MemberID,
				profile.Ordinal,
				memberOrderCollection,
				hotListCategoryId,
				false,
				(int)BreadCrumbHelper.PremiumEntryPoint.NoPremium
				);

			if (profile.MyEntryPoint == BreadCrumbHelper.EntryPoint.ReverseSearchResults)
			{
				Int32 page;
				if (Int32.TryParse(Request["pg"], out page) == false || page < 1) page = 1;

                viewProfileUrl = BreadCrumbHelper.AppendParamToProfileLink(viewProfileUrl, "&pg=" + page);

				if (String.IsNullOrEmpty(Request["sl"]) == false)
				{
                    viewProfileUrl = BreadCrumbHelper.AppendParamToProfileLink(viewProfileUrl, "&sl=" + Request["sl"]);
				}

				if (String.IsNullOrEmpty(Request["op"]) == false)
				{
                    viewProfileUrl = BreadCrumbHelper.AppendParamToProfileLink(viewProfileUrl, "&op=" + Request["op"]);
				}
			}


			if (omnitureEnabled)
			{
                viewProfileUrl = BreadCrumbHelper.AppendParamToProfileLink(viewProfileUrl, "ClickFrom=" + HttpContext.Current.Server.UrlEncode("Profile Views - Mini Search"));
                viewProfileUrl = BreadCrumbHelper.AppendParamToProfileLink(viewProfileUrl, Omniture.GetOmnitureClickToPageURLParams(
                    "Mini Search", WebConstants.PageIDs.MiniSearch,
                    "Mini Profile", WebConstants.Action.None, false));
			}

			//set basic info
			Panel panelImgThumb = e.Item.FindControl("panelImgThumb") as Panel;
			Image imgThumb = e.Item.FindControl("imgThumb") as Image;
			PageElements.NoPhoto20 noPhoto = e.Item.FindControl("noPhoto") as PageElements.NoPhoto20;
            //MemberPhotoHelper photoHelper = new MemberPhotoHelper(RuntimeSettings.Instance);
            noPhoto.NoPhotoFileName = MemberPhotoDisplayManager.Instance.GetNoPhotoFile(NoPhotoImageType.TinyThumbV2, true);
			searchResult.SetThumb(imgThumb, noPhoto, viewProfileUrl);
            noPhoto.MemberID = profile.Member.MemberID;
			noPhoto.Mode = PageElements.NoPhoto20.PhotoMode.MiniSearch;
			noPhoto.Username = FrameworkGlobals.Ellipsis(profile.Member.GetUserName(_g.Brand), 8);
			panelImgThumb.Visible = !searchResult.NoPhotoVisible;

			if (profile.Member.MemberID == selectedMemberId)
			{
				panelImgThumb.CssClass = "photos-border-active";
				noPhoto.DivCSSClass = "photos-border-active";
			}
			else
			{
				panelImgThumb.CssClass = "photos-border";
				noPhoto.DivCSSClass = "photos-border";
			}
		}		

		#endregion

		#region Public Methods

		public override void LoadMiniSearch()
		{
			try
			{
				//get member id
				int layoutImpersonateMemberId;
				if (g.Member != null)
				{
					layoutImpersonateMemberId = _g.Member.MemberID;
				}
				else
				{
					layoutImpersonateMemberId = 9; //visitors
					isVisitor = true;
				}

				//determine if mini-search is enabled for member/visitor
				AnaliticsScenarioBase scenario = new AnaliticsScenarioBase(
					"JDATEREDESIGN_PROFILE",
					_g.Brand.Site.Community.CommunityID,
					_g.Brand.Site.SiteID,
					layoutImpersonateMemberId);

				Scenario scen = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetAnaliticsScenario(scenario);

				if (scen == Scenario.B)
				{
					displayMiniSearchProfile = true;
				}

				if (displayMiniSearchProfile)
				{
					//get back to results URL
					NameValueCollection btrQueryString = new NameValueCollection();
					backToResultUrl = FrameworkGlobals.GetCookie(WebConstants.SESSION_PROPERTY_NAME_BACKTORESULT_URL, null, true);
					string btrPath = String.Empty;
					if (!String.IsNullOrEmpty(backToResultUrl))
					{
						btrPath = backToResultUrl;
						int queryIndex = backToResultUrl.IndexOf("?");
						if (queryIndex >= 0)
						{
							btrQueryString = HttpUtility.ParseQueryString(backToResultUrl.Substring(queryIndex + 1));
							btrPath = backToResultUrl.Substring(0, queryIndex);
						}
					}

					Int32.TryParse(Request["Ordinal"], out ordinal);
					Int32.TryParse(Request["HotListCategoryID"], out hotListCategoryId);

					memberOrderCollection = BreadCrumbHelper.AssembleMOCollection(Request);

					SetEntryPoints();

					SetSelectedMemberId();

					SetStartRow();

					PopulateMiniSearch();

					//update back to results URL
					string btrNewURL = backToResultUrl;
					StringBuilder param = new StringBuilder();

					if (premiumEntryPoint == BreadCrumbHelper.PremiumEntryPoint.Spotlight || showNew)
					{
						//spotlighted profile coming from anywhere should go back to search page - newest
						btrNewURL = "~/Applications/Search/SearchResults.aspx?SearchOrderBy=1&StartRow=" + startRow;
						linkBack.ResourceConstant = "TXT_BACK_LINK_VIEWALL";
					}
					else if (!String.IsNullOrEmpty(btrNewURL))
					{
						//update values
						btrQueryString["StartRow"] = startRow.ToString();

						//build query string
						foreach (var s in btrQueryString.AllKeys)
						{
							param.Append("&" + s + "=" + btrQueryString[s]);
						}

						if (!String.IsNullOrEmpty(btrPath))
						{
							btrNewURL = btrPath + "?" + param.ToString().Substring(1);
						}
						else
						{
							btrNewURL = btrNewURL + "?" + param.ToString().Substring(1);
						}
					}
					else
					{
						var mmvid = String.Empty;
						if (Request["mmvid"] != null) mmvid = "?MMVID=" + Request["mmvid"];

						btrNewURL = "~/Applications/Search/SearchResults.aspx" + mmvid;
					}

					//save new url to cookie
					FrameworkGlobals.SaveBackToResultsURL(g, btrNewURL);

					//set back to results link
					linkBack.NavigateUrl = GetUrl(btrNewURL);

					//update next/previous page links
					bool displayPrev = false;
					if (startRow > PageSize)
					{
						phPreviousPage.Visible = true;
						displayPrev = true;

						if (isVisitor)
						{
							string vURL = g.GetLogonRedirect(VisitorLimitHelper.GetVisitorPagePath(String.Empty), "TXT_LOGIN_OR_REGISTER_TO_CONTINUE_SEARCHING");
							//this.linkPreviousPage.NavigateUrl = vURL;
							tdPrevious.Attributes.Add("onclick", "javascript:window.document.location = '" + vURL + "';");
						}
						else
						{
							//build query string
							param = new StringBuilder();
							foreach (var s in Request.QueryString.AllKeys)
							{
								var parameter = s.ToLower();
								if (parameter == "startrow") continue;

								if (parameter == "pg")
								{
									Int32 pageNumber;

									if (Int32.TryParse(Request.QueryString[parameter], out pageNumber) == false || pageNumber < 1) pageNumber = 1;

									param.Append("&pg=" + (pageNumber - 1));
								}
								else
								{
									param.Append("&" + s + "=" + Request.QueryString[s]);
								}
							}
							param.Append("&startrow=" + (startRow - PageSize));

							tdPrevious.Attributes.Add("onclick", "javascript:GetMiniSearch('" + param.ToString().Substring(1) + "', 'prev'," + isVisitor.ToString().ToLower() + ");return false;");
						}
					}

					bool displayNext = false;
					if (totalCount >= (startRow + PageSize))
					{
						phNextPage.Visible = true;
						displayNext = true;

						if (isVisitor)
						{
							string vURL = g.GetLogonRedirect(VisitorLimitHelper.GetVisitorPagePath(""), "TXT_LOGIN_OR_REGISTER_TO_CONTINUE_SEARCHING");
							//this.linkNextPage.NavigateUrl = vURL;
							tdNext.Attributes.Add("onclick", "javascript:window.document.location = '" + vURL + "';");
						}
						else
						{
							//build query string
							param = new StringBuilder();
							foreach (var s in Request.QueryString.AllKeys)
							{
								var parameter = s.ToLower();
								if (parameter == "startrow") continue;

								if (parameter == "pg")
								{
									Int32 pageNumber;

									if (Int32.TryParse(Request.QueryString[parameter], out pageNumber) == false || pageNumber < 1) pageNumber = 1;

									param.Append("&pg=" + (pageNumber + 1));
								}
								else
								{
									param.Append("&" + s + "=" + Request.QueryString[s]);
								}
							}
							param.Append("&startrow=" + (startRow + PageSize));

							tdNext.Attributes.Add("onclick", "javascript:GetMiniSearch('" + param.ToString().Substring(1) + "', 'next', " + isVisitor.ToString().ToLower() + ");return false;");
						}
					}

					if (displayPrev && displayNext)
					{
						literalNextPage.Visible = false;
						literalPreviousPage.Visible = false;
					}

				}
			}
			catch (Exception ex)
			{
                this.Visible = false;
				g.ProcessException(ex);
			}
		}

		#endregion

		#region Private Methods

        private void PopulateMiniSearch()
        {
            BindMemberProfiles(GetProfileList());

            UpdateTitle();
        }

		private void BindMemberProfiles(ICollection<ProfileHolder> profileList)
		{
			if (profileList != null && profileList.Count > 0)
			{
				repeaterProfiles.DataSource = profileList;
				repeaterProfiles.DataBind();
			}
			else
			{
                this.Visible = false;
				displayMiniSearchProfile = false;
			}
		}

		private void UpdateTitle()
		{
			if (String.IsNullOrEmpty(titleResource))
			{
				literalTitle.Text = FrameworkGlobals.Ellipsis(titleName, 19, "&hellip;");
				literalLoadingTitle.Text = titleName;
				literalTitle.ResourceConstant = "";
				literalLoadingTitle.ResourceConstant = "";
			}
			else
			{
				literalTitle.Text = FrameworkGlobals.Ellipsis(g.GetResource(titleResource, this), 19, "&hellip;");
				literalTitle.ResourceConstant = "";
				literalLoadingTitle.ResourceConstant = titleResource;
			}
		}

		#endregion
	}
}
