﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Lib;
using Matchnet.List.ValueObjects;
using Matchnet.Web.Applications.MemberProfile;

namespace Matchnet.Web.Framework.Ui.ProfileElements
{
    public partial class M2MCommunication : FrameworkControl
    {
        private Member.ServiceAdapters.Member _Member;
        private string _PageName = "";

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected override void OnPreRender(EventArgs e)
        {
            
            if (this.phYNM.Visible)
            {
                Page.ClientScript.RegisterClientScriptBlock(typeof(System.Web.UI.Page), "M2MCommunication", "<script type=\"text/javascript\">" +
               "tabProfileObject.YYImageID = '" + this.divMutualYes.ClientID + "';" +
               "</script>");

                //omniture
            }

            base.OnPreRender(e);
        }

        public void LoadM2MCommunication(Member.ServiceAdapters.Member member, string pageName)
        {
            _Member = member;
            _PageName = pageName;

            //M2M Title
            int genderMask = member.GetAttributeInt(g.Brand, "gendermask");
            if ((genderMask & ConstantsTemp.GENDERID_MALE) == ConstantsTemp.GENDERID_MALE)
                literalM2MTitle.Text = g.GetResource("M2MTITLE_M", this);
            else if ((genderMask & ConstantsTemp.GENDERID_FEMALE) == ConstantsTemp.GENDERID_FEMALE)
                literalM2MTitle.Text = g.GetResource("M2MTITLE_F", this);

            //set send e-card link
            if (g.EcardsEnabled)
            {
                // Generate mingle ecard page url. (to parameter is the username of the reciever)
                string destPageUrl = FrameworkGlobals.BuildConnectFrameworkLink("cards/categories.html?to=" + member.GetUserName(_g.Brand) + "&MemberID=" + member.MemberID.ToString() + "&return=1", g, true);

                // Assign url to ECard link.
                this.lnkEcard.NavigateUrl = destPageUrl;

            }
            else
            {
                this.phEcardLink.Visible = false;
            }

            //set email and flirt/tease link
            bool returnToMember = (g.GetEntryPoint(Request) == BreadCrumbHelper.EntryPoint.LookUpMember);
            this.lnkEmail.NavigateUrl = ProfileDisplayHelper.GetEmailLink(member.MemberID, returnToMember);
            this.lnkFlirt.NavigateUrl = ProfileDisplayHelper.GetFlirtLink(g, member.MemberID);
            ProfileDisplayHelper.AddFlirtOnClick(g, ref this.lnkFlirt);

            //set YNM
            if (g.IsYNMEnabled)
            {
                                
                try
                {
                    ClickMask clickMask = g.List.GetClickMask(g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, member.MemberID);

                    //Put the clickMask in a hidden input for use by JavaScript (YNMVote)
                    YNMVoteStatus.Value = ((int)clickMask).ToString();

                    //Mutual yes image
                    if (!(((clickMask & ClickMask.TargetMemberYes) == ClickMask.TargetMemberYes) && ((clickMask & ClickMask.MemberYes) == ClickMask.MemberYes)))
                    {
                        this.divMutualYes.Style.Add("display", "none");
                    }
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine(ex.Message);

                    //error here is likely in GetClickMask, because there is no member (e.g. Visitor)
                    this.divMutualYes.Style.Add("display", "none");
                    YNMVoteStatus.Value = Convert.ToInt32(ClickMask.None).ToString();
                }

                //set Vote buttons
                this.ImageYes.MemberID = member.MemberID;
                this.ImageMaybe.MemberID = member.MemberID;
                this.ImageNo.MemberID = member.MemberID;

            }
            else
            {
                phYNM.Visible = false;
            }


        }
    }
}