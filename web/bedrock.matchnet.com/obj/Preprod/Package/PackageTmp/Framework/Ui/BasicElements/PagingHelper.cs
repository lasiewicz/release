﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Contexts;
using System.Web;
using System.Text;
using System.Web.UI.WebControls;
using System.Collections.Specialized;
using System.Web.UI;

namespace Matchnet.Web.Framework.Ui.BasicElements
{
    public class PagingHelper
    {
        public const string QSPARAM_STARTROW = "StartRow";
        public const string QSPARAM_RESOURCECONSTANT = "rc";
        public const string CHAPTER_SPACER = "&nbsp;/&nbsp;";

        public static void LoadPageNavigation(Control topPagingControl, Control bottomPagingControl, string pageName, int totalRow, int chapterSize, int pageSize, int startRow, string NextLinkText, string PreviousLinkText)
        {
            LoadPageNavigation(topPagingControl, bottomPagingControl, pageName, totalRow, chapterSize, pageSize, startRow, NextLinkText, PreviousLinkText, false);
        }

        public static void LoadPageNavigation(Control topPagingControl, Control bottomPagingControl, string pageName, int totalRow, int chapterSize, int pageSize, int startRow, string NextLinkText, string PreviousLinkText, bool PhotosOnly)
        {
            if (IsPageNumberBasedPaginationEnabled(null))
            {
                LoadPageNavigationVersion1(topPagingControl, bottomPagingControl, pageName, totalRow, chapterSize, pageSize, startRow, PhotosOnly);
            }
            else
            {
                LoadPageNavigationVersion0(topPagingControl, bottomPagingControl, pageName, totalRow, chapterSize, pageSize, startRow, NextLinkText, PreviousLinkText);
            }

        }

        public static bool IsPageNumberBasedPaginationEnabled(ContextGlobal g)
        {
            bool isEnabled = false;
            if (g == null)
            {
                g = HttpContext.Current.Items["g"] as ContextGlobal;
            }

            try
            {
                //0 (off), 1 (On - Everywhere), 2 (On - Search Results Beta), 4 (On - Existing Search Results)
                int isEnabledMask = Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ENABLE_PAGE_NUMBER_BASED_PAGINATION_MASK", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID));
                if ((isEnabledMask & 1) == 1)
                    isEnabled = true;
                else if ((isEnabledMask & 2) == 2 && BetaHelper.IsSearchRedesign30Enabled(g))
                {
                    isEnabled = true;
                }
                else if ((isEnabledMask & 4) == 4)
                {
                    isEnabled = true;
                }
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
                isEnabled = false;
            }
            return isEnabled;
        }

        #region Result based pagination
        /// <summary>
        /// Standard navigation on bedrock which displays as results instead of page numbers
        /// </summary>
        private static void LoadPageNavigationVersion0(Control topPagingControl, Control bottomPagingControl, string pageName, int totalRow, int chapterSize, int pageSize, int startRow, string NextLinkText, string PreviousLinkText)
        {
            // if the start row is greater than the totalrows, set the startrow to the nearest round page_size less than totalrows
            if (startRow > totalRow)
            {
                startRow = (totalRow / pageSize) * pageSize + 1;
            }

            // define needed vars
            int pageCount = ((totalRow - 1) / pageSize) + 1;
            int pageRequested = startRow / pageSize + 1;

            int chapter;
            int pageCurrent = 0;
            int extraRows = totalRow - (chapterSize * pageSize);
            int extraPagesNeeded = Convert.ToInt32(Math.Ceiling((double)extraRows / (double)pageSize));

            // if the requested page is greater than the count, set the requested page to the count
            if (pageRequested > pageCount)
            {
                pageRequested = pageCount;
            }

            // set the chapter
            if (pageRequested > chapterSize)
            {
                chapter = (startRow / pageSize / (chapterSize + extraPagesNeeded)) + 1;
            }
            else
            {
                chapter = (startRow / pageSize / chapterSize) + 1;
            }

            // set the start page depending on the startrow
            int startSize = 0;
            int maxChapter = chapterSize;
            if (startRow > pageSize * 2)
            {
                startSize = (startRow - 1) / pageSize;

                // if the start size is within the last two page elements of all of the pages,
                // decrement the start size.  This means that always three page links
                // will be shown at the end of the list.
                if ((startSize > chapterSize && pageCount - startSize < chapterSize - 1) || pageCount == chapterSize)
                {
                    startSize--;
                }

                maxChapter = ((startRow - 1) / pageSize) + (chapterSize - 1);
            }
            else
            {
                startSize = 1;
            }

            AddPageNavigation(topPagingControl, bottomPagingControl, pageName, pageRequested, pageCount, pageCurrent, startSize, chapter, maxChapter, chapterSize, startRow, pageSize, totalRow, NextLinkText, PreviousLinkText);
        }

        private static void AddPageNavigation(Control topPagingControl, Control bottomPagingControl, string pageName, int pageRequested, int pageCount, int pageCurrent, int startSize, int chapter, int maxChapter, int chapterSize, int startRow, int pageSize, int totalRow, string NextLinkText, string PreviousLinkText)
        {
            if (topPagingControl != null && bottomPagingControl != null)
            {
                // show back to start pipe and arrow if necessary.
                if (pageCount > chapterSize && startRow >= pageSize * (chapterSize - 1))
                {
                    topPagingControl.Controls.Add(CreateArrowLink(1, "breadCrumbPagesLink", "start", pageSize, pageName));
                    bottomPagingControl.Controls.Add(CreateArrowLink(1, String.Empty, "start", pageSize, pageName));

                    topPagingControl.Controls.Add(new LiteralControl(CHAPTER_SPACER));
                    bottomPagingControl.Controls.Add(new LiteralControl(CHAPTER_SPACER));
                }

                // Do not show a previous link if we are on the first page.
                if (pageRequested > 1)
                {
                    topPagingControl.Controls.Add(CreateListArrowLink(pageRequested, String.Empty, false, pageSize, pageName, NextLinkText, PreviousLinkText));
                    topPagingControl.Controls.Add(new LiteralControl(CHAPTER_SPACER));
                    bottomPagingControl.Controls.Add(CreateListArrowLink(pageRequested, String.Empty, false, pageSize, pageName, NextLinkText, PreviousLinkText));
                    bottomPagingControl.Controls.Add(new LiteralControl(CHAPTER_SPACER));
                }

                // display each page
                for (int page = startSize; (page <= maxChapter && page <= pageCount); page++)
                {
                    pageCurrent = page * chapter;

                    bool isCurrentPage = (page == pageRequested);

                    topPagingControl.Controls.Add(CreatePagingLink(pageCurrent, "breadCrumbPagesLink", isCurrentPage, pageSize, pageName, totalRow));
                    bottomPagingControl.Controls.Add(CreatePagingLink(pageCurrent, String.Empty, isCurrentPage, pageSize, pageName, totalRow));

                    // display the character spacer in between page links (but not at the end)
                    if ((page < maxChapter) && (pageCurrent != pageCount))
                    {
                        topPagingControl.Controls.Add(new LiteralControl(CHAPTER_SPACER));
                        bottomPagingControl.Controls.Add(new LiteralControl(CHAPTER_SPACER));
                    }
                }

                // show end arrows if necessary.
                if (pageCount > chapterSize && ((totalRow - startRow) > (pageSize * 2)))
                {
                    // Do not show a next link if we are on the last page.
                    if (pageRequested < pageCount)
                    {
                        topPagingControl.Controls.Add(new LiteralControl(CHAPTER_SPACER));
                        bottomPagingControl.Controls.Add(new LiteralControl(CHAPTER_SPACER));

                        topPagingControl.Controls.Add(CreateListArrowLink(pageRequested, String.Empty, true, pageSize, pageName, NextLinkText, PreviousLinkText));
                        bottomPagingControl.Controls.Add(CreateListArrowLink(pageRequested, String.Empty, true, pageSize, pageName, NextLinkText, PreviousLinkText));
                    }
                }
            }
        }

        private static string GetPageText(int pageCurrent, bool showBold, int pageSize, int totalRow)
        {
            int start = (pageCurrent * pageSize) - pageSize + 1;
            int end = start + pageSize - 1;

            // use the total rows instead of the normal last number if it is the last number
            if (totalRow < end && totalRow >= start)
            {
                end = totalRow;
            }

            if (showBold)
            {
                return "<strong>" + start + "-" + end + "</strong>";
            }

            return start + "-" + end;
        }

        private static HyperLink CreateArrowLink(int nextPage, string cssClass, string direction, int pageSize, string pageName)
        {
            int pageToLink;
            HyperLink link = new HyperLink();
            if (cssClass != String.Empty)
            {
                link.CssClass = cssClass;
            }
            if (direction == "next")
            {
                link.Text = "&gt;&gt;";
                pageToLink = (nextPage - 1) * pageSize + 1;
            }
            else if (direction == "end")
            {
                link.Text = "&gt;|";
                pageToLink = (nextPage - 1) * pageSize + 1;
            }
            else if (direction == "start")
            {
                link.Text = "|&lt;";
                pageToLink = (nextPage - 1) * pageSize + 1;
            }
            else
            {
                link.Text = "&lt;&lt;";
                pageToLink = nextPage;
            }

            link.NavigateUrl = BuildPagingURL(pageToLink, pageName);
            return link;
        }

        /// <summary>
        /// This is used to create the Next and Previous arrows.
        /// </summary>
        /// <param name="page"></param>
        /// <param name="cssClass"></param>
        /// <param name="isNext"></param>
        /// <returns></returns>
        private static HyperLink CreateListArrowLink(int page, string cssClass, bool isNext, int pageSize, string pageName, string nextLinkText, string previousLinkText)
        {
            int pageToLink;
            HyperLink link = new HyperLink();
            if (cssClass != String.Empty)
            {
                link.CssClass = cssClass;
            }

            if (isNext)
            {
                link.Text = nextLinkText;
                pageToLink = page * pageSize + 1;
            }
            else
            {
                link.Text = previousLinkText;
                pageToLink = (page - 2) * pageSize + 1;
            }

            link.NavigateUrl = BuildPagingURL(pageToLink, pageName);
            return link;
        }

        private static HyperLink CreatePagingLink(int pageCurrent, string cssClass, bool showBold, int pageSize, string pageName, int totalRow)
        {
            int pageToLink = (pageCurrent - 1) * pageSize + 1;
            HyperLink link = new HyperLink();
            if (cssClass != String.Empty)
            {
                link.CssClass = cssClass;
            }
            link.Text = GetPageText(pageCurrent, showBold, pageSize, totalRow);
            link.NavigateUrl = BuildPagingURL(pageToLink, pageName);
            return link;
        }


        #endregion

        #region Page number based pagination
        /// <summary>
        /// Revised pagination which displays using standard page numbering
        /// </summary>
        private static void LoadPageNavigationVersion1(Control topPagingControl, Control bottomPagingControl, string pageName, int totalRow, int chapterSize, int pageSize, int startRow, bool photosOnly)
        {
            if (totalRow > 0)
            {
                ContextGlobal g = HttpContext.Current.Items["g"] as ContextGlobal;

                //These shouldn't change so we can specify them all here so it's the same anywhere page-based pagination is used
                string NextLinkText = g.GetResource("TXT_PAGE_NEXT");
                string PreviousLinkText = g.GetResource("TXT_PAGE_PREVIOUS");
                string FirstLinkText = g.GetResource("TXT_PAGE_FIRST");
                string LastLinkText = g.GetResource("TXT_PAGE_LAST");

                // if the start row is greater than the totalrows, set the startrow to the nearest round page_size less than totalrows
                if (startRow > totalRow)
                {
                    startRow = (totalRow / pageSize) * pageSize + 1;
                }

                // determine requested page and total pages available
                int pageCount = ((totalRow - 1) / pageSize) + 1;
                int pageRequested = startRow / pageSize + 1;

                if (pageCount > 1)
                {
                    // if the requested page is greater than the count, set the requested page to the count
                    if (pageRequested > pageCount)
                    {
                        pageRequested = pageCount;
                    }

                    //determine pages to display based on chapter size
                    chapterSize = 4;
                    int startPage = 1;
                    int endPage = 1;
                    if (pageCount > 1)
                    {
                        if (pageRequested < chapterSize)
                        {
                            endPage = (chapterSize < pageCount) ? chapterSize : pageCount;
                        }
                        else if (pageRequested == pageCount)
                        {
                            endPage = pageCount;
                        }
                        else
                        {
                            endPage = pageRequested + 1;
                        }

                        startPage = endPage - chapterSize + 1;
                        if (startPage <= 0)
                            startPage = 1;
                    }

                    string pageCssClass = "pagination-neo";
                    string pageActiveCssClass = "pagination-neo active";
                    string nextCssClass = "pagination-neo next";
                    string prevCssClass = "pagination-neo prev";
                    string firstCssClass = "pagination-neo first s-icon-pagination-neo-first spr";
                    string lastCssClass = "pagination-neo last s-icon-pagination-neo-last spr";
                    string moreIndicatorCssClass = "more";

                    //display first page and previous page link
                    if (pageRequested > 1)
                    {
                        topPagingControl.Controls.Add(CreateFirstLastLinkV1(pageCount, firstCssClass, pageSize, pageName, FirstLinkText, true, photosOnly));
                        bottomPagingControl.Controls.Add(CreateFirstLastLinkV1(pageCount, firstCssClass, pageSize, pageName, FirstLinkText, true, photosOnly));

                        topPagingControl.Controls.Add(CreateNextPreviousLinkV1(pageRequested, prevCssClass, pageSize, pageName, PreviousLinkText, null, photosOnly));
                        bottomPagingControl.Controls.Add(CreateNextPreviousLinkV1(pageRequested, prevCssClass, pageSize, pageName, PreviousLinkText, null, photosOnly));

                        if (startPage > 1)
                        {
                            Literal moreIndicatorTop = new Literal();
                            moreIndicatorTop.Text = "<span class=\"" + moreIndicatorCssClass + "\">&hellip;</span>";
                            topPagingControl.Controls.Add(moreIndicatorTop);

                            Literal moreIndicatorBottom = new Literal();
                            moreIndicatorBottom.Text = "<span class=\"" + moreIndicatorCssClass + "\">&hellip;</span>";
                            bottomPagingControl.Controls.Add(moreIndicatorBottom);
                        }
                    }

                    //display page links
                    for (int i = startPage; i <= endPage; i++)
                    {
                        bool isCurrentPage = (i == pageRequested);
                        string pageCss = isCurrentPage ? pageActiveCssClass : pageCssClass;
                        topPagingControl.Controls.Add(CreatePagingLinkV1(i, pageCss, pageSize, pageName, photosOnly));
                        bottomPagingControl.Controls.Add(CreatePagingLinkV1(i, pageCss, pageSize, pageName, photosOnly));
                    }

                    //display next page link last page link
                    if (pageRequested < pageCount)
                    {
                        if (endPage < pageCount)
                        {
                            Literal moreIndicatorTop = new Literal();
                            moreIndicatorTop.Text = "<span class=\"" + moreIndicatorCssClass + "\">&hellip;</span>";
                            topPagingControl.Controls.Add(moreIndicatorTop);

                            Literal moreIndicatorBottom = new Literal();
                            moreIndicatorBottom.Text = "<span class=\"" + moreIndicatorCssClass + "\">&hellip;</span>";
                            bottomPagingControl.Controls.Add(moreIndicatorBottom);
                        }

                        topPagingControl.Controls.Add(CreateNextPreviousLinkV1(pageRequested, nextCssClass, pageSize, pageName, null, NextLinkText, photosOnly));
                        bottomPagingControl.Controls.Add(CreateNextPreviousLinkV1(pageRequested, nextCssClass, pageSize, pageName, null, NextLinkText, photosOnly));

                        topPagingControl.Controls.Add(CreateFirstLastLinkV1(pageCount, lastCssClass, pageSize, pageName, LastLinkText, false, photosOnly));
                        bottomPagingControl.Controls.Add(CreateFirstLastLinkV1(pageCount, lastCssClass, pageSize, pageName, LastLinkText, false, photosOnly));
                    }
                }
            }
        }

        private static HyperLink CreatePagingLinkV1(int pageCurrent, string cssClass, int pageSize, string pageName, bool photoOnly)
        {
            int pageToLink = (pageCurrent - 1) * pageSize + 1;

            HyperLink link = new HyperLink();
            if (cssClass != String.Empty)
            {
                link.CssClass = cssClass;
            }
            link.Text = pageCurrent.ToString();
            link.NavigateUrl = BuildPagingURL(pageToLink, pageName, photoOnly);
            return link;
        }

        private static HyperLink CreateNextPreviousLinkV1(int pageCurrent, string cssClass, int pageSize, string pageName, string previousText, string nextText, bool photoOnly)
        {
            int pageToLink = 0;
            HyperLink link = new HyperLink();
            bool isNext = !string.IsNullOrEmpty(nextText) ? true : false;
            if (isNext)
            {
                link.Text = nextText;
                pageToLink = pageCurrent * pageSize + 1;
            }
            else
            {
                link.Text = previousText;
                pageToLink = (pageCurrent - 2) * pageSize + 1;
            }

            if (cssClass != String.Empty)
            {
                link.CssClass = cssClass;
            }
            link.NavigateUrl = BuildPagingURL(pageToLink, pageName, photoOnly);
            return link;
        }

        private static HyperLink CreateFirstLastLinkV1(int pageCount, string cssClass, int pageSize, string pageName, string linkText, bool isFirst, bool photoOnly)
        {
            int pageToLink = 0;
            HyperLink link = new HyperLink();
            link.Text = linkText;

            if (isFirst)
            {
                pageToLink = 1;
            }
            else
            {
                pageToLink = (pageCount - 1) * pageSize + 1;
            }

            if (cssClass != String.Empty)
            {
                link.CssClass = cssClass;
            }
            link.NavigateUrl = BuildPagingURL(pageToLink, pageName, photoOnly);
            return link;
        }

        #endregion

        private static string BuildPagingURL(int startRow, string pageName)
        {
            return BuildPagingURL(startRow, pageName, false);
        }

        private static string BuildPagingURL(int startRow, string pageName, bool photoOnly)
        {
            //start with the name of the current control
            StringBuilder url = null;
            if (pageName.ToLower().IndexOf(".aspx?") < 0)
                url = new StringBuilder(pageName + ".aspx?");
            else
                url = new StringBuilder(pageName);
            NameValueCollection qsParams = HttpContext.Current.Request.QueryString;
            if (qsParams.Keys.Count > 0)
            {
                //rebuild the querystring portion of the URL 
                foreach (string key in qsParams.Keys)
                {
                    //We're adding our own start row parameter, so ignore that part of the querystring. Also, 
                    //we don't want to pass the resource constant around since it's a one time message, 
                    // so strip it out as well. 
                    //Also ignore some other params for beta and resetting of start row
                    if (key != null && !key.Equals(QSPARAM_STARTROW) && !key.Equals(QSPARAM_RESOURCECONSTANT)
                        && !key.ToLower().Equals("viewmode") && !key.ToLower().Equals("pgrefresh") 
                        && !key.ToLower().Equals("enablebeta") && !key.ToLower().Equals("disablebeta"))
                    {
                        if (url.ToString().ToLower().IndexOf(key.ToLower() + "=") < 0)
                            url.Append(key + "=" + qsParams[key] + "&");
                    }
                }
            }

            url.Append(QSPARAM_STARTROW + "=" + startRow);

            if (photoOnly)
            {
                url.Append("&op=true");
            }

            if (!string.IsNullOrEmpty(FrameworkGlobals.CheckForFriendlyURL()))
            {
                url.Insert(0, "/Applications/Search/");
            }

            return url.ToString();
        }
    }
}
