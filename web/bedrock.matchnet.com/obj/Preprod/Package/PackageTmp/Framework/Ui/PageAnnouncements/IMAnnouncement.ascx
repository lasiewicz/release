﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="IMAnnouncement.ascx.cs" Inherits="Matchnet.Web.Framework.Ui.PageAnnouncements.IMAnnouncement" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>

<%--IM Announcement, you only see it once until you hit close if you want to see it again, add this to url &showIMAnnouncement=true--%>
<div id="IMAnnouncement" class="feature-announcement image im">
    <a class="click-close" href="#"><mn:Txt ID="txtClose" ResourceConstant="TXT_CLOSE" runat="server" /></a>
    <mn:image runat="server" id="Image3" CssClass="fb-ico" FileName="img-announcement-im.jpg" ResourceConstant="ALT_ANNOUNCEMENT" />
</div>

<script type="text/javascript">
    var featureAnnouncement = $j("#IMAnnouncement");

    $j(".click-close", featureAnnouncement).click(function (e) {
        e.preventDefault();
        IMAnnouncementClosePrompt();
    });

    //close prompt
    function IMAnnouncementClosePrompt() {
        //hide prompt
        featureAnnouncement.hide();

        //update member info that prompt was displayed and closed
        IMAnnouncementUpdatePromptSeen();

        return false;
    }

    function IMAnnouncementUpdatePromptSeen() {
        $j.ajax({
            type: "POST",
            url: "/Applications/API/Member.asmx/UpdateAttributeInt",
            data: "{memberID:" + <%=MemberID.ToString() %> + ", attributeName: \"PageAnnouncementViewedMask\", attributeValue:2}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function(result) {
                var updateResult = (typeof result.d == 'undefined') ? result : result.d;
                if (updateResult.Status != 2) {
                    alert(updateResult.StatusMessage);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert(textStatus);
            }
        });
    }

</script>