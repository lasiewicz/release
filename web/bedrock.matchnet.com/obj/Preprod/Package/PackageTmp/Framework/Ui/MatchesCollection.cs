﻿using System;

using Matchnet.MembersOnline.ValueObjects;

namespace Matchnet.Web.Framework.Ui
{
    /// <summary>
    /// Summary description for MatchesCollection
    /// This will store the settings for filtering MatchMeter matches page.
    /// </summary>
    public class MatchesCollection
    {
        #region Members

        private int _genderMask;
        private int _regionID;
        private int _ageMin;
        private int _ageMax;
        private bool _photoRequired;

        #endregion

        #region Properties

        public int GenderMask
        {
            get { return (_genderMask); }
        }

        public int RegionID
        {
            get { return (_regionID); }
        }

        public int AgeMin
        {
            get { return (_ageMin); }
        }

        public int AgeMax
        {
            get { return (_ageMax); }
        }

        public bool PhotoRequired
        { get { return _photoRequired; } }

        #endregion

        #region Constructors

        public MatchesCollection(int genderMask, int regionID, int ageMin, int ageMax, bool photoRequired)
        {
            _genderMask = genderMask;
            _regionID = regionID;
            _ageMin = ageMin;
            _ageMax = ageMax;
            _photoRequired = photoRequired;
        }

        #endregion
    }
}
