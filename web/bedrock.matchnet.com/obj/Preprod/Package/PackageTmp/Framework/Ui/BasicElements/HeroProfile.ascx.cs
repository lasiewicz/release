﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Member.ValueObjects;
using Matchnet.Member.ValueObjects.Interfaces;
using Matchnet.Member.ValueObjects.Photos;
using Matchnet.Web.Framework.Managers;
using Matchnet.Web.Framework.Ui.PageElements;
using Matchnet.Web.Applications.MemberProfile;
using Matchnet.Lib;
using System.Collections;
using Matchnet.List.ValueObjects;
using Matchnet.Content.ServiceAdapters.Links;
using Matchnet.Configuration.ServiceAdapters.Analitics;
using Matchnet.Web.Applications.ColorCode;

namespace Matchnet.Web.Framework.Ui.BasicElements
{
    public partial class HeroProfile : FrameworkControl
    {
        #region Fields
        private int _ordinal;
        private int _memberID;
        private BreadCrumbHelper.EntryPoint _myEntryPoint;
        private MOCollection _myMOCollection;
        private int _hotListCategoryID;
        private IMemberDTO _Member;
        private bool _isSpotlighted = false;
        protected string viewProfileUrl;
        private DateTime _lastViewedDate = DateTime.MinValue;
        private DisplayContextType _DisplayContext = DisplayContextType.Default;
        private int _approvedPhotosCount = -1;
        protected Color _Color = Color.none;
        protected string _ColorText = "";

        const int MAX_WORD_LENGTH = 10;
        const int MAX_STRING_LENGTH = 40;
        const int MAX_USERNAME_LENGTH = 16;
        const int MAX_ESSAY_LENTH = 300;
        #endregion

        #region Properties

        public DisplayContextType DisplayContext
        {
            get { return _DisplayContext; }
            set { _DisplayContext = value; }
        }

        public IMemberDTO member
        {
            get { return _Member; }
            set
            {
                _Member = value;
                _memberID = _Member.MemberID;
            }
        }

        public int MemberID
        {
            get { return (_memberID); }
        }

        public int Ordinal
        {
            get { return (_ordinal); }
            set { _ordinal = value; }
        }


        public BreadCrumbHelper.EntryPoint MyEntryPoint
        {
            get { return (_myEntryPoint); }
            set { _myEntryPoint = value; }
        }

        public MOCollection MyMOCollection
        {
            get { return (_myMOCollection); }
            set { _myMOCollection = value; }
        }

        public int HotListCategoryID
        {
            get { return (_hotListCategoryID); }
            set { _hotListCategoryID = value; }
        }

        public bool IsSpotlighted
        {
            get { return (this._isSpotlighted); }
            set { this._isSpotlighted = value; }
        }

        public DateTime LastViewedDate
        {
            get { return this._lastViewedDate; }
            set { this._lastViewedDate = value; }
        }

        public int ApprovedPhotosCount
        {
            get
            {
                try
                {
                    if (_approvedPhotosCount == -1)
                    {
                        _approvedPhotosCount = MemberPhotoDisplayManager.Instance.GetApprovedPhotosCount(g.Member, member, g.Brand);
                    }

                    return _approvedPhotosCount;

                }
                catch (Exception ex)
                { return 0; }

            }

        }
        #endregion

        #region Event Handler
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected override void OnPreRender(EventArgs e)
        {

            //omniture
            ImageYes.Attributes.Add("onclick", g.AnalyticsOmniture.GetOnClickCustomLinkTracking(WebConstants.Action.ClickY, g.AnalyticsOmniture.PageName, "", false, false));
            ImageNo.Attributes.Add("onclick", g.AnalyticsOmniture.GetOnClickCustomLinkTracking(WebConstants.Action.ClickN, g.AnalyticsOmniture.PageName, "", false, false));
            ImageMaybe.Attributes.Add("onclick", g.AnalyticsOmniture.GetOnClickCustomLinkTracking(WebConstants.Action.ClickM, g.AnalyticsOmniture.PageName, "", false, false));


            base.OnPreRender(e);
        }
        #endregion


        #region Methods
        /// <summary>
        /// Loads Member Profile based on properties already set into the control, including the Member object
        /// </summary>
        public void LoadMemberProfile()
        {
            try
            {
                SearchResultProfile searchResult = new SearchResultProfile(_Member, Ordinal);

                if (this.IsSpotlighted)
                {
                    viewProfileUrl = BreadCrumbHelper.MakeViewProfileLink(this.MyEntryPoint, this.MemberID, this.Ordinal, this.MyMOCollection, this.HotListCategoryID, false, (int)BreadCrumbHelper.PremiumEntryPoint.Spotlight);
                    viewProfileUrl = BreadCrumbHelper.AppendParamToProfileLink(viewProfileUrl, WebConstants.URL_PARAMETER_NAME_HERO_PROFILE + "=" + "Hero+Spotlight");
                }
                else
                {
                    viewProfileUrl = BreadCrumbHelper.MakeViewProfileLink(this.MyEntryPoint, this.MemberID, this.Ordinal, this.MyMOCollection, this.HotListCategoryID, false, (int)BreadCrumbHelper.PremiumEntryPoint.NoPremium);
                    viewProfileUrl = BreadCrumbHelper.AppendParamToProfileLink(viewProfileUrl, WebConstants.URL_PARAMETER_NAME_HERO_PROFILE + "=" + "Hero+Match");
                    this.panelHeroProfileContent.CssClass = "hero-profile-content-div";
                }
                imgProfile.NavigateUrl = viewProfileUrl;
                //set basic info
                lnkUserName1.NavigateUrl = viewProfileUrl;
                lnkUserName1.Text = FrameworkGlobals.Ellipsis(_Member.GetUserName(_g.Brand), 12);
                this.literalAge.Text = FrameworkGlobals.GetAge(this.member, g.Brand).ToString();
                if (this.literalAge.Text.Trim() == "")
                {
                    this.literalAge.Text = "&nbsp;";
                }
                else
                {
                    if (FrameworkGlobals.isHebrewSite(g.Brand))
                    {
                        literalAge.Text = g.GetResource("TXT_YEARS_OLD", this) + " " + literalAge.Text;
                    }
                    else
                    {
                        literalAge.Text += " " + g.GetResource("TXT_YEARS_OLD", this);
                    }
                }

                this.literalLocation.Text = ProfileDisplayHelper.GetRegionDisplay(this.member, g);
                if (this.literalLocation.Text.Trim() == "") this.literalLocation.Text = "&nbsp;";

                DisplayMemberPhoto();

                LinkParent parent = LinkParent.HomePageHeroProfile;

                switch (_DisplayContext)
                {
                    case DisplayContextType.Default:
                        parent = LinkParent.HomePageHeroProfile;
                        break;
                }

                lnkEmail.NavigateUrl = String.Format(FrameworkGlobals.GetEmailLink(this.MemberID, false) + "&LinkParent={0}", parent.ToString("d"));

                literalSeeking.Text = ProfileDisplayHelper.GetMaritalStatusSeekingGenderDisplay(this.member, this.g);
                if (this.literalSeeking.Text.Trim() == "") literalSeeking.Text = "&nbsp;";

                this.literalSeekingFor.Text = ProfileDisplayHelper.GetMaskContent(this.member, g, "RelationshipMask");
                if (this.literalSeekingFor.Text.Trim() == "") this.literalSeekingFor.Text = "&nbsp;";

                //displayed last time this profile viewed you
                if (this._lastViewedDate != DateTime.MinValue)
                {
                    this.phViewedProfile.Visible = true;

                    int genderMask = this.member.GetAttributeInt(g.Brand, "gendermask");
                    if ((genderMask & ConstantsTemp.GENDERID_MALE) == ConstantsTemp.GENDERID_MALE)
                        this.literalHeViewed.Visible = true;
                    else if ((genderMask & ConstantsTemp.GENDERID_FEMALE) == ConstantsTemp.GENDERID_FEMALE)
                        this.literalSheViewed.Visible = true;
                    else
                        this.literalUndefinedViewed.Visible = true;


                    lastTimeAction.LoadLastTime(this._lastViewedDate);
                }


                //display either "We both like" or "About me"
                this.lnkAboutMe.NavigateUrl = viewProfileUrl;
                this.lnkAboutMe.Text = FrameworkGlobals.GetMorePhotoLink(ApprovedPhotosCount, g, this);
                this.lnkWeBothLikeMore.NavigateUrl = viewProfileUrl;
                string weBothLike = ProfileDisplayHelper.GetSpotlightMatchString(this.g, this.member);

                if (this.IsSpotlighted && !String.IsNullOrEmpty(weBothLike))
                {
                    this.phWeBothLike.Visible = true;
                    literalWeBothLikeText.Text = FrameworkGlobals.Ellipsis(weBothLike, MAX_ESSAY_LENTH);
                }
                else
                {
                    this.phAboutMe.Visible = true;
                    this.literalAboutMeText.Text = FrameworkGlobals.Ellipsis(ProfileDisplayHelper.GetFormattedEssayText(this.member, this.g, "aboutme"), MAX_ESSAY_LENTH);
                    if (this.literalAboutMeText.Text.Trim() == "") literalAboutMeText.Text = "&nbsp;";
                }

                //load ymn
                if(g.IsYNMEnabled)
                {
                    LoadYMN();
                }
                
                SetEmailButtonABTest();

                //color code
                if (ColorCodeHelper.IsColorCodeEnabled(g.Brand) && ColorCodeHelper.HasMemberCompletedQuiz(_Member, g.Brand) && !ColorCodeHelper.IsMemberColorCodeHidden(_Member, g.Brand))
                {
                    phColorCode.Visible = true;
                    _Color = ColorCodeHelper.GetPrimaryColor(_Member, g.Brand);
                    _ColorText = ColorCodeHelper.GetFormattedColorText(_Color);
                    switch (_Color)
                    {
                        case Color.blue:
                            phBlueTip.Visible = true;
                            break;
                        case Color.red:
                            phRedTip.Visible = true;
                            break;
                        case Color.white:
                            phWhiteTip.Visible = true;
                            break;
                        case Color.yellow:
                            phYellowTip.Visible = true;
                            break;
                    }
                }

            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }

        }

        private void SetEmailButtonABTest()
        {
            Scenario scenario = g.GetABScenario("MINIPROFILE_BUTTON");
            if (scenario == Scenario.B)
            {
                imgEmail.FileName = "btn-view-fullprofile.gif";
                imgEmail.ResourceConstant = "VIEW_MEMBER";
                imgEmail.TitleResourceConstant = "VIEW_MEMBER";
                lnkEmail.NavigateUrl = g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.ViewProfile, WebConstants.Action.ViewMemberButton, lnkUserName1.NavigateUrl, string.Empty);
            }
            else
            {
                lnkEmail.NavigateUrl = g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.Compose, WebConstants.Action.EmailMeNowButton, lnkEmail.NavigateUrl, string.Empty);
            }
        }

        private void DisplayMemberPhoto()
        {
            // This must be appended to all photos that hit the file servers in order for the site-specific
            // "No Photo" images to appear.  Appending this to any other photos should not cause errors.
            string siteIDParam = "?" + WebConstants.URL_PARAMETER_NAME_SITEID + "=" + g.Brand.Site.SiteID.ToString();

            /* Only approved photos should be shown to both viewing member and 
            * the member who owns the photos */
            
            bool photoFound = false;
            var photo1 = MemberPhotoDisplayManager.Instance.GetDefaultPhoto(g.Member, _Member, g.Brand);

            if (photo1 != null)
            {
                //display large photo
                //MemberPhotoHelper photoHelper = new MemberPhotoHelper(RuntimeSettings.Instance);
                string imageURL = MemberPhotoDisplayManager.Instance.GetPhotoDisplayURL(g.Member, _Member, g.Brand, photo1, PhotoType.Full, PrivatePhotoImageType.Full); 
               
                photoFound = true;
                imgProfile.ImageUrl = imageURL;

                //add javascript to resize photo
                imgProfile.Attributes.Add("onload", "profile_resizePhoto(this);");

            }

            if (!photoFound)
            {
                //no approved photos, so hide photo
                imgProfile.Visible = false;

                //display ask me for my photo link
                pnlNoPhoto.Visible = true;
                lnkRequestPhotoUpload.NavigateUrl = ProfileDisplayHelper.GetEmailLink(this._Member.MemberID);
            }

        }

        private void LoadYMN()
        {
            // make sure not trying to add the vote bar to member's own profile
            if (this.MemberID != g.Member.MemberID)
            {
                this.phCommunicate.Visible = true;

                try
                {
                    //Put the clickMask in a hidden input for use by JavaScript (YNMVote)
                    ClickMask clickMask = g.List.GetClickMask(g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, member.MemberID);
                    YNMVoteStatus.Value = ((int)clickMask).ToString();

                    // if the target member voted yes, we want mutual yes to show up if current member votes yes or has already voted yes
                    if ((clickMask & ClickMask.TargetMemberYes) == ClickMask.TargetMemberYes)
                    {
                        if ((clickMask & ClickMask.MemberYes) == ClickMask.MemberYes) // if current member already voted yes
                        {
                            //show mutual yes icon
                            this.divBothSaidYes.Visible = true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine(ex.Message);

                    //error here is likely in GetClickMask, because there is no member (e.g. Visitor)
                    YNMVoteStatus.Value = Convert.ToInt32(ClickMask.None).ToString();
                }

                //set Vote buttons
                this.ImageYes.MemberID = member.MemberID;
                this.ImageMaybe.MemberID = member.MemberID;
                this.ImageNo.MemberID = member.MemberID;

                //set vote button text links
                YNMVoteParameters parameters = new YNMVoteParameters();

                parameters.SiteID = g.Brand.Site.SiteID;
                parameters.CommunityID = g.Brand.Site.Community.CommunityID;
                parameters.FromMemberID = g.Session.GetInt(WebConstants.SESSION_PROPERTY_NAME_MEMBERID);
                parameters.ToMemberID = _Member.MemberID;

                string encodedParameters = parameters.EncodeParams();
                TxtYes.Href = string.Format("javascript:YNMVote('{0}','1','{1}');",
                YNMVoteStatus.NamingContainer.ClientID, encodedParameters);
                TxtYes.Attributes.Add("onmouseover", string.Format("document." + WebConstants.HTML_FORM_ID + ".{0}.onmouseover();", ImageYes.ClientID));
                TxtYes.Attributes.Add("onmouseout", string.Format("document." + WebConstants.HTML_FORM_ID + ".{0}.onmouseout();", ImageYes.ClientID));

                TxtNo.Href = string.Format("javascript:YNMVote('{0}','2','{1}');",
                YNMVoteStatus.NamingContainer.ClientID, encodedParameters);
                TxtNo.Attributes.Add("onmouseover", string.Format("document." + WebConstants.HTML_FORM_ID + ".{0}.onmouseover();", ImageNo.ClientID));
                TxtNo.Attributes.Add("onmouseout", string.Format("document." + WebConstants.HTML_FORM_ID + ".{0}.onmouseout();", ImageNo.ClientID));

                TxtMaybe.Href = string.Format("javascript:YNMVote('{0}','3','{1}');",
                YNMVoteStatus.NamingContainer.ClientID, encodedParameters);
                TxtMaybe.Attributes.Add("onmouseover", string.Format("document." + WebConstants.HTML_FORM_ID + ".{0}.onmouseover();", ImageMaybe.ClientID));
                TxtMaybe.Attributes.Add("onmouseout", string.Format("document." + WebConstants.HTML_FORM_ID + ".{0}.onmouseout();", ImageMaybe.ClientID));



            }
        }

        #endregion

    }
}