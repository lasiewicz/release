﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ActivityCenter.ascx.cs"
    Inherits="Matchnet.Web.Framework.Ui.ActivityCenter" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="uc1" TagName="NoPhoto" Src="PageElements/NoPhoto20.ascx" %>
<!--START Activity Center-->
<asp:PlaceHolder ID="phActivityCenter" runat="server">
<div id="activity-center" class="rbox-style-clear clearfix">
<%--<div id="activity-center" class="activity-wrapper">--%>
<%--    <div class="activity-border-top"></div>
    <div class="activity-border-base">--%>
            <div class="activity-content">
                <%--Full Activity Content--%>
                <asp:PlaceHolder ID="plcActivityCenter" runat="server">
                    <div class="activity-profile-wrapper">
                        <asp:Panel ID="panelProfileIMageLargeVersion" runat="server" CssClass="activity-profile-pic"
                            Visible="false">
                            <mn:Image runat="server" ID="imgThumb" class="profileImageHover" Visible="false"
                                Border="0" Style="width: 54px; height: 70px;" ResourceConstant="ALT_PROFILE_PICTURE"
                                TitleResourceConstant="TTL_VIEW_MY_PROFILE" />
                            <uc1:NoPhoto runat="server" ID="noPhoto" class="no-photo" Visible="True" Width="54px"
                                Height="70px" />
                        </asp:Panel>
                        <%-- START LOGIN/LOGOUT --%>
                        <asp:PlaceHolder ID="plcLogoff" runat="Server" Visible="false">
                            <!-- logged in -->
                            <div id="statusLoggedIn" class="activity-profile-info clearfix">
                                <%=H2TagOpen%>
                                    <asp:HyperLink runat="server" ID="hplUsername" NavigateUrl="" /><%=H2TagClose%>
                                <mn:Txt ID="txtLogout" runat="server" TitleResourceConstant="NAV_LOGOUT" ResourceConstant="LEFT_LOGOUT"
                                    EnableViewState="false" CssClass="log-in-out" />
                                <asp:PlaceHolder ID="phEditProfileInfo" runat="server" Visible="false">
                                    <!--edit links-->
                                    <mn:Txt runat="server" ID="txtEditProfile" CssClass="manage-item" ResourceConstant="TXT_EDIT_PROFILE"
                                        Href="/Applications/MemberProfile/ViewProfile.aspx?Mode=Edit" Visible="false" />
                                    <mn:Txt runat="server" ID="txtCompleteProfile" CssClass="manage-item" ResourceConstant="TXT_COMPLETE_PROFILE"
                                        Href="/Applications/MemberProfile/ViewProfile.aspx?Mode=Edit" Visible="false" />
                                    <mn:Txt runat="server" ID="txtManagePhotos" CssClass="manage-item" ResourceConstant="TXT_MANAGE_PHOTOS"
                                        Href="/Applications/MemberProfile/MemberPhotoEdit.aspx" />
                                </asp:PlaceHolder>
                            </div>
                        </asp:PlaceHolder>
                        <!-- logged out (client side)-->
                        <div id="statusLoggedOut" style="display: none" class="activity-profile-info clearfix">
                            <%=H2TagOpen%>
                                <mn:Txt runat="server" ID="txtJoinNow1" ResourceConstant="JOIN_NOW" />
                            <%=H2TagClose%>
                            <a href="javascript:window.location.href='/Applications/Logon/Logon.aspx?DestinationURL='+ window.location.href">
                                <mn:Txt runat="server" TitleResourceConstant="NAV_LOGIN" ResourceConstant="NAVLOGIN"
                                    EnableViewState="false" ID="txtLogon" CssClass="log-in-out" />
                            </a>
                        </div>
                        <%--Join Now--%>
                        <asp:PlaceHolder ID="plcJoinNow" runat="server" Visible="False">
                            <!-- join now, displayed for non-logged in members-->
                            <div class="activity-profile-info clearfix">
                                <%=H2TagOpen%>
                                    <mn:Txt runat="server" ID="txtJoinNow" ResourceConstant="JOIN_NOW" />
                                <%=H2TagClose%>
                                <mn:Txt ID="txtLogin" runat="server" TitleResourceConstant="NAV_LOGIN" ResourceConstant="NAVLOGIN"
                                    EnableViewState="false" CssClass="log-in-out" />
                            </div>
                        </asp:PlaceHolder>
                        <!-- END LOGIN/LOGOUT -->
                    </div>
                    <!-- START MAIN CONTENT-->
                    <div class="activity clearfix" runat="server" id="divActivity">
                        <%--MOL--%>
                        <asp:PlaceHolder ID="plcMemberOnline" runat="server">
                            <!-- Members online count -->
                            <div id="divMembersOnline" class="activity-item online">
                                <span class="activity-icon online-icon">
                                    <mn:Image App="Home" runat="server" FileName="icon-status-online.gif" ID="imgMembersOnline"
                                    alt="" /></span>&#160;
                                <asp:HyperLink ID="lnkMembersOnline" runat="Server">
                                    <strong>
                                    <asp:Literal runat="server" ID="litMembersOnlineCount" /></strong>&#160;<mn:Txt ID="Txt2"
                                            runat="server" ResourceConstant="NAV_ONLINE" />
                                </asp:HyperLink>
                            </div>
                        </asp:PlaceHolder>
                        <%--NEW EMAIL MESSAGES--%>
                        <asp:PlaceHolder runat="server" ID="plcNewEmailMessages" Visible="false">
                            <!-- New email messages -->
                            <div class="activity-item email">
                                <span class="activity-icon">
                                    <mn:Image App="Home" runat="server" ID="imgNewMessages" FileName="icon-email-new.gif"
                                        ResourceConstant="ALT_NEW_MESSAGES_ICON" /></span>
                                <mn:Txt runat="server" ID="txtYouHave" ResourceConstant="TXT_YOU_HAVE" Visible="false" />
                                <asp:HyperLink runat="server" ID="lnkNewMessages">
                                    <mn:Txt runat="server" ID="txtNewMessages" ResourceConstant="LEFT_NEW_MESSAGES" />
                                </asp:HyperLink>
                            </div>
                        </asp:PlaceHolder>
                        <%--CHECK IM--%>
                        <asp:PlaceHolder ID="plcCheckIM" Visible="false" runat="server">
                            <!-- Active IM -->
                            <asp:PlaceHolder ID="plcIMActive" Visible="false" runat="server">
                                <asp:PlaceHolder ID="plcIMOnlineDebug" runat="server" Visible="false">
                                    <%--This is an AppDev debugging feature, add ?showimstatus=true to url to see it--%>
                                    <!-- Show IM Status -->
                                    <div id="divIMOnline" class="activity-item">
                                        <a href="javascript:initOnlineChecker(false);" title="<mn:Txt runat='server' resourceconstant='TTL_ONLINE_STATUS_ONLINE' />"
                                            onmouseover="toggle('divCheckingInvitationsCounter')" onmouseout="toggle('divCheckingInvitationsCounter')">
                                            <mn:Image App="Home" runat="server" ID="imgOnlineStatus" FileName="icon-chat.gif"
                                                TitleResourceConstant="TTL_ONLINE_STATUS_ONLINE" />
                                            <mn:Txt ID="Txt3" runat="server" ResourceConstant="TXT_ONLINE_STATUS_ONLINE" />
                                        </a><span id="lblCheckingInvitations" style="display: none">...</span>
                                        <div id="divCheckingInvitationsCounter" style="display: none">
                                            <mn:Txt runat="server" ResourceConstant="TXT_ONLINE_STATUS_NEXT_CHECK_IN" ID="Txt4" />
                                            <span id="lblCheckingInvitationsCounter">0</span>
                                            <mn:Txt runat="server" ResourceConstant="TXT_ONLINE_STATUS_TIME_UNIT" ID="Txt5" />
                                        </div>
                                    </div>
                                </asp:PlaceHolder>
                                <!-- Disconnected/Offline -->
                                <div id="divIMOffline" style="display: none" class="activity-item">
                                    <a href="javascript:window.location.href=window.location.href;" title="<mn:Txt runat='server' resourceconstant='TTL_ONLINE_STATUS_OFFLINE' />">
                                        <span class="activity-icon">
                                            <mn:Image App="Home" runat="server" ID="Image3" FileName="icon-status-disconnected.gif"
                                                TitleResourceConstant="TTL_ONLINE_STATUS_OFFLINE" alt="" /></span> <strong>
                                                    <mn:Txt runat="server" ResourceConstant="TXT_ONLINE_STATUS_OFFLINE" ID="Txt6" />
                                                </strong></a>
                                </div>
                            </asp:PlaceHolder>
                            <!--Inactive IM-->
                            <asp:PlaceHolder ID="plcIMHidden" Visible="false" runat="server">
                                <!-- Hidden -->
                                <div id="divIMHidden" class="activity-item">
                                    <a href="/Applications/MemberServices/DisplaySettings.aspx" title="<mn:Txt runat='server' resourceconstant='TTL_ONLINE_STATUS_HIDDEN' />">
                                        <span class="activity-icon">
                                            <mn:Image ID="Image1" App="Home" runat="server" FileName="icon-status-hidden.gif"
                                                TitleResourceConstant="TTL_ONLINE_STATUS_HIDDEN" /></span></a><mn:Txt ID="txtOnlineStatus" runat="server"
                                                    ResourceConstant="TXT_ONLINE_STATUS" />
                                    <b>
                                        <mn:Txt runat="server" ID="lblOnlineStatusHidden" ResourceConstant="TXT_ONLINE_STATUS_HIDDEN" />
                                    </b><a href="/Applications/MemberServices/DisplaySettings.aspx">
                                        <mn:Txt ID="txtONLINESTATUSEDIT" runat="server" ResourceConstant="TXT_ONLINE_STATUS_EDIT" />
                                    </a>
                                </div>
                            </asp:PlaceHolder>
                            <!-- End Instant Messenger Check IM and Status Indication -->
                        </asp:PlaceHolder>
                        <%--NEW YY CLICKS--%>
                        <asp:PlaceHolder runat="server" ID="plcNewClicks" Visible="false">
                            <!-- New Clicks -->
                            <div class="activity-item">
                                <span class="activity-icon">
                                    <mn:Image App="Home" runat="server" ID="imgMatches" FileName="icon-click-yy.gif"
                                        ResourceConstant="ALT_CLICK_ICON" /></span>
                                <asp:HyperLink runat="server" ID="lnkNewClicks">
                                    <strong>
                                        <mn:Txt runat="server" ID="Txt8" ResourceConstant="HOM_NEW_CLICKS" />
                                    </strong>
                                </asp:HyperLink>
                            </div>
                        </asp:PlaceHolder>
                        <%--JMETER LOGO--%>
                        <asp:PlaceHolder runat="server" ID="plcMatchMeter" Visible="false">
                            <div class="activity-item jmeter-logo">
                                <asp:HyperLink runat="server" ID="lnkMatchMeter">
                                    <mn:Image App="MatchMeter" runat="server" ID="imgMatchMeter" FileName="jmeter_logo_side.gif"
                                        ResourceConstant="ALT_JMETER_LOGO" TitleResourceConstant="TTL_JMETER_LOGO" />
                                </asp:HyperLink>
                            </div>
                        </asp:PlaceHolder>
                        <%--JMETER LOGO--%>
                        <asp:PlaceHolder runat="server" ID="plcSMSAlerts" Visible="false">
                            <div class="activity-item jalerts-logo">
                                <asp:HyperLink runat="server" ID="lnkSMSAlerts">
                                    <mn:Image runat="server" ID="imgSMSAlerts" FileName="jalerts_logo.gif" />
                                </asp:HyperLink>
                            </div>
                        </asp:PlaceHolder>
                    </div>
                    <!-- END MAIN CONTENT -->
                </asp:PlaceHolder>
                <%--Simple Activity Content Only - Part of Social Nav Changes--%>
                <asp:PlaceHolder ID="plcSimpleActivityCenter" runat="server" Visible="false">
                    <div class="activity-profile-wrapper">
                        <asp:Panel ID="panelProfileIMageLargeVersion2" runat="server" CssClass="activity-profile-pic"
                            Visible="false">
                            <mn:Image runat="server" ID="imgThumb2" class="profileImageHover" Visible="false"
                                Border="0" Style="width: 54px; height: 70px;" ResourceConstant="ALT_PROFILE_PICTURE"
                                TitleResourceConstant="TTL_VIEW_MY_PROFILE" />
                            <uc1:NoPhoto runat="server" ID="noPhoto2" class="no-photo" Visible="True" Width="54px"
                                Height="70px" />
                        </asp:Panel>
                        <!-- logged in -->
                        <div id="Div1" class="activity-profile-info clearfix">
                            <%=H2TagOpen%>
                                <asp:HyperLink runat="server" ID="hplUsername2" NavigateUrl="" /><%=H2TagClose%>
                            <asp:PlaceHolder ID="phEditProfileInfo2" runat="server" Visible="false">
                                <!--edit links-->
                                <mn:Txt runat="server" ID="txtEditProfile2" CssClass="manage-item" ResourceConstant="TXT_EDIT_PROFILE"
                                    Href="/Applications/MemberProfile/ViewProfile.aspx?Mode=Edit" Visible="false" />
                                <mn:Txt runat="server" ID="txtCompleteProfile2" CssClass="manage-item" ResourceConstant="TXT_COMPLETE_PROFILE"
                                    Href="/Applications/MemberProfile/ViewProfile.aspx?Mode=Edit" Visible="false" />
                                <mn:Txt runat="server" ID="txtManagePhotos2" CssClass="manage-item" ResourceConstant="TXT_MANAGE_PHOTOS"
                                    Href="/Applications/MemberProfile/MemberPhotoEdit.aspx" />
                            </asp:PlaceHolder>
                        </div>
                    </div>
                    <div class="activity clearfix">
                        &nbsp;</div>
                </asp:PlaceHolder>
            </div>
<%--    </div>--%>
    </div>
</asp:PlaceHolder>
<!--END Activity Center-->
