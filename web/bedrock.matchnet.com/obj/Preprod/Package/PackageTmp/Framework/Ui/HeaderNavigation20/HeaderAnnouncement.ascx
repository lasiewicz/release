﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HeaderAnnouncement.ascx.cs" Inherits="Matchnet.Web.Framework.Ui.HeaderNavigation20.HeaderAnnouncement" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn" TagName="AdUnit" Src="/Framework/UI/Advertising/AdUnit.ascx" %>

<asp:PlaceHolder ID="phAnnouncement" runat="server" Visible="false">
<script type="text/javascript">
    
    var showAnnouncementMessage = true; 
    
    $j("#nav ul").hover(
        function (e) {
            if(showAnnouncementMessage == true){
                $j("div.announcement").css("display","none");}
        }, 
        function (e) {
            if(showAnnouncementMessage == true){
                $j("div.announcement").css("display","block");}
            }
        );

    function closeAnnouncement() {
      try {
          //hide announcement area
          $j("div.announcement").css("display", "none");

          //ajax call to update session to not show announcement next time
          var url = "/Applications/API/SessionUtilAPI.aspx?random=" + randomNumber();
          executeFireAndForgetUrl(url + "&HideAnnouncement=1&ajxmode=true", null);

          showAnnouncementMessage = false;
      }
      catch (e) {
          if (isAjaxVerboseMode) alert(e.description);
      } 
      return false;
    }
</script>
<div id="announcement">
    <%--message text--%>
    <mn:Txt ID="literalAnnouncement" runat="server" ExpandImageTokens="true"/>
</div>
</asp:PlaceHolder>
<asp:PlaceHolder ID="phSubNowAd" runat="server" Visible="false">
<div id="subNowAd" class="hide ribbon-fold-right">
    <mn:AdUnit id="AdUnitGAMSubnowRight" runat="server" expandImageTokens="true" Size="Square" GAMAdSlot="subnow_right_320x95" GAMPageMode="Auto" Disabled="true" />
</div>
</asp:PlaceHolder>
