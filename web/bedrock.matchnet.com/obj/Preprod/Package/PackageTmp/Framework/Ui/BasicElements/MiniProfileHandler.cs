﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.List.ServiceAdapters;
using Matchnet.List.ValueObjects;
using Matchnet.Lib.Util;
using Matchnet.Content.ServiceAdapters.Links;
using Matchnet.Member.ServiceAdapters.Interfaces;
using Matchnet.Member.ValueObjects.Interfaces;
using Matchnet.Web.Framework.Managers;

namespace Matchnet.Web.Framework.Ui.BasicElements

{
    #region DisplayContextType enum

    public enum DisplayContextType
    {
        Default, HotList, SearchResult, MOL,YNMList
    }

    #endregion
    public class MiniProfileHandler
    {
        IMiniProfile _miniProfile;

       

        public MiniProfileHandler(IMiniProfile profileControl)
        {
            _miniProfile = profileControl;
        }

        public void SetMemberDetails(ContextGlobal g, int maxStringLength)
        {
            //gender
            _miniProfile.SearchResult.SetGenderMask(g.Brand.Site.Community.CommunityID, _miniProfile.TXTGender);

            //age
            DateTime birthDate = _miniProfile.Member.GetAttributeDate(g.Brand, "BirthDate", DateTime.MinValue);
            _miniProfile.TXTAge.Text = FrameworkGlobals.GetAgeCaption(birthDate, g, _miniProfile.ThisControl, "AGE_UNSPECIFIED");

            _miniProfile.SearchResult.SetRegion(g.Brand.Site.LanguageID, _miniProfile.TXTRegion, maxStringLength);

            //headline

            _miniProfile.SearchResult.SetHeadline(_miniProfile.TXTHeadline, g.Session.GetInt(WebConstants.SESSION_PROPERTY_NAME_MEMBERID), 60, 20);
        }

        public void SetMemberPhotoAndName(ContextGlobal g)
        {
            string ViewProfileUrl;
            if (_miniProfile.IsHighlighted)
            {
                ViewProfileUrl = BreadCrumbHelper.MakeViewProfileLink(_miniProfile.MyEntryPoint, _miniProfile.MemberID, _miniProfile.Ordinal, _miniProfile.MyMOCollection, (int)_miniProfile.HotListCategory, false, (int)BreadCrumbHelper.PremiumEntryPoint.Highlight);
            }
            else if (_miniProfile.IsSpotlight)
            {
                ViewProfileUrl = BreadCrumbHelper.MakeViewProfileLink(_miniProfile.MyEntryPoint, _miniProfile.MemberID, _miniProfile.Ordinal, _miniProfile.MyMOCollection, (int)_miniProfile.HotListCategory, false, (int)BreadCrumbHelper.PremiumEntryPoint.Spotlight);
            }
            else
            {
                ViewProfileUrl = BreadCrumbHelper.MakeViewProfileLink(_miniProfile.MyEntryPoint, _miniProfile.MemberID, _miniProfile.Ordinal, _miniProfile.MyMOCollection, (int)_miniProfile.HotListCategory, false, (int)BreadCrumbHelper.PremiumEntryPoint.NoPremium);
                //ViewProfileUrl = BreadCrumbHelper.MakeViewProfileLink(MyEntryPoint, MemberID, Ordinal, MyMOCollection, (int)_hotListCategory);
            }
            _miniProfile.LNKUserName.NavigateUrl = ViewProfileUrl;
            _miniProfile.LNKUserName.Attributes.Add("onmouseover", "this.title='" + _miniProfile.Member.GetUserName(g.Brand) + "'");
            _miniProfile.LNKUserName.Text = FrameworkGlobals.Ellipsis(_miniProfile.Member.GetUserName(g.Brand), 17);
            // TPJ-94
            _miniProfile.IMGIsNew.Visible = _miniProfile.Member.IsNewMember(g.Brand.Site.Community.CommunityID);
            _miniProfile.IMGIsUpdate.Visible = !_miniProfile.IMGIsNew.Visible && _miniProfile.Member.IsUpdatedMember(g.Brand.Site.Community.CommunityID);

            _miniProfile.LITContainer.Text = "<div class=\"results list-view\" style=\"z-index: " + Convert.ToString(100 - _miniProfile.Counter) + ";\">";
            if (g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.JDateCoIL)
            {
                _miniProfile.IMGPremiumAuthenticated.Visible = FrameworkGlobals.IsMemberPremiumAuthenticated(_miniProfile.Member.MemberID, g.Brand);
            }
            else
            {
                _miniProfile.IMGPremiumAuthenticated.Visible = false;
            }

            //if (FrameworkGlobals.memberHighlighted(_Member.MemberID, g.Brand))     
            if (_miniProfile.IsHighlighted)
            {
                _miniProfile.LITContainer.Text = "<div class=\"results list-view highlighted highlightedProfile\" style=\"z-index: " + Convert.ToString(100 - _miniProfile.Counter) + ";\">";
                //boxContainer.Attributes.Remove("class");                
                //boxContainer.Attributes.Add("class", "box highlightedProfile");

                _miniProfile.UCHighlightProfileInfoDisplay.MemberHighlighted = true;
                _miniProfile.UCHighlightProfileInfoDisplay.ViewedMemberID = _miniProfile.Member.MemberID;
            }
            else
            {
                _miniProfile.UCHighlightProfileInfoDisplay.Visible = false;
            }
            _miniProfile.ViewProfileURL = ViewProfileUrl;

        }

        public void SetLoginUpdateDate(ContextGlobal g)
        {
            string resourceValue = "";
            DateTime dateValue = DateTime.MinValue;

            switch (_miniProfile.DisplayContext)
            {
                case Matchnet.Web.Framework.Ui.BasicElements.ResultContextType.HotList:

                    HotListType hotListType;
                    HotListDirection hotListDirection;

                    ListInternal.MapListTypeDirection(_miniProfile.HotListCategory, out hotListType, out hotListDirection);

                    ListItemDetail listItemDetail = g.List.GetListItemDetail(_miniProfile.HotListCategory,
                        g.Brand.Site.Community.CommunityID,
                        g.Brand.Site.SiteID,
                        _miniProfile.Member.MemberID);

                    try
                    {
                        dateValue = listItemDetail.ActionDate;
                    }
                    catch
                    {
                        dateValue = DateTime.Now;
                    }

                    switch (_miniProfile.HotListCategory)
                    {
                        case HotListCategory.WhosOnline:
                            resourceValue = g.GetResource("LOGGED_IN", _miniProfile.ThisControl);
                            dateValue = _miniProfile.Member.GetLastLogonDate(g.Brand.Site.Community.CommunityID);
                            break;

                        case HotListCategory.WhoViewedYourProfile:
                        case HotListCategory.MembersYouViewed:
                            resourceValue = g.GetResource("VIEWED_ON", _miniProfile.ThisControl);
                            break;

                        case HotListCategory.WhoEmailedYou:
                        case HotListCategory.WhoIMedYou:
                        case HotListCategory.MembersYouIMed:
                        case HotListCategory.MembersYouEmailed:
                            resourceValue = g.GetResource("DATE_SENT", _miniProfile.ThisControl);
                            break;

                        case HotListCategory.WhoAddedYouToTheirFavorites:
                            resourceValue = g.GetResource("ADDED_ON", _miniProfile.ThisControl);
                            break;

                        case HotListCategory.WhoTeasedYou:
                        case HotListCategory.MembersYouTeased:
                            resourceValue = g.GetResource("TEASE_SENT__519989", _miniProfile.ThisControl);
                            break;

                        default:
                            resourceValue = g.GetResource("LAST_CHANGED", _miniProfile.ThisControl);
                            break;
                    }
                    break;

                default:
                    resourceValue = g.GetResource("LOGGED_IN", _miniProfile.ThisControl);
                    dateValue = _miniProfile.Member.GetLastLogonDate(g.Brand.Site.Community.CommunityID);
                    break;
            }

            _miniProfile.LastLoginDate.LastLoginDateLabel = resourceValue;
            _miniProfile.LastLoginDate.LastLoginDateValue = dateValue;
        }

        public void SetThumb(Framework.Ui.PageElements.NoPhoto nophoto)
        {
            _miniProfile.SearchResult.SetThumb(_miniProfile.IMGThumb, nophoto, _miniProfile.ViewProfileURL);
        }

        public void ShowNonHotListMember()
        {
            _miniProfile.TXTFavoriteNote.Visible = false;
            _miniProfile.BTNSave.Visible = false;
        }

        public void ShowHotListMember(ContextGlobal g)
        {

            if (_miniProfile.HotListCategory == HotListCategory.Default || _miniProfile.HotListCategory > 0)
            {
                _miniProfile.PNLComments.Visible = true;
                _miniProfile.PLCBreaks.Visible = true;
                _miniProfile.TXTFavoriteNote.Visible = true;
                _miniProfile.BTNSave.Visible = true;

                _miniProfile.PNLComments.Attributes["style"] = "z-index:" + (50 - _miniProfile.Counter).ToString() + ";";
                _miniProfile.TXTFavoriteNote.Attributes["style"] = "z-index:" + (50 - _miniProfile.Counter).ToString() + ";";

                ListItemDetail listItemDetail = g.List.GetListItemDetail(_miniProfile.HotListCategory,
                    g.Brand.Site.Community.CommunityID,
                    g.Brand.Site.SiteID,
                    _miniProfile.Member.MemberID);

                if (listItemDetail.Comment != _miniProfile.DefaultNote && (listItemDetail.Comment != null && listItemDetail.Comment != ""))
                {
                    _miniProfile.TXTFavoriteNote.Value = StringUtil.MaxWordLength(listItemDetail.Comment, 35);
                }
                else
                {
                    string strJavaScript = "if(this.value=='" + _miniProfile.DefaultNote + "')this.value='';";

                    _miniProfile.TXTFavoriteNote.Value = _miniProfile.DefaultNote;

                    _miniProfile.TXTFavoriteNote.Attributes.Remove("onFocus");
                    _miniProfile.TXTFavoriteNote.Attributes.Add("onFocus", strJavaScript);
                }
            }

            if (_miniProfile.HotListCategory != HotListCategory.MutualYes &&
                _miniProfile.HotListCategory != HotListCategory.MembersClickedNo &&
                _miniProfile.HotListCategory != HotListCategory.MembersClickedYes &&
                _miniProfile.HotListCategory != HotListCategory.MembersClickedMaybe &&
                _miniProfile.HotListCategory != HotListCategory.YourMatches &&
                _miniProfile.HotListCategory != HotListCategory.IgnoreList)
            {
                _miniProfile.LBRemoveProfile.Visible = true;
                _miniProfile.LBRemoveProfile.Text = g.GetResource("REMOVE_THIS_PROFILE", _miniProfile.ThisControl);
            }

            // When in any of the hot list categories page, a Delete from list displays.
            // When this displays, do not display Learn about Highlighted Profiles.
            _miniProfile.UCHighlightProfileInfoDisplay.Visible = false;
        }

        public void SetThumb(Framework.Ui.PageElements.NoPhoto20 noPhoto, Matchnet.Content.ValueObjects.BrandConfig.Brand brand )
        {
            //MemberPhotoHelper photoHelper = new MemberPhotoHelper(RuntimeSettings.Instance);
            noPhoto.NoPhotoFileName = MemberPhotoDisplayManager.Instance.GetNoPhotoFile(NoPhotoImageType.BackgroundThumb, _miniProfile.SearchResult._Member, brand);
            noPhoto.PrivatePhotoFileName =
                MemberPhotoDisplayManager.Instance.GetPrivatePhotoFile(PrivatePhotoImageType.BackgroundThumb,
                                                                       _miniProfile.SearchResult._Member, brand);
            _miniProfile.SearchResult.SetThumb(_miniProfile.IMGThumb, noPhoto, _miniProfile.ViewProfileURL);
        }

        public void SetLinks()
        {
            _miniProfile.TXTMore.Href = _miniProfile.ViewProfileURL;
        }

        public void SetOnlineLink()
        {
            bool isOnline;
            OnlineLinkHelper.SetOnlineLink(_miniProfile.DisplayContext, _miniProfile.SearchResult._Member, _miniProfile.Member, _miniProfile.IMGChat, _miniProfile.LNKOnline, LinkParent.MiniProfile, out isOnline);
            if (!isOnline)
            {
                _miniProfile.TXTOnline.ResourceConstant = "SPACES";
                _miniProfile.CurrentlyOnline = false;
            }
            else
                _miniProfile.CurrentlyOnline = true;
        }
        public void SetOnlineLink(string imgonline, string imgoffline)
        {
            bool isOnline;
            OnlineLinkHelper.SetOnlineLink(_miniProfile.DisplayContext, _miniProfile.SearchResult._Member, _miniProfile.Member, _miniProfile.IMGChat, _miniProfile.LNKOnline, LinkParent.MiniProfile,imgonline,imgoffline, out isOnline);
            if (!isOnline)
            {
                _miniProfile.TXTOnline.ResourceConstant = "SPACES";
                _miniProfile.CurrentlyOnline = false;
            }
            else
                _miniProfile.CurrentlyOnline = true;

        }

        public void ToggleDisplayType(ContextGlobal g)
        {
            _miniProfile.BTNSave.Visible = false;
            _miniProfile.TXTFavoriteNote.Visible = false;

            if (g.Member != null)
            {
                _miniProfile.IsHotListFriend = g.List.IsHotListed(HotListCategory.Default,
                    g.Brand.Site.Community.CommunityID,
                    _miniProfile.MemberID);
            }

            switch (_miniProfile.DisplayContext)
            {
                case Matchnet.Web.Framework.Ui.BasicElements.ResultContextType.MembersOnline:
                    _miniProfile.LastLoginDate.LastLoginDateLabel = g.GetResource("TXT_ONLINE_NOW", _miniProfile.ThisControl);
                    break;
                case Matchnet.Web.Framework.Ui.BasicElements.ResultContextType.HotList:
                    SetLoginUpdateDate(g);
                    _miniProfile.LastLoginDate.ShowCurrentlyOnline = false;
                    break;
                default:
                    // TT16550 - set the online label appropriately if the user is in fact online
                    if (_miniProfile.CurrentlyOnline)
                    {
                        _miniProfile.LastLoginDate.LastLoginDateLabel = g.GetResource("TXT_ONLINE_NOW", this);
                    }
                    else
                    {
                        //lastLoginDate.Visible = false;
                        SetLoginUpdateDate(g);
                    }
                    ShowNonHotListMember();
                    break;
            }
        }

        public void SetYNMControls(ContextGlobal g)
        {
            ClickMask clickMask = g.List.GetClickMask(g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, _miniProfile.Member.MemberID);

            _miniProfile.YNMVOTEStatus.Value = ((int)clickMask).ToString();

            if ((clickMask & ClickMask.MemberYes) == ClickMask.MemberYes)
            {
                _miniProfile.IMGYes.FileName = _miniProfile.YesFileNameSelected;
            }
            else if ((clickMask & ClickMask.MemberNo) == ClickMask.MemberNo)
            {
                _miniProfile.IMGNo.FileName = _miniProfile.NoFileNameSelected;
            }
            else if ((clickMask & ClickMask.MemberMaybe) == ClickMask.MemberMaybe)
            {
                _miniProfile.IMGMaybe.FileName = _miniProfile.MaybeFileNameSelected;
            }

            if ((clickMask & ClickMask.MemberYes) == ClickMask.MemberYes && (clickMask & ClickMask.TargetMemberYes) == ClickMask.TargetMemberYes)
            {
                if(g.IsYNMEnabled)
                {
                    _miniProfile.IMGBothSaidYes.Visible = true;    
                }
            }

            YNMVoteParameters parameters = new YNMVoteParameters();

            parameters.SiteID = g.Brand.Site.SiteID;
            parameters.CommunityID = g.Brand.Site.Community.CommunityID;
            parameters.FromMemberID = g.Session.GetInt(WebConstants.SESSION_PROPERTY_NAME_MEMBERID);
            parameters.ToMemberID = _miniProfile.Member.MemberID;

            string encodedParameters = parameters.EncodeParams();

            _miniProfile.IMGYes.NavigateUrl = string.Format("javascript:YNMVote('{0}','1','{1}');",
             _miniProfile.YNMVOTEStatus.Parent.ClientID, encodedParameters);
            _miniProfile.IMGYes.Attributes.Add("onMouseOver", string.Format("YNMMouseOver(this, '{0}', '1')", _miniProfile.YNMVOTEStatus.Parent.ClientID));
            _miniProfile.IMGYes.Attributes.Add("onMouseOut", string.Format("YNMMouseOut(this, '{0}', '1')", _miniProfile.YNMVOTEStatus.Parent.ClientID));

            _miniProfile.IMGNo.NavigateUrl = string.Format("javascript:YNMVote('{0}','2','{1}');",
                _miniProfile.YNMVOTEStatus.Parent.ClientID, encodedParameters);
            _miniProfile.IMGNo.Attributes.Add("onMouseOver", string.Format("YNMMouseOver(this, '{0}', '2')", _miniProfile.YNMVOTEStatus.Parent.ClientID));
            _miniProfile.IMGNo.Attributes.Add("onMouseOut", string.Format("YNMMouseOut(this, '{0}', '2')", _miniProfile.YNMVOTEStatus.Parent.ClientID));

            _miniProfile.IMGMaybe.NavigateUrl = string.Format("javascript:YNMVote('{0}','3','{1}');",
              _miniProfile.YNMVOTEStatus.Parent.ClientID, encodedParameters);
            _miniProfile.IMGMaybe.Attributes.Add("onMouseOver", string.Format("YNMMouseOver(this, '{0}', '4')", _miniProfile.YNMVOTEStatus.Parent.ClientID));
            _miniProfile.IMGMaybe.Attributes.Add("onMouseOut", string.Format("YNMMouseOut(this, '{0}', '4')", _miniProfile.YNMVOTEStatus.Parent.ClientID));

            _miniProfile.TXTYes.Href = _miniProfile.IMGYes.NavigateUrl;
            _miniProfile.TXTYes.Attributes.Add("onmouseover", string.Format("document." + WebConstants.HTML_FORM_ID + ".{0}.onmouseover();", _miniProfile.IMGYes.ClientID));
            _miniProfile.TXTYes.Attributes.Add("onmouseout", string.Format("document." + WebConstants.HTML_FORM_ID + ".{0}.onmouseout();", _miniProfile.IMGYes.ClientID));

            _miniProfile.TXTNo.Href = _miniProfile.IMGNo.NavigateUrl;
            _miniProfile.TXTNo.Attributes.Add("onmouseover", string.Format("document." + WebConstants.HTML_FORM_ID + ".{0}.onmouseover();", _miniProfile.IMGNo.ClientID));
            _miniProfile.TXTNo.Attributes.Add("onmouseout", string.Format("document." + WebConstants.HTML_FORM_ID + ".{0}.onmouseout();", _miniProfile.IMGNo.ClientID));

            _miniProfile.TXTMaybe.Href = _miniProfile.IMGMaybe.NavigateUrl;
            _miniProfile.TXTMaybe.Attributes.Add("onmouseover", string.Format("document." + WebConstants.HTML_FORM_ID + ".{0}.onmouseover();", _miniProfile.IMGMaybe.ClientID));
            _miniProfile.TXTMaybe.Attributes.Add("onmouseout", string.Format("document." + WebConstants.HTML_FORM_ID + ".{0}.onmouseout();", _miniProfile.IMGMaybe.ClientID));

            if (_miniProfile.YNMBackgroundAnimation != null)
            {
                _miniProfile.YNMBackgroundAnimation.Attributes.Add("onmouseover", "this.style.backgroundImage='URL(" + Framework.Image.GetURLFromFilename("bknd_vote_anim.gif") + ")';");
                _miniProfile.YNMBackgroundAnimation.Attributes.Add("onmouseout", "this.style.backgroundImage='URL(" + Framework.Image.GetURLFromFilename("bknd_vote.gif") + ")';");
            }
            if (g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.JDateCoIL)
            {
                _miniProfile.IMGPremiumAuthenticated.Visible = FrameworkGlobals.IsMemberPremiumAuthenticated(_miniProfile.Member.MemberID, g.Brand);
            }

            //if (FrameworkGlobals.memberHighlighted(_Member.MemberID, g.Brand))     
            
        }

   

    }



    public class SaveNoteEventArgs : EventArgs
    {
        public IMemberDTO Member;
        public string Note;
    }
}
