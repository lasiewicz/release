﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Matchnet.Web.Framework.Ui.FormElements
{
    public partial class WatermarkTextbox : FrameworkControl
    {
        private string _WatermarkText = "";
        private string _WatermarkTextResourceConstant = "";
        private string _WatermarkCssClass = "";
        private string _CssClass = "";

        #region Properties
        /// <summary>
        /// The resource constant of text to display when textbox is empty
        /// </summary>
        public string WatermarkTextResourceConstant
        {
            get { return _WatermarkTextResourceConstant; }
            set { _WatermarkTextResourceConstant = value; }
        }

        /// <summary>
        /// The CSS class used when watermark text is displayed
        /// </summary>
        public string WatermarkCssClass
        {
            get { return _WatermarkCssClass; }
            set { _WatermarkCssClass = value; }
        }

        /// <summary>
        /// The text to display when textbox is empty
        /// </summary>
        public string WatermarkText
        {
            get { return _WatermarkText; }
            set { _WatermarkText = value; }
        }

        public string CssClass
        {
            get { return _CssClass; }
            set { _CssClass = value; }
        }

        public int MaxLength
        {
            get { return WatermarkTextBox1.MaxLength; }
            set { WatermarkTextBox1.MaxLength = value; }
        }

        public TextBox TextBox
        {
            get { return WatermarkTextBox1; }
        }

        public string Text
        {
            get { return WatermarkTextBox1.Text; }
            set { WatermarkTextBox1.Text = value; }
        }

        public TextBoxMode TextMode
        {
            get { return WatermarkTextBox1.TextMode; }
            set { WatermarkTextBox1.TextMode = value; }
        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected override void OnPreRender(EventArgs e)
        {
            if (WatermarkTextBox1.Text.Trim() == "" || (GetWatermarkText() == WatermarkTextBox1.Text.Trim()))
            {
                if (!String.IsNullOrEmpty(_WatermarkTextResourceConstant))
                {
                    _WatermarkText = g.GetResource(_WatermarkTextResourceConstant, this);
                }

                if (!String.IsNullOrEmpty(WatermarkCssClass))
                {
                    WatermarkTextBox1.CssClass = CssClass + " " + WatermarkCssClass;
                }
                else if (!String.IsNullOrEmpty(CssClass))
                {
                    WatermarkTextBox1.CssClass = CssClass;
                }
            }
            else
            {
                if (!String.IsNullOrEmpty(CssClass))
                {
                    WatermarkTextBox1.CssClass = CssClass;
                }
            }

            base.OnPreRender(e);
        }

        private string GetWatermarkText()
        {
            if (!String.IsNullOrEmpty(_WatermarkTextResourceConstant))
            {
                return g.GetResource(_WatermarkTextResourceConstant, this);
            }
            else
            {
                return _WatermarkText;
            }
        }
    }
}