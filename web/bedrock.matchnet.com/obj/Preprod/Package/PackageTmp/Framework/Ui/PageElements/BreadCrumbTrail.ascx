<%@ Control Language="c#" AutoEventWireup="false" Codebehind="BreadCrumbTrail.ascx.cs" Inherits="Matchnet.Web.Framework.Ui.PageElements.BreadCrumbTrail" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<!-- begin breadCrumb nav links -->
<asp:Placeholder Runat="server" id="pnlBreadNav" >
<div id=divBreadNav runat="server" style="z-index: -100">
<asp:Label id=lblNav runat="server" cssclass="breadCrumbUpper">

			
			<asp:HyperLink runat="server" ID="lnkNav3" Visible="False" cssclass="Breadhome" /><mn:Txt ID="Txt4" runat="server" ResourceConstant="TXT_DIRECTION" />
			<asp:Literal Runat="server" ID="ltlArrow3" Visible="False">&nbsp;&#62; </asp:Literal> 
			<asp:HyperLink runat="server" ID="lnkNav2" cssclass="breadCrumbPagesLink" Visible="False" /><mn:Txt ID="Txt1" runat="server" ResourceConstant="TXT_DIRECTION" />
			<asp:Literal Runat="server" ID="ltlArrow2" Visible="False">&nbsp;&#62; </asp:Literal> 
			<asp:HyperLink runat="server" ID="lnkNav4" Visible="False" cssclass="Breadhome" /><mn:Txt ID="Txt2" runat="server" ResourceConstant="TXT_DIRECTION" /> 
			<asp:Literal Runat="server" ID="ltlArrow4" Visible="False">&nbsp;&#62; </asp:Literal> 
			<asp:Label Runat="server" ID="lblCurrentPage" Visible="False" />
			<asp:HyperLink Runat="server" ID="lnkNav1" Visible="False" CssClass="breadCrumbPagesLink" /><mn:Txt ID="Txt3" runat="server" ResourceConstant="TXT_DIRECTION" /> 
			<asp:Literal Runat="server" ID="ltlNameArrows" Visible="False">&nbsp;&#62;</asp:Literal>
			
				
			<strong> <asp:Literal Runat="server" ID="ltlName" Visible="False" /> </strong>
			<asp:HyperLink cssclass="breadcrumbpageslink" Runat="server" ID="lnkMore" Visible="False"></asp:HyperLink>
		</asp:Label><!-- begin dynamic page links -->
<asp:Label id=lblMiddlePages Runat="server" cssclass="breadCrumbPagesUpper"></asp:Label>
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;<asp:Label id=lblEndNavigation Runat="server" cssclass="breadCrumbUpperRight"></asp:Label><!-- end dynamic page links/breadCrumb nav links --></div>
</asp:Placeholder> <!-- end subNav -->
