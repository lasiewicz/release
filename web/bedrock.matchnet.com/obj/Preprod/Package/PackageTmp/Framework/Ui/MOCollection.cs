using System;

using Matchnet.MembersOnline.ValueObjects;

namespace Matchnet.Web.Framework.Ui
{
	/// <summary>
	/// Summary description for MOCollection.
	/// </summary>
	public class MOCollection
	{
		#region Members

		private readonly SortFieldType _orderBy;
		private readonly int _genderMask;
		private readonly int _regionID;
		private readonly int _ageMin;
		private readonly int _ageMax;
		private readonly int _languageMask;

		#endregion

		#region Properties

		public SortFieldType OrderBy
		{
			get { return(_orderBy); }
		}

		public int GenderMask
		{
			get { return(_genderMask); }
		}

		public int RegionID
		{
			get { return(_regionID); }
		}

		public int AgeMin
		{
			get { return(_ageMin); }
		}

		public int AgeMax
		{
			get { return(_ageMax); }
		}

		public int LanguageMask
		{
			get { return(_languageMask); }
		}

		#endregion

		#region Constructors

		public MOCollection(string orderBy, int genderMask, int regionID, int ageMin, int ageMax, int languageMask)
		{
			_genderMask = genderMask;
			_regionID = regionID;
			_ageMin = ageMin;
			_ageMax = ageMax;
			_languageMask = languageMask;

			try
			{
				_orderBy = (SortFieldType) Enum.Parse(typeof(SortFieldType), orderBy);
			}
			catch // Throws exception if bad enum type or null value, so use default
			{
				_orderBy = SortFieldType.InsertDate;
			}
		}

		#endregion
	}
}
