﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Web.Analytics;
using Matchnet.Web.Applications.Home;
using System.Text;
using System.Collections.Specialized;
using Matchnet.Configuration.ServiceAdapters.Analitics;
using Matchnet.List.ValueObjects;
using System.Web.UI.HtmlControls;
using Matchnet.Web.Framework.Managers;
using Matchnet.Web.Framework.Util;

namespace Matchnet.Web.Framework.Ui.SearchElements
{
    /// <summary>
    /// This control displays a mini search results horizontally like a bar with page navigation via ajax.
    /// </summary>
    public partial class MiniSearchBar : MiniSearchBase
    {
        public MiniSearchBar()
        {
            DisplayMiniSearchMarketingDiv = false;
            DisplayMiniSearchAjaxLoadingDiv = true;
            DisplayMiniSearchContainingDiv = true;
            PageSize = 16;
        }

        protected string FishEyeItemsDirection
        {
            get { return (g.Brand.Site.Direction == DirectionType.ltr ? "left" : "right"); }
        }

        #region Event Handlers
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            includeNextPrevProfile = true;
            omnitureEnabled = Convert.ToBoolean(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting(
                "ANALYTICS_OMNITURE", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID));
        }

        protected override void OnPreRender(EventArgs e)
        {
            if (displayMiniSearchProfile == false || !this.Visible) return;

            try
            {
                ToggleElementVisibility();
                HandleOmnitureAnalytics();
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }

            base.OnPreRender(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void repeaterProfiles_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.Item && e.Item.ItemType != ListItemType.AlternatingItem) return;

            ProfileHolder profile = e.Item.DataItem as ProfileHolder;

            if (profile == null) return;

            SearchResultProfile searchResult = new SearchResultProfile(profile.Member, profile.Ordinal);

            String viewProfileUrl = BreadCrumbHelper.MakeViewProfileLink(
                profile.MyEntryPoint,
                profile.Member.MemberID,
                profile.Ordinal,
                memberOrderCollection,
                hotListCategoryId,
                false,
                (int)BreadCrumbHelper.PremiumEntryPoint.NoPremium
                );

            viewProfileUrl = AppendProfileURLOptionalParameters(viewProfileUrl);

            if (profile.MatchRating > 0)
            {
                //add match rating to URL to display on profile
                viewProfileUrl = BreadCrumbHelper.AppendParamToProfileLink(viewProfileUrl, WebConstants.URL_PARAMETER_MATCH_RATING + "=" + MatchRatingManager.Instance.EncryptRating(profile.MatchRating, true));
            }

            //set basic info
            Panel panelImgThumb = e.Item.FindControl("panelImgThumb") as Panel;
            Image imgThumb = e.Item.FindControl("imgThumb") as Image;
            PageElements.NoPhoto20 noPhoto = e.Item.FindControl("noPhoto") as PageElements.NoPhoto20;
            //MemberPhotoHelper photoHelper = new MemberPhotoHelper(RuntimeSettings.Instance);
            noPhoto.NoPhotoFileName = MemberPhotoDisplayManager.Instance.GetNoPhotoFile(NoPhotoImageType.TinyThumbV2, true);
            searchResult.SetThumb(imgThumb, noPhoto, viewProfileUrl);
            noPhoto.MemberID = profile.Member.MemberID;
            noPhoto.Mode = PageElements.NoPhoto20.PhotoMode.MiniSearch;
            noPhoto.Username = FrameworkGlobals.Ellipsis(profile.Member.GetUserName(_g.Brand), 8);
            panelImgThumb.Visible = !searchResult.NoPhotoVisible;

            if (profile.Member.MemberID == selectedMemberId)
            {
                panelImgThumb.CssClass = "photos-border-active";
                noPhoto.DivCSSClass = "photos-border-active";
            }
            else
            {
                panelImgThumb.CssClass = "photos-border";
                noPhoto.DivCSSClass = "photos-border";
            }

            //set YNM
            if (g.IsYNMEnabled)
            {
                try
                {
                    ClickMask clickMask = g.List.GetClickMask(g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, profile.Member.MemberID);

                    if ((((clickMask & ClickMask.TargetMemberYes) == ClickMask.TargetMemberYes) && ((clickMask & ClickMask.MemberYes) == ClickMask.MemberYes)))
                    {
                        (e.Item.FindControl("phYY") as PlaceHolder).Visible = true;
                        HtmlGenericControl spanSecretAdmirerYY = e.Item.FindControl("spanSecretAdmirerYY") as HtmlGenericControl;
                        spanSecretAdmirerYY.Attributes["title"] = g.GetResource("YNM_YY", this);
                    }
                    else if ((clickMask & ClickMask.MemberYes) == ClickMask.MemberYes)
                    {
                        (e.Item.FindControl("phY") as PlaceHolder).Visible = true;
                        HtmlGenericControl spanSecretAdmirerY = e.Item.FindControl("spanSecretAdmirerY") as HtmlGenericControl;
                        spanSecretAdmirerY.Attributes["title"] = g.GetResource("YNM_Y", this);
                    }
                    else if ((clickMask & ClickMask.MemberNo) == ClickMask.MemberNo)
                    {
                        (e.Item.FindControl("phN") as PlaceHolder).Visible = true;
                        HtmlGenericControl spanSecretAdmirerN = e.Item.FindControl("spanSecretAdmirerN") as HtmlGenericControl;
                        spanSecretAdmirerN.Attributes["title"] = g.GetResource("YNM_N", this);
                    }
                    else if ((clickMask & ClickMask.MemberMaybe) == ClickMask.MemberMaybe)
                    {
                        (e.Item.FindControl("phM") as PlaceHolder).Visible = true;
                        HtmlGenericControl spanSecretAdmirerM = e.Item.FindControl("spanSecretAdmirerM") as HtmlGenericControl;
                        spanSecretAdmirerM.Attributes["title"] = g.GetResource("YNM_M", this);
                    }

                }
                catch (Exception ex)
                {
                    g.ProcessException(ex);
                }
            }
        }

        #endregion

        #region Public Methods
        public override void LoadMiniSearch()
        {
            try
            {
                //get member id
                int layoutImpersonateMemberId;
                if (g.Member != null)
                {
                    layoutImpersonateMemberId = _g.Member.MemberID;
                }
                else
                {
                    layoutImpersonateMemberId = 9; //visitors
                    isVisitor = true;
                }

                //display profile30 Beta toggler
                if (BetaHelper.GetProfile30BetaTest(g) == BetaHelper.BetaTestType.ProfileRedesign30_DefaultOff
                    || BetaHelper.GetProfile30BetaTest(g) == BetaHelper.BetaTestType.ProfileRedesign30_DefaultOn)
                {
                    phProfile30BetaToggler.Visible = true;
                    StringBuilder profileLink = new StringBuilder("/Applications/MemberProfile/ViewProfile.aspx?");
                    bool first = true;
                    foreach (string key in Request.QueryString.AllKeys)
                    {
                        if (!String.IsNullOrEmpty(Request.QueryString[key]) &&
                            key != WebConstants.URL_PARAMETER_BETA_TEST_ENABLE
                            && key != WebConstants.URL_PARAMETER_BETA_TEST_DISABLE)
                        {
                            if (!first)
                            {
                                profileLink.Append("&");
                            }
                            profileLink.Append(key + "=" + Server.UrlEncode(Request.QueryString[key]));
                            first = false;
                        }
                    }

                    if (BetaHelper.GetProfile30BetaTest(g) == BetaHelper.BetaTestType.ProfileRedesign30_DefaultOn)
                        lnkProfile30BetaToggler.NavigateUrl = BreadCrumbHelper.AppendParamToProfileLink(profileLink.ToString(), WebConstants.URL_PARAMETER_BETA_TEST_ENABLE + "=" + ((int)BetaHelper.BetaTestType.ProfileRedesign30_DefaultOn).ToString());
                    else
                        lnkProfile30BetaToggler.NavigateUrl = BreadCrumbHelper.AppendParamToProfileLink(profileLink.ToString(), WebConstants.URL_PARAMETER_BETA_TEST_DISABLE + "=" + ((int)BetaHelper.BetaTestType.ProfileRedesign30_DefaultOff).ToString());

                    lnkProfile30BetaToggler.ToolTip = g.GetResource("TTL_BETA_TOGGLER_BUTTON", this);
                }

                //determine if mini-search is enabled for member/visitor
                AnaliticsScenarioBase scenario = new AnaliticsScenarioBase(
                    "JDATEREDESIGN_PROFILE",
                    _g.Brand.Site.Community.CommunityID,
                    _g.Brand.Site.SiteID,
                    layoutImpersonateMemberId);

                Scenario scen = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetAnaliticsScenario(scenario);

                if (scen == Scenario.B)
                {
                    displayMiniSearchProfile = true;
                }

                if (displayMiniSearchProfile)
                {
                    //get back to results URL
                    NameValueCollection btrQueryString = new NameValueCollection();
                    backToResultUrl = FrameworkGlobals.GetCookie(WebConstants.SESSION_PROPERTY_NAME_BACKTORESULT_URL, null, true);
                    string btrPath = String.Empty;
                    if (!String.IsNullOrEmpty(backToResultUrl))
                    {
                        btrPath = backToResultUrl;
                        int queryIndex = backToResultUrl.IndexOf("?");
                        if (queryIndex >= 0)
                        {
                            btrQueryString = HttpUtility.ParseQueryString(backToResultUrl.Substring(queryIndex + 1));
                            btrPath = backToResultUrl.Substring(0, queryIndex);
                        }
                    }

                    Int32.TryParse(Request["Ordinal"], out ordinal);
                    Int32.TryParse(Request["HotListCategoryID"], out hotListCategoryId);

                    SetEntryPoints();

                    memberOrderCollection = BreadCrumbHelper.AssembleMOCollection(Request, entryPoint);

                    SetSelectedMemberId();

                    SetStartRow();

                    PopulateMiniSearch();

                    //update back to results URL
                    string btrNewURL = backToResultUrl;
                    StringBuilder param = new StringBuilder();

                    if (premiumEntryPoint == BreadCrumbHelper.PremiumEntryPoint.Spotlight || showNew)
                    {
                        //spotlighted profile coming from anywhere should go back to search page - newest
                        btrNewURL = "~/Applications/Search/SearchResults.aspx?SearchOrderBy=1&StartRow=" + startRow;
                    }
                    if (entryPoint == BreadCrumbHelper.EntryPoint.Bachelor)
                    {
                        btrNewURL = "~/Applications/MembersOnline/MembersOnline.aspx";
                    }
                    else if (!String.IsNullOrEmpty(btrNewURL))
                    {
                        //update values
                        btrQueryString["StartRow"] = startRow.ToString();

                        //build query string
                        foreach (var s in btrQueryString.AllKeys)
                        {
                            param.Append("&" + s + "=" + btrQueryString[s]);
                        }

                        if (!String.IsNullOrEmpty(btrPath))
                        {
                            btrNewURL = btrPath + "?" + param.ToString().Substring(1);
                        }
                        else
                        {
                            btrNewURL = btrNewURL + "?" + param.ToString().Substring(1);
                        }
                    }
                    else
                    {
                        var mmvid = String.Empty;
                        if (Request["mmvid"] != null) mmvid = "?MMVID=" + Request["mmvid"];

                        btrNewURL = "~/Applications/Search/SearchResults.aspx" + mmvid;
                    }

                    //save new url to cookie
                    FrameworkGlobals.SaveBackToResultsURL(g, btrNewURL);

                    //set back to results link
                    lnkBack.NavigateUrl = GetUrl(btrNewURL);

                    //update next/previous profile links
                    try
                    {
                        if (nextMemberID > 0)
                        {
                            string nextProfileLink = BreadCrumbHelper.MakeViewProfileLink(
                                this.entryPoint,
                                nextMemberID,
                                this.ordinal + 1,
                                this.memberOrderCollection,
                                this.hotListCategoryId,
                                false,
                                (int)BreadCrumbHelper.PremiumEntryPoint.NoPremium
                                );

                            nextProfileLink = AppendProfileURLOptionalParameters(nextProfileLink);
                            if (MatchPercentages.ContainsKey(nextMemberID))
                            {
                                int nextProfileMatchRating = MatchPercentages[nextMemberID];
                                if (nextProfileMatchRating > 0)
                                {
                                    //add match rating to URL to display on profile
                                    nextProfileLink = BreadCrumbHelper.AppendParamToProfileLink(nextProfileLink, WebConstants.URL_PARAMETER_MATCH_RATING + "=" + MatchRatingManager.Instance.EncryptRating(nextProfileMatchRating, true));
                                }
                            }

                            if (!String.IsNullOrEmpty(nextProfileLink))
                            {
                                phNextProfile.Visible = true;
                                lnkNextProfile.NavigateUrl = nextProfileLink;
                            }
                        }

                        if (prevMemberID > 0)
                        {
                            string prevProfileLink = BreadCrumbHelper.MakeViewProfileLink(
                                this.entryPoint,
                                prevMemberID,
                                this.ordinal - 1,
                                this.memberOrderCollection,
                                this.hotListCategoryId,
                                false,
                                (int)BreadCrumbHelper.PremiumEntryPoint.NoPremium
                                );

                            prevProfileLink = AppendProfileURLOptionalParameters(prevProfileLink);
                            if (MatchPercentages.ContainsKey(nextMemberID))
                            {
                                int prevProfileMatchRating = MatchPercentages[prevMemberID];
                                if (prevProfileMatchRating > 0)
                                {
                                    //add match rating to URL to display on profile
                                    prevProfileLink = BreadCrumbHelper.AppendParamToProfileLink(prevProfileLink, WebConstants.URL_PARAMETER_MATCH_RATING + "=" + MatchRatingManager.Instance.EncryptRating(prevProfileMatchRating, true));
                                }
                            }

                            if (!String.IsNullOrEmpty(prevProfileLink))
                            {
                                phPreviousProfile.Visible = true;
                                lnkPreviousProfile.NavigateUrl = prevProfileLink;
                            }
                        }

                        if (g.Brand.Site.Direction == DirectionType.rtl && !string.IsNullOrEmpty(lnkPreviousProfile.NavigateUrl))
                        {
                            FeatureThrottler ft = new FeatureThrottler(ThrottleMode.LastDigit, "ENABLE_PROFILES_ARROWS_NAV_LASTDIGITS",
                                                             g.Brand, g.Member);
                            if (!ft.IsFeatureEnabled())
                            {
                                string tmp = lnkNextProfile.NavigateUrl;
                                lnkNextProfile.NavigateUrl = lnkPreviousProfile.NavigateUrl;
                                lnkPreviousProfile.NavigateUrl = tmp;
                            }
                            else
                            {
                                lnkNextProfile.NavigateUrl += "&arrowsnav=an";
                                lnkPreviousProfile.NavigateUrl += "&arrowsnav=ap";
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        g.ProcessException(ex);
                    }

                    //update next/previous page links
                    bool displayPrev = false;
                    if (startRow > PageSize)
                    {
                        phPreviousPage.Visible = true;
                        displayPrev = true;

                        if (!isVisitor || entryPoint == BreadCrumbHelper.EntryPoint.Bachelor)
                        {
                            //build query string
                            param = new StringBuilder();
                            foreach (string s in Request.QueryString.AllKeys)
                            {
                                if (!string.IsNullOrEmpty(s))
                                {
                                    string parameter = s.ToLower();
                                    if (parameter == "startrow") continue;

                                    if (parameter == "pg")
                                    {
                                        Int32 pageNumber;

                                        if (Int32.TryParse(Request.QueryString[parameter], out pageNumber) == false || pageNumber < 1) pageNumber = 1;

                                        param.Append("&pg=" + (pageNumber - 1));
                                    }
                                    else
                                    {
                                        param.Append("&" + s + "=" + Request.QueryString[s]);
                                    }
                                }
                            }
                            param.Append("&startrow=" + (startRow - PageSize));

                            tdPrevious.Attributes.Add("onclick", "javascript:GetMiniSearch('" + param.ToString().Substring(1) + "', 'prev'," + isVisitor.ToString().ToLower() + ",true);return false;");

                        }
                        else
                        {

                            string vURL = g.GetLogonRedirect(VisitorLimitHelper.GetVisitorPagePath(String.Empty), "TXT_LOGIN_OR_REGISTER_TO_CONTINUE_SEARCHING");
                            //this.linkPreviousPage.NavigateUrl = vURL;
                            tdPrevious.Attributes.Add("onclick", "javascript:window.document.location = '" + vURL + "';");
                        }
                    }

                    bool displayNext = false;
                    if (totalCount >= (startRow + PageSize))
                    {
                        phNextPage.Visible = true;
                        displayNext = true;

                        if (!isVisitor || entryPoint == BreadCrumbHelper.EntryPoint.Bachelor)
                        {
                            //build query string
                            param = new StringBuilder();
                            foreach (string s in Request.QueryString.AllKeys)
                            {
                                if (!string.IsNullOrEmpty(s))
                                {
                                    string parameter = s.ToLower();
                                    if (parameter == "startrow") continue;

                                    if (parameter == "pg")
                                    {
                                        Int32 pageNumber;

                                        if (Int32.TryParse(Request.QueryString[parameter], out pageNumber) == false || pageNumber < 1) pageNumber = 1;

                                        param.Append("&pg=" + (pageNumber + 1));
                                    }
                                    else
                                    {
                                        param.Append("&" + s + "=" + Request.QueryString[s]);
                                    }
                                }
                            }
                            param.Append("&startrow=" + (startRow + PageSize));

                            tdNext.Attributes.Add("onclick", "javascript:GetMiniSearch('" + param.ToString().Substring(1) + "', 'next', " + isVisitor.ToString().ToLower() + ",true);return false;");

                        }
                        else
                        {
                            string vURL = g.GetLogonRedirect(VisitorLimitHelper.GetVisitorPagePath(""), "TXT_LOGIN_OR_REGISTER_TO_CONTINUE_SEARCHING");
                            //this.linkNextPage.NavigateUrl = vURL;
                            tdNext.Attributes.Add("onclick", "javascript:window.document.location = '" + vURL + "';");
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                this.Visible = false;
                g.ProcessException(ex);
            }
        }

        #endregion

        #region Private Methods
        private void HandleOmnitureAnalytics()
        {
            if (!omnitureEnabled) return;

            if (_g.AnalyticsOmniture != null)
            {
                _g.AnalyticsOmniture.AddEvent("event24");
            }

            AddClickToPageForOmniture();
        }

        private void AddClickToPageForOmniture()
        {
            if (lnkBack.NavigateUrl.IndexOf("?") > 0)
            {
                lnkBack.NavigateUrl += Omniture.GetOmnitureClickToPageURLParams(
                    "Mini Search", WebConstants.PageIDs.MiniSearch, "Back to Search",
                    WebConstants.Action.None, false);
            }
            else
            {
                lnkBack.NavigateUrl += "?" + Omniture.GetOmnitureClickToPageURLParams(
                                                            "Mini Search", WebConstants.PageIDs.MiniSearch, "Back to Search",
                                                            WebConstants.Action.None, false).Substring(1);
            }
        }

        private void PopulateMiniSearch()
        {
            BindMemberProfiles(GetProfileList());
            UpdateTitle();
        }

        private void BindMemberProfiles(ICollection<ProfileHolder> profileList)
        {
            if (profileList != null)
            {
                if (profileList.Count > 0)
                {
                    repeaterProfiles.DataSource = profileList;
                    repeaterProfiles.DataBind();
                }

                if (profileList.Count <= 10)
                {
                    if (entryPoint != BreadCrumbHelper.EntryPoint.HotLists
                        && entryPoint != BreadCrumbHelper.EntryPoint.MembersOnline)
                    {
                        phLowMessaging.Visible = true;
                    }
                }
            }
            else
            {
                phLowMessaging.Visible = true;
            }
        }

        private void UpdateTitle()
        {
            if (String.IsNullOrEmpty(titleResource))
            {
                literalTitle.Text = FrameworkGlobals.Ellipsis(titleName, 1000, "&hellip;");
                literalLoadingTitle.Text = titleName;
                literalTitle.ResourceConstant = "";
                literalLoadingTitle.ResourceConstant = "";
            }
            else
            {
                literalTitle.Text = FrameworkGlobals.Ellipsis(g.GetResource(titleResource, this), 1000, "&hellip;");
                literalTitle.ResourceConstant = "";
                literalLoadingTitle.ResourceConstant = titleResource;
            }
        }

        private void ToggleElementVisibility()
        {
            this.Visible = displayMiniSearchProfile;
            phAjaxLoading.Visible = DisplayMiniSearchAjaxLoadingDiv;
            phDivMiniSearchClose.Visible = DisplayMiniSearchContainingDiv;
            phDivMiniSearchOpen.Visible = DisplayMiniSearchContainingDiv;

            //fisheye related
            this.phDockDivClose.Visible = this.phDockDivOpen.Visible = this.phFisheyeJS.Visible = IsMiniSearchFisheyeEnabled();
        }

        private string AppendProfileURLOptionalParameters(string viewProfileUrl)
        {
            if (this.entryPoint == BreadCrumbHelper.EntryPoint.ReverseSearchResults)
            {
                Int32 page;
                if (Int32.TryParse(Request["pg"], out page) == false || page < 1) page = 1;

                viewProfileUrl = BreadCrumbHelper.AppendParamToProfileLink(viewProfileUrl, "&pg=" + page);

                if (String.IsNullOrEmpty(Request["sl"]) == false)
                {
                    viewProfileUrl = BreadCrumbHelper.AppendParamToProfileLink(viewProfileUrl, "&sl=" + Request["sl"]);
                }

                if (String.IsNullOrEmpty(Request["op"]) == false)
                {
                    viewProfileUrl = BreadCrumbHelper.AppendParamToProfileLink(viewProfileUrl, "&op=" + Request["op"]);
                }
            }

            if (omnitureEnabled)
            {
                viewProfileUrl = BreadCrumbHelper.AppendParamToProfileLink(viewProfileUrl, "ClickFrom=" + HttpContext.Current.Server.UrlEncode("Profile Views - Mini Search"));
                viewProfileUrl = BreadCrumbHelper.AppendParamToProfileLink(viewProfileUrl, Omniture.GetOmnitureClickToPageURLParams(
                    "Mini Search", WebConstants.PageIDs.MiniSearch,
                    "Mini Profile", WebConstants.Action.None, false));
            }

            return viewProfileUrl;
        }

        protected bool IsMiniSearchFisheyeEnabled()
        {
            //return false;

            string isEnabled = "false";
            try
            {
                isEnabled = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ENABLE_MINI_SEARCH_FISHEYE", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID);
            }
            catch (Exception ex)
            {
                //setting missing
                isEnabled = "false";
            }

            if (isEnabled.ToLower().Trim() == "true")
                return true;
            else
                return false;
        }

        protected string GetFishEyeCSS()
        {
            return IsMiniSearchFisheyeEnabled() ? "fisheye" : "";
        }

        #endregion

        protected string GetArrowsNavCSSClass()
        {
            string result = string.Empty;

            if (g.Member != null)
            {
                FeatureThrottler ft = new FeatureThrottler(ThrottleMode.LastDigit, "ENABLE_PROFILES_ARROWS_NAV_LASTDIGITS",
                                                             g.Brand, g.Member);
                if (ft.IsFeatureEnabled())
                {
                    lnkNextProfile.ToolTip = g.GetResource("TXT_NEXT_PROFILE_ARROW_NAV", this);
                    lnkPreviousProfile.ToolTip = g.GetResource("TXT_PREVIOUS_PROFILE_ARROW_NAV", this);
                    result = " arrows-nav";
                }
            }

            return result;
        }
    }
}
