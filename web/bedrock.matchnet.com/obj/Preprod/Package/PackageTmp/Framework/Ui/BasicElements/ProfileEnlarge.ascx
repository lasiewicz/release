﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProfileEnlarge.ascx.cs" Inherits="Matchnet.Web.Framework.Ui.BasicElements.ProfileEnlarge" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="uc1" TagName="NoPhoto" Src="../PageElements/NoPhoto.ascx" %>

<asp:HyperLink href="#" ID="lnkProfileEnlarge"  Runat="server" onmouseover="TogglePopupDivDisplay('divProfileEnlarge');return false;" onmouseout="TogglePopupDivDisplay('divProfileEnlarge');return false;">
    <mn:image id="imageProfileEnlarge" runat="server" filename="icon_enlarge.gif" visible="true" CssClass="imageAlignInline profileEnlargeIcon" /><strong><mn:Txt runat="server" ID="txtEnlarge" ResourceConstant="TXT_ENLARGE" /></strong>
</asp:HyperLink>	

	<div class="clearFloats" id="divProfileEnlarge" style="display:none;">
		<div id="profileEnlargeArrow">
		    <mn:image id="profileEnlargeArrow" runat="server" filename="profileEnlargeArrow.gif" titleresourceconstant="" />
	    </div>
	    
		<div id="cProfilePhoto" class="profileEnlargeMemberPic">
		    <uc1:NoPhoto runat="server" id="noPhoto" class="noPhoto" visible="False" />
		    <mn:image id="ImageThumb" runat="server"  border="0" CssClass="profileImageHover" />
		</div>
		
		<div class="profileEnlargeMemberInfo">
	    	<p>
		        <strong><asp:literal id="TextUserName" runat="server" /></strong>
		        <mn:image id="Image1" runat="server" filename="icon_newMember.gif" CssClass="imageAlignInline" visible="false" hspace="2" titleresourceconstant="ALT_NEW_MEMBER" />
				<mn:image id="Image2" runat="server" filename="icon_updated.gif" CssClass="imageAlignInline" visible="false" hspace="2" titleresourceconstant="ALT_UPDATED_MEMBER" />
		        <mn:image id="ImageIsNew" runat="server" filename="icon_newMember.gif" CssClass="imageAlignInline" visible="false" hspace="2" titleresourceconstant="ALT_NEW_MEMBER" />
		        <mn:image id="ImageIsUpdate" runat="server" filename="icon_updated.gif" CssClass="imageAlignInline" visible="false" hspace="2" titleresourceconstant="ALT_UPDATED_MEMBER" />
		    </p>
		    <ul>
			    <li><asp:literal id="TextGender" runat="server" /></li>
			    <li><mn:txt id="lblRelationship" runat="server" ResourceConstant="PRO_FOR" /> <asp:literal id="TextRelationship" runat="server" /></li>
			    <li><mn:txt id="lblFromLocation" runat="server" ResourceConstant="PRO_FROM" /> <asp:literal id="TextRegion" runat="server" /></li>
			    <li><mn:txt id="lblAge" runat="server" ResourceConstant="PRO_AGE"></mn:txt> <asp:literal id="TextAge" runat="server" />&nbsp;<mn:txt id="AgeDisplay" runat="server" ResourceConstant="TXT_YEARS_OLD" /></li>
			</ul>
		    <p>
		        <mn:image runat="server" id="bothLike" FileName="icon_bothLike.gif" CssClass="imageAlignInline" runat="server"/> <strong><mn:txt id="TextBothLike" runat="server" ResourceConstant="TXT_BOTH_LIKE" /></strong> <asp:literal id="TextHeadline" runat="server" />
            </p>
		    <ul id="profileEnlargeMemberThumbs">
		        <asp:Repeater ID="rptPhotos" runat="server" >
			        <ItemTemplate>
			            <li><mn:image ID="imgTinyThumb" runat="server" width="80" Height="104"/></li>
			        </ItemTemplate>
		        </asp:Repeater>
		    </ul>
		</div>
						
	</div>
	

