﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Content.ServiceAdapters.Links;
using Matchnet.List.ServiceAdapters;
using Matchnet.List.ValueObjects;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects.Interfaces;
using Matchnet.Member.ValueObjects.Photos;
using Matchnet.Web.Applications.MemberProfile;

using Matchnet.Configuration.ServiceAdapters.Analitics;
using Matchnet.Web.Applications.ColorCode;
using Matchnet.Web.Framework.Managers;
using Matchnet.Web.Framework.Ui.BasicElements;
using Spark.Common.AccessService;
namespace Matchnet.Web.Framework.Ui.ProfileElements
{
    public partial class SpotlightProfile : FrameworkControl
    {
        #region Members
        const int MAX_SPOTLIGHT_PHOTO = 4;
        protected Color _Color = Color.none;
        protected string _ColorText = "";
        private IMemberDTO _Member;
        private ResultContextType _DisplayContext = ResultContextType.SearchResult;
        private string _categoryID = String.Empty;
        private int _memberID;
        const int MAX_STRING_LENGTH = 31;
        bool promotionMemberFlag;
        protected Image ImageList;
        private string offlineImageName;
        private string onlineImageName;
        private BreadCrumbHelper.EntryPoint _entryPoint = BreadCrumbHelper.EntryPoint.SearchResults;
        private BreadCrumbHelper.PremiumEntryPoint _premiumEntryPoint = BreadCrumbHelper.PremiumEntryPoint.Spotlight;

        #endregion

        public SpotlightProfile()
        {

        }

        #region Properties

        public string OfflineImageName
        {
            get { return offlineImageName; }
            set { offlineImageName = value; }
        }

        public string OnlineImageName
        {
            get { return onlineImageName; }
            set { onlineImageName = value; }
        }

        public string CategoryID
        {
            get { return (_categoryID); }
            set { _categoryID = value; }
        }

        public int Counter { get; set; }

        public IMemberDTO Member
        {
            get { return _Member; }
            set
            {
                _Member = value;
                _memberID = _Member.MemberID;
            }
        }

        public bool IsPromotionalMember
        {
            get { return (this.promotionMemberFlag); }
            set { this.promotionMemberFlag = value; }
        }

        public String ViewProfileURL { get; set; }

        public ResultContextType DisplayContext
        {
            set { _DisplayContext = value;}
            get { return _DisplayContext;}
        }

        public BreadCrumbHelper.EntryPoint EntryPoint
        {
            get { return _entryPoint; }
            set { _entryPoint = value; }
        }

        #endregion

        #region Event Handlers
        private void Page_Load(object sender, System.EventArgs e)
        {
            if (_Member == null || g.Member == null)
            {
                this.Visible = false;
                return;
            }
            try
            {
                this.ViewProfileURL = BreadCrumbHelper.MakeViewProfileLink(_entryPoint, _Member.MemberID, 0, null, 0, false, (int)_premiumEntryPoint);
                SetSpotlightDetails();
                insertPixel();
                
                //color code
                if (ColorCodeHelper.IsColorCodeEnabled(g.Brand) && ColorCodeHelper.HasMemberCompletedQuiz(_Member, g.Brand) && !ColorCodeHelper.IsMemberColorCodeHidden(_Member, g.Brand))
                {
                    phColorCode.Visible = true;
                    _Color = ColorCodeHelper.GetPrimaryColor(_Member, g.Brand);
                    _ColorText = ColorCodeHelper.GetFormattedColorText(_Color);
                }

            }
            catch (Exception ex)
            {
                _g.ProcessException(ex);
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            try
            {
                base.OnPreRender(e);

                if (g.AnalyticsOmniture != null)
                {
                    //Omniture
                    string additionalSource = "";
                    if (_DisplayContext == ResultContextType.HotList)
                    {
                        additionalSource = "List";
                    }
                    else if (_DisplayContext == ResultContextType.MembersOnline)
                    {
                        additionalSource = "List";
                    }

                    lnkFlirt.NavigateUrl = g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.Tease, WebConstants.Action.Flirt, lnkFlirt.NavigateUrl, additionalSource);
                    lnkUserName.NavigateUrl = g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.ViewProfile, WebConstants.Action.ProfileName, lnkUserName.NavigateUrl, additionalSource);
                    ImageThumb.NavigateUrl = g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.ViewProfile, WebConstants.Action.ProfilePhoto, ImageThumb.NavigateUrl, additionalSource);
                    lnkIMOnline.Attributes.Add("onclick", g.AnalyticsOmniture.GetOnClickCustomLinkTracking(WebConstants.Action.IM, g.AnalyticsOmniture.PageName, "", false, false));
                    lnkEcard.NavigateUrl = g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.ECard, WebConstants.Action.ECard, lnkEcard.NavigateUrl, additionalSource);
                    add2List.ActionCallPage = g.AnalyticsOmniture.PageName;
                    add2List.ActionCallPageDetail = "";

                    lnkEmail.NavigateUrl = g.AnalyticsOmniture.GetActionURL(WebConstants.PageIDs.Compose, WebConstants.Action.EmailMeNowButton, lnkEmail.NavigateUrl, additionalSource);
                }
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        protected void rptPhotos_ItemDataBound(Object sender, RepeaterItemEventArgs e)
        {
            try
            {
                //MemberPhotoHelper photoHelper = new MemberPhotoHelper(RuntimeSettings.Instance);
                Photo photo = (Photo)e.Item.DataItem;
                Image img = ((Image)e.Item.FindControl("imgTinyThumb"));
                img.NavigateUrl = this.ViewProfileURL;

                img.ImageUrl = MemberPhotoDisplayManager.Instance.GetNoPhotoFile(NoPhotoImageType.Thumb, _Member, g.Brand);

                if (MemberPhotoDisplayManager.Instance.IsPrivatePhoto(photo, g.Brand))
                    return;

                if (!MemberPhotoDisplayManager.Instance.PhotoIsEmpty(photo, PhotoType.Thumbnail, g.Brand))
                {
                    img.ImageUrl = MemberPhotoDisplayManager.Instance.GetPhotoDisplayURL(g.Member, _Member, g.Brand, photo, PhotoType.Thumbnail,
                                                         PrivatePhotoImageType.Thumb, NoPhotoImageType.Thumb);
                }
                else if (!MemberPhotoDisplayManager.Instance.PhotoIsEmpty(photo, PhotoType.Full, g.Brand))
                {
                    img.ImageUrl = MemberPhotoDisplayManager.Instance.GetPhotoDisplayURL(g.Member, _Member, g.Brand, photo, PhotoType.Full,
                                                         PrivatePhotoImageType.Full, NoPhotoImageType.None);
                }
            }
            catch (Exception ex)
            {

            }

        }
        #endregion

        #region Private Methods

        private void insertPixel()
        {
            try
            {
                if (g.PromotionalProfileEnabled() && g.PromotionalProfileMember() == _Member.MemberID)
                {
                    string pixelResx = g.GetResource("PROMOTION_MINIPROFILE_PIXEL");
                    if (!string.IsNullOrEmpty(pixelResx))
                    { plcPixel.Text = pixelResx; }
                }
            }
            catch (Exception ex) { }
        }


        private void SetSpotlightDetails()
        {
            SearchResultProfile searchResult = new SearchResultProfile(_Member, 0);

            bool selfSpotlight = (g.Member.MemberID == _Member.MemberID);
            //plcSpotlightSettings.Visible = selfSpotlight;
            string matches = ProfileDisplayHelper.GetSpotlightMatchString(g, _Member);

            ImageIsNew.Visible = Member.IsNewMember(g.Brand.Site.Community.CommunityID);
            ImageIsUpdate.Visible = !ImageIsNew.Visible && Member.IsUpdatedMember(g.Brand.Site.Community.CommunityID);

            //username
            lnkUserName.NavigateUrl = this.ViewProfileURL;
            string userName = String.IsNullOrEmpty(_Member.GetUserName(g.Brand)) ? _Member.MemberID.ToString() : _Member.GetUserName(g.Brand);
            if (ImageIsNew.Visible)
                lnkUserName.Text = FrameworkGlobals.Ellipsis(userName, 13, "&hellip;");
            else if (ImageIsUpdate.Visible)
                lnkUserName.Text = FrameworkGlobals.Ellipsis(userName, 11, "&hellip;");
            else
                lnkUserName.Text = FrameworkGlobals.Ellipsis(userName, 15, "&hellip;");

            lnkUserName.ToolTip = userName;

            //set you both like info
            if (!String.IsNullOrEmpty(matches) && !selfSpotlight)
            {
                txtBothLike.Visible = true;
                searchResult.SetHeadline(TextHeadline, matches, _memberID, 80, 20);
            }
            else
            {
                searchResult.SetHeadline(TextHeadline, g.Session.GetInt(WebConstants.SESSION_PROPERTY_NAME_MEMBERID), 60, 20);
            }
            txtMore.Text = String.Format(g.GetResource("TXT_READ_MORE", this), this.ViewProfileURL);

            //set age/location
            DateTime birthDate = _Member.GetAttributeDate(g.Brand, "BirthDate", DateTime.MinValue);
            string age = FrameworkGlobals.GetAgeCaption(birthDate, g, this, "AGE_UNSPECIFIED");
            string regionInfo = String.Empty;
            int regionid = _Member.GetAttributeInt(g.Brand, "RegionID");
            regionInfo = FrameworkGlobals.GetRegionString(regionid, g.Brand.Site.LanguageID);
            TextInfo.Text = String.Format("{0}, {1}", age, regionInfo);

            // This must be appended to all photos that hit the file servers in order for the site-specific
            // "No Photo" images to appear.  Appending this to any other photos should not cause errors.
            string siteIDParam = "?" + WebConstants.URL_PARAMETER_NAME_SITEID + "=" + g.Brand.Site.SiteID.ToString();
            bool photoFound = false;
            ImageThumb.NavigateUrl = this.ViewProfileURL;
            //MemberPhotoHelper photoHelper = new MemberPhotoHelper(RuntimeSettings.Instance);

            var photo = MemberPhotoDisplayManager.Instance.GetDefaultPhoto(g.Member, Member, g.Brand);

            if (photo != null)
            {
                ImageThumb.ImageUrl = MemberPhotoDisplayManager.Instance.GetPhotoDisplayURL(g.Member, Member, g.Brand,
                                                                       photo, PhotoType.Thumbnail,
                                                                       PrivatePhotoImageType.Thumb,
                                                                        NoPhotoImageType.Thumb);
                
                //set smaller thumbnails for approvedphotos excluding main photo
                var otherApprovedPhotos = MemberPhotoDisplayManager.Instance.GetApprovedPhotosExcludingMain(g.Member, Member, g.Brand, false);
                if (otherApprovedPhotos.Count > 0)
                {
                    if(otherApprovedPhotos.Count > MAX_SPOTLIGHT_PHOTO)
                    {
                        otherApprovedPhotos.RemoveRange(MAX_SPOTLIGHT_PHOTO, otherApprovedPhotos.Count - MAX_SPOTLIGHT_PHOTO);
                    }

                    rptPhotos.DataSource = otherApprovedPhotos;
                    rptPhotos.DataBind();

                }
            }
            else
            {
                ImageThumb.ImageUrl = MemberPhotoDisplayManager.Instance.GetNoPhotoFile(NoPhotoImageType.Thumb, _Member, g.Brand);
            }
            
            //display spotlight info
            ucSpotlightInfoDisplay1.Spotlight = true;
            ucSpotlightInfoDisplay1.Visible = true;
            ucSpotlightInfoDisplay1.ViewedMemberID = Member.MemberID;
            if (g.Member != null)
            {
                AccessPrivilege apSpotlight = g.Member.GetUnifiedAccessPrivilege(PrivilegeType.SpotlightMember, g.Brand.BrandID, g.Brand.Site.SiteID, g.Brand.Site.Community.CommunityID);
                if (apSpotlight != null && apSpotlight.EndDatePST > DateTime.Now)
                {
                    ucSpotlightInfoDisplay1.Visible = false;
                }
            }

            //set IM
            bool isOnline = false;
            OnlineLinkHelper onlineLinkHelper = OnlineLinkHelper.getOnlineLinkHelper(Matchnet.Web.Framework.Ui.BasicElements.ResultContextType.None, this.Member, g.Member, LinkParent.FullProfile, (int)PurchaseReasonType.AttemptToIMFullProfileM2M, out isOnline);
            if (!isOnline)
            {
                this.lnkIMOnline.Visible = false;
                this.txtIMOffline.Visible = true;
            }
            else
            {
                this.lnkIMOnline.NavigateUrl = onlineLinkHelper.OnlineLink;
                this.lnkIMOnline.Visible = true;
                this.txtIMOffline.Visible = false;
            }

            //set communication links
            lnkEmail.NavigateUrl = ProfileDisplayHelper.GetEmailLink(Member.MemberID) + "&" + WebConstants.URL_PARAMETER_PURCHASEREASONTYPEID + "=999";
            lnkFlirt.NavigateUrl = ProfileDisplayHelper.GetFlirtLink(g, Member.MemberID);
            ProfileDisplayHelper.AddFlirtOnClick(g, ref this.lnkFlirt);
            add2List.MemberID = Member.MemberID;

            // Show hide send ECard link based on setting from database.  (ON or OFF)
            //if (g.EcardsEnabled)
            //{
            //    phEcardLink.Visible = true;

            //    // Generate mingle ecard page url. (to parameter is the username of the reciever)
            //    string destPageUrl = FrameworkGlobals.BuildConnectFrameworkLink("cards/categories.html?to=" + _Member.GetUserName(_g.Brand) + "&return=1", g, true);

            //    // Assign url to ECard link.
            //    lnkEcard.NavigateUrl = destPageUrl;
            //}

            //set YNM
            if (g.IsYNMEnabled)
            {
                try
                {
                    ClickMask clickMask = g.List.GetClickMask(g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, this.Member.MemberID);

                    //Put the clickMask in a hidden input for use by JavaScript (YNMVote)
                    YNMVoteStatus.Value = ((int)clickMask).ToString();

                    if ((((clickMask & ClickMask.TargetMemberYes) == ClickMask.TargetMemberYes) && ((clickMask & ClickMask.MemberYes) == ClickMask.MemberYes)))
                    {
                        spanSecretAdmirerYY.Style["display"] = "inline-block";
                        secretAdmirerPopup_descYY.Style["display"] = "block";
                        spanYes.Attributes["class"] = "spr-btn sbtn-click20-yes-sm-on";
                    }
                    else if ((clickMask & ClickMask.MemberYes) == ClickMask.MemberYes)
                    {
                        spanSecretAdmirerY.Style["display"] = "inline-block";
                        secretAdmirerPopup_descY.Style["display"] = "block";
                        spanYes.Attributes["class"] = "spr-btn sbtn-click20-yes-sm-on";
                    }
                    else if ((clickMask & ClickMask.MemberNo) == ClickMask.MemberNo)
                    {
                        spanSecretAdmirerN.Style["display"] = "inline-block";
                        secretAdmirerPopup_descN.Style["display"] = "block";
                        spanNo.Attributes["class"] = "spr-btn sbtn-click20-no-sm-on";
                    }
                    else if ((clickMask & ClickMask.MemberMaybe) == ClickMask.MemberMaybe)
                    {
                        spanSecretAdmirerM.Style["display"] = "inline-block";
                        secretAdmirerPopup_descM.Style["display"] = "block";
                        spanMaybe.Attributes["class"] = "spr-btn sbtn-click20-maybe-sm-on";
                    }
                    else
                    {
                        secretAdmirerPopup_desc.Style["display"] = "block";
                    }

                    YNMVoteParameters parameters = new YNMVoteParameters();

                    parameters.SiteID = _g.Brand.Site.SiteID;
                    parameters.CommunityID = _g.Brand.Site.Community.CommunityID;
                    parameters.FromMemberID = _g.Member.MemberID;
                    parameters.ToMemberID = this.Member.MemberID;
                    string encodedParameters = parameters.EncodeParams();

                    //Yes Vote button
                    lnkYes.ToolTip = g.GetResource("YNM_Y_IMG_ALT_TEXT", this);
                    lnkYes.NavigateUrl = string.Format("javascript:SpotlightProfile_ProcessYNM('{0}','{1}','{2}');", lnkYes.NamingContainer.ClientID, 1, encodedParameters);

                    //No Vote button
                    lnkNo.ToolTip = g.GetResource("YNM_N_IMG_ALT_TEXT", this);
                    lnkNo.NavigateUrl = string.Format("javascript:SpotlightProfile_ProcessYNM('{0}','{1}','{2}');", lnkNo.NamingContainer.ClientID, 2, encodedParameters);

                    //Maybe Vote button
                    lnkMaybe.ToolTip = g.GetResource("YNM_M_IMG_ALT_TEXT", this);
                    lnkMaybe.NavigateUrl = string.Format("javascript:SpotlightProfile_ProcessYNM('{0}','{1}','{2}');", lnkMaybe.NamingContainer.ClientID, 3, encodedParameters);

                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine(ex.Message);

                    //error here is likely in GetClickMask, because there is no member (e.g. Visitor)
                    //this.divMutualYes.Style.Add("display", "none");
                    YNMVoteStatus.Value = Convert.ToInt32(ClickMask.None).ToString();
                }


            }
            else
            {
                phSecretAdmirer.Visible = false;
            }
        }

        #endregion

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
            rptPhotos.ItemDataBound += new RepeaterItemEventHandler(rptPhotos_ItemDataBound);
        }
        #endregion

    }

}
