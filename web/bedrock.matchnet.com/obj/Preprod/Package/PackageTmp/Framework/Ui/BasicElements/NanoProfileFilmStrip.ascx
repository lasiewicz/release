﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NanoProfileFilmStrip.ascx.cs" Inherits="Matchnet.Web.Framework.Ui.BasicElements.NanoProfileFilmStrip" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mnl" Namespace="Matchnet.Web.Framework.Ui.BasicElements.Links" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mnbe" Namespace="Matchnet.Web.Framework.Ui.BasicElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="uc1" TagName="FilmStripNanoProfile" Src="FilmStripNanoProfile.ascx" %>

<asp:PlaceHolder ID="phHasProfiles" runat="server">
    
    <button class="filmstrip-prev disabled">&#9668;</button>
    <button class="filmstrip-next">&#9658;</button>
    <div id="nano-filmstrip-cont">
        <div class="ajax-loading">
            <mn:image id="mnimage4704" runat="server" filename="ajax-loader.gif" titleresourceconstant="" resourceconstant="" />
            <p><mn:txt id="mntxt7220" runat="server" resourceconstant="TXT_LOADING" expandimagetokens="true" /></p>
        </div>
        <div class="nano-filmstrip results">
            <ul>            		
                <asp:Repeater ID="repeaterProfiles" runat="server" OnItemDataBound="repeaterProfiles_ItemDataBound">
                    <ItemTemplate>
                        <%--note: highlighted class added in code-behind --%>
                        <li class="item style-basic" id="liBegin" runat="server">
                            <uc1:FilmStripNanoProfile ID="FilmStripNanoProfile1" runat="server"></uc1:FilmStripNanoProfile>
                        </li>
                    </ItemTemplate>
                </asp:Repeater>
                <asp:PlaceHolder ID="phViewMore" runat="server">
                    <li class="item view-more"><asp:HyperLink  ID="lnkViewMore"  runat="server" class="memberpic"><mn2:FrameworkLiteral ID="FrameworkLiteral3" runat="server" ResourceConstant="TXT_VIEW_MORE"></mn2:FrameworkLiteral></asp:HyperLink></li>
                </asp:PlaceHolder>
            </ul>
        </div>
    </div>
    <mn:txt runat="server" ResourceConstant="TXT_NANO_FILMSTRIP_MESSAGE" ExpandImageTokens="true" />
</asp:PlaceHolder>

<script type="text/javascript">
$j(document).ready(function(){
	var pictureSet = $j("#hp-hero-strip .picture");
    var siteDirection=$j("html").attr("dir");
    $j("#hp-hero-strip .picture:first").addClass('turn-on');
    
	$j(pictureSet).hover(function(event){
		$j(pictureSet).removeClass('turn-on');
		$j(this).toggleClass('turn-on');
	});
	
	if(siteDirection=="rtl")
	{
	    $j(".nano-filmstrip").jCarouselLite({
		    btnNext: ".filmstrip-prev",
		    btnPrev: ".filmstrip-next",
		    circular: false,
		    mouseWheel: true,
		    scroll:6,
		    speed:600,
		    visible:6
	    });
	}
	else
	{
	    $j(".nano-filmstrip").jCarouselLite({
		    btnNext: ".filmstrip-next",
		    btnPrev: ".filmstrip-prev",
		    circular: false,
		    mouseWheel: true,
		    scroll:6,
		    speed:600,
		    visible:6
	    });
	}
	
	$j(".nano-filmstrip.results ul").css('margin-left','auto');
	
});
</script>