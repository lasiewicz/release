﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Matchnet.Lib;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Ui.BasicElements;
using Matchnet.Content.ValueObjects.BrandConfig;

namespace Matchnet.Web.Framework.Ui.BasicElements
{
    public partial class ProgressBar : FrameworkControl
    {
        string PROGRESS_FORMAT = "{0}%";
        private string PROGRESS_STYLE_FORMAT = "style=\"left:{0}%\"";
        private string PROGRESS_STYLE_FORMAT_RTL = "style=\"right:{0}%\"";
        FrameworkControl _resourceControl;


        int _progress;

        public int ProgressPercent
        {
            get { return _progress; }
            set { _progress = value; }

        }

        public FrameworkControl ResourceControl
        {
            get { return _resourceControl; }
            set { _resourceControl = value; }

        }
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        private void Page_PreRender(object sender, System.EventArgs e)
        {
            txtProgress.ResourceControl = _resourceControl;
            litProgress.Text = String.Format(PROGRESS_FORMAT, _progress);
            if (g.Brand.Site.Direction == DirectionType.ltr)
            {
                litStyle.Text = String.Format(PROGRESS_STYLE_FORMAT, _progress);
            }
            else
            {
                litStyle.Text = String.Format(PROGRESS_STYLE_FORMAT_RTL, _progress);
            }
        }
    }
}