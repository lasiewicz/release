using System;
using System.ComponentModel;

namespace Matchnet.Web.Framework.Ui.FormElements
{
	/// <summary>
	/// Custom Label to support Spark behaviors
	/// </summary>
	public class FrameworkLabel : System.Web.UI.WebControls.Label
	{
		string _ResourceConstant = String.Empty;

		#region Properties
		/// <summary>
		/// Resource constant representing the key to a value in a resource file
		/// </summary>
		[Category("Appearance")]
		[DefaultValue("")]
		[Description("Resource key for value in resource file")]
		public string ResourceConstant
		{
			get { return this._ResourceConstant;}
			set { this._ResourceConstant = value;}
		}
		#endregion

		public FrameworkLabel() : base()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		protected override void Render(System.Web.UI.HtmlTextWriter writer)
		{
			try
			{
				//assign resource value to text
				ContextGlobal g = System.Web.HttpContext.Current.Items["g"] as ContextGlobal;
				if (g != null && this._ResourceConstant.Trim() != "")
					this.Text += g.GetResource(this._ResourceConstant, this);

			}
			catch (Exception ex)
			{
				System.Diagnostics.Debug.WriteLine(ex.Message);
			}
			finally
			{
				base.Render (writer);
			}
		}


		
	}
}
