<%@ Control Language="c#" AutoEventWireup="false" CodeBehind="MiniProfile.ascx.cs"
    Inherits="Matchnet.Web.Framework.Ui.BasicElements.MiniProfile" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="uc1" TagName="LastLoginDate" Src="LastLoginDate.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Add2List" Src="Add2List.ascx" %>
<%@ Register TagPrefix="uc1" TagName="NoPhoto" Src="../PageElements/NoPhoto.ascx" %>
<%@ Register TagPrefix="uc2" TagName="MiniProfileHotListBar" Src="MiniProfileHotListBar.ascx" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="uc2" TagName="HighlightProfileInfoDisplay" Src="HighlightProfileInfoDisplay.ascx" %>
<%@ Register TagPrefix="uc3" TagName="SpotlightInfoDisplay" Src="SpotlightInfoDisplay.ascx" %>
<%@ Register TagPrefix="uc4" TagName="ProfileEnlarge" Src="ProfileEnlarge.ascx" %>
<asp:PlaceHolder ID="plcSpotlight" runat="server" Visible="false">
    <div id="spotlight_curveTop" class="clearFloats">
        <div style="margin: 0 1em;">
            <div class="floatInside whiteText">
                <strong>
                    <mn:Txt ID="txtSpotlight" runat="server" ResourceConstant="SPOTLIGHT_TITLE" Visible="false" />
                    <mn:Txt ID="txtSpotlightPromo" runat="server" ResourceConstant="SPOTLIGHT_PROMO_TITLE"
                        Visible="false" />
                    <mn:Txt ID="txtSelfSpotlight" runat="server" ResourceConstant="SPOTLIGHT_SELF_TITLE"
                        Visible="false" />
                </strong>
            </div>
            <div class="floatOutside" style="position: relative; z-index: 200;">
                <uc3:SpotlightInfoDisplay runat="server" ID="ucSpotlightInfoDisplay" ShowImageNoText="false"
                    Visible="false" />
                <asp:PlaceHolder ID="plcSpotlightSettings" runat="server" Visible="false"><a href="/Applications/MemberServices/PremiumServiceSettings.aspx"
                    class="whiteText"><strong>
                        <mn:Txt ID="txtlnkSpotlightSettings" runat="server" ResourceConstant="SPOTLIGHT_SETTINGS" />
                    </strong></a></asp:PlaceHolder>
            </div>
        </div>
    </div>
</asp:PlaceHolder>
<%@ Register TagPrefix="uc2" TagName="MatchMeterInfoDisplay" Src="MatchMeterInfoDisplay.ascx" %>
<asp:Literal ID="plcPixel" runat="server"></asp:Literal><asp:Literal ID="litContainer"
    runat="server"></asp:Literal>
<table border="0" class="box" id="boxContent" cellspacing="0" width="592" cellpadding="4">
    <tr>
        <td class="mp1" rowspan="2" id="cProfilePhoto">
            <uc1:NoPhoto runat="server" ID="noPhoto" class="noPhoto" Visible="False" />
            <mn:Image ID="ImageThumb" runat="server" Width="80" Height="104" Border="0" CssClass="profileImageHover" />
        </td>
        <td class="mp2" align="right" colspan="2" runat="server" id="tdAnimBG">
            <asp:PlaceHolder runat="server" ID="plcYNM">
                <mn:Txt runat="server" ID="TxtThinkYoud" ResourceConstant="PRO_THINK_YOUD" />
                &nbsp;<a id="lnkClick" runat="server"><strong><mn:Txt runat="server" ID="txtClick"
                    ResourceConstant="TXT_CLICK_TRADE" />
                </strong></a>
                <mn:Txt runat="server" ID="Txt1" ResourceConstant="PRO_THINK_YOUD_AFTER_CLICK_LINK" />
                &nbsp;
                <mn:Image ID="ImageYes" FileName="icon-click-y-off.gif" runat="server" Hspace="3"
                    ResourceConstant="YNM_Y_IMG_ALT_TEXT" align="absmiddle" />
                <mn:Txt ID="TxtYes" runat="server" ResourceConstant="YNM_YES" />
                &nbsp;
                <mn:Image ID="ImageMaybe" FileName="icon-click-m-off.gif" runat="server" Hspace="3"
                    ResourceConstant="YNM_M_IMG_ALT_TEXT" align="absmiddle" />
                <mn:Txt ID="TxtMaybe" runat="server" ResourceConstant="YNM_MAYBE" />
                &nbsp;
                <mn:Image ID="ImageNo" FileName="icon-click-n-off.gif" runat="server" Hspace="3"
                    ResourceConstant="YNM_N_IMG_ALT_TEXT" align="absmiddle" />
                <mn:Txt ID="TxtNo" runat="server" ResourceConstant="YNM_NO" />
                &nbsp;
                <uc2:MatchMeterInfoDisplay runat="server" ID="ucMatchMeterInfoDisplay" DispalyType="1" />
            </asp:PlaceHolder>
            <asp:PlaceHolder runat="server" ID="plcEmailMe" Visible="false">
                <asp:HyperLink runat="server" ID="lnkEmail" CssClass="emailMeLnk" Style="display: block;">
                    <mn:Image ID="Image1" runat="server" FileName="icon_email.gif" align="absMiddle"
                        Hspace="3" Vspace="0" ResourceConstant="ALT_EMAIL_ME" />
                    <mn:Txt runat="server" ID="txt2" ResourceConstant="PRO_EMAIL" />
                </asp:HyperLink>
            </asp:PlaceHolder>
            <asp:PlaceHolder runat="server" ID="plcUpperComm" Visible="false">
                <asp:HyperLink runat="server" ID="lnkOnline1" CssClass="onlineIconProfile">
                    <mn:Image ID="imgChat1" runat="server" FileName="icon_chat.gif" align="absMiddle"
                        Hspace="1" Vspace="0" ResourceConstant="ALT_IM_ONLINE" />
                    <mn:Txt runat="server" ResourceConstant="PRO_ONLINE" ID="txtOnline1" />
                </asp:HyperLink>
                <a id="lnkTeaseMe1" runat="server">
                    <mn:Image ID="icon_tease1" runat="server" FileName="icon_tease.gif" align="absMiddle"
                        Hspace="1" Vspace="0" ResourceConstant="PRO_TEASE" TitleResourceConstant="ALT_TEASE_ME" />
                    <mn:Txt runat="server" ID="txtTease1" ResourceConstant="PRO_TEASE" />
                </a><a id="lnkECard1" runat="server">
                    <mn:Image ID="icon_ecard1" runat="server" FileName="icon_ecard.gif" align="absMiddle"
                        Hspace="1" Vspace="0" ResourceConstant="PRO_ECARD" TitleResourceConstant="ALT_ECARD" />
                    <mn:Txt runat="server" ID="txtECard1" ResourceConstant="PRO_ECARD" />
                </a>
                <uc1:Add2List ID="add2List1" runat="server" />
            </asp:PlaceHolder>
        </td>
        <input type="hidden" id="YNMVoteStatus" runat="server" name="YNMVoteStatus" />
    </tr>
    <tr>
        <td class="mp3">
            <mn:Image ID="trans" runat="server" FileName="trans.gif" Width="265" Height="1" />
            <br />
            <span class="miniProfileTitle">
                <asp:HyperLink ID="LinkUserName" runat="server" /></span>
            <mn:Image ID="imgBothSaidYes" runat="server" TitleResourceConstant="TTL_CLICK_YOU_BOTH_SAID_YES_EXCLAMATION"
                FileName="icon-click-yy.gif" Hspace="2" Border="0" Visible="false" CssClass="imageAlignInline" />
            <mn:Image ID="ImageOnline" runat="server" FileName="icon_IM_members.gif" Visible="false"
                CssClass="imageAlignInline" />
            <mn:Image ID="ImageIsNew" runat="server" FileName="icon_newMember.gif" Visible="false"
                Hspace="2" TitleResourceConstant="ALT_NEW_MEMBER" CssClass="imageAlignInline" />
            <mn:Image ID="ImageIsUpdate" runat="server" FileName="icon_updated.gif" Visible="false"
                Hspace="2" TitleResourceConstant="ALT_UPDATED_MEMBER" CssClass="imageAlignInline" />
            <mn:Image ID="imgPremiumAuthenticated" runat="server" FileName="icon_premiumAuthenticated_l.gif"
                Visible="false" TitleResourceConstant="ALT_PREMIUM_AUTHENTICATED" ResourceConstant="ALT_PREMIUM_AUTHENTICATED"
                class="verifiedIcon" />
            <asp:PlaceHolder ID="plcHotListBar" runat="server"></asp:PlaceHolder>
            <br />
            <asp:PlaceHolder ID="plcNormalView" runat="server">
                <asp:Literal ID="TextAge" runat="server" /><asp:Literal ID="TextGender" runat="server" /><br />
                <asp:Literal ID="TextRegion" runat="server" /><br />
                <span class="activity">
                    <uc1:LastLoginDate ID="lastLoginDate" runat="server" />
                </span></asp:PlaceHolder>
            <asp:PlaceHolder ID="plcSpotlightView" runat="server" Visible="false">
                <asp:Literal ID="TextInfo" runat="server" /><br />
                <asp:Repeater ID="rptPhotos" runat="server">
                    <ItemTemplate>
                        <mn:Image ID="imgTinyThumb" runat="server" Width="35" Height="43" />
                    </ItemTemplate>
                </asp:Repeater>
            </asp:PlaceHolder>
        </td>
        <td class="mp4" valign="top">
            <uc4:ProfileEnlarge runat="server" ID="ucProfileEnlarge" Visible="false" />
            <br />
            <mn:Txt runat="server" ID="txtBothLike" ResourceConstant="TXT_BOTH_LIKE" Visible="false"
                ExpandImageTokens="true" />
            <asp:Literal ID="TextHeadline" runat="server" />
            &nbsp;&nbsp;<mn:Txt runat="server" ID="txtMore" ResourceConstant="TXT_VIEW_MEMBER_ARROWS" />
        </td>
    </tr>
    <tr id="TableRowComments" class="TableRowComments" runat="server">
        <td class="mp5" colspan="2" id="cProfileBottomLeft" height="31">
            <table cellpadding="0" cellspacing="0" border="0">
                <tr>
                    <asp:PlaceHolder runat="server" ID="plcLowerComm">
                        <td class="mp5-td">
                            <asp:HyperLink runat="server" ID="lnkOnline">
                                <mn:Image ID="imgChat" runat="server" FileName="icon_chat.gif" align="absMiddle"
                                    Hspace="1" Vspace="0" ResourceConstant="ALT_IM_ONLINE" />
                                <mn:Txt runat="server" ResourceConstant="PRO_ONLINE" ID="txtOnline" />
                            </asp:HyperLink>
                        </td>
                        <td class="mp5-td">
                            <a id="lnkEmailMe" runat="server">
                                <mn:Image ID="ImageEmailMe" runat="server" FileName="icon_email.gif" align="absMiddle"
                                    Hspace="3" Vspace="0" ResourceConstant="ALT_EMAIL_ME" />
                                <mn:Txt runat="server" ID="txtEmail" ResourceConstant="PRO_EMAIL" />
                            </a>
                        </td>
                        <td class="mp5-td">
                            <a id="lnkTeaseMe" runat="server">
                                <mn:Image ID="icon_tease" runat="server" FileName="icon_tease.gif" align="absMiddle"
                                    Hspace="1" Vspace="0" ResourceConstant="PRO_TEASE" TitleResourceConstant="ALT_TEASE_ME" />
                                <mn:Txt runat="server" ID="txtTease" ResourceConstant="PRO_TEASE" />
                            </a>
                        </td>
                        <td class="mp5-td" runat="server" id="TableCellECard">
                            <a id="lnkECard" runat="server">
                                <mn:Image ID="icon_ecard" runat="server" FileName="icon_ecard.gif" align="absMiddle"
                                    Hspace="1" Vspace="0" ResourceConstant="PRO_ECARD" TitleResourceConstant="ALT_ECARD" />
                                <mn:Txt runat="server" ID="txtECard" ResourceConstant="PRO_ECARD" />
                            </a>
                        </td>
                        <td class="mp5-td">
                            <uc1:Add2List ID="add2List" runat="server" />
                        </td>
                    </asp:PlaceHolder>
                </tr>
            </table>
        </td>
        <td class="mp6" id="cProfileBottomRight">
            <asp:LinkButton Visible="False" runat="server" ID="lbRemoveProfile" />&nbsp;
            <uc2:HighlightProfileInfoDisplay runat="server" ID="ucHighlightProfileInfoDisplay"
                ShowImageNoText="false" />
        </td>
    </tr>
</table>
<!-- memberSpotlight_curveBottom needs to be hidden from all non-spotlight profiles -->
<mn:Image ID="memberSpotlight_curveBottom" runat="server" FileName="memberSpotlight_curveBottom.gif"
    Visible="false" Style="display: block" />
</div>
<asp:PlaceHolder runat="server" ID="plcTransparent" Visible="False">
    <div id="transContainer">
        <div id="trans1">
        </div>
        <div id="trans2">
        </div>
        <div id="trans3">
        </div>
    </div>
</asp:PlaceHolder>
<asp:Panel runat="server" ID="pnlComments" Visible="False">
    <input class="editHotList" id="txtFavoriteNote" type="text" maxlength="55" name="<%= Member.MemberID %>"
        runat="server">&nbsp;
    <mn2:FrameworkButton class="activityButton" ID="btnSave" runat="server" ResourceConstant="SAVE"
        name="Save" type="submit"></mn2:FrameworkButton>&nbsp;
</asp:Panel>
<asp:PlaceHolder runat="server" ID="plcBreaks" Visible="False">
    <br />
    <br />
</asp:PlaceHolder>
