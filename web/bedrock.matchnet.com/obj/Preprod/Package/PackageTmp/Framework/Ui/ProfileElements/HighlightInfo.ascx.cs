﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Matchnet.Web.Framework.Util;
using Matchnet.Content.ServiceAdapters.Links;

namespace Matchnet.Web.Framework.Ui.ProfileElements
{
    public partial class HighlightInfo : FrameworkControl
    {
    
        FrameworkControl _resourceControl = null;

        //string _subscriberTextResourceConstant = null;
        //string _nonSubscriberTextResourceConstant = null;
        string _outerLinkTextResourceConstant = "TXT_LEARN_ABOUT_HIGHLIGHTED_PROFILES";
        string _outerLinkImageResourceConstant = "IMAGE_LEARN_ABOUT_HIGHLIGHTED_PROFILES";

        public string InfoClientID
        { get { return divInfo.ClientID; } }
        public string InfoLinkClientID
        { get { return divLink.ClientID; } }

        public FrameworkControl ResourceControl
        {
            get { if (_resourceControl == null) { return this; } else { return _resourceControl; } }
            set { _resourceControl = value; }
        }
        public bool Highlighted { get; set; }
        public bool OuterLinkDisplayText { get; set; }
        public string OuterLinkResourceConstant { get; set; }
        public int HighlightedMemberID { get; set; }
       
        public string OuterLinkTextResourceConstant { get { return _outerLinkTextResourceConstant; } set { _outerLinkTextResourceConstant = value; } }
        public string OuterLinkImageResourceConstant { get { return _outerLinkImageResourceConstant; } set { _outerLinkImageResourceConstant = value; } }


        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Highlighted)
                    phHighlightInfo.Visible = false;

                if (OuterLinkDisplayText)
                {   txtInfoLink.Text = g.GetResource(OuterLinkTextResourceConstant, null,true,ResourceControl);}
                else
                { txtInfoLink.Text = g.GetResource(OuterLinkImageResourceConstant, null, true, ResourceControl); }
                lnkSubscribe.NavigateUrl = LinkFactory.Instance.GetLink("/Applications/Subscription/Subscribe.aspx?prtid=" + ((int)PurchaseReasonType.ViewedHighlightProfileInfo).ToString(), g.Brand, System.Web.HttpContext.Current.Request.Url.ToString()) + "&srid=" + HighlightedMemberID.ToString();

            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }


        protected override void OnPreRender(EventArgs e)
        {
          
            base.OnPreRender(e);
        }
    }
}