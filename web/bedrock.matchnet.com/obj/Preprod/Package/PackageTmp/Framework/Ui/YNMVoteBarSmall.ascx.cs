using System;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Matchnet.Web.Framework.Ui.BasicElements;
using Matchnet.List.ValueObjects;

namespace Matchnet.Web.Framework.Ui
{
	/// <summary>
	///		Summary description for YNMVoteBarSmall
	/// </summary>
	public class YNMVoteBarSmall : FrameworkControl
	{
		protected Image imgMutualYes;
		protected YNMVoteButton ImageYes;
		protected YNMVoteButton ImageNo;
		protected YNMVoteButton ImageMaybe;
		protected HtmlInputHidden YNMVoteStatus;

		protected Literal litMemberID;
		protected Literal litMutualYesID;
		//protected Literal litYesButtonID;
		//protected Literal litNoButtonID;
		//protected Literal litMaybeButtonID;
		protected Literal litVoteButtonsID;

		protected HtmlControl divMutualYes;
		protected HtmlControl divVoteButtons;

		private int _MemberID;

        private string _yesMutualFileName = "icon-click-yy.gif";

        private string _yesFileName = "icon-click-y-off.gif";
        private string _noFileName = "icon-click-n-off.gif";
        private string _maybeFileName = "icon-click-m-off.gif";


        private string _yesFileNameSelected = "icon-click-y-on.gif";
        private string _noFileNameSelected = "icon-click-n-on.gif";
        private string _maybeFileNameSelected = "icon-click-m-on.gif";
        
		public int MemberID
		{
			get
			{
				return _MemberID;
			}
			set
			{
				_MemberID = value;
			}
		}

        public string YesMutualFileName
        {
            get { return _yesMutualFileName; }
            set { _yesMutualFileName = value; }
        }


        public string YesFileName
        {
            get {   return _yesFileName; }
            set {   _yesFileName = value; }
        }

        public string NoFileName
        {
            get { return _noFileName; }
            set { _noFileName = value; }
        }

        public string MaybeFileName
        {
            get { return _maybeFileName; }
            set { _maybeFileName = value; }
        }

        public string YesFileNameSelected
        {
            get { return _yesFileNameSelected; }
            set { _yesFileNameSelected = value; }
        }

        public string NoFileNameSelected
        {
            get { return _noFileNameSelected; }
            set { _noFileNameSelected = value; }
        }

        public string MaybeFileNameSelected
        {
            get { return _maybeFileNameSelected; }
            set { _maybeFileNameSelected = value; }
        }
        

		public void Page_Load(object sender, EventArgs e)
		{
			if (g.Member != null)
			{
				SetMemberIDs();
				//SetJavascript();
                ImageYes.FileName = YesFileName;
                ImageNo.FileName = NoFileName;
                ImageMaybe.FileName = MaybeFileName;

                ImageYes.SelectedFileName = YesFileNameSelected;
                ImageNo.SelectedFileName = NoFileNameSelected;
                ImageMaybe.SelectedFileName = MaybeFileNameSelected;

                imgMutualYes.FileName = YesMutualFileName;
            }
		}

		private void SetMemberIDs()
		{
			//Setup the YNMVoteButtons
			ImageYes.MemberID = _MemberID;
			ImageNo.MemberID = _MemberID;
			ImageMaybe.MemberID = _MemberID;
		}

		private void SetJavascript()
		{
			// make sure not trying to add the vote bar to member's own profile
			if( _MemberID == g.Member.MemberID ) 
			{
				divMutualYes.Style.Add( "display", "none" );
				divVoteButtons.Style.Add( "display", "none" );
				return;
			}

			//Put the clickMask in a hidden input for use by JavaScript
			//int clickMask = (int) g.List.GetClickMask(g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, _MemberID);
			ClickMask clickMask = g.List.GetClickMask(g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, _MemberID);
			YNMVoteStatus.Value = ((int)clickMask).ToString();

			// if the target member voted yes, we want mutual yes to show up if current member votes yes or has already voted yes
			if ( (clickMask & ClickMask.TargetMemberYes) == ClickMask.TargetMemberYes )
			{
				if ( (clickMask & ClickMask.MemberYes) == ClickMask.MemberYes ) // if current member already voted yes
				{
					// hide the vote buttons and show mutual yes icon
					divMutualYes.Style.Add( "display", "inline" );
					divVoteButtons.Style.Add( "display", "none" );

				}
				else // add javascript to the yes button.
					ImageYes.ExtraJavaScriptToRun = "ShowMutualYes('" + divMutualYes.ClientID + "','" + divVoteButtons.ClientID + "')";
				//ImageYes.Attributes.Add( "onClick", String.Format( "ShowMutualYes{0}('{1}','{2}')", _MemberID.ToString(), divMutualYes.ClientID, divVoteButtons.ClientID ) );
			}
            ImageYes.Attributes.Add("onclick", g.AnalyticsOmniture.GetOnClickCustomLinkTracking(WebConstants.Action.ClickY, g.AnalyticsOmniture.PageName, "", false, false));
            ImageNo.Attributes.Add("onclick", g.AnalyticsOmniture.GetOnClickCustomLinkTracking(WebConstants.Action.ClickN, g.AnalyticsOmniture.PageName, "", false, false));
            ImageMaybe.Attributes.Add("onclick", g.AnalyticsOmniture.GetOnClickCustomLinkTracking(WebConstants.Action.ClickM, g.AnalyticsOmniture.PageName, "", false, false));
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new EventHandler(this.Page_Load);
			this.PreRender +=new EventHandler(YNMVoteBarSmall_PreRender);
		}

		#endregion

        private void YNMVoteBarSmall_PreRender(object sender, EventArgs e)
        {
            if (g.Member != null)
            {
                SetJavascript();
            }

            if (g.IsSite20Enabled)
            {
                this.ImageYes.FileName = "icon-click-y-off.gif";
                this.ImageYes.SelectedFileName = "icon-click-y-on.gif";

                this.ImageNo.FileName = "icon-click-n-off.gif";
                this.ImageNo.SelectedFileName = "icon-click-n-on.gif";

                this.ImageMaybe.FileName = "icon-click-m-off.gif";
                this.ImageMaybe.SelectedFileName = "icon-click-m-on.gif";
            }
        }
	}
}
