﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.Region;
using Matchnet.Lib;
using Matchnet.Search.ValueObjects;
using Matchnet.Web.Framework.Managers;
using Matchnet.Web.Framework.Util;
using MemberSearch = Spark.SAL.MemberSearch;
using MemberSearchCollection = Spark.SAL.MemberSearchCollection;
using Spark.SAL;
using Preference = Spark.SAL.Preference;
using Matchnet.Web.Applications.QuickSearch;

namespace Matchnet.Web.Framework.Ui.BasicElements
{
    /// <summary>
    /// MPR-1260 Quick Search Box for JD Redesign
    /// </summary>
    public partial class QuickSearchBox : FrameworkControl
    {
        private int countryRegionID = Constants.NULL_INT;
        private int _qsearchCriteriaExpirationDays = 365;
        private const int USA_REGIONID = 223;
        private const int IL_REGIONID = 105;
        private bool isRedesign = false;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ButtonSearchNow.Text = g.GetResource("TXT_BUTTON_SEARCH_NOW", this);
                ButtonSearchNowRedesign.Text = g.GetResource("TXT_BUTTON_SEARCH_NOW", this);
                if (!IsControlEnabled())
                {
                    PlaceHolderMain.Visible = false;
                    return;
                }
                if (g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.JDateCoIL)
                {
                    isRedesign = true;
                    plcQuickSearchBoxOld.Visible = false;
                    plcQuickSearchBoxRedesign.Visible = true;
                    cbHasPhoto.Text = g.GetResource("TXT_SHOW_PHOTO_PROFILES_ONLY", this);
                }
                PopulateControls();
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        /// <summary>
        /// Determine if this control needs to be loaded using setting.
        /// </summary>
        private bool IsControlEnabled()
        {
            var sector = SectorsManager.GetSector();
            if (sector == Sector.Academic || sector == Sector.Elderly || sector == Sector.Divorced || sector == Sector.Religious)
            {
                return true;
            }

            if (!RuntimeSettings.SettingExists("QUICK_SEARCH_BOX", g.Brand.Site.Community.CommunityID,
                                                              g.Brand.Site.SiteID))
            {
                return false;
            }

            //do not show quick search box on matches beta page
            if (BetaHelper.IsDisplayingSearchRedesign30(g))
            {
                return false;
            }
            else
            {
                string settingValue = RuntimeSettings.GetSetting("QUICK_SEARCH_BOX",
                                                                 g.Brand.Site.Community.CommunityID,
                                                                 g.Brand.Site.SiteID, g.Brand.BrandID);

                List<string> pageIDs = new List<string>(settingValue.Split(new char[] { ',' }));

                List<string> found = (from pageID in pageIDs
                                      where pageID.Equals(g.AppPage.ID.ToString())
                                      select pageID).ToList();

                if (found.Count == 0)
                    return false;
            }

            return true;
        }

        /// <summary>
        /// Load search prefs and bind to controls.
        /// </summary>
        private void PopulateControls()
        {
            #region Load from Search Preferences

            MemberSearch memberSearch = getMemberSearch();
            RegionLanguage regionLanguage;

            int regionID = memberSearch.RegionID;
            MemberSearchPreferenceInt areaCodePreference1 =
                       memberSearch[Preference.GetInstance("AreaCode1")] as MemberSearchPreferenceInt;

            if (regionID <= 0)
            {
                regionID = g.Brand.Site.DefaultRegionID;
                regionLanguage = RegionSA.Instance.RetrievePopulatedHierarchy(regionID, g.Brand.Site.LanguageID);
                countryRegionID = regionLanguage.CountryRegionID;
            }
            else
            {
                regionLanguage = RegionSA.Instance.RetrievePopulatedHierarchy(regionID, g.Brand.Site.LanguageID);
                countryRegionID = regionLanguage.CountryRegionID;
                // Last minute decision by product since they didn't include country option in the spec.
                if (countryRegionID != USA_REGIONID)
                {
                    if (g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.JDateCoIL ||
                        g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.Cupid)
                    {
                        plcZipCode.Visible = false;
                        plcZipCodeRedesign.Visible = false;
                        plcRegion.Visible = true;
                        plcRegionRedesign.Visible = true;

                    }
                    else
                    {
                        PlaceHolderMain.Visible = false;
                        return;
                    }
                }
                else
                {
                    if (g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.JDateCoIL)
                    {
                        plcZipCode.Visible = false;
                        plcZipCodeRedesign.Visible = false;
                        plcRegion.Visible = true;
                        plcRegionRedesign.Visible = true;
                        if (areaCodePreference1 == null)
                        {
                            areaCodePreference1 = new MemberSearchPreferenceInt { Value = 3 };
                        }
                    }
                }
            }

            string postalCode = regionLanguage.PostalCode;
            int distance = Conversion.CInt(memberSearch.Distance, g.Brand.DefaultSearchRadius);

            #endregion

            #region Gender

            ListItem itemMale = new ListItem(g.GetResource("MALE"), ((Int32)Gender.Male).ToString());
            ListItem itemFemale = new ListItem(g.GetResource("FEMALE"), ((Int32)Gender.Female).ToString());

            DropDownListGender.Items.Add(itemMale);
            DropDownListGender.Items.Add(itemFemale);

            itemMale = new ListItem(g.GetResource("MALE"), ((Int32)Gender.Male).ToString());
            itemFemale = new ListItem(g.GetResource("FEMALE"), ((Int32)Gender.Female).ToString());

            DropDownListSeekingGender.Items.Add(itemMale);
            DropDownListSeekingGender.Items.Add(itemFemale);

            DropDownListGenderSeeking.Items.Add(new ListItem(g.GetResource("MALE_SEEK_FEMALE"), ((Int32)GenderMask.Male + (Int32)GenderMask.SeekingFemale).ToString()));
            DropDownListGenderSeeking.Items.Add(new ListItem(g.GetResource("FEMALE_SEEK_MALE"), ((Int32)GenderMask.Female + (Int32)GenderMask.SeekingMale).ToString()));
            DropDownListGenderSeeking.Items.Add(new ListItem(g.GetResource("MALE_SEEK_MALE"), ((Int32)GenderMask.Male + (Int32)GenderMask.SeekingMale).ToString()));
            DropDownListGenderSeeking.Items.Add(new ListItem(g.GetResource("FEMALE_SEEK_FEMALE"), ((Int32)GenderMask.Female + (Int32)GenderMask.SeekingFemale).ToString()));

            foreach (ListItem item in DropDownListGender.Items)
            {
                if ((Convert.ToInt32(item.Value) == (int)memberSearch.Gender))
                {
                    item.Selected = true;
                    break;
                }
            }

            foreach (ListItem item in DropDownListSeekingGender.Items)
            {
                if ((Convert.ToInt32(item.Value) == (int)memberSearch.SeekingGender))
                {
                    item.Selected = true;
                    break;
                }
            }

            foreach (ListItem item in DropDownListGenderSeeking.Items)
            {
                if ((Convert.ToInt32(item.Value) == (int)memberSearch.Gender + (int)memberSearch.SeekingGender * 4))
                {
                    item.Selected = true;
                    break;
                }
            }

            #endregion

            #region Distance
            DataTable dataTableDistance = Option.GetOptions(Matchnet.Web.Applications.Search.SearchUtil.GetDistanceAttribute(g), g);

            DropDownListDistance.DataSource = dataTableDistance;
            DropDownListDistanceRedesign.DataSource = dataTableDistance;

            DropDownListDistance.DataTextField = "Content";
            DropDownListDistance.DataValueField = "Value";
            DropDownListDistance.DataBind();

            DropDownListDistanceRedesign.DataTextField = "Content";
            DropDownListDistanceRedesign.DataValueField = "Value";
            DropDownListDistanceRedesign.DataBind();

            foreach (ListItem item in DropDownListDistance.Items)
            {
                if (Conversion.CInt(item.Value) == distance)
                {
                    item.Selected = true;
                }
            }

            if (DropDownListDistance.SelectedItem == null)
            {
                int defaultDistanceListOrder = SettingsManager.GetSettingInt(SettingConstants.DEFAULT_DISTANCE_LIST_ORDER, g.Brand) - 1;
                if (DropDownListDistance.Items.Count < defaultDistanceListOrder)
                {
                    DropDownListDistance.Items[defaultDistanceListOrder].Selected = true;
                }
            }
            foreach (ListItem item in DropDownListDistanceRedesign.Items)
            {
                if (Conversion.CInt(item.Value) == distance)
                {
                    item.Selected = true;
                }
            }
            #endregion

            TextBoxPostalCode.Text = postalCode;
            TextBoxPostalCodeRedesign.Text = postalCode;

            #region Min & Max Age

            int minAge = SharedLib.Utils.Conversion.CInt(memberSearch.Age.MinValue);
            if (minAge == Constants.NULL_INT || minAge == 0)
            {
                minAge = g.Brand.DefaultAgeMin;
            }

            int maxAge = SharedLib.Utils.Conversion.CInt(memberSearch.Age.MaxValue);
            if (maxAge == Constants.NULL_INT || maxAge == 0)
            {
                maxAge = g.Brand.DefaultAgeMax;
            }

            TextBoxAgeMin.Text = minAge.ToString();
            TextBoxAgeMax.Text = maxAge.ToString();

            for (int i = 18; i < 99; i++)
            {
                dropDownMinAge.Items.Add(new ListItem(i.ToString(), i.ToString()));
                dropDownMaxAge.Items.Add(new ListItem(i.ToString(), i.ToString()));
            }
            dropDownMinAge.SelectedValue = minAge.ToString();
            dropDownMaxAge.SelectedValue = maxAge.ToString();
            #endregion


            ListItem currentItem = null;

            currentItem = new ListItem(g.GetResource("CUPID_SPLASH_DDL_TWO", this), "02");
            cupidRegionDropDownList.Items.Add(currentItem);
            cupidRegionDropDownListRedesign.Items.Add(currentItem);
            currentItem = new ListItem(g.GetResource("CUPID_SPLASH_DDL_THREE", this), "03");
            cupidRegionDropDownList.Items.Add(currentItem);
            cupidRegionDropDownListRedesign.Items.Add(currentItem);
            currentItem = new ListItem(g.GetResource("CUPID_SPLASH_DDL_FOUR", this), "04");
            cupidRegionDropDownList.Items.Add(currentItem);
            cupidRegionDropDownListRedesign.Items.Add(currentItem);
            currentItem = new ListItem(g.GetResource("CUPID_SPLASH_DDL_EIGHT", this), "08");
            cupidRegionDropDownList.Items.Add(currentItem);
            cupidRegionDropDownListRedesign.Items.Add(currentItem);
            currentItem = new ListItem(g.GetResource("CUPID_SPLASH_DDL_NINE", this), "09");
            cupidRegionDropDownList.Items.Add(currentItem);
            cupidRegionDropDownListRedesign.Items.Add(currentItem);
            cupidRegionDropDownList.SelectedIndex = 1;
            cupidRegionDropDownListRedesign.SelectedIndex = 1;


            if (areaCodePreference1 != null)
            {
                string val = "0" + areaCodePreference1.Value.ToString();
                if (cupidRegionDropDownList.Items.FindByValue(val) != null)
                {
                    cupidRegionDropDownList.SelectedIndex =
                        cupidRegionDropDownList.Items.IndexOf(cupidRegionDropDownList.Items.FindByValue(val));
                }
                if (cupidRegionDropDownListRedesign.Items.FindByValue(val) != null)
                {
                    cupidRegionDropDownListRedesign.SelectedIndex =
                        cupidRegionDropDownListRedesign.Items.IndexOf(cupidRegionDropDownListRedesign.Items.FindByValue(val));
                }

            }

            cbHasPhoto.Checked = memberSearch.HasPhoto;
        }

        private int GetDefaultGenderMask()
        {
            if (g.Brand.Site.Community.CommunityID == (int)WebConstants.COMMUNITY_ID.Glimpse)
            {
                return (ConstantsTemp.GENDERID_MALE | ConstantsTemp.GENDERID_SEEKING_MALE);
            }
            else
            {
                return (ConstantsTemp.GENDERID_FEMALE | ConstantsTemp.GENDERID_SEEKING_MALE);
            }
        }

        private MemberSearch getMemberSearch()
        {
            MemberSearch search = null;

            try
            {
                if (SearchFoundInSession())
                {
                    search = MemberSearchCollection.Load(Constants.NULL_INT, g.Brand).PrimarySearch;
                    if (search != null)
                    {
                        UpdateQSFromPersistence(search);
                    }

                }
                else
                {
                    if (g.Member != null)
                    {
                        search = MemberSearchCollection.Load(g.Member.MemberID, g.Brand).PrimarySearch;

                        if (search != null)
                        {
                            UpdateQSFromPersistence(search);
                        }
                        search.MemberID = Constants.NULL_INT;
                    }
                    else
                    {
                        search = MemberSearchCollection.Load(Constants.NULL_INT, g.Brand).PrimarySearch;
                    }
                }

                return search;
            }
            catch (Exception ex)
            {
                return search;
            }
        }

        private bool SearchFoundInSession()
        {
            int memberID = Constants.NULL_INT;
            if (g.Member != null) memberID = g.Member.MemberID;
            bool found = true;

            QuickSearchPersistence persistence = new QuickSearchPersistence(memberID, getCriteriaExpirationDays());

            if (persistence.GenderMask == Constants.NULL_INT || persistence.RegionID == Constants.NULL_INT ||
                persistence.MinAge == Constants.NULL_INT || persistence.MaxAge == Constants.NULL_INT || persistence.Distance == Constants.NULL_INT)
                found = false;

            return found;
        }

        public void PersistQSValues(MemberSearch memberSearch)
        {

            int memberID = Constants.NULL_INT;
            if (g.Member != null) memberID = g.Member.MemberID;

            QuickSearchPersistence persistence = new QuickSearchPersistence(memberID, getCriteriaExpirationDays());

            foreach (Preference Preference in memberSearch.SearchPreferences)
            {
                string prefName = Preference.Attribute.Name;
                switch (prefName.ToLower())
                {
                    case "birthdate":
                        MemberSearchPreferenceRange prefRange = (MemberSearchPreferenceRange)memberSearch[Preference];
                        persistence.MinAge = prefRange.MinValue;
                        persistence.MaxAge = prefRange.MaxValue;
                        break;
                    case "gendermask":
                        persistence.GenderMask = ((MemberSearchPreferenceInt)memberSearch[Preference]).Value;
                        break;
                    case "distance":
                        persistence.Distance = ((MemberSearchPreferenceInt)memberSearch[Preference]).Value;
                        break;
                    case "regionid":
                        persistence.RegionID = ((MemberSearchPreferenceInt)memberSearch[Preference]).Value;
                        break;
                    case "hasphotoflag":
                        persistence.HasPhotoFlag = ((MemberSearchPreferenceInt)memberSearch[Preference]).Value;
                        break;
                    case "schoolid":
                        persistence.SchoolID = ((MemberSearchPreferenceInt)memberSearch[Preference]).Value;
                        break;
                    case "searchtypeid":
                        persistence.SearchTypeID = ((MemberSearchPreferenceInt)memberSearch[Preference]).Value;
                        break;
                    case "jdatereligion":
                        persistence.JdateReligion = ((MemberSearchPreferenceInt)memberSearch[Preference]).Value;
                        break;
                    case "areacode1":
                        persistence.AreaCode1 = ((MemberSearchPreferenceInt)memberSearch[Preference]).Value;
                        break;
                    case "areacode2":
                        persistence.AreaCode2 = ((MemberSearchPreferenceInt)memberSearch[Preference]).Value;
                        break;
                    case "areacode3":
                        persistence.AreaCode3 = ((MemberSearchPreferenceInt)memberSearch[Preference]).Value;
                        break;
                    case "areacode4":
                        persistence.AreaCode4 = ((MemberSearchPreferenceInt)memberSearch[Preference]).Value;
                        break;
                    case "areacode5":
                        persistence.AreaCode5 = ((MemberSearchPreferenceInt)memberSearch[Preference]).Value;
                        break;
                    case "areacode6":
                        persistence.AreaCode6 = ((MemberSearchPreferenceInt)memberSearch[Preference]).Value;
                        break;
                }
            }

            persistence.PersistValues();

        }

        private void UpdateQSFromPersistence(MemberSearch search)
        {
            int memberID = Constants.NULL_INT;
            if (g.Member != null) memberID = g.Member.MemberID;

            QuickSearchPersistence persistence = new QuickSearchPersistence(memberID, getCriteriaExpirationDays());

            if (persistence.GenderMask == Constants.NULL_INT || persistence.RegionID == Constants.NULL_INT ||
                persistence.MinAge == Constants.NULL_INT || persistence.MaxAge == Constants.NULL_INT || persistence.Distance == Constants.NULL_INT)
                return;

            search.Gender = Spark.SAL.GenderUtils.GetGender(persistence.GenderMask);
            search.SeekingGender = Spark.SAL.GenderUtils.GetSeekingGender(persistence.GenderMask);
            search.Age.MinValue = persistence.MinAge;
            search.Age.MaxValue = persistence.MaxAge;
            search.Distance = persistence.Distance;
            search.RegionID = persistence.RegionID;

            if (persistence.HasPhotoFlag != Constants.NULL_INT)
                search.HasPhoto = Convert.ToBoolean(persistence.HasPhotoFlag);
            if (persistence.SchoolID != Constants.NULL_INT)
                search.SchoolID = persistence.SchoolID;
            if (persistence.SearchTypeID != Constants.NULL_INT)
                search.SearchType = (SearchType)persistence.SearchTypeID;
            if (persistence.AreaCode1 != Constants.NULL_INT)
            {
                MemberSearchPreferenceInt areaCodePreference1 = search[Preference.GetInstance("AreaCode1")] as MemberSearchPreferenceInt;
                areaCodePreference1.Value = persistence.AreaCode1;
            }
            if (persistence.AreaCode2 != Constants.NULL_INT)
            {
                MemberSearchPreferenceInt areaCodePreference2 = search[Preference.GetInstance("AreaCode2")] as MemberSearchPreferenceInt;
                areaCodePreference2.Value = persistence.AreaCode2;
            }
            if (persistence.AreaCode1 != Constants.NULL_INT)
            {
                MemberSearchPreferenceInt areaCodePreference3 = search[Preference.GetInstance("AreaCode3")] as MemberSearchPreferenceInt;
                areaCodePreference3.Value = persistence.AreaCode3;
            }
            if (persistence.AreaCode4 != Constants.NULL_INT)
            {
                MemberSearchPreferenceInt areaCodePreference4 = search[Preference.GetInstance("AreaCode4")] as MemberSearchPreferenceInt;
                areaCodePreference4.Value = persistence.AreaCode4;
            }
            if (persistence.AreaCode5 != Constants.NULL_INT)
            {
                MemberSearchPreferenceInt areaCodePreference5 = search[Preference.GetInstance("AreaCode5")] as MemberSearchPreferenceInt;
                areaCodePreference5.Value = persistence.AreaCode5;
            }
            if (persistence.AreaCode6 != Constants.NULL_INT)
            {
                MemberSearchPreferenceInt areaCodePreference6 = search[Preference.GetInstance("AreaCode6")] as MemberSearchPreferenceInt;
                areaCodePreference6.Value = persistence.AreaCode6;
            }
            if (persistence.JdateReligion != Constants.NULL_INT)
            {
                Spark.SAL.MemberSearchPreference jdatereligionPreference = search[Preference.GetInstance("jdatereligion")];
                (jdatereligionPreference as MemberSearchPreferenceMask).Value = persistence.JdateReligion;
            }
        }

        private int getCriteriaExpirationDays()
        {
            int days = _qsearchCriteriaExpirationDays;
            try
            {
                days = Conversion.CInt(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("QSEARCH_CRITERIA_EXPIRATION", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID), _qsearchCriteriaExpirationDays);
                return days;
            }
            catch (Exception ex)
            {
                return days;
            }
        }

        protected void ButtonSearchNow_Click(object sender, EventArgs e)
        {
            try
            {
                Page.Validate("QuickSearch");

                if (!Page.IsValid)
                    return;

                MemberSearch memberSearch = getMemberSearch();
                if (isRedesign)
                {
                    int gender = Convert.ToInt16(DropDownListGenderSeeking.SelectedValue);
                    gender = gender % 2 == 0 ? 2 : 1;
                    int seekGender = Convert.ToInt16(DropDownListGenderSeeking.SelectedValue) - gender;
                    seekGender = seekGender == (int)GenderMask.SeekingMale ? 1 : 2;
                    memberSearch.Gender = (Gender)Enum.ToObject(typeof(Gender), gender);
                    memberSearch.SeekingGender = (Gender)Enum.ToObject(typeof(Gender), seekGender);
                    memberSearch.Distance = Convert.ToInt32(DropDownListDistanceRedesign.SelectedValue);
                    memberSearch.Age.MinValue = Convert.ToInt32(dropDownMinAge.SelectedValue);
                    memberSearch.Age.MaxValue = Convert.ToInt32(dropDownMaxAge.SelectedValue);
                    memberSearch.HasPhoto = cbHasPhoto.Checked;
                }
                else
                {
                    memberSearch.Gender = (Gender)Enum.ToObject(typeof(Gender), Convert.ToInt16(DropDownListGender.SelectedValue));
                    memberSearch.SeekingGender = (Gender)Enum.ToObject(typeof(Gender), Convert.ToInt16(DropDownListSeekingGender.SelectedValue));
                    memberSearch.Distance = Convert.ToInt32(DropDownListDistance.SelectedValue);
                    memberSearch.Age.MinValue = Convert.ToInt32(TextBoxAgeMin.Text);
                    memberSearch.Age.MaxValue = Convert.ToInt32(TextBoxAgeMax.Text);
                }
                if (plcZipCode.Visible || plcZipCodeRedesign.Visible)
                {
                    RegionID postalCodeRegionID;
                    if (isRedesign)
                    {
                        postalCodeRegionID = RegionSA.Instance.FindRegionIdByPostalCode(countryRegionID, TextBoxPostalCodeRedesign.Text);
                    }
                    else
                    {
                        postalCodeRegionID = RegionSA.Instance.FindRegionIdByPostalCode(countryRegionID, TextBoxPostalCode.Text);
                    }

                    if (postalCodeRegionID == null)
                    {
                        LabelPostalCodeError.Text = "<div class=\"error\">" + g.GetResource("INVALID_POSTAL_CODE") + @"</br></div>";
                        LabelPostalCodeError.Visible = true;

                        LabelPostalCodeErrorRedesign.Text = "<div class=\"error\">" + g.GetResource("INVALID_POSTAL_CODE") + @"</br></div>";
                        LabelPostalCodeErrorRedesign.Visible = true;
                        return;
                    }

                    memberSearch.RegionID = postalCodeRegionID.ID;
                    memberSearch.SearchType = SearchType.PostalCode;
                }
                else
                {
                    MemberSearchPreferenceInt areaCodePreference1 =
                            memberSearch[Preference.GetInstance("AreaCode1")] as MemberSearchPreferenceInt;
                    if (areaCodePreference1 != null)
                    {
                        if (isRedesign)
                        {
                            areaCodePreference1.Value = Conversion.CInt(cupidRegionDropDownListRedesign.SelectedValue);
                        }
                        else
                        {
                            areaCodePreference1.Value = Conversion.CInt(cupidRegionDropDownList.SelectedValue);
                        }
                    }
                    for (Int32 i = 2; i <= 6; i++)
                    {
                        MemberSearchPreferenceInt areaCodePreference =
                            memberSearch[Preference.GetInstance("AreaCode" + i)] as MemberSearchPreferenceInt;
                        if (areaCodePreference != null)
                        {
                            areaCodePreference.Value = Constants.NULL_INT;
                        }

                    }
                    memberSearch.SearchType = SearchType.AreaCode;
                }

                string fullURL = Context.Request.Url.AbsoluteUri.ToLower();

                if (fullURL.Contains("academic-dating"))
                {
                    MemberSearchPreferenceMask educationLevelPreference = memberSearch[Preference.GetInstance("EducationLevel")] as MemberSearchPreferenceMask;
                    educationLevelPreference.Value = 120;
                }



                PersistQSValues(memberSearch);

                memberSearch.Save();

                g.Session.Add("QuickSearchBoxSearchClick", "True", Matchnet.Session.ValueObjects.SessionPropertyLifetime.Temporary);

                string extraParams = string.Empty;


                g.Transfer("/Applications/QuickSearch/QuickSearchResults.aspx" + extraParams);
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }
    }
}
