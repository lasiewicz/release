﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="ResultList20.ascx.cs" Inherits="Matchnet.Web.Framework.Ui.BasicElements.ResultList20" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn" TagName="GalleryMiniProfile20" Src="GalleryMiniProfile20.ascx" %>
<%@ Register TagPrefix="mn" TagName="miniProfile" Src="MiniProfile20.ascx" %>
<%@ Register TagPrefix="mn" TagName="PhotoGalleryProfile" Src="../../../Applications/PhotoGallery/Controls/PhotoGalleryProfile20.ascx" %>
<%@ Register TagPrefix="mn" TagName="noResults" Src="NoResults.ascx" %>
<%@ Register TagPrefix="mn" TagName="MemberMap" Src="Maps/MemberMap.ascx" %>
<%@ Register tagPrefix="mn" tagName="JmeterMiniProfile" src="~/Applications/CompatibilityMeter/Controls/JMeterMiniProfile.ascx" %>
<input id="hidStartRow" type="hidden" name="hidStartRow" runat="server" />
<asp:PlaceHolder ID="plcPromotionalProfile" Runat="server" Visible="false">
<mn:miniprofile runat="server" id="promotionProfile" visible="true" OnlineImageName="icon-status-online.gif" OfflineImageName="icon-status-offline.gif" />
</asp:PlaceHolder>
<!-- begin dynamic member loop -->
	<asp:repeater id="memberRepeater" runat="server" Visible="True" OnItemDataBound="MemberRepeater_OnItemDataBound">
		<itemtemplate>
		 
			<mn:miniprofile runat="server" id="miniProfile" member="<%# Container.DataItem %>" OnlineImageName="icon-status-online.gif" OfflineImageName="icon-status-offline.gif" />
			  
			
		</itemtemplate>
	</asp:repeater>
	<asp:repeater id="galleryMemberRepeater" runat="server" Visible="False" OnItemDataBound="GalleryMemberRepeater_OnItemDataBound">
		<itemtemplate>
		   
                <mn:GalleryMiniProfile20 runat="server" id="GalleryMiniProfile" member="<%# Container.DataItem %>" OnlineImageName="icon-status-online.gif" OfflineImageName="icon-status-offline.gif" />
            
		</itemtemplate>
	</asp:repeater>
	<asp:repeater id="photoGalleryRepeater" runat="server" Visible="False" OnItemDataBound="PhotoGalleryRepeater_OnItemDataBound"  >
		<itemtemplate>
		
				<mn:PhotoGalleryProfile runat="server" id="PhotoGalleryProfile"  ResultMember="<%# Container.DataItem %>" />

		</itemtemplate>
	</asp:repeater>
    <asp:repeater id="jmeterRepeater" runat="server" Visible="False" OnItemDataBound="JMeterRepeater_OnItemDataBound">
		<itemtemplate>
		 
			<mn:JmeterMiniProfile runat="server" id="jmeterMiniProfile" member="<%# Container.DataItem %>" OnlineImageName="icon-status-online.gif" OfflineImageName="icon-status-offline.gif" />
			  
			
		</itemtemplate>
	</asp:repeater>
<!-- end dynamic member loop -->

<%--various copies to display in place of results--%>
<mn:noResults runat="server" id="noSearchResults" visible="False" />

<asp:PlaceHolder ID="phMembersOnlineNoResults" Runat="server" Visible="False">
    <mn:Txt runat="server" expandImageTokens="true" id="txtMOL" ResourceConstant="NO_MEMBERS_ONLINE_FOUND_MATCHING_YOUR_SEARCH_CRITERIA__519973" />
</asp:PlaceHolder>

<mn:Txt id="noUsersText" Visible="false" Runat="server"></mn:Txt>	

<asp:PlaceHolder ID="phViewedYourProfileForSubscribersOnlyCopy" runat="server" Visible="false">
    <div class="blurred-area">
        <div class="modal-over-blurred">
		    <mn2:FrameworkLiteral ID="litViewedYourProfileForSubscribersOnly" runat="server" ResourceConstant="TXT_VIEWED_YOUR_PROFILE_FOR_SUBSCRIBERS_ONLY_DEFAULT_COPY"></mn2:FrameworkLiteral>
	    </div>
        <mn:Image ID="imgBlurredFemale" FileName="img-results-blurred-female.jpg" runat="server" />
        <mn:Image ID="imgBlurredMale" FileName="img-results-blurred-male.jpg" runat="server" Visible="False" />
    </div>
</asp:PlaceHolder>

<iframe tabindex="-1" name="FrameYNMVote" frameborder="0" width="1" scrolling="no" height="1">
	<layer name="FrameYNMVote" frameborder="0" scrolling="no" width="1" height="1"></layer>
</iframe>