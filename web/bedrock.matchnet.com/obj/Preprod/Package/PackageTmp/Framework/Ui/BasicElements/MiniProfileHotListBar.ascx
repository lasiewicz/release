<%@ Control Language="c#" AutoEventWireup="false" Codebehind="MiniProfileHotListBar.ascx.cs" Inherits="Matchnet.Web.Framework.Ui.BasicElements.MiniProfileHotListBar" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>

<mn:Image ID="FriendIconImage" Runat="server" Visible=false />
<mn:Image ID="ContactedIconImage" Runat="server" Visible=false/>
<mn:Image ID="TeaseIconImage" Runat="server" Visible=false/>
<mn:Image ID="IMIconImage" Runat="server" Visible=false/>
<mn:Image ID="ViewProfileIconImage" Runat="server" Visible=false/>
<mn:Image ID="OmnidateIconImage" runat="server" Visible="false" />
