﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SpotlightProfile.ascx.cs" Inherits="Matchnet.Web.Framework.Ui.ProfileElements.SpotlightProfile" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="uc1" TagName="LastLoginDate" Src="../BasicElements/LastLoginDate.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Add2List" Src="../BasicElements/Add2List.ascx" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="uc3" TagName="SpotlightInfoDisplay" Src="../BasicElements/SpotlightInfoDisplay20.ascx" %>

<script type="text/javascript">
    function SpotlightProfile_ProcessYNM(parentClientID, type, encryptedParams) {
        if (ConfirmYNMVote(parentClientID, type)) {
            SaveYNMVote(type, encryptedParams);

            $j('#<%=secretAdmirerPopup.ClientID%>').css("display", "none");

            $j('#<%=secretAdmirerPopup_desc.ClientID%>').css("display", "none");
            $j('#<%=secretAdmirerPopup_descY.ClientID%>').css("display", "none");
            $j('#<%=secretAdmirerPopup_descN.ClientID%>').css("display", "none");
            $j('#<%=secretAdmirerPopup_descM.ClientID%>').css("display", "none");
            $j('#<%=secretAdmirerPopup_descYY.ClientID%>').css("display", "none");
            $j('#<%=spanSecretAdmirerY.ClientID%>').css("display", "none");
            $j('#<%=spanSecretAdmirerN.ClientID%>').css("display", "none");
            $j('#<%=spanSecretAdmirerM.ClientID%>').css("display", "none");
            $j('#<%=spanSecretAdmirerYY.ClientID%>').css("display", "none");
            $j('#<%=spanYes.ClientID%>').removeClass().addClass('spr-btn sbtn-click20-yes-sm-off');
            $j('#<%=spanNo.ClientID%>').removeClass().addClass('spr-btn sbtn-click20-no-sm-off');
            $j('#<%=spanMaybe.ClientID%>').removeClass().addClass('spr-btn sbtn-click20-maybe-sm-off');

            var currentStatus = $j('#<%=YNMVoteStatus.ClientID%>');
            var ynmType = '';
            switch (type) {
                case "1":
                    ynmType = "Y";
                    if ((currentStatus.val() & 2) == 2) {
                        currentStatus.val(currentStatus.val() ^ 2);
                    }
                    if ((currentStatus.val() & 4) == 4) {
                        currentStatus.val(currentStatus.val() ^ 4);
                    }
                    currentStatus.val(currentStatus.val() | 1);

                    //If the other user has clicked yes, display the "you both clicked yes" icon:
                    if ((currentStatus.val() & 8) == 8) {
                        $j('#<%=secretAdmirerPopup_descYY.ClientID%>').css("display", "block");
                        $j('#<%=spanSecretAdmirerYY.ClientID%>').css("display", "inline-block");
                    }
                    else {
                        $j('#<%=secretAdmirerPopup_descY.ClientID%>').css("display", "block");
                        $j('#<%=spanSecretAdmirerY.ClientID%>').css("display", "inline-block");
                    }
                    $j('#<%=spanYes.ClientID%>').removeClass().addClass('spr-btn sbtn-click20-yes-sm-on');

                    break;

                case "2":
                    ynmType = "N";
                    if ((currentStatus.val() & 1) == 1) {
                        currentStatus.val(currentStatus.val() ^ 1);
                    }
                    if ((currentStatus.val() & 4) == 4) {
                        currentStatus.val(currentStatus.val() ^ 4);
                    }
                    currentStatus.val(currentStatus.val() | 2);

                    $j('#<%=secretAdmirerPopup_descN.ClientID%>').css("display", "block");
                    $j('#<%=spanSecretAdmirerN.ClientID%>').css("display", "inline-block");
                    $j('#<%=spanNo.ClientID%>').removeClass().addClass('spr-btn sbtn-click20-no-sm-on');
                    break;

                case "3":
                    ynmType = "M";
                    if ((currentStatus.val() & 1) == 1) {
                        currentStatus.val(currentStatus.val() ^ 1);
                    }
                    if ((currentStatus.val() & 2) == 2) {
                        currentStatus.val(currentStatus.val() ^ 2);
                    }
                    currentStatus.val(currentStatus.val() | 4);

                    $j('#<%=secretAdmirerPopup_descM.ClientID%>').css("display", "block");
                    $j('#<%=spanSecretAdmirerM.ClientID%>').css("display", "inline-block");
                    $j('#<%=spanMaybe.ClientID%>').removeClass().addClass('spr-btn sbtn-click20-maybe-sm-on');
                    break;
            }

            //update omniture
            if (s != null) {
                PopulateS(true); //clear existing values in omniture "s" object

                //s.events = "event41";
                s.prop30 = ynmType + " - Mini-Profile";
                s.prop31 = '<%= Member.MemberID %>';
                s.t(); //send omniture updated values as page load
            }
        }
    }

    function SpotlightProfile_ToggleYNMPopup() {
        $j('#<%=secretAdmirerPopup.ClientID%>').toggle();
    }
</script>
<asp:Literal ID="plcPixel" runat="server"></asp:Literal>

<div class="profile30-spotlight clearfix">
    <div class="header">
        <h2 class="title">
            <mn:Txt ID="txtSpotlight" runat="server" ResourceConstant="SPOTLIGHT_TITLE" />
        </h2>
        <uc3:SpotlightInfoDisplay runat="server" ID="ucSpotlightInfoDisplay1" ShowImageNoText="false"
                Visible="false" TextResourceConstant="TXT_WHAT_IS_THIS" />
        <asp:PlaceHolder ID="plcSpotlightSettings" runat="server" Visible="false">
            <a href="/Applications/MemberServices/PremiumServiceSettings.aspx">
                <strong><mn:Txt ID="txtlnkSpotlightSettings" runat="server" ResourceConstant="SPOTLIGHT_SETTINGS" /></strong>
            </a>
        </asp:PlaceHolder>
    </div>
    <div class="picture">
        <%--large thumbnail--%>
        <mn:Image ID="ImageThumb" runat="server" Width="80" Height="104" />
        <asp:PlaceHolder ID="phColorCode" runat="server" Visible="false">
            <div class="cc-pic-tag cc-pic-tag-sm cc-pic-<%=_ColorText.ToLower() %>">
                <span><%=_ColorText %></span>
            </div>
        </asp:PlaceHolder>
    </div>

    <div class="info">
        <%--username and icons--%>
        <h3>
            <asp:HyperLink ID="lnkUserName" runat="server" />
            <mn:Image ID="ImageIsNew" runat="server" FileName="icon-new-member.gif" Visible="false" TitleResourceConstant="ALT_NEW_MEMBER" />
            <mn:Image ID="ImageIsUpdate" runat="server" FileName="icon-updated.gif" Visible="false" TitleResourceConstant="ALT_UPDATED_MEMBER" />
        </h3>
        <h4><%--age and location--%>
            <asp:Literal ID="TextInfo" runat="server" />
        </h4>
        <%--small thumbnails--%>
        <asp:Repeater ID="rptPhotos" runat="server">
            <ItemTemplate>
                <mn:Image ID="imgTinyThumb" runat="server" Width="35" Height="43" />
            </ItemTemplate>
        </asp:Repeater>
    </div>
    
    <div class="details">
        <%--We both like text and more link--%>
        <mn:Txt runat="server" ID="txtBothLike" ResourceConstant="TXT_BOTH_LIKE" Visible="false"
            ExpandImageTokens="true" />
        <asp:Literal ID="TextHeadline" runat="server" />
        <mn:Txt runat="server" ID="txtMore" />
    </div>
    <div class="profile30-comm">
        <%--Communication links--%>
        <ul class="clearfix">
            <li class="email">
                <asp:HyperLink ID="lnkEmail" runat="server">
                    <mn:Txt ID="txtEmail" ResourceConstant="PRO_EMAIL" TitleResourceConstant="ALT_EMAIL_ME" runat="server" />
                </asp:HyperLink>
            </li>
            <li class="online">
                <!--online-->
                <asp:HyperLink ID="lnkIMOnline" runat="server">
                    <mn:Txt ID="txtIMOnline" ResourceConstant="PRO_IM" TitleResourceConstant="ALT_IM_ONLINE" runat="server" />
                </asp:HyperLink>
                <!--offline-->
                <mn:Txt ID="txtIMOffline" ResourceConstant="PRO_IM" TitleResourceConstant="ALT_IM_OFFLINE" runat="server" CssClass="offline" />
            </li>
            <li class="flirt">
                <%--<asp:HyperLink ID="lnkFlirt" runat="server">--%>
                <asp:HyperLink ID="lnkFlirt" runat="server">
                    <mn:Txt ID="txtFlirt" ResourceConstant="PRO_TEASE" TitleResourceConstant="ALT_TEASE_ME" runat="server" />
                </asp:HyperLink>
            </li>
            <asp:PlaceHolder ID="phEcardLink" runat="server" Visible="false">
            <li class="ecard">
                <asp:HyperLink ID="lnkEcard" runat="server">
                    <mn:Txt ID="txtEcard" ResourceConstant="PRO_ECARDS" TitleResourceConstant="ALT_ECARDS" runat="server" />
                </asp:HyperLink>
            </li>
            </asp:PlaceHolder>
            <asp:PlaceHolder ID="phSecretAdmirer" runat="server">
            <li class="secret-admirer-short">
                <div class="bubble-area trigger-on-click">
                <asp:HyperLink ID="lnkSecretAdmirer" runat="server" CssClass="trigger" NavigateUrl="#">
                    <span id="spanSecretAdmirerY" runat="server" style="display:none;" class="spr s-icon-click-y-on-sm"><span><mn:Txt ID="txt1" runat="server" ResourceConstant="YNM_Y" /></span></span>
                    <span id="spanSecretAdmirerN" runat="server" style="display:none;" class="spr s-icon-click-n-on-sm"><span><mn:Txt ID="txt2" runat="server" ResourceConstant="YNM_N" /></span></span>
                    <span id="spanSecretAdmirerM" runat="server" style="display:none;" class="spr s-icon-click-m-on-sm"><span><mn:Txt ID="txt3" runat="server" ResourceConstant="YNM_M" /></span></span>
                    <span id="spanSecretAdmirerYY" runat="server" style="display:none;" class="spr s-icon-click-yy-on-sm"><span><mn:Txt ID="txt4" runat="server" ResourceConstant="YNM_YY" /></span></span>
                    <mn:Txt ID="txtSeccretAdmirer" ResourceConstant="PRO_SECRETADMIRER" TitleResourceConstant="ALT_SECRETADMIRER" runat="server" />
                </asp:HyperLink>
                <div id="secretAdmirerPopup" runat="server" style="display:none" class="secret-admirer bubble-layer bottom">
                    <h2 id="secretAdmirerPopup_title"><mn2:FrameworkLiteral ID="literalSecretAdmirerTitle" ResourceConstant="TXT_SECRETADMIRER_TITLE" runat="server"></mn2:FrameworkLiteral></h2>
                    <p id="secretAdmirerPopup_desc" style="display:none" runat="server" class="description"><mn2:FrameworkLiteral ID="literalSecretAdmirerDesc" ResourceConstant="TXT_SECRETADMIRER_DESC" runat="server"></mn2:FrameworkLiteral></p>
                    <p id="secretAdmirerPopup_descY" style="display:none" runat="server" class="description"><mn2:FrameworkLiteral ID="literalSecretAdmirerDescY" ResourceConstant="TXT_SECRETADMIRER_DESC_YES" runat="server"></mn2:FrameworkLiteral></p>
                    <p id="secretAdmirerPopup_descN" style="display:none" runat="server" class="description"><mn2:FrameworkLiteral ID="literalSecretAdmirerDescN" ResourceConstant="TXT_SECRETADMIRER_DESC_NO" runat="server"></mn2:FrameworkLiteral></p>
                    <p id="secretAdmirerPopup_descM" style="display:none" runat="server" class="description"><mn2:FrameworkLiteral ID="literalSecretAdmirerDescM" ResourceConstant="TXT_SECRETADMIRER_DESC_MAYBE" runat="server"></mn2:FrameworkLiteral></p>
                    <p id="secretAdmirerPopup_descYY" style="display:none" runat="server" class="description"><mn2:FrameworkLiteral ID="literalSecretAdmirerDescYY" ResourceConstant="TXT_SECRETADMIRER_DESC_MUTUALYES" runat="server"></mn2:FrameworkLiteral></p>
    	            
                    <div class="click20-horizontal">
                        <div class="click20-option">
                            <asp:HyperLink ID="lnkYes" runat="server">
                                <span id="spanYes" runat="server" class="spr-btn sbtn-click20-yes-sm-off"><span><mn:Txt ID="txt5" runat="server" ResourceConstant="YNM_Y" /></span></span>
                            </asp:HyperLink>
                        </div>
                        <div class="click20-option">
                            <asp:HyperLink ID="lnkNo" runat="server">
                                <span id="spanNo" runat="server" class="spr-btn sbtn-click20-no-sm-off"><span><mn:Txt ID="txt6" runat="server" ResourceConstant="YNM_N" /></span></span>
                            </asp:HyperLink>
                        </div>
                        <div class="click20-option">
                            <asp:HyperLink ID="lnkMaybe" runat="server">
                                <span id="spanMaybe" runat="server" class="spr-btn sbtn-click20-maybe-sm-off"><span><mn:Txt ID="txt7" runat="server" ResourceConstant="YNM_M" /></span></span>
                            </asp:HyperLink>
                        </div>
                        
                        <%--input to hold current vote status, used by js--%>
                        <input id="YNMVoteStatus" type="hidden" name="YNMVoteStatus" runat="server"/>
                    </div>
                </div>
                </div>
            </li>
            </asp:PlaceHolder>
            <li class="favorite">
                <uc1:Add2List id="add2List" runat="server" 
                    ClearIconInlineAlignments="true" 
                    Orientation = "Horizontal"
                    HotlistResourceConstant = "TXT_FAVORITE"
                    UnHotlistResourceConstant = "TXT_UNFAVORITE"
                    IncludeSpanAroundDescriptionText="true" 
                    ExcludeBlockAsFavorited = "true"
                    DisplayLargeIcon = "false" 
                    EnableSprite = "true"
                    SpriteHotlistCSS = ""
                    SprintUnHotlistCSS = "spr s-icon-favorites-added">
                </uc1:Add2List>
            </li>
        </ul>           
    </div>

</div>
