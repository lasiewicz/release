using System;
using System.ComponentModel;
using System.Web;
using Matchnet.List.ValueObjects;
using Matchnet.Content.ServiceAdapters;

namespace Matchnet.Web.Framework.Ui.BasicElements
{
	/// <summary>
	/// Summary description for YNMVoteButton.
	/// </summary>
	public class YNMVoteButton : Matchnet.Web.Framework.Image
	{
		private YNMVoteType _type;
		private string _selectedFileName;
   
		private string _extraJavaScriptToRun;
		private int _memberID;
        private string _source;

		public enum YNMVoteType 
		{
			Yes, No, Maybe			
		}

		public YNMVoteButton()
		{
			//TOFIX: Review whether the code below is needed since this is done in the base class.
			if ( Context != null )
			{
				if ( Context.Items["g"] != null )
					_g = (ContextGlobal) Context.Items["g"];
			}
			else
			{
				_g = new ContextGlobal( null );
			}

			if (_g.Member == null)
			{
				this.Visible = false;
			}
			else
			{
				this.PreRender += new EventHandler(YNMVoteButton_PreRender);
			}
		}

		#region Properties

		[Bindable(true), 
		Category("Appearance"), 
		DefaultValue("")] 
		public string Type
		{
			get
			{
				return _type.ToString();
			}
			set
			{
				_type = (YNMVoteType)Enum.Parse( typeof(YNMVoteType), value, true );
			}
		}

        public string Source
        {
            get
            {
                return _source;
            }
            set
            {
                _source = value;
            }
        }

		[Bindable(true), 
		Category("Appearance"), 
		DefaultValue("")] 
		public string SelectedFileName
		{
			get
			{
				return _selectedFileName;
			}
			set
			{
				_selectedFileName = value;
			}
		}

      
		[Bindable(true), 
		Category("Appearance"), 
		DefaultValue("")] 
		public string ExtraJavaScriptToRun
		{
			get
			{
				return _extraJavaScriptToRun;
			}
			set
			{
				_extraJavaScriptToRun = value;
			}
		}

		public int MemberID
		{
			get
			{
				return _memberID;
			}
			set
			{
				_memberID = value;
			}
		}


		#endregion

		private void YNMVoteButton_PreRender(object sender, EventArgs e)
		{
			ClickMask clickMask = _g.List.GetClickMask(_g.Brand.Site.Community.CommunityID, _g.Brand.Site.SiteID, _memberID);
			ClickMask currentMask;
			int currentButton;  //Used by Javascript YNMVote function to determine which button was pressed

			//if (_type == null)
			//	_type = String.Empty;

			switch ( _type )
			{
				case YNMVoteType.Yes:
					currentMask = ClickMask.MemberYes;
					currentButton = 1;
					break;
				case YNMVoteType.No:
					currentMask = ClickMask.MemberNo;
					currentButton = 2;
					break;
				case YNMVoteType.Maybe:
					currentMask = ClickMask.MemberMaybe;
					currentButton = 3;
					break;
				default:
					throw new Exception("Invalid Type attribute supplied to YNMVoteButton.  Type must be 'yes', 'no', or 'maybe'.");
			}

            if ((clickMask & currentMask) == currentMask)
            {
                base.FileName = _selectedFileName;
            }
           

			YNMVoteParameters parameters = new YNMVoteParameters();

			parameters.SiteID = _g.Brand.Site.SiteID;
			parameters.CommunityID = _g.Brand.Site.Community.CommunityID;
			parameters.FromMemberID = _g.Session.GetInt(WebConstants.SESSION_PROPERTY_NAME_MEMBERID);
			parameters.ToMemberID = _memberID;

			string encodedParameters = parameters.EncodeParams();
			NavigateUrl = string.Format("javascript:YNMVote('{0}','{1}','{2}','{3}');", NamingContainer.ClientID, currentButton, encodedParameters, _source);
			if( _extraJavaScriptToRun != null )
				NavigateUrl += _extraJavaScriptToRun;

            if (_g.AppPage.ControlName.ToLower() == "viewprofilefbtab")
            {
                string host = HttpContext.Current.Request.Url.Host;
                NavigateUrl = "http://" + host +
                            "/Applications/MemberProfile/ViewProfile.aspx?prm=80633&mailtypeid=1&MemberID=" +
                            _memberID;
            }

			//Get existing onMouseOver/onMouseOut attributes if they exist
			string mouseOver = Attributes["onMouseOver"];
			string mouseOut = Attributes["onMouseOut"];

			//If this image already has onMouseOver/onMouseOut attributes, we'll append a ';' to them
			//Otherwise just create new strings
			if (mouseOver == null)
				mouseOver = String.Empty;
			else
				mouseOver += "; ";

			if (mouseOut == null)
				mouseOut = String.Empty;
			else
				mouseOver += "; ";

			//Now append the JavaScript function call to do the image swap
			mouseOver += string.Format("YNMMouseOver(this, '{0}', '{1}')", NamingContainer.ClientID, (int) currentMask);
			mouseOut += string.Format("YNMMouseOut(this, '{0}', '{1}')", NamingContainer.ClientID, (int) currentMask);
			
			Attributes.Add("onMouseOver", mouseOver);
			Attributes.Add("onMouseOut", mouseOut);

			
		}
	}
}
