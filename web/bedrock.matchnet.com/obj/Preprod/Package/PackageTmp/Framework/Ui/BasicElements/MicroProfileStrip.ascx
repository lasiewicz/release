﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MicroProfileStrip.ascx.cs" Inherits="Matchnet.Web.Framework.Ui.BasicElements.MicroProfileStrip" %>
<%@ Register Src="/Framework/Ui/BasicElements/MicroProfile20.ascx" TagName="MicroProfile20" TagPrefix="uc2" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>

<asp:PlaceHolder ID="phNoResultDisplay" runat="server" Visible="false">
    <mn:Txt ID="noResultDisplay" runat="server" ResourceConstant="" ExpandImageTokens="true"/>
</asp:PlaceHolder>

<asp:PlaceHolder ID="phMember" runat="server" Visible="true">
    <asp:PlaceHolder ID="phMember2" runat="server" Visible="true">
        <uc2:MicroProfile20 ID="memberProfile21" runat="server" Visible="false" DisplayContext="Default"/>
    </asp:PlaceHolder>
    
    <asp:PlaceHolder ID="phMember1" runat="server" Visible="true">
        <uc2:MicroProfile20 ID="memberProfile11" runat="server" Visible="false" DisplayContext="Default" />
    </asp:PlaceHolder>
    
    <asp:PlaceHolder ID="phMember3" runat="server" Visible="true">
        <uc2:MicroProfile20 ID="memberProfile31" runat="server" Visible="false" DisplayContext="Default" />
    </asp:PlaceHolder>
    
<%--    <asp:PlaceHolder ID="phMember4" runat="server" Visible="true">
        <uc2:MicroProfile20 ID="memberProfile41" runat="server" Visible="false" DisplayContext="Default" />
    </asp:PlaceHolder>--%>
</asp:PlaceHolder>