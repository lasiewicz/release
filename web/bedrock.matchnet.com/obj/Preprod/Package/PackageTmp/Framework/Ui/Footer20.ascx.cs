﻿using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ServiceAdapters.Links;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Web.Framework.Util;
using Matchnet.Web.Applications.ColorCode;


namespace Matchnet.Web.Framework.Ui
{
    /// <summary>
    ///		Summary description for Footer.
    /// </summary>
    partial class Footer20 : FrameworkControl
    {
        #region Static Form Elements
        protected Matchnet.Web.Framework.Txt txtExplanation;
        protected Matchnet.Web.Framework.Txt txtHome;
        //protected Matchnet.Web.Framework.Txt txtOurMission;
        //protected Matchnet.Web.Framework.Txt txtEvents;
        protected Matchnet.Web.Framework.Txt txtMembership;
        //protected Matchnet.Web.Framework.Txt txtInTheNews;
        //protected Matchnet.Web.Framework.Txt txtHelp;
        //protected Matchnet.Web.Framework.Txt txtPrivacy;
        //protected Matchnet.Web.Framework.Txt txtJMAG;
        //protected Matchnet.Web.Framework.Txt txtTerms;
        //protected Matchnet.Web.Framework.Txt txtDatingSafety;
        //protected Matchnet.Web.Framework.Txt txtAdWithUs;
        protected Matchnet.Web.Framework.Txt txtSubscribe;
        protected Matchnet.Web.Framework.Txt txtContactUs;
        protected Matchnet.Web.Framework.Txt txtMemberServices;
        protected Matchnet.Web.Framework.Txt txtFooterHtml;
        //protected PlaceHolder plcDateGuide;
        //protected System.Web.UI.HtmlControls.HtmlAnchor lnkAbout;

        //protected System.Web.UI.WebControls.PlaceHolder plcOurMission;
        //protected System.Web.UI.WebControls.PlaceHolder plcEvents;
        protected PlaceHolder BreadCrumbPHFooter;
        //protected Matchnet.Web.Framework.Txt txtAbout;
        protected Matchnet.Web.Framework.Txt Txt12;
        //protected Matchnet.Web.Framework.Txt txtInvestorRelations;
        //protected Matchnet.Web.Framework.Txt txtJobs;
        //protected Matchnet.Web.Framework.Txt txtMediaRelations;
        //protected Matchnet.Web.Framework.Txt txtAffiliateProgram;
        protected Matchnet.Web.Framework.Txt txtTrademark;
        protected Matchnet.Web.Framework.Txt txtPatentNotice;
        //protected Matchnet.Web.Framework.Txt lnkStates;

        protected PlaceHolder phlSubscriptionDisplay;

        protected PlaceHolder plcFooter;
        protected PlaceHolder plcAbout;
        protected PlaceHolder plcContactUs;
        protected PlaceHolder plcMembership;
        protected PlaceHolder plcMemberServices;
        protected PlaceHolder plcSubscribe;
        /*protected PlaceHolder plcDatingSafety;
        protected PlaceHolder plcPrivacy;
        protected PlaceHolder plcJMAG;
        protected PlaceHolder phInvestorRelations;
        protected PlaceHolder plcMemberDirectory;
        */
        protected PlaceHolder plcFooterExplanation;

        protected Literal litSiteName;
        protected Literal ynmConfirm;
        //protected System.Web.UI.WebControls.PlaceHolder plcAdWithUs;

        protected Panel PanelActivatePageObjects;
        protected System.Web.UI.WebControls.PlaceHolder plcFooterHtml;

        public enum FooterDisplayType
        {
            Default, Splash
        }

        protected FooterDisplayType _contextType;

        public FooterDisplayType ContextType
        {
            get
            {
                return _contextType;
            }
            set
            {
                _contextType = value;
            }
        }

        #endregion

        public void HideFooterLinks()
        {
            plcFooter.Visible = false;
            plcFooterIL.Visible = false;
        }

        private void Page_Init(object sender, EventArgs e)
        {
            try
            {
                //if (RuntimeSettings.GetSetting("BREADCRUMBS_ON_REDESIGN_FLAG", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID).ToLower() == "true")
                //{
                //    BreadCrumbPHFooter.Controls.Add(g.BreadCrumbTrailFooter);
                //}

            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            if (g.Head20 != null)
            {
                PanelActivatePageObjects.Visible = !g.Head20.IsBlank;
                TxtPreloadImages.Visible = !g.Head20.IsBlank;
                phColorCodeHelpContainer.Visible = !g.Head20.IsBlank && ColorCodeHelper.IsColorCodeEnabled(g.Brand);
            }
            base.OnPreRender(e);
        }

        private void Page_Load(object sender, System.EventArgs e)
        {
            try
            {
                if (ContextType == FooterDisplayType.Splash)
                { BreadCrumbPHFooter.Visible = false; }

                if (!IsPostBack)
                { DataBind(); }

                if (g.AppPage.ControlName.ToLower() == "astroview")
                { PanelActivatePageObjects.Visible = false; }

                if (g.Brand.IsPaySite)
                { plcSubscribe.Visible = true; }

                if (FrameworkGlobals.IsMingleSite(g.Brand.Site.Community.CommunityID) && g.Member == null)
                {
                    plcMemberServices.Visible = false;
                    plcMembership.Visible = false;
                }

                setupLink(txtHome, true, false);
                txtHome.Href = FrameworkGlobals.GetHomepageAbsURL(g, Request.ServerVariables.Get("HTTP_HOST"));

                setupLink(txtMembership, true, false);
                setupLink(txtSubscribe, true, true);
                setupLink(txtContactUs, true, false);
                setupLink(txtMemberServices, true, false);

                string htmlBlob = g.GetResource("FOOTER_HTML", this);
                txtFooterHtml.Visible = (htmlBlob != null && htmlBlob != String.Empty);

                //color code
                phColorCodeHelpContainer.Visible = ColorCodeHelper.IsColorCodeEnabled(g.Brand);
                if (g.Member != null && ColorCodeHelper.HasMemberCompletedQuiz(g.Member, g.Brand))
                {
                    phColorCodeModalTakeTest.Visible = false;
                }

                if (g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.JDateCoIL ||
                   g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.JDateUK ||
                    g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.JDateFR ||
                    g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.Cupid)
                {
                    plcFooter.Visible = false;
                    plcFooterIL.Visible = true;
                    setupLink(txtSubscribeIL, true, true);
                    setupLink(txtHomeIL, true, false);
                    setupLink(txtMembershipIL, true, false);
                    setupLink(txtContactUsIL, true, false);
                    setupLink(txtMemberServicesIL, true, false);
                    copyright.Visible = false;
                    txtHelpILCupid.Visible = false;

                    if (g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.Cupid)
                    {
                        setupLink(txtSubscribeILCupid, true, true);
                        txtSubscribeIL.Visible = false;
                        txtHelpILCupid.Visible = true;
                    }

                }
                else if (g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.AmericanSingles)
                {
                    plcFooter.Visible = false;
                    plcFooterIL.Visible = false;
                    plcFooterSpark.Visible = true;
                    setupLink(txtSubscribeSpark, true, true);
                    setupLink(txtHomeSpark, true, false);
                    setupLink(txtMembershipSpark, true, false);
                    setupLink(txtContactUsSpark, true, false);
                    setupLink(txtMemberServicesSpark, true, false);
                    copyright.Visible = false;
                }
                if (g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.JDateCoIL)
                {
                    plcSuccessStoriesLink.Visible = true;
                }
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        private void setupLink(Matchnet.Web.Framework.Txt txtLink, bool checkForHttps, bool ssl)
        {
            // Man... ( we don't need to do SSL either, only UPS should be using it)
            if ((txtLink.ID == "txtHomeSpark" || txtLink.ID == "txtHomeIL" || txtLink.ID == "txtHome")
                && (g.Member != null && g.AppPage.ControlName.ToLower().IndexOf("logoff") == -1))
            {
                txtLink.Href = "/Applications/Home/Default.aspx";
                return;
            }

            string url = g.GetResource(txtLink.ResourceConstant + "_URL", this);
            if (!ssl)
            { txtLink.Href = FrameworkGlobals.LinkHref(url, checkForHttps); }
            else
            { { txtLink.Href = FrameworkGlobals.LinkHrefSSL(url); } }
        }
        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
            this.Init += new System.EventHandler(this.Page_Init);

        }
        #endregion
    }
}
