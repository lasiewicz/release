using System;
using System.Web;
using System.Web.UI;
using System.ComponentModel;

namespace Matchnet.Web.Framework.Ui.FormElements
{
	/// <summary>
	/// Summary description for FrameworkButton.
	/// </summary>
	[DefaultProperty("Resource")]
	public class FrameworkButton : System.Web.UI.WebControls.Button
	{
		// private Translator.Resources _Resource;
        string _ResourceConstant;
        FrameworkControl _resourceControl;

		[System.Security.Permissions.PermissionSet(System.Security.Permissions.SecurityAction.Demand, Name="FullTrust")] 
		protected override void OnInit(EventArgs e)
		{
			base.OnInit(e);
			this.Load+=new EventHandler(Page_Load);
			// Actually load a resource here
			ContextGlobal g = (ContextGlobal)HttpContext.Current.Items["g"];

			// added htmldecode to get html special characters to render like the txt control
			// specifically for registrationwelcome_sp in memberprofile
			// wel 01.18.05
            if(_resourceControl==null)
			    Text = HttpUtility.HtmlDecode((g.GetResource(_ResourceConstant, this)));
            else
                Text = HttpUtility.HtmlDecode((g.GetResource(_ResourceConstant, _resourceControl)));
		}
        protected void Page_Load(object sender, EventArgs e)
        {
            ContextGlobal g = (ContextGlobal)HttpContext.Current.Items["g"];
             if(_resourceControl!=null)
                 Text = HttpUtility.HtmlDecode((g.GetResource(_ResourceConstant, _resourceControl)));
        }
		public string ResourceConstant
		{
			get
			{
				return _ResourceConstant;
			}
			set
			{
				_ResourceConstant = value;
			}
		}


        public FrameworkControl ResourceControl
        {
            get
            {
                return _resourceControl;
            }
            set
            {
                _resourceControl = value;
            }
        }

	}
}
