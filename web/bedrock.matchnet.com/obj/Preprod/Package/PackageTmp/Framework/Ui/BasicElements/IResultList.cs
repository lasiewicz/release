﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI.WebControls;
using System.Collections;
using Matchnet.List.ValueObjects;

namespace Matchnet.Web.Framework.Ui.BasicElements
{
    public interface IResultList
    {
        #region properties
        Int32 StartRow{ get;set;}
        string StartRowClientID{ get;}
        ResultContextType ResultListContextType{ get;set;}
        bool EnableSingleSelect{ get;set;}
        int PageSize{ get;set;}
        int ChapterSize{ get;set;}
        MOCollection MyMOCollection{ get;set;}
        HotListCategory HotListCategory{ get;set;}
        HotListType HotListType{ get;set;}
        HotListDirection HotListDirection { get; set; }
        DataTable Dt { get;  }
        ArrayList TargetMemberIDs { get; set; }
        string GalleryView { get; set; }
        int EndRow { get; set; }
        int TotalRows { get; set; }
        int MemberNum { get; set; }
        List<int> SearchIDs { get; set; }
        bool EnableMultipleSearchViews { get; }
        bool IgnoreSAFilteredSearchResultsCache { get; set; }
        bool IgnoreSASearchResultsCache { get; set; }
        bool IgnoreAllSearchResultsCache { get; set; }
        bool PhotoOnly { get; set; }
        #endregion

        //implementation will return "this" for retrieving resources
        FrameworkControl ThisControl { get; }

        #region controls
        Repeater GalleryMemberRepeater { get;  }
        Repeater MemberRepeater { get;  }
        Repeater JMeterRepeater { get; }
        Repeater PhotoGalleryRepeater { get;  }
        Matchnet.Web.Framework.Ui.BasicElements.NoResults NoSearchResults { get;  }
        PlaceHolder PLCMembersOnlineNoResults { get;  }
        PlaceHolder PLCPromotionalProfile { get; }
       //Matchnet.Web.Framework.Ui.BasicElements.MiniProfile PromotionProfile { get; }
        Txt NoUsersText { get; }
        #endregion
        
        #region methods
        DropDownList GetMOLSortDropDown();
        void RenderAnalytics(ArrayList members, bool moreResultsAvailable);
        #endregion

    }
}
