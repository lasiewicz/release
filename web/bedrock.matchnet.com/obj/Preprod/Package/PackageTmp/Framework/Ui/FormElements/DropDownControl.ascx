<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Control Language="c#" AutoEventWireup="false" Codebehind="DropDownControl.ascx.cs" Inherits="Matchnet.Web.Framework.Ui.FormElements.DropDownControl" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>

<div><!-- begin majortype div -->
	<span <%if (IsCollapsed){%>class="search-pref-closed"<%}else{ %>class="search-pref-open" <%} %> onclick="toggleTags(this.parentNode);return false;">
		<a href="#"><mn:txt id="txtPreferenceName" Runat="server" />:</a>
	</span>
	<span id="divCurrentValue" class="prefSearchSelectedItems" runat="server">
		<asp:Literal ID="litCurrentValue" Runat="server" />
	</span>
	
	<div <% if (IsCollapsed){ %> style="display:none;"<% }%> class="search-pref-option-container" style="padding-bottom: 12px;background-color: white;">	
		<span class="preferencesContainerHeight">
			<p>
				<asp:DropDownList ID="ddlOptions" Runat="server" />
			</p>
		</span>
	</div>
</div><!-- end majortype div -->
