﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="Header20.ascx.cs" Inherits="Matchnet.Web.Framework.Ui.Header20" %>
<%@ Register TagPrefix="mnbe" Namespace="Matchnet.Web.Framework.Ui.BasicElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn" TagName="MiniSearchBar" Src="/Framework/UI/SearchElements/MiniSearchBar.ascx" %>
<%@ Register TagPrefix="mn" TagName="SecretAdmirerGame" Src="/Applications/MemberProfile/ProfileTabs30/Controls/SecretAdmirerGame.ascx" %>

<asp:PlaceHolder ID="DefaultHeader" runat="server" Visible="True" />
<asp:PlaceHolder ID="noNavHeader" runat="server" Visible="False">
    <div id="tblHeaderAlternate" runat="server" class="nav-alternative">
        <mn:Txt ID="txtNoNavTitle" runat="server" />
        <!--<a id="lnkDefault2" runat="server" href="~/default.aspx"></a>
		<mn:Image id="imgDefault" runat="server" titleResourceConstant="ALT_FIND_LOVE_TODAY" ResourceConstant="ALT_FIND_LOVE_TODAY"	FileName="bknd_nav.gif" width="480" height="68" border="0" />
		<mn:Image id="imgLogin" runat="server" titleResourceConstant="ALT_FIND_LOVE_TODAY" ResourceConstant="ALT_FIND_LOVE_TODAY" FileName="log_top_right.gif" width="570" height="68" visible="false" border="0" />
		<mn:Image id="imgRegistration" runat="server" titleResourceConstant="ALT_FIND_LOVE_TODAY" ResourceConstant="ALT_FIND_LOVE_TODAY" FileName="reg_top_right.gif" width="570" height="68" visible="false" border="0" />
		-->
    </div>
</asp:PlaceHolder>

<mn:MiniSearchBar ID="miniSearchBar1" runat="server" visible="false"></mn:MiniSearchBar>
<mn:SecretAdmirerGame ID="secretAdmirerGame0" runat="server" visible="false"></mn:SecretAdmirerGame>
<asp:PlaceHolder ID="phYourProfileMessage" runat="server" Visible="false">
    <%--Message at the top of profile page that spans all the way across, only on edit profile (viewing your own profile)--%>
    <h1 class="self">
        <mn:Txt ID="txtEditProfileMessage" runat="server" ResourceConstant="TXT_YOUR_PROFILE_MESSAGE" />
    </h1>
</asp:PlaceHolder>
<asp:PlaceHolder ID="BreadCrumbPH" runat="server" />
