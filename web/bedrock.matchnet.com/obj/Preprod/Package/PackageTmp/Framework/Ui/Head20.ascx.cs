﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Content.ValueObjects.PageConfig;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Session.ServiceAdapters;
using Matchnet.Web.Analytics;
using Matchnet.Web.Framework;
using System.Text.RegularExpressions;
using Spark.Common.MemberLevelTracking;
using Matchnet.Web.Framework.Managers;
using Matchnet.MembersOnline.ServiceAdapters;

namespace Matchnet.Web.Framework.Ui
{
    public partial class Head20 : FrameworkControl
    {
        const string structCSSSetting = "CSS_STRUCT";
        const string styleCSSSetting = "CSS_STYLE";
        const string FACEBOOK_REG_CSS = "fbReg.{0}.css";

        private bool _includeCSS = true;
        private bool _includeJSLibrary = true;
        private bool _isBlank = false;
        public string JSMessageAttachmentURLPath = "";
        protected string _RESTAuthority = "";
        protected string _IMPrtId = ((int)Content.ServiceAdapters.Links.PurchaseReasonType.ImUpgradeToChatLink).ToString();

        protected Literal litMeta;
        protected Literal litJavascript;
        protected Literal litJavascriptMenu;
        protected Literal litJavascriptMain;
        protected Literal litAdditionalContent;
        protected HtmlGenericControl configurableStyleSheet;
        protected HtmlGenericControl commonStyleSheet;
        protected System.Web.UI.WebControls.PlaceHolder phAslMainScript;
        protected System.Web.UI.WebControls.PlaceHolder PlaceHolderGAM;

        //Constants for javascript and css file locations
        protected const string JAVASCRIPT_FILE_PATH_MAIN = "/javascript20/spark.js";
        //later change from Erik file name

        public PlaceHolder CollageStyle
        {
            get
            {
                return plcCollageStyle;
            }
        }

        protected const string CSS_PATH = "/css/";
        protected const string HREF = "href";

        public bool IncludeCSS
        {
            set
            {
                _includeCSS = value;
            }
        }

        public string PageTitle
        {
            set
            {
                litTitle.Text = value;
            }
        }

        public string MetaDescription
        {
            set { litMetaDescription.Text = value; }
        }

        public string Meta
        {
            set { litMeta.Text = value; }
        }

        public bool IncludeJSLibrary
        {
            set
            {
                _includeJSLibrary = value;
            }
        }

        public bool IsBlank
        {
            get
            {
                return _isBlank;
            }
            set
            {
                _isBlank = value;
            }
        }

        public bool GAMEnableOnly { get; set; }

        public bool IsOnePageReg { get; set; }
        public bool IncludeFacebookLikesScript { get; set; }
        public bool EnableCORS { get; set; }
        public bool IsSparkMOLConsolidationEnabled { get; set; }
        public bool IsSparkWebchatConsolidationEnabled { get; set; }

        private string GetCanonicalURL()
        {
            string url = string.Empty;
            if (_g.IsAPI)
                return "";
            switch (_g.AppPage.PageName)
            {
                case "Splash":
                    url = "http://www." + g.Brand.Uri.ToLower() + "/";
                    break;
                case "SiteMap":
                    if (g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.JDateFR)
                    {
                        url = Request.Url.AbsoluteUri.Split(new char[] { '?' })[0];
                        url = url.Replace("Sitemap", "SiteMap");
                    }
                    else if (g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.JDateCoIL)
                    {
                        url = Request.Url.AbsoluteUri.Split(new char[] { '?' })[0];
                    }
                    break;
                case "articleview":
                    if (g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.JDateCoIL ||
                        g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.JDateFR ||
                        g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.Cupid)
                    {
                        if (Request.Url.AbsoluteUri.Contains("CategoryID=1998&HideNav=True"))
                            url = Request.Url.AbsoluteUri;
                        else if (Request.Url.AbsoluteUri.Contains("CategoryID=114&HideNav=true"))
                            url = Request.Url.AbsoluteUri;
                        else if (Request.Url.AbsoluteUri.Contains("CategoryID=1948&ArticleID=6498&HideNav=True"))
                            url = Request.Url.AbsoluteUri;
                    }
                    break;
                case "articledefault":
                    {
                        if (Request.Url.AbsoluteUri.Contains("CategoryID=2308"))
                            url = Request.Url.AbsoluteUri;
                    }
                    break;
                case "faqmain":
                    if (g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.Cupid || g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.JDateCoIL)
                    {
                        url = Request.Url.AbsoluteUri.Split(new char[] { '?' })[0];
                    }
                    break;
                case "SearchResults":
                    if (!string.IsNullOrEmpty(Request.QueryString["Sector"]))
                    {
                        url = Request.Url.AbsoluteUri;
                    }
                    break;
                case "lobby":
                    url = "http://www." + g.Brand.Uri.ToLower() + Request.RawUrl;
                    break;
                case "SearchPreferences":
                case "MembersOnline":
                case "QuickSearch":
                case "PhotoGallery":
                case "Events":
                case "QuickSearchResults":
                case "error":
                case "logon":
                case "logoff":
                case "contactusconfirmation":
                case "contactus":
                case "searchpreferences":
                case "membersonline":
                case "iconlist":
                case "sitemap":
                case "quicksearchresults":
                case "dontgo":
                case "forgotpassword":
                case "jexperts":
                case "searchresults":
                    url = Request.Url.AbsoluteUri.Split(new char[] { '?' })[0];
                    break;
                default:
                    url = string.Empty;
                    break;
            }
            return url;
        }

        public string GetCanonicalTag()
        {
            string result = string.Empty;
            if (bool.Parse(RuntimeSettings.GetSetting("ENABLE_CANONICAL_URL_TAG", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID)))
            {
                string url = GetCanonicalURL();
                if (!string.IsNullOrEmpty(url))
                {
                    result = string.Format("<link rel=\"canonical\" href=\"{0}\" />", url);
                }
            }

            return result;
        }

        private void Page_Init(object sender, System.EventArgs e)
        {
            // GAM
            if (Convert.ToBoolean(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("GOOGLE_AD_MANAGER", _g.Brand.Site.Community.CommunityID, _g.Brand.Site.SiteID)))
            {
                Advertising.GAM gam = (Advertising.GAM)Page.LoadControl("~/Framework/Ui/Advertising/GAM.ascx");
                PlaceHolderGAM.Controls.Add(gam);
                g.GAM = gam;
            }
        }

        private void Page_Load(object sender, System.EventArgs e)
        {
            //Get Title of HTML page from resources. TODO: expose the Title property and allow dynamic setting of page Title
            litTitle.Text = g.GetResource("TITLE", this);
            litMetaDescription.Text = g.GetResource("META_DESCRIPTION", this);
            litMeta.Text = g.GetResource("META", this);

            if (!_g.IsAPI && _g.AppPage.ControlName == "Splash20")
            {
                litGoogleWebmaster.Text = g.GetResource("GOOGLE_WEBMASTER", this);
                litBingWebmaster.Text = g.GetResource("BING_WEBMASTER", this);
            }

            //check if Hebrew or Non-Hebrew site
            bool hebrew = (g.Brand.Site.LanguageID == (int)Matchnet.Language.Hebrew);

            ////check if this is splash page. TODO: Either use the same common CSS file for the whole site, or make a new template for splash page. That's the point of templates!
            //bool splash = (_g.AppPage.ControlName == "Splash");

            ////get name of setting of appropriate site specific css file from db settings (splash page has different setting name)
            //string cssSetting = splash ? "CSS_SPLASH" : "CSS_MAIN";

            //set source of appropriate site specific css file from db settings, append path in front, and .css and version timestamp behind it
            //configurableStyleSheet.Attributes[HREF] = AppendVersion(GetConfigurableStyleSheetName(cssSetting));

            //set source of main javascript file and append version timestamp behind it
            litJavascriptMain.Text = AppendVersion(JAVASCRIPT_FILE_PATH_MAIN);

            //set source of javascript that controls navigation menu* (* you really shouldn't need to include this on the splash but since the body tag of every page calls a javascript Init() that touches the menu, we need this here - lucena ???)
            // litJavascriptMenu.Text = hebrew ? AppendVersion(JAVASCRIPT_FILE_PATH_MENU_HEBREW) : AppendVersion(JAVASCRIPT_FILE_PATH_MENU_NONHEBREW);

            if (_includeCSS)
            {//temporary hack don't forget to remove and configure thru settings 06/02/2010
                if (g.Brand.Site.SiteID != (int)WebConstants.SITE_ID.AmericanSingles
                    && g.Brand.Site.SiteID != (int)WebConstants.SITE_ID.ItalianSinglesConnection
                    //  && g.Brand.Site.SiteID != (int)WebConstants.SITE_ID.JDateCoIL
                    && g.Brand.Site.SiteID != (int)WebConstants.SITE_ID.BBW
                    && g.Brand.Site.SiteID != (int)WebConstants.SITE_ID.Cupid
                    && g.Brand.Site.SiteID != (int)WebConstants.SITE_ID.BlackSingles)
                {
                    structStyleSheet.Attributes[HREF] = AppendVersion(GetConfigurableStyleSheetName(structCSSSetting));
                }
                else
                {
                    phCSSStructIncludes.Visible = false;
                }
                styleStyleSheet.Attributes[HREF] = AppendVersion(GetConfigurableStyleSheetName(styleCSSSetting));

                if (!string.IsNullOrEmpty(Request.QueryString[WebConstants.URL_PARAMETER_FACEBOOK_REGISTRATION]))
                {
                    styleStyleSheet.Attributes[HREF] = CSS_PATH + AppendVersion(string.Format(FACEBOOK_REG_CSS, g.Brand.Site.SiteID));

                    structStyleSheet.Attributes[HREF] = string.Empty;

                    HttpContext.Current.Response.AddHeader("p3p", "CP=\"IDC DSP COR ADM DEVi TAIi PSA PSD IVAi IVDi CONi HIS OUR IND CNT\"");
                }
            }
            else
            {
                phCSSIncludes.Visible = false;
            }

            phJavascriptLibrary.Visible = _includeJSLibrary;

            //set charset in use for this brand. TODO: this charset meta tag has never been rendered in Bedrock since day 1. It gets overwritten during OnPreRender. Afraid to turn it on when it's never been used ;) See OnPreRender.
            //litMeta.Text = "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=" + g.Brand.Site.Charset.ToString() + "\">";

            if (g.IsSecureRequest())
            {
                phAslMainScript.Visible = false;
            }

            if (g.Brand.Site.Direction == DirectionType.rtl)
            {
                litJavascript.Text = "/javascript20/library/javascript-rtl.js";
            }
            if (!litJavascript.Text.Contains("?v="))
            {
                litJavascript.Text = AppendVersion(litJavascript.Text);
            }

            string enableRedirectionOfMobileDevicesToWAP =
                RuntimeSettings.GetSetting("ENABLE_REDIRECTION_OF_MOBILE_DEVICES_TO_WAP",
                                           g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID).ToLower();
            if (enableRedirectionOfMobileDevicesToWAP != "false")
            {
                if (enableRedirectionOfMobileDevicesToWAP == "client")
                {
                    plcPassCallWAPRedirect.Visible = true;
                }
                else
                {
                    if (Regex.IsMatch(Request.UserAgent, @"(^((SO|Se|ha|[m-p]{2})(NIM|ndo|ier|[h-k]{2}a)|(MO|HT|Pa|EZ)(T-|C|lm|OS)|(SA|po|Ph|UP|Mo|Er|fr|Ic|Bl|So)(MSU|rta|ili|Br|bil|ics|omp|op|ack|nyE)(NG|lmmm|ps|owse|eExp|so|assc|ie|Berr|rics)|(S(EC|GH|IE)|[O-Q][a-d]L\d{2}[a-z]{2}\d+|LG)))|([l-p]{2}[x-z]([(h-m)]|a){4}/((1\.22)|([45]\.0).*?(H|sym|win|pal|ProF|we)(TC_|bia|dows|msou|iLE.+m|b)(HD\d|n\s*os|\sce|rce|IDp|os)))|((q|O|U|SZ)(q|ww|p|O|i)(v|dv|e|ds|OO)(Q|W|R|OO|Z)(V|PP|YY|A).*?\s[k-p](in|OB)[h-j])|([h-j][o-q][g-i][n-p]{2}[d-f])|([ba][na][qd][n-s]{2}[h-j][c-e])", RegexOptions.IgnoreCase) &&
                        !Regex.IsMatch(Request.UserAgent, "([h-j]pa[c-e]|GT-P10{3})", RegexOptions.IgnoreCase))
                    {
                        Response.Redirect("http://nb.jdate.go.passcall.mobi", true);
                    }
                }
            }

            //total hack needed because we had to move some inline styles to an external style sheet so it would 
            //stop breaking when the region control was used, but we don't want to include the styles on every page
            if (g.AppPage.ControlName == "Splash20" && g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.JDate)
            {
                //plcCollageStyle.Visible = true;
            }

            //set message attachment url for js
            JSMessageAttachmentURLPath = ConfigurationManager.AppSettings["MessageAttachmentURLPath"];

            //set url to be used for API which will be included for CORS
            _RESTAuthority = ConfigurationManager.AppSettings["RESTAuthority"];

            txtDynamicYieldHeadCode.Visible = SettingsManager.GetSettingBool("ENABLE_DYNAMIC_YIELD_PIXEL", g.Brand, false);

            plcMediaPrintCSS.Visible = SettingsManager.GetSettingBool("ENABLE_CSS_PRINT_MEDIA", g.Brand, false);

            //display stage or prod consolidated presence scripts
            if (g.Member != null)
            {
                if (g.Environment == String.Empty || g.Environment == WebConstants.ENV_PROD)
                {
                    phConsolidatedPresenceProd.Visible = true;
                }
                else
                {
                    phConsolidatedPresenceDevStage.Visible = true;
                }

                //cookie to support hide/visible IM
                CookieHelper cookieHelper = new CookieHelper();
                int hideMask = g.Member.GetAttributeInt(g.Brand, "HideMask", 0);
                string sparkIMStatusCookie = "spark_im_status";
                string sparkIMStatusCookieValue = cookieHelper.DoesCookieExist(sparkIMStatusCookie) ? cookieHelper.GetCookie(sparkIMStatusCookie, "", false) : "";
                if ((hideMask & (Int32)WebConstants.AttributeOptionHideMask.HideMembersOnline) == (Int32)WebConstants.AttributeOptionHideMask.HideMembersOnline)
                {
                    //add cookie with offline value, if needed
                    if (sparkIMStatusCookieValue != "offline")
                    {
                        cookieHelper.AddCookie(sparkIMStatusCookie, "offline", Constants.NULL_INT, false, "." + g.Brand.Uri, null);
                    }
                }
                else
                {
                    //remove cookie, unless it's already online
                    if (sparkIMStatusCookieValue != "online")
                    {
                        cookieHelper.AddCookie(sparkIMStatusCookie, "online", Constants.NULL_INT, false, "." + g.Brand.Uri, null);
                    }
                }

            }
            IsSparkMOLConsolidationEnabled = MembersOnlineManager.Instance.IsMOLRefresherEnabled(g.Brand);
            IsSparkWebchatConsolidationEnabled = MembersOnlineManager.Instance.IsConsolidatedWebChatEnabled(g.Brand);
            
        }

        // Added for QA. Deprecate this after the first deployment. 11/20/11.
        public string GetMemberLevelTrackingData()
        {
            var data = "";

            if (g.Member != null)
            {
                data = "TrackingReg:" + g.Member.GetAttributeInt(g.Brand, Key.TrackingRegApplication.ToString(), 0)
                       + "," + g.Member.GetAttributeText(g.Brand, Key.TrackingRegOs.ToString(), "")
                       + "," + g.Member.GetAttributeText(g.Brand, Key.TrackingRegFormFactor.ToString(), "");
            }

            var session = "TrackingCurrent:" + Spark.Common.MemberLevelTracking.Application.FullSite.ToString()
                + "," + MemberLevelTrackingHelper.GetOs(Request)
                + "," + MemberLevelTrackingHelper.GetFormFactor(Request);

            return data + Environment.NewLine + session;
        }
        /// <summary>
        /// Appends ?v=[GetLastWriteTimeString] to the given path. (Useful for defeating browser caching when deploying new 
        /// versions (e.g. sparkCommonNonHe.css?v=11172005115104)
        /// </summary>
        /// <param name="path">The relative to site root or relative path to file</param>
        /// <returns></returns>
        protected string AppendVersion(string path)
        {
            return path + "?v=" + FrameworkGlobals.GetLastWriteTimeString(path);
        }

        /// <summary>
        /// Returns site specific css file from db settings (as of this writing), appends path in front and .css behind it
        /// GetSetting will check site-specific then community-specific, and then grab the globaldefault if it doesn't find anything.
        /// </summary>
        /// <param name="cssSettingName">The name of the setting to look up in runtime settings</param>
        /// <returns></returns>
        protected string GetConfigurableStyleSheetName(string cssSettingName)
        {
            string configuredCss = RuntimeSettings.GetSetting(cssSettingName, g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID);

            if (!String.IsNullOrEmpty(configuredCss))
                return CSS_PATH + configuredCss + ".css";
            else
                return "";
        }

        protected override void OnPreRender(EventArgs e)
        {
            // normally go off the _isBlank value but GAMEnableOnly will overrule this value only if it's true
            PlaceHolderGAM.Visible = !_isBlank;
            if (GAMEnableOnly)
                PlaceHolderGAM.Visible = true;

            PlaceHolderBlank.Visible = !_isBlank;
            PlaceHolderSplashCSS.Visible = _isBlank;
            if (IsOnePageReg)
            {
                PlaceHolderSplashCSS.Visible = false;
                plcOnePageReg.Visible = true;
            }

            //TODO: The PageConfig service doesn't have support for MetaDescription and MetaKeywords at this time.
            //Implementation pending per G. Peterson (12/1/04 K. Landrus)
            /*
            if(g.AppPage.WebPage.MetaDescription.Length == 0 && g.AppPage.WebPage.MetaKeywords.Length == 0) 
            {
                if (g.Member != null == false && g.AppPage.Name == "Home") 
                {
                    ltlMeta.Text = g.Translator.Value("META", g.Brand.Site.LanguageID, g.Brand.BrandID);
                }
            } 
            else 
            {
                ltlMeta.Text += "\n<meta name=\"description\" content=\"" + g.AppPage.WebPage.MetaDescription + "\">\n";
                ltlMeta.Text += "<meta name=\"keywords\" content=\"" + g.AppPage.WebPage.MetaKeywords + "\">\n";
            }
            */

            if (g.HeadContent != null && g.HeadContent.Length > 0)
            {
                //This is apparently a chenyard special in use in subscriptions to hold a whole bunch of invisible temporary values. (Adam :-))
                litAdditionalContent.Text = g.HeadContent;
            }

            if (IncludeFacebookLikesScript)
            {
                phFacebookLikes.Visible = true;
            }

            if (EnableCORS)
            {
                phCORS.Visible = true;
            }

            base.OnPreRender(e);
        }

        public string GetMOLCountHelper()
        {
            return MembersOnlineManager.Instance.GetMOLCommunityCount(g.Brand, false).ToString();
        }

        public string GetSecondsSinceReg()
        {
            if (g.Member != null && RuntimeSettings.GetSetting("ENABLE_SECONDS_SINCE_REG_RENDERING", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID).ToLower() == "true")
            {
                DateTime regDate = Convert.ToDateTime(g.Member.GetAttributeDate(g.Brand, "BrandInsertDate"));
                long seconds = (long)DateTime.Now.Subtract(regDate).TotalSeconds;
                return "SecondsSinceReg: " + seconds;
            }
            return string.Empty;
        }

        public string GetShortcutIconURL()
        {
            return FrameworkGlobals.GetShortcutIconURL();
        }

        public string GetPreviewImgURL()
        {
            return this.Image(1, "logo-facebook.png");
        }

        protected string GetGenderText()
        {
            return g.AnalyticsOmniture.Prop18;
        }

        protected string GetSeekingGenderText()
        {
            string seekingGender = string.Empty;

            if (g.Member != null && g.Member.MemberID > 0)
            {
                int genderMask = g.Member.GetAttributeInt(g.Brand, "gendermask") & Matchnet.Lib.ConstantsTemp.GENDERID_FEMALE;
                seekingGender = (genderMask == Matchnet.Lib.ConstantsTemp.GENDERID_FEMALE) ? "Female" : "Male";
            }

            return seekingGender;
        }

        protected string GetAge()
        {
            return FrameworkGlobals.GetAge(g.Member, g.Brand).ToString();
        }

        protected string GetMemberStatus()
        {
            return g.AnalyticsOmniture.Prop22;
        }
        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
            this.Init += new EventHandler(this.Page_Init);
        }
        #endregion
    }
}
