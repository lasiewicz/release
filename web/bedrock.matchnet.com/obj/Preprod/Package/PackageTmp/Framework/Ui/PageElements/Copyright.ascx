<%@ Control Language="c#" AutoEventWireup="false" Codebehind="Copyright.ascx.cs" Inherits="Matchnet.Web.Framework.Ui.PageElements.Copyright" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>

<mn:Txt id="txtTrademark" ResourceConstant="TXT_FOOTER_TRADEMARK" runat="server" ExpandImageTokens="true" />
<mn:Txt id="txtPatentNotice" ResourceConstant="PATENT_NOTICE" runat="server" ExpandImageTokens="true" />
