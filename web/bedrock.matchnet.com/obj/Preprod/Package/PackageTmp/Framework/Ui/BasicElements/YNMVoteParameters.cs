using System;
using System.Web;

using Matchnet.Security;

namespace Matchnet.Web.Framework.Ui.BasicElements
{
	public class YNMVoteParameters
	{
		private const string ENCRYPT_KEY = "137ecfa4";

		private int _fromMemberID;
		private int _toMemberID;
		private int _communityID;
		private int _siteID;

		public void DecodeParams(string encoded) 
		{
			string parameters = Crypto.Decrypt(ENCRYPT_KEY, encoded);
			string[] tokens = parameters.Split(new char[] { ',' });

			if (tokens.Length == 5) 
			{
				try 
				{
					_fromMemberID = Conversion.CInt(tokens[0], Constants.NULL_INT);
					_toMemberID = Conversion.CInt(tokens[1], Constants.NULL_INT);
					_communityID = Conversion.CInt(tokens[2], Constants.NULL_INT);
					_siteID = Conversion.CInt(tokens[3], Constants.NULL_INT);
				} 
				catch { /* ignore */ }
			} // else, we have an invalid YNM vote
		}

		public string EncodeParams() 
		{
			string parameters = _fromMemberID + "," +
				_toMemberID + "," +
				_communityID + "," +
				_siteID + "," +
				DateTime.Now.ToString();

			string encrypted = Crypto.Encrypt(ENCRYPT_KEY, parameters);

			return HttpContext.Current.Server.UrlEncode(encrypted);
		}

		public int FromMemberID  
		{
			get { return _fromMemberID; }
			set { _fromMemberID = value; }
		}

		public int ToMemberID 
		{
			get { return _toMemberID; }
			set { _toMemberID = value; }
		}

		public int CommunityID 
		{
			get { return _communityID; }
			set { _communityID = value; }
		}

		public int SiteID 
		{
			get { return _siteID; }
			set { _siteID = value; }
		}
	}
}
