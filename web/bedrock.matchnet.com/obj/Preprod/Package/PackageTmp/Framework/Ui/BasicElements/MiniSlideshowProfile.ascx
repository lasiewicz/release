﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MiniSlideshowProfile.ascx.cs" Inherits="Matchnet.Web.Framework.Ui.BasicElements.MiniSlideshowProfile" %>
<%@ Register TagPrefix="uc1" TagName="SlideshowPreferences" Src="/Applications/Search/Controls/SlideshowPreferences.ascx" %>
<%@ Register TagPrefix="mnl" Namespace="Matchnet.Web.Framework.Ui.BasicElements.Links"
    Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>

<%--NOTE: Javascript for YNM moved to Slideshow.js--%>
<div id="miniSlideshowContainer" runat="server">
    <div id="secretAdmirer" class="secret-admirer-module clearfix">
	    <div class="inner-wrapper clearfix block-on-load">
            
            <asp:PlaceHolder ID="plcRegularTitle" runat="server">
	            <h2 class="title"><mn:txt id="txtSlideshowTitle" runat="server" resourceconstant="TXT_SLIDESHOW_TITLE" expandimagetokens="false" /></h2>
	            <div class="rel-layer-container">
		            <a href="#" rel="hover"><span class="spr s-icon-help"><span><mn:txt id="txtHelp" runat="server" resourceconstant="TXT_HELP" expandimagetokens="false" /></span></span></a>
		            <div id="ynmHelp" class="ynm-help rel-layer-div">
			            <mn:txt id="txtHelpText" runat="server" resourceconstant="TXT_HELP_TEXT" expandimagetokens="false" />
		            </div>
	            </div>
        	</asp:PlaceHolder>

	        <div id="prefWrapper" class="pref-wrapper">
		        <a title="open form" href="#" class="edit-pref"><mn:txt id="txtChangePreferences2" runat="server" resourceconstant="TXT_CHANGE_PREFERENCES" expandimagetokens="false" /></a>
	        </div>
    	
    	    <div id="theFormDiv" class="pref-form">
		        <div id="closeButton">
			        <a href="#" title="Close" class="pref-close"><mn:txt id="txtChangePreferences" runat="server" resourceconstant="TXT_CHANGE_PREFERENCES" expandimagetokens="false" /></a>
		        </div>
		        <div class="pref-form-cont">
    	                <uc1:SlideshowPreferences runat="server" id="slideshowPreferences" />
			            <%--<mn:Image id="imgSave" runat="server" FileName="btn-save-sm.gif" onclick="SaveSettings();" />--%>
		        </div>
	        </div>
    	    <asp:Panel ID="pnlAdmirer" runat="server">
	            <div id="admirerWrapper" class="admirer-wrapper clearfix">

                    <div class="prof-img-wrapper">
			            <asp:HyperLink ID="lnkPhoto" runat="server">
                            <asp:PlaceHolder ID="phBeautyBeastImage" runat="server" Visible="false">
                                <mn:image id="mnImageBeautyBeast" runat="server" filename="secret-admirer-TBAB-photo.gif" alt="" CssClass="promo" />
                                <%-- Playing cached animated GIF image - http://stackoverflow.com/questions/2831676/animated-gif-not-working-in-firefox-after-cache --%>
                                <script type="text/javascript">
                                    $j("#<%=mnImageBeautyBeast.ClientID %>").attr("src", function (i, val) {
                                        return val + "?t=" + new Date().getTime();
                                    });
                                </script>
                            </asp:PlaceHolder>
                            <mn:Image ID="imgProfile" runat="server" Border="0" class="photo centered" />
                        </asp:HyperLink>
		            </div>
                    <div class="prof-info-wrapper">
                        <asp:PlaceHolder ID="plcShiftedTitle" runat="server" Visible="false">
                            <div class="title-area">
                                <h2 class="title"><mn:txt id="txtSlideshowTitleShifted" runat="server" resourceconstant="TXT_SLIDESHOW_TITLE" expandimagetokens="false" /></h2>
                                <div class="rel-layer-container">
		                            <a href="#" rel="hover"><span class="spr s-icon-help"><span><mn:txt id="txtHelpShifted" runat="server" resourceconstant="TXT_HELP" expandimagetokens="false" /></span></span></a>
		                            <div id="Div1" class="ynm-help rel-layer-div">
			                            <mn:txt id="txtHelpTextShifted" runat="server" resourceconstant="TXT_HELP_TEXT" expandimagetokens="false" />
		                            </div>
	                            </div>
                            </div>
                        </asp:PlaceHolder>
                        <h3 class="prof-info"><asp:HyperLink ID="lnkUserName" runat="server" /> <em><span class="age"><asp:Literal ID="litAge" runat="server"></asp:Literal>,</span> <asp:Literal ID="litLocation" runat="server"></asp:Literal></em></h3>
		                <ul class="ynm-wrapper" runat="server" id="ulButtons">
			                <li><span runat="server" class="click-btn yes" id="spanYes">Yes</span></li>
			                <li><span runat="server" class="click-btn no" id="spanNo">No</span></li>
			                <li><span runat="server" class="click-btn maybe" id="spanMaybe">Maybe</span></li>
		                </ul>
                    </div>
		            
                    <asp:PlaceHolder ID="phBeautyBeastTrackingPixel" runat="server" Visible="false">
                        <%--MediaMind Tracking Pixel--%>
                        <img alt="" src="http://bs.serving-sys.com/BurstingPipe/adServer.bs?cn=tf&c=19&mc=imp&pli=5495811&PluID=0&ord=<%=DateTime.Now.ToString("yyyyMMddHHmmssffff")%>&rtu=-1" />
                    </asp:PlaceHolder>
	            </div>
        	

	            <div class="slideshow-waiting" style="display:none;">
                    <h2 class="text-center"><mn:txt id="mntxt3616" runat="server" resourceconstant="TXT_LOADING_MSG" expandimagetokens="true" /></h2>
                    <mn:image id="mnimage6491" runat="server" filename="ajax-loader.gif" alt="" CssClass="centered" />
                </div>
            </asp:Panel>    
            <asp:Panel ID="pnlSlideshowEnd" runat="server" Visible="false" CssClass="no-match">
                <h2><mn:txt id="mntxt1293" runat="server" resourceconstant="TXT_SLIDESHOW_END_MSG_HEADER" expandimagetokens="true" /></h2>
                <p><mn:txt id="Txt1" runat="server" resourceconstant="TXT_SLIDESHOW_END_MSG_BODY_TXT" expandimagetokens="true" /></p>
            </asp:Panel>
	    </div><!-- /.inner-wrapper -->
    </div><!-- /#secretAdmirer -->
</div>
