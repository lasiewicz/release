<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Control Language="c#" AutoEventWireup="false" Codebehind="GenericMaskControl.ascx.cs" Inherits="Matchnet.Web.Framework.Ui.FormElements.GenericMaskControl" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>

<div>
	<asp:Repeater ID="rptOptions" Runat="server">
		<HeaderTemplate>
			<div><ul class="<%# this.ULClassName %>">
		</HeaderTemplate>
		<ItemTemplate>
			<li><asp:CheckBox id="cbOption" Runat="server" /></li>
		</ItemTemplate>
		<FooterTemplate>
			</ul></div>
		</FooterTemplate>
	</asp:Repeater>	
</div>