﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FooterMobileAnnouncement.ascx.cs" Inherits="Matchnet.Web.Framework.Ui.PageAnnouncements.FooterMobileAnnouncement" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>

<%--Facebook feature prompt--%>
<div id="MobileFeatureAnnouncement" class="feature-announcement sticky">
    <a class="click-close" href="#"><mn:Txt ID="txtClose" ResourceConstant="TXT_CLOSE" runat="server" /></a>
    <blockquote>
        <mn:Txt ID="txtAnnouncementBody" ResourceConstant="TXT_ANNOUNCEMENT_BODY" runat="server" expandImageTokens="True" />
    </blockquote>
</div>

<script type="text/javascript">
    var featureAnnouncement = $j("#MobileFeatureAnnouncement");

    $j(".click-close", featureAnnouncement).click(function (e) {
        e.preventDefault();
        MobileAnnouncementClosePrompt();
    });

    //close prompt
    function MobileAnnouncementClosePrompt() {
        //hide prompt
        featureAnnouncement.hide();

        //update member info that prompt was displayed and closed
        MobileAnnouncementUpdatePromptSeen();

        return false;
    }

    function MobileAnnouncementUpdatePromptSeen() {
        $j.ajax({
            type: "POST",
            url: "/Applications/API/Member.asmx/UpdateAttributeInt",
            data: "{memberID:" + <%=MemberID.ToString() %> + ", attributeName: \"PageAnnouncementViewedMask\", attributeValue:8}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                var updateResult = (typeof result.d == 'undefined') ? result : result.d;
                if (updateResult.Status != 2) {
                    alert(updateResult.StatusMessage);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert(textStatus);
            }
        });
    }

</script>
