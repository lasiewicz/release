﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ActivitySupport.ascx.cs" Inherits="Matchnet.Web.Framework.Ui.ActivitySupport" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>

<div>
    <%--CHECK IM Supporting elements--%>
    <asp:PlaceHolder ID="plcIMActive" Visible="false" runat="server">
        <div style="width: 1px; height: 1px; position: relative; margin-top: -4px; z-index: 10000;" class="im-wrapper">
            <!-- div used for the content of the DHTML IM notifier -->
            <div id="IMAlerts" style="position: fixed;"></div>
            <!--checkIM - for compatibility with https - make sure to use relative path to something secure-->
            <div id="frm_checkim"></div>
            <%--<iframe name="frm_checkim" src="/img/trans.gif" frameborder="0" scrolling="no" width="1" height="1" tabindex="-1"></iframe>--%>
            <!--reject IM - for compatibility with https - make sure to use relative path to something secure-->
            <div id="frm_rejectim"></div>
            <%--<iframe name="frm_rejectim" src="/img/trans.gif" frameborder="0" scrolling="no" width="1" height="1" tabindex="-1"></iframe>--%>
            <!--remove IM invitation - for compatibility with https - make sure to use relative path to something secure-->
            <div id="frm_removeim"></div>
            <%--<iframe name="frm_removeim" src="/img/trans.gif" frameborder="0" scrolling="no" width="1" height="1" tabindex="-1"></iframe>--%>
            <!--update mol status - for compatibility with https - make sure to use relative path to something secure-->
            <div id="frm_updateMOL"></div>
            <%--<iframe name="frm_updateMOL" src="/img/trans.gif" frameborder="0" scrolling="no" width="1" height="1" tabindex="-1"></iframe>--%>
            <!-- div used for checkIM via ajax -->
            <div id="divAj" style="display: none;"></div>
        </div>
    </asp:PlaceHolder>
    
    <%--SESSION--%>
    <!--sessionKeepAlive - for compatibility with https - make sure to use relative path to something secure-->
    <iframe style="display: none;" name="frm_sessionKeepAlive" src="/img/trans.gif" frameborder="0"
        scrolling="no" width="1" height="1" tabindex="-1"></iframe>
</div>