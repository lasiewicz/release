using System;
using System.Collections;
using System.Data;
using System.Text;
using System.Web.UI.WebControls;

using Matchnet.Web.Framework.Util;

using Spark.SAL;
using Matchnet.Web.Applications.Search;
using Matchnet.Search.Interfaces;
using Matchnet.Web.Framework.Managers;

namespace Matchnet.Web.Framework.Ui.FormElements
{
    /// <summary>
    ///		Summary description for MaskControl.
    /// </summary>
    public class MaskControl20 : FrameworkControl
    {
        protected PlaceHolder plcBasicPreference;
        protected PlaceHolder plcAdditionalPreference;
        protected PlaceHolder PlaceHolderJdateReligion;
        protected Txt txtPreferenceName;
        protected Txt txtAdditionalPreferenceName;
        protected Literal litSelectedItems;
        protected System.Web.UI.HtmlControls.HtmlGenericControl divSelectedItems;
        protected Repeater rptOptions;

        protected Repeater RepeaterJdateReligion;
        protected Repeater RepeaterNonJdateReligion;

        private CheckBox _blankOption;

        private Hashtable _nonBlankOptions = new Hashtable();
        private Hashtable _optionListOrder = new Hashtable();

        public MemberSearchPreferenceMask MemberSearchPreferenceMask;
        public bool AutoSelectHigherValues;
        public bool Collapsible;
        public bool IsCollapsed;
        private bool _isJDateReligion = false;
        public int SearchWeightValue = (int)SearchWeight.VeryImportant;

        public string ResourceConstant
        {
            get
            {
                return txtPreferenceName.ResourceConstant;
            }
            set
            {
                txtPreferenceName.ResourceConstant = value;
                txtAdditionalPreferenceName.ResourceConstant = value;
            }
        }

        public bool IsSliderWeightEnabled
        {
            get
            {
                return MatchRatingManager.Instance.IsSliderWeightingEnabled(g.Brand.BrandID, g.Brand.Site.SiteID, g.Brand.Site.Community.CommunityID);
            }
        }

        private void Page_Load(object sender, System.EventArgs e)
        {
            try
            {
                IsCollapsed = (MemberSearchPreferenceMask.Value <= 0);
                bindData();
                setupClientScript();
                showSelectedText();
                showHideCollapsible();
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        private void bindData()
        {
            string attributeName = MemberSearchPreferenceMask.Preference.Attribute.Name.ToLower();

            switch (attributeName)
            {
                case "childrencount":
                    rptOptions.DataSource = Option.GetChildrenCountOptions(g);
                    rptOptions.DataBind();
                    break;

                // Made major hacks for supporting sub groups within this attribute. Don't hate me. MPR-940
                case "jdatereligion":
                    _isJDateReligion = true;

                    PlaceHolderJdateReligion.Visible = true;

                    DataTable options = Option.GetOptions(MemberSearchPreferenceMask.Preference.Attribute.Name, g);

                    DataTable jewishOptions = options.Clone();
                    foreach (DataRow row in options.Select(@"Value not in (2048, 4096, 16384) and Content not like '' ", "ListOrder asc"))
                        jewishOptions.ImportRow(row);

                    DataTable nonjewishOptions = options.Clone();
                    foreach (DataRow row in options.Select(@"Value in (2048, 4096, 16384)  and Content not like '' ", "ListOrder asc"))
                        nonjewishOptions.ImportRow(row);

                    RepeaterJdateReligion.DataSource = jewishOptions;
                    RepeaterNonJdateReligion.DataSource = nonjewishOptions;

                    RepeaterJdateReligion.DataBind();
                    RepeaterNonJdateReligion.DataBind();

                    break;
                default:
                    rptOptions.DataSource = Option.GetOptions(MemberSearchPreferenceMask.Preference.Attribute.Name, g);
                    rptOptions.DataBind();
                    break;

            }
        }

        private void showSelectedText()
        {
            if (MemberSearchPreferenceMask.Preference.Attribute.Name == "ChildrenCount")
            {
                litSelectedItems.Text = Option.GetChildrenCountMaskContent(MemberSearchPreferenceMask.Value, g);
            }
            else if (MemberSearchPreferenceMask.Preference.Attribute.Name.ToLower() == "jdatereligion" && MemberSearchPreferenceMask.Value == FrameworkGlobals.GetJewishReligionOnly(g))
            {
                litSelectedItems.Text = g.GetResource("TXT_JEWISH", this);
            }
            else
            {
                litSelectedItems.Text = Option.GetMaskContent(MemberSearchPreferenceMask.Preference.Attribute.Name, MemberSearchPreferenceMask.Value, g);
            }

            if (litSelectedItems.Text == string.Empty)
            {
                litSelectedItems.Text = g.GetResource("ANY_");
            }
        }

        private void showHideCollapsible()
        {
            if (Collapsible)
            {
                plcBasicPreference.Visible = true;
                plcAdditionalPreference.Visible = false;
            }
            else
            {
                plcBasicPreference.Visible = false;
                plcAdditionalPreference.Visible = true;
            }
        }

        private bool _firstItem = true;

        private void rptOptions_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                DataRowView row = e.Item.DataItem as DataRowView;

                CheckBox cbOption = e.Item.FindControl("cbOption") as CheckBox;
                cbOption.CheckedChanged += new EventHandler(cbOption_CheckedChanged);

                if (_firstItem)
                {
                    _firstItem = false;
                    _blankOption = cbOption;
                    cbOption.Text = g.GetResource("ANY_");
                    cbOption.Checked = (MemberSearchPreferenceMask.Value == Constants.NULL_INT);
                }
                else
                {
                    _nonBlankOptions.Add(row["Value"], cbOption);
                    cbOption.Text = row["Content"].ToString();
                    cbOption.Checked = MemberSearchPreferenceMask.ContainsValue(Conversion.CInt(row["Value"]));
                }

                _optionListOrder.Add(cbOption, row["ListOrder"]);
            }
        }

        private string clientIDGroupJdateReligionCheckBox, clientIDGroupNonJdateReligionCheckBox;

        private void RepeaterJdateReligion_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Header)
            {
                // Only selects Jewish religion checkboxes.
                CheckBox groupJdateReligionCheckBox = e.Item.FindControl("CheckBoxGroupJdateReligion") as CheckBox;
                if (groupJdateReligionCheckBox != null)
                {
                    clientIDGroupJdateReligionCheckBox = groupJdateReligionCheckBox.ClientID;
                    groupJdateReligionCheckBox.CheckedChanged += new EventHandler(cbOption_CheckedChanged);
                    groupJdateReligionCheckBox.Attributes.Add("onclick",
                                                              "CheckBoxListSelect(this,'RepeaterJdateReligion','" +
                                                              divSelectedItems.ClientID + "','" +
                                                              g.GetResource("TXT_JEWISH", this) + "')");

                    if (MemberSearchPreferenceMask.Value == FrameworkGlobals.GetJewishReligionOnly(g))
                    {
                        groupJdateReligionCheckBox.Checked = true;
                        //divSelectedItems.InnerHtml = g.GetResource("TXT_JEWISH", this);
                    }

                    if (g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.Cupid)
                    {
                        groupJdateReligionCheckBox.Visible = false;
                        Txt txtJewish = e.Item.FindControl("txtJewish") as Txt;
                        if (txtJewish != null)
                        {
                            txtJewish.Visible = false;
                        }
                    }
                }

                // Selecting Any - behaves just like other Any option.
                CheckBox groupNonJdateReligionCheckBox = e.Item.FindControl("CheckBoxGroupNonJdateReligion") as CheckBox;
                if (groupNonJdateReligionCheckBox != null)
                {
                    groupNonJdateReligionCheckBox.CheckedChanged += new EventHandler(cbOption_CheckedChanged);
                    clientIDGroupNonJdateReligionCheckBox = groupNonJdateReligionCheckBox.ClientID;
                    groupNonJdateReligionCheckBox.Attributes.Add("onclick",
                                                                 groupNonJdateReligionCheckBox.Attributes["onclick"] +
                                                                 ";document.getElementById('" +
                                                                 clientIDGroupJdateReligionCheckBox +
                                                                 "').checked = false;");
                    _blankOption = groupNonJdateReligionCheckBox;

                    groupNonJdateReligionCheckBox.Checked = (MemberSearchPreferenceMask.Value == Constants.NULL_INT);
                }
            }

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                DataRowView row = e.Item.DataItem as DataRowView;

                CheckBox cbOption = e.Item.FindControl("cbOption") as CheckBox;
                cbOption.CheckedChanged += new EventHandler(cbOption_CheckedChanged);

                _nonBlankOptions.Add(row["Value"], cbOption);
                cbOption.Text = row["Content"].ToString();
                cbOption.Checked = MemberSearchPreferenceMask.ContainsValue(Conversion.CInt(row["Value"])) || MemberSearchPreferenceMask.Value == Constants.NULL_INT;
                string clientID = clientIDGroupJdateReligionCheckBox;
                if (e.Item.Parent.ID == "RepeaterNonJdateReligion")
                {
                    clientID = clientIDGroupNonJdateReligionCheckBox;
                }

                cbOption.Attributes.Add("onclick", ";ToggleGroupCheckBox(this, '" + clientID + "')");

                _optionListOrder.Add(cbOption, row["ListOrder"]);
            }
        }
        private bool maskRecomputed = false;
        /// <summary>
        /// If any checkboxes change recompute the mask.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cbOption_CheckedChanged(object sender, EventArgs e)
        {
            if (!maskRecomputed)
            {
                maskRecomputed = true;
                MemberSearchPreferenceMask.ClearMask();

                if (_isJDateReligion && _blankOption != null && _blankOption.Checked)
                {
                    MemberSearchPreferenceMask.AddValue(Constants.NULL_INT);
                    showSelectedText();
                }
                else
                {
                    foreach (DictionaryEntry de in _nonBlankOptions)
                    {
                        Int32 val = Conversion.CInt(de.Key);
                        CheckBox cbOption = de.Value as CheckBox;

                        if (cbOption.Checked)
                        {
                            MemberSearchPreferenceMask.AddValue(val);
                        }
                    }

                    showSelectedText();
                }

                IsCollapsed = (MemberSearchPreferenceMask.Value <= 0);
            }
        }

        private void setupClientScript()
        {
            if (_blankOption != null)
            {
                StringBuilder clientScript = new StringBuilder();
                foreach (CheckBox cbOption in _nonBlankOptions.Values)
                {
                    clientScript.Append(getCheckScript(cbOption, _isJDateReligion));
                }
                clientScript.Append(getChangeLabelScript());

                _blankOption.Attributes.Add("onClick",
                    _blankOption.Attributes["onclick"] + ";" +
                    clientScript.ToString());

                foreach (CheckBox cbOption1 in _nonBlankOptions.Values)
                {
                    clientScript = new StringBuilder(getCheckScript(_blankOption, false));

                    if (AutoSelectHigherValues)
                    {
                        clientScript.Append("if (document.getElementById('" + cbOption1.ClientID + "').checked) {");
                        foreach (CheckBox cbOption2 in _nonBlankOptions.Values)
                        {
                            Int32 listOrder1 = (Int32)_optionListOrder[cbOption1];
                            Int32 listOrder2 = (Int32)_optionListOrder[cbOption2];

                            if (listOrder2 > listOrder1)
                            {
                                clientScript.Append(getCheckScript(cbOption2, true));
                            }
                        }
                        clientScript.Append("}");
                    }

                    clientScript.Append(getChangeLabelScript());

                    cbOption1.Attributes.Add("onClick", cbOption1.Attributes["onclick"] + ";" + clientScript.ToString());
                }
            }
        }

        private static string getCheckScript(CheckBox cb, bool isChecked)
        {
            return "document.getElementById('" + cb.ClientID + "').checked = " + isChecked.ToString().ToLower() + ";";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>Script to change the current text to reflect the selected checkboxes.
        /// Also checks "Any" option if all other options are unchecked.
        /// </returns>
        private string getChangeLabelScript()
        {
            StringBuilder sbScript = new StringBuilder();
            sbScript.Append("var newText = '';");
            sbScript.Append("if (" + _blankOption.ClientID + ".checked) newText += '" + g.GetResource("ANY_") + ", ';");

            foreach (CheckBox cbOption in _nonBlankOptions.Values)
            {
                sbScript.Append("if (" + cbOption.ClientID + ".checked) newText += '" + cbOption.Text.Replace("'", "\\'") + ", ';");
            }
            sbScript.Append("if (newText == '')");
            sbScript.Append("{");
            sbScript.Append("newText += '" + g.GetResource("ANY_") + ", ';");
            sbScript.Append(getCheckScript(_blankOption, true));
            sbScript.Append("}");
            sbScript.Append("newText = newText.substring(0, newText.length - 2);");

            sbScript.Append("document.getElementById('" + divSelectedItems.ClientID + "').innerHTML = newText;");

            return sbScript.ToString();
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
            rptOptions.ItemDataBound += new RepeaterItemEventHandler(rptOptions_ItemDataBound);
            RepeaterJdateReligion.ItemDataBound += new RepeaterItemEventHandler(RepeaterJdateReligion_ItemDataBound);
            RepeaterNonJdateReligion.ItemDataBound += new RepeaterItemEventHandler(RepeaterJdateReligion_ItemDataBound);
        }
        #endregion
    }
}
