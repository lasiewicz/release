<%@ Register TagPrefix="mn" TagName="YNMVoteBarSmall" Src="../YNMVoteBarSmall.ascx" %>
<%@ Control Language="c#" AutoEventWireup="false" CodeBehind="MicroProfile.ascx.cs"
    Inherits="Matchnet.Web.Framework.Ui.BasicElements.MicroProfile" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<%@ Register TagPrefix="uc1" TagName="NoPhoto" Src="../PageElements/NoPhoto.ascx" %>
<%@ Register TagPrefix="mnbe" Namespace="Matchnet.Web.Framework.Ui.BasicElements"
    Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mnl" Namespace="Matchnet.Web.Framework.Ui.BasicElements.Links"
    Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="uc2" TagName="HighlightProfileInfoDisplay" Src="HighlightProfileInfoDisplay.ascx" %>
<%@ Register TagPrefix="uc2" TagName="MatchMeterInfoDisplay" Src="MatchMeterInfoDisplay.ascx" %>
<li id="tblProfile" runat="server">
    <%-- <asp:PlaceHolder ID="plcSpotlight" runat="server">
        <h2><mn:txt ID="txtSpotlight" runat="server" ResourceConstant="SPOTLIGHT_TITLE"  Visible="false" />
        <mn:txt ID="txtSpotlightPromo" runat="server" ResourceConstant="SPOTLIGHT_PROMO_TITLE"  Visible="false" /></h2>
    </asp:PlaceHolder>--%>
    <%--  <uc2:SpotlightInfoDisplay runat="server" id="ucSpotlightProfileInfoDisplay" ShowImageNoText="true" />--%>
    <uc2:MatchMeterInfoDisplay runat="server" ID="ucMatchMeterInfoDisplay" DispalyType="4"
        Visible="false" />
    <div class="memberPic">
        <asp:HyperLink ID="lnkThumb" runat="server">
            <mn:Image runat="server" ID="imgThumb" class="profileImageHover" Border="0" ResourceConstant="ALT_PROFILE_PICTURE"
                TitleResourceConstant="TTL_VIEW_MY_PROFILE" Width="80" Height="104" />
        </asp:HyperLink>
        <uc1:NoPhoto runat="server" ID="noPhoto" class="noPhoto" Visible="False" />
    </div>
    <div class="memberInfo">
        <uc2:HighlightProfileInfoDisplay runat="server" ID="ucHighlightProfileInfoDisplay"
            ShowImageNoText="true" />
        <asp:PlaceHolder runat="server" ID="plcEmailMe" Visible="false">
            <asp:HyperLink runat="server" ID="lnkEmail">
                <mn:Image ID="ImageEmailMe" CssClass="ImageEmailMe" runat="server" FileName="icon_email.gif" align="absMiddle"
                    Hspace="3" Vspace="0" ResourceConstant="ALT_EMAIL_ME" />
                <mn:Txt runat="server" ID="txtEmail" ResourceConstant="PRO_EMAIL" />
            </asp:HyperLink></asp:PlaceHolder>
        <mn:YNMVoteBarSmall ID="VoteBar" runat="server" />
        <br />
        <mnl:Link runat="server" ID="lnkUsername" TitleResourceConstant="TTL_VIEW_MY_PROFILE"
            CssClass="miniProfileTitle" /><br />
        <asp:Literal ID="txtAge" runat="server" />,
        <asp:Literal ID="txtRegion" runat="server" />
        <p class="microProfileAboutMe">
            <asp:Literal ID="txtHeadline" runat="server" /></p>
        <p class="textOutside">
            <mn:Txt TitleResourceConstant="TTL_VIEW_MY_PROFILE" ResourceConstant="MORE_ARROWS"
                runat="server" ID="txtViewProfile" />
            <mn:Txt TitleResourceConstant="TTL_VIEW_MY_PROFILE" ResourceConstant="MORE_ARROWS_MATCH_METER"
                runat="server" ID="txtViewProfileMatchMeter" Visible="false" />
        </p>
    </div>
</li>
