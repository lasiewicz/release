﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProfileStatusSummary.ascx.cs" Inherits="Matchnet.Web.Framework.Ui.ProfileStatusSummary" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="uc1" TagName="NoPhoto" Src="PageElements/NoPhoto20.ascx" %>

<div class="profile-activity">
    <div class="pic">
        <mn:Image runat="server" ID="imgThumb2" class="profileImageHover" Visible="false" Border="0" Style="width: 38px; height: 50px;" ResourceConstant="ALT_PROFILE_PICTURE" TitleResourceConstant="TTL_VIEW_MY_PROFILE" />
        <uc1:NoPhoto runat="server" ID="noPhoto2" class="no-photo" Visible="True" Width="38px" Height="50px" />
    </div>
    <div class="info">
        <h2><asp:HyperLink runat="server" ID="lnkUserName" NavigateUrl="" /></h2>
        <mn:Txt runat="server" ID="txtEditProfile" CssClass="bullet right" ResourceConstant="TXT_EDIT_PROFILE" Href="/Applications/MemberProfile/ViewProfile.aspx?Mode=Edit"/>
        <mn:Txt runat="server" ID="txtManagePhotos" CssClass="bullet right" ResourceConstant="TXT_MANAGE_PHOTOS" Href="/Applications/MemberProfile/MemberPhotoEdit.aspx" />
    </div>
</div>