﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Feedback.ascx.cs" Inherits="Matchnet.Web.Framework.Ui.Feedback.Feedback" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>

<!-- begin feedback widget html-->
<script type="text/javascript">
    $j(document).ready(function(){
        $j('#feedbackForm').dialog({
            modal: true,
            autoOpen: false,
            width: 730,
            height: 400,
            title: '<%=g.GetResource("TXT_FEEDBACK_MODAL_HEADER",this) %>'
        });
    });

    function launchfeedbackOpen() {
        $j("#feedbackForm").dialog("open");
    }

    function launchfeedbackClose() {
        $j("#feedbackForm").dialog("close");
    }
    
    var SendFeedbackInProgress = false;
    function SendFeedbackMessage() {
        
        if (SendFeedbackInProgress == true){
            return false;
        }
        
        var isValid = true;
        var isVerbose = true;
        var feedbackTypeID = <%=((int)FeedbackType).ToString() %>;
        var email = $j.trim($j('#<%=txtYourEmail.ClientID%>').val());
        var name = $j.trim($j('#<%=txtYourName.ClientID%>').val());
        var message = $j.trim($j('#<%=txtYourFeedback.ClientID%>').val());
        
        //reset messages
        $j('#divHoverSendingFeedback').css("display", "block");
        $j("#feedbackSubmitSuccess").hide();
        $j("#feedbackSubmitError").hide();
        $j("#feedbackNameError").hide();
        $j("#feedbackEmailError").hide();
        $j("#feedbackTextError").hide();
        
        //validate
        if (message == '' || email == '' || name == ''){
            isValid = false;
        }
        else if (!IsEmail(email, 3)){
            $j("#feedbackEmailInvalid").show();
        }
        
        if (message == '') {$j("#feedbackTextError").show(); }
        if (email == '') {$j("#feedbackEmailError").show(); }
        if (name == '') {$j("#feedbackNameError").show(); }
        
        if (isValid) {
            SendFeedbackInProgress = true;
            $j.ajax({
                type: "POST",
                url: "/Applications/API/IMailService.asmx/SendFeedback",
                data: "{ feedbackTypeID: " + feedbackTypeID
                    + ", senderName: \"" + spark.util.uniescape(name) + "\""
                    + ", senderEmail: \"" + spark.util.uniescape(email) + "\""
                    + ", message: \"" + spark.util.uniescape(message) + "\"}",
                contentType: "application/json; charset=utf-8",
                complete: function() {
                    SendFeedbackInProgress = false;
                },
                error: function(jqXHR, textStatus, errorThrown){
                    //if (isVerbose) { alertMessage(textStatus, 3); }
                    $j("#feedbackSubmitError").show();
                },
                success: function(result) {
                    var resultStatus = (typeof result.d == 'undefined') ? result.Status : result.d.Status;
                    if (resultStatus == 5){
                        //session expired
                        window.location.href = '/Applications/Logon/Logon.aspx?DestinationURL=' + encodeURIComponent(window.location.href.replace('http://'+window.location.host,''));
                    }
                    if (resultStatus == 3) {
                        //if (isVerbose) { alertMessage(result.StatusMessage, 3); }
                        $j("#feedbackSubmitError").show();
                    }
                    else if (resultStatus == 2) {
                        $j('#<%=txtYourFeedback.ClientID%>').val('');
                        $j("#feedbackSubmitSuccess").show();
                    }
                }
            });
        }
        
        //setTimeout("$j('#divHoverSendingFeedback').css('display', 'none');", 5000); //make ajax loader stay up for testing
        $j('#divHoverSendingFeedback').css("display", "none");
    }
    
</script>

<button id="feedbackButton" class="feedback-button" onclick="launchfeedbackOpen();return false;"></button>

<div id="feedbackForm" class="feedback-form" style="display:none">
    <p class="editorial"><mn2:FrameworkLiteral ID="literalFeedbackTitle" runat="server" ResourceConstant="TXT_FEEDBACK_TITLE"></mn2:FrameworkLiteral></p>
    <div id="feedbackSubmitSuccess" style="display:none;" class="notification">
        <mn:Txt ID="txtSubmitSuccess" ResourceConstant="TXT_SUCCESS" runat="server" />
    </div>
    <div id="feedbackSubmitError" style="display:none;" class="notification error">
        <mn:Txt ID="txtSubmitError" ResourceConstant="TXT_ERROR" runat="server" />
    </div>
    <dl class="form-set dl-form clearfix">
        <dt>
            <label for="<%=txtYourName.ClientID%>" id="yourNameLabel">
                <mn2:FrameworkLiteral ID="literalName" runat="server" ResourceConstant="TXT_NAME"></mn2:FrameworkLiteral>
            </label>
        </dt>
        <dd>
            <asp:TextBox ID="txtYourName" runat="server" CssClass="large" MaxLength="100"></asp:TextBox>
                <span id="feedbackNameError" style="display:none;" class="error"><mn:Txt ID="txtNameError" ResourceConstant="TXT_NAME_TEXT_ERROR" runat="server" /></span>
        </dd>
        
        <dt>
            <label for="<%=txtYourEmail.ClientID%>" id="yourEmailLabel">
                <mn2:FrameworkLiteral ID="literalEmail" runat="server" ResourceConstant="TXT_EMAIL"></mn2:FrameworkLiteral>
            </label>
        </dt>
        <dd>
            <asp:TextBox ID="txtYourEmail" runat="server" CssClass="large" MaxLength="100"></asp:TextBox>
                <span id="feedbackEmailInvalid" style="display:none;" class="error"><mn:Txt ID="txt1" ResourceConstant="TXT_EMAIL_TEXT_INVALID" runat="server" /></span>
                <span id="feedbackEmailError" style="display:none;" class="error"><mn:Txt ID="txtEmailError" ResourceConstant="TXT_EMAIL_TEXT_ERROR" runat="server" /></span>
        </dd>
        
        <dt>
            <label for="<%=txtYourFeedback.ClientID%>" id="yourFeedbackLabel">
                <mn2:FrameworkLiteral ID="literalFeedback" runat="server" ResourceConstant="TXT_FEEDBACK"></mn2:FrameworkLiteral>
            </label>
        </dt>
        <dd>
            <asp:TextBox ID="txtYourFeedback" runat="server" CssClass="large" TextMode="MultiLine" MaxLength="3000"></asp:TextBox>
                <span id="feedbackTextError" style="display:none;" class="error"><mn:Txt ID="txtFeedbackTextError" ResourceConstant="TXT_FEEDBACK_TEXT_ERROR" runat="server" /></span>
        </dd>
        
        <dt><span style="visibility:hidden;">&nbsp;</span></dt>
        <dd><mn2:FrameworkButton ID="buttonSendFeedback" runat="server" CssClass="btn primary large wide link-primary" ResourceConstant="TXT_SUBMIT" OnClientClick="SendFeedbackMessage();return false;" /></dd>
    </dl>
    
    <div id="divHoverSendingFeedback" style="display:none" class="pre-loader">
        <mn:Image ID="imageHoverBlock" runat="server" FileName="ajax-loader.gif" />
    </div>
</div>
