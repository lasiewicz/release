﻿

using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Session.ServiceAdapters;
using System.Text;
using Matchnet.Session.ValueObjects;
using Matchnet.Web.Framework.Ui.BasicElements;
using Matchnet.Web.Framework.Ui.ProfileElements;
using Matchnet.Web.Applications.MemberProfile.ProfileTabs30.Controls;
namespace Matchnet.Web.Framework.Ui
{
    public partial class MingleRight : FrameworkControl, IRight
    {
        #region Member Variables
        private bool renderAdHeaderTopIFrameIDToJS = false;
        protected PlaceHolder PlaceHolderGAMChannels;
        #endregion

        #region Events
        protected override void OnInit(EventArgs e)
        {
            //update AdUnit for pages that needs to use IFrame.
            switch ((WebConstants.APP)g.AppPage.App.ID)
            {
                case WebConstants.APP.MemberProfile:
                    if (g.AppPage.ControlName.ToLower() == "viewprofile" || g.AppPage.ControlName.ToLower() == "viewtabbedprofile20")
                    {
                        this.adSquareRight.GAMAdSlot = "profile_basics_right_300x250";
                        this.adSquareRight.GAMPageMode = Matchnet.Web.Framework.Ui.Advertising.AdUnit.GAMPageModeType.None;
                        this.adSquareRight.GAMIframe = true;
                        this.adSquareRight.GAMIframeHeight = 250;
                        this.adSquareRight.GAMIframeWidth = 300;
                        this.renderAdHeaderTopIFrameIDToJS = true;
                    }
                    break;
                case WebConstants.APP.Home:
                    if (g.AppPage.ControlName.ToLower() == "default")
                    {
                        this.adSquareRight.GAMAdSlot = "home_right_300x250";
                        this.adSquareRight.GAMPageMode = Matchnet.Web.Framework.Ui.Advertising.AdUnit.GAMPageModeType.None;
                        this.adSquareRight.GAMIframe = true;
                        this.adSquareRight.GAMIframeHeight = 250;
                        this.adSquareRight.GAMIframeWidth = 300;
                        this.renderAdHeaderTopIFrameIDToJS = true;
                    }
                    break;
            }

            lnkColorCodeMailTo.HRef = "mailto:colorcode@" + g.Brand.Site.Name.ToString() + "?subject=Color Code Feedback";

            base.OnInit(e);

        }

        private void Page_Load(object sender, EventArgs e)
        {
            try
            {
                // Hide Jmessage unless querystring has override
                if (Request.QueryString["ShowJMessage"] == "1")
                {
                    plcJMessage.Visible = true;
                }

                /*AdServerTest.Visible = false;
                if(RuntimeSettings.GetSetting("ADSERVER") != null)
                {
                    if(RuntimeSettings.GetSetting("ADSERVER") =="true")
                    {
                        AdServerTest.Visible = true;
                    }
                }*/

                if (Request["ClickedHomepage"] != null)
                {
                    ClickedMakeHomepage = Request["ClickedHomepage"].ToLower() == "true" ? true : false;
                }

                PlaceHolderGAMChannels.Visible = true;
                rightContent.Visible = false;

                if (g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.JDateCoIL ||
                    g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.Cupid)
                {
                    //if (g.AppPage.ID == (int)WebConstants.PageIDs.ArticleView ||
                    //    g.AppPage.ID == (int)WebConstants.PageIDs.FAQMain)
                    //{
                    txtTips.Visible = true;
                    //}
                }
                if (g.AppPage.App.ID == (int)WebConstants.APP.ColorCode && g.AppPage.ControlName.ToLower() == "analysis")
                {
                    if (g.Member != null)
                    {
                        int ccpurchase = g.Member.GetAttributeInt(g.Brand, "ColorAnalysis");
                        bool purchaseEnabled = SettingsManager.GetSettingBool(SettingConstants.ENABLE_COLORCODE_PURCHASE, g.Brand);
                        phColorCodePurchase.Visible = ((ccpurchase != 1) && purchaseEnabled);
                    }
                }
                else
                {
                    phColorCodePurchase.Visible = false;
                }

                //Disable some channel adslots that will display on homepage instead of Right control of new JDate homepage version
                if (g.AppPage.App.ID == (int)WebConstants.APP.Home && g.AppPage.ID == (int)WebConstants.PageIDs.Default)
                {
                    Matchnet.Configuration.ServiceAdapters.Analitics.Scenario scenario = g.GetABScenario("HOMEPAGE");
                    if (scenario == Matchnet.Configuration.ServiceAdapters.Analitics.Scenario.B)
                    {
                        if (Matchnet.Web.Applications.Home.HomeUtil.IsHomepageChannelAdsOverrideEnabled(g.Brand))
                        {
                            AdUnitGAMChannel3.Disabled = true;
                            AdUnitGAMChannel5.Disabled = true;
                            AdUnitGAMChannel6.Disabled = true;
                            AdUnitGAMChannel7.Disabled = true;
                        }
                    }
                }
                // Nice...
                if (g.AppPage.App.ID == (int)WebConstants.APP.Home && g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.AmericanSingles
                    || g.AppPage.App.ID == (int)WebConstants.APP.Home && g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.JDate
                    || g.AppPage.App.ID == (int)WebConstants.APP.Home && g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.JDateFR
                    || g.AppPage.App.ID == (int)WebConstants.APP.Home && g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.JDateUK
                    || g.AppPage.App.ID == (int)WebConstants.APP.Home && g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.JDateCoIL
                    || g.AppPage.App.ID == (int)WebConstants.APP.Home && g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.BBW
                    || g.AppPage.App.ID == (int)WebConstants.APP.Home && g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.BlackSingles
                    )
                {
                        phHeaderAnnouncement.Visible = HeaderAnnouncement1.LoadAnnouncementMessage();
                }
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            if (this.renderAdHeaderTopIFrameIDToJS)
            {
                Page.ClientScript.RegisterClientScriptBlock(typeof(System.Web.UI.Page), "RightJSAd",
                       "<script type=\"text/javascript\">"
                       + "var rightAd300x250IFrameID = \"" + this.adSquareRight.AdIFrameID + "\";"
                       + "var rightAdProfilePopup = \"false\";"
                       + "</script>");
            }

            base.OnPreRender(e);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Gets desired attributes from clients browser passed in http header.
        /// At some point this should be completed and moved to some common utility class.
        /// </summary>
        /// <param name="requestedAttribute">examples: BrowserType, HasFlash, MonitorRes, Bandwidth, CookiesEnabled</param>
        /// <returns>result from http header</returns>
        private string BrowserAttribute(string requestedAttribute)
        {
            string browserAttribute = string.Empty;
            HttpContext context = HttpContext.Current;
            HttpBrowserCapabilities httpbrowsercapabilitiesBrowser = context.Request.Browser;

            switch (requestedAttribute)
            {
                case "BrowserType":
                    browserAttribute = httpbrowsercapabilitiesBrowser.Browser;
                    return browserAttribute;
                case "HasFlash":
                    return browserAttribute;
                case "MonitorRes":
                    return browserAttribute;
                case "Bandwidth":
                    return browserAttribute;
                case "CookiesEnabled":
                    return browserAttribute;
                default:
                    return browserAttribute;
            }
        }

        private bool IsInvalidRequest()
        {
            //this is a HUGE HACK. There's a problem with some gam ads calling to images and stylesheets that
            //no longer exist, and the resulting 404 causes default.aspx to be reloaded and the slideshow to advance. 
            //This code prevents that, and can be removed once the gam issue is resolved. 
            bool invalid = false;
            string controlname = g.AppPage.ControlName;
            string uri = HttpContext.Current.Request.RawUrl;
            string referrer = HttpContext.Current.Request.UrlReferrer != null ? HttpContext.Current.Request.UrlReferrer.AbsoluteUri : string.Empty;


            if (controlname.ToLower() == "default" &&
                (uri.ToLower().IndexOf("/?") >= 0 ||
                uri.ToLower().IndexOf("default.aspx?siteid=") >= 0 ||
                uri.ToLower().IndexOf("default.aspx?v=") >= 0) ||
                referrer.ToLower().IndexOf("red01.css") >= 0)
            {
                Trace.Write("is: true");
                invalid = true;
            }

            return invalid;
        }


        //Methods below should be renamed, and ultimately superceded by using different templates for Login, Registration, and subscription
        //instead of trying to hard code this logic.

        /// <summary>
        /// Subscription Mode. Redundant method called from subscription common pages to hide left nav area if Member is not a paying member
        /// Rationale: not to distract users during subscription. Join Now will be hidden, because they are a member.
        /// </summary>
        public void HideUserNameLogout()
        {
            HideLoginLink();
        }

        /// <summary>
        /// Registration Mode. Redundant Method called from the registration control that is mostly redundant with g.Member = null.
        /// Hide Join Now cuz you're already registering. (we also now hide the login link because there are two other login links
        /// We should do the same on other sites.
        /// </summary>
        public void ShowWelcomeText()
        {
            this.ActivityCenter1.JoinNowSection.Visible = false;
            HideLoginLink();
        }

        /// <summary>
        /// Login Mode. Redundant method called from the logon control. It is mostly redundant with g.Member = null
        /// It asks that we hide the MembersOnline count (because we're logging in and don't need the tease
        /// and show the Join Now (which is redundant  because if wer're logged out it'll be shown anyway).
        /// Logon should use different template that does not include the left nav!
        /// </summary>
        public void HideLoginLink()
        {
            this.ActivityCenter1.ActivityCenterSection.Visible = false;
        }

        /// <summary>
        /// Shows the color code section for profile examples
        /// </summary>
        public void ShowColorCodeProfileExamples()
        {
            this.phColorCodeProfileExamples.Visible = true;
        }

        /// <summary>
        /// Shows the color code section for test help
        /// </summary>
        public void ShowColorCodeTestHelp()
        {
            this.phColorCodeTestHelp.Visible = true;
        }

        /// <summary>
        /// Shows the color code section for the about color code and meet dr. hartman info
        /// </summary>
        public void ShowColorCodeAbout()
        {
            this.phColorCodeAbout.Visible = true;
        }

        /// <summary>
        /// Shows the color code section for member feedback
        /// </summary>
        public void ShowColorCodeFeedback()
        {
            this.phColorCodeMemberFeedback.Visible = true;
        }

        public void HideGamChannels()
        {
            this.PlaceHolderGAMChannels.Visible = false;
        }

        public void ShowMiniMemberSlideShow()
        {
            phMiniMemberSlideshow.Visible = true;
            ucMiniSlideshowProfile.ReloadAds = true;
            ucMiniSlideshowProfile.PopulateYNMBuckets = false;
            ucMiniSlideshowProfile.SlideshowOuterContainerDivID = "slideshowOuter";
            ucMiniSlideshowProfile.Load(true);

            bool enableConfiguration = Conversion.CBool(RuntimeSettings.GetSetting("ENABLE_CONFIGURABLE_SLIDESHOW", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID));
            if (enableConfiguration)
            {
                ucMiniSlideshowProfile.LoadPreferences();
            }
        }

        public void ShowMiniMemberSlideShowBelowAd()
        {
            //not implemented
        }
        #endregion

        #region Properties
        /// <summary>
        /// gets/sets whether the user clicked on the set as homepage
        /// </summary>
        private bool ClickedMakeHomepage
        {
            get
            {
                bool clickedHomepage = false;

                try
                {
                    clickedHomepage = bool.Parse(g.Session["ClickedHomepage"]);
                }
                catch
                {

                }

                return clickedHomepage;
            }
            set
            {
                g.Session.Add("ClickedHomepage", value.ToString(), SessionPropertyLifetime.Persistent);
            }
        }

        public string MovieUrl
        {
            get
            {
                StringBuilder sb = new StringBuilder(200);
                if (g.Member != null)
                {
                    sb.Append("http://strategy.matchnet.com/jmessage/jmessage.swf?Happen_URL=http://strategy.matchnet.com/m_online/Happen.xml&amp;Inbox_URL=http://strategy.matchnet.com/m_online/Inbox.xml&amp;IM_URL=http://strategy.matchnet.com/m_online/IM.xml");

                    /*
					
                    sb.Append(Server.UrlEncode("http://dv-wlybrand.jdate.com/Applications/API/FlashListAPI.xml"));
                    sb.Append(Server.UrlEncode("?MemberID=" + g.Member.MemberID));
                    sb.Append(Server.UrlEncode("&BrandID=" + g.Brand.BrandID));

                    sb.Append("&Inbox_URL=http://strategy.matchnet.com/m_online/inbox.xml");
					
                    sb.Append(Server.UrlEncode("http://dv-wlybrand.jdate.com/Applications/API/FlashEmailAPI.aspx"));
                    sb.Append(Server.UrlEncode("?MemberID=" + g.Member.MemberID));
                    sb.Append(Server.UrlEncode("&BrandID=" + g.Brand.BrandID));
                    sb.Append(Server.UrlEncode("&MemberFolderID=1&StartRow=1&PageSize=4&OrderBy=InsertDate desc"));
					
                    //sb.Append("");

                    sb.Append("&IM_URL=");
                    sb.Append(Server.UrlEncode("http://strategy.matchnet.com/m_online/IM.xml"));
                    */
                }
                return sb.ToString();
            }
        }

        /// <summary>
        /// Gets reference to the MiniSearch control
        /// </summary>
        public MiniSearch MiniSearchControl
        {
            get { return this.MiniSearch1; }
        }

        public ActivityCenter ActivityCenter
        {
            get { return this.ActivityCenter1; }
        }

        public ProfileToolBar ProfileToolBar
        {
            get { return null; }
        }

        public ProfileDetails ProfileDetails
        {
            get { return null; }
        }

        public bool IsVisible
        {
            get
            {
                return this.Visible;
            }
            set
            {
                this.Visible = value;
            }
        }

        public FrameworkControl Control
        {
            get
            {
                return this;
            }
        }

        #endregion
    }
}
