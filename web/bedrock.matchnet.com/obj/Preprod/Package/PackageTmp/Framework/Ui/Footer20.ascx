﻿<%@ Register TagPrefix="mn" TagName="AdUnit" Src="Advertising/AdUnit.ascx" %>
<%@ Register TagPrefix="mn" TagName="Copyright" Src="PageElements/Copyright.ascx" %>
<%@ Register TagPrefix="uc1" TagName="LogonBox" Src="BasicElements/LogonBox.ascx" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Footer20.ascx.cs" Inherits="Matchnet.Web.Framework.Ui.Footer20" %>
<%--<mn:AdUnit id="adWideskyscraper" Size="Wideskyscraper" runat="server" visible="false" ExpandImageTokens="true" GAMAdSlot="right_160x600" GAMPageMode="Auto"/>
--%>
<div class="footerLeaderBoard">
    <mn:AdUnit ID="adFooterLeaderboard" Size="Leaderboard" ExpandImageTokens="true" runat="server" />
</div>
<asp:PlaceHolder ID="BreadCrumbPHFooter" Runat="server" /><!-- begin footer -->

<ul class="nav-footer clearfix">
    <asp:PlaceHolder ID="plcFooterExplanation" runat="server" Visible="False">
        <li><mn:Txt ID="txtExplanation" ExpandImageTokens="true" runat="server"></mn:Txt></li>
    </asp:PlaceHolder>
    <asp:PlaceHolder ID="plcFooter" runat="server">
        <%--<asp:PlaceHolder ID="phlSubscriptionDisplay" Runat="server">	--%>
        <li><mn:Txt ID="txtHome" runat="server" ResourceConstant="NAVHOME"></mn:Txt> / </li>
        <li><uc1:LogonBox ID="LogonBox1" runat="server" LogonResource="Unbracketed" cssClass="footerlink"></uc1:LogonBox> / </li>
        <asp:PlaceHolder ID="plcContactUs" runat="server" Visible="true">
            <li><mn:Txt ID="txtContactUs" runat="server" ResourceConstant="NAV_CONTACTUS" CssClass="footerlink"></mn:Txt> / </li>
        </asp:PlaceHolder>
        <asp:PlaceHolder ID="plcMemberServices" runat="server" Visible="true">
            <li><mn:Txt ID="txtMemberServices" runat="server" ResourceConstant="MEMBER_SERVICES" Height="11px" CssClass="footerlink"></mn:Txt> / </li>
        </asp:PlaceHolder>
        <asp:PlaceHolder ID="plcMembership" Visible="true" runat="server">
            <li><mn:Txt ID="txtMembership" runat="server" ResourceConstant="ACTIVATE_MEMBERSHIP" CssClass="footerlink"></mn:Txt> / </li>
        </asp:PlaceHolder>
        <asp:PlaceHolder ID="plcSubscribe" Visible="false" runat="server">
            <li><mn:Txt ID="txtSubscribe" runat="server" ResourceConstant="SUBSCRIBE"></mn:Txt></li>
        </asp:PlaceHolder>
    </asp:PlaceHolder>
</ul>
<asp:PlaceHolder runat="server" ID="plcFooterIL" Visible="false">
    <div id="footer-links-main" class="clearfix">
        <ul class="four-column">
            <li class="footer-group-title"><mn:Txt ID="Txt1" runat="server" ResourceConstant="NAVTITLE" /></li>
            <li><mn:Txt ID="txtHomeDatingIL" runat="server" ResourceConstant="NAVHOME_DATING"></mn:Txt></li>
            <li><mn:Txt ID="txtHomeIL" runat="server" ResourceConstant="NAVHOME"></mn:Txt></li>
            <li><mn:Txt ID="txtContactUsIL" runat="server" ResourceConstant="NAV_CONTACTUS" CssClass="footerlink" /></li>
            <li><mn:Txt runat="server" ResourceConstant="NAV_ABOUTUS" /></li>
            <li class="showHide"><mn:Txt runat="server" ResourceConstant="NAV_JMAG" /></li>
            <li><mn:Txt runat="server" ResourceConstant="NAV_SAFETY" /></li>
            <li class="showHide"><mn:Txt ID="txtSubscribeIL" runat="server" ResourceConstant="SUBSCRIBE"></mn:Txt></li>
            <li><mn:Txt ID="txtHelpILCupid" runat="server" ResourceConstant="NAV_HELP2"></mn:Txt></li>
            <li><mn:Txt runat="server" ResourceConstant="NAV_OUR_MISSION" /></li>
            <li><mn:Txt ID="TxtNavColumnRow" runat="server" ResourceConstant="NAV_COLUMN1_1_ROW_9" /></li>
            <li><mn:Txt runat="server" ResourceConstant="NAV_WIKI_IL" /></li>
            <li class="showHide"><mn:Txt runat="server" ResourceConstant="NAV_TNC_IL" /></li>
        </ul>
        <ul class="four-column">
            <li class="footer-group-title"><mn:Txt runat="server" ResourceConstant="NAV_MOREON" /></li>
            <li><mn:Txt runat="server" ResourceConstant="NAV_HELP" /></li>
            <li><mn:Txt runat="server" ResourceConstant="NAV_SITEMAP" /></li>
            <li><mn:Txt runat="server" ResourceConstant="NAV_WIKI" /></li>
            <li class="showHide"><mn:Txt runat="server" ResourceConstant="NAV_TNC" /></li>
            <li><mn:Txt runat="server" ResourceConstant="NAV_NEWS_CENTER" /></li>
            <li><mn:Txt runat="server" ResourceConstant="NAV_GIFT" /></li>
            <asp:PlaceHolder runat="server" ID="plcSuccessStoriesLink" Visible="false">
            <li><mn:Txt ID="Txt7" runat="server" ResourceConstant="NAV_SUCCESS_STORIES" /></li>
            <li><mn:Txt runat="server" ResourceConstant="NAV_SAFETY_IL" /></li>
            </asp:PlaceHolder>
            <li><mn:Txt runat="server" ResourceConstant="NAVIL_PAIRING" /></li>
            <li><mn:Txt runat="server" ResourceConstant="NAVIL_SINGLES" /></li>
            <li><mn:Txt runat="server" ResourceConstant="NAVIL_BLINDATE" /></li>
        </ul>
        <ul class="four-column hide">
            <li class="footer-group-title"><mn:Txt runat="server" ResourceConstant="NAVIL_COMMUNITIES" /></li>
            <li><mn:Txt runat="server" ResourceConstant="NAVIL_RELIGIOUS" /></li>
            <li><mn:Txt runat="server" ResourceConstant="NAVIL_ACADEMIC" /></li>
            <li><mn:Txt runat="server" ResourceConstant="NAVIL_DIVORCED" /></li>
            <li><mn:Txt runat="server" ResourceConstant="NAVIL_ELDERLY" /></li>
        </ul>
        <ul class="four-column">
            <li class="footer-group-title"><mn:Txt runat="server" ResourceConstant="NAV_FRIENDS" /></li>
            <li><uc1:LogonBox ID="LogonBox2" runat="server" LogonResource="Unbracketed" cssClass="footerlink" /></li>
            <li><mn:Txt ID="txtMembershipIL" runat="server" ResourceConstant="ACTIVATE_MEMBERSHIP" CssClass="footerlink" /></li>
            <li><mn:Txt ID="txtMemberServicesIL" runat="server" ResourceConstant="MEMBER_SERVICES" Height="11px" CssClass="footerlink" /></li>
            <li><mn:Txt  runat="server" ResourceConstant="PRIVACY_STATEMENT" CssClass="footerlink" /></li>
            <li class="showHide"><mn:Txt runat="server" ResourceConstant="NAV_JMETER" /></li> 
            <li><mn:Txt ID="txtSubscribeILCupid" runat="server" ResourceConstant="SUBSCRIBE2"></mn:Txt></li>  
            <li><mn:Txt ID="txtGooglePlusIL" runat="server" ResourceConstant="NAV_GOOGLE_PLUS" CssClass="footerlink" /></li>         
        </ul>
        <ul class="four-column">
            <li class="footer-group-title"><mn:Txt runat="server" ResourceConstant="NAV_SPARK_NETWORKS" /></li>
            <li><mn:Txt runat="server" ResourceConstant="NAV_ABOUT_SPARK" /></li>
            <li><mn:Txt runat="server" ResourceConstant="NAV_PARTNERSHIP" /></li>
            <li><mn:Txt runat="server" ResourceConstant="NAV_NEWS" /></li>
            <li><mn:Txt runat="server" ResourceConstant="NAV_INVEST" /></li>
            <li><mn:Txt runat="server" ResourceConstant="NAV_PROPERTY" /></li>
            <li><mn:Txt runat="server" ResourceConstant="NAV_SITES" /></li>
            <li><mn:Txt runat="server" ResourceConstant="NAV_ADVERTISE" /></li>
        </ul>
    </div>
    <div class="text-center">
        <mn:Copyright ID="copyrightIL" runat="server"></mn:Copyright>
    </div>
</asp:PlaceHolder>
<asp:PlaceHolder runat="server" ID="plcFooterSpark" Visible="false">
    <div id="footer-columns">
        <ul class="four-column">
            <li class="footer-group-title"><mn:Txt ID="Txt3" runat="server" ResourceConstant="NAVTITLE" /></li>
            <li><mn:Txt ID="txtHomeSpark" runat="server" ResourceConstant="NAVHOME"></mn:Txt></li>
            <li><uc1:LogonBox ID="LogonBox3" runat="server" LogonResource="Unbracketed" cssClass="footerlink" /></li>
            <li><mn:Txt ID="txtContactUsSpark" runat="server" ResourceConstant="NAV_CONTACTUS" CssClass="footerlink" /></li>
            <li><mn:Txt ID="txtMemberServicesSpark" runat="server" ResourceConstant="MEMBER_SERVICES" Height="11px" CssClass="footerlink" /></li>
            <li><mn:Txt ID="txtMembershipSpark" runat="server" ResourceConstant="ACTIVATE_MEMBERSHIP" CssClass="footerlink" /></li>
                  
        </ul>
        <ul class="four-column">
            <li class="footer-group-title"><mn:Txt ID="Txt8" runat="server" ResourceConstant="NAV_MOREON" />&nbsp;</li>
             <li><mn:Txt ID="Txt4" runat="server" ResourceConstant="NAV_ABOUTUS" /></li>
            <%--<li><mn:txt id="linkSparkTV" runat="server" resourceconstant="NAV_SPARKTV" expandimagetokens="true" /></li>--%>
            <li><mn:Txt ID="Txt6" runat="server" ResourceConstant="NAV_SAFETY" /></li>
            <li><mn:Txt ID="Txt9" runat="server" ResourceConstant="NAV_HELP" /></li>
            <li><mn:Txt ID="Txt10" runat="server" ResourceConstant="NAV_SITEMAP" /></li>
             <li><mn:Txt ID="txtSubscribeSpark" runat="server" ResourceConstant="SUBSCRIBE"></mn:Txt></li>

        </ul>
        <ul class="four-column">
            <li class="footer-group-title"><mn:Txt ID="Txt16" runat="server" ResourceConstant="NAV_SPARK_NETWORKS" /></li>
            <li><mn:Txt ID="Txt5" runat="server" ResourceConstant="NAV_ABOUT_SPARK" /></li>
            <li><mn:Txt ID="Txt25" runat="server" ResourceConstant="NAV_ADVERTISE" /></li>
                  <li><mn:Txt ID="Txt23" runat="server" ResourceConstant="NAV_PROPERTY" /></li>
             <li><mn:Txt ID="Txt30" runat="server" ResourceConstant="NAV_INVEST" /></li>
            <li><mn:Txt ID="Txt29" runat="server" ResourceConstant="NAV_LOCAL_DATING" /></li>
                                    
        </ul>
        <ul class="four-column">
            <li class="footer-group-title"><mn:Txt ID="Txt18" runat="server" ResourceConstant="NAV_MOREON" />&nbsp;</li>
              <li><mn:Txt ID="Txt24" runat="server" ResourceConstant="NAV_SITES" /></li>
             <li><mn:Txt ID="Txt28" runat="server" ResourceConstant="NAV_PARTNERSHIP" /></li>
             <li><mn:Txt ID="Txt21" runat="server" ResourceConstant="NAV_PRIVACY" /></li>
              <li><mn:Txt ID="Txt12" runat="server" ResourceConstant="NAV_TNC" /></li>
              <li><mn:Txt ID="Txt2" runat="server" ResourceConstant="NAV_COOKIE" /></li>
        </ul>
    </div>        
    
    <div class="footer-copyright">
        <mn:Copyright ID="copyrightSpark" runat="server"></mn:Copyright>
    </div>
    
    <div class="clearfix"></div>
    
    
</asp:PlaceHolder>
<asp:PlaceHolder ID="plcFooterHtml" Visible="true" runat="server">
    <mn:Txt ID="txtFooterHtml" runat="server" ResourceConstant="FOOTER_HTML"></mn:Txt>
</asp:PlaceHolder>
<mn:Copyright ID="copyright" runat="server"></mn:Copyright>
<asp:Panel ID="PanelActivatePageObjects" runat="server">

    <script type="text/javascript">
        activatePageObjects();
    </script>

</asp:Panel>
<!-- end footer -->
<mn:Txt ID="TxtPreloadImages" runat="server" ResourceConstant="TXT_PRELOAD_IMAGES_FOOTER">
</mn:Txt>
<%-- put below in cc conditional container --%>
<asp:PlaceHolder ID="phColorCodeHelpContainer" runat="server">
    <div id="cc-pic-tag-help" class="cc-pic-tag-help">
        <b>What does this mean?</b><br />
        Click the bar to find out!</div>
    <div id="cc-info-modal" class="cc-modal ui-modal-window">
        <div class="close">
            <span title="Close" class="spr s-icon-close"><span>Close</span></span> <a href="#">Close</a></div>
        <h2>What is the Color Code?</h2>
        <p>The Color Code icon tells you that this member has taken the FREE Color Code Personality Test and wants to share his/her results with you. You can read all about their Color Code profile on the new Color Code tab.</p>
        <p>The Color Code, by Dr. Taylor Hartman, is one of the most insightful personality tests in existence, and is made up of four personality Colors, or driving Core Motives. The Color Code teaches you the real motives behind your actions and how to better relate to other personality types.</p>
        <p>Don't miss this chance to get valuable, life-changing personal insight, it only takes less than 15 minutes and it's FREE!</p>
        <asp:PlaceHolder ID="phColorCodeModalTakeTest" runat="server">
            <div id="cc-info-modal-more">
                Are you a <span class="cc-red">RED</span>, <span class="cc-blue">BLUE</span>, <span class="cc-white">WHITE</span>, or <span class="cc-yellow">YELLOW</span>?<br />
                <a href="/Applications/ColorCode/Landing.aspx?colortracking=miniprofilewit">Find out now! &raquo;</a>
            </div>
        </asp:PlaceHolder>
        <div id="cc-info-modal-fp-link">
            <p>Want to read more about this member's color code?</p>
            <p><a id="cc-goto-fp-cctab" href="#" class="link-primary">Read more now</a></p>
        </div>
    </div>
        
    <div id="cc-help-modal" class="cc-modal ui-modal-window">
        <div class="close"><span title="Close" class="spr s-icon-close"><span></span></span><a href="#">Close</a></div>
        <h2>The Color Code Personality Test Help</h2>
        <h3>Why am I asked to respond to the questions as a child?</h3>
        <p>Any woman who has more than one child can tell you how different each child is from birth. You are born with your personality intact. The Color Code is based on your natural, innate personality type. Of course, people may change over time, but your Core Color will always remain the same.</p>
        <h3>How do I respond to those questions where I don't really fit any of the four options?</h3>
        <p>Sometimes it is difficult to choose from the four options given, but there is usually one that fits better than the rest, or at least one that you could see yourself as more so than the others. If you're still unsure, try asking someone who knows you well, such as a parent or sibling. They can give clarity on areas of your personality that you may not notice.</p>
    </div>
</asp:PlaceHolder>
<%-- put above in cc conditional container --%>
