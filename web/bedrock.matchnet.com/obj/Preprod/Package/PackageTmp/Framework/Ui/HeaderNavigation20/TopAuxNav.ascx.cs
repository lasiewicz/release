﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using Matchnet.Web.Framework;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Web.Framework.TemplateControls;
using Matchnet.Web.Framework.Menus;
using Matchnet.Web.Framework.Managers;
namespace Matchnet.Web.Framework.Ui.HeaderNavigation20
{
    public partial class TopAuxNav : FrameworkControl
    {
        //private bool renderAdHeaderTopIFrameIDToJS = false;
        private string IMG_LOGO_FILE_NAME = "logo-header.png";
        //private const string IMG_LOGO_FILE_NAME = "logo-header-img.png";
        private Matchnet.Web.Framework.Menus.Menu _menus;
        protected string _SocialNavLeaderboardAdCSS = "";
        private bool _IsSocialNavEnabled = false;
        private bool _AlreadyBindFirstMenuItem = false;
        public bool ApiMenu { get; set; }
        private string _absolutehosthref = "";
        public string AbsoluteHostHref { get { return _absolutehosthref; } } 

        protected override void OnInit(EventArgs e)
        {
            try
            {
                base.OnInit(e);

                IMG_LOGO_FILE_NAME = g.GetResource("LOGO_NAME", this);
                    

                if (ApiMenu)
                {
                    string apiimagehost = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("API_CALL_IMAGE_HOST", _g.Brand.Site.Community.CommunityID, _g.Brand.Site.SiteID);
                    _absolutehosthref = "http://" + apiimagehost + "." + _g.Brand.Uri;

                }
                _IsSocialNavEnabled = MenuUtils.IsSocialNavEnabled(g) && (!g.IsSiteMingle || Matchnet.Web.Applications.MemberProfile.ProfileTabs30.ProfileUtility.IsProfile30Enabled(g));

                string menufile = String.Format("/Framework/Ui/HeaderNavigation20/XML/TopAuxMenu.{0}.xml", g.Brand.Site.SiteID);
                _menus = MenuUtils.GetMenuItems(g, menufile, String.Format("TOP_AUX_MENU_{0}", g.Brand.Site.SiteID));

                if (_IsSocialNavEnabled)
                {
                    phNavTopHeaderSocialNav.Visible = true;
                    rptMenuSocialNav.DataSource = _menus.MenuItems;
                    rptMenuSocialNav.DataBind();

                    if (g.Member != null)
                    {
                        //logged in version of social nav
                        phLoggedInSocialNav.Visible = true;

                        //logout link moved into xml file
                        //if (!ApiMenu)
                        //    txtMenuItemLogout.Href = FrameworkGlobals.LinkHref("/Applications/Logon/Logoff.aspx", true);
                        //else
                        //{
                        //     txtMenuItemLogout.Href = _absolutehosthref + "/Applications/Logon/Logoff.aspx";
                        
                        //}

                        bool useFirstnameOverUsername = Convert.ToBoolean(RuntimeSettings.GetSetting("USE_FIRSTNAME_OVER_USERNAME", _g.Brand.Site.Community.CommunityID, _g.Brand.Site.SiteID));
                        if (useFirstnameOverUsername)
                        {
                            hplUsername.Text = g.Member.GetAttributeText(_g.Brand, "SiteFirstName", g.Member.GetUserName(g.Brand)); //FrameworkGlobals.Ellipsis(g.Member.Username, 9);
                            hplUsername.ToolTip = g.GetResource("FIRSTNAME_OVER_USERNAME_TOOLTIP", this);
                        }
                        else
                        {
                            hplUsername.Text = g.Member.GetUserName(g.Brand); //FrameworkGlobals.Ellipsis(g.Member.Username, 9);
                            hplUsername.ToolTip = g.Member.GetUserName(g.Brand);
                        }

                        string viewProfileUrl = "";
                        if (!ApiMenu)
                        {
                            viewProfileUrl = FrameworkGlobals.LinkHref("/Applications/MemberProfile/ViewProfile.aspx?EntryPoint=99999", true);
                        }
                        else
                        {
                            viewProfileUrl = _absolutehosthref + "/Applications/MemberProfile/ViewProfile.aspx?EntryPoint=99999";
                        }
                        hplUsername.NavigateUrl = viewProfileUrl;

                        //show hidden profile status
                        if ((g.Member.GetAttributeInt(g.Brand, "HideMask") & (Int32)WebConstants.AttributeOptionHideMask.HideSearch) == (Int32)WebConstants.AttributeOptionHideMask.HideSearch)
                        {
                            phProfileStatus.Visible = true;
                        }

                    }
                    else
                    {
                        //logged out version of social nav
                        phLoggedOutSocialNav.Visible = true;
                        if (!ApiMenu)
                            txtJoinNow.Href = FrameworkGlobals.LinkHref(FrameworkGlobals.GetRegistrationPageURL(g), true);
                        else
                        {
                            txtJoinNow.Href = _absolutehosthref + FrameworkGlobals.GetRegistrationPageURL(g);
                        }

                    }

                    if (MenuUtils.IsTopLeaderboardAdEnabled(g))
                    {
                        headerCobrand.Attributes["class"] += " ad-on";
                    }
                    else
                    {
                        headerCobrand.Attributes["class"] += " ad-off"; //need "ad-off"?
                    }

                    // display the facebook like link or not
                    phFacebookLike.Visible = Convert.ToBoolean(RuntimeSettings.GetSetting("ENABLE_FACEBOOK_LIKE", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID));
                    if (phFacebookLike.Visible)
                    {
                        litFacebookLikeCode.Text = _menus.FacebookLikeCode;
                    }

                    // display Google+1
                    phGooglePlus1.Visible = Convert.ToBoolean(RuntimeSettings.GetSetting("ENABLE_GOOGLE_PLUS_1", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID));
                    if (phGooglePlus1.Visible)
                    {
                        litGooglePlus1Code.Text = _menus.GooglePlus1Code;
                    }

                    // display Twitter follow button
                    phTwitterFollow.Visible = Convert.ToBoolean(RuntimeSettings.GetSetting("ENABLE_TWITTER_FOLLOW", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID));
                    if (phTwitterFollow.Visible)
                    {
                        litTwitterFollowCode.Text = _menus.TwitterFollowCode;
                    }

                }
                else
                {
                    if (g.Member != null)
                    {
                        phNavTopHeaderOriginal.Visible = true;
                        rptMenu.DataSource = _menus.MenuItems;
                        rptMenu.DataBind();

                        //logged out version of social nav
                    }
                    else
                    {

                        phNavTopHeaderSocialNav.Visible = true;

                        phLoggedOutSocialNav.Visible = true;
                        if (!ApiMenu)
                            txtJoinNow.Href = FrameworkGlobals.LinkHref(FrameworkGlobals.GetRegistrationPageURL(g), true);
                        else
                        {
                            txtJoinNow.Href = _absolutehosthref + FrameworkGlobals.GetRegistrationPageURL(g);
                        }
                    }
                }
            }
            catch (Exception ex)
            { g.ProcessException(ex); }

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            string imgLogoFileName = IMG_LOGO_FILE_NAME;
            string navigateURL = string.Empty;
            bool isPromoOn =
                Convert.ToBoolean(RuntimeSettings.GetSetting("PROMO_LOGO_ON", g.Brand.Site.Community.CommunityID,
                                                             g.Brand.Site.SiteID, g.Brand.BrandID));

            if (isPromoOn)
            {
                imgLogoFileName = g.GetResource("IMG_PROMO_LOGO", this);
                navigateURL = g.GetResource(RuntimeSettings.GetSetting("PROMO_LOGO_NAVIGATE_URL_RESOURCE_CONSTANT",
                                                                       g.Brand.Site.Community.CommunityID,
                                                                       g.Brand.Site.SiteID, g.Brand.BrandID), this);

                // Set target so it opens in a new window
                imgLogo.Target = "_blank";
                imgLogoNo.Target = "_blank";
                imgLogoSocialNav.Target = "_blank";

                // title / tooltip text
                string titleResourceConstant = RuntimeSettings.GetSetting("PROMO_LOGO_TITLE_RESOURCE_CONSTANT",
                                                                          g.Brand.Site.Community.CommunityID,
                                                                          g.Brand.Site.SiteID, g.Brand.BrandID);
                imgLogo.ResourceConstant = titleResourceConstant;
                imgLogoNo.ResourceConstant = titleResourceConstant;
                imgLogoSocialNav.ResourceConstant = titleResourceConstant;

                imgLogo.TitleResourceConstant = titleResourceConstant;
                imgLogoNo.TitleResourceConstant = titleResourceConstant;
                imgLogoSocialNav.TitleResourceConstant = titleResourceConstant;

                // add extra css class
                headerCobrand.Attributes["class"] = headerCobrand.Attributes["class"] + " promo-logo";
            }

            // figure out the image path
            string imagePath = Matchnet.Web.Framework.Image.GetURLFromFilename(imgLogoFileName);
            if (g.BrandAlias != null)
            {
                imagePath = Matchnet.Web.Framework.Image.GetBrandAliasImagePath(imgLogoFileName, g.BrandAlias);
            }

            // navigate url setting
            string host = HttpContext.Current.Request.Url.Host;

            string url = string.Empty;
            if (g.AppPage != null)
            {
                string pagename = g.AppPage.PageName.ToLower();

                if (g.Member != null && pagename != "logon" && pagename != "logoff")
                    url = "http://" + host + "/Applications/Home/Default.aspx";
                else
                    url = "http://" + host + "/";
            }

            imgLogo.NavigateUrl = url;
            imgLogoNo.NavigateUrl = url;
            imgLogoSocialNav.NavigateUrl = url;
            if (ApiMenu)
            {
                imgLogo.NavigateUrl = _absolutehosthref;
                imgLogoNo.NavigateUrl = _absolutehosthref;
                imgLogoSocialNav.NavigateUrl = _absolutehosthref;
            }
            if (isPromoOn)
            {
                imgLogo.NavigateUrl = navigateURL;
                imgLogoNo.NavigateUrl = navigateURL;
                imgLogoSocialNav.NavigateUrl = navigateURL;
            }

            // setting the actual image logo
            headerCobrand.Style.Add("background-image", "url(" + imagePath + ")");
        }

        protected override void OnPreRender(EventArgs e)
        {
            if (_IsSocialNavEnabled)
            {
                // check for online status onprerender (no page_load) so that it fires AFTER saving of displaysettings 16766 to get most recent hidemask
                // hide Instant Messenger checkIM if this page does not support checking IM
                // Don't check IM on page where we don't want interruptions
                // such as during subscription, initial registration and admin pages (related bug 15463)
                // Don't check IM if user has chosen to be hidden.
                // Also, if the page is Unsuspend, don't load Checkim because FrameBuster will hijack the page
                // (this might not be the best place to do this, however time is of the essence - this note borrowed from elsewhere)
                string controlname = g.AppPage != null ? g.AppPage.PageName : "";
                bool adminpage = g.AppPage != null ? Matchnet.Web.Lib.LayoutTemplateBase.IsAdminPage(g) : false;
                if (!_g.SilentCheckIM
                    && g.Member != null
                    && controlname != "Unsuspend"
                    && g.Member.GetAttributeInt(g.Brand, "SelfSuspendedFlag") != 1
                    && !adminpage
                    )
                {
                    //plcCheckIM.Visible = true;
                    //show online status as hidden if user wants to be hidden (should not check IM's or add to MOL. That is handled elsewhere by javascript)
                    plcIMHidden.Visible = (g.Member.GetAttributeInt(g.Brand, "HideMask") & (Int32)WebConstants.AttributeOptionHideMask.HideMembersOnline) == (Int32)WebConstants.AttributeOptionHideMask.HideMembersOnline;
                    plcIMActive.Visible = !plcIMHidden.Visible;

                    //im status
                    if (Request.QueryString["showimstatus"] != null) { this.plcIMOnlineDebug.Visible = true; }

                }

                //hide Active IM section if member is blocked
                if (_g.EmailVerify.IsMemberBlocked(_g.Member))
                {
                    plcIMActive.Visible = false;
                }
            }
            base.OnPreRender(e);
        }


        public void SetTopNavVisibility(bool TopNavVisible, bool NoNavVisible)
        {
            NavTopHeader.Visible = TopNavVisible;
            NoNavTopHeader.Visible = NoNavVisible;
        }

        public void BindMenuItem(object sender, RepeaterItemEventArgs e)
        {
            Matchnet.Web.Framework.Menus.MenuItem i = (Matchnet.Web.Framework.Menus.MenuItem)e.Item.DataItem;
            if (e.Item.ItemType == ListItemType.Footer || e.Item.ItemType == ListItemType.Header)
            { return; }

            Txt itm = (Txt)e.Item.FindControl("txtMenuItem");
            PlaceHolder plcMenuItem = (PlaceHolder)e.Item.FindControl("plcMenuItem");

            if (!string.IsNullOrEmpty(i.ConfigurationCondition))
            {
                bool visible = MenuUtils.IsSettingConfiguredVisible(i.ConfigurationCondition, g);
                plcMenuItem.Visible = visible;
            }

            if (!string.IsNullOrEmpty(i.ShowConditionFunction))
            {
                bool visible = MenuUtils.IsConditionallyVisible(i.ShowConditionFunction, g);
                plcMenuItem.Visible = visible;
            }

            HtmlGenericControl liControl = (HtmlGenericControl)e.Item.FindControl("liMenu");
            itm.ResourceConstant = i.ResourceConstant;

            if (!String.IsNullOrEmpty(i.Link))
            {
                itm.Href = i.GetExpandedLink(g);
                if (!String.IsNullOrEmpty(i.Target))
                    itm.Target = i.Target;
            }
            else
                itm.Href = MenuUtils.HandleCustomLink(i, g);


            if (ApiMenu && itm.Href.IndexOf("http") < 0)
            {
                string href = _absolutehosthref + HttpUtility.HtmlEncode(itm.Href);
                itm.Href = href;
            }
            if (i.MenuItems != null && i.MenuItems.Count > 0)
            {
                Repeater rptsubItems = (Repeater)e.Item.FindControl("rptSubMenu");
                rptsubItems.DataSource = i.MenuItems;
                rptsubItems.DataBind();
            }
            if (MenuUtils.IsMenuSelected(i, g))
            {
                itm.Attributes.Add("class", "current");
                // liControl.Attributes.Add("class", "current"); 
            }

            if (!string.IsNullOrEmpty(i.LIClass))
            {
                liControl.Attributes.Add("class", i.LIClass);
            } 


            
        }
    }

    public class TopAuxLink
    {
        public const string MessageSettings = "/Applications/Email/MessageSettings.aspx?NavPoint=top";
        public const string AdminHome = "/Applications/Admin/MemberSearch/Search.aspx?NavPoint=top";
        public const string MemberServices = "/Applications/MemberServices/MemberServices.aspx?NavPoint=top";
        public const string Events = "javascript:window.open('http://static.jdate.com/microsites/travel2014/', '_blank');";
        public const string EventsJDIL = "http://premium.jdate.co.il";
        public const string FAQ = "/Applications/Article/FAQMain.aspx?NavPoint=top";
        public const string JMag = "http://www.jdate.com/jmag/default.htm";
        public const string ContactUs = "/Applications/ContactUs/ContactUs.aspx?NavPoint=top";
        public const string Blog = "/Blog/";
    }
}
