using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Drawing.Design;
using System.Text;

using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Util;

namespace Matchnet.Web.Framework.Ui.Advertising
{
	/// <summary>
	/// The AdSlot requires the following properties to show, otherwise its not visible: Domains, SubsribeLevels, and AdRegion
	/// </summary>
	[DefaultProperty("Domains"), 
	ToolboxData("<{0}:AdSlot runat=server></{0}:AdSlot>"),
	Designer(typeof(Matchnet.Web.Framework.Ui.Advertising.AdSlot))]
	public class AdSlot : System.Web.UI.WebControls.Panel
	{
		#region Variables and Constants
		private ContextGlobal _g;
		private string				_SubscribeLevels;  // subscriber levels that see ad
		private string[]			_SubscribeLevelsArray;
		private string				_Domains; // domains that see ad
		private string[]			_DomainsArray;
		private Unit				_Hspace = Unit.Empty;
		private Unit				_Vspace = Unit.Empty;
		private Unit				_Border = Unit.Empty;
//		private Unit				_Height = Unit.Empty;
//		private Unit				_Width = Unit.Empty;
		private string				_ResourceConstant = string.Empty;
		private string				_AdRegion = string.Empty;
		private WebConstants.APP _App = WebConstants.APP.None;

//		private enum SUBSCRIBER_LEVEL:
//		{
//			NotSubscribed = 0,
//			Subscribed = 1,
//			PremiumServiceA = 2,
//			PremiumServiceB = 3,
//		    PremiumServiceC = 4
//		}

//		private const string		SUBSCRIBER_LEVEL0 = "0"; // not subscribed
//		private const string		SUBSCRIBER_LEVEL1 = "1"; // subscribed/paying member
//		private const string		SUBSCRIBER_LEVEL2 = "2"; // ...premium services A not implemented
//		private const string		SUBSCRIBER_LEVEL3 = "3"; // ...premium services B not implemented
//		private const string		SUBSCRIBER_LEVEL4 = "4"; // ...premium services C not implemented
		#endregion

		#region Properties
		[Bindable(true), Category("Appearance"), DefaultValue("")]
		public string SubscribeLevels
		{
			get
			{
				return _SubscribeLevels;
			}

			set
			{
				_SubscribeLevels = value;
				_SubscribeLevelsArray = _SubscribeLevels.Split(';');
			}
		}

		[Bindable(true), Category("Appearance"), DefaultValue("")]
		public string Domains
		{
			get
			{
				return _Domains;
			}

			set
			{
				_Domains = value;
				_DomainsArray = _Domains.Split(';');
			}
		}

		[Bindable(true), Category("Appearance"), DefaultValue("")]
		public string AdRegion
		{
			get
			{
				if (_AdRegion == string.Empty || _AdRegion == null)
				{
					_AdRegion = WebConstants.ADREGION_NONE;
				}
				return _AdRegion;
			}

			set
			{
				_AdRegion = value;
			}
		}

		[Bindable(true), Category("Appearance"), DefaultValue("")] 
		public WebConstants.APP App
		{
			get
			{			
				if (_App == WebConstants.APP.None && (_g.AppPage.App.ID != (int)WebConstants.APP.None))
				{				
					_App = (WebConstants.APP)_g.AppPage.App.ID;
				}
				return _App;
			}

			set
			{
				_App = value;
			}
		}

		[Bindable(true), Category("Appearance"), DefaultValue("")] 
		public Unit Hspace 
		{
			get { return _Hspace; }
			set { _Hspace = value;}
		}

		[Bindable(true), Category("Appearance"), DefaultValue("")] 
		public Unit Vspace 
		{
			get { return _Vspace; }
			set { _Vspace = value; }
		}

		[Bindable(true), Category("Appearance"), DefaultValue("")] 
		public Unit Border
		{
			get { return _Border; }
			set { _Border = value; }
		}

//		[Bindable(true), Category("Appearance"), DefaultValue("")] 
//		public Unit Height
//		{
//			get { return _Height; }
//			set { _Height = value; }
//		}
//
//		[Bindable(true), Category("Appearance"), DefaultValue("")] 
//		public Unit Width
//		{
//			get { return _Width; }
//			set { _Width = value; }
//		}

		[Bindable(true), Category("Appearance"), DefaultValue("")] 
		public string ResourceConstant
		{
			get 
			{
				return _ResourceConstant; 
			}

			set 
			{ 
				_ResourceConstant = value; 
			}
		}
		#endregion

		public AdSlot()
		{
			if ( Context != null )
			{
				if ( Context.Items["g"] != null )
					_g = (ContextGlobal) Context.Items["g"];
			}
			else
			{
				_g = new ContextGlobal( null );
			}
		}

		protected override void OnInit(EventArgs e)
		{
			base.OnInit (e);

			this.Visible = false;

			try
			{
				// check if member's in subscriber level - at the moment, 1/9/06, theres only one level implemented in the framework
				if (_SubscribeLevelsArray != null)
				{
					if (_g.Member != null) // for exit pages, theres no member
					{
						string memberSubscribeLevel = CheckSubscriberLevel();
						foreach (string subLevel in _SubscribeLevelsArray)
						{
							if (subLevel.Equals(memberSubscribeLevel))
							{
								this.Visible = true; // !this.Visible;
								break;
							}
						}
					}
				}

				// check if member is in domain
				if (_DomainsArray != null)
				{
					string domainAbbreviation = BrandUtils.GetDomainAbbreviation(_g.Brand.Site.Community.CommunityID);
					foreach (string domain in _DomainsArray)
					{
						if (domain.ToLower().Equals(domainAbbreviation.ToLower()))
						{
							this.Visible = true; // !this.Visible;
							break;
						}
					}
				}

				if (_AdRegion == string.Empty || _AdRegion == WebConstants.ADREGION_NONE)
				{
					this.Visible = false;
				}
			}
			catch(Exception ex)
			{
				_g.ProcessException(ex);
			}
		}

		// add the banner image
		protected override void Render(HtmlTextWriter writer)
		{
			try
			{
				if (this.Visible && _g.GetResource(ResourceConstant).Trim() != string.Empty)
				{
					// set the resource and dimensions per per ad region
					// SetAdSlotProperties();
					writer.WriteLine(_g.GetResource(ResourceConstant));
				}
				else
				{
				}
			}
			catch(Exception ex)
			{
				_g.ProcessException(ex);
			}
		}


		#region helper methods
		private string CheckSubscriberLevel()
		{
			string sl = string.Empty;
			if (_g.TargetBrand.IsPaySite) 
			{
				if (!MemberPrivilegeAttr.IsCureentSubscribedMember(_g) || _g.IsAdmin) 
				{
					sl = "1";
				}
				// in the futrue we'd check for other types of subscriptions:  ...IsPayingMemberPlus ...IsPayingMemberUltraSuperDuper ...and so on.
				/*
				 * the comments from above is partially true now with MemberPrivilegeAttr class -Hung */
			}

			return sl;
		}

		#region unused
//		private void SetAdSlotProperties()
//		{
//			if (AdRegion != string.Empty)
//			{
//				switch (AdRegion)
//				{
//					case WebConstants.ADREGION_A1:
//						// left skyscraper
//						ResourceConstant = "ADREGION_A1";
//						Width = 160;
//						Height = 600;
//						break;
//					case WebConstants.ADREGION_A2:
//						// right content area skyscraper
//						ResourceConstant = "ADREGION_A2";
//						Width = 160;
//						Height = 600;
//						break;
//					case WebConstants.ADREGION_A3:
//						// farthest right skyscraper
//						ResourceConstant = "ADREGION_A3";
//						Width = 160;
//						Height = 600;
//						break;
//					case WebConstants.ADREGION_A4:
//						// bottom of page
//						ResourceConstant = "ADREGION_A4";
//						Width = 728;
//						Height = 90;
//						break;
//					case WebConstants.ADREGION_C1:
//						// bottom of page
//						ResourceConstant = "ADREGION_C1";
//						Width = 264;
//						Height = 60;
//						break;
//					case WebConstants.ADREGION_L1:
//						// log out page (huge ad)
//						ResourceConstant = "ADREGION_L1";
//						Width = 400;
//						Height = 600;
//						break;
//					case WebConstants.ADREGION_M1:
//						// top of page
//						ResourceConstant = "ADREGION_M1";
//						Width = 590;
//						Height = 60;
//						break;
//					case WebConstants.ADREGION_M2:
//						// left skyscraper
//						ResourceConstant = "ADREGION_M2";
//						Width = 160;
//						Height = 600;
//						break;
//					case WebConstants.ADREGION_M3:
//						// right skyscraper
//						ResourceConstant = "ADREGION_M3";
//						Width = 160;
//						Height = 600;
//						break;
//					case WebConstants.ADREGION_M4:
//						// bottom of page
//						ResourceConstant = "ADREGION_M4";
//						Width = 728;
//						Height = 90;
//						break;
//					case WebConstants.ADREGION_S1:
//						// right skyscraper
//						ResourceConstant = "ADREGION_S1";
//						Width = 160;
//						Height = 600;
//						break;
//					case WebConstants.ADREGION_S2:
//						// bottom of page
//						ResourceConstant = "ADREGION_S2";
//						Width = 728;
//						Height = 90;
//						break;
//					case WebConstants.ADREGION_V1:
//						// bottom of page
//						ResourceConstant = "ADREGION_V1";
//						Width = 468;
//						Height = 60;
//						break;
//					case WebConstants.ADREGION_NONE:
//						// this would've already been check in the OnInit
//						this.Visible = false;
//						break;
//				}
//		
//			}
//			}
		#endregion
		
		#endregion
	}
}
