﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SubscriptionExpirationAlert.ascx.cs"
    Inherits="Matchnet.Web.Framework.Ui.PageAnnouncements.SubscriptionExpirationAlert" %>
<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Register TagPrefix="mn2" Namespace="Matchnet.Web.Framework.Ui.FormElements" Assembly="Matchnet.Web" %>
<div class="content-announcement">
    <div class="welcome-box">
        <h2>
            <!--<mn:Txt ID="txtAnnouncementTitle" ResourceConstant="TXT_ANNOUNCEMENT_TITLE" runat="server" />-->
            <asp:Label runat="server" ID="lblAnnouncementTitle"></asp:Label></h2>
        <a class="click-close" data-expires="<%=CookieExpirationHours %>" href="#">
            <mn:Txt ID="txtClose" ResourceConstant="TXT_CLOSE" runat="server" />
        </a>
        <div class="text-intro">
            <!--<mn:Txt ID="txtAnnouncementIntro" ResourceConstant="TXT_ANNOUNCEMENT_INTRO" runat="server" />-->
            <asp:Label runat="server" ID="lblAnnouncementIntro"></asp:Label>
        </div>
        <div class="announcement-buttons clearfix">
            <div class="btn-renewal"  data-expires="<%=CookieExpirationHours %>">
                <a id="A1" runat="server" class="link-primary">
                    <mn:Txt ID="btnTxtRenewal" ResourceConstant="TXT_BTN_RENEWAL" runat="server" />
                </a>
            </div>
            <asp:PlaceHolder runat="server" ID="plcPremiumButton" Visible="False">
                <div class="btn-premium">
                    <a id="A4" href="/Applications/subscription/upsale.aspx?prtid=1710" runat="server"
                        class="link-primary">
                        <mn:Txt ID="btnTxtPremium" ResourceConstant="TXT_BTN_PREMIUM" runat="server" />
                    </a>
                </div>
            </asp:PlaceHolder>
        </div>
        <div class="announcement-loader">
            <span><mn:Txt ID="ajaxLoading" ResourceConstant="TXT_AJAX_LOADING" runat="server" /></span>
        </div>
    </div>
    <div class="thankyou-box">
        <a class="click-close" href="#">
            <mn:Txt ID="txtClose2" ResourceConstant="TXT_CLOSE" runat="server" />
        </a>
        <p class="text-intro">
            <asp:Label runat="server" ID="lblAnnouncementMessage"></asp:Label>
            <%--<mn:Txt ID="Txt4" ResourceConstant="TXT_ANNOUNCEMENT_MESSAGE_MALE" runat="server" />--%>
        </p>
        <p class="text-intro">
            <mn:Txt ID="txt2" ResourceConstant="TXT_ANNOUNCEMENT_SUCCESS" runat="server" />
        </p>
        <div class="announcement-details">
            <span>
                <mn:Txt ID="txtAnnouncementDetails" ResourceConstant="TXT_ANNOUNCEMENT_DETAILS" runat="server" />
            </span>
        </div>
        <div class="announcement-finish">
            <mn:Txt ID="txtAnnouncementFinish" ResourceConstant="TXT_ANNOUNCEMENT_FINISH" runat="server" />
        </div>
    </div>
    <div class="error-box">
        <a class="click-close" href="#">
            <mn:Txt ID="txt3" ResourceConstant="TXT_CLOSE" runat="server" />
        </a>
        <div class="error-text">
            <img src="/img/Community/JDate/page_message.gif" />
            <mn:Txt ID="txtAnnouncementError" ResourceConstant="TXT_ANNOUNCEMENT_ERROR" runat="server" />
        </div>
        <a id="A3" runat="server" class="link-primary" href="/Applications/MemberServices/AutoRenewalSettings.aspx">
            <mn:Txt ID="Txt1" ResourceConstant="TXT_BTN_RENEWAL" runat="server" />
        </a>
    </div>
</div>
