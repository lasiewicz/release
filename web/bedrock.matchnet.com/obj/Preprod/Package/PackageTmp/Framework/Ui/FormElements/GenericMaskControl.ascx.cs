using System;
using System.Collections;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace Matchnet.Web.Framework.Ui.FormElements
{


    /// <summary>
    ///		Summary description for GenericMaskControl.
    /// </summary>
    public class GenericMaskControl : FrameworkControl
    {
        private string _ulClassName;
        private int _maskAttrValue = Matchnet.Constants.NULL_INT;
        private Hashtable _nonBlankOptions = new Hashtable();
        private Hashtable _optionListOrder = new Hashtable();

        protected Repeater rptOptions;

        #region Properties
        public string ULClassName
        {
            get
            {
                if (_ulClassName == null)
                    return "optionsContainer";
                return _ulClassName;
            }
            set
            {
                _ulClassName = value;
            }

        }

        public object DataSource
        {
            set
            {
                rptOptions.DataSource = value;
            }
        }

        public string CssClass { get; set; }

        #endregion

        private void Page_Load(object sender, System.EventArgs e)
        {
            // Put user code to initialize the page here
        }

        #region Public Methods
        public void BindData()
        {
            if (rptOptions.DataSource == null)
                throw new ApplicationException("Option Repeater has no datasource");

            rptOptions.DataBind();
        }

        public int ComputeMaskAttributeValue()
        {
            int mask = 0;

            CheckBox cbOption = null;
            string maskStr = string.Empty;

            foreach (RepeaterItem item in rptOptions.Items)
            {
                if (item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem)
                {
                    cbOption = item.FindControl("cbOption") as CheckBox;

                    if (cbOption.Checked)
                    {
                        maskStr = (string)_nonBlankOptions[cbOption];
                        mask = (mask | Convert.ToInt32(maskStr));
                    }
                }
            }

            _maskAttrValue = mask;
            return mask;
        }

        public void SetSelectedMasks(int maskValue)
        {
            _maskAttrValue = maskValue;

            string maskStr = string.Empty;

            foreach (RepeaterItem item in rptOptions.Items)
            {
                if (item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem)
                {
                    CheckBox cbOption = item.FindControl("cbOption") as CheckBox;
                    maskStr = (string)_nonBlankOptions[cbOption];
                    cbOption.Checked = ContainsValue(Convert.ToInt32(maskStr));
                }
            }
        }
        #endregion

        #region Private Methods
        private void rptOptions_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                DataRowView row = e.Item.DataItem as DataRowView;
                CheckBox cbOption = e.Item.FindControl("cbOption") as CheckBox;

                _nonBlankOptions.Add(cbOption, row["Value"]);
                cbOption.Text = row["Content"].ToString();

                _optionListOrder.Add(cbOption, row["ListOrder"]);

                if (!string.IsNullOrEmpty(CssClass))
                {
                    cbOption.CssClass += " " + CssClass;
                }
            }
        }

        private bool ContainsValue(int targetValue)
        {
            if (_maskAttrValue == Matchnet.Constants.NULL_INT)
            {
                return false;
            }

            return ((_maskAttrValue & targetValue) == targetValue);
        }

        #endregion

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
            rptOptions.ItemDataBound += new RepeaterItemEventHandler(rptOptions_ItemDataBound);
        }
        #endregion

        public int ComputeMaskAttributeValueFromPost()
        {
            int mask = 0;

            CheckBox cbOption = null;
            string maskStr = string.Empty;

            foreach (RepeaterItem item in rptOptions.Items)
            {
                if (item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem)
                {
                    cbOption = item.FindControl("cbOption") as CheckBox;

                    if (cbOption!=null)
                    {
                        if (Request[cbOption.UniqueID].ToLower() == "checked")
                        {
                            maskStr = (string)_nonBlankOptions[cbOption];
                            mask = (mask | Convert.ToInt32(maskStr));
                        }
                    }
                }
            }

            _maskAttrValue = mask;
            return mask;
        }
    }
}
