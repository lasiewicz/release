using System;

using Spark.SAL;

namespace Matchnet.Web.Framework.Ui.FormElements
{
	/// <summary>
	/// Summary description for SearchPreferenceControl.
	/// </summary>
	public class SearchPreferenceControl : FrameworkControl
	{
		public SearchPreferenceControl()
		{
			this.Load += new EventHandler(SearchPreferenceControl_Load);
		}

		private MemberSearchPreference _memberSearchPreference;
		private string _resourceConstant;
		private bool _collapsible;
		private bool _autoSelectHigherValues;

		public MemberSearchPreference MemberSearchPreference
		{
			get
			{
				return _memberSearchPreference;
			}
			set
			{
				_memberSearchPreference = value;
			}
		}

		public string ResourceConstant
		{
			get
			{
				return _resourceConstant;
			}
			set
			{
				_resourceConstant = value;
			}
		}

		public bool Collapsible
		{
			get
			{
				return _collapsible;
			}
			set
			{
				_collapsible = value;
			}
		}

		public bool AutoSelectHigherValues
		{
			get
			{
				return _autoSelectHigherValues;
			}
			set
			{
				_autoSelectHigherValues = value;
			}
		}

		private void SearchPreferenceControl_Load(object sender, EventArgs e)
		{
			try
			{
				if (_memberSearchPreference.Preference.Attribute.Name == "Height")
				{
					HeightRangeControl heightRangeControl = (HeightRangeControl) Page.LoadControl(@"Framework\Ui\FormElements\HeightRangeControl.ascx");
					heightRangeControl.MemberSearchPreferenceRange = _memberSearchPreference as MemberSearchPreferenceRange;
					heightRangeControl.ResourceConstant = _resourceConstant;
					Controls.Add(heightRangeControl);
				}
				else if (_memberSearchPreference.Preference.Attribute.Name == "MajorType")
				{
					DropDownControl dropDownControl = (DropDownControl) Page.LoadControl(@"Framework\Ui\FormElements\DropDownControl.ascx");
					dropDownControl.MemberSearchPreferenceInt = _memberSearchPreference as MemberSearchPreferenceInt;
					dropDownControl.ResourceConstant = _resourceConstant;
					Controls.Add(dropDownControl);
				}
				else
				{
                    
                    MaskControl20 maskControl;
                    maskControl = (MaskControl20)Page.LoadControl(@"Framework\Ui\FormElements\MaskControl20.ascx");

                    maskControl.MemberSearchPreferenceMask = _memberSearchPreference as MemberSearchPreferenceMask;
                    maskControl.ResourceConstant = _resourceConstant;
                    maskControl.Collapsible = _collapsible;
                    maskControl.AutoSelectHigherValues = _autoSelectHigherValues;
                    Controls.Add(maskControl);
				}
			}
			catch (Exception ex)
			{
				g.ProcessException(ex);
			}
		}
	}
}
