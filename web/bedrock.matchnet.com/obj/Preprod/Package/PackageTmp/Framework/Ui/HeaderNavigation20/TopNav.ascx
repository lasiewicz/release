﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TopNav.ascx.cs" Inherits="Matchnet.Web.Framework.Ui.HeaderNavigation20.TopNav" %>


			<div id="nav">
					<ul class="clearfix">
						<li id="nav-tab-home"><a href="http://redesign.jdate.com/default.asp" id="home">Home</a></li>
						<li id="nav-tab-your-profile"><a class="selected" href="http://redesign.jdate.com/profile.asp" id="your-profile">Your Profile</a></li>
						<li id="nav-tab-search"><a href="http://redesign.jdate.com/search.asp" id="search">Search</a></li>
						<li id="nav-tab-inbox"><a href="http://redesign.jdate.com/inbox.asp" id="inbox">Inbox</a></li>
						<li id="nav-tab-connect"><a href="http://redesign.jdate.com/connect.asp" id="connect">Connect</a></li>
						<li id="nav-tab-hot-lists"><a href="http://redesign.jdate.com/hotlists.asp" id="hot-lists">Hot Lists</a></li>
						<li id="nav-tab-subscribe"><a href="http://redesign.jdate.com/subscribe.asp" id="subscribe">Subscribe</a></li>
					</ul>
				</div>
         
				<div id="nav-sub">
					<ul style="display: block;" id="nav-sub-your-profile" class="clearfix">
						<li><a class="selected" href="http://redesign.jdate.com/profile.asp">Edit Your Profile</a></li>
						<li><a href="#">Edit Your Photos</a></li>
						<li><a href="http://redesign.jdate.com/profile_settings.asp">Settings</a></li>
					</ul>
					<ul style="display: none;" id="nav-sub-search" class="clearfix">
						<li><a href="http://redesign.jdate.com/search_quickSearch.asp">Quick Search</a></li>
						<li><a href="http://redesign.jdate.com/search.asp">Your Matches</a></li>
						<li><a href="http://redesign.jdate.com/search_photoGallery.asp">Photo Gallery</a></li>
						<li><a href="#">Look Up Member</a></li>
						<li><a href="#">Preferences</a></li>
					</ul>
					<ul style="display: none;" id="nav-sub-inbox" class="clearfix">
						<li><a href="#">Inbox</a></li>
						<li><a href="#">Sent Messages</a></li>
						<li><a href="#">Message Settings</a></li>
						<li><a href="#">Customized Folders</a></li>
					</ul>
					<ul style="display: none;" id="nav-sub-connect" class="clearfix">
						<li><a href="#">Chat Rooms</a></li>
						<li><a href="http://redesign.jdate.com/connect_membersOnline.asp">Members Online</a></li>			
						<li><a href="#">Message Boards</a></li>
						<li><a href="#">E-cards</a></li>
						<li><a href="#">Download Toolbar</a></li>
					</ul>
					<ul style="display: none;" id="nav-sub-hot-lists" class="clearfix">
						<li><a href="#">Member Who Have</a></li>
						<li><a href="#">Members You Have</a></li>
						<li><a href="#">Click YNM</a></li>
						<li><a href="#">Your Folders</a></li>
					</ul>
				</div>