﻿<%@ Register TagPrefix="mn" Namespace="Matchnet.Web.Framework" Assembly="Matchnet.Web" %>
<%@ Control Language="c#" AutoEventWireup="false" Codebehind="SpotlightInfoDisplay.ascx.cs" Inherits="Matchnet.Web.Framework.Ui.BasicElements.SpotlightInfoDisplay" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>

<asp:HyperLink CssClass="whiteText" ID="lnkSpotlightInfo" Visible="False" Runat="server"></asp:HyperLink>	
<asp:Panel ID="pnlSpotlightInfoForMembersWithSubscription" Runat="server" CssClass="highlightProfileInfo">
	<div class="highlightProfileInfoClose">
		<asp:HyperLink ID="lnkSpotlightInfoForMembersWithSubscriptionCloseText" Runat="server"></asp:HyperLink>	
		<asp:HyperLink ID="lnkSpotlightInfoForMembersWithSubscriptionCloseImage" Runat="server"></asp:HyperLink>							
	</div>
	
	<mn:Txt id="txtSpotlightInfoForMembersWithSubscription" runat="server" ResourceConstant="TXT_SPOTLIGHTINFO_SUBSCRIBER" expandImageTokens="True" />
</asp:Panel>
<asp:Panel ID="pnlSpotlightInfoForMembersWithoutSubscription" Runat="server" CssClass="highlightProfileInfo">
	<div class="highlightProfileInfoClose">
		<asp:HyperLink ID="lnkSpotlightInfoForMembersWithoutSubscriptionCloseText" Runat="server"></asp:HyperLink>	
		<asp:HyperLink ID="lnkSpotlightInfoForMembersWithoutSubscriptionCloseImage" Runat="server"></asp:HyperLink>							
	</div>

	<mn:Txt id="txtSpotlightInfoForMembersWithoutSubscription" runat="server" ResourceConstant="TXT_SPOTLIGHTINFO_REGISTERED" expandImageTokens="True" />
	<asp:HyperLink ID="lnkSpotlightInfoSubscribe" Runat="server"><mn:Txt id="imgSpotlightInfoForMembersWithoutSubscription" runat="server" ResourceConstant="SPOTLIGHTINFO_TO_SUBSCRIPTION" expandImageTokens="True" /></asp:HyperLink>		
</asp:Panel>