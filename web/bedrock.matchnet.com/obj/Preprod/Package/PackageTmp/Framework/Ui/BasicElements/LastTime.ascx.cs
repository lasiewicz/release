﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Matchnet.Web.Framework.Ui.BasicElements
{
    /// <summary>
    /// Control that renders calculates a date against the current date, and renders a time representation.
    /// Example: 24 minutes ago; 5 hours ago; or 10 days ago
    /// </summary>
    public partial class LastTime : FrameworkControl
    {
        public Literal LiteralTimeControl { get { return literalTime; } }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        #region Methods
        /// <summary>
        /// Calculates and displays the last time with respect to current date/time (e.g. 24 minutes ago; 5 hours ago; 10 days ago)
        /// </summary>
        /// <param name="lastDate"></param>
        public void LoadLastTime(DateTime lastDate)
        {
            if (lastDate != DateTime.MinValue)
            {
                DateTime now = DateTime.Now;
                TimeSpan timeSpan = now.Subtract(lastDate);

                this.literalTime.Text = CalculateTimeRepresentation(timeSpan);
            }
        }

        private string CalculateTimeRepresentation(TimeSpan timeSpan)
        {
            String ago = g.GetResource("PRO_AGO", this);
            String and = g.GetResource("TXT_AND", this);
            String minute = g.GetResource("PRO_MINUTE", this);
            String minutes = g.GetResource("PRO_MINUTES", this);
            String hour = g.GetResource("PRO_HOUR", this);
            String hours = g.GetResource("PRO_HOURS", this);
            String day = g.GetResource("PRO_DAY", this);
            String days = g.GetResource("PRO_DAYS", this);
            String before = g.GetResource("PRO_BEFORE", this);

            if (timeSpan.Days >= 60)
            {
                if (g.Brand.Site.Direction.ToString() == "rtl")
                {
                    return String.Format("{0} {1}", before, g.GetResource("PRO_MORE_THAN_60_DAYS_AGO", this));
                }
                else
                {
                    return g.GetResource("PRO_MORE_THAN_60_DAYS_AGO", this);
                }                
            }
            else if (timeSpan.Days >= 1)
            {
                String outday = (timeSpan.Days == 1) ? day : days;

                if (g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.JDateFR)
                {
                    return String.Format("{0} {1} {2}", ago, timeSpan.Days, outday);
                }
                else if (g.Brand.Site.Direction.ToString() == "rtl")
                {
                    return String.Format("{0} {1} {2} {3}", before, timeSpan.Days, outday, ago);
                } 
                else
                {
                    return String.Format("{0} {1} {2}", timeSpan.Days, outday, ago);
                }
            }
            else if (timeSpan.Hours >= 1)
            {
                String outHour = (timeSpan.Hours == 1) ? hour : hours;

                if (g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.JDateFR)
                {
                    return String.Format("{0} {1} {2}", ago, timeSpan.Hours, outHour);
                }
                else if (g.Brand.Site.Direction.ToString() == "rtl")
                {
                    return String.Format("{0} {1} {2} {3}", before, timeSpan.Hours, outHour, ago);
                } 
                else
                {
                    return String.Format("{0} {1} {2}", timeSpan.Hours, outHour, ago);
                }
            }
            else
            {
                int totalMinutes = (timeSpan.Minutes > 1) ? timeSpan.Minutes : 1;
                String outminute = (totalMinutes == 1) ? minute : minutes;

                if (g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.JDateFR)
                {
                    return String.Format("{0} {1} {2}", ago, totalMinutes, outminute);
                }
                else if (g.Brand.Site.Direction.ToString() == "rtl")
                {
                    return String.Format("{0} {1} {2} {3}", before, totalMinutes, outminute, ago);
                } 
                else
                {
                    return String.Format("{0} {1} {2}", totalMinutes, outminute, ago);
                }
            }
        
        }

        #endregion
    }
}