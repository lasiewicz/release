﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Configuration.ValueObjects;
using Matchnet.PremiumServiceSearch11.ServiceAdapters;
using Matchnet.Web.Applications.CompatibilityMeter;
using Matchnet.Web.Applications.Email;
using Matchnet.Web.Framework.Managers;
using Matchnet.Web.Framework.Ui.Advertising;
using Matchnet.Web.Framework.Ui.HeaderNavigation20;
using Matchnet.Web.Framework.Util;
using Matchnet.Web.Interfaces;
using Spark.Common.AccessService;
using ServiceConstants = Matchnet.PremiumServices.ValueObjects.ServiceConstants;

namespace Matchnet.Web.Framework.Menus
{
    public class MenuUtils
    {
        private static SettingsManager _settingsManager = new SettingsManager();
        private static ILoggingManager _loggingManager = null;

        public static ILoggingManager LoggingManager
        {
            get
            {
                if (_loggingManager == null)
                {
                    _loggingManager = new LoggingManager();
                }
                return _loggingManager;
            }
        }

        public static Menu GetMenuItems(ContextGlobal g, string XMLPath, string cacheKey)
        {
            Matchnet.Web.Framework.Menus.Menu menu = (Matchnet.Web.Framework.Menus.Menu)g.Cache.Get(cacheKey);
            if (menu == null)
            {
                string xmlFile = System.Web.HttpContext.Current.Server.MapPath(XMLPath);
                XMLObjectSerializer objectSerializer = new XMLObjectSerializer();
                menu = objectSerializer.FromXmlFile<Matchnet.Web.Framework.Menus.Menu>(xmlFile);

                //System.Web.Caching.CacheDependency cacheFileDependencies = new System.Web.Caching.CacheDependency(new string[] { xmlFile });
                g.Cache.Insert(cacheKey, menu);
            }
            return menu;

        }


        public static bool IsMenuSelected(MenuItem i, ContextGlobal g)
        {
            try
            {

                if (i.SelectConditions == null || i.SelectConditions.Count == 0)
                    return false;

                string currentcontrol = "";
                string currentapp = "";
                if (g.AppPage != null && g.AppPage.ControlName != null)
                {
                    currentcontrol = g.AppPage.ControlName.ToLower();
                    currentapp = g.AppPage.App.Path.ToLower().Replace("/applications", "").Replace("/", "");
                }
                SelectCondition selectRequestParam = i.SelectConditions.Find(delegate(SelectCondition c) { return (!String.IsNullOrEmpty(c.App) && c.App == currentapp && !String.IsNullOrEmpty(c.ControlName) && c.ControlName == currentcontrol && !String.IsNullOrEmpty(c.RequestParameter)); });

                if (selectRequestParam != null)
                {
                    return HandleReqParameterSelection(selectRequestParam);
                }

                SelectCondition selectFunction = i.SelectConditions.Find(delegate(SelectCondition c) { return (!String.IsNullOrEmpty(c.SelectFunction)); });
                if (selectFunction != null)
                {
                    bool select = HandleCustomMenuSelection(selectFunction.SelectFunction, g);
                    if (select) return true;
                }

                //check if App condition
                SelectCondition selectCondition = i.SelectConditions.Find(delegate(SelectCondition c) { return (!String.IsNullOrEmpty(c.App) && c.App.ToLower() == currentapp && String.IsNullOrEmpty(c.ControlName)); });
                if (selectCondition != null)
                { return true; }

                //check if ControlName condition
                SelectCondition controlapp = i.SelectConditions.Find(delegate(SelectCondition c) { return (!String.IsNullOrEmpty(c.ControlName) && c.ControlName.ToLower() == currentcontrol); });
                if (controlapp != null)
                { return true; }


                return false;


            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
                return false;
            }
        }
        public static string HandleCustomLink(Matchnet.Web.Framework.Menus.MenuItem item, ContextGlobal g)
        {
            try
            {
                if (String.IsNullOrEmpty(item.LinkFunction))
                    return "";

                string link = "";

                switch (item.LinkFunction)
                {
                    case ("GetCustomFolderLink"):
                        link = getCustomFolderLink(g);
                        break;
                    case ("GetCustomListLink"):
                        link = getCustomListLink(g);
                        break;
                    case ("GetChatLink"):
                        link = getChatLink(g);
                        break;
                    case ("GetBoardLink"):
                        link = getBoardLink(g);
                        break;
                    case ("GetEcardLink"):
                        link = getEcardLink(g);
                        break;
                    case ("GetAccountLink"):
                        link = getAccountLink(g);
                        break;
                    case ("GetAccountLinkNoPremium"):
                        link = getAccountLinkNoPremium(g);
                        break;
                    case ("GetFriendInviteLink"):
                        link = getFriendInviteLink(g);
                        break;
                    case ("getQnAPageLink"):
                        link = getQnAPageLink(g);
                        break;
                    case ("getEventsLink"):
                        link = getEventsLink(g);
                        break;

                    default:
                        break;
                }

                return link;
            }
            catch (Exception ex)
            {
                throw (ex);
            }

        }


        public static bool HandleCustomMenuSelection(string selectionFunction, ContextGlobal g)
        {
            try
            {
                if (String.IsNullOrEmpty(selectionFunction))
                    return false;
                if (g == null || g.AppPage == null)
                    return false;

                switch (selectionFunction)
                {
                    case ("SelectCustomFolder"):
                        return IsCustomMailBoxSelected(g);
                    case ("SelectFolder"):
                        return IsMailBoxFolderSelected(g);
                    case ("SelectYourProfile"):
                        return IsYourProfileSelected(g);

                    default:
                        break;
                }

                return false;
            }
            catch (Exception ex)
            {
                throw (ex);
            }

        }


        public static bool HandleReqParameterSelection(SelectCondition condition)
        {
            try
            {
                string arg = HttpContext.Current.Request[condition.RequestParameter].ToString();
                List<string> args = condition.RequestParameterValue.Split(',').ToList<string>();

                string val = args.Find(delegate(string s) { return s == arg; });

                if (!String.IsNullOrEmpty(val))
                    return true;


                return false;


            }
            catch (Exception ex)
            {

                return false;
            }

        }


        public static bool IsSettingConfiguredVisible(string setting, ContextGlobal g)
        {
            try
            {
                bool enabled = Convert.ToBoolean(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting(setting, g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID));
                if (!enabled)
                    return false;

                return true;
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
                return false;
            }

        }


        public static bool IsConditionallyVisible(string condition, ContextGlobal g)
        {
            try
            {
                switch (condition)
                {
                    case ("ShowYourProfile"):
                        if (g.Member == null)
                            return false;
                        else
                            return true;
                        break;
                    case ("ShowAccount"):
                        return ShowAccount(g);
                    case ("ShowPremiumServices"):
                        return ShowPremiumServicesSettings(g);
                    case ("ShowBillingInfo"):
                        return ShowBillingInfo(g);
                    case ("ShowUpgrade"):
                        return ShowUpgrade(g);
                    case ("ShowSubscribe"):
                        return !MemberPrivilegeAttr.IsCureentSubscribedMember(g);
                    case ("ShowIfHadSub"):
                        return FrameworkGlobals.IfHadSubscription(g);
                    case ("ShowIfHasSub"):
                        {
                            if (Convert.ToBoolean((RuntimeSettings.GetSetting("IS_UPS_INTEGRATION_ENABLED", g.Brand.Site.Community.CommunityID,
                                g.Brand.Site.SiteID, g.Brand.BrandID))))
                                return FrameworkGlobals.IfMemberHasSubscribed(g);
                            else
                                return false;
                        }

                    case ("ShowAdmin"):
                        {
                            if (g.IsAdmin && g.Member != null && Convert.ToBoolean(RuntimeSettings.GetSetting("ENABLE_WEB_ADMIN"))
                                      && (HttpContext.Current.Request.Url.Host.ToLower().StartsWith("admin") || g.IsDevMode))
                            {
                                return true;
                            }

                            break;

                        }
                    case ("HasSubWithoutAA"):
                        {
                            return HasSubWithoutAA(g);
                        }
                        break;
                    case ("ShowReverseSearch"):
                        return ShowReverseSearch(g);
                    case "ShowLinkToJMeterWelcome":
                        {
                            var cmh = new CompatibilityMeterHandler(g);
                            return !cmh.IsMemeberHasFeedback();
                            break;
                        }
                    case "ShowLinkToJMeterCompletion":
                        {
                            var cmh = new CompatibilityMeterHandler(g);
                            return cmh.IsMemeberHasFeedback();
                            break;
                        }
                    case "ShowJMeterMenu":
                        {
                            return CompatibilityMeterHandler.IsJMeterV3(g.Brand);
                            break;
                        }
                    case "ShowJExperts":
                        {
                            return
                                Convert.ToBoolean(RuntimeSettings.GetSetting("ENABLE_JEXPERTS",
                                                                             g.Brand.Site.Community.CommunityID,
                                                                             g.Brand.Site.SiteID, g.Brand.BrandID));
                            break;
                        }
                    case "ShowCommunities":
                        {
                            return
                                !Convert.ToBoolean(RuntimeSettings.GetSetting("ENABLE_JEXPERTS",
                                                                             g.Brand.Site.Community.CommunityID,
                                                                             g.Brand.Site.SiteID, g.Brand.BrandID));
                            break;
                        }
                    case "ShowJMeterMatches":
                        {
                            CompatibilityMeterHandler cmh = new CompatibilityMeterHandler(g);
                            return cmh.IsJMeterMatchesEnabled();
                            break;
                        }
                    case "ShowLivePerson":
                        {
                            return LivePersonManager.Instance.IsLivePersonEnabled(g.Member, g.Brand);
                        }
                    case "ShowAutoRenewal":
                        {
                            return ShowAutoRenewal(g);
                        }

                    case "PremiumServicesLeftToPurchase":
                        {
                            return PremiumServicesLeftToPurchase(g);
                        }
                       
                }

                return false;
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
                return false;
            }

        }

        private static bool PremiumServicesLeftToPurchase(ContextGlobal g)
        {
            bool memberHasPremiuimServiceToPurchase = false;

            if (g.Member != null)
            {
                bool hasHighlightedProfile = false;
                bool hadSpotlightMember = false;
                bool hasJMeter = false;
                bool hasReadReceipts = false;
                bool hasAllAccess = false;

                var apHighlightedProfile = g.Member.GetUnifiedAccessPrivilege(PrivilegeType.HighlightedProfile, g.Brand.BrandID, g.Brand.Site.SiteID, g.Brand.Site.Community.CommunityID);
                if (apHighlightedProfile != null && apHighlightedProfile.EndDatePST > DateTime.Now)
                {
                    hasHighlightedProfile = true;
                }

                var apSpotlightMember = g.Member.GetUnifiedAccessPrivilege(PrivilegeType.SpotlightMember, g.Brand.BrandID, g.Brand.Site.SiteID, g.Brand.Site.Community.CommunityID);
                if (apSpotlightMember != null && apSpotlightMember.EndDatePST > DateTime.Now)
                {
                    hadSpotlightMember = true;
                }

                if (CompatibilityMeterHandler.IsJMeterEnabledAsPremium(g))
                {
                    var apJMeter = g.Member.GetUnifiedAccessPrivilege(PrivilegeType.JMeter, g.Brand.BrandID, g.Brand.Site.SiteID, g.Brand.Site.Community.CommunityID);
                    if (apJMeter != null && apJMeter.EndDatePST > DateTime.Now)
                    {
                        hasJMeter = true;
                    }
                }
                else
                {
                    hasJMeter = true; //free or not enabled
                }

                if (MessageManager.Instance.IsReadRecieptPremiumFeatureEnabled(g.Brand))
                {
                    hasReadReceipts = MessageManager.Instance.DoesMemberHaveReadRecieptPrivilege(g.Member, g.Brand);
                }
                else
                {
                    hasReadReceipts = true; //free
                }

                if (VIPMailUtils.IsVIPSendEnabledSite(g.Brand))
                {
                    hasAllAccess = VIPMailUtils.IsVIPMember(g, g.Member.MemberID);
                }
                else
                {
                    hasAllAccess = true; //not enabled
                }

                if (!hasHighlightedProfile || !hadSpotlightMember || !hasJMeter || !hasAllAccess || !hasReadReceipts)
                {
                    memberHasPremiuimServiceToPurchase = true;
                }
            }

            return memberHasPremiuimServiceToPurchase;
        }

        private static bool ShowAutoRenewal(ContextGlobal g)
        {
            if (!Convert.ToBoolean(RuntimeSettings.GetSetting("ENABLE_TOPAUXNAV_REACTIVATE_AR_LINK",
                                                              g.Brand.Site.Community.CommunityID,
                                                              g.Brand.Site.SiteID, g.Brand.BrandID)))
            {
                return false;
            }


            if (g.Member != null)
            {
                // paying member that has auto renewal turned off should see this link
                return g.Member.GetAttributeInt(g.Brand, "AutoRenewalStatus", 1) == 0 &&
                       g.Member.IsPayingMember(g.Brand.Site.SiteID);
            }

            return false;
        }


        public static string getCustomFolderLink(ContextGlobal g)
        {
            string link = HeaderLinks.CustomFolders;
            int folderid = 0;
            try
            {
                //4 system folders
                if (g.Member == null || g.MemberEmailFolders.Count == 4)
                {
                    return link;
                }

                foreach (Email.ValueObjects.EmailFolder folder in g.MemberEmailFolders)
                {
                    if (!folder.SystemFolder)
                    {
                        folderid = folder.MemberFolderID;
                        break;
                    }

                }
                link = HeaderLinks.Mailbox + "?MemberFolderID=" + folderid.ToString();
                return link;
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
                return link;
            }

        }


        public static string getCustomListLink(ContextGlobal g)
        {
            string link = HeaderLinks.CustomLists;
            try
            {
                if (g.Member != null)
                {

                    List.ValueObjects.CustomCategoryCollection customLists = List.ServiceAdapters.ListSA.Instance.GetCustomListCategories(g.Session.GetInt(WebConstants.SESSION_PROPERTY_NAME_MEMBERID), g.Brand.Site.Community.CommunityID);
                    if (customLists != null && customLists.Count > 0)
                    {
                        link = HeaderLinks.HotListView + "?CategoryID=" + customLists[0].CategoryID;
                    }
                }

                return link;
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
                return link;
            }

        }


        public static string getChatLink(ContextGlobal g)
        {
            string link = HeaderLinks.Chat;
            try
            {
                if (Convert.ToBoolean(RuntimeSettings.GetSetting("CONNECT_CHAT_ENABLE", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID)))
                    link = FrameworkGlobals.BuildConnectFrameworkLink("chat", g, true);
                else
                {
                    if (g.IsAPI)
                    {
                        string apiimagehost = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("API_CALL_IMAGE_HOST", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID);
                        link = "http://" + apiimagehost + "." + g.Brand.Uri + link;
                    }
                }
                return link;
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
                return link;
            }

        }

        public static string getFriendInviteLink(ContextGlobal g)
        {
            string link = "";
            try
            {
                link = FrameworkGlobals.BuildConnectFrameworkLink("invite", g, true);

                return link;
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
                return link;
            }

        }

        public static string getBoardLink(ContextGlobal g)
        {
            string link = HeaderLinks.Chat;
            try
            {
                link = FrameworkGlobals.BuildConnectFrameworkLink("boards", g, true);
                return link;
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
                return link;
            }

        }

        public static string getEcardLink(ContextGlobal g)
        {

            try
            {
                string link = FrameworkGlobals.BuildConnectFrameworkLink("cards/categories.html?", g, true);
                return link;
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
                return "";
            }

        }

        public static string getAccountLink(ContextGlobal g)
        {
            string link = HeaderLinks.Subscribe + "?prtid=425&NavPoint=top";
            if (MemberPrivilegeAttr.IsCureentSubscribedMember(g))
            {
                bool premiumServices = FrameworkGlobals.IfHasPremiumService(g);
                if (premiumServices || g.Brand.Site.SiteID == (int)WebConstants.SITE_ID.Cupid)
                {
                    link = HeaderLinks.SubscriptionHistory + "?NavPoint=top";
                }
            }

            return link;


        }

        public static string getAccountLinkNoPremium(ContextGlobal g)
        {
            string link = HeaderLinks.Subscribe + "?prtid=425&NavPoint=top";
            if (MemberPrivilegeAttr.IsCureentSubscribedMember(g))
            {
                link = HeaderLinks.SubscriptionHistory + "?NavPoint=top";
            }

            return link;
        }



        public static bool IsCustomMailBoxSelected(ContextGlobal g)
        {
            if (g.AppPage.ID != (int)WebConstants.PageIDs.MailBox)
                return false;

            int folderid = Conversion.CInt(HttpContext.Current.Request["MemberFolderID"], Constants.NULL_INT);
            if (folderid > 4)
                return true;

            return false;
        }

        public static bool IsMailBoxFolderSelected(ContextGlobal g)
        {
            if (g.AppPage.ID != (int)WebConstants.PageIDs.MailBox)
                return false;

            int folderid = Conversion.CInt(HttpContext.Current.Request["MemberFolderID"], Constants.NULL_INT);
            if (folderid <= 4)
                return true;

            return false;
        }

        public static string getQnAPageLink(ContextGlobal g)
        {
            string link = "";
            try
            {
                if (_settingsManager.GetSettingBool(SettingConstants.ENABLE_QNA_HOME_PAGE, g.Brand))
                {
                    link = "/Applications/QuestionAnswer/Home.aspx?NavPoint=sub&tabid=1";
                }
                else
                {
                    link = "/Applications/QuestionAnswer/Question.aspx?NavPoint=sub";
                }

                return link;
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
                return link;
            }  

        }

        public static string getEventsLink(ContextGlobal g)
        {
            return "javascript:showTravelEventsWindow();";
        }

        public static bool IsYourProfileSelected(ContextGlobal g)
        {
            bool selected = false;
            if (g.AppPage.ID == (int)WebConstants.PageIDs.ViewProfile)
            {
                //don't select any top menu if members are viewing not their profile
                if (HttpContext.Current.Request["MemberID"] != null && g.Member != null)
                {
                    string profilememberid = HttpContext.Current.Request["MemberID"];
                    if (!String.IsNullOrEmpty(profilememberid))
                    {
                        int memberid = Conversion.CInt(profilememberid);
                        if (memberid != g.Member.MemberID)
                            selected = false;
                    }
                }
                else
                    selected = true;
            }
            else if (g.AppPage.App.ID == (int)WebConstants.APP.MemberProfile)
                selected = true;
            else if (g.AppPage.App.ID == (int)WebConstants.APP.MemberServices && g.AppPage.ID == (int)WebConstants.PageIDs.DisplaySettings)
                selected = true;

            return selected;
        }


        public static bool ShowAccount(ContextGlobal g)
        {
            bool visible = false;
            if (g.Member == null)
                return visible;

             if (SubscriberManager.Instance.IsIAPSubscriber(g.Member, g.Brand)) // check to see if member bought via in app purchase.
                visible = true;

            if (!g.Brand.IsPaySite)
                return visible;

            if (MemberPrivilegeAttr.IsCureentSubscribedMember(g))
                visible = true;

            return visible;
        }
         

        public static bool ShowReverseSearch(ContextGlobal g)
        {
            return Conversion.CBool(RuntimeSettings.GetSetting("ENABLE_REVERSE_SEARCH", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID));
        }


        public static bool ShowPremiumServicesSettings(ContextGlobal g)
        {
            if (g.Member == null)
                return false;

            if (!g.Brand.IsPaySite)
                return false;

            if (SubscriberManager.Instance.IsIAPSubscriber(g.Member, g.Brand)) // check to see if member bought via in app purchase. 
                return false;

            //only highlight/spotlight has settings
            ArrayList arrActivePremiumServices = PremiumServiceSA.Instance.GetMemberActivePremiumServices(g.Member.MemberID, g.Brand);
            if (arrActivePremiumServices.Contains(Spark.Common.AccessService.PrivilegeType.HighlightedProfile.ToString()) || arrActivePremiumServices.Contains(Spark.Common.AccessService.PrivilegeType.SpotlightMember.ToString()))
            {
                return true;
            }
            return false;
        }

        public static bool ShowUpgrade(ContextGlobal g)
        {
            bool visible = false;
            if (g.Member == null)
                return visible;

            if (!g.Brand.IsPaySite)
                return visible; 

            if (SubscriberManager.Instance.IsIAPSubscriber(g.Member, g.Brand)) // check to see if member bought via in app purchase.
                return visible;

            return PremiumServicesLeftToPurchase(g);
        }

        public static bool HasSubWithoutAA(ContextGlobal g)
        {
            bool result = false;

            if (g.Member == null)
                return result;

            if (!g.Brand.IsPaySite)
                return result;

            bool premiumServices = FrameworkGlobals.IfHasPremiumService(g);

            if (premiumServices)
            {
                result = !PremiumServiceSA.Instance.MemberHasAllAccessEmailsPremiumService(g.Member, g.Brand);
            }
            else
            {
                result = true;
            }

            return result;
        }


        public static bool ShowBillingInfo(ContextGlobal g)
        {
            bool visible = false;
            if (Convert.ToBoolean((RuntimeSettings.GetSetting("IS_UPS_INTEGRATION_ENABLED", g.Brand.Site.Community.CommunityID,
               g.Brand.Site.SiteID, g.Brand.BrandID))))
            {
                visible = FrameworkGlobals.IfMemberHasSubscribed(g);

            }
            return visible;
        }

        /// <summary>
        /// Determines whether navigation changes (including related activity center) for social navigation optimization is enabled for site
        /// </summary>
        /// <param name="g"></param>
        /// <returns></returns>
        public static bool IsSocialNavEnabled(ContextGlobal g)
        {
            bool enabled = false;
            try
            {
                enabled = Convert.ToBoolean((RuntimeSettings.GetSetting("ENABLED_SOCIAL_NAV", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID)));
            }
            catch (Exception ex)
            {
                enabled = false;
                g.ProcessException(ex);
            }

            return enabled;
        }

        /// <summary>
        /// Determines whether top leaderboard ad is enabled for social nav changes
        /// </summary>
        /// <param name="g"></param>
        /// <returns></returns>
        public static bool IsTopLeaderboardAdEnabled(ContextGlobal g)
        {
            bool enabled = false;
            try
            {
                enabled = Convert.ToBoolean((RuntimeSettings.GetSetting("ENABLED_TOP_LEADERBOARD_AD", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID)));
            }
            catch (Exception ex)
            {
                enabled = false;
                g.ProcessException(ex);
            }

            return enabled;
        }

        /// <summary>
        /// Determines whether the top announcement message is to be displayed
        /// </summary>
        /// <param name="g"></param>
        /// <returns></returns>
        public static bool IsAnnouncementMessageEnabled(ContextGlobal g)
        {
            bool enabled = false;
            try
            {
                enabled = (Convert.ToBoolean((RuntimeSettings.GetSetting("ENABLED_TOP_ANNOUNCEMENT_MESSAGE", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID)))
                    && !Convert.ToBoolean(g.Session.GetString(WebConstants.SESSION_PROPERTY_NAME_HIDE_ANNOUNCEMENT_MESSAGE, "false")));
            }
            catch (Exception ex)
            {
                enabled = false;
                g.ProcessException(ex);
            }

            return enabled;
        }

    }
}
