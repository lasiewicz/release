﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace Matchnet.Web.Framework
{
    public static class ExtensionMethods
    {
        public static MemoryStream CopyStream(this Stream input)
        {
            MemoryStream ms = new MemoryStream();
            byte[] b = new byte[32768];
            int r;
            while ((r = input.Read(b, 0, b.Length)) > 0)
            {
                ms.Write(b, 0, r);
            }

            return ms;
        }
    }
}