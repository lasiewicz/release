using System;
using System.Web;
using Matchnet.Web.Framework;
using Matchnet.Session.ValueObjects;
using Matchnet.Member.ValueObjects;
using Matchnet.Member.ServiceAdapters;


namespace Matchnet.Web.Framework
{
	/// <summary>
	/// Summary description for RegistrationReason.
	/// </summary>
	public class RegistrationReason : FrameworkControl
	{
		/// <summary>
		/// An enum representing which keys should be added for AddToSession
		/// </summary>
		public enum KeysToAdd
		{
			Both,
			Target,
			Referrer
		}

		/// <summary>
		/// The two types of keys relevant to a registration reason
		/// </summary>
		public enum KeyName
		{
			RegistrationTargetPage,
			RegistrationReferrerPage
		}

		/// <summary>
		/// Constuctor currently not in use
		/// </summary>
		public RegistrationReason()
		{
			
		}

		public static void AddDirectLinkToSession(ContextGlobal g)
		{
			try
			{
				if(g.Session[KeyName.RegistrationReferrerPage.ToString()] == String.Empty)
				{
					SetKey(g, KeyName.RegistrationReferrerPage);
				}
				
				if(g.Session[KeyName.RegistrationTargetPage.ToString()] == String.Empty)
				{
					SetKey(g, KeyName.RegistrationTargetPage);
				}
			}
			catch(Exception ex)
			{
				g.ProcessException(ex);
			}
		}

		/// <summary>
		/// Statis method to add registration reason keys to the session
		/// </summary>
		/// <param name="g">The global context necessary because of the static type</param>
		/// <param name="keys">An enum representing which keys should be added to the session</param>
		public static void AddToSession(ContextGlobal g, KeysToAdd keys)
		{
			try
			{
				switch(keys)
				{
					case (KeysToAdd.Target):
						SetKey(g, KeyName.RegistrationTargetPage);
						break;

					case (KeysToAdd.Referrer):
						SetKey(g, KeyName.RegistrationReferrerPage);
						break;

					case (KeysToAdd.Both):
					default:
						SetKey(g, KeyName.RegistrationTargetPage);
						SetKey(g, KeyName.RegistrationReferrerPage);
						break;
				}
			}
			catch(Exception ex)
			{
				g.ProcessException(ex);
			}
		}

		/// <summary>
		/// Private helper method to set the key/value for a registration reason
		/// </summary>
		/// <param name="g">The global context necessary because of the static type</param>
		/// <param name="keyName">The type of key to be added</param>
		private static void SetKey(ContextGlobal g, KeyName keyName)
		{
			string keyValue = String.Empty;

			if(keyName == KeyName.RegistrationReferrerPage)
			{
				keyValue = g.GetReferrerUrl();
			}
			else
			{
				keyValue = g.GetDestinationURL();

				if(keyValue == String.Empty)
				{
					keyValue = HttpContext.Current.Items["PagePath"].ToString();
				
					if(HttpContext.Current.Request.QueryString.Count > 0) 
					{
						keyValue += "?" + HttpContext.Current.Request.QueryString;
					}
				}
			}

			g.Session.Add(keyName.ToString(), keyValue, SessionPropertyLifetime.Temporary);
		}

		/// <summary>
		/// Sets member attributes for each of the registration reason keys
		/// </summary>
		/// <param name="member">The member object for the user to be added keys to</param>
		/// <param name="g">The global context necessary because of the static type</param>
		public static void SetMemberAttributes(Member.ServiceAdapters.Member member, ContextGlobal g)
		{
			try
			{
				string RegistrationTargetPage = g.Session[KeyName.RegistrationTargetPage.ToString()].ToString();
				string RegistrationReferrerPage = g.Session[KeyName.RegistrationReferrerPage.ToString()].ToString();

				if(RegistrationTargetPage == null || RegistrationTargetPage == String.Empty)
				{
					RegistrationTargetPage = g.GetDestinationURL();
				}

				member.SetAttributeText(g.Brand, KeyName.RegistrationTargetPage.ToString(), RegistrationTargetPage, TextStatusType.Auto);
				member.SetAttributeText(g.Brand, KeyName.RegistrationReferrerPage.ToString(), RegistrationReferrerPage, TextStatusType.Auto);
			}
			catch(Exception ex)
			{
				g.ProcessException(ex);
			}
		}


	}
}
