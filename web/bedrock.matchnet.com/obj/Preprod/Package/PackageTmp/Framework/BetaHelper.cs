﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Configuration.ServiceAdapters.Analitics;

namespace Matchnet.Web.Framework
{
    /// <summary>
    /// Helper class related to supporting UI Beta tests
    /// </summary>
    public static class BetaHelper
    {
        [Flags]
        public enum BetaTestType : int
        {
            None = 0,
            ProfileRedesign30_DefaultOff = 1, //This test requires users to opt-in to see profile30
            ProfileRedesign30_DefaultOn = 2, //This test requires users to opt-out to not see profile30
            SearchRedesign30_DefaultOff = 4,
            SearchRedesign30_DefaultOn = 8
        }

        #region Public Methods
        public static void ToggleBetaRequest(ContextGlobal g)
        {
            try
            {
                BetaTestType betaRequested = BetaTestType.None;
                if (!String.IsNullOrEmpty(HttpContext.Current.Request[WebConstants.URL_PARAMETER_BETA_TEST_ENABLE]))
                {
                    betaRequested = (BetaTestType)Enum.Parse(typeof(BetaTestType), HttpContext.Current.Request[WebConstants.URL_PARAMETER_BETA_TEST_ENABLE]);
                    if (betaRequested != BetaTestType.None)
                    {
                        AddBetaTest(g, betaRequested);
                    }
                }
                else if (!String.IsNullOrEmpty(HttpContext.Current.Request[WebConstants.URL_PARAMETER_BETA_TEST_DISABLE]))
                {
                    betaRequested = (BetaTestType)Enum.Parse(typeof(BetaTestType), HttpContext.Current.Request[WebConstants.URL_PARAMETER_BETA_TEST_DISABLE]);
                    if (betaRequested != BetaTestType.None)
                    {
                        RemoveBetaTest(g, betaRequested);
                    }
                }

            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        /// <summary>
        /// Adds the specified beta test for a member
        /// </summary>
        public static void AddBetaTest(ContextGlobal g, BetaTestType betaTestType)
        {
            try
            {
                int currentMask = g.Member.GetAttributeInt(g.Brand, "BetaTestMask", 0);
                if (!((currentMask & (int)betaTestType) == (int)betaTestType))
                {
                    g.Member.SetAttributeInt(g.Brand, "BetaTestMask", currentMask + ((int)betaTestType));
                    MemberSA.Instance.SaveMember(g.Member);
                }
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        /// <summary>
        /// Removes the specified beta test for a member
        /// </summary>
        public static void RemoveBetaTest(ContextGlobal g, BetaTestType betaTestType)
        {
            try
            {
                int currentMask = g.Member.GetAttributeInt(g.Brand, "BetaTestMask", 0);
                if ((currentMask & (int)betaTestType) == (int)betaTestType)
                {
                    g.Member.SetAttributeInt(g.Brand, "BetaTestMask", currentMask - ((int)betaTestType));
                    MemberSA.Instance.SaveMember(g.Member);
                }
            }
            catch (Exception ex)
            {
                g.ProcessException(ex);
            }
        }

        /// <summary>
        /// Checks if member has a beta test 
        /// </summary>
        public static bool HasBetaTest(ContextGlobal g, BetaTestType betaTestType)
        {
            bool hasBeta = false;
            try
            {
                if (g.Member != null)
                {
                    int currentMask = g.Member.GetAttributeInt(g.Brand, "BetaTestMask", 0);
                    if ((currentMask & (int)betaTestType) == (int)betaTestType)
                    {
                        hasBeta = true;
                    }
                }
            }
            catch (Exception ex)
            {
                hasBeta = false;
                g.ProcessException(ex);
            }
            return hasBeta;
        }

        /// <summary>
        /// Gets the current Profile30 beta test, so we know what Beta test is on and we can show the right Beta Toggler and such
        /// </summary>
        /// <returns></returns>
        public static BetaTestType GetProfile30BetaTest(ContextGlobal g)
        {
            BetaTestType result = BetaTestType.None;

            try
            {
                //beta not available for visitors
                if (g.Member != null)
                {
                    //ENABLE_PROFILE30_MASK: 0 (off), 1 (on), 2 (beta-defaultOn), 4 (beta-defaultOff), 8 (a/b test)
                    int enableProfile30Mask = Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ENABLE_PROFILE30_MASK", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID));
                    if (enableProfile30Mask > 0)
                    {
                        //check "On" state
                        if ((enableProfile30Mask & 1) == 1)
                        {
                            //check beta (only one beta test can be enabled at a time)
                            if ((enableProfile30Mask & 2) == 2)
                            {
                                //default on - opt out approach
                                result = BetaTestType.ProfileRedesign30_DefaultOn;
                            }
                            else if ((enableProfile30Mask & 4) == 4)
                            {
                                //default off - opt in approach
                                result = BetaTestType.ProfileRedesign30_DefaultOff;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //missing setting
                g.ProcessException(ex);
            }

            return result;
        }

        #endregion

        #region Search Redesign 3.0 Related
        /// <summary>
        /// Gets the current Profile30 beta test, so we know what Beta test is on and we can show the right Beta Toggler and such
        /// </summary>
        /// <returns></returns>
        public static BetaTestType GetSearchRedesign30BetaTest(ContextGlobal g)
        {
            BetaTestType result = BetaTestType.None;

            try
            {
                //beta not available for visitors
                if (g.Member != null)
                {
                    //ENABLE_SEARCH_REDESIGN_MASK: 0 (off), 1 (on), 2 (beta-defaultOn), 4 (beta-defaultOff), 8 (a/b test)
                    int enableMask = Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ENABLE_SEARCH_REDESIGN_MASK", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID));
                    if (enableMask > 0)
                    {
                        //check "On" state
                        if ((enableMask & 1) == 1)
                        {
                            //check beta (only one beta test can be enabled at a time)
                            if ((enableMask & 2) == 2)
                            {
                                //default on - opt out approach
                                result = BetaTestType.SearchRedesign30_DefaultOn;
                            }
                            else if ((enableMask & 4) == 4)
                            {
                                //default off - opt in approach
                                result = BetaTestType.SearchRedesign30_DefaultOff;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //missing setting
                g.ProcessException(ex);
            }

            return result;
        }

        /// <summary>
        /// Indicates whether Search 3.0 redesigned page is enabled
        /// This takes into account beta testing and whether member has beta enabled
        /// </summary>
        /// <returns></returns>
        public static bool IsSearchRedesign30Enabled(ContextGlobal g)
        {
            bool result = false;

            try
            {
                //ENABLE_SEARCH_REDESIGN_MASK: 0 (off), 1 (on), 2 (beta-defaultOn), 4 (beta-defaultOff), 8 (a/b test)
                int enableMask = Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ENABLE_SEARCH_REDESIGN_MASK", g.Brand.Site.Community.CommunityID, g.Brand.Site.SiteID, g.Brand.BrandID));
                if (enableMask > 0)
                {
                    //check "On" state
                    if ((enableMask & 1) == 1)
                    {
                        result = true;

                        //check beta (only one beta test can be enabled at a time)
                        if ((enableMask & 2) == 2)
                        {
                            //default on - opt out approach
                            result = !BetaHelper.HasBetaTest(g, BetaHelper.BetaTestType.SearchRedesign30_DefaultOn);
                        }
                        else if ((enableMask & 4) == 4)
                        {
                            //default off - opt in approach
                            result = BetaHelper.HasBetaTest(g, BetaHelper.BetaTestType.SearchRedesign30_DefaultOff);
                        }

                        //check a/b (this will override any beta)
                        if ((enableMask & 8) == 8)
                        {
                            result = false;
                            if (g.Member != null)
                            {
                                Scenario search30Scenario = g.GetABScenario("SEARCH30");
                                if (search30Scenario == Scenario.B)
                                    result = true;
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                //missing setting
                g.ProcessException(ex);
                result = false;
            }

            return result;
        }

        /// <summary>
        /// Indicates whether Search 3.0 redesigned page will be displayed
        /// </summary>
        /// <returns></returns>
        public static bool IsDisplayingSearchRedesign30(ContextGlobal g)
        {
            bool result = false;

            try
            {
                if (g != null && g.Member != null && g.AppPage.ID == (int)WebConstants.PageIDs.SearchResults)
                {
                    result = IsSearchRedesign30Enabled(g);
                }
            }
            catch (Exception ex)
            {
                //missing setting
                g.ProcessException(ex);
                result = false;
            }

            return result;
        }


        #endregion
    }
}
