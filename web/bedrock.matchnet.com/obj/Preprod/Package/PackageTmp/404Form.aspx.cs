#region System References
using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
#endregion

#region Matchnet Web App References
using Matchnet.Web.Framework;
using Matchnet;
using Matchnet.Web.Framework.Diagnostics;
#endregion

#region Matchnet Service References
using Matchnet.Member.ServiceAdapters;
#endregion

namespace global.matchnet.com
{
	/// <summary>
	/// Handles 404 Redirection
	/// 
	/// 1. Retrieve the path being access
	/// 2. Parse for username
	///		a) Lookup username
	///		b) If found redirect to view profile
	/// 3. If all else fails, redirect to default.aspx?rc=WE_WERE_UNABLE_TO_FIND_THE_PAGE_YOU_ARE_LOOKING_FOR_X handling 404 error
	/// </summary>
	public class _404Form : System.Web.UI.Page 
	{
		// Legacy Actions IDs
		const int ACTION_MEMBER_SERVICES_VERIFY_EMAIL = 18540;
		const int ACTION_SEARCH = 22540;
		const int ACTION_SEARCH_LARGE_FORM_SUBMIT = 22500;

		Hashtable _queryParameters;
		
		// Consider request a real 404 until valid url is derived
		bool Is404 = true;

		private void Page_Load(object sender, System.EventArgs e)
		{
			try
			{
				string url = Matchnet.Constants.NULL_STRING;

				_queryParameters = new Hashtable();

				string queryString = Request.ServerVariables["QUERY_STRING"];

				//is this really a 404?
				if (queryString.StartsWith("404;")) 
				{
					Uri uri = new Uri(queryString.Substring(4));
					
					// Ads with relative paths are causing 404s on our side. Prevent ads from busting out of frames by rendering a dummy pixel.
					if (uri.AbsolutePath.ToString().ToLower() == @"/doubleclick/dartiframe.html")
					{
						Response.Redirect(@"/img/trans.gif");
					}
					
					if (queryString.ToLower().EndsWith(".jpg") || queryString.ToLower().EndsWith(".gif")
                        || queryString.ToLower().EndsWith(".swf"))

					{
						Response.Status = "404 Object Not Found";
						Response.Write("404 not found");
						return;
					}

					if (uri.Query.Length > 0)
					{
						string query = uri.Query;
						if (query.StartsWith("?"))
						{
							query = query.Substring(1);
						}

						// HtmlDecode the querystring because sometimes the links contain encoded values.
						string[] queryParts = System.Web.HttpUtility.HtmlDecode(query).Split('&');

						foreach (string queryPart in queryParts)
						{
							string[] nameValue = queryPart.Split('=');

							if (nameValue.Length == 2 && !_queryParameters.ContainsKey(nameValue[0].ToLower()))
							{
								_queryParameters.Add(nameValue[0].ToLower(), nameValue[1]);
							}
						}
					}

					foreach (string key in Request.Form)
					{
						if (!_queryParameters.ContainsKey(key.ToLower()))
						{
							_queryParameters.Add(key.ToLower().ToString(), Request.Form[key]);
						}
					}
				
			
					int pageID = Constants.NULL_INT;
					double pageIDD;
					int actionID = Constants.NULL_INT;
					double actionIDD;

					//look for "p" parameter in querystring or form
					if (double.TryParse(getQueryParameter("p"), System.Globalization.NumberStyles.Integer, System.Globalization.NumberFormatInfo.InvariantInfo, out pageIDD))
						pageID = (int)pageIDD;

					if (double.TryParse(getQueryParameter("a"), System.Globalization.NumberStyles.Integer, System.Globalization.NumberFormatInfo.InvariantInfo, out actionIDD))
						actionID = (int)actionIDD;

					//	Handle a couple of page-specific overrides to handle situations where
					//	the blanket "go to the new equivalent of the page with the same p-value"
					//	logic doesn't work
					switch (pageID)
					{
							#region PAGE_ARTICLES_DEFAULT
						case (int) ContextGlobal.Pages.PAGE_ARTICLES_DEFAULT:
							url = "/Applications/Article/ArticleView.aspx";
							break;
							#endregion
							#region PAGE_EMAIL_DEFAULT
						case (int) ContextGlobal.Pages.PAGE_EMAIL_DEFAULT:
							url = "/Applications/Email/Mailbox.aspx";
							break;
							#endregion
							#region PAGE_MEMBER_LIST_DEFAULT
						case (int) ContextGlobal.Pages.PAGE_MEMBER_LIST_DEFAULT:
							url = "/Applications/HotList/View.aspx";
							break;
							#endregion
							#region PAGE_DEFAULT
						case (int) ContextGlobal.Pages.PAGE_HOME_HOME:
						switch (actionID)
						{
							case ACTION_SEARCH_LARGE_FORM_SUBMIT:
								url = "/Applications/Search/SearchResults.aspx";
								break;
						}
							break;
							#endregion
							#region PAGE_LOGON_DEFAULT 
						case (int) ContextGlobal.Pages.PAGE_LOGON_DEFAULT:
						case (int) ContextGlobal.Pages.PAGE_LOGON_CONTACT_LOGON:
							//	Handle the special case where p=2000 and we actually need to redirect
							//	the user to another page.  2000 used to be the login page, but we will
							//	just send the user along to the actual destination page and let the 
							//	new system determine what the user needs to be logged in for.

							if(_queryParameters.ContainsKey("destinationurl")) 
							{
								url = (string)_queryParameters["destinationurl"];
								url = Server.UrlDecode(url);
							}
							else
							{
								// Go to logon page if no destination url was provided.
								url = "/Applications/Logon/Logon.aspx?DestUrlMissing";
							}

							// Regardless of url values, it's still a valid page call
							Is404 = false;
							break;
							#endregion
							#region PAGE_MEMBER_PROFILE_STEP_1
						case (int) ContextGlobal.Pages.PAGE_MEMBER_PROFILE_STEP_1:
							url = "/Applications/MemberProfile/RegistrationStep1.aspx";
							break;
							#endregion
							#region PAGE_MEMBER_PROFILE_VIEW_PROFILE_MAIN
						case (int) ContextGlobal.Pages.PAGE_MEMBER_PROFILE_VIEW_PROFILE_MAIN:
							//	Handle a deprecated AttractionBar.aspx page.  This will show up from the
							//	page lookup above, but goes to a page that is no longer any good.  Since
							//	the page isn't any good we will just send the user to the closest equivalent
							//	which happens to be the member profile page.                                                                     

							url = "/Applications/MemberProfile/ViewProfile.aspx";
							Is404 = false;
							break;
							#endregion
							#region PAGE_MEMBER_PROFILE_EDIT_PROFILE
						case (int) ContextGlobal.Pages.PAGE_MEMBER_PROFILE_EDIT_PROFILE:
							url = "/Applications/MemberProfile/RegistrationStep1.aspx?ForceLogon=1";
							Is404 = false;
							break;
							#endregion
							#region PAGE_MEMBERS_ONLINE_DEFAULT
						case (int) ContextGlobal.Pages.PAGE_MEMBERS_ONLINE_DEFAULT:
							url = "/Applications/MembersOnline/MembersOnline.aspx";
							break;
							#endregion
							#region PAGE_LOGON_RETRIEVE_PASSWORD
						case (int) ContextGlobal.Pages.PAGE_LOGON_RETRIEVE_PASSWORD:
							url = "/Applications/Logon/RetrievePassword.aspx";
							break;
							#endregion
							#region PAGE_MEMBER_SERVICES_EMAIL_SETTINGS
						case (int) ContextGlobal.Pages.PAGE_MEMBER_SERVICES_EMAIL_SETTINGS:
							url = "/Applications/Email/MessageSettings.aspx";
							break;
							#endregion
							#region PAGE_MEMBER_PHOTOS_DEFAULT
						case (int) ContextGlobal.Pages.PAGE_MEMBER_PHOTOS_DEFAULT:
							url = "/Applications/MemberProfile/MemberPhotoUpload.aspx";
							break;
							#endregion
							#region PAGE_MEMBER_SERVICES_PROFILE_DISPLAY_SETTINGS
						case (int) ContextGlobal.Pages.PAGE_MEMBER_SERVICES_PROFILE_DISPLAY_SETTINGS:
							url = "/Applications/MemberServices/DisplaySettings.aspx";
							break;
							#endregion
							#region PAGE_MEMBER_SERVICES_VERIFY_EMAIL
						case (int) ContextGlobal.Pages.PAGE_MEMBER_SERVICES_VERIFY_EMAIL:
							url = "/Applications/MemberServices/VerifyEmail.aspx";
							break;
							#endregion
							#region PAGE_ARTICLES_VIEW_ARTICLE
						case (int) ContextGlobal.Pages.PAGE_ARTICLES_VIEW_ARTICLE:
							url = "/Applications/Article/ArticleDefault.aspx";
							break;
							#endregion
						default:
							// If page is not found null value is returned
							//TODO: Need this capability on PageConfigSA ?
							//url = WebPage.GetPathByPageID((int)pageID);

							// Only append querystring if valid url was returned
							if (url != null)
								url += buildQueryString(url);
							break;
					}

					// Look for "a" parameter in querystring or form
					if ((url == null) && (Is404))
					{
						switch (actionID)
						{
							case ACTION_MEMBER_SERVICES_VERIFY_EMAIL:
								url = "/Applications/MemberServices/VerifyEmail.aspx?a=verify";
								break;
							case ACTION_SEARCH:
								url = "/Applications/Search/SearchResults.aspx?SaveSearchPrefs=true";
								break;
							case ACTION_SEARCH_LARGE_FORM_SUBMIT:
								url = "/Applications/Search/SearchResults.aspx";
								break;
							case (int) ContextGlobal.Pages.ACTION_MEMBER_PROFILE_SAVE_STEP_1:
								url = "/Applications/MemberProfile/RegistrationStep1.aspx";
								break;
						}
					}
			
					//look for username
					if ((url == null) && (Is404))
					{
						string username = uri.LocalPath.Substring(1);
						if (username.EndsWith("/"))
						{
							username = username.Substring(0, username.Length - 1);
						}

						// This prevents crap like default.asp from being passed in as a username. TT#20443
						if(username != String.Empty && username.IndexOf(".") < 0  && username.IndexOf("/") < 0)
						{
							int memberID = Constants.NULL_INT;

							memberID = MemberSA.Instance.GetMemberID(username);

							if (memberID != Constants.NULL_INT)
							{
								url = string.Format("/Applications/MemberProfile/ViewProfile.aspx?MemberID={0}", memberID.ToString());
							}
						}
					}
				}
                else if(queryString.StartsWith("403;"))
			    {
                    Response.Redirect(@"/");
                    Response.End();
			    }

				url += buildQueryString(url);

				if (url != null)
				{
					Response.Redirect(url,false);
					HttpContext.Current.ApplicationInstance.CompleteRequest();
					return;
				}

			
				// If page request is a valid hit, then do not display 404 notification msg.
				string mid = String.Empty;
			
				//Ensure that legacy urls of the form /default.asp?PRM=xxx do not display "the page cannot be found" error message.
				if (Is404 && getQueryParameter("PRM") == string.Empty)
					mid = "rc=WE_WERE_UNABLE_TO_FIND_THE_PAGE_YOU_ARE_LOOKING_FOR_X";
                Matchnet.Web.Applications.PremiumServices.VelocityHandler.DebugTrace("404Form", "redirecting to homepage", null);
				Response.Redirect("/Default.aspx" + buildQueryString("/Default.aspx") + mid, false);
				HttpContext.Current.ApplicationInstance.CompleteRequest();
				return;
			}
			catch (ThreadAbortException)
			{
				// threadabortexception caused by Redirect() with EndResponse set to true
				// we cant set EndResponse to false or it will process a full request after a redirect
				// we cant log threadabortexception because it occurs too frequently
				// this is currently the best known way to handle this issue
			}
			catch(Exception ex)
			{
				ContextExceptions _ContextExceptions = new ContextExceptions();
				_ContextExceptions.LogException(ex);
				Response.Redirect("/Applications/Home/Error.aspx",false);
				HttpContext.Current.ApplicationInstance.CompleteRequest();
				return;
			}
		}

		private string getQueryParameter(string name)
		{
			string val = _queryParameters[name.ToLower()] as string;

			if (val == null)
			{
				return "";
			}
			else
			{
				return val;
			}
		}

		// NOTE:  I uncommented lines 249-255 in order for this function to work with the PRM bug fix (TT: 8367).
		// If this causes problems, this will need to be re-commented out and a duplicate function with those lines
		// not commented out will need to be created.
		private string buildQueryString(string url)
		{
			StringBuilder sb = new StringBuilder();

			if (url != null)
			{
				sb.Append(url.IndexOf("?") < 0 ? "?" : "&");
			}
			else
			{
				sb.Append("?");
			}

			foreach (string name in _queryParameters.Keys)
			{
				if (name.ToLower() != "p" && name != "a")
				{
					string paramName = name;
					//Hack for legacy registration forms -- Issue #13642.
					if (name.ToLower() == "termsofagreementflag")
					{
						paramName = "termsandconditions";
					}
					
					sb.Append(paramName + "=" + HttpUtility.UrlEncode(_queryParameters[name].ToString()) + "&");
				}
			}

			//Form variables are added to the _queryParamters hashtable in Page_Load above.
			//Adding them here results in duplicate parameters in the URL.
			/*foreach (string name in Request.Form.AllKeys)
			{
				if (name.ToLower() != "p" && name != "a")
				{
					sb.Append(name + "=" + HttpUtility.UrlEncode(Request.Form[name]) + "&");
				}
			}*/

			return sb.ToString();
		}		
		
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
