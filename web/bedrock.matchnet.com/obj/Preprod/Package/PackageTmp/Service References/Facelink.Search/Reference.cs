﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.17929
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Matchnet.Web.Facelink.Search {
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(Namespace="http://facelink.com/facelink", ConfigurationName="Facelink.Search.SearchPortType")]
    public interface SearchPortType {
        
        // CODEGEN: Generating message contract since the operation getSearchResults is neither RPC nor document wrapped.
        [System.ServiceModel.OperationContractAttribute(Action="", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute()]
        Matchnet.Web.Facelink.Search.getSearchResultsResponse getSearchResults(Matchnet.Web.Facelink.Search.getSearchResultsRequest request);
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.17929")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true, Namespace="http://facelink.com/facelink")]
    public partial class searchRequest : object, System.ComponentModel.INotifyPropertyChanged {
        
        private string queryField;
        
        private int ageField;
        
        private string locationField;
        
        private string preferenceField;
        
        private string pageField;
        
        private string pageSizeField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=0)]
        public string query {
            get {
                return this.queryField;
            }
            set {
                this.queryField = value;
                this.RaisePropertyChanged("query");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=1)]
        public int age {
            get {
                return this.ageField;
            }
            set {
                this.ageField = value;
                this.RaisePropertyChanged("age");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=2)]
        public string location {
            get {
                return this.locationField;
            }
            set {
                this.locationField = value;
                this.RaisePropertyChanged("location");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=3)]
        public string preference {
            get {
                return this.preferenceField;
            }
            set {
                this.preferenceField = value;
                this.RaisePropertyChanged("preference");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=4)]
        public string page {
            get {
                return this.pageField;
            }
            set {
                this.pageField = value;
                this.RaisePropertyChanged("page");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=5)]
        public string pageSize {
            get {
                return this.pageSizeField;
            }
            set {
                this.pageSizeField = value;
                this.RaisePropertyChanged("pageSize");
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.17929")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://facelink.com/facelink")]
    public partial class searchResultItem : object, System.ComponentModel.INotifyPropertyChanged {
        
        private string userNameField;
        
        private string memberIdField;
        
        private int ageField;
        
        private string urlField;
        
        private string siteIdField;
        
        private string communityField;
        
        private string aboutMeField;
        
        private string regionField;
        
        private string thumbFileField;
        
        private string displayPhotoField;
        
        private string photoCountField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=0)]
        public string userName {
            get {
                return this.userNameField;
            }
            set {
                this.userNameField = value;
                this.RaisePropertyChanged("userName");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=1)]
        public string memberId {
            get {
                return this.memberIdField;
            }
            set {
                this.memberIdField = value;
                this.RaisePropertyChanged("memberId");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=2)]
        public int age {
            get {
                return this.ageField;
            }
            set {
                this.ageField = value;
                this.RaisePropertyChanged("age");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=3)]
        public string url {
            get {
                return this.urlField;
            }
            set {
                this.urlField = value;
                this.RaisePropertyChanged("url");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=4)]
        public string siteId {
            get {
                return this.siteIdField;
            }
            set {
                this.siteIdField = value;
                this.RaisePropertyChanged("siteId");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=5)]
        public string community {
            get {
                return this.communityField;
            }
            set {
                this.communityField = value;
                this.RaisePropertyChanged("community");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=6)]
        public string aboutMe {
            get {
                return this.aboutMeField;
            }
            set {
                this.aboutMeField = value;
                this.RaisePropertyChanged("aboutMe");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=7)]
        public string region {
            get {
                return this.regionField;
            }
            set {
                this.regionField = value;
                this.RaisePropertyChanged("region");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=8)]
        public string thumbFile {
            get {
                return this.thumbFileField;
            }
            set {
                this.thumbFileField = value;
                this.RaisePropertyChanged("thumbFile");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=9)]
        public string displayPhoto {
            get {
                return this.displayPhotoField;
            }
            set {
                this.displayPhotoField = value;
                this.RaisePropertyChanged("displayPhoto");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=10)]
        public string photoCount {
            get {
                return this.photoCountField;
            }
            set {
                this.photoCountField = value;
                this.RaisePropertyChanged("photoCount");
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.17929")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true, Namespace="http://facelink.com/facelink")]
    public partial class searchResultResponse : object, System.ComponentModel.INotifyPropertyChanged {
        
        private int totalField;
        
        private searchResultItem[] getSearchResultField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=0)]
        public int total {
            get {
                return this.totalField;
            }
            set {
                this.totalField = value;
                this.RaisePropertyChanged("total");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order=1)]
        [System.Xml.Serialization.XmlArrayItemAttribute("item")]
        public searchResultItem[] getSearchResult {
            get {
                return this.getSearchResultField;
            }
            set {
                this.getSearchResultField = value;
                this.RaisePropertyChanged("getSearchResult");
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class getSearchResultsRequest {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://facelink.com/facelink", Order=0)]
        public Matchnet.Web.Facelink.Search.searchRequest searchRequest;
        
        public getSearchResultsRequest() {
        }
        
        public getSearchResultsRequest(Matchnet.Web.Facelink.Search.searchRequest searchRequest) {
            this.searchRequest = searchRequest;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class getSearchResultsResponse {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://facelink.com/facelink", Order=0)]
        public Matchnet.Web.Facelink.Search.searchResultResponse searchResultResponse;
        
        public getSearchResultsResponse() {
        }
        
        public getSearchResultsResponse(Matchnet.Web.Facelink.Search.searchResultResponse searchResultResponse) {
            this.searchResultResponse = searchResultResponse;
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface SearchPortTypeChannel : Matchnet.Web.Facelink.Search.SearchPortType, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class SearchPortTypeClient : System.ServiceModel.ClientBase<Matchnet.Web.Facelink.Search.SearchPortType>, Matchnet.Web.Facelink.Search.SearchPortType {
        
        public SearchPortTypeClient() {
        }
        
        public SearchPortTypeClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public SearchPortTypeClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public SearchPortTypeClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public SearchPortTypeClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        Matchnet.Web.Facelink.Search.getSearchResultsResponse Matchnet.Web.Facelink.Search.SearchPortType.getSearchResults(Matchnet.Web.Facelink.Search.getSearchResultsRequest request) {
            return base.Channel.getSearchResults(request);
        }
        
        public Matchnet.Web.Facelink.Search.searchResultResponse getSearchResults(Matchnet.Web.Facelink.Search.searchRequest searchRequest) {
            Matchnet.Web.Facelink.Search.getSearchResultsRequest inValue = new Matchnet.Web.Facelink.Search.getSearchResultsRequest();
            inValue.searchRequest = searchRequest;
            Matchnet.Web.Facelink.Search.getSearchResultsResponse retVal = ((Matchnet.Web.Facelink.Search.SearchPortType)(this)).getSearchResults(inValue);
            return retVal.searchResultResponse;
        }
    }
}
