using System;
using System.Data;
using System.Drawing;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using Matchnet.Web.Framework;

namespace Matchnet.Web.Controls.BannerAd {
	/// <summary>
	///		Summary description for BannerAd.
	/// </summary>
	public class BannerAd : FrameworkControl {
		private string _providerTag = "";
		
		public string ProviderTag {
			get {
				return _providerTag;
			}
			set {
				_providerTag = value;
			}
		}

		private const string Const_AdBrite120x240 = "AdBrite120x240";
		private const string _AdBrite120x240 = "<!-- Begin: AdBrite -->" +
			"<iframe frameborder=\"0\" width=\"120\" height=\"240\" marginwidth=\"0\" marginheight=\"0\" scrolling=\"no\" src=\"http://3.adbrite.com/mb/banner.php?sid=21430&html=1\">" +
			"<script type=\"text/javascript\" src=\"http://2.adbrite.com/mb/banner.php?sid=21430\"></script>" +
			"</iframe><!-- End: AdBrite -->";

		
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e) {
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}

		#endregion

		private void InitializeComponent() {
			this.Load += new System.EventHandler(this.Page_Load);
		}

		private void Page_Load(object sender, System.EventArgs e) {
		}
	
		protected override void Render(System.Web.UI.HtmlTextWriter writer) {		
			if (_providerTag == Const_AdBrite120x240) {
				writer.WriteLine("<div>");
				writer.Write(_AdBrite120x240);
				writer.WriteLine("</div>");
			}
			base.Render(writer);
		}
	}
}
