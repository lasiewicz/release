using System;
using System.Text;
using System.Configuration;

using Matchnet.Web.Framework;

namespace Matchnet.Web.Controls.RichTextBox
{
	public class ClientSideScript
	{
		private RichTextEdit _RichTextBox;
		private string _FormName = Matchnet.Constants.NULL_STRING;
		private string _DialogPath = Matchnet.Constants.NULL_STRING;
		//private bool _AllowHtmlEditing = false;
		//private bool _FocusOnLoad = false;
		private string _TextBoxSize = "false";
		private System.Drawing.Color _ToolbarColor;
		private System.Drawing.Color _ToolbarItemOverColor;
		private System.Drawing.Color _ToolbarItemOverBorderColor;
		private string _TabIndexOverrides = Matchnet.Constants.NULL_STRING;
		private string _RTBName = Matchnet.Constants.NULL_STRING;
		private string _ClientID = Matchnet.Constants.NULL_STRING;
		private string _HtmlName = Matchnet.Constants.NULL_STRING;
		//private string _RowHeight = Matchnet.Constants.NULL_STRING;
		private string _DefaultSCR = "/blank.htm";
		private Matchnet.Web.Framework.Image _image;
		private const string DEBUG = "\r\n";

		public ClientSideScript(RichTextEdit parent)
		{
			System.Web.UI.Control control = new System.Web.UI.Control();
			_image = new Matchnet.Web.Framework.Image();
			_RichTextBox = parent;
			_DialogPath = parent.DialogPath;
			_TextBoxSize = parent.TextBoxHeight;
			_ToolbarColor = parent.ToolbarColor;
			_ToolbarItemOverColor = parent.ToolbarItemOver;
			_ToolbarItemOverBorderColor = parent.ToolbarItemOverBorder;
			_FormName = parent.FormName;
			_RTBName = _RichTextBox.TidyObjectRef(_RichTextBox.ClientID + "_RTB");
			_ClientID = _RichTextBox.TidyObjectRef(_RichTextBox.ClientID);
			_HtmlName = "document.getElementById('" + parent.UniqueID + "')";	
		}


		public string RegisterIFrame()
		{
			return "<iframe id=\"" + _ClientID + "_RTB\" " +
				"src=\"" + _DefaultSCR + "\" " +
				"tabindex=\"" + _TabIndexOverrides + 
				"\" onBlur=\"" + _ClientID + "_output();\" onFocus=\"" + 
				_ClientID + "_fill();\" style=\"padding: 0px;font-family:verdana;border: solid 1px #727272;\" " +
				"width=\"100%\" height=\" " + 
				_TextBoxSize + "\" " + 
				"marginheight= \"0\" " +
				"marginwidth= \"0\" " +
				"scrolling = \"auto\" " +
				"frameborder = \"0\" " +
				"></iframe>";
		}


		public string RegisterSwitchButtons()
		{
			StringBuilder sb = new StringBuilder();

			sb.Append("<div style=\"width:100%; background-color:" + System.Drawing.ColorTranslator.ToHtml(_ToolbarColor) + "; padding-top:4px; padding-bottom:0px;\">");
			sb.Append("<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\"><tr><td>&nbsp;");
			sb.Append("<img name=\"" + _ClientID + "_tab1\" unselectable=\"on\" src=\"" +  _image.GetUrl("tab_edit_on.gif", Constants.APP_FREETEXTAPPROVAL, false) + "\" style=\"cursor:hand;\" width=\"65\" height=\"19\" border=\"0\" onClick=\"" + _ClientID + "_SwitchEditor()\">");
			sb.Append("<img name=\"" + _ClientID + "_tab2\" unselectable=\"on\" src=\"" +  _image.GetUrl("tab_html_off.gif", Constants.APP_FREETEXTAPPROVAL, false) + "\" style=\"cursor:hand;\" border=\"0\" width=\"65\" height=\"19\" onClick=\"" + _ClientID + "_SwitchHTML()\">");
			sb.Append("</td></tr></table>");
			sb.Append("</div>");
				
			return sb.ToString();
		}


		public string RenderIEObjects()
		{
			StringBuilder sb = new StringBuilder();

			sb.Append("<object id=\"" + _ClientID + "_ObjTableInfo\" CLASSID=\"clsid:47B0DFC7-B7A3-11D1-ADC5-006008A5848C\"");
			sb.Append(" VIEWASTEXT width=\"0px\" height=\"0px\"></object>");
			sb.Append("<object id=\"" + _ClientID + "_dlgHelper\" CLASSID=\"clsid:3050f819-98b5-11cf-bb82-00aa00bdce0b\"");
			sb.Append(" width=\"0px\" height=\"0px\"></object>");

			return sb.ToString();
		}


		public string RegisterClientScript()

		{
			StringBuilder sb = new StringBuilder();

			sb.Append("<style type=\"text/css\">");
			sb.Append("." + _ClientID + "_MenuButton {background-color:" + System.Drawing.ColorTranslator.ToHtml(_ToolbarColor) + 
				"; border:1px " + System.Drawing.ColorTranslator.ToHtml(_ToolbarColor) + 
				" solid; width: 24; font-family:MS Sans Serif; font-size: 8pt;}");
			sb.Append(".emot {background-color:" + System.Drawing.ColorTranslator.ToHtml(_ToolbarColor) +
				"; border:1px " + System.Drawing.ColorTranslator.ToHtml(_ToolbarColor) + 
				" solid; width: 24; font-family:MS Sans Serif; font-size: 8pt;}");
			sb.Append("." + _ClientID + "_MenuItemSeperator {border: 0px " + System.Drawing.ColorTranslator.ToHtml(_ToolbarColor).ToString() + " solid;}");
			sb.Append("." + _ClientID + "_ToolBarSeperator {width: 100%; height:2px; margin-top:2px; margin-bottom:2px;}");
			sb.Append("</style>");

			sb.Append("<script language=\"Javascript\" TYPE=\"text/javascript\">");

			sb.Append("var " + _ClientID + "_isHTMLMode = false;");
			sb.Append(DEBUG);
			sb.Append("var " + _ClientID + "_perline = 10;");
			sb.Append(DEBUG);
			sb.Append("var " + _ClientID + "_RTBGuideLineStatus;");
			sb.Append(DEBUG);
			sb.Append("var " + _ClientID + "_colorLevels = Array('1', '3', '5', '7', '9', 'B', 'D','F');");
			sb.Append(DEBUG);
			sb.Append("var " + _ClientID + "_colorArray = Array();");
			sb.Append(DEBUG);
			sb.Append("var " + _ClientID + "_ie = false;");
			sb.Append(DEBUG);
			sb.Append("var " + _ClientID + "_nocolor = 'none';");
			sb.Append(DEBUG);
			sb.Append("if (document.all) { " + _ClientID + "_ie = true; " + _ClientID + "_nocolor = ''; }");
			sb.Append(DEBUG);

			sb.Append("window.attachEvent(\"onload\", " + _ClientID + "_doInit);");
			sb.Append(DEBUG);

			sb.Append("function " + _ClientID + "_doInit(){");
			sb.Append(DEBUG);
			sb.Append("window.setTimeout(\"" + _ClientID + "_filliframe()\", 250);");
			sb.Append(DEBUG);
			sb.Append(_RTBName + ".document.body.contentEditable = 'True';");
			sb.Append(DEBUG);
			sb.Append(_RTBName + ".document.designMode = \"On\";");
			sb.Append(DEBUG);
			sb.Append(_RTBName + ".document.execCommand(\"LiveResize\", true, true);");
			sb.Append(DEBUG);
			sb.Append(_RTBName + ".document.execCommand(\"2D-Position\", true, true);");
			sb.Append(DEBUG);
			sb.Append(_RTBName + ".document.execCommand(\"MultipleSelection\", true, true);");
			sb.Append(DEBUG);
			sb.Append(_ClientID + "_RTBGuideLineStatus = false;");
			sb.Append(DEBUG);
			sb.Append(_RTBName + ".document.focus;");
			sb.Append(DEBUG);
			if (_RichTextBox.ToolBarConfiguration != RichTextEdit.ToolBarTemplates.FreeTextApproval)
				sb.Append(_ClientID + "_genColors();");
			sb.Append(DEBUG);
			sb.Append("}");
			sb.Append(DEBUG);

			sb.Append("function " + _ClientID + "_filliframe(){");
			sb.Append(DEBUG);
			sb.Append("try{");
			sb.Append("if (" + _RTBName + ".document.body.innerHTML == \"\"){");
			sb.Append(DEBUG);
			sb.Append(_RTBName + ".document.body.innerHTML = " + _HtmlName + ".value;");
			sb.Append(DEBUG);
			sb.Append(_RTBName + ".document.body.style.fontFamily = \"verdana, arial, tahoma\";");
			sb.Append(DEBUG);
			sb.Append(_RTBName + ".document.body.style.fontSize = \"10pt\";");
			sb.Append(DEBUG);
			sb.Append("window.setTimeout(\"" + _ClientID + "_filliframe()\",  250);}");
			sb.Append(DEBUG);
			sb.Append("}");
			sb.Append("catch(er){");
			sb.Append("window.setTimeout(\"" + _ClientID + "_filliframe()\", 500);");
			sb.Append("}");
			sb.Append("}");
			sb.Append(DEBUG);

			sb.Append("function " + _ClientID + "_fill(){");
			sb.Append(DEBUG);
			sb.Append("var hideme = " + _ClientID + "_getObj('" + _ClientID + "_Highlight_DivLayer');");
			sb.Append(DEBUG);
			sb.Append("if (hideme != null) {hideme.style.display = 'none';}");
			sb.Append(DEBUG);
			sb.Append("var hideme = " + _ClientID + "_getObj('" + _ClientID + "_FontColor_DivLayer');");
			sb.Append(DEBUG);
			sb.Append("if (hideme != null) {hideme.style.display = 'none';}");
			sb.Append(DEBUG);
			sb.Append("var hideme = " + _ClientID + "_getObj('" + _ClientID + "_TableMenu_tableMenu');");
			sb.Append(DEBUG);
			sb.Append("if (hideme != null) {hideme.style.display = 'none';}");
			sb.Append(DEBUG);

			sb.Append("if (" + _RTBName + ".document.body.innerHTML == \"\")");
			sb.Append(DEBUG);
			sb.Append("{" + _HtmlName + ".value = " + _RTBName + ".document.body.innerHTML;}");
			sb.Append(DEBUG);
			sb.Append("}");
			sb.Append(DEBUG);

			sb.Append("function " + _ClientID + "_output(){");
			sb.Append(DEBUG);
			sb.Append("if (" + _ClientID + "_isHTMLMode == true){");
			sb.Append(DEBUG);
			sb.Append(_HtmlName + ".value = " + _RTBName + ".document.body.innerText;}");
			sb.Append(DEBUG);
			sb.Append("else {");
			sb.Append(DEBUG);
			sb.Append(_HtmlName + ".value = " + _RTBName + ".document.body.innerHTML;}");
			sb.Append(DEBUG);


			sb.Append("if (" + _RTBName + ".document.body.innerText == \"\" || " + _RTBName + ".document.body.innerText == \" \") {");
			sb.Append(DEBUG);
			sb.Append(_HtmlName + ".value = \"\";");
			sb.Append(DEBUG);
			sb.Append("}");


			sb.Append("}");
			sb.Append(DEBUG);

			
			sb.Append("function " + _ClientID + "_getObj(id) {if (" + _ClientID + "_ie) {return document.all[id];}");
			sb.Append(DEBUG);
			sb.Append("else {return document.getElementById(id);}}");
			sb.Append(DEBUG);



			// Minimize Javascript Function - Really heavy for multiple controls on one page
			if (_RichTextBox.ToolBarConfiguration == RichTextEdit.ToolBarTemplates.FreeTextApproval)
			{
				sb.Append("</script>");
				return sb.ToString();
			}

			sb.Append("function " + _ClientID + "_cmdExec(cmd,opt){");
			sb.Append(DEBUG);
			sb.Append("if (" + _ClientID + "_isHTMLMode) {alert(\"Function not available in HTML Mode\"); return;}");
			sb.Append(DEBUG);
			sb.Append(_RTBName + ".focus(); " + _RTBName + ".document.execCommand(cmd,'',opt); " + _RTBName + ".focus();");
			sb.Append(DEBUG);
			sb.Append("}");
			sb.Append(DEBUG);

			sb.Append("function " + _ClientID + "_SwitchEditor(){");
			sb.Append(DEBUG);
			sb.Append("if (" + _ClientID + "_isHTMLMode == true){");
			sb.Append(DEBUG);
			sb.Append(_ClientID + "_isHTMLMode = false;");
			sb.Append(DEBUG);
			sb.Append("sTmp = " + _RTBName + ".document.body.innerText; " + _RTBName + ".document.body.innerHTML = sTmp;");
			sb.Append(DEBUG);
			sb.Append(_RTBName + ".document.body.style.fontFamily = \"verdana, arial, tahoma\";");
			sb.Append(DEBUG);
			sb.Append(_RTBName + ".document.body.style.fontSize = \"10pt\";");
			sb.Append(DEBUG);
			sb.Append("document." + _ClientID + "_tab1.src = \"" +  _image.GetUrl("tab_edit_on.gif", Constants.APP_FREETEXTAPPROVAL, false) +  "\"; document." + _ClientID + "_tab2.src = \"" +  _image.GetUrl("tab_html_off.gif", Constants.APP_FREETEXTAPPROVAL, false) +  "\"; ");
			sb.Append(DEBUG);
			sb.Append(_RTBName + ".focus(); " + _ClientID + "_enableButtons();");
			sb.Append(DEBUG);
			sb.Append(_ClientID + "_toggleGuidelines();");
			sb.Append(DEBUG);
			sb.Append("return true;}");
			sb.Append(DEBUG);
			sb.Append("}");
			sb.Append(DEBUG);

			sb.Append("function commentEmoticon(emot){");
			sb.Append("if (" + _ClientID + "_isHTMLMode) {alert(\"Function not available in HTML Mode\"); return;}");
			sb.Append(_RTBName + ".focus();");
			sb.Append("var r = " + _RTBName + ".document.selection.createRange();");
			sb.Append("r.pasteHTML(emot);}");


			sb.Append("function " + _ClientID + "_InsertFloatFont() {");
			sb.Append(DEBUG);
			sb.Append("if (" + _ClientID + "_isHTMLMode) {alert(\"Function not available in HTML Mode\"); return;}");
			sb.Append(DEBUG);
			sb.Append(_RTBName + ".focus();");
			sb.Append(DEBUG);
			sb.Append("var elemStr = \"<div><span id=\'temp\' onclick=\'this.focus();\' style=\'width:100\'></span></div>\";");
			sb.Append(DEBUG);
			sb.Append("var r = " + _RTBName + ".document.selection.createRange();");
			sb.Append(DEBUG);
			sb.Append("r.pasteHTML(elemStr); var thisElem = " + _RTBName + ".document.getElementById('temp'); thisElem.focus();");
			sb.Append(DEBUG);
			sb.Append("thisElem.removeAttribute('id');");
			sb.Append(DEBUG);
			sb.Append("}");
			sb.Append(DEBUG);

			sb.Append("function " + _ClientID + "_SwitchHTML(){");
			sb.Append(DEBUG);
			sb.Append("if (" + _ClientID + "_isHTMLMode == false){");
			sb.Append(DEBUG);
			sb.Append(_ClientID + "_isHTMLMode = true;");
			sb.Append(DEBUG);
			sb.Append("sTmp = " + _RTBName + ".document.body.innerHTML; " + _RTBName + ".document.body.innerText = sTmp;");
			sb.Append(DEBUG);
			sb.Append(_RTBName + ".document.body.style.fontFamily = \"courier new, monospace\";");
			sb.Append(DEBUG);
			sb.Append(_RTBName + ".document.body.style.fontSize = \"10pt\";");
			sb.Append(DEBUG);
			sb.Append("document." + _ClientID + "_tab1.src = \"" +  _image.GetUrl("tab_edit_off.gif", Constants.APP_FREETEXTAPPROVAL, false) + "\"; document." + _ClientID + "_tab2.src = \"" +  _image.GetUrl("tab_html_on.gif", Constants.APP_FREETEXTAPPROVAL, false) +  "\";");
			sb.Append(DEBUG);
			sb.Append(_RTBName + ".focus(); " + _ClientID + "_disableButtons();");
			sb.Append(DEBUG);
			sb.Append("if(" + _ClientID + "_RTBGuideLineStatus) {" + _ClientID + "_RTBGuideLineStatus = false;} else {" + _ClientID + "_RTBGuideLineStatus = true;}");
			sb.Append(DEBUG);
			sb.Append("return true;}");
			sb.Append(DEBUG);
			sb.Append("}");
			sb.Append(DEBUG);

			sb.Append("function " + _ClientID + "_insertImage() {");
			sb.Append(DEBUG);
			sb.Append("if (" + _ClientID + "_isHTMLMode) {alert(\"Function not available in HTML Mode\"); return;}");
			sb.Append(DEBUG);
			sb.Append(_RTBName + ".focus();");
			sb.Append(DEBUG);
			sb.Append(_RTBName + ".document.execCommand('InsertImage','1','');");
			sb.Append(DEBUG);
			sb.Append("return;");
			sb.Append(DEBUG);
			sb.Append("}");
			sb.Append(DEBUG);

			sb.Append("function " + _ClientID + "_InsertDateTime() {");
			sb.Append(DEBUG);
			sb.Append("if (" + _ClientID + "_isHTMLMode) {alert(\"Function not available in HTML Mode\"); return;}");
			sb.Append(DEBUG);
			sb.Append("var d = new Date();");
			sb.Append(DEBUG);
			sb.Append(_RTBName + ".focus();");
			sb.Append(DEBUG);
			sb.Append("var r = " + _RTBName + ".document.selection.createRange();");
			sb.Append(DEBUG);
			sb.Append("r.pasteHTML(d.toLocaleDateString() + \" \" + d.toLocaleTimeString());}");
			sb.Append(DEBUG);

			sb.Append("function " + _ClientID + "_Undo() {");
			sb.Append(DEBUG);
			sb.Append("if (" + _ClientID + "_isHTMLMode) {alert(\"Function not available in HTML Mode\"); return;}");
			sb.Append(DEBUG);
			sb.Append(_RTBName + ".focus();");
			sb.Append(DEBUG);
			sb.Append(_RTBName + ".document.execCommand('undo','',null);}");
			sb.Append(DEBUG);

			sb.Append("function " + _ClientID + "_Redo() {");
			sb.Append(DEBUG);
			sb.Append("if (" + _ClientID + "_isHTMLMode) {alert(\"Function not available in HTML Mode\"); return;}");
			sb.Append(DEBUG);
			sb.Append(_RTBName + ".focus();");
			sb.Append(DEBUG);
			sb.Append(_RTBName + ".document.execCommand('redo','',null);}");
			sb.Append(DEBUG);

			//create link function
			sb.Append("function " + _ClientID + "_createLink() {");
			sb.Append(DEBUG);
			sb.Append("if (" + _ClientID + "_isHTMLMode){alert(\"Function not available in HTML Mode\");return;}");
			sb.Append(DEBUG);
			sb.Append(_ClientID + "_cmdExec(\"CreateLink\");");
			sb.Append(DEBUG);
			sb.Append("}");
			sb.Append(DEBUG);

			// toggle runtime guidelines

			sb.Append("function " + _ClientID + "_toggleGuidelines() {");
			sb.Append(DEBUG);
			sb.Append("if (" + _ClientID + "_isHTMLMode){alert(\"Function not available in HTML Mode\");return;}");
			sb.Append(DEBUG);
			sb.Append("var oTables = " + _RTBName + ".document.getElementsByTagName('TABLE');");
			sb.Append(DEBUG);
			sb.Append("if(" + _ClientID + "_RTBGuideLineStatus) {");
			sb.Append(DEBUG);
			sb.Append("for (i=0;i<oTables.length;i++) {");
			sb.Append(DEBUG);
			sb.Append("if(oTables[i].border == 0) {");
			sb.Append(DEBUG);
			sb.Append("oTables[i].runtimeStyle.borderWidth = '';");
			sb.Append(DEBUG);
			sb.Append("oTables[i].runtimeStyle.borderColor = '';");
			sb.Append(DEBUG);
			sb.Append("oTables[i].runtimeStyle.borderStyle = '';");
			sb.Append(DEBUG);
			sb.Append("for(j=0;j<oTables[i].getElementsByTagName('TD').length;j++) {");
			sb.Append(DEBUG);
			sb.Append("oTables[i].getElementsByTagName('TD')[j].runtimeStyle.border = '';");
			sb.Append(DEBUG);
			sb.Append(DEBUG);
			sb.Append("}}}");
			sb.Append(DEBUG);
			sb.Append(_ClientID + "_RTBGuideLineStatus = false;");
			sb.Append(DEBUG);
			sb.Append("} else {");
			sb.Append(DEBUG);
			sb.Append("for (i=0;i<oTables.length;i++) {");
			sb.Append(DEBUG);
			sb.Append("if(oTables[i].border == 0) {");
			sb.Append(DEBUG);
			sb.Append("oTables[i].runtimeStyle.borderWidth = 1;");
			sb.Append(DEBUG);
			sb.Append("oTables[i].runtimeStyle.borderColor = '#888888';");
			sb.Append(DEBUG);
			sb.Append("oTables[i].runtimeStyle.borderStyle = 'dotted';");
			sb.Append(DEBUG);
			sb.Append("for (j=0;j<oTables[i].getElementsByTagName('TD').length;j++)");
			sb.Append(DEBUG);
			sb.Append("oTables[i].getElementsByTagName('TD')[j].runtimeStyle.border = '#888888 1 dotted';");
			sb.Append(DEBUG);
			sb.Append("}}");
			sb.Append(DEBUG);
			sb.Append(_ClientID + "_RTBGuideLineStatus = true;");
			sb.Append(DEBUG);
			sb.Append("}}");
			sb.Append(DEBUG);

			// insert table model dialog display

			sb.Append("function " + _ClientID + "_insertTable() {");
			sb.Append(DEBUG);
			sb.Append("var args = new Array();");
			sb.Append(DEBUG);
			sb.Append("var arr = null;");
			sb.Append(DEBUG);
			sb.Append("if (" + _ClientID + "_isHTMLMode) {alert(\"Function not available in HTML Mode\"); return;}");
			sb.Append(DEBUG);
			sb.Append("if (" + _RTBName + ".document.selection.type == \"Control\") {alert(\"You cannot insert a table when you have a control selected.\"); return;}");
			sb.Append(DEBUG);
			sb.Append("var hideme = " + _ClientID + "_getObj('" + _ClientID + "_TableMenu_tableMenu');"); // hide table menu
			sb.Append(DEBUG);
			sb.Append("if (hideme != null) {hideme.style.display = 'none';}");
			sb.Append(DEBUG);
			sb.Append("args[\"NumRows\"] = " + _FormName + "." + _ClientID + "_ObjTableInfo.NumRows;");
			sb.Append(DEBUG);
			sb.Append("args[\"NumCols\"] = " + _FormName + "." + _ClientID + "_ObjTableInfo.NumCols;");
			sb.Append(DEBUG);
			sb.Append("args[\"TableAttrs\"] = " + _FormName + "." + _ClientID + "_ObjTableInfo.TableAttrs;");
			sb.Append(DEBUG);
			sb.Append("args[\"CellAttrs\"] = " + _FormName + "." + _ClientID + "_ObjTableInfo.CellAttrs;");
			sb.Append(DEBUG);
			sb.Append("args[\"Caption\"] = " + _FormName + "." + _ClientID + "_ObjTableInfo.Caption;");
			sb.Append(DEBUG);
			sb.Append("arr = null;");
			sb.Append(DEBUG);
			sb.Append("arr = showModalDialog(\"" + _DialogPath + "/insert_table.aspx\",args,\"font-family:Verdana; font-size:10; dialogWidth:310px; dialogHeight:263px; center:yes; scroll:no; help:no; edge: Sunken; status: no;\");");
			sb.Append(DEBUG);
			sb.Append("if (arr != null) {");
			sb.Append(DEBUG);
			sb.Append("for ( elem in arr ) {");
			sb.Append(DEBUG);
			sb.Append("if (\"NumRows\" == elem && arr[\"NumRows\"] != null) {");
			sb.Append(DEBUG);
			sb.Append("NumRows = arr[\"NumRows\"];");
			sb.Append(DEBUG);
			sb.Append("} else if (\"NumCols\" == elem && arr[\"NumCols\"] != null) {");
			sb.Append(DEBUG);
			sb.Append("NumCols = arr[\"NumCols\"];");
			sb.Append(DEBUG);
			sb.Append("} else if (\"TableAttrs\" == elem) {");
			sb.Append(DEBUG);
			sb.Append("TableAttrs = arr[\"TableAttrs\"];");
			sb.Append(DEBUG);
			sb.Append("} else if (\"CellAttrs\" == elem) {");
			sb.Append(DEBUG);
			sb.Append("CellAttrs = arr[\"CellAttrs\"];");
			sb.Append(DEBUG);
			sb.Append("} else if (\"Caption\" == elem) {");
			sb.Append(DEBUG);
			sb.Append("Caption = arr[\"Caption\"];");
			sb.Append(DEBUG);
			sb.Append("}}");
			sb.Append(DEBUG);
			sb.Append("var strTable = \"<table \" + TableAttrs + \">\";");
			sb.Append(DEBUG);
			sb.Append("for (i=0; i < NumRows; i++) {");
			sb.Append(DEBUG);
			sb.Append("strTable = strTable + \" <tr> \";");
			sb.Append(DEBUG);
			sb.Append("for (j=0; j < NumCols; j++) {strTable = strTable + \"<td \" + CellAttrs + \"></td>\";}");
			sb.Append(DEBUG);
			sb.Append("strTable = strTable + \"</tr>\";");
			sb.Append(DEBUG);
			sb.Append("}");
			sb.Append(DEBUG);
			sb.Append("strTable = strTable + \"</table>\";");
			sb.Append(DEBUG);
			sb.Append(_RTBName + ".focus();");
			sb.Append(DEBUG);
			sb.Append("var r = " + _RTBName + ".document.selection.createRange();");
			sb.Append(DEBUG);
			sb.Append("r.pasteHTML(strTable);");
			sb.Append(DEBUG);
			sb.Append("if(" + _ClientID + "_RTBGuideLineStatus) {" + _ClientID + "_RTBGuideLineStatus = false;} else {" + _ClientID + "_RTBGuideLineStatus = true;}");
			sb.Append(DEBUG);
			sb.Append(_ClientID + "_toggleGuidelines();");
			sb.Append(DEBUG);
			sb.Append("}}");
			sb.Append(DEBUG);

			sb.Append("function " + _ClientID + "_table_insert_row(editor)");
			sb.Append(DEBUG);
			sb.Append("{");
			sb.Append(DEBUG);
			sb.Append(_RTBName + ".focus();");
			sb.Append(DEBUG);
			sb.Append("var ct = " + _ClientID + "_getTable(editor);");
			sb.Append(DEBUG);
			sb.Append("var cr = " + _ClientID + "_getTR(editor);");
			sb.Append(DEBUG);
			sb.Append("if (ct && cr){");
			sb.Append(DEBUG);
			sb.Append("var newr = ct.insertRow(cr.rowIndex+1);");
			sb.Append(DEBUG);
			sb.Append("for (i=0; i<cr.cells.length; i++)");
			sb.Append(DEBUG);
			sb.Append("{if (cr.cells(i).rowSpan > 1)");
			sb.Append(DEBUG);
			sb.Append("{cr.cells(i).rowSpan++;}");
			sb.Append(DEBUG);
			sb.Append("else");
			sb.Append(DEBUG);
			sb.Append("{var newc = cr.cells(i).cloneNode(); newr.appendChild(newc);}");
			sb.Append(DEBUG);
			sb.Append("}");
			sb.Append(DEBUG);
			sb.Append("for (i=0; i<cr.rowIndex; i++)");
			sb.Append(DEBUG);
			sb.Append("{var tempr = ct.rows(i);");
			sb.Append(DEBUG);
			sb.Append("for (j=0; j<tempr.cells.length; j++)");
			sb.Append(DEBUG);
			sb.Append("{if (tempr.cells(j).rowSpan > (cr.rowIndex - i)) {tempr.cells(j).rowSpan++;}");
			sb.Append(DEBUG);
			sb.Append("}}");
			sb.Append(DEBUG);
			sb.Append("}");
			sb.Append(DEBUG);
			sb.Append("var tControl = window[editor].document.selection.createRange();");
			sb.Append(DEBUG);
			sb.Append("tControl.select();");
			sb.Append(DEBUG);
			sb.Append("}");
			sb.Append(DEBUG);

			sb.Append("function " + _ClientID + "_table_delete_row(editor)");
			sb.Append(DEBUG);
			sb.Append("{");
			sb.Append(DEBUG);
			sb.Append(_RTBName + ".focus();");
			sb.Append(DEBUG);
			sb.Append("var ct = " + _ClientID + "_getTable(editor);");
			sb.Append(DEBUG);
			sb.Append("var cr = " + _ClientID + "_getTR(editor);");
			sb.Append(DEBUG);
			sb.Append("var cd = " + _ClientID + "_getTD(editor);");
			sb.Append(DEBUG);
			sb.Append("if (cd && cr && ct)");
			sb.Append(DEBUG);
			sb.Append("{if (ct.rows.length<=1) {ct.removeNode(true);}");
			sb.Append(DEBUG);
			sb.Append("else {");
			sb.Append(DEBUG);
			sb.Append("var tm = " + _ClientID + "_formCellMatrix(ct);");
			sb.Append(DEBUG);
			sb.Append("for (i=0; i<cr.rowIndex; i++)");
			sb.Append(DEBUG);
			sb.Append("{var tempr = ct.rows(i);");
			sb.Append(DEBUG);
			sb.Append("for (j=0; j<tempr.cells.length; j++)");
			sb.Append(DEBUG);
			sb.Append("{if (tempr.cells(j).rowSpan > (cr.rowIndex - i))");
			sb.Append(DEBUG);
			sb.Append("{tempr.cells(j).rowSpan--;}}}");
			sb.Append(DEBUG);
			sb.Append("curCI = -1;");
			sb.Append(DEBUG);
			sb.Append("for (i=0; i<tm[cr.rowIndex].length; i++)");
			sb.Append(DEBUG);
			sb.Append("{prevCI = curCI;");
			sb.Append(DEBUG);
			sb.Append("curCI = tm[cr.rowIndex][i];");
			sb.Append(DEBUG);
			sb.Append("if (curCI != -1 && curCI != prevCI && cr.cells(curCI).rowSpan>1 && (cr.rowIndex+1)<ct.rows.length)");
			sb.Append(DEBUG);
			sb.Append("{ni = i;");
			sb.Append(DEBUG);
			sb.Append("nrCI = tm[cr.rowIndex+1][ni];");
			sb.Append(DEBUG);
			sb.Append("while (nrCI == -1)");
			sb.Append(DEBUG);
			sb.Append("{ni++;");
			sb.Append(DEBUG);
			sb.Append("if (ni<ct.rows(cr.rowIndex+1).cells.length) {nrCI = tm[cr.rowIndex+1][ni];}");
			sb.Append(DEBUG);
			sb.Append("else {nrCI = ct.rows(cr.rowIndex+1).cells.length;}}");
			sb.Append(DEBUG);
			sb.Append("var newc = ct.rows(cr.rowIndex+1).insertCell(nrCI);");
			sb.Append(DEBUG);
			sb.Append("ct.rows(cr.rowIndex).cells(curCI).rowSpan--;");
			sb.Append(DEBUG);
			sb.Append("var nc = ct.rows(cr.rowIndex).cells(curCI).cloneNode();");
			sb.Append(DEBUG);
			sb.Append("newc.replaceNode(nc);");
			sb.Append(DEBUG);
			sb.Append("cs = (cr.cells(curCI).colSpan>1)?cr.cells(curCI).colSpan:1;");
			sb.Append(DEBUG);
			sb.Append("for (j=i; j<(i+cs);j++)");
			sb.Append(DEBUG);
			sb.Append("{tm[cr.rowIndex+1][j] = nrCI; nj = j;}");
			sb.Append(DEBUG);
			sb.Append("for (j=nj; j<tm[cr.rowIndex+1].length; j++)");
			sb.Append(DEBUG);
			sb.Append("{if (tm[cr.rowIndex+1][j] != -1) {tm[cr.rowIndex+1][j]++;}");
			sb.Append(DEBUG);
			sb.Append("}}}");
			sb.Append(DEBUG);
			sb.Append("ct.deleteRow(cr.rowIndex);");
			sb.Append(DEBUG);
			sb.Append("}}");
			sb.Append(DEBUG);
			sb.Append("var tControl = window[editor].document.selection.createRange();");
			sb.Append(DEBUG);
			sb.Append("tControl.select();");
			sb.Append(DEBUG);
			sb.Append("}");
			sb.Append(DEBUG);	
			sb.Append("function " + _ClientID + "_table_insert_column(editor)");
			sb.Append(DEBUG);
			sb.Append("{");
			sb.Append(DEBUG);
			sb.Append(_RTBName + ".focus();");
			sb.Append(DEBUG);
			sb.Append("var ct = " + _ClientID + "_getTable(editor);");
			sb.Append(DEBUG);
			sb.Append("var cr = " + _ClientID + "_getTR(editor);");
			sb.Append(DEBUG);
			sb.Append("var cd = " + _ClientID + "_getTD(editor);");
			sb.Append(DEBUG);
			sb.Append("if (cd && cr && ct)");
			sb.Append(DEBUG);
			sb.Append("{var tm = " + _ClientID + "_formCellMatrix(ct);");
			sb.Append(DEBUG);
			sb.Append("for (j=0; j<tm[cr.rowIndex].length; j++)");
			sb.Append(DEBUG);
			sb.Append("{if (tm[cr.rowIndex][j] == cd.cellIndex)");
			sb.Append(DEBUG);
			sb.Append("{realIndex=j; break;}}");
			sb.Append(DEBUG);
			sb.Append("for (i=0; i<ct.rows.length; i++)");
			sb.Append(DEBUG);
			sb.Append("{if (tm[i][realIndex] != -1)");
			sb.Append(DEBUG);
			sb.Append("{if (ct.rows(i).cells(tm[i][realIndex]).colSpan > 1)");
			sb.Append(DEBUG);
			sb.Append("{ct.rows(i).cells(tm[i][realIndex]).colSpan++;}");
			sb.Append(DEBUG);
			sb.Append("else {");
			sb.Append(DEBUG);
			sb.Append("var newc = ct.rows(i).insertCell(tm[i][realIndex]+1);");
			sb.Append(DEBUG);
			sb.Append("var nc = ct.rows(i).cells(tm[i][realIndex]).cloneNode();");
			sb.Append(DEBUG);
			sb.Append("newc.replaceNode(nc);");
			sb.Append(DEBUG);
			sb.Append("}}}}");
			sb.Append(DEBUG);
			sb.Append("var tControl = window[editor].document.selection.createRange();");
			sb.Append(DEBUG);
			sb.Append("tControl.select();");
			sb.Append(DEBUG);
			sb.Append("}");
			sb.Append(DEBUG);

			sb.Append("function " + _ClientID + "_table_delete_column(editor)");
			sb.Append(DEBUG);
			sb.Append("{");
			sb.Append(DEBUG);
			sb.Append(_RTBName + ".focus();");
			sb.Append(DEBUG);
			sb.Append("var ct = " + _ClientID + "_getTable(editor);");
			sb.Append(DEBUG);
			sb.Append("var cr = " + _ClientID + "_getTR(editor);");
			sb.Append(DEBUG);
			sb.Append("var cd = " + _ClientID + "_getTD(editor);");
			sb.Append(DEBUG);
			sb.Append("if (cd && cr && ct) {");
			sb.Append(DEBUG);
			sb.Append("var tm = " + _ClientID + "_formCellMatrix(ct);");
			sb.Append(DEBUG);
			sb.Append("if (tm[0].length<=1)");
			sb.Append(DEBUG);
			sb.Append("{ct.removeNode(true);}");
			sb.Append(DEBUG);
			sb.Append("else");
			sb.Append(DEBUG);
			sb.Append("{for (j=0; j<tm[cr.rowIndex].length; j++)");
			sb.Append(DEBUG);
			sb.Append("{if (tm[cr.rowIndex][j] == cd.cellIndex)");
			sb.Append(DEBUG);
			sb.Append("{realIndex=j; break;}}");
			sb.Append(DEBUG);
			sb.Append("for (i=0; i<ct.rows.length; i++)");
			sb.Append(DEBUG);
			sb.Append("{if (tm[i][realIndex] != -1)");
			sb.Append(DEBUG);
			sb.Append("{if (ct.rows(i).cells(tm[i][realIndex]).colSpan>1) {ct.rows(i).cells(tm[i][realIndex]).colSpan--;}");
			sb.Append(DEBUG);
			sb.Append("else {ct.rows(i).deleteCell(tm[i][realIndex]);}");
			sb.Append(DEBUG);
			sb.Append("}}}}");
			sb.Append(DEBUG);
			sb.Append("var tControl = window[editor].document.selection.createRange();");
			sb.Append(DEBUG);
			sb.Append("tControl.select();");
			sb.Append(DEBUG);
			sb.Append("}");
			sb.Append(DEBUG);

			sb.Append("function " + _ClientID + "_getTable(editor)");
			sb.Append(DEBUG);
			sb.Append("{if (window[editor].document.selection.type == \"Control\") {");
			sb.Append(DEBUG);
			sb.Append("var tControl = window[editor].document.selection.createRange();");
			sb.Append(DEBUG);
			sb.Append("if (tControl(0).tagName == 'TABLE') {return(tControl(0));}");
			sb.Append(DEBUG);
			sb.Append("else{return(null);}");
			sb.Append(DEBUG);
			sb.Append("} else {");
			sb.Append(DEBUG);
			sb.Append("var tControl = window.frames[editor].document.selection.createRange();");
			sb.Append(DEBUG);
			sb.Append("tControl = tControl.parentElement();");
			sb.Append(DEBUG);
			sb.Append("while ((tControl.tagName != 'TABLE') && (tControl.tagName != 'BODY') && (tControl.tagName != 'FORM'))");
			sb.Append(DEBUG);
			sb.Append("{tControl = tControl.parentElement;}");
			sb.Append(DEBUG);
			sb.Append("if (tControl.tagName == 'TABLE'){return(tControl);}");
			sb.Append(DEBUG);
			sb.Append("else {return(null);}");
			sb.Append(DEBUG);
			sb.Append("}");
			sb.Append(DEBUG);
			sb.Append("}");
			sb.Append(DEBUG);

			sb.Append("function " + _ClientID + "_getTR(editor)");
			sb.Append(DEBUG);
			sb.Append("{");
			sb.Append(DEBUG);
			sb.Append("if (window[editor].document.selection.type != \"Control\")");
			sb.Append(DEBUG);
			sb.Append("{");
			sb.Append(DEBUG);
			sb.Append("var tControl = window[editor].document.selection.createRange();");
			sb.Append(DEBUG);
			sb.Append("tControl = tControl.parentElement();");
			sb.Append(DEBUG);
			sb.Append("while ((tControl.tagName != 'TR') && (tControl.tagName != 'TABLE') && (tControl.tagName != 'BODY'))");
			sb.Append(DEBUG);
			sb.Append("{tControl = tControl.parentElement;}");
			sb.Append(DEBUG);
			sb.Append("if (tControl.tagName.toUpperCase() == 'TR')");
			sb.Append(DEBUG);
			sb.Append("{return(tControl);}");
			sb.Append(DEBUG);
			sb.Append("else");
			sb.Append(DEBUG);
			sb.Append("{return(null);}");
			sb.Append(DEBUG);
			sb.Append("}");
			sb.Append(DEBUG);
			sb.Append("else");
			sb.Append(DEBUG);
			sb.Append("{return(null);}");
			sb.Append(DEBUG);
			sb.Append("}");
			sb.Append(DEBUG);

			sb.Append("function " + _ClientID + "_getTD(editor) {");
			sb.Append(DEBUG);
			sb.Append("if (window[editor].document.selection.type != \"Control\") {");
			sb.Append(DEBUG);
			sb.Append("var tControl = window[editor].document.selection.createRange();");
			sb.Append(DEBUG);
			sb.Append("tControl = tControl.parentElement();");
			sb.Append(DEBUG);
			sb.Append("while ((tControl.tagName != 'TD') && (tControl.tagName != 'TH') && (tControl.tagName != 'TABLE') && (tControl.tagName != 'BODY')) {tControl = tControl.parentElement;}");
			sb.Append(DEBUG);
			sb.Append("if ((tControl.tagName.toUpperCase() == 'TD') || (tControl.tagName.toUpperCase() == 'TH'))");
			sb.Append(DEBUG);
			sb.Append("{return(tControl);}");
			sb.Append(DEBUG);
			sb.Append("else");
			sb.Append(DEBUG);
			sb.Append("{return(null);}");
			sb.Append(DEBUG);
			sb.Append("}");
			sb.Append(DEBUG);
			sb.Append("else");
			sb.Append(DEBUG);
			sb.Append("{return(null);}");
			sb.Append(DEBUG);
			sb.Append("}");
			sb.Append(DEBUG);

			sb.Append("function " + _ClientID + "_formCellMatrix(ct)");
			sb.Append(DEBUG);
			sb.Append("{");
			sb.Append(DEBUG);
			sb.Append("var tm = new Array();");
			sb.Append(DEBUG);
			sb.Append("for (i=0; i<ct.rows.length; i++) {tm[i]=new Array();}");
			sb.Append(DEBUG);
			sb.Append("for (i=0; i<ct.rows.length; i++)");
			sb.Append(DEBUG);
			sb.Append("{jr=0; ");
			sb.Append(DEBUG);
			sb.Append("for (j=0; j<ct.rows(i).cells.length;j++) {");
			sb.Append(DEBUG);
			sb.Append("while (tm[i][jr] != undefined) {jr++;}");
			sb.Append(DEBUG);
			sb.Append("for (jh=jr; jh<jr+(ct.rows(i).cells(j).colSpan?ct.rows(i).cells(j).colSpan:1);jh++) {");
			sb.Append(DEBUG);
			sb.Append("for (jv=i; jv<i+(ct.rows(i).cells(j).rowSpan?ct.rows(i).cells(j).rowSpan:1);jv++)");
			sb.Append(DEBUG);
			sb.Append("{if (jv==i){tm[jv][jh]=ct.rows(i).cells(j).cellIndex;}");
			sb.Append(DEBUG);
			sb.Append("else {tm[jv][jh]=-1;}}");
			sb.Append(DEBUG);
			sb.Append("}}}");
			sb.Append(DEBUG);
			sb.Append("return(tm);");
			sb.Append(DEBUG);
			sb.Append("}");
			sb.Append(DEBUG);

			sb.Append("function " + _ClientID + "_addColor(r, g, b) {");
			sb.Append(DEBUG);
			sb.Append("var red = " + _ClientID + "_colorLevels[r];");
			sb.Append(DEBUG);
			sb.Append("var green = " + _ClientID + "_colorLevels[g];");
			sb.Append(DEBUG);
			sb.Append("var blue = " + _ClientID + "_colorLevels[b];");
			sb.Append(DEBUG);
			sb.Append(_ClientID + "_addColorValue(red, green, blue);");
			sb.Append(DEBUG);
			sb.Append("}");
			sb.Append(DEBUG);

			sb.Append("function " + _ClientID + "_addColorValue(r, g, b) {");
			sb.Append(DEBUG);
			sb.Append(_ClientID + "_colorArray[" + _ClientID + "_colorArray.length] = '#' + r + r + g + g + b + b;");
			sb.Append(DEBUG);
			sb.Append("}");
			sb.Append(DEBUG);

			sb.Append("function " + _ClientID + "_genColors() {");
			sb.Append(DEBUG);
			sb.Append(_ClientID + "_addColorValue('1','1','1');");
			sb.Append(DEBUG);
			sb.Append(_ClientID + "_addColorValue('3','3','3');");
			sb.Append(DEBUG);
			sb.Append(_ClientID + "_addColorValue('5','5','5');");
			sb.Append(DEBUG);
			sb.Append(_ClientID + "_addColorValue('7','7','7');");
			sb.Append(DEBUG);
			sb.Append(_ClientID + "_addColorValue('9','9','9');");
			sb.Append(DEBUG);
			sb.Append(_ClientID + "_addColorValue('B','B','B');");
			sb.Append(DEBUG);
			sb.Append(_ClientID + "_addColorValue('D','D','D');");
			sb.Append(DEBUG);
			sb.Append(_ClientID + "_addColorValue('F','F','F');");
			sb.Append(DEBUG);
			sb.Append("for (a = 1; a < " + _ClientID + "_colorLevels.length; a++)");
			sb.Append(DEBUG);
			sb.Append(_ClientID + "_addColor(6,6,a);");
			sb.Append(DEBUG);
			sb.Append("for (a = 1; a < " + _ClientID + "_colorLevels.length - 1; a++)");
			sb.Append(DEBUG);
			sb.Append(_ClientID + "_addColor(6,a,6);");
			sb.Append(DEBUG);
			sb.Append("for (a = 1; a < " + _ClientID + "_colorLevels.length; a++)");
			sb.Append(DEBUG);
			sb.Append(_ClientID + "_addColor(a,6,6);");
			sb.Append(DEBUG);
			sb.Append("for (a = 1; a < " + _ClientID + "_colorLevels.length - 1; a++)");
			sb.Append(DEBUG);
			sb.Append(_ClientID + "_addColor(a,a,6);");
			sb.Append(DEBUG);
			sb.Append("for (a = 1; a < " + _ClientID + "_colorLevels.length; a++)");
			sb.Append(DEBUG);
			sb.Append(_ClientID + "_addColor(a,6,a);");
			sb.Append(DEBUG);
			sb.Append("for (a = 1; a < " + _ClientID + "_colorLevels.length - 1; a++)");
			sb.Append(DEBUG);
			sb.Append(_ClientID + "_addColor(6,a,a);");
			sb.Append(DEBUG);
			sb.Append("for (a = 1; a < " + _ClientID + "_colorLevels.length - 1; a++)");
			sb.Append(DEBUG);
			sb.Append(_ClientID + "_addColor(5,5,a);");
			sb.Append(DEBUG);
			sb.Append("for (a = 1; a < " + _ClientID + "_colorLevels.length; a++)");
			sb.Append(DEBUG);
			sb.Append(_ClientID + "_addColor(5,a,5);");
			sb.Append(DEBUG);
			sb.Append("for (a = 1; a < " + _ClientID + "_colorLevels.length; a++)");
			sb.Append(DEBUG);
			sb.Append(_ClientID + "_addColor(a,5,5);");
			sb.Append(DEBUG);
			sb.Append("for (a = 1; a < " + _ClientID + "_colorLevels.length; a++)");
			sb.Append(DEBUG);
			sb.Append(_ClientID + "_addColor(a,a,5);");
			sb.Append(DEBUG);
			sb.Append("for (a = 1; a < " + _ClientID + "_colorLevels.length; a++)");
			sb.Append(DEBUG);
			sb.Append(_ClientID + "_addColor(a,5,a);");
			sb.Append(DEBUG);
			sb.Append("for (a = 1; a < " + _ClientID + "_colorLevels.length; a++)");
			sb.Append(DEBUG);
			sb.Append(_ClientID + "_addColor(5,a,a);");
			sb.Append(DEBUG);
			sb.Append("for (a = 1; a < " + _ClientID + "_colorLevels.length; a++)");
			sb.Append(DEBUG);
			sb.Append(_ClientID + "_addColor(0,0,a);");
			sb.Append(DEBUG);
			sb.Append("for (a = 1; a < " + _ClientID + "_colorLevels.length; a++)");
			sb.Append(DEBUG);
			sb.Append(_ClientID + "_addColor(0,a,0);");
			sb.Append(DEBUG);
			sb.Append("for (a = 1; a < " + _ClientID + "_colorLevels.length; a++)");
			sb.Append(DEBUG);
			sb.Append(_ClientID + "_addColor(a,0,0);");
			sb.Append(DEBUG);
			sb.Append("for (a = 1; a < " + _ClientID + "_colorLevels.length; a++)");
			sb.Append(DEBUG);
			sb.Append(_ClientID + "_addColor(a,a,0);");
			sb.Append(DEBUG);
			sb.Append("for (a = 1; a < " + _ClientID + "_colorLevels.length; a++)");
			sb.Append(DEBUG);
			sb.Append(_ClientID + "_addColor(a,0,a);");
			sb.Append(DEBUG);
			sb.Append("for (a = 1; a < " + _ClientID + "_colorLevels.length; a++)");
			sb.Append(DEBUG);
			sb.Append(_ClientID + "_addColor(0,a,a);");
			sb.Append(DEBUG);
			sb.Append("return " + _ClientID + "_colorArray;");
			sb.Append(DEBUG);
			sb.Append("}");
			sb.Append(DEBUG);

			sb.Append("function " + _ClientID + "_getAbsoluteOffsetTop(obj) {");
			sb.Append(DEBUG);
			sb.Append("var top = obj.offsetTop;");
			sb.Append(DEBUG);
			sb.Append("var parent = obj.offsetParent;");
			sb.Append(DEBUG);
			sb.Append("while (parent != document.body) {");
			sb.Append(DEBUG);
			sb.Append("top += parent.offsetTop;");
			sb.Append(DEBUG);
			sb.Append("parent = parent.offsetParent;");
			sb.Append(DEBUG);
			sb.Append("}");
			sb.Append(DEBUG);
			sb.Append("return top;");
			sb.Append(DEBUG);
			sb.Append("}");

			sb.Append("function " + _ClientID + "_getAbsoluteOffsetLeft(obj) {");
			sb.Append(DEBUG);
			sb.Append("var left = obj.offsetLeft;");
			sb.Append(DEBUG);
			sb.Append("var parent = obj.offsetParent;");
			sb.Append(DEBUG);
			sb.Append("while (parent != document.body) {");
			sb.Append(DEBUG);
			sb.Append("left += parent.offsetLeft;");
			sb.Append(DEBUG);
			sb.Append("parent = parent.offsetParent;");
			sb.Append(DEBUG);
			sb.Append("}");
			sb.Append(DEBUG);
			sb.Append("return left;");
			sb.Append(DEBUG);
			sb.Append("}");
			sb.Append(DEBUG);

			sb.Append("</script>");

			return sb.ToString();
		}


		public string ToggleButtons()
		{
			StringBuilder sb = new StringBuilder();

			sb.Append("<script language=\"JavaScript1.2\" TYPE=\"text/javascript\">");

			sb.Append("function " + _ClientID + "_disableButtons() {");
			sb.Append(DEBUG);
			sb.Append("allBUTs = " + _ClientID + "_toolbar.children;");
			sb.Append(DEBUG);
			sb.Append("for (i=0;i<allBUTs.length;i++) {");
			sb.Append(DEBUG);
			sb.Append("tSpan = allBUTs(i);");
			sb.Append(DEBUG);
			sb.Append("if (tSpan.className == \"" + _ClientID + "_MenuButton\") {");
			sb.Append(DEBUG);
			sb.Append("tSpan.style.filter = 'alpha(Opacity=20)';}");
			sb.Append(DEBUG);
			sb.Append("}");
			sb.Append(DEBUG);
			sb.Append("}");
			sb.Append(DEBUG);

			sb.Append("function " + _ClientID + "_enableButtons() {");
			sb.Append(DEBUG);
			sb.Append("allBUTs = " + _ClientID + "_toolbar.children;");
			sb.Append(DEBUG);
			sb.Append("for (i=0;i<allBUTs.length;i++) {");
			sb.Append(DEBUG);
			sb.Append("tSpan = allBUTs(i);");
			sb.Append(DEBUG);
			sb.Append("if (tSpan.className == \"" + _ClientID + "_MenuButton\") {");
			sb.Append(DEBUG);
			sb.Append("tSpan.style.filter = 'alpha(Opacity=100)';}");
			sb.Append(DEBUG);
			sb.Append("}");
			sb.Append(DEBUG);
			sb.Append("}");
			sb.Append(DEBUG);
			sb.Append("</script>");
			sb.Append(DEBUG);

			return sb.ToString();
		}

		public string RegisterButtonMouseOver(string mnuObj)
		{
			StringBuilder sb = new StringBuilder();

			sb.Append("<script language=\"JavaScript1.2\" TYPE=\"text/javascript\">");

			sb.Append("allBUTs = " + _ClientID + "_toolbar.children;");
			sb.Append(DEBUG);
			sb.Append("for (i=0;i < allBUTs.length;i++) {");
			sb.Append(DEBUG);
			sb.Append("tSpan = allBUTs(i);");
			sb.Append(DEBUG);
			sb.Append("if (tSpan.className == \"" + _ClientID + "_MenuButton\") {");
			sb.Append(DEBUG);
			sb.Append("tSpan.onselectstart = function() {this.style.border = \"1px " + System.Drawing.ColorTranslator.ToHtml(_ToolbarColor) + " solid\"; return false;}");
			sb.Append(DEBUG);
			sb.Append("tSpan.onmouseover = function() {");
			sb.Append("this.style.border = \"1px " + System.Drawing.ColorTranslator.ToHtml(_ToolbarItemOverBorderColor) + " solid\";");
			sb.Append(DEBUG);
			sb.Append("this.style.background = \"" + System.Drawing.ColorTranslator.ToHtml(_ToolbarItemOverColor) + "\";");
			sb.Append(DEBUG);
			sb.Append("}");
			sb.Append(DEBUG);
			sb.Append("tSpan.onmouseout = function() {");
			sb.Append("this.style.border = \"1px " + System.Drawing.ColorTranslator.ToHtml(_ToolbarColor) + " solid\";");
			sb.Append(DEBUG);
			sb.Append("this.style.background = \"" + System.Drawing.ColorTranslator.ToHtml(_ToolbarColor) + "\";");
			sb.Append(DEBUG);
			sb.Append("}");
			sb.Append(DEBUG);
			sb.Append("tSpan.onmousedown = function() {");
			sb.Append(DEBUG);
			sb.Append("this.style.border = \"1px " + System.Drawing.ColorTranslator.ToHtml(_ToolbarItemOverBorderColor) + " inset\";");
			sb.Append(DEBUG);
			sb.Append("this.style.background = \"" + System.Drawing.ColorTranslator.ToHtml(_ToolbarItemOverColor) + "\";");
			sb.Append(DEBUG);
			sb.Append("}");
			sb.Append(DEBUG);
			sb.Append("tSpan.onmouseup = function() {");
			sb.Append(DEBUG);
			sb.Append("this.style.border = \"1px " + System.Drawing.ColorTranslator.ToHtml(_ToolbarColor) + " solid\";");
			sb.Append(DEBUG);
			sb.Append("this.style.background = \"" + System.Drawing.ColorTranslator.ToHtml(_ToolbarColor) + "\";");
			sb.Append(DEBUG);
			sb.Append("window.focus();}");
			sb.Append(DEBUG);
			sb.Append("}}");
			sb.Append(DEBUG);
			sb.Append("</script>");
			sb.Append(DEBUG);

			return sb.ToString();
		}
	}
}
