using System;
using System.Text;

using Matchnet.Lib;
using Matchnet.Lib.Util;

namespace Matchnet.Web.Controls.RichTextBox
{
	public class TableMenu
	{
		private RichTextEdit _RichTextBox;
		private System.Drawing.Color _ToolbarColor;
		private System.Drawing.Color _ToolbarItemOverColor;
		private System.Drawing.Color _ToolbarItemOverBorderColor;
		private string _ClientID = Matchnet.Constants.NULL_STRING;
		private Matchnet.Web.Framework.Image _image;
		private const string DEBUG = "\r\n";

		public TableMenu(RichTextEdit parent)
		{
			_RichTextBox = parent;
			_ToolbarColor = parent.ToolbarColor;
			_ToolbarItemOverColor = parent.ToolbarItemOver;
			_ToolbarItemOverBorderColor = parent.ToolbarItemOverBorder;
			_ClientID = _RichTextBox.TidyObjectRef(_RichTextBox.ClientID);
			_image = new Matchnet.Web.Framework.Image();
		}


		public string CreateTableMenuCode()
		{

			string pickerObj = _ClientID + "_TableMenu";
			string rTBName = _ClientID + "_RTB";
			StringBuilder sb = new StringBuilder();
			
			sb.Append("<script language=\"Javascript\">");

			sb.Append("var " + pickerObj + "_TableDivSet = false; var " + pickerObj + "_TableCurID;");
			sb.Append(DEBUG);
			sb.Append("var " + pickerObj + "_TblMnuOpt = new Array();");
			sb.Append(DEBUG);
			sb.Append(pickerObj + "_TblMnuOpt[0] = new " + pickerObj + "_MnuItem(\"Instant Table\", \"javascript:" + _ClientID + "_insertTable();\", \"" +   _image.GetUrl("tblmnu_table.gif", Constants.APP_FREETEXTAPPROVAL, false) + "\");");
			sb.Append(DEBUG);
			sb.Append(pickerObj + "_TblMnuOpt[1] = new " + pickerObj + "_MnuItem();");
			sb.Append(DEBUG);
			sb.Append(pickerObj + "_TblMnuOpt[2] = new " + pickerObj + "_MnuItem(\"Insert Table Row\", \"javascript:" + _ClientID + "_table_insert_row('" + rTBName + "');\", \"" +   _image.GetUrl("tblmnu_insertrow.gif", Constants.APP_FREETEXTAPPROVAL, false) + "\");");
			sb.Append(DEBUG);
			sb.Append(pickerObj + "_TblMnuOpt[3] = new " + pickerObj + "_MnuItem(\"Insert Table Column\", \"javascript:" + _ClientID + "_table_insert_column('" + rTBName + "');\", \"" +  _image.GetUrl("tblmnu_insertcol.gif", Constants.APP_FREETEXTAPPROVAL, false) + "\");");
			sb.Append(DEBUG);
			sb.Append(pickerObj + "_TblMnuOpt[4] = new " + pickerObj + "_MnuItem();");
			sb.Append(DEBUG);
			sb.Append(pickerObj + "_TblMnuOpt[5] = new " + pickerObj + "_MnuItem(\"Delete Table Row\", \"javascript:" + _ClientID + "_table_delete_row('" + rTBName + "');\", \"" +   _image.GetUrl("tblmnu_deleterow.gif", Constants.APP_FREETEXTAPPROVAL, false) + "\");");
			sb.Append(DEBUG);
			sb.Append(pickerObj + "_TblMnuOpt[6] = new " + pickerObj + "_MnuItem(\"Delete Table Column\", \"javascript:" + _ClientID + "_table_delete_column('" + rTBName + "');\", \"" +   _image.GetUrl("tblmnu_deletecol.gif", Constants.APP_FREETEXTAPPROVAL, false) + "\");");
			sb.Append(DEBUG);

			sb.Append("function " + pickerObj + "_setTableDiv() {");
			sb.Append(DEBUG);
			sb.Append("if(!document.createElement) { return; }");
			sb.Append(DEBUG);
			sb.Append(" var elemDiv = document.createElement('div');");
			sb.Append(DEBUG);
			sb.Append("if (typeof(elemDiv.innerHTML) != 'string') { return; }");
			sb.Append(DEBUG);
			sb.Append("elemDiv.id = '" + pickerObj + "_tableMenu';");
			sb.Append(DEBUG);
			sb.Append("elemDiv.style.position = 'absolute';");
			sb.Append(DEBUG);
			sb.Append("elemDiv.style.display = 'none';");
			sb.Append(DEBUG);
			sb.Append("elemDiv.style.width = '188px';");
			sb.Append(DEBUG);
			sb.Append("elemDiv.style.border = '1 px " + System.Drawing.ColorTranslator.ToHtml(_ToolbarColor) + " outset';");
			sb.Append(DEBUG);
			sb.Append("elemDiv.style.background = 'white';");
			sb.Append(DEBUG);
			sb.Append("elemDiv.style.filter = 'progid:DXImageTransform.Microsoft.Shadow(color=\"#888888\", Direction=120, Strength=3); alpha(Opacity=100);';");
			sb.Append(DEBUG);
			sb.Append("elemDiv.innerHTML = " + pickerObj + "_getTableMenu();");
			sb.Append(DEBUG);
			sb.Append("document.body.appendChild(elemDiv);");
			sb.Append(DEBUG);
			sb.Append(pickerObj + "_TableDivSet = true;");
			sb.Append(DEBUG);
			sb.Append("}");
			sb.Append(DEBUG);

			sb.Append("function " + pickerObj + "_MnuItem(text, command, image) {");
			sb.Append(DEBUG);
			sb.Append("this.text = text;");
			sb.Append(DEBUG);
			sb.Append("this.command = command;");
			sb.Append(DEBUG);
			sb.Append("this.image = image;");
			sb.Append(DEBUG);
			sb.Append("this.seperator = '<tr><td colspan=\"2\" style=\"background: graytext;\"></td></tr><tr><td colspan=\"2\" style=\"background: white;\"></td></tr>';");
			sb.Append(DEBUG);
			sb.Append("}");
			sb.Append(DEBUG);

			sb.Append("function " + pickerObj + "_getTableMenu() {");
			sb.Append(DEBUG);
			sb.Append("var tableCode = '';");
			sb.Append(DEBUG);
			sb.Append("tableCode += '<table border=\"0\" cellspacing=\"1\" width=\"100%\" cellpadding=\"0\">';");
			sb.Append(DEBUG);
			sb.Append("tableCode += '<tr><td style=\"font-family:verdana; font-size: 8pt;\">';");
			sb.Append(DEBUG);
			sb.Append("tableCode += '<table width=\"100%\" cellspacing=\"0\" cellpadding=\"2\">';");
			sb.Append(DEBUG);
			sb.Append("for (i = 0; i < " + pickerObj + "_TblMnuOpt.length; i++); {");
			sb.Append(DEBUG);
			sb.Append("if (i == 1 || i == 4)");
			sb.Append(DEBUG);
			sb.Append("{tableCode += " + pickerObj + "_TblMnuOpt[i].seperator;");
			sb.Append(DEBUG);
			sb.Append("}else{");
			sb.Append(DEBUG);
			sb.Append("tableCode += '<tr><td align=\"center\" style=\"background: " + System.Drawing.ColorTranslator.ToHtml(_ToolbarColor) + "; width:20px\" font-family:verdana; font-size: 8pt;\">';");
			sb.Append(DEBUG);
			sb.Append("tableCode += '<img src=\"' + " + pickerObj + "_TblMnuOpt[i].image + '\" border=\"0\" align=\"absmiddle\"></td>';");
			sb.Append(DEBUG);
			sb.Append("tableCode += '<td style=\"font-family:verdana; font-size: 8pt;\">';");
			sb.Append(DEBUG);
			sb.Append("tableCode += '<a style=\"text-decoration: none; width:100%; padding:3px; border: 1px white solid\" ';");
			sb.Append(DEBUG);
			sb.Append("tableCode += 'onmouseover=\\\'this.style.border=\"1px " + System.Drawing.ColorTranslator.ToHtml(_ToolbarItemOverColor) + " solid\";this.style.border=\"1px " + System.Drawing.ColorTranslator.ToHtml(_ToolbarItemOverBorderColor) + " solid\"; this.style.background=\\\"" + System.Drawing.ColorTranslator.ToHtml(_ToolbarItemOverColor) + "\\\";\\\' ';");
			sb.Append(DEBUG);
			sb.Append("tableCode += 'onmouseout=\\\'this.style.border=\"1px white solid\";this.style.background=\\\"white\\\";\\\' ';");
			sb.Append(DEBUG);
			sb.Append("tableCode += 'href=\"' + " + pickerObj + "_TblMnuOpt[i].command + '\">';");
			sb.Append(DEBUG);
			sb.Append("tableCode += '<font color=\"#000000\">' + " + pickerObj + "_TblMnuOpt[i].text + '</a></td></tr>';");
			sb.Append(DEBUG);
			sb.Append("}}");
			sb.Append(DEBUG);
			sb.Append("tableCode += '</table></td></tr>';");
			sb.Append(DEBUG);
			sb.Append("tableCode += '</table>';");
			sb.Append(DEBUG);
			sb.Append("return tableCode;");
			sb.Append(DEBUG);
			sb.Append("}");
			sb.Append(DEBUG);

			sb.Append("function " + pickerObj + "_displayTableMenu(id) {");
			sb.Append(DEBUG);
			sb.Append("if (" + _ClientID + "_isHTMLMode); {alert(\"Function not available in HTML Mode\"); return;}");
			sb.Append(DEBUG);
			sb.Append("var hideme = " + _ClientID + "_getObj('" + _ClientID + "_Highlight_DivLayer');");
			sb.Append(DEBUG);
			sb.Append("if (hideme != null); {hideme.style.display = 'none';}");
			sb.Append(DEBUG);
			sb.Append("var hideme = " + _ClientID + "_getObj('" + _ClientID + "_FontColor_DivLayer');");
			sb.Append(DEBUG);
			sb.Append("if (hideme != null); {hideme.style.display = 'none';}");
			sb.Append(DEBUG);
			sb.Append("if (!" + pickerObj + "_TableDivSet); { " + pickerObj + "_setTableDiv(); }");
			sb.Append(DEBUG);
			sb.Append("var tableMenu = " + _ClientID + "_getObj('" + pickerObj + "_tableMenu');");
			sb.Append(DEBUG);
			sb.Append("if (id == " + pickerObj + "_TableCurID && tableMenu.style.display == 'block') {");
			sb.Append(DEBUG);
			sb.Append("tableMenu.style.display = 'none';");
			sb.Append(DEBUG);
			sb.Append("return;");
			sb.Append(DEBUG);
			sb.Append("}");
			sb.Append(DEBUG);
			sb.Append(pickerObj + "_TableCurID = id;");
			sb.Append(DEBUG);
			sb.Append("var thelink = " + _ClientID + "_getObj(id);");
			sb.Append(DEBUG);
			sb.Append("tableMenu.style.top = " + _ClientID + "_getAbsoluteOffsetTop(thelink); + 20;");
			sb.Append(DEBUG);
			sb.Append("tableMenu.style.left = " + _ClientID + "_getAbsoluteOffsetLeft(thelink);");
			sb.Append(DEBUG);
			sb.Append("tableMenu.style.display = 'block';");
			sb.Append(DEBUG);
			sb.Append("}");
			sb.Append(DEBUG);

			sb.Append("</script>");

			return sb.ToString();
		}
	}
}
