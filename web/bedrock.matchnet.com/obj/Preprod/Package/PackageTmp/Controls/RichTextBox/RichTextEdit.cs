using System;
using System.ComponentModel;
using System.Web.UI;
using System.Text;
using System.Drawing;
using System.Drawing.Design;
using System.Collections.Specialized;

namespace Matchnet.Web.Controls.RichTextBox
{
	[
	Designer(typeof(Matchnet.Web.Controls.RichTextBox.RichTextEditDesigner)),
	DefaultProperty("Text"), 
	ValidationProperty("Text"),
	ToolboxData("<{0}:RichTextEdit runat=server></{0}:RichTextEdit>")
	]
	public class RichTextEdit : System.Web.UI.WebControls.TextBox
	{
		public enum ToolBarTemplates
		{
			None,
			DefaultTemplate,
			BasicOptions,
			MinimalOptions,
			FreeTextApproval
		}

		private string _Text = Matchnet.Constants.NULL_STRING;
		private string _FormName = "Main";
		private bool _ReadOnly = false;
		private string _TextHtml = Matchnet.Constants.NULL_STRING;
		private string _TabIndexOverrides = Matchnet.Constants.NULL_STRING;
		private bool _DisableWYSIWYG = false;
		private string _DialogPath = Matchnet.Constants.NULL_STRING;
		private bool _AllowHtmlEditing = true;
		private ToolBarTemplates _Template = ToolBarTemplates.DefaultTemplate;
		private string _TextBoxHeight = "400";
		private System.Drawing.Color _ToolbarColor = System.Drawing.Color.Gainsboro;
		private System.Drawing.Color _ToolbarItemOverColor = System.Drawing.Color.LightBlue;
		private System.Drawing.Color _ToolbarItemOverBorderColor = System.Drawing.Color.Navy;
		//private string _DefaultSCR = "/blank.htm";
		private System.Web.UI.HtmlControls.HtmlInputHidden hidden = new System.Web.UI.HtmlControls.HtmlInputHidden();

		[
		Bindable(true),
		Category("RichTextEdit"),
		DefaultValue(""),
		Description("Use this property to get or set the rich-text within the control. Read or Write.")
		]
		public override string Text
		{
			get
			{
				return base.Text;
			}
			set
			{
				base.Text = value;
			}
		}


		[
		Bindable(true),
		Category("RichTextEdit"),
		DefaultValue(""),
		Description("Use this property to get or set the form that hosts the control. Read or Write.")
		]
		public string FormName
		{
			get
			{
				return _FormName;
			}
			set
			{
				_FormName = value;
			}
		}


		[
		Bindable(true),
		Category("RichTextEdit"),
		DefaultValue(""),
		Description("Use this property to display only the RichTextEdit content.")
		]
		public override bool ReadOnly
		{
			get
			{
				return _ReadOnly;
			}
			set
			{
				_ReadOnly = value;
			}
		}


		[
		Bindable(true),
		Category("RichTextEdit"),
		DefaultValue(""),
		Description("Read only. An HTML encoded variant of the text property.")
		]
		public string TextHtml
		{
			get
			{
				return _TextHtml;
			}
			set
			{
				_TextHtml = value;
			}
		}


		[
		Bindable(true),
		Category("RichTextEdit"),
		DefaultValue(""),
		Description("Overrides the default tab index property.")
		]
		public string TabIndexOverrides
		{
			get
			{
				return _TabIndexOverrides;
			}
			set
			{
				_TabIndexOverrides = value;
			}
		}

		[
		Bindable(true),
		Category("RichTextEdit"),
		DefaultValue(""),
		Description("Boolean value to determine if standard textarea should be displayed.")
		]
		public bool DisableWYSIWYG
		{
			get
			{
				return _DisableWYSIWYG;
			}
			set
			{
				_DisableWYSIWYG = value;
			}
		}


		[
		Bindable(true),
		Category("RichTextEdit"),
		DefaultValue(""),
		Description("The relative path to the dialog directory.")
		]
		public string DialogPath
		{
			get
			{
				return _DialogPath;
			}
			set
			{
				_DialogPath = value;
			}
		}


		[
		Bindable(true),
		Category("RichTextEdit"),
		DefaultValue(""),
		Description("Boolean value indicating if HTML editing is allowed.")
		]
		public bool AllowHtmlEditing
		{
			get
			{
				return _AllowHtmlEditing;
			}
			set
			{
				_AllowHtmlEditing = value;
			}
		}


		[
		Bindable(true),
		Category("RichTextEdit"),
		DefaultValue("ToolBarTemplates.DefaultTemplate"),
		Description("Collection of default toolbar configurations. You must select None to programatically build the toolbars.")
		]
		public ToolBarTemplates ToolBarConfiguration
		{
			get
			{
				return _Template;
			}
			set
			{
				_Template = value;
			}
		}


		[
		Bindable(true),
		Category("RichTextEdit"),
		DefaultValue("100"),
		Description("Sets the height in pixels of the editng area.")
		]
		public string TextBoxHeight
		{
			get
			{
				return _TextBoxHeight;
			}
			set
			{
				_TextBoxHeight = value;
			}
		}


		[
		Bindable(true),
		Category("RichTextEdit"),
		DefaultValue("System.Drawing.Color.Gainsboro"),
		Description("Sets the background color of the toolbars.")
		]
		public System.Drawing.Color ToolbarColor
		{
			get
			{
				return _ToolbarColor;
			}
			set
			{
				_ToolbarColor = value;
			}
		}


		[
		Bindable(true),
		Category("RichTextEdit"),
		DefaultValue("System.Drawing.Color.LightBlue"),
		Description("Sets the background color of the toolbar items on mouseover.")
		]
		public System.Drawing.Color ToolbarItemOver
		{
			get
			{
				return _ToolbarItemOverColor;
			}
			set
			{
				_ToolbarItemOverColor = value;
			}
		}


		[
		Bindable(true),
		Category("RichTextEdit"),
		DefaultValue("System.Drawing.Color.LightBlue"),
		Description("Sets the background color of the toolbar border items on mouseover.")
		]
		public System.Drawing.Color ToolbarItemOverBorder
		{
			get
			{
				return _ToolbarItemOverBorderColor;
			}
			set
			{
				_ToolbarItemOverBorderColor = value;
			}
		}


		public RichTextEdit()
		{
			this.EnableViewState = true;
		}


		protected override void Render(HtmlTextWriter writer)
		{
			RichTextBox.ClientSideScript clientSideScript = new RichTextBox.ClientSideScript(this);
			RichTextBox.MenuLayouts menuLayouts = new RichTextBox.MenuLayouts(this);
			RichTextBox.ToolBar toolBar = new RichTextBox.ToolBar(this);
			RichTextBox.ToolBarItems toolBarItems = new RichTextBox.ToolBarItems(this);
			string browser = System.Web.HttpContext.Current.Request.Browser.Browser;
			string platform = System.Web.HttpContext.Current.Request.Browser.Platform;
			int version = Matchnet.Conversion.CInt(System.Web.HttpContext.Current.Request.Browser.Version.Substring(0, 1));
			
			if( _ReadOnly)
			{
				writer.Write(_Text);
				this.Attributes.Add("style", "display:none;");
			}
			else
			{
				bool isIEBrowser = (browser.ToLower().IndexOf("ie") != -1) ? true : false;
				bool isWindows = (platform.ToLower().IndexOf("win") != -1) ? true: false;

				if ( isIEBrowser & isWindows  & (!_DisableWYSIWYG))
				{
					this.Attributes.Add("style", "display:none;");

					writer.Write(clientSideScript.RegisterClientScript());
					writer.Write(clientSideScript.ToggleButtons());
					
					switch(ToolBarConfiguration)
					{
						case ToolBarTemplates.DefaultTemplate:
							writer.Write(menuLayouts.DefaultTemplate());
							break;
						case ToolBarTemplates.BasicOptions:
							writer.Write(menuLayouts.BasicOptions());
							break;
						case ToolBarTemplates.MinimalOptions:
							writer.Write(menuLayouts.MinimalOptions());
							break;
						case ToolBarTemplates.FreeTextApproval:
							writer.Write(menuLayouts.FreeTextApproval());
							break;
						default:
							if (CustomTools.menuItems.Length > 0)
								writer.Write(CustomTools.menuItems.ToString());
							break;
					}

					writer.Write(clientSideScript.RegisterButtonMouseOver("MenuIcons"));
					writer.Write("<table width='100%' cellpadding='0' cellspacing='0'>");
					writer.Write("<tr><td>");
					writer.Write(clientSideScript.RegisterIFrame());
					writer.Write("</td>");

					writer.Write("<td valign='top' width='1' align='center' style='background-color:" + System.Drawing.ColorTranslator.ToHtml(_ToolbarColor) + ";'>");
					writer.Write(clientSideScript.RenderIEObjects());
					writer.Write("</td>");

					writer.Write("</tr></table>");

					if (this.AllowHtmlEditing)
						writer.Write(clientSideScript.RegisterSwitchButtons());
				}
				else
				{
					this.Visible = true;
					this.TextMode = System.Web.UI.WebControls.TextBoxMode.MultiLine;
					if (TextBoxHeight != String.Empty)
						this.Rows = Matchnet.Conversion.CInt(TextBoxHeight);
					else
						this.Rows = 15;

					this.Attributes.Add("style", "width:100%;");
				}
			}

			base.Render(writer);
		}


		public string TidyObjectRef(string input)
		{
			if (input.StartsWith("_"))
				input = input.Substring(1, (input.Length - 1));

			return input;
		}


		public class CustomTools
		{
			public static System.Text.StringBuilder menuItems = new System.Text.StringBuilder();

			public CustomTools()
			{
				menuItems.Remove(0, menuItems.Length);
			}

			public void AddItem(string item)
			{
				menuItems.Append(item);
			}
		}
	}
}
