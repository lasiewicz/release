<%@ Page language="C#" %>
<% 
/*	NOTE:
	This page is served by matchnet.st.sageanalyst.net
	as a javascript include for Sage Analyst.
	It checks to see if the user has been cookied for st.sageanalyst.net,
	and if not, sets a cookie according to sage specifications:
	DDMMYYYY
	-744-
	IPADDRESS	(numeric IP with dots removed)
	RANDOM8C	(8-digit random number)
	UNIXTIME	(seconds since midnight, 1 Jan 1970 GMT)
*/
	string strKey = "smuid";
	if (HttpContext.Current.Request.Cookies[strKey] == null) 
	{
		long SECS_BETWEEN_EPOCHS = 11644473600; // offset to FILETIME epoch
		long HECTONS_PER_SEC = 10000000;		// 10^7
		/* Magic number explanation:
			Both epochs are Gregorian. 1970 - 1601 = 369. Assuming a leap
			year every four years, 369 / 4 = 92. However, 1700, 1800, and 1900
			were NOT leap years, so 89 leap years, 280 non-leap years.
			89 * 366 + 280 * 365 = 134744 days between epochs. Of course
			60 * 60 * 24 = 86400 seconds per day, so 134744 * 86400 =
			11644473600 = SECS_BETWEEN_EPOCHS.

			This result is also confirmed in the MSDN documentation on how
			to convert a time_t value to a win32 FILETIME.
		*/

		DateTime now = DateTime.Now;
		string dmystr = now.ToString("ddMMyyyy");
		string ipstr = HttpContext.Current.Request.UserHostAddress;
		int rand8 = (int)(100000000 * (new Random()).NextDouble());
		UInt32 time_t = (UInt32)(now.ToFileTime()/HECTONS_PER_SEC - SECS_BETWEEN_EPOCHS);

		// DDMMYYYY-744-IPADDRESSRANDOM8CUNIXTIME
		string strVal = dmystr + "-744-" + ipstr.Replace(".","") +
						rand8.ToString("00000000") + time_t.ToString();

		HttpCookie newCookie = new HttpCookie(strKey, strVal);
		newCookie.Domain = "sageanalyst.net";
		TimeSpan ts = new TimeSpan((int)(5*365.25),0,0,0);
		newCookie.Expires = DateTime.Now.Add(ts);
		HttpContext.Current.Response.Cookies.Add(newCookie);
	}
	HttpContext.Current.Response.ContentType = "application/x-javascript";
%>

//744


var st_iu=st_rs=st_cd=st_rf=st_us=st_hn=st_qs=st_ce="";
var d=document;
var l=location;
var n=navigator;
st_ce=n.cookieEnabled;
st_ai=st_ai.replace('+','%2b');

st_qs=escape(l.search.substring(1));
st_us=escape(l.pathname);
st_hn=l.host;
st_rf=escape(d.referrer);
if(st_v==1.2) {
        var s=screen;
        st_rs=escape(s.width+"x"+s.height);
        st_cd=s.colorDepth;
}
proto = "http:";
if (location.protocol == "https:") {
        proto = "https:";
}
st_iu=proto+"//"+st_dd+"/" + (new Date()).getTime() + "/JS?ci="+st_ci+"&di="+st_di+"&pg="
+st_pg+"&us="+st_us+"&qs="+st_qs+"&rf="+st_rf+"&rs="+st_rs+"&cd="+st_cd+"&hn="
+st_hn+"&ce="+st_ce+"&jv="+st_v+"&tai="+st_tai+"&ai="+st_ai;

var iXz=new Image(); iXz.src=st_iu;
