﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Spark.SAL;

namespace Matchnet.Web.Interfaces
{
    public interface ISlideshowPersistence
    {
        int MinAge { get; set; }
        int MaxAge { get; set; }
        int RegionID { get; set; }
        SearchType SearchType { get; set; }
        int AreaCode1 { get; set; }
        int AreaCode2 { get; set; }
        int AreaCode3 { get; set; }
        int AreaCode4 { get; set; }
        int AreaCode5 { get; set; }
        int AreaCode6 { get; set; }

        void PersistValues();
    }
}
