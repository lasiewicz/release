﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Member.ServiceAdapters.Interfaces;
using Matchnet.Member.ValueObjects.Interfaces;
using Matchnet.Member.ValueObjects.Photos;
using Matchnet.Web.Framework;

namespace Matchnet.Web.Interfaces
{
    public interface IMemberPhotoDisplayManager
    {
        string ImageDirectory { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="photo"></param>
        /// <param name="brand"></param>
        /// <returns>True if the Photo is private and the Brand allows private photos.</returns>
        bool IsPrivatePhoto(Photo photo, Brand brand);

        bool PhotoIsEmpty(Photo photo, PhotoType photoType, Brand brand);

        /// <summary>
        /// Selects a "default" photo object to use as a thumbnail (or whatever), returning a private photo if there are no approved,
        /// non-private photos, and null if there are no approved photos.
        /// </summary>
        /// <param name="member"></param>
        /// <returns>The member's Photo or null if there is no photo.</returns>
        Photo GetDefaultPhoto(IMemberDTO viewingMember, IMemberDTO member, Brand brand);

        /// <summary>
        /// Retrieves one random photo out of all main-eligible photos.
        /// </summary>
        /// <param name="member"></param>
        /// <param name="communityId"></param>
        /// <returns></returns>
        Photo GetRandomApprovedForMainPhoto(IMemberDTO viewingMember, IMemberDTO member, Brand brand);

        /// <summary>
        /// Gets all photos that approved for main.
        /// </summary>
        /// <param name="member"></param>
        /// <param name="communityId"></param>
        /// <returns></returns>
        List<Photo> GetApprovedForMainPhotos(IMemberDTO viewingMember, IMemberDTO member, Brand brand);

        /// <summary>
        /// Gets any approved photos; including approved photos not suitable for main.
        /// </summary>
        /// <param name="member"></param>
        /// <param name="communityId"></param>
        /// <returns></returns>
        List<Photo> GetApprovedPhotos(IMemberDTO viewingMember, IMemberDTO member, Brand brand);

        /// <summary>
        /// Count can be obtained by calling GetApprovedPhotos too, but by making this into a separate method, it's easier to tell the caller's intention.
        /// This will help us during future refactoring.  I see too many places where GetApprovedPhotos is called just for count.
        /// </summary>
        /// <param name="member"></param>
        /// <param name="communityId"></param>
        /// <returns></returns>
        int GetApprovedPhotosCount(IMemberDTO viewingMember, IMemberDTO member, Brand brand);

        string GetPhotoDisplayURL(IMemberDTO memberViewing, IMemberDTO memberBeingViewed, Brand brand,
                                                  Photo photo, PhotoType photoType, PrivatePhotoImageType privatePhotoType, bool useFullURL);

        string GetPhotoDisplayURL(IMemberDTO memberViewing, IMemberDTO memberBeingViewed, Brand brand,
                                                  Photo photo, PhotoType photoType, PrivatePhotoImageType privatePhotoType);

        string GetPhotoDisplayURL(IMemberDTO memberViewing, IMemberDTO memberBeingViewed, Brand brand,
                                                  Photo photo, PhotoType photoType, PrivatePhotoImageType privatePhotoType, NoPhotoImageType noPhotoType);

        string GetPhotoDisplayURL(IMemberDTO memberViewing, IMemberDTO memberBeingViewed, Brand brand,
                                                  Photo photo, PhotoType photoType, PrivatePhotoImageType privatePhotoType, NoPhotoImageType noPhotoType, bool useFullURL);

        string GetPhotoDisplayURL(IMemberDTO memberViewing, IMemberDTO memberBeingViewed, Brand brand,
                                                  Photo photo, PhotoType photoType, PrivatePhotoImageType privatePhotoType, NoPhotoImageType noPhotoType, bool useFullURL, bool displayPrivatePhotos);

        string GetPrivatePhotoFile(PrivatePhotoImageType imageType, IMemberDTO member, Brand brand);
        string GetPrivatePhotoFile(PrivatePhotoImageType imageType, bool isMale);
        string GetNoPhotoFile(NoPhotoImageType imageType, IMemberDTO member, Brand brand);
        string GetNoPhotoFile(NoPhotoImageType imageType, bool isMale);
        bool IsPhotosAvailableToGuests(IMemberDTO memberViewing, IMemberDTO memberBeingViewed, Brand brand);
    }
}