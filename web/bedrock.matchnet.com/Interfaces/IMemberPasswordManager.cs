﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.Web.Interfaces
{
    public interface IMemberPasswordManager
    {
        bool ResetPassword(int brandId, string emailAddress);
        int ValidateResetPasswordToken(string tokenGuidString);
    }
}
