﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Matchnet.Member.ServiceAdapters.Interfaces;
using Matchnet.Purchase.ValueObjects;
using Matchnet.Web.Framework;
using Matchnet.Web.Framework.Managers;
using Brand = Matchnet.Content.ValueObjects.BrandConfig.Brand;

namespace Matchnet.Web.Interfaces
{
    public interface IOneClickPurchaseManager
    {
        bool IsOneClickAllAccessEnabled(Brand brand, IMember member);
        bool MemberHaveValidOneClickPaymentProfile(int memberID, int callingSystemID);
        Spark.Common.PurchaseService.purchaseResponse DoOneClickPurchase(ContextGlobal g, int prtid, string oneClickPackages);
        string GetCreditCardTypeText(int orderID, int CustomerID, int CallingSystemID, ContextGlobal g);
        string SerializeOneClickPackage(OneClickPurchaseManager.OneClickPackage package);
        OneClickPurchaseManager.OneClickPackage GetUpsalePackage(ContextGlobal g, PremiumType premiumType);
        decimal GetPackageOverrideTotalAmount(ContextGlobal g, PremiumType premiumType);
        OneClickPurchaseManager.OneClickPackage GetFixedPricingUpslaePackage(ContextGlobal g);
    }
}
