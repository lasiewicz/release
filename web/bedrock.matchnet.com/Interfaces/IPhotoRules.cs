﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;

namespace Matchnet.Web.Interfaces
{
    public interface IPhotoRules
    {
        bool IsValidImage(Stream stream, int contentLength);
        bool IsFileSizeAllowed(int fileSize);
        int GetMaxPhotoCount(Brand brand);
        bool IsAcceptedImageType(byte[] input);
        bool FileNameExtensionAllowed(string filename);
    }
}
