﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;

namespace Matchnet.Web.Interfaces
{
    public interface ICurrentRequest
    {
        NameValueCollection QueryString { get; }
        NameValueCollection Form { get; }
        string this[string index] { get; }
        Uri Url { get; }
        string RawUrl { get; }
        string UrlReferrer { get; }
        string ClientIP { get; }
    }
}
