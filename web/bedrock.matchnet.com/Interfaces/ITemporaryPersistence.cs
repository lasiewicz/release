﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using Matchnet.Content.ValueObjects.BrandConfig;

namespace Matchnet.Web.Interfaces
{
    public interface ITemporaryPersistence
    {
        string this[string index] { get; set; }
        string Name { get; }
        DateTime Expires { get; set; }
        NameValueCollection Values { get; }
        bool HasValues { get; }
        void Persist(Brand brand);
        void Clear();
        
    }
}