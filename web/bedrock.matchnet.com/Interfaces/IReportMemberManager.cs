﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.Web.Interfaces
{
    interface IReportMemberManager
    {
        string ComposeMessage(string EmailAddress, string SiteName, string ReportedUsername, string Explanation, string EnvironmentType,
                              string EndLineDelimiter, int? MemberId);
    }
}
