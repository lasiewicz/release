USE mnsystem

DECLARE @Attribute1AID int = 000
DECLARE @Attribute2AID int = 001
DECLARE @AttributeGroupID int

SELECT @AttributeGroupID = (SELECT MAX(AttributeGroupID) FROM AttributeGroup (NOLOCK))

INSERT INTO Attribute (AttributeID, ScopeID, AttributeName, DataType, UpdateDate, AttributeIDOldValueContainer)
	SELECT @Attribute1AID, 0, 'ATTRIBUTENAME', 'DATATYPE', GETDATE(), null
	WHERE NOT EXISTS (SELECT 1 FROM Attribute (NOLOCK) WHERE AttributeID = @Attribute1AID)
UNION
	SELECT @Attribute2AID, 0, 'ATTRIBUTENAME2', 'DATATYPE2', GETDATE(), null
	WHERE NOT EXISTS (SELECT 1 FROM Attribute (NOLOCK) WHERE AttributeID = @Attribute2AID)

INSERT INTO AttributeGroup (AttributeGroupID, AttributeID, GroupID, AttributeTypeID, AttributeStatusMask, [Length], EncryptFlag, DefaultValue, UpdateDate, AttributeGroupIDOldValueContainer)
	SELECT @AttributeGroupID+1, @Attribute1AID, 8383, 1, 1, 2000, 0, null, GETDATE(), null
	WHERE NOT EXISTS (SELECT 1 FROM AttributeGroup (NOLOCK) WHERE AttributeID = @Attribute1AID AND GroupID = 8383)
UNION
	SELECT @AttributeGroupID+2, @Attribute2AID, 8383, 1, 1, null, 0, null, GETDATE(), null
	WHERE NOT EXISTS (SELECT 1 FROM AttributeGroup (NOLOCK) WHERE AttributeID = @Attribute2AID AND GroupID = 8383)
