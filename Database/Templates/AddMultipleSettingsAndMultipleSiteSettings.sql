USE mnsystem

DECLARE @FirstSettingID int
DECLARE @SecondSettingID int
DECLARE @SiteSettingID int
SELECT @FirstSettingID=MAX(settingid)+1 FROM setting (NOLOCK)
SELECT @SecondSettingID=MAX(settingid)+2 FROM setting (NOLOCK)
SELECT @SiteSettingID=MAX(sitesettingid) FROM sitesetting (NOLOCK)


INSERT INTO Setting (SettingID,SettingCategoryID,SettingConstant,GlobalDefaultValue,SettingDescription,IsRequiredForCommunity,IsRequiredForSite,CreateDate) 
	SELECT @FirstSettingID, 1, 'CONSTANT', 'GLOBALDEFAULTVALUE', 'DESCRIPTION', null, null, GETDATE()
	WHERE NOT EXISTS (SELECT 1 FROM Setting (NOLOCK) WHERE SettingConstant='CONSTANT')
UNION
	SELECT @SecondSettingID, 1, 'CONSTANT2', 'GLOBALDEFAULTVALUE', 'DESCRIPTION', null, null, GETDATE()
	WHERE NOT EXISTS (SELECT 1 FROM Setting (NOLOCK) WHERE SettingConstant='CONSTANT2')


INSERT INTO SiteSetting (SiteSettingID, SiteID, SettingID, SiteSettingValue) 
	SELECT @SiteSettingID+1, 000, @FirstSettingID, 'SITESETTINGVALUE'
	WHERE NOT EXISTS (SELECT 1 FROM SiteSetting (NOLOCK) WHERE SettingID = @FirstSettingID AND SiteID = 000)
UNION
	SELECT @SiteSettingID+2, 001, @FirstSettingID, 'SITESETTINGVALUE'
	WHERE NOT EXISTS (SELECT 1 FROM SiteSetting (NOLOCK) WHERE SettingID = @FirstSettingID AND SiteID = 001)
UNION
	SELECT @SiteSettingID+3, 000, @SecondSettingID, 'SITESETTINGVALUE'
	WHERE NOT EXISTS (SELECT 1 FROM SiteSetting (NOLOCK) WHERE SettingID = @SecondSettingID AND SiteID = 000)


