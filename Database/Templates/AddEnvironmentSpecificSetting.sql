USE mnsystem

DECLARE @SettingID int
DECLARE @GlobalDefaultValue varchar(400)
DECLARE @ScriptingEnvironment varchar(400)
SELECT @SettingID=MAX(settingid)+1 FROM setting (NOLOCK)

SELECT @ScriptingEnvironment = GlobalDefaultValue FROM Setting (NOLOCK) WHERE SettingConstant = 'SCRIPTING_ENVIRONMENT'

SELECT @GlobalDefaultValue = 
CASE 
	 WHEN @ScriptingEnvironment = 'DEV' THEN 'DEVGLOBALDEFAULTVALUE'
	 WHEN @ScriptingEnvironment = 'STGV1' THEN 'STGV1GLOBALDEFAULTVALUE'
	 WHEN @ScriptingEnvironment = 'STGV2' THEN 'STGV2GLOBALDEFAULTVALUE'
	 WHEN @ScriptingEnvironment = 'STGV3' THEN 'STGV3GLOBALDEFAULTVALUE'
	 WHEN @ScriptingEnvironment = 'PROD' THEN 'PRODGLOBALDEFAULTVALUE'
END

IF NOT EXISTS (SELECT 1 FROM Setting (NOLOCK) WHERE SettingConstant = 'XXXXXX')
BEGIN 
	INSERT INTO Setting  (SettingID,SettingCategoryID,SettingConstant,GlobalDefaultValue,SettingDescription,IsRequiredForCommunity,IsRequiredForSite,CreateDate) VALUES
	(@SettingID, 1, 'CONSTANT', @GlobalDefaultValue, 'DESCRIPTION', null, null, GETDATE())
END




