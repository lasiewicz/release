USE mnsystem

DECLARE @SettingID int
SELECT @SettingID=MAX(settingid)+1 FROM setting (NOLOCK)

IF NOT EXISTS (SELECT 1 FROM Setting (NOLOCK) WHERE SettingConstant = 'XXXXXX')
BEGIN 
	INSERT INTO Setting  (SettingID,SettingCategoryID,SettingConstant,GlobalDefaultValue,SettingDescription,IsRequiredForCommunity,IsRequiredForSite,CreateDate) VALUES
	(@SettingID, 1, 'CONSTANT', 'GLOBALDEFAULTVALUE', 'DESCRIPTION', null, null, GETDATE())
END