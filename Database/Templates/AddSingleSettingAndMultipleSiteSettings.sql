USE mnsystem

DECLARE @SettingID int
DECLARE @SiteSettingID int
SELECT @SettingID=MAX(settingid)+1 FROM setting (NOLOCK)
SELECT @SiteSettingID=MAX(sitesettingid) FROM sitesetting (NOLOCK)


IF NOT EXISTS (SELECT 1 FROM Setting (NOLOCK) WHERE SettingConstant = 'XXXXXX')
BEGIN 
	INSERT INTO Setting  (SettingID,SettingCategoryID,SettingConstant,GlobalDefaultValue,SettingDescription,IsRequiredForCommunity,IsRequiredForSite,CreateDate) VALUES
	(@SettingID, 1, 'CONSTANT', 'GLOBALDEFAULTVALUE', 'DESCRIPTION', null, null, GETDATE())
END

INSERT INTO SiteSetting (SiteSettingID, SiteID, SettingID, SiteSettingValue) 
	SELECT @SiteSettingID+1, 000, @SettingID, 'SITESETTINGVALUE'
	WHERE NOT EXISTS (SELECT 1 FROM SiteSetting (NOLOCK) WHERE SettingID = @SettingID AND SiteID = 000)
UNION
	SELECT @SiteSettingID+2, 001, @SettingID, 'SITESETTINGVALUE'
	WHERE NOT EXISTS (SELECT 1 FROM SiteSetting (NOLOCK) WHERE SettingID = @SettingID AND SiteID = 001)
UNION
	SELECT @SiteSettingID+3, 002, @SettingID, 'SITESETTINGVALUE'
	WHERE NOT EXISTS (SELECT 1 FROM SiteSetting (NOLOCK) WHERE SettingID = @SettingID AND SiteID = 002)



