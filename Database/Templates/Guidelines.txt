
An overreaching goal here is to make sure that all scripts are re-runable on all of our environments, so keep that in mind as you review these rules. 

1) All table create statements should be preceded by a "if exists then drop" clause, like so:

if exists (select 1 from INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'TableX')
      drop table [dbo].[TableX]
GO

2) All table schema changes - adding a column, adding an index, whatever - should be wrapped in similar appropriate "if exists" statements so that the script will only attempt the change if it hasn't already been done

3) All tables should have a primary key unless there's a really, really, really good reason why it shouldn't, and David Martin will never want to report on the data in that table. 

4) All modifications to a stored proc, whether creating or altering, should be done as a conditional drop and create, like so: 

if exists (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'up_spX' AND ROUTINE_TYPE='PROCEDURE')
      drop procedure [dbo].[up_spX]
GO

create procedure [dbo].[up_spX]
                ....
GO

5) You should always grant db_executor the execute permission on your stored proc: 
                grant execute on [dbo].[up_spX] to db_executor
                go

6) All stored procs should start with the following two lines:
                SET NOCOUNT ON  
                SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  

7) All new settings (and site/server settings) should get their settingID dynamically (max+1) and should be wrapped in "if exists insert " clause:

DECLARE @SettingID int
SELECT @SettingID=MAX(settingid)+1 FROM setting (NOLOCK)

IF NOT EXISTS (SELECT 1 FROM Setting (NOLOCK) WHERE SettingConstant = 'XXXXXX')
BEGIN 
                INSERT INTO Setting  (SettingID,SettingCategoryID,SettingConstant,GlobalDefaultValue,SettingDescription,IsRequiredForCommunity,IsRequiredForSite,CreateDate) VALUES
                (@SettingID, 1, 'CONSTANT', 'GLOBALDEFAULTVALUE', 'DESCRIPTION', null, null, GETDATE())
END

8) Include a USE statement at the top of each script (and anywhere else appropriate)

9) Use (NOLOCK) with all select statements

10) If you run a script on stage and it doesn't work as expected, instead of simply fixing the results with an update statement, you should delete all the changes you made, fix the script, and have DB rerun it. 

11) Avoid nested select statements and use variables set outside of the main logic instead where possible - makes scripts more readable.

AttributeID, PageID - Hard code in values, everything else is dynamic IDs