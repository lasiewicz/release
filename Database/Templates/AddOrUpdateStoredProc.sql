USE mnSystem
GO

DECLARE @dbname sysname=DB_NAME()
EXEC mnsystem.dbo.usp_AssureExistance 'usp_select_test',@dbname
GO

ALTER PROCEDURE 
	usp_select_test
	AS
	BEGIN
		SET NOCOUNT ON  
		SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  

		--logic goes here
	END
	GO
                        
GRANT EXECUTE ON [dbo].[usp_select_test] TO db_executor
GO
