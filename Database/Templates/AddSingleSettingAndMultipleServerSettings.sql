USE mnsystem

DECLARE @SettingID int
DECLARE @ServerSettingID int
SELECT @SettingID=MAX(settingid)+1 FROM setting (NOLOCK)
SELECT @ServerSettingID=MAX(serversettingid) FROM serversetting (NOLOCK)


IF NOT EXISTS (SELECT 1 FROM Setting (NOLOCK) WHERE SettingConstant = 'XXXXXX')
BEGIN 
	INSERT INTO Setting  (SettingID,SettingCategoryID,SettingConstant,GlobalDefaultValue,SettingDescription,IsRequiredForCommunity,IsRequiredForSite,CreateDate) VALUES
	(@SettingID, 1, 'CONSTANT', 'GLOBALDEFAULTVALUE', 'DESCRIPTION', null, null, GETDATE())
END

INSERT INTO ServerSetting (ServerSettingID, ServerID, SettingID, CommunityID, SiteID, BrandID, ServerSettingValue) 
	SELECT @ServerSettingID+1, 000, @SettingID, null, null, null, 'SERVERSETTINGVALUE'
	WHERE NOT EXISTS (SELECT 1 FROM ServerSetting (NOLOCK) WHERE SettingID = @SettingID AND ServerID = 000)
UNION
	SELECT @ServerSettingID+2, 001, @SettingID, null, null, null, 'SERVERSETTINGVALUE'
	WHERE NOT EXISTS (SELECT 1 FROM ServerSetting (NOLOCK) WHERE SettingID = @SettingID AND ServerID = 001)
UNION
	SELECT @ServerSettingID+3, 002, @SettingID, null, null, null, 'SERVERSETTINGVALUE'
	WHERE NOT EXISTS (SELECT 1 FROM ServerSetting (NOLOCK) WHERE SettingID = @SettingID AND ServerID = 002)



