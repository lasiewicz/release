-- Settings for Spark.API.Service 
use mnsystem
go
declare @FirstSettingID int
declare @SecondSettingID int
declare @ThirdSettingID int

select @FirstSettingID=max(settingid)+1 from setting (nolock)
select @SecondSettingID=max(settingid)+2 from setting (nolock)
select @ThirdSettingID=max(settingid)+3 from setting (nolock)

insert into Setting (SettingID,SettingCategoryID,SettingConstant,GlobalDefaultValue,SettingDescription,IsRequiredForCommunity,IsRequiredForSite,CreateDate) 
      select @FirstSettingID, 7, 'APISVC_SA_CONNECTION_LIMIT', 10, 'max connections per URI', 0, 0, getdate()
      where not exists (select 1 from Setting (nolock) where SettingConstant='APISVC_SA_CONNECTION_LIMIT')
union
      select @SecondSettingID, 7, 'APISVC_SA_HOST_OVERRIDE', '', 'alternate hostname (for local testing)', 0, 0, getdate()
      where not exists (select 1 from Setting (nolock) where SettingConstant='APISVC_SA_HOST_OVERRIDE')
union
      select @ThirdSettingID, 7, 'APISVC_REPLICATION_OVERRIDE', '', 'replication partner override', 0, 0, getdate()
      where not exists (select 1 from Setting (nolock) where SettingConstant='APISVC_REPLICATION_OVERRIDE')
