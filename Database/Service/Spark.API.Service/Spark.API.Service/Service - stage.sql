use mnSystem
go
--Port 50800
if not exists(select 1  from [Service] where ServiceID = 50800)
begin
insert into Service (ServiceID, ServiceConstant, ServiceDescription, ChannelTypeID, ServiceReplicationModeID, WindowsServiceName)
values (50800, 'API_SVC', 'API Service', 1, 1, null)
end

if not exists (select 1 from [ServicePartition] where ServicePartitionID = 50800)
begin
insert into ServicePartition (ServicePartitionID, ServiceID, PartitionOffset, PartitionExclusiveUri, PartitionSharedUri, PartitionUri, PartitionPort)
values (50800, 50800, 0, '', '', '172.16.210.23', 50800) -- Stage3 VIP
end

if not exists(select 1  from [ServiceInstance] where ServiceInstanceID in (50800, 50801))
begin
insert into ServiceInstance (ServiceInstanceID, ServicePartitionID, ServerID, IsEnabled)
values (50800, 50800, 401, 1) -- LASVCSTAGEV301
insert into ServiceInstance (ServiceInstanceID, ServicePartitionID, ServerID, IsEnabled)
values (50801, 50800, 402, 1) -- LASVCSTAGEV302
end

--up_service_list api


