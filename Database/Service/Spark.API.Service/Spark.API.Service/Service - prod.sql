use mnSystem
go
--Port 50800
if not exists(select 1  from [Service] where ServiceID = 50800)
begin
insert into Service (ServiceID, ServiceConstant, ServiceDescription, ChannelTypeID, ServiceReplicationModeID)
values (50800, 'API_SVC', 'API Service', 1, 1)
end

if not exists (select 1 from [ServicePartition] where ServicePartitionID = 50800)
begin
insert into ServicePartition (ServicePartitionID, ServiceID, PartitionOffset, PartitionExclusiveUri, PartitionSharedUri, PartitionUri, PartitionPort)
values (50800, 50800, 0, '', '', '172.16.100.51', 50800) -- PROD VIP
end

if not exists(select 1  from [ServiceInstance] where ServiceInstanceID in (50800, 50801))
begin
insert into ServiceInstance (ServiceInstanceID, ServicePartitionID, ServerID, IsEnabled)
values (50800, 50800, 20011, 1) -- LASVC11
insert into ServiceInstance (ServiceInstanceID, ServicePartitionID, ServerID, IsEnabled)
values (50801, 50800, 20012, 1) -- LASVC12
end

--up_service_list api

