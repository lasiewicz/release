/******* AppMember CRUD ********/
use [mnAPI];
go

if object_id('[dbo].[up_AppMember_List]') is not null
begin 
    drop proc [dbo].[up_AppMember_List] 
end 
go
create proc [dbo].[up_AppMember_List] 
    @AppID int,
    @MemberID int
as 
	set nocount on 
	set xact_abort on  
	set transaction isolation level read UNCOMMITTED 

	begin tran

	select [AppID], [MemberID], [AccessToken], [AccessTokenExpirationDateTime], [RefreshToken], [RefreshTokenExpirationDateTime], [UpdateDateTime], [InsertDateTime] 
	from   [dbo].[AppMember] 
	where  ([AppID] = @AppID or @AppID is null) 
	       and ([MemberID] = @MemberID or @MemberID is null) 

	commit
go
if object_id('[dbo].[up_AppMember_Insert]') is not null
begin 
    drop proc [dbo].[up_AppMember_Insert] 
end 
go
create proc [dbo].[up_AppMember_Insert] 
    @AppID int,
    @MemberID int,
    @AccessToken nvarchar(100),
    @AccessTokenExpirationDateTime smalldatetime,
    @RefreshToken nvarchar(100),
    @RefreshTokenExpirationDateTime smalldatetime
as 
	set nocount on 
	set xact_abort on  
	set transaction isolation level read UNCOMMITTED 

	begin tran
	
	insert into [dbo].[AppMember] ([AppID], [MemberID], [AccessToken], [AccessTokenExpirationDateTime], [RefreshToken], [RefreshTokenExpirationDateTime], [UpdateDateTime], [InsertDateTime])
	select @AppID, @MemberID, @AccessToken, @AccessTokenExpirationDateTime, @RefreshToken, @RefreshTokenExpirationDateTime, GETDATE(), GETDATE()
	
	/*/*/*-- Begin Return Select <- do not remove
	select [AppID], [MemberID], [AccessToken], [AccessTokenExpirationDateTime], [RefreshToken], [RefreshTokenExpirationDateTime], [UpdateDateTime], [InsertDateTime]
	from   [dbo].[AppMember]
	where  [AppID] = @AppID
	       and [MemberID] = @MemberID
	-- End Return Select <- do not remove*/*/*/
               
	commit
go
if object_id('[dbo].[up_AppMember_Update]') is not null
begin 
    drop proc [dbo].[up_AppMember_Update] 
end 
go
create proc [dbo].[up_AppMember_Update] 
    @AppID int,
    @MemberID int,
    @AccessToken nvarchar(100),
    @AccessTokenExpirationDateTime smalldatetime,
    @RefreshToken nvarchar(100),
    @RefreshTokenExpirationDateTime smalldatetime
as 
	set nocount on 
	set xact_abort on  
	set transaction isolation level read UNCOMMITTED 

	begin tran

	update [dbo].[AppMember]
	set    [AppID] = @AppID, [MemberID] = @MemberID, [AccessToken] = @AccessToken, [AccessTokenExpirationDateTime] = @AccessTokenExpirationDateTime, [RefreshToken] = @RefreshToken, [RefreshTokenExpirationDateTime] = @RefreshTokenExpirationDateTime, [UpdateDateTime] = GETDATE()
	where  [AppID] = @AppID
	       and [MemberID] = @MemberID
	
	/*/*/*-- Begin Return Select <- do not remove
	select [AppID], [MemberID], [AccessToken], [AccessTokenExpirationDateTime], [RefreshToken], [RefreshTokenExpirationDateTime], [UpdateDateTime], [InsertDateTime]
	from   [dbo].[AppMember]
	where  [AppID] = @AppID
	       and [MemberID] = @MemberID	
	-- End Return Select <- do not remove*/*/*/

	commit tran
go
if object_id('[dbo].[up_AppMember_Delete]') is not null
begin 
    drop proc [dbo].[up_AppMember_Delete] 
end 
go
create proc [dbo].[up_AppMember_Delete] 
    @AppID int,
    @MemberID int
as 
	set nocount on 
	set xact_abort on  
	set transaction isolation level read UNCOMMITTED 
	
	begin tran

	delete
	from   [dbo].[AppMember]
	where  [AppID] = @AppID
	       and [MemberID] = @MemberID

	commit
go

----------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------

