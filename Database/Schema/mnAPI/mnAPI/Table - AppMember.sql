use [mnAPI]
go

if exists (select 1 from INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'AppMember')
      drop table [dbo].[AppMember]
      
go

/****** Object:  Table [dbo].[AppMember]    Script Date: 09/19/2011 14:25:26 ******/
set ansi_nulls on
go

set quoted_identifier on
go

create table [dbo].[AppMember](
	[AppID] [int] not null,
	[MemberID] [int] not null,
	[AccessToken] [nvarchar](100) not null,
	[AccessTokenExpirationDateTime] [smalldatetime] not null,
	[RefreshToken] [nvarchar](100) not null,
	[RefreshTokenExpirationDateTime] [smalldatetime] not null,
	[UpdateDateTime] [smalldatetime] not null,
	[InsertDateTime] [smalldatetime] not null,
 constraint [PK_AppMember_1] primary key clustered 
(
	[AppID] asc,
	[MemberID] asc
)with (pad_index  = off, statistics_norecompute  = off, ignore_dup_key = off, allow_row_locks  = on, allow_page_locks  = on) on [primary]
) on [primary]

create index IX_AppMember_AccessToken_RefreshToken on [dbo].[AppMember] (AccessToken asc, RefreshToken asc)

go

alter table [dbo].[AppMember] add  constraint [DF_AppMember_UpdateDateTime]  default (getdate()) for [UpdateDateTime]
go

alter table [dbo].[AppMember] add  constraint [DF_AppMember_InsertDateTime]  default (getdate()) for [InsertDateTime]
go

