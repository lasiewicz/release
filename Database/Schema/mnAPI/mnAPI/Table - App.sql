use [mnAPI]
go

if exists (select 1 from INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'App')
      drop table [dbo].App
go

/****** Object:  Table [dbo].[App]    Script Date: 09/12/2011 15:00:53 ******/
set ansi_nulls on
go

set quoted_identifier on
go



create table [dbo].[App](
	[AppID] [int] not null,
	[BrandID] [int] not null,
	[Status] [smallint] not null,
	[MemberID] [int] not null,
	[Secret] [nvarchar](50) not null,
	[RedirectUrl] [nvarchar](max) not null,
	[AutoAuth] [bit] not null,
	[Title] [nvarchar](50) not null,
	[Description] [nvarchar](50) not null,
	[InsertDateTime] [smalldatetime] not null,
	[UpdateDateTime] [smalldatetime] not null,
 constraint [PK_App] primary key clustered 
(
	[AppID] asc
)with (pad_index  = off, statistics_norecompute  = off, ignore_dup_key = off, allow_row_locks  = on, allow_page_locks  = on) on [primary]
) on [primary]

go

alter table [dbo].[App] add  constraint [DF_App_Status]  default ((0)) for [Status]
go

alter table [dbo].[App] add  constraint [DF_App_AutoAuth]  default ((0)) for [AutoAuth]
go

alter table [dbo].[App] add  constraint [DF_App_InsertDateTime]  default (getdate()) for [InsertDateTime]
go

alter table [dbo].[App] add  constraint [DF_App_UpdateDateTime]  default (getdate()) for [UpdateDateTime]
go


