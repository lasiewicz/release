/************** NEW TABLES *****************/
use [mnAPI]
go

IF EXISTS (select 1 from INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'AppMember')
      DROP TABLE [dbo].[AppMember]
GO

IF EXISTS (select 1 from INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'App')
      DROP TABLE [dbo].App
GO



/****** Object:  Table [dbo].[App]    Script Date: 09/12/2011 15:00:53 ******/
set ansi_nulls on
go

set quoted_identifier on
go



create table [dbo].[App](
	[AppID] [int] not null,
	[BrandID] [int] not null,
	[Status] [smallint] not null,
	[MemberID] [int] not null,
	[Secret] [nvarchar](50) not null,
	[RedirectUrl] [nvarchar](max) not null,
	[AutoAuth] [bit] not null,
	[Title] [nvarchar](50) not null,
	[Description] [nvarchar](50) not null,
	[InsertDateTime] [smalldatetime] not null,
	[UpdateDateTime] [smalldatetime] not null,
 constraint [PK_App] primary key clustered 
(
	[AppID] asc
)with (pad_index  = off, statistics_norecompute  = off, ignore_dup_key = off, allow_row_locks  = on, allow_page_locks  = on) on [primary]
) on [primary]

go

alter table [dbo].[App] add  constraint [DF_App_Status]  default ((0)) for [Status]
go

alter table [dbo].[App] add  constraint [DF_App_AutoAuth]  default ((0)) for [AutoAuth]
go

alter table [dbo].[App] add  constraint [DF_App_InsertDateTime]  default (getdate()) for [InsertDateTime]
go

alter table [dbo].[App] add  constraint [DF_App_UpdateDateTime]  default (getdate()) for [UpdateDateTime]
go


use [mnAPI]
go

/****** Object:  Table [dbo].[AppMember]    Script Date: 09/19/2011 14:25:26 ******/
set ansi_nulls on
go

set quoted_identifier on
go

create table [dbo].[AppMember](
	[AppID] [int] not null,
	[MemberID] [int] not null,
	[AccessToken] [nvarchar](100) not null,
	[AccessTokenExpirationDateTime] [smalldatetime] not null,
	[RefreshToken] [nvarchar](100) not null,
	[RefreshTokenExpirationDateTime] [smalldatetime] not null,
	[UpdateDateTime] [smalldatetime] not null,
	[InsertDateTime] [smalldatetime] not null,
 constraint [PK_AppMember_1] primary key clustered 
(
	[AppID] asc,
	[MemberID] asc
)with (pad_index  = off, statistics_norecompute  = off, ignore_dup_key = off, allow_row_locks  = on, allow_page_locks  = on) on [primary]
) on [primary]

create index IX_AppMember_AccessToken_RefreshToken on [dbo].[AppMember] (AccessToken asc, RefreshToken asc)

go

alter table [dbo].[AppMember] add  constraint [DF_AppMember_UpdateDateTime]  default (getdate()) for [UpdateDateTime]
go

alter table [dbo].[AppMember] add  constraint [DF_AppMember_InsertDateTime]  default (getdate()) for [InsertDateTime]
go


