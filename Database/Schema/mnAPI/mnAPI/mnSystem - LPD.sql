use mnSystem
go

if not exists(select 1 from logicaldatabase where logicaldatabasename = 'mnAPI')
begin

insert into LogicalDatabase (LogicalDatabaseID, LogicalDatabaseName, HydraModeID, HydraThreads, HydraFailureThreshold, HealthCheckProc)
values (80000, 'mnAPI', 2, 1, 4, '')

end

-- Different values for PRODUCTION!!!
select * from LogicalDatabase
select * from LogicalPhysicalDatabase
select * from PhysicalDatabase

insert into PhysicalDatabase(PhysicalDatabaseID, ServerName, PhysicalDatabaseName, ActiveFlag, MaintenanceTypeMask)
values (80000, 'DEVSQL01', 'mnAPI', 1, null)
insert into LogicalPhysicalDatabase(LogicalPhysicalDatabaseID, LogicalDatabaseID, PhysicalDatabaseID, Offset)
values (80000, 80000, 80000, 0)

