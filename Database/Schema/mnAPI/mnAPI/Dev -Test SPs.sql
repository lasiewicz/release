
execute [mnAPI].[dbo].[up_App_Insert] 
   1
  ,1003
  ,1
  ,16000206
  ,'mysecret'
  ,'someurl'
  ,1
  ,'mytitle'
  ,'mydescription'
go

[mnAPI].[dbo].[up_App_List] 
go

[mnAPI].[dbo].[up_App_Update] 
   1
  ,1003
  ,1
  ,16000206
  ,'updatedsecret'
  ,'updatedurl'
  ,1
  ,'updatedtitle'
  ,'updateddescription'
go

[mnAPI].[dbo].[up_App_List] 
go

execute [mnAPI].[dbo].[up_App_Delete] 
   1
go
  
[mnAPI].[dbo].[up_AppMember_Insert] 
   1
  ,16000206
  ,'myacesstoken'
  ,'2012-12-12 23:00'
  ,'myrefreshtoken'
  ,'2014-12-12 22:00'
go

[mnAPI].[dbo].[up_AppMember_List] 
   1
  ,16000206
go

[mnAPI].[dbo].[up_AppMember_Update] 
   1
  ,16000206
  ,'updatedaccesstoken'
  ,'2012-10-9 13:00'
  ,'updateaccesstoken'
  ,'2016-2-3 13:33'
go

[mnAPI].[dbo].[up_AppMember_List] 
   1
  ,16000206
go

execute [mnAPI].[dbo].[up_AppMember_Delete] 
   1,
   16000206
go
