/************ APP CRUD ************/
use [mnAPI];
go

if object_id('[dbo].[up_App_List]') is not null
begin 
    drop proc [dbo].[up_App_List] 
end 
go
create proc [dbo].[up_App_List] 

as 
	set nocount on 
	set xact_abort on  
	set transaction isolation level read UNCOMMITTED 

	begin tran

	select [AppID], [BrandID], [Status], [MemberID], [Secret], [RedirectUrl], [AutoAuth], [Title], [Description], [InsertDateTime], [UpdateDateTime] 
	from   [dbo].[App] (nolock)

	commit
go
if object_id('[dbo].[up_App_Insert]') is not null
begin 
    drop proc [dbo].[up_App_Insert] 
end 
go
create proc [dbo].[up_App_Insert] 
    @AppID int,
    @BrandID int,
    @Status smallint,
    @MemberID int,
    @Secret nvarchar(50),
    @RedirectUrl nvarchar(max),
    @AutoAuth bit,
    @Title nvarchar(50),
    @Description nvarchar(50)
as 
	set nocount on 
	set xact_abort on  
	set transaction isolation level read UNCOMMITTED 

	begin tran
	
	insert into [dbo].[App] ([AppID], [BrandID], [Status], [MemberID], [Secret], [RedirectUrl], [AutoAuth], [Title], [Description], [InsertDateTime], [UpdateDateTime])
	select @AppID, @BrandID, @Status, @MemberID, @Secret, @RedirectUrl, @AutoAuth, @Title, @Description, getdate(), getdate()
	
	/*/*/*-- Begin Return Select <- do not remove
	select [AppID], [BrandID], [Status], [MemberID], [Secret], [RedirectUrl], [AutoAuth], [Title], [Description], [InsertDateTime], [UpdateDateTime]
	from   [dbo].[App]
	where  [AppID] = @AppID
	-- End Return Select <- do not remove*/*/*/
               
	commit
go
if object_id('[dbo].[up_App_Update]') is not null
begin 
    drop proc [dbo].[up_App_Update] 
end 
go
create proc [dbo].[up_App_Update] 
    @AppID int,
    @BrandID int,
    @Status smallint,
    @MemberID int,
    @Secret nvarchar(50),
    @RedirectUrl nvarchar(max),
    @AutoAuth bit,
    @Title nvarchar(50),
    @Description nvarchar(50)
as 
	set nocount on 
	set xact_abort on  
	set transaction isolation level read UNCOMMITTED 

	begin tran

	update [dbo].[App]
	set    [AppID] = @AppID, [BrandID] = @BrandID, [Status] = @Status, [MemberID] = @MemberID, [Secret] = @Secret, [RedirectUrl] = @RedirectUrl, [AutoAuth] = @AutoAuth, [Title] = @Title, [Description] = @Description,  [UpdateDateTime] = getdate()
	where  [AppID] = @AppID
	
	/*/*/*-- Begin Return Select <- do not remove
	select [AppID], [BrandID], [Status], [MemberID], [Secret], [RedirectUrl], [AutoAuth], [Title], [Description], [InsertDateTime], [UpdateDateTime]
	from   [dbo].[App]
	where  [AppID] = @AppID	
	-- End Return Select <- do not remove*/*/*/

	commit tran
go
if object_id('[dbo].[up_App_Delete]') is not null
begin 
    drop proc [dbo].[up_App_Delete] 
end 
go
create proc [dbo].[up_App_Delete] 
    @AppID int
as 
	set nocount on 
	set xact_abort on  
	set transaction isolation level read UNCOMMITTED 
	
	begin tran

	delete
	from   [dbo].[App]
	where  [AppID] = @AppID

	commit
go

----------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------

exec mnapi..up_grantexecute
