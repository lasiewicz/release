
  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D23YNMList_List0]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D23YNMList0 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D23YNMList_List0] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D23YNMList_MutualMail_List0]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D23YNMList0 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D23YNMList_MutualMail_List0] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D23YNMList_Save0]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D23YNMList0 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D23YNMList0 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D23YNMList0 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D23YNMList_Save0] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D23YNMList_Status_Save0]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D23YNMList0 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D23YNMList_Status_Save0] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D23YNMList_ViralMail_List0]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D23YNMList0 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D23YNMList_ViralMail_List0] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D23YNMList_List1]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D23YNMList1 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D23YNMList_List1] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D23YNMList_MutualMail_List1]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D23YNMList1 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D23YNMList_MutualMail_List1] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D23YNMList_Save1]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D23YNMList1 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D23YNMList1 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D23YNMList1 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D23YNMList_Save1] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D23YNMList_Status_Save1]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D23YNMList1 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D23YNMList_Status_Save1] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D23YNMList_ViralMail_List1]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D23YNMList1 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D23YNMList_ViralMail_List1] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D23YNMList_List2]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D23YNMList2 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D23YNMList_List2] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D23YNMList_MutualMail_List2]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D23YNMList2 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D23YNMList_MutualMail_List2] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D23YNMList_Save2]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D23YNMList2 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D23YNMList2 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D23YNMList2 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D23YNMList_Save2] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D23YNMList_Status_Save2]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D23YNMList2 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D23YNMList_Status_Save2] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D23YNMList_ViralMail_List2]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D23YNMList2 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D23YNMList_ViralMail_List2] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D23YNMList_List3]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D23YNMList3 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D23YNMList_List3] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D23YNMList_MutualMail_List3]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D23YNMList3 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D23YNMList_MutualMail_List3] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D23YNMList_Save3]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D23YNMList3 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D23YNMList3 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D23YNMList3 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D23YNMList_Save3] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D23YNMList_Status_Save3]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D23YNMList3 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D23YNMList_Status_Save3] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D23YNMList_ViralMail_List3]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D23YNMList3 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D23YNMList_ViralMail_List3] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D23YNMList_List4]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D23YNMList4 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D23YNMList_List4] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D23YNMList_MutualMail_List4]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D23YNMList4 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D23YNMList_MutualMail_List4] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D23YNMList_Save4]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D23YNMList4 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D23YNMList4 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D23YNMList4 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D23YNMList_Save4] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D23YNMList_Status_Save4]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D23YNMList4 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D23YNMList_Status_Save4] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D23YNMList_ViralMail_List4]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D23YNMList4 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D23YNMList_ViralMail_List4] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D23YNMList_List5]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D23YNMList5 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D23YNMList_List5] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D23YNMList_MutualMail_List5]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D23YNMList5 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D23YNMList_MutualMail_List5] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D23YNMList_Save5]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D23YNMList5 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D23YNMList5 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D23YNMList5 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D23YNMList_Save5] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D23YNMList_Status_Save5]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D23YNMList5 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D23YNMList_Status_Save5] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D23YNMList_ViralMail_List5]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D23YNMList5 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D23YNMList_ViralMail_List5] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D23YNMList_List6]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D23YNMList6 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D23YNMList_List6] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D23YNMList_MutualMail_List6]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D23YNMList6 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D23YNMList_MutualMail_List6] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D23YNMList_Save6]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D23YNMList6 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D23YNMList6 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D23YNMList6 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D23YNMList_Save6] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D23YNMList_Status_Save6]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D23YNMList6 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D23YNMList_Status_Save6] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D23YNMList_ViralMail_List6]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D23YNMList6 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D23YNMList_ViralMail_List6] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D23YNMList_List7]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D23YNMList7 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D23YNMList_List7] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D23YNMList_MutualMail_List7]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D23YNMList7 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D23YNMList_MutualMail_List7] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D23YNMList_Save7]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D23YNMList7 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D23YNMList7 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D23YNMList7 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D23YNMList_Save7] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D23YNMList_Status_Save7]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D23YNMList7 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D23YNMList_Status_Save7] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D23YNMList_ViralMail_List7]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D23YNMList7 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D23YNMList_ViralMail_List7] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D23YNMList_List8]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D23YNMList8 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D23YNMList_List8] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D23YNMList_MutualMail_List8]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D23YNMList8 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D23YNMList_MutualMail_List8] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D23YNMList_Save8]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D23YNMList8 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D23YNMList8 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D23YNMList8 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D23YNMList_Save8] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D23YNMList_Status_Save8]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D23YNMList8 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D23YNMList_Status_Save8] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D23YNMList_ViralMail_List8]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D23YNMList8 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D23YNMList_ViralMail_List8] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D23YNMList_List9]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D23YNMList9 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D23YNMList_List9] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D23YNMList_MutualMail_List9]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D23YNMList9 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D23YNMList_MutualMail_List9] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D23YNMList_Save9]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D23YNMList9 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D23YNMList9 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D23YNMList9 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D23YNMList_Save9] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D23YNMList_Status_Save9]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D23YNMList9 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D23YNMList_Status_Save9] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D23YNMList_ViralMail_List9]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D23YNMList9 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D23YNMList_ViralMail_List9] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D23YNMList_List10]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D23YNMList10 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D23YNMList_List10] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D23YNMList_MutualMail_List10]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D23YNMList10 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D23YNMList_MutualMail_List10] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D23YNMList_Save10]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D23YNMList10 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D23YNMList10 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D23YNMList10 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D23YNMList_Save10] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D23YNMList_Status_Save10]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D23YNMList10 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D23YNMList_Status_Save10] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D23YNMList_ViralMail_List10]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D23YNMList10 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D23YNMList_ViralMail_List10] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D23YNMList_List11]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D23YNMList11 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D23YNMList_List11] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D23YNMList_MutualMail_List11]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D23YNMList11 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D23YNMList_MutualMail_List11] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D23YNMList_Save11]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D23YNMList11 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D23YNMList11 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D23YNMList11 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D23YNMList_Save11] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D23YNMList_Status_Save11]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D23YNMList11 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D23YNMList_Status_Save11] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D23YNMList_ViralMail_List11]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D23YNMList11 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D23YNMList_ViralMail_List11] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D23YNMList_List12]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D23YNMList12 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D23YNMList_List12] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D23YNMList_MutualMail_List12]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D23YNMList12 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D23YNMList_MutualMail_List12] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D23YNMList_Save12]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D23YNMList12 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D23YNMList12 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D23YNMList12 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D23YNMList_Save12] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D23YNMList_Status_Save12]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D23YNMList12 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D23YNMList_Status_Save12] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D23YNMList_ViralMail_List12]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D23YNMList12 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D23YNMList_ViralMail_List12] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D23YNMList_List13]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D23YNMList13 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D23YNMList_List13] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D23YNMList_MutualMail_List13]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D23YNMList13 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D23YNMList_MutualMail_List13] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D23YNMList_Save13]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D23YNMList13 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D23YNMList13 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D23YNMList13 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D23YNMList_Save13] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D23YNMList_Status_Save13]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D23YNMList13 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D23YNMList_Status_Save13] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D23YNMList_ViralMail_List13]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D23YNMList13 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D23YNMList_ViralMail_List13] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D23YNMList_List14]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D23YNMList14 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D23YNMList_List14] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D23YNMList_MutualMail_List14]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D23YNMList14 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D23YNMList_MutualMail_List14] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D23YNMList_Save14]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D23YNMList14 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D23YNMList14 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D23YNMList14 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D23YNMList_Save14] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D23YNMList_Status_Save14]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D23YNMList14 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D23YNMList_Status_Save14] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D23YNMList_ViralMail_List14]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D23YNMList14 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D23YNMList_ViralMail_List14] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D23YNMList_List15]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D23YNMList15 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D23YNMList_List15] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D23YNMList_MutualMail_List15]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D23YNMList15 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D23YNMList_MutualMail_List15] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D23YNMList_Save15]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D23YNMList15 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D23YNMList15 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D23YNMList15 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D23YNMList_Save15] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D23YNMList_Status_Save15]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D23YNMList15 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D23YNMList_Status_Save15] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D23YNMList_ViralMail_List15]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D23YNMList15 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D23YNMList_ViralMail_List15] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D23YNMList_List16]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D23YNMList16 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D23YNMList_List16] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D23YNMList_MutualMail_List16]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D23YNMList16 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D23YNMList_MutualMail_List16] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D23YNMList_Save16]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D23YNMList16 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D23YNMList16 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D23YNMList16 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D23YNMList_Save16] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D23YNMList_Status_Save16]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D23YNMList16 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D23YNMList_Status_Save16] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D23YNMList_ViralMail_List16]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D23YNMList16 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D23YNMList_ViralMail_List16] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D23YNMList_List17]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D23YNMList17 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D23YNMList_List17] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D23YNMList_MutualMail_List17]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D23YNMList17 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D23YNMList_MutualMail_List17] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D23YNMList_Save17]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D23YNMList17 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D23YNMList17 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D23YNMList17 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D23YNMList_Save17] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D23YNMList_Status_Save17]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D23YNMList17 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D23YNMList_Status_Save17] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D23YNMList_ViralMail_List17]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D23YNMList17 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D23YNMList_ViralMail_List17] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D23YNMList_List18]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D23YNMList18 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D23YNMList_List18] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D23YNMList_MutualMail_List18]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D23YNMList18 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D23YNMList_MutualMail_List18] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D23YNMList_Save18]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D23YNMList18 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D23YNMList18 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D23YNMList18 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D23YNMList_Save18] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D23YNMList_Status_Save18]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D23YNMList18 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D23YNMList_Status_Save18] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D23YNMList_ViralMail_List18]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D23YNMList18 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D23YNMList_ViralMail_List18] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D23YNMList_List19]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D23YNMList19 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D23YNMList_List19] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D23YNMList_MutualMail_List19]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D23YNMList19 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D23YNMList_MutualMail_List19] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D23YNMList_Save19]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D23YNMList19 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D23YNMList19 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D23YNMList19 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D23YNMList_Save19] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D23YNMList_Status_Save19]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D23YNMList19 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D23YNMList_Status_Save19] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D23YNMList_ViralMail_List19]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D23YNMList19 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D23YNMList_ViralMail_List19] to public
go
