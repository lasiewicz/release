
--    SET IDENTITY_INSERT mnKey1.dbo.[ResourceTemplateID] ON   
--    INSERT INTO [mnKey1].[dbo].[ResourceTemplateID]
--([PKID],[InputValue] ,[InsertDate])
-- VALUES
-- ('3057',1 ,GETDATE())        
--  SET IDENTITY_INSERT mnKey1.dbo.[ResourceTemplateID] off   


use mnSubscription 
go


    insert into [mnsubscription].[dbo].[resourcetemplate]
([resourcetemplateid],[content],[description],[groupid],[groupdefault],[resourcetemplatetypeid],[insertdatetime],[updatedatetime])
 values
('2976',N'<div class="cell-row-header classic">
<h2>{0} Month</h2>
</div>',N'AS Classic row header (1 month)','90410','0','1',getdate(),getdate())
    

    insert into [mnsubscription].[dbo].[resourcetemplate]
([resourcetemplateid],[content],[description],[groupid],[groupdefault],[resourcetemplatetypeid],[insertdatetime],[updatedatetime])
 values
('2979',N'<div class="cell-plan classic shade"> <h2>${0} <span class="per-month">per month</span></h2> <p class="fine-print">one payment of ${1}</p></div>',N'AS Classic plan (ch) - shade','90410','0','3',getdate(),getdate())
    

    insert into [mnsubscription].[dbo].[resourcetemplate]
([resourcetemplateid],[content],[description],[groupid],[groupdefault],[resourcetemplatetypeid],[insertdatetime],[updatedatetime])
 values
('2982',N'<div class="cell-plan classic strong"> <h2>${0} <span class="per-month">per month</span></h2> <p class="fine-print">one payment of ${1}</p></div>',N'AS Classic plan (ch) - strong','90410','0','3',getdate(),getdate())
    

    insert into [mnsubscription].[dbo].[resourcetemplate]
([resourcetemplateid],[content],[description],[groupid],[groupdefault],[resourcetemplatetypeid],[insertdatetime],[updatedatetime])
 values
('2985',N'<div class="cell-plan classic strongest"> <h2>${0} <span class="per-month">per month</span></h2> <p class="fine-print">one payment of ${1}</p></div>',N'AS Classic plan (ch) - strongest','90410','0','3',getdate(),getdate())
    

    insert into [mnsubscription].[dbo].[resourcetemplate]
([resourcetemplateid],[content],[description],[groupid],[groupdefault],[resourcetemplatetypeid],[insertdatetime],[updatedatetime])
 values
('2988',N'',N'[x] unused','90410','0','1',getdate(),getdate())
    

    insert into [mnsubscription].[dbo].[resourcetemplate]
([resourcetemplateid],[content],[description],[groupid],[groupdefault],[resourcetemplatetypeid],[insertdatetime],[updatedatetime])
 values
('2991',N'',N'[x] unused','90410','0','1',getdate(),getdate())
    

    insert into [mnsubscription].[dbo].[resourcetemplate]
([resourcetemplateid],[content],[description],[groupid],[groupdefault],[resourcetemplatetypeid],[insertdatetime],[updatedatetime])
 values
('2994',N'<div class="cell-row-header classic shade"> <h2>{0} Months</h2> </div>',N'AS Classic row header - shaded','90410','0','1',getdate(),getdate())
    

    insert into [mnsubscription].[dbo].[resourcetemplate]
([resourcetemplateid],[content],[description],[groupid],[groupdefault],[resourcetemplatetypeid],[insertdatetime],[updatedatetime])
 values
('2997',N'<div class="cell-row-header classic"> <h2>{0} Months</h2> </div>',N'AS Classic row header','90410','0','1',getdate(),getdate())
    

    insert into [mnsubscription].[dbo].[resourcetemplate]
([resourcetemplateid],[content],[description],[groupid],[groupdefault],[resourcetemplatetypeid],[insertdatetime],[updatedatetime])
 values
('3000',N'<div class="cell-col-header classic premium">  
	<h2>Premium Plans</h2>  
	<p>Includes</p>  
	<ul class="rowHeaderInfoOutside">   
		<li>Member Spotlight</li>   
		<li>Highlighted Profile</li>  
	</ul>    
	<div class="rowHeaderHPDetails" onmouseover="TogglePopupDiv(''listOfBenefits_details''); return false;" onmouseout="TogglePopupDiv(''listOfBenefits_details''); return false;">   
		<img src="((image:sub_memberSpotlight_mediumThumb.gif))">   
		<a href="#" onclick="return false">Details</a>  
	</div> 
</div>  
<div id="listOfBenefits_details" class="helpLayerContainerB">  
	<div class="helpLayerMiddle">   
		<div class="helpLayerInner lightest borderDark">    
			<h2>List of benefits</h2>    
			<img align="absmiddle" src="((image:sub_listOfBenefitsChart.gif))" border="0" />   
		</div>  
	</div> 
</div>',N'AS Classic column header - premium','90410','0','2',getdate(),getdate())
    

    insert into [mnsubscription].[dbo].[resourcetemplate]
([resourcetemplateid],[content],[description],[groupid],[groupdefault],[resourcetemplatetypeid],[insertdatetime],[updatedatetime])
 values
('3003',N'<div class="cell-plan classic"> <h2>${0} <span class="per-month">per month</span></h2> <p class="fine-print">one payment of ${1}</p></div>',N'AS Classic plan (ch)','90410','0','3',getdate(),getdate())
    

    insert into [mnsubscription].[dbo].[resourcetemplate]
([resourcetemplateid],[content],[description],[groupid],[groupdefault],[resourcetemplatetypeid],[insertdatetime],[updatedatetime])
 values
('3006',N'<div class="cell-col-header classic" style="background-color:#FFF9E3;"><h2>Standard Plans</h2>  <div class="rowHeaderHPDetails" onmouseover="TogglePopupDiv(''listOfBenefitsD'');TogglePopupDivDisplay(''listOfBenefitsText'');" onmouseout="TogglePopupDiv(''listOfBenefitsD'');TogglePopupDivDisplay(''listOfBenefitsText'');">   <a href="#" onclick="return false">Details</a>  </div> </div>  <div id="listOfBenefitsD" class="helpLayerContainerB">  <div class="helpLayerMiddle">   <div class="helpLayerInner lightest borderDark">    <h2>List of benefits</h2>    <img align="absmiddle" src="/img/Community/AmericanSingles/sub_listOfBenefitsWithoutPremiumChart.gif" border="0" />   </div>  </div> </div>',N'AS Classic column header - standard','90410','0','2',getdate(),getdate())
    

    insert into [mnsubscription].[dbo].[resourcetemplate]
([resourcetemplateid],[content],[description],[groupid],[groupdefault],[resourcetemplatetypeid],[insertdatetime],[updatedatetime])
 values
('3009',N'<div class="cell-plan classic"> <h2>${0} <span class="per-month">per month</span></h2> <p class="fine-print">w/${2} monthly renewal*</p> </div>',N'AS Classic plan (1 month)','90410','0','3',getdate(),getdate())
    

    insert into [mnsubscription].[dbo].[resourcetemplate]
([resourcetemplateid],[content],[description],[groupid],[groupdefault],[resourcetemplatetypeid],[insertdatetime],[updatedatetime])
 values
('3012',N'<div class="cell-plan classic shade"> <h2>${0} <span class="per-month">per month</span></h2> <p class="fine-print">w/${2} monthly renewal*</p> </div>',N'AS Classic plan (1 month) - shade','90410','0','3',getdate(),getdate())
    

    insert into [mnsubscription].[dbo].[resourcetemplate]
([resourcetemplateid],[content],[description],[groupid],[groupdefault],[resourcetemplatetypeid],[insertdatetime],[updatedatetime])
 values
('3015',N'<div class="cell-plan classic strong"> <h2>${0} <span class="per-month">per month</span></h2> <p class="fine-print">w/${2} monthly renewal*</p> </div>',N'AS Classic plan (1 month) - strong','90410','0','3',getdate(),getdate())
    

    insert into [mnsubscription].[dbo].[resourcetemplate]
([resourcetemplateid],[content],[description],[groupid],[groupdefault],[resourcetemplatetypeid],[insertdatetime],[updatedatetime])
 values
('3018',N'<div class="cell-plan classic strongest"> <h2>${0} <span class="per-month">per month</span></h2> <p class="fine-print">w/${2} monthly renewal*</p> </div>',N'AS Classic plan (1 month) - strongest','90410','0','3',getdate(),getdate())
    

    insert into [mnsubscription].[dbo].[resourcetemplate]
([resourcetemplateid],[content],[description],[groupid],[groupdefault],[resourcetemplatetypeid],[insertdatetime],[updatedatetime])
 values
('3021',N'<div class="cell-plan classic"> <h2>${0} <span class="per-month">per month, plus tax</span></h2> <p class="fine-print">one payment of ${1}<br/> w/${2} monthly renewal*</p> </div>',N'[x] unused','90410','0','3',getdate(),getdate())
    

    insert into [mnsubscription].[dbo].[resourcetemplate]
([resourcetemplateid],[content],[description],[groupid],[groupdefault],[resourcetemplatetypeid],[insertdatetime],[updatedatetime])
 values
('3024',N'<div class="cell-plan classic shade">  <h2>${0} <span class="per-month">per month</span></h2>  <p class="fine-print">one payment of ${1}<br/>  w/${2} monthly renewal*</p> </div>',N'AS Classic plan - shade','90410','0','3',getdate(),getdate())
    

    insert into [mnsubscription].[dbo].[resourcetemplate]
([resourcetemplateid],[content],[description],[groupid],[groupdefault],[resourcetemplatetypeid],[insertdatetime],[updatedatetime])
 values
('3027',N'<div class="cell-plan classic">  <h2>${0} <span class="per-month">per month</span></h2>  <p class="fine-print">one payment of ${1}<br/>  w/${2} monthly renewal*</p> </div>',N'AS Classic plan','90410','0','3',getdate(),getdate())
    

    insert into [mnsubscription].[dbo].[resourcetemplate]
([resourcetemplateid],[content],[description],[groupid],[groupdefault],[resourcetemplatetypeid],[insertdatetime],[updatedatetime])
 values
('3030',N'<div class="cell-plan classic strongest">  <h2>${0} <span class="per-month">per month</span></h2>  <p class="fine-print">one payment of ${1}<br/>  w/${2} monthly renewal*</p> </div>',N'AS Classic plan - strongest','90410','0','3',getdate(),getdate())
    

    insert into [mnsubscription].[dbo].[resourcetemplate]
([resourcetemplateid],[content],[description],[groupid],[groupdefault],[resourcetemplatetypeid],[insertdatetime],[updatedatetime])
 values
('3033',N'<div class="cell-plan classic strong">  <h2>${0} <span class="per-month">per month</span></h2>  <p class="fine-print">one payment of ${1}<br/>  w/${2} monthly renewal*</p> </div>',N'AS Classic plan - strong','90410','0','3',getdate(),getdate())
    

    insert into [mnsubscription].[dbo].[resourcetemplate]
([resourcetemplateid],[content],[description],[groupid],[groupdefault],[resourcetemplatetypeid],[insertdatetime],[updatedatetime])
 values
('3036',N'<div class="cell-row-header classic shade">
<h2>{0} Month</h2>
</div>',N'AS Classic row header - shaded (1 month)','90410','0','1',getdate(),getdate())
    

    insert into [mnsubscription].[dbo].[resourcetemplate]
([resourcetemplateid],[content],[description],[groupid],[groupdefault],[resourcetemplatetypeid],[insertdatetime],[updatedatetime])
 values
('3039',N'<div class="cell-col-header classic premium">
	<h2>Premium Plans</h2>
	<p>Includes</p>
	<ul class="rowHeaderInfoOutside">
	<li>Member Spotlight</li>
	</ul>

    <div class="rowHeaderHPDetails" onmouseover="TogglePopupDiv(''listOfBenefits_details_spotlight''); return false;" onmouseout="TogglePopupDiv(''listOfBenefits_details_spotlight''); return false;">
		<img src="/img/Community/AmericanSingles/sub_memberSpotlight_mediumThumb.gif">
		<a href="#" onclick="return false">Details</a>
	</div>
</div>

<div id="listOfBenefits_details_spotlight" class="helpLayerContainerB">
	<div class="helpLayerMiddle">
		<div class="helpLayerInner lightest borderDark">
	    <h2>List of benefits</h2>
	    <img align="absmiddle" src="/img/Community/AmericanSingles/sub_listOfBenefitsChart_spotlight.gif" border="0" />
		</div>
	</div>
</div>',N'AS Classic column header - member spotlight','90410','0','2',getdate(),getdate())
    

    insert into [mnsubscription].[dbo].[resourcetemplate]
([resourcetemplateid],[content],[description],[groupid],[groupdefault],[resourcetemplatetypeid],[insertdatetime],[updatedatetime])
 values
('3042',N'<div class="cell-col-header classic premium">
	<h2>Premium Plans</h2>
	<p>Includes</p>
	<ul class="rowHeaderInfoOutside">
		<li>Highlighted Profile</li>
	</ul>

    <div class="rowHeaderHPDetails" onmouseover="TogglePopupDiv(''listOfBenefits_details_highlighted''); return false;" onmouseout="TogglePopupDiv(''listOfBenefits_details_highlighted''); return false;">
		<img src="/img/Community/AmericanSingles/sub_profileHighlight_mediumThumb.gif">
		<a href="#" onclick="return false">Details</a>
	</div>
</div>

<div id="listOfBenefits_details_highlighted" class="helpLayerContainerB">
	<div class="helpLayerMiddle">
		<div class="helpLayerInner lightest borderDark">
	    <h2>List of benefits</h2>
	    <img align="absmiddle" src="/img/Community/AmericanSingles/sub_listOfBenefitsChart_highlight.gif" border="0" />
		</div>
	</div>
</div>',N'AS Classic column header - highlighted profile','90410','0','2',getdate(),getdate())
    

    insert into [mnsubscription].[dbo].[resourcetemplate]
([resourcetemplateid],[content],[description],[groupid],[groupdefault],[resourcetemplatetypeid],[insertdatetime],[updatedatetime])
 values
('3045',N'<div class="cell-plan classic"> 
	<h2><img alt="" src="/img/site/americansingles-com/sub-price-strike-2499.gif " style="vertical-align:middle;"/> ${0} <span class="per-month">per month</span></h2> 
	<p class="fine-print">one payment of ${1}<br/> 
	w/${2} monthly renewal*</p> 
</div>',N'AS Classic plan - strike - 2499','90410','0','3',getdate(),getdate())
    

    insert into [mnsubscription].[dbo].[resourcetemplate]
([resourcetemplateid],[content],[description],[groupid],[groupdefault],[resourcetemplatetypeid],[insertdatetime],[updatedatetime])
 values
('3048',N'<div class="cell-plan classic"> 
	<h2><img alt="" src="/img/site/americansingles-com/sub-price-strike-5994.gif " style="vertical-align:middle;"/> ${0} <span class="per-month">per month</span></h2> 
	<p class="fine-print">one payment of ${1}<br/> 
	w/${2} monthly renewal*</p> 
</div>',N'AS Classic plan - strike - 5994','90410','0','3',getdate(),getdate())
    

    insert into [mnsubscription].[dbo].[resourcetemplate]
([resourcetemplateid],[content],[description],[groupid],[groupdefault],[resourcetemplatetypeid],[insertdatetime],[updatedatetime])
 values
('3051',N'<div class="cell-plan classic"> 
	<h2><img alt="" src="/img/site/americansingles-com/sub-price-strike-3999.gif " style="vertical-align:middle;"/> ${0} <span class="per-month">per month</span></h2> 
	<p class="fine-print">one payment of ${1}<br/> 
	w/${2} monthly renewal*</p> 
</div>',N'AS Classic plan - strike - 3999','90410','0','3',getdate(),getdate())
    

    insert into [mnsubscription].[dbo].[resourcetemplate]
([resourcetemplateid],[content],[description],[groupid],[groupdefault],[resourcetemplatetypeid],[insertdatetime],[updatedatetime])
 values
('3054',N'<div class="cell-plan classic strongest">
	<h2 style="color: #9c042a;padding-top:4px">Buy 6 months, get 6 months <span style="color:#9c042a">FREE!</span></h2>
	<p class="fine-print">One payment of ${1}<br/>
	w/${2} monthly renewal*</p>
</div>',N'SP Plan Buy 6 months, get 6 months FREE!','90410','0','3',getdate(),getdate())
    

    insert into [mnsubscription].[dbo].[resourcetemplate]
([resourcetemplateid],[content],[description],[groupid],[groupdefault],[resourcetemplatetypeid],[insertdatetime],[updatedatetime])
 values
('3057',N'<div class="cell-plan classic shaded">
	<h2 style="padding-top:4px">Buy 3 months, get 3 months <span style="color:#9c042a">FREE!</span></h2>
	<p class="fine-print">One payment of ${1}<br/>
	w/${2} monthly renewal*</p>
</div>',N'SP Plan Buy 3 months, get 3 months FREE!','90410','0','3',getdate(),getdate())
    