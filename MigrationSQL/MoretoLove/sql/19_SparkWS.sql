use mnSystem

go
select * from AccessTicket

insert into ProfileSectionDefinition values (32, 'Mingle profile data MoreToLove')
insert into AccessTicketProfileSection values (7, 32)
insert into ProfileSectionDefinitionCommunity values (32, 23)
insert into ProfileSectionDefinitionSite values (32, 9041)
insert into ProfileSectionDefinitionPhoto values (32, 23)
insert into ProfileSectionDefinitionAttribute values(32,12,4,0,0,0,2)
insert into ProfileSectionDefinitionAttribute values(32,69,1,23,0,0,0)
insert into ProfileSectionDefinitionAttribute values(32,70,2,0,0,0,0)
insert into ProfileSectionDefinitionAttribute values(32,120,2,23,0,0,0)
insert into ProfileSectionDefinitionAttribute values(32,122,1,0,0,0,0)
insert into ProfileSectionDefinitionAttribute values(32,268,1,0,0,0,0)
insert into ProfileSectionDefinitionAttribute values(32,270,1,23,0,0,0)
insert into ProfileSectionDefinitionAttribute values(32,302,4,0,0,0,2)
insert into ProfileSectionDefinitionAttribute values(32,495,2,23,9041,90410,0)
insert into ProfileSectionDefinitionAttribute values(32,497,2,23,9041,90410,0)
insert into ProfileSectionDefinitionAttribute values(32,501,2,23,9041,0,0)
insert into ProfileSectionDefinitionAttribute values(32,506,1,23,0,0,0)
insert into ProfileSectionDefinitionAttribute values(32,509,1,0,0,0,0)
insert into ProfileSectionDefinitionAttribute values(32,570,1,0,0,0,0)

insert into ProfileSectionDefinition values (33, 'Get demographic data More To Love')
insert into ProfileSectionDefinitionCommunity values (33, 23)
insert into ProfileSectionDefinitionAttribute values(33,69,1,23,0,0,0)
insert into ProfileSectionDefinitionAttribute values(33,70,2,0,0,0,0)
insert into ProfileSectionDefinitionAttribute values(33,88,1,0,0,0,0)
insert into ProfileSectionDefinitionAttribute values(33,122,1,0,0,0,0)