  SET IDENTITY_INSERT CreditCardGroup ON 


    --cc types 'American Express'

  insert into [mnsystem].[dbo].[creditcardgroup]
  ([creditcardgroupid],[creditcardid],[groupid],[updatedate])
  values
  ('151','1','9041',getdate())
  

    --cc types 'Visa'

  insert into [mnsystem].[dbo].[creditcardgroup]
  ([creditcardgroupid],[creditcardid],[groupid],[updatedate])
  values
  ('152','64','9041',getdate())
  

    --cc types 'MasterCard'

  insert into [mnsystem].[dbo].[creditcardgroup]
  ([creditcardgroupid],[creditcardid],[groupid],[updatedate])
  values
  ('153','32','9041',getdate())
  

    --cc types 'Discover'

  insert into [mnsystem].[dbo].[creditcardgroup]
  ([creditcardgroupid],[creditcardid],[groupid],[updatedate])
  values
  ('154','8','9041',getdate())
  