--community group 

/*insert into [mnsystem].[dbo].[group]([groupid],[groupname],[scopeid],[updatedate])
  values
  ('23','BBWPersonalsPlus','100',getdate())
  
      insert into [mnsystem].[dbo].[community]
([communityid],[communityname],[insertdate],[updatedate])
values
('23','BBWPersonalsPlus',getdate(),getdate())
insert into [mnsystem].[dbo].[group]([groupid],[groupname],[scopeid],[updatedate])
  values
  ('9041','BBWPersonalsPlus.com','1000',getdate())
  
  
insert into [mnsystem].[dbo].[group]([groupid],[groupname],[scopeid],[updatedate])
  values
  ('90410','BBWPersonalsPlus.com','10000',getdate())
  
  insert into [mnsystem].[dbo].[brand]
([brandid],[siteid],[uri],[statusmask],[phonenumber],[defaultagemin],[defaultagemax],[defaultsearchradius],[insertdate],[updatedate])
values
 ('90410', '9041', 'BBWPersonalsPlus.com', '3081', '(888) 854-3803', '35', '44', '40', getdate(), getdate())
 
 insert into [mnsystem].[dbo].[site]
 ([siteid],[communityid],[sitename],[currencyid],[languageid],[culturename],[defaultregionid],[searchtypemask],[defaultsearchtypeid],
 [csspath],[charset],[direction],[gmtoffset],[defaulthost] ,[sslurl],[paymenttypemask],[insertdate],[updatedate])
  values
  ('9041', '23', 'BBWPersonalsPlus.com', '1', '2', 'en-US', '223', '7', '1', '/lib/css/as.css', '1252', 'ltr', '-8', 'www' ,'https://www.bbwpersonalsplus.com', '3',  getdate(), getdate())
  
*/
--community group
  update [mnsystem].[dbo].[group]
  set GroupName='BBWPersonalsPlus'
  where GroupID=23
     
--site group 
update [mnsystem].[dbo].[group]
  set GroupName='BBWPersonalsPlus.com'
  where GroupID=9041


--brand group
    update [mnsystem].[dbo].[group]
  set GroupName='BBWPersonalsPlus.com'
  where GroupID=90410
  
  --community record
  update [mnsystem].[dbo].community
  set communityname='BBWPersonalsPlus'
  where communityid=23

--site record 

      update [mnsystem].[dbo].[site]
  set [sitename]='BBWPersonalsPlus.com',
  [sslurl]='https://www.bbwpersonalsplus.com'
  where [siteid]=9041
  
--brand record 

        update [mnsystem].[dbo].[brand]
  set [uri]='BBWPersonalsPlus.com'
  where [brandid]=90410