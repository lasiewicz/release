use mnSystem
go

declare @attributeid  int

--attribute  'SiteFirstName'
    select @attributeid=attributeid from attribute
    where attributename='SiteFirstName'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3114',@attributeid,'9041','4','1','100'
    ,'False',null,getdate(),null)
   
--attribute  'HighlightedFlag'
    select @attributeid=attributeid from attribute
    where attributename='HighlightedFlag'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3115',@attributeid,'9041','0','1','0'
    ,'False','0',getdate(),null)
   
--attribute  'JMeterFlag'
    select @attributeid=attributeid from attribute
    where attributename='JMeterFlag'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3116',@attributeid,'9041','0','1','0'
    ,'False','0',getdate(),null)
   
--attribute  'ForcedPageTimedPromotionVersion'
    select @attributeid=attributeid from attribute
    where attributename='ForcedPageTimedPromotionVersion'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3117',@attributeid,'9041','3','1','0'
    ,'False','0',getdate(),null)
   
--attribute  'MiniSearchMarketingFlag'
    select @attributeid=attributeid from attribute
    where attributename='MiniSearchMarketingFlag'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3118',@attributeid,'9041','0','1','0'
    ,'False','0',getdate(),null)
   
--attribute  'SubscriptionExpirationDate'
    select @attributeid=attributeid from attribute
    where attributename='SubscriptionExpirationDate'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3119',@attributeid,'9041','1','1','0'
    ,'False',null,getdate(),null)
   
--attribute  'SubscriptionLastInitialPurchaseDate'
    select @attributeid=attributeid from attribute
    where attributename='SubscriptionLastInitialPurchaseDate'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3120',@attributeid,'9041','1','1','0'
    ,'False',null,getdate(),null)
   
--attribute  'LastBrandID'
    select @attributeid=attributeid from attribute
    where attributename='LastBrandID'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3121',@attributeid,'9041','3','1','0'
    ,'False','91610',getdate(),null)
   
--attribute  'SpotlightExpirationDate'
    select @attributeid=attributeid from attribute
    where attributename='SpotlightExpirationDate'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3122',@attributeid,'9041','1','1','0'
    ,'False',null,getdate(),null)
   
--attribute  'SubscriptionStatus'
    select @attributeid=attributeid from attribute
    where attributename='SubscriptionStatus'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3123',@attributeid,'9041','3','1','0'
    ,'False','1',getdate(),null)
   
--attribute  'LastHotListPageVisit-WhoAddedYouToTheirFavorites'
    select @attributeid=attributeid from attribute
    where attributename='LastHotListPageVisit-WhoAddedYouToTheirFavorites'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3124',@attributeid,'9041','1','1','0'
    ,'False',null,getdate(),null)
   
--attribute  'LastHotListPageVisit-WhoEmailedYou'
    select @attributeid=attributeid from attribute
    where attributename='LastHotListPageVisit-WhoEmailedYou'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3125',@attributeid,'9041','1','1','0'
    ,'False',null,getdate(),null)
   
--attribute  'LastHotListPageVisit-WhoViewedYourProfile'
    select @attributeid=attributeid from attribute
    where attributename='LastHotListPageVisit-WhoViewedYourProfile'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3126',@attributeid,'9041','1','1','0'
    ,'False',null,getdate(),null)
   
--attribute  'LastHotListPageVisit-WhoSentYouECards'
    select @attributeid=attributeid from attribute
    where attributename='LastHotListPageVisit-WhoSentYouECards'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3127',@attributeid,'9041','1','1','0'
    ,'False',null,getdate(),null)
   
--attribute  'LastSuspendedAdminReasonID'
--this attribute is missing on stagev300 but is present in prod
--tht's why  it gives error - ignore

    select @attributeid=attributeid from attribute
    where attributename='LastSuspendedAdminReasonID'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3128',@attributeid,'9041','3','1','0'
    ,'False',null,getdate(),null)
   
--attribute  'JMeterExpirationDate'
    select @attributeid=attributeid from attribute
    where attributename='JMeterExpirationDate'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3129',@attributeid,'9041','1','1','0'
    ,'False',null,getdate(),null)
   
--attribute  'HasNewNotificationMask'
    select @attributeid=attributeid from attribute
    where attributename='HasNewNotificationMask'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3130',@attributeid,'9041','2','1','0'
    ,'False',null,getdate(),null)
   
--attribute  'ToolbarFlag'
    select @attributeid=attributeid from attribute
    where attributename='ToolbarFlag'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3131',@attributeid,'9041','0','1','0'
    ,'False','0',getdate(),null)
   
--attribute  'SMSAlertPreference'
    select @attributeid=attributeid from attribute
    where attributename='SMSAlertPreference'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3132',@attributeid,'9041','2','1','0'
    ,'False','31',getdate(),null)
   
--attribute  'SMSPhoneIsValidated'
    select @attributeid=attributeid from attribute
    where attributename='SMSPhoneIsValidated'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3133',@attributeid,'9041','0','1','0'
    ,'False','0',getdate(),null)
   
--attribute  'SMSConfirmationCode'
    select @attributeid=attributeid from attribute
    where attributename='SMSConfirmationCode'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3134',@attributeid,'9041','4','1','10'
    ,'False',null,getdate(),null)
   
--attribute  'SMSCarrier'
    select @attributeid=attributeid from attribute
    where attributename='SMSCarrier'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3135',@attributeid,'9041','3','1','0'
    ,'False',null,getdate(),null)
   
--attribute  'SMSPhone'
    select @attributeid=attributeid from attribute
    where attributename='SMSPhone'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3136',@attributeid,'9041','4','1','10'
    ,'False',null,getdate(),null)
   
--attribute  'HighlightedExpirationDate'
    select @attributeid=attributeid from attribute
    where attributename='HighlightedExpirationDate'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3137',@attributeid,'9041','1','1','0'
    ,'False',null,getdate(),null)
   
--attribute  'PremiumAuthenticatedExpirationDate'
    select @attributeid=attributeid from attribute
    where attributename='PremiumAuthenticatedExpirationDate'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3138',@attributeid,'9041','1','1','0'
    ,'False',null,getdate(),null)
   
--attribute  'SiteLastName'
    select @attributeid=attributeid from attribute
    where attributename='SiteLastName'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3139',@attributeid,'9041','4','1','100'
    ,'False',null,getdate(),null)
   
--attribute  'LastHotListPageVisit-MutualYes'
    select @attributeid=attributeid from attribute
    where attributename='LastHotListPageVisit-MutualYes'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3140',@attributeid,'9041','1','1','0'
    ,'False',null,getdate(),null)
   
--attribute  'MingleCookieMigrationFlag'
    select @attributeid=attributeid from attribute
    where attributename='MingleCookieMigrationFlag'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3141',@attributeid,'9041','0','1','0'
    ,'False',null,getdate(),null)
   
--attribute  'LastHotListPageVisit-WhoTeasedYou'
    select @attributeid=attributeid from attribute
    where attributename='LastHotListPageVisit-WhoTeasedYou'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3142',@attributeid,'9041','1','1','0'
    ,'False',null,getdate(),null)
   
--attribute  'LastHotListPageVisit-WhoIMedYou'
    select @attributeid=attributeid from attribute
    where attributename='LastHotListPageVisit-WhoIMedYou'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3143',@attributeid,'9041','1','1','0'
    ,'False',null,getdate(),null)
   
--attribute  'BrandLastLogonDateOldValue'
    select @attributeid=attributeid from attribute
    where attributename='BrandLastLogonDateOldValue'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3144',@attributeid,'90410','1','1','0'
    ,'False',null,getdate(),null)
   
--attribute  'BrandInsertDate'
    select @attributeid=attributeid from attribute
    where attributename='BrandInsertDate'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3145',@attributeid,'90410','1','3','0'
    ,'False',null,getdate(),null)
   
--attribute  'BrandLogonCount'
    select @attributeid=attributeid from attribute
    where attributename='BrandLogonCount'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3146',@attributeid,'90410','3','1','0'
    ,'False','0',getdate(),null)
   
--attribute  'BrandLastLogonDate'
    select @attributeid=attributeid from attribute
    where attributename='BrandLastLogonDate'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3147',@attributeid,'90410','1','1','0'
    ,'False',null,getdate(),null)
   
--attribute  'BrandBannerID'
    select @attributeid=attributeid from attribute
    where attributename='BrandBannerID'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3148',@attributeid,'90410','3','3','0'
    ,'False',null,getdate(),null)
   
--attribute  'BrandLandingPageID'
    select @attributeid=attributeid from attribute
    where attributename='BrandLandingPageID'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3149',@attributeid,'90410','3','3','0'
    ,'False',null,getdate(),null)
   
--attribute  'BrandPromotionID'
    select @attributeid=attributeid from attribute
    where attributename='BrandPromotionID'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3150',@attributeid,'90410','3','3','0'
    ,'False',null,getdate(),null)
   
--attribute  'PhotoStatusMask'
    select @attributeid=attributeid from attribute
    where attributename='PhotoStatusMask'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3151',@attributeid,'90410','2','1','0'
    ,'False',null,getdate(),null)
   
--attribute  'BrandLuggage'
    select @attributeid=attributeid from attribute
    where attributename='BrandLuggage'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3152',@attributeid,'90410','4','3','150'
    ,'False',null,getdate(),null)
   
--attribute  'RebrandFirstLogonDate'
    select @attributeid=attributeid from attribute
    where attributename='RebrandFirstLogonDate'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3153',@attributeid,'90410','1','3','0'
    ,'False',null,getdate(),null)
   
   
   
  