use mnList1 
go
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D23YNMList0](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D23YNMList0] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D23YNMList1](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D23YNMList1] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D23YNMList2](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D23YNMList2] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D23YNMList3](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D23YNMList3] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D23YNMList4](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D23YNMList4] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D23YNMList5](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D23YNMList5] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D23YNMList6](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D23YNMList6] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D23YNMList7](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D23YNMList7] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D23YNMList8](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D23YNMList8] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D23YNMList9](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D23YNMList9] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D23YNMList10](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D23YNMList10] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D23YNMList11](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D23YNMList11] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D23YNMList12](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D23YNMList12] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D23YNMList13](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D23YNMList13] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D23YNMList14](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D23YNMList14] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D23YNMList15](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D23YNMList15] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D23YNMList16](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D23YNMList16] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D23YNMList17](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D23YNMList17] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D23YNMList18](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D23YNMList18] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D23YNMList19](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D23YNMList19] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    