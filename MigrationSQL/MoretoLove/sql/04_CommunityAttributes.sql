 
use mnSystem
go


declare @attributeid  int

--attribute  'HasNewMutualYes'
    select @attributeid=attributeid from attribute
    where attributename='HasNewMutualYes'
    
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3001',@attributeid,'23','0','1','0'
    ,'False','0',getdate(),null)
   
--attribute  'FacePageIntroduction'
    select @attributeid=attributeid from attribute
    where attributename='FacePageIntroduction'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3002',@attributeid,'23','4','1','600'
    ,'False',null,getdate(),null)
   
--attribute  'LastMatchNewsletterDate'
    select @attributeid=attributeid from attribute
    where attributename='LastMatchNewsletterDate'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3003',@attributeid,'23','1','1','0'
    ,'False',null,getdate(),null)
   
--attribute  'AdminSuspendedFlag'
    select @attributeid=attributeid from attribute
    where attributename='AdminSuspendedFlag'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3004',@attributeid,'23','0','1','0'
    ,'False','0',getdate(),null)
   
--attribute  'MajorType'
    select @attributeid=attributeid from attribute
    where attributename='MajorType'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3005',@attributeid,'23','3','1','0'
    ,'False',null,getdate(),null)
   
--attribute  'VacationStartDate'
    select @attributeid=attributeid from attribute
    where attributename='VacationStartDate'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3006',@attributeid,'23','1','1','0'
    ,'False',null,getdate(),null)
   
--attribute  'EssayOrganizations'
    select @attributeid=attributeid from attribute
    where attributename='EssayOrganizations'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3007',@attributeid,'23','4','5','4000'
    ,'False',null,getdate(),null)
   
--attribute  'EssayFirstThingNoticeAboutYou'
    select @attributeid=attributeid from attribute
    where attributename='EssayFirstThingNoticeAboutYou'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3008',@attributeid,'23','4','5','4000'
    ,'False',null,getdate(),null)
   
--attribute  'WhenIHaveTime'
    select @attributeid=attributeid from attribute
    where attributename='WhenIHaveTime'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3009',@attributeid,'23','4','5','100'
    ,'False',null,getdate(),null)
   
--attribute  'EssayDescribeHumor'
    select @attributeid=attributeid from attribute
    where attributename='EssayDescribeHumor'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3010',@attributeid,'23','4','5','4000'
    ,'False',null,getdate(),null)
   
--attribute  'CannotDoWithout'
    select @attributeid=attributeid from attribute
    where attributename='CannotDoWithout'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3011',@attributeid,'23','4','5','100'
    ,'False',null,getdate(),null)
   
--attribute  'EssayFriendsDescription'
    select @attributeid=attributeid from attribute
    where attributename='EssayFriendsDescription'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3012',@attributeid,'23','4','5','4000'
    ,'False',null,getdate(),null)
   
--attribute  'HasPhotoFlag'
    select @attributeid=attributeid from attribute
    where attributename='HasPhotoFlag'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3013',@attributeid,'23','0','1','0'
    ,'False','0',getdate(),null)
   
--attribute  'PerfectFirstDateEssay'
    select @attributeid=attributeid from attribute
    where attributename='PerfectFirstDateEssay'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3014',@attributeid,'23','4','5','4000'
    ,'False',null,getdate(),'3087')
   
--attribute  'DesiredMaxAge'
    select @attributeid=attributeid from attribute
    where attributename='DesiredMaxAge'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3015',@attributeid,'23','3','1','0'
    ,'False',null,getdate(),null)
   
--attribute  'IdealRelationshipEssay'
    select @attributeid=attributeid from attribute
    where attributename='IdealRelationshipEssay'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3016',@attributeid,'23','4','5','4000'
    ,'False',null,getdate(),'3086')
   
--attribute  'IsraelImmigrationDate'
    select @attributeid=attributeid from attribute
    where attributename='IsraelImmigrationDate'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3017',@attributeid,'23','3','1','0'
    ,'False',null,getdate(),null)
   
--attribute  'LearnFromThePastEssay'
    select @attributeid=attributeid from attribute
    where attributename='LearnFromThePastEssay'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3018',@attributeid,'23','4','5','4000'
    ,'False',null,getdate(),'3085')
   
--attribute  'OccupationDescription'
    select @attributeid=attributeid from attribute
    where attributename='OccupationDescription'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3019',@attributeid,'23','4','5','200'
    ,'False',null,getdate(),'3084')
   
--attribute  'HideMask'
    select @attributeid=attributeid from attribute
    where attributename='HideMask'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3020',@attributeid,'23','2','1','0'
    ,'False','0',getdate(),null)
   
--attribute  'Fashion'
    select @attributeid=attributeid from attribute
    where attributename='Fashion'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3021',@attributeid,'23','3','1','0'
    ,'False',null,getdate(),null)
   
--attribute  'Timeliness'
    select @attributeid=attributeid from attribute
    where attributename='Timeliness'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3022',@attributeid,'23','3','1','0'
    ,'False',null,getdate(),null)
   
--attribute  'SchoolsAttended-old'
    select @attributeid=attributeid from attribute
    where attributename='SchoolsAttended-old'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3023',@attributeid,'23','4','5','100'
    ,'False',null,getdate(),null)
   
--attribute  'SchoolsAttended'
    select @attributeid=attributeid from attribute
    where attributename='SchoolsAttended'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3024',@attributeid,'23','4','5','100'
    ,'False',null,getdate(),'3023')
   
--attribute  'FavoriteRestaurant-old'
    select @attributeid=attributeid from attribute
    where attributename='FavoriteRestaurant-old'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3025',@attributeid,'23','4','5','100'
    ,'False',null,getdate(),null)
   
--attribute  'FavoriteRestaurant'
    select @attributeid=attributeid from attribute
    where attributename='FavoriteRestaurant'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3026',@attributeid,'23','4','5','100'
    ,'False',null,getdate(),'3025')
   
--attribute  'MyGreatTripIdea-old'
    select @attributeid=attributeid from attribute
    where attributename='MyGreatTripIdea-old'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3027',@attributeid,'23','4','5','100'
    ,'False',null,getdate(),null)
   
--attribute  'MyGreatTripIdea'
    select @attributeid=attributeid from attribute
    where attributename='MyGreatTripIdea'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3028',@attributeid,'23','4','5','100'
    ,'False',null,getdate(),'3027')
   
--attribute  'FavoriteTV-old'
    select @attributeid=attributeid from attribute
    where attributename='FavoriteTV-old'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3029',@attributeid,'23','4','5','100'
    ,'False',null,getdate(),null)
   
--attribute  'FavoriteTV'
    select @attributeid=attributeid from attribute
    where attributename='FavoriteTV'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3030',@attributeid,'23','4','5','100'
    ,'False',null,getdate(),'3029')
   
--attribute  'FavoriteMovies-old'
    select @attributeid=attributeid from attribute
    where attributename='FavoriteMovies-old'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3031',@attributeid,'23','4','5','100'
    ,'False',null,getdate(),null)
   
--attribute  'FavoriteMovies'
    select @attributeid=attributeid from attribute
    where attributename='FavoriteMovies'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3032',@attributeid,'23','4','5','100'
    ,'False',null,getdate(),'3031')
   
--attribute  'FavoriteBands-old'
    select @attributeid=attributeid from attribute
    where attributename='FavoriteBands-old'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3033',@attributeid,'23','4','5','100'
    ,'False',null,getdate(),null)
   
--attribute  'FavoriteBands'
    select @attributeid=attributeid from attribute
    where attributename='FavoriteBands'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3034',@attributeid,'23','4','5','100'
    ,'False',null,getdate(),'3033')
   
--attribute  'SelfDescription-old'
    select @attributeid=attributeid from attribute
    where attributename='SelfDescription-old'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3035',@attributeid,'23','4','5','100'
    ,'False',null,getdate(),null)
   
--attribute  'SelfDescription'
    select @attributeid=attributeid from attribute
    where attributename='SelfDescription'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3036',@attributeid,'23','4','5','100'
    ,'False',null,getdate(),'3035')
   
--attribute  'DesiredMinAge'
    select @attributeid=attributeid from attribute
    where attributename='DesiredMinAge'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3037',@attributeid,'23','3','1','0'
    ,'False',null,getdate(),null)
   
--attribute  'LastUpdated'
    select @attributeid=attributeid from attribute
    where attributename='LastUpdated'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3038',@attributeid,'23','1','1','0'
    ,'False',null,getdate(),null)
   
--attribute  'EssayMoreThingsToAdd-old'
    select @attributeid=attributeid from attribute
    where attributename='EssayMoreThingsToAdd-old'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3039',@attributeid,'23','4','5','400'
    ,'False',null,getdate(),null)
   
--attribute  'EssayMoreThingsToAdd'
    select @attributeid=attributeid from attribute
    where attributename='EssayMoreThingsToAdd'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3040',@attributeid,'23','4','5','4000'
    ,'False',null,getdate(),'3039')
   
--attribute  'NextRegistrationActionPageID'
    select @attributeid=attributeid from attribute
    where attributename='NextRegistrationActionPageID'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3041',@attributeid,'23','3','1','0'
    ,'False','0',getdate(),null)
   
--attribute  'PerfectMatchEssay'
    select @attributeid=attributeid from attribute
    where attributename='PerfectMatchEssay'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3042',@attributeid,'23','4','5','4000'
    ,'False',null,getdate(),'3088')
   
--attribute  'AboutMe'
    select @attributeid=attributeid from attribute
    where attributename='AboutMe'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3043',@attributeid,'23','4','5','4000'
    ,'False',null,getdate(),'3089')
   
--attribute  'EssayPerfectDay-old'
    select @attributeid=attributeid from attribute
    where attributename='EssayPerfectDay-old'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3044',@attributeid,'23','4','5','400'
    ,'False',null,getdate(),null)
   
--attribute  'EssayPerfectDay'
    select @attributeid=attributeid from attribute
    where attributename='EssayPerfectDay'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3045',@attributeid,'23','4','5','400'
    ,'False',null,getdate(),'3044')
   
--attribute  'EssayImportantThings-old'
    select @attributeid=attributeid from attribute
    where attributename='EssayImportantThings-old'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3046',@attributeid,'23','4','5','400'
    ,'False',null,getdate(),null)
   
--attribute  'EssayImportantThings'
    select @attributeid=attributeid from attribute
    where attributename='EssayImportantThings'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3047',@attributeid,'23','4','5','4000'
    ,'False',null,getdate(),'3046')
   
--attribute  'EssayPastRelationship-old'
    select @attributeid=attributeid from attribute
    where attributename='EssayPastRelationship-old'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3048',@attributeid,'23','4','5','400'
    ,'False',null,getdate(),null)
   
--attribute  'EssayPastRelationship'
    select @attributeid=attributeid from attribute
    where attributename='EssayPastRelationship'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3049',@attributeid,'23','4','5','4000'
    ,'False',null,getdate(),'3048')
   
--attribute  'EssayGetToKnowMe-old'
    select @attributeid=attributeid from attribute
    where attributename='EssayGetToKnowMe-old'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3050',@attributeid,'23','4','5','400'
    ,'False',null,getdate(),null)
   
--attribute  'EssayGetToKnowMe'
    select @attributeid=attributeid from attribute
    where attributename='EssayGetToKnowMe'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3051',@attributeid,'23','4','5','4000'
    ,'False',null,getdate(),'3050')
   
--attribute  'EssayFirstDate-old'
    select @attributeid=attributeid from attribute
    where attributename='EssayFirstDate-old'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3052',@attributeid,'23','4','5','400'
    ,'False',null,getdate(),null)
   
--attribute  'EssayFirstDate'
    select @attributeid=attributeid from attribute
    where attributename='EssayFirstDate'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3053',@attributeid,'23','4','5','4000'
    ,'False',null,getdate(),'3052')
   
--attribute  'MailboxPreference'
    select @attributeid=attributeid from attribute
    where attributename='MailboxPreference'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3054',@attributeid,'23','2','1','0'
    ,'False',null,getdate(),null)
   
--attribute  'AboutMyAppearanceEssay'
    select @attributeid=attributeid from attribute
    where attributename='AboutMyAppearanceEssay'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3055',@attributeid,'23','4','5','4000'
    ,'False',null,getdate(),null)
   
--attribute  'ColorAnalysis'
    select @attributeid=attributeid from attribute
    where attributename='ColorAnalysis'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3056',@attributeid,'23','0','1','0'
    ,'False','0',getdate(),null)
   
--attribute  'NewsletterMask'
    select @attributeid=attributeid from attribute
    where attributename='NewsletterMask'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3057',@attributeid,'23','2','1','0'
    ,'False','7',getdate(),null)
   
--attribute  'MatchNewsletterPeriod'
    select @attributeid=attributeid from attribute
    where attributename='MatchNewsletterPeriod'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3058',@attributeid,'23','3','1','0'
    ,'False','3',getdate(),null)
   
--attribute  'ColorCodeQuizCompleteDate'
    select @attributeid=attributeid from attribute
    where attributename='ColorCodeQuizCompleteDate'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3059',@attributeid,'23','1','1','100'
    ,'False',null,getdate(),null)
   
--attribute  'ColorCodeHidden'
    select @attributeid=attributeid from attribute
    where attributename='ColorCodeHidden'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3060',@attributeid,'23','4','1','100'
    ,'False','false',getdate(),null)
   
--attribute  'ColorCodeRedScore'
    select @attributeid=attributeid from attribute
    where attributename='ColorCodeRedScore'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3061',@attributeid,'23','4','1','100'
    ,'False','0',getdate(),null)
   
--attribute  'ColorCodeWhiteScore'
    select @attributeid=attributeid from attribute
    where attributename='ColorCodeWhiteScore'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3062',@attributeid,'23','4','1','100'
    ,'False','0',getdate(),null)
   
--attribute  'ColorCodeYellowScore'
    select @attributeid=attributeid from attribute
    where attributename='ColorCodeYellowScore'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3063',@attributeid,'23','4','1','100'
    ,'False','0',getdate(),null)
   
--attribute  'ColorCodeBlueScore'
    select @attributeid=attributeid from attribute
    where attributename='ColorCodeBlueScore'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3064',@attributeid,'23','4','1','100'
    ,'False','0',getdate(),null)
   
--attribute  'ColorCodeSecondaryColor'
    select @attributeid=attributeid from attribute
    where attributename='ColorCodeSecondaryColor'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3065',@attributeid,'23','4','1','100'
    ,'False','0',getdate(),null)
   
--attribute  'ColorCodePrimaryColor'
    select @attributeid=attributeid from attribute
    where attributename='ColorCodePrimaryColor'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3066',@attributeid,'23','4','1','100'
    ,'False','0',getdate(),null)
   
--attribute  'ColorCodeQuizAnswers'
    select @attributeid=attributeid from attribute
    where attributename='ColorCodeQuizAnswers'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3067',@attributeid,'23','4','1','3900'
    ,'False','0',getdate(),null)
   
--attribute  'PromotionID'
    select @attributeid=attributeid from attribute
    where attributename='PromotionID'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3068',@attributeid,'23','3','3','0'
    ,'False','0',getdate(),null)
   
--attribute  'BannerID'
    select @attributeid=attributeid from attribute
    where attributename='BannerID'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3069',@attributeid,'23','3','3','0'
    ,'False','0',getdate(),null)
   
--attribute  'MessmoCapable'
    select @attributeid=attributeid from attribute
    where attributename='MessmoCapable'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3070',@attributeid,'23','3','1','0'
    ,'False',null,getdate(),null)
   
--attribute  'Headline'
    select @attributeid=attributeid from attribute
    where attributename='Headline'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3071',@attributeid,'23','4','5','400'
    ,'False',null,getdate(),'3090')
   
--attribute  'NewMemberEmailPeriod'
    select @attributeid=attributeid from attribute
    where attributename='NewMemberEmailPeriod'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3072',@attributeid,'23','3','1','0'
    ,'False','0',getdate(),null)
   
--attribute  'IdealVacation'
    select @attributeid=attributeid from attribute
    where attributename='IdealVacation'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3073',@attributeid,'23','4','5','100'
    ,'False',null,getdate(),null)
   
--attribute  'FavoriteMovieDirectorActor'
    select @attributeid=attributeid from attribute
    where attributename='FavoriteMovieDirectorActor'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3074',@attributeid,'23','4','5','200'
    ,'False',null,getdate(),null)
   
--attribute  'LandingPageID'
    select @attributeid=attributeid from attribute
    where attributename='LandingPageID'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3075',@attributeid,'23','3','3','0'
    ,'False','0',getdate(),null)
   
--attribute  'FavoriteBookAuthor'
    select @attributeid=attributeid from attribute
    where attributename='FavoriteBookAuthor'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3076',@attributeid,'23','4','5','200'
    ,'False',null,getdate(),null)
   
--attribute  'FavoriteAlbumSongArtist'
    select @attributeid=attributeid from attribute
    where attributename='FavoriteAlbumSongArtist'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3077',@attributeid,'23','4','5','200'
    ,'False',null,getdate(),null)
   
--attribute  'DisplayPhotosToGuests'
    select @attributeid=attributeid from attribute
    where attributename='DisplayPhotosToGuests'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3078',@attributeid,'23','0','1','0'
    ,'False','1',getdate(),null)
   
--attribute  'Refcd'
    select @attributeid=attributeid from attribute
    where attributename='Refcd'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3079',@attributeid,'23','4','3','200'
    ,'False',null,getdate(),null)
   
--attribute  'GenderMask'
    select @attributeid=attributeid from attribute
    where attributename='GenderMask'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3080',@attributeid,'23','2','1','0'
    ,'False',null,getdate(),null)
   
--attribute  'UserName'
    select @attributeid=attributeid from attribute
    where attributename='UserName'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3081',@attributeid,'23','4','5','200'
    ,'False',null,getdate(),null)
   
--attribute  'StudiesEmphasis-old'
    select @attributeid=attributeid from attribute
    where attributename='StudiesEmphasis-old'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3082',@attributeid,'23','4','5','400'
    ,'False',null,getdate(),null)
   
--attribute  'GrewUpIn-old'
    select @attributeid=attributeid from attribute
    where attributename='GrewUpIn-old'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3083',@attributeid,'23','4','5','400'
    ,'False',null,getdate(),null)
   
--attribute  'OccupationDescription-old'
    select @attributeid=attributeid from attribute
    where attributename='OccupationDescription-old'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3084',@attributeid,'23','4','5','400'
    ,'False',null,getdate(),null)
   
--attribute  'LearnFromThePastEssay-old'
    select @attributeid=attributeid from attribute
    where attributename='LearnFromThePastEssay-old'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3085',@attributeid,'23','4','5','400'
    ,'False',null,getdate(),null)
   
--attribute  'IdealRelationshipEssay-old'
    select @attributeid=attributeid from attribute
    where attributename='IdealRelationshipEssay-old'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3086',@attributeid,'23','4','5','400'
    ,'False',null,getdate(),null)
   
--attribute  'PerfectFirstDateEssay-old'
    select @attributeid=attributeid from attribute
    where attributename='PerfectFirstDateEssay-old'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3087',@attributeid,'23','4','5','400'
    ,'False',null,getdate(),null)
   
--attribute  'PerfectMatchEssay-old'
    select @attributeid=attributeid from attribute
    where attributename='PerfectMatchEssay-old'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3088',@attributeid,'23','4','5','400'
    ,'False',null,getdate(),null)
   
--attribute  'AboutMe-old'
    select @attributeid=attributeid from attribute
    where attributename='AboutMe-old'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3089',@attributeid,'23','4','5','400'
    ,'False',null,getdate(),null)
   
--attribute  'Headline-old'
    select @attributeid=attributeid from attribute
    where attributename='Headline-old'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3090',@attributeid,'23','4','5','400'
    ,'False',null,getdate(),null)
   
--attribute  'SuspendMask'
    select @attributeid=attributeid from attribute
    where attributename='SuspendMask'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3091',@attributeid,'23','2','1','0'
    ,'False','0',getdate(),null)
   
--attribute  'AwayMessage'
    select @attributeid=attributeid from attribute
    where attributename='AwayMessage'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3092',@attributeid,'23','4','1','500'
    ,'False',null,getdate(),null)
   
--attribute  'DesiredReligious'
    select @attributeid=attributeid from attribute
    where attributename='DesiredReligious'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3093',@attributeid,'23','2','1','0'
    ,'False','0',getdate(),null)
   
--attribute  'SelfSuspendedReasonType'
    select @attributeid=attributeid from attribute
    where attributename='SelfSuspendedReasonType'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3094',@attributeid,'23','3','1','0'
    ,'False',null,getdate(),null)
   
--attribute  'Luggage'
    select @attributeid=attributeid from attribute
    where attributename='Luggage'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3095',@attributeid,'23','4','3','150'
    ,'False',null,getdate(),null)
   
--attribute  'AlertEmailMask'
    select @attributeid=attributeid from attribute
    where attributename='AlertEmailMask'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3096',@attributeid,'23','2','1','0'
    ,'False','15',getdate(),null)
   
--attribute  'SelfSuspendedFlag'
    select @attributeid=attributeid from attribute
    where attributename='SelfSuspendedFlag'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3097',@attributeid,'23','0','1','0'
    ,'False','0',getdate(),null)
   
--attribute  'ReportAbuseCount'
    select @attributeid=attributeid from attribute
    where attributename='ReportAbuseCount'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3098',@attributeid,'23','3','1','0'
    ,'False','0',getdate(),null)
   
--attribute  'ChatBanned'
    select @attributeid=attributeid from attribute
    where attributename='ChatBanned'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3099',@attributeid,'23','3','1','0'
    ,'False','0',getdate(),null)
   
--attribute  'RegistrationTargetPage'
    select @attributeid=attributeid from attribute
    where attributename='RegistrationTargetPage'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3100',@attributeid,'23','4','1','3900'
    ,'False',null,getdate(),null)
   
--attribute  'RegistrationReferrerPage'
    select @attributeid=attributeid from attribute
    where attributename='RegistrationReferrerPage'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3101',@attributeid,'23','4','1','3900'
    ,'False',null,getdate(),null)
   
--attribute  'HasNewMail'
    select @attributeid=attributeid from attribute
    where attributename='HasNewMail'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3102',@attributeid,'23','0','1','0'
    ,'False',null,getdate(),null)
   
--attribute  'LastEmailReceiveDate'
    select @attributeid=attributeid from attribute
    where attributename='LastEmailReceiveDate'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3103',@attributeid,'23','1','1','0'
    ,'False',null,getdate(),null)
   
--attribute  'RelationshipMask'
    select @attributeid=attributeid from attribute
    where attributename='RelationshipMask'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3104',@attributeid,'23','2','1','0'
    ,'False',null,getdate(),null)
   
--attribute  'CommunityEmailCount'
    select @attributeid=attributeid from attribute
    where attributename='CommunityEmailCount'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3105',@attributeid,'23','3','1','0'
    ,'False','0',getdate(),null)
   
--attribute  'VoiceProfilePath'
    select @attributeid=attributeid from attribute
    where attributename='VoiceProfilePath'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3106',@attributeid,'23','4','1','255'
    ,'False',null,getdate(),null)
   
--attribute  'OptOutDate'
    select @attributeid=attributeid from attribute
    where attributename='OptOutDate'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3107',@attributeid,'23','1','1','0'
    ,'False',null,getdate(),null)
   
--attribute  'TelephonyStatusMask'
    select @attributeid=attributeid from attribute
    where attributename='TelephonyStatusMask'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3108',@attributeid,'23','2','1','0'
    ,'False','0',getdate(),null)
   
--attribute  'OptOut'
    select @attributeid=attributeid from attribute
    where attributename='OptOut'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3109',@attributeid,'23','0','1','0'
    ,'False','0',getdate(),null)
   
--attribute  'StudiesEmphasis'
    select @attributeid=attributeid from attribute
    where attributename='StudiesEmphasis'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3110',@attributeid,'23','4','5','50'
    ,'False',null,getdate(),'3082')
   
--attribute  'GrewUpIn'
    select @attributeid=attributeid from attribute
    where attributename='GrewUpIn'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3111',@attributeid,'23','4','5','1000'
    ,'False',null,getdate(),'3083')
   
--attribute  'DomainAliasID'
    select @attributeid=attributeid from attribute
    where attributename='DomainAliasID'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3112',@attributeid,'23','3','1','0'
    ,'False',null,getdate(),null)
   
--attribute  'IsrealImmigrationYear'
    select @attributeid=attributeid from attribute
    where attributename='IsrealImmigrationYear'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('3113',@attributeid,'23','3','1','0'
    ,'False',null,getdate(),null)
   
insert into AttributeCollectionAttribute 
select attributecollectionid,attributeid,23,optionalflag
from AttributeCollectionAttribute
where GroupID=21

