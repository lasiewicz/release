use mnsystem
go

--Attribute Options Scope Change 
  declare @attributeid int
   declare @attributeoptionid int
   declare @AttributeOptionGroupID int
   

    --attributeoption  'gendermask'
    
    select @attributeid=attributeid from attribute
    where attributename= 'gendermask'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_BLANK_105416'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9041','1')
 
    --attributeoption  'gendermask'
    
    select @attributeid=attributeid from attribute
    where attributename= 'gendermask'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_69_MALE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9041','2')
  
   --attributeoption  'gendermask'
    
    select @attributeid=attributeid from attribute
    where attributename= 'gendermask'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_69_FEMALE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9041','3')
  
  
     --attributeoption  'gendermask'
    
    select @attributeid=attributeid from attribute
    where attributename= 'gendermask'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_69_SEEKING_MALE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9041','4')
  
  
       --attributeoption  'gendermask'
    
    select @attributeid=attributeid from attribute
    where attributename= 'gendermask'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_69_SEEKING_FEMALE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9041','5')