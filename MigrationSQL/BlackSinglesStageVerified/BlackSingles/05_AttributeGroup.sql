use mnsystem


select * from attributegroup
order by attributegroupid desc
where groupid  = 9051

delete from attributegroup
where groupid  in ( 9051, 90510)
--SitePages  
  declare @attributeid int
  
   
--attribute  'ROLFScoreDescription'
    select @attributeid=attributeid from attribute
    where attributename='ROLFScoreDescription'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4121',@attributeid,'9051','4','1','25'
    ,'False',null,getdate(),null)
   
--attribute  'LastHotListPageVisit-WhoEmailedYou'
    select @attributeid=attributeid from attribute
    where attributename='LastHotListPageVisit-WhoEmailedYou'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4122',@attributeid,'9051','1','1','0'
    ,'False',null,getdate(),null)
   
--attribute  'LastHotListPageVisit-MutualYes'
    select @attributeid=attributeid from attribute
    where attributename='LastHotListPageVisit-MutualYes'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4123',@attributeid,'9051','1','1','0'
    ,'False',null,getdate(),null)
   
--attribute  'JMeterFlag'
    select @attributeid=attributeid from attribute
    where attributename='JMeterFlag'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4124',@attributeid,'9051','0','1','0'
    ,'False','0',getdate(),null)
   
--attribute  'SiteFirstName'
    select @attributeid=attributeid from attribute
    where attributename='SiteFirstName'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4125',@attributeid,'9051','4','1','100'
    ,'False',null,getdate(),null)
   
--attribute  'ForcedPageTimedPromotionVersion'
    select @attributeid=attributeid from attribute
    where attributename='ForcedPageTimedPromotionVersion'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4126',@attributeid,'9051','3','1','0'
    ,'False','0',getdate(),null)
   
--attribute  'MiniSearchMarketingFlag'
    select @attributeid=attributeid from attribute
    where attributename='MiniSearchMarketingFlag'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4127',@attributeid,'9051','0','1','0'
    ,'False','0',getdate(),null)
   
--attribute  'SubscriptionExpirationDate'
    select @attributeid=attributeid from attribute
    where attributename='SubscriptionExpirationDate'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4128',@attributeid,'9051','1','1','0'
    ,'False',null,getdate(),null)
   
--attribute  'SubscriptionLastInitialPurchaseDate'
    select @attributeid=attributeid from attribute
    where attributename='SubscriptionLastInitialPurchaseDate'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4129',@attributeid,'9051','1','1','0'
    ,'False',null,getdate(),null)
   
--attribute  'LastBrandID'
    select @attributeid=attributeid from attribute
    where attributename='LastBrandID'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4130',@attributeid,'9051','3','1','0'
    ,'False','91610',getdate(),null)
   
--attribute  'SpotlightExpirationDate'
    select @attributeid=attributeid from attribute
    where attributename='SpotlightExpirationDate'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4131',@attributeid,'9051','1','1','0'
    ,'False',null,getdate(),null)
   
--attribute  'SubscriptionStatus'
    select @attributeid=attributeid from attribute
    where attributename='SubscriptionStatus'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4132',@attributeid,'9051','3','1','0'
    ,'False','1',getdate(),null)
   
--attribute  'LastHotListPageVisit-WhoAddedYouToTheirFavorites'
    select @attributeid=attributeid from attribute
    where attributename='LastHotListPageVisit-WhoAddedYouToTheirFavorites'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4133',@attributeid,'9051','1','1','0'
    ,'False',null,getdate(),null)
   
--attribute  'HighlightedFlag'
    select @attributeid=attributeid from attribute
    where attributename='HighlightedFlag'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4134',@attributeid,'9051','0','1','0'
    ,'False','0',getdate(),null)
   
--attribute  'LastHotListPageVisit-WhoViewedYourProfile'
    select @attributeid=attributeid from attribute
    where attributename='LastHotListPageVisit-WhoViewedYourProfile'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4135',@attributeid,'9051','1','1','0'
    ,'False',null,getdate(),null)
   
--attribute  'HasAffiliationGroup'
    select @attributeid=attributeid from attribute
    where attributename='HasAffiliationGroup'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4136',@attributeid,'9051','0','1','0'
    ,'False',null,getdate(),null)
   
--attribute  'LastHotListPageVisit-WhoSentYouECards'
    select @attributeid=attributeid from attribute
    where attributename='LastHotListPageVisit-WhoSentYouECards'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4137',@attributeid,'9051','1','1','0'
    ,'False',null,getdate(),null)
    
    

--attribute  'LastSuspendedAdminReasonID'
    select @attributeid=attributeid from attribute
    where attributename='LastSuspendedAdminReasonID'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4138',@attributeid,'9051','3','1','0'
    ,'False',null,getdate(),null)
   
--attribute  'JMeterExpirationDate'
    select @attributeid=attributeid from attribute
    where attributename='JMeterExpirationDate'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4139',@attributeid,'9051','1','1','0'
    ,'False',null,getdate(),null)
   
--attribute  'HasNewNotificationMask'
    select @attributeid=attributeid from attribute
    where attributename='HasNewNotificationMask'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4140',@attributeid,'9051','2','1','0'
    ,'False',null,getdate(),null)
   
--attribute  'ToolbarFlag'
    select @attributeid=attributeid from attribute
    where attributename='ToolbarFlag'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4141',@attributeid,'9051','0','1','0'
    ,'False','0',getdate(),null)
   
--attribute  'SMSAlertPreference'
    select @attributeid=attributeid from attribute
    where attributename='SMSAlertPreference'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4142',@attributeid,'9051','2','1','0'
    ,'False','31',getdate(),null)
   
--attribute  'SMSPhoneIsValidated'
    select @attributeid=attributeid from attribute
    where attributename='SMSPhoneIsValidated'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4143',@attributeid,'9051','0','1','0'
    ,'False','0',getdate(),null)
   
--attribute  'SMSConfirmationCode'
    select @attributeid=attributeid from attribute
    where attributename='SMSConfirmationCode'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4144',@attributeid,'9051','4','1','10'
    ,'False',null,getdate(),null)
   
--attribute  'SMSCarrier'
    select @attributeid=attributeid from attribute
    where attributename='SMSCarrier'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4145',@attributeid,'9051','3','1','0'
    ,'False',null,getdate(),null)
   
--attribute  'SMSPhone'
    select @attributeid=attributeid from attribute
    where attributename='SMSPhone'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4146',@attributeid,'9051','4','1','10'
    ,'False',null,getdate(),null)
   
--attribute  'HighlightedExpirationDate'
    select @attributeid=attributeid from attribute
    where attributename='HighlightedExpirationDate'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4147',@attributeid,'9051','1','1','0'
    ,'False',null,getdate(),null)
   
--attribute  'PremiumAuthenticatedExpirationDate'
    select @attributeid=attributeid from attribute
    where attributename='PremiumAuthenticatedExpirationDate'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4148',@attributeid,'9051','1','1','0'
    ,'False',null,getdate(),null)
   
--attribute  'SiteLastName'
    select @attributeid=attributeid from attribute
    where attributename='SiteLastName'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4149',@attributeid,'9051','4','1','100'
    ,'False',null,getdate(),null)
   
--attribute  'ROLFScore'
    select @attributeid=attributeid from attribute
    where attributename='ROLFScore'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4150',@attributeid,'9051','3','1','0'
    ,'False',null,getdate(),null)
   
--attribute  'MingleCookieMigrationFlag'
    select @attributeid=attributeid from attribute
    where attributename='MingleCookieMigrationFlag'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4151',@attributeid,'9051','0','1','0'
    ,'False',null,getdate(),null)
   
--attribute  'LastHotListPageVisit-WhoTeasedYou'
    select @attributeid=attributeid from attribute
    where attributename='LastHotListPageVisit-WhoTeasedYou'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4152',@attributeid,'9051','1','1','0'
    ,'False',null,getdate(),null)
   
--attribute  'LastHotListPageVisit-WhoIMedYou'
    select @attributeid=attributeid from attribute
    where attributename='LastHotListPageVisit-WhoIMedYou'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4153',@attributeid,'9051','1','1','0'
    ,'False',null,getdate(),null)
   
--attribute  'BrandLastLogonDateOldValue'
    select @attributeid=attributeid from attribute
    where attributename='BrandLastLogonDateOldValue'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4154',@attributeid,'90510','1','1','0'
    ,'False',null,getdate(),null)
   
--attribute  'BrandInsertDate'
    select @attributeid=attributeid from attribute
    where attributename='BrandInsertDate'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4155',@attributeid,'90510','1','3','0'
    ,'False',null,getdate(),null)
   
--attribute  'BrandLogonCount'
    select @attributeid=attributeid from attribute
    where attributename='BrandLogonCount'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4156',@attributeid,'90510','3','1','0'
    ,'False','0',getdate(),null)
   
--attribute  'BrandLastLogonDate'
    select @attributeid=attributeid from attribute
    where attributename='BrandLastLogonDate'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4157',@attributeid,'90510','1','1','0'
    ,'False',null,getdate(),null)
   
--attribute  'BrandBannerID'
    select @attributeid=attributeid from attribute
    where attributename='BrandBannerID'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4158',@attributeid,'90510','3','3','0'
    ,'False',null,getdate(),null)
   
--attribute  'BrandLandingPageID'
    select @attributeid=attributeid from attribute
    where attributename='BrandLandingPageID'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4159',@attributeid,'90510','3','3','0'
    ,'False',null,getdate(),null)
   
--attribute  'BrandPromotionID'
    select @attributeid=attributeid from attribute
    where attributename='BrandPromotionID'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4160',@attributeid,'90510','3','3','0'
    ,'False',null,getdate(),null)
   
--attribute  'PhotoStatusMask'
    select @attributeid=attributeid from attribute
    where attributename='PhotoStatusMask'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4161',@attributeid,'90510','2','1','0'
    ,'False',null,getdate(),null)
   
--attribute  'BrandLuggage'
    select @attributeid=attributeid from attribute
    where attributename='BrandLuggage'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4162',@attributeid,'90510','4','3','150'
    ,'False',null,getdate(),null)
   
--attribute  'RebrandFirstLogonDate'
    select @attributeid=attributeid from attribute
    where attributename='RebrandFirstLogonDate'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4163',@attributeid,'90510','1','3','0'
    ,'False',null,getdate(),null)
   