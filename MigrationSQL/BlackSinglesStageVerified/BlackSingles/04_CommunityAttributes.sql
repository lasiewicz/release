USE MNSYSTEM
GO



delete from attributegroup
where GroupID = 24


delete from AttributeCollectionAttribute
where GroupID = 24


--SitePages  
  declare @attributeid int
  
   
--attribute  'HaveChildren'
    select @attributeid=attributeid from attribute
    where attributename='HaveChildren'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4000',@attributeid,'24','3','1','0'
    ,'False',null,getdate(),null)
   
--attribute  'HasNewMutualYes'
    select @attributeid=attributeid from attribute
    where attributename='HasNewMutualYes'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4001',@attributeid,'24','0','1','0'
    ,'False','0',getdate(),null)
   
--attribute  'FacePageIntroduction'
    select @attributeid=attributeid from attribute
    where attributename='FacePageIntroduction'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4002',@attributeid,'24','4','1','600'
    ,'False',null,getdate(),null)
   
--attribute  'LastMatchNewsletterDate'
    select @attributeid=attributeid from attribute
    where attributename='LastMatchNewsletterDate'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4003',@attributeid,'24','1','1','0'
    ,'False',null,getdate(),null)
   
--attribute  'AdminSuspendedFlag'
    select @attributeid=attributeid from attribute
    where attributename='AdminSuspendedFlag'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4004',@attributeid,'24','0','1','0'
    ,'False','0',getdate(),null)
   
--attribute  'MajorType'
    select @attributeid=attributeid from attribute
    where attributename='MajorType'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4005',@attributeid,'24','3','1','0'
    ,'False',null,getdate(),null)
   
--attribute  'VacationStartDate'
    select @attributeid=attributeid from attribute
    where attributename='VacationStartDate'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4006',@attributeid,'24','1','1','0'
    ,'False',null,getdate(),null)
   
--attribute  'EssayOrganizations'
    select @attributeid=attributeid from attribute
    where attributename='EssayOrganizations'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4007',@attributeid,'24','4','5','4000'
    ,'False',null,getdate(),null)
   
--attribute  'EssayFirstThingNoticeAboutYou'
    select @attributeid=attributeid from attribute
    where attributename='EssayFirstThingNoticeAboutYou'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4008',@attributeid,'24','4','5','4000'
    ,'False',null,getdate(),null)
   
--attribute  'WhenIHaveTime'
    select @attributeid=attributeid from attribute
    where attributename='WhenIHaveTime'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4009',@attributeid,'24','4','5','100'
    ,'False',null,getdate(),null)
   
--attribute  'EssayDescribeHumor'
    select @attributeid=attributeid from attribute
    where attributename='EssayDescribeHumor'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4010',@attributeid,'24','4','5','4000'
    ,'False',null,getdate(),null)
   
--attribute  'CannotDoWithout'
    select @attributeid=attributeid from attribute
    where attributename='CannotDoWithout'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4011',@attributeid,'24','4','5','100'
    ,'False',null,getdate(),null)
   
--attribute  'EssayFriendsDescription'
    select @attributeid=attributeid from attribute
    where attributename='EssayFriendsDescription'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4012',@attributeid,'24','4','5','4000'
    ,'False',null,getdate(),null)
   
--attribute  'HasPhotoFlag'
    select @attributeid=attributeid from attribute
    where attributename='HasPhotoFlag'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4013',@attributeid,'24','0','1','0'
    ,'False','0',getdate(),null)
   
--attribute  'ReligionActivityLevel'
    select @attributeid=attributeid from attribute
    where attributename='ReligionActivityLevel'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4014',@attributeid,'24','3','1','0'
    ,'False',null,getdate(),null)
   
--attribute  'RelationshipStatus'
    select @attributeid=attributeid from attribute
    where attributename='RelationshipStatus'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4015',@attributeid,'24','3','1','0'
    ,'False',null,getdate(),null)
   
--attribute  'PerfectFirstDateEssay'
    select @attributeid=attributeid from attribute
    where attributename='PerfectFirstDateEssay'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4016',@attributeid,'24','4','5','4000'
    ,'False',null,getdate(),'4093')
   
--attribute  'IdealRelationshipEssay'
    select @attributeid=attributeid from attribute
    where attributename='IdealRelationshipEssay'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4017',@attributeid,'24','4','5','4000'
    ,'False',null,getdate(),'4092')
   
--attribute  'IsraelImmigrationDate'
    select @attributeid=attributeid from attribute
    where attributename='IsraelImmigrationDate'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4018',@attributeid,'24','3','1','0'
    ,'False',null,getdate(),null)
   
--attribute  'SelfDescription-old'
    select @attributeid=attributeid from attribute
    where attributename='SelfDescription-old'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4019',@attributeid,'24','4','5','100'
    ,'False',null,getdate(),null)
   
--attribute  'LearnFromThePastEssay'
    select @attributeid=attributeid from attribute
    where attributename='LearnFromThePastEssay'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4020',@attributeid,'24','4','5','4000'
    ,'False',null,getdate(),'4091')
   
--attribute  'OccupationDescription'
    select @attributeid=attributeid from attribute
    where attributename='OccupationDescription'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4021',@attributeid,'24','4','5','200'
    ,'False',null,getdate(),'4090')
   
--attribute  'HideMask'
    select @attributeid=attributeid from attribute
    where attributename='HideMask'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4022',@attributeid,'24','2','1','0'
    ,'False','0',getdate(),null)
   
--attribute  'DesiredMaritalStatus'
    select @attributeid=attributeid from attribute
    where attributename='DesiredMaritalStatus'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4023',@attributeid,'24','2','1','0'
    ,'False',null,getdate(),null)
   
--attribute  'Fashion'
    select @attributeid=attributeid from attribute
    where attributename='Fashion'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4024',@attributeid,'24','3','1','0'
    ,'False',null,getdate(),null)
   
--attribute  'Timeliness'
    select @attributeid=attributeid from attribute
    where attributename='Timeliness'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4025',@attributeid,'24','3','1','0'
    ,'False',null,getdate(),null)
   
--attribute  'SchoolsAttended-old'
    select @attributeid=attributeid from attribute
    where attributename='SchoolsAttended-old'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4026',@attributeid,'24','4','5','100'
    ,'False',null,getdate(),null)
   
--attribute  'SchoolsAttended'
    select @attributeid=attributeid from attribute
    where attributename='SchoolsAttended'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4027',@attributeid,'24','4','5','100'
    ,'False',null,getdate(),'4026')
   
--attribute  'FavoriteRestaurant-old'
    select @attributeid=attributeid from attribute
    where attributename='FavoriteRestaurant-old'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4028',@attributeid,'24','4','5','100'
    ,'False',null,getdate(),null)
   
--attribute  'FavoriteRestaurant'
    select @attributeid=attributeid from attribute
    where attributename='FavoriteRestaurant'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4029',@attributeid,'24','4','5','100'
    ,'False',null,getdate(),'4028')
   
--attribute  'MyGreatTripIdea-old'
    select @attributeid=attributeid from attribute
    where attributename='MyGreatTripIdea-old'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4030',@attributeid,'24','4','5','100'
    ,'False',null,getdate(),null)
   
--attribute  'MyGreatTripIdea'
    select @attributeid=attributeid from attribute
    where attributename='MyGreatTripIdea'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4031',@attributeid,'24','4','5','100'
    ,'False',null,getdate(),'4030')
   
--attribute  'FavoriteTV-old'
    select @attributeid=attributeid from attribute
    where attributename='FavoriteTV-old'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4032',@attributeid,'24','4','5','100'
    ,'False',null,getdate(),null)
   
--attribute  'FavoriteTV'
    select @attributeid=attributeid from attribute
    where attributename='FavoriteTV'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4033',@attributeid,'24','4','5','100'
    ,'False',null,getdate(),'4032')
   
--attribute  'FavoriteMovies-old'
    select @attributeid=attributeid from attribute
    where attributename='FavoriteMovies-old'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4034',@attributeid,'24','4','5','100'
    ,'False',null,getdate(),null)
   
--attribute  'FavoriteMovies'
    select @attributeid=attributeid from attribute
    where attributename='FavoriteMovies'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4035',@attributeid,'24','4','5','100'
    ,'False',null,getdate(),'4034')
   
--attribute  'FavoriteBands-old'
    select @attributeid=attributeid from attribute
    where attributename='FavoriteBands-old'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4036',@attributeid,'24','4','5','100'
    ,'False',null,getdate(),null)
   
--attribute  'FavoriteBands'
    select @attributeid=attributeid from attribute
    where attributename='FavoriteBands'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4037',@attributeid,'24','4','5','100'
    ,'False',null,getdate(),'4036')
   
--attribute  'DesiredMaxAge'
    select @attributeid=attributeid from attribute
    where attributename='DesiredMaxAge'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4038',@attributeid,'24','3','1','0'
    ,'False',null,getdate(),null)
   
--attribute  'SelfDescription'
    select @attributeid=attributeid from attribute
    where attributename='SelfDescription'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4039',@attributeid,'24','4','5','100'
    ,'False',null,getdate(),'4019')
   
--attribute  'DesiredMinAge'
    select @attributeid=attributeid from attribute
    where attributename='DesiredMinAge'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4040',@attributeid,'24','3','1','0'
    ,'False',null,getdate(),null)
   
--attribute  'NextRegistrationActionPageID'
    select @attributeid=attributeid from attribute
    where attributename='NextRegistrationActionPageID'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4041',@attributeid,'24','3','1','0'
    ,'False','0',getdate(),null)
   
--attribute  'LastUpdated'
    select @attributeid=attributeid from attribute
    where attributename='LastUpdated'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4042',@attributeid,'24','1','1','0'
    ,'False',null,getdate(),null)
   
--attribute  'EssayMoreThingsToAdd-old'
    select @attributeid=attributeid from attribute
    where attributename='EssayMoreThingsToAdd-old'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4043',@attributeid,'24','4','5','400'
    ,'False',null,getdate(),null)
   
--attribute  'EssayMoreThingsToAdd'
    select @attributeid=attributeid from attribute
    where attributename='EssayMoreThingsToAdd'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4044',@attributeid,'24','4','5','4000'
    ,'False',null,getdate(),'4043')
   
--attribute  'ChildrenCount'
    select @attributeid=attributeid from attribute
    where attributename='ChildrenCount'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4045',@attributeid,'24','3','1','0'
    ,'False',null,getdate(),null)
   
--attribute  'PerfectMatchEssay'
    select @attributeid=attributeid from attribute
    where attributename='PerfectMatchEssay'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4046',@attributeid,'24','4','5','4000'
    ,'False',null,getdate(),'4094')
   
--attribute  'AboutMe'
    select @attributeid=attributeid from attribute
    where attributename='AboutMe'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4047',@attributeid,'24','4','5','4000'
    ,'False',null,getdate(),'4095')
   
--attribute  'PromotionID'
    select @attributeid=attributeid from attribute
    where attributename='PromotionID'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4048',@attributeid,'24','3','3','0'
    ,'False','0',getdate(),null)
   
--attribute  'EssayPerfectDay-old'
    select @attributeid=attributeid from attribute
    where attributename='EssayPerfectDay-old'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4049',@attributeid,'24','4','5','400'
    ,'False',null,getdate(),null)
   
--attribute  'EssayPerfectDay'
    select @attributeid=attributeid from attribute
    where attributename='EssayPerfectDay'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4050',@attributeid,'24','4','5','400'
    ,'False',null,getdate(),'4049')
   
--attribute  'EssayImportantThings-old'
    select @attributeid=attributeid from attribute
    where attributename='EssayImportantThings-old'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4051',@attributeid,'24','4','5','400'
    ,'False',null,getdate(),null)
   
--attribute  'EssayImportantThings'
    select @attributeid=attributeid from attribute
    where attributename='EssayImportantThings'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4052',@attributeid,'24','4','5','4000'
    ,'False',null,getdate(),'4051')
   
--attribute  'EssayPastRelationship-old'
    select @attributeid=attributeid from attribute
    where attributename='EssayPastRelationship-old'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4053',@attributeid,'24','4','5','400'
    ,'False',null,getdate(),null)
   
--attribute  'EssayPastRelationship'
    select @attributeid=attributeid from attribute
    where attributename='EssayPastRelationship'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4054',@attributeid,'24','4','5','4000'
    ,'False',null,getdate(),'4053')
   
--attribute  'EssayGetToKnowMe-old'
    select @attributeid=attributeid from attribute
    where attributename='EssayGetToKnowMe-old'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4055',@attributeid,'24','4','5','400'
    ,'False',null,getdate(),null)
   
--attribute  'EssayGetToKnowMe'
    select @attributeid=attributeid from attribute
    where attributename='EssayGetToKnowMe'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4056',@attributeid,'24','4','5','4000'
    ,'False',null,getdate(),'4055')
   
--attribute  'EssayFirstDate-old'
    select @attributeid=attributeid from attribute
    where attributename='EssayFirstDate-old'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4057',@attributeid,'24','4','5','400'
    ,'False',null,getdate(),null)
   
--attribute  'EssayFirstDate'
    select @attributeid=attributeid from attribute
    where attributename='EssayFirstDate'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4058',@attributeid,'24','4','5','4000'
    ,'False',null,getdate(),'4057')
   
--attribute  'MailboxPreference'
    select @attributeid=attributeid from attribute
    where attributename='MailboxPreference'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4059',@attributeid,'24','2','1','0'
    ,'False','3',getdate(),null)
   
--attribute  'ColorCodeQuizAnswers'
    select @attributeid=attributeid from attribute
    where attributename='ColorCodeQuizAnswers'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4060',@attributeid,'24','4','1','3900'
    ,'False','0',getdate(),null)
   
--attribute  'AboutMyAppearanceEssay'
    select @attributeid=attributeid from attribute
    where attributename='AboutMyAppearanceEssay'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4061',@attributeid,'24','4','5','4000'
    ,'False',null,getdate(),null)
   
--attribute  'ColorAnalysis'
    select @attributeid=attributeid from attribute
    where attributename='ColorAnalysis'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4062',@attributeid,'24','0','1','0'
    ,'False','0',getdate(),null)
   
--attribute  'NewsletterMask'
    select @attributeid=attributeid from attribute
    where attributename='NewsletterMask'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4063',@attributeid,'24','2','1','0'
    ,'False','7',getdate(),null)
   
--attribute  'MatchNewsletterPeriod'
    select @attributeid=attributeid from attribute
    where attributename='MatchNewsletterPeriod'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4064',@attributeid,'24','3','1','0'
    ,'False','3',getdate(),null)
   
--attribute  'ColorCodeQuizCompleteDate'
    select @attributeid=attributeid from attribute
    where attributename='ColorCodeQuizCompleteDate'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4065',@attributeid,'24','1','1','100'
    ,'False',null,getdate(),null)
   
--attribute  'ColorCodeHidden'
    select @attributeid=attributeid from attribute
    where attributename='ColorCodeHidden'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4066',@attributeid,'24','4','1','100'
    ,'False','false',getdate(),null)
   
--attribute  'ColorCodeRedScore'
    select @attributeid=attributeid from attribute
    where attributename='ColorCodeRedScore'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4067',@attributeid,'24','4','1','100'
    ,'False','0',getdate(),null)
   
--attribute  'ColorCodeWhiteScore'
    select @attributeid=attributeid from attribute
    where attributename='ColorCodeWhiteScore'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4068',@attributeid,'24','4','1','100'
    ,'False','0',getdate(),null)
   
--attribute  'ColorCodeYellowScore'
    select @attributeid=attributeid from attribute
    where attributename='ColorCodeYellowScore'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4069',@attributeid,'24','4','1','100'
    ,'False','0',getdate(),null)
   
--attribute  'ColorCodeBlueScore'
    select @attributeid=attributeid from attribute
    where attributename='ColorCodeBlueScore'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4070',@attributeid,'24','4','1','100'
    ,'False','0',getdate(),null)
   
--attribute  'ColorCodeSecondaryColor'
    select @attributeid=attributeid from attribute
    where attributename='ColorCodeSecondaryColor'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4071',@attributeid,'24','4','1','100'
    ,'False','0',getdate(),null)
   
--attribute  'ColorCodePrimaryColor'
    select @attributeid=attributeid from attribute
    where attributename='ColorCodePrimaryColor'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4072',@attributeid,'24','4','1','100'
    ,'False','0',getdate(),null)
   
--attribute  'Religion'
    select @attributeid=attributeid from attribute
    where attributename='Religion'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4073',@attributeid,'24','3','1','0'
    ,'False',null,getdate(),null)
   
--attribute  'BodyType'
    select @attributeid=attributeid from attribute
    where attributename='BodyType'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4074',@attributeid,'24','2','1','0'
    ,'False',null,getdate(),null)
   
--attribute  'BannerID'
    select @attributeid=attributeid from attribute
    where attributename='BannerID'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4075',@attributeid,'24','3','3','0'
    ,'False','0',getdate(),null)
   
--attribute  'MessmoCapable'
    select @attributeid=attributeid from attribute
    where attributename='MessmoCapable'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4076',@attributeid,'24','3','1','0'
    ,'False',null,getdate(),null)
   
--attribute  'Headline'
    select @attributeid=attributeid from attribute
    where attributename='Headline'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4077',@attributeid,'24','4','5','400'
    ,'False',null,getdate(),'4096')
   
--attribute  'NewMemberEmailPeriod'
    select @attributeid=attributeid from attribute
    where attributename='NewMemberEmailPeriod'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4078',@attributeid,'24','3','1','0'
    ,'False','0',getdate(),null)
   
--attribute  'IdealVacation'
    select @attributeid=attributeid from attribute
    where attributename='IdealVacation'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4079',@attributeid,'24','4','5','100'
    ,'False',null,getdate(),null)
   
--attribute  'FavoriteMovieDirectorActor'
    select @attributeid=attributeid from attribute
    where attributename='FavoriteMovieDirectorActor'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4080',@attributeid,'24','4','5','200'
    ,'False',null,getdate(),null)
   
--attribute  'LandingPageID'
    select @attributeid=attributeid from attribute
    where attributename='LandingPageID'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4081',@attributeid,'24','3','3','0'
    ,'False','0',getdate(),null)
   
--attribute  'FavoriteBookAuthor'
    select @attributeid=attributeid from attribute
    where attributename='FavoriteBookAuthor'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4082',@attributeid,'24','4','5','200'
    ,'False',null,getdate(),null)
   
--attribute  'FavoriteAlbumSongArtist'
    select @attributeid=attributeid from attribute
    where attributename='FavoriteAlbumSongArtist'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4083',@attributeid,'24','4','5','200'
    ,'False',null,getdate(),null)
   
--attribute  'DisplayPhotosToGuests'
    select @attributeid=attributeid from attribute
    where attributename='DisplayPhotosToGuests'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4084',@attributeid,'24','0','1','0'
    ,'False','1',getdate(),null)
   
--attribute  'Refcd'
    select @attributeid=attributeid from attribute
    where attributename='Refcd'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4085',@attributeid,'24','4','3','200'
    ,'False',null,getdate(),null)
   
--attribute  'GenderMask'
    select @attributeid=attributeid from attribute
    where attributename='GenderMask'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4086',@attributeid,'24','2','1','0'
    ,'False',null,getdate(),null)
   
--attribute  'UserName'
    select @attributeid=attributeid from attribute
    where attributename='UserName'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4087',@attributeid,'24','4','5','200'
    ,'False',null,getdate(),null)
   
--attribute  'StudiesEmphasis-old'
    select @attributeid=attributeid from attribute
    where attributename='StudiesEmphasis-old'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4088',@attributeid,'24','4','5','400'
    ,'False',null,getdate(),null)
   
--attribute  'GrewUpIn-old'
    select @attributeid=attributeid from attribute
    where attributename='GrewUpIn-old'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4089',@attributeid,'24','4','5','400'
    ,'False',null,getdate(),null)
   
--attribute  'OccupationDescription-old'
    select @attributeid=attributeid from attribute
    where attributename='OccupationDescription-old'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4090',@attributeid,'24','4','5','400'
    ,'False',null,getdate(),null)
   
--attribute  'LearnFromThePastEssay-old'
    select @attributeid=attributeid from attribute
    where attributename='LearnFromThePastEssay-old'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4091',@attributeid,'24','4','5','400'
    ,'False',null,getdate(),null)
   
--attribute  'IdealRelationshipEssay-old'
    select @attributeid=attributeid from attribute
    where attributename='IdealRelationshipEssay-old'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4092',@attributeid,'24','4','5','400'
    ,'False',null,getdate(),null)
   
--attribute  'PerfectFirstDateEssay-old'
    select @attributeid=attributeid from attribute
    where attributename='PerfectFirstDateEssay-old'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4093',@attributeid,'24','4','5','400'
    ,'False',null,getdate(),null)
   
--attribute  'PerfectMatchEssay-old'
    select @attributeid=attributeid from attribute
    where attributename='PerfectMatchEssay-old'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4094',@attributeid,'24','4','5','400'
    ,'False',null,getdate(),null)
   
--attribute  'AboutMe-old'
    select @attributeid=attributeid from attribute
    where attributename='AboutMe-old'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4095',@attributeid,'24','4','5','400'
    ,'False',null,getdate(),null)
   
--attribute  'Headline-old'
    select @attributeid=attributeid from attribute
    where attributename='Headline-old'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4096',@attributeid,'24','4','5','400'
    ,'False',null,getdate(),null)
   
--attribute  'SuspendMask'
    select @attributeid=attributeid from attribute
    where attributename='SuspendMask'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4097',@attributeid,'24','2','1','0'
    ,'False','0',getdate(),null)
   
--attribute  'AlertEmailMask'
    select @attributeid=attributeid from attribute
    where attributename='AlertEmailMask'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4098',@attributeid,'24','2','1','0'
    ,'False','15',getdate(),null)
   
--attribute  'AwayMessage'
    select @attributeid=attributeid from attribute
    where attributename='AwayMessage'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4099',@attributeid,'24','4','1','500'
    ,'False',null,getdate(),null)
   
--attribute  'DesiredReligious'
    select @attributeid=attributeid from attribute
    where attributename='DesiredReligious'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4100',@attributeid,'24','2','1','0'
    ,'False','0',getdate(),null)
   
--attribute  'SelfSuspendedReasonType'
    select @attributeid=attributeid from attribute
    where attributename='SelfSuspendedReasonType'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4101',@attributeid,'24','3','1','0'
    ,'False',null,getdate(),null)
   
--attribute  'Luggage'
    select @attributeid=attributeid from attribute
    where attributename='Luggage'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4102',@attributeid,'24','4','3','150'
    ,'False',null,getdate(),null)
   
--attribute  'MaritalStatus'
    select @attributeid=attributeid from attribute
    where attributename='MaritalStatus'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4103',@attributeid,'24','3','1','0'
    ,'False',null,getdate(),null)
   
--attribute  'SelfSuspendedFlag'
    select @attributeid=attributeid from attribute
    where attributename='SelfSuspendedFlag'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4104',@attributeid,'24','0','1','0'
    ,'False','0',getdate(),null)
   
--attribute  'ReportAbuseCount'
    select @attributeid=attributeid from attribute
    where attributename='ReportAbuseCount'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4105',@attributeid,'24','3','1','0'
    ,'False','0',getdate(),null)
   
--attribute  'ChatBanned'
    select @attributeid=attributeid from attribute
    where attributename='ChatBanned'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4106',@attributeid,'24','3','1','0'
    ,'False','0',getdate(),null)
   
--attribute  'RegistrationTargetPage'
    select @attributeid=attributeid from attribute
    where attributename='RegistrationTargetPage'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4107',@attributeid,'24','4','1','3900'
    ,'False',null,getdate(),null)
   
--attribute  'RegistrationReferrerPage'
    select @attributeid=attributeid from attribute
    where attributename='RegistrationReferrerPage'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4108',@attributeid,'24','4','1','3900'
    ,'False',null,getdate(),null)
   
--attribute  'HasNewMail'
    select @attributeid=attributeid from attribute
    where attributename='HasNewMail'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4109',@attributeid,'24','0','1','0'
    ,'False',null,getdate(),null)
   
--attribute  'LastEmailReceiveDate'
    select @attributeid=attributeid from attribute
    where attributename='LastEmailReceiveDate'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4110',@attributeid,'24','1','1','0'
    ,'False',null,getdate(),null)
   
--attribute  'RelationshipMask'
    select @attributeid=attributeid from attribute
    where attributename='RelationshipMask'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4111',@attributeid,'24','2','1','0'
    ,'False',null,getdate(),null)
   
--attribute  'CommunityEmailCount'
    select @attributeid=attributeid from attribute
    where attributename='CommunityEmailCount'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4112',@attributeid,'24','3','1','0'
    ,'False','0',getdate(),null)
   
--attribute  'VoiceProfilePath'
    select @attributeid=attributeid from attribute
    where attributename='VoiceProfilePath'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4113',@attributeid,'24','4','1','255'
    ,'False',null,getdate(),null)
   
--attribute  'OptOutDate'
    select @attributeid=attributeid from attribute
    where attributename='OptOutDate'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4114',@attributeid,'24','1','1','0'
    ,'False',null,getdate(),null)
   
--attribute  'TelephonyStatusMask'
    select @attributeid=attributeid from attribute
    where attributename='TelephonyStatusMask'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4115',@attributeid,'24','2','1','0'
    ,'False','0',getdate(),null)
   
--attribute  'OptOut'
    select @attributeid=attributeid from attribute
    where attributename='OptOut'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4116',@attributeid,'24','0','1','0'
    ,'False','0',getdate(),null)
   
--attribute  'StudiesEmphasis'
    select @attributeid=attributeid from attribute
    where attributename='StudiesEmphasis'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4117',@attributeid,'24','4','5','50'
    ,'False',null,getdate(),'4088')
   
--attribute  'GrewUpIn'
    select @attributeid=attributeid from attribute
    where attributename='GrewUpIn'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4118',@attributeid,'24','4','5','1000'
    ,'False',null,getdate(),'4089')
   
--attribute  'DomainAliasID'
    select @attributeid=attributeid from attribute
    where attributename='DomainAliasID'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4119',@attributeid,'24','3','1','0'
    ,'False',null,getdate(),null)
   
--attribute  'IsrealImmigrationYear'
    select @attributeid=attributeid from attribute
    where attributename='IsrealImmigrationYear'
      if (@attributeid is not null)
     insert into [mnsystem].[dbo].[attributegroup]
    ([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
    ,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
    values
      ('4120',@attributeid,'24','3','1','0'
    ,'False',null,getdate(),null)
   
insert into AttributeCollectionAttribute 
select attributecollectionid,attributeid,24,optionalflag
from AttributeCollectionAttribute
where GroupID=23
