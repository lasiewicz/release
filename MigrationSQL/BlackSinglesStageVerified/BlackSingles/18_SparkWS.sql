use mnSystem

go
declare @attributeid int

select @attributeid=attributeid from Attribute where AttributeName='PassedFraudCheck'

--select * from AccessTicket
insert into ProfileSectionDefinition values (34, 'Mingle profile data BlackSingles')
insert into AccessTicketProfileSection values (7, 34)
insert into ProfileSectionDefinitionCommunity values (34, 24)
insert into ProfileSectionDefinitionSite values (34, 9051)
insert into ProfileSectionDefinitionPhoto values (34, 24)
insert into ProfileSectionDefinitionAttribute values(34,12,4,0,0,0,2)
insert into ProfileSectionDefinitionAttribute values(34,69,1,24,0,0,0)
insert into ProfileSectionDefinitionAttribute values(34,70,2,0,0,0,0)
insert into ProfileSectionDefinitionAttribute values(34,120,2,24,0,0,0)
insert into ProfileSectionDefinitionAttribute values(34,122,1,0,0,0,0)
insert into ProfileSectionDefinitionAttribute values(34,268,1,0,0,0,0)
insert into ProfileSectionDefinitionAttribute values(34,270,1,24,0,0,0)
insert into ProfileSectionDefinitionAttribute values(34,302,4,0,0,0,2)
insert into ProfileSectionDefinitionAttribute values(34,495,2,24,9051,90510,0)
insert into ProfileSectionDefinitionAttribute values(34,497,2,24,9051,90510,0)
insert into ProfileSectionDefinitionAttribute values(34,501,2,24,9051,0,0)
insert into ProfileSectionDefinitionAttribute values(34,506,1,24,0,0,0)
insert into ProfileSectionDefinitionAttribute values(34,509,1,0,0,0,0)
insert into ProfileSectionDefinitionAttribute values(34,@attributeid,1,0,0,0,0)

insert into ProfileSectionDefinition values (35, 'Get demographic data BlackSingles')
insert into ProfileSectionDefinitionCommunity values (35, 24)
insert into ProfileSectionDefinitionAttribute values(35,69,1,24,0,0,0)
insert into ProfileSectionDefinitionAttribute values(35,70,2,0,0,0,0)
insert into ProfileSectionDefinitionAttribute values(35,88,1,0,0,0,0)
insert into ProfileSectionDefinitionAttribute values(35,122,1,0,0,0,0)



