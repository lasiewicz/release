
  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List0]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList0 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List0] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List0]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList0 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List0] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save0]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList0 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList0 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList0 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save0] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save0]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList0 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save0] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List0]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList0 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List0] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List1]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList1 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List1] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List1]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList1 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List1] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save1]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList1 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList1 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList1 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save1] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save1]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList1 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save1] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List1]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList1 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List1] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List2]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList2 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List2] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List2]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList2 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List2] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save2]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList2 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList2 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList2 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save2] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save2]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList2 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save2] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List2]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList2 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List2] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List3]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList3 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List3] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List3]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList3 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List3] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save3]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList3 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList3 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList3 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save3] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save3]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList3 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save3] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List3]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList3 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List3] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List4]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList4 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List4] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List4]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList4 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List4] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save4]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList4 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList4 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList4 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save4] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save4]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList4 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save4] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List4]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList4 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List4] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List5]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList5 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List5] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List5]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList5 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List5] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save5]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList5 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList5 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList5 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save5] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save5]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList5 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save5] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List5]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList5 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List5] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List6]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList6 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List6] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List6]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList6 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List6] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save6]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList6 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList6 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList6 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save6] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save6]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList6 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save6] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List6]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList6 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List6] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List7]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList7 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List7] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List7]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList7 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List7] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save7]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList7 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList7 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList7 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save7] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save7]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList7 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save7] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List7]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList7 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List7] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List8]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList8 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List8] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List8]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList8 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List8] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save8]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList8 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList8 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList8 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save8] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save8]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList8 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save8] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List8]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList8 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List8] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List9]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList9 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List9] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List9]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList9 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List9] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save9]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList9 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList9 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList9 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save9] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save9]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList9 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save9] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List9]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList9 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List9] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List10]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList10 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List10] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List10]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList10 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List10] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save10]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList10 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList10 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList10 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save10] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save10]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList10 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save10] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List10]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList10 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List10] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List11]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList11 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List11] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List11]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList11 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List11] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save11]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList11 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList11 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList11 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save11] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save11]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList11 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save11] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List11]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList11 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List11] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List12]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList12 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List12] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List12]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList12 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List12] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save12]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList12 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList12 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList12 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save12] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save12]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList12 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save12] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List12]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList12 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List12] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List13]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList13 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List13] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List13]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList13 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List13] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save13]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList13 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList13 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList13 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save13] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save13]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList13 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save13] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List13]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList13 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List13] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List14]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList14 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List14] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List14]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList14 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List14] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save14]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList14 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList14 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList14 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save14] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save14]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList14 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save14] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List14]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList14 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List14] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List15]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList15 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List15] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List15]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList15 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List15] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save15]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList15 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList15 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList15 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save15] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save15]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList15 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save15] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List15]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList15 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List15] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List16]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList16 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List16] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List16]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList16 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List16] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save16]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList16 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList16 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList16 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save16] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save16]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList16 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save16] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List16]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList16 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List16] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List17]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList17 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List17] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List17]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList17 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List17] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save17]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList17 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList17 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList17 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save17] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save17]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList17 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save17] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List17]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList17 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List17] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List18]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList18 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List18] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List18]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList18 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List18] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save18]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList18 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList18 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList18 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save18] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save18]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList18 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save18] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List18]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList18 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List18] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List19]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList19 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List19] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List19]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList19 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List19] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save19]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList19 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList19 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList19 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save19] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save19]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList19 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save19] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List19]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList19 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List19] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List20]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList20 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List20] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List20]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList20 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List20] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save20]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList20 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList20 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList20 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save20] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save20]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList20 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save20] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List20]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList20 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List20] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List21]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList21 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List21] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List21]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList21 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List21] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save21]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList21 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList21 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList21 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save21] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save21]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList21 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save21] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List21]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList21 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List21] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List22]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList22 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List22] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List22]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList22 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List22] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save22]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList22 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList22 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList22 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save22] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save22]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList22 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save22] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List22]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList22 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List22] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List23]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList23 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List23] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List23]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList23 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List23] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save23]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList23 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList23 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList23 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save23] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save23]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList23 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save23] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List23]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList23 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List23] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List24]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList24 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List24] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List24]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList24 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List24] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save24]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList24 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList24 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList24 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save24] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save24]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList24 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save24] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List24]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList24 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List24] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List25]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList25 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List25] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List25]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList25 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List25] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save25]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList25 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList25 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList25 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save25] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save25]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList25 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save25] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List25]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList25 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List25] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List26]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList26 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List26] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List26]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList26 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List26] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save26]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList26 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList26 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList26 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save26] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save26]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList26 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save26] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List26]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList26 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List26] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List27]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList27 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List27] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List27]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList27 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List27] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save27]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList27 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList27 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList27 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save27] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save27]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList27 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save27] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List27]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList27 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List27] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List28]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList28 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List28] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List28]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList28 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List28] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save28]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList28 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList28 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList28 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save28] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save28]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList28 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save28] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List28]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList28 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List28] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List29]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList29 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List29] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List29]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList29 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List29] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save29]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList29 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList29 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList29 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save29] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save29]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList29 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save29] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List29]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList29 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List29] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List30]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList30 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List30] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List30]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList30 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List30] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save30]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList30 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList30 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList30 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save30] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save30]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList30 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save30] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List30]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList30 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List30] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List31]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList31 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List31] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List31]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList31 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List31] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save31]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList31 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList31 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList31 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save31] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save31]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList31 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save31] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List31]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList31 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List31] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List32]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList32 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List32] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List32]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList32 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List32] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save32]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList32 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList32 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList32 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save32] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save32]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList32 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save32] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List32]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList32 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List32] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List33]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList33 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List33] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List33]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList33 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List33] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save33]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList33 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList33 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList33 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save33] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save33]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList33 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save33] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List33]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList33 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List33] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List34]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList34 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List34] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List34]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList34 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List34] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save34]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList34 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList34 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList34 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save34] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save34]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList34 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save34] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List34]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList34 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List34] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List35]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList35 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List35] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List35]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList35 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List35] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save35]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList35 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList35 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList35 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save35] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save35]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList35 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save35] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List35]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList35 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List35] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List36]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList36 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List36] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List36]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList36 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List36] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save36]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList36 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList36 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList36 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save36] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save36]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList36 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save36] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List36]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList36 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List36] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List37]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList37 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List37] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List37]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList37 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List37] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save37]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList37 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList37 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList37 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save37] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save37]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList37 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save37] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List37]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList37 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List37] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List38]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList38 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List38] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List38]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList38 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List38] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save38]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList38 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList38 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList38 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save38] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save38]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList38 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save38] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List38]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList38 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List38] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List39]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList39 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List39] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List39]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList39 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List39] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save39]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList39 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList39 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList39 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save39] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save39]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList39 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save39] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List39]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList39 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List39] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List40]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList40 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List40] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List40]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList40 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List40] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save40]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList40 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList40 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList40 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save40] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save40]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList40 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save40] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List40]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList40 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List40] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List41]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList41 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List41] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List41]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList41 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List41] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save41]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList41 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList41 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList41 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save41] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save41]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList41 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save41] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List41]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList41 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List41] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List42]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList42 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List42] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List42]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList42 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List42] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save42]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList42 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList42 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList42 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save42] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save42]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList42 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save42] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List42]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList42 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List42] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List43]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList43 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List43] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List43]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList43 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List43] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save43]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList43 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList43 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList43 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save43] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save43]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList43 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save43] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List43]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList43 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List43] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List44]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList44 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List44] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List44]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList44 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List44] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save44]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList44 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList44 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList44 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save44] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save44]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList44 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save44] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List44]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList44 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List44] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List45]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList45 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List45] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List45]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList45 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List45] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save45]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList45 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList45 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList45 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save45] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save45]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList45 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save45] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List45]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList45 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List45] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List46]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList46 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List46] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List46]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList46 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List46] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save46]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList46 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList46 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList46 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save46] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save46]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList46 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save46] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List46]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList46 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List46] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List47]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList47 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List47] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List47]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList47 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List47] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save47]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList47 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList47 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList47 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save47] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save47]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList47 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save47] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List47]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList47 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List47] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List48]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList48 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List48] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List48]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList48 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List48] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save48]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList48 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList48 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList48 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save48] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save48]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList48 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save48] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List48]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList48 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List48] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List49]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList49 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List49] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List49]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList49 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List49] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save49]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList49 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList49 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList49 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save49] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save49]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList49 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save49] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List49]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList49 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List49] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List50]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList50 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List50] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List50]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList50 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List50] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save50]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList50 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList50 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList50 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save50] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save50]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList50 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save50] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List50]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList50 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List50] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List51]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList51 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List51] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List51]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList51 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List51] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save51]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList51 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList51 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList51 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save51] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save51]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList51 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save51] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List51]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList51 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List51] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List52]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList52 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List52] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List52]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList52 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List52] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save52]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList52 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList52 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList52 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save52] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save52]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList52 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save52] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List52]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList52 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List52] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List53]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList53 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List53] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List53]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList53 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List53] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save53]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList53 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList53 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList53 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save53] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save53]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList53 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save53] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List53]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList53 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List53] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List54]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList54 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List54] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List54]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList54 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List54] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save54]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList54 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList54 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList54 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save54] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save54]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList54 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save54] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List54]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList54 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List54] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List55]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList55 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List55] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List55]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList55 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List55] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save55]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList55 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList55 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList55 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save55] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save55]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList55 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save55] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List55]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList55 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List55] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List56]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList56 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List56] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List56]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList56 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List56] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save56]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList56 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList56 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList56 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save56] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save56]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList56 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save56] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List56]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList56 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List56] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List57]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList57 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List57] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List57]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList57 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List57] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save57]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList57 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList57 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList57 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save57] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save57]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList57 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save57] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List57]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList57 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List57] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List58]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList58 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List58] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List58]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList58 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List58] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save58]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList58 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList58 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList58 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save58] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save58]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList58 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save58] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List58]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList58 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List58] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List59]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList59 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List59] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List59]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList59 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List59] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save59]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList59 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList59 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList59 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save59] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save59]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList59 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save59] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List59]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList59 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List59] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List60]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList60 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List60] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List60]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList60 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List60] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save60]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList60 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList60 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList60 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save60] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save60]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList60 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save60] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List60]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList60 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List60] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List61]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList61 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List61] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List61]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList61 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List61] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save61]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList61 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList61 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList61 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save61] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save61]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList61 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save61] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List61]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList61 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List61] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List62]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList62 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List62] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List62]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList62 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List62] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save62]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList62 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList62 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList62 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save62] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save62]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList62 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save62] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List62]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList62 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List62] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List63]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList63 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List63] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List63]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList63 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List63] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save63]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList63 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList63 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList63 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save63] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save63]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList63 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save63] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List63]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList63 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List63] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List64]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList64 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List64] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List64]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList64 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List64] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save64]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList64 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList64 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList64 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save64] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save64]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList64 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save64] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List64]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList64 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List64] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List65]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList65 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List65] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List65]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList65 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List65] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save65]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList65 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList65 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList65 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save65] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save65]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList65 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save65] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List65]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList65 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List65] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List66]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList66 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List66] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List66]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList66 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List66] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save66]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList66 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList66 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList66 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save66] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save66]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList66 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save66] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List66]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList66 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List66] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List67]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList67 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List67] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List67]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList67 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List67] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save67]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList67 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList67 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList67 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save67] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save67]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList67 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save67] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List67]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList67 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List67] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List68]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList68 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List68] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List68]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList68 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List68] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save68]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList68 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList68 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList68 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save68] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save68]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList68 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save68] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List68]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList68 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List68] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List69]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList69 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List69] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List69]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList69 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List69] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save69]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList69 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList69 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList69 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save69] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save69]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList69 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save69] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List69]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList69 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List69] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List70]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList70 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List70] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List70]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList70 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List70] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save70]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList70 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList70 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList70 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save70] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save70]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList70 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save70] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List70]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList70 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List70] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List71]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList71 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List71] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List71]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList71 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List71] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save71]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList71 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList71 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList71 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save71] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save71]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList71 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save71] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List71]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList71 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List71] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List72]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList72 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List72] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List72]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList72 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List72] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save72]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList72 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList72 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList72 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save72] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save72]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList72 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save72] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List72]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList72 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List72] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List73]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList73 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List73] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List73]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList73 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List73] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save73]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList73 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList73 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList73 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save73] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save73]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList73 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save73] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List73]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList73 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List73] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List74]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList74 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List74] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List74]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList74 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List74] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save74]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList74 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList74 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList74 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save74] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save74]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList74 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save74] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List74]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList74 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List74] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List75]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList75 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List75] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List75]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList75 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List75] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save75]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList75 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList75 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList75 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save75] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save75]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList75 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save75] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List75]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList75 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List75] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List76]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList76 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List76] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List76]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList76 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List76] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save76]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList76 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList76 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList76 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save76] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save76]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList76 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save76] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List76]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList76 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List76] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List77]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList77 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List77] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List77]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList77 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List77] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save77]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList77 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList77 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList77 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save77] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save77]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList77 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save77] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List77]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList77 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List77] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List78]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList78 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List78] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List78]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList78 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List78] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save78]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList78 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList78 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList78 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save78] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save78]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList78 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save78] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List78]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList78 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List78] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List79]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList79 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List79] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List79]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList79 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List79] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save79]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList79 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList79 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList79 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save79] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save79]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList79 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save79] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List79]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList79 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List79] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List80]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList80 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List80] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List80]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList80 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List80] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save80]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList80 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList80 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList80 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save80] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save80]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList80 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save80] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List80]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList80 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List80] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List81]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList81 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List81] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List81]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList81 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List81] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save81]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList81 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList81 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList81 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save81] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save81]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList81 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save81] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List81]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList81 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List81] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List82]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList82 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List82] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List82]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList82 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List82] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save82]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList82 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList82 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList82 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save82] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save82]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList82 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save82] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List82]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList82 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List82] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List83]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList83 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List83] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List83]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList83 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List83] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save83]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList83 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList83 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList83 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save83] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save83]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList83 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save83] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List83]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList83 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List83] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List84]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList84 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List84] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List84]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList84 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List84] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save84]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList84 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList84 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList84 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save84] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save84]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList84 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save84] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List84]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList84 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List84] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List85]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList85 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List85] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List85]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList85 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List85] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save85]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList85 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList85 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList85 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save85] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save85]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList85 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save85] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List85]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList85 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List85] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List86]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList86 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List86] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List86]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList86 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List86] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save86]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList86 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList86 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList86 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save86] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save86]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList86 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save86] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List86]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList86 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List86] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List87]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList87 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List87] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List87]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList87 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List87] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save87]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList87 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList87 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList87 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save87] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save87]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList87 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save87] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List87]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList87 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List87] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List88]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList88 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List88] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List88]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList88 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List88] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save88]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList88 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList88 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList88 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save88] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save88]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList88 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save88] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List88]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList88 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List88] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List89]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList89 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List89] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List89]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList89 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List89] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save89]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList89 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList89 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList89 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save89] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save89]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList89 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save89] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List89]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList89 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List89] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List90]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList90 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List90] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List90]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList90 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List90] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save90]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList90 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList90 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList90 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save90] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save90]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList90 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save90] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List90]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList90 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List90] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List91]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList91 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List91] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List91]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList91 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List91] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save91]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList91 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList91 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList91 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save91] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save91]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList91 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save91] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List91]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList91 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List91] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List92]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList92 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List92] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List92]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList92 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List92] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save92]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList92 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList92 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList92 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save92] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save92]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList92 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save92] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List92]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList92 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List92] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List93]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList93 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List93] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List93]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList93 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List93] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save93]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList93 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList93 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList93 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save93] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save93]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList93 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save93] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List93]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList93 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List93] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List94]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList94 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List94] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List94]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList94 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List94] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save94]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList94 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList94 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList94 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save94] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save94]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList94 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save94] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List94]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList94 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List94] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List95]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList95 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List95] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List95]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList95 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List95] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save95]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList95 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList95 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList95 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save95] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save95]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList95 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save95] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List95]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList95 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List95] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List96]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList96 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List96] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List96]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList96 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List96] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save96]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList96 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList96 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList96 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save96] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save96]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList96 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save96] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List96]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList96 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List96] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List97]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList97 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List97] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List97]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList97 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List97] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save97]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList97 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList97 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList97 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save97] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save97]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList97 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save97] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List97]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList97 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List97] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List98]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList98 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List98] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List98]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList98 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List98] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save98]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList98 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList98 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList98 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save98] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save98]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList98 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save98] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List98]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList98 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List98] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List99]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList99 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List99] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List99]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList99 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List99] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save99]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList99 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList99 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList99 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save99] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save99]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList99 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save99] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List99]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList99 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List99] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List100]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList100 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List100] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List100]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList100 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List100] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save100]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList100 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList100 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList100 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save100] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save100]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList100 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save100] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List100]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList100 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List100] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List101]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList101 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List101] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List101]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList101 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List101] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save101]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList101 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList101 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList101 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save101] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save101]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList101 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save101] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List101]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList101 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List101] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List102]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList102 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List102] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List102]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList102 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List102] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save102]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList102 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList102 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList102 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save102] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save102]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList102 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save102] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List102]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList102 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List102] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List103]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList103 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List103] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List103]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList103 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List103] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save103]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList103 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList103 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList103 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save103] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save103]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList103 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save103] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List103]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList103 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List103] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List104]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList104 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List104] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List104]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList104 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List104] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save104]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList104 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList104 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList104 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save104] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save104]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList104 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save104] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List104]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList104 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List104] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List105]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList105 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List105] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List105]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList105 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List105] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save105]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList105 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList105 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList105 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save105] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save105]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList105 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save105] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List105]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList105 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List105] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List106]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList106 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List106] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List106]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList106 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List106] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save106]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList106 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList106 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList106 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save106] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save106]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList106 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save106] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List106]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList106 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List106] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List107]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList107 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List107] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List107]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList107 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List107] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save107]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList107 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList107 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList107 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save107] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save107]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList107 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save107] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List107]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList107 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List107] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List108]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList108 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List108] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List108]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList108 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List108] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save108]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList108 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList108 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList108 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save108] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save108]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList108 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save108] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List108]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList108 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List108] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List109]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList109 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List109] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List109]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList109 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List109] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save109]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList109 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList109 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList109 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save109] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save109]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList109 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save109] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List109]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList109 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List109] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List110]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList110 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List110] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List110]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList110 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List110] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save110]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList110 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList110 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList110 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save110] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save110]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList110 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save110] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List110]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList110 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List110] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List111]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList111 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List111] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List111]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList111 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List111] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save111]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList111 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList111 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList111 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save111] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save111]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList111 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save111] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List111]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList111 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List111] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List112]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList112 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List112] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List112]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList112 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List112] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save112]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList112 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList112 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList112 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save112] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save112]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList112 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save112] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List112]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList112 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List112] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List113]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList113 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List113] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List113]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList113 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List113] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save113]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList113 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList113 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList113 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save113] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save113]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList113 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save113] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List113]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList113 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List113] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List114]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList114 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List114] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List114]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList114 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List114] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save114]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList114 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList114 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList114 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save114] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save114]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList114 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save114] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List114]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList114 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List114] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List115]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList115 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List115] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List115]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList115 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List115] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save115]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList115 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList115 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList115 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save115] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save115]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList115 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save115] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List115]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList115 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List115] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List116]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList116 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List116] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List116]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList116 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List116] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save116]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList116 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList116 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList116 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save116] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save116]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList116 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save116] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List116]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList116 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List116] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List117]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList117 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List117] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List117]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList117 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List117] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save117]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList117 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList117 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList117 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save117] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save117]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList117 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save117] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List117]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList117 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List117] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List118]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList118 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List118] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List118]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList118 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List118] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save118]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList118 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList118 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList118 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save118] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save118]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList118 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save118] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List118]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList118 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List118] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List119]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList119 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List119] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List119]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList119 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List119] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save119]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList119 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList119 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList119 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save119] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save119]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList119 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save119] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List119]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList119 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List119] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List120]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList120 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List120] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List120]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList120 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List120] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save120]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList120 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList120 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList120 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save120] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save120]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList120 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save120] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List120]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList120 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List120] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List121]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList121 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List121] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List121]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList121 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List121] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save121]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList121 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList121 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList121 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save121] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save121]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList121 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save121] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List121]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList121 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List121] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List122]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList122 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List122] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List122]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList122 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List122] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save122]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList122 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList122 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList122 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save122] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save122]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList122 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save122] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List122]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList122 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List122] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List123]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList123 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List123] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List123]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList123 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List123] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save123]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList123 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList123 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList123 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save123] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save123]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList123 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save123] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List123]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList123 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List123] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List124]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList124 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List124] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List124]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList124 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List124] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save124]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList124 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList124 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList124 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save124] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save124]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList124 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save124] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List124]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList124 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List124] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List125]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList125 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List125] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List125]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList125 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List125] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save125]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList125 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList125 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList125 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save125] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save125]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList125 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save125] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List125]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList125 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List125] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List126]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList126 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List126] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List126]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList126 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List126] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save126]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList126 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList126 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList126 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save126] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save126]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList126 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save126] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List126]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList126 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List126] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List127]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList127 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List127] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List127]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList127 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List127] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save127]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList127 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList127 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList127 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save127] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save127]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList127 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save127] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List127]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList127 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List127] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List128]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList128 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List128] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List128]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList128 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List128] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save128]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList128 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList128 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList128 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save128] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save128]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList128 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save128] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List128]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList128 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List128] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List129]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList129 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List129] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List129]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList129 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List129] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save129]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList129 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList129 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList129 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save129] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save129]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList129 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save129] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List129]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList129 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List129] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List130]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList130 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List130] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List130]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList130 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List130] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save130]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList130 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList130 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList130 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save130] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save130]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList130 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save130] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List130]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList130 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List130] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List131]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList131 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List131] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List131]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList131 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List131] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save131]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList131 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList131 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList131 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save131] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save131]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList131 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save131] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List131]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList131 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List131] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List132]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList132 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List132] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List132]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList132 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List132] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save132]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList132 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList132 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList132 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save132] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save132]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList132 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save132] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List132]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList132 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List132] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List133]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList133 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List133] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List133]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList133 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List133] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save133]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList133 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList133 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList133 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save133] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save133]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList133 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save133] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List133]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList133 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List133] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List134]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList134 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List134] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List134]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList134 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List134] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save134]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList134 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList134 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList134 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save134] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save134]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList134 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save134] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List134]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList134 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List134] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List135]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList135 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List135] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List135]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList135 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List135] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save135]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList135 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList135 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList135 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save135] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save135]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList135 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save135] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List135]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList135 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List135] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List136]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList136 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List136] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List136]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList136 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List136] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save136]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList136 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList136 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList136 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save136] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save136]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList136 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save136] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List136]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList136 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List136] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List137]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList137 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List137] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List137]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList137 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List137] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save137]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList137 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList137 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList137 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save137] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save137]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList137 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save137] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List137]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList137 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List137] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List138]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList138 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List138] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List138]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList138 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List138] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save138]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList138 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList138 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList138 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save138] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save138]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList138 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save138] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List138]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList138 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List138] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List139]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList139 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List139] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List139]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList139 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List139] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save139]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList139 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList139 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList139 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save139] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save139]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList139 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save139] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List139]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList139 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List139] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List140]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList140 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List140] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List140]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList140 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List140] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save140]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList140 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList140 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList140 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save140] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save140]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList140 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save140] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List140]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList140 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List140] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List141]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList141 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List141] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List141]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList141 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List141] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save141]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList141 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList141 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList141 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save141] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save141]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList141 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save141] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List141]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList141 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List141] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List142]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList142 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List142] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List142]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList142 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List142] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save142]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList142 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList142 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList142 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save142] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save142]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList142 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save142] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List142]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList142 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List142] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List143]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList143 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List143] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List143]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList143 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List143] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save143]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList143 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList143 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList143 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save143] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save143]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList143 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save143] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List143]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList143 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List143] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List144]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList144 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List144] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List144]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList144 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List144] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save144]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList144 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList144 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList144 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save144] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save144]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList144 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save144] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List144]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList144 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List144] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List145]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList145 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List145] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List145]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList145 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List145] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save145]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList145 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList145 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList145 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save145] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save145]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList145 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save145] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List145]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList145 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List145] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List146]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList146 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List146] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List146]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList146 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List146] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save146]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList146 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList146 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList146 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save146] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save146]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList146 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save146] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List146]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList146 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List146] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List147]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList147 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List147] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List147]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList147 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List147] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save147]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList147 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList147 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList147 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save147] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save147]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList147 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save147] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List147]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList147 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List147] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List148]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList148 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List148] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List148]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList148 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List148] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save148]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList148 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList148 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList148 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save148] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save148]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList148 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save148] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List148]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList148 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List148] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List149]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList149 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List149] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List149]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList149 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List149] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save149]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList149 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList149 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList149 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save149] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save149]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList149 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save149] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List149]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList149 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List149] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List150]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList150 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List150] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List150]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList150 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List150] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save150]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList150 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList150 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList150 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save150] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save150]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList150 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save150] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List150]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList150 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List150] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List151]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList151 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List151] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List151]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList151 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List151] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save151]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList151 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList151 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList151 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save151] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save151]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList151 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save151] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List151]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList151 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List151] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List152]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList152 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List152] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List152]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList152 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List152] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save152]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList152 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList152 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList152 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save152] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save152]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList152 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save152] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List152]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList152 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List152] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List153]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList153 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List153] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List153]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList153 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List153] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save153]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList153 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList153 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList153 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save153] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save153]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList153 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save153] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List153]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList153 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List153] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List154]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList154 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List154] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List154]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList154 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List154] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save154]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList154 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList154 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList154 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save154] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save154]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList154 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save154] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List154]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList154 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List154] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List155]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList155 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List155] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List155]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList155 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List155] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save155]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList155 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList155 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList155 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save155] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save155]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList155 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save155] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List155]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList155 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List155] to public
go

  SET ANSI_NULLS ON
 GO
 SET QUOTED_IDENTIFIER ON
 GO
CREATE procedure [dbo].[up_D24YNMList_List156]
(
	@MemberID int,
	@ListTypeID tinyint = 1
)
as 

set nocount on 

select 	MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate,ListTypeID
from	D24YNMList156 with (nolock)
where 	MemberID = @MemberID 
  and	ListTypeID = case @ListTypeID
						when 0 then ListTypeID
						else @ListTypeID
					 end

return

GO
grant execute on [dbo].[up_D24YNMList_List156] to public
go
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_MutualMail_List156]
(
	@MemberID int,
	@TargetMemberID int
)
as

set nocount on 

declare @IsMutual int

set @IsMutual = 0

select @IsMutual = count(*) from D24YNMList156 with (nolock)
where ListTypeID = 1
  and	((MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 0 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0))
	 	or (MemberID = @MemberID and TargetMemberID = @TargetMemberID and DirectionFlag = 1 and YNMTypeID = 1 and (MailMask = 2 or MailMask = 3 or MailMask = 0)))

if @IsMutual > 1 begin
	set @IsMutual = 1
end
else
begin
	set @IsMutual = 0
end	

select @IsMutual

return
GO
grant execute on [dbo].[up_D24YNMList_MutualMail_List156] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[up_D24YNMList_Save156]
(
	@MemberID 	int,
	@TargetMemberID int,
	@PrivateLabelID	int,
	@DirectionFlag 	tinyint,
	@YNMTypeID 	tinyint,
	@UpdateDate 	smalldatetime,
	@ListTypeID  tinyint = 1
)
as

set nocount on 

if exists (
	select 	MemberID
	from 	D24YNMList156 (nolock)
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and	ListTypeID = @ListTypeID)
begin 
	update	D24YNMList156 set
		PrivateLabelID = @PrivateLabelID,
		YNMTypeID = @YNMTypeID,
		UpdateDate = @UpdateDate
	where 	MemberID = @MemberID 
		and TargetMemberID = @TargetMemberID 
		and DirectionFlag = @DirectionFlag
		and ListTypeID = @ListTypeID
end else begin
	insert into D24YNMList156 (MemberID, TargetMemberID, PrivateLabelID, DirectionFlag, YNMTypeID, UpdateDate, MailMask,ListTypeID)
	values (@MemberID, @TargetMemberID, @PrivateLabelID, @DirectionFlag, @YNMTypeID, @UpdateDate, 0,@ListTypeID)
end

return
GO
grant execute on [dbo].[up_D24YNMList_Save156] to public
go 
    
    SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[up_D24YNMList_Status_Save156]
(
	@MemberID int,
	@TargetMemberID int,
	@MailMask int,
	@ListTypeID tinyint = 1
)
as

set nocount on 

update		D24YNMList156 set
	MailMask = MailMask | @MailMask
where	MemberID = @MemberID 
	and TargetMemberID = @TargetMemberID
	and	ListTypeID = @ListTypeID

return
go
grant execute on  [dbo].[up_D24YNMList_Status_Save156] to public
go 
    
    SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[up_D24YNMList_ViralMail_List156]
(
	@MemberID int,
	@MaxCount int = 5,
	@ListTypeID tinyint = 1
)
as

set nocount on 

set rowcount @MaxCount

select	TargetMemberID, UpdateDate
from 	D24YNMList156 with (nolock)
where	MemberID = @MemberID 
	and ListTypeID = @ListTypeID
	and DirectionFlag = 0 
	and YNMTypeID = 1
	and MailMask & 2 = 0

return
GO
grant execute on  [dbo].[up_D24YNMList_ViralMail_List156] to public
go
