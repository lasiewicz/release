--AttributeOptions  
  declare @attributeid int
   declare @attributeoptionid int
   declare @AttributeOptionGroupID int
   
  
    --attributeoption  'timeliness'
    
    select @attributeid=attributeid from attribute
    where attributename= 'timeliness'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_630_BLANK'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'25','10')
    
  
    --attributeoption  'timeliness'
    
    select @attributeid=attributeid from attribute
    where attributename= 'timeliness'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_630_USUALLY_EARLY'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'25','15')
    
  
    --attributeoption  'timeliness'
    
    select @attributeid=attributeid from attribute
    where attributename= 'timeliness'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_630_LITTLE_LATE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'25','20')
    
  
    --attributeoption  'timeliness'
    
    select @attributeid=attributeid from attribute
    where attributename= 'timeliness'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_630_ONTIME'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'25','30')
    
  
    --attributeoption  'timeliness'
    
    select @attributeid=attributeid from attribute
    where attributename= 'timeliness'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_630_FORGET'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'25','40')
    
  
    --attributeoption  'timeliness'
    
    select @attributeid=attributeid from attribute
    where attributename= 'timeliness'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_630_I_AM_STAR'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'25','50')
    
  
    --attributeoption  'fashion'
    
    select @attributeid=attributeid from attribute
    where attributename= 'fashion'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_631_BLANK'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'25','10')
    
  
    --attributeoption  'fashion'
    
    select @attributeid=attributeid from attribute
    where attributename= 'fashion'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_631_DONT_NEED_ADVICE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'25','20')
    
  
    --attributeoption  'fashion'
    
    select @attributeid=attributeid from attribute
    where attributename= 'fashion'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_631_DONT_CARE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'25','30')
    
  
    --attributeoption  'fashion'
    
    select @attributeid=attributeid from attribute
    where attributename= 'fashion'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_631_DRESS_COMFY'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'25','40')
    
  
    --attributeoption  'fashion'
    
    select @attributeid=attributeid from attribute
    where attributename= 'fashion'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_631_SOMEWHAT_FASHIONABLE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'25','50')
    
  
    --attributeoption  'fashion'
    
    select @attributeid=attributeid from attribute
    where attributename= 'fashion'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_631_VERY_TRENDY'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'25','60')
    
  
    --attributeoption  'intelligenceimportance'
    
    select @attributeid=attributeid from attribute
    where attributename= 'intelligenceimportance'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_BLANK_74307'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','1')
    
  
    --attributeoption  'intelligenceimportance'
    
    select @attributeid=attributeid from attribute
    where attributename= 'intelligenceimportance'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_99_NOT_IMPORTANT'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','2')
    
  
    --attributeoption  'intelligenceimportance'
    
    select @attributeid=attributeid from attribute
    where attributename= 'intelligenceimportance'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_99_LOW_IMPORTANCE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','3')
    
  
    --attributeoption  'intelligenceimportance'
    
    select @attributeid=attributeid from attribute
    where attributename= 'intelligenceimportance'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_99_MEDIUM_IMPORTANCE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','4')
    
  
    --attributeoption  'intelligenceimportance'
    
    select @attributeid=attributeid from attribute
    where attributename= 'intelligenceimportance'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_99_MOST_IMPORTANT'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','5')
    
  
    --attributeoption  'hivstatustype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'hivstatustype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_440_UNSPECIFIED'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','10')
    
  
    --attributeoption  'hivstatustype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'hivstatustype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_440_NEGATIVE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','20')
    
  
    --attributeoption  'hivstatustype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'hivstatustype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_440_POSITIVE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','30')
    
  
    --attributeoption  'travelmask'
    
    select @attributeid=attributeid from attribute
    where attributename= 'travelmask'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_396_SOUTH_AMERICA'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','10')
    
  
    --attributeoption  'travelmask'
    
    select @attributeid=attributeid from attribute
    where attributename= 'travelmask'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_396_FAR_EAST'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','20')
    
  
    --attributeoption  'travelmask'
    
    select @attributeid=attributeid from attribute
    where attributename= 'travelmask'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_396_AUSTRALIA'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','30')
    
  
    --attributeoption  'travelmask'
    
    select @attributeid=attributeid from attribute
    where attributename= 'travelmask'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_396_EUROPE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','40')
    
  
    --attributeoption  'travelmask'
    
    select @attributeid=attributeid from attribute
    where attributename= 'travelmask'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_396_UNITED_STATES'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','50')
    
  
    --attributeoption  'travelmask'
    
    select @attributeid=attributeid from attribute
    where attributename= 'travelmask'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_396_OTHER'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','1000')
    
  
    --attributeoption  'entertainmentlocation'
    
    select @attributeid=attributeid from attribute
    where attributename= 'entertainmentlocation'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_117_SHOPPING'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','0')
    
  
    --attributeoption  'entertainmentlocation'
    
    select @attributeid=attributeid from attribute
    where attributename= 'entertainmentlocation'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_117_AMUSEMENT_PARKS'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','10')
    
  
    --attributeoption  'entertainmentlocation'
    
    select @attributeid=attributeid from attribute
    where attributename= 'entertainmentlocation'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_117_ANTIQUE_STORESFLEA_MARKETSGARAGE_SALES'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','20')
    
  
    --attributeoption  'entertainmentlocation'
    
    select @attributeid=attributeid from attribute
    where attributename= 'entertainmentlocation'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_117_ART_GALLERIES'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','30')
    
  
    --attributeoption  'entertainmentlocation'
    
    select @attributeid=attributeid from attribute
    where attributename= 'entertainmentlocation'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_117_BARSNIGHTCLUBS'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','40')
    
  
    --attributeoption  'entertainmentlocation'
    
    select @attributeid=attributeid from attribute
    where attributename= 'entertainmentlocation'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_117_BEACH'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','50')
    
  
    --attributeoption  'entertainmentlocation'
    
    select @attributeid=attributeid from attribute
    where attributename= 'entertainmentlocation'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_117_BOOKSTORES'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','60')
    
  
    --attributeoption  'entertainmentlocation'
    
    select @attributeid=attributeid from attribute
    where attributename= 'entertainmentlocation'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_117_CHARITY_EVENTS'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','70')
    
  
    --attributeoption  'entertainmentlocation'
    
    select @attributeid=attributeid from attribute
    where attributename= 'entertainmentlocation'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_117_COFFEE_HOUSES'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','90')
    
  
    --attributeoption  'entertainmentlocation'
    
    select @attributeid=attributeid from attribute
    where attributename= 'entertainmentlocation'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_117_COMEDY_CLUBS'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','100')
    
  
    --attributeoption  'entertainmentlocation'
    
    select @attributeid=attributeid from attribute
    where attributename= 'entertainmentlocation'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_117_CONCERTS'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','110')
    
  
    --attributeoption  'entertainmentlocation'
    
    select @attributeid=attributeid from attribute
    where attributename= 'entertainmentlocation'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_117_DANCES__LINE_BALLROOM_TANGO'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','120')
    
  
    --attributeoption  'entertainmentlocation'
    
    select @attributeid=attributeid from attribute
    where attributename= 'entertainmentlocation'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_117_KARAOKESINGALONG'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','130')
    
  
    --attributeoption  'entertainmentlocation'
    
    select @attributeid=attributeid from attribute
    where attributename= 'entertainmentlocation'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_117_LIBRARIES'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','140')
    
  
    --attributeoption  'entertainmentlocation'
    
    select @attributeid=attributeid from attribute
    where attributename= 'entertainmentlocation'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_117_LIVE_THEATER'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','150')
    
  
    --attributeoption  'entertainmentlocation'
    
    select @attributeid=attributeid from attribute
    where attributename= 'entertainmentlocation'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_117_MOVIES'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','160')
    
  
    --attributeoption  'entertainmentlocation'
    
    select @attributeid=attributeid from attribute
    where attributename= 'entertainmentlocation'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_117_MUSEUMS'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','170')
    
  
    --attributeoption  'entertainmentlocation'
    
    select @attributeid=attributeid from attribute
    where attributename= 'entertainmentlocation'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_117_OPERA'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','180')
    
  
    --attributeoption  'entertainmentlocation'
    
    select @attributeid=attributeid from attribute
    where attributename= 'entertainmentlocation'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_117_PARKS'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','190')
    
  
    --attributeoption  'entertainmentlocation'
    
    select @attributeid=attributeid from attribute
    where attributename= 'entertainmentlocation'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_117_POLITICAL_EVENTS'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','200')
    
  
    --attributeoption  'entertainmentlocation'
    
    select @attributeid=attributeid from attribute
    where attributename= 'entertainmentlocation'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_117_RAVESUNDERGROUND_PARTIES'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','210')
    
  
    --attributeoption  'entertainmentlocation'
    
    select @attributeid=attributeid from attribute
    where attributename= 'entertainmentlocation'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_117_RESTAURANTS'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','220')
    
  
    --attributeoption  'entertainmentlocation'
    
    select @attributeid=attributeid from attribute
    where attributename= 'entertainmentlocation'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_117_SHOPPING_MALLS'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','230')
    
  
    --attributeoption  'entertainmentlocation'
    
    select @attributeid=attributeid from attribute
    where attributename= 'entertainmentlocation'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_117_SKATEBIKE_PARKS'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','240')
    
  
    --attributeoption  'entertainmentlocation'
    
    select @attributeid=attributeid from attribute
    where attributename= 'entertainmentlocation'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_117_SPORTING_EVENTS'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','250')
    
  
    --attributeoption  'entertainmentlocation'
    
    select @attributeid=attributeid from attribute
    where attributename= 'entertainmentlocation'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_117_SYMPHONY'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','260')
    
  
    --attributeoption  'entertainmentlocation'
    
    select @attributeid=attributeid from attribute
    where attributename= 'entertainmentlocation'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_117_VOLUNTEER_EVENTS'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','270')
    
  
    --attributeoption  'favoritetimeofday'
    
    select @attributeid=attributeid from attribute
    where attributename= 'favoritetimeofday'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_BLANK_74312'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','10')
    
  
    --attributeoption  'favoritetimeofday'
    
    select @attributeid=attributeid from attribute
    where attributename= 'favoritetimeofday'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_100_MORNING_PERSON'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','20')
    
  
    --attributeoption  'favoritetimeofday'
    
    select @attributeid=attributeid from attribute
    where attributename= 'favoritetimeofday'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_100_NIGHT_PERSON'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','30')
    
  
    --attributeoption  'favoritetimeofday'
    
    select @attributeid=attributeid from attribute
    where attributename= 'favoritetimeofday'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_100_UP_ALL_THE_TIME'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','40')
    
  
    --attributeoption  'synagogueattendance'
    
    select @attributeid=attributeid from attribute
    where attributename= 'synagogueattendance'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_BLANK_78810'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','1')
    
  
    --attributeoption  'synagogueattendance'
    
    select @attributeid=attributeid from attribute
    where attributename= 'synagogueattendance'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_366_EVERY_SHABBAT'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','2')
    
  
    --attributeoption  'synagogueattendance'
    
    select @attributeid=attributeid from attribute
    where attributename= 'synagogueattendance'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_366_ON_HIGH_HOLIDAYS'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','3')
    
  
    --attributeoption  'synagogueattendance'
    
    select @attributeid=attributeid from attribute
    where attributename= 'synagogueattendance'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_366_NEVER'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','4')
    
  
    --attributeoption  'synagogueattendance'
    
    select @attributeid=attributeid from attribute
    where attributename= 'synagogueattendance'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_366_SOMETIMES'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','5')
    
  
    --attributeoption  'synagogueattendance'
    
    select @attributeid=attributeid from attribute
    where attributename= 'synagogueattendance'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_366_ON_SOME_SHABBAT'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','6')
    
  
    --attributeoption  'synagogueattendance'
    
    select @attributeid=attributeid from attribute
    where attributename= 'synagogueattendance'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_366_WILL_TELL_YOU_LATER'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','7')
    
  
    --attributeoption  'israelareacode'
    
    select @attributeid=attributeid from attribute
    where attributename= 'israelareacode'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_BLANK_4628'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','0')
    
  
    --attributeoption  'israelareacode'
    
    select @attributeid=attributeid from attribute
    where attributename= 'israelareacode'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_174_02'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','5')
    
  
    --attributeoption  'israelareacode'
    
    select @attributeid=attributeid from attribute
    where attributename= 'israelareacode'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_174_03'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','6')
    
  
    --attributeoption  'incomelevel'
    
    select @attributeid=attributeid from attribute
    where attributename= 'incomelevel'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_BLANK_79054'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','1')
    
  
    --attributeoption  'incomelevel'
    
    select @attributeid=attributeid from attribute
    where attributename= 'incomelevel'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_147_WILL_TELL_YOU_LATER'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','2')
    
  
    --attributeoption  'incomelevel'
    
    select @attributeid=attributeid from attribute
    where attributename= 'incomelevel'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_147_UNDER_15000'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','3')
    
  
    --attributeoption  'incomelevel'
    
    select @attributeid=attributeid from attribute
    where attributename= 'incomelevel'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_147_15000__25000'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','4')
    
  
    --attributeoption  'incomelevel'
    
    select @attributeid=attributeid from attribute
    where attributename= 'incomelevel'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_147_25000__35000'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','5')
    
  
    --attributeoption  'incomelevel'
    
    select @attributeid=attributeid from attribute
    where attributename= 'incomelevel'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_147_35000__50000'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','6')
    
  
    --attributeoption  'incomelevel'
    
    select @attributeid=attributeid from attribute
    where attributename= 'incomelevel'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_147_50000__100000'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','7')
    
  
    --attributeoption  'incomelevel'
    
    select @attributeid=attributeid from attribute
    where attributename= 'incomelevel'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_147_OVER_100000'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','8')
    
  
    --attributeoption  'incomelevel'
    
    select @attributeid=attributeid from attribute
    where attributename= 'incomelevel'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_147_AVERAGE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','11')
    
  
    --attributeoption  'keepkosher'
    
    select @attributeid=attributeid from attribute
    where attributename= 'keepkosher'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_BLANK_78824'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','1')
    
  
    --attributeoption  'keepkosher'
    
    select @attributeid=attributeid from attribute
    where attributename= 'keepkosher'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_368_AT_HOME_ONLY'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','2')
    
  
    --attributeoption  'keepkosher'
    
    select @attributeid=attributeid from attribute
    where attributename= 'keepkosher'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_368_AT_HOME_AND_OUTSIDE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','3')
    
  
    --attributeoption  'keepkosher'
    
    select @attributeid=attributeid from attribute
    where attributename= 'keepkosher'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_368_NOT_AT_ALL'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','4')
    
  
    --attributeoption  'keepkosher'
    
    select @attributeid=attributeid from attribute
    where attributename= 'keepkosher'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_368_TO_SOME_DEGREE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','5')
    
  
    --attributeoption  'pets'
    
    select @attributeid=attributeid from attribute
    where attributename= 'pets'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_240_BIRD'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','2')
    
  
    --attributeoption  'pets'
    
    select @attributeid=attributeid from attribute
    where attributename= 'pets'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_240_CAT'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','3')
    
  
    --attributeoption  'pets'
    
    select @attributeid=attributeid from attribute
    where attributename= 'pets'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_240_DOG'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','4')
    
  
    --attributeoption  'pets'
    
    select @attributeid=attributeid from attribute
    where attributename= 'pets'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_240_FISH'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','5')
    
  
    --attributeoption  'pets'
    
    select @attributeid=attributeid from attribute
    where attributename= 'pets'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_240_REPTILE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','30')
    
  
    --attributeoption  'pets'
    
    select @attributeid=attributeid from attribute
    where attributename= 'pets'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_240_HAMSTER'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','50')
    
  
    --attributeoption  'pets'
    
    select @attributeid=attributeid from attribute
    where attributename= 'pets'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_240_RABBIT'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','60')
    
  
    --attributeoption  'pets'
    
    select @attributeid=attributeid from attribute
    where attributename= 'pets'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_240_OTHER_PETS'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','100')
    
  
    --attributeoption  'pets'
    
    select @attributeid=attributeid from attribute
    where attributename= 'pets'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_240_I_AM_NOT_A_PET_PERSON'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','110')
    
  
    --attributeoption  'mailboxpreference'
    
    select @attributeid=attributeid from attribute
    where attributename= 'mailboxpreference'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_354_INCLUDE_ORIGINAL_MESSAGES_IN_REPLIES'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','1')
    
  
    --attributeoption  'mailboxpreference'
    
    select @attributeid=attributeid from attribute
    where attributename= 'mailboxpreference'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_354_SAVE_COPIES_OF_SENT_MESSAGES'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','2')
    
  
    --attributeoption  'majortype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'majortype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_432_AMERICAN_STUDIES'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','0')
    
  
    --attributeoption  'majortype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'majortype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_BLANK_109688'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','1')
    
  
    --attributeoption  'majortype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'majortype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_432_ANCIENT_NEAR_EASTERN_CIVILIZATIONS'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','2')
    
  
    --attributeoption  'majortype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'majortype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_432_ANTHROPOLOGY'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','3')
    
  
    --attributeoption  'majortype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'majortype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_432_ARABIC'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','13')
    
  
    --attributeoption  'majortype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'majortype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_432_CHINESE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','32')
    
  
    --attributeoption  'majortype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'majortype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_432_EDUCATION'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','54')
    
  
    --attributeoption  'majortype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'majortype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_432_ENGLISH'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','62')
    
  
    --attributeoption  'majortype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'majortype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_432_FRENCH'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','75')
    
  
    --attributeoption  'majortype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'majortype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_432_GERMAN'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','80')
    
  
    --attributeoption  'majortype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'majortype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_432_GREEK'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','81')
    
  
    --attributeoption  'majortype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'majortype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_432_HEBREW'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','83')
    
  
    --attributeoption  'majortype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'majortype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_432_ITALIAN'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','92')
    
  
    --attributeoption  'majortype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'majortype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_432_JAPANESE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','94')
    
  
    --attributeoption  'majortype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'majortype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_432_KOREAN'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','96')
    
  
    --attributeoption  'majortype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'majortype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_432_LANGUAGES'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','97')
    
  
    --attributeoption  'majortype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'majortype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_432_LATIN'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','98')
    
  
    --attributeoption  'majortype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'majortype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_432_OTHER'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','134')
    
  
    --attributeoption  'majortype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'majortype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_432_PORTUGUESE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','142')
    
  
    --attributeoption  'majortype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'majortype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_432_SPANISH'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','161')
    
  
    --attributeoption  'majortype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'majortype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_432_THEATER'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','168')
    
  
    --attributeoption  'appearanceimportance'
    
    select @attributeid=attributeid from attribute
    where attributename= 'appearanceimportance'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_98_NOT_IMPORTANT'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','2')
    
  
    --attributeoption  'appearanceimportance'
    
    select @attributeid=attributeid from attribute
    where attributename= 'appearanceimportance'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_98_LOW_IMPORTANCE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','3')
    
  
    --attributeoption  'appearanceimportance'
    
    select @attributeid=attributeid from attribute
    where attributename= 'appearanceimportance'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_98_IMPORTANT'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','4')
    
  
    --attributeoption  'appearanceimportance'
    
    select @attributeid=attributeid from attribute
    where attributename= 'appearanceimportance'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_98_MOST_IMPORTANT'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','6')
    
  
    --attributeoption  'relationshipstatus'
    
    select @attributeid=attributeid from attribute
    where attributename= 'relationshipstatus'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_BLANK_105428'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','10')
    
  
    --attributeoption  'relationshipstatus'
    
    select @attributeid=attributeid from attribute
    where attributename= 'relationshipstatus'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_414_SINGLE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','20')
    
  
    --attributeoption  'relationshipstatus'
    
    select @attributeid=attributeid from attribute
    where attributename= 'relationshipstatus'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_414_ATTACHED_BUT_LOOKING'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','30')
    
  
    --attributeoption  'relationshipstatus'
    
    select @attributeid=attributeid from attribute
    where attributename= 'relationshipstatus'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_414_OPEN_RELATIONSHIP'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','40')
    
  
    --attributeoption  'relationshipstatus'
    
    select @attributeid=attributeid from attribute
    where attributename= 'relationshipstatus'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_414_COMMITTEDMONOGAMOUS'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','50')
    
  
    --attributeoption  'relationshipstatus'
    
    select @attributeid=attributeid from attribute
    where attributename= 'relationshipstatus'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_414_DIVORCED'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','60')
    
  
    --attributeoption  'relationshipstatus'
    
    select @attributeid=attributeid from attribute
    where attributename= 'relationshipstatus'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_414_ASK_ME'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','70')
    
  
    --attributeoption  'newone4'
    
    select @attributeid=attributeid from attribute
    where attributename= 'newone4'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_178_1234'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','1')
    
  
    --attributeoption  'newone4'
    
    select @attributeid=attributeid from attribute
    where attributename= 'newone4'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_178_234'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','2')
    
  
    --attributeoption  'moviegenremask'
    
    select @attributeid=attributeid from attribute
    where attributename= 'moviegenremask'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_418_ROMANCE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','1')
    
  
    --attributeoption  'moviegenremask'
    
    select @attributeid=attributeid from attribute
    where attributename= 'moviegenremask'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_418_ACTIONADVENTURE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','20')
    
  
    --attributeoption  'moviegenremask'
    
    select @attributeid=attributeid from attribute
    where attributename= 'moviegenremask'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_418_DONT_LIKE_MOVIES'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','21')
    
  
    --attributeoption  'moviegenremask'
    
    select @attributeid=attributeid from attribute
    where attributename= 'moviegenremask'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_418_DOCUMENTARY'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','22')
    
  
    --attributeoption  'moviegenremask'
    
    select @attributeid=attributeid from attribute
    where attributename= 'moviegenremask'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_418_SUSPENSE_THRILLER'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','23')
    
  
    --attributeoption  'moviegenremask'
    
    select @attributeid=attributeid from attribute
    where attributename= 'moviegenremask'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_418_MYSTERY'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','24')
    
  
    --attributeoption  'moviegenremask'
    
    select @attributeid=attributeid from attribute
    where attributename= 'moviegenremask'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_418_WESTERN'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','25')
    
  
    --attributeoption  'moviegenremask'
    
    select @attributeid=attributeid from attribute
    where attributename= 'moviegenremask'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_418_CARTOONS'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','26')
    
  
    --attributeoption  'moviegenremask'
    
    select @attributeid=attributeid from attribute
    where attributename= 'moviegenremask'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_418_COMEDY'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','30')
    
  
    --attributeoption  'moviegenremask'
    
    select @attributeid=attributeid from attribute
    where attributename= 'moviegenremask'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_418_DRAMA'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','40')
    
  
    --attributeoption  'moviegenremask'
    
    select @attributeid=attributeid from attribute
    where attributename= 'moviegenremask'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_418_FOREIGNINDEPENDENT'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','50')
    
  
    --attributeoption  'moviegenremask'
    
    select @attributeid=attributeid from attribute
    where attributename= 'moviegenremask'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_418_HORROR'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','60')
    
  
    --attributeoption  'moviegenremask'
    
    select @attributeid=attributeid from attribute
    where attributename= 'moviegenremask'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_418_SCIFI'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','70')
    
  
    --attributeoption  'moviegenremask'
    
    select @attributeid=attributeid from attribute
    where attributename= 'moviegenremask'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_418_MUSICAL'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','80')
    
  
    --attributeoption  'eyecolor'
    
    select @attributeid=attributeid from attribute
    where attributename= 'eyecolor'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_BLANK_74228'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','1')
    
  
    --attributeoption  'eyecolor'
    
    select @attributeid=attributeid from attribute
    where attributename= 'eyecolor'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_86_BLACKEBONY'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','2')
    
  
    --attributeoption  'eyecolor'
    
    select @attributeid=attributeid from attribute
    where attributename= 'eyecolor'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_86_BLUE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','3')
    
  
    --attributeoption  'eyecolor'
    
    select @attributeid=attributeid from attribute
    where attributename= 'eyecolor'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_86_BLUE_GRAY'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','4')
    
  
    --attributeoption  'eyecolor'
    
    select @attributeid=attributeid from attribute
    where attributename= 'eyecolor'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_86_BLUE_GREEN'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','5')
    
  
    --attributeoption  'eyecolor'
    
    select @attributeid=attributeid from attribute
    where attributename= 'eyecolor'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_86_BROWN'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','6')
    
  
    --attributeoption  'eyecolor'
    
    select @attributeid=attributeid from attribute
    where attributename= 'eyecolor'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_86_DARK_BROWN'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','7')
    
  
    --attributeoption  'eyecolor'
    
    select @attributeid=attributeid from attribute
    where attributename= 'eyecolor'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_86_GRAY'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','8')
    
  
    --attributeoption  'eyecolor'
    
    select @attributeid=attributeid from attribute
    where attributename= 'eyecolor'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_86_GREEN'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','9')
    
  
    --attributeoption  'eyecolor'
    
    select @attributeid=attributeid from attribute
    where attributename= 'eyecolor'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_86_GREEN_BROWN'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','10')
    
  
    --attributeoption  'eyecolor'
    
    select @attributeid=attributeid from attribute
    where attributename= 'eyecolor'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_86_GREEN_GRAY'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','11')
    
  
    --attributeoption  'eyecolor'
    
    select @attributeid=attributeid from attribute
    where attributename= 'eyecolor'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_86_HAZEL'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','12')
    
  
    --attributeoption  'eyecolor'
    
    select @attributeid=attributeid from attribute
    where attributename= 'eyecolor'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_86_HAZEL_BROWN'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','13')
    
  
    --attributeoption  'eyecolor'
    
    select @attributeid=attributeid from attribute
    where attributename= 'eyecolor'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_86_TURQUOISE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','14')
    
  
    --attributeoption  'jdateethnicity'
    
    select @attributeid=attributeid from attribute
    where attributename= 'jdateethnicity'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_362_ASHKENAZI'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','2')
    
  
    --attributeoption  'jdateethnicity'
    
    select @attributeid=attributeid from attribute
    where attributename= 'jdateethnicity'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_362_MIXED_ETHNIC'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','3')
    
  
    --attributeoption  'jdateethnicity'
    
    select @attributeid=attributeid from attribute
    where attributename= 'jdateethnicity'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_362_ANOTHER_ETHNIC'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','4')
    
  
    --attributeoption  'jdateethnicity'
    
    select @attributeid=attributeid from attribute
    where attributename= 'jdateethnicity'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_362_SEPHARDIC'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','5')
    
  
    --attributeoption  'jdateethnicity'
    
    select @attributeid=attributeid from attribute
    where attributename= 'jdateethnicity'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_362_WILL_TELL_YOU_LATER'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','6')
    
  
    --attributeoption  'transactiontype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'transactiontype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_264_INITIATED_SUBSCRIPTION'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','1')
    
  
    --attributeoption  'transactiontype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'transactiontype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_264_RENEWED_SUBSCRIPTION'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','2')
    
  
    --attributeoption  'transactiontype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'transactiontype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_264_TERMINATED_SUBSCRIPTION'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','3')
    
  
    --attributeoption  'transactiontype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'transactiontype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_264_REFUND_FOR_BOUNCED_EMAIL'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','4')
    
  
    --attributeoption  'transactiontype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'transactiontype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_264_ADMIN_MONTHLY_CHANGE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','5')
    
  
    --attributeoption  'transactiontype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'transactiontype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_264_ADMIN_UNITS_CHANGE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','6')
    
  
    --attributeoption  'transactiontype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'transactiontype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_264_COMPLIMENTARY'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','7')
    
  
    --attributeoption  'transactiontype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'transactiontype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_264_CHECK_RECEIVED_BY_MAIL'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','8')
    
  
    --attributeoption  'transactiontype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'transactiontype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_264_ADMIN_ADJUSTMENT'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','9')
    
  
    --attributeoption  'transactiontype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'transactiontype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_264_MEMBER_TERMINATED_SUBSCRIPTION__GOOD'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','10')
    
  
    --attributeoption  'transactiontype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'transactiontype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_264_MEMBER_TERMINATED_SUBSCRIPTION__BAD'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','11')
    
  
    --attributeoption  'industrytype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'industrytype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_392_CONSTRUCTION'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','0')
    
  
    --attributeoption  'industrytype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'industrytype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_392_ADMINISTRATIVEHUMAN_RESOURCES'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','2')
    
  
    --attributeoption  'industrytype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'industrytype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_392_ADVERTISINGMARKETINGPR'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','3')
    
  
    --attributeoption  'industrytype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'industrytype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_392_ARCHITECTUREINTERIOR_DESIGN'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','4')
    
  
    --attributeoption  'industrytype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'industrytype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_392_AUTOMOTIVEAVIATIONTRANSPORTATION'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','5')
    
  
    --attributeoption  'industrytype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'industrytype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_392_COMMUNICATIONTELECOM'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','6')
    
  
    --attributeoption  'industrytype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'industrytype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_392_CONSTRUCTIONAGRICULTURELANDSCAPING'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','7')
    
  
    --attributeoption  'industrytype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'industrytype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_392_DESIGNVISUAL__GRAPHIC_ARTS'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','8')
    
  
    --attributeoption  'industrytype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'industrytype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_392_EDUCATIONTEACHINGCHILD_CARE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','9')
    
  
    --attributeoption  'industrytype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'industrytype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_392_ENTERTAINMENTMEDIADRAMATIC_ARTS'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','10')
    
  
    --attributeoption  'industrytype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'industrytype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_392_ENTREPRENEURIALSTARTUP'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','11')
    
  
    --attributeoption  'industrytype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'industrytype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_392_EXECUTIVEGENERAL_MGTCONSULTING'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','12')
    
  
    --attributeoption  'industrytype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'industrytype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_392_FASHIONSTYLEMODELINGAPPARELBEAUTY'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','13')
    
  
    --attributeoption  'industrytype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'industrytype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_392_FINANCIALACCOUNTINGINSURANCEREAL_ESTATE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','14')
    
  
    --attributeoption  'industrytype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'industrytype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_392_GOVERNMENTCIVIL_SERVICEPUBLIC_POLICY'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','15')
    
  
    --attributeoption  'industrytype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'industrytype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_392_HOMEMAKINGCHILD_REARING'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','16')
    
  
    --attributeoption  'industrytype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'industrytype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_392_INTERNETECOMMERCETECHNOLOGY'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','17')
    
  
    --attributeoption  'industrytype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'industrytype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_392_LAW_ENFORCEMENTMILITARYSECURITY'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','18')
    
  
    --attributeoption  'industrytype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'industrytype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_392_LAWLEGALJUDICIARY'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','19')
    
  
    --attributeoption  'industrytype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'industrytype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_392_MANUFACTURINGWAREHOUSINGSHIPPINGFACILITIES'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','20')
    
  
    --attributeoption  'industrytype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'industrytype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_392_MEDICALHEALTHFITNESSSOCIAL_SERVICES'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','21')
    
  
    --attributeoption  'industrytype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'industrytype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_392_NONPROFITVOLUNTEERACTIVIST'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','22')
    
  
    --attributeoption  'industrytype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'industrytype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_392_OTHER_PROFESSIONALSERVICESTRADE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','23')
    
  
    --attributeoption  'industrytype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'industrytype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_392_PUBLIC_SAFETYFIREPARAMEDIC'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','24')
    
  
    --attributeoption  'industrytype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'industrytype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_392_PUBLISHINGPRINT_MEDIA'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','25')
    
  
    --attributeoption  'industrytype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'industrytype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_392_RESTAURANTFOOD_SERVICESCATERING'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','26')
    
  
    --attributeoption  'industrytype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'industrytype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_392_RETIRED'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','27')
    
  
    --attributeoption  'industrytype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'industrytype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_392_SALES_REPRESENTATIVERETAILWHOLESALE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','28')
    
  
    --attributeoption  'industrytype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'industrytype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_392_STUDENT'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','29')
    
  
    --attributeoption  'industrytype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'industrytype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_392_TECHNICALSCIENCEENGINEERING'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','30')
    
  
    --attributeoption  'industrytype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'industrytype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_392_TRAVELRECREATIONLEISUREHOSPITALITY'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','31')
    
  
    --attributeoption  'industrytype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'industrytype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_392_OTHER'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','32')
    
  
    --attributeoption  'emailtransactiontype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'emailtransactiontype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_306_SENT'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','1')
    
  
    --attributeoption  'emailtransactiontype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'emailtransactiontype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_306_RECEIVED'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','2')
    
  
    --attributeoption  'creditcardtypes'
    
    select @attributeid=attributeid from attribute
    where attributename= 'creditcardtypes'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_74_CHOOSE_ONE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','0')
    
  
    --attributeoption  'creditcardtypes'
    
    select @attributeid=attributeid from attribute
    where attributename= 'creditcardtypes'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_74_AMERICAN_EXPRESS_CREDIT_CARD_TYPE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','5')
    
  
    --attributeoption  'creditcardtypes'
    
    select @attributeid=attributeid from attribute
    where attributename= 'creditcardtypes'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_74_CARTE_BLANCHE_CREDIT_CARD_TYPE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','10')
    
  
    --attributeoption  'creditcardtypes'
    
    select @attributeid=attributeid from attribute
    where attributename= 'creditcardtypes'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_74_DINERS_CREDIT_CARD_TYPE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','15')
    
  
    --attributeoption  'creditcardtypes'
    
    select @attributeid=attributeid from attribute
    where attributename= 'creditcardtypes'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_74_DISCOVER_CREDIT_CARD_TYPE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','20')
    
  
    --attributeoption  'creditcardtypes'
    
    select @attributeid=attributeid from attribute
    where attributename= 'creditcardtypes'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_74_JCB_CREDIT_CARD_TYPE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','25')
    
  
    --attributeoption  'creditcardtypes'
    
    select @attributeid=attributeid from attribute
    where attributename= 'creditcardtypes'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_74_MASTERCARD_CREDIT_CARD_TYPE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','30')
    
  
    --attributeoption  'creditcardtypes'
    
    select @attributeid=attributeid from attribute
    where attributename= 'creditcardtypes'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_74_VISA_CARD_CREDIT_CARD_TYPE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','45')
    
  
    --attributeoption  'subscriptionterminationreasontype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'subscriptionterminationreasontype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_274_ADMIN_SUSPENDED'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','1')
    
  
    --attributeoption  'subscriptionterminationreasontype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'subscriptionterminationreasontype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_274_MEMBER_SUSPENDED'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','2')
    
  
    --attributeoption  'subscriptionterminationreasontype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'subscriptionterminationreasontype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_274_CHECK_PERIOD_FINISHED'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','3')
    
  
    --attributeoption  'subscriptionterminationreasontype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'subscriptionterminationreasontype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_274_UNABLE_TO_RENEW'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','4')
    
  
    --attributeoption  'sexualidentitytype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'sexualidentitytype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_BLANK_109686'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','0')
    
  
    --attributeoption  'sexualidentitytype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'sexualidentitytype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_328_STRAIGHT'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','1')
    
  
    --attributeoption  'sexualidentitytype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'sexualidentitytype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_328_BISEXUAL'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','2')
    
  
    --attributeoption  'sexualidentitytype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'sexualidentitytype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_328_CURIOUSQUESTIONING'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','3')
    
  
    --attributeoption  'sexualidentitytype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'sexualidentitytype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_328_GAY'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','4')
    
  
    --attributeoption  'sexualidentitytype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'sexualidentitytype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_328_LESBIAN'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','5')
    
  
    --attributeoption  'habitattype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'habitattype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_BLANK_78408'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','1')
    
  
    --attributeoption  'habitattype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'habitattype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_314_BIG_CITY'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','2')
    
  
    --attributeoption  'habitattype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'habitattype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_314_SUBURBIA'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','3')
    
  
    --attributeoption  'habitattype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'habitattype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_314_SMALL_TOWN'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','4')
    
  
    --attributeoption  'habitattype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'habitattype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_314_MIDDLE_OF_NOWHERE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','5')
    
  
    --attributeoption  'militaryservicetype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'militaryservicetype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_402_COMBAT_UNIT'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','1')
    
  
    --attributeoption  'militaryservicetype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'militaryservicetype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_402_BASE_NEAR_HOME'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','2')
    
  
    --attributeoption  'militaryservicetype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'militaryservicetype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_402_NATIONAL_SERVICE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','3')
    
  
    --attributeoption  'militaryservicetype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'militaryservicetype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_402_DID_NOT_SERVE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','4')
    
  
    --attributeoption  'politicalorientation'
    
    select @attributeid=attributeid from attribute
    where attributename= 'politicalorientation'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_BLANK_74321'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','1')
    
  
    --attributeoption  'politicalorientation'
    
    select @attributeid=attributeid from attribute
    where attributename= 'politicalorientation'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_102_UNSPECIFIED'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','2')
    
  
    --attributeoption  'politicalorientation'
    
    select @attributeid=attributeid from attribute
    where attributename= 'politicalorientation'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_102_EXTREME_LIBERAL'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','3')
    
  
    --attributeoption  'politicalorientation'
    
    select @attributeid=attributeid from attribute
    where attributename= 'politicalorientation'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_102_LEFT_WING'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','4')
    
  
    --attributeoption  'politicalorientation'
    
    select @attributeid=attributeid from attribute
    where attributename= 'politicalorientation'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_102_LIBERAL'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','5')
    
  
    --attributeoption  'politicalorientation'
    
    select @attributeid=attributeid from attribute
    where attributename= 'politicalorientation'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_102_LEFT_WING_MODERATE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','6')
    
  
    --attributeoption  'politicalorientation'
    
    select @attributeid=attributeid from attribute
    where attributename= 'politicalorientation'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_102_MODERATE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','7')
    
  
    --attributeoption  'politicalorientation'
    
    select @attributeid=attributeid from attribute
    where attributename= 'politicalorientation'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_102_MIDWAY_MODERATE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','8')
    
  
    --attributeoption  'politicalorientation'
    
    select @attributeid=attributeid from attribute
    where attributename= 'politicalorientation'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_102_CONSERVATIVE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','9')
    
  
    --attributeoption  'politicalorientation'
    
    select @attributeid=attributeid from attribute
    where attributename= 'politicalorientation'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_102_RIGHT_WING_MODERATE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','10')
    
  
    --attributeoption  'politicalorientation'
    
    select @attributeid=attributeid from attribute
    where attributename= 'politicalorientation'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_102_EXTREME_CONSERVATIVE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','11')
    
  
    --attributeoption  'politicalorientation'
    
    select @attributeid=attributeid from attribute
    where attributename= 'politicalorientation'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_102_RIGHT_WING'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','12')
    
  
    --attributeoption  'selfsuspendedreasontype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'selfsuspendedreasontype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_266_FOUND_MY_SOULMATE_ON_THE_SITE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','1')
    
  
    --attributeoption  'selfsuspendedreasontype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'selfsuspendedreasontype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_266_FOUND_MY_SOULMATE_ON_MY_OWN'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','2')
    
  
    --attributeoption  'selfsuspendedreasontype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'selfsuspendedreasontype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_266_TOO_MANY_RESPONSES'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','3')
    
  
    --attributeoption  'selfsuspendedreasontype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'selfsuspendedreasontype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_266_TOO_FEW_RESPONSES'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','4')
    
  
    --attributeoption  'selfsuspendedreasontype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'selfsuspendedreasontype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_266_TAKING_A_BREAK__VACATION'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','5')
    
  
    --attributeoption  'selfsuspendedreasontype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'selfsuspendedreasontype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_266_TECHNICAL_ISSUES_LOGGING_IN_ETC'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','6')
    
  
    --attributeoption  'selfsuspendedreasontype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'selfsuspendedreasontype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_266_SITE_PERFORMANCE_PHOTOS_ESSAYS_ETC'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','7')
    
  
    --attributeoption  'selfsuspendedreasontype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'selfsuspendedreasontype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_266_SUSPEND_OTHER'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','8')
    
  
    --attributeoption  'israelipersonalitytrait'
    
    select @attributeid=attributeid from attribute
    where attributename= 'israelipersonalitytrait'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_182_CRITICIZING'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','0')
    
  
    --attributeoption  'desiredreligious'
    
    select @attributeid=attributeid from attribute
    where attributename= 'desiredreligious'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_519_SECULAR'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','1')
    
  
    --attributeoption  'bodytype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'bodytype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_BLANK_75374'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','0')
    
  
    --attributeoption  'bodytype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'bodytype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_87_PORTLY'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','1')
    
  
    --attributeoption  'bodytype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'bodytype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_87_RUBENESQUE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','20')
    
  
    --attributeoption  'bodytype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'bodytype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_87_FULL'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','30')
    
  
    --attributeoption  'bodytype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'bodytype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_87_LARGEBROAD_BUILD'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','40')
    
  
    --attributeoption  'bodytype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'bodytype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_87_A_FEW_EXTRA_POUNDS'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','50')
    
  
    --attributeoption  'bodytype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'bodytype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_87_AVERAGEMEDIUM_BUILD'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','60')
    
  
    --attributeoption  'bodytype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'bodytype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_87_ATHLETICFIT'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','70')
    
  
    --attributeoption  'bodytype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'bodytype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_87_RIPPED'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','80')
    
  
    --attributeoption  'bodytype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'bodytype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_87_LEANSLENDER'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','90')
    
  
    --attributeoption  'bodytype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'bodytype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_87_TELL_YOU_LATER'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','150')
    
  
    --attributeoption  'smsalertpreference'
    
    select @attributeid=attributeid from attribute
    where attributename= 'smsalertpreference'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_528_GOTCLICKALERT'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','1')
    
  
    --attributeoption  'smsalertpreference'
    
    select @attributeid=attributeid from attribute
    where attributename= 'smsalertpreference'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_528_NEWEMAILALERT'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','2')
    
  
    --attributeoption  'smsalertpreference'
    
    select @attributeid=attributeid from attribute
    where attributename= 'smsalertpreference'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_528_ECARDALERT'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','3')
    
  
    --attributeoption  'smsalertpreference'
    
    select @attributeid=attributeid from attribute
    where attributename= 'smsalertpreference'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_528_HOTLISTEDALERT'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','4')
    
  
    --attributeoption  'smsalertpreference'
    
    select @attributeid=attributeid from attribute
    where attributename= 'smsalertpreference'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_528_NEWFLIRTALERT'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','5')
    
  
    --attributeoption  'mailboxsystemfolders'
    
    select @attributeid=attributeid from attribute
    where attributename= 'mailboxsystemfolders'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_360_INBOX'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','1')
    
  
    --attributeoption  'mailboxsystemfolders'
    
    select @attributeid=attributeid from attribute
    where attributename= 'mailboxsystemfolders'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_360_DRAFT'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','2')
    
  
    --attributeoption  'mailboxsystemfolders'
    
    select @attributeid=attributeid from attribute
    where attributename= 'mailboxsystemfolders'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_360_SENT'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','3')
    
  
    --attributeoption  'mailboxsystemfolders'
    
    select @attributeid=attributeid from attribute
    where attributename= 'mailboxsystemfolders'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_360_TRASH'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','4')
    
  
    --attributeoption  'drinkinghabits'
    
    select @attributeid=attributeid from attribute
    where attributename= 'drinkinghabits'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_BLANK_74265'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','1')
    
  
    --attributeoption  'drinkinghabits'
    
    select @attributeid=attributeid from attribute
    where attributename= 'drinkinghabits'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_93_NEVER'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','2')
    
  
    --attributeoption  'drinkinghabits'
    
    select @attributeid=attributeid from attribute
    where attributename= 'drinkinghabits'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_93_SOCIALLY'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','3')
    
  
    --attributeoption  'drinkinghabits'
    
    select @attributeid=attributeid from attribute
    where attributename= 'drinkinghabits'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_93_ON_OCCASION'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','4')
    
  
    --attributeoption  'drinkinghabits'
    
    select @attributeid=attributeid from attribute
    where attributename= 'drinkinghabits'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_93_FREQUENTLY'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','5')
    
  
    --attributeoption  'tease'
    
    select @attributeid=attributeid from attribute
    where attributename= 'tease'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_334_SELECT_A_TEASE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','0')
    
  
    --attributeoption  'tease'
    
    select @attributeid=attributeid from attribute
    where attributename= 'tease'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_334_I_LIKED_YOUR_PROFILE_AND_THINK_WE_COULD_BE_RIGHT_FOR_EACH_OTHER'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','1')
    
  
    --attributeoption  'tease'
    
    select @attributeid=attributeid from attribute
    where attributename= 'tease'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_334_I_LOVED_YOUR_PROFILE_WHAT_DO_YOU_THINK_OF_MINE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','2')
    
  
    --attributeoption  'tease'
    
    select @attributeid=attributeid from attribute
    where attributename= 'tease'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_334_YOUR_ESSAYS_ARE_GREAT_BUT_ID_REALLY_LIKE_TO_SEE_A_PHOTO_OF_YOU_FIRST'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','3')
    
  
    --attributeoption  'tease'
    
    select @attributeid=attributeid from attribute
    where attributename= 'tease'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_334_HOW_ABOUT_A_DATE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','4')
    
  
    --attributeoption  'tease'
    
    select @attributeid=attributeid from attribute
    where attributename= 'tease'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_334_IM_NEW_AT_THIS_BUT_WHEN_I_SAW_YOU_I_COULDNT_HELP_IT_BUT_TO_SAY_"HI"'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','5')
    
  
    --attributeoption  'tease'
    
    select @attributeid=attributeid from attribute
    where attributename= 'tease'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_334_WOW!_I_THINK_IM_IN_LOVE!_TELL_ME_YOU_FEEL_THE_SAME_WAY!'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','6')
    
  
    --attributeoption  'tease'
    
    select @attributeid=attributeid from attribute
    where attributename= 'tease'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_334_IF_I_COULD_REARRANGE_THE_ALPHABET_ID_PUT_U_AND_I_TOGETHER!'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','7')
    
  
    --attributeoption  'tease'
    
    select @attributeid=attributeid from attribute
    where attributename= 'tease'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_334_DO_YOU_BELIEVE_IN_LOVE_AT_FIRST_TYPE!!!!!'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','8')
    
  
    --attributeoption  'tease'
    
    select @attributeid=attributeid from attribute
    where attributename= 'tease'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_334_ARE_YOU_FROM_OUTER_SPACE_CAUSE_HONEY_YOURE_OUT_OF_THIS_WORLD!'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','9')
    
  
    --attributeoption  'tease'
    
    select @attributeid=attributeid from attribute
    where attributename= 'tease'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_334_DINNER_MOVIE_MARRIAGE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','10')
    
  
    --attributeoption  'tease'
    
    select @attributeid=attributeid from attribute
    where attributename= 'tease'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_334_DID_IT_HURT_WHEN_YOU_FELL_FROM_HEAVEN'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','11')
    
  
    --attributeoption  'tease'
    
    select @attributeid=attributeid from attribute
    where attributename= 'tease'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_334_IVE_LOST_MY_PHONE_NUMBER_CAN_I_HAVE_YOURS'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','12')
    
  
    --attributeoption  'globalstatusmask'
    
    select @attributeid=attributeid from attribute
    where attributename= 'globalstatusmask'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_268_ADMIN_SUSPENDED'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','1')
    
  
    --attributeoption  'globalstatusmask'
    
    select @attributeid=attributeid from attribute
    where attributename= 'globalstatusmask'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_268_BOUNCED_EMAIL'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','2')
    
  
    --attributeoption  'globalstatusmask'
    
    select @attributeid=attributeid from attribute
    where attributename= 'globalstatusmask'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_268_VERIFIED_EMAIL'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','3')
    
  
    --attributeoption  'globalstatusmask'
    
    select @attributeid=attributeid from attribute
    where attributename= 'globalstatusmask'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_268_ARCHIVED'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','4')
    
  
    --attributeoption  'matchnewsletterperiod'
    
    select @attributeid=attributeid from attribute
    where attributename= 'matchnewsletterperiod'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_344_DAILY'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','1')
    
  
    --attributeoption  'matchnewsletterperiod'
    
    select @attributeid=attributeid from attribute
    where attributename= 'matchnewsletterperiod'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_344_TWICE_A_WEEK'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','2')
    
  
    --attributeoption  'matchnewsletterperiod'
    
    select @attributeid=attributeid from attribute
    where attributename= 'matchnewsletterperiod'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_344_ONCE_A_WEEK'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','3')
    
  
    --attributeoption  'matchnewsletterperiod'
    
    select @attributeid=attributeid from attribute
    where attributename= 'matchnewsletterperiod'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_344_NEVER'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','4')
    
  
    --attributeoption  'suspendmask'
    
    select @attributeid=attributeid from attribute
    where attributename= 'suspendmask'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_260_ADMIN'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','0')
    
  
    --attributeoption  'suspendmask'
    
    select @attributeid=attributeid from attribute
    where attributename= 'suspendmask'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_260_SELF'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','1')
    
  
    --attributeoption  'personalitytrait'
    
    select @attributeid=attributeid from attribute
    where attributename= 'personalitytrait'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_97_ADVENTUROUSWILDSPONTANEOUS'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','10')
    
  
    --attributeoption  'personalitytrait'
    
    select @attributeid=attributeid from attribute
    where attributename= 'personalitytrait'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_97_ARGUMENTATIVE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','20')
    
  
    --attributeoption  'personalitytrait'
    
    select @attributeid=attributeid from attribute
    where attributename= 'personalitytrait'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_97_ARTISTIC'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','30')
    
  
    --attributeoption  'personalitytrait'
    
    select @attributeid=attributeid from attribute
    where attributename= 'personalitytrait'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_97_COMPULSIVE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','40')
    
  
    --attributeoption  'personalitytrait'
    
    select @attributeid=attributeid from attribute
    where attributename= 'personalitytrait'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_97_CONSERVATIVECLEAN_CUT'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','50')
    
  
    --attributeoption  'personalitytrait'
    
    select @attributeid=attributeid from attribute
    where attributename= 'personalitytrait'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_97_EARTHY'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','60')
    
  
    --attributeoption  'personalitytrait'
    
    select @attributeid=attributeid from attribute
    where attributename= 'personalitytrait'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_97_EASYGOINGFLEXIBLEOPENMINDED'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','70')
    
  
    --attributeoption  'personalitytrait'
    
    select @attributeid=attributeid from attribute
    where attributename= 'personalitytrait'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_97_ECCENTRIC'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','80')
    
  
    --attributeoption  'personalitytrait'
    
    select @attributeid=attributeid from attribute
    where attributename= 'personalitytrait'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_97_FLAMBOYANT'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','90')
    
  
    --attributeoption  'personalitytrait'
    
    select @attributeid=attributeid from attribute
    where attributename= 'personalitytrait'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_97_FLIRTATIOUSPLAYFUL'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','100')
    
  
    --attributeoption  'personalitytrait'
    
    select @attributeid=attributeid from attribute
    where attributename= 'personalitytrait'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_97_FRIENDLYKIND'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','110')
    
  
    --attributeoption  'personalitytrait'
    
    select @attributeid=attributeid from attribute
    where attributename= 'personalitytrait'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_97_HIGH_ENERGY'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','120')
    
  
    --attributeoption  'personalitytrait'
    
    select @attributeid=attributeid from attribute
    where attributename= 'personalitytrait'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_97_HIGH_MAINTENACE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','130')
    
  
    --attributeoption  'personalitytrait'
    
    select @attributeid=attributeid from attribute
    where attributename= 'personalitytrait'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_97_HUMOROUSWITTY'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','140')
    
  
    --attributeoption  'personalitytrait'
    
    select @attributeid=attributeid from attribute
    where attributename= 'personalitytrait'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_97_INTELLECTUAL'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','150')
    
  
    --attributeoption  'personalitytrait'
    
    select @attributeid=attributeid from attribute
    where attributename= 'personalitytrait'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_97_LOW_MAINTENANCE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','160')
    
  
    --attributeoption  'personalitytrait'
    
    select @attributeid=attributeid from attribute
    where attributename= 'personalitytrait'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_97_SENSITIVENURTURINGLOVING'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','170')
    
  
    --attributeoption  'personalitytrait'
    
    select @attributeid=attributeid from attribute
    where attributename= 'personalitytrait'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_97_OUTGOING'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','180')
    
  
    --attributeoption  'personalitytrait'
    
    select @attributeid=attributeid from attribute
    where attributename= 'personalitytrait'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_97_PRACTICAL'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','190')
    
  
    --attributeoption  'personalitytrait'
    
    select @attributeid=attributeid from attribute
    where attributename= 'personalitytrait'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_97_PROCRASTINATOR'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','200')
    
  
    --attributeoption  'personalitytrait'
    
    select @attributeid=attributeid from attribute
    where attributename= 'personalitytrait'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_97_QUIETSHY'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','210')
    
  
    --attributeoption  'personalitytrait'
    
    select @attributeid=attributeid from attribute
    where attributename= 'personalitytrait'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_97_ROMANTIC'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','220')
    
  
    --attributeoption  'personalitytrait'
    
    select @attributeid=attributeid from attribute
    where attributename= 'personalitytrait'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_97_SELF_CONFIDENT'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','230')
    
  
    --attributeoption  'personalitytrait'
    
    select @attributeid=attributeid from attribute
    where attributename= 'personalitytrait'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_97_SERIOUSRESPONSIBLE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','240')
    
  
    --attributeoption  'personalitytrait'
    
    select @attributeid=attributeid from attribute
    where attributename= 'personalitytrait'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_97_SIMPLE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','250')
    
  
    --attributeoption  'personalitytrait'
    
    select @attributeid=attributeid from attribute
    where attributename= 'personalitytrait'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_97_SOPHISTICATEDWORLDLY'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','260')
    
  
    --attributeoption  'personalitytrait'
    
    select @attributeid=attributeid from attribute
    where attributename= 'personalitytrait'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_97_SPIRITUAL'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','270')
    
  
    --attributeoption  'personalitytrait'
    
    select @attributeid=attributeid from attribute
    where attributename= 'personalitytrait'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_97_STUBBORN'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','280')
    
  
    --attributeoption  'personalitytrait'
    
    select @attributeid=attributeid from attribute
    where attributename= 'personalitytrait'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_97_TALKATIVE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','290')
    
  
    --attributeoption  'personalitytrait'
    
    select @attributeid=attributeid from attribute
    where attributename= 'personalitytrait'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_97_UNCONVENTIONALFREESPIRITED'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','300')
    
  
    --attributeoption  'gotaclickemailperiod'
    
    select @attributeid=attributeid from attribute
    where attributename= 'gotaclickemailperiod'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_204_DAILY'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','1')
    
  
    --attributeoption  'gotaclickemailperiod'
    
    select @attributeid=attributeid from attribute
    where attributename= 'gotaclickemailperiod'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_204_TWICE_A_WEEK'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','2')
    
  
    --attributeoption  'gotaclickemailperiod'
    
    select @attributeid=attributeid from attribute
    where attributename= 'gotaclickemailperiod'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_204_ONCE_A_WEEK'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','3')
    
  
    --attributeoption  'gotaclickemailperiod'
    
    select @attributeid=attributeid from attribute
    where attributename= 'gotaclickemailperiod'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_204_NEVER'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','4')
    
  
    --attributeoption  'bankaccounttype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'bankaccounttype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_234_CHECKING'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','1')
    
  
    --attributeoption  'bankaccounttype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'bankaccounttype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_234_SAVINGS'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','2')
    
  
    --attributeoption  'appearingonthemedia'
    
    select @attributeid=attributeid from attribute
    where attributename= 'appearingonthemedia'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_194_ON_OTHER_COBRANDED_SITES_IN_THE_INTERNT'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','0')
    
  
    --attributeoption  'searchtypeid'
    
    select @attributeid=attributeid from attribute
    where attributename= 'searchtypeid'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_480_POSTAL_CODE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','1')
    
  
    --attributeoption  'searchtypeid'
    
    select @attributeid=attributeid from attribute
    where attributename= 'searchtypeid'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_480_AREA_CODE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','2')
    
  
    --attributeoption  'searchtypeid'
    
    select @attributeid=attributeid from attribute
    where attributename= 'searchtypeid'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_480_REGION'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','4')
    
  
    --attributeoption  'searchtypeid'
    
    select @attributeid=attributeid from attribute
    where attributename= 'searchtypeid'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_480_COLLEGE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','8')
    
  
    --attributeoption  'morechildrenflag'
    
    select @attributeid=attributeid from attribute
    where attributename= 'morechildrenflag'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_250_NO'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','1')
    
  
    --attributeoption  'morechildrenflag'
    
    select @attributeid=attributeid from attribute
    where attributename= 'morechildrenflag'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_250_YES'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','3')
    
  
    --attributeoption  'morechildrenflag'
    
    select @attributeid=attributeid from attribute
    where attributename= 'morechildrenflag'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_250_NOT_SURE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','5')
    
  
    --attributeoption  'physicalactivity'
    
    select @attributeid=attributeid from attribute
    where attributename= 'physicalactivity'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_116_AEROBICS'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','1')
    
  
    --attributeoption  'physicalactivity'
    
    select @attributeid=attributeid from attribute
    where attributename= 'physicalactivity'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_116_BASEBALLSOFTBALL'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','20')
    
  
    --attributeoption  'physicalactivity'
    
    select @attributeid=attributeid from attribute
    where attributename= 'physicalactivity'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_116_BASKETBALL'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','30')
    
  
    --attributeoption  'physicalactivity'
    
    select @attributeid=attributeid from attribute
    where attributename= 'physicalactivity'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_116_BIKING'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','40')
    
  
    --attributeoption  'physicalactivity'
    
    select @attributeid=attributeid from attribute
    where attributename= 'physicalactivity'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_116_BOATINGSAILINGRAFTING'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','50')
    
  
    --attributeoption  'physicalactivity'
    
    select @attributeid=attributeid from attribute
    where attributename= 'physicalactivity'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_116_BOWLING'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','70')
    
  
    --attributeoption  'physicalactivity'
    
    select @attributeid=attributeid from attribute
    where attributename= 'physicalactivity'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_116_CRICKET'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','80')
    
  
    --attributeoption  'physicalactivity'
    
    select @attributeid=attributeid from attribute
    where attributename= 'physicalactivity'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_116_DANCING'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','90')
    
  
    --attributeoption  'physicalactivity'
    
    select @attributeid=attributeid from attribute
    where attributename= 'physicalactivity'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_116_FOOTBALL__AMERICAN'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','100')
    
  
    --attributeoption  'physicalactivity'
    
    select @attributeid=attributeid from attribute
    where attributename= 'physicalactivity'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_116_GOLF'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','110')
    
  
    --attributeoption  'physicalactivity'
    
    select @attributeid=attributeid from attribute
    where attributename= 'physicalactivity'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_116_HIKINGWALKING'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','120')
    
  
    --attributeoption  'physicalactivity'
    
    select @attributeid=attributeid from attribute
    where attributename= 'physicalactivity'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_116_HOCKEY'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','130')
    
  
    --attributeoption  'physicalactivity'
    
    select @attributeid=attributeid from attribute
    where attributename= 'physicalactivity'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_116_HORSEBACK_RIDING'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','140')
    
  
    --attributeoption  'physicalactivity'
    
    select @attributeid=attributeid from attribute
    where attributename= 'physicalactivity'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_116_HUNTINGFISHING'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','150')
    
  
    --attributeoption  'physicalactivity'
    
    select @attributeid=attributeid from attribute
    where attributename= 'physicalactivity'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_116_INLINE_SKATINGROLLER_SKATING'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','160')
    
  
    --attributeoption  'physicalactivity'
    
    select @attributeid=attributeid from attribute
    where attributename= 'physicalactivity'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_116_ICE_SKATING'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','170')
    
  
    --attributeoption  'physicalactivity'
    
    select @attributeid=attributeid from attribute
    where attributename= 'physicalactivity'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_116_JETWATER_SKIING'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','180')
    
  
    --attributeoption  'physicalactivity'
    
    select @attributeid=attributeid from attribute
    where attributename= 'physicalactivity'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_116_JOGGINGRUNNING'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','190')
    
  
    --attributeoption  'physicalactivity'
    
    select @attributeid=attributeid from attribute
    where attributename= 'physicalactivity'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_116_MARTIAL_ARTS'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','200')
    
  
    --attributeoption  'physicalactivity'
    
    select @attributeid=attributeid from attribute
    where attributename= 'physicalactivity'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_116_ROCK_CLIMBING'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','210')
    
  
    --attributeoption  'physicalactivity'
    
    select @attributeid=attributeid from attribute
    where attributename= 'physicalactivity'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_116_RUGBY'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','220')
    
  
    --attributeoption  'physicalactivity'
    
    select @attributeid=attributeid from attribute
    where attributename= 'physicalactivity'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_116_SKYDIVINGHANG_GLIDINGFLYING'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','230')
    
  
    --attributeoption  'physicalactivity'
    
    select @attributeid=attributeid from attribute
    where attributename= 'physicalactivity'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_116_SNORKELINGSCUBA_DIVING'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','240')
    
  
    --attributeoption  'physicalactivity'
    
    select @attributeid=attributeid from attribute
    where attributename= 'physicalactivity'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_116_SNOW_SKIING'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','250')
    
  
    --attributeoption  'physicalactivity'
    
    select @attributeid=attributeid from attribute
    where attributename= 'physicalactivity'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_116_SOCCER'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','260')
    
  
    --attributeoption  'physicalactivity'
    
    select @attributeid=attributeid from attribute
    where attributename= 'physicalactivity'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_116_SURFINGSNOWBOARDINGSKATEBOARDING'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','270')
    
  
    --attributeoption  'physicalactivity'
    
    select @attributeid=attributeid from attribute
    where attributename= 'physicalactivity'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_116_SWIMMINGDIVINGWATER_POLO'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','280')
    
  
    --attributeoption  'physicalactivity'
    
    select @attributeid=attributeid from attribute
    where attributename= 'physicalactivity'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_116_TENNISRACQUET_SPORTS'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','290')
    
  
    --attributeoption  'physicalactivity'
    
    select @attributeid=attributeid from attribute
    where attributename= 'physicalactivity'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_116_VOLLEYBALL'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','300')
    
  
    --attributeoption  'physicalactivity'
    
    select @attributeid=attributeid from attribute
    where attributename= 'physicalactivity'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_116_WORKING_OUTWEIGHTLIFTING'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','305')
    
  
    --attributeoption  'physicalactivity'
    
    select @attributeid=attributeid from attribute
    where attributename= 'physicalactivity'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_116_YOGAMEDITATION'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','310')
    
  
    --attributeoption  'creditcardtype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'creditcardtype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_33_CHOOSE_ONE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','0')
    
  
    --attributeoption  'creditcardtype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'creditcardtype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_33_AMERICAN_EXPRESS_CREDIT_CARD_TYPE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','1')
    
  
    --attributeoption  'creditcardtype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'creditcardtype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_33_CARTE_BLANCHE_CREDIT_CARD_TYPE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','2')
    
  
    --attributeoption  'creditcardtype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'creditcardtype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_33_DINERS_CREDIT_CARD_TYPE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','3')
    
  
    --attributeoption  'creditcardtype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'creditcardtype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_33_DISCOVER_CREDIT_CARD_TYPE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','4')
    
  
    --attributeoption  'creditcardtype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'creditcardtype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_33_JCB_CREDIT_CARD_TYPE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','5')
    
  
    --attributeoption  'creditcardtype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'creditcardtype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_33_MASTERCARD_CREDIT_CARD_TYPE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','6')
    
  
    --attributeoption  'creditcardtype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'creditcardtype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_33_VISA_CARD_CREDIT_CARD_TYPE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','7')
    
  
    --attributeoption  'cuisine'
    
    select @attributeid=attributeid from attribute
    where attributename= 'cuisine'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_109_AMERICAN'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','10')
    
  
    --attributeoption  'cuisine'
    
    select @attributeid=attributeid from attribute
    where attributename= 'cuisine'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_109_BARBECUE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','20')
    
  
    --attributeoption  'cuisine'
    
    select @attributeid=attributeid from attribute
    where attributename= 'cuisine'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_109_CAJUNSOUTHERN'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','30')
    
  
    --attributeoption  'cuisine'
    
    select @attributeid=attributeid from attribute
    where attributename= 'cuisine'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_109_CALIFORNIAFUSION'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','40')
    
  
    --attributeoption  'cuisine'
    
    select @attributeid=attributeid from attribute
    where attributename= 'cuisine'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_109_CARIBBEANCUBAN'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','50')
    
  
    --attributeoption  'cuisine'
    
    select @attributeid=attributeid from attribute
    where attributename= 'cuisine'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_109_CHINESEDIM_SUM'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','60')
    
  
    --attributeoption  'cuisine'
    
    select @attributeid=attributeid from attribute
    where attributename= 'cuisine'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_109_CONTINENTAL'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','70')
    
  
    --attributeoption  'cuisine'
    
    select @attributeid=attributeid from attribute
    where attributename= 'cuisine'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_109_DELI'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','80')
    
  
    --attributeoption  'cuisine'
    
    select @attributeid=attributeid from attribute
    where attributename= 'cuisine'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_109_EASTERN_EUROPEAN'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','90')
    
  
    --attributeoption  'cuisine'
    
    select @attributeid=attributeid from attribute
    where attributename= 'cuisine'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_109_FAST_FOODPIZZA'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','100')
    
  
    --attributeoption  'cuisine'
    
    select @attributeid=attributeid from attribute
    where attributename= 'cuisine'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_109_FRENCH'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','110')
    
  
    --attributeoption  'cuisine'
    
    select @attributeid=attributeid from attribute
    where attributename= 'cuisine'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_109_GERMAN'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','120')
    
  
    --attributeoption  'cuisine'
    
    select @attributeid=attributeid from attribute
    where attributename= 'cuisine'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_109_GREEK'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','130')
    
  
    --attributeoption  'cuisine'
    
    select @attributeid=attributeid from attribute
    where attributename= 'cuisine'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_109_INDIAN'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','140')
    
  
    --attributeoption  'cuisine'
    
    select @attributeid=attributeid from attribute
    where attributename= 'cuisine'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_109_ITALIAN'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','150')
    
  
    --attributeoption  'cuisine'
    
    select @attributeid=attributeid from attribute
    where attributename= 'cuisine'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_109_JAPANESESUSHI'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','160')
    
  
    --attributeoption  'cuisine'
    
    select @attributeid=attributeid from attribute
    where attributename= 'cuisine'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_109_JEWISHKOSHER'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','170')
    
  
    --attributeoption  'cuisine'
    
    select @attributeid=attributeid from attribute
    where attributename= 'cuisine'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_109_KOREAN'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','180')
    
  
    --attributeoption  'cuisine'
    
    select @attributeid=attributeid from attribute
    where attributename= 'cuisine'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_109_MEDITERRANEAN'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','190')
    
  
    --attributeoption  'cuisine'
    
    select @attributeid=attributeid from attribute
    where attributename= 'cuisine'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_109_MEXICAN'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','200')
    
  
    --attributeoption  'cuisine'
    
    select @attributeid=attributeid from attribute
    where attributename= 'cuisine'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_109_MIDDLE_EASTERN'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','210')
    
  
    --attributeoption  'cuisine'
    
    select @attributeid=attributeid from attribute
    where attributename= 'cuisine'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_109_SEAFOOD'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','220')
    
  
    --attributeoption  'cuisine'
    
    select @attributeid=attributeid from attribute
    where attributename= 'cuisine'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_109_SOUL_FOOD'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','230')
    
  
    --attributeoption  'cuisine'
    
    select @attributeid=attributeid from attribute
    where attributename= 'cuisine'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_109_SOUTH_AMERICAN'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','240')
    
  
    --attributeoption  'cuisine'
    
    select @attributeid=attributeid from attribute
    where attributename= 'cuisine'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_109_SOUTHWESTERN'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','250')
    
  
    --attributeoption  'cuisine'
    
    select @attributeid=attributeid from attribute
    where attributename= 'cuisine'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_109_SPANISH'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','260')
    
  
    --attributeoption  'cuisine'
    
    select @attributeid=attributeid from attribute
    where attributename= 'cuisine'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_109_THAI'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','270')
    
  
    --attributeoption  'cuisine'
    
    select @attributeid=attributeid from attribute
    where attributename= 'cuisine'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_109_VEGETARIANORGANIC'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','280')
    
  
    --attributeoption  'cuisine'
    
    select @attributeid=attributeid from attribute
    where attributename= 'cuisine'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_109_VEGAN'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','290')
    
  
    --attributeoption  'cuisine'
    
    select @attributeid=attributeid from attribute
    where attributename= 'cuisine'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_109_VIETNAMESE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','300')
    
  
    --attributeoption  'languagemask'
    
    select @attributeid=attributeid from attribute
    where attributename= 'languagemask'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_374_ARABIC'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','10')
    
  
    --attributeoption  'languagemask'
    
    select @attributeid=attributeid from attribute
    where attributename= 'languagemask'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_374_BENGALI'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','20')
    
  
    --attributeoption  'languagemask'
    
    select @attributeid=attributeid from attribute
    where attributename= 'languagemask'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_374_BULGARIAN'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','30')
    
  
    --attributeoption  'languagemask'
    
    select @attributeid=attributeid from attribute
    where attributename= 'languagemask'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_374_CHINESE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','40')
    
  
    --attributeoption  'languagemask'
    
    select @attributeid=attributeid from attribute
    where attributename= 'languagemask'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_374_CZECH'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','50')
    
  
    --attributeoption  'languagemask'
    
    select @attributeid=attributeid from attribute
    where attributename= 'languagemask'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_374_DUTCH'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','60')
    
  
    --attributeoption  'languagemask'
    
    select @attributeid=attributeid from attribute
    where attributename= 'languagemask'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_374_ENGLISH'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','70')
    
  
    --attributeoption  'languagemask'
    
    select @attributeid=attributeid from attribute
    where attributename= 'languagemask'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_374_FINNISH'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','80')
    
  
    --attributeoption  'languagemask'
    
    select @attributeid=attributeid from attribute
    where attributename= 'languagemask'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_374_FRENCH'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','90')
    
  
    --attributeoption  'languagemask'
    
    select @attributeid=attributeid from attribute
    where attributename= 'languagemask'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_374_GERMAN'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','100')
    
  
    --attributeoption  'languagemask'
    
    select @attributeid=attributeid from attribute
    where attributename= 'languagemask'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_374_GREEK'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','110')
    
  
    --attributeoption  'languagemask'
    
    select @attributeid=attributeid from attribute
    where attributename= 'languagemask'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_374_HEBREW'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','120')
    
  
    --attributeoption  'languagemask'
    
    select @attributeid=attributeid from attribute
    where attributename= 'languagemask'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_374_HINDI'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','130')
    
  
    --attributeoption  'languagemask'
    
    select @attributeid=attributeid from attribute
    where attributename= 'languagemask'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_374_ITALIAN'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','140')
    
  
    --attributeoption  'languagemask'
    
    select @attributeid=attributeid from attribute
    where attributename= 'languagemask'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_374_JAPANESE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','150')
    
  
    --attributeoption  'languagemask'
    
    select @attributeid=attributeid from attribute
    where attributename= 'languagemask'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_374_KOREAN'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','160')
    
  
    --attributeoption  'languagemask'
    
    select @attributeid=attributeid from attribute
    where attributename= 'languagemask'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_374_MALAY'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','170')
    
  
    --attributeoption  'languagemask'
    
    select @attributeid=attributeid from attribute
    where attributename= 'languagemask'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_374_NORWEGIAN'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','180')
    
  
    --attributeoption  'languagemask'
    
    select @attributeid=attributeid from attribute
    where attributename= 'languagemask'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_374_PERSIANFARSI'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','190')
    
  
    --attributeoption  'languagemask'
    
    select @attributeid=attributeid from attribute
    where attributename= 'languagemask'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_374_POLISH'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','200')
    
  
    --attributeoption  'languagemask'
    
    select @attributeid=attributeid from attribute
    where attributename= 'languagemask'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_374_PORTUGUESE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','210')
    
  
    --attributeoption  'languagemask'
    
    select @attributeid=attributeid from attribute
    where attributename= 'languagemask'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_374_ROMANIAN'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','220')
    
  
    --attributeoption  'languagemask'
    
    select @attributeid=attributeid from attribute
    where attributename= 'languagemask'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_374_RUSSIAN'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','230')
    
  
    --attributeoption  'languagemask'
    
    select @attributeid=attributeid from attribute
    where attributename= 'languagemask'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_374_SPANISH'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','240')
    
  
    --attributeoption  'languagemask'
    
    select @attributeid=attributeid from attribute
    where attributename= 'languagemask'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_374_SWEDISH'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','250')
    
  
    --attributeoption  'languagemask'
    
    select @attributeid=attributeid from attribute
    where attributename= 'languagemask'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_374_TAGALOG'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','260')
    
  
    --attributeoption  'languagemask'
    
    select @attributeid=attributeid from attribute
    where attributename= 'languagemask'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_374_THAI'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','270')
    
  
    --attributeoption  'languagemask'
    
    select @attributeid=attributeid from attribute
    where attributename= 'languagemask'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_374_URDU'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','280')
    
  
    --attributeoption  'languagemask'
    
    select @attributeid=attributeid from attribute
    where attributename= 'languagemask'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_374_VIETNAMESE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','290')
    
  
    --attributeoption  'languagemask'
    
    select @attributeid=attributeid from attribute
    where attributename= 'languagemask'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_374_YIDDISH'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','300')
    
  
    --attributeoption  'languagemask'
    
    select @attributeid=attributeid from attribute
    where attributename= 'languagemask'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_374_OTHER'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','999')
    
  
    --attributeoption  'hasphotos'
    
    select @attributeid=attributeid from attribute
    where attributename= 'hasphotos'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_470_HAS_PHOTOS'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','1')
    
  
    --attributeoption  'hasphotos'
    
    select @attributeid=attributeid from attribute
    where attributename= 'hasphotos'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_470_HAS_NO_PHOTOS'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','2')
    
  
    --attributeoption  'haircolor'
    
    select @attributeid=attributeid from attribute
    where attributename= 'haircolor'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_BLANK_74218'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','1')
    
  
    --attributeoption  'haircolor'
    
    select @attributeid=attributeid from attribute
    where attributename= 'haircolor'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_85_AUBURN'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','2')
    
  
    --attributeoption  'haircolor'
    
    select @attributeid=attributeid from attribute
    where attributename= 'haircolor'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_85_BALDSHAVED'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','3')
    
  
    --attributeoption  'haircolor'
    
    select @attributeid=attributeid from attribute
    where attributename= 'haircolor'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_85_BLACK'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','4')
    
  
    --attributeoption  'haircolor'
    
    select @attributeid=attributeid from attribute
    where attributename= 'haircolor'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_85_BLONDE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','5')
    
  
    --attributeoption  'haircolor'
    
    select @attributeid=attributeid from attribute
    where attributename= 'haircolor'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_85_BROWN'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','6')
    
  
    --attributeoption  'haircolor'
    
    select @attributeid=attributeid from attribute
    where attributename= 'haircolor'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_85_DARK_BROWN'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','7')
    
  
    --attributeoption  'haircolor'
    
    select @attributeid=attributeid from attribute
    where attributename= 'haircolor'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_85_DIRTY_BLONDE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','8')
    
  
    --attributeoption  'haircolor'
    
    select @attributeid=attributeid from attribute
    where attributename= 'haircolor'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_85_GRAY'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','9')
    
  
    --attributeoption  'haircolor'
    
    select @attributeid=attributeid from attribute
    where attributename= 'haircolor'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_85_LIGHT_BROWN'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','10')
    
  
    --attributeoption  'haircolor'
    
    select @attributeid=attributeid from attribute
    where attributename= 'haircolor'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_85_PLATINUM_BLONDE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','11')
    
  
    --attributeoption  'haircolor'
    
    select @attributeid=attributeid from attribute
    where attributename= 'haircolor'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_85_RADICALLY_DYED'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','12')
    
  
    --attributeoption  'haircolor'
    
    select @attributeid=attributeid from attribute
    where attributename= 'haircolor'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_85_RED'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','13')
    
  
    --attributeoption  'haircolor'
    
    select @attributeid=attributeid from attribute
    where attributename= 'haircolor'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_85_SALT__PEPPER'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','14')
    
  
    --attributeoption  'haircolor'
    
    select @attributeid=attributeid from attribute
    where attributename= 'haircolor'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_85_SILVERWHITE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','15')
    
  
    --attributeoption  'haircolor'
    
    select @attributeid=attributeid from attribute
    where attributename= 'haircolor'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_85_STRAWBERRY_BLONDE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','16')
    
  
    --attributeoption  'haircolor'
    
    select @attributeid=attributeid from attribute
    where attributename= 'haircolor'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_85_OTHER'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','17')
    
  
    --attributeoption  'photorejectreason'
    
    select @attributeid=attributeid from attribute
    where attributename= 'photorejectreason'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_127_COPYRIGHT_REJECT_LETTER'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','1')
    
  
    --attributeoption  'photorejectreason'
    
    select @attributeid=attributeid from attribute
    where attributename= 'photorejectreason'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_127_BLURY_REJECT_LETTER'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','2')
    
  
    --attributeoption  'photorejectreason'
    
    select @attributeid=attributeid from attribute
    where attributename= 'photorejectreason'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_127_FILE_SIZE_REJECT_LETTER'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','3')
    
  
    --attributeoption  'photorejectreason'
    
    select @attributeid=attributeid from attribute
    where attributename= 'photorejectreason'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_127_GENERAL_REJECT_LETTER'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','4')
    
  
    --attributeoption  'photorejectreason'
    
    select @attributeid=attributeid from attribute
    where attributename= 'photorejectreason'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_127_UNKNOWN_MEMBER'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','5')
    
  
    --attributeoption  'photorejectreason'
    
    select @attributeid=attributeid from attribute
    where attributename= 'photorejectreason'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_127_SUGGESTIVE_PHOTO'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','6')
    
  
    --attributeoption  'religionactivitylevel'
    
    select @attributeid=attributeid from attribute
    where attributename= 'religionactivitylevel'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_BLANK_105462'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','1')
    
  
    --attributeoption  'religionactivitylevel'
    
    select @attributeid=attributeid from attribute
    where attributename= 'religionactivitylevel'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_416_VERY_ACTIVE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','2')
    
  
    --attributeoption  'religionactivitylevel'
    
    select @attributeid=attributeid from attribute
    where attributename= 'religionactivitylevel'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_416_SOMEWHAT_ACTIVE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','3')
    
  
    --attributeoption  'religionactivitylevel'
    
    select @attributeid=attributeid from attribute
    where attributename= 'religionactivitylevel'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_416_ONLY_ON_RELIGIOUS_HOLIDAYS'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','4')
    
  
    --attributeoption  'religionactivitylevel'
    
    select @attributeid=attributeid from attribute
    where attributename= 'religionactivitylevel'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_416_NOT_ACTIVE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','5')
    
  
    --attributeoption  'religionactivitylevel'
    
    select @attributeid=attributeid from attribute
    where attributename= 'religionactivitylevel'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_416_PREFER_NOT_TO_SAY'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','6')
    
  
    --attributeoption  'relocateflag'
    
    select @attributeid=attributeid from attribute
    where attributename= 'relocateflag'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_244_YES'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','2')
    
  
    --attributeoption  'relocateflag'
    
    select @attributeid=attributeid from attribute
    where attributename= 'relocateflag'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_244_NO'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','3')
    
  
    --attributeoption  'relocateflag'
    
    select @attributeid=attributeid from attribute
    where attributename= 'relocateflag'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_244_NOT_SURE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','4')
    
  
    --attributeoption  'newslettermask'
    
    select @attributeid=attributeid from attribute
    where attributename= 'newslettermask'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_346_NEWS_AND_TIPS'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','1')
    
  
    --attributeoption  'newslettermask'
    
    select @attributeid=attributeid from attribute
    where attributename= 'newslettermask'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_346_MEMBER_EVENTS'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','2')
    
  
    --attributeoption  'newslettermask'
    
    select @attributeid=attributeid from attribute
    where attributename= 'newslettermask'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_346_QUALIFIED_PARTNERS'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','3')
    
  
    --attributeoption  'leisureactivity'
    
    select @attributeid=attributeid from attribute
    where attributename= 'leisureactivity'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_115_ANTIQUING'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','10')
    
  
    --attributeoption  'leisureactivity'
    
    select @attributeid=attributeid from attribute
    where attributename= 'leisureactivity'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_115_BOARD_GAMESBACKGAMMONCHESS'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','20')
    
  
    --attributeoption  'leisureactivity'
    
    select @attributeid=attributeid from attribute
    where attributename= 'leisureactivity'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_115_CAMPING'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','30')
    
  
    --attributeoption  'leisureactivity'
    
    select @attributeid=attributeid from attribute
    where attributename= 'leisureactivity'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_115_CARD_GAMESBRIDGECANASTA'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','40')
    
  
    --attributeoption  'leisureactivity'
    
    select @attributeid=attributeid from attribute
    where attributename= 'leisureactivity'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_115_COLLECTING'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','50')
    
  
    --attributeoption  'leisureactivity'
    
    select @attributeid=attributeid from attribute
    where attributename= 'leisureactivity'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_115_COOKINGBARBECUING'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','60')
    
  
    --attributeoption  'leisureactivity'
    
    select @attributeid=attributeid from attribute
    where attributename= 'leisureactivity'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_115_DINING_OUT'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','70')
    
  
    --attributeoption  'leisureactivity'
    
    select @attributeid=attributeid from attribute
    where attributename= 'leisureactivity'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_115_ENTERTAINING'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','80')
    
  
    --attributeoption  'leisureactivity'
    
    select @attributeid=attributeid from attribute
    where attributename= 'leisureactivity'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_115_GAMBLING'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','90')
    
  
    --attributeoption  'leisureactivity'
    
    select @attributeid=attributeid from attribute
    where attributename= 'leisureactivity'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_115_GARDENING'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','100')
    
  
    --attributeoption  'leisureactivity'
    
    select @attributeid=attributeid from attribute
    where attributename= 'leisureactivity'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_115_HANGING_OUT_WITH_FRIENDS'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','110')
    
  
    --attributeoption  'leisureactivity'
    
    select @attributeid=attributeid from attribute
    where attributename= 'leisureactivity'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_115_HOME_IMPROVEMENTDECORATING'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','120')
    
  
    --attributeoption  'leisureactivity'
    
    select @attributeid=attributeid from attribute
    where attributename= 'leisureactivity'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_115_INTIMATE_CONVERSATIONS'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','130')
    
  
    --attributeoption  'leisureactivity'
    
    select @attributeid=attributeid from attribute
    where attributename= 'leisureactivity'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_115_INVESTING'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','140')
    
  
    --attributeoption  'leisureactivity'
    
    select @attributeid=attributeid from attribute
    where attributename= 'leisureactivity'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_115_LISTENING_TOPLAYING_MUSIC'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','150')
    
  
    --attributeoption  'leisureactivity'
    
    select @attributeid=attributeid from attribute
    where attributename= 'leisureactivity'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_115_MOTORCYCLING'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','160')
    
  
    --attributeoption  'leisureactivity'
    
    select @attributeid=attributeid from attribute
    where attributename= 'leisureactivity'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_115_PAINTINGDRAWING'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','170')
    
  
    --attributeoption  'leisureactivity'
    
    select @attributeid=attributeid from attribute
    where attributename= 'leisureactivity'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_115_PARTYING'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','180')
    
  
    --attributeoption  'leisureactivity'
    
    select @attributeid=attributeid from attribute
    where attributename= 'leisureactivity'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_115_PEOPLE_WATCHING'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','190')
    
  
    --attributeoption  'leisureactivity'
    
    select @attributeid=attributeid from attribute
    where attributename= 'leisureactivity'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_115_PHOTOGRAPHY'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','200')
    
  
    --attributeoption  'leisureactivity'
    
    select @attributeid=attributeid from attribute
    where attributename= 'leisureactivity'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_115_POOLBILLIARDSDARTS'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','210')
    
  
    --attributeoption  'leisureactivity'
    
    select @attributeid=attributeid from attribute
    where attributename= 'leisureactivity'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_115_READINGWRITING'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','220')
    
  
    --attributeoption  'leisureactivity'
    
    select @attributeid=attributeid from attribute
    where attributename= 'leisureactivity'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_115_SEWINGCRAFTS'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','230')
    
  
    --attributeoption  'leisureactivity'
    
    select @attributeid=attributeid from attribute
    where attributename= 'leisureactivity'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_115_SHOPPING'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','240')
    
  
    --attributeoption  'leisureactivity'
    
    select @attributeid=attributeid from attribute
    where attributename= 'leisureactivity'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_115_SURFING_THE_WEBCHATTING_ONLINE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','250')
    
  
    --attributeoption  'leisureactivity'
    
    select @attributeid=attributeid from attribute
    where attributename= 'leisureactivity'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_115_TAKING_LONG_WALKS'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','260')
    
  
    --attributeoption  'leisureactivity'
    
    select @attributeid=attributeid from attribute
    where attributename= 'leisureactivity'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_115_TRAVELINGWEEKEND_TRIPSADVENTURE_TRAVEL'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','270')
    
  
    --attributeoption  'leisureactivity'
    
    select @attributeid=attributeid from attribute
    where attributename= 'leisureactivity'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_115_VIDEO_GAMESONLINE_GAMES'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','280')
    
  
    --attributeoption  'leisureactivity'
    
    select @attributeid=attributeid from attribute
    where attributename= 'leisureactivity'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_115_MOVIESTV'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','290')
    
  
    --attributeoption  'leisureactivity'
    
    select @attributeid=attributeid from attribute
    where attributename= 'leisureactivity'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_115_WINE_TASTING'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','300')
    
  
    --attributeoption  'maritalstatus'
    
    select @attributeid=attributeid from attribute
    where attributename= 'maritalstatus'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_BLANK_75376'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','1')
    
  
    --attributeoption  'maritalstatus'
    
    select @attributeid=attributeid from attribute
    where attributename= 'maritalstatus'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_32_SINGLE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','2')
    
  
    --attributeoption  'maritalstatus'
    
    select @attributeid=attributeid from attribute
    where attributename= 'maritalstatus'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_32_DIVORCED'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','3')
    
  
    --attributeoption  'maritalstatus'
    
    select @attributeid=attributeid from attribute
    where attributename= 'maritalstatus'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_32_SEPARATED'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','4')
    
  
    --attributeoption  'maritalstatus'
    
    select @attributeid=attributeid from attribute
    where attributename= 'maritalstatus'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_32_WIDOWED'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','5')
    
  
    --attributeoption  'contactuslist'
    
    select @attributeid=attributeid from attribute
    where attributename= 'contactuslist'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_218_UPDATING_MY_EMAIL_ADDRESS'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','3')
    
  
    --attributeoption  'contactuslist'
    
    select @attributeid=attributeid from attribute
    where attributename= 'contactuslist'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_218_BILLING_AND_SUBSCRIPTIONS'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','4')
    
  
    --attributeoption  'contactuslist'
    
    select @attributeid=attributeid from attribute
    where attributename= 'contactuslist'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_218_MY_PROFILE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','5')
    
  
    --attributeoption  'contactuslist'
    
    select @attributeid=attributeid from attribute
    where attributename= 'contactuslist'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_218_LOGGING_IN'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','6')
    
  
    --attributeoption  'contactuslist'
    
    select @attributeid=attributeid from attribute
    where attributename= 'contactuslist'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_218_UPDATING_MY_PHOTOS'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','7')
    
  
    --attributeoption  'contactuslist'
    
    select @attributeid=attributeid from attribute
    where attributename= 'contactuslist'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_218_GENERAL_QUESTION_OR_SUGGESTION'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','8')
    
  
    --attributeoption  'contactuslist'
    
    select @attributeid=attributeid from attribute
    where attributename= 'contactuslist'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_218_CONTACTING_OTHERS'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','9')
    
  
    --attributeoption  'contactuslist'
    
    select @attributeid=attributeid from attribute
    where attributename= 'contactuslist'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_218_SEARCHING__SEARCH'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','10')
    
  
    --attributeoption  'contactuslist'
    
    select @attributeid=attributeid from attribute
    where attributename= 'contactuslist'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_218_PR__MEDIA_RELATIONS'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','11')
    
  
    --attributeoption  'contactuslist'
    
    select @attributeid=attributeid from attribute
    where attributename= 'contactuslist'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_218_INVESTOR_RELATIONS'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','12')
    
  
    --attributeoption  'contactuslist'
    
    select @attributeid=attributeid from attribute
    where attributename= 'contactuslist'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_218_AFFILIATE_PROGRAM'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','13')
    
  
    --attributeoption  'contactuslist'
    
    select @attributeid=attributeid from attribute
    where attributename= 'contactuslist'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_218_EVENT__TRAVEL'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','14')
    
  
    --attributeoption  'contactuslist'
    
    select @attributeid=attributeid from attribute
    where attributename= 'contactuslist'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_218_GIVING_BACK_PROGRAM'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','15')
    
  
    --attributeoption  'contactuslist'
    
    select @attributeid=attributeid from attribute
    where attributename= 'contactuslist'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_218_MISSING_PHOTOS'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','16')
    
  
    --attributeoption  'reading'
    
    select @attributeid=attributeid from attribute
    where attributename= 'reading'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_188_FICTION'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','1')
    
  
    --attributeoption  'reading'
    
    select @attributeid=attributeid from attribute
    where attributename= 'reading'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_188_MAGAZINES'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','2')
    
  
    --attributeoption  'reading'
    
    select @attributeid=attributeid from attribute
    where attributename= 'reading'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_188_NEWSPAPERS'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','3')
    
  
    --attributeoption  'reading'
    
    select @attributeid=attributeid from attribute
    where attributename= 'reading'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_188_NONFICTION'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','4')
    
  
    --attributeoption  'reading'
    
    select @attributeid=attributeid from attribute
    where attributename= 'reading'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_188_POETRY'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','5')
    
  
    --attributeoption  'reading'
    
    select @attributeid=attributeid from attribute
    where attributename= 'reading'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_188_TRADE_JOURNALS'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','6')
    
  
    --attributeoption  'reading'
    
    select @attributeid=attributeid from attribute
    where attributename= 'reading'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_188_COMICS'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','7')
    
  
    --attributeoption  'blockingmask'
    
    select @attributeid=attributeid from attribute
    where attributename= 'blockingmask'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_543_NOTBLOCKED'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','1')
    
  
    --attributeoption  'blockingmask'
    
    select @attributeid=attributeid from attribute
    where attributename= 'blockingmask'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_543_BLOCKEDAFTERREG'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','2')
    
  
    --attributeoption  'blockingmask'
    
    select @attributeid=attributeid from attribute
    where attributename= 'blockingmask'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_543_BLOCKEDAFTEREMAIL'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','3')
    
  
    --attributeoption  'havechildren'
    
    select @attributeid=attributeid from attribute
    where attributename= 'havechildren'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_BLANK_75888'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','1')
    
  
    --attributeoption  'havechildren'
    
    select @attributeid=attributeid from attribute
    where attributename= 'havechildren'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_238_HAVE_CHILDREN_LIVING_AT_HOME'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','2')
    
  
    --attributeoption  'havechildren'
    
    select @attributeid=attributeid from attribute
    where attributename= 'havechildren'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_238_HAVE_CHILDREN_BUT_NOT_LIVING_AT_HOME'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','3')
    
  
    --attributeoption  'havechildren'
    
    select @attributeid=attributeid from attribute
    where attributename= 'havechildren'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_238_DO_NOT_HAVE_CHILDREN'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','4')
    
  
    --attributeoption  'imforbiddenwordlist'
    
    select @attributeid=attributeid from attribute
    where attributename= 'imforbiddenwordlist'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_412_SHIT'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','1')
    
  
    --attributeoption  'imforbiddenwordlist'
    
    select @attributeid=attributeid from attribute
    where attributename= 'imforbiddenwordlist'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_412_FUCK'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','2')
    
  
    --attributeoption  'imforbiddenwordlist'
    
    select @attributeid=attributeid from attribute
    where attributename= 'imforbiddenwordlist'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_412_DICK'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','3')
    
  
    --attributeoption  'gayscenemask'
    
    select @attributeid=attributeid from attribute
    where attributename= 'gayscenemask'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_438_ACTIVIST'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','10')
    
  
    --attributeoption  'gayscenemask'
    
    select @attributeid=attributeid from attribute
    where attributename= 'gayscenemask'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_438_BEAR'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','20')
    
  
    --attributeoption  'gayscenemask'
    
    select @attributeid=attributeid from attribute
    where attributename= 'gayscenemask'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_438_BLUE_COLLAR'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','30')
    
  
    --attributeoption  'gayscenemask'
    
    select @attributeid=attributeid from attribute
    where attributename= 'gayscenemask'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_438_CIRCUIT_PARTY'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','40')
    
  
    --attributeoption  'gayscenemask'
    
    select @attributeid=attributeid from attribute
    where attributename= 'gayscenemask'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_438_COUNTRYWESTERN'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','50')
    
  
    --attributeoption  'gayscenemask'
    
    select @attributeid=attributeid from attribute
    where attributename= 'gayscenemask'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_438_DRAG'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','60')
    
  
    --attributeoption  'gayscenemask'
    
    select @attributeid=attributeid from attribute
    where attributename= 'gayscenemask'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_438_GYM_BUNNY'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','70')
    
  
    --attributeoption  'gayscenemask'
    
    select @attributeid=attributeid from attribute
    where attributename= 'gayscenemask'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_438_HIPPIEGRANOLA'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','80')
    
  
    --attributeoption  'gayscenemask'
    
    select @attributeid=attributeid from attribute
    where attributename= 'gayscenemask'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_438_HOMEBODY'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','90')
    
  
    --attributeoption  'gayscenemask'
    
    select @attributeid=attributeid from attribute
    where attributename= 'gayscenemask'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_438_JOCKSPORTY'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','100')
    
  
    --attributeoption  'gayscenemask'
    
    select @attributeid=attributeid from attribute
    where attributename= 'gayscenemask'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_438_LEATHER'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','110')
    
  
    --attributeoption  'gayscenemask'
    
    select @attributeid=attributeid from attribute
    where attributename= 'gayscenemask'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_438_LIPSTICK_LESBIAN'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','120')
    
  
    --attributeoption  'gayscenemask'
    
    select @attributeid=attributeid from attribute
    where attributename= 'gayscenemask'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_438_MILITARYCOP'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','130')
    
  
    --attributeoption  'gayscenemask'
    
    select @attributeid=attributeid from attribute
    where attributename= 'gayscenemask'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_438_PREPPY'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','140')
    
  
    --attributeoption  'gayscenemask'
    
    select @attributeid=attributeid from attribute
    where attributename= 'gayscenemask'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_438_PROFESSIONAL'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','150')
    
  
    --attributeoption  'gayscenemask'
    
    select @attributeid=attributeid from attribute
    where attributename= 'gayscenemask'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_438_WOMYN'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','160')
    
  
    --attributeoption  'desiredjdatereligion'
    
    select @attributeid=attributeid from attribute
    where attributename= 'desiredjdatereligion'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_382_RELIGIOUS'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','0')
    
  
    --attributeoption  'desiredjdatereligion'
    
    select @attributeid=attributeid from attribute
    where attributename= 'desiredjdatereligion'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_382_REFORM'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','1')
    
  
    --attributeoption  'desiredjdatereligion'
    
    select @attributeid=attributeid from attribute
    where attributename= 'desiredjdatereligion'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_382_CONSERVATIVE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','2')
    
  
    --attributeoption  'desiredjdatereligion'
    
    select @attributeid=attributeid from attribute
    where attributename= 'desiredjdatereligion'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_382_ORTHODOXBAAL_TESHUVA'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','4')
    
  
    --attributeoption  'desiredjdatereligion'
    
    select @attributeid=attributeid from attribute
    where attributename= 'desiredjdatereligion'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_382_MODERN_ORTHODOX'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','5')
    
  
    --attributeoption  'desiredjdatereligion'
    
    select @attributeid=attributeid from attribute
    where attributename= 'desiredjdatereligion'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_382_TRADITIONAL'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','6')
    
  
    --attributeoption  'desiredjdatereligion'
    
    select @attributeid=attributeid from attribute
    where attributename= 'desiredjdatereligion'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_382_CONSERVADOX'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','7')
    
  
    --attributeoption  'desiredjdatereligion'
    
    select @attributeid=attributeid from attribute
    where attributename= 'desiredjdatereligion'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_382_HASSIDIC'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','8')
    
  
    --attributeoption  'desiredjdatereligion'
    
    select @attributeid=attributeid from attribute
    where attributename= 'desiredjdatereligion'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_382_RECONSTRUCTIONIST'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','9')
    
  
    --attributeoption  'desiredjdatereligion'
    
    select @attributeid=attributeid from attribute
    where attributename= 'desiredjdatereligion'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_382_ANOTHER_STREAM'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','10')
    
  
    --attributeoption  'desiredjdatereligion'
    
    select @attributeid=attributeid from attribute
    where attributename= 'desiredjdatereligion'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_382_SECULAR'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','11')
    
  
    --attributeoption  'jdatereligion'
    
    select @attributeid=attributeid from attribute
    where attributename= 'jdatereligion'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_364_RELIGIOUS'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','0')
    
  
    --attributeoption  'jdatereligion'
    
    select @attributeid=attributeid from attribute
    where attributename= 'jdatereligion'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_BLANK_78782'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','1')
    
  
    --attributeoption  'jdatereligion'
    
    select @attributeid=attributeid from attribute
    where attributename= 'jdatereligion'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_364_REFORM'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','2')
    
  
    --attributeoption  'jdatereligion'
    
    select @attributeid=attributeid from attribute
    where attributename= 'jdatereligion'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_364_CONSERVATIVE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','3')
    
  
    --attributeoption  'jdatereligion'
    
    select @attributeid=attributeid from attribute
    where attributename= 'jdatereligion'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_364_ORTHODOX_FRUM_FROM_BIRTH'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','4')
    
  
    --attributeoption  'jdatereligion'
    
    select @attributeid=attributeid from attribute
    where attributename= 'jdatereligion'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_364_ORTHODOXBAAL_TESHUVA'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','5')
    
  
    --attributeoption  'jdatereligion'
    
    select @attributeid=attributeid from attribute
    where attributename= 'jdatereligion'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_364_MODERN_ORTHODOX'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','6')
    
  
    --attributeoption  'jdatereligion'
    
    select @attributeid=attributeid from attribute
    where attributename= 'jdatereligion'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_364_TRADITIONAL'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','7')
    
  
    --attributeoption  'jdatereligion'
    
    select @attributeid=attributeid from attribute
    where attributename= 'jdatereligion'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_364_CONSERVADOX'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','8')
    
  
    --attributeoption  'jdatereligion'
    
    select @attributeid=attributeid from attribute
    where attributename= 'jdatereligion'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_364_HASSIDIC'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','9')
    
  
    --attributeoption  'jdatereligion'
    
    select @attributeid=attributeid from attribute
    where attributename= 'jdatereligion'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_364_RECONSTRUCTIONIST'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','10')
    
  
    --attributeoption  'jdatereligion'
    
    select @attributeid=attributeid from attribute
    where attributename= 'jdatereligion'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_364_ANOTHER_STREAM'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','11')
    
  
    --attributeoption  'jdatereligion'
    
    select @attributeid=attributeid from attribute
    where attributename= 'jdatereligion'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_364_SECULAR'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','12')
    
  
    --attributeoption  'albummemberstatus'
    
    select @attributeid=attributeid from attribute
    where attributename= 'albummemberstatus'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_220_ALL_MEMBERS'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','1')
    
  
    --attributeoption  'albummemberstatus'
    
    select @attributeid=attributeid from attribute
    where attributename= 'albummemberstatus'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_220_AWAITING_APPROVAL'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','2')
    
  
    --attributeoption  'albummemberstatus'
    
    select @attributeid=attributeid from attribute
    where attributename= 'albummemberstatus'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_220_APPROVED'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','3')
    
  
    --attributeoption  'albummemberstatus'
    
    select @attributeid=attributeid from attribute
    where attributename= 'albummemberstatus'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_220_DENIED'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','4')
    
  
    --attributeoption  'zodiac'
    
    select @attributeid=attributeid from attribute
    where attributename= 'zodiac'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_BLANK_79066'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','1')
    
  
    --attributeoption  'zodiac'
    
    select @attributeid=attributeid from attribute
    where attributename= 'zodiac'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_94_ARIES'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','30')
    
  
    --attributeoption  'zodiac'
    
    select @attributeid=attributeid from attribute
    where attributename= 'zodiac'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_94_AQUARIUS'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','40')
    
  
    --attributeoption  'zodiac'
    
    select @attributeid=attributeid from attribute
    where attributename= 'zodiac'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_94_CANCER'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','50')
    
  
    --attributeoption  'zodiac'
    
    select @attributeid=attributeid from attribute
    where attributename= 'zodiac'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_94_CAPRICORN'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','60')
    
  
    --attributeoption  'zodiac'
    
    select @attributeid=attributeid from attribute
    where attributename= 'zodiac'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_94_GEMINI'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','70')
    
  
    --attributeoption  'zodiac'
    
    select @attributeid=attributeid from attribute
    where attributename= 'zodiac'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_94_LEO'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','80')
    
  
    --attributeoption  'zodiac'
    
    select @attributeid=attributeid from attribute
    where attributename= 'zodiac'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_94_LIBRA'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','90')
    
  
    --attributeoption  'zodiac'
    
    select @attributeid=attributeid from attribute
    where attributename= 'zodiac'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_94_PISCES'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','100')
    
  
    --attributeoption  'zodiac'
    
    select @attributeid=attributeid from attribute
    where attributename= 'zodiac'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_94_SAGITTARIUS'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','110')
    
  
    --attributeoption  'zodiac'
    
    select @attributeid=attributeid from attribute
    where attributename= 'zodiac'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_94_SCORPIO'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','120')
    
  
    --attributeoption  'zodiac'
    
    select @attributeid=attributeid from attribute
    where attributename= 'zodiac'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_94_TAURUS'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','130')
    
  
    --attributeoption  'zodiac'
    
    select @attributeid=attributeid from attribute
    where attributename= 'zodiac'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_94_VIRGO'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','140')
    
  
    --attributeoption  'zodiac'
    
    select @attributeid=attributeid from attribute
    where attributename= 'zodiac'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_94_I_DONT_BELIEVE_IN_IT'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','150')
    
  
    --attributeoption  'selfterminatereasontypegood'
    
    select @attributeid=attributeid from attribute
    where attributename= 'selfterminatereasontypegood'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_288_FOUND_MY_SOUL_MATE_ON_THE_SITE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','1')
    
  
    --attributeoption  'selfterminatereasontypegood'
    
    select @attributeid=attributeid from attribute
    where attributename= 'selfterminatereasontypegood'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_288_FOUND_MY_SOUL_MATE_ON_MY_OWN'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','2')
    
  
    --attributeoption  'selfterminatereasontypegood'
    
    select @attributeid=attributeid from attribute
    where attributename= 'selfterminatereasontypegood'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_288_TOO_MANY_PEOPLE_ARE_CONTACTING_ME'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','3')
    
  
    --attributeoption  'selfterminatereasontypegood'
    
    select @attributeid=attributeid from attribute
    where attributename= 'selfterminatereasontypegood'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_288_GIVING_UP__COULDNT_FIND_A_SOUL_MATE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','4')
    
  
    --attributeoption  'selfterminatereasontypegood'
    
    select @attributeid=attributeid from attribute
    where attributename= 'selfterminatereasontypegood'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_288_NOBODY_CONTACTS_ME'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','5')
    
  
    --attributeoption  'selfterminatereasontypegood'
    
    select @attributeid=attributeid from attribute
    where attributename= 'selfterminatereasontypegood'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_288_NOBODY_REPLIES_TO_ME'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','6')
    
  
    --attributeoption  'selfterminatereasontypegood'
    
    select @attributeid=attributeid from attribute
    where attributename= 'selfterminatereasontypegood'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_288_I_DIDNT_LIKE_THE_MATCHES_I_GOT'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','7')
    
  
    --attributeoption  'selfterminatereasontypegood'
    
    select @attributeid=attributeid from attribute
    where attributename= 'selfterminatereasontypegood'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_288_TOO_EXPENSIVE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','8')
    
  
    --attributeoption  'educationlevel'
    
    select @attributeid=attributeid from attribute
    where attributename= 'educationlevel'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_BLANK_75972'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','10')
    
  
    --attributeoption  'educationlevel'
    
    select @attributeid=attributeid from attribute
    where attributename= 'educationlevel'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_89_ELEMENTARY'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','20')
    
  
    --attributeoption  'educationlevel'
    
    select @attributeid=attributeid from attribute
    where attributename= 'educationlevel'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_89_HIGH_SCHOOL'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','30')
    
  
    --attributeoption  'educationlevel'
    
    select @attributeid=attributeid from attribute
    where attributename= 'educationlevel'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_89_SOME_COLLEGE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','40')
    
  
    --attributeoption  'educationlevel'
    
    select @attributeid=attributeid from attribute
    where attributename= 'educationlevel'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_89_ASSOCIATES_DEGREE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','50')
    
  
    --attributeoption  'educationlevel'
    
    select @attributeid=attributeid from attribute
    where attributename= 'educationlevel'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_89_BACHELORS_DEGREE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','60')
    
  
    --attributeoption  'educationlevel'
    
    select @attributeid=attributeid from attribute
    where attributename= 'educationlevel'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_89_MASTERS_DEGREE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','70')
    
  
    --attributeoption  'educationlevel'
    
    select @attributeid=attributeid from attribute
    where attributename= 'educationlevel'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_89_DOCTORATE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','80')
    
  
    --attributeoption  'educationlevel'
    
    select @attributeid=attributeid from attribute
    where attributename= 'educationlevel'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_89_PHDPOSTDOCTORAL'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','90')
    
  
    --attributeoption  'educationlevel'
    
    select @attributeid=attributeid from attribute
    where attributename= 'educationlevel'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_89_PROFESSIONAL_DEGREE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','100')
    
  
    --attributeoption  'educationlevel'
    
    select @attributeid=attributeid from attribute
    where attributename= 'educationlevel'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_89_GRADUATE_STUDENT'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','110')
    
  
    --attributeoption  'educationlevel'
    
    select @attributeid=attributeid from attribute
    where attributename= 'educationlevel'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_89_ILL_TELL_YOU_LATER'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','120')
    
  
    --attributeoption  'childrencount'
    
    select @attributeid=attributeid from attribute
    where attributename= 'childrencount'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_BLANK_370'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','0')
    
  
    --attributeoption  'childrencount'
    
    select @attributeid=attributeid from attribute
    where attributename= 'childrencount'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_370_NONE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','1')
    
  
    --attributeoption  'childrencount'
    
    select @attributeid=attributeid from attribute
    where attributename= 'childrencount'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_370_1'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','2')
    
  
    --attributeoption  'childrencount'
    
    select @attributeid=attributeid from attribute
    where attributename= 'childrencount'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_370_2'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','3')
    
  
    --attributeoption  'childrencount'
    
    select @attributeid=attributeid from attribute
    where attributename= 'childrencount'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_370_3_OR_MORE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','4')
    
  
    --attributeoption  'chatrooms'
    
    select @attributeid=attributeid from attribute
    where attributename= 'chatrooms'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_214_MENS_LOUNGE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','1')
    
  
    --attributeoption  'chatrooms'
    
    select @attributeid=attributeid from attribute
    where attributename= 'chatrooms'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_214_20S_ROOM'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','2')
    
  
    --attributeoption  'chatrooms'
    
    select @attributeid=attributeid from attribute
    where attributename= 'chatrooms'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_214_30S_ROOM'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','3')
    
  
    --attributeoption  'chatrooms'
    
    select @attributeid=attributeid from attribute
    where attributename= 'chatrooms'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_214_40S_ROOM'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','4')
    
  
    --attributeoption  'chatrooms'
    
    select @attributeid=attributeid from attribute
    where attributename= 'chatrooms'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_214_50S_ROOM'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','5')
    
  
    --attributeoption  'chatrooms'
    
    select @attributeid=attributeid from attribute
    where attributename= 'chatrooms'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_214_60S_ROOM'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','6')
    
  
    --attributeoption  'chatrooms'
    
    select @attributeid=attributeid from attribute
    where attributename= 'chatrooms'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_214_70S_ROOM'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','7')
    
  
    --attributeoption  'smokinghabits'
    
    select @attributeid=attributeid from attribute
    where attributename= 'smokinghabits'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_BLANK_74260'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','1')
    
  
    --attributeoption  'smokinghabits'
    
    select @attributeid=attributeid from attribute
    where attributename= 'smokinghabits'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_92_NONSMOKER'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','2')
    
  
    --attributeoption  'smokinghabits'
    
    select @attributeid=attributeid from attribute
    where attributename= 'smokinghabits'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_92_OCCASIONAL_SMOKER'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','3')
    
  
    --attributeoption  'smokinghabits'
    
    select @attributeid=attributeid from attribute
    where attributename= 'smokinghabits'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_92_SMOKES_REGULARLY'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','4')
    
  
    --attributeoption  'smokinghabits'
    
    select @attributeid=attributeid from attribute
    where attributename= 'smokinghabits'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_92_TRYING_TO_QUIT'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','5')
    
  
    --attributeoption  'searchorderby'
    
    select @attributeid=attributeid from attribute
    where attributename= 'searchorderby'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_476_LAST_TO_REGISTER'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','1')
    
  
    --attributeoption  'searchorderby'
    
    select @attributeid=attributeid from attribute
    where attributename= 'searchorderby'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_476_LAST_TO_LOGON'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','2')
    
  
    --attributeoption  'searchorderby'
    
    select @attributeid=attributeid from attribute
    where attributename= 'searchorderby'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_476_PROXIMITY'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','3')
    
  
    --attributeoption  'searchorderby'
    
    select @attributeid=attributeid from attribute
    where attributename= 'searchorderby'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_476_MOST_POPULAR_MEMBERS'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','4')
    
  
    --attributeoption  'hidemask'
    
    select @attributeid=attributeid from attribute
    where attributename= 'hidemask'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_394_HIDE_ME_IN_SEARCH'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','1')
    
  
    --attributeoption  'hidemask'
    
    select @attributeid=attributeid from attribute
    where attributename= 'hidemask'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_394_HIDE_ME_IN_HOT_LISTS'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','2')
    
  
    --attributeoption  'hidemask'
    
    select @attributeid=attributeid from attribute
    where attributename= 'hidemask'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_394_HIDE_ME_IN_MEMBERS_ONLINE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','3')
    
  
    --attributeoption  'hidemask'
    
    select @attributeid=attributeid from attribute
    where attributename= 'hidemask'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_394_HIDE_MY_PHOTOS'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','4')
    
  
    --attributeoption  'music'
    
    select @attributeid=attributeid from attribute
    where attributename= 'music'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_78_ALTERNATIVE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','10')
    
  
    --attributeoption  'music'
    
    select @attributeid=attributeid from attribute
    where attributename= 'music'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_78_BIG_BANDSWING'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','20')
    
  
    --attributeoption  'music'
    
    select @attributeid=attributeid from attribute
    where attributename= 'music'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_78_BLUES'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','30')
    
  
    --attributeoption  'music'
    
    select @attributeid=attributeid from attribute
    where attributename= 'music'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_78_CHRISTIAN__GOSPEL'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','40')
    
  
    --attributeoption  'music'
    
    select @attributeid=attributeid from attribute
    where attributename= 'music'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_78_CLASSIC_ROCK_N_ROLL'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','50')
    
  
    --attributeoption  'music'
    
    select @attributeid=attributeid from attribute
    where attributename= 'music'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_78_CLASSICAL'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','60')
    
  
    --attributeoption  'music'
    
    select @attributeid=attributeid from attribute
    where attributename= 'music'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_78_COUNTRY'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','70')
    
  
    --attributeoption  'music'
    
    select @attributeid=attributeid from attribute
    where attributename= 'music'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_78_DANCEELECTRONICA'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','80')
    
  
    --attributeoption  'music'
    
    select @attributeid=attributeid from attribute
    where attributename= 'music'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_78_DISCO'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','90')
    
  
    --attributeoption  'music'
    
    select @attributeid=attributeid from attribute
    where attributename= 'music'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_78_EASY_LISTENING'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','100')
    
  
    --attributeoption  'music'
    
    select @attributeid=attributeid from attribute
    where attributename= 'music'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_78_FOLK'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','110')
    
  
    --attributeoption  'music'
    
    select @attributeid=attributeid from attribute
    where attributename= 'music'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_78_HARD_ROCK__METAL'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','120')
    
  
    --attributeoption  'music'
    
    select @attributeid=attributeid from attribute
    where attributename= 'music'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_78_JAZZ'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','130')
    
  
    --attributeoption  'music'
    
    select @attributeid=attributeid from attribute
    where attributename= 'music'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_78_LATIN'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','140')
    
  
    --attributeoption  'music'
    
    select @attributeid=attributeid from attribute
    where attributename= 'music'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_78_MODERN_ROCK_N_ROLL'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','150')
    
  
    --attributeoption  'music'
    
    select @attributeid=attributeid from attribute
    where attributename= 'music'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_78_NEW_AGE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','160')
    
  
    --attributeoption  'music'
    
    select @attributeid=attributeid from attribute
    where attributename= 'music'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_78_OLDIES'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','170')
    
  
    --attributeoption  'music'
    
    select @attributeid=attributeid from attribute
    where attributename= 'music'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_78_OPERA'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','180')
    
  
    --attributeoption  'music'
    
    select @attributeid=attributeid from attribute
    where attributename= 'music'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_78_POPTOP_40'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','190')
    
  
    --attributeoption  'music'
    
    select @attributeid=attributeid from attribute
    where attributename= 'music'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_78_RAPHIP_HOP'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','200')
    
  
    --attributeoption  'music'
    
    select @attributeid=attributeid from attribute
    where attributename= 'music'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_78_REGGAE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','210')
    
  
    --attributeoption  'music'
    
    select @attributeid=attributeid from attribute
    where attributename= 'music'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_78_SHOWTUNES'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','220')
    
  
    --attributeoption  'music'
    
    select @attributeid=attributeid from attribute
    where attributename= 'music'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_78_SOULRB'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','230')
    
  
    --attributeoption  'music'
    
    select @attributeid=attributeid from attribute
    where attributename= 'music'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_78_WORLD_MUSICETHNIC'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','240')
    
  
    --attributeoption  'music'
    
    select @attributeid=attributeid from attribute
    where attributename= 'music'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_78_PUNK'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','250')
    
  
    --attributeoption  'music'
    
    select @attributeid=attributeid from attribute
    where attributename= 'music'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_78_INSTRUMENTAL'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','260')
    
  
    --attributeoption  'music'
    
    select @attributeid=attributeid from attribute
    where attributename= 'music'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_78_SOUNDTRACKS'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','270')
    
  
    --attributeoption  'music'
    
    select @attributeid=attributeid from attribute
    where attributename= 'music'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_78_ACOUSTIC'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','280')
    
  
    --attributeoption  'music'
    
    select @attributeid=attributeid from attribute
    where attributename= 'music'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_78_INDIE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','290')
    
  
    --attributeoption  'music'
    
    select @attributeid=attributeid from attribute
    where attributename= 'music'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_78_OTHER'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','300')
    
  
    --attributeoption  'desireddrinkinghabits'
    
    select @attributeid=attributeid from attribute
    where attributename= 'desireddrinkinghabits'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_388_NEVER'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','1')
    
  
    --attributeoption  'desireddrinkinghabits'
    
    select @attributeid=attributeid from attribute
    where attributename= 'desireddrinkinghabits'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_388_SOCIALLY'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','2')
    
  
    --attributeoption  'desireddrinkinghabits'
    
    select @attributeid=attributeid from attribute
    where attributename= 'desireddrinkinghabits'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_388_OCCASIONALLY'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','3')
    
  
    --attributeoption  'desireddrinkinghabits'
    
    select @attributeid=attributeid from attribute
    where attributename= 'desireddrinkinghabits'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_388_FREQUENTLY'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','4')
    
  
    --attributeoption  'custody'
    
    select @attributeid=attributeid from attribute
    where attributename= 'custody'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_BLANK_74337'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','1')
    
  
    --attributeoption  'custody'
    
    select @attributeid=attributeid from attribute
    where attributename= 'custody'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_107_I_HAVE_NO_KIDS'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','2')
    
  
    --attributeoption  'custody'
    
    select @attributeid=attributeid from attribute
    where attributename= 'custody'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_107_THEY_ARE_FAR_AWAY'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','3')
    
  
    --attributeoption  'custody'
    
    select @attributeid=attributeid from attribute
    where attributename= 'custody'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_107_SOMETIMES'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','4')
    
  
    --attributeoption  'custody'
    
    select @attributeid=attributeid from attribute
    where attributename= 'custody'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_107_DONT_LIVE_WITH_ME'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','5')
    
  
    --attributeoption  'custody'
    
    select @attributeid=attributeid from attribute
    where attributename= 'custody'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_107_SOME_LIVE_WITH_ME'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','6')
    
  
    --attributeoption  'custody'
    
    select @attributeid=attributeid from attribute
    where attributename= 'custody'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_107_WEEKENDS_ONLY'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','7')
    
  
    --attributeoption  'custody'
    
    select @attributeid=attributeid from attribute
    where attributename= 'custody'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_107_LIVE_WITH_ME'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','8')
    
  
    --attributeoption  'selfterminatereasontypebad'
    
    select @attributeid=attributeid from attribute
    where attributename= 'selfterminatereasontypebad'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_290_JUST_TESTING_THE_SERVICE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','1')
    
  
    --attributeoption  'selfterminatereasontypebad'
    
    select @attributeid=attributeid from attribute
    where attributename= 'selfterminatereasontypebad'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_290_DONT_HAVE_TIME_TO_DATE_RIGHT_NOW'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','2')
    
  
    --attributeoption  'selfterminatereasontypebad'
    
    select @attributeid=attributeid from attribute
    where attributename= 'selfterminatereasontypebad'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_290_UNABLE_TO_UPLOAD_A_PHOTO'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','3')
    
  
    --attributeoption  'selfterminatereasontypebad'
    
    select @attributeid=attributeid from attribute
    where attributename= 'selfterminatereasontypebad'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_290_DIFFICULTY_USING_THE_SITE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','4')
    
  
    --attributeoption  'selfterminatereasontypebad'
    
    select @attributeid=attributeid from attribute
    where attributename= 'selfterminatereasontypebad'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_290_TAKING_A_BREAK__VACATION'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','5')
    
  
    --attributeoption  'selfterminatereasontypebad'
    
    select @attributeid=attributeid from attribute
    where attributename= 'selfterminatereasontypebad'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_290_TOO_FEW_MATCHES_IN_MY_AREA'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','6')
    
  
    --attributeoption  'selfterminatereasontypebad'
    
    select @attributeid=attributeid from attribute
    where attributename= 'selfterminatereasontypebad'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_290_CUSTOMER_SERVICE_ISSUES'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','7')
    
  
    --attributeoption  'selfterminatereasontypebad'
    
    select @attributeid=attributeid from attribute
    where attributename= 'selfterminatereasontypebad'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_290_GOING_TO_TRY_ANOTHER_SERVICE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','8')
    
  
    --attributeoption  'selfterminatereasontypebad'
    
    select @attributeid=attributeid from attribute
    where attributename= 'selfterminatereasontypebad'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_290_OTHER'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','9')
    
  
    --attributeoption  'gendermask'
    
    select @attributeid=attributeid from attribute
    where attributename= 'gendermask'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_BLANK_105416'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','1')
    
  
    --attributeoption  'gendermask'
    
    select @attributeid=attributeid from attribute
    where attributename= 'gendermask'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_69_MALE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','2')
    
  
    --attributeoption  'gendermask'
    
    select @attributeid=attributeid from attribute
    where attributename= 'gendermask'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_69_FEMALE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','3')
    
  
    --attributeoption  'gendermask'
    
    select @attributeid=attributeid from attribute
    where attributename= 'gendermask'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_69_SEEKING_MALE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','4')
    
  
    --attributeoption  'gendermask'
    
    select @attributeid=attributeid from attribute
    where attributename= 'gendermask'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_69_SEEKING_FEMALE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','5')
    
  
    --attributeoption  'outnesstype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'outnesstype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_BLANK_74256'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','1')
    
  
    --attributeoption  'outnesstype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'outnesstype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_91_COMPLETELY'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','2')
    
  
    --attributeoption  'outnesstype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'outnesstype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_91_OUT_TO_SOME_PEOPLE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','3')
    
  
    --attributeoption  'outnesstype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'outnesstype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_91_IN_THE_CLOSET'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','4')
    
  
    --attributeoption  'desirededucationlevel'
    
    select @attributeid=attributeid from attribute
    where attributename= 'desirededucationlevel'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_386_ELEMENTARY'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','5')
    
  
    --attributeoption  'desirededucationlevel'
    
    select @attributeid=attributeid from attribute
    where attributename= 'desirededucationlevel'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_386_HIGH_SCHOOL'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','10')
    
  
    --attributeoption  'desirededucationlevel'
    
    select @attributeid=attributeid from attribute
    where attributename= 'desirededucationlevel'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_386_SOME_COLLEGE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','20')
    
  
    --attributeoption  'desirededucationlevel'
    
    select @attributeid=attributeid from attribute
    where attributename= 'desirededucationlevel'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_386_ASSOCIATE_DEGREE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','30')
    
  
    --attributeoption  'desirededucationlevel'
    
    select @attributeid=attributeid from attribute
    where attributename= 'desirededucationlevel'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_386_BACHELORS_DEGREE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','40')
    
  
    --attributeoption  'desirededucationlevel'
    
    select @attributeid=attributeid from attribute
    where attributename= 'desirededucationlevel'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_386_MASTERS_DEGREE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','50')
    
  
    --attributeoption  'desirededucationlevel'
    
    select @attributeid=attributeid from attribute
    where attributename= 'desirededucationlevel'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_386_DOCTORATE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','60')
    
  
    --attributeoption  'activitylevel'
    
    select @attributeid=attributeid from attribute
    where attributename= 'activitylevel'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_BLANK_76020'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','1')
    
  
    --attributeoption  'activitylevel'
    
    select @attributeid=attributeid from attribute
    where attributename= 'activitylevel'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_149_NEVER_ACTIVE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','2')
    
  
    --attributeoption  'activitylevel'
    
    select @attributeid=attributeid from attribute
    where attributename= 'activitylevel'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_149_RARELY_ACTIVE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','3')
    
  
    --attributeoption  'activitylevel'
    
    select @attributeid=attributeid from attribute
    where attributename= 'activitylevel'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_149_SELECTED_ACTIVITIES'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','4')
    
  
    --attributeoption  'activitylevel'
    
    select @attributeid=attributeid from attribute
    where attributename= 'activitylevel'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_149_ACTIVE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','5')
    
  
    --attributeoption  'activitylevel'
    
    select @attributeid=attributeid from attribute
    where attributename= 'activitylevel'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_149_VERY_ACTIVE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','6')
    
  
    --attributeoption  'desiredsmokinghabits'
    
    select @attributeid=attributeid from attribute
    where attributename= 'desiredsmokinghabits'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_390_NONSMOKER'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','1')
    
  
    --attributeoption  'desiredsmokinghabits'
    
    select @attributeid=attributeid from attribute
    where attributename= 'desiredsmokinghabits'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_390_OCCASIONAL_SMOKER'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','2')
    
  
    --attributeoption  'desiredsmokinghabits'
    
    select @attributeid=attributeid from attribute
    where attributename= 'desiredsmokinghabits'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_390_SMOKES_REGULARLY'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','3')
    
  
    --attributeoption  'desiredsmokinghabits'
    
    select @attributeid=attributeid from attribute
    where attributename= 'desiredsmokinghabits'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_390_TRYING_TO_QUIT'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','4')
    
  
    --attributeoption  'ethnicity'
    
    select @attributeid=attributeid from attribute
    where attributename= 'ethnicity'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_BLANK_78514'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','0')
    
  
    --attributeoption  'ethnicity'
    
    select @attributeid=attributeid from attribute
    where attributename= 'ethnicity'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_88_AFRICAN_DESCENTBLACK'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','2')
    
  
    --attributeoption  'ethnicity'
    
    select @attributeid=attributeid from attribute
    where attributename= 'ethnicity'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_88_ASIAN'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','3')
    
  
    --attributeoption  'ethnicity'
    
    select @attributeid=attributeid from attribute
    where attributename= 'ethnicity'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_88_CAUCASIANWHITE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','4')
    
  
    --attributeoption  'ethnicity'
    
    select @attributeid=attributeid from attribute
    where attributename= 'ethnicity'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_88_HISPANIC'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','5')
    
  
    --attributeoption  'ethnicity'
    
    select @attributeid=attributeid from attribute
    where attributename= 'ethnicity'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_88_MIDDLE_EASTERN'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','6')
    
  
    --attributeoption  'ethnicity'
    
    select @attributeid=attributeid from attribute
    where attributename= 'ethnicity'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_88_MIXED_RACE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','7')
    
  
    --attributeoption  'ethnicity'
    
    select @attributeid=attributeid from attribute
    where attributename= 'ethnicity'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_88_NATIVE_AMERICAN'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','8')
    
  
    --attributeoption  'ethnicity'
    
    select @attributeid=attributeid from attribute
    where attributename= 'ethnicity'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_88_PACIFIC_ISLANDER'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','9')
    
  
    --attributeoption  'ethnicity'
    
    select @attributeid=attributeid from attribute
    where attributename= 'ethnicity'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_88_SOUTH_ASIANEAST_INDIAN'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','10')
    
  
    --attributeoption  'ethnicity'
    
    select @attributeid=attributeid from attribute
    where attributename= 'ethnicity'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_88_CARRIBEAN'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','12')
    
  
    --attributeoption  'bodyart'
    
    select @attributeid=attributeid from attribute
    where attributename= 'bodyart'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_BLANK_109690'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','1')
    
  
    --attributeoption  'bodyart'
    
    select @attributeid=attributeid from attribute
    where attributename= 'bodyart'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_434_ONE_OR_TWO'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','2')
    
  
    --attributeoption  'bodyart'
    
    select @attributeid=attributeid from attribute
    where attributename= 'bodyart'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_434_A_FEW_HERE_AND_THERE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','3')
    
  
    --attributeoption  'bodyart'
    
    select @attributeid=attributeid from attribute
    where attributename= 'bodyart'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_434_I_DONT_HAVE_ANY_BUT_WANT'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','4')
    
  
    --attributeoption  'bodyart'
    
    select @attributeid=attributeid from attribute
    where attributename= 'bodyart'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_434_NOT_FOR_ME'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','5')
    
  
    --attributeoption  'bodyart'
    
    select @attributeid=attributeid from attribute
    where attributename= 'bodyart'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_434_ASK_ME_IM_SHY'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','6')
    
  
    --attributeoption  'smscarrier'
    
    select @attributeid=attributeid from attribute
    where attributename= 'smscarrier'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_525_VERIZON_WIRELESS'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','1')
    
  
    --attributeoption  'telephonystatusmask'
    
    select @attributeid=attributeid from attribute
    where attributename= 'telephonystatusmask'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_254_ACCOUNTEXISTS'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','1')
    
  
    --attributeoption  'telephonystatusmask'
    
    select @attributeid=attributeid from attribute
    where attributename= 'telephonystatusmask'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_254_ACTIVE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','2')
    
  
    --attributeoption  'suspectwords'
    
    select @attributeid=attributeid from attribute
    where attributename= 'suspectwords'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_310_MORNING_WOOD'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','0')
    
  
    --attributeoption  'suspectwords'
    
    select @attributeid=attributeid from attribute
    where attributename= 'suspectwords'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_310_ASSHOLE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','1')
    
  
    --attributeoption  'suspectwords'
    
    select @attributeid=attributeid from attribute
    where attributename= 'suspectwords'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_310_DIGITS'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','2')
    
  
    --attributeoption  'suspectwords'
    
    select @attributeid=attributeid from attribute
    where attributename= 'suspectwords'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_310_ANUS'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','3')
    
  
    --attributeoption  'suspectwords'
    
    select @attributeid=attributeid from attribute
    where attributename= 'suspectwords'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_310_AOL_78020'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','4')
    
  
    --attributeoption  'suspectwords'
    
    select @attributeid=attributeid from attribute
    where attributename= 'suspectwords'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_310_BASTARD_78022'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','5')
    
  
    --attributeoption  'suspectwords'
    
    select @attributeid=attributeid from attribute
    where attributename= 'suspectwords'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_310_BITCH'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','6')
    
  
    --attributeoption  'suspectwords'
    
    select @attributeid=attributeid from attribute
    where attributename= 'suspectwords'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_310_BLOWJOB'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','7')
    
  
    --attributeoption  'suspectwords'
    
    select @attributeid=attributeid from attribute
    where attributename= 'suspectwords'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_310_BOOBS'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','8')
    
  
    --attributeoption  'suspectwords'
    
    select @attributeid=attributeid from attribute
    where attributename= 'suspectwords'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_310_BUTT[^EO]'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','9')
    
  
    --attributeoption  'suspectwords'
    
    select @attributeid=attributeid from attribute
    where attributename= 'suspectwords'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_310_CLIT_78032'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','10')
    
  
    --attributeoption  'suspectwords'
    
    select @attributeid=attributeid from attribute
    where attributename= 'suspectwords'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_310_COCK[^T]'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','11')
    
  
    --attributeoption  'suspectwords'
    
    select @attributeid=attributeid from attribute
    where attributename= 'suspectwords'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_310_CUNT_78036'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','12')
    
  
    --attributeoption  'suspectwords'
    
    select @attributeid=attributeid from attribute
    where attributename= 'suspectwords'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_310_FUCK_78040'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','14')
    
  
    --attributeoption  'suspectwords'
    
    select @attributeid=attributeid from attribute
    where attributename= 'suspectwords'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_310_GANGBANG'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','15')
    
  
    --attributeoption  'suspectwords'
    
    select @attributeid=attributeid from attribute
    where attributename= 'suspectwords'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_310_HOTMAIL'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','16')
    
  
    --attributeoption  'suspectwords'
    
    select @attributeid=attributeid from attribute
    where attributename= 'suspectwords'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_310_HTTP'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','17')
    
  
    --attributeoption  'suspectwords'
    
    select @attributeid=attributeid from attribute
    where attributename= 'suspectwords'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_310_ICQ_78048'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','18')
    
  
    --attributeoption  'suspectwords'
    
    select @attributeid=attributeid from attribute
    where attributename= 'suspectwords'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_310_JUNO_78050'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','19')
    
  
    --attributeoption  'suspectwords'
    
    select @attributeid=attributeid from attribute
    where attributename= 'suspectwords'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_310_NIGGER'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','20')
    
  
    --attributeoption  'suspectwords'
    
    select @attributeid=attributeid from attribute
    where attributename= 'suspectwords'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_310_NIPPLE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','21')
    
  
    --attributeoption  'suspectwords'
    
    select @attributeid=attributeid from attribute
    where attributename= 'suspectwords'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_310_ORGASM'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','22')
    
  
    --attributeoption  'suspectwords'
    
    select @attributeid=attributeid from attribute
    where attributename= 'suspectwords'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_310_PENIS'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','23')
    
  
    --attributeoption  'suspectwords'
    
    select @attributeid=attributeid from attribute
    where attributename= 'suspectwords'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_310_PISS'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','24')
    
  
    --attributeoption  'suspectwords'
    
    select @attributeid=attributeid from attribute
    where attributename= 'suspectwords'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_310_PRICK'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','25')
    
  
    --attributeoption  'suspectwords'
    
    select @attributeid=attributeid from attribute
    where attributename= 'suspectwords'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_310_PUSSY_78064'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','26')
    
  
    --attributeoption  'suspectwords'
    
    select @attributeid=attributeid from attribute
    where attributename= 'suspectwords'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_310_SHIT_78066'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','27')
    
  
    --attributeoption  'suspectwords'
    
    select @attributeid=attributeid from attribute
    where attributename= 'suspectwords'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_310_SLUT_78068'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','28')
    
  
    --attributeoption  'suspectwords'
    
    select @attributeid=attributeid from attribute
    where attributename= 'suspectwords'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_310_TITS_78070'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','29')
    
  
    --attributeoption  'suspectwords'
    
    select @attributeid=attributeid from attribute
    where attributename= 'suspectwords'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_310_VAGINA'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','30')
    
  
    --attributeoption  'suspectwords'
    
    select @attributeid=attributeid from attribute
    where attributename= 'suspectwords'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_310_WWW'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','31')
    
  
    --attributeoption  'suspectwords'
    
    select @attributeid=attributeid from attribute
    where attributename= 'suspectwords'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_310_YAHOO_78076'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','32')
    
  
    --attributeoption  'suspectwords'
    
    select @attributeid=attributeid from attribute
    where attributename= 'suspectwords'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_310_CALL_ME_78078'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','33')
    
  
    --attributeoption  'suspectwords'
    
    select @attributeid=attributeid from attribute
    where attributename= 'suspectwords'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_310_EMAIL_ME'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','34')
    
  
    --attributeoption  'suspectwords'
    
    select @attributeid=attributeid from attribute
    where attributename= 'suspectwords'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_310_CUM_78082'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','35')
    
  
    --attributeoption  'suspectwords'
    
    select @attributeid=attributeid from attribute
    where attributename= 'suspectwords'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_310_SUCK_78084'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','36')
    
  
    --attributeoption  'suspectwords'
    
    select @attributeid=attributeid from attribute
    where attributename= 'suspectwords'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_310_WET'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','37')
    
  
    --attributeoption  'suspectwords'
    
    select @attributeid=attributeid from attribute
    where attributename= 'suspectwords'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_310_KKK'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','38')
    
  
    --attributeoption  'suspectwords'
    
    select @attributeid=attributeid from attribute
    where attributename= 'suspectwords'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_310_JDATE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','39')
    
  
    --attributeoption  'suspectwords'
    
    select @attributeid=attributeid from attribute
    where attributename= 'suspectwords'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_310_MATCHNET'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','40')
    
  
    --attributeoption  'suspectwords'
    
    select @attributeid=attributeid from attribute
    where attributename= 'suspectwords'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_310_CUNNILINGUS'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','42')
    
  
    --attributeoption  'suspectwords'
    
    select @attributeid=attributeid from attribute
    where attributename= 'suspectwords'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_310_DAMN_78098'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','43')
    
  
    --attributeoption  'suspectwords'
    
    select @attributeid=attributeid from attribute
    where attributename= 'suspectwords'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_310_DICK_78100'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','44')
    
  
    --attributeoption  'suspectwords'
    
    select @attributeid=attributeid from attribute
    where attributename= 'suspectwords'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_310_FAG'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','45')
    
  
    --attributeoption  'suspectwords'
    
    select @attributeid=attributeid from attribute
    where attributename= 'suspectwords'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_310_FELLATIO'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','46')
    
  
    --attributeoption  'suspectwords'
    
    select @attributeid=attributeid from attribute
    where attributename= 'suspectwords'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_310_GENITALIA'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','47')
    
  
    --attributeoption  'suspectwords'
    
    select @attributeid=attributeid from attribute
    where attributename= 'suspectwords'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_310_HORNY_78108'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','48')
    
  
    --attributeoption  'suspectwords'
    
    select @attributeid=attributeid from attribute
    where attributename= 'suspectwords'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_310_MASTURBATE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','49')
    
  
    --attributeoption  'suspectwords'
    
    select @attributeid=attributeid from attribute
    where attributename= 'suspectwords'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_310_SEX'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','50')
    
  
    --attributeoption  'suspectwords'
    
    select @attributeid=attributeid from attribute
    where attributename= 'suspectwords'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_310_NAZI'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','51')
    
  
    --attributeoption  'suspectwords'
    
    select @attributeid=attributeid from attribute
    where attributename= 'suspectwords'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_310_AT'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','52')
    
  
    --attributeoption  'suspectwords'
    
    select @attributeid=attributeid from attribute
    where attributename= 'suspectwords'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_310_COM'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','53')
    
  
    --attributeoption  'suspectwords'
    
    select @attributeid=attributeid from attribute
    where attributename= 'suspectwords'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_310_DOT_78120'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','54')
    
  
    --attributeoption  'suspectwords'
    
    select @attributeid=attributeid from attribute
    where attributename= 'suspectwords'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_310_CO'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','55')
    
  
    --attributeoption  'suspectwords'
    
    select @attributeid=attributeid from attribute
    where attributename= 'suspectwords'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_310_NET_78124'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','56')
    
  
    --attributeoption  'suspectwords'
    
    select @attributeid=attributeid from attribute
    where attributename= 'suspectwords'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_310_YYY'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','57')
    
  
    --attributeoption  'suspectwords'
    
    select @attributeid=attributeid from attribute
    where attributename= 'suspectwords'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_310_XXX'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','58')
    
  
    --attributeoption  'suspectwords'
    
    select @attributeid=attributeid from attribute
    where attributename= 'suspectwords'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_310_ASS'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','59')
    
  
    --attributeoption  'suspectwords'
    
    select @attributeid=attributeid from attribute
    where attributename= 'suspectwords'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_310_AIM_78532'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','60')
    
  
    --attributeoption  'suspectwords'
    
    select @attributeid=attributeid from attribute
    where attributename= 'suspectwords'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_310_H_OT_M_A_I_L'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','61')
    
  
    --attributeoption  'suspectwords'
    
    select @attributeid=attributeid from attribute
    where attributename= 'suspectwords'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_310_YAHOOCAP'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','62')
    
  
    --attributeoption  'suspectwords'
    
    select @attributeid=attributeid from attribute
    where attributename= 'suspectwords'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_310_DOT_79142'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','63')
    
  
    --attributeoption  'suspectwords'
    
    select @attributeid=attributeid from attribute
    where attributename= 'suspectwords'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_310_EMAIL_79144'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','64')
    
  
    --attributeoption  'suspectwords'
    
    select @attributeid=attributeid from attribute
    where attributename= 'suspectwords'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_310_MESSAGE_79146'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','65')
    
  
    --attributeoption  'suspectwords'
    
    select @attributeid=attributeid from attribute
    where attributename= 'suspectwords'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_310_CONTACT_79148'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','66')
    
  
    --attributeoption  'suspectwords'
    
    select @attributeid=attributeid from attribute
    where attributename= 'suspectwords'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_310_HOT'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','67')
    
  
    --attributeoption  'suspectwords'
    
    select @attributeid=attributeid from attribute
    where attributename= 'suspectwords'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_310_Y_A_H_O_O'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','68')
    
  
    --attributeoption  'suspectwords'
    
    select @attributeid=attributeid from attribute
    where attributename= 'suspectwords'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_310_H_O_T_M_A_I_L'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','69')
    
  
    --attributeoption  'suspectwords'
    
    select @attributeid=attributeid from attribute
    where attributename= 'suspectwords'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_310_AOL_79156'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','70')
    
  
    --attributeoption  'suspectwords'
    
    select @attributeid=attributeid from attribute
    where attributename= 'suspectwords'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_310_INSTANT'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','71')
    
  
    --attributeoption  'suspectwords'
    
    select @attributeid=attributeid from attribute
    where attributename= 'suspectwords'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_310_MARRIED'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','72')
    
  
    --attributeoption  'suspectwords'
    
    select @attributeid=attributeid from attribute
    where attributename= 'suspectwords'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_310_NETSCAPE_1092'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','73')
    
  
    --attributeoption  'suspectwords'
    
    select @attributeid=attributeid from attribute
    where attributename= 'suspectwords'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_310_NETZERO_1094'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','74')
    
  
    --attributeoption  'suspectwords'
    
    select @attributeid=attributeid from attribute
    where attributename= 'suspectwords'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_310_MSN_1096'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','75')
    
  
    --attributeoption  'suspectwords'
    
    select @attributeid=attributeid from attribute
    where attributename= 'suspectwords'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_310_EARTHLINK'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','76')
    
  
    --attributeoption  'suspectwords'
    
    select @attributeid=attributeid from attribute
    where attributename= 'suspectwords'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_310_EXCITE_1100'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','77')
    
  
    --attributeoption  'suspectwords'
    
    select @attributeid=attributeid from attribute
    where attributename= 'suspectwords'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_310_LYCOS_1102'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','78')
    
  
    --attributeoption  'suspectwords'
    
    select @attributeid=attributeid from attribute
    where attributename= 'suspectwords'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_310_NET_1104'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','79')
    
  
    --attributeoption  'desiredmaritalstatus'
    
    select @attributeid=attributeid from attribute
    where attributename= 'desiredmaritalstatus'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_380_MARRIED'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','0')
    
  
    --attributeoption  'desiredmaritalstatus'
    
    select @attributeid=attributeid from attribute
    where attributename= 'desiredmaritalstatus'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_380_SINGLE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','1')
    
  
    --attributeoption  'desiredmaritalstatus'
    
    select @attributeid=attributeid from attribute
    where attributename= 'desiredmaritalstatus'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_380_DIVORCED'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','2')
    
  
    --attributeoption  'desiredmaritalstatus'
    
    select @attributeid=attributeid from attribute
    where attributename= 'desiredmaritalstatus'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_380_SEPARATED'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','3')
    
  
    --attributeoption  'desiredmaritalstatus'
    
    select @attributeid=attributeid from attribute
    where attributename= 'desiredmaritalstatus'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_380_WIDOWED'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','4')
    
  
    --attributeoption  'packagetype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'packagetype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_300_PUBLIC'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','1')
    
  
    --attributeoption  'packagetype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'packagetype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_300_DISCOUNTED'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','2')
    
  
    --attributeoption  'packagetype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'packagetype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_300_CLOSED'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','3')
    
  
    --attributeoption  'religion'
    
    select @attributeid=attributeid from attribute
    where attributename= 'religion'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_BLANK_74282'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','1')
    
  
    --attributeoption  'religion'
    
    select @attributeid=attributeid from attribute
    where attributename= 'religion'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_95_AGNOSTIC'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','2')
    
  
    --attributeoption  'religion'
    
    select @attributeid=attributeid from attribute
    where attributename= 'religion'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_95_ANGLICAN'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','3')
    
  
    --attributeoption  'religion'
    
    select @attributeid=attributeid from attribute
    where attributename= 'religion'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_95_ATHEIST'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','4')
    
  
    --attributeoption  'religion'
    
    select @attributeid=attributeid from attribute
    where attributename= 'religion'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_95_BAPTIST'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','5')
    
  
    --attributeoption  'religion'
    
    select @attributeid=attributeid from attribute
    where attributename= 'religion'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_95_BUDDHIST'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','6')
    
  
    --attributeoption  'religion'
    
    select @attributeid=attributeid from attribute
    where attributename= 'religion'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_95_CATHOLIC'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','7')
    
  
    --attributeoption  'religion'
    
    select @attributeid=attributeid from attribute
    where attributename= 'religion'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_95_CHRISTIAN'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','8')
    
  
    --attributeoption  'religion'
    
    select @attributeid=attributeid from attribute
    where attributename= 'religion'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_95_HINDU'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','9')
    
  
    --attributeoption  'religion'
    
    select @attributeid=attributeid from attribute
    where attributename= 'religion'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_95_JEWISH'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','10')
    
  
    --attributeoption  'religion'
    
    select @attributeid=attributeid from attribute
    where attributename= 'religion'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_95_LUTHERAN'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','11')
    
  
    --attributeoption  'religion'
    
    select @attributeid=attributeid from attribute
    where attributename= 'religion'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_95_METHODIST'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','12')
    
  
    --attributeoption  'religion'
    
    select @attributeid=attributeid from attribute
    where attributename= 'religion'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_95_MORMON'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','13')
    
  
    --attributeoption  'religion'
    
    select @attributeid=attributeid from attribute
    where attributename= 'religion'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_95_MUSLIM'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','14')
    
  
    --attributeoption  'religion'
    
    select @attributeid=attributeid from attribute
    where attributename= 'religion'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_95_NON_RELIGIOUS'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','15')
    
  
    --attributeoption  'religion'
    
    select @attributeid=attributeid from attribute
    where attributename= 'religion'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_95_OTHER'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','16')
    
  
    --attributeoption  'religion'
    
    select @attributeid=attributeid from attribute
    where attributename= 'religion'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_95_OTHER_CHRISTIAN'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','17')
    
  
    --attributeoption  'religion'
    
    select @attributeid=attributeid from attribute
    where attributename= 'religion'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_95_PAGAN'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','18')
    
  
    --attributeoption  'religion'
    
    select @attributeid=attributeid from attribute
    where attributename= 'religion'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_95_PRESBYTERIAN'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','19')
    
  
    --attributeoption  'religion'
    
    select @attributeid=attributeid from attribute
    where attributename= 'religion'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_95_PROTESTANT'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','20')
    
  
    --attributeoption  'religion'
    
    select @attributeid=attributeid from attribute
    where attributename= 'religion'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_95_QUAKER'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','21')
    
  
    --attributeoption  'religion'
    
    select @attributeid=attributeid from attribute
    where attributename= 'religion'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_95_SIKH'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','22')
    
  
    --attributeoption  'religion'
    
    select @attributeid=attributeid from attribute
    where attributename= 'religion'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_95_UNAFFILIATED'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','23')
    
  
    --attributeoption  'religion'
    
    select @attributeid=attributeid from attribute
    where attributename= 'religion'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_95_BAHAI'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','24')
    
  
    --attributeoption  'religion'
    
    select @attributeid=attributeid from attribute
    where attributename= 'religion'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_95_CONFUCIAN'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','25')
    
  
    --attributeoption  'religion'
    
    select @attributeid=attributeid from attribute
    where attributename= 'religion'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_95_TAOIST'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','26')
    
  
    --attributeoption  'religion'
    
    select @attributeid=attributeid from attribute
    where attributename= 'religion'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_95_UNITARIAN_UNIVERSALIST'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','27')
    
  
    --attributeoption  'religion'
    
    select @attributeid=attributeid from attribute
    where attributename= 'religion'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_95_INTERDENOMINATIONAL'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','28')
    
  
    --attributeoption  'governmentissuedid'
    
    select @attributeid=attributeid from attribute
    where attributename= 'governmentissuedid'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_206_MERCURY'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','1')
    
  
    --attributeoption  'governmentissuedid'
    
    select @attributeid=attributeid from attribute
    where attributename= 'governmentissuedid'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_206_VENUS'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','2')
    
  
    --attributeoption  'governmentissuedid'
    
    select @attributeid=attributeid from attribute
    where attributename= 'governmentissuedid'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_206_EARTH'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','3')
    
  
    --attributeoption  'governmentissuedid'
    
    select @attributeid=attributeid from attribute
    where attributename= 'governmentissuedid'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_206_MARS'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','4')
    
  
    --attributeoption  'governmentissuedid'
    
    select @attributeid=attributeid from attribute
    where attributename= 'governmentissuedid'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_206_SATURN'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','5')
    
  
    --attributeoption  'governmentissuedid'
    
    select @attributeid=attributeid from attribute
    where attributename= 'governmentissuedid'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_206_JUPITER'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','6')
    
  
    --attributeoption  'governmentissuedid'
    
    select @attributeid=attributeid from attribute
    where attributename= 'governmentissuedid'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_206_URANUS'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','7')
    
  
    --attributeoption  'governmentissuedid'
    
    select @attributeid=attributeid from attribute
    where attributename= 'governmentissuedid'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_206_NEPTUNE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','8')
    
  
    --attributeoption  'governmentissuedid'
    
    select @attributeid=attributeid from attribute
    where attributename= 'governmentissuedid'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_206_PLUTO'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','9')
    
  
    --attributeoption  'governmentissuedid'
    
    select @attributeid=attributeid from attribute
    where attributename= 'governmentissuedid'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_206_AS_PLANET'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','10')
    
  
    --attributeoption  'relationshipmask'
    
    select @attributeid=attributeid from attribute
    where attributename= 'relationshipmask'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_262_A_DATE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','2')
    
  
    --attributeoption  'relationshipmask'
    
    select @attributeid=attributeid from attribute
    where attributename= 'relationshipmask'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_262_FRIEND'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','3')
    
  
    --attributeoption  'relationshipmask'
    
    select @attributeid=attributeid from attribute
    where attributename= 'relationshipmask'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_262_MARRIAGE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','4')
    
  
    --attributeoption  'relationshipmask'
    
    select @attributeid=attributeid from attribute
    where attributename= 'relationshipmask'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_262_A_LONGTERM_RELATIONSHIP'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','6')
    
  
    --attributeoption  'relationshipmask'
    
    select @attributeid=attributeid from attribute
    where attributename= 'relationshipmask'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_262_INTIMACYPHYSICAL'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','8')
    
  
    --attributeoption  'relationshipmask'
    
    select @attributeid=attributeid from attribute
    where attributename= 'relationshipmask'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_262_MARRIAGE__CHILDREN'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','40')
    
  
    --attributeoption  'relationshipmask'
    
    select @attributeid=attributeid from attribute
    where attributename= 'relationshipmask'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_262_ACTIVITY_PARTNER'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','60')
    
  
    --attributeoption  'relationshipmask'
    
    select @attributeid=attributeid from attribute
    where attributename= 'relationshipmask'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_262_EMAILCHAT'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9051','80')
    
  
    --attributeoption  'transactiontype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'transactiontype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_264_INITIATED_SUBSCRIPTION'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'90510','1')
    
  
    --attributeoption  'transactiontype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'transactiontype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_264_RENEWED_SUBSCRIPTION'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'90510','2')
    
  
    --attributeoption  'transactiontype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'transactiontype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_264_TERMINATED_SUBSCRIPTION'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'90510','3')
    
  
    --attributeoption  'transactiontype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'transactiontype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_264_REFUND_FOR_BOUNCED_EMAIL'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'90510','4')
    
  
    --attributeoption  'transactiontype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'transactiontype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_264_ADMIN_MONTHLY_CHANGE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'90510','5')
    
  
    --attributeoption  'transactiontype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'transactiontype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_264_ADMIN_UNITS_CHANGE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'90510','6')
    
  
    --attributeoption  'transactiontype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'transactiontype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_264_COMPLIMENTARY'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'90510','7')
    
  
    --attributeoption  'transactiontype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'transactiontype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_264_CHECK_RECEIVED_BY_MAIL'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'90510','8')
    
  
    --attributeoption  'transactiontype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'transactiontype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_264_ADMIN_ADJUSTMENT'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'90510','9')
    
  
    --attributeoption  'transactiontype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'transactiontype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_264_MEMBER_TERMINATED_SUBSCRIPTION__GOOD'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'90510','10')
    
  
    --attributeoption  'transactiontype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'transactiontype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_264_MEMBER_TERMINATED_SUBSCRIPTION__BAD'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'90510','11')
    
  
    --attributeoption  'industrytype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'industrytype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_BLANK_79068'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'90510','0')
    
  
    --attributeoption  'industrytype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'industrytype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_392_ADMINISTRATIVEHUMAN_RESOURCES'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'90510','2')
    
  
    --attributeoption  'industrytype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'industrytype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_392_ADVERTISINGMARKETINGPR'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'90510','3')
    
  
    --attributeoption  'industrytype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'industrytype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_392_ARCHITECTUREINTERIOR_DESIGN'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'90510','4')
    
  
    --attributeoption  'industrytype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'industrytype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_392_AUTOMOTIVEAVIATIONTRANSPORTATION'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'90510','5')
    
  
    --attributeoption  'industrytype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'industrytype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_392_COMMUNICATIONTELECOM'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'90510','6')
    
  
    --attributeoption  'industrytype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'industrytype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_392_CONSTRUCTIONAGRICULTURELANDSCAPING'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'90510','7')
    
  
    --attributeoption  'industrytype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'industrytype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_392_DESIGNVISUAL__GRAPHIC_ARTS'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'90510','8')
    
  
    --attributeoption  'industrytype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'industrytype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_392_EDUCATIONTEACHINGCHILD_CARE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'90510','9')
    
  
    --attributeoption  'industrytype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'industrytype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_392_ENTERTAINMENTMEDIADRAMATIC_ARTS'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'90510','10')
    
  
    --attributeoption  'industrytype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'industrytype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_392_ENTREPRENEURIALSTARTUP'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'90510','11')
    
  
    --attributeoption  'industrytype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'industrytype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_392_EXECUTIVEGENERAL_MGTCONSULTING'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'90510','12')
    
  
    --attributeoption  'industrytype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'industrytype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_392_FASHIONSTYLEMODELINGAPPARELBEAUTY'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'90510','13')
    
  
    --attributeoption  'industrytype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'industrytype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_392_FINANCIALACCOUNTINGINSURANCEREAL_ESTATE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'90510','14')
    
  
    --attributeoption  'industrytype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'industrytype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_392_GOVERNMENTCIVIL_SERVICEPUBLIC_POLICY'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'90510','15')
    
  
    --attributeoption  'industrytype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'industrytype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_392_HOMEMAKINGCHILD_REARING'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'90510','16')
    
  
    --attributeoption  'industrytype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'industrytype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_392_INTERNETECOMMERCETECHNOLOGY'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'90510','17')
    
  
    --attributeoption  'industrytype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'industrytype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_392_LAW_ENFORCEMENTMILITARYSECURITY'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'90510','18')
    
  
    --attributeoption  'industrytype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'industrytype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_392_LAWLEGALJUDICIARY'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'90510','19')
    
  
    --attributeoption  'industrytype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'industrytype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_392_MANUFACTURINGWAREHOUSINGSHIPPINGFACILITIES'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'90510','20')
    
  
    --attributeoption  'industrytype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'industrytype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_392_MEDICALHEALTHFITNESSSOCIAL_SERVICES'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'90510','21')
    
  
    --attributeoption  'industrytype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'industrytype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_392_NONPROFITVOLUNTEERACTIVIST'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'90510','22')
    
  
    --attributeoption  'industrytype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'industrytype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_392_OTHER_PROFESSIONALSERVICESTRADE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'90510','23')
    
  
    --attributeoption  'industrytype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'industrytype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_392_PUBLIC_SAFETYFIREPARAMEDIC'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'90510','24')
    
  
    --attributeoption  'industrytype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'industrytype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_392_PUBLISHINGPRINT_MEDIA'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'90510','25')
    
  
    --attributeoption  'industrytype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'industrytype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_392_RESTAURANTFOOD_SERVICESCATERING'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'90510','26')
    
  
    --attributeoption  'industrytype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'industrytype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_392_RETIRED'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'90510','27')
    
  
    --attributeoption  'industrytype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'industrytype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_392_SALES_REPRESENTATIVERETAILWHOLESALE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'90510','28')
    
  
    --attributeoption  'industrytype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'industrytype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_392_STUDENT'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'90510','29')
    
  
    --attributeoption  'industrytype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'industrytype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_392_TECHNICALSCIENCEENGINEERING'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'90510','30')
    
  
    --attributeoption  'industrytype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'industrytype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_392_TRAVELRECREATIONLEISUREHOSPITALITY'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'90510','31')
    
  
    --attributeoption  'industrytype'
    
    select @attributeid=attributeid from attribute
    where attributename= 'industrytype'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_392_OTHER'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'90510','32')
    
  
    --attributeoption  'religionactivitylevel'
    
    select @attributeid=attributeid from attribute
    where attributename= 'religionactivitylevel'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_416_ONLY_ON_RELIGIOUS_HOLIDAYS'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'90510','0')
    
  
    --attributeoption  'contactuslist'
    
    select @attributeid=attributeid from attribute
    where attributename= 'contactuslist'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_218_UPDATING_MY_EMAIL_ADDRESS'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'90510','3')
    
  
    --attributeoption  'contactuslist'
    
    select @attributeid=attributeid from attribute
    where attributename= 'contactuslist'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_218_BILLING_AND_SUBSCRIPTIONS'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'90510','4')
    
  
    --attributeoption  'contactuslist'
    
    select @attributeid=attributeid from attribute
    where attributename= 'contactuslist'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_218_MY_PROFILE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'90510','5')
    
  
    --attributeoption  'contactuslist'
    
    select @attributeid=attributeid from attribute
    where attributename= 'contactuslist'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_218_LOGGING_IN'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'90510','6')
    
  
    --attributeoption  'contactuslist'
    
    select @attributeid=attributeid from attribute
    where attributename= 'contactuslist'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_218_UPDATING_MY_PHOTOS'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'90510','7')
    
  
    --attributeoption  'contactuslist'
    
    select @attributeid=attributeid from attribute
    where attributename= 'contactuslist'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_218_GENERAL_QUESTION_OR_SUGGESTION'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'90510','8')
    
  
    --attributeoption  'contactuslist'
    
    select @attributeid=attributeid from attribute
    where attributename= 'contactuslist'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_218_CONTACTING_OTHERS'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'90510','9')
    
  
    --attributeoption  'contactuslist'
    
    select @attributeid=attributeid from attribute
    where attributename= 'contactuslist'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_218_SEARCHING__SEARCH'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'90510','10')
    
  
    --attributeoption  'contactuslist'
    
    select @attributeid=attributeid from attribute
    where attributename= 'contactuslist'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_218_PR__MEDIA_RELATIONS'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'90510','11')
    
  
    --attributeoption  'contactuslist'
    
    select @attributeid=attributeid from attribute
    where attributename= 'contactuslist'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_218_INVESTOR_RELATIONS'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'90510','12')
    
  
    --attributeoption  'contactuslist'
    
    select @attributeid=attributeid from attribute
    where attributename= 'contactuslist'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_218_AFFILIATE_PROGRAM'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'90510','13')
    
  
    --attributeoption  'gendermask'
    
    select @attributeid=attributeid from attribute
    where attributename= 'gendermask'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_69_MALE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'90510','2')
    
  
    --attributeoption  'gendermask'
    
    select @attributeid=attributeid from attribute
    where attributename= 'gendermask'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_69_FEMALE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'90510','3')
    
  
    --attributeoption  'gendermask'
    
    select @attributeid=attributeid from attribute
    where attributename= 'gendermask'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_69_SEEKING_MALE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'90510','4')
    
  
    --attributeoption  'gendermask'
    
    select @attributeid=attributeid from attribute
    where attributename= 'gendermask'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_69_SEEKING_FEMALE'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'90510','5')
    
  
    --attributeoption  'suspectwords'
    
    select @attributeid=attributeid from attribute
    where attributename= 'suspectwords'
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_310_XXX'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'90510','0')
    