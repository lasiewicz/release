
    SET IDENTITY_INSERT mnKey1.dbo.[ResourceTemplateID] ON   
    INSERT INTO [mnKey1].[dbo].[ResourceTemplateID]
([PKID],[InputValue] ,[InsertDate])
 VALUES
 ('3372',1 ,GETDATE())        
  SET IDENTITY_INSERT mnKey1.dbo.[ResourceTemplateID] off   


use mnSubscription 
go


    insert into [mnsubscription].[dbo].[resourcetemplate]
([resourcetemplateid],[content],[description],[groupid],[groupdefault],[resourcetemplatetypeid],[insertdatetime],[updatedatetime])
 values
('3291',N'<div class="cell-row-header classic"> <h2>{0} Month</h2> </div>',N'BBW Classic row header (1 month)','90510','0','1',getdate(),getdate())
    

    insert into [mnsubscription].[dbo].[resourcetemplate]
([resourcetemplateid],[content],[description],[groupid],[groupdefault],[resourcetemplatetypeid],[insertdatetime],[updatedatetime])
 values
('3294',N'<div class="cell-plan classic shade"> <h2>${0} <span class="per-month">per month</span></h2> <p class="fine-print">one payment of ${1}</p></div>',N'AS Classic plan (ch) - shade','90510','0','3',getdate(),getdate())
    

    insert into [mnsubscription].[dbo].[resourcetemplate]
([resourcetemplateid],[content],[description],[groupid],[groupdefault],[resourcetemplatetypeid],[insertdatetime],[updatedatetime])
 values
('3297',N'<div class="cell-plan classic strong"> <h2>${0} <span class="per-month">per month</span></h2> <p class="fine-print">one payment of ${1}</p></div>',N'AS Classic plan (ch) - strong','90510','0','3',getdate(),getdate())
    

    insert into [mnsubscription].[dbo].[resourcetemplate]
([resourcetemplateid],[content],[description],[groupid],[groupdefault],[resourcetemplatetypeid],[insertdatetime],[updatedatetime])
 values
('3300',N'<div class="cell-plan classic strongest"> <h2>${0} <span class="per-month">per month</span></h2> <p class="fine-print">one payment of ${1}</p></div>',N'AS Classic plan (ch) - strongest','90510','0','3',getdate(),getdate())
    

    insert into [mnsubscription].[dbo].[resourcetemplate]
([resourcetemplateid],[content],[description],[groupid],[groupdefault],[resourcetemplatetypeid],[insertdatetime],[updatedatetime])
 values
('3303',N'',N'[x] unused','90510','0','1',getdate(),getdate())
    

    insert into [mnsubscription].[dbo].[resourcetemplate]
([resourcetemplateid],[content],[description],[groupid],[groupdefault],[resourcetemplatetypeid],[insertdatetime],[updatedatetime])
 values
('3306',N'',N'[x] unused','90510','0','1',getdate(),getdate())
    

    insert into [mnsubscription].[dbo].[resourcetemplate]
([resourcetemplateid],[content],[description],[groupid],[groupdefault],[resourcetemplatetypeid],[insertdatetime],[updatedatetime])
 values
('3309',N'<div class="cell-row-header classic shade"> <h2>{0} Months</h2> </div>',N'BBW Classic row header - shaded','90510','0','1',getdate(),getdate())
    

    insert into [mnsubscription].[dbo].[resourcetemplate]
([resourcetemplateid],[content],[description],[groupid],[groupdefault],[resourcetemplatetypeid],[insertdatetime],[updatedatetime])
 values
('3312',N'<div class="cell-row-header classic"> <h2>{0} Months</h2> </div>',N'BBW Classic row header','90510','0','1',getdate(),getdate())
    

    insert into [mnsubscription].[dbo].[resourcetemplate]
([resourcetemplateid],[content],[description],[groupid],[groupdefault],[resourcetemplatetypeid],[insertdatetime],[updatedatetime])
 values
('3315',N'<div class="cell-col-header premium"><h2>Premium Plans</h2><div class="rel-layer-container"><a href="#" rel="hover">(details)</a><div class="list-of-benefits rel-layer-div"><h2>List of benefits</h2><img src="Content/images/9041/sub_listOfBenefitsWithoutPremiumChart.gif" alt="List of benefits"/></div></div><div class="plan-includes"><h3 class="float-inside">Includes:</h3><ul class="float-inside premium-features"><li><span>&#9658;</span> Member Spotlight</li><li><span>&#9658;</span> Highlighted Profile</li></ul></div><img src="/img/Community/AmericanSingles/sub_memberSpotlight_mediumThumb.gif" class="premium-features-thumb"> </div>',N'BBW Classic column header - premium','90510','0','2',getdate(),getdate())
    

    insert into [mnsubscription].[dbo].[resourcetemplate]
([resourcetemplateid],[content],[description],[groupid],[groupdefault],[resourcetemplatetypeid],[insertdatetime],[updatedatetime])
 values
('3318',N'<div class="cell-plan classic"> <h2>${0} <span class="per-month">per month</span></h2> <p class="fine-print">one payment of ${1}</p></div>',N'AS Classic plan (ch)','90510','0','3',getdate(),getdate())
    

    insert into [mnsubscription].[dbo].[resourcetemplate]
([resourcetemplateid],[content],[description],[groupid],[groupdefault],[resourcetemplatetypeid],[insertdatetime],[updatedatetime])
 values
('3321',N'<div class="cell-col-header standard"> <h2>Standard Plans</h2> <div class="rel-layer-container"> <a href="#" rel="hover">(details)</a> <div class="list-of-benefits rel-layer-div"> <h2>List of benefits</h2> <img src="Content/images/9041/sub_listOfBenefitsWithoutPremiumChart.gif" alt="List of benefits"/> </div> </div> </div>',N'BBW Classic column header - standard','90510','0','2',getdate(),getdate())
    

    insert into [mnsubscription].[dbo].[resourcetemplate]
([resourcetemplateid],[content],[description],[groupid],[groupdefault],[resourcetemplatetypeid],[insertdatetime],[updatedatetime])
 values
('3324',N'<div class="cell-plan classic"> <h2>${0} <span class="per-month">per month</span></h2> <p class="fine-print">w/${2} monthly renewal*</p> </div>',N'BBWClassic plan (1 month)','90510','0','3',getdate(),getdate())
    

    insert into [mnsubscription].[dbo].[resourcetemplate]
([resourcetemplateid],[content],[description],[groupid],[groupdefault],[resourcetemplatetypeid],[insertdatetime],[updatedatetime])
 values
('3327',N'<div class="cell-plan classic shade"> <h2>${0} <span class="per-month">per month</span></h2> <p class="fine-print">w/${2} monthly renewal*</p> </div>',N'AS Classic plan (1 month) - shade','90510','0','3',getdate(),getdate())
    

    insert into [mnsubscription].[dbo].[resourcetemplate]
([resourcetemplateid],[content],[description],[groupid],[groupdefault],[resourcetemplatetypeid],[insertdatetime],[updatedatetime])
 values
('3330',N'<div class="cell-plan classic strong"> <h2>${0} <span class="per-month">per month</span></h2> <p class="fine-print">w/${2} monthly renewal*</p> </div>',N'AS Classic plan (1 month) - strong','90510','0','3',getdate(),getdate())
    

    insert into [mnsubscription].[dbo].[resourcetemplate]
([resourcetemplateid],[content],[description],[groupid],[groupdefault],[resourcetemplatetypeid],[insertdatetime],[updatedatetime])
 values
('3333',N'<div class="cell-plan classic strongest"> <h2>${0} <span class="per-month">per month</span></h2> <p class="fine-print">w/${2} monthly renewal*</p> </div>',N'AS Classic plan (1 month) - strongest','90510','0','3',getdate(),getdate())
    

    insert into [mnsubscription].[dbo].[resourcetemplate]
([resourcetemplateid],[content],[description],[groupid],[groupdefault],[resourcetemplatetypeid],[insertdatetime],[updatedatetime])
 values
('3336',N'<div class="cell-plan classic"> <h2>${0} <span class="per-month">per month, plus tax</span></h2> <p class="fine-print">one payment of ${1}<br/> w/${2} monthly renewal*</p> </div>',N'[x] unused','90510','0','3',getdate(),getdate())
    

    insert into [mnsubscription].[dbo].[resourcetemplate]
([resourcetemplateid],[content],[description],[groupid],[groupdefault],[resourcetemplatetypeid],[insertdatetime],[updatedatetime])
 values
('3339',N'<div class="cell-plan classic shade">  <h2>${0} <span class="per-month">per month</span></h2>  <p class="fine-print">one payment of ${1}<br/>  w/${2} monthly renewal*</p> </div>',N'AS Classic plan - shade','90510','0','3',getdate(),getdate())
    

    insert into [mnsubscription].[dbo].[resourcetemplate]
([resourcetemplateid],[content],[description],[groupid],[groupdefault],[resourcetemplatetypeid],[insertdatetime],[updatedatetime])
 values
('3342',N'<div class="cell-plan classic"> <h2>${0} <span class="per-month">per month</span></h2> <p class="fine-print">one payment of ${1}<br/> w/${2} monthly renewal*</p> </div>
',N'BBW Classic','90510','0','3',getdate(),getdate())
    

    insert into [mnsubscription].[dbo].[resourcetemplate]
([resourcetemplateid],[content],[description],[groupid],[groupdefault],[resourcetemplatetypeid],[insertdatetime],[updatedatetime])
 values
('3345',N'<div class="cell-plan classic strongest">  <h2>${0} <span class="per-month">per month</span></h2>  <p class="fine-print">one payment of ${1}<br/>  w/${2} monthly renewal*</p> </div>',N'AS Classic plan - strongest','90510','0','3',getdate(),getdate())
    

    insert into [mnsubscription].[dbo].[resourcetemplate]
([resourcetemplateid],[content],[description],[groupid],[groupdefault],[resourcetemplatetypeid],[insertdatetime],[updatedatetime])
 values
('3348',N'<div class="cell-plan classic strong">  <h2>${0} <span class="per-month">per month</span></h2>  <p class="fine-print">one payment of ${1}<br/>  w/${2} monthly renewal*</p> </div>',N'AS Classic plan - strong','90510','0','3',getdate(),getdate())
    

    insert into [mnsubscription].[dbo].[resourcetemplate]
([resourcetemplateid],[content],[description],[groupid],[groupdefault],[resourcetemplatetypeid],[insertdatetime],[updatedatetime])
 values
('3351',N'<div class="cell-row-header classic shade">
<h2>{0} Month</h2>
</div>',N'AS Classic row header - shaded (1 month)','90510','0','1',getdate(),getdate())
    

    insert into [mnsubscription].[dbo].[resourcetemplate]
([resourcetemplateid],[content],[description],[groupid],[groupdefault],[resourcetemplatetypeid],[insertdatetime],[updatedatetime])
 values
('3354',N'<div class="cell-col-header classic premium">
	<h2>Premium Plans</h2>
	<p>Includes</p>
	<ul class="rowHeaderInfoOutside">
	<li>Member Spotlight</li>
	</ul>

    <div class="rowHeaderHPDetails" onmouseover="TogglePopupDiv(''listOfBenefits_details_spotlight''); return false;" onmouseout="TogglePopupDiv(''listOfBenefits_details_spotlight''); return false;">
		<img src="/img/Community/AmericanSingles/sub_memberSpotlight_mediumThumb.gif">
		<a href="#" onclick="return false">Details</a>
	</div>
</div>

<div id="listOfBenefits_details_spotlight" class="helpLayerContainerB">
	<div class="helpLayerMiddle">
		<div class="helpLayerInner lightest borderDark">
	    <h2>List of benefits</h2>
	    <img align="absmiddle" src="/img/Community/AmericanSingles/sub_listOfBenefitsChart_spotlight.gif" border="0" />
		</div>
	</div>
</div>',N'AS Classic column header - member spotlight','90510','0','2',getdate(),getdate())
    

    insert into [mnsubscription].[dbo].[resourcetemplate]
([resourcetemplateid],[content],[description],[groupid],[groupdefault],[resourcetemplatetypeid],[insertdatetime],[updatedatetime])
 values
('3357',N'<div class="cell-col-header classic premium">
	<h2>Premium Plans</h2>
	<p>Includes</p>
	<ul class="rowHeaderInfoOutside">
		<li>Highlighted Profile</li>
	</ul>

    <div class="rowHeaderHPDetails" onmouseover="TogglePopupDiv(''listOfBenefits_details_highlighted''); return false;" onmouseout="TogglePopupDiv(''listOfBenefits_details_highlighted''); return false;">
		<img src="/img/Community/AmericanSingles/sub_profileHighlight_mediumThumb.gif">
		<a href="#" onclick="return false">Details</a>
	</div>
</div>

<div id="listOfBenefits_details_highlighted" class="helpLayerContainerB">
	<div class="helpLayerMiddle">
		<div class="helpLayerInner lightest borderDark">
	    <h2>List of benefits</h2>
	    <img align="absmiddle" src="/img/Community/AmericanSingles/sub_listOfBenefitsChart_highlight.gif" border="0" />
		</div>
	</div>
</div>',N'AS Classic column header - highlighted profile','90510','0','2',getdate(),getdate())
    

    insert into [mnsubscription].[dbo].[resourcetemplate]
([resourcetemplateid],[content],[description],[groupid],[groupdefault],[resourcetemplatetypeid],[insertdatetime],[updatedatetime])
 values
('3360',N'<div class="cell-plan classic"> 
	<h2><img alt="" src="/img/site/americansingles-com/sub-price-strike-2499.gif " style="vertical-align:middle;"/> ${0} <span class="per-month">per month</span></h2> 
	<p class="fine-print">one payment of ${1}<br/> 
	w/${2} monthly renewal*</p> 
</div>',N'AS Classic plan - strike - 2499','90510','0','3',getdate(),getdate())
    

    insert into [mnsubscription].[dbo].[resourcetemplate]
([resourcetemplateid],[content],[description],[groupid],[groupdefault],[resourcetemplatetypeid],[insertdatetime],[updatedatetime])
 values
('3363',N'<div class="cell-plan classic"> 
	<h2><img alt="" src="/img/site/americansingles-com/sub-price-strike-5994.gif " style="vertical-align:middle;"/> ${0} <span class="per-month">per month</span></h2> 
	<p class="fine-print">one payment of ${1}<br/> 
	w/${2} monthly renewal*</p> 
</div>',N'AS Classic plan - strike - 5994','90510','0','3',getdate(),getdate())
    

    insert into [mnsubscription].[dbo].[resourcetemplate]
([resourcetemplateid],[content],[description],[groupid],[groupdefault],[resourcetemplatetypeid],[insertdatetime],[updatedatetime])
 values
('3366',N'<div class="cell-plan classic"> 
	<h2><img alt="" src="/img/site/americansingles-com/sub-price-strike-3999.gif " style="vertical-align:middle;"/> ${0} <span class="per-month">per month</span></h2> 
	<p class="fine-print">one payment of ${1}<br/> 
	w/${2} monthly renewal*</p> 
</div>',N'AS Classic plan - strike - 3999','90510','0','3',getdate(),getdate())
    

    insert into [mnsubscription].[dbo].[resourcetemplate]
([resourcetemplateid],[content],[description],[groupid],[groupdefault],[resourcetemplatetypeid],[insertdatetime],[updatedatetime])
 values
('3369',N'<div class="cell-plan classic strongest">
	<h2 style="color: #9c042a;padding-top:4px">Buy 6 months, get 6 months <span style="color:#9c042a">FREE!</span></h2>
	<p class="fine-print">One payment of ${1}<br/>
	w/${2} monthly renewal*</p>
</div>',N'SP Plan Buy 6 months, get 6 months FREE!','90510','0','3',getdate(),getdate())
    

    insert into [mnsubscription].[dbo].[resourcetemplate]
([resourcetemplateid],[content],[description],[groupid],[groupdefault],[resourcetemplatetypeid],[insertdatetime],[updatedatetime])
 values
('3372',N'<div class="cell-plan classic shaded">
	<h2 style="padding-top:4px">Buy 3 months, get 3 months <span style="color:#9c042a">FREE!</span></h2>
	<p class="fine-print">One payment of ${1}<br/>
	w/${2} monthly renewal*</p>
</div>',N'SP Plan Buy 3 months, get 3 months FREE!','90510','0','3',getdate(),getdate())
    