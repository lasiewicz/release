

  --registration promos 'PRM_EMAIL_ADVERTISING'
  insert into [mnsystem].[dbo].[registrationpromotion]
([registrationpromotionid],[promotionid],[domainid],[privatelabelid],[resourcekey],[resourceconstant],[listorder],[insertdate])
values
('18113','3220','24',null,'521718','PRM_EMAIL_ADVERTISING','1',getdate())

  --registration promos 'PRM_SEARCH_ENGINE'
  insert into [mnsystem].[dbo].[registrationpromotion]
([registrationpromotionid],[promotionid],[domainid],[privatelabelid],[resourcekey],[resourceconstant],[listorder],[insertdate])
values
('18114','3217','24',null,'521742','PRM_SEARCH_ENGINE','2',getdate())

  --registration promos 'PRM_PRINT_ARTICLE'
  insert into [mnsystem].[dbo].[registrationpromotion]
([registrationpromotionid],[promotionid],[domainid],[privatelabelid],[resourcekey],[resourceconstant],[listorder],[insertdate])
values
('18115','3219','24',null,'521738','PRM_PRINT_ARTICLE','3',getdate())

  --registration promos 'PRM_CHAT'
  insert into [mnsystem].[dbo].[registrationpromotion]
([registrationpromotionid],[promotionid],[domainid],[privatelabelid],[resourcekey],[resourceconstant],[listorder],[insertdate])
values
('18116','17558','24',null,'521716','PRM_CHAT','4',getdate())

  --registration promos 'PRM_ONLINE_ADVERTISING'
  insert into [mnsystem].[dbo].[registrationpromotion]
([registrationpromotionid],[promotionid],[domainid],[privatelabelid],[resourcekey],[resourceconstant],[listorder],[insertdate])
values
('18117','2947','24',null,'521732','PRM_ONLINE_ADVERTISING','5',getdate())

  --registration promos 'PRM_LOCAL_NEWS_SHOWS_RADIO'
  insert into [mnsystem].[dbo].[registrationpromotion]
([registrationpromotionid],[promotionid],[domainid],[privatelabelid],[resourcekey],[resourceconstant],[listorder],[insertdate])
values
('18118','3305','24',null,'521720','PRM_LOCAL_NEWS_SHOWS_RADIO','6',getdate())

  --registration promos 'PRM_RECOMMENDED_BY_A_FRIEND'
  insert into [mnsystem].[dbo].[registrationpromotion]
([registrationpromotionid],[promotionid],[domainid],[privatelabelid],[resourcekey],[resourceconstant],[listorder],[insertdate])
values
('18119','3216','24',null,'521740','PRM_RECOMMENDED_BY_A_FRIEND','7',getdate())

  --registration promos 'PRM_OTHER_ONLINE'
  insert into [mnsystem].[dbo].[registrationpromotion]
([registrationpromotionid],[promotionid],[domainid],[privatelabelid],[resourcekey],[resourceconstant],[listorder],[insertdate])
values
('18120','17564','24',null,'521736','PRM_OTHER_ONLINE','8',getdate())

  --registration promos 'PRM_NEWS_ARTICLE_PRESS_RELEASE'
  insert into [mnsystem].[dbo].[registrationpromotion]
([registrationpromotionid],[promotionid],[domainid],[privatelabelid],[resourcekey],[resourceconstant],[listorder],[insertdate])
values
('18121','54924','24',null,'0','PRM_NEWS_ARTICLE_PRESS_RELEASE','9',getdate())