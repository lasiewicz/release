
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList0](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList0] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList1](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList1] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList2](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList2] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList3](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList3] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList4](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList4] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList5](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList5] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList6](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList6] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList7](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList7] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList8](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList8] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList9](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList9] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList10](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList10] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList11](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList11] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList12](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList12] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList13](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList13] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList14](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList14] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList15](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList15] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList16](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList16] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList17](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList17] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList18](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList18] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList19](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList19] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList20](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList20] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList21](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList21] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList22](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList22] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList23](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList23] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList24](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList24] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList25](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList25] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList26](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList26] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList27](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList27] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList28](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList28] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList29](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList29] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList30](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList30] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList31](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList31] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList32](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList32] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList33](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList33] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList34](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList34] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList35](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList35] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList36](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList36] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList37](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList37] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList38](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList38] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList39](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList39] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList40](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList40] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList41](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList41] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList42](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList42] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList43](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList43] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList44](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList44] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList45](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList45] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList46](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList46] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList47](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList47] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList48](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList48] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList49](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList49] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList50](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList50] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList51](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList51] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList52](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList52] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList53](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList53] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList54](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList54] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList55](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList55] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList56](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList56] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList57](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList57] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList58](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList58] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList59](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList59] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList60](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList60] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList61](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList61] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList62](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList62] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList63](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList63] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList64](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList64] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList65](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList65] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList66](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList66] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList67](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList67] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList68](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList68] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList69](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList69] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList70](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList70] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList71](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList71] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList72](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList72] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList73](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList73] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList74](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList74] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList75](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList75] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList76](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList76] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList77](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList77] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList78](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList78] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList79](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList79] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList80](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList80] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList81](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList81] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList82](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList82] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList83](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList83] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList84](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList84] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList85](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList85] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList86](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList86] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList87](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList87] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList88](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList88] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList89](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList89] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList90](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList90] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList91](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList91] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList92](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList92] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList93](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList93] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList94](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList94] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList95](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList95] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList96](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList96] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList97](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList97] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList98](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList98] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList99](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList99] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList100](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList100] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList101](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList101] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList102](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList102] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList103](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList103] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList104](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList104] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList105](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList105] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList106](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList106] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList107](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList107] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList108](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList108] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList109](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList109] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList110](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList110] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList111](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList111] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList112](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList112] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList113](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList113] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList114](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList114] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList115](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList115] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList116](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList116] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList117](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList117] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList118](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList118] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList119](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList119] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList120](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList120] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList121](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList121] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList122](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList122] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList123](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList123] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList124](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList124] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList125](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList125] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList126](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList126] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList127](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList127] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList128](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList128] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList129](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList129] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList130](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList130] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList131](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList131] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList132](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList132] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList133](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList133] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList134](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList134] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList135](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList135] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList136](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList136] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList137](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList137] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList138](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList138] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList139](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList139] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList140](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList140] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList141](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList141] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList142](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList142] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList143](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList143] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList144](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList144] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList145](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList145] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList146](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList146] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList147](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList147] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList148](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList148] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList149](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList149] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList150](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList150] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList151](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList151] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList152](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList152] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList153](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList153] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList154](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList154] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList155](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList155] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[D24YNMList156](
	[MemberID] [int] NOT NULL,
	[TargetMemberID] [int] NOT NULL,
	[DirectionFlag] [tinyint] NOT NULL,
	[PrivateLabelID] [int] NOT NULL,
	[YNMTypeID] [tinyint] NULL,
	[UpdateDate] [smalldatetime] NOT NULL,
	[MailMask] [int] NOT NULL,
	[ListTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_D24YNMList156] PRIMARY KEY NONCLUSTERED 
(
	[MemberID] ASC,
	[TargetMemberID] ASC,
	[ListTypeID] ASC,
	[DirectionFlag] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [SECONDARY]
) ON [PRIMARY]

GO

    