

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100018797','2948','9051',0,N'Dating Safety',N'<TABLE cellSpacing=4 cellPadding=4 bgColor=#ffffff border=0> 
<TBODY> 
<TR> <TD class=default>With access to ((BRANDNAME))’s many features like IM, email and chat, you have the most powerful online tools available to meet your match.  With these tools, however, we want to remind you of some online safety guidelines to help make your journey to find love more enjoyable and worry-free.  With ((BRANDNAME)), you are in complete control of your online experience and should only share information about yourself once you are comfortable doing so.</TD></TR> 
<TR> <TD class=default> <UL> <LI> <SPAN class=bold>The same rules for offline dating apply to online dating.</SPAN>By following standard dating precautions, cyber-savvy daters can feel truly at ease while getting to know each other. The main thing to remember: trust your instincts and use common sense just like you would offline.  Using your own good judgment is your best tool because, ultimately, you are responsible for your own experience.</LI></UL></TD></TR> 
<TR> <TD class=default> <UL> <LI> <SPAN class=bold>Rest assured you are in the right place.</SPAN>((BRANDNAME)) has been in the online personals business since the early days of the Internet and we go to great lengths to ensure that the website stays clean and safe. We also keep all personal member contact information, such as your name and personal email address, completely confidential.  You can read our <a href="http://((PLDOMAIN))/Applications/Article/ArticleView.aspx?CategoryID=1948&ArticleID=6498&HideNav=True#privacy">privacy policy</a> for more details.</LI></UL></TD></TR> 
<TR> <TD class=default> <UL> <LI> <SPAN class=bold>You are in complete control of your online experience at all times.</SPAN>You control the pace of the relationship.  You can remain anonymous until you feel ready, communicate with members at your own discretion, and decide when it’s time to take your online relationship offline.  One advantage of meeting online is that you can start to “get to know” a person before communicating and learn about them before meeting in-person.    ((BRANDNAME)) also offers many safe opportunities for you to meet offline, in a group setting, so check out our regularly updated <a href="http://www.hurrydate.com/" target="_blank">((BRANDNAME)) event schedule</a> for parties and events in your area.</LI></UL></TD></TR> 
<TR> <TD class=default> <UL> <LI> <SPAN class=bold>When you decide to meet face to face, pick a public place and provide your own transportation to and from the location.</SPAN>Restaurants, coffee shops and malls are examples of public places; private homes or hotel rooms are not.  Before going out on a date, always tell a friend or family member where you are going and check-in with them when you return home.</LI></UL></TD></TR>
<TR> <TD class=default> <UL> <LI> <SPAN class=bold>Never include your personal contact information in your profile.</SPAN>Examples of the kind of information you should not include in your profile include telephone numbers, email addresses, IM addresses, your home or work address, and your last name.  Passwords should be unique, not a version of your name or birth date.</LI></UL></TD></TR> 
<TR> <TD class=default> <UL> <LI> <SPAN class=bold>Ask a lot of questions.</SPAN>Stay away from anyone who won''t take no for an answer or pressure you for any kind of personal information. Potential dates will respect your space and allow you to take your time.</LI></UL></TD></TR>
<TR> <TD class=default> <UL> <LI> <SPAN class=bold>Never send money to someone you meet online.</SPAN>We work hard to maintain the integrity of our community using both human and electronic screening techniques. However, from time to time, someone may get past our net. If someone asks you for money, for whatever reason, do not do so and please, report the situation to us via the “Report a Concern” button.  If somebody is asking you, they may be asking others, and your report can help protect other members.</LI></UL></TD></TR>
<TR> <TD class=default> <UL> <LI> <SPAN class=bold>Even with our strong effort to ensure the quality of our site, we cannot guarantee perfection. </SPAN>We have a customer care hotline, and ask you to call toll-free if you see anything strange so we can check it out.</LI></UL></TD></TR>
<TR> <TD class=default> <SPAN class=bold>We Wish You a Happy Search and the Best of Luck Finding Your Match, <br />The ((BRANDNAME)) Team</SPAN></TD></TR>
</TBODY></TABLE>',null,
'0',GETDATE(),'1','True',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100018800','2950','9051',0,N'Dating Safety',N'<TABLE cellSpacing=4 cellPadding=4 bgColor=#ffffff border=0> <TBODY> <TR> <TD class=default><SPAN class=bold>Safety-wise, meeting on the Net is inherently similar to meeting offline.</SPAN> As long as the same standard dating precautions are followed, cyber-savvy daters can feel truly at ease while getting to know each other. The main thing to remember: trust your instincts and use common sense just like you would offline.</TD></TR> <TR> <TD class=default><SPAN class=bold>You can rest assured that you''re in the right place</SPAN> because MatchNet, the company that brings you JDate.com, has been in the global dating and romance business since the early days of the Internet and goes to extra lengths to ensure that its sites stay clean and safe. We do our best to monitor and block emails of anyone who uses profanity, hustles or is not serious about finding a relationship. We also keep all contacts between members onsite so your email address remains confidential.</TD></TR> <TR> <TD class=default></TD></TR> <TR> <TD class=default>Before you get started, here are a few guidelines to keep in mind.</TD></TR> <TR> <TD class=default><SPAN class=bold>Helpful Hints for Common Sense Online Dating</SPAN></TD></TR> <TR> <TD class=default> <UL> <LI>Remember that you are in control of your online experience at all times. You can remain completely anonymous until you feel ready.</LI></UL></TD></TR> <TR> <TD class=default> <UL> <LI>You are also in control when it comes to taking an online relationship offline. Plus, you have an advantage online because you can get to know each other before you meet. Remember that you don''t need to take anything further than the computer or phone wires until you feel completely at ease. Go at your own pace!</LI></UL></TD></TR> <TR> <TD class=default> <UL> <LI>When you do decide to meet face to face, pick a public place and provide your own transportation to and fro. Tell a friend where you''re going and check in when you return home.</LI></UL></TD></TR> <TR> <TD class=default> <UL> <LI>Never include your personal contact information in your profile, especially telephone numbers, email, home address or your last name, and only give them out when your instincts tell you this is someone you can trust. It''s okay to take your time.</LI></UL></TD></TR> <TR> <TD class=default> <UL> <LI>Set up an email account just for online dating.</LI></UL></TD></TR> <TR> <TD class=default> <UL> <LI>Ask a lot of questions and watch for inconsistencies.</LI></UL></TD></TR> <TR> <TD class=default> <UL> <LI>Stay away from members who won''t take no for an answer or pressure you for any kind of personal information. Serious cyber-savvy daters will respect your space and allow you to take your time.</LI></UL></TD></TR> <TR> <TD class=default> <UL> <LI>Even with our strong effort to ensure the quality of our sites, we cannot guarantee perfection in the system. To deal with this, we have a 24 hour customer care staff hotline, and we ask members to call toll free if they see anything strange so that we can research it.</LI></UL></TD></TR> <TR> <TD class=default> <UL> <LI>If someone asks you to go to their personal web site or asks you for money, use common sense not to oblige and then report the situation to us.</LI></UL></TD></TR> <TR> <TD class=default> <UL> <LI>If someone gives you a phone number with a strange area code, check it out to make sure it''s not a charge number before you make the call.</LI></UL></TD></TR> <TR> <TD class=default> <UL> <LI>Using your own good judgment is your best bet because ultimately you are responsible for your personal dating experience. Trust your instincts and then have fun with the right people!</LI></UL></TD></TR></TBODY></TABLE>',null,
'0',GETDATE(),'1','False',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100018803','6602','9051',0,N'About BBWPersonalsPlus.com',N'<div id="article">
<img border="0" src="((image:Articles/cont_about_couple.jpg))" />
<!-- <img border="0" src="((image:Articles/cont_about_whyJoin.gif))" width="336" height="101" /> -->

<table border="0" cellspacing="10" cellpadding="0" width="100%">

<tr>
<td colspan="2">

<p><strong>Top 10 reasons to join ((BRANDNAME)):</strong></p>

	<p><strong>1. ((BRANDNAME)) is the #1 way to jump-start your love life! No more wasting precious time…</strong><br />
…and no more blind-date blahs! Whether you''re simply looking to have a great time, seeking a serious relationship or are returning to the dating scene after some time away, ((BRANDNAME)) is the place for you. We''re here to help you meet wonderful people for wonderful relationships: romance, friendships and more. And however full your life is now, it''ll be even better once you’re tuned into this social scene. ((BRANDNAME)) is fun, laid-back and comfortable.</p>

<img border="0" src="((image:Articles/cont_about_couple_girls.jpg))" />

<p><strong>2. Take part in one of the world’s greatest online communities.</strong><br />
      We''re very proud of our members: smart, attractive, successful people from all walks of life, all professions and all ages, living life to its fullest. They’re all looking to connect, network and find romance! ((BRANDNAME)) has hundreds of thousands of members worldwide and is one of the few personals sites around that boasts a nearly perfect 50:50 male-to-female ratio.</p>

<p><strong>3. This is WAY better than the bar scene.</strong><br />
      ((BRANDNAME)) is more interesting and considerably less expensive than the bar scene. For the price of a few drinks, you can subscribe for <strong>a month</strong> and meet a new person every day if you like. You can also get to know people and what they''re looking for <strong>before</strong> you contact them, thanks to detailed profiles with photos, essays and all kinds of personal tidbits.</p>

<p><strong>4. A tradition of connecting singles – 21st Century style!</strong><br />
	Established in 1997, ((BRANDNAME)) has been the premier online  personals community for nearly a decade. We''re the modern way for  people to find love. Check out our <a href="/Applications/Article/ArticleDefault.aspx?CategoryID=2006&ShowTitle=1">Success Stories</a> section to see hundreds of couples who met and got married thanks to ((BRANDNAME)). In fact, you probably have friends and family who are ((BRANDNAME)) success stories! Why not be one of them? All you have to do is take the first step and become a <a href="((PLSECURELINK))">Subscriber</a>.</p>

<p><strong>5. Expand your social circle.</strong><br />
     Meet people from around the corner or around the world! ((BRANDNAME)) makes it easy to get to know people you might not otherwise meet. Just think, without ((BRANDNAME)) you could walk past your soul mate every day and never even know it.</p>

<p><strong>6. Save beaucoup bucks ($$ bling-bling $$).</strong><br />
      ((BRANDNAME)) makes meeting new people quick, easy, and inexpensive. One month''s Subscriber costs less than a night on the town and delivers the people you WANT to meet, WHEN you want to meet them. With over half-a-million fabulous members, there''s someone for everyone to meet – and that definitely includes you!</p>

<p><strong>7. More members. More parties. More travel and events.</strong><br>
     We throw some of the largest and most popular <a href="/Applications/Events/Events.aspx">events</a> and offer travel adventures to many of the world’s most desirable vacation spots. We provide our members with many, many opportunities to get out there and have a great time.</p>

<p><strong>8. It''s not all about looks. It''s how you feel and who you are.</strong><br />
      ((BRANDNAME)) helps you express what you’re all about – your passions, sense of humor, and values – better than chats over cocktails ever could. Common interests are the building blocks for the best relationships. Let the real you shine and likeminded members will take notice.</p>


	<p><strong>9. Meet, greet and kick up your feet!</strong><br />
	Our secure online server makes dating at ((BRANDNAME)) anonymous and safe, so you''re free to sit back, relax and meet people from the comfort of your home or office, any time of day or night.</p>
	<p>Check out some of the features we''ve developed to help you get to know the people you’re looking for:</p>
</td></tr>

<tr><td colspan="2"><hr></td></tr>

<tr><td>
<!-- <img border="0" src="((image:Articles/cont_about_screen_click.gif))" align="left" /> --></td>
<td><p><strong>Secret Admirer?</strong> <br />  
Secret Admirer? is a simple way for you to meet the members you think you''d click with and who feel the same about you. It''s fun, easy, and everyone can participate!</p>
                                                                                                                                 
<p>Just look for Secret Admirer? in members'' profiles and tell us if you think the two of you would click by picking Y, M or N. Don''t worry, we keep it confidential unless they also click Y for you. If there''s a mutual "Yes," we''ll give you both a Secret Admirer? Alert. That''s when the magic happens. Send an email and introduce yourself!</p>
</td></tr>

<tr height="30px"><td colspan="2"><hr></td></tr>


<tr><td>

<!-- <img border="0" src="((image:Articles/cont_about_screen_profile.gif))" align="left" /> --></td>
<td><p><strong>Create a profile </strong><br>
Our user-friendly interface will get you dating in no time. Just answer a few questions, describe yourself, and post up to four fabulous photos. Within minutes, you''re on the scene!</p>
</td></tr>
<tr><td colspan="2"><hr /></td></tr>

	 	  <tr>
<td><!-- <img border="0" src="((image:Articles/cont_about_screen_search.gif))" /> --></td>
<td><p><strong>Search – Find someone intriguing today!</strong><br />
Just enter the characteristics and desired location of the person you want to meet and get ready to be wowed.</p></td>
<td></tr>

<tr><td colspan="2"><hr /></td></tr>

<tr>
<td><!-- <img border="0" src="((image:Articles/cont_about_screen_email.gif))" /> --></td>
<td><p><strong>Email</strong><br />
Once your profile is up, other members can email you.  However, as a Premium Member, you can write to anyone you choose. If you''re not interested, you can simply decline contact and remain anonymous. But if they pique your interest, write back and forth to test the chemistry! There’s no pressure to meet in person until you’re ready.</p></td></tr>

<tr><td colspan="2"><hr /></td></tr>

	 	  
<tr><td>
<!-- <img border="0" src="((image:Articles/cont_about_screen_IM.gif))" /> --></td>
<td>
<p><strong>Instant messaging - Instant gratification!</strong><br />
Did someone’s profile grab your attention? If they''re online, send an instant message and see if sparks fly. Video chat, audio chat and good old-fashioned text chat enable you to make the connection.</p></td></tr>

<tr><td colspan="2"><hr /></td></tr>

<tr><td>
<!-- <img border="0" src="((image:Articles/cont_about_screen_chat.gif))" /> --></td>
<td>
<p><strong>Real-time chat</strong><br />
Stop by our lively chat rooms to mingle with other singles.</p></td>
</tr>

<tr><td colspan="2"><hr /></td></tr>

<tr><td>
<!-- <img border="0" src="((image:Articles/cont_about_screen_msgBrd.jpg))" /> --></td>
<td>
<p><strong>Message Boards – Express yourself!</strong><br />
With everything from Judaism to entertainment to relationships and dating, message boards let you connect and interact with other members while you get informed, share your thoughts and see what other ((BRANDNAME))rs have to say. It’s also an amazing resource and a fabulous way to get hooked into the community and find people with common interests.</p></td>
</tr>

<tr><td colspan="2"><hr /></td></tr>

<tr><td>
<!-- <img border="0" src="((image:Articles/cont_about_screen_ecards.jpg))" /> --></td>
<td>
<p><strong>E-cards – make some fun connections</strong><br />
E-cards are another great way to communicate and connect with people on the site or to send greetings to friends outside the ((BRANDNAME)) community. That''s right, you can send E-cards to anyone you like!</p></td>
</tr>

<tr><td colspan="2"><hr /></td></tr>

<tr><td>
<!-- <img border="0" src="((image:Articles/cont_about_screen_mol.gif))" /> --></td>
<td>
<p><strong>Members Online</strong><br />
See photos and profiles of everyone who''s online when you are. ((BRANDNAME)) makes it easy to keep track of everyone you meet with our “Hot List” feature. Add interesting singles to your "Favorites" or to any other personalized category that you create!</p></td>
</tr>

<tr><td colspan="2"><hr /></td></tr>
</table>

	<p><strong>10. The ((BRANDNAME)) Team is available.</strong><br />

	Our mission is to make sure you''re successful. Dial ((CSSUPPORTPHONENUMBER)) for friendly and knowledgeable assistance. ((BRANDNAME)) Consultants are here to answer your questions and help you find who you''re looking for. The first step is up to you. Why not expand your social scene, explore new possibilities and enjoy exciting adventures? ((BRANDNAME)) is where it happens.</p>

<p><a href="((PLSECURELINK))prtid=143">Become a Premium Member today!</a></p>
</div>',null,
'0',GETDATE(),'1','True',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100018806','6296','9051',0,N'What is Secret Admirer?',N'<p><table border="0" cellpadding="2" cellspacing="0" width="592" id="table1">
         <tr> 
          <td width="2%" valign="top"><strong>Q.</strong></td>
          <td><strong>What is Secret Admirer?</strong></td>
        </tr>
        <tr> 
          <td valign="top">A.</td>
          <td><p>Secret Admirer is a fun game where you can secretly find out if the one you like likes you back. As your Spark.com community continues to grow, so do the chances that you’ll meet someone just right for you.</p>
<p>We’re working behind the scenes to help provide you with more matches like your Yes’s and fewer like your No’s. Think of Secret Admirer as your online personals confidant, a go-between for you and the singles with whom you’re most likely to connect.</p>

<p><strong>Here’s how it works:</strong></p>

<img src="((image:Articles/click_mf.gif))" border="0" align="right" width="280" height="280" class="float-outside">

<p>As you search, we’ll show you members who may pique your interest. Confide in us if you think you’d hit it off by clicking “Yes”, “No” or “Maybe.” Every Yes will be sent a Secret Admirer Alert email, which includes you and several other member profiles.</p> 

<p>Secret Admirer is completely confidential unless it’s a mutual Yes. When there is a mutual Yes, we’ll introduce each of you with a You Both Said Yes Alert email. The rest is up to you!</p>
</td>
              </tr>

</table></p>',null,
'1',GETDATE(),'1','True',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100018809','6290','9051',0,N'What are Secret Admirer Alert emails?',N'<p><strong>Q. What are Secret Admirer Alert emails?</strong> <br />

A. We send Secret Admirer Alert emails to show you members who clicked “Yes” for you, as well as other members we think you might like. You can log in to view their profiles and  click “Yes”, “No” or “Maybe” for each. If you click “Yes” to a member who clicked “Yes” for you, we’ll introduce each of you with a You Both Said Yes Alert email.
</p>',null,
'0',GETDATE(),'1','False',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100018812','6294','9051',0,N'What happens when I click "Yes"?',N'<p><table border="0" cellpadding="2" cellspacing="0" width="592" id="table1">
	 <tr>
		<td width="2%" valign="top"><strong>Q.</strong></td>
		<td><strong>What happens when I click "Yes"? </strong></td>
	</tr>
	<tr>
		<td valign="top">A.</td>
		<td> 
<p>When you click "Yes" for a member, you''ll be included in that person''s next People 
              You Click&trade; With email. He or she won''t know that you 
              clicked "Yes" but is given the chance to click "Yes", "No" or "Maybe" for 
              all members featured in the email. If he or she clicks "Yes" back 
              to you, you''ll both get a YY Click&trade;! Alert
              email so you can take it to the next level.</p> 
</td></tr></table></p>',null,
'0',GETDATE(),'1','False',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100018815','6292','9051',0,N'What happens when I click "No?"',N'<p><table border="0" cellpadding="2" cellspacing="0" width="592" id="table1">
	 <tr>
		<td width="2%" valign="top"><strong>Q.</strong></td>
		<td><strong>What happens when I click "No?"</strong></td>
	</tr>
	<tr>
		<td valign="top">A.</td>
		<td> 
<p>When you click “No” for a member, we''ll know you''re not interested and they''ll never know you weren’t interested.</p>

            <p>The only time Secret Admirers are revealed is when both members click “Yes” for each other. When that happens, both members get a You Both Said Yes Alert email.</p> 
</td></tr></table></p>',null,
'0',GETDATE(),'1','False',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100018818','6288','9051',0,N'How do I know who clicked Yes to my profile?',N'<p><table border="0" cellpadding="2" cellspacing="0" width="592" id="table1">
	 <tr>
		<td width="2%" valign="top"><strong>Q.</strong></td>
		<td><strong>How do I know who clicked Yes to my profile?</strong></td>
	</tr>
	<tr>
		<td valign="top">A.</td>
		<td> 
			<p>You''ll only know if someone clicks “Yes” for you if you click “Yes” back for that person. Otherwise, all Secret Admirers are kept secret.</p>            
            <p>However, if a member clicks “Yes” for you, he or she will be included in your next Secret Admirer Alert email, as well as other members whose profiles show compatibility with yours. If you click “Yes” for a person who has clicked “Yes” for you, we''ll notify you both via email with a You Both Said Yes Alert email.</p>
            <p>Review your Secret Admirer Alert emails as they arrive, check out the listed members and click back so we can introduce you to your You Both Said Yes matches.</p> 
</td></tr></table></p>',null,
'0',GETDATE(),'1','False',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100018821','6358','9051',0,N'I forgot my password. Can you tell me what it is?',N'<p><table border="0" cellpadding="2" cellspacing="0" width="592" id="table1">
	<tr>
		 <td width="2%" valign="top"><strong>Q.</strong></td>
		<td><strong>I forgot my password. Can you tell me what it is?</strong></td>
	</tr>
	<tr>
		<td valign="top">A.</td>
		<td><p>No problem. Just click <a href="/Applications/Logon/RetrievePassword.aspx">Forgot password?</a> on the home page, enter your email address and we''ll send you your password.</p>

			<p>If you don''t remember the email address you used or need additional help, call our toll-free number: ((CSSUPPORTPHONENUMBER)). 
			If outside the U.S. and Canada, please contact our <a href="http://www.spark.net/contact.htm">Beverly Hills Headquarters.</a></p></td>
	</tr>
</table></p>',null,
'1',GETDATE(),'1','True',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100018824','6354','9051',0,N'How do I change my username, email address or password?',N'<p><table border="0" cellpadding="2" cellspacing="0" width="592" id="table1">
	 <tr>
		<td width="2%" valign="top"><strong>Q.</strong></td>
		<td><strong>How do I change my username, email address or password?</strong></td>
	</tr>
	<tr>
		<td valign="top">A.</td>
		<td>
<p>You can change your username, email address or password on your profile page. 
                <ul>
                  <li>Click "Your Profile" in the Profile & Photos drop-down menu</li>
                  <li>Click "Edit" next to your username</li>
		  <li>Click either <a href="/Applications/MemberProfile/ChangeEmail.aspx">Change Email</a> or <a href="/Applications/MemberProfile/ChangeEmail.aspx">Change Password</a> or simply edit your username</li>
                </ul>
                After you''ve made your changes, click "Save" and you''re done.</p></td>
	</tr>
</table></p>',null,
'2',GETDATE(),'1','False',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100018827','6356','9051',0,N'How do I contact customer service?',N'<p><table border="0" cellpadding="2" cellspacing="0" width="592" id="table1">
	 <tr>
		<td width="2%" valign="top"><strong>Q.</strong></td>
		<td><strong>How do I contact customer service?</strong></td>
	</tr>
	<tr>
		<td valign="top">A.</td>
		<td><p>Our customer service department is standing by to help you make the very most of your membership. They can help you navigate the site, improve your profile, or find answers to all your questions. Feel free to <a href="/Applications/ContactUs/ContactUs.aspx">email us</a> or call toll-free: ((CSSUPPORTPHONENUMBER)). If outside the U.S. and Canada, please contact our <a href="http://www.spark.net/contact.htm">Beverly Hills Headquarters.</a></p></td>
	</tr>
</table></p>',null,
'3',GETDATE(),'1','False',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100018830','100000924','9051',0,N'What is auto-login ("remember me") and how does it work?',N'<p><table border="0" cellpadding="2" cellspacing="0" width="592" id="table1">
	 <tr>
		<td width="2%" valign="top"><strong>Q.</strong></td>
		<td><strong>What is auto-login ("remember me") and how does it work?</strong></td>
	</tr>
	<tr>
		<td valign="top">A.</td>
		<td>
<p>Auto-login allows you to visit ((BRANDNAME)) without having to enter your email address and password each time.</p>

<p>To set up auto-login, simply check the "remember me" box on the ((BRANDNAME)) login page. Your login information will be saved to the computer you are using, and you will be automatically logged in each time you visit ((BRANDNAME)). We suggest that you don''t use auto-login on shared computers to keep your information private.</p>

<p>When accessing your private and account information, you will still be asked to enter your login information to keep these details secure.</p>

<p>If you click the ((BRANDNAME)) logout button, do not visit the site for 30 days, or change your email or password, you will be prompted to login again the next time you visit.</p>

</td>
	</tr>
</table></p>',null,
'4',GETDATE(),'1','False',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100018833','100000927','9051',0,N'How do I deactivate auto-login?',N'<p><table border="0" cellpadding="2" cellspacing="0" width="592" id="table1">
	 <tr>
		<td width="2%" valign="top"><strong>Q.</strong></td>
		<td><strong>How do I deactivate auto-login?</strong></td>
	</tr>
	<tr>
		<td valign="top">A.</td>
		<td>
<p>If you click the logout button in the top left of your ((BRANDNAME)) homepage, your auto-login will be deactivated, and you will be prompted to login again the next time you visit.</p>

</td>
	</tr>
</table></p>',null,
'5',GETDATE(),'1','False',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100018836','6416','9051',0,N'How do I create a profile?',N'<p><table border="0" cellpadding="2" cellspacing="0" width="592" id="table1">
	 <tr>
		<td width="2%" valign="top"><strong>Q.</strong></td>
		<td><strong>How do I create a profile?</strong></td>
	</tr>
	<tr>
		<td valign="top">A.</td>
		<td><p>Creating and posting a profile is free and only takes a bit of time. If you''re logged in now, you''ve probably already finished the first step. To finish the rest, login and click <a href="/Applications/MemberProfile/ViewProfile.aspx">Your Profile</a> in the Profile & Photos drop-down menu at the top of the screen. Click "Edit" next to the section you''d like to finish, make your changes and click "Save" when you''re done.</p>
		    <p>When creating your profile, remember that the essay screen will time 
              out after seven minutes, so save often to make sure you don''t lose anything. You can also  
              write your essays ahead of time and paste them into your profile. Just be sure to save each essay after you paste it.</p>
		<p>Helpful hints:</p>
		<ul>
		   <li>Your profile is the first impression you make, so put your best foot forward.</li>
		   <li>Be honest, have fun and stay positive.</li>
		         <li>Your essays give you the chance to speak directly to members and maybe even the person of your 
                dreams. Essays should offer insight into your lifestyle 
                and what is most important to you in a relationship. They should also reveal what makes you unique. Don''t worry about saying too much. The more you write and the more you give specific examples, the 
                more likely it is that someone out there will read your profile 
                and say, &quot;Wow! Me, too.&quot;</li>
		</ul>
		<p><strong>Don''t know what to say in your essays?</strong><br>
Take a look at our list of <a href="/Applications/Article/ArticleView.aspx?CategoryID=1956&ArticleID=6426">Things To Write About</a>.</p>
</td></tr></table></p>',null,
'1',GETDATE(),'1','True',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100018839','6426','9051',0,N'Things to Write About In Your Essays',N'<p><table border="0" cellpadding="2" cellspacing="0" width="592" id="table1">
	 <tr>
		<td width="2%" valign="top"><strong>Q.</strong></td>
		<td><strong>Things to Write About In Your Essays</strong></td>
	</tr>
	<tr>
		<td valign="top">A.</td>
		<td><p>
<a href="#aboutMe">About Me</a><br> 
<a href="#lookingFor">About the One I''m Looking For</a><br>
<a href="#firstDate">My Idea of a Perfect First Date</a><br>
<a href="#perception">My Idea of an Ideal Relationship</a><br>
<a href="#pastRelationships">What I''ve Learned From Past Relationships</a></p>

<p><a name="aboutMe"><strong>About Me</strong></a></p>
<ul>
<li>My best friend describes me as...</li>
<li>I''m happiest when I''m...</li>
<li>Here''s what you''d find if you looked around my place...</li>
<li>If I could live anywhere in the world, my top three choices would be...</li>
<li>A great day in my life would include...</li>
<li>If there was a fire drill at work and I got the afternoon off, I would...</li>
<li>The last great book I read was...</li>
<li>What makes me laugh the most is...</li>
<li>If I were stuck in a cabin during a snowstorm, the five things I''d most like to have with me would be...</li>
<li>If my best friend could set me up with anyone famous, it would probably be...</li>
<li>If someone were to cast me in any movie or TV show, it would probably be as ___________ in ______________ .</li>
<li>The last great movie I rented was...</li> 
<li>The last great movie I saw in a theater was...</li> 
<li>The last concert I went to was...</li>
<li>My favorite season is...</li> 
<li>The color I wear most is... </li>
<li>If you asked me what I''m wearing, I''d say...</li>
<li>My favorite TV show ever is....</li>
<li>My favorite sound is...</li>
<li>My favorite word is...</li>
<li>My least favorite word is....</li>
<li>My favorite on-screen love scene is...</li>
<li>The funniest thing I''ve ever seen was...</li>
<li>If I could be any profession other than my own, it would be...</li>
<li>When I walk through the gates of heaven, they''d say to me...</li>
<li>The music that moves me most is...</li>
<li>The most embarrassing thing that ever happened to me was...</li>
<li>The best gift I ever got was a...</li>
<li>Today in my car I was listening to...</li>
<li>If I could invite any five people to a dinner party, they would be...</li>
<li>My favorite memory from high school is...</li>
<li>Last Saturday night I...</li>
<li>The last vacation I went on was...</li>
<li>The most adventurous thing I''ve ever done was...</li>
<li>The most challenging thing I''ve ever done was...</li>
<li>If I could go out with anyone famous, it would be...</li></ul>

<a href="#top"><img border="0" src="((image:icon_gry_btn_top.gif))" align="right" width="16" height="15" alt="Back to top"></a>
<br>
<hr SIZE="1">

<p class="bold"><a name="lookingFor"><strong>About the one I''m looking for:</strong></a></p>
<ul>
<li>I''m happiest when I''m with someone who...</li>
<li>If a Genie in a bottle gave me three wishes, I''d wish for you to be...</li>
<li>The things I like about me that I''d like you to be too are...</li>
<li>The things I''d like you to like about me are that I''m...</li>
<li>On the weekends, you''d like to _____________ with me.</li>
<li>You''d make me laugh by...</li>
<li>On my birthday you...</li>
<li>If I asked you what you were wearing right now, it''d probably be...</li>
<li>If your best friend were to fix you up with any famous person, it would probably be...</li>
<li>____________ makes me weak at the knees, but _____________ makes me swoon.</li>
<li>____________ is hot. _____________ is hotter.</li>
<li>You amaze me with your...</li>
<li>You''re sexy because you...</li>
</ul>

<a href="#top"><img border="0" src="((image:icon_gry_btn_top.gif))" align="right" width="16" height="15" alt="Back to top"></a>
<br>
<hr SIZE="1">

<p class="bold"><a name="firstDate"><strong>My idea of our perfect first date:</strong></a></p>
<ul>
<li>If our date was on a desert island, we would...</li>
<li>If I won tickets on a radio show for our first date, we''d be going to...</li>
<li>If we could be teleported anywhere in the world for a date for just one afternoon or evening, I''d want to go to ___________ and ______________.</li>
<li>I ask you out.</li>
<li>You ask me out.</li>
<li>We talk about....</li>
<li>When we''re not talking, we''re...</li>
<li>At the end of the date, you compliment me on my...</li>
<li>At the end of the date, I compliment you on your...</li>
<li>In five words, the date would be...</li>
</ul>

<a href="#top"><img border="0" src="((image:icon_gry_btn_top.gif))" align="right" width="16" height="15" alt="Back to top"></a>
<br>
<hr SIZE="1">

<p class="bold"><a name="perception"><strong>My perception of an ideal relationship:</strong></a> </p>
<ul>
<li>A typical evening would be...</li>
<li>A typical morning would be...</li>
<li>A typical Saturday would be...</li>
<li>In ten years, over dinner, we''d probably be talking about...</li>
<li>The movie that describes our relationship best would be...</li>
<li>The relationship in five words...</li>
</ul>

<a href="#top"><img border="0" src="((image:icon_gry_btn_top.gif))" align="right" width="16" height="15" alt="Back to top"></a>
<br>
<hr SIZE="1">

<p class="bold"><a name="pastRelationships"><strong>What I''ve learned from my past relationships:</strong></a></p>
<ul>
<li>The best thing that ever happened to me in a relationship was...</li>
<li>If I had to pick three qualities in a partner that I know make me happy, they would be...</li>
<li>The most embarrassing thing that ever happened to me while I was dating was...</li>
<li>The top three things that make me feel comfortable in a relationship are...</li>
<li>My favorite clich&eacute; about relationships is... </li>
<li>The thing that I hate to fight about most is...</li>
<li>The thing that I actually enjoy fighting about is...</li>
<li>The best way to make up is...</li>
<li>My best friends tell me that I should stick to going out with people who...</li>
</ul>

<a href="#top"><img border="0" src="((image:icon_gry_btn_top.gif))" align="right" width="16" height="15" alt="Back to top"></a>

</td></tr></table></p>',null,
'2',GETDATE(),'1','False',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100018842','6424','9051',0,N'How do I view my own profile?',N'<p><table border="0" cellpadding="2" cellspacing="0" width="592" id="table1">
	 <tr>
		<td width="2%" valign="top"><strong>Q.</strong></td>
		<td><strong>How do I view my own profile?</strong></td>
	</tr>
	<tr>
		<td valign="top">A.</td>
		<td><p>To see your own profile, login and click <a href="/Applications/MemberProfile/ViewProfile.aspx">Your Profile</a> in the Profile & Photos drop-down menu at the top of the screen.
</p>

</td></tr></table></p>',null,
'3',GETDATE(),'1','False',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100018845','6422','9051',0,N'How do I update my profile?',N'<p><table border="0" cellpadding="2" cellspacing="0" width="592" id="table1">
	 <tr>
		<td width="2%" valign="top"><strong>Q.</strong></td>
		<td><strong>How do I update my profile?</strong></td>
	</tr>
	<tr>
		<td valign="top">A.</td>
		<td><p>To update your profile, login and click <a href="/Applications/MemberProfile/ViewProfile.aspx">Your Profile</a> in the Profile & Photos drop-down menu at the top of the screen. Click "Edit" next to the section you want to update and make your changes. Be sure to save at the bottom of each section when you''re done.</p>

<p>Try to put your best foot forward with at least one <a href="/Applications/MemberProfile/MemberPhotoUpload.aspx">photo</a> and snappy, detailed essays. Your photo is the key because most people feel more comfortable writing to members they can see. Try to stay positive in your essays and let the real you shine.
</p>

</td></tr></table></p>',null,
'4',GETDATE(),'1','False',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100018848','6436','9051',0,N'Why is it so important to fill out a complete profile?',N'<p><table border="0" cellpadding="2" cellspacing="0" width="592" id="table1">
	 <tr>
		<td width="2%" valign="top"><strong>Q.</strong></td>
		<td><strong>Why is it so important to fill out a complete profile?</strong></td>
	</tr>
	<tr>
		<td valign="top">A.</td>
		<td><p>Your <a href="/Applications/MemberProfile/ViewProfile.aspx">profile</a> lets people know that you are single and available!</p>
<p>Plus, it lets them know that you are interesting and worth writing to. If someone looks at your profile and there''s not much there, why would they bother?</p>
<p>This is not the time for brevity, folks. More is more. Make yourself interesting to others and you will get a great response on the site. If you <a href="/Applications/MemberProfile/MemberPhotoUpload.aspx">post photos</a> too, you''ll get even more people writing to you. The ball''s in your court.</p>
<p><strong>Don''t know what to say in your essays?</strong><br>
Take a look at our list of <a href="/Applications/Article/ArticleView.aspx?CategoryID=1956&ArticleID=6426">Things To Write About</a>.</p>
</td></tr></table></p>',null,
'5',GETDATE(),'1','False',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100018851','6432','9051',0,N'Where does my profile show up and how do I change the settings?',N'<p><table border="0" cellpadding="2" cellspacing="0" width="592" id="table1">
	 <tr>
		<td width="2%" valign="top"><strong>Q.</strong></td>
		<td><strong>Where does my profile show up and how do I change the settings?</strong></td>
	</tr>
	<tr>
		<td valign="top">A.</td>
		<td><p>Our site is set up so that there are many ways for members to find each other, including 
<a href="/Applications/MembersOnline/MembersOnline.aspx">Members Online</a>, 
<a href="/Applications/Search/SearchResults.aspx">Search</a>, Your Click&trade;! emails and 
<a href="/Applications/HotList/View.aspx?CategoryID=0">Hot Lists</a>. Your profile automatically shows up in all these sections. With your username or member number, members can also view your profile in <a href="/Applications/LookupProfile/LookupProfile.aspx">Look Up Member</a>.  They can also click through to your profile if you send them an email, instant message or Smile.</p>

<p>Members who are most active on the site keep their profile visible in all places because it increases their chances of connecting with people.</p>

<p>If you don''t want your profile to show up in Members Online, Search or Hot Lists, you can go to Show/Hide Profile in Member Services and hide your profile.</p>

<p>Just remember, someone special may be looking for you and never get to find you if your profile is hidden.</p>
</td></tr></table></p>',null,
'6',GETDATE(),'1','False',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100018854','6430','9051',0,N'What are the profile guidelines?',N'<p><table border="0" cellpadding="2" cellspacing="0" width="592" id="table1">
	 <tr>
		<td width="2%" valign="top"><strong>Q.</strong></td>
		<td><strong>What are the profile guidelines?</strong></td>
	</tr>
	<tr>
		<td valign="top">A.</td>
		<td><p>((PLDOMAIN)) welcomes all single adults who are courteous in word and action. Our Customer Care team reviews each and every profile to create a comfortable environment for our members.</p>

<p><strong>We do not accept profiles that contain any of the following:</strong></p>
<ul><li>Any direct contact information (e.g., email address, URL, ICQ/instant messenger ID, phone number, full name, address)</li>
<li>Any location or descriptive information that threatens a member''s anonymity</li>
<li>Abusive language of any kind (i.e., vulgarity, racism)</li> 
<li>Discussion or descriptions of illegal acts or behavior (e.g., drug use, violence)</li>
<li>Business or political advertisements or solicitations</li>
<li>Material that exploits or solicits personal information from individuals under the age of 18</li>
<li>Foreign languages (to effectively monitor profiles, they must be in English)</li>
<li>Solicitation of multiple or additional partners</li>
<li>Unauthorized use of copyrighted or trademarked material</li>
<li>Overt sexual innuendo or discussion</li></ul>
 
<p><strong>We do not accept profiles from:</strong></p>
 
<ul><li>Incarcerated individuals</li>
<li>Individuals under the age of 18</li> </ul>
</td></tr></table></p>',null,
'7',GETDATE(),'1','False',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100018857','6412','9051',0,N'How confidential is my profile?',N'<p><table border="0" cellpadding="2" cellspacing="0" width="592" id="table1">
 	 <tr>
		<td width="2%" valign="top"><strong>Q.</strong></td>
		<td><strong>How confidential is my profile?</strong></td>
	</tr>
	<tr>
		<td valign="top">A.</td>
		<td><p>Your email address and password are kept completely confidential. Members can only see your essays, photos and other personality details that you post in your <a href="/Applications/MemberProfile/ViewProfile.aspx">profile</a>. </p>
<p>For more information, please review our <a href="/Applications/Article/ArticleView.aspx?CategoryID=1948&ArticleID=6498&HideNav=True#privacy">Privacy policy</a>.</p>
</td></tr></table></p>',null,
'8',GETDATE(),'1','False',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100018860','6434','9051',0,N'Why am I not getting very many responses from other members?',N'<p><table border="0" cellpadding="2" cellspacing="0" width="592" id="table1">
	<tr>
		 <td width="2%" valign="top"><strong>Q.</strong></td>
		<td><strong>Why am I not getting very many responses from other members?</strong></td>
	</tr>
	<tr>
		<td valign="top">A.</td>
		<td><p>If you''re not getting many responses, it might be a good time to spice up your <a href="/Applications/MemberProfile/ViewProfile.aspx">profile</a> and get personal with your emails. Here are a few suggestions to get you started.  For further advice, you may call our Customer Care Team, toll-free, at ((CSSUPPORTPHONENUMBER)).  If outside the U.S. and Canada, please contact our <a href="http://www.spark.net/contact.htm">Beverly Hills Headquarters.</a></p>

<ul>
              <li><strong>Post honest and expressive photos in your profile.</strong> Most members only search for and write to people with photos.<br>
                <br>

<a href="/Applications/MemberProfile/MemberPhotoUpload.aspx">Your photos</a> should show more than just what you look like -- they should communicate your personality and the things that you love about your life! Ideally, a life that someone would want to be a part of.<br><br>

What should you show? Think fun, happy, warm, interesting, smiles... and think about the things that you love about your life (skiing, painting, traveling, playing with your dog, etc.). The first photo should be a headshot and the others can be more creative. Just remember to make them clear, recent and in focus!<br><br></li>

<li><strong>Make your essays more descriptive.</strong> Vivid, evocative essays are what it takes for you to stand out in the dating crowd and, more importantly, attract the people who are right for you.<br><br>

The key to writing great essays?  It''s all in the details... in giving specific examples.  No one wants to see boring lists of adjectives (e.g., nice, smart, funny, successful, attractive).  They want to know exactly what makes you different, interesting and fun to be with.  Write like you talk.  Pretend you''re hanging out with your best friends, telling them just what''s on your mind.  Don’t be vague or wishy-washy!.<br><br>

Take a look at our list of <a href="/Applications/Article/ArticleView.aspx?CategoryID=1956&ArticleID=6426">Things To Write About</a> if you get really stuck.<br><br></li>

              <li><strong>Write great personal introductory emails.</strong> Although 
                &quot;Hey, you look cute, check out my profile,&quot; may sound 
                short and sweet, in fact, it really is a canned intro that is 
                easily dismissed. When contacting other members, remember to ask 
                yourself, &quot;Why am I writing to this person?&quot; and then 
                tell him/her. Flattery, writing about things you have in common, 
                and a sense of humor will get you everywhere.<br>
                <br>

Take a look at our 5 Step Email Guide for tips on <a href="/Applications/Article/ArticleView.aspx?CategoryID=2010&ArticleID=6222">How To Write Your First Email</a>.<br><br></li>

              <li><strong>Check your personal <a href="/Applications/Email/MailBox.aspx">Inbox</a> on the site.</strong> 
                There may be messages waiting there that you''re not aware of. If you have &quot;spam guard&quot; on your personal 
                email, our notices many not be reaching you. You 
                can set your personal email to recognize email from us so you 
                won''t miss future notices. See the Never Miss Email section in Member Services for instructions and check with your Internet Service Provider 
                (ISP) if you have additional questions.</li>
            </ul>

<p>Found someone you like? Ready to send emails?<br><a href="((PLSECURELINK))">Subscribe</a> today and get our low monthly rate.</p>
</td></tr></table></p>',null,
'9',GETDATE(),'1','False',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100018863','6438','9051',0,N'Will my profile still show up if I end my subscription?',N'<p><table border="0" cellpadding="2" cellspacing="0" width="592" id="table1">
	 <tr>
		<td width="2%" valign="top"><strong>Q.</strong></td>
		<td><strong>Will my profile still show up if I end my subscription?</strong></td>
	</tr>
	<tr>
		<td valign="top">A.</td>
		<td><p>Yes, if you end your subscription, your <a href="/Applications/MemberProfile/ViewProfile.aspx">profile</a> stays up on the site for free. You can keep it up as long as you want and become a Subscription Member again when you see someone you''d like to connect with.</p>
</td></tr></table></p>',null,
'10',GETDATE(),'1','False',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100018866','6418','9051',0,N'How do I hide my profile?',N'<p><table border="0" cellpadding="2" cellspacing="0" width="592" id="table1">
	 <tr>
		<td width="2%" valign="top"><strong>Q.</strong></td>
		<td><strong>How do I hide my profile?</strong></td>
	</tr>
	<tr>
		<td valign="top">A.</td>
		<td><p>Members who are most active on the site keep their profile visible in all places because it increases their chances of connecting with people.</p>

<p>If you don''t want your profile to show up in <a href="/Applications/MembersOnline/MembersOnline.aspx">Members Online</a>, <a href="/Applications/HotList/View.aspx?CategoryID=0">Hot Lists</a> or <a href="/Applications/Search/SearchResults.aspx">Search</a>, you can go to <a href="/Applications/MemberServices/DisplaySettings.aspx">Show/Hide Profile</a> in <a href="/Applications/MemberServices/MemberServices.aspx">Member Services</a> and hide your profile. You will still be able to use the site and browse through members anonymously. Just remember, someone special may be looking for you and will never get to find you if your profile is hidden.</p>

<p>If you would like to take a break from online personals, you may remove your profile by selecting <a href="/Applications/MemberServices/Suspend.aspx">Suspend Membership</a> in <a href="/Applications/MemberServices/MemberServices.aspx">Member Services</a>.  When you are ready to reactivate your profile, simply log in and click Unsuspend.  Please note that a suspended Subscription Membership does not put your subscription on hold until you reactivate.  For example, if you selected a three-month membership plan, your subscription expires three months from your signup date even if your membership is suspended for any portion of that time.  Please note that your membership will be automatically renewed if you do not cancel it before the conclusion of the term.</p>
</td></tr></table></p>',null,
'11',GETDATE(),'1','False',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100018869','6420','9051',0,N'Can I reactivate my profile once I''ve removed it?',N'<p><table border="0" cellpadding="2" cellspacing="0" width="592" id="table1">
	 <tr>
		<td width="2%" valign="top"><strong>Q.</strong></td>
		<td><strong>Can I reactivate my profile once I''ve removed it?</strong></td>
	</tr>
	<tr>
		<td valign="top">A.</td>
		<td><p>Yes. Login to the site and you will be given the option to reactivate your profile.</p>
</td></tr></table></p>',null,
'12',GETDATE(),'1','False',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100018872','6414','9051',0,N'How do I change my username, email address or password?',N'<p><table border="0" cellpadding="2" cellspacing="0" width="592" id="table1">
	 <tr>
		<td width="2%" valign="top"><strong>Q.</strong></td>
		<td><strong>How do I change my username, email address or password?</strong></td>
	</tr>
	<tr>
		<td valign="top">A.</td>
		<td>
<p>You can change your username, email address or password on your profile page. 
                <ul>
                  <li>Click "Your Profile" in the Profile & Photos drop-down menu</li>
                  <li>Click "Edit" next to your username</li>
		  <li>Click either <a href="/Applications/MemberProfile/ChangeEmail.aspx">Change Email</a> or <a href="/Applications/MemberProfile/ChangeEmail.aspx">Change Password</a> or simply edit your username</li>
                </ul>
                After you''ve made your changes, click "Save" and you''re done.</p></td>
	</tr>
</table></p>',null,
'13',GETDATE(),'1','False',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100018875','6370','9051',0,N'How do I submit photos for my profile?',N'<p><table border="0" cellpadding="2" cellspacing="0" width="592" id="table1">
	 <tr>
		<td width="2%" valign="top"><strong>Q.</strong></td>
		<td><strong>How do I submit photos for my profile?</strong></td>
	</tr>
	<tr>
		<td valign="top">A.</td>
		<td><p><strong>First select your photos:</strong></p>  
<p>Your photos should show more than just what you look like -- they should communicate your personality and the things that you love about your life! Ideally, a life that someone would want to be a part of.</p>

<p>What should you show? Think fun, happy, warm, interesting, smiles... and think about the things that you love about your life (skiing, painting, traveling, playing with your dog, etc.). The first photo should be a headshot and the others can be more creative. Just remember to make them clear, recent and in focus!</p>

<p>Do not send composite/collage or indecent photos. We reserve the right to choose whether to post, edit or reject any photo.</p>

 
<p><strong>Next, choose how you''d like to submit your photos:</strong></p> 
<p><strong>Direct Upload</strong><br>
The fastest way to get your photos posted. Click <a href="/Applications/MemberProfile/MemberPhotoUpload.aspx">Your Photos</a> in the Profile & Photos drop-down menu at the top of the screen.</p>
 
<p><strong>Send scanned photos as an email attachment</strong><br>
Use this method if Direct Upload does not work for you. Email photos to <a href="mailto:Photos@((PLDOMAIN))">Photos@((PLDOMAIN))</a>. Be sure to include your member number, username and registered email address.</p>
 
<p><strong>Send photos by regular mail</strong><br>
The last resort. Use only if you have Web TV or if you were unsuccessful using the first two options. Be sure to include your member number, username and registered email address with your photos.</p>

 
<p><strong>Mail photos to:</strong><br>
Photos at ((PLDOMAIN))<br>
c/o Spark Networks&reg; Limited<br>
PO Box 739<br />
Orem, UT 84059-0739<br />
United States</p>
   
<p><strong>Still have questions?</strong><br>
<a href="/Applications/ContactUs/ContactUs.aspx">Email us</a> or call toll-free at: ((CSSUPPORTPHONENUMBER)).<br>
If outside the U.S. and Canada, please contact our <a href="http://www.spark.net/contact.htm">Beverly Hills Headquarters.</a></p>
</td></tr></table></p>',null,
'1',GETDATE(),'1','True',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100018878','6372','9051',0,N'How do I update my photos?',N'<p><table border="0" cellpadding="2" cellspacing="0" width="592" id="table1">
	 <tr>
		<td width="2%" valign="top"><strong>Q.</strong></td>
		<td><strong>How do I update my photos?</strong></td>
	</tr>
	<tr>
		<td valign="top">A.</td>
		<td><p>To update your photos, click <a href="/Applications/MemberProfile/MemberPhotoUpload.aspx">Your Photos</a> in the Profile & Photos drop-down menu at the top of the screen and add, delete or change the order of your photos as you like.</p>

<p><strong>Still have questions?</strong><br>
<a href="/Applications/ContactUs/ContactUs.aspx">Email us</a> or call toll free at: ((CSSUPPORTPHONENUMBER)).<br>
If outside the U.S. and Canada, please contact our <a href="http://www.spark.net/contact.htm">Beverly Hills Headquarters.</a></p>
</td></tr></table></p>',null,
'2',GETDATE(),'1','False',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100018881','6374','9051',0,N'What are the acceptable photo formats if I upload or email them to you?',N'<p><table border="0" cellpadding="2" cellspacing="0" width="592" id="table1">
	 <tr>
		<td width="2%" valign="top"><strong>Q.</strong></td>
		<td><strong>What are the acceptable photo formats if I upload or email them to you?</strong></td>
	</tr>
	<tr>
		<td valign="top">A.</td>
		<td><p><ul><li>Only send photo files with a JPG extension</li> 
				<li>Minimum: 350 x 350 pixels at 72 DPI </li>
				<li>Maximum: 5 MB</li></ul></p>
	</td></tr></table></p>',null,
'3',GETDATE(),'1','False',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100018884','6376','9051',0,N'What size photo files can I send?',N'<p><table border="0" cellpadding="2" cellspacing="0" width="592" id="table1">
	 <tr>
		<td width="2%" valign="top"><strong>Q.</strong></td>
		<td><strong>What size photo files can I send?</strong></td>
	</tr>
	<tr>
		<td valign="top">A.</td>
		<td><p>You can send photo files up to 5 MB..</p>
	</td></tr></table></p>',null,
'4',GETDATE(),'1','False',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100018887','6360','9051',0,N'How do I convert my photos into JPG format?',N'<p><table border="0" cellpadding="2" cellspacing="0" width="592" id="table1">
	 <tr>
		<td width="2%" valign="top"><strong>Q.</strong></td>
		<td><strong>How do I convert my photos into JPG format?</strong></td>
	</tr>
	<tr>
		<td valign="top">A.</td>
		<td><p>If you have a graphics program like Photoshop or LView Pro, you can use it to save files in a JPG file. If not, a copy/print shop can do it for you. Just remember to bring your photos so they can scan them in.</p>
	</td></tr></table></p>',null,
'5',GETDATE(),'1','False',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100018890','6368','9051',0,N'How do I send photos through AOL email?',N'<p><table border="0" cellpadding="2" cellspacing="0" width="592" id="table1">
	 <tr>
		<td width="2%" valign="top"><strong>Q.</strong></td>
		<td><strong>How do I send photos through AOL email?</strong></td>
	</tr>
	<tr>
		<td valign="top">A.</td>
		<td><p>Send one photo per email and make sure to attach or enclose them rather than pasting them into the body of the email.  Please include your member number in the email, too.</p>
<p>To attach:</p>

<p><ol>
              <li>Click "Attach"</li>
              <li>Use the Browse button to locate your photo</li>
              <li>Select the photo and click "Open"</li>
              <li>Click "OK" to return to the email message and then 
                click "Send"</li>
            </ol></p>
	</td></tr></table></p>',null,
'6',GETDATE(),'1','False',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100018893','6362','9051',0,N'How do I convert my photos into JPG format for AOL?',N'<p><table border="0" cellpadding="2" cellspacing="0" width="592" id="table1">
	 <tr>
		<td width="2%" valign="top"><strong>Q.</strong></td>
		<td><strong>How do I convert my photos into JPG format for AOL?</strong></td>
	</tr>
	<tr>
		<td valign="top">A.</td>
		<td><p><ol><li>Open your photo in Word, then select and copy it</li>
<li>Minimize Word </li>
              <li>Open Photoshop, click "File" and then click "Open A New 
                Window"</li>              <li>Click "OK"</li>
              <li>Go to Edit then click "Paste." The photo 
                will be pasted into the blank window. </li>
              <li>Go to File and click "Save a Copy" 
              </li>
<li>Give it a name and it will save as a JPG file. You can save it on your hard drive or on a diskette. </li>
<li>Attach it to your email and send to us</li></ol></p>
	</td></tr></table></p>',null,
'7',GETDATE(),'1','False',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100018896','6366','9051',0,N'How do I make sure that my photos will get posted?',N'<p><table border="0" cellpadding="2" cellspacing="0" width="592" id="table1">
	<tr>
		<td width="2%" valign="top"><strong>Q.</strong></td>
		<td><strong>How do I make sure that my photos will get posted?</strong></td>
	</tr>
	<tr>
		<td valign="top">A.</td>
		<td><p>Once we receive your photos, it usually takes approximately 24 - 48 hours to post them and we will send you an email confirmation once they are up. If you email us your photos or send them by regular mail, please include your member number, username and email address to ensure prompt posting.</p>
	</td></tr></table></p>',null,
'8',GETDATE(),'1','False',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100018899','6364','9051',0,N'How do I know when my photos are posted?',N'<p><table border="0" cellpadding="2" cellspacing="0" width="592" id="table1">
	 <tr>
		<td width="2%" valign="top"><strong>Q.</strong></td>
		<td><strong>How do I know when my photos are posted?</strong></td>
	</tr>
	<tr>
		<td valign="top">A.</td>
		<td><p>Once we receive your photos, it usually takes approximately 24 - 48 hours to post them. We will send you an email confirmation once they are up. To ensure prompt posting when submitting photos by email or regular mail, please include your member number, username and email address.</p>

<p><strong>Still have questions?</strong><br>
Email us or call toll-free at: ((CSSUPPORTPHONENUMBER)).<br>
If outside the U.S. and Canada, please contact our <a href="http://www.spark.net/contact.htm">Beverly Hills Headquarters.</a></p>
	</td></tr></table></p>',null,
'9',GETDATE(),'1','False',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100018902','6378','9051',0,N'Why can''t I see my new photos?',N'<p><table border="0" cellpadding="2" cellspacing="0" width="592" id="table1">
	 <tr>
		<td width="2%" valign="top"><strong>Q.</strong></td>
		<td><strong>Why can''t I see my new photos?</strong></td>
	</tr>
	<tr>
		<td valign="top">A.</td>
		<td><p>
Once we receive your photos, it usually takes 24 - 48 hours for them to be approved and posted, so you may have to wait a day or two.</p> 

<p>If it''s already been that long, they may not be showing up for technical reasons, but this is easy to fix. See below for the problem that seems most like yours and then follow the simple steps. If you''re still having trouble, feel free to call our Customer Care team at our toll-free number: ((CSSUPPORTPHONENUMBER)). If outside the U.S. and Canada, please contact our <a href="http://www.spark.net/contact.htm">Beverly Hills Headquarters.</a></p>

<p><strong>1. You can''t see the new photos on your computer</strong><br>
This means you need to delete the old photos stored in your computer''s temporary memory, called cache, to make room for new ones.  (Why does this happen?  Each time you visit a web page, the information is saved in the Temporary Internet File.  Your computer keeps the old info so it can load this page faster the next time you visit it).</p>

<p>Deleting old photos from cache to make room for new ones is easy.  Just print out and follow the steps below.  When you’re done, you’ll be able to see the new photos currently on the site.</p>
  
<p><strong>For Internet Explorer users:</strong><br>
<ol>
              <li>Click "Tools"</li>
              <li>Click "Internet Options"</li>
              <li>Go to the General tab</li>
              <li>Go to Temporary Internet Files and click "Delete 
                Files"</li>
              <li>Click "OK" and then "Exit" </li>
<li>Check out your profile and see your new photos!</li></ol></p>

 
<p><strong>For Netscape users:</strong><br>  
<ol>
              <li>Click "Edit" </li>
              <li>Click "Preferences" </li>
              <li>Click "Advanced" </li>
              <li>Click "Clear Memory Cache" </li>
              <li>Click "OK" and then exit </li>
<li>Check out your profile and see your new photos!</li></ol></p>

 
<p><strong>For AOL users:</strong><br>
<ol>
              <li>Click "My AOL"</li>
              <li>Click "Preferences" </li>
              <li>Click "WWW"</li>
              <li>Click the General tab </li>
              <li>Go to Temporary Internet Files and click "Delete 
                Files" </li>
              <li>Click "OK" and then exit </li>
<li>Check out your profile and see your new photos!</li></ol></p>

 
<p><strong>If you use AOL 5.0 and still can''t see your photos:</strong><br>  
<ol>
              <li>Click "Start" on the Task Bar</li>
              <li>Go to Programs and then to America Online</li>
              <li>Open AOL System Information and click the Utilities 
                tab </li>
              <li>Click "Clear Browser Cache" </li>
            </ol></p>

 
<p><strong>2. The new photos look out of focus or blurry on AOL </strong><br> 
The new AOL web browser compresses the image files. This degrades the quality of your photos on your monitor. Fortunately, this setting can easily be changed. The images will load slower, but it''s worth the wait. </p> 
   
<p><strong>Improve photo quality on AOL</strong><br>  
<ol>
              <li>Click "My AOL"</li>
              <li>Click "Preferences" </li>
              <li>Click "WWW" </li>
              <li>Click the "Web Graphics" tab </li>
              <li>Uncheck the Use Compressed Graphics box</li>
              <li>Click "Apply" </li>
              <li>Clear your computer of your blurry photos by following the Updating 
                Your Computer steps above</li>
<li>Check out your profile and see your new photos!</li></ol></p>
 
   
<p>For further questions, please contact us at <a href="mailto:Photos@((PLDOMAIN))">Photos@((PLDOMAIN)).</a></p>
</p>
	</td></tr></table></p>',null,
'10',GETDATE(),'1','False',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100018905','100001509','9051',0,N'What is The Color Code Personality Profile?',N'<table border="0" cellpadding="2" cellspacing="0" width="592">
	<tr>
		<td width="2%" valign="top"><strong>Q.</strong></td>
		<td><strong>What is The Color Code Personality Profile?</strong></td>
	</tr>
	<tr>
		<td valign="top">A.</td>
		<td>
			<p>The Color Code, created by Dr. Taylor Hartman, is one of the most insightful personality tests in existence, and is made up of four personality Colors or driving Core Motives (Power, Intimacy, Peace, Fun). The Color Code teaches you the real motives behind your actions and how to better relate to other personality types. The Color Code is not a matching system; it’s designed to help you build stronger and more meaningful relationships by understanding what drives the behavior of yourself and others. </p>
            <p>After you take the Color Code test, you will receive a 15-page detailed personality profile that tells you all about your Color and how you relate to other Colors. You will also learn your personality’s pros and cons, wants and needs.  To access your Color Code results at any time, just click on the "Color Code" link on the "Your Profile" menu.  Also, your Color will be added to your mini-profile in search results so members can immediately identify your personality type (and you theirs). Finally, members will be able to understand you even better by getting a more in-depth look into what really makes you tick by reading more about your Color and how your Colors match-up on the new Color Code tab on member profiles. </p>
		</td>
    </tr>
</table>',null,
'0',GETDATE(),'1','True',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100018908','100001518','9051',0,N'Can I retake the Color Code?',N'<table border="0" cellpadding="2" cellspacing="0" width="592">
	<tr>
		<td width="2%" valign="top"><strong>Q.</strong></td>
		<td><strong>Can I retake the Color Code?</strong></td>
	</tr>
	<tr>
		<td valign="top">A.</td>
		<td>
			<p>No, currently, members are only allowed to take the Color Code once.</p>
		</td>
    </tr>
</table>',null,
'0',GETDATE(),'1','False',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100018911','100000178','9051',0,N'What do The Color Code “Colors” represent?',N'<table border="0" cellpadding="2" cellspacing="0" width="592">
	<tr>
		<td width="2%" valign="top"><strong>Q.</strong></td>
		<td><strong>What do The Color Code “Colors” represent?</strong></td>
	</tr>
	<tr>
		<td valign="top">A.</td>
		<td>
			<p>The Color Code "Colors" represent the core underlying motives that drive people’s behavior. There are four Colors and they each represent a different personality type. The Color Code is not a matching system because any two Colors can have a successful relationship. Instead, the Color Code is designed to help you build stronger and more meaningful relationships by giving you the tools to understand what drives the behavior of yourself and others.  After you take the test, you’ll learn all about your Core Color, what each of the other Core Colors represent, as well as how well you match with each Core Color.  You’ll also be able to see other members’ Core Colors in search results and by viewing the Color Code tab on member profiles, which will compare both of your Core Colors and explore how your Colors match.</p>
		</td>
    </tr>
</table>',null,
'0',GETDATE(),'1','False',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100018914','100000203','9051',0,N'How do I take The Color Code Test?',N'<table border="0" cellpadding="2" cellspacing="0" width="592">
	<tr>
		<td width="2%" valign="top"><strong>Q.</strong></td>
		<td><strong>How do I take The Color Code Test?</strong></td>
	</tr>
	<tr>
		<td valign="top">A.</td>
		<td>
			<p>You can take the test by clicking on the "Color Code" link on the "Your Profile"” menu. The test is only two pages and should only take you 10-15 minutes to complete. You can also take the personality test by clicking the Color Code tab on your profile.  And don’t worry, your answers to the test questions are private and will only be used to tabulate your test results.</p>
		</td>
    </tr>
</table>',null,
'0',GETDATE(),'1','False',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100018917','100001515','9051',0,N'How can I share my test results with friends who aren’t on BBWPersonalsPlus?',N'<table border="0" cellpadding="2" cellspacing="0" width="592">
	<tr>
		<td width="2%" valign="top"><strong>Q.</strong></td>
		<td><strong>How can I share my test results with friends who aren’t on ((BRANDNAME))?</strong></td>
	</tr>
	<tr>
		<td valign="top">A.</td>
		<td>
			<p>There is a "Send to Friend" link atop the "Color Code Personality Results" pages. Simply click the link and you can email your results to friends.</p>
		</td>
    </tr>
</table>',null,
'0',GETDATE(),'1','False',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100018920','100001512','9051',0,N'How can I hide or remove my Color Code results?',N'<table border="0" cellpadding="2" cellspacing="0" width="592">
	<tr>
		<td width="2%" valign="top"><strong>Q.</strong></td>
		<td><strong>How can I hide or remove my Color Code results?</strong></td>
	</tr>
	<tr>
		<td valign="top">A.</td>
		<td>
			<p>You cannot remove your Color Code results, but can select to hide your results by going to the Member Services page and clicking on the "Color Code Settings" link under the "Profile" section.  Hiding your results will remove the display of your resulting color on your profile and in search results.</p>
		</td>
    </tr>
</table>',null,
'0',GETDATE(),'1','False',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100018923','100000181','9051',0,N'What happens after I take The Color Code Test?',N'<table border="0" cellpadding="2" cellspacing="0" width="592">
	<tr>
		<td width="2%" valign="top"><strong>Q.</strong></td>
		<td><strong>What happens after I take The Color Code Test?</strong></td>
	</tr>
	<tr>
		<td valign="top">A.</td>
		<td>
			<p>After you take the Color Code test, you will receive a 15-page detailed personality profile that tells you all about your Color and how you relate to other Colors. You will also learn your personality’s pros and cons, wants and needs.  To access your Color Code results at any time, just click on the "Color Code" link on the "Your Profile" menu.  Also, your Color will be added to your mini-profile in search results so members can immediately identify your personality type (and you theirs). Finally, members will be able to understand you even better by getting a more in-depth look into what really makes you tick by reading more about your Color and how your Colors match-up on the new Color Code tab on Your Profile.</p>
		</td>
    </tr>
</table>',null,
'0',GETDATE(),'1','False',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100018926','6392','9051',0,N'What''s the difference between a free membership and a Subscription Membership?',N'<p><table border="0" cellpadding="2" cellspacing="0" width="592" id="table1">
	<tr>
	  <td width="2%" valign="top"><strong>Q.</strong></td>
	  <td><strong>What''s the difference between a free membership and a Subscription Membership? </strong></td>
	</tr>
	<tr>
	  <td valign="top">A.</td>
	  <td>
 
<p>With your free membership, you can post a profile and photos, search and view other members'' profiles, receive and reply to instant messages when you''re online, send Smiles, view message boards, send E-cards, use Secret Admirer, read dating advice and more.</p>

<p>When you upgrade to a subscription, you can also send, read and reply to email, send instant messages, post replies and topics on message boards, personalize E-cards and mingle in the chat rooms.</p>

<p>To upgrade to a Subscription Membership at our low monthly rate, go to any of the <a href="((PLSECURELINK))&PRTID=17">Subscribe</a> links around the site.</p>
</td></tr></table></p>',null,
'1',GETDATE(),'1','True',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100018929','6404','9051',0,N'Why should I get a Subscription Membership?',N'<p><table border="0" cellpadding="2" cellspacing="0" width="592" id="table1">
	<tr>
	  <td width="2%" valign="top"><strong>Q.</strong></td>
	  <td><strong>Why should I get a Subscription Membership?</strong></td>
	</tr>
	<tr>
	  <td valign="top">A.</td>
	  <td>
 
<p>As a Subscription Member, you have total access to every communication tool on the site, including email, instant messaging, message boards, E-cards and chat. What’s more, being a Subscription Member sends a message to other members that you''re serious about finding a relationship. A three-month subscription costs less than a night out on the town, so it’s an easy fit for even the tightest budgets.</p>

<p>To upgrade to a Subscription Membership at our low monthly rate, go to any of the <a href="((PLSECURELINK))&PRTID=17">Subscribe</a> links around the site.</p>

</td></tr></table></p>',null,
'2',GETDATE(),'1','False',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100018932','6388','9051',0,N'What are my payment options?',N'<p><table border="0" cellpadding="2" cellspacing="0" width="592" id="table1">
	<tr>
	  <td width="2%" valign="top"><strong>Q.</strong></td>
	  <td><strong>What are my payment options?</strong></td>
	</tr>
	<tr>
	  <td valign="top">A.</td>
	  <td>
 
<p>You can <a href="((PLSECURELINK))&PRTID=17">subscribe</a> online with a credit card or check, or you can mail in a check or money order.</p>
   
<p>You can also subscribe over the phone by calling our toll-free number: ((CSSUPPORTPHONENUMBER)). If outside the U.S. and Canada, please contact our <a href="http://www.spark.net/contact.htm">Beverly Hills Headquarters.</a></p>

</td></tr></table></p>',null,
'4',GETDATE(),'1','False',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100018935','6400','9051',0,N'Which credit cards do you accept?',N'<p><table border="0" cellpadding="2" cellspacing="0" width="592" id="table1">
	<tr>
	  <td width="2%" valign="top"><strong>Q.</strong></td>
	  <td><strong>Which credit cards do you accept?</strong></td>
	</tr>
	<tr>
	  <td valign="top">A.</td>
	  <td>
 
<p>We accept: </p>
  
<ul>
<li>American Express </li>
<li>Discover </li>
<li>Master Card </li>
<li>Visa </li>
</ul>

</td></tr></table></p>',null,
'5',GETDATE(),'1','False',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100018938','6386','9051',0,N'I''m having trouble using my credit card. What should I do?',N'<p><table border="0" cellpadding="2" cellspacing="0" width="592" id="table1">
	<tr>
	  <td width="2%" valign="top"><strong>Q.</strong></td>
	  <td><strong>I''m having trouble using my credit card. What should I do?</strong></td>
	</tr>
	<tr>
	  <td valign="top">A.</td>
	  <td>
 
<p>When making your purchase on the credit card screen, the transaction is not complete until a confirmation appears.  Wait for the confirmation before visiting another page or closing your browser window. Feel free to call our customer care team at ((CSSUPPORTPHONENUMBER)). If outside the U.S. and Canada, please contact our <a href="http://www.spark.net/contact.htm">Beverly Hills Headquarters.</a> We are here to make sure you are successful on ((BRANDNAME)).</p>

</td></tr></table></p>',null,
'6',GETDATE(),'1','False',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100018941','6398','9051',0,N'Where do I mail my check or money order when I subscribe?',N'<p><table border="0" cellpadding="2" cellspacing="0" width="592" id="table1">
	<tr>
	  <td width="2%" valign="top"><strong>Q.</strong></td>
	  <td><strong>Where do I mail my check or money order when I subscribe?</strong></td>
	</tr>
	<tr>
	  <td valign="top">A.</td>
	  <td> 
  
<p>You can mail your check or money order (in U.S. funds) to:</p>
<p>((PLDOMAIN))<br>
Attn: BILLING<br>
8383 Wilshire Boulevard, Suite 800<br>
Beverly Hills, CA 90211<br>
USA</p>

<p>Make all checks and money orders payable to: ((PLDOMAIN))</p>

<p>Please include your full name, registered username, registered email address and member number to ensure that we credit payment to the correct membership.</p>

</td></tr></table></p>',null,
'7',GETDATE(),'1','False',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100018944','6402','9051',0,N'Why isn''t my billing information being accepted or saved when I enter it?',N'<p><table border="0" cellpadding="2" cellspacing="0" width="592" id="table1">
	<tr>
	  <td width="2%" valign="top"><strong>Q.</strong></td>
	  <td><strong>Why isn''t my billing information being accepted or saved when I enter it? </strong></td>
	</tr>
	<tr>
	  <td valign="top">A.</td>
	  <td> 
  
<p>There may be a few reasons why you can''t enter or save your billing information:</p>

<p><ul><li>Incorrect/invalid credit card number</li>
<li>Expired credit card </li>
<li>Credit card over limit </li>
<li>Incorrect/invalid online check information </li></ul></p>

<p>Try re-entering your information or try using a different payment method. If you continue to have problems, please feel free to call our toll-free Customer Care number: ((CSSUPPORTPHONENUMBER)). If outside the U.S. and Canada, please contact our <a href="http://www.spark.net/contact.htm">Beverly Hills Headquarters.</a></p>

</td></tr></table></p>',null,
'8',GETDATE(),'1','False',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100018947','6406','9051',0,N'Why was my subscription automatically renewed?',N'<p><table border="0" cellpadding="2" cellspacing="0" width="592" id="table1">
	<tr>
	  <td width="2%" valign="top"><strong>Q.</strong></td>
	  <td><strong>Why was my subscription automatically renewed?</strong></td>
	</tr>
	<tr>
	  <td valign="top">A.</td>
	  <td> 
  
<p>If you pay by credit card, you are charged immediately for the subscription plan you selected. When the subscription plan ends, we automatically renew your Premium Membership each month at the low monthly rate, unless you choose a different subscription plan or end your subscription before it runs out. For more detailed information, please review our <a href="/Applications/Article/ArticleView.aspx?CategoryID=1948&ArticleID=6498&HideNav=True#purchase">Terms and Conditions of Purchase</a>.</p>

</td></tr></table></p>',null,
'9',GETDATE(),'1','False',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100018950','6396','9051',0,N'When will my subscription expire?',N'<p><table border="0" cellpadding="2" cellspacing="0" width="592" id="table1">
	<tr>
	  <td width="2%" valign="top"><strong>Q.</strong></td>
	  <td><strong>When will my subscription expire? </strong></td>
	</tr>
	<tr>
	  <td valign="top">A.</td>
	  <td> 
  
<p>To find out when your subscription expires, go to <a href="/Applications/Subscription/History.aspx">Account Information</a> in <a href="/Applications/MemberServices/MemberServices.aspx">Member Services</a> or click Account in the top menu.</p>

</td></tr></table></p>',null,
'10',GETDATE(),'1','False',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100018953','6410','9051',0,N'Will my profile still show up if I end my subscription?',N'<p><table border="0" cellpadding="2" cellspacing="0" width="592" id="table1">
	<tr>
	  <td width="2%" valign="top"><strong>Q.</strong></td>
	  <td><strong>Will my profile still show up if I end my subscription? </strong></td>
	</tr>
	<tr>
	  <td valign="top">A.</td>
	  <td> 
  
<p>Yes, if you end your subscription, your profile stays up on the site for free. You can keep it up as long as you want and become a Subscription Member again when you see someone you''d like to connect with.</p>

</td></tr></table></p>',null,
'11',GETDATE(),'1','False',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100018956','6394','9051',0,N'If I remove my profile, will it end my subscription?',N'<p><table border="0" cellpadding="2" cellspacing="0" width="592" id="table1">
	<tr>
	  <td width="2%" valign="top"><strong>Q.</strong></td>
	  <td><strong>If I remove my profile, will it end my subscription?</strong></td>
	</tr>
	<tr>
	  <td valign="top">A.</td>
	  <td> 
  
<p>No. Once you purchase a subscription, it will remain active for the remainder of your subscription plan.</p>

</td></tr></table></p>',null,
'12',GETDATE(),'1','False',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100018959','6380','9051',0,N'Can I put my subscription on hold?',N'<p><table border="0" cellpadding="2" cellspacing="0" width="592" id="table1">
	<tr>
	  <td width="2%" valign="top"><strong>Q.</strong></td>
	  <td><strong>Can I put my subscription on hold?</strong></td>
	</tr>
	<tr>
	  <td valign="top">A.</td>
	  <td> 
  
<p>No. Once you purchase a subscription, it will remain active for the remainder of your subscription plan.</p>

</td></tr></table></p>',null,
'13',GETDATE(),'1','False',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100018962','6384','9051',0,N'Can I still read and respond to messages if I cancel my Subscription Membership?',N'<p><table border="0" cellpadding="2" cellspacing="0" width="592" id="table1">
	<tr>
	  <td width="2%" valign="top"><strong>Q.</strong></td>
	  <td><strong>Can I still read and respond to messages if I cancel my Subscription Membership?</strong></td>
	</tr>
	<tr>
	  <td valign="top">A.</td>
	  <td> 
  
<p>If you cancel your Subscription Membership, you will not be refunded the unused portion of membership fees paid for your subscription term.  However, all the membership benefits remain in effect until the term expires.  For example, if you purchase a one-month subscription on June 1st and cancel on June 21st, you may continue to send email and instant messages until the end of June.</p>

<p>Once your subscription term is over, you will no longer be able to send or receive email.  With a free membership, you may still reply to instant messages, but not initiate a conversation.</p>

</td></tr></table></p>',null,
'14',GETDATE(),'1','False',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100018965','6408','9051',0,N'Can I still send email and instant messages if I cancel my subscription today?',N'<p><table border="0" cellpadding="2" cellspacing="0" width="592" id="table1">
	<tr>
	  <td width="2%" valign="top"><strong>Q.</strong></td>
	  <td><strong>Can I still send email and instant messages if I cancel my subscription today?</strong></td>
	</tr>
	<tr>
	  <td valign="top">A.</td>
	  <td> 
  
<p>If you cancel your Subscription Membership in the middle of your subscription plan, you can continue to use all Subscription Member benefits until your subscription plan ends. For example, if you subscribed for one month on June 1st and you cancelled your subscription on June 21st, you could continue to send email and instant messages through June 30th.</p>

</td></tr></table></p>',null,
'15',GETDATE(),'1','False',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100018968','6382','9051',0,N'If I bought a 3-month subscription and cancel after one month, can I get a refund for the remaining two months?',N'<p><table border="0" cellpadding="2" cellspacing="0" width="592" id="table1">
	<tr>
	  <td width="2%" valign="top"><strong>Q.</strong></td>
	  <td><strong>If I bought a 3-month subscription and cancel after one month, can I get a refund for the remaining two months?</strong></td>
	</tr>
	<tr>
	  <td valign="top">A.</td>
	  <td> 
  
<p>No, we do not prorate subscriptions. If you fall in love and cancel after your first month, your Subscription Membership will stay active for the rest of the subscription plan.</p>

</td></tr></table></p>',null,
'16',GETDATE(),'1','False',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100018971','100001338','9051',0,N'What are Premium Services?',N'<p><table border="0" cellpadding="2" cellspacing="0" width="592" id="table1">
	<tr>
	  <td width="2%" valign="top"><strong>Q.</strong></td>
	  <td><strong>What are Premium Services?</strong></td>
	</tr>
	<tr>
	  <td valign="top">A.</td>
	  <td> 
  
<p>Premium Services include two features, Member Spotlight and Profile Highlight, which greatly increase your visibility on the site so you''ll receive more emails, IMs, Flirts and possibly dates! They''re a great way to fast track your love life and get more attention from the people you want to meet! </p>

</td></tr></table></p>',null,
'1',GETDATE(),'1','False',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100018974','100001341','9051',0,N'What is Member Spotlight?',N'<p><table border="0" cellpadding="2" cellspacing="0" width="592" id="table1">
	<tr>
	  <td width="2%" valign="top"><strong>Q.</strong></td>
	  <td><strong>What is Member Spotlight?</strong></td>
	</tr>
	<tr>
	  <td valign="top">A.</td>
	  <td> 
  
<p>Member Spotlight prominently presents your profile to the members you select based on age, gender and where they live. Your profile will be featured at the top of the homepage and first in "Your Matches" so you’ll get tons of attention from the people you’re looking to meet. Also, only "Spotlight" profiles get a "You both like" box that appears to members with whom you share common interests.</p>

</td></tr></table></p>',null,
'2',GETDATE(),'1','False',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100018977','100001344','9051',0,N'How can I select who my Member Spotlight profile is featured to?',N'<p><table border="0" cellpadding="2" cellspacing="0" width="592" id="table1">
	<tr>
	  <td width="2%" valign="top"><strong>Q.</strong></td>
	  <td><strong>How can I select who my Member Spotlight profile is featured to?</strong></td>
	</tr>
	<tr>
	  <td valign="top">A.</td>
	  <td> 
  
<p>You can select and change your preferences on the Premium Services Settings page:</p>
<ul>
	<li>Go to the Premium Services Settings page</li>
	<li>Select which gender you''d like to be featured to</li>
	<li>Select an age range</li>
	<li>Choose which country, region and city you want to be featured to</li>
	<li>Click "Update My Settings" and you’re done!</li>
</ul>
</td></tr></table></p>',null,
'3',GETDATE(),'1','False',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100018980','100001347','9051',0,N'On a Spotlighted Member''s Profile, what does "You both like" mean?',N'<p><table border="0" cellpadding="2" cellspacing="0" width="592" id="table1">
	<tr>
	  <td width="2%" valign="top"><strong>Q.</strong></td>
	  <td><strong>On a Spotlighted Member''s Profile, what does "You both like" mean?</strong></td>
	</tr>
	<tr>
	  <td valign="top">A.</td>
	  <td> 
  
<p>This box tells you which interests you have in common, making it easier to see if you''re a good match.</p>

</td></tr></table></p>',null,
'4',GETDATE(),'1','False',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100018983','100001350','9051',0,N'What is Profile Highlight?',N'<p><table border="0" cellpadding="2" cellspacing="0" width="592" id="table1">
	<tr>
	  <td width="2%" valign="top"><strong>Q.</strong></td>
	  <td><strong>What is Profile Highlight?</strong></td>
	</tr>
	<tr>
	  <td valign="top">A.</td>
	  <td> 
  
<p>Highlighted profiles have an eye-catching tint that makes you stand out! You''ll shine brighter in search results, other people''s matches and in the Photo Gallery, increasing the chances of receiving messages from the people you''d like to meet!</p>

</td></tr></table></p>',null,
'5',GETDATE(),'1','False',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100018986','100001353','9051',0,N'How can I get Premium Services?',N'<p><table border="0" cellpadding="2" cellspacing="0" width="592" id="table1">
	<tr>
	  <td width="2%" valign="top"><strong>Q.</strong></td>
	  <td><strong>How can I get Premium Services?</strong></td>
	</tr>
	<tr>
	  <td valign="top">A.</td>
	  <td> 
  
<p><a href="((PLSECURELINK))&PRTID=17">Subscribe now!</a> This feature is available to all members; if you are a subscriber you may upgrade your subscription to one including Premium Services. For further questions, please call Customer Service at ((CSSUPPORTPHONENUMBER)).</p>

</td></tr></table></p>',null,
'6',GETDATE(),'1','False',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100018989','100001356','9051',0,N'How do I enable/disable Premium Services?',N'<p><table border="0" cellpadding="2" cellspacing="0" width="592" id="table1">
	<tr>
	  <td width="2%" valign="top"><strong>Q.</strong></td>
	  <td><strong>How do I enable/disable Premium Services?</strong></td>
	</tr>
	<tr>
	  <td valign="top">A.</td>
	  <td> 
  
<p>You can enable or disable Premium Services on the Premium Services Settings page:</p>

<ul>
	<li>Go to the <a href="/Applications/MemberServices/PremiumServiceSettings.aspx">Premium Service Settings</a> page</li>
	<li>Click "Enable" to turn a feature on or "Disable" to turn either feature off</li>
	<li>Click "Update My Settings" and you''re done</li>
</ul>

<p>Disabling Premium Settings does not remove it from your subscription or affect your auto-renewal settings. If you wish to cancel the feature, please see: <a href="/Applications/Article/ArticleView.aspx?CategoryID=1000543&amp;ArticleID=100001365">How do I remove Premium Services feature from my subscription?</a></p>

</td></tr></table></p>',null,
'7',GETDATE(),'1','False',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100018992','100001359','9051',0,N'Are Premium Services included in my auto-renewal?',N'<p><table border="0" cellpadding="2" cellspacing="0" width="592" id="table1">
	<tr>
	  <td width="2%" valign="top"><strong>Q.</strong></td>
	  <td><strong>Are Premium Services included in my auto-renewal?</strong></td>
	</tr>
	<tr>
	  <td valign="top">A.</td>
	  <td> 
  
<p>Yes, when your Subscription automatically renews, it will continue to include Premium Services.</p>

</td></tr></table></p>',null,
'8',GETDATE(),'1','False',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100018995','100001362','9051',0,N'Can I purchase Premium Services separately?',N'<p><table border="0" cellpadding="2" cellspacing="0" width="592" id="table1">
	<tr>
	  <td width="2%" valign="top"><strong>Q.</strong></td>
	  <td><strong>Can I purchase Premium Services separately?</strong></td>
	</tr>
	<tr>
	  <td valign="top">A.</td>
	  <td> 
  
<p>No, Member Spotlight and Profile Highlight are only available together, but you may choose to turn each feature on or off whenever you like.</p>

</td></tr></table></p>',null,
'9',GETDATE(),'1','False',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100018998','100001365','9051',0,N'How do I remove Premium Services from my subscription?',N'<p><table border="0" cellpadding="2" cellspacing="0" width="592" id="table1">
	<tr>
	  <td width="2%" valign="top"><strong>Q.</strong></td>
	  <td><strong>How do I remove Premium Services from my subscription?</strong></td>
	</tr>
	<tr>
	  <td valign="top">A.</td>
	  <td> 
  
<p>To remove Premium Services, you must cancel your subscription, then re-subscribe with a plan that does not include Premium Services. For further questions, please call Customer Service at ((CSSUPPORTPHONENUMBER)).</p>

</td></tr></table></p>',null,
'10',GETDATE(),'1','False',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100019001','100001368','9051',0,N'Can I add Premium Services to my existing subscription?',N'<p><table border="0" cellpadding="2" cellspacing="0" width="592" id="table1">
	<tr>
	  <td width="2%" valign="top"><strong>Q.</strong></td>
	  <td><strong>Can I add Premium Services to my existing subscription?</strong></td>
	</tr>
	<tr>
	  <td valign="top">A.</td>
	  <td> 
  
<p>Yes! Go to <a href="((PLSECURELINK))&PRTID=17">Subscribe</a> and upgrade your subscription to one including Premium Services. For further questions, please call Customer Service at ((CSSUPPORTPHONENUMBER)).</p>

</td></tr></table></p>',null,
'11',GETDATE(),'1','False',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100019004','100000987','9051',0,N'How do I update or change the credit card on my JDate account?',N'<p><table border="0" cellpadding="2" cellspacing="0" width="592" id="table1">
	<tr>
	  <td width="2%" valign="top"><strong>Q.</strong></td>
	  <td><strong>How do I update or change the credit card on my ((BRANDNAME)) account?</strong></td>
	</tr>
	<tr>
	  <td valign="top">A.</td>
	  <td>

<p>You can update your credit card information on the payment profile page.</p>

<p>To update your current card:</p>

<ul>
  <li>Click "Update Current Card"</li>
  <li>Enter your updated credit card information into the appropriate fields</li>
  <li>When you''re finished, click "Update" and your new account information will be updated</li>
</ul>

<p>To enter a new card:</p>

<ul>
  <li>Click "Use Different Card"</li>
  <li>Enter the new card information into the appropriate fields</li>
  <li>Click "Save This Card"</li>
</ul>

<p>Your new credit card information will take effect in the next billing cycle. To establish the new card on your account, it will be authorized by ((BRANDNAME)) for $1. This authorization will only appear on your account as a pending transaction and you will not be charged. If your card fails the authorization process, your former card will act as the default billing method for your account.</p>

</td></tr></table></p>',null,
'0',GETDATE(),'1','True',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100019007','100000990','9051',0,N'If I enter a new credit card, when will it be billed?',N'<p><table border="0" cellpadding="2" cellspacing="0" width="592" id="table1">
	<tr>
	  <td width="2%" valign="top"><strong>Q.</strong></td>
	  <td><strong>If I enter a new credit card, when will it be billed?</strong></td>
	</tr>
	<tr>
	  <td valign="top">A.</td>
	  <td>

<p>Your new credit card information will take effect in the next billing cycle.</p>

</td></tr></table></p>',null,
'0',GETDATE(),'1','False',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100019010','100000993','9051',0,N'I entered a new credit card and there was a $1 authorization. Please explain.',N'<p><table border="0" cellpadding="2" cellspacing="0" width="592" id="table1">
	<tr>
	  <td width="2%" valign="top"><strong>Q.</strong></td>
	  <td><strong>I entered a new credit card and there was a $1 authorization. Please explain.</strong></td>
	</tr>
	<tr>
	  <td valign="top">A.</td>
	  <td>

<p>To establish the new card on your account, it will be authorized by ((BRANDNAME)) for $1. This charge will not be reflected on your credit card account balance.</p>

</td></tr></table></p>',null,
'0',GETDATE(),'1','False',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100019013','100000996','9051',0,N'What happens if my credit card fails the authorization?',N'<p><table border="0" cellpadding="2" cellspacing="0" width="592" id="table1">
	<tr>
	  <td width="2%" valign="top"><strong>Q.</strong></td>
	  <td><strong>What happens if my credit card fails the authorization?</strong></td>
	</tr>
	<tr>
	  <td valign="top">A.</td>
	  <td>

<p>If your card fails the authorization process your former card will act as the default billing method for your account.</p>

</td></tr></table></p>',null,
'0',GETDATE(),'1','False',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100019016','100000160','9051',0,N'What is You''re Searching for Each Other?',N'<p>
<table border="0" cellpadding="2" cellspacing="0" width="529" id="table1">
	<tr>
		<td width="2%" valign="top"><strong>Q.</strong></td>
		<td><strong>What is You''re Searching for Each Other?</strong></td>
	</tr>
	<tr>
		<td valign="top">A.</td>
		<td>
			<p>You''re Searching for Each Other is a search tool that displays the members who not only match your search preferences, but are also looking for someone just like you. In a nutshell, they are the people in Your Matches who also have you as one of their matches. You already have a lot in common with these members so they’re probably your best bet for creating a lasting relationship. Start communicating with them and see what happens!</p> 
		</td>
	</tr>
</table>
</p>',null,
'0',GETDATE(),'1','True',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100019019','100001494','9051',0,N'How do I filter out the members without photos in You''re Searching for Each Other?',N'<p>
<table border="0" cellpadding="2" cellspacing="0" width="529" id="table1">
	<tr>
		<td width="2%" valign="top"><strong>Q.</strong></td>
		<td><strong>How do I filter out the members without photos in You''re Searching for Each Other?</strong></td>
	</tr>
	<tr>
		<td valign="top">A.</td>
		<td>
			<p>Simply check the box that says, “Display members with photos only.”</p> 
		</td>
	</tr>
</table>
</p>',null,
'0',GETDATE(),'1','False',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100019022','100000191','9051',0,N'How can I modify my You''re Searching for Each Other results?',N'<p>
<table border="0" cellpadding="2" cellspacing="0" width="529" id="table1">
	<tr>
		<td width="2%" valign="top"><strong>Q.</strong></td>
		<td><strong>How can I modify my You''re Searching for Each Other results?</strong></td>
	</tr>
	<tr>
		<td valign="top">A.</td>
		<td>
			<p>You will receive different results whenever you change your profile, preferences, personal info or interests. Your best bet is to completely fill out your profile so you''ll be matched with the best person for you.</p> 
		</td>
	</tr>
</table>
</p>',null,
'0',GETDATE(),'1','False',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100019025','100001440','9051',0,N'What is the Keyword Search feature?',N'<p><table border="0" cellpadding="2" cellspacing="0" width="529" id="table1">
	 <tr>
		<td width="2%" valign="top"><strong>Q.</strong></td>
		<td><strong>What is the Keyword Search feature?</strong></td>
	</tr>
	<tr>
		<td valign="top">A.</td>
		<td><p>Keyword Search allows you to search for a specific term or phrase to find the members with that same term or phrase written in their profile. The terms you enter should be as wide and varied as you like in order to find members with specific hobbies, personal qualities, professions, entertainment tastes and more!</p> 
 

<p><strong>Still have questions?</strong><br>
<a href="/Applications/ContactUs/ContactUs.aspx">Email</a> us or call toll-free at: ((CSSUPPORTPHONENUMBER)). If outside the U.S. and Canada, please contact our <a href="http://www.spark.net/contact.htm">Beverly Hills Headquarters.</a></p> 
  
	</td></tr></table></p>',null,
'0',GETDATE(),'1','False',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100019028','100000103','9051',0,N'How do I perform a Keyword Search?',N'<p><table border="0" cellpadding="2" cellspacing="0" width="529" id="table1">
	 <tr>
		<td width="2%" valign="top"><strong>Q.</strong></td>
		<td><strong>How do I perform a Keyword Search?</strong></td>
	</tr>
	<tr>
		<td valign="top">A.</td>
		<td><p>It’s easy! Think about what you’re really looking for in a person. Do you have a specific interest or hobby that you’re really passionate about (e.g. political cause, career, a band or art movement)? Is there a “type” of person you’re looking for (e.g. nurturing, sarcastic, loyal)? Try as many terms as you like, the search can go as deep as your imagination allows.</p> 
		<p>How to Keyword Search:</p>
		<ol>
		    <li>Enter a term or phrase in the Keyword Search box.</li>
		    <li>Enter your gender, the gender you’re looking to meet, and the age range you’d like to search. You can leave the “Age” setting in the “All” default setting to get  the largest amount of search results. The “Near” setting selects the geographic area you’d like to search. You may enter any city, state, country or zip code.</li>
		    <li>Click “Search” and see which profiles appear below the search box!</li>
		</ol>
 

<p><strong>Still have questions?</strong><br>
<a href="/Applications/ContactUs/ContactUs.aspx">Email</a> us or call toll-free at: ((CSSUPPORTPHONENUMBER)). If outside the U.S. and Canada, please contact our <a href="http://www.spark.net/contact.htm">Beverly Hills Headquarters.</a></p> 
  
	</td></tr></table></p>',null,
'0',GETDATE(),'1','False',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100019031','100000106','9051',0,N'What if my Keyword Search doesn’t return any results?',N'<p><table border="0" cellpadding="2" cellspacing="0" width="529" id="table1">
	 <tr>
		<td width="2%" valign="top"><strong>Q.</strong></td>
		<td><strong>What if my Keyword Search doesn’t return any results?</strong></td>
	</tr>
	<tr>
		<td valign="top">A.</td>
		<td><p>If your search doesn’t yield any results:</p> 
		<ul>
		    <li>Try searching one of the popular terms in the box below.</li>
		    <li>Check your spelling and try again.</li>
		    <li>Enter a synonym or similar phrase and see who matches.</li>
		</ul>
 

<p><strong>Still have questions?</strong><br>
<a href="/Applications/ContactUs/ContactUs.aspx">Email</a> us or call toll-free at: ((CSSUPPORTPHONENUMBER)). If outside the U.S. and Canada, please contact our <a href="http://www.spark.net/contact.htm">Beverly Hills Headquarters.</a></p> 
  
	</td></tr></table></p>',null,
'0',GETDATE(),'1','False',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100019034','6442','9051',0,N'How do I look for members?',N'<p><table border="0" cellpadding="2" cellspacing="0" width="592" id="table1">
	 <tr>
		<td width="2%" valign="top"><strong>Q.</strong></td>
		<td><strong>How do I look for members?</strong></td>
	</tr>
	<tr>
		<td valign="top">A.</td>
		<td>
<p>To search for members, first go to <a href="/Applications/Search/SearchPreferences.aspx">Preferences</a> under Search in the top menu and tell us what you''re looking for. Choose between various characteristics, including: height, age, marital status, education level, location, and distance from your home.</p>  
 
<p>Next, click <a href="/Applications/Search/SearchResults.aspx">Search</a> for your results. Click any member''s username to view their full profile. To change your preferences and see different matches, go back to <a href="/Applications/Search/SearchPreferences.aspx">Preferences</a>, edit and click Save Preferences.</p>
 
 
<p><strong>Still have questions?</strong><br>
<a href="/Applications/ContactUs/ContactUs.aspx">Email</a> us or call toll-free at: ((CSSUPPORTPHONENUMBER)). If outside the U.S. and Canada, please contact our <a href="http://www.spark.net/contact.htm">Beverly Hills Headquarters.</a></p> 
  
	</td></tr></table></p>',null,
'1',GETDATE(),'1','True',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100019037','100000136','9051',0,N'How do I filter out the members without photos in Who’s Searching for You?',N'<p>
	<table border="0" cellpadding="2" cellspacing="0" width="592" id="table1">
		<tr>
			<td width="2%" valign="top"><strong>Q.</strong></td>
			<td><strong>How do I filter out the members without photos in Who''s Searching for You?</strong></td>
		</tr>
		<tr>
			<td valign="top">A.</td>
			<td>
				<p>Simply check the box that says, “Show profiles with photos only.”</p> 
			</td>
		</tr>
	</table>
</p>',null,
'0',GETDATE(),'1','False',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100019040','100000170','9051',0,N'How can I modify my Who’s Searching for You? results?',N'<p>
	<table border="0" cellpadding="2" cellspacing="0" width="592" id="table1">
		<tr>
			<td width="2%" valign="top"><strong>Q.</strong></td>
			<td><strong>How can I modify my Who''s Searching for You? results?</strong></td>
		</tr>
		<tr>
			<td valign="top">A.</td>
			<td>
				<p>Remember, the BBWPersonalsPlus users you see in Who''s Searching for You are those who are looking for someone like you.  As a result, you will receive different results if you change or update your profile. By making changes to your personal info, interests or hobbies, you will find the members who have set their preferences to search for someone with your specific qualities. Your best bet is to completely fill out your profile, so you will be matched with the best person for you.  Remember, by updating your profile, not only will you see different results in Who''s Searching for You?, but other BBWPersonalsPlus users will see you in their primary search results as well.</p> 
			</td>
		</tr>
	</table>
</p>',null,
'0',GETDATE(),'1','False',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100019043','100000133','9051',0,N'What is Who''s Searching for You?&trade;',N'<p>
	<table border="0" cellpadding="2" cellspacing="0" width="592" id="table1">
		<tr>
			<td width="2%" valign="top"><strong>Q.</strong></td>
			<td><strong>What is Who''s Searching for You?&trade;</strong></td>
		</tr>
		<tr>
			<td valign="top">A.</td>
			<td>
				<p>Who''s Searching for You? finds BBWPersonalsPlus users looking to meet someone like you - you match their Search Preferences. The Members you will find in your Who''s Searching for You? results aren''t filtered by your preferences, and oftentimes, they will be people you have not seen before. This search option allows you to look beyond Your Matches to find people that you may not have previously considered. See someone who catches your eye? Send them a message, because chances are, you are exactly who they have been looking for!</p> 
			</td>
		</tr>
	</table>
</p>',null,
'0',GETDATE(),'1','False',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100019046','6446','9051',0,N'How do I view my search results?',N'<p><table border="0" cellpadding="2" cellspacing="0" width="592" id="table1">
	 <tr>
		<td width="2%" valign="top"><strong>Q.</strong></td>
		<td><strong>How do I view my search results?</strong></td>
	</tr>
	<tr>
		<td valign="top">A.</td>
		<td>
<p>After setting your Search Preferences, click <a href="/Applications/Search/SearchResults.aspx">Search</a> in the top menu to see your search results.  Click any member''s username to view their full profile.  To change your search settings and see different matches, click <a href="/Applications/Search/SearchPreferences.aspx">Preferences</a> in the drop-down Search menu, edit your settings, and click Save Preferences.</p>
 

<p><strong>Still have questions?</strong><br>
<a href="/Applications/ContactUs/ContactUs.aspx">Email</a> us or call toll-free at: ((CSSUPPORTPHONENUMBER)). If outside the U.S. and Canada, please contact our <a href="http://www.spark.net/contact.htm">Beverly Hills Headquarters.</a></p> 
  
	</td></tr></table></p>',null,
'2',GETDATE(),'1','False',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100019049','6440','9051',0,N'How do I change my search results?',N'<p><table border="0" cellpadding="2" cellspacing="0" width="529" id="table1">
	 <tr>
		<td width="2%" valign="top"><strong>Q.</strong></td>
		<td><strong>How do I change my search results?</strong></td>
	</tr>
	<tr>
		<td valign="top">A.</td>
		<td><p>You can change your search results by editing your search criteria on the Preferences page. Click <a href="/Applications/Search/SearchPreferences.aspx">Preferences</a> under Search in the top menu or click <a href="/Applications/Search/SearchPreferences.aspx">[edit]</a> from your search results page.</p> 
 

<!--<p><strong>Found someone you like?</strong> Become a Premium Member at a low monthly rate to send an email or instant message.</p> -->
 
<p><strong>Still have questions?</strong><br>
<a href="/Applications/ContactUs/ContactUs.aspx">Email</a> us or call toll-free at: ((CSSUPPORTPHONENUMBER)). If outside the U.S. and Canada, please contact our <a href="http://www.spark.net/contact.htm">Beverly Hills Headquarters.</a></p> 
  
	</td></tr></table></p>',null,
'3',GETDATE(),'1','False',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100019052','6444','9051',0,N'How do I keep members from knowing that I looked at their profile or Hot Listed them?',N'<p><table border="0" cellpadding="2" cellspacing="0" width="592" id="table1">
	 <tr>
		<td width="2%" valign="top"><strong>Q.</strong></td>
		<td><strong>How do I keep members from knowing that I looked at their profile or Hot Listed them?</strong></td>
	</tr>
	<tr>
		<td valign="top">A.</td>
		<td> 
<p>If you don''t want other members to know when you''ve looked at their profile 
              or Hot Listed them, go to <a href="/Applications/MemberServices/DisplaySettings.aspx">Show/Hide Profile</a> 
              in <a href="/Applications/MemberServices/MemberServices.aspx">Member Services</a> and select &quot;hide&quot; under &quot;Show/Hide When You View Or Hot List Members.&quot;</p>

<p>Keep in mind that members who are most active on the site keep their profile visible in all places because it increases their chances of connecting. You never know, someone special may be looking for you but will never find you if your profile is hidden.</p>

            <p>Hot Lists have been a real hit on our sites and have helped bring 
              some unsuspecting couples together. People even tell us that 
              they''ll contact a member simply because he or she looked at their profile. So before you hit &quot;hide,&quot; 
              give it a chance to work.</p> 
  
	</td></tr></table></p>',null,
'4',GETDATE(),'1','False',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100019055','6448','9051',0,N'Why do people want other members to know when they look at their profile or Hot List them?',N'<p><table border="0" cellpadding="2" cellspacing="0" width="592" id="table1">
	 <tr>
		<td width="2%" valign="top"><strong>Q.</strong></td>
		<td><strong>Why do people want other members to know when they look at their profile or Hot List them?</strong></td>
	</tr>
	<tr>
		<td valign="top">A.</td>
		<td> 
<p>Members who are really active on the site like to let other members know when they look at their profile or Hot List them.  It livens things up and often breaks the ice when it comes to emailing. </p>

<p>So many times we hear that a member contacted someone simply because that person had looked at their profile or Hot Listed them. Members also say that Hot Lists lead them to meet members they might not have met otherwise.</p>

<p>Think about it. If you saw that someone had Hot Listed you and you liked what you saw, wouldn''t you be more likely to send an email?
</p> 
  
	</td></tr></table></p>',null,
'5',GETDATE(),'1','False',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100019058','6304','9051',0,N'How do I contact someone that interests me?',N'<p>
	<table border="0" cellpadding="2" cellspacing="0" width="592" id="table1">
		<tr>
			<td width="2%" valign="top"><strong>Q.</strong></td>
			<td><strong>How do I contact someone that interests me?</strong></td>
		</tr>
		<tr>
			<td valign="top">A.</td>
			<td>
				<p>You can contact a member by sending an email, instant message or Flirt.</p>
					<ul>
						<li>To send an email, simply go to the person''s profile and click <img border="0" src="((image:icon_email.gif))" width="18" height="19" align="absmiddle" alt="Email me" hspace="3"> EMAIL. Type in your message and click Send.</li> 
						<li>If the person is online, you can send an instant message and start a conversation in real-time. The <img border="0" src="((image:icon_chat.gif))" width="29" height="19" align="absmiddle" alt="I''m Online! Chat with me" hspace="3"> ONLINE button flashes in the person''s profile if they''re logged in. Click it, and you''re on your way.</li>
					</ul>
				</p> 
			</td>
		</tr>
	</table>
</p>

<p>It''s free to send a Flirt, but to send an email or instant message, you''ll need to <a href="((PLSECURELINK))&PRTID=17">subscribe.</a> As a Subscriber you''ll have full access to all of the site''s communication tools. It costs less than a night on the town, so it''s well worth it. </p>',null,
'1',GETDATE(),'1','True',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100019061','100001317','9051',0,N'How do I verify my email address?',N'<p>
<table border="0" cellpadding="2" cellspacing="0" width="592" id="table1">
	<tr>
		 <td width="2%" valign="top"><strong>Q.</strong></td>
		<td><strong> How do I verify my email address?</strong></td>
	</tr>
	<tr>
		<td valign="top">A.</td>
		<td> 
		<p>To confirm your email address belongs to you, we automatically send a verification message to your personal email account. If you’re a current member changing your email, the message is sent to your new email address.</p>

		<p>To verify your email address:
			<ul>
			<li>Go to your personal email and open the verification message with a subject line that reads “Verify your email address to receive your emails”</li>
			<li>Click “Verify Your Email Address Now” </li>
			<li>Your email is now verified and you’re ready to go!</li>
			</ul>
		</p> 
		</td>
	</tr>
</table>
</p>',null,
'0',GETDATE(),'1','False',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100019064','100001320','9051',0,N'I clicked the “Verify Your Email Address Now” button and it didn’t work. What should I do?',N'<p>
<table border="0" cellpadding="2" cellspacing="0" width="592" id="table1">
	<tr>
		 <td width="2%" valign="top"><strong>Q.</strong></td>
		<td><strong>I clicked the “Verify Your Email Address Now” button and it didn’t work. What should I do?</strong></td>
	</tr>
	<tr>
		<td valign="top">A.</td>
		<td>
		<p>If the “Verify Your Email Address Now” button isn’t working for you:
			<ul>
			<li>Copy the blue code beneath the "Verify Your Email Address Now” button</li>
			<li>Paste it in the verification code text box</li>
			<li>Click “Enter” and you’re all set!</li>
			</ul>
		</p> 
		</td>
	</tr>
</table>
</p>',null,
'0',GETDATE(),'1','False',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100019067','100001323','9051',0,N'I haven’t received a verification email. How can I have one re-sent?',N'<p>
<table border="0" cellpadding="2" cellspacing="0" width="592" id="table1">
	<tr>
		 <td width="2%" valign="top"><strong>Q.</strong></td>
		<td><strong>I haven’t received a verification email. How can I have one re-sent?</strong></td>
	</tr>
	<tr>
		<td valign="top">A.</td>
		<td>
		<p>First make sure that the verification message didn’t go to your junk mail/ spam folder. To ensure that future emails aren’t sent to your junk mail/ spam folder <a href="/Applications/Article/ArticleView.aspx?CategoryID=1938&ArticleID=6312&RowNumber=1&eid=email-mn1ht1ab0">click here</a>. To have the verification message re-sent; click “Re-send verification email” and you’ll receive a new one shortly. Still having problems? Call Customer Service at ((CSSUPPORTPHONENUMBER)).
		</p> 
		</td>
	</tr>
</table>
</p>',null,
'0',GETDATE(),'1','False',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100019070','6308','9051',0,N'When someone contacts me, where does the message go and how do I respond?',N'<p><table border="0" cellpadding="2" cellspacing="0" width="592" id="table1">
	<tr>
		<td width="2%" valign="top"><strong>Q.</strong></td>
		 <td><strong>When someone contacts me, where does the message go and how do I respond?</strong></td>
	</tr>
	<tr>
		<td valign="top">A.</td>
		<td> 
<p>Once you''ve created your own profile, other members can start contacting you. We''ll let you know that you have messages by sending a notice to your personal email address. If you''re already a Subscription Member, you can pick up your messages from your onsite Inbox by clicking Messages in the top menu. If you decide to write back, click Reply, write your own message and send away.</p>
<p>Not yet a Subscription Member? Click Subscribe in the top menu for our low monthly rates.</p>
<p>All communications with other members stay onsite so you never have to give out personal information until you feel completely ready. Then when you are, you can exchange phone numbers and even meet in person.</p> 
</td></tr></table></p>',null,
'2',GETDATE(),'1','False',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100019073','6306','9051',0,N'Do I have to be a Subscription Member to read and reply to messages from other members?',N'<p><table border="0" cellpadding="2" cellspacing="0" width="592" id="table1">
	<tr>
		 <td width="2%" valign="top"><strong>Q.</strong></td>
		<td><strong> Do I have to be a Subscription Member to read and reply to messages from other members?</strong></td>
	</tr>
	<tr>
		<td valign="top">A.</td>
		<td> 
<p>Yes, you must be a Subscription Member to read and reply to messages. Not yet a Subscription Member? Click Subscribe in the top menu, and you can sign up at our low monthly rates.</p>

<p>Once you''ve subscribed, you can access your onsite <a href="/Applications/Email/MailBox.aspx">Inbox</a> by clicking Messages in the top menu. To respond to someone, simply open the message, click reply, write your message and send away. Your message will go straight into the member''s onsite Inbox, and we''ll notify them that it''s there.</p>

<p>All communications with other members stay onsite so you never have to give out any personal information until you feel completely ready. Then, when you are, you can exchange phone numbers and even meet in person.</p> 
</td></tr></table></p>',null,
'3',GETDATE(),'1','False',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100019076','6310','9051',0,N'Who can contact me?',N'<p><table border="0" cellpadding="2" cellspacing="0" width="592" id="table1">
	<tr>
		  <td width="2%" valign="top"><strong>Q.</strong></td>
		<td><strong>Who can contact me?</strong></td>
	</tr>
	<tr>
		<td valign="top">A.</td>
		<td> 
<p>Any Subscription Member can contact you.  When they do, the message goes straight into your onsite <a href="/Applications/Email/MailBox.aspx">Inbox</a> and we notify you at your personal email address. Likewise, if you reply, your message will go straight into their onsite Inbox.  All member-to-member contacts stay onsite, so you never have to disclose personal information until you choose to do so.</p> 

<p>You must be a Subscription Member to read and reply to messages. Still need to sign up? Click Subscribe in the top menu and become a Subscription Member at our low monthly rates.</p>

<p>Not interested in someone who wrote to you? We recommend replying with a friendly "no, thank you" note. If necessary, you can also prevent further messages by opening an email, IM or Flirt from the member and clicking the Block button. The member will be added to your Blocked Hot List.</p> 

         
</td></tr></table></p>',null,
'4',GETDATE(),'1','False',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100019079','6300','9051',0,N'How can I see who has contacted me?',N'<p><table border="0" cellpadding="2" cellspacing="0" width="592" id="table1">
	<tr>
		<td width="2%" valign="top"><strong>Q.</strong></td>
		<td><strong>How can I see who has contacted me?</strong></td>
	</tr>
	<tr>
		 <td valign="top">A.</td>
		<td> 
<p>All messages from other members go straight into your onsite <a href="/Applications/Email/MailBox.aspx">Inbox</a> and are kept there unless you choose to delete them. To read messages, login to the site and go to your Inbox by clicking Messages in the top menu. From each message, you can also click through to the member''s profile.</p>

<p>Each time you get a new message in your Inbox, we send an email notification to your personal email address to let you know it''s there. In addition, your <a href="/Applications/HotList/View.aspx?CategoryID=0">Hot Lists</a> can show you all members who have contacted you.</p> 
</td></tr></table></p>',null,
'5',GETDATE(),'1','False',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100019082','6302','9051',0,N'How can I see who I have contacted?',N'<p><table border="0" cellpadding="2" cellspacing="0" width="592" id="table1">
	<tr>
		<td width="2%" valign="top"><strong>Q.</strong></td>
		 <td><strong>How can I see who I have contacted?</strong></td>
	</tr>
	<tr>
		<td valign="top">A.</td>
		 <td>
<p>All messages that you send are kept in the Sent section of your onsite <a href="/Applications/Email/MailBox.aspx">Inbox</a> until you choose to delete them.</p>

<p>In addition, you can see a list of all the members you have contacted in your <a href="/Applications/HotList/View.aspx?CategoryID=0">Hot Lists</a>. </p> 
</td></tr></table></p>',null,
'6',GETDATE(),'1','False',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100019085','6326','9051',0,N'Why am I not getting very many responses from other members?',N'<p><table border="0" cellpadding="2" cellspacing="0" width="592" id="table1">
	<tr>
		 <td width="2%" valign="top"><strong>Q.</strong></td>
		<td><strong>Why am I not getting very many responses from other members?</strong></td>
	</tr>
	<tr>
		<td valign="top">A.</td>
		<td><p>If you''re not getting many responses, it might be a good time to spice up your <a href="/Applications/MemberProfile/ViewProfile.aspx">profile</a> and get personal with your emails. Here are a few suggestions to get you started, and you can also call our Customer Care team toll-free for further advice: ((CSSUPPORTPHONENUMBER)).</p>

<ul>
              <li><strong>Post photos in your profile.</strong> People like to 
                talk to people they can see, so the more photos you have up, the 
                more comfortable people will be emailing to you. In fact, most 
                members only search for and write to people with photos, and members with photos get contacted more frequently than members without.<br>
                <br>

<a href="/Applications/MemberProfile/MemberPhotoUpload.aspx">Your photos</a> should show more than just what you look like -- they should communicate your personality and the things that you love about your life! Ideally, a life that someone would want to be a part of.<br><br>

What should you show? Think fun, happy, warm, interesting, smiles... and think about the things that you love about your life (skiing, painting, traveling, playing with your dog, etc.). The first photo should be a headshot and the others can be more creative. Just remember to make them clear, recent and in focus!<br><br></li>

<li><strong>Make your essays more descriptive.</strong> Snappy essays are what it takes for you to stand out in the dating crowd and, more importantly, attract the people who are right for you.<br><br>

The key to writing great essays? It''s all in the details... in giving specific examples. No one wants to see boring lists of adjectives (nice, smart, funny, successful, attractive). They want specific examples of the things that make you different, interesting and fun to be with. Write like you talk. Pretend you''re hanging out with your best friends, telling them exactly what''s on your mind.<br><br>

Take a look at our list of <a href="/Applications/Article/ArticleView.aspx?CategoryID=1956&ArticleID=6426">Things To Write About</a> if you get really stuck.<br><br></li>

              <li><strong>Write great personal introductory emails.</strong> Although 
                &quot;Hey, you look cute, check out my profile,&quot; may sound 
                short and sweet, in fact, it really is a canned intro that is 
                easily dismissed. When contacting other members, remember to ask 
                yourself, &quot;Why am I writing to this person?&quot; and then 
                tell him/her. Flattery, writing about things you have in common, 
                and a sense of humor will get you everywhere.<br>
                <br>

Take a look at our 5 Step Email Guide for tips on <a href="/Applications/Article/ArticleView.aspx?CategoryID=2010&ArticleID=6222">How To Write Your First Email</a>.<br><br></li>

              <li><strong>Check your personal <a href="/Applications/Email/MailBox.aspx">Inbox</a> on the site.</strong> 
                There may be messages waiting there that you''re not aware of. If you have &quot;spam guard&quot; on your personal 
                email, our notices many not be reaching you. You 
                can set your personal email to recognize email from us so you 
                won''t miss future notices. See the Never Miss Email section in Member Services for instructions and check with your Internet Service Provider 
                (ISP) if you have additional questions.</li>
            </ul>

<p>Found someone you like? Ready to send emails?<br><a href="((PLSECURELINK))">Subscribe</a> today and get our low monthly rate.</p>
</td></tr></table></p>',null,
'7',GETDATE(),'1','False',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100019088','6298','9051',0,N'How can I decline contact?',N'<p><table border="0" cellpadding="2" cellspacing="0" width="592" id="table1">
	<tr>
		<td width="2%" valign="top"><strong>Q.</strong></td>
		<td><strong>How can I decline contact?</strong></td>
	 </tr>
	<tr>
		<td valign="top">A.</td>
		<td> 
<p>If you''re not interested in someone who sent you a message, we recommend replying 
              with a friendly &quot;no thank you&quot; note. You can also block a member 
              from sending additional messages by going to your Inbox, selecting a message from that member and clicking the Block button. The member will then be added to your Blocked Hot List.</p>

<p>To unblock a member, go to your Blocked Hot List, click Move in the member''s profile and select Unlist From Blocked in the drop-down menu. The member will be removed from your Blocked list and will be able to contact you again.</p>
</td></tr></table></p>',null,
'9',GETDATE(),'1','False',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100019091','100000083','9051',0,N'What happens when I block someone?',N'<p><table border="0" cellpadding="2" cellspacing="0" width="592" id="table1">
	<tr>
		<td width="2%" valign="top"><strong>Q.</strong></td>
		<td><strong>What happens when I block someone?</strong></td>
	</tr>
	 <tr>
		<td valign="top">A.</td>
		<td> 
			<p>When you block a member, he or she can no longer send you emails, IMs or Flirts.</p>
			<p>All blocked members can be found on your Blocked Hot List. If you would like to unblock a member, simply go to your Blocked list, click Move in the member’s profile and select Unlist from Blocked in the drop-down menu. The member will be removed from your Blocked list and will be able to contact you again.</p>
</td></tr></table></p>',null,
'10',GETDATE(),'1','False',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100019094','6312','9051',0,N'Why am I not getting all of my important email?',N'<p><table border="0" cellpadding="2" cellspacing="0" width="592" id="table1">
	<tr>
	  <td width="2%" valign="top"><strong>Q.</strong></td>
	  <td><strong>Why am I not getting all of my important email?</strong></td>
	</tr>
	<tr>
	  <td valign="top">A.</td>
 	  <td> 
  
<p>Don''t want to miss email from other members? Click your email provider below 
              and follow the simple instructions to keep ((PLDOMAIN)) email 
              out of your junk mail folder.</p>
  
<p><a href="/Applications/Article/ArticleView.aspx?CategoryID=1938&ArticleID=6318&RowNumber=1">HOTMAIL</a>&nbsp;&nbsp;&nbsp;
<a href="/Applications/Article/ArticleView.aspx?CategoryID=1938&ArticleID=6324&RowNumber=1">YAHOO!</a>&nbsp;&nbsp;&nbsp;
<a href="/Applications/Article/ArticleView.aspx?CategoryID=1938&ArticleID=6316&RowNumber=1">AOL</a>&nbsp;&nbsp;&nbsp;
<a href="/Applications/Article/ArticleView.aspx?CategoryID=1938&ArticleID=6320&RowNumber=1">MSN</a>&nbsp;&nbsp;&nbsp;
<a href="/Applications/Article/ArticleView.aspx?CategoryID=1938&ArticleID=6322&RowNumber=1">OTHER</a></p>
  
<p>If you have any questions, please contact our Customer Care team or your email provider.</p>

</td></tr></table>
</p>',null,
'10',GETDATE(),'1','False',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100019097','6316','9051',0,N'Why am I not getting all of my important email? AOL',N'<p><hr size="1" style="width: 592px;">
<a href="/Applications/Article/ArticleView.aspx?CategoryID=1938&ArticleID=6318&RowNumber=1">HOTMAIL</a>&nbsp;&nbsp;&nbsp;
<a href="/Applications/Article/ArticleView.aspx?CategoryID=1938&ArticleID=6324&RowNumber=1">YAHOO!</a>&nbsp;&nbsp;&nbsp;
<span class="copyright">AOL</span>&nbsp;&nbsp;&nbsp;
<a href="/Applications/Article/ArticleView.aspx?CategoryID=1938&ArticleID=6320&RowNumber=1">MSN</a>&nbsp;&nbsp;&nbsp;
<a href="/Applications/Article/ArticleView.aspx?CategoryID=1938&ArticleID=6322&RowNumber=1">OTHER</a></p>
 
<table border="0" cellpadding="2" cellspacing="0" width="592" id="table1">
        <tr> 
          <td colspan="2"><strong>AOL</strong></td>
        </tr>
        <tr> 
          <td colspan="2"><p>4 easy steps to keep ((PLDOMAIN)) email out 
              of your spam folder and in your AOL Inbox</p>
            <br> </td>
        </tr>
      </table>
		
      <table cellpadding="4" cellspacing="1" bgcolor="#DDDCD3" width="592">
        <tr> 
          <td colspan="2" class="medium"><strong>Step 1 - Add ((PLDOMAIN)) 
            to your AOL Address Book: </strong></td>
        </tr>
        <tr> 
          <td width="400" rowspan="2" valign="top" class="white"> <p>1. Go to 
              your AOL Mailbox, and in the &quot;Mail&quot; pull-down menu, click 
              &quot;Address Book.&quot;</p></td>
          <td class="white"><a href="javascript:launchWindow(''((image:Articles/scrn_importantEmail_AOL_01Full.jpg))'', ''Why am I not getting all of my important email? AOL'', ''610'', ''510'', '''')"><img src="((image:Articles/scrn_importantEmail_AOL_01.gif))" alt="Click for larger view" width="180" height="150" border="0" class="profileImageHover"></a></td>
        </tr>
        <tr> 
          <td class="white"> <a href="javascript:launchWindow(''((image:Articles/scrn_importantEmail_AOL_01Full.jpg))'', ''Why am I not getting all of my important email? AOL'', ''610'', ''510'', '''')"><img border="0" src="((image:icon_increaseSize.gif))" width="20" height="19" align="absmiddle" alt="Click for larger view" hspace="3"></a>Click 
            for larger view</td>
        </tr>
        <tr> 
          <td width="400" rowspan="2" valign="top" class="white"> <p>2. Click 
              &quot;Add.&quot;</p></td>
          <td class="white"><a href="javascript:launchWindow(''((image:Articles/scrn_importantEmail_AOL_02Full.jpg))'', ''Why am I not getting all of my important email? AOL'', ''610'', ''510'', '''')"><img src="((image:Articles/scrn_importantEmail_AOL_02.gif))" class="profileImageHover" width="180" height="150" border="0" alt="Click for larger view"></a></td>
        </tr>
        <tr> 
          <td class="white"><a href="javascript:launchWindow(''((image:Articles/scrn_importantEmail_AOL_02Full.jpg))'', ''Why am I not getting all of my important email? AOL'', ''610'', ''510'', '''')"><img border="0" src="((image:icon_increaseSize.gif))" width="20" height="19" align="absmiddle" alt="Click for larger view" hspace="3"></a>Click 
            for larger view</td>
        </tr>
        <tr> 
          <td width="400" rowspan="2" valign="top" class="white"> <p>3. In &quot;Other 
              E-mail&quot; add: YourMatches@Mail.((PLDOMAIN))</p>
            <p>4. Select this address as the &quot;Primary E-Mail&quot; Address.</p>
            <p>5. Click &quot;Save.&quot; </p></td>
          <td class="white"><a href="javascript:launchWindow(''((image:Articles/scrn_importantEmail_AOL_03Full.jpg))'', ''Why am I not getting all of my important email? AOL'', ''610'', ''510'', '''')"><img src="((image:Articles/scrn_importantEmail_AOL_03.gif))" class="profileImageHover" width="180" height="150" border="0" alt="Click for larger view"></a></td>
        </tr>
        <tr> 
          <td class="white"><a href="javascript:launchWindow(''((image:Articles/scrn_importantEmail_AOL_03Full.jpg))'', ''Why am I not getting all of my important email? AOL'', ''610'', ''510'', '''')"><img border="0" src="((image:icon_increaseSize.gif))" width="20" height="19" align="absmiddle" alt="Click for larger view" hspace="3"></a>Click 
            for larger view</td>
        </tr>
        <tr> 
          <td colspan="2" valign="top" class="white"> <p>6. Repeat to add each 
              of our other email addresses:</p>
<ol>
<li><strong>Click@Mail.((PLDOMAIN))</strong></li>
<li><strong>Communications@Mail.((PLDOMAIN))</strong></li>
<li><strong>MemberServices@Mail.((PLDOMAIN))</strong></li>
<li><strong>VerifyEmail@Mail.((PLDOMAIN))</strong></li>
<li><strong>YourMatches@Mail.((PLDOMAIN))</strong></li>
<li><strong>Photos@Mail.((PLDOMAIN))</strong></li>
<li><strong>Comments@Mail.((PLDOMAIN))</strong></li>
</strong></li>
</ol></td>
        </tr>
        <tr> 
          <td colspan="2" valign="top" class="medium"><strong>Step 2 - DO NOT 
            click &quot;Report Spam&quot; for any email from ((PLDOMAIN))</strong></td>
        </tr>
        <tr> 
          <td width="400" rowspan="2" valign="top" class="white"> <p>If you block 
              just one email from ((PLDOMAIN)), you may not see ANY email 
              from us, including the ones you want to get!</p>
            <p>What happens when you send something to Spam? From that moment 
              on, all email from that ((PLDOMAIN)) address will be blocked 
              from appearing in your AOL Mailbox.<br>
              <br>
            </p>
            </td>
          <td class="white"><a href="javascript:launchWindow(''((image:Articles/scrn_importantEmail_AOL_04Full.jpg))'', ''Why am I not getting all of my important email? AOL'', ''610'', ''510'', '''')"><img src="((image:Articles/scrn_importantEmail_AOL_04.gif))" class="profileImageHover" width="180" height="150" border="0" alt="Click for larger view"></a></td>
        </tr>
        <tr> 
          <td class="white"><a href="javascript:launchWindow(''((image:Articles/scrn_importantEmail_AOL_04Full.jpg))'', ''Why am I not getting all of my important email? AOL'', ''610'', ''510'', '''')"><img border="0" src="((image:icon_increaseSize.gif))" width="20" height="19" align="absmiddle" alt="Click for larger view" hspace="3"></a>Click 
            for larger view</td>
        </tr>
        <tr> 
          <td colspan="2" valign="top" class="medium"><strong>Step 3 - Remove 
            ((PLDOMAIN)) from your spam list</strong></td>
        </tr>
        <tr> 
          <td width="400" rowspan="2" valign="top" class="white"> <p>If you already 
              marked an email from ((PLDOMAIN)) as spam, you can take us 
              off your spam list by following these two steps:</p>
            <ol>
              <li>Click &quot;Spam Folder&quot; in your Mailbox.</li>
              <li> Select the email you mistakenly marked as spam and click &quot;This 
                is Not Spam.&quot; </li>
            </ol></td>
          <td class="white"><a href="javascript:launchWindow(''((image:Articles/scrn_importantEmail_AOL_05Full.jpg))'', ''Why am I not getting all of my important email? AOL'', ''610'', ''510'', '''')"><img src="((image:Articles/scrn_importantEmail_AOL_05.gif))" class="profileImageHover" width="180" height="150" border="0" alt="Click for larger view"></a></td>
        </tr>
        <tr> 
          <td class="white"><a href="javascript:launchWindow(''((image:Articles/scrn_importantEmail_AOL_05Full.jpg))'', ''Why am I not getting all of my important email? AOL'', ''610'', ''510'', '''')"><img border="0" src="((image:icon_increaseSize.gif))" width="20" height="19" align="absmiddle" alt="Click for larger view" hspace="3"></a>Click 
            for larger view</td>
        </tr>
        <tr> 
          <td colspan="2" valign="top" class="medium"><strong>Step 4 - When opening 
            an email from ((PLDOMAIN)), click &quot;Yes&quot; in the pop-up.</strong></td>
        </tr>
		<tr> 
          <td width="400" rowspan="2" valign="top" class="white"> <p>If you already 
              marked an email from ((PLDOMAIN)) as spam, you can take us 
              off your spam list by following these two steps:</p>
            <ol>
              <li>Click &quot;Spam Folder&quot; in your Mailbox.</li>
              <li> Select the email you mistakenly marked as spam and click &quot;This 
                is Not Spam.&quot; </li>
            </ol></td>
          <td class="white"><a href="javascript:launchWindow(''((image:Articles/scrn_importantEmail_AOL_06Full.jpg))'', ''Why am I not getting all of my important email? AOL'', ''610'', ''510'', '''')"><img src="((image:Articles/scrn_importantEmail_AOL_06.gif))" class="profileImageHover" width="180" height="150" border="0" alt="Click for larger view"></a></td>
        </tr>
        <tr> 
          <td class="white"><a href="javascript:launchWindow(''((image:Articles/scrn_importantEmail_AOL_06Full.jpg))'', ''Why am I not getting all of my important email? AOL'', ''610'', ''510'', '''')"><img border="0" src="((image:icon_increaseSize.gif))" width="20" height="19" align="absmiddle" alt="Click for larger view" hspace="3"></a>Click 
            for larger view</td>
        </tr>
      </table>

<p>If you have any questions, please contact your email provider.</p>',null,
'11',GETDATE(),'1','False',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100019100','6318','9051',0,N'Why am I not getting all of my important email? Hotmail',N'<p><hr style="width: 592px;">
<span class="copyright">HOTMAIL</span>&nbsp;&nbsp;&nbsp;
<a href="/Applications/Article/ArticleView.aspx?CategoryID=1938&ArticleID=6324&RowNumber=1">YAHOO!</a>&nbsp;&nbsp;&nbsp;
<a href="/Applications/Article/ArticleView.aspx?CategoryID=1938&ArticleID=6316&RowNumber=1">AOL</a>&nbsp;&nbsp;&nbsp;
<a href="/Applications/Article/ArticleView.aspx?CategoryID=1938&ArticleID=6320&RowNumber=1">MSN</a>&nbsp;&nbsp;&nbsp;
<a href="/Applications/Article/ArticleView.aspx?CategoryID=1938&ArticleID=6322&RowNumber=1">OTHER</a></p>
 
     <table border="0" cellpadding="2" cellspacing="0" width="592">
        <tr> 
          <td colspan="2"><strong>Hotmail</strong></td>
        </tr>
        <tr> 
          <td colspan="2"><p>3 easy steps to keep ((PLDOMAIN)) email out 
              of your junk folder and in your Hotmail Inbox</p>
            <br> </td>
        </tr>
      </table>
		
      <table cellpadding="4" cellspacing="1" bgcolor="#DDDCD3" width="592">
        <tr> 
          <td colspan="2" class="medium"><strong>Step 1- Add ((PLDOMAIN)) 
            to your &quot;Safe List&quot;</strong></td>
        </tr>
        <tr> 
          <td rowspan="2" valign="top" class="white"> <p>1. Go to 
              your Hotmail Inbox and click &quot;Options&quot; in the top right 
              corner.</p></td>
          <td class="white"><a href="javascript:launchWindow(''((image:Articles/scrn_importantEmail_hotmail_01Full.jpg))'', ''Clear Your Cache & Cookies AOL (version 8.0)'', ''610'', ''510'', '''')"><img src="((image:Articles/scrn_importantEmail_hotmail_01.gif))" alt="Click for larger view" width="180" height="150" border="0" class="profileImageHover"></a></td>
        </tr>
        <tr> 
          <td class="white"> <a href="javascript:launchWindow(''((image:Articles/scrn_importantEmail_hotmail_01Full.jpg))'', ''Clear Your Cache & Cookies AOL (version 8.0)'', ''610'', ''510'', '''')"><img border="0" src="((image:icon_increaseSize.gif))" width="20" height="19" align="absmiddle" alt="Click for larger view" hspace="3"></a>Click 
            for larger view</td>
        </tr>
        <tr> 
          <td rowspan="2" valign="top" class="white"> <p>2. Select 
              &quot;Mail&quot; in the left menu and click &quot;Junk E-mail Protection.&quot;</p></td>
          <td class="white"><a href="javascript:launchWindow(''((image:Articles/scrn_importantEmail_hotmail_02Full.jpg))'', ''Clear Your Cache & Cookies AOL (version 8.0)'', ''610'', ''510'', '''')"><img src="((image:Articles/scrn_importantEmail_hotmail_02.gif))" class="profileImageHover" width="180" height="150" border="0" alt="Click for larger view"></a></td>
        </tr>
        <tr> 
          <td class="white"><a href="javascript:launchWindow(''((image:Articles/scrn_importantEmail_hotmail_02Full.jpg))'', ''Clear Your Cache & Cookies AOL (version 8.0)'', ''610'', ''510'', '''')"><img border="0" src="((image:icon_increaseSize.gif))" width="20" height="19" align="absmiddle" alt="Click for larger view" hspace="3"></a>Click 
            for larger view</td>
        </tr>
        <tr> 
          <td rowspan="2" valign="top" class="white"> <p>3. Click 
              &quot;Safe List.&quot;</p></td>
          <td class="white"><a href="javascript:launchWindow(''((image:Articles/scrn_importantEmail_hotmail_03Full.jpg))'', ''Clear Your Cache & Cookies AOL (version 8.0)'', ''610'', ''510'', '''')"><img src="((image:Articles/scrn_importantEmail_hotmail_03.gif))" class="profileImageHover" width="180" height="150" border="0" alt="Click for larger view"></a></td>
        </tr>
        <tr> 
          <td class="white"><a href="javascript:launchWindow(''((image:Articles/scrn_importantEmail_hotmail_03Full.jpg))'', ''Clear Your Cache & Cookies AOL (version 8.0)'', ''610'', ''510'', '''')"><img border="0" src="((image:icon_increaseSize.gif))" width="20" height="19" align="absmiddle" alt="Click for larger view" hspace="3"></a>Click 
            for larger view</td>
        </tr>
        <tr> 
          <td rowspan="2" valign="top" class="white"> <p>4. Type in 
              the following email domains and click add: </p>
            <ol>
              <li><strong>Mail.((PLDOMAIN)) </strong></li>
              <li><strong>Mail.spark.net</strong></li>
            </ol></td>
          <td class="white"><a href="javascript:launchWindow(''((image:Articles/scrn_importantEmail_hotmail_04Full.jpg))'', ''Clear Your Cache & Cookies AOL (version 8.0)'', ''610'', ''510'', '''')"><img src="((image:Articles/scrn_importantEmail_hotmail_04.gif))" class="profileImageHover" width="180" height="150" border="0" alt="Click for larger view"></a></td>
        </tr>
        <tr> 
          <td class="white"><a href="javascript:launchWindow(''((image:Articles/scrn_importantEmail_hotmail_04Full.jpg))'', ''Clear Your Cache & Cookies AOL (version 8.0)'', ''610'', ''510'', '''')"><img border="0" src="((image:icon_increaseSize.gif))" width="20" height="19" align="absmiddle" alt="Click for larger view" hspace="3"></a>Click 
            for larger view</td>
        </tr>
        <tr> 
          <td colspan="2" valign="top" class="medium"><strong>Step 2 - DO NOT 
            click &quot;Report Junk E-mail&quot; or &quot;Report and Block Sender&quot; 
            for any email from ((PLDOMAIN)) </strong></td>
        </tr>
        <tr> 
          <td rowspan="2" valign="top" class="white"> <p>If you block 
              or junk just one email from ((PLDOMAIN)), you won''t see ANY 
              email from us, including the ones you want to get!</p>
            <p> What happens when you send something to Junk E-mail? The email 
              is deleted automatically.</p>
            <p>What happens when you Report or Block an email from ((PLDOMAIN))? 
              From that moment on, all email from ((PLDOMAIN)) will be 
              blocked from appearing in your Hotmail Inbox.</p></td>
          <td class="white"><a href="javascript:launchWindow(''((image:Articles/scrn_importantEmail_hotmail_05Full.jpg))'', ''Clear Your Cache & Cookies AOL (version 8.0)'', ''610'', ''510'', '''')"><img src="((image:Articles/scrn_importantEmail_hotmail_05.gif))" class="profileImageHover" width="180" height="150" border="0" alt="Click for larger view"></a></td>
        </tr>
        <tr> 
          <td class="white"><a href="javascript:launchWindow(''((image:Articles/scrn_importantEmail_hotmail_05Full.jpg))'', ''Clear Your Cache & Cookies AOL (version 8.0)'', ''610'', ''510'', '''')"><img border="0" src="((image:icon_increaseSize.gif))" width="20" height="19" align="absmiddle" alt="Click for larger view" hspace="3"></a>Click 
            for larger view</td>
        </tr>
        <tr> 
          <td colspan="2" valign="top" class="medium"><strong>Step 3 - Remove 
            ((PLDOMAIN)) from your Junk or Block folders</strong></td>
        </tr>
        <tr> 
          <td rowspan="2" valign="top" class="white"> <p>If you already 
              junked or blocked an email from ((PLDOMAIN)), you can un-block 
              or un-junk us as follows:</p>
            <ol>
              <li>Go to your &quot;Junk E-mail&quot; folder.</li>
              <li>Select the email from us and click &quot;This is not Junk&quot; 
                in the top menu.</li>
            </ol></td>
          <td class="white"><a href="javascript:launchWindow(''((image:Articles/scrn_importantEmail_hotmail_06Full.jpg))'', ''Clear Your Cache & Cookies AOL (version 8.0)'', ''610'', ''510'', '''')"><img src="((image:Articles/scrn_importantEmail_hotmail_06.gif))" class="profileImageHover" width="180" height="150" border="0" alt="Click for larger view"></a></td>
        </tr>
        <tr> 
          <td class="white"><a href="javascript:launchWindow(''((image:Articles/scrn_importantEmail_hotmail_06Full.jpg))'', ''Clear Your Cache & Cookies AOL (version 8.0)'', ''610'', ''510'', '''')"><img border="0" src="((image:icon_increaseSize.gif))" width="20" height="19" align="absmiddle" alt="Click for larger view" hspace="3"></a>Click 
            for larger view</td>
        </tr>
      </table>

<p>If you have any questions, please contact your email provider.</p>',null,
'12',GETDATE(),'1','False',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100019103','6320','9051',0,N'Why am I not getting all of my important email? MSN',N'<p><hr style="width: 592px;">
<a href="/Applications/Article/ArticleView.aspx?CategoryID=1938&ArticleID=6318&RowNumber=1=6318">HOTMAIL</a>&nbsp;&nbsp;&nbsp;
<a href="/Applications/Article/ArticleView.aspx?CategoryID=1938&ArticleID=6324&RowNumber=1=6324">YAHOO!</a>&nbsp;&nbsp;&nbsp;
<a href="/Applications/Article/ArticleView.aspx?CategoryID=1938&ArticleID=6316&RowNumber=1=6316">AOL</a>&nbsp;&nbsp;&nbsp;
<span class="copyright">MSN</span>&nbsp;&nbsp;&nbsp;
<a href="/Applications/Article/ArticleView.aspx?CategoryID=1938&ArticleID=6322&RowNumber=1=6322">OTHER</a></p>
 

      <table border="0" cellpadding="2" cellspacing="0" width="592" id="table1">
        <tr> 
          <td colspan="2"><strong>MSN</strong></td>
        </tr>
        <tr> 
          <td colspan="2"><p>3 easy steps to keep ((PLDOMAIN)) email out 
              of your junk folder and in your MSN Inbox</p>
            <br> </td>
        </tr>
      </table>
		
      <table cellpadding="4" cellspacing="1" bgcolor="#DDDCD3" width="592">
        <tr> 
          <td colspan="2" class="medium"><strong>Step 1 - Add ((PLDOMAIN)) 
            to your &quot;Safe List&quot;</strong></td>
        </tr>
        <tr> 
          <td rowspan="2" valign="top" class="white"> <p>1. Go to 
              your MSN Inbox and click &quot;Junk E-mail&quot; settings in the 
              lower left corner.</p></td>
          <td class="white"><a href="javascript:launchWindow(''((image:Articles/scrn_importantEmail_MSN_01Full.jpg))'', ''Why am I not getting all of my important email? MSN'', ''610'', ''510'', '''')"><img src="((image:Articles/scrn_importantEmail_MSN_01.gif))" alt="Click for larger view" width="180" height="150" border="0" class="profileImageHover" alt="Click for larger view"></a></td>
        </tr>
        <tr> 
          <td class="white"> <a href="javascript:launchWindow(''((image:Articles/scrn_importantEmail_MSN_01Full.jpg))'', ''Why am I not getting all of my important email? MSN'', ''610'', ''510'', '''')"><img border="0" src="((image:icon_increaseSize.gif))" width="20" height="19" align="absmiddle" alt="Click for larger view" hspace="3"></a>Click 
            for larger view</td>
        </tr>
        <tr> 
          <td rowspan="2" valign="top" class="white"> <p>2. Select 
              &quot;Safe List.&quot;</p></td>
          <td class="white"><a href="javascript:launchWindow(''((image:Articles/scrn_importantEmail_MSN_02Full.jpg))'', ''Why am I not getting all of my important email? MSN'', ''610'', ''510'', '''')"><img src="((image:Articles/scrn_importantEmail_MSN_02.gif))" class="profileImageHover" width="180" height="150" border="0" alt="Click for larger view"></a></td>
        </tr>
        <tr> 
          <td class="white"><a href="javascript:launchWindow(''((image:Articles/scrn_importantEmail_MSN_02Full.jpg))'', ''Why am I not getting all of my important email? MSN'', ''610'', ''510'', '''')"><img border="0" src="((image:icon_increaseSize.gif))" width="20" height="19" align="absmiddle" alt="Click for larger view" hspace="3"></a>Click 
            for larger view</td>
        </tr>
        <tr> 
          <td rowspan="2" valign="top" class="white"> <p>3. In the 
              pop-up window, type in the following email address and click add:<br>
              <strong>YourMatches@Mail.((PLDOMAIN))</strong></p></td>
          <td class="white"><a href="javascript:launchWindow(''((image:Articles/scrn_importantEmail_MSN_03Full.jpg))'', ''Why am I not getting all of my important email? MSN'', ''610'', ''510'', '''')"><img src="((image:Articles/scrn_importantEmail_MSN_03.gif))" class="profileImageHover" width="180" height="150" border="0" alt="Click for larger view"></a></td>
        </tr>
        <tr> 
          <td class="white"><a href="javascript:launchWindow(''((image:Articles/scrn_importantEmail_MSN_03Full.jpg))'', ''Why am I not getting all of my important email? MSN'', ''610'', ''510'', '''')"><img border="0" src="((image:icon_increaseSize.gif))" width="20" height="19" align="absmiddle" alt="Click for larger view" hspace="3"></a>Click 
            for larger view</td>

        </tr>
        <tr> 
          <td colspan="2" valign="top" class="white"> <p>4. Repeat to add each 
              of our other email addresses:</p>
           <ol>
<li><strong>Click@Mail.((PLDOMAIN))</strong></li>
<li><strong>Communications@Mail.((PLDOMAIN))</strong></li>
<li><strong>MemberServices@Mail.((PLDOMAIN))</strong></li>
<li><strong>VerifyEmail@Mail.((PLDOMAIN))</strong></li>
<li><strong>YourMatches@Mail.((PLDOMAIN))</strong></li>
<li><strong>Photos@Mail.((PLDOMAIN))</strong></li>
<li><strong>Comments@Mail.((PLDOMAIN))</strong></li>
</strong></li>
</ol></td>
        </tr>
        <tr> 
          <td rowspan="2" valign="top" class="white"> <p>5. Click 
              &quot;Save Changes.&quot;</p></td>
          <td class="white"><a href="javascript:launchWindow(''((image:Articles/scrn_importantEmail_MSN_04Full.jpg))'', ''Why am I not getting all of my important email? MSN'', ''610'', ''510'', '''')"><img src="((image:Articles/scrn_importantEmail_MSN_04.gif))" class="profileImageHover" width="180" height="150" border="0" alt="Click for larger view"></a></td>
        </tr>
        <tr> 
          <td class="white"><a href="javascript:launchWindow(''((image:Articles/scrn_importantEmail_MSN_04Full.jpg))'', ''Why am I not getting all of my important email? MSN'', ''610'', ''510'', '''')"><img border="0" src="((image:icon_increaseSize.gif))" width="20" height="19" align="absmiddle" alt="Click for larger view" hspace="3"></a>Click 
            for larger view</td>
        </tr>
        <tr> 
          <td colspan="2" valign="top" class="medium"><strong>Step 2 - DO NOT 
            click &quot;Junk&quot; for any email from ((PLDOMAIN))</strong></td>
        </tr>
        <tr> 
          <td rowspan="2" valign="top" class="white"> <p>If you junk 
              just one email from ((PLDOMAIN)), you won''t see ANY email 
              from that email address in your MSN Inbox again, including the ones 
              you want to get! </p>
            <p> What happens when you junk something? The email goes to your Junk 
              E-Mail box and is deleted automatically. From that moment on, all 
              email from that ((PLDOMAIN)) address will be blocked from 
              appearing in your MSN Inbox.<br>
              <br>
            </p>
            <p><br>
              <br>
            </p></td>
          <td class="white"><a href="javascript:launchWindow(''((image:Articles/scrn_importantEmail_MSN_05Full.jpg))'', ''Why am I not getting all of my important email? MSN'', ''610'', ''510'', '''')"><img src="((image:Articles/scrn_importantEmail_MSN_05.gif))" class="profileImageHover" width="180" height="150" border="0" alt="Click for larger view"></a></td>
        </tr>
        <tr> 
          <td class="white"><a href="javascript:launchWindow(''((image:Articles/scrn_importantEmail_MSN_05Full.jpg))'', ''Why am I not getting all of my important email? MSN'', ''610'', ''510'', '''')"><img border="0" src="((image:icon_increaseSize.gif))" width="20" height="19" align="absmiddle" alt="Click for larger view" hspace="3"></a>Click 
            for larger view</td>
        </tr>
        <tr> 
          <td colspan="2" valign="top" class="medium"><strong>Step 3 - Remove 
            ((PLDOMAIN)) from your junk list</strong></td>
        </tr>
        <tr> 
          <td rowspan="2" valign="top" class="white"> <p>1. If you 
              already marked an email from ((PLDOMAIN)) as spam, you can 
              take us off your junk list as follows:</p>
            <ol>
              <li>Go to your Junk E-Mail box. </li>
              <li>In your Junk E-Mail box, check the email you mistakenly marked 
                as junk and click &quot;Not Junk&quot; at the top of the screen.</li>
            </ol></td>
          <td class="white"><a href="javascript:launchWindow(''((image:Articles/scrn_importantEmail_MSN_06Full.jpg))'', ''Why am I not getting all of my important email? MSN'', ''610'', ''510'', '''')"><img src="((image:Articles/scrn_importantEmail_MSN_06.gif))" class="profileImageHover" width="180" height="150" border="0" alt="Click for larger view"></a></td>
        </tr>
        <tr> 
          <td class="white"><a href="javascript:launchWindow(''((image:Articles/scrn_importantEmail_MSN_06Full.jpg))'', ''Why am I not getting all of my important email? MSN'', ''610'', ''510'', '''')"><img border="0" src="((image:icon_increaseSize.gif))" width="20" height="19" align="absmiddle" alt="Click for larger view" hspace="3"></a>Click 
            for larger view</td>
        </tr>
        <tr> 
          <td rowspan="2" valign="top" class="white"> <p>2. Click 
              &quot;Add to Safe List.&quot;</p>
            <p>The email will be returned to your Inbox and your junk filter will 
              know that this email was not junk.</p></td>
          <td class="white"><a href="javascript:launchWindow(''((image:Articles/scrn_importantEmail_MSN_07Full.jpg))'', ''Why am I not getting all of my important email? MSN'', ''610'', ''510'', '''')"><img src="((image:Articles/scrn_importantEmail_MSN_07.gif))" class="profileImageHover" width="180" height="150" border="0" alt="Click for larger view"></a></td>
        </tr>
        <tr> 
          <td class="white"><a href="javascript:launchWindow(''((image:Articles/scrn_importantEmail_MSN_07Full.jpg))'', ''Why am I not getting all of my important email? MSN'', ''610'', ''510'', '''')"><img border="0" src="((image:icon_increaseSize.gif))" width="20" height="19" align="absmiddle" alt="Click for larger view" hspace="3"></a>Click 
            for larger view</td>
        </tr>
      </table>

      <p>If you have any questions, please contact your email provider.</p>',null,
'13',GETDATE(),'1','False',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100019106','6324','9051',0,N'Why am I not getting all of my important email? Yahoo!',N'<p><hr style="width: 592px;">
<a href="/Applications/Article/ArticleView.aspx?CategoryID=1938&ArticleID=6318&RowNumber=1">HOTMAIL</a>&nbsp;&nbsp;&nbsp;
<span class="copyright">YAHOO!</span>&nbsp;&nbsp;&nbsp;
<a href="/Applications/Article/ArticleView.aspx?CategoryID=1938&ArticleID=6316&RowNumber=1">AOL</a>&nbsp;&nbsp;&nbsp;
<a href="/Applications/Article/ArticleView.aspx?CategoryID=1938&ArticleID=6320&RowNumber=1">MSN</a>&nbsp;&nbsp;&nbsp;
<a href="/Applications/Article/ArticleView.aspx?CategoryID=1938&ArticleID=6322&RowNumber=1">OTHER</a></p>

     <table border="0" cellpadding="2" cellspacing="0" width="592" id="table1">
        <tr> 
          <td colspan="2"><strong>Yahoo!</strong></td>
        </tr>
        <tr> 
          <td colspan="2"><p>3 easy steps to keep ((PLDOMAIN)) email out 
              of your bulk folder and in your Yahoo! Inbox</p>
            <br> </td>
        </tr>
      </table>
		
      <table cellpadding="4" cellspacing="1" bgcolor="#DDDCD3" width="592">
        <tr> 
          <td colspan="2" class="medium"><strong>Step 1 - Add ((PLDOMAIN)) 
            to your &quot;Address Book:&quot;</strong></td>
        </tr>
        <tr> 
          <td rowspan="2" valign="top" class="white"> <p>1. Go to 
              your Yahoo! Inbox.<br>
              <br>
              Go to the &quot;Addresses&quot; pull-down menu at the top and click 
              &quot;Add Contact.&quot;</p></td>
          <td class="white"><a href="javascript:launchWindow(''((image:Articles/scrn_importantEmail_yahoo_01Full.jpg))'', ''Why am I not getting all of my important email? Yahoo!'', ''610'', ''510'', '''')"><img src="((image:Articles/scrn_importantEmail_yahoo_01.gif))" alt="Click for larger view" width="180" height="150" border="0" class="profileImageHover"></a></td>
        </tr>
        <tr> 
          <td class="white"> <a href="javascript:launchWindow(''((image:Articles/scrn_importantEmail_yahoo_01Full.jpg))'', ''Why am I not getting all of my important email? Yahoo!'', ''610'', ''510'', '''')"><img border="0" src="((image:icon_increaseSize.gif))" width="20" height="19" align="absmiddle" alt="Click for larger view" hspace="3"></a>Click 
            for larger view</td>
        </tr>
        <tr> 
          <td rowspan="2" valign="top" class="white"> <p>2. Where 
              it asks for &quot;Email,&quot; type: </p>
            <p><strong>YourMatches@mail.((PLDOMAIN))</strong></p></td>
          <td class="white"><a href="javascript:launchWindow(''((image:Articles/scrn_importantEmail_yahoo_02Full.jpg))'', ''Why am I not getting all of my important email? Yahoo!'', ''610'', ''510'', '''')"><img src="((image:Articles/scrn_importantEmail_yahoo_02.gif))" class="profileImageHover" width="180" height="150" border="0" alt="Click for larger view"></a></td>
        </tr>
        <tr> 
          <td class="white"><a href="javascript:launchWindow(''((image:Articles/scrn_importantEmail_yahoo_02Full.jpg))'', ''Why am I not getting all of my important email? Yahoo!'', ''610'', ''510'', '''')"><img border="0" src="((image:icon_increaseSize.gif))" width="20" height="19" align="absmiddle" alt="Click for larger view" hspace="3"></a>Click 
            for larger view</td>
        </tr>
        <tr> 
          <td colspan="2" valign="top" class="white"> <p>Leave the rest blank 
              and click &quot;Save Contact,&quot; then &quot;Done.&quot; <br>
              <br>
              Repeat to add each of our other email addresses:</p>
            <ol>
              <li><strong>Click@Mail.((PLDOMAIN))</strong></li>
              <li><strong>Communications@Mail.((PLDOMAIN))</strong></li>
              <li><strong>MemberServices@Mail.((PLDOMAIN))</strong></li>
              <li><strong>VerifyEmail@Mail.((PLDOMAIN))</strong></li>
              <li><strong>YourMatches@Mail.((PLDOMAIN))</strong>
			  <li><strong>Photos@Mail.((PLDOMAIN))</strong></li>
			   <li><strong>Comments@Mail.((PLDOMAIN))</strong></li><br>
                </li>
            </ol></td>
        </tr>
        <tr> 
          <td colspan="2" valign="top" class="medium"><strong>Step 2 - DO NOT 
            click &quot;Spam&quot; for any email from ((PLDOMAIN))</strong></td>
        </tr>
        <tr> 
          <td rowspan="2" valign="top" class="white"> <p>If you block 
              just one email from ((PLDOMAIN)), you won''t see ANY email 
              from us, including the ones you want to get!</p>
            <p> What happens when you send something to Spam? From that moment 
              on, all email from that ((PLDOMAIN)) address will be blocked 
              from appearing in your Yahoo! Inbox.</p></td>
          <td class="white"><a href="javascript:launchWindow(''((image:Articles/scrn_importantEmail_yahoo_03Full.jpg))'', ''Why am I not getting all of my important email? Yahoo!'', ''610'', ''510'', '''')"><img src="((image:Articles/scrn_importantEmail_yahoo_03.gif))" class="profileImageHover" width="180" height="150" border="0" alt="Click for larger view"></a></td>
        </tr>
        <tr> 
          <td class="white"><a href="javascript:launchWindow(''((image:Articles/scrn_importantEmail_yahoo_03Full.jpg))'', ''Why am I not getting all of my important email? Yahoo!'', ''610'', ''510'', '''')"><img border="0" src="((image:icon_increaseSize.gif))" width="20" height="19" align="absmiddle" alt="Click for larger view" hspace="3"></a>Click 
            for larger view</td>
        </tr>
        <tr> 
          <td colspan="2" valign="top" class="medium"><strong>Step 3 - Remove 
            ((PLDOMAIN)) from your spam list</strong></td>
        </tr>
        <tr> 
          <td colspan="2" valign="top" class="white"> <p>If you already marked 
              an email from ((PLDOMAIN)) as spam, you can take us off your 
              spam list as follows:</p>
            <ol>
              <li>Go to the &quot;trash/bulk&quot; folder. </li>
              <li>Open the email you mistakenly marked as spam. </li>
              <li>Click the &quot;Not Spam&quot; button.</li>
            </ol>
            <p>The email will be returned to your Inbox and your spam filter will 
              know that this email was not spam.</p></td>
        </tr>
      </table>

<p>If you have any questions, please contact your email provider.</p>',null,
'14',GETDATE(),'1','False',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100019109','6322','9051',0,N'Why am I not getting all of my important email? Other',N'<p><hr style="width: 592px;">
<a href="/Applications/Article/ArticleView.aspx?CategoryID=1938&ArticleID=6318&RowNumber=1">HOTMAIL</a>&nbsp;&nbsp;&nbsp;
<a href="/Applications/Article/ArticleView.aspx?CategoryID=1938&ArticleID=6324&RowNumber=1">YAHOO!</a>&nbsp;&nbsp;&nbsp;
<a href="/Applications/Article/ArticleView.aspx?CategoryID=1938&ArticleID=6316&RowNumber=1">AOL</a>&nbsp;&nbsp;&nbsp;
<a href="/Applications/Article/ArticleView.aspx?CategoryID=1938&ArticleID=6320&RowNumber=1">MSN</a>&nbsp;&nbsp;&nbsp;
<span class="copyright">OTHER</span></p> 

      <table border="0" cellpadding="2" cellspacing="0" width="592" id="table1">
        <tr> 
          <td colspan="2"><strong>Other</strong></td>
        </tr>
        <tr> 
          <td colspan="2"><p>3 easy steps to keep ((PLDOMAIN)) email out 
              of your junk/spam box and in your inbox.</p>
            <br> </td>
        </tr>
      </table>
		
      <table cellpadding="4" cellspacing="1" bgcolor="#DDDCD3" width="592">
        <tr> 
          <td class="medium"><strong>Step 1 - Add ((PLDOMAIN)) email addresses 
            to your Address Book or Safe List:</strong></td>
        </tr>
        <tr> 
          <td class="white">
<ol>
<li>Click@Mail.((PLDOMAIN))</li>
<li>Communications@Mail.((PLDOMAIN))</li>
<li>MemberServices@Mail.((PLDOMAIN))</li>
<li>VerifyEmail@Mail.((PLDOMAIN))</li>
<li>YourMatches@Mail.((PLDOMAIN))</li>
<li>Photos@Mail.((PLDOMAIN))</li>
<li>Comments@Mail.((PLDOMAIN))</li>
</li>
</ol>

		  </td>
        </tr>
        <tr> 
          <td valign="top" class="medium"><strong>Step 2 - DO NOT click &quot;Junk&quot; 
            or &quot;Spam&quot; for any email from ((PLDOMAIN)) </strong></td>
        </tr>
        <tr> 
          <td class="white"> <p><br>
              If you junk or spam one email from ((PLDOMAIN)), you probably 
              won''t see ANY email from that email address in your Inbox again, 
              including the ones you want to get from other members.<br>
              <br>
            </p>
            </td>
        </tr>
        <tr> 
          <td valign="top" class="medium"><strong>Step 3 - Remove ((PLDOMAIN)) 
            from your junk or spam list</strong></td>
        </tr>
        <tr> 
          <td class="white"> <p><br>
              If you already marked an email from ((PLDOMAIN)) as junk 
              or spam, follow your email provider''s instructions to take us off 
              your junk or spam list.<br>
              <br>
            </p>
            </td>
        </tr>
      </table>

<p>If you have any questions, please contact your email provider.</p>',null,
'15',GETDATE(),'1','False',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100019112','6348','9051',0,N'How do I initiate an instant message?',N'<p><table border="0" cellpadding="2" cellspacing="0" width="592" id="table1">
	 <tr>
	  <td width="2%" valign="top"><strong>Q.</strong></td>
	  <td><strong>How do I initiate an instant message?</strong></td>
	</tr>
	<tr>
	  <td valign="top">A.</td>
	  <td> 

<p>To start an instant message conversation, click the flashing  <img border="0" src="((image:icon-status-online.gif))" align="absmiddle" alt="I''m Online! Chat with me" hspace="3"> I''M ONLINE button in the member''s profile, and the instant message box will appear. Type your message and click Send.</p>                                                                                                                                        
<p>You can switch your instant message setting to away by moving the AWAY SWITCH from green to red. You can also mute the volume by clicking the speaker icon.</p>
 
<p>Find someone you''d like to instant message? Upgrade to a Subscription Membership at a low monthly rate so you can send instant messages and email at any time.</p>

<p><strong>Still have questions?</strong><br>
<a href="/Applications/ContactUs/ContactUs.aspx">Email us</a> or call toll-free at: ((CSSUPPORTPHONENUMBER)).<br>
If outside the U.S. and Canada, please contact our <a href="http://www.spark.net/contact.htm">Beverly Hills Headquarters.</a></p>

</td></tr></table></p>',null,
'1',GETDATE(),'1','True',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100019115','100001452','9051',0,N'What features does Instant Messenger have?',N'<p><table border="0" cellpadding="2" cellspacing="0" width="592" id="table1">
	 <tr>
	  <td width="2%" valign="top"><strong>Q.</strong></td>
	  <td><strong>What features does Instant Messenger have?</strong></td>
	</tr>
	<tr>
	  <td valign="top">A.</td>
	  <td> 

<p>Our IM feature makes it easy to communicate with potential dates and new friends in real time!</p>

<p><strong>One-on-one text chatting</strong></p>

<p>The IM system has all the features you would want or need for live chat. You can change the text style and font or add smiley faces before you hit SEND.</p>

<p><strong>Add people straight to your Hot List</strong></p>

<p>Chatting up a storm with someone you like? Just click the “HOT LIST” link below the text window and your chat partner will instantly be added to your Hot List so you can talk to them again.</p>

<p>Is someone not taking the hint that you don’t want them to IM you anymore? Click the “Block” link and they will appear on your Blocked Hot List. Blocked members can’t send you email or IM.</p>

<p><strong>Add your own personal icon</strong></p>

<p>When the IM window opens, you’ll see an icon next to your name. <a href="/Applications/Article/ArticleView.aspx?CategoryID=1944&ArticleID=100000118&RowNumber=1">Click here</a> to learn how to find the icon that best represents YOU!</p>

</td></tr></table></p>',null,
'0',GETDATE(),'1','False',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100019118','100000155','9051',0,N'How do I know if a member is available to receive an Instant Message?',N'<p><table border="0" cellpadding="2" cellspacing="0" width="592" id="table1">
	 <tr>
	  <td width="2%" valign="top"><strong>Q.</strong></td>
	  <td><strong>How do I know if a member is available to receive an Instant Message?</strong></td>
	</tr>
	<tr>
	  <td valign="top">A.</td>
	  <td> 

<p>If a member is online, the <img border="0" src="((image:icon-status-online.gif))" align="absmiddle" alt="I''m Online! Chat with me" hspace="3"> I’M ONLINE button flashes on their profile. To start a conversation, click the <img border="0" src="((image:icon-status-online.gif))" align="absmiddle" alt="I''m Online! Chat with me" hspace="3"> and send a message.</p>

</td></tr></table></p>',null,
'0',GETDATE(),'1','False',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100019121','100001455','9051',0,N'What if I want to contact someone who isn''t online?',N'<p><table border="0" cellpadding="2" cellspacing="0" width="592" id="table1">
	 <tr>
	  <td width="2%" valign="top"><strong>Q.</strong></td>
	  <td><strong>What if I want to contact someone who isn''t online?</strong></td>
	</tr>
	<tr>
	  <td valign="top">A.</td>
	  <td> 

<p>If you want to contact someone who isn''t online, you can send an email, Smile or E-card. You must be a Subscriber to send an email or E-card but you can Smile with members for free.</p>

</td></tr></table></p>',null,
'0',GETDATE(),'1','False',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100019124','100000201','9051',0,N'What do I need to use the instant messenger?',N'<p><table border="0" cellpadding="2" cellspacing="0" width="592" id="table1">
	 <tr>
	  <td width="2%" valign="top"><strong>Q.</strong></td>
	  <td><strong>What do I need to use the instant messenger?</strong></td>
	</tr>
	<tr>
	  <td valign="top">A.</td>
	  <td>

<p>You will need a connection to the internet, a web browser (e.g. Internet Explorer, Netscape Navigator, Safari or Opera) and the Macromedia Flash Player 6.0+. The Macromedia Flash Player is a plugin for your web browser which allows you to view Flash content on the web. Most current web browsers come with the Flash Player plugin installed. If you receive a notice&#8212;when trying to connect to the IM&#8212;that the Flash Player is not installed, you may download it and install it for free from Macromedia <a href="http://www.macromedia.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash" target="_blank">here</a>.</p>

<p><a href="http://www.macromedia.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash" target="_blank"><img border="0" src="((image:logo_flash.gif))" width="62" height="80" align="absmiddle" alt="Macromedia" hspace="3"></a></p>

<p>For more information on Macromedia Flash and the Macromedia Flash Player, please visit <a href="http://www.macromedia.com" target="_blank">www.macromedia.com</a></p>

</td></tr></table></p>',null,
'5',GETDATE(),'1','False',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100019127','6452','9051',0,N'What is a Smile? How many times can I Smile at someone?',N'<p>
	<table border="0" cellpadding="2" cellspacing="0" width="592" id="table1">
		<tr>
			<td width="2%" valign="top"><strong>Q.</strong></td>
			<td><strong>What is a Smile? How many times can I Smile at someone? </strong></td>
		</tr>
		<tr>
			<td valign="top">A.</td>
			<td>
				<p>A Smile is a quick, fun way to let someone know you''re interested. You pick a one-liner from our list of Smiles, and it''s sent to the member of your choice.</p> 

				<p>To send a Smile, go to any member''s profile and click Smile. Pick your favorite Smile and we''ll send it to the member''s onsite Inbox. He or she can then go to your profile and Smile back with you. You can send up to 30 Smiles a day, but each member can only be Smiled at once. </p>

				<p>All members can send Smiles for free. If you''d like to take it a step further, we suggest <a href="((PLSECURELINK))">subscribing</a> and sending an email to start up a real conversation.</p>

			</td>
		</tr>
	</table>
</p>',null,
'0',GETDATE(),'1','True',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100019130','6466','9051',0,N'What is Members Online?',N'<p><table border="0" cellpadding="2" cellspacing="0" width="592" id="table1">
	 <tr>
		<td width="2%" valign="top"><strong>Q.</strong></td>
		<td><strong>What is Members Online?</strong></td>
	</tr>
	<tr>
		<td valign="top">A.</td>
		<td> 
<p><a href="/Applications/MembersOnline/MembersOnline.aspx">Members Online</a> shows members who are logged into the site at any given moment. If someone catches your eye, you can send an instant message and start up a real time conversation.</p>

<p>To send an instant message, you need to be a Subscription Member, but you can reply to any instant message with your free membership.</p> 
</td></tr></table></p>',null,
'1',GETDATE(),'1','True',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100019133','6458','9051',0,N'How can I see the members currently online?',N'<p><table border="0" cellpadding="2" cellspacing="0" width="592" id="table1">
	 <tr>
		<td width="2%" valign="top"><strong>Q.</strong></td>
		<td><strong>How can I see the members currently online?</strong></td>
	</tr>
	<tr>
		<td valign="top">A.</td>
		<td> 
<p>To see the members currently online, click Members Online in the Connect drop-down menu at the top of the screen.</p>

</td></tr></table></p>',null,
'2',GETDATE(),'1','False',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100019136','6468','9051',0,N'Why do I see people online from outside my local area?',N'<p><table border="0" cellpadding="2" cellspacing="0" width="592" id="table1">
	 <tr>
		<td width="2%" valign="top"><strong>Q.</strong></td>
		<td><strong>Why do I see people online from outside my local area?</strong></td>
	</tr>
	<tr>
		<td valign="top">A.</td>
		<td> 
<p><a href="/Applications/MembersOnline/MembersOnline.aspx">Members Online</a> automatically shows you members from all locations, but you can customize your settings so you only see people in your area.</p>

<p>To change your settings, go to Members Online and change Anywhere to My Region.</p> 
</td></tr></table></p>',null,
'3',GETDATE(),'1','False',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100019139','6470','9051',0,N'Why do I see people online from outside my age range?',N'<p><table border="0" cellpadding="2" cellspacing="0" width="592" id="table1">
	 <tr>
		<td width="2%" valign="top"><strong>Q.</strong></td>
		<td><strong>Why do I see people online from outside my age range?</strong></td>
	</tr>
	<tr>
		<td valign="top">A.</td>
		<td> 
<p><a href="/Applications/MembersOnline/MembersOnline.aspx">Members Online</a> automatically shows you members of all ages, but you can customize your settings so you only see people in a specific age range.</p>

<p>To change your settings, go to Members Online and enter the age range you''re looking for.</p> 
</td></tr></table></p>',null,
'4',GETDATE(),'1','False',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100019142','6460','9051',0,N'How do I customize the members I see in Members Online?',N'<p><table border="0" cellpadding="2" cellspacing="0" width="592" id="table1">
	 <tr>
		<td width="2%" valign="top"><strong>Q.</strong></td>
		<td><strong>How do I customize the members I see in Members Online?</strong></td>
	</tr>
	<tr>
		<td valign="top">A.</td>
		<td> 
<p>You can customize <a href="/Applications/MembersOnline/MembersOnline.aspx">Members Online</a> on the Members Online page: 
<p><ul><li>To customize by gender, change Everyone to the type of person you''re looking for.</li> 
<li>To customize by region, change Anywhere to My Region or the location of your choice. </li>
<li>To customize by age, fill in the age range you''re looking for.</li></ul></p>
<p>Just remember, the more specific you get, the more narrow your options.</p> 
</td></tr></table></p>',null,
'5',GETDATE(),'1','False',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100019145','6454','9051',0,N'Can I customize the order of how members appear in Online Now?',N'<p><table border="0" cellpadding="2" cellspacing="0" width="592" id="table1">
	 <tr>
		<td width="2%" valign="top"><strong>Q.</strong></td>
		<td><strong>Can I customize the order of how members appear in Members Online?</strong></td>
	</tr>
	<tr>
		<td valign="top">A.</td>
		<td> 
<p>Yes, you can sort the members by when they joined, whether or not they have photos, alphabetically, by age or gender.</p>

<p>To sort, go to Members Online and select your option in the "View by" drop-down menu.</p> 
</td></tr></table></p>',null,
'6',GETDATE(),'1','False',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100019148','6456','9051',0,N'Can I save my Members Online customized settings?',N'<p><table border="0" cellpadding="2" cellspacing="0" width="592" id="table1">
	 <tr>
		<td width="2%" valign="top"><strong>Q.</strong></td>
		<td><strong>Can I save my Online Now customized settings?</strong></td>
	</tr>
	<tr>
		<td valign="top">A.</td>
		<td> 
<p>Yes, your customized settings will be saved. When you return, you can change your Members Online preferences at any time.</p> 
</td></tr></table></p>',null,
'7',GETDATE(),'1','False',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100019151','6462','9051',0,N'I''m not seeing many members when I customize my settings. How can I see more?',N'<p><table border="0" cellpadding="2" cellspacing="0" width="592" id="table1">
	 <tr>
	  <td width="2%" valign="top"><strong>Q.</strong></td>
	  <td><strong>I''m not seeing many members when I customize my settings. How can I see more?</strong>	</td>
	</tr>
	<tr>
	  <td valign="top">A.</td>
	  <td> 

<p>If you aren''t seeing many members with your customized settings, try changing <strong>My Region</strong> back to <strong>Anywhere</strong>. </p>

</td></tr></table></p>',null,
'8',GETDATE(),'1','False',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100019154','6464','9051',0,N'I''ve been looking through Members Online, but how do I see other members?',N'<p><table border="0" cellpadding="2" cellspacing="0" width="592" id="table1">
	 <tr>
	  <td width="2%" valign="top"><strong>Q.</strong></td>
	  <td><strong>I''ve been looking through Members Online, but how do I see other members?</strong>	</td>
	</tr>
	<tr>
	  <td valign="top">A.</td>
	  <td> 

<p>To search through all members, go to Preferences in the Search drop-down menu, customize your search, and then click Your Matches in the Search drop-down menu to see your results.</p>

<p>You may customize your search criteria by gender, height, marital status, education, religion, ethnicity, smoking/drinking habits, location, and distance from your home.  Mix it up with your searches to obtain different results – you never know where you’ll find your beshert!</p>

</td></tr></table></p>',null,
'9',GETDATE(),'1','False',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100019157','6330','9051',0,N'How do I add someone to my Hot Lists?',N'<p><table border="0" cellpadding="2" cellspacing="0" width="592" id="table1">
	 <tr>
	  <td width="2%" valign="top"><strong>Q.</strong></td>
	  <td><strong>How do I add someone to my Hot Lists?</strong></td>
	</tr>
	<tr>
	  <td valign="top">A.</td>
	  <td>

<p>You can add anyone to your Hot Lists by clicking <a href="/Applications/HotList/View.aspx?CategoryID=0"><img border="0" src="((image:icon-hotlist.gif))" width="18" height="19" align="absmiddle" alt="List me" hspace="3">HOT LIST</a> in the person''s profile.</p>

</td></tr></table></p>',null,
'1',GETDATE(),'1','False',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100019160','6342','9051',0,N'How do I view my Hot Lists?',N'<p><table border="0" cellpadding="2" cellspacing="0" width="592" id="table1">
	 <tr>
	  <td width="2%" valign="top"><strong>Q.</strong></td>
	  <td><strong>How do I view my Hot Lists? </strong></td>
	</tr>
	<tr>
	  <td valign="top">A.</td>
	  <td>

<p>To view your Hot Lists, click <a href="/Applications/HotList/View.aspx?CategoryID=0">Hot Lists</a> in the top menu. You''ll land on Your Favorites page and can go to other Hot Lists from the left menu.</p>

</td></tr></table></p>
',null,
'2',GETDATE(),'1','False',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100019163','6328','9051',0,N'Can I divide my Hot Listed members into categories?',N'<p><table border="0" cellpadding="2" cellspacing="0" width="592" id="table1">
	 <tr>
	  <td width="2%" valign="top"><strong>Q.</strong></td>
	  <td><strong>Can I divide my Hot Listed members into categories?</strong></td>
	</tr>
	<tr>
	  <td valign="top">A.</td>
	  <td>

<p>You <a href="/Applications/HotList/ManageListCategory.aspx">can create your own Hot List categories</a> to help you keep track of members. For example, you can create a category of people you''ve been talking with on the phone or people you''ve already been on a date with, etc.</p>
 
<p>To create a new category:<br>
<ul><li>Go to your <a href="/Applications/HotList/View.aspx?CategoryID=0">Hot Lists</a></li> 
<li>Click <a href="/Applications/HotList/ManageListCategory.aspx">Create/Edit Your Hot Lists</a></li>
<li>Enter the name of the new category and click Create.</li></ul></p>
 
<p>To move a Hot Listed member into your new category or onto a different Hot List:<br>
<ul>
            <li>Go to the <a href="/Applications/HotList/View.aspx?CategoryID=0"> Hot List</a></a> where the member appears</li>
<li>Click Move in the person''s profile </li>
<li>Select the category where you want to move the member</li>
</ul></p>

<p>Any new category you create will appear in the left menu for easy access, together with your other Hot Lists.</p>
 

<p><strong>Still have questions?</strong></br>
<a href="/Applications/ContactUs/ContactUs.aspx">Email us</a> or call toll-free at: ((CSSUPPORTPHONENUMBER)).<br>
If outside the U.S. and Canada, please contact our <a href="http://www.spark.net/contact.htm">Beverly Hills Headquarters.</a></p>

</td></tr></table></p>',null,
'3',GETDATE(),'1','False',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100019166','6338','9051',0,N'How do I move a Hot Listed member into another category?',N'<p><table border="0" cellpadding="2" cellspacing="0" width="592" id="table1">
	 <tr>
	  <td width="2%" valign="top"><strong>Q.</strong></td>
	  <td><strong>How do I move a Hot Listed member into another category?</strong></td>
	</tr>
	<tr>
	  <td valign="top">A.</td>
	  <td>
 
<p>To move a Hot Listed member into a specific category:<br>
<ul>
<li>Go to the <a href="/Applications/HotList/View.aspx?CategoryID=0">Hot List</a> where the member appears</li>
<li>Click Move in the member''s profile</li>
<li>Select the category that you want to move the member to</li></ul></p>

<p><strong>Still have questions?</strong></br>
<a href="/Applications/ContactUs/ContactUs.aspx">Email us</a> or call toll-free at: ((CSSUPPORTPHONENUMBER)).<br>
If outside the U.S. and Canada, please contact our <a href="http://www.spark.net/contact.htm">Beverly Hills Headquarters.</a></p>

</td></tr></table></p>',null,
'4',GETDATE(),'1','False',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100019169','6332','9051',0,N'How do I create new Hot List categories?',N'<p><table border="0" cellpadding="2" cellspacing="0" width="592" id="table1">
	 <tr>
	  <td width="2%" valign="top"><strong>Q.</strong></td>
	  <td><strong>How do I create new Hot List categories?</strong></td>
	</tr>
	<tr>
	  <td valign="top">A.</td>
	  <td>
 
<p>You can create your own Hot List categories to help you keep track of members. For example, you can create a category of people you''ve been talking to on the phone, people you might email in the future or people who live in a certain city.</p>

<p>To create a new category:<br>
<ul><li>Go to your <a href="/Applications/HotList/View.aspx?CategoryID=0">Hot Lists</a></li> 
<li>Click <a href="/Applications/HotList/ManageListCategory.aspx">Create/Edit Your Hot Lists</a></li>
<li>Enter the name of the new category and click Create</li></ul></p>


<p>Any new category you create will appear in the left menu together with your other Hot Lists for easy access.</p>
 

<p>To move a Hot Listed member into your new category:<br>
<ul>
            <li>Go to the <a href="/Applications/HotList/View.aspx?CategoryID=0"> Hot List</a></a> where the member appears</li>
<li>Click Move in the person''s profile </li>
<li>Select the category that you want to move the member to </li>
</ul></p>


<p><strong>Still have questions?</strong></br>
<a href="/Applications/ContactUs/ContactUs.aspx">Email us</a> or call toll-free at: ((CSSUPPORTPHONENUMBER)).<br>
If outside the U.S. and Canada, please contact our <a href="http://www.spark.net/contact.htm">Beverly Hills Headquarters.</a></p>

</td></tr></table></p>',null,
'5',GETDATE(),'1','False',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100019172','6336','9051',0,N'How do I make notes about the people on my Hot Lists?',N'<p><table border="0" cellpadding="2" cellspacing="0" width="592" id="table1">
	 <tr>
	  <td width="2%" valign="top"><strong>Q.</strong></td>
	  <td><strong>How do I make notes about the people on my Hot Lists?</strong></td>
	</tr>
	<tr>
	  <td valign="top">A.</td>
	  <td>

<p>To write comments about someone in your Hot Lists:<br>
<ul>
              <li>Find the member in your <a href="/Applications/HotList/View.aspx?CategoryID=0">Hot Lists</a></li>
              <li>Write your comments in the box provided and click Save</li> 
            </ul></p>

<p><strong>Still have questions?</strong></br>
<a href="/Applications/ContactUs/ContactUs.aspx">Email us</a> or call toll-free at: ((CSSUPPORTPHONENUMBER)).<br>
If outside the U.S. and Canada, please contact our <a href="http://www.spark.net/contact.htm">Beverly Hills Headquarters.</a></p>

</td></tr></table></p>',null,
'6',GETDATE(),'1','False',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100019175','6340','9051',0,N'How do I take someone off a Hot List?',N'<p><table border="0" cellpadding="2" cellspacing="0" width="592" id="table1">
	 <tr>
	  <td width="2%" valign="top"><strong>Q.</strong></td>
	  <td><strong>How do I take someone off a Hot List?</strong></td>
	</tr>
	<tr>
	  <td valign="top">A.</td>
	  <td>

<p>You can take someone off of your Favorites list or any custom list that you''ve created. Here''s how:<br>
 <ul>
              <li>Go to your Favorites list or custom list and click Move in the profile of the member you want to take off</a></li>
              <li>Click Unlist in the drop-down menu</li>
            </ul>
            </p>

<p>For your convenience, members will remain in Viewed, Emailed, IM''d and Flirt lists so that you have a record. <br>
 <ul>

</td></tr></table></p>',null,
'7',GETDATE(),'1','False',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100019178','6334','9051',0,N'How do I keep other members from knowing that I looked at their profile or Hot Listed them?',N'<p><table border="0" cellpadding="2" cellspacing="0" width="592" id="table1">
	 <tr>
	  <td width="2%" valign="top"><strong>Q.</strong></td>
	  <td><strong>How do I keep other members from knowing that I looked at their profile or Hot Listed them?</strong></td>
	</tr>
	<tr>
	  <td valign="top">A.</td>
	  <td>
 
<p>If you don''t want other members to know when you''ve looked at their profile 
              or Hot Listed them, go to <a href="/Applications/MemberServices/DisplaySettings.aspx">Show/Hide Profile</a> 
              in <a href="/Applications/MemberServices/MemberServices.aspx">Member Services</a> and select &quot;hide&quot; under &quot;Show/Hide When You View Or Hot List Members.&quot;</p>

<p>Keep in mind that members who are most active on the site keep their profile visible in all places because it increases their chances of connecting. You never know, someone special may be looking for you but will never find you if your profile is hidden.</p>

            <p>Hot Lists have been a real hit on the site and have helped bring 
              some unsuspecting couples together. People even tell us that 
              they will contact a member simply because he or she looked at their profile. So before you hit &quot;hide,&quot; 
              give it a chance to work.</p> 
  

</td></tr></table></p>',null,
'8',GETDATE(),'1','False',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100019181','6346','9051',0,N'Why do people want other members to know when they look at their profile or Hot List them?',N'<p><table border="0" cellpadding="2" cellspacing="0" width="592" id="table1">
	 <tr>
	  <td width="2%" valign="top"><strong>Q.</strong></td>
	  <td><strong>Why do people want other members to know when they look at their profile or Hot List them? </strong></td>
	</tr>
	<tr>
	  <td valign="top">A.</td>
	  <td>
 
<p>Members who are very active on the site like to let other members know when they look at their profile or Hot List them because it livens things up and often breaks the ice when it comes to emailing. </p>

<p>So many times we hear that members contact someone simply because he or she looked their profile. Members also say that their Hot Lists lead them to meet members they might not have met otherwise.</p>

<p>Think about it. If you saw that someone had Hot Listed you and you liked what you saw, wouldn''t you be more likely to send an email?
</p> 

</td></tr></table></p>',null,
'9',GETDATE(),'1','False',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100019184','6344','9051',0,N'Why does my Hot List counter change? ',N'
<p><table border="0" cellpadding="2" cellspacing="0" width="592" id="table1">
	 <tr>
	  <td width="2%" valign="top"><strong>Q.</strong></td>
	  <td><strong>Why does my Hot List counter change?  </strong></td>
	</tr>
	<tr>
	  <td valign="top">A.</td>
	  <td>
 
<p>Half of your Hot Lists show your activity on the site. Every time you look 
              at or contact another member, it shows up on your Hot Lists and 
              the numbers go up accordingly.</p>

<p>The other half of your Hot Lists tell you about what other members are doing with regards to you -- looking at your profile, emailing you, etc. Every time people look at your profile or contact you, their profiles show up on your Hot Lists, and the numbers go up accordingly. Keep in mind, however, that some members choose to hide their profile from showing up in your Hot Lists.</p>

<p>Also, in order to keep your Hot Lists up to date, we periodically clean out the members that have been in your "viewed" Hot List for a while. This also changes the counter.</p>

</td></tr></table></p>',null,
'10',GETDATE(),'1','False',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100019187','6480','9051',0,N'How do I enable cookies?',N'<p>
       <table border="0" cellpadding="2" cellspacing="0" width="592" id="table1">
        <tr> 
          <td width="2%" valign="top"><strong>Q.</strong></td>
          <td><strong>How do I enable cookies?</strong></td>
        </tr>
        <tr> 
          <td valign="top">A.</td>
          <td> <p><strong>AOL 8.0</strong></p>
            <p>From the AOL Toolbar, select Settings</p>
            <ol>
              <li>Select Preferences</li>
              <li>Select Internet Properties (WWW)</li>
              <li>Select the Privacy tab</li>
              <li>Select Advanced</li>
              <li>Deselect the Override Automatic Cookie Handling button</li>
              <li>Click OK to exit</li>
            </ol>
            <p><strong>AOL 7.0 with IE 6.x</strong></p>
            <p>From the AOL Toolbar, select Settings</p>
            <ol>
              <li>Select Preferences</li>
              <li>Select Internet Properties (WWW)</li>
              <li>Select the Privacy tab</li>
              <li>Select Advanced</li>
              <li>Deselect the Override Automatic Cookie Handling button</li>
              <li>Click OK exit</li>
            </ol>
            <p><strong>AOL 7.0 with IE 5.5</strong> </p>
            <p>From the AOL Toolbar, select Settings</p>
            <ol>
              <li>Select Preferences.</li>
              <li>Select Internet Properties (WWW)</li>
              <li>Select the Security tab</li>
              <li>Select the Custom Level tab under Allow Cookies 
                That Are Stored On Your Computer</li>
              <li>Click Enable</li>
              <li>Under Allow Per-Session Cookies (not stored), click Enable.</li>
              <li>Select OK and Yes (you want to save 
                the settings)</li>
            </ol>
            <p><strong>AOL 6.0 </strong></p>
            <p>From the AOL Toolbar, select Settings</p>
            <ol>
              <li>Select Preferences</li>
              <li>Select Internet Properties (WWW)</li>
              <li>Select the Security tab</li>
              <li>Select the Custom Level tab under Allow Cookies 
                That Are Stored On Your Computer</li>
              <li>Click Enable</li>
              <li>Under Allow Per-Session Cookies (not stored), click Enable.</li>
              <li>Select OK and Yes (you want to save 
                the settings)</li>
            </ol>
            <p><strong>AOL 5.0 </strong></p>
            <p>Go to My AOL</p>
            <ol>
              <li>Pick WWW</li>
              <li>Click the Security tab</li>
              <li>Go to Custom Level</li>
              <li>Scroll down to find Cookie</li>
              <li>Click Enable</li>
              <li> Click OK</li>
            </ol>
            <p><strong>AOL 4.0 </strong></p>
            <p>Click Preferences</p>
            <ol>
              <li>Click the WWW button</li>
              <li>Click the Advanced tab</li>
              <li>Select Accept All Cookies</li>
              <li>Check the box </li>
            </ol>
            <p><strong>AOL for Windows 3.1</strong></p>
            <p>Browser does not give you the ability to turn off cookies.</p>
            <p><strong>Windows IE 6.x Browser </strong></p>
            <p>Select Tools</p>
            <ol>
              <li>Select Internet Options</li>
              <li>Select the Privacy tab</li>
              <li>Select Advanced</li>
              <li>Deselect the Override Automatic Cookie Handling button</li>
              <li>Click OK at the bottom of the screen</li>
              <li>Click OK to exit </li>
            </ol>
            <p><strong>Windows IE 5.x Browser</strong></p>
            <p>Go to Tools on the menu bar</p>
            <ol>
              <li>Pick Internet Options</li>
              <li>Click the Security tab</li>
              <li>Select the Custom Level tab</li>
              <li>Under Allow Cookies That Are Stored On Your Computer, click 
                Enable </li>
              <li>Under Allow Per-Session Cookies (not stored), click Enable</li>
              <li>Select OK and Yes (you want to save 
                the settings)</li>
            </ol>
            <p><strong>Windows IE 4.x Browser</strong></p>
            <p>Go to View on the menu bar</p>
            <ol>
              <li>Pick Internet Options</li>
              <li>Click the Advanced tab</li>
              <li>Go down to the Cookies section</li>
              <li> Click Always Accept Cookies</li>
              <li>Click OK </li>
            </ol>
            <p><strong>Windows IE 3.x Browser</strong></p>
            <p>Go to View on the menu bar</p>
            <ol>
              <li>Pick Options</li>
              <li>Click the Advanced tab</li>
              <li>Go down to the Cookies section</li>
              <li>Click Warn Before Accept Cookies</li>
              <li>Click OK </li>
            </ol>
            <p><strong>Netscape 7.x </strong></p>
            <p>Click Edit on the toolbar </p>
            <ol>
              <li>Click Preferences</li>
              <li>Click the Privacy and Security category and expand 
                the list to show the subcategories</li>
              <li>Click Cookies</li>
              <li>Check either Enable Cookies For The Originating Web Site 
                Only, Enable Cookies Based On Privacy Settings 
                or Enable All Cookies</li>
            </ol>
			  
              </td>
        </tr>
        <tr>
          <td valign="top">&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
      </table>
      </p>',null,
'1',GETDATE(),'1','False',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100019190','6482','9051',0,N'Why does the site seem to be slow?',N'<p><table border="0" cellpadding="2" cellspacing="0" width="592" id="table1">
	 <tr>
	  <td width="2%" valign="top"><strong>Q.</strong></td>
	  <td><strong>Why does the site seem to be slow?</strong></td>
	</tr>
	<tr>
	  <td valign="top">A.</td>
	  <td> 
  
<p>If our site seems slow, it''s usually one of three things:</p> 
<ul><li>Temporary technical down-time while we''re making quick updates to the site. Try back in a while, and if the problem persists please call our Customer Care team toll-free: ((CSSUPPORTPHONENUMBER)). If outside the U.S. and Canada, please contact our <a href="http://www.spark.net/contact.htm">Beverly Hills Headquarters.</a></li>
<li>Your Internet connection rate is slow. Double-check your modem settings or connection speed to make sure you''re connected at the highest possible rate. </li>
<li>The Internet itself may be experiencing a traffic jam. Try back in a while.</li></ul></p>

</td></tr></table></p>',null,
'2',GETDATE(),'1','False',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100019193','100000048','9051',0,N'How do I clear my cache & cookies for Internet Explorer (version 6.0)?',N'<p>
       <table border="0" cellpadding="2" cellspacing="0" width="592" id="table1">
        <tr> 
          <td colspan="2"><strong>How do I clear my cache &amp; cookies for Internet Explorer (version 6.0)?</strong></td>
        </tr>
        <tr> 
          <td colspan="2"><p>Follow the steps below to clear your Internet cache 
              and your browser cookies. Click the images to see 
              a larger view.</p>
            <br> </td>
        </tr>
      </table>
		
      <table cellpadding="4" cellspacing="1" bgcolor="#DDDCD3" width="592">
        <tr> 
          <td colspan="2" class="medium"><strong>Step 1</strong></td>
        </tr>
        <tr> 
          <td width="80%" rowspan="2" valign="top" class="white"> <p>Click on the ‘Tools’ tab on the top of your browser and choose ‘Internet Options’ from the bottom of the menu.</p></td>
          <td class="white">


<a href="javascript:launchWindow(''((image:articles/scrn_technicalBasics_IE_01Fu.gif))'', ''Clear Your Cache & Cookies IE (version 6.0)'', ''610'', ''510'', '''')"><img src="((image:articles/scrn_technicalBasics_IE_01.gif))" alt="Click for larger view" width="180" height="150" border="0" class="profileImageHover"></a></td>
		</tr>
        <tr> 
          <td class="white"> <div align="left"><a href="javascript:launchWindow(''((image:articles/scrn_technicalBasics_IE_01Fu.gif))'', ''Clear Your Cache & Cookies IE (version 6.0)'', ''610'', ''510'', '''')"><img border="0" src="((image:icon_increaseSize.gif))" width="20" height="19" align="absmiddle" alt="Click for larger view" hspace="3"></a>Click 
              for larger view</div></td>
        </tr>
        <tr> 
          <td colspan="2" valign="top" class="medium"><strong>Step 2</strong></td>
        </tr>
        <tr> 
          <td width="80%" rowspan="2" valign="top" class="white"> <p>The ‘Internet Options’ window should open with the contents of the ‘General’ tab displayed.  If it does not open with the ‘General’ tab displayed, click on the ‘General’ tab.</p>
			<p>To execute the ‘clear cache’ process, click on the ‘Delete Files’ button.</p></td>
          <td class="white"><a href="javascript:launchWindow(''((image:articles/scrn_technicalBasics_IE_02Fu.gif))'', ''Clear Your Cache & Cookies IE (version 6.0)'', ''610'', ''510'', '''')"><img src="((image:articles/scrn_technicalBasics_IE_02.gif))" class="profileImageHover" alt="Click for larger view" width="180" height="150" border="0"></a></td>
        </tr>
        <tr>
          <td class="white"><a href="javascript:launchWindow(''((image:articles/scrn_technicalBasics_IE_02Fu.gif))'', ''Clear Your Cache & Cookies IE (version 6.0)'', ''610'', ''510'', '''')"><img border="0" src="((image:icon_increaseSize.gif))" width="20" height="19" align="absmiddle" alt="Click for larger view" hspace="3"></a>Click 
            for larger view</td>
        </tr>
        <tr> 
          <td colspan="2" valign="top" class="medium"><strong>Step 3</strong></td>
        </tr>
        <tr> 
          <td width="80%" rowspan="2" valign="top" class="white"> <p>A new box will appear confirming the action to delete all cookies in the Temporary Internet Files folder.  Click ‘OK’.</p>
			<p>The process may take a couple of minutes depending on how much data has to be cleared.</p></td>
          <td class="white"><a href="javascript:launchWindow(''((image:articles/scrn_technicalBasics_IE_03Fu.gif))'', ''Clear Your Cache & Cookies IE (version 6.0)'', ''610'', ''510'', '''')"><img src="((image:articles/scrn_technicalBasics_IE_03.gif))" class="profileImageHover" alt="Click for larger view" width="180" height="150" border="0"></a></td>
        </tr>
        <tr> 
          <td class="white"><a href="javascript:launchWindow(''((image:articles/scrn_technicalBasics_IE_03Fu.gif))'', ''Clear Your Cache & Cookies IE (version 6.0)'', ''610'', ''510'', '''')"><img border="0" src="((image:icon_increaseSize.gif))" width="20" height="19" align="absmiddle" alt="Click for larger view" hspace="3"></a>Click 
            for larger view</td>
        </tr>
        <tr> 
          <td colspan="2" valign="top" class="medium"><strong>Step 4</strong></td>
        </tr>
        <tr> 
          <td width="80%" rowspan="2" valign="top" class="white"> <p>After the process of deleting temporary files has been completed, the ‘Internet Options’ window should remain open.  To delete old cookies, click ‘Delete Cookies’.</p></td>
          <td class="white"><a href="javascript:launchWindow(''((image:articles/scrn_technicalBasics_IE_04Fu.gif))'', ''Clear Your Cache & Cookies IE (version 6.0)'', ''610'', ''510'', '''')"><img src="((image:articles/scrn_technicalBasics_IE_04.gif))" class="profileImageHover" alt="Click for larger view" width="180" height="150" border="0"></a></td>
        </tr>
        <tr> 
          <td class="white"><a href="javascript:launchWindow(''((image:articles/scrn_technicalBasics_IE_04Fu.gif))'', ''Clear Your Cache & Cookies IE (version 6.0)'', ''610'', ''510'', '''')"><img border="0" src="((image:icon_increaseSize.gif))" width="20" height="19" align="absmiddle" alt="Click for larger view" hspace="3"></a>Click 
            for larger view</td>
        </tr>
        <tr> 
          <td colspan="2" valign="top" class="medium"><strong>Step 5</strong></td>
        </tr>
        <tr> 
          <td width="80%" rowspan="2" valign="top" class="white"> <p>Click the ‘OK’ to initiate the process.</p>
			<p>After the process of deleting temporary files has been completed, the ‘Internet Options’ window should remain open.  To delete old cookies, click ‘Delete Cookies’.</p>
			<p>The process may take a couple of minutes depending on how much data has to be cleared.</p></td>
          <td class="white"><a href="javascript:launchWindow(''((image:articles/scrn_technicalBasics_IE_05Fu.gif))'', ''Clear Your Cache & Cookies IE (version 6.0)'', ''610'', ''510'', '''')"><img src="((image:articles/scrn_technicalBasics_IE_05.gif))" class="profileImageHover" alt="Click for larger view" width="180" height="150" border="0"></a></td>
        </tr>
        <tr> 
          <td class="white"><a href="javascript:launchWindow(''((image:articles/scrn_technicalBasics_IE_05Fu.gif))'', ''Clear Your Cache & Cookies IE (version 6.0)'', ''610'', ''510'', '''')"><img border="0" src="((image:icon_increaseSize.gif))" width="20" height="19" align="absmiddle" alt="Click for larger view" hspace="3"></a>Click 
            for larger view</td>
        </tr>
        <tr> 
          <td colspan="2" valign="top" class="medium"><strong>Step 6</strong></td>
        </tr>
        <tr> 
          <td width="80%" rowspan="2" valign="top" class="white"> <p>After the process of deleting cookies has been completed, close the window by clicking the ’OK’ button.</p></td>
          <td class="white"><a href="javascript:launchWindow(''((image:articles/scrn_technicalBasics_IE_06Fu.gif))'', ''Clear Your Cache & Cookies IE (version 6.0)'', ''610'', ''510'', '''')"><img src="((image:articles/scrn_technicalBasics_IE_06.gif))" class="profileImageHover" alt="Click for larger view" width="180" height="150" border="0"></a></td>
        </tr>
        <tr> 
          <td class="white"><a href="javascript:launchWindow(''((image:articles/scrn_technicalBasics_IE_06Fu.gif))'', ''Clear Your Cache & Cookies IE (version 6.0)'', ''610'', ''510'', '''')"><img border="0" src="((image:icon_increaseSize.gif))" width="20" height="19" align="absmiddle" alt="Click for larger view" hspace="3"></a>Click 
            for larger view</td>
        </tr>
      </table>

<p></p>',null,
'3',GETDATE(),'1','False',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100019196','6478','9051',0,N'How do I clear my cache & cookies for Netscape Navigator (version 7.0)?',N'<p>
       <table border="0" cellpadding="2" cellspacing="0" width="592" id="table1">
        <tr> 
          <td width="2%" valign="top"><strong>Q.</strong></td>
          <td colspan="2"><strong>How do I clear my cache &amp; cookies for Netscape Navigator (version 7.0)?</strong></td>
        </tr>
        <tr> 
          <td valign="top">A.</td>
          <td colspan="2"><p>Follow the steps below to clear your Internet cache 
              and your browser cookies. Click the images to see 
              a larger view.</p>
            <br> </td>
        </tr>
		</table>
		
      <table cellpadding="4" cellspacing="1" bgcolor="#DDDCD3" width="592">
        <tr> 
          <td colspan="2" class="medium"><strong>Step 1</strong></td>
        </tr>
        <tr> 
          <td width="80%" rowspan="2" valign="top" class="white"> <p>Click 
              the Edit tab at the top of your browser and choose Preferences 
              from the drop down menu.</p></td>
          <td class="white"><a href="javascript:launchWindow(''((image:articles/scrn_technicalBasics_netscape_01Full.gif))'', ''Clear Your Cache & Cookies Netscape Navigator (version 7.0)'', ''610'', ''510'', '''')"><img src="((image:articles/scrn_technicalBasics_netscape_01.gif))" alt="Click for larger view" width="180" height="150" border="0" class="profileImageHover"></a></td>
		</tr>
        <tr> 
          <td class="white"> <div align="left"><a href="javascript:launchWindow(''((image:articles/scrn_technicalBasics_netscape_01Full.gif))'', ''Clear Your Cache & Cookies Netscape Navigator (version 7.0)'', ''610'', ''510'', '''')"><img border="0" src="((image:icon_increaseSize.gif))" width="20" height="19" align="absmiddle" alt="Click for larger view" hspace="3"></a>Click 
              for larger view</div></td>
        </tr>
        <tr> 
          <td colspan="2" valign="top" class="medium"><strong>Step 2</strong></td>
        </tr>
        <tr> 
          <td width="80%" rowspan="2" valign="top" class="white"> <p>On the left 
              side under the categories, open the subcategory Privacy 
              Security by clicking the arrow next to the topic.</p></td>
          <td class="white"><a href="javascript:launchWindow(''((image:articles/scrn_technicalBasics_netscape_02Full.gif))'', ''Clear Your Cache & Cookies Netscape Navigator (version 7.0)'', ''610'', ''510'', '''')"><img src="((image:articles/scrn_technicalBasics_netscape_02.gif))" class="profileImageHover" alt="Click for larger view" width="180" height="150" border="0"></a></td>
        </tr>
        <tr>
          <td class="white"><a href="javascript:launchWindow(''((image:articles/scrn_technicalBasics_netscape_02Full.gif))'', ''Clear Your Cache & Cookies Netscape Navigator (version 7.0)'', ''610'', ''510'', '''')"><img border="0" src="((image:icon_increaseSize.gif))" width="20" height="19" align="absmiddle" alt="Click for larger view" hspace="3"></a>Click 
            for larger view</td>
        </tr>
        <tr> 
          <td colspan="2" valign="top" class="medium"><strong>Step 3</strong></td>
        </tr>
        <tr> 
          <td width="80%" rowspan="2" valign="top" class="white"> <p>Choose Cookies 
              and be sure that Enable All Cookies is selected. Then 
              click the Manage Stored Cookies button on the right.</p></td>
          <td class="white"><a href="javascript:launchWindow(''((image:articles/scrn_technicalBasics_netscape_03Full.gif))'', ''Clear Your Cache & Cookies Netscape Navigator (version 7.0)'', ''610'', ''510'', '''')"><img src="((image:articles/scrn_technicalBasics_netscape_03.gif))" class="profileImageHover" alt="Click for larger view" width="180" height="150" border="0"></a></td>
        </tr>
        <tr> 
          <td class="white"><a href="javascript:launchWindow(''((image:articles/scrn_technicalBasics_netscape_03Full.gif))'', ''Clear Your Cache & Cookies Netscape Navigator (version 7.0)'', ''610'', ''510'', '''')"><img border="0" src="((image:icon_increaseSize.gif))" width="20" height="19" align="absmiddle" alt="Click for larger view" hspace="3"></a>Click 
            for larger view</td>
        </tr>
        <tr> 
          <td colspan="2" valign="top" class="medium"><strong>Step 4</strong></td>
        </tr>
        <tr> 
          <td width="80%" rowspan="2" valign="top" class="white"> <p>In the new 
              window, choose the ((PLDOMAIN)) cookie from the 
              white box. Click the Remove Cookie button 
              on the lower left and click OK.</p></td>
          <td class="white"><a href="javascript:launchWindow(''((image:articles/scrn_technicalBasics_netscape_04Full.gif))'', ''Clear Your Cache & Cookies Netscape Navigator (version 7.0)'', ''610'', ''510'', '''')"><img src="((image:articles/scrn_technicalBasics_netscape_04.gif))" class="profileImageHover" alt="Click for larger view" width="180" height="150" border="0"></a></td>
        </tr>
        <tr> 
          <td class="white"><a href="javascript:launchWindow(''((image:articles/scrn_technicalBasics_netscape_04Full.gif))'', ''Clear Your Cache & Cookies Netscape Navigator (version 7.0)'', ''610'', ''510'', '''')"><img border="0" src="((image:icon_increaseSize.gif))" width="20" height="19" align="absmiddle" alt="Click for larger view" hspace="3"></a>Click 
            for larger view</td>
        </tr>
        <tr> 
          <td colspan="2" valign="top" class="medium"><strong>Step 5</strong></td>
        </tr>
        <tr> 
          <td width="80%" rowspan="2" valign="top" class="white"> <p>Open the 
              Advance category then select Cache from 
              the listings. Make sure you have the option Once Per Session 
              chosen.</p></td>
          <td class="white"><a href="javascript:launchWindow(''((image:articles/scrn_technicalBasics_netscape_05Full.gif))'', ''Clear Your Cache & Cookies Netscape Navigator (version 7.0)'', ''610'', ''510'', '''')"><img src="((image:articles/scrn_technicalBasics_netscape_05.gif))" class="profileImageHover" alt="Click for larger view" width="180" height="150" border="0"></a></td>
        </tr>
        <tr> 
          <td class="white"><a href="javascript:launchWindow(''((image:articles/scrn_technicalBasics_netscape_05Full.gif))'', ''Clear Your Cache & Cookies Netscape Navigator (version 7.0)'', ''610'', ''510'', '''')"><img border="0" src="((image:icon_increaseSize.gif))" width="20" height="19" align="absmiddle" alt="Click for larger view" hspace="3"></a>Click 
            for larger view</td>
        </tr>
        <tr> 
          <td colspan="2" valign="top" class="medium"><strong>Step 6</strong></td>
        </tr>
        <tr> 
          <td width="80%" rowspan="2" valign="top" class="white"> <p>Now, click 
              the Clear Memory Cache button...</p></td>
          <td class="white"><a href="javascript:launchWindow(''((image:articles/scrn_technicalBasics_netscape_06Full.gif))'', ''Clear Your Cache & Cookies Netscape Navigator (version 7.0)'', ''610'', ''510'', '''')"><img src="((image:articles/scrn_technicalBasics_netscape_06.gif))" class="profileImageHover" alt="Click for larger view" width="180" height="150" border="0"></a></td>
        </tr>
        <tr> 
          <td class="white"><a href="javascript:launchWindow(''((image:articles/scrn_technicalBasics_netscape_06Full.gif))'', ''Clear Your Cache & Cookies Netscape Navigator (version 7.0)'', ''610'', ''510'', '''')"><img border="0" src="((image:icon_increaseSize.gif))" width="20" height="19" align="absmiddle" alt="Click for larger view" hspace="3"></a>Click 
            for larger view</td>
        </tr>
        <tr> 
          <td colspan="2" valign="top" class="medium"><strong>Step 7</strong></td>
        </tr>
        <tr> 
          <td width="80%" rowspan="2" valign="top" class="white"> <p>...then click 
              Clear Disk Cache and OK.</p></td>
          <td class="white"><a href="javascript:launchWindow(''((image:articles/scrn_technicalBasics_netscape_07Full.gif))'', ''Clear Your Cache & Cookies Netscape Navigator (version 7.0)'', ''610'', ''510'', '''')"><img src="((image:articles/scrn_technicalBasics_netscape_07.gif))" class="profileImageHover" alt="Click for larger view" width="180" height="150" border="0"></a></td>
        </tr>
        <tr> 
          <td class="white"><a href="javascript:launchWindow(''((image:articles/scrn_technicalBasics_netscape_07Full.gif))'', ''Clear Your Cache & Cookies Netscape Navigator (version 7.0)'', ''610'', ''510'', '''')"><img border="0" src="((image:icon_increaseSize.gif))" width="20" height="19" align="absmiddle" alt="Click for larger view" hspace="3"></a>Click 
            for larger view</td>
        </tr>
      </table>

<p></p>',null,
'4',GETDATE(),'1','False',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100019199','6472','9051',0,N'How do I clear my cache & cookies for AOL (version 8.0)?',N'<p>
       <table border="0" cellpadding="2" cellspacing="0" width="592" id="table1">
        <tr> 
          <td colspan="2"><strong>How do I clear my cache & cookies for AOL (version 8.0)?</strong></td>
        </tr>
        <tr> 
          <td colspan="2"><p>Follow the steps below to clear your internet cache 
              and your browser cookies. Click the images to see 
              a larger view.</p>
            <br> </td>
        </tr>
      </table>
		
      <table cellpadding="4" cellspacing="1" bgcolor="#DDDCD3" width="592">
        <tr> 
          <td colspan="2" class="medium"><strong>Step 1</strong></td>
        </tr>
        <tr> 
          <td width="80%" rowspan="2" valign="top" class="white"> <p>Click 
              the Settings Tab and choose Preferences
              from the drop down list.</p></td>
          <td class="white">


<a href="javascript:launchWindow(''((image:articles/scrn_technicalBasics_AOL_01Full.gif))'', ''Clear Your Cache & Cookies AOL (version 8.0)'', ''610'', ''510'', '''')"><img src="((image:articles/scrn_technicalBasics_AOL_01.gif))" alt="Click for larger view" width="180" height="150" border="0" class="profileImageHover"></a></td>
		</tr>
        <tr> 
          <td class="white"> <div align="left"><a href="javascript:launchWindow(''((image:articles/help_technicalBasics_aol01Full.gif))'', ''Clear Your Cache & Cookies AOL (version 8.0)'', ''561'', ''468'', '''')"><img border="0" src="((image:icon_increaseSize.gif))" width="20" height="19" align="absmiddle" alt="Click for larger view" hspace="3"></a>Click 
              for larger view</div></td>
        </tr>
        <tr> 
          <td colspan="2" valign="top" class="medium"><strong>Step 2</strong></td>
        </tr>
        <tr> 
          <td width="80%" rowspan="2" valign="top" class="white"> <p>Select Internet Properties WWW in the 
              lower right.</p></td>
          <td class="white"><a href="javascript:launchWindow(''((image:articles/scrn_technicalBasics_AOL_02Full.gif))'', ''Clear Your Cache & Cookies AOL (version 8.0)'', ''561'', ''468'', '''')"><img src="((image:articles/scrn_technicalBasics_AOL_02.gif))" class="profileImageHover" alt="Click for larger view" width="180" height="150" border="0"></a></td>
        </tr>
        <tr>
          <td class="white"><a href="javascript:launchWindow(''((image:articles/scrn_technicalBasics_AOL_02Full.gif))'', ''Clear Your Cache & Cookies AOL (version 8.0)'', ''561'', ''468'', '''')"><img border="0" src="((image:icon_increaseSize.gif))" width="20" height="19" align="absmiddle" alt="Click for larger view" hspace="3"></a>Click 
            for larger view</td>
        </tr>
        <tr> 
          <td colspan="2" valign="top" class="medium"><strong>Step 3</strong></td>
        </tr>
        <tr> 
          <td width="80%" rowspan="2" valign="top" class="white"> <p>Now, click 
              the Delete File button...</p></td>
          <td class="white"><a href="javascript:launchWindow(''((image:articles/scrn_technicalBasics_AOL_03Full.gif))'', ''Clear Your Cache & Cookies AOL (version 8.0)'', ''561'', ''468'', '''')"><img src="((image:articles/scrn_technicalBasics_AOL_03.gif))" class="profileImageHover" alt="Click for larger view" width="180" height="150" border="0"></a></td>
        </tr>
        <tr> 
          <td class="white"><a href="javascript:launchWindow(''((image:articles/scrn_technicalBasics_AOL_03Full.gif))'', ''Clear Your Cache & Cookies AOL (version 8.0)'', ''561'', ''468'', '''')"><img border="0" src="((image:icon_increaseSize.gif))" width="20" height="19" align="absmiddle" alt="Click for larger view" hspace="3"></a>Click 
            for larger view</td>
        </tr>
        <tr> 
          <td colspan="2" valign="top" class="medium"><strong>Step 4</strong></td>
        </tr>
        <tr> 
          <td width="80%" rowspan="2" valign="top" class="white"> <p>...then click 
              the white box next to Delete All Offline Content.
              Click OK.</p></td>
          <td class="white"><a href="javascript:launchWindow(''((image:articles/scrn_technicalBasics_AOL_04Full.gif))'', ''Clear Your Cache & Cookies AOL (version 8.0)'', ''561'', ''468'', '''')"><img src="((image:articles/scrn_technicalBasics_AOL_04.gif))" class="profileImageHover" alt="Click for larger view" width="180" height="150" border="0"></a></td>
        </tr>
        <tr> 
          <td class="white"><a href="javascript:launchWindow(''((image:articles/scrn_technicalBasics_AOL_04Full.gif))'', ''Clear Your Cache & Cookies AOL (version 8.0)'', ''561'', ''468'', '''')"><img border="0" src="((image:icon_increaseSize.gif))" width="20" height="19" align="absmiddle" alt="Click for larger view" hspace="3"></a>Click 
            for larger view</td>
        </tr>
        <tr> 
          <td colspan="2" valign="top" class="medium"><strong>Step 5</strong></td>
        </tr>
        <tr> 
          <td width="80%" rowspan="2" valign="top" class="white"> <p>Now click 
              the Settings button. At the top of the page, verify 
              that your settings read &quot;Automatically,&quot; then click 
              the View Files at the bottom of the page.</p></td>
          <td class="white"><a href="javascript:launchWindow(''((image:articles/scrn_technicalBasics_AOL_05Full.gif))'', ''Clear Your Cache & Cookies AOL (version 8.0)'', ''561'', ''468'', '''')"><img src="((image:articles/scrn_technicalBasics_AOL_05.gif))" class="profileImageHover" alt="Click for larger view" width="180" height="150" border="0"></a></td>
        </tr>
        <tr> 
          <td class="white"><a href="javascript:launchWindow(''((image:articles/scrn_technicalBasics_AOL_05Full.gif))'', ''Clear Your Cache & Cookies AOL (version 8.0)'', ''561'', ''468'', '''')"><img border="0" src="((image:icon_increaseSize.gif))" width="20" height="19" align="absmiddle" alt="Click for larger view" hspace="3"></a>Click 
            for larger view</td>
        </tr>
        <tr> 
          <td colspan="2" valign="top" class="medium"><strong>Step 6</strong></td>
        </tr>
        <tr> 
          <td width="80%" rowspan="2" valign="top" class="white"> <p>Click 
              Edit in the upper left, then choose Select All 
              from the drop down menu.</p></td>
          <td class="white"><a href="javascript:launchWindow(''((image:articles/scrn_technicalBasics_AOL_06Full.gif))'', ''Clear Your Cache & Cookies AOL (version 8.0)'', ''561'', ''468'', '''')"><img src="((image:articles/scrn_technicalBasics_AOL_06.gif))" class="profileImageHover" alt="Click for larger view" width="180" height="150" border="0"></a></td>
        </tr>
        <tr> 
          <td class="white"><a href="javascript:launchWindow(''((image:articles/scrn_technicalBasics_AOL_06Full.gif))'', ''Clear Your Cache & Cookies AOL (version 8.0)'', ''561'', ''468'', '''')"><img border="0" src="((image:icon_increaseSize.gif))" width="20" height="19" align="absmiddle" alt="Click for larger view" hspace="3"></a>Click 
            for larger view</td>
        </tr>
        <tr> 
          <td colspan="2" valign="top" class="medium"><strong>Step 7</strong></td>
        </tr>
        <tr> 
          <td width="80%" rowspan="2" valign="top" class="white"> <p>Make sure 
              that all the files are selected, then press the Delete key from 
              your keyboard or click the Delete button in the top
              tool bar.</p></td>
          <td class="white"><a href="javascript:launchWindow(''((image:articles/scrn_technicalBasics_AOL_07Full.gif))'', ''Clear Your Cache & Cookies AOL (version 8.0)'', ''561'', ''468'', '''')"><img src="((image:articles/scrn_technicalBasics_AOL_07.gif))" class="profileImageHover" alt="Click for larger view" width="180" height="150" border="0"></a></td>
        </tr>
        <tr> 
          <td class="white"><a href="javascript:launchWindow(''((image:articles/scrn_technicalBasics_AOL_07Full.gif))'', ''Clear Your Cache & Cookies AOL (version 8.0)'', ''561'', ''468'', '''')"><img border="0" src="((image:icon_increaseSize.gif))" width="20" height="19" align="absmiddle" alt="Click for larger view" hspace="3"></a>Click 
            for larger view</td>
        </tr>
        <tr> 
          <td colspan="2" valign="top" class="medium"><strong>Step 8</strong></td>
        </tr>
		<tr> 
          <td width="80%" rowspan="2" valign="top" class="white"> <p>When you 
              are asked, &quot;Are you sure you want to delete the selected Cookie(s)?,&quot; 
              click Yes.</p></td>
          <td class="white"><a href="javascript:launchWindow(''((image:articles/scrn_technicalBasics_AOL_08Full.gif))'', ''Clear Your Cache & Cookies AOL (version 8.0)'', ''561'', ''468'', '''')"><img src="((image:articles/scrn_technicalBasics_AOL_08.gif))" class="profileImageHover" alt="Click for larger view" width="180" height="150" border="0"></a></td>
        </tr>
        <tr> 
          <td class="white"><a href="javascript:launchWindow(''((image:articles/scrn_technicalBasics_AOL_08Full.gif))'', ''Clear Your Cache & Cookies AOL (version 8.0)'', ''561'', ''468'', '''')"><img border="0" src="((image:icon_increaseSize.gif))" width="20" height="19" align="absmiddle" alt="Click for larger view" hspace="3"></a>Click 
            for larger view</td>
        </tr>
        <tr> 
          <td colspan="2" valign="top" class="medium"><strong>Step 9</strong></td>
        </tr>
        <tr> 
          <td width="80%" rowspan="2" valign="top" class="white"> <p>Confirm that 
              all the files are deleted. Close this window and click OK
              on all subsequent windows.</p></td>
          <td class="white"><a href="javascript:launchWindow(''((image:articles/scrn_technicalBasics_AOL_09Full.gif))'', ''Clear Your Cache & Cookies AOL (version 8.0)'', ''561'', ''468'', '''')"><img src="((image:articles/scrn_technicalBasics_AOL_09.gif))" class="profileImageHover" alt="Click for larger view" width="180" height="150" border="0"></a></td>
        </tr>
        <tr> 
          <td class="white"><a href="javascript:launchWindow(''((image:articles/scrn_technicalBasics_AOL_09Full.gif))'', ''Clear Your Cache & Cookies AOL (version 8.0)'', ''561'', ''468'', '''')"><img border="0" src="((image:icon_increaseSize.gif))" width="20" height="19" align="absmiddle" alt="Click for larger view" hspace="3"></a>Click 
            for larger view</td>
        </tr>
      </table>

<p></p>',null,
'5',GETDATE(),'1','False',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100019202','6474','9051',0,N'How do I send photos through AOL email?',N'<p><table border="0" cellpadding="2" cellspacing="0" width="592" id="table1">
	 <tr>
		<td width="2%" valign="top"><strong>Q.</strong></td>
		<td><strong>How do I send photos through AOL email?</strong></td>
	</tr>
	<tr>
		<td valign="top">A.</td>
		  <td><p>Send one photo per email and make sure to attach or enclose rather than paste it into the body of the email.</p>

<p>To attach:</p>
<p><ol>
              <li>Click Attach</li>
              <li>Use the Browse button to locate your photo</li>
              <li>Select the photo and click Open</li>
              <li>Click OK to return to the email message and then 
                click Send</li>
            </ol></p></td>
        </tr></table></p>',null,
'6',GETDATE(),'1','False',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100019205','6476','9051',0,N'What if I''m having trouble with my AOL browser?',N'<p><table border="0" cellpadding="2" cellspacing="0" width="592" id="table1">
	 <tr>
		<td width="2%" valign="top"><strong>Q.</strong></td>
		<td><strong>What if I''m having trouble with my AOL browser?</strong></td>
	</tr>
	<tr>
		<td valign="top">A.</td>
		  <td> <p>If you are using AOL as your Internet Service Provider, you may have trouble accessing our site. We suggest downloading Netscape Navigator/Communicator or Microsoft Internet Explorer and using one of these browsers to access the site once you are online with AOL. This should eliminate many problems.</p></td>
        </tr></table></p>',null,
'7',GETDATE(),'1','False',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100019208','6450','9051',0,N'Still Stumped?',N'<div id="article">
<p><img class="photo-right" src="((image:jdate_customer_service.jpg))" /><strong>Q. Still Stumped?</strong></p>
<p>A. Don''t hesitate to contact our Customer Care team via <a href="/Applications/ContactUs/ContactUs.aspx">email</a> or by
    calling our toll free number: ((CSSUPPORTPHONENUMBER)). If outside the U.S. and Canada, please contact our <a href="http://www.spark.net/contact.htm">Beverly Hills Headquarters.</a></p>
</p>

</div>',null,
'0',GETDATE(),'1','True',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100019211','6498','9051',0,N'Legal Documents',N'<div>
<a name="top"></a>
<!-- begin Privacy Statement-->
<p><a name="privacy"></a></p>

<a href="#privacy">Privacy Statement</a> | <a href="#service">Terms and Conditions of Service</a> | <a href="#purchase">Terms and Conditions of Purchase</a>
<div align="right"><a href="#top"><img src="((image:btn_top.gif))" width="16" height="15" border="0" /></a></div>
<h1><strong>Privacy Statement</strong></h1>
<p>Spark Networks&reg; Limited has created this privacy policy (the "Privacy Policy") as part of our commitment to helping our users better understand what information we collect about them and what we may do with that information.</p>

<p>The following discloses our information gathering and dissemination practices for the Spark Networks websites.</p>

<p><b>Overview</b></p>

<p>As part of the normal operation of our services we collect and, in some cases, may disclose information about you. By accepting this Privacy Policy and our Terms and Conditions, you expressly consent to our use and disclosure of your personal information in the manner described in this Privacy Policy. This Privacy Policy is incorporated into and subject to the terms of the Spark Networks Terms and Conditions.</p>
  <p>This Privacy Policy applies to all Spark Networks sites, including but not limited to: AmericanSingles.com, Relationships.com, JDate.com, and BlackSingles.com (the "Websites").</p>

<p>1. <b>A Special Note About Children</b></p>
<p>Children are not eligible to use our services, and we ask that minors (under the age of 18) not submit any personal information to us.</p>
<p>2. <b>Information We Collect</b></p>

<p>Our primary goal in collecting personal information is to provide you with a smooth, efficient, and customized experience. This allows us to provide services and features and to customize our service to make your experience better. You agree that we may feature members'' profiles, including your profile, in, among other places, editorials and newsletters that we may periodically send out to our members. Your name, postal address, telephone number and email address are confidential and will not be posted in your profile. Your profile is available for other members to view. Members'' profiles include a description, photos, likes and dislikes, individual essays, and other information helpful in determining matches. Your viewable profile does not include any identifying information about you, except the username you chose upon registering.</p>

<p>Please note that your personally identifiable information will be stored and processed on our computers in the United States. The laws regarding the handling of personal data in the United States may differ from the laws of other countries but, as we explain below, Spark Networks will hold and transmit your information in a safe, confidential and secure environment.</p>

<p>We use data collection devices such as "cookies" on certain pages of our Websites. "Cookies" are small files placed on your hard drive that assist us in providing customized services. We also offer certain features that are only available through the use of a "cookie." Cookies can also help us provide information which is targeted to your interests. We use this information to determine our members'' demographics, interests, and behavior to better understand and serve you and our community. This information may include the URL of the website that you visited prior to our Website (whether this is on our site or not), the URL of the website you next go to (whether this URL is on our site or not), what browser you are using, and your Internet Protocol ("IP") address. Most cookies are "session cookies", meaning that they are automatically deleted from your hard drive at the end of a session. You are always free to decline our cookies if your browser permits.</p>
<p>We use a third party advertising company to serve ads on our behalf across the Internet. That advertising company may also collect anonymous information about your visits to our Websites. This is primarily accomplished through the use of a technology device, commonly referred to as a Web beacon, cookie or an action tag, which is placed on various Web pages within our Websites or in an HTML e-mail that allows the third party advertising company to collect anonymous information. There may also be a number of services offered by external service providers that help you use our Websites. If you choose to use these optional services, and in the course of doing so, disclose information to the external service providers, and/or grant them permission to collect information about you, then their use of your information is governed by their private policies, if any.</p>
<p>Some of the third party advertisers that we use have their own privacy policies and may allow you to opt out of having your information collected as discussed above.  The privacy policies of third party advertisers that we may be using during your use of our services can be found at the following URL(s):<br />
<a href="http://www.247realmedia.com/privacy.html" target="_blank">http://www.247realmedia.com/privacy.html</a>,
<br />
 <a href="http://www.valueclickmedia.com/member_privacy.shtml" target="_blank">http://www.valueclickmedia.com/member_privacy.shtml</a>,
<br />
 <a href="http://www.advertising.com/privacy" target="_blank">http://www.advertising.com/privacy</a><br />
 <a href="http://www.networkadvertising.org/managing/opt_out.asp">http://www.networkadvertising.org/managing/opt_out.asp</a></p>
<p>3. <strong>Our Use of Your Information</strong></p>

<p>We use information in the files we maintain about you, and the other information we obtain from your current and past activities on the Websites to resolve disputes, troubleshoot problems and enforce our Terms and Conditions. At times, we may review the data of multiple members to identify problems or resolve disputes.</p>

<p>You agree that we may use personally identifiable information about you to improve our marketing and promotional efforts, to analyze Website usage, improve our content and product offerings, and customize our Websites'' content, layout, and services. These uses improve our Websites and may better tailor them to meet your needs.</p>

<p>You agree that we may use your information to contact you and to deliver information to you that, in some cases, is targeted to your interests, such as targeted banner advertisements, administrative notices, product offerings, and communications relevant to your use of the Websites. By accepting this Privacy Policy, you expressly agree to receive this information. If you do not wish to receive these communications, you may opt out of the receipt of certain communications. To learn how to do so, visit the Help section of the Websites or send us an email at  <a href="/Applications/ContactUs/ContactUs.aspx">Contact Us</a>. </p>

<p>4. <strong>Our Disclosure of Your Information</strong></p>

<p>Due to the regulatory environment in which we operate, we cannot ensure that all of your private communications and other personally identifiable information will never be disclosed in ways not otherwise described in this Privacy Policy. By way of example (without limiting any of the foregoing), we may be required to disclose information to the government, law enforcement agencies or third parties. Under certain circumstances, third parties may unlawfully intercept or access transmissions or private communications, or members may abuse or misuse your information that they collect from our Websites. Accordingly, although we use industry standard practices to protect your privacy, we do not promise, and you should not expect, that your personally identifiable information or private communications will always remain private. </p>

<p>As a matter of policy, we <strong>do not</strong> sell or rent any personally identifiable information about you to any third party. However, the following describes some of the ways that your personally identifiable information may be used or disclosed.</p>

<p><strong>Financial Information.</strong>   Under some circumstances we may require certain additional information, including but not limited to your credit card billing information. We use this financial information, including your name, address, and other information to bill you for use of our Services and products. By making a purchase, or engaging in any other kind of activity or transaction that uses financial information on the Websites, you consent to our providing of your financial information to our service providers and to such third parties as we determine necessary to support and process your activities and transactions, as well as to your credit card issuer for their purposes. These third parties may include the credit card companies and banking institutions used to process and support your transaction or activity. By purchasing, or registering for, or making reservations for products or services of third parties offered on the Websites, or by participating in programs offered on the Websites that are administered by third parties and that require you to submit financial information in order to use them, you also consent to our providing your financial information to those third parties. Any of these various third parties may be authorized to use your financial information in accordance with our contractual arrangements with such third parties and in accordance with their own privacy policies, over which we have no control, and you agree that we are not responsible or liable for any of their actions or omissions. Additionally, you agree that we may use and disclose all information submitted to such third parties in the same manner in which we are entitled to use and disclose any other information that you submit to us. </p>

<p><strong>Advertisers. </strong>We aggregate (gather up data across multiple members'' accounts) personally identifiable information and disclose such information in a non-personally identifiable manner to advertisers and other third parties for marketing and promotional purposes. However, in these situations, we do not disclose to these entities any information that could be used to identify you personally. Certain information, such as your name, email address, password, credit card number and bank account number, are never disclosed to marketing advertisers. We may use third-party advertising companies to serve ads on our behalf. These companies may employ cookies and action tags (also known as single pixel gifs or web beacons) to measure advertising effectiveness. </p>
<p><strong>Other Corporate Entities. </strong> We share much of our data, including personally identifiable information about you, with our parent and/or subsidiaries that are committed to serving your online needs and related services, throughout the world. To the extent that these entities have access to your information, they will treat it at least as protectively as they treat information they obtain from their other members. Our parent and/or subsidiaries will follow privacy practices no less protective of all members than our practices described in this document, to the extent allowed by applicable law. It is possible that Spark Networks and/or its subsidiaries, or any combination of such, could merge with or be acquired by another business entity. Should such a combination occur, you should expect that Spark Networks would share some or all of your information in order to continue to provide the Service. You will receive notice of such event (to the extent it occurs) and we will require that the new combined entity follow the practices disclosed in this Privacy Policy, as it may be amended from time to time. </p>
<p><b>Legal Requests. </b> Spark Networks cooperates with law enforcement inquiries, as well as other third parties to enforce laws, such as: intellectual property rights, fraud and other rights. We can (and you authorize us to) disclose any information about you to law enforcement and other government officials as we, in our sole discretion, believe necessary or appropriate, in connection with any investigation of fraud, intellectual property infringements, or other activity that is illegal or may expose us or you to legal liability. </p>

<p>5. <b>Your Use of other Members Information</b></p>

<p>Our services also include access to instant messaging and chat rooms. As a member you have access to members'' identification numbers and/or user names, and you might gain access to other contact information of other members through the regular use of the Services. By accepting this Privacy Policy, you agree that, with respect to other members'' personally identifiable information that you obtain through the Services, Spark Networks hereby grants to you a license to use such information only for: (a) Spark Networks related communications that are not unsolicited commercial messages, and (b) any other purpose to which such member expressly agrees after complete disclosure of the purpose. In all cases, you must give members an opportunity to remove themselves from your database. In addition, under no circumstances, except as defined in this Privacy Policy, may you disclose personally identifiable information about another member to any third party without our consent and the consent of such other member after adequate disclosure. Spark Networks and our members do not tolerate spam. Therefore, without limiting the foregoing, you are not licensed to add a Spark Networks member to your mail list (email or physical mail) without their express consent after adequate disclosure. To report spam from other Spark Networks members, please <a href="/Applications/ContactUs/ContactUs.aspx">Contact Us</a>. </p>

<p>6. <b>Accessing, Reviewing and Changing Your Profile</b></p>

<p>Following registration, you can review and change the information you submitted during registration. For instructions on how to do so, visit the Help section of the Websites. If you change your password and email address we will retain a record of your old password and email address. You can also change your registration information such as: name, address, city, state, zip code, country, phone number, profile, likes and dislikes, desired date profile, essays and saved search criteria.</p>

<p>Upon your notification, we will remove your membership from our active databases as soon as reasonably possible in accordance with our policy and applicable law. To learn how to hide or remove your profile so that others cannot view it, visit the Help section of the Websites.</p>



<p>We will retain in our files information you have requested be removed from our active databases for certain purposes, such as to resolve disputes, troubleshoot problems and enforce our terms and conditions. Further, such prior information may never be completely removed from our databases due to technical and legal constraints, including stored ‘back up'' systems. Therefore, you should not expect that all of your personally identifiable information will be completely removed from our databases in response to any request you may submit.</p>

<p>7. <b>Control of Your Password</b></p>

<p>You are responsible for all actions taken with your login information and password, including fees. Therefore we do not recommend that you disclose your Spark Networks password or login information to any third party. If you choose to share this information with any third party, you are responsible for all actions taken with your login information and password, and therefore should review each third party''s privacy policy. If you lose control of your password, you may lose substantial control over your personally identifiable information and may be subject to legally binding actions taken on your behalf. Therefore, if your password has been compromised in any way, you should immediately change your password.</p>

<p>8. <b>Other Information Collectors</b></p>

<p>Except as otherwise expressly described in this Privacy Policy, this document only addresses the use and disclosure of information we collect from you. To the extent that you disclose your information to other parties, whether they are on our Websites or on other sites throughout the Internet, different rules may apply to their use or disclosure of the information you disclose to them. To the extent that we use third party advertisers, they adhere to their own privacy policies. Since Spark Networks does not control the privacy policies of any third parties, you should investigate their policies before you disclose your personal information to them.</p>

<p>Mobile Alerts. If you opt to receive mobile alerts through the Website, any information that you provide to 4INFO will be subject to 4INFO''s Terms of Use and Privacy Policy which may be found at <a href="http://www.4info.net" target="_blank">www.4info.net</a>. We are not responsible for any information that you submit to 4INFO. The Company does not currently charge for this service, but standard or other charges may apply from your wireless carrier. Please check your plan for further details. To stop receiving text messages at anytime, text STOP to 44636. For help, text HELP to 44636 or email <a href="mailto:support@4info.net" target="_blank">support@4info.net</a>.</p>

<p>9. <b>Security</b></p>

  <p>Spark Networks uses industry standard practices, including "firewalls" and Secure Socket Layers, to safeguard the confidentiality of your personal identifiable information. Spark Networks treats data as an asset that must be protected against loss and unauthorized access. We employ many different security techniques to protect such data from unauthorized access by others inside and outside the company. However, "perfect security" does not exist on the Internet.</p>

<p>10. <b>Notice</b></p>

<p>We may change this Privacy Policy from time to time based on changes in the law, your comments, or our need to accurately reflect our data collection and disclosure practices. We will notify you about significant changes in the way we treat personal information by sending you a notice via email or by placing a prominent notice on our Website.</p>

<!-- end Privacy Statement-->


<br />
<br />
<br />
<br />
<br />
<br />
<p><a name="service"></a></p>
<a href="#privacy">Privacy Statement</a> | <a href="#service">Terms and Conditions of Service</a> | <a href="#purchase">Terms and Conditions of Purchase</a>

<div align="right"><a href="#top"><img src="((image:btn_top.gif))" width="16" height="15" border="0" /></a></div>

<!-- begin Terms and Conditions of Service -->
<h1><strong>Terms and Conditions of Service</strong></h1>


<p>This online personals service (the "Service") is a way for adults to meet each other. The Service is provided by Spark Networks&reg; Limited, whose principal office is located at 8383 Wilshire Blvd., Suite 800, Beverly Hills, California 90211 ("Spark Networks").</p>

<p>This legal agreement ("Agreement") is made between You and Us. This Agreement, as it may be amended from time to time, applies to all users of any Spark Networks site, including but not limited to: AmericanSingles.com, Relationships.com, JDate.com, and BlackSingles.com (the "Websites")</p>

<p>You may not use the Service if you are under the age of 18 or you are not able to form legally binding contracts, or if your membership has been suspended by Us. Please read this Agreement carefully before registering for the Service. By registering for the Service, You become a Spark Networks member (a "Member"), and You agree to be bound by the terms and conditions of this Agreement for as long as You continue to be a Member. IF YOU DO NOT AGREE WITH THE TERMS AND CONDITIONS OF THIS AGREEMENT, DO NOT REGISTER FOR THE SERVICE. THE TERMS AND CONDITIONS OF THIS AGREEMENT ARE SUBJECT TO CHANGE BY US AT ANY TIME, EFFECTIVE UPON NOTICE TO YOU, WHICH NOTICE SHALL BE DEEMED TO HAVE BEEN PROVIDED UPON OUR POSTING OF THE CURRENT VERSION OF THIS AGREEMENT ON THE SERVICE.</p>

<p>In this Agreement the following terms have the following meanings unless the context requires otherwise:</p>

<p><strong>"Agreement"</strong> means the agreement between You and Us incorporating these terms and conditions for the provision of the Service, as amended from time to time in the manner set forth herein;</p>

<p><strong>"We, Us, Our"</strong> means Spark Networks&reg; Limited; and </p>

<p><strong>"You, Your, Yourself"</strong> means the person who registers for the Service, accepts the terms and conditions of this Agreement and whose application for membership of the Service is accepted by Us.</p>
<p><strong>ELIGIBILITY: MINORS MAY NOT BECOME MEMBERS.</strong> By becoming a Member, You represent and warrant that You are at least 18 years old. By using the Service, You represent and warrant that You have the right, authority and capacity to enter into this Agreement and to abide by the terms and conditions of this Agreement. Your membership for the Service is for Your sole, personal use. You may not authorize others to use Your membership, and You may not assign or otherwise transfer Your account to any other person or entity.</p>
<p><strong>REGISTRATION AND SUBSCRIPTION:</strong> Although You may register as a Member of the Service for free, if You wish to use the Service to initiate most communication with other members and use certain other elements of the Service, You must become a Subscriber and pay the fees that are set out in  Our price list. This price list is part of this Agreement and We reserve the right, at any time, to change any fees or charges for using the Service. To become a Member, You must register for the Service. When and if You register to become a Member, You agree to provide accurate, current and complete information about Yourself as prompted by Our registration form ("Registration Data"), and to maintain and update Your information to keep it accurate, current and complete. You agree that We may rely on Your Registration Data as accurate, current and complete. You acknowledge that if Your Registration Data is untrue, inaccurate, not current or incomplete in any respect, We may terminate this Agreement and Your use of the Service and, in such event, You shall not be entitled to a refund of any unused subscription fees. Although we aim to verify the accuracy of the information provided by Our Members, We do not verify information in profiles, and We have no control over, do not guarantee, and are not responsible for the quality, truth, accuracy, legality or safety of Our Members.</p>
<p><strong>TERM AND TERMINATION: </strong>This Agreement will remain in full force and effect while You use the Service and/or are a Member. You may terminate your membership at any time via the Websites or by sending Us written or email notice of termination. To learn how to terminate your membership, visit the Help section of the Websites. Either You or We may terminate your membership by removing your profile, at any time, for any reason, with or without explanation, effective upon sending written or email notice to the other party. Upon such termination by Us without cause, We shall refund, pro rata, any unused portion of any subscription payments that We have received from You. In the event that (a) You terminate your subscription or membership or (b) We determine, in our sole discretion, that You have violated this Agreement or our posted <a href="#privacy">Privacy Statement</a>,
You shall not be entitled to, nor shall We be liable to You for, any refund of any unused portion of any subscription payments We have received from You, and We may continue to bar Your use of the Service in the future. Even after membership is terminated, this Agreement will remain in effect. </p>

<p><strong>PROPRIETARY RIGHTS: </strong>You represent and warrant to Us that the information posted in Your profile, including Your photograph, is posted by You and that You are the exclusive author of Your profile and the exclusive owner of Your photographs. You assign to Us, with full title guarantee, all copyright in Your profile, Your photographs posted, and any additional information sent to Us at any time in connection with Your use of the Service. You waive absolutely any and all moral rights to be identified as author of Your profile and owner of Your photograph and any similar rights in any jurisdiction in the world. In addition, other Members may post copyrighted information, which has copyright protection, whether or not it is identified as copyrighted. Except for that information which is in the public domain or for which You have been given express written permission, You will not copy, modify, publish, transmit, distribute, perform, display, or sell any such proprietary information. By posting information, photographs or content on any Service, You automatically grant, and You represent and warrant that You have the right to grant, to Us and other members, free of charge, an irrevocable, perpetual, non-exclusive, royalty-free, fully-paid, worldwide license to use, copy, perform, display, promote, publish and distribute such information, content and photographs and to prepare derivative works of, or incorporate into other works, such information and content, and to grant and authorize sub-licenses of the foregoing.</p>

<p><strong>YOUR USE OF THE SERVICE:</strong> As a Member, You agree that:</p>

<p>(1) You will use the Service in a manner consistent with any and all applicable laws and regulations. You will not include in Your profile any telephone numbers, street addresses, last names, URL''s or email addresses, other than in response to Our prompts in the personal or general information sections of the Websites. You will not engage in advertising to, or solicitation of, other members to buy or sell any products or services through the Service. You will not transmit any chain letters or junk email to other members. To protect Our members against such conduct, depending on the Website you Subscribe to, We currently limit the number of messages that any subscriber may send in a day. You are solely responsible for Your interactions with other members. We reserve the right, but have no obligation, to monitor and/or mediate disputes between You and other members.</p>

<p>(2) You are solely responsible for the content or information You publish or display (hereinafter, "post") on the Service, or transmit to other members. You will not post on the Service, or transmit to other members or to Us or Our employees, any defamatory, inaccurate, abusive, obscene, profane, offensive, sexually oriented, threatening, harassing, racially offensive, or illegal material, or any material that infringes or violates another party''s rights (including, but not limited to, intellectual property rights, and rights of privacy and publicity). You will not include in Your profile any offensive language, including but not limited to offensive anatomical or sexual references, or offensive sexually suggestive or connotative language, and You will not post any photos containing nudity or personal information. We reserve the right, but We have no obligation, to reject any profile or photo that does not comply with the prohibitions set forth in this section. We reserve the right to refuse service to anyone, at our sole discretion.</p>

<p>(3) By becoming a Member, You agree to accept and consent to receiving email communications initiated from Us or through Us including, without limitation: message notification emails, "Your Match" emails, emails informing you about events and parties We organize, emails informing You of changes to the Service and emails informing You of promotions that either We provide or that are being provided by third parties. If you choose to respond to promotions that are provided by third parties and in the course of doing so, disclose information to any external service providers, and/or grant them permission to collect information about you, then their use of your information is governed by their private policies. Message notification emails such as "Your Match" emails, emails informing you about events and parties We organize and emails informing You of changes to the Service are provided by Us as part of the operation of the Service and you will receive these messages for as long as you are Our member. Should You not wish to receive any of Our email communications, please do not register with Us for the Service. However, You may opt-out of receiving Email communications sent from Us or through Us offering You third party goods or services. To learn how to do so, visit the Help section of the Websites. </p>

<p>(4) You agree that We have no responsibility or liability for the deletion, corruption or failure to store any messages or other content maintained or transmitted by Our Service. You acknowledge that features, parameters or other services We provide may change at any time. You acknowledge that We reserve the right to sign out, terminate, delete or purge Your account from the Service if it is inactive. "Inactive" means that you have not signed in to the Service for a particular period of time, as determined by Us, in Our sole discretion.</p>

<p>(5) Our customer service employees are here to make your online experience enjoyable by providing assistance and guidance to You. When speaking to Our customer service employees on the telephone or communicating with them by any other means, You agree not to be abusive, obscene, profane, offensive, sexually oriented, threatening, harassing or racially offensive. Should any of Our customer service employees feel, at any point, threatened or offended by Your conduct, We reserve the right to immediately terminate Your membership and You shall not be entitled to the refund of any subscription payments We have received from You.</p>

<p>We are entitled to investigate and terminate Your membership if You have misused the Service, or behaved in a way which could be regarded as inappropriate, unlawful or illegal. The following is a partial, but not exhaustive, list of the types of actions that are illegal or prohibited under this Agreement:</p>

<p>You will not harass or impersonate any person or entity.  You will not use any manual or automatic device or process to retrieve, index, data mine, or, in any way reproduce or circumvent the navigational structure or presentation of the Service or its contents.  You will not express or imply that any of Your statements are endorsed by Us, without Our specific prior written consent.  You will not interfere with or disrupt any Service or any Website, servers or networks connected to any Service or Website.  You will not post, distribute or reproduce, in any way, any copyrighted material, trademarks, or other proprietary information without obtaining the prior written consent of the owner of such proprietary rights. You will not remove any copyright, trademark or other proprietary rights notices contained in the Service or forge headers or otherwise manipulate identifiers in order to disguise the origin of any information transmitted through the Service.  You will not use meta tags or code or other devices containing any reference to Us or the Service or the Website connected to the Service in order to direct any person to any other website for any purpose.   You will not modify, adapt, sublicense, translate, sell, reverse engineer, decipher, decompile or otherwise disassemble any portion of the Service or any software used on or for the Service or cause or enable others to do so.  You will not post, email or otherwise transmit any material that contains software viruses or any other computer code, files or programs designed to interrupt, harm or limit the functionality of any computer software or hardware.</p>

<p><strong>INDEMNITY BY MEMBER:</strong> You will defend, indemnify, and hold Us and Our officers, directors, employees, agents and third parties harmless, for any losses, costs, liabilities or expenses relating to or arising out of Your use of the Service, including:</p>

<p>(I) Your breach of this Agreement; (II) any allegation that any materials that You submit to Us or transmit to the Service infringe or otherwise violate the copyright, trademark, trade secret or other intellectual property or other rights of any third party; and/or (III) Your activities in connection with the Service. This indemnity shall be applicable without regard to the negligence of any party, including any indemnified person.</p>
<p><strong>RELEASE:</strong> If You have a dispute with one or more of Our Members, you release Us (and our officers, directors, agents, subsidiaries, joint ventures and employees) from any claims, demands and damages (actual and consequential) of every kind and nature, known and unknown, arising out of or in any way connected with such dispute.&nbsp;</p>
<p><strong>ONLINE CONTENT:</strong> Opinions, advice, statements, offers, or other information or content made available through the Service, but not directly by Us, are those of their respective authors, and should not necessarily be relied upon. Such authors are solely responsible for such content. WE DO NOT GUARANTEE THE ACCURACY, COMPLETENESS, OR USEFULNESS OF ANY INFORMATION ON THE SERVICE AND WE NEITHER ADOPT NOR ENDORSE, NOR ARE WE RESPONSIBLE FOR, THE ACCURACY OR RELIABILITY OF ANY OPINION, ADVICE, OR STATEMENT MADE BY ANY PARTY OTHER THAN US. UNDER NO CIRCUMSTANCES ARE WE RESPONSIBLE FOR ANY LOSS OR DAMAGE RESULTING FROM ANY PERSON''S RELIANCE ON INFORMATION OR OTHER CONTENT POSTED ON THE SERVICE OR TRANSMITTED TO MEMBERS.</p>

<p>WE RESERVE THE RIGHT, BUT WE HAVE NO OBLIGATION, TO MONITOR THE MATERIALS POSTED IN THE PUBLIC AREAS OF THE SERVICE. WE SHALL HAVE THE RIGHT TO REMOVE ANY SUCH MATERIAL THAT, IN OUR SOLE DISCRETION, VIOLATES, OR IS ALLEGED TO VIOLATE, THE LAW OR THIS AGREEMENT. NOTWITHSTANDING THIS RIGHT, YOU REMAIN SOLELY RESPONSIBLE FOR THE CONTENT OF THE MATERIALS YOU POST IN THE PUBLIC AREAS OF THE SERVICE AND IN YOUR PRIVATE EMAIL MESSAGES. EMAILS SENT BETWEEN YOU AND OTHER MEMBERS THAT ARE NOT READILY ACCESSIBLE TO THE GENERAL PUBLIC MAY BE REVIEWED BY US FOR COMPLIANCE WITH THIS AGREEMENT, BUT WILL BE TREATED BY US AS PRIVATE TO THE EXTENT REQUIRED BY APPLICABLE LAW.</p>

<p><strong>INTELLECTUAL PROPERTY. </strong>All intellectual property rights in and to the Service are and shall be owned by Us, absolutely. Those rights include, but are not limited to, database rights, copyright, design rights (whether registered or unregistered), patents, trademarks (whether registered or unregistered) and other similar rights, wherever existing in the world, together with the right to apply for protection of the same. All other trademarks, logos, service marks, company or product names set forth in the Service are the property of their respective owners. </p>

<p><strong>PRIVACY:</strong> The personal information (including sensitive personal information) You provide to Us will be stored on computers and/or servers. You consent to the use this information to create a profile of interests, preferences and browsing patterns and to allow You to participate in the Service. You also agree to read, review, comply with, uphold and maintain Our <a href="#privacy">Privacy Statement</a> and the terms and conditions thereof. If you are located outside of the United States, please note that the information that you provide is being sent to the United States. By becoming a Member of the Service, you consent to your data being sent to the United States and to such other third parties and jurisdictions as may be involved in the provision and operation of the Service.</p>

<p><strong>DISCLAIMERS:</strong> WE PROVIDE THE SERVICE ON AN "AS IS" BASIS AND GRANT NO WARRANTIES OF ANY KIND, EXPRESSED, IMPLIED OR STATUTORY, IN ANY COMMUNICATION WITH OUR REPRESENTATIVES, OR US OR OTHERWISE WITH RESPECT TO THE SERVICE. WE SPECIFICALLY DISCLAIM ANY IMPLIED WARRANTIES OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. FURTHER, WE DO NOT WARRANT THAT YOUR USE OF THE SERVICE WILL BE SECURE, UNINTERRUPTED, ALWAYS AVAILABLE OR ERROR-FREE, OR THAT THE SERVICE WILL MEET YOUR REQUIREMENTS OR THAT ANY DEFECTS IN THE SERVICE WILL BE CORRECTED. WE DISCLAIM LIABILITY FOR, AND NO WARRANTY IS MADE WITH RESPECT TO, CONNECTIVITY AND AVAILABILITY.</p>

<p>Although each member must agree to Our terms and conditions, We cannot guarantee that each member is at least the required minimum age, nor do we accept responsibility or liability for any content, communication or other use or access of the Service by persons under the age of 18 in violation of this Agreement. Also, it is possible that other members or users (including unauthorized users, or "hackers") may post or transmit offensive or obscene materials through the Service and that You may be involuntarily exposed to such offensive or obscene materials. It also is possible for others to obtain personal information about You due to Your use of the Service. Those others may use your information for purposes other than what You intended. We are not responsible for the use of any personal information that You disclose on the Service. Please carefully select the type of information that You post on the Service or release to others. WE DISCLAIM ALL LIABILITY, REGARDLESS OF THE FORM OF ACTION, FOR THE ACTS OR OMISSIONS OF OTHER MEMBERS OR USERS (INCLUDING UNAUTHORIZED USERS), WHETHER SUCH ACTS OR OMISSIONS OCCUR DURING THE USE OF THE SERVICE OR OTHERWISE.</p>

<p><strong>LIMITATION OF LIABILITY: </strong>IN NO EVENT WILL WE BE LIABLE TO YOU OR TO ANY OTHER PERSON FOR ANY INCIDENTAL, CONSEQUENTIAL, OR INDIRECT DAMAGES (INCLUDING, BUT NOT LIMITED TO, DAMAGES FOR LOSS OF DATA, LOSS OF PROGRAMS, COST OF PROCUREMENT OF SUBSTITUTE SERVICES OR SERVICE INTERRUPTIONS) ARISING OUT OF THE USE OF OR INABILITY TO USE THE SERVICE, EVEN IF WE OR OUR AGENTS OR REPRESENTATIVES KNOW OR HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES. NOTWITHSTANDING ANYTHING TO THE CONTRARY CONTAINED HEREIN, OUR LIABILITY TO YOU FOR ANY CAUSE WHATSOEVER, AND REGARDLESS OF THE FORM OF THE ACTION, WILL AT ALL TIMES BE LIMITED TO THE AMOUNT PAID, IF ANY, BY YOU TO US FOR THE SERVICE DURING THE TERM OF YOUR MEMBERSHIP.  WE DO NOT CONDUCT BACKGROUND CHECKS OR OTHERWISE SCREEN MEMBERS REGISTERING TO THE SERVICE IN ANY WAY. WE WILL NOT BE LIABLE FOR ANY DAMAGES, DIRECT, INDIRECT, INCIDENTAL AND/OR CONSEQUENTIAL, INCLUDING BY NOT LIMITED TO PHYSICAL DAMAGES, BODILY INJURY OR EMOTIONAL DISTRESS, ARISING OUT OF THE USE OF THIS SERVICE, INCLUDING, WITHOUT LIMITATION, DAMAGES ARISING OUT OF YOUR COMMUNICATIONS WITH AND/OR INTERACTIONS WITH ANY OTHER MEMBER OF THE SERVICE, OR ANY INDIVIDUAL YOU MEET VIA THE SERVICE. COMPLAINTS: To resolve a complaint regarding the Service, You should first contact Our customer service department by clicking: <a href="/Applications/ContactUs/ContactUs.aspx">contact us</a> or by calling ((CSSUPPORTPHONENUMBER)).</p>

<p><strong>DISPUTE RESOLUTION:</strong> This Agreement is governed by the laws of the State of California without regard to its conflict of law provisions. You agree to personal jurisdiction by and exclusive venue in the state and federal courts of the State of California, City of Los Angeles with regard to any and all claims by you arising out of or related to the Websites. This Agreement shall not be governed by the United Nations Convention on Contracts for the International Sale of Goods, the application of which is hereby expressly excluded. </p>
<p> <strong>WAIVER AND SEVERABILITY OF TERMS:</strong> Our failure to exercise or enforce any right or provision of this Agreement shall not constitute a waiver of such right or provision. If any provision of this Agreement is found by a court of competent jurisdiction to be void, invalid or unenforceable, the parties nevertheless agree that the court should endeavor to give effect to the parties'' intentions as reflected in the provision. In such case, the other provisions of this Agreement shall remain in full force and effect. </p>
<p> You certify that You have read and that You agree to be bound by the terms and conditions in this Agreement and our <a href="#privacy">Privacy Policy</a>.</p>
<!-- end Terms and Conditions of Service -->


<br />
<br />
<br />
<br />
<br />
<br />
<!-- begin Terms and Conditions of Service -->
<p><a name="purchase"></a></p>
<a href="#privacy">Privacy Statement</a> | <a href="#service">Terms and Conditions of Service</a> | <a href="#purchase">Terms and Conditions of Purchase</a>
<div align="right"><a href="#top"><img src="((image:btn_top.gif))" width="16" height="15" border="0"></a></div>
<h1><strong>Terms and Conditions of Purchase</strong></h1>

<p>All purchases are subject to the following terms and conditions:</p> <p>
The current Subscriber and renewal rates offered by ((PLDOMAIN)) are available on the <a href="/Applications/Subscription/Subscribe.aspx?prtid=262">subscription page.</a> Current Subscribers can view their subscription plan including any applicable renewal rate and period on the <a href="/Applications/Subscription/History.aspx">Account Information page.</a>
</p> 
<p><strong>Additional terms and conditions:</strong></p> 
<ol>
<li>All purchases are final. No refund will be given for unused portions of your subscription period.</li> 
<li>((PLDOMAIN)) guarantees that mail sent by Purchaser will be processed by the 
  ((PLDOMAIN)) system and sent to the recipient''s mailbox on the ((PLDOMAIN)) 
  system. Notwithstanding, ((PLDOMAIN)) does not guarantee a response from the 
  recipient nor does it make any other promises or warranties of any kind. </li>
<li>Purchaser hereby agrees that subscription privileges are non-refundable in the event that Purchaser chooses to suspend or cancel his/her membership. Furthermore, no refund will be made in the event of termination of Purchaser''s membership due to a violation of the <a href="#service">Terms and Conditions of Service</a> as outlined in there in. In that respect, Purchaser hereby agrees that offensive behavior towards other members or use of foul language in the chat room, emails to members or in Purchaser''s profile will constitute sufficient grounds for such termination of Purchaser''s membership by ((PLDOMAIN)).</li>
<li>All disputes resulting from the purchase tokens, other emailing privileges or any other purchase made within the ((PLDOMAIN)) website shall be brought to a binding arbitration in accordance with the rules of the Better Business Bureau and all such arbitration shall take place in Los Angeles, California.</li>
<li>By submitting this purchase, Purchaser hereby acknowledges, agrees and authorizes ((PLDOMAIN)) to renew Purchaser''s subscription, automatically, every month, at the guaranteed renewal rate that applies to the purchase option chosen by Purchaser, until such time as Purchaser instructs ((PLDOMAIN)) to stop the renewals. Renewals can be stopped by logging in as a member and clicking on "Member Services" or by going to:
  <a href="/Applications/Termination/TerminateReason.aspx">Cancel Subscription</a>.</li> 
<li>If you are awarded a "Gift Membership for a Friend" that membership may only be used by someone other than yourself. Gift Memberships expire one year after their grant date. All terms and conditions contained on the award or subscription page or in any applicable award email apply. Please contact Customer Service for further details. </li>
</ol></div>
<!-- end Terms and Conditions of Service -->

<br />
<br />
<br />
<br />
<br />
<br />
<a href="#privacy">Privacy Statement</a> | <a href="#service">Terms and Conditions of Service</a> | <a href="#purchase">Terms and Conditions of Purchase</a>
<div align="right"><a href="#top"><img src="((image:btn_top.gif))" width="16" height="15" border="0" /></a></div>',null,
'0',GETDATE(),'1','True',null)    

    

  INSERT INTO [mnContent].[dbo].[SiteArticle]
([SiteArticleID],[ArticleID],[SiteID],[MemberID],[Title],[Content],[FileID],
[Ordinal],[LastUpdated],[PublishedFlag],[FeaturedFlag],[PublishID])
 VALUES
 ('100019214','100001005','9051',0,N'Terms and Conditions',N'<div>

<h2>Terms and Conditions</h2>

<p>In these conditions the following words have the following meanings unless the context requires otherwise: "We, Us, Our" means Spark Networks&reg; Limited. "You, Your, Yourself" means the person whose application for membership on ((PLDOMAIN)) is accepted by Us.</p> 

<p>As part of the process of updating your credit card information, You will be required to submit a valid, current credit card. For authorization and verification purposes, $1.00 will be authorized on your card.  This $1.00 charge may appear as a pending authorization, but the charge will not be processed and it will not appear as a charge on your credit card statement. If the credit card that you provide cannot be authorized and verified, we will continue to bill the credit card previously on file on the next renewal date.</p>

<p>CONFIDENTIALITY: All information supplied by You to Us is subject to the privacy rules described on ((PLDOMAIN)).</p> 

<p>BILLING UPON EXPIRATION: If You do not cancel Your subscription, You will be automatically billed for a full monthly paying subscription to ((PLDOMAIN)) pursuant to the Terms and Conditions of Purchase (available at www.((PLDOMAIN))). You will be automatically renewed on a monthly basis thereafter until You terminate Your subscription in the manner described on the site.</p>

<p>TERMS OF MEMBERSHIP: Your Membership is subject to all the terms and conditions and privacy policy on ((PLDOMAIN)). Please review those terms and conditions by going to www.((PLDOMAIN)).</p>
 
</div>',null,
'0',GETDATE(),'1','False',null)    

    