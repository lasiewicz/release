  SET IDENTITY_INSERT CreditCardGroup ON 


    --cc types 'American Express'

  insert into [mnsystem].[dbo].[creditcardgroup]
  ([creditcardgroupid],[creditcardid],[groupid],[updatedate])
  values
  ('159','1','9051',getdate())
  

    --cc types 'Visa'

  insert into [mnsystem].[dbo].[creditcardgroup]
  ([creditcardgroupid],[creditcardid],[groupid],[updatedate])
  values
  ('160','64','9051',getdate())
  

    --cc types 'MasterCard'

  insert into [mnsystem].[dbo].[creditcardgroup]
  ([creditcardgroupid],[creditcardid],[groupid],[updatedate])
  values
  ('161','32','9051',getdate())
  

    --cc types 'Discover'

  insert into [mnsystem].[dbo].[creditcardgroup]
  ([creditcardgroupid],[creditcardid],[groupid],[updatedate])
  values
  ('162','8','9051',getdate())
  