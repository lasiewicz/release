use mnsystem
go
   declare @attributeid int
declare @attributeoptionid int

--timeliness
declare @attributeoptiongroupid int
select @attributeoptiongroupid =  MAX(AttributeOptionGroupID) + 1 from AttributeOptionGroup

select @attributeoptionid=AttributeOptionID from AttributeOption
where AttributeID=630 and ResourceKey='ATTOPT_630_USUALLY_EARLY'
insert into AttributeOptionGroup
(AttributeOptionGroupID, AttributeOptionID,GroupID,ListOrder)
values
(@attributeoptiongroupid,@attributeoptionid,21,15)

--fashion
select @attributeoptionid=AttributeOptionID from AttributeOption
where AttributeID=631 and ResourceKey='ATTOPT_631_VERY_TRENDY'

update AttributeOption
set attributevalue=16,
listorder=60
where attributeoptionid=@attributeoptionid 

--moviegenremask
   
 
 select @attributeid=attributeid from attribute
    where attributename= 'moviegenremask'
    
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_418_DONT_LIKE_MOVIES'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9161','21')
  
  
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_418_DOCUMENTARY'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9161','22')
  
  
  
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_418_SUSPENSE_THRILLER'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9161','23')
  
      select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_418_MYSTERY'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9161','24')
  
  
    select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_418_WESTERN'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9161','25')
  
          select @attributeoptionid=attributeoptionid from attributeoption
    where attributeid=@attributeid and resourcekey='ATTOPT_418_CARTOONS'
    select @attributeoptiongroupid=max(attributeoptiongroupid) + 1 from attributeoptiongroup
    
    insert into [mnsystem].[dbo].[attributeoptiongroup]
  ([attributeoptiongroupid],[attributeoptionid],[groupid],[listorder])
  values
  (@attributeoptiongroupid,@attributeoptionid,'9161','26')