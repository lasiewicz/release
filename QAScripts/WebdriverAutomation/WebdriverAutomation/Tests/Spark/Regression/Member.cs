﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.Events;
using NUnit.Framework;
using log4net;
using WebdriverAutomation.Tests.ReusableCode;

namespace WebdriverAutomation.Tests.Spark.Regression
{
    [TestFixture]
    public class Member : BaseTest
    {

        [Test]
        public void Validate_Registration()
        {
            try
            {
                log.Info("START TEST - Spark/Regression/Member/Registration");

                Setup();

                StartTest_UsingSpark_WithDummyPRM();

                RegisterNewUser();

                //Remove the user we just created
                WebdriverAutomation.Tests.ReusableCode.AccountSettings accountSettings = new AccountSettings();
                accountSettings.RemoveProfile_SkippingUpdateProfileAndImportantNoticePages(driver, log);

                TearDown();

                log.Info("END TEST - Spark/Regression/Member/Registration");
            }
            catch (Exception e)
            {
                CleanupFailedTestcase(e);
                throw;
            }
        }

        [Test]
        public void Validate_EditProfile_InMyOwnWordsTab()
        {
            try
            {
                log.Info("START TEST - Spark/Regression/Member/EditProfile_InMyOwnWordsTab");

                Setup();

                StartTest_UsingSpark();

                string login = System.Configuration.ConfigurationManager.AppSettings["Spark_RegisteredUser1_Email"];
                string password = System.Configuration.ConfigurationManager.AppSettings["Spark_RegisteredUser1_Password"];

                WebdriverAutomation.Tests.ReusableCode.LoggingInAndOut loggingInAndOut = new LoggingInAndOut();
                WebdriverAutomation.Framework.Site_Pages.Home home = loggingInAndOut.LogIn_ProfileComplete(driver, log, login, password);
                WebdriverAutomation.Framework.Site_Pages.YourProfile profile = home.Header.YourProfileHeaderLink_Click();

                string new_AboutMe = "This is a test account operated by an employee of Spark Networks. Testing changes.";
                string new_MyLifeAndAmbitions = "I always want the big piece of chicken";
                string new_ABriefHistoryOfMyLife = "Born in NYC. Raised in SF. Living in LA.";
                string new_OnOurFirstDateRemindMeToTellYouTheStoryAbout = "The time we parachuted into Burning Man";
                string new_TheThingsICouldNeverLiveWithout = "My phone and lasagna";
                string new_MyFavoriteBooksMoviesTVShowsMusicAndFood = "Some of that oontz oontz";
                string new_TheCoolestPlacesIveVisited = "Thom Yorke's house";
                string new_ForFunILikeTo = "Sleep all day";
                string new_OnFridayAndSaturdayNightsITypically = "Am watching a show";
                string new_ImLookingFor = "I am looking for someone who likes dance, music, the arts, travel and having a good time. Spontanaeity helps.";
                string new_YouShouldDefinitelyMessageMeIfYou = "Are not crazy";

                // Overwrite new values to the page
                int loopMax = 5;

                for (int i = 0; i < 5; i++)
                {
                    try
                    {
                        profile.AboutMe = new_AboutMe;
                        profile.MyLifeAndAmbitions = new_MyLifeAndAmbitions;
                        profile.ABriefHistoryOfMyLife = new_ABriefHistoryOfMyLife;
                        profile.OnOurFirstDateRemindMeToTellYouTheStoryAbout = new_OnOurFirstDateRemindMeToTellYouTheStoryAbout;
                        profile.TheThingsICouldNeverLiveWithout = new_TheThingsICouldNeverLiveWithout;
                        profile.MyFavoriteBooksMoviesTVShowsMusicAndFood = new_MyFavoriteBooksMoviesTVShowsMusicAndFood;
                        profile.TheCoolestPlacesIveVisited = new_TheCoolestPlacesIveVisited;
                        profile.ForFunILikeTo = new_ForFunILikeTo;
                        profile.OnFridayAndSaturdayNightsITypically = new_OnFridayAndSaturdayNightsITypically;
                        profile.ImLookingFor = new_ImLookingFor;
                        profile.YouShouldDefinitelyMessageMeIfYou = new_YouShouldDefinitelyMessageMeIfYou;

                        break;
                    }
                    catch
                    {
                        profile = profile.Header.YourProfileHeaderLink_Click();
                    }

                    if (i == loopMax - 1)
                        Assert.Fail("Errors are occurring while trying to hover over certain areas of the In My Own Words section of the Profile Page to change some values.  We failed after trying " + loopMax + " times unsuccessfully.");
                }

                // Go to the Home Page and then come back to the Profile Page to verify the values saved correctly
                home = profile.Header.HomeHeaderLink_Click();
                profile = home.Header.YourProfileHeaderLink_Click();

                string stringToVerifyOnProfilePage = string.Empty;

                stringToVerifyOnProfilePage = profile.AboutMe;
                Assert.IsTrue(new_AboutMe == stringToVerifyOnProfilePage, "In the Validate_EditProfile_InMyOwnWordsTab test case, after saving new values for 'About me' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_AboutMe + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.MyLifeAndAmbitions;
                Assert.IsTrue(new_MyLifeAndAmbitions == stringToVerifyOnProfilePage, "In the Validate_EditProfile_InMyOwnWordsTab test case, after saving new values for 'My life and ambitions' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_MyLifeAndAmbitions + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.ABriefHistoryOfMyLife;
                Assert.IsTrue(new_ABriefHistoryOfMyLife == stringToVerifyOnProfilePage, "In the Validate_EditProfile_InMyOwnWordsTab test case, after saving new values for 'A brief history of my life' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_ABriefHistoryOfMyLife + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.OnOurFirstDateRemindMeToTellYouTheStoryAbout;
                Assert.IsTrue(new_OnOurFirstDateRemindMeToTellYouTheStoryAbout == stringToVerifyOnProfilePage, "In the Validate_EditProfile_InMyOwnWordsTab test case, after saving new values for 'On our first date, remind me to tell you the story about...' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_OnOurFirstDateRemindMeToTellYouTheStoryAbout + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.TheThingsICouldNeverLiveWithout;
                Assert.IsTrue(new_TheThingsICouldNeverLiveWithout == stringToVerifyOnProfilePage, "In the Validate_EditProfile_InMyOwnWordsTab test case, after saving new values for 'The things I could never live without' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_TheThingsICouldNeverLiveWithout + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.MyFavoriteBooksMoviesTVShowsMusicAndFood;
                Assert.IsTrue(new_MyFavoriteBooksMoviesTVShowsMusicAndFood == stringToVerifyOnProfilePage, "In the Validate_EditProfile_InMyOwnWordsTab test case, after saving new values for 'My favorite books, movies, TV shows, music and food' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_MyFavoriteBooksMoviesTVShowsMusicAndFood + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.TheCoolestPlacesIveVisited;
                Assert.IsTrue(new_TheCoolestPlacesIveVisited == stringToVerifyOnProfilePage, "In the Validate_EditProfile_InMyOwnWordsTab test case, after saving new values for 'The coolest places I've visited' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_TheCoolestPlacesIveVisited + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.ForFunILikeTo;
                Assert.IsTrue(new_ForFunILikeTo == stringToVerifyOnProfilePage, "In the Validate_EditProfile_InMyOwnWordsTab test case, after saving new values for 'For fun, I like to...' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_ForFunILikeTo + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.OnFridayAndSaturdayNightsITypically;
                Assert.IsTrue(new_OnFridayAndSaturdayNightsITypically == stringToVerifyOnProfilePage, "In the Validate_EditProfile_InMyOwnWordsTab test case, after saving new values for 'On Friday and Saturday nights I typically...' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_OnFridayAndSaturdayNightsITypically + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.ImLookingFor;
                Assert.IsTrue(new_ImLookingFor == stringToVerifyOnProfilePage, "In the Validate_EditProfile_InMyOwnWordsTab test case, after saving new values for 'I'm looking for...' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_ImLookingFor + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.YouShouldDefinitelyMessageMeIfYou;
                Assert.IsTrue(new_YouShouldDefinitelyMessageMeIfYou == stringToVerifyOnProfilePage, "In the Validate_EditProfile_InMyOwnWordsTab test case, after saving new values for 'You should definitely message me if you...' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_YouShouldDefinitelyMessageMeIfYou + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                // reinitialize to original values
                for (int i = 0; i < 5; i++)
                {
                    try
                    {
                        profile.AboutMe = "This is a test account operated by an employee of Spark Networks.";
                        profile.MyLifeAndAmbitions = "I want to win the lottery";
                        profile.ABriefHistoryOfMyLife = "Sex drugs and rock and roll";
                        profile.OnOurFirstDateRemindMeToTellYouTheStoryAbout = "My trip to Ibiza";
                        profile.TheThingsICouldNeverLiveWithout = "The 16 switches in my car";
                        profile.MyFavoriteBooksMoviesTVShowsMusicAndFood = "Game of Thrones FTW";
                        profile.TheCoolestPlacesIveVisited = "Paris";
                        profile.ForFunILikeTo = "Drink hot sauce straight from the bottle";
                        profile.OnFridayAndSaturdayNightsITypically = "Am beached on my couch";
                        profile.ImLookingFor = "Someone outgoing who can do outdoor activities with me like camping and skydiving and fishing and stuff.";
                        profile.YouShouldDefinitelyMessageMeIfYou = "Like turtles.";

                        break;
                    }
                    catch
                    {
                        profile = profile.Header.YourProfileHeaderLink_Click();
                    }

                    if (i == loopMax - 1)
                        Assert.Fail("Errors are occurring while trying to hover over certain areas of the In My Own Words section of the Profile Page to change some values.  We failed after trying " + loopMax + " times unsuccessfully.");
                }

                profile.Header.Logout_Click();

                TearDown();

                log.Info("END TEST - Spark/Regression/Member/EditProfile_InMyOwnWordsTab");
            }
            catch (Exception e)
            {
                CleanupFailedTestcase(e);
                throw;
            }
        }

        [Test]
        public void Validate_EditProfile_MoreAboutMeTab()
        {
            try
            {
                log.Info("START TEST - Spark/Regression/Member/EditProfile_MoreAboutMeTab");

                Setup();

                StartTest_UsingSpark();

                string login = System.Configuration.ConfigurationManager.AppSettings["Spark_RegisteredUser1_Email"];
                string password = System.Configuration.ConfigurationManager.AppSettings["Spark_RegisteredUser1_Password"];

                WebdriverAutomation.Tests.ReusableCode.LoggingInAndOut loggingInAndOut = new LoggingInAndOut();
                WebdriverAutomation.Framework.Site_Pages.Home home = loggingInAndOut.LogIn_ProfileComplete(driver, log, login, password);
                WebdriverAutomation.Framework.Site_Pages.YourProfile profile = home.Header.YourProfileHeaderLink_Click();

                string new_MyPerfectFirstDate = "My perfect first date is with someone who really likes automation. Someone I can talk about code with.";
                string new_MyIdealRelationship = "My ideal relationship would be with someone who likes to rock climb. Because I like to rock climb too.";
                string new_MyPastRelationships = "I would love to talk about my past but it shouldn't be done here. Ask me in person and I will tell you.";

                List<string> new_MyPersonalityIsBestDescribedAs = new List<string>();
                new_MyPersonalityIsBestDescribedAs.Add("Artistic");
                new_MyPersonalityIsBestDescribedAs.Add("Talkative");

                List<string> new_InMyFreeTimeIEnjoy = new List<string>();
                new_InMyFreeTimeIEnjoy.Add("Dining Out");
                new_InMyFreeTimeIEnjoy.Add("Shopping");

                List<string> new_InMyFreeTimeILike = new List<string>();
                new_InMyFreeTimeILike.Add("Concerts");
                new_InMyFreeTimeILike.Add("Restaurants");

                List<string> new_MyFavoritePhysicalActivites = new List<string>();
                new_MyFavoritePhysicalActivites.Add("Aerobics");
                new_MyFavoritePhysicalActivites.Add("Martial Arts");

                List<string> new_MyFavoriteFoods = new List<string>();
                new_MyFavoriteFoods.Add("Continental");
                new_MyFavoriteFoods.Add("Seafood");

                List<string> new_MyFavoriteMusic = new List<string>();
                new_MyFavoriteMusic.Add("Modern Rock n' Roll");
                new_MyFavoriteMusic.Add("Punk");

                List<string> new_ILikeToRead = new List<string>();
                new_ILikeToRead.Add("Fiction");
                new_ILikeToRead.Add("Poetry");

                profile.MoreAboutMeTab_Click();

                // Overwrite new values to the page
                int loopMax = 5;

                for (int i = 0; i < 5; i++)
                {
                    try
                    {
                        profile.MyPerfectFirstDate = new_MyPerfectFirstDate;
                        profile.MyIdealRelationship = new_MyIdealRelationship;
                        profile.MyPastRelationships = new_MyPastRelationships;
                        profile.MyPersonalityIsBestDescribedAs = new_MyPersonalityIsBestDescribedAs;
                        profile.InMyFreeTimeIEnjoy_JDatePage = new_InMyFreeTimeIEnjoy;
                        profile.InMyFreeTimeILike = new_InMyFreeTimeILike;
                        profile.MyFavoritePhysicalActivities_JDatePage = new_MyFavoritePhysicalActivites;
                        profile.MyFavoriteFoods_JDatePage = new_MyFavoriteFoods;
                        profile.MyFavoriteMusic_JDatePage = new_MyFavoriteMusic;
                        profile.ILikeToRead = new_ILikeToRead;

                        break;
                    }
                    catch
                    {
                        profile = profile.Header.YourProfileHeaderLink_Click();

                        profile.MoreAboutMeTab_Click();
                    }

                    if (i == loopMax - 1)
                        Assert.Fail("Errors are occurring while trying to hover over certain areas of the In My Own Words section of the Profile Page to change some values.  We failed after trying " + loopMax + " times unsuccessfully.");
                }

                // Go to the Home Page and then come back to the Profile Page to verify the values saved correctly
                home = profile.Header.HomeHeaderLink_Click();
                profile = home.Header.YourProfileHeaderLink_Click();

                profile.MoreAboutMeTab_Click();

                string stringToVerifyOnProfilePage = string.Empty;
                List<string> listToVerifyOnProfilePage = new List<string>();

                stringToVerifyOnProfilePage = profile.MyPerfectFirstDate;
                Assert.IsTrue(new_MyPerfectFirstDate == stringToVerifyOnProfilePage, "In the Validate_EditProfile_MoreAboutMeTab test case, after saving new values for 'My perfect first date' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_MyPerfectFirstDate + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.MyIdealRelationship;
                Assert.IsTrue(new_MyIdealRelationship == stringToVerifyOnProfilePage, "In the Validate_EditProfile_MoreAboutMeTab test case, after saving new values for 'My ideal relationship' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_MyIdealRelationship + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.MyPastRelationships;
                Assert.IsTrue(new_MyPastRelationships == stringToVerifyOnProfilePage, "In the Validate_EditProfile_MoreAboutMeTab test case, after saving new values for 'My past relationships' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_MyPastRelationships + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                listToVerifyOnProfilePage = profile.MyPersonalityIsBestDescribedAs;
                new_MyPersonalityIsBestDescribedAs.Sort();
                Assert.IsTrue(AreValuesAndCountEqual(new_MyPersonalityIsBestDescribedAs, listToVerifyOnProfilePage), "In the Validate_EditProfile_MoreAboutMeTab test case, after saving new values for 'My personality is best described as...' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + string.Join(", ", new_MyPersonalityIsBestDescribedAs.ToArray()) + "' \r\nValue currently in Profile Page: '" + string.Join(", ", listToVerifyOnProfilePage.ToArray()) + "'");

                listToVerifyOnProfilePage = profile.InMyFreeTimeIEnjoy_JDatePage;
                new_InMyFreeTimeIEnjoy.Sort();
                Assert.IsTrue(AreValuesAndCountEqual(new_InMyFreeTimeIEnjoy, listToVerifyOnProfilePage), "In the Validate_EditProfile_MoreAboutMeTab test case, after saving new values for 'In my spare time, I enjoy...' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + string.Join(", ", new_InMyFreeTimeIEnjoy.ToArray()) + "' \r\nValue currently in Profile Page: '" + string.Join(", ", listToVerifyOnProfilePage.ToArray()) + "'");

                listToVerifyOnProfilePage = profile.InMyFreeTimeILike;
                new_InMyFreeTimeILike.Sort();
                Assert.IsTrue(AreValuesAndCountEqual(new_InMyFreeTimeILike, listToVerifyOnProfilePage), "In the Validate_EditProfile_MoreAboutMeTab test case, after saving new values for 'In my free time, I like...' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + string.Join(", ", new_InMyFreeTimeILike.ToArray()) + "' \r\nValue currently in Profile Page: '" + string.Join(", ", listToVerifyOnProfilePage.ToArray()) + "'");

                listToVerifyOnProfilePage = profile.MyFavoritePhysicalActivities_JDatePage;
                new_MyFavoritePhysicalActivites.Sort();
                Assert.IsTrue(AreValuesAndCountEqual(new_MyFavoritePhysicalActivites, listToVerifyOnProfilePage), "In the Validate_EditProfile_MoreAboutMeTab test case, after saving new values for 'My favorite physical activities' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + string.Join(", ", new_MyFavoritePhysicalActivites.ToArray()) + "' \r\nValue currently in Profile Page: '" + string.Join(", ", listToVerifyOnProfilePage.ToArray()) + "'");

                listToVerifyOnProfilePage = profile.MyFavoriteFoods_JDatePage;
                new_MyFavoriteFoods.Sort();
                Assert.IsTrue(AreValuesAndCountEqual(new_MyFavoriteFoods, listToVerifyOnProfilePage), "In the Validate_EditProfile_MoreAboutMeTab test case, after saving new values for 'My favorite food(s)' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + string.Join(", ", new_MyFavoriteFoods.ToArray()) + "' \r\nValue currently in Profile Page: '" + string.Join(", ", listToVerifyOnProfilePage.ToArray()) + "'");

                listToVerifyOnProfilePage = profile.MyFavoriteMusic_JDatePage;
                new_MyFavoriteMusic.Sort();
                Assert.IsTrue(AreValuesAndCountEqual(new_MyFavoriteMusic, listToVerifyOnProfilePage), "In the Validate_EditProfile_MoreAboutMeTab test case, after saving new values for 'My favorite music' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + string.Join(", ", new_MyFavoriteMusic.ToArray()) + "' \r\nValue currently in Profile Page: '" + string.Join(", ", listToVerifyOnProfilePage.ToArray()) + "'");

                listToVerifyOnProfilePage = profile.ILikeToRead;
                new_ILikeToRead.Sort();
                Assert.IsTrue(AreValuesAndCountEqual(new_ILikeToRead, listToVerifyOnProfilePage), "In the Validate_EditProfile_MoreAboutMeTab test case, after saving new values for 'I like to read' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + string.Join(", ", new_ILikeToRead.ToArray()) + "' \r\nValue currently in Profile Page: '" + string.Join(", ", listToVerifyOnProfilePage.ToArray()) + "'");

                // reinitialize to original values              
                for (int i = 0; i < 5; i++)
                {
                    try
                    {
                        profile.MyPerfectFirstDate = "My perfect first date is going to dinner then a club for drinks and dancing. Lots of drinks. Don't judge me.";
                        profile.MyIdealRelationship = "My ideal relationship would be with someone who likes to party. Someone who can hang all night long.";
                        profile.MyPastRelationships = "I have had tons of past relationships. I don't like talking about my past on here though. More in person.";

                        List<string> orig_MyPersonalityIsBestDescribedAs = new List<string>();
                        orig_MyPersonalityIsBestDescribedAs.Add("Eccentric");
                        orig_MyPersonalityIsBestDescribedAs.Add("Outgoing");

                        profile.MyPersonalityIsBestDescribedAs = orig_MyPersonalityIsBestDescribedAs;

                        List<string> orig_InMyFreeTimeIEnjoy = new List<string>();
                        orig_InMyFreeTimeIEnjoy.Add("Investing");
                        orig_InMyFreeTimeIEnjoy.Add("Wine Tasting");

                        profile.InMyFreeTimeIEnjoy_JDatePage = orig_InMyFreeTimeIEnjoy;

                        List<string> orig_InMyFreeTimeILike = new List<string>();
                        orig_InMyFreeTimeILike.Add("Art Galleries");
                        orig_InMyFreeTimeILike.Add("Raves/Underground Parties");

                        profile.InMyFreeTimeILike = orig_InMyFreeTimeILike;

                        List<string> orig_MyFavoritePhysicalActivites = new List<string>();
                        orig_MyFavoritePhysicalActivites.Add("Cricket");
                        orig_MyFavoritePhysicalActivites.Add("Soccer");

                        profile.MyFavoritePhysicalActivities_JDatePage = orig_MyFavoritePhysicalActivites;

                        List<string> orig_MyFavoriteFoods = new List<string>();
                        orig_MyFavoriteFoods.Add("Italian");
                        orig_MyFavoriteFoods.Add("Soul Food");

                        profile.MyFavoriteFoods_JDatePage = orig_MyFavoriteFoods;

                        List<string> orig_MyFavoriteMusic = new List<string>();
                        orig_MyFavoriteMusic.Add("Dance/Electronica");
                        orig_MyFavoriteMusic.Add("Indie");

                        profile.MyFavoriteMusic_JDatePage = orig_MyFavoriteMusic;

                        List<string> orig_ILikeToRead = new List<string>();
                        orig_ILikeToRead.Add("Non-Fiction");
                        orig_ILikeToRead.Add("Comics");

                        profile.ILikeToRead = orig_ILikeToRead;

                        break;
                    }
                    catch
                    {
                        profile = profile.Header.YourProfileHeaderLink_Click();

                        profile.MoreAboutMeTab_Click();
                    }

                    if (i == loopMax - 1)
                        Assert.Fail("Errors are occurring while trying to hover over certain areas of the In My Own Words section of the Profile Page to change some values.  We failed after trying " + loopMax + " times unsuccessfully.");
                }

                profile.Header.Logout_Click();

                TearDown();

                log.Info("END TEST - Spark/Regression/Member/EditProfile_MoreAboutMeTab");
            }
            catch (Exception e)
            {
                CleanupFailedTestcase(e);
                throw;
            }
        }

        [Test]
        public void Validate_EditProfile_DetailsPhysicalInfo()
        {
            try
            {
                log.Info("START TEST - Spark/Regression/Member/EditProfile_DetailsPhysicalInfo");

                Setup();

                StartTest_UsingSpark();

                string login = System.Configuration.ConfigurationManager.AppSettings["Spark_RegisteredUser1_Email"];
                string password = System.Configuration.ConfigurationManager.AppSettings["Spark_RegisteredUser1_Password"];

                WebdriverAutomation.Tests.ReusableCode.LoggingInAndOut loggingInAndOut = new LoggingInAndOut();
                WebdriverAutomation.Framework.Site_Pages.Home home = loggingInAndOut.LogIn_ProfileComplete(driver, log, login, password);
                WebdriverAutomation.Framework.Site_Pages.YourProfile profile = home.Header.YourProfileHeaderLink_Click();

                string new_height = "4'6\"(137 cm)";
                string new_bodyType = "Soft";
                string new_hairColor = "Red";
                string new_eyeColor = "Green";

                // Overwrite new values to the page
                profile.PhysicalInfo_ClickToEdit();

                profile.Height_InOverlay = new_height;
                profile.BodyType_InOverlay = new_bodyType;
                profile.HairColor_InOverlay = new_hairColor;
                profile.EyeColor_InOverlay = new_eyeColor;

                profile.PhysicalInfo_SaveChanges();

                // Go to the Home Page and then come back to the Profile Page to verify the values saved correctly
                home = profile.Header.HomeHeaderLink_Click();
                profile = home.Header.YourProfileHeaderLink_Click();

                string stringToVerifyOnProfilePage = string.Empty;

                stringToVerifyOnProfilePage = profile.Height;
                Assert.IsTrue(new_height.Replace(" ", "") == stringToVerifyOnProfilePage.Replace(" ", ""), "In the Validate_EditProfile_DetailsPhysicalInfo test case, after saving new values for 'Height' in the the Click to Edit overlay the values were not saved properly in the Profile Page \r\nValue we saved in Click to Edit overlay: '" + new_height + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.BodyType;
                Assert.IsTrue(new_bodyType == stringToVerifyOnProfilePage, "In the Validate_EditProfile_DetailsPhysicalInfo test case, after saving new values for 'Body Type' in the the Click to Edit overlay the values were not saved properly in the Profile Page \r\nValue we saved in Click to Edit overlay: '" + new_bodyType + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.HairColor;
                Assert.IsTrue(new_hairColor == stringToVerifyOnProfilePage, "In the Validate_EditProfile_DetailsPhysicalInfo test case, after saving new values for 'Hair color' in the the Click to Edit overlay the values were not saved properly in the Profile Page \r\nValue we saved in Click to Edit overlay: '" + new_hairColor + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.EyeColor;
                Assert.IsTrue(new_eyeColor == stringToVerifyOnProfilePage, "In the Validate_EditProfile_DetailsPhysicalInfo test case, after saving new values for 'Eye color' in the the Click to Edit overlay the values were not saved properly in the Profile Page \r\nValue we saved in Click to Edit overlay: '" + new_eyeColor + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.Height_Dealbreakers;
                Assert.IsTrue(new_height.Replace(" ", "") == stringToVerifyOnProfilePage.Replace(" ", ""), "In the Validate_EditProfile_Dealbreakers test case, after saving new values for 'Height' the values were not saved properly in the Profile Page in the Dealbreakers section \r\nValue we saved in Edit Profile: '" + new_height + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                // reinitialize to original values
                profile.PhysicalInfo_ClickToEdit();

                profile.Height_InOverlay = "5'5\"(165 cm)";
                profile.BodyType_InOverlay = "Petite";
                profile.HairColor_InOverlay = "Black";
                profile.EyeColor_InOverlay = "Brown";

                profile.PhysicalInfo_SaveChanges();

                profile.Header.Logout_Click();

                TearDown();

                log.Info("END TEST - Spark/Regression/Member/EditProfile_DetailsPhysicalInfo");
            }
            catch (Exception e)
            {
                CleanupFailedTestcase(e);
                throw;
            }
        }

        [Test]
        public void Validate_EditProfile_DetailsLifestyle()
        {
            try
            {
                log.Info("START TEST - Spark/Regression/Member/EditProfile_DetailsLifestyle");

                Setup();

                StartTest_UsingSpark();

                string login = System.Configuration.ConfigurationManager.AppSettings["Spark_RegisteredUser1_Email"];
                string password = System.Configuration.ConfigurationManager.AppSettings["Spark_RegisteredUser1_Password"];

                WebdriverAutomation.Tests.ReusableCode.LoggingInAndOut loggingInAndOut = new LoggingInAndOut();
                WebdriverAutomation.Framework.Site_Pages.Home home = loggingInAndOut.LogIn_ProfileComplete(driver, log, login, password);
                WebdriverAutomation.Framework.Site_Pages.YourProfile profile = home.Header.YourProfileHeaderLink_Click();

                string new_MaritalStatus = "Widowed";
                string new_Relocation = "Yes";
                string new_HaveKids = "3 or More";
                string new_Custody = "Some live with me";
                string new_WantKids = "No";

                List<string> new_Pets = new List<string>();
                new_Pets.Add("Bird");
                new_Pets.Add("Rabbit");

                string new_Smoke = "Non-Smoker";
                string new_Drink = "Never";
                string new_ActivityLevel = "Never Active";

                // Overwrite new values to the page
                profile.Lifestyle_ClickToEdit();

                profile.MaritalStatus_InOverlay = new_MaritalStatus;
                profile.Relocation_InOverlay = new_Relocation;
                profile.HaveKids_InOverlay = new_HaveKids;
                profile.Custody_InOverlay = new_Custody;
                profile.WantKids_InOverlay = new_WantKids;
                profile.Pets_InOverlay = new_Pets;
                profile.Smoking_InOverlay = new_Smoke;
                profile.Drinking_InOverlay = new_Drink;
                profile.ActivityLevel_InOverlay = new_ActivityLevel;

                profile.Lifestyle_SaveChanges();

                // Go to the Home Page and then come back to the Profile Page to verify the values saved correctly
                home = profile.Header.HomeHeaderLink_Click();
                profile = home.Header.YourProfileHeaderLink_Click();

                string stringToVerifyOnProfilePage = string.Empty;
                List<string> listToVerifyOnProfilePage = new List<string>();

                stringToVerifyOnProfilePage = profile.MaritalStatus;
                Assert.IsTrue(new_MaritalStatus == stringToVerifyOnProfilePage, "In the Validate_EditProfile_DetailsLifestyle test case, after saving new values for 'Marital Status' in the the Click to Edit overlay the values were not saved properly in the Profile Page \r\nValue we saved in Click to Edit overlay: '" + new_MaritalStatus + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.Relocation;
                Assert.IsTrue(new_Relocation == stringToVerifyOnProfilePage, "In the Validate_EditProfile_DetailsLifestyle test case, after saving new values for 'Relocation' in the the Click to Edit overlay the values were not saved properly in the Profile Page \r\nValue we saved in Click to Edit overlay: '" + new_Relocation + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.HaveKids;
                Assert.IsTrue(new_HaveKids == stringToVerifyOnProfilePage, "In the Validate_EditProfile_DetailsLifestyle test case, after saving new values for 'Have kids' in the the Click to Edit overlay the values were not saved properly in the Profile Page \r\nValue we saved in Click to Edit overlay: '" + new_HaveKids + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.Custody;
                Assert.IsTrue(new_Custody == stringToVerifyOnProfilePage, "In the Validate_EditProfile_DetailsLifestyle test case, after saving new values for 'Custody' in the the Click to Edit overlay the values were not saved properly in the Profile Page \r\nValue we saved in Click to Edit overlay: '" + new_Custody + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.WantKids;
                Assert.IsTrue(new_WantKids == stringToVerifyOnProfilePage, "In the Validate_EditProfile_DetailsLifestyle test case, after saving new values for 'Wants kids' in the the Click to Edit overlay the values were not saved properly in the Profile Page \r\nValue we saved in Click to Edit overlay: '" + new_WantKids + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                listToVerifyOnProfilePage = profile.Pets;
                new_Pets.Sort();
                Assert.IsTrue(AreValuesAndCountEqual(new_Pets, listToVerifyOnProfilePage), "In the Validate_EditProfile_MoreAboutMeTab test case, after saving new values for 'Pets' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + string.Join(", ", new_Pets.ToArray()) + "' \r\nValue currently in Profile Page: '" + string.Join(", ", listToVerifyOnProfilePage.ToArray()) + "'");

                stringToVerifyOnProfilePage = profile.Smoking;
                Assert.IsTrue(new_Smoke == stringToVerifyOnProfilePage, "In the Validate_EditProfile_DetailsLifestyle test case, after saving new values for 'Smoking' in the the Click to Edit overlay the values were not saved properly in the Profile Page \r\nValue we saved in Click to Edit overlay: '" + new_Smoke + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.Drinking;
                Assert.IsTrue(new_Drink == stringToVerifyOnProfilePage, "In the Validate_EditProfile_DetailsLifestyle test case, after saving new values for 'Drinking' in the the Click to Edit overlay the values were not saved properly in the Profile Page \r\nValue we saved in Click to Edit overlay: '" + new_MaritalStatus + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.HaveKids_Dealbreakers;
                Assert.IsTrue(new_HaveKids == stringToVerifyOnProfilePage, "In the Validate_EditProfile_Dealbreakers test case, after saving new values for 'Have kids' the values were not saved properly in the Profile Page in the Dealbreakers section \r\nValue we saved in Edit Profile: '" + new_HaveKids + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.WantsKids_Dealbreakers;
                Assert.IsTrue(new_WantKids == stringToVerifyOnProfilePage, "In the Validate_EditProfile_Dealbreakers test case, after saving new values for 'Wants kids' the values were not saved properly in the Profile Page in the Dealbreakers section \r\nValue we saved in Edit Profile: '" + new_WantKids + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.Smoking_Dealbreakers;
                Assert.IsTrue(new_Smoke == stringToVerifyOnProfilePage, "In the Validate_EditProfile_Dealbreakers test case, after saving new values for 'Smoking' the values were not saved properly in the Profile Page in the Dealbreakers section \r\nValue we saved in Edit Profile: '" + new_Smoke + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.Drinking_Dealbreakers;
                Assert.IsTrue(new_Drink == stringToVerifyOnProfilePage, "In the Validate_EditProfile_Dealbreakers test case, after saving new values for 'Drinking' the values were not saved properly in the Profile Page in the Dealbreakers section \r\nValue we saved in Edit Profile: '" + new_Drink + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                // reinitialize to original values
                List<string> orig_Pets = new List<string>();
                orig_Pets.Add("Reptile");
                orig_Pets.Add("Hamster");

                profile.Lifestyle_ClickToEdit();

                profile.MaritalStatus_InOverlay = "Single";
                profile.Relocation_InOverlay = "No";
                profile.HaveKids_InOverlay = "None";
                profile.Custody_InOverlay = "I have no kids";
                profile.WantKids_InOverlay = "Not sure";
                profile.Pets_InOverlay = orig_Pets;
                profile.Smoking_InOverlay = "Smokes Regularly";
                profile.Drinking_InOverlay = "Frequently";
                profile.ActivityLevel_InOverlay = "Very Active";

                profile.Lifestyle_SaveChanges();

                profile.Header.Logout_Click();

                TearDown();

                log.Info("END TEST - Spark/Regression/Member/EditProfile_DetailsLifestyle");
            }
            catch (Exception e)
            {
                CleanupFailedTestcase(e);
                throw;
            }
        }

        [Test]
        public void Validate_EditProfile_DetailsBackground()
        {
            try
            {
                log.Info("START TEST - Spark/Regression/Member/EditProfile_DetailsBackground");

                Setup();

                StartTest_UsingSpark();

                string login = System.Configuration.ConfigurationManager.AppSettings["Spark_RegisteredUser1_Email"];
                string password = System.Configuration.ConfigurationManager.AppSettings["Spark_RegisteredUser1_Password"];

                WebdriverAutomation.Tests.ReusableCode.LoggingInAndOut loggingInAndOut = new LoggingInAndOut();
                WebdriverAutomation.Framework.Site_Pages.Home home = loggingInAndOut.LogIn_ProfileComplete(driver, log, login, password);
                WebdriverAutomation.Framework.Site_Pages.YourProfile profile = home.Header.YourProfileHeaderLink_Click();

                string new_GrewUpIn = "The old country";
                string new_MyEthnicityIs = "Hispanic";

                List<string> new_Languages = new List<string>();
                new_Languages.Add("Czech");
                new_Languages.Add("Tagalog");

                string new_Religion = "Hindu";
                string new_Education = "Elementary";
                string new_AreaOfStudy = "Math";
                string new_Occuaption = "Retired";
                string new_WhatIDo = "Student";
                string new_AnnualIncome = "Under $15,000";
                string new_Politics = "Left Wing";

                // Overwrite new values to the page
                profile.Background_ClickToEdit();

                profile.GrewUpIn_InOverlay = new_GrewUpIn;
                profile.Ethnicity_InOverlay = new_MyEthnicityIs;
                profile.Languages_InOverlay = new_Languages;
                profile.Religion_InOverlay = new_Religion;
                profile.Education_InOverlay = new_Education;
                profile.AreaOfStudy_InOverlay = new_AreaOfStudy;
                profile.Occupation_InOverlay = new_Occuaption;
                profile.WhatIDo_InOverlay = new_WhatIDo;
                profile.AnnualIncome_InOverlay = new_AnnualIncome;
                profile.Politics_InOverlay = new_Politics;

                profile.Background_SaveChanges();

                // Go to the Home Page and then come back to the Profile Page to verify the values saved correctly
                home = profile.Header.HomeHeaderLink_Click();
                profile = home.Header.YourProfileHeaderLink_Click();

                string stringToVerifyOnProfilePage = string.Empty;
                List<string> listToVerifyOnProfilePage = new List<string>();

                stringToVerifyOnProfilePage = profile.GrewUpIn;
                Assert.IsTrue(new_GrewUpIn == stringToVerifyOnProfilePage, "In the Validate_EditProfile_DetailsBackground test case, after saving new values for 'Grew up in' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_GrewUpIn + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.Ethnicity;
                Assert.IsTrue(new_MyEthnicityIs == stringToVerifyOnProfilePage, "In the Validate_EditProfile_DetailsBackground test case, after saving new values for 'Ethnicity' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_MyEthnicityIs + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                listToVerifyOnProfilePage = profile.Languages;
                new_Languages.Sort();
                Assert.IsTrue(AreValuesAndCountEqual(new_Languages, listToVerifyOnProfilePage), "In the Validate_EditProfile_MoreAboutMeTab test case, after saving new values for 'Languages' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + string.Join(", ", new_Languages.ToArray()) + "' \r\nValue currently in Profile Page: '" + string.Join(", ", listToVerifyOnProfilePage.ToArray()) + "'");

                stringToVerifyOnProfilePage = profile.Religion;
                Assert.IsTrue(new_Religion == stringToVerifyOnProfilePage, "In the Validate_EditProfile_DetailsBackground test case, after saving new values for 'Religion' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_Religion + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.Education;
                Assert.IsTrue(new_Education == stringToVerifyOnProfilePage, "In the Validate_EditProfile_DetailsBackground test case, after saving new values for 'Education' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_Education + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.AreaOfStudy;
                Assert.IsTrue(new_AreaOfStudy == stringToVerifyOnProfilePage, "In the Validate_EditProfile_DetailsBackground test case, after saving new values for 'Area of study' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_AreaOfStudy + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.Occupation;
                Assert.IsTrue(new_Occuaption == stringToVerifyOnProfilePage, "In the Validate_EditProfile_DetailsBackground test case, after saving new values for 'Occupation' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_Occuaption + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.WhatIDo;
                Assert.IsTrue(new_WhatIDo == stringToVerifyOnProfilePage, "In the Validate_EditProfile_DetailsBackground test case, after saving new values for 'What I do' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_WhatIDo + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.AnnualIncome;
                Assert.IsTrue(new_AnnualIncome == stringToVerifyOnProfilePage, "In the Validate_EditProfile_DetailsBackground test case, after saving new values for 'Annual income' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_AnnualIncome + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.Politics;
                Assert.IsTrue(new_Politics == stringToVerifyOnProfilePage, "In the Validate_EditProfile_DetailsBackground test case, after saving new values for 'Politics' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_Politics + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.Ethnicity_Dealbreakers;
                Assert.IsTrue(new_MyEthnicityIs == stringToVerifyOnProfilePage, "In the Validate_EditProfile_DetailsBackground test case, after saving new values for 'Ethnicity' the values were not saved properly in the Profile Page in the Dealbreakers section \r\nValue we saved in Edit Profile: '" + new_MyEthnicityIs + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.Religion_Dealbreakers;
                Assert.IsTrue(new_Religion == stringToVerifyOnProfilePage, "In the Validate_EditProfile_DetailsBackground test case, after saving new values for 'Religion' the values were not saved properly in the Profile Page in the Dealbreakers section \r\nValue we saved in Edit Profile: '" + new_Religion + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.Education_Dealbreakers;
                Assert.IsTrue(new_Education == stringToVerifyOnProfilePage, "In the Validate_EditProfile_DetailsBackground test case, after saving new values for 'Education' the values were not saved properly in the Profile Page in the Dealbreakers section \r\nValue we saved in Edit Profile: '" + new_Education + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.Occupation_Dealbreakers;
                Assert.IsTrue(new_Occuaption == stringToVerifyOnProfilePage, "In the Validate_EditProfile_DetailsBackground test case, after saving new values for 'Occupation' the values were not saved properly in the Profile Page in the Dealbreakers section \r\nValue we saved in Edit Profile: '" + new_Occuaption + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.AnnualIncome_Dealbreakers;
                Assert.IsTrue(new_AnnualIncome == stringToVerifyOnProfilePage, "In the Validate_EditProfile_DetailsBackground test case, after saving new values for 'Annual Income the values were not saved properly in the Profile Page in the Dealbreakers section \r\nValue we saved in Edit Profile: '" + new_AnnualIncome + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                // reinitialize to original values
                List<string> orig_Languages = new List<string>();
                orig_Languages.Add("Chinese");
                orig_Languages.Add("Russian");

                profile.Background_ClickToEdit();

                profile.GrewUpIn_InOverlay = "The City";
                profile.Ethnicity_InOverlay = "Asian";
                profile.Languages_InOverlay = orig_Languages;
                profile.Religion_InOverlay = "Atheist";
                profile.Education_InOverlay = "Master's Degree";
                profile.AreaOfStudy_InOverlay = "Video games";
                profile.Occupation_InOverlay = "Other";
                profile.WhatIDo_InOverlay = "Teacher";
                profile.AnnualIncome_InOverlay = "Average";
                profile.Politics_InOverlay = "Liberal";

                profile.Background_SaveChanges();

                profile.Header.Logout_Click();

                TearDown();

                log.Info("END TEST - Spark/Regression/Member/EditProfile_DetailsBackground");
            }
            catch (Exception e)
            {
                CleanupFailedTestcase(e);
                throw;
            }
        }

        /// <summary>
        /// We do not test the Username field
        /// </summary>
        [Test]
        public void Validate_EditProfile_YourBasics()
        {
            try
            {
                log.Info("START TEST - Spark/Regression/Member/EditProfile_YourBasics");
                Setup();

                StartTest_UsingSpark();

                string login = System.Configuration.ConfigurationManager.AppSettings["Spark_RegisteredUser1_Email"];
                string password = System.Configuration.ConfigurationManager.AppSettings["Spark_RegisteredUser1_Password"];

                WebdriverAutomation.Tests.ReusableCode.LoggingInAndOut loggingInAndOut = new LoggingInAndOut();
                WebdriverAutomation.Framework.Site_Pages.Home home = loggingInAndOut.LogIn_ProfileComplete(driver, log, login, password);
                WebdriverAutomation.Framework.Site_Pages.YourProfile profile = home.Header.YourProfileHeaderLink_Click();

                string new_FirstName = "Mark";
                string new_LastName = "Azuras";
                string new_DateOfBirthMonth = "Sep";
                string new_DateOfBirthDay = "15";
                string new_DateOfBirthYear = "1980";
                string new_Country = "Canada";
                string new_Zipcode = "K8N5W6";
                string new_ImA = "woman seeking men";
                string new_RelationshipStatus = "Divorced";

                // Overwrite new values to the page
                profile.YourBasics_ClickToEdit();

                profile.FirstName_InOverlay = new_FirstName;
                profile.LastName_InOverlay = new_LastName;
                profile.YourDateOfBirthMonth_InOverlay = new_DateOfBirthMonth;
                profile.YourDateOfBirthDay_InOverlay = new_DateOfBirthDay;
                profile.YourDateOfBirthYear_InOverlay = new_DateOfBirthYear;
                profile.Country_InOverlay = new_Country;
                profile.ZipCode_InOverlay = new_Zipcode;
                profile.ImA_InOverlay = new_ImA;
                profile.RelationshipStatus_InOverlay = new_RelationshipStatus;

                profile.YourBasics_SaveChanges();

                // Go to the Home Page and then come back to the Profile Page to verify the values saved correctly
                home = profile.Header.HomeHeaderLink_Click();
                profile = home.Header.YourProfileHeaderLink_Click();

                string stringToVerifyOnProfilePage = string.Empty;

                stringToVerifyOnProfilePage = profile.RelationshipStatus;
                Assert.IsTrue(new_RelationshipStatus == stringToVerifyOnProfilePage, "In the Validate_EditProfile_YourBasics test case, after saving new values for 'Relationship status' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_RelationshipStatus + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.ImA;
                // stupid small discrepancy where the text is slightly different on the Profile Page than on the Edit Profile overlay
                if (new_ImA == "woman seeking men")
                    new_ImA = "Woman seeking Man";

                Assert.IsTrue(new_ImA == stringToVerifyOnProfilePage, "In the Validate_EditProfile_YourBasics test case, after saving new values for 'I'm a' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_ImA + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                int ageOfUser = AgeOfUser(new_DateOfBirthMonth, new_DateOfBirthDay, new_DateOfBirthYear);
                stringToVerifyOnProfilePage = profile.Age;
                Assert.IsTrue(Convert.ToInt32(stringToVerifyOnProfilePage) == ageOfUser, "In the Validate_EditProfile_YourBasics test case, after saving new values for 'Age' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + ageOfUser + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.Country;
                Assert.IsTrue(new_Country == stringToVerifyOnProfilePage, "In the Validate_EditProfile_YourBasics test case, after saving new values for 'Country' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_Country + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                // reinitialize to original values
                profile.YourBasics_ClickToEdit();

                profile.FirstName_InOverlay = "Theo";
                profile.LastName_InOverlay = "Wang";
                profile.YourDateOfBirthMonth_InOverlay = "Dec";
                profile.YourDateOfBirthDay_InOverlay = "16";
                profile.YourDateOfBirthYear_InOverlay = "1979";
                profile.Country_InOverlay = "USA";
                profile.ZipCode_InOverlay = "90210";
                profile.ImA_InOverlay = "man seeking women";
                profile.RelationshipStatus_InOverlay = "Single";

                profile.YourBasics_SaveChanges();

                profile.Header.Logout_Click();

                TearDown();

                log.Info("END TEST - Spark/Regression/Member/EditProfile_YourBasics");
            }
            catch (Exception e)
            {
                CleanupFailedTestcase(e);
                throw;
            }
        }

        [Test]
        public void Validate_EditProfile_Dealbreakers()
        {
            try
            {
                log.Info("START TEST - Spark/Regression/Member/EditProfile_Dealbreakers");
                Setup();

                StartTest_UsingSpark();

                string login = System.Configuration.ConfigurationManager.AppSettings["Spark_RegisteredUser1_Email"];
                string password = System.Configuration.ConfigurationManager.AppSettings["Spark_RegisteredUser1_Password"];

                WebdriverAutomation.Tests.ReusableCode.LoggingInAndOut loggingInAndOut = new LoggingInAndOut();
                WebdriverAutomation.Framework.Site_Pages.Home home = loggingInAndOut.LogIn_ProfileComplete(driver, log, login, password);
                WebdriverAutomation.Framework.Site_Pages.YourProfile profile = home.Header.YourProfileHeaderLink_Click();

                string new_HaveKids = "3 or More";
                string new_WantsKids = "No";
                string new_height = "4'6\"(137 cm)";
                string new_Ethnicity = "Hispanic";
                string new_Religion = "Agnostic";
                string new_Smoking = "Non-Smoker";
                string new_Drinking = "Never";
                string new_Education = "Elementary";
                string new_Occupation = "Retired";
                string new_AnnualIncome = "Average";

                // Overwrite new values to the page
                profile.Dealbreakers_ClickToEdit();

                profile.HaveKids_InDealbreakersOverlay = new_HaveKids;
                profile.WantsKids_InDealbreakersOverlay = new_WantsKids;
                profile.Height_InDealbreakersOverlay = new_height;
                profile.Ethnicity_InDealbreakersOverlay = new_Ethnicity;
                profile.Religion_InDealbreakersOverlay = new_Religion;
                profile.Smoking_InDealbreakersOverlay = new_Smoking;
                profile.Drinking_InDealbreakersOverlay = new_Drinking;
                profile.Education_InDealbreakersOverlay = new_Education;
                profile.Occupation_InDealbreakersOverlay = new_Occupation;
                profile.AnnualIncome_InDealbreakersOverlay = new_AnnualIncome;

                profile.Dealbreakers_SaveChanges();

                // Go to the Home Page and then come back to the Profile Page to verify the values saved correctly
                home = profile.Header.HomeHeaderLink_Click();
                profile = home.Header.YourProfileHeaderLink_Click();

                string stringToVerifyOnProfilePage = string.Empty;

                stringToVerifyOnProfilePage = profile.HaveKids_Dealbreakers;
                Assert.IsTrue(new_HaveKids == stringToVerifyOnProfilePage, "In the Validate_EditProfile_Dealbreakers test case, after saving new values for 'Have kids' the values were not saved properly in the Profile Page in the Dealbreakers section \r\nValue we saved in Edit Profile: '" + new_HaveKids + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.WantsKids_Dealbreakers;
                Assert.IsTrue(new_WantsKids == stringToVerifyOnProfilePage, "In the Validate_EditProfile_Dealbreakers test case, after saving new values for 'Wants kids' the values were not saved properly in the Profile Page in the Dealbreakers section \r\nValue we saved in Edit Profile: '" + new_WantsKids + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.Height_Dealbreakers;
                Assert.IsTrue(new_height.Replace(" ", "") == stringToVerifyOnProfilePage.Replace(" ", ""), "In the Validate_EditProfile_Dealbreakers test case, after saving new values for 'Height' the values were not saved properly in the Profile Page in the Dealbreakers section \r\nValue we saved in Edit Profile: '" + new_height + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.Ethnicity_Dealbreakers;
                Assert.IsTrue(new_Ethnicity == stringToVerifyOnProfilePage, "In the Validate_EditProfile_Dealbreakers test case, after saving new values for 'Ethnicity' the values were not saved properly in the Profile Page in the Dealbreakers section \r\nValue we saved in Edit Profile: '" + new_Ethnicity + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.Religion_Dealbreakers;
                Assert.IsTrue(new_Religion == stringToVerifyOnProfilePage, "In the Validate_EditProfile_Dealbreakers test case, after saving new values for 'Religion' the values were not saved properly in the Profile Page in the Dealbreakers section \r\nValue we saved in Edit Profile: '" + new_Religion + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.Smoking_Dealbreakers;
                Assert.IsTrue(new_Smoking == stringToVerifyOnProfilePage, "In the Validate_EditProfile_Dealbreakers test case, after saving new values for 'Smoking' the values were not saved properly in the Profile Page in the Dealbreakers section \r\nValue we saved in Edit Profile: '" + new_Smoking + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.Drinking_Dealbreakers;
                Assert.IsTrue(new_Drinking == stringToVerifyOnProfilePage, "In the Validate_EditProfile_Dealbreakers test case, after saving new values for 'Drinking' the values were not saved properly in the Profile Page in the Dealbreakers section \r\nValue we saved in Edit Profile: '" + new_Drinking + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.Education_Dealbreakers;
                Assert.IsTrue(new_Education == stringToVerifyOnProfilePage, "In the Validate_EditProfile_Dealbreakers test case, after saving new values for 'Education' the values were not saved properly in the Profile Page in the Dealbreakers section \r\nValue we saved in Edit Profile: '" + new_Education + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.Occupation_Dealbreakers;
                Assert.IsTrue(new_Occupation == stringToVerifyOnProfilePage, "In the Validate_EditProfile_Dealbreakers test case, after saving new values for 'Occupation' the values were not saved properly in the Profile Page in the Dealbreakers section \r\nValue we saved in Edit Profile: '" + new_Occupation + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.AnnualIncome_Dealbreakers;
                Assert.IsTrue(new_AnnualIncome == stringToVerifyOnProfilePage, "In the Validate_EditProfile_Dealbreakers test case, after saving new values for 'Annual $' the values were not saved properly in the Profile Page in the Dealbreakers section \r\nValue we saved in Edit Profile: '" + new_AnnualIncome + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.HaveKids;
                Assert.IsTrue(new_HaveKids == stringToVerifyOnProfilePage, "In the Validate_EditProfile_Dealbreakers test case, after saving new values for 'Have kids' the values were not saved properly in the Profile Page in the Details section \r\nValue we saved in Edit Profile: '" + new_HaveKids + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.WantKids;
                Assert.IsTrue(new_WantsKids == stringToVerifyOnProfilePage, "In the Validate_EditProfile_Dealbreakers test case, after saving new values for 'Wants kids' the values were not saved properly in the Profile Page in the Details section \r\nValue we saved in Edit Profile: '" + new_WantsKids + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.Height;
                Assert.IsTrue(new_height.Replace(" ", "") == stringToVerifyOnProfilePage.Replace(" ", ""), "In the Validate_EditProfile_Dealbreakers test case, after saving new values for 'Height' the values were not saved properly in the Profile Page in the Details section \r\nValue we saved in Edit Profile: '" + new_height + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.Ethnicity;
                Assert.IsTrue(new_Ethnicity == stringToVerifyOnProfilePage, "In the Validate_EditProfile_Dealbreakers test case, after saving new values for 'Ethnicity' the values were not saved properly in the Profile Page in the Details section \r\nValue we saved in Edit Profile: '" + new_Ethnicity + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.Religion;
                Assert.IsTrue(new_Religion == stringToVerifyOnProfilePage, "In the Validate_EditProfile_Dealbreakers test case, after saving new values for 'Religion' the values were not saved properly in the Profile Page in the Details section \r\nValue we saved in Edit Profile: '" + new_Religion + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.Smoking;
                Assert.IsTrue(new_Smoking == stringToVerifyOnProfilePage, "In the Validate_EditProfile_Dealbreakers test case, after saving new values for 'Smoking' the values were not saved properly in the Profile Page in the Details section \r\nValue we saved in Edit Profile: '" + new_Smoking + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.Drinking;
                Assert.IsTrue(new_Drinking == stringToVerifyOnProfilePage, "In the Validate_EditProfile_Dealbreakers test case, after saving new values for 'Drinking' the values were not saved properly in the Profile Page in the Details section \r\nValue we saved in Edit Profile: '" + new_Drinking + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.Education;
                Assert.IsTrue(new_Education == stringToVerifyOnProfilePage, "In the Validate_EditProfile_Dealbreakers test case, after saving new values for 'Education' the values were not saved properly in the Profile Page in the Details section \r\nValue we saved in Edit Profile: '" + new_Education + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.Occupation;
                Assert.IsTrue(new_Occupation == stringToVerifyOnProfilePage, "In the Validate_EditProfile_Dealbreakers test case, after saving new values for 'Occupation' the values were not saved properly in the Profile Page in the Details section \r\nValue we saved in Edit Profile: '" + new_Occupation + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.AnnualIncome;
                Assert.IsTrue(new_AnnualIncome == stringToVerifyOnProfilePage, "In the Validate_EditProfile_Dealbreakers test case, after saving new values for 'Annual $' the values were not saved properly in the Profile Page in the Details section \r\nValue we saved in Edit Profile: '" + new_Occupation + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                // reinitialize to original values
                profile.Dealbreakers_ClickToEdit();

                profile.HaveKids_InDealbreakersOverlay = "None";
                profile.WantsKids_InDealbreakersOverlay = "Yes";
                profile.Height_InDealbreakersOverlay = "5'5\"(165 cm)";
                profile.Ethnicity_InDealbreakersOverlay = "Asian";
                profile.Religion_InDealbreakersOverlay = "Atheist";
                profile.Smoking_InDealbreakersOverlay = "Occasional Smoker";
                profile.Drinking_InDealbreakersOverlay = "Frequently";
                profile.Education_InDealbreakersOverlay = "High School";
                profile.Occupation_InDealbreakersOverlay = "Student";
                profile.AnnualIncome_InDealbreakersOverlay = "Will tell you later";

                profile.Dealbreakers_SaveChanges();

                profile.Header.Logout_Click();

                TearDown();

                log.Info("END TEST - Spark/Regression/Member/EditProfile_Dealbreakers");
            }
            catch (Exception e)
            {
                CleanupFailedTestcase(e);
                throw;
            }
        }

        /// <summary>
        /// In order to verify pagination we need to ensure that our search results have enough results to span more than one page (12 results).
        /// Using a search of Located Within 160 miles of 90210 (Beverly Hills) for this test case.
        /// 
        /// Note:  This test case is unchanged from the JDate test case.
        /// </summary>
        [Test]
        public void Validate_QuickSearchPagination()
        {
            try
            {
                log.Info("START TEST - Spark/Regression/Member/QuickSearchPagination");

                Setup();

                StartTest_UsingSpark();

                string login = System.Configuration.ConfigurationManager.AppSettings["Spark_RegisteredUser1_Email"];
                string password = System.Configuration.ConfigurationManager.AppSettings["Spark_RegisteredUser1_Password"];

                WebdriverAutomation.Tests.ReusableCode.LoggingInAndOut loggingInAndOut = new LoggingInAndOut();
                WebdriverAutomation.Framework.Site_Pages.Home home = loggingInAndOut.LogIn_ProfileComplete(driver, log, login, password);
                Framework.Site_Pages.QuickSearch quickSearch = home.Header.SearchHeaderLink_Click();

                string youreA = "Man";
                string seekingA = "Woman";
                string ageFrom = "20";
                string ageTo = "30";
                string locatedWithin = "200 Miles";
                string country = "USA";
                string zipCode = "90210";

                quickSearch.YoureADropdown = youreA;
                quickSearch.SeekingADropdown = seekingA;
                quickSearch.AgeFromTextbox = ageFrom;
                quickSearch.AgeToTextbox = ageTo;
                quickSearch.LocatedWithinDropdown = locatedWithin;
                quickSearch.EditLink_Click();
                quickSearch.EditOverlay_ZipCodeTab_Click();
                quickSearch.EditOverlay_ZipCodeTab_CountryDropdown = country;
                quickSearch.EditOverlay_ZipCodeTab_ZipCodeTextbox = zipCode;
                quickSearch.EditOverlay_SaveButton_Click();
                quickSearch.SearchButton_Click();

                quickSearch.GalleryLink_Click();

                // save all the usernames from our search in a list
                List<string> userNames1Through12 = quickSearch.GetAllNamesOfSearchResultsOnThisPage();
                Assert.IsFalse(userNames1Through12.Count < 12, "ERROR - QUICK SEARCH PAGE - We cannot test pagination because we are not getting enough results back in our quicksearch.  \r\nWe are currently searching for a " + seekingA + " between the ages of " + ageFrom + " to " + ageTo + " within " + locatedWithin + " miles of " + zipCode + ".\r\nThe number of search results returned:  " + userNames1Through12.Count);

                // go to the next page - users 13-24 and save those names in another list
                quickSearch = quickSearch.Pagination_13To24_Click();
                List<string> userNames13Through24 = quickSearch.GetAllNamesOfSearchResultsOnThisPage();

                // verify that none of the user names match - we truly have reached a new page
                bool doAnyNamesFromPage1MatchPage2 = quickSearch.DoAnyUserNamesMatchInOurLists(userNames1Through12, userNames13Through24);
                Assert.IsFalse(doAnyNamesFromPage1MatchPage2, "ERROR - QUICK SEARCH PAGE - After paging through to the 2nd page of results (13-24) a user from page 1 was found in page 2");

                // verify Previous link
                quickSearch = quickSearch.Pagination_PreviousLink_Click();
                List<string> userNamesOnCurrentPage = quickSearch.GetAllNamesOfSearchResultsOnThisPage();

                bool doALLNamesFromPage1MatchPage2 = quickSearch.DoAllUserNamesMatchInOurLists(userNames1Through12, userNamesOnCurrentPage);
                Assert.IsTrue(doALLNamesFromPage1MatchPage2, "ERROR - QUICK SEARCH PAGE - After navigating to page 2 (users 13-24) and clicking the Previous link the usernames we saved from page 1 do not match");

                // verify Next link
                quickSearch = quickSearch.Pagination_NextLink_Click();
                userNamesOnCurrentPage = quickSearch.GetAllNamesOfSearchResultsOnThisPage();

                doALLNamesFromPage1MatchPage2 = quickSearch.DoAllUserNamesMatchInOurLists(userNames13Through24, userNamesOnCurrentPage);
                Assert.IsTrue(doALLNamesFromPage1MatchPage2, "ERROR - QUICK SEARCH PAGE - After navigating to page 1 (users 1-12) and clicking the Next link the usernames we saved from page 2 do not match");

                // verify |< link
                quickSearch = quickSearch.Pagination_25To36_Click();
                quickSearch = quickSearch.Pagination_FirstPageLink_Click();
                userNamesOnCurrentPage = quickSearch.GetAllNamesOfSearchResultsOnThisPage();

                doALLNamesFromPage1MatchPage2 = quickSearch.DoAllUserNamesMatchInOurLists(userNames1Through12, userNamesOnCurrentPage);
                Assert.IsTrue(doALLNamesFromPage1MatchPage2, "ERROR - QUICK SEARCH PAGE - After navigating to page 3 (users 25-36) and clicking the |< link the usernames we saved from page 1 do not match");

                TearDown();

                log.Info("END TEST - Spark/Regression/Member/QuickSearchPagination");
            }
            catch (Exception e)
            {
                CleanupFailedTestcase(e);
                throw;
            }
        }

        /// <summary>
        /// Note:  This test case is unchanged from the JDate test case.
        /// </summary>
        [Test]
        public void Validate_QuickSearchResults()
        {
            try
            {
                log.Info("START TEST - Spark/Regression/Member/QuickSearchResults");

                Setup();

                StartTest_UsingSpark();

                string login = System.Configuration.ConfigurationManager.AppSettings["Spark_RegisteredUser1_Email"];
                string password = System.Configuration.ConfigurationManager.AppSettings["Spark_RegisteredUser1_Password"];

                WebdriverAutomation.Tests.ReusableCode.LoggingInAndOut loggingInAndOut = new LoggingInAndOut();
                WebdriverAutomation.Framework.Site_Pages.Home home = loggingInAndOut.LogIn_ProfileComplete(driver, log, login, password);
                Framework.Site_Pages.QuickSearch quickSearch = home.Header.SearchHeaderLink_Click();

                string youreA = "Man";
                string seekingA = "Woman";
                string ageFrom = "18";
                string ageTo = "50";
                string locatedWithin = "5 Miles";
                string country = "USA";
                string zipCode = "94133";

                // add all cities located within 5 miles of 94133 (San Francisco)
                List<string> cities = new List<string>();
                cities.Add("Alameda");
                cities.Add("Albany");
                cities.Add("Berkeley");
                cities.Add("Broadmoor Vlg");
                cities.Add("Daly City");
                cities.Add("El Cerrito");
                cities.Add("Emeryville");
                cities.Add("Mill Valley");
                cities.Add("Oakland");
                cities.Add("Piedmont");
                cities.Add("Pt Richmond");
                cities.Add("Richmond");
                cities.Add("S San Fran");
                cities.Add("San Francisco");
                cities.Add("Sausalito");
                cities.Add("South San Francisco");
                cities.Add("Tiburon");
                cities.Add("UC Berkeley");

                quickSearch.YoureADropdown = youreA;
                quickSearch.SeekingADropdown = seekingA;
                quickSearch.AgeFromTextbox = ageFrom;
                quickSearch.AgeToTextbox = ageTo;
                quickSearch.LocatedWithinDropdown = locatedWithin;
                quickSearch.EditLink_Click();
                quickSearch.EditOverlay_ZipCodeTab_Click();
                quickSearch.EditOverlay_ZipCodeTab_CountryDropdown = country;
                quickSearch.EditOverlay_ZipCodeTab_ZipCodeTextbox = zipCode;
                quickSearch.EditOverlay_SaveButton_Click();
                quickSearch.SearchButton_Click();

                quickSearch.GalleryLink_Click();

                // GALLERY VIEW - verify that quick search results appear
                Assert.True(quickSearch.SearchResultsCount_GalleryView() > 0, "ERROR - QUICK SEARCH PAGE - After doing a quick search in Gallery View, no results appeared.  \r\nThe search was for " + youreA + " seeking a " + seekingA + " searching within " + locatedWithin + " of " + zipCode);

                // GALLERY VIEW - verify that the results are all within the correct age range
                Assert.True(quickSearch.SearchResultsWithinAgeRange_GalleryView(Convert.ToInt32(ageFrom), Convert.ToInt32(ageTo)), "ERROR - QUICK SEARCH PAGE - After doing a quick search in Gallery View, the ages of the search results were not in the correct range.  \r\nThe search was for ages " + ageFrom + " to " + ageTo);

                // GALLERY VIEW - verify that the results are all within the correct zip code (checking for 5 miles within 94133 - San Francisco)
                string listOfCities = cities[0];

                for (int i = 1; i < cities.Count; i++)
                    listOfCities = listOfCities + ", " + cities[i];

                Assert.True(quickSearch.SearchResultsWithinExpectedCities_GalleryView(cities), "ERROR - QUICK SEARCH PAGE - After doing a quick search in Gallery View, the location range of the search result was not correct.  \r\nThe search was for " + locatedWithin + " within " + zipCode + " and the search results should have all returned one of the following cities: " + listOfCities);

                quickSearch.ListLink_Click();

                // LIST VIEW - verify that quick search results appear
                Assert.True(quickSearch.SearchResultsCount_ListView() > 0, "ERROR - QUICK SEARCH PAGE - After doing a quick search in List View, no results appeared.  \r\nThe search was for " + youreA + " seeking a " + seekingA + " searching within " + locatedWithin + " of " + zipCode);

                // LIST VIEW - verify that the results are all within the correct age range
                Assert.True(quickSearch.SearchResultsWithinAgeRange_ListView(Convert.ToInt32(ageFrom), Convert.ToInt32(ageTo)), "ERROR - QUICK SEARCH PAGE - After doing a quick search in List View, the ages of the search results were not in the correct range.");

                // LIST VIEW - verify that the results are all within the correct zip code (checking for 5 miles within 94133 - San Francisco)
                Assert.True(quickSearch.SearchResultsWithinExpectedCities_ListView(cities), "ERROR - QUICK SEARCH PAGE - After doing a quick search in List View, the location range of the search result was not correct.  \r\nThe search was for " + locatedWithin + " within " + zipCode + " and the search results should have all returned one of the following cities: " + cities[0] + ", " + cities[1] + ", " + cities[2] + ", " + cities[3]);

                // LIST VIEW - verify that the results show the correct gender seeking the correct gender
                Assert.True(quickSearch.SearchResultsIAmASeekingA_ListView(youreA, seekingA), "ERROR - QUICK SEARCH PAGE - After doing a quick search in List View, the gender preference was not correct.  \r\nThe search should have returned matches for " + seekingA + " seeking a " + youreA);

                TearDown();

                log.Info("END TEST - Spark/Regression/Member/QuickSearchResults");
            }
            catch (Exception e)
            {
                CleanupFailedTestcase(e);
                throw;
            }
        }

        ///// <summary>
        ///// Setup:  The user that we login with for this test case should have exactly 1 photo currently uploaded. Else this test case will delete all other existing photos
        ///// before starting.
        ///// </summary>
        //[Test]
        //public void Validate_UploadAndEditPhotos()
        //{
        //    try
        //    {
        //        log.Info("START TEST - Spark/Regression/Member/UploadAndEditPhotos");

        //        Setup();

        //        StartTest_UsingSpark();

        //        string login = System.Configuration.ConfigurationManager.AppSettings["Spark_RegisteredUser2_Email"];
        //        string password = System.Configuration.ConfigurationManager.AppSettings["Spark_RegisteredUser2_Password"];

        //        WebdriverAutomation.Tests.ReusableCode.LoggingInAndOut loggingInAndOut = new LoggingInAndOut();
        //        WebdriverAutomation.Framework.Site_Pages.Home home = loggingInAndOut.LogIn_ProfileComplete(driver, log, login, password);
        //        WebdriverAutomation.Framework.Site_Pages.YourProfile profile = home.Header.YourProfileHeaderLink_Click();
        //        Framework.Site_Pages.ManageYourPhotos manageYourPhotos = profile.ManageYourPhotosButton_Click();

        //        // delete all existing photos except for the first one
        //        while (manageYourPhotos.Photo2PhotoExists())
        //        {
        //            manageYourPhotos.Photo1XButton_Click();
        //            manageYourPhotos.SaveChangesButton_Click();
        //        }

        //        // grab a random picture in our solution's file path and put it in the Upload a Photo textbox
        //        string strFileName = string.Empty;
        //        Random random = new Random();

        //        // instead of grabbing our profile picture from the solution now we are grabbing it from a networked folder both QA and I can access:
        //        //   S:\Quality Assurance\Automation Builds\PicsForAutomation
        //        //FileInfo[] files = HelperMethods.GetReqFileInfo(@"*.jpg");
        //        //if (files.Length > 0)
        //        //    strFileName = files[random.Next(0, files.Length - 1)].FullName;
        //        strFileName = "s:\\Quality Assurance\\Automation Builds\\PicsForAutomation\\ProfilePicture.jpg";

        //        string photoCaption = "I look better in this photo";

        //        manageYourPhotos.Photo2UploadAPhotoTextbox_Write(strFileName);
        //        manageYourPhotos.Photo2CaptionTextarea = photoCaption;
        //        manageYourPhotos.SaveChangesButton_Click();

        //        // verify the text, "Profile successfully changed" appears
        //        Assert.True(manageYourPhotos.NotificationText == "Profile successfully changed", "ERROR - MANAGE YOUR PHOTOS PAGE - After Uploading a photo and pressing the Save Changes button, the 'Profile successfuly changed' text did not appear");

        //        // Delete and then Undelete the newly uploaded photo
        //        manageYourPhotos.Photo2XButton_Click();
        //        Assert.True(manageYourPhotos.Photo2Notification.Contains("Photo 2 and its caption will be deleted"), "ERROR - MANAGE YOUR PHOTOS PAGE - After Uploading a photo and pressing the Delete link for Photo 2, the 'Photo 2 and its caption wil be deleted...' text did not appear");
        //        manageYourPhotos.Photo2UndeleteLink_Click();
        //        manageYourPhotos.SaveChangesButton_Click();

        //        Assert.True(manageYourPhotos.Photo2PhotoExists(), "ERROR - MANAGE YOUR PHOTOS PAGE - After Uploading a photo and pressing the Save Changes button, we deleted the photo then immediately undeleted it and pressed Save again \r\nThe photo that was uploaded and then undeleted is not found");
        //        Assert.True(manageYourPhotos.Photo2CaptionTextarea == photoCaption, "ERROR - MANAGE YOUR PHOTOS PAGE - After Uploading a photo and pressing the Save Changes button, we deleted the photo then immediately undeleted it and pressed Save again \r\nThe caption that was uploaded previously is not the same. Previous caption value: " + photoCaption + ". The caption value now: " + manageYourPhotos.Photo1CaptionTextarea);

        //        // go to the Home Page then back to Manage Your Photos and verify that a photo still exists (the Upload a New Photo image does not) 
        //        //   and that the caption is still there
        //        home = manageYourPhotos.Header.HomeHeaderLink_Click();
        //        profile = home.Header.YourProfileHeaderLink_Click();
        //        manageYourPhotos = profile.ManageYourPhotosButton_Click();

        //        Assert.True(manageYourPhotos.Photo2PhotoExists(), "ERROR - MANAGE YOUR PHOTOS PAGE - After Uploading a photo and pressing the Save Changes button, we navigated back to the Home page and then back to the Manage Your Photos page. The photo that was uploaded previously cannot be found");
        //        Assert.True(manageYourPhotos.Photo2CaptionTextarea == photoCaption, "ERROR - MANAGE YOUR PHOTOS PAGE - After Uploading a photo and pressing the Save Changes button, we navigated back to the Home page and then back to the Manage Your Photos page. The caption that was uploaded previously is not the same. Previous caption value: " + photoCaption + ". The caption value now: " + manageYourPhotos.Photo1CaptionTextarea);

        //        // Reinitialize: Delete the photo we just added and verify that it really gets deleted
        //        manageYourPhotos.Photo2XButton_Click();
        //        manageYourPhotos.SaveChangesButton_Click();

        //        Assert.IsFalse(manageYourPhotos.Photo2PhotoExists(), "ERROR - MANAGE YOUR PHOTOS PAGE - After deleting a photo and pressing the Save Changes button, the photo was not deleted");

        //        manageYourPhotos.Header.Logout_Click();

        //        TearDown();

        //        log.Info("END TEST - Spark/Regression/Member/UploadAndEditPhotos()");
        //    }
        //    catch (Exception e)
        //    {
        //        CleanupFailedTestcase(e);
        //        throw;
        //    }
        //}        

        [Test]
        public void Validate_OmnitureExistsInLandingAndRegPages()
        {
            string errorLogNotes = string.Empty;

            try
            {
                log.Info("START TEST - Spark/Regression/Member/OmnitureExistsInLandingAndRegPages");

                Setup();

                StartTest_UsingSpark();

                WebdriverAutomation.Framework.Site_Pages.Splash splash = new WebdriverAutomation.Framework.Site_Pages.Splash(driver);                

                string omnitureSearchQuery1 = "s.prop";
                string omnitureSearchQuery2 = "s.eVar";

                if (!driver.PageSource.Contains(omnitureSearchQuery1) || !driver.PageSource.Contains(omnitureSearchQuery2))
                    Assert.Fail("In the Landing Page we are unable to find any omniture variables beginning with 's.prop' or 's.eVar' within the page source.");

                RegisterNewUser();

                if (!driver.PageSource.Contains(omnitureSearchQuery1) || !driver.PageSource.Contains(omnitureSearchQuery2))
                    Assert.Fail("In the Registration Complete Page we are unable to find any omniture variables beginning with 's.prop' or 's.eVar' within the page source.");

                //Remove the user we just created
                WebdriverAutomation.Tests.ReusableCode.AccountSettings accountSettings = new AccountSettings();
                accountSettings.RemoveProfile_SkippingUpdateProfileAndImportantNoticePages(driver, log);

                TearDown();

                log.Info("END TEST - Spark/Regression/Member/OmnitureExistsInLandingAndRegPages");
            }
            catch (Exception e)
            {
                CleanupFailedTestcase(e, errorLogNotes);
                throw;
            }
        }

        private int AgeOfUser(string birthdayMonth, string birthdayDay, string birthdayYear)
        {
            string myDateTimeString = string.Empty;
            myDateTimeString = birthdayDay + " " + birthdayMonth + "," + birthdayYear;

            DateTime birthday;
            birthday = Convert.ToDateTime(myDateTimeString);

            DateTime now = DateTime.Today;
            int age = now.Year - birthday.Year;
            if (birthday > now.AddYears(-age))
                age--;

            return age;
        }

        private string RemoveAllWhitespacesFromString(string originalString)
        {
            return Regex.Replace(originalString, @"\s", "");
        }

        private bool AreValuesAndCountEqual(List<string> list1, List<string> list2)
        {
            if (list1.Count != list2.Count)
                return false;

            for (int i = 0; i < list1.Count; i++)
            {
                if (list1[i] != list2[i])
                    return false;
            }

            return true;
        }

        private void RegisterNewUser()
        {
            WebdriverAutomation.Framework.Site_Pages.Splash splash = new WebdriverAutomation.Framework.Site_Pages.Splash(driver);

            WebdriverAutomation.Framework.Site_Pages.Registration registration;

            if (driver.Url.Contains("spark.com"))
            {
                registration = splash.BrowseForFreeLink_Click();

                // page 1
                registration.YouAreA_NewRegNotOnBedrock_Select(2);
                registration.ContinueButton_NewRegNotOnBedrock_ClickUntilProgressBarChanges();

                // page 2
                registration.WhatIsYourCurrentRelationshipStatus_NewRegNotOnBedrock_Select(2);
                registration.ContinueButton_NewRegNotOnBedrock_ClickUntilProgressBarChanges();

                // page 3
                registration.WhatIsYourEthnicity_NewRegNotOnBedrock_Select_Select(1);
                registration.ContinueButton_NewRegNotOnBedrock_ClickUntilProgressBarChanges();

                // page 4
                registration.WhatIsYourReligiousBackground_NewRegNotOnBedrock_Select(2);
                registration.ContinueButton_NewRegNotOnBedrock_ClickUntilProgressBarChanges();

                // page 5
                registration.ZipCodeTextbox_NewRegNotOnBedrock_Write("90210");
                registration.ContinueButton_NewRegNotOnBedrock_ClickUntilProgressBarChanges();

                // page 6
                registration.UserNameTextbox_NewRegNotOnBedrock_Write("QAAutomation");

                registration.BirthDateMonthList_NewRegNotOnBedrock_Select(4);
                registration.BirthDateDayList_NewRegNotOnBedrock_Select(4);
                registration.BirthDateYearList_NewRegNotOnBedrock_Select(4);

                registration.ContinueButton_NewRegNotOnBedrock_ClickUntilProgressBarChanges();

                // page 7
                string emailAddress = "QAAutomation_" + DateTime.Now.ToString() + "@gmail.com";
                emailAddress = emailAddress.Replace("/", "_").Replace(" ", "_").Replace(":", "_");
                registration.EmailAddressTextbox_NewRegNotOnBedrock_Write(emailAddress);

                registration.ChooseYourPasswordTextbox_NewRegNotOnBedrock_Write("Automation");

                registration.ContinueButton_NewRegNotOnBedrock_Click();

                // page 8
                registration.IWouldLikeSpecialOffersAndAnnouncements_NewRegNotOnBedrock_Click();

                registration.ContinueButton_NewRegNotOnBedrock_Click();

                // registration complete
                WebdriverAutomation.Framework.Site_Pages.RegistrationComplete registrationComplete = new WebdriverAutomation.Framework.Site_Pages.RegistrationComplete(driver);
            }


        }
    }
}
