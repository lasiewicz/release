﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.Events;
using NUnit.Framework;
using log4net;
using WebdriverAutomation.Tests.ReusableCode;
using WebdriverAutomation.Framework.Site_Pages;

namespace WebdriverAutomation.Tests.Spark.Regression
{
    [TestFixture]
    public class Subscriber : BaseTest
    {
        /// <summary>
        /// For Spark:
        /// A registered user who views any other profile and clicks on the Email Me Now successfully reaches the Compose page.  But if the user presses the
        /// Send button they get taken to the Subscribe Page.
        /// </summary>
        [Test]
        public void Validate_EMail_RegisteredUserToAnyUser()
        {
            try
            {
                log.Info("START TEST - Spark/Regression/Member/EMail_RegisteredUserToAnyUser");

                Setup();

                StartTest_UsingSpark();

                string login = System.Configuration.ConfigurationManager.AppSettings["Spark_RegisteredUser1_Email"];
                string password = System.Configuration.ConfigurationManager.AppSettings["Spark_RegisteredUser1_Password"];

                WebdriverAutomation.Tests.ReusableCode.LoggingInAndOut loggingInAndOut = new LoggingInAndOut();
                WebdriverAutomation.Framework.Site_Pages.Home home = loggingInAndOut.LogIn_ProfileComplete(driver, log, login, password);

                // user 1 - search for user 2 (sparksub1@gmail.com, a subscribed user)
                string user2MemberNumber = System.Configuration.ConfigurationManager.AppSettings["Spark_SubscribedUser1_MemberID"];

                Framework.Site_Pages.LookUpMember lookUpMember = home.Header.LookUpMemberSubHeaderLink_Click();

                lookUpMember.MemberNumberTextbox = user2MemberNumber;
                Framework.Site_Pages.YourProfile yourProfile = lookUpMember.MemberNumberSearchButton_Click();

                // user 1 - attempt to message user 2 and verify that the user is taken to the Subscribe page when the send button is pressed in the Compose page.    
                string subject = "reg to sub " + DateTime.Now.ToString();
                string message = "Validate EMail Registered User To Any User test case Message timestamp: " + DateTime.Now.ToString();

                Framework.Site_Pages.ComposeMessage composeMessage = yourProfile.EmailMeNowButton_Click();
                composeMessage.SubjectTextbox = subject;
                composeMessage.MessageTextarea = message;
                Framework.Site_Pages.Subscribe subscribe = composeMessage.SendButton_NonSubscribedSparkUser_Click();

                loggingInAndOut.LogOut(driver, log);                

                TearDown();

                log.Info("END TEST - Spark/Regression/Member/EMail_RegisteredUserToAnyUser");
            }
            catch (Exception e)
            {
                CleanupFailedTestcase(e);
                throw;
            }
        }

        /// <summary>
        /// For Spark:
        /// A subscribed user is allowed to send an email to anybody
        /// When the registered user gets the email they can enter their inbox and can see that they have emails waiting to be read. The can also see who sent the email and
        /// the subject.  If they click on the email, they can read the contents of the email and they can reply back.
        /// </summary>
        [Test]
        public void Validate_EMail_SubscribedUserToAnyUser()
        {
            try
            {
                log.Info("START TEST - Spark/Regression/Member/EMail_SubscribedUserToAnyUser");

                Setup();

                StartTest_UsingSpark();

                string user1_login = System.Configuration.ConfigurationManager.AppSettings["Spark_SubscribedUser1_Email"];
                string user1_password = System.Configuration.ConfigurationManager.AppSettings["Spark_SubscribedUser1_Password"];
                string user1_Username = System.Configuration.ConfigurationManager.AppSettings["Spark_SubscribedUser1_Username"];

                string user2_login = System.Configuration.ConfigurationManager.AppSettings["Spark_RegisteredUser1_Email"];
                string user2_password = System.Configuration.ConfigurationManager.AppSettings["Spark_RegisteredUser1_Password"];
                string user2_Username = System.Configuration.ConfigurationManager.AppSettings["Spark_RegisteredUser1_Username"];
                string user2_MemberID = System.Configuration.ConfigurationManager.AppSettings["Spark_RegisteredUser1_MemberID"];

                // login as user 1 (a subscribed user)
                WebdriverAutomation.Tests.ReusableCode.LoggingInAndOut loggingInAndOut = new LoggingInAndOut();
                WebdriverAutomation.Framework.Site_Pages.Home home = loggingInAndOut.LogIn_ProfileComplete(driver, log, user1_login, user1_password);
                Framework.Site_Pages.QuickSearch quickSearch = home.Header.SearchHeaderLink_Click();

                // user 1 - search for user 2 (a registered user)
                Framework.Site_Pages.LookUpMember lookUpMember = quickSearch.Header.LookUpMemberSubHeaderLink_Click();

                lookUpMember.MemberNumberTextbox = user2_MemberID;
                Framework.Site_Pages.YourProfile yourProfile = lookUpMember.MemberNumberSearchButton_Click();

                // user 1 - message user 2          
                string subject = "sub to reg " + DateTime.Now.ToString();
                string message = "Validate EMail Subscribed User To Any User test case Message timestamp: " + DateTime.Now.ToString();

                Framework.Site_Pages.ComposeMessage composeMessage = yourProfile.EmailMeNowButton_Click();
                composeMessage.SubjectTextbox = subject;
                composeMessage.MessageTextarea = message;
                Framework.Site_Pages.MessageSent messageSent = composeMessage.SendButton_Click();

                // user 1 - verify that the "Your AllAccess Message has been sent" text appears
                Assert.IsTrue(messageSent.VerificationMessage().Contains("Your Message has been sent"), "In the EMail_SubscribedUserToAnyUser test case, after All Access user1 emails Registered user2 the 'Your AllAccess Message has been sent' message did not appear");

                // user 1 - log out user 1 and login user 2 (a non subscribed user)
                Framework.Site_Pages.Login login = loggingInAndOut.LogOut(driver, log);
                home = loggingInAndOut.LogIn_ProfileComplete(driver, log, user2_login, user2_password);

                // user 2 - go into the inbox and verify that the message just sent exists
                Framework.Site_Pages.Messages messages = home.Header.InboxHeaderLink_Click();
                Assert.IsTrue(messages.DoesEmailExist(user1_Username, subject), "In the EMail_SubscribedUserToAnyUser test case, after Subscribed user1 emails Registered user2 and user2 logs in, the email that was sent does not appear in user2's inbox.  \r\nWe are looking for an email with user1_Username: '" + user1_Username + "' and subject: '" + subject + "'");

                // user 2 - verify that the message just sent has an unopened envelope icon next to it
                Assert.IsFalse(messages.IsEnvelopeIconOpened(user1_Username, subject), "In the EMail_SubscribedUserToAnyUser test case, after Subscribed user1 emails Registered user2 and user2 logs in, the envelope icon next to the email that was just sent does not appear as a closed envelope");

                // user 2 - click on the message and verify that the message sent is the same
                Framework.Site_Pages.Message messagePage = messages.EmailLink_Click(user1_Username, subject);
                Assert.IsTrue(messagePage.DoesEmailContainCorrectMessage(message), "In the EMail_SubscribedUserToAnyUser test case, after Subscribed user1 emails Registered user2 and user2 logs in, the email's MESSAGE that was sent is not the same message that was originally sent from user1.  \r\nOriginal message: '" + message + "'\r\nMessage currently onscreen: '" + messagePage.GetEmailMessage() + "'");

                // user 2 - use the Reply Back for Free feature to message back to user1
                composeMessage = messagePage.ReplyForFreeButton_Click();

                string replySubject = "reg to sub " + DateTime.Now.ToString();
                string replyMessage = "Validate EMail Subscribed User To Registered User test case Message timestamp: " + DateTime.Now.ToString();
                composeMessage.SubjectTextbox = replySubject;
                composeMessage.MessageTextarea = replyMessage;
                messageSent = composeMessage.SendButton_Click_ReplyingToEmail();

                // user 2 - verify that the "Your Message has been sent" text appears
                Assert.IsTrue(messageSent.VerificationMessage().Contains("Your Message has been sent"), "In the EMail_SubscribedUserToAnyUser test case, after Subscribed user1 emails Registered user2 the 'Your Message has been sent' message did not appear");

                messages = messageSent.BackToInboxLink_Click();

                // user 2 - verify that the message just sent has an unopened envelope icon with a red reply arrow next to it
                Assert.IsTrue(messages.IsEnvelopeIconOpened(user1_Username, subject), "In the EMail_SubscribedUserToAnyUser test case, after Subscribed user1 emails Registered user2 and user2 replies, the envelope icon next to the email that was just sent appears as a closed envelope");
                
                // user 2 - reininitialize - delete all All Access emails and logout
                messages.EmailSelectAllCheckboxes_Check();
                messages.DeleteLink_Click();

                // log out user 2 and login user 1 again
                login = loggingInAndOut.LogOut(driver, log);
                home = loggingInAndOut.LogIn_ProfileComplete(driver, log, user1_login, user1_password);

                // user 1 - go to the Inbox and verify that the email sent back from Subscribed user 2 exists
                messages = home.Header.InboxHeaderLink_Click();
                messagePage = messages.EmailLink_Click(user2_MemberID, replySubject);
                Assert.IsTrue(messagePage.DoesEmailContainCorrectMessage(replyMessage), "In the EMail_SubscribedUserToAnyUser test case, after Subscribed user1 emails Registered user2 and user2 replied back, user 1 logged back in and the replied email's MESSAGE that was sent is not the same message that was originally sent from user2  \r\nReply message that we are expecting: '" + replyMessage + "' \r\nMessage currently onscreen: '" + messagePage.GetEmailMessage() + "'");

                // user 1 - reininitialize - delete all All Access emails and logout
                messages = messagePage.BackToMessagesLink_Click();
                messages.EmailSelectAllCheckboxes_Check();
                messages.DeleteLink_Click();

                loggingInAndOut.LogOut(driver, log);

                TearDown();
                
                log.Info("END TEST - Spark/Regression/Member/EMail_SubscribedUserToAnyUser");
            }
            catch (Exception e)
            {
                CleanupFailedTestcase(e);
                throw;
            }
        }

        [Test]
        public void Validate_YourAccountLinks()
        {
            try
            {
                log.Info("START TEST - Spark/Regression/Member/YourAccountLinks");

                Setup();

                StartTest_UsingSpark();

                string login = System.Configuration.ConfigurationManager.AppSettings["Spark_SubscribedUser1_Email"];
                string password = System.Configuration.ConfigurationManager.AppSettings["Spark_SubscribedUser1_Password"];

                string yourAccountPageURL = string.Empty;

                if (driver.Url.Contains("preprod"))
                    yourAccountPageURL = @"http://preprod.spark.com/Applications/MemberServices/MemberServices.aspx?NavPoint=top";
                else
                    yourAccountPageURL = @"http://www.spark.com/Applications/MemberServices/MemberServices.aspx?NavPoint=top";

                WebdriverAutomation.Tests.ReusableCode.LoggingInAndOut loggingInAndOut = new LoggingInAndOut();
                WebdriverAutomation.Framework.Site_Pages.Home home = loggingInAndOut.LogIn_ProfileComplete(driver, log, login, password);

                WebdriverAutomation.Framework.Site_Pages.YourAccount yourAccount = home.Header.YourAccountLink_Click();

                WebdriverAutomation.Framework.Site_Pages.ProfileDisplaySettings profileDisplaySettings = yourAccount.ProfileDisplaySettingsLink_Click();
                yourAccount = profileDisplaySettings.Header.YourAccountLink_Click();

                WebdriverAutomation.Framework.Site_Pages.YourProfile profile = yourAccount.ChangeYourProfileLink_Click();
                yourAccount = profileDisplaySettings.Header.YourAccountLink_Click();

                WebdriverAutomation.Framework.Site_Pages.ChangeYourEmailOrPassword changeYourEmailOrPassword = yourAccount.ChangeYourEmailOrPasswordLink_Click();
                yourAccount = profileDisplaySettings.Header.YourAccountLink_Click();

                WebdriverAutomation.Framework.Site_Pages.ColorCodeSettings colorCodeSettings = yourAccount.ColorCodeSettingsLink_Click();
                yourAccount = profileDisplaySettings.Header.YourAccountLink_Click();

                WebdriverAutomation.Framework.Site_Pages.Subscribe subscribe = yourAccount.MembershipPlansAndCostsLink_Click();
                driver.Navigate().GoToUrl(yourAccountPageURL);

                WebdriverAutomation.Framework.Site_Pages.AutoRenewalSettings autoRenewalSettings = yourAccount.AutoRenewalSettingsLink_Click();
                yourAccount = profileDisplaySettings.Header.YourAccountLink_Click();

                subscribe = yourAccount.SubscribeLink_Click();
                driver.Navigate().GoToUrl(yourAccountPageURL);

                //// Since this user account already has Highlighted Profile and Member Spotlight clicking on Premium Services takes the user to the Home Page.  
                ////    Lawrence gave the okay to just skip this link for this account for now.
                //WebdriverAutomation.Framework.Site_Pages.GetMoreAttentionByAddingPremiumServices premiumServices = yourAccount.PremiumServicesLink_Click();
                //driver.Navigate().GoToUrl(yourAccountPageURL);

                //// This user has a credit card that Lawrence put in on file but for some reason this link still doesn't shot up
                //WebdriverAutomation.Framework.Site_Pages.PremiumServiceSettings premiumServiceSettings = yourAccount.PremiumServiceSettingsLink_Click();
                //yourAccount = profileDisplaySettings.Header.YourAccountLink_Click();

                WebdriverAutomation.Framework.Site_Pages.RemoveMyProfile removeMyProfile = yourAccount.RemoveMyProfileLink_Click();
                yourAccount = profileDisplaySettings.Header.YourAccountLink_Click();

                WebdriverAutomation.Framework.Site_Pages.AccountHistory accountHistory = yourAccount.AccountInformationLink_Click();
                yourAccount = profileDisplaySettings.Header.YourAccountLink_Click();

                WebdriverAutomation.Framework.Site_Pages.VerifyYourEmailAddress verifyEmail = yourAccount.VerifyEmailLink_Click();
                yourAccount = profileDisplaySettings.Header.YourAccountLink_Click();

                WebdriverAutomation.Framework.Site_Pages.MessageSettings messageSettings = yourAccount.MessageSettingsLink_Click();
                yourAccount = profileDisplaySettings.Header.YourAccountLink_Click();

                messageSettings = yourAccount.OffSiteMessageSettingsLink_Click();
                yourAccount = profileDisplaySettings.Header.YourAccountLink_Click();

                WebdriverAutomation.Framework.Site_Pages.WhosCheckingYouOut whosCheckingYouOut = yourAccount.MembersWhoEmailedYouLink_Click();
                yourAccount = profileDisplaySettings.Header.YourAccountLink_Click();

                WebdriverAutomation.Framework.Site_Pages.ContactingMembers contactingMembers = yourAccount.NeverMissEmailsLink_Click();
                yourAccount = profileDisplaySettings.Header.YourAccountLink_Click();

                whosCheckingYouOut = yourAccount.MembersBlockedFromContactingYouLink_Click();
                yourAccount = profileDisplaySettings.Header.YourAccountLink_Click();

                WebdriverAutomation.Framework.Site_Pages.YoureCheckingOut youreCheckingOut = yourAccount.MembersBlockedFromAppearingInYourSearchResultsLink_Click();
                yourAccount = profileDisplaySettings.Header.YourAccountLink_Click();

                whosCheckingYouOut = yourAccount.MembersBlockedFromViewingYourProfileInHisHerSearchResults_Click();
                yourAccount = profileDisplaySettings.Header.YourAccountLink_Click();

                WebdriverAutomation.Framework.Site_Pages.FrequentlyAskedQuestions faq = yourAccount.FAQLink_Click();
                yourAccount = profileDisplaySettings.Header.YourAccountLink_Click();

                WebdriverAutomation.Framework.Site_Pages.ContactUs contactUs = yourAccount.ContactUsLink_Click();
                yourAccount = profileDisplaySettings.Header.YourAccountLink_Click();

                WebdriverAutomation.Framework.Site_Pages.Privacy privacyStatement = yourAccount.PrivacyStatementLink_Click();
                yourAccount = profileDisplaySettings.Header.YourAccountLink_Click();

                privacyStatement = yourAccount.TermsAndConditionsOfServiceLink_Click();
                yourAccount = profileDisplaySettings.Header.YourAccountLink_Click();

                privacyStatement = yourAccount.TermsAndConditionsOfPurchaseLink_Click();
                yourAccount = profileDisplaySettings.Header.YourAccountLink_Click();

                TearDown();

                log.Info("END TEST - Spark/Regression/Member/YourAccountLinks");
            }
            catch (Exception e)
            {
                CleanupFailedTestcase(e);
                throw;
            }
        }
    }
}
