﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.Events;
using NUnit.Framework;
using log4net;
using WebdriverAutomation.Tests.ReusableCode;
using WebdriverAutomation.Framework.Site_Pages;

namespace WebdriverAutomation.Tests.AdminTool.Regression
{
    [TestFixture]
    public class HomePage : BaseTest
    {        
        [Test]
        public void Validate_Header()
        {
            try
            {
                log.Info("START TEST - AdminTool/Regression/HomePage/Validate_Header");

                const string QA_Login = "lbenedicto@spark.net";
                const string QA_Password = "1111";

                Setup_WithFirefoxCustomProfile();

                StartTest_UsingAdminTool();

                WebdriverAutomation.Framework.AdminTool_Pages.HomePage homePage = new WebdriverAutomation.Framework.AdminTool_Pages.HomePage(driver);

                //// Free Text Approval
                //WebdriverAutomation.Framework.AdminTool_Pages.FreeTextApproval freeTextApprovalPage = homePage.Header.FreeTextApproval_EnglishLink_Click();

                //// Photo Approval
                //WebdriverAutomation.Framework.AdminTool_Pages.PhotoApproval photoApproval = freeTextApprovalPage.Header.PhotoApproval_JDatecomLink_Click();

                // Question/Answer Approval
                WebdriverAutomation.Framework.AdminTool_Pages.QuestionAnswerApproval questionAnswerApproval = homePage.Header.QuestionAnswerApproval_EnglishLink_Click();

                // Credit Tool
                WebdriverAutomation.Framework.AdminTool_Pages.CreditTool creditTool = questionAnswerApproval.Header.CreditTool_Click();

                // Message Boards
                questionAnswerApproval.Header.MessageBoards_JDate_Click();
                List<string> handlers = new List<string>();
                SwitchToNewWindow(handlers);

                WebdriverAutomation.Framework.AdminTool_Pages.MessageBoardsLogin messageBoardsLogin = new Framework.AdminTool_Pages.MessageBoardsLogin(driver);
                messageBoardsLogin.EmailTextbox = QA_Login;
                messageBoardsLogin.PasswordTextbox = QA_Password;
                WebdriverAutomation.Framework.AdminTool_Pages.MessageBoards messageBoards = messageBoardsLogin.LogInButton_Click();

                CloseNewWindow(handlers);

                // Reports
                questionAnswerApproval.Header.Reports_AdminActionLog_Click();
                handlers = new List<string>();
                SwitchToNewWindow(handlers);

                WebdriverAutomation.Framework.Site_Pages.Login login = null;

                if (IsCurrentPageTheLoginPage())
                {
                    login = new Framework.Site_Pages.Login(driver);
                    login.EmailTextbox_Write(QA_Login);
                    login.PasswordTextbox_Write(QA_Password);
                    login.RememberMeCheckbox_Uncheck();
                    login.LoginButton_Click(QA_Login);
                }

                WebdriverAutomation.Framework.AdminTool_Pages.ActionReport actionReport = new Framework.AdminTool_Pages.ActionReport(driver);
                login = actionReport.LogoutLink_Click();

                CloseNewWindow(handlers);

                WebdriverAutomation.Framework.AdminTool_Pages.ApprovalCounts approvalCounts = questionAnswerApproval.Header.Reports_ApprovalCounts_Click();

                WebdriverAutomation.Framework.AdminTool_Pages.ViewQueueCounts viewQueueCounts = approvalCounts.Header.Reports_QueueStats_Click();

                WebdriverAutomation.Framework.AdminTool_Pages.BulkMailBatchReport bulkMailBatchReport = viewQueueCounts.Header.Reports_BulkMailBatch_Click();

                WebdriverAutomation.Framework.AdminTool_Pages.PhotosQueue photosQueue = viewQueueCounts.Header.Reports_PhotosQueue_Click();

                // Subscription Admin
                WebdriverAutomation.Framework.AdminTool_Pages.PromoSearch promoSearch = viewQueueCounts.Header.SubscriptionAdmin_PromoSearch_Click();
                driver.Navigate().GoToUrl(System.Configuration.ConfigurationManager.AppSettings["AdminTool"]);

                homePage.Header.SubscriptionAdmin_SubAdminToolLegacy_Click();
                handlers = new List<string>();
                SwitchToNewWindow(handlers);

                if (IsCurrentPageTheLoginPage())
                {
                    login = new Framework.Site_Pages.Login(driver);
                    login.EmailTextbox_Write(QA_Login);
                    login.PasswordTextbox_Write(QA_Password);
                    login.RememberMeCheckbox_Uncheck();
                    login.LoginButton_Click(QA_Login);
                }

                WebdriverAutomation.Framework.AdminTool_Pages.SubAdminToolLegacy subAdminToolLegacy = new Framework.AdminTool_Pages.SubAdminToolLegacy(driver);
                login = subAdminToolLegacy.Header.Logout_Click();

                CloseNewWindow(handlers);

                WebdriverAutomation.Framework.AdminTool_Pages.CreatePlan createPlan = viewQueueCounts.Header.SubscriptionAdmin_CreatePlan_Click();
                driver.Navigate().GoToUrl(System.Configuration.ConfigurationManager.AppSettings["AdminTool"]);

                // DNE
                WebdriverAutomation.Framework.AdminTool_Pages.DNESearch dneSearch = viewQueueCounts.Header.DNE_Search_Click();

                WebdriverAutomation.Framework.AdminTool_Pages.DNEDownload dneDownload = dneSearch.Header.DNE_Download_Click();

                dneDownload.Header.DNE_Upload_Click();
                handlers = new List<string>();
                SwitchToNewWindow(handlers);

                WebdriverAutomation.Framework.AdminTool_Pages.DNEUpload dneUpload = new Framework.AdminTool_Pages.DNEUpload(driver);

                CloseNewWindow(handlers);

                TearDown();

                log.Info("END TEST - AdminTool/Regression/HomePage/Validate_Header");
            }
            catch (Exception e)
            {
                CleanupFailedTestcase(e);
                throw;
            }            

        }

        //[Test]
        //public void Validate_Search_Email()
        //{
        //    try
        //    {
        //        log.Info("START TEST - AdminTool/Regression/HomePage/Validate_Search_Email");

        //        Setup_WithFirefoxCustomProfile();

        //        StartTest_UsingAdminTool();

        //        string email = System.Configuration.ConfigurationManager.AppSettings["BBW_SubscribedUser1_Email"];

        //        WebdriverAutomation.Framework.AdminTool_Pages.HomePage homePage = new WebdriverAutomation.Framework.AdminTool_Pages.HomePage(driver);

        //        homePage.EmailTextbox_Write(email);

        //        Framework.AdminTool_Pages.SearchResults searchResults = homePage.SearchButton_Click();







        //        TearDown();

        //        log.Info("END TEST - AdminTool/Regression/HomePage/Validate_Search_Email");
        //    }
        //    catch (Exception e)
        //    {
        //        CleanupFailedTestcase(e);
        //        throw;
        //    }

        //}

        public bool IsCurrentPageTheLoginPage()
        {
            string textThatVerifiesThisPageIsCorrect = "Login now";

            if (driver.FindElements(By.XPath("//fieldset[@id='loginBox']/h2/b")).Count > 0)
            {
                IWebElement textOnPage = driver.FindElement(By.XPath("//fieldset[@id='loginBox']/h2/b"));

                if (textOnPage.Text == textThatVerifiesThisPageIsCorrect)
                    return true;
            }

            return false;
        }
    }
}
