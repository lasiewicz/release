﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.Events;
using NUnit.Framework;
using log4net;
using WebdriverAutomation.Tests.ReusableCode;
using WebdriverAutomation.Framework.Site_Pages;

namespace WebdriverAutomation.Tests.JDate.Regression
{
    [TestFixture]
    public class Subscriber : BaseTest
    {
        /*
        /// <summary>
        /// This test case is half finished.  Put on hold because the credit card does not work anymore
        /// </summary>
        [Test]
        public void Validate_Subscribe()
        {
            log.Info("START TEST - JDate/Regression/Member/Subscribe");

            Setup();

            StartTest_UsingJDate();

            WebdriverAutomation.Tests.ReusableCode.Regstration registration = new Regstration();
            registration.RegisterNewUser(driver, log);

            driver.Navigate().GoToUrl(System.Configuration.ConfigurationManager.AppSettings["JDate"]);
            Framework.Site_Pages.Home home = new Framework.Site_Pages.Home(driver);
            Framework.Site_Pages.Subscribe subscribe = home.Header.SubscribeHeaderLink_Click();

            // randomly select any of the Standard Plans
            Random random = new Random();
            subscribe.SelectAPlanRadioButton_Select((Subscribe.SubscriptionPlans)random.Next(1, 4));

            // fill out billing info
            string firstName = "Lawrence";
            string lastName = "Benedicto";
            string address = "8383 Wilshire Blvd. Suite 800";
            string city = "Beverly Hills";
            string state = "CA";
            string zip = "90211";
            string country = "USA";
            string phoneNumber = "3236583000";
            string creditCardType = "Visa";
            string cardNumber = "4339931404904468";
            string expirationMonth = "09";
            string expirationYear = "2013";
            string CSC = "963";

            subscribe.FirstNameTextbox = firstName;
            subscribe.LastNameTextbox = lastName;
            subscribe.AddressTextbox = address;
            subscribe.CityTextbox = city;
            subscribe.StateTextbox = state;
            subscribe.ZipTextbox = zip;
            subscribe.CountryDropdown = country;
            subscribe.PhoneNumberTextbox = phoneNumber;
            subscribe.CreditCardTypeDropdown = creditCardType;
            subscribe.CardNumberTextbox = cardNumber;
            subscribe.ExpirationMonthDropdown = expirationMonth;
            subscribe.ExpirationYearDropdown = expirationYear;
            subscribe.CSCTextbox = CSC;

            Framework.Site_Pages.SubscribeConfirmation subscribeConfirmation = subscribe.ProcessOrderButton_Click();
            home = subscribeConfirmation.ContinueWithoutAddingAllAccessLink_Click();

            Framework.Site_Pages.YourProfile profile = home.Header.YourProfileHeaderLink_Click();          

            // verify that the Subscription link in the header is now the Upgrade link
            Assert.IsTrue(profile.Header.UpgradeHeaderLinkExists(), "In the Validate_Subscribe test case, after subscribing a user account the Upgrade link in the header should be visible and should replace the Subscribe link. The Upgrade link cannot be found");
            
            // verify that the Source contains information that a subscription was added
            


            //Remove the user we just created
            WebdriverAutomation.Tests.ReusableCode.AccountSettings accountSettings = new AccountSettings();
            accountSettings.RemoveProfile(driver, log);

            TearDown();

            log.Info("END TEST - JDate/Regression/Member/Subscribe");

        }
        */

        [Test]
        public void Validate_EMail_RegisteredUserToAnyUser()
        {
            try
            {
                log.Info("START TEST - JDate/Regression/Member/EMail_RegisteredUserToAnyUser");

                Setup();

                StartTest_UsingJDate();

                string login = System.Configuration.ConfigurationManager.AppSettings["JDate_RegisteredUser1_Email"];
                string password = System.Configuration.ConfigurationManager.AppSettings["JDate_RegisteredUser1_Password"];

                WebdriverAutomation.Tests.ReusableCode.LoggingInAndOut loggingInAndOut = new LoggingInAndOut();
                WebdriverAutomation.Framework.Site_Pages.Home home = loggingInAndOut.LogIn_ProfileComplete(driver, log, login, password);

                // just verify that a registered user cannot even check their inbox - the Subscribe page appears instead
                Framework.Site_Pages.Subscribe subscribe = home.Header.InboxHeaderLink_Click_JDateNonSubscribedUser();

                driver.Navigate().GoToUrl(System.Configuration.ConfigurationManager.AppSettings["JDate"]);
                home = new WebdriverAutomation.Framework.Site_Pages.Home(driver);
                home.Footer.LogoutLink_Click();

                TearDown();

                log.Info("END TEST - JDate/Regression/Member/EMail_RegisteredUserToAnyUser");
            }
            catch (Exception e)
            {
                CleanupFailedTestcase(e);
                throw;
            }
        }

        [Test]
        public void Validate_EMail_SubscribedUserToRegisteredUser()
        {
            try
            {
                log.Info("START TEST - JDate/Regression/Member/EMail_SubscribedUserToRegisteredUser");

                Setup();

                StartTest_UsingJDate();

                string user1_login = System.Configuration.ConfigurationManager.AppSettings["JDate_SubscribedUser1_Email"];
                string user1_password = System.Configuration.ConfigurationManager.AppSettings["JDate_SubscribedUser1_Password"];
                string user1_Username = System.Configuration.ConfigurationManager.AppSettings["JDate_SubscribedUser1_Username"];

                // login as user 1 (a subscribed user)
                WebdriverAutomation.Tests.ReusableCode.LoggingInAndOut loggingInAndOut = new LoggingInAndOut();
                WebdriverAutomation.Framework.Site_Pages.Home home = loggingInAndOut.LogIn_ProfileComplete(driver, log, user1_login, user1_password);
                Framework.Site_Pages.QuickSearch quickSearch = home.Header.SearchHeaderLink_Click();

                // user 1 - search for user 2 (a registered user)
                string user2Username = "bashreg1";

                Framework.Site_Pages.LookUpMember lookUpMember = quickSearch.Header.LookUpMemberSubHeaderLink_Click();

                lookUpMember.UsernameTextbox = user2Username;
                Framework.Site_Pages.YourProfile yourProfile = lookUpMember.UsernameSearchButton_Click();

                // user 1 - message user 2          
                string subject = "sub to reg " + DateTime.Now.ToString();
                string message = "Validate EMail Subscribed User To Registered User test case Message timestamp: " + DateTime.Now.ToString();

                Framework.Site_Pages.ComposeMessage composeMessage = yourProfile.EmailMeNowButton_Click();
                composeMessage.SubjectTextbox = subject;
                composeMessage.MessageTextarea = message;
                composeMessage.SendButton_Click_AllAccessUnchecked();
                Framework.Site_Pages.MessageSent messageSent = composeMessage.NoThanksLink_Click();

                // user 1 - verify that the "Your AllAccess Message has been sent" text appears
                Assert.IsTrue(messageSent.VerificationMessage().Contains("Your Message has been sent"), "In the EMail_AllAccessUserToRegisteredUser test case, after All Access user1 emails Registered user2 the 'Your AllAccess Message has been sent' message did not appear");

                // user 1 - log out user 1 and login user 2
                Framework.Site_Pages.Login login = loggingInAndOut.LogOut(driver, log);

                string user2_login = System.Configuration.ConfigurationManager.AppSettings["JDate_RegisteredUser1_Email"];
                string user2_password = System.Configuration.ConfigurationManager.AppSettings["JDate_RegisteredUser1_Password"];

                home = loggingInAndOut.LogIn_ProfileComplete(driver, log, user2_login, user2_password);

                // user 2 - attempt to go to the Inbox and verify that the Subscribe page appears instead
                Framework.Site_Pages.Subscribe subscribe = home.Header.InboxHeaderLink_Click_JDateNonSubscribedUser();

                // user 2 - verify that the message, "Become a Premium Member today" or "Become a Subscriber today" or "Home > Subscribe" appears
                string registeredUserInboxStatusMessage1 = "Become a Premium Member today";
                string registeredUserInboxStatusMessage2 = "Become a Subscriber today";
                string registeredUserInboxStatusMessage3 = "Home > Subscribe >";
                string headerStatusMessage = subscribe.HeaderStatusMessage();
                Assert.IsTrue(headerStatusMessage.Contains(registeredUserInboxStatusMessage1) || headerStatusMessage.Contains(registeredUserInboxStatusMessage2) || headerStatusMessage.Contains(registeredUserInboxStatusMessage3), "In the EMail_SubscribedUserToRegisteredUser test case, after Subscribed user1 emails Registered user2 and user2 logs in and attempts to access their Inbox, the Subscribe page should appear with the text, 'Want to read your message? Become a Premium Member today!'.  Instead the text reads: " + subscribe.HeaderStatusMessage());

                TearDown();

                log.Info("END TEST - JDate/Regression/Member/EMail_SubscribedUserToRegisteredUser");
            }
            catch (Exception e)
            {
                CleanupFailedTestcase(e);
                throw;
            }
        }

        [Test]
        public void Validate_EMail_SubscribedUserToSubscribedUser()
        {
            try
            {
                log.Info("START TEST - JDate/Regression/Member/EMail_SubscribedUserToSubscribedUser");

                Setup();

                StartTest_UsingJDate();

                string user1_login = System.Configuration.ConfigurationManager.AppSettings["JDate_SubscribedUser1_Email"];
                string user1_password = System.Configuration.ConfigurationManager.AppSettings["JDate_SubscribedUser1_Password"];
                string user1_Username = System.Configuration.ConfigurationManager.AppSettings["JDate_SubscribedUser1_Username"];

                string user2_login = System.Configuration.ConfigurationManager.AppSettings["JDate_SubscribedUser3_Email"];
                string user2_password = System.Configuration.ConfigurationManager.AppSettings["JDate_SubscribedUser3_Password"];
                string user2_MemberID = System.Configuration.ConfigurationManager.AppSettings["JDate_SubscribedUser3_MemberID"];

                // login as user 1 (a subscribed user)
                WebdriverAutomation.Tests.ReusableCode.LoggingInAndOut loggingInAndOut = new LoggingInAndOut();
                WebdriverAutomation.Framework.Site_Pages.Home home = loggingInAndOut.LogIn_ProfileComplete(driver, log, user1_login, user1_password);
                Framework.Site_Pages.QuickSearch quickSearch = home.Header.SearchHeaderLink_Click();

                // user 1 - search for user 2 (also a subscribed user)
                Framework.Site_Pages.LookUpMember lookUpMember = quickSearch.Header.LookUpMemberSubHeaderLink_Click();

                lookUpMember.MemberNumberTextbox = user2_MemberID;
                Framework.Site_Pages.YourProfile yourProfile = lookUpMember.MemberNumberSearchButton_Click();

                // user 1 - message user 2          
                string subject = "sub to sub " + DateTime.Now.ToString();
                string message = "Validate EMail Subscribed User To Subscribed User test case Message timestamp: " + DateTime.Now.ToString();

                Framework.Site_Pages.ComposeMessage composeMessage = yourProfile.EmailMeNowButton_Click();
                composeMessage.SubjectTextbox = subject;
                composeMessage.MessageTextarea = message;
                composeMessage.SendButton_Click_AllAccessUnchecked();
                Framework.Site_Pages.MessageSent messageSent = composeMessage.NoThanksLink_Click();

                // user 1 - verify that the "Your AllAccess Message has been sent" text appears
                Assert.IsTrue(messageSent.VerificationMessage().Contains("Your Message has been sent"), "In the EMail_AllAccessUserToRegisteredUser test case, after All Access user1 emails Registered user2 the 'Your AllAccess Message has been sent' message did not appear");

                // log out user 1 and login user 2
                Framework.Site_Pages.Login login = loggingInAndOut.LogOut(driver, log);

                home = loggingInAndOut.LogIn_ProfileComplete(driver, log, user2_login, user2_password);

                // user 2 - go into the inbox and verify that the message just sent exists
                Framework.Site_Pages.Messages messages = home.Header.InboxHeaderLink_Click();
                Assert.IsTrue(messages.DoesEmailExist(user1_Username, subject), "In the EMail_SubscribedUserToSubscribedUser test case, after Subscribed user1 emails Subscribed user2 and user2 logs in, the email that was sent does not appear in user2's inbox.  \r\nWe are looking for an email with user1_Username: '" + user1_Username + "' and subject: '" + subject + "'");

                // user 2 - verify that the message just sent has an unopened envelope icon next to it
                Assert.IsFalse(messages.IsEnvelopeIconOpened(user1_Username, subject), "In the EMail_SubscribedUserToSubscribedUser test case, after Subscribed user1 emails Subscribed user2 and user2 logs in, the envelope icon next to the email that was just sent does not appear as a red closed envelope");

                // user 2 - click on the message and verify that the message sent is the same
                Framework.Site_Pages.Message messagePage = messages.EmailLink_Click(user1_Username, subject);
                Assert.IsTrue(messagePage.DoesEmailContainCorrectMessage(message), "In the EMail_SubscribedUserToSubscribedUser test case, after Subscribed user1 emails Subscribed user2 and user2 logs in, the email's MESSAGE that was sent is not the same message that was originally sent from user1.  \r\nOriginal message: '" + message + "'\r\nMessage currently onscreen: '" + messagePage.GetEmailMessage() + "'");

                // user 2 - verify that the message that was just opened now has an opened envelope icon next to it
                messages = messagePage.BackToMessagesLink_Click();
                Assert.IsTrue(messages.IsEnvelopeIconOpened(user1_Username, subject), "In the EMail_SubscribedUserToSubscribedUser test case, after Subscribed user1 emails Subscribed user2 and user2 logs in and opens the message, the envelope icon next to the email that was just opened does not appear as a green opened envelope");

                // user 2 - reininitialize - delete all messages and logout
                messages.EmailSelectAllCheckboxes_Check();
                messages.DeleteLink_Click();

                messages.Header.Logout_Click();

                TearDown();

                log.Info("END TEST - JDate/Regression/Member/EMail_SubscribedUserToSubscribedUser");
            }
            catch (Exception e)
            {
                CleanupFailedTestcase(e);
                throw;
            }
        }

        /// <summary>
        /// All Access user 1 emails Registered user 2
        /// user 2 verifes email and replies back to user 1
        /// user 1 verifies email reply
        /// 
        /// In order for this test case to work, both user 1 and user 2 must not have any previous All Access messages sent to each other or the subject/messages will
        /// not match and the test case will fail.
        /// </summary>
        [Test]
        public void Validate_EMail_AllAccessUserToRegisteredUser()
        {
            try
            {
                log.Info("START TEST - JDate/Regression/Member/EMail_AllAccessUserToRegisteredUser");

                Setup();

                StartTest_UsingJDate();

                #region Reinitialize User 2's state: get rid of any existing All Access emails whether they've been read or not
                string user2_login = System.Configuration.ConfigurationManager.AppSettings["JDate_RegisteredUser2_Email"];
                string user2_password = System.Configuration.ConfigurationManager.AppSettings["JDate_RegisteredUser2_Password"];
                string user2_MemberNumber = System.Configuration.ConfigurationManager.AppSettings["JDate_RegisteredUser2_MemberID"];

                WebdriverAutomation.Tests.ReusableCode.LoggingInAndOut loggingInAndOut = new LoggingInAndOut();
                Framework.Site_Pages.Home home = loggingInAndOut.LogIn_ProfileComplete(driver, log, user2_login, user2_password);
                string pageWeAreOnAfterClickingInboxLink = home.Header.InboxHeaderLink_Click_ForJDateNonSubscribedUsersWhoDoNotKnowIfThereAreAllAccessMessagesWaiting();

                Framework.Site_Pages.Messages messages;
                Framework.Site_Pages.Message messagePage;

                if (pageWeAreOnAfterClickingInboxLink == "Messages")
                {
                    // All Access messages exist.  Delete all of them before starting the test 
                    messages = new Messages(driver);

                    // get rid of overlay if it exists
                    if (messages.DoesYouRecievedAnAllAccessEMailOverlayExist())
                    {
                        messagePage = messages.ReadNowButton_Click();
                        messages = messagePage.BackToMessagesLink_Click();

                        messages.AllAccessEmailSelectAllCheckboxes_Check();
                        messages.DeleteLink_Click();
                    }                    
                }
                // if we are on the subscription page we're okay and we continue but if we're on a page we don't recognize, fail out. if we are on subscribe but it gets to this else statement might have to sleep longer above 
                else if (pageWeAreOnAfterClickingInboxLink == "another page")
                    Assert.Fail("After logging in as a registered user and clicking on the Inbox header link we did not land on a page that we expected to.");

                // log out user 2 and login user 1
                Framework.Site_Pages.Login login = loggingInAndOut.LogOut(driver, log);
                #endregion

                string user1_login = System.Configuration.ConfigurationManager.AppSettings["JDate_AllAccessUser1_Email"];
                string user1_password = System.Configuration.ConfigurationManager.AppSettings["JDate_AllAccessUser1_Password"];
                string user1_Username = System.Configuration.ConfigurationManager.AppSettings["JDate_AllAccessUser1_Username"];

                // login as user 1 (an all access user)            
                home = loggingInAndOut.LogIn_ProfileComplete(driver, log, user1_login, user1_password);

                #region Reinitialize User 2's state: get rid of any existing All Access emails whether they've been read or not
                messages = home.Header.InboxHeaderLink_Click();

                // get rid of overlay if it exists
                if (messages.DoesYouRecievedAnAllAccessEMailOverlayExist())
                {
                    messagePage = messages.ReadNowButton_Click();
                    messages = messagePage.BackToMessagesLink_Click();

                    messages.AllAccessEmailSelectAllCheckboxes_Check();
                    messages.DeleteLink_Click();
                }                
                #endregion

                Framework.Site_Pages.QuickSearch quickSearch = messages.Header.SearchHeaderLink_Click();
                               
                // user 1 - search for user 2 (a registered user)              
                Framework.Site_Pages.LookUpMember lookUpMember = quickSearch.Header.LookUpMemberSubHeaderLink_Click();

                lookUpMember.MemberNumberTextbox = user2_MemberNumber;
                Framework.Site_Pages.YourProfile yourProfile = lookUpMember.MemberNumberSearchButton_Click();

                // user 1 - message user 2          
                string subject = "allaccess to reg " + DateTime.Now.ToString();
                string message = "Validate EMail All Access Use To Registered User test case Message timestamp: " + DateTime.Now.ToString();

                Framework.Site_Pages.ComposeMessage composeMessage = yourProfile.EmailMeNowButton_Click();
                composeMessage.SubjectTextbox = subject;
                composeMessage.MessageTextarea = message;
                composeMessage.SendAsAllAccessEmailCheckbox_Check();
                Framework.Site_Pages.MessageSent messageSent = composeMessage.SendButton_Click_AllAccessChecked();

                // user 1 - verify that the "Your AllAccess Message has been sent" text appears
                Assert.IsTrue(messageSent.VerificationMessage().Contains("Your AllAccess Message has been sent"), "In the EMail_AllAccessUserToRegisteredUser test case, after All Access user1 emails Registered user2 the 'Your AllAccess Message has been sent' message did not appear");

                // log out user 1 and login user 2
                login = loggingInAndOut.LogOut(driver, log);

                home = loggingInAndOut.LogIn_ProfileComplete(driver, log, user2_login, user2_password);

                // user 2 - go to the Inbox and verify that the All Access message appears
                messages = home.Header.InboxHeaderLink_Click();
                Assert.IsTrue(messages.DoesYouRecievedAnAllAccessEMailOverlayExist(), "In the EMail_AllAccessUserToRegisteredUser test case, after All Access user1 emails Registered user2 and user2 logs in and goes to their Inbox, the 'You've recieved an All Access Email' message does not appear");

                // user 2 - read the message and verify that the subject and message are correct
                messagePage = messages.ReadNowButton_Click();
                string allAccessEmailSubject = messagePage.GetEmailSubject();
                string allAccessEmailMessage = messagePage.GetEmailMessage();
                Assert.IsTrue(subject == allAccessEmailSubject, "In the EMail_AllAccessUserToRegisteredUser test case, after All Access user1 emails Registered user2 and user2 logs in and goes to their Inbox and checks the email, the Subject does not match the subject user1 originally sent.  \r\nOriginal subject: '" + subject + "' \r\nAll Access subject: '" + allAccessEmailSubject + "'");
                Assert.IsTrue(message == allAccessEmailMessage, "In the EMail_AllAccessUserToRegisteredUser test case, after All Access user1 emails Registered user2 and user2 logs in and goes to their Inbox and checks the email, the Message does not match the message user1 originally sent.  \r\nOriginal message: '" + message + "' \r\nAll Access message: '" + allAccessEmailMessage + "'");

                // user 2 - reply back to user1
                string replyMessage = "Validate EMail All Access Use To Registered User test case Message timestamp: " + DateTime.Now.ToString();
                messagePage.MessageTextarea = replyMessage;
                messageSent = messagePage.SendButton_Click_ReplyingToEmail();

                // user 2 - verify that the "Your AllAccess Message has been sent" text appears
                Assert.IsTrue(messageSent.VerificationMessage().Contains("Your AllAccess Message has been sent"), "In the EMail_AllAccessUserToRegisteredUser test case, after All Access user1 emails Registered user2 the 'Your AllAccess Message has been sent' message did not appear");

                messages = messageSent.BackToInboxLink_Click();

                // user 2 - reininitialize - delete all All Access emails and logout
                messages.AllAccessEmailSelectAllCheckboxes_Check();
                messages.DeleteLink_Click();

                // log out user 2 and login user 1 again
                login = loggingInAndOut.LogOut(driver, log);
                home = loggingInAndOut.LogIn_ProfileComplete(driver, log, user1_login, user1_password);

                // user 1 - go to the Inbox and verify that the All Access message appears
                messages = home.Header.InboxHeaderLink_Click();
                Assert.IsTrue(messages.DoesYouRecievedAnAllAccessEMailOverlayExist(), "In the EMail_AllAccessUserToRegisteredUser test case, after All Access user1 emails Registered user2 and user2 logs in and goes to their Inbox, the 'You've recieved an All Access Email' message does not appear");

                // user 1 - read the message and verify that the subject and message are correct
                messagePage = messages.ReadNowButton_Click();
                Assert.IsTrue(messagePage.DoesEmailContainCorrectMessage(replyMessage), "In the EMail_AllAccessUserToRegisteredUser test case, after All Access user1 emails Registered user2 and user2 logs in and goes to their Inbox and checks the email, the Message does not match the message user1 originally sent  \r\nReply message that we are expecting: '" + replyMessage + "' \r\nMessage currently onscreen: '" + messagePage.GetEmailMessage() + "'");

                // user 1 - reininitialize - delete all All Access emails and logout
                messages = messagePage.BackToMessagesLink_Click();
                messages.AllAccessEmailSelectAllCheckboxes_Check();
                messages.DeleteLink_Click();

                loggingInAndOut.LogOut(driver, log);

                TearDown();

                log.Info("END TEST - JDate/Regression/Member/EMail_AllAccessUserToRegisteredUser");
            }
            catch (Exception e)
            {
                CleanupFailedTestcase(e);
                throw;
            }
        }


        //  Since the IM window is flash we cannot verify anything inside the window. We just launch the IM window and verify that the window's URL is correct
        [Test]
        public void Validate_IMWindowAppearsCorrectly()
        {
            string errorLogNotes = string.Empty;

            try
            {
                log.Info("START TEST - JDate/Regression/Subscriber/IMWindowAppearsCorrectly");

                Setup();

                StartTest_UsingJDate();

                string login = System.Configuration.ConfigurationManager.AppSettings["JDate_SubscribedUser1_Email"];
                string password = System.Configuration.ConfigurationManager.AppSettings["JDate_SubscribedUser1_Password"];

                WebdriverAutomation.Tests.ReusableCode.LoggingInAndOut loggingInAndOut = new LoggingInAndOut();
                WebdriverAutomation.Framework.Site_Pages.Home home = loggingInAndOut.LogIn_ProfileComplete(driver, log, login, password);

                WebdriverAutomation.Framework.Site_Pages.MembersOnline membersOnline = home.Header.MembersOnline_Click();

                WebdriverAutomation.Framework.Site_Pages.YourProfile yourProfile = membersOnline.ClickONFirstOnlineMemberInList();

                yourProfile.IM_Click();

                Assert.IsTrue(yourProfile.IMWindowExists(), "In the EMail_AllAccessUserToRegisteredUser test case, after All Access user1 emails Registered user2 and user2 logs in and goes to their Inbox, the 'You've recieved an All Access Email' message does not appear");

                yourProfile.IMWindowClose();

                TearDown();

                log.Info("END TEST - JDate/Regression/Subscriber/IMWindowAppearsCorrectly");
            }
            catch (Exception e)
            {
                CleanupFailedTestcase(e, errorLogNotes);
                throw;
            }
        }


        /// <summary>
        /// We verify that a user can reach the overlay where they can send a flirt correctly.
        /// We don't verify actually sending a flirt because there is a restriction where a user that has already sent a flirt to another user cannot send it over and over again.
        /// </summary>
        [Test]
        public void Validate_FlirtOverlayAppearsCorrectly()
        {
            string errorLogNotes = string.Empty;

            try
            {
                log.Info("START TEST - JDate/Regression/Subscriber/FlirtOverlayAppearsCorrectly");

                Setup();

                StartTest_UsingJDate();

                string user1_login = System.Configuration.ConfigurationManager.AppSettings["JDate_RegisteredUser1_Email"];
                string user1_password = System.Configuration.ConfigurationManager.AppSettings["JDate_RegisteredUser1_Password"];
                string user1_Username = System.Configuration.ConfigurationManager.AppSettings["JDate_RegisteredUser1_Username"];

                string user2_login = System.Configuration.ConfigurationManager.AppSettings["JDate_RegisteredUser2_Email"];
                string user2_password = System.Configuration.ConfigurationManager.AppSettings["JDate_RegisteredUser2_Password"];
                string user2_MemberID = System.Configuration.ConfigurationManager.AppSettings["JDate_RegisteredUser2_MemberID"];

                // login as user 1 (a subscribed user)
                WebdriverAutomation.Tests.ReusableCode.LoggingInAndOut loggingInAndOut = new LoggingInAndOut();
                WebdriverAutomation.Framework.Site_Pages.Home home = loggingInAndOut.LogIn_ProfileComplete(driver, log, user1_login, user1_password);
                Framework.Site_Pages.QuickSearch quickSearch = home.Header.SearchHeaderLink_Click();

                // user 1 - search for user 2 (also a subscribed user)
                Framework.Site_Pages.LookUpMember lookUpMember = quickSearch.Header.LookUpMemberSubHeaderLink_Click();

                lookUpMember.MemberNumberTextbox = user2_MemberID;
                Framework.Site_Pages.YourProfile yourProfile = lookUpMember.MemberNumberSearchButton_Click();

                yourProfile.Flirt_Click();

                Assert.IsTrue(yourProfile.FlirtOverlayExists(), "In the FlirtOverlayAppearsCorrectly test case, after user 1 navigates to user 2's Profile Page and clicks on the Flirt button the Send a Flirt Page does not appear");

                TearDown();

                log.Info("END TEST - JDate/Regression/Subscriber/FlirtOverlayAppearsCorrectly");
            }
            catch (Exception e)
            {
                CleanupFailedTestcase(e, errorLogNotes);
                throw;
            }
        }


        [Test]
        public void Validate_ECard()
        {
            string errorLogNotes = string.Empty;

            try
            {
                log.Info("START TEST - JDate/Regression/Subscriber/ECard");

                Setup();

                StartTest_UsingJDate();

                string user1_login = System.Configuration.ConfigurationManager.AppSettings["JDate_RegisteredUser1_Email"];
                string user1_password = System.Configuration.ConfigurationManager.AppSettings["JDate_RegisteredUser1_Password"];
                string user1_Username = System.Configuration.ConfigurationManager.AppSettings["JDate_RegisteredUser1_Username"];

                string user2_login = System.Configuration.ConfigurationManager.AppSettings["JDate_SubscribedUser1_Email"];
                string user2_password = System.Configuration.ConfigurationManager.AppSettings["JDate_SubscribedUser1_Password"];
                string user2_MemberID = System.Configuration.ConfigurationManager.AppSettings["JDate_SubscribedUser1_MemberID"];

                // login as user 1 (a registered user)
                WebdriverAutomation.Tests.ReusableCode.LoggingInAndOut loggingInAndOut = new LoggingInAndOut();
                WebdriverAutomation.Framework.Site_Pages.Home home = loggingInAndOut.LogIn_ProfileComplete(driver, log, user1_login, user1_password);
                Framework.Site_Pages.QuickSearch quickSearch = home.Header.SearchHeaderLink_Click();

                // user 1 - search for user 2 (a subscribed user)
                Framework.Site_Pages.LookUpMember lookUpMember = quickSearch.Header.LookUpMemberSubHeaderLink_Click();

                lookUpMember.MemberNumberTextbox = user2_MemberID;
                Framework.Site_Pages.YourProfile yourProfile = lookUpMember.MemberNumberSearchButton_Click();
                
                Framework.Site_Pages.ECards eCards = yourProfile.ECard_Click();

                eCards.MostPopularECard_ClickFirstCard();

                Framework.Site_Pages.EditECard editECard = eCards.PersonalizeAndSend_Click();

                editECard.ToTextbox_Write(user2_MemberID);
                eCards = editECard.SendECard_Click();

                //// user 1 - verify that the "The Card was sent to *user*" text appears.  update: we don't verify this anymore since after talking with Ray we realized this doesn't always appear and is a bug that   
                ////    might not ever get fixed.
                //Assert.IsTrue(eCards.VerificationMessage().Contains("The Card was sent to"), "In the ECards test case, after user1 sends an E-Card to Subscribed user2 the 'The Card was sent to *user*' message did not appear");

                // log out user 1 and login user 2
                WebdriverAutomation.Framework.Site_Pages.Login login = loggingInAndOut.LogOut(driver, log);

                home = loggingInAndOut.LogIn_ProfileComplete(driver, log, user2_login, user2_password);

                // user 2 - go to the Inbox and verify that the Ecard appears (if more than one Ecard is sent from the same user they will all appear until they are deleted at the end of this test case)
                Framework.Site_Pages.Messages messages = home.Header.InboxHeaderLink_Click();

                Assert.IsTrue(messages.DoesEmailExist(user1_Username, "Ecard"), "In the ECards test case, after user1 sends an E-Card to Subscribed user2 the ECard did not appear in user 2's Messages Page Inbox");

                //Framework.Site_Pages.ECard eCard = messages.ECardLink_Click(user1_Username, "Ecard");

                //// we cannot verify the look and feel of the ECard but we can verify that the opened ECard is sent from the correct user
                //Assert.IsTrue(eCard.IsCardFromCorrectUser(user1_Username), "In the ECards test case, after user1 sends an E-Card to Subscribed user2 and user 2 opened it, the ECard should show user 1 as the sender but does not.");

                //// delete all messages from Inbox
                //messages = eCard.Header.InboxHeaderLink_Click();

                messages.EmailSelectAllCheckboxes_Check();
                messages.DeleteLink_Click();

                loggingInAndOut.LogOut(driver, log);

                TearDown();

                log.Info("END TEST - JDate/Regression/Subscriber/ECard");
            }
            catch (Exception e)
            {
                CleanupFailedTestcase(e, errorLogNotes);
                throw;
            }
        }
        
        [Test]
        public void Validate_YourAccountLinks()
        {
            try
            {
                log.Info("START TEST - JDate/Regression/Member/YourAccountLinks");

                Setup();

                StartTest_UsingJDate();

                string login = System.Configuration.ConfigurationManager.AppSettings["JDate_AllAccessUser1_Email"];
                string password = System.Configuration.ConfigurationManager.AppSettings["JDate_AllAccessUser1_Password"];

                string yourAccountPageURL = string.Empty;

                if (driver.Url.Contains("preprod"))
                    yourAccountPageURL = @"http://preprod.jdate.com/Applications/MemberServices/MemberServices.aspx?NavPoint=top";
                else
                    yourAccountPageURL = @"http://www.jdate.com/Applications/MemberServices/MemberServices.aspx?NavPoint=top";

                WebdriverAutomation.Tests.ReusableCode.LoggingInAndOut loggingInAndOut = new LoggingInAndOut();
                WebdriverAutomation.Framework.Site_Pages.Home home = loggingInAndOut.LogIn_ProfileComplete(driver, log, login, password);

                WebdriverAutomation.Framework.Site_Pages.YourAccount yourAccount = home.Header.YourAccountLink_Click();

                WebdriverAutomation.Framework.Site_Pages.ProfileDisplaySettings profileDisplaySettings = yourAccount.ProfileDisplaySettingsLink_Click();
                yourAccount = profileDisplaySettings.Header.YourAccountLink_Click();

                WebdriverAutomation.Framework.Site_Pages.YourProfile profile = yourAccount.ChangeYourProfileLink_Click();
                yourAccount = profileDisplaySettings.Header.YourAccountLink_Click();

                WebdriverAutomation.Framework.Site_Pages.ChangeYourEmailOrPassword changeYourEmailOrPassword = yourAccount.ChangeYourEmailOrPasswordLink_Click();
                yourAccount = profileDisplaySettings.Header.YourAccountLink_Click();

                WebdriverAutomation.Framework.Site_Pages.ColorCodeSettings colorCodeSettings = yourAccount.ColorCodeSettingsLink_Click();
                yourAccount = profileDisplaySettings.Header.YourAccountLink_Click();

                WebdriverAutomation.Framework.Site_Pages.Subscribe subscribe = yourAccount.MembershipPlansAndCostsLink_Click();
                driver.Navigate().GoToUrl(yourAccountPageURL);

                WebdriverAutomation.Framework.Site_Pages.AutoRenewalSettings autoRenewalSettings = yourAccount.AutoRenewalSettingsLink_Click();
                yourAccount = profileDisplaySettings.Header.YourAccountLink_Click();

                subscribe = yourAccount.SubscribeLink_Click();
                driver.Navigate().GoToUrl(yourAccountPageURL);

                WebdriverAutomation.Framework.Site_Pages.BuyOneGiveOneFreeSettings buyOneGiveOneFreeSettings = yourAccount.BuyOneGiveOneFreeSettingsLink_Click();
                driver.Navigate().GoToUrl(yourAccountPageURL);

                WebdriverAutomation.Framework.Site_Pages.GetMoreAttentionByAddingPremiumServices premiumServices = yourAccount.PremiumServicesLink_Click();
                driver.Navigate().GoToUrl(yourAccountPageURL);

                //// the Premium Service Settings link is commented out for now.  the Premium Service Settings Page cannot be reached without having a subscribed account,
                ////    but due to David's script which deletes accounts that have been subscribed we are currently not testing this
                //WebdriverAutomation.Framework.Site_Pages.PremiumServiceSettings premiumServiceSettings = yourAccount.PremiumServiceSettingsLink_Click();
                //yourAccount = profileDisplaySettings.Header.YourAccountLink_Click();

                WebdriverAutomation.Framework.Site_Pages.RemoveMyProfile removeMyProfile = yourAccount.RemoveMyProfileLink_Click();
                yourAccount = profileDisplaySettings.Header.YourAccountLink_Click();

                WebdriverAutomation.Framework.Site_Pages.AccountHistory accountHistory = yourAccount.AccountInformationLink_Click();
                yourAccount = profileDisplaySettings.Header.YourAccountLink_Click();

                WebdriverAutomation.Framework.Site_Pages.BillingInformation billingInformation = yourAccount.BillingInformationLink_Click();
                driver.Navigate().GoToUrl(yourAccountPageURL);

                WebdriverAutomation.Framework.Site_Pages.VerifyYourEmailAddress verifyEmail = yourAccount.VerifyEmailLink_Click();
                yourAccount = profileDisplaySettings.Header.YourAccountLink_Click();

                WebdriverAutomation.Framework.Site_Pages.Settings chatAndIMSettings = yourAccount.ChatAndIMSettingsLink_Click();
                driver.Navigate().GoToUrl(yourAccountPageURL);

                WebdriverAutomation.Framework.Site_Pages.MessageSettings messageSettings = yourAccount.MessageSettingsLink_Click();
                yourAccount = profileDisplaySettings.Header.YourAccountLink_Click();

                messageSettings = yourAccount.OffSiteMessageSettingsLink_Click();
                yourAccount = profileDisplaySettings.Header.YourAccountLink_Click();

                WebdriverAutomation.Framework.Site_Pages.WhosCheckingYouOut whosCheckingYouOut = yourAccount.MembersWhoEmailedYouLink_Click();
                yourAccount = profileDisplaySettings.Header.YourAccountLink_Click();

                WebdriverAutomation.Framework.Site_Pages.ContactingMembers contactingMembers = yourAccount.NeverMissEmailsLink_Click();
                yourAccount = profileDisplaySettings.Header.YourAccountLink_Click();

                whosCheckingYouOut = yourAccount.MembersBlockedFromContactingYouLink_Click();
                yourAccount = profileDisplaySettings.Header.YourAccountLink_Click();

                WebdriverAutomation.Framework.Site_Pages.YoureCheckingOut youreCheckingOut = yourAccount.MembersBlockedFromAppearingInYourSearchResultsLink_Click();
                yourAccount = profileDisplaySettings.Header.YourAccountLink_Click();

                whosCheckingYouOut = yourAccount.MembersBlockedFromViewingYourProfileInHisHerSearchResults_Click();
                yourAccount = profileDisplaySettings.Header.YourAccountLink_Click();

                WebdriverAutomation.Framework.Site_Pages.FrequentlyAskedQuestions faq = yourAccount.FAQLink_Click();
                yourAccount = profileDisplaySettings.Header.YourAccountLink_Click();

                WebdriverAutomation.Framework.Site_Pages.ContactUs contactUs = yourAccount.ContactUsLink_Click();
                yourAccount = profileDisplaySettings.Header.YourAccountLink_Click();

                WebdriverAutomation.Framework.Site_Pages.Privacy privacyStatement = yourAccount.PrivacyStatementLink_Click();
                yourAccount = profileDisplaySettings.Header.YourAccountLink_Click();

                privacyStatement = yourAccount.TermsAndConditionsOfServiceLink_Click();
                yourAccount = profileDisplaySettings.Header.YourAccountLink_Click();

                privacyStatement = yourAccount.TermsAndConditionsOfPurchaseLink_Click();
                yourAccount = profileDisplaySettings.Header.YourAccountLink_Click();

                TearDown();

                log.Info("END TEST - JDate/Regression/Member/YourAccountLinks");
            }
            catch (Exception e)
            {
                CleanupFailedTestcase(e);
                throw;
            }
        }        
    }
}
