﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.Events;
using NUnit.Framework;
using log4net;
using WebdriverAutomation.Tests.ReusableCode;

namespace WebdriverAutomation.Tests.JDate.Regression
{
    [TestFixture]
    public class Member : BaseTest
    {

        [Test]
        public void Validate_Registration()
        {
            string errorLogNotes = string.Empty;

            try
            {
                log.Info("START TEST - JDate/Regression/Member/Registration");

                Setup();

                StartTest_UsingJDate_WithDummyPRM();                             

                RegisterNewUser();

                //Remove the user we just created
                WebdriverAutomation.Tests.ReusableCode.AccountSettings accountSettings = new AccountSettings();
                accountSettings.RemoveProfile_SkippingUpdateProfileAndImportantNoticePages(driver, log);

                TearDown();

                log.Info("END TEST - JDate/Regression/Member/Registration");
            }
            catch (Exception e)
            {
                CleanupFailedTestcase(e, errorLogNotes);
                throw;
            }
        }


        [Test]
        public void Validate_OmnitureExistsInLandingAndRegConfirmationPages()
        {
            string errorLogNotes = string.Empty;

            try
            {
                log.Info("START TEST - JDate/Regression/Member/OmnitureExistsInLandingAndRegConfirmationPages");

                Setup();

                StartTest_UsingJDate_WithDummyPRM();

                WebdriverAutomation.Framework.Site_Pages.Splash splash = new WebdriverAutomation.Framework.Site_Pages.Splash(driver);

                string omnitureSearchQuery1 = "s.prop";
                string omnitureSearchQuery2 = "s.eVar";

                if (!driver.PageSource.Contains(omnitureSearchQuery1) || !driver.PageSource.Contains(omnitureSearchQuery2))
                    Assert.Fail("In the Landing Page we are unable to find any omniture variables beginning with 's.prop' or 's.eVar' within the page source.");

                RegisterNewUser();

                if (!driver.PageSource.Contains(omnitureSearchQuery1) || !driver.PageSource.Contains(omnitureSearchQuery2))
                    Assert.Fail("In the Registration Confirmation Page we are unable to find any omniture variables beginning with 's.prop' or 's.eVar' within the page source.");

                //Remove the user we just created
                WebdriverAutomation.Tests.ReusableCode.AccountSettings accountSettings = new AccountSettings();
                accountSettings.RemoveProfile_SkippingUpdateProfileAndImportantNoticePages(driver, log);

                TearDown();

                log.Info("END TEST - JDate/Regression/Member/OmnitureExistsInLandingAndRegConfirmationPages");
            }
            catch (Exception e)
            {
                CleanupFailedTestcase(e, errorLogNotes);
                throw;
            }
        }


        [Test]
        public void Validate_EditProfile_InMyOwnWordsTab()
        {
            try
            {
                log.Info("START TEST - JDate/Regression/Member/EditProfile_InMyOwnWordsTab");

                Setup();

                StartTest_UsingJDate();

                string login = System.Configuration.ConfigurationManager.AppSettings["JDate_RegisteredUser1_Email"];
                string password = System.Configuration.ConfigurationManager.AppSettings["JDate_RegisteredUser1_Password"];

                WebdriverAutomation.Tests.ReusableCode.LoggingInAndOut loggingInAndOut = new LoggingInAndOut();
                WebdriverAutomation.Framework.Site_Pages.Home home = loggingInAndOut.LogIn_ProfileComplete(driver, log, login, password);
                WebdriverAutomation.Framework.Site_Pages.YourProfile profile = home.Header.YourProfileHeaderLink_Click();

                string new_AboutMe = "This is a test account operated by an employee of Spark Networks. Testing changes.";
                string new_MyLifeAndAmbitions = "I always want the big piece of chicken";
                string new_ABriefHistoryOfMyLife = "Born in NYC. Raised in SF. Living in LA.";
                string new_MyPerfectFirstDate = "My perfect first date is with someone who really likes automation. Someone I can talk about code with.";
                string new_OnOurFirstDateRemindMeToTellYouTheStoryAbout = "The time we parachuted into Burning Man";
                string new_TheThingsICouldNeverLiveWithout = "My phone and lasagna";
                string new_MyFavoriteBooksMoviesTVShowsMusicAndFood = "Some of that oontz oontz";
                string new_TheCoolestPlacesIveVisited = "Thom Yorke's house";
                string new_ForFunILikeTo = "Sleep all day";
                string new_OnFridayAndSaturdayNightsITypically = "Am watching a show";
                string new_ImLookingFor = "I am looking for someone who likes dance, music, the arts, travel and having a good time. Spontanaeity helps.";
                string new_MyIdealRelationship = "My ideal relationship would be with someone who likes to rock climb. Because I like to rock climb too.";
                string new_MyPastRelationships = "I would love to talk about my past but it shouldn't be done here. Ask me in person and I will tell you.";
                string new_YouShouldDefinitelyMessageMeIfYou = "Are not crazy";

                // Overwrite new values to the page
                profile.AboutMe = new_AboutMe;
                profile.MyLifeAndAmbitions = new_MyLifeAndAmbitions;
                profile.ABriefHistoryOfMyLife = new_ABriefHistoryOfMyLife;
                profile.MyPerfectFirstDate = new_MyPerfectFirstDate;
                profile.OnOurFirstDateRemindMeToTellYouTheStoryAbout = new_OnOurFirstDateRemindMeToTellYouTheStoryAbout;
                profile.TheThingsICouldNeverLiveWithout = new_TheThingsICouldNeverLiveWithout;
                profile.MyFavoriteBooksMoviesTVShowsMusicAndFood = new_MyFavoriteBooksMoviesTVShowsMusicAndFood;
                profile.TheCoolestPlacesIveVisited = new_TheCoolestPlacesIveVisited;
                profile.ForFunILikeTo = new_ForFunILikeTo;
                profile.OnFridayAndSaturdayNightsITypically = new_OnFridayAndSaturdayNightsITypically;
                profile.ImLookingFor = new_ImLookingFor;
                profile.MyIdealRelationship = new_MyIdealRelationship;
                profile.MyPastRelationships = new_MyPastRelationships;
                profile.YouShouldDefinitelyMessageMeIfYou = new_YouShouldDefinitelyMessageMeIfYou;

                // Go to the Home Page and then come back to the Profile Page to verify the values saved correctly
                home = profile.Header.HomeHeaderLink_Click();
                profile = home.Header.YourProfileHeaderLink_Click();

                string stringToVerifyOnProfilePage = string.Empty;

                stringToVerifyOnProfilePage = profile.AboutMe;
                Assert.IsTrue(new_AboutMe == stringToVerifyOnProfilePage, "In the Validate_EditProfile_InMyOwnWordsTab test case, after saving new values for 'About me' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_AboutMe + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.MyLifeAndAmbitions;
                Assert.IsTrue(new_MyLifeAndAmbitions == stringToVerifyOnProfilePage, "In the Validate_EditProfile_InMyOwnWordsTab test case, after saving new values for 'My life and ambitions' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_MyLifeAndAmbitions + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.ABriefHistoryOfMyLife;
                Assert.IsTrue(new_ABriefHistoryOfMyLife == stringToVerifyOnProfilePage, "In the Validate_EditProfile_InMyOwnWordsTab test case, after saving new values for 'A brief history of my life' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_ABriefHistoryOfMyLife + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.MyPerfectFirstDate;
                Assert.IsTrue(new_MyPerfectFirstDate == stringToVerifyOnProfilePage, "In the Validate_EditProfile_MoreAboutMeTab test case, after saving new values for 'My perfect first date' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_MyPerfectFirstDate + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.OnOurFirstDateRemindMeToTellYouTheStoryAbout;
                Assert.IsTrue(new_OnOurFirstDateRemindMeToTellYouTheStoryAbout == stringToVerifyOnProfilePage, "In the Validate_EditProfile_InMyOwnWordsTab test case, after saving new values for 'On our first date, remind me to tell you the story about...' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_OnOurFirstDateRemindMeToTellYouTheStoryAbout + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.TheThingsICouldNeverLiveWithout;
                Assert.IsTrue(new_TheThingsICouldNeverLiveWithout == stringToVerifyOnProfilePage, "In the Validate_EditProfile_InMyOwnWordsTab test case, after saving new values for 'The things I could never live without' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_TheThingsICouldNeverLiveWithout + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.MyFavoriteBooksMoviesTVShowsMusicAndFood;
                Assert.IsTrue(new_MyFavoriteBooksMoviesTVShowsMusicAndFood == stringToVerifyOnProfilePage, "In the Validate_EditProfile_InMyOwnWordsTab test case, after saving new values for 'My favorite books, movies, TV shows, music and food' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_MyFavoriteBooksMoviesTVShowsMusicAndFood + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.TheCoolestPlacesIveVisited;
                Assert.IsTrue(new_TheCoolestPlacesIveVisited == stringToVerifyOnProfilePage, "In the Validate_EditProfile_InMyOwnWordsTab test case, after saving new values for 'The coolest places I've visited' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_TheCoolestPlacesIveVisited + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.ForFunILikeTo;
                Assert.IsTrue(new_ForFunILikeTo == stringToVerifyOnProfilePage, "In the Validate_EditProfile_InMyOwnWordsTab test case, after saving new values for 'For fun, I like to...' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_ForFunILikeTo + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.OnFridayAndSaturdayNightsITypically;
                Assert.IsTrue(new_OnFridayAndSaturdayNightsITypically == stringToVerifyOnProfilePage, "In the Validate_EditProfile_InMyOwnWordsTab test case, after saving new values for 'On Friday and Saturday nights I typically...' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_OnFridayAndSaturdayNightsITypically + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.ImLookingFor;
                Assert.IsTrue(new_ImLookingFor == stringToVerifyOnProfilePage, "In the Validate_EditProfile_InMyOwnWordsTab test case, after saving new values for 'I'm looking for...' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_ImLookingFor + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.MyIdealRelationship;
                Assert.IsTrue(new_MyIdealRelationship == stringToVerifyOnProfilePage, "In the Validate_EditProfile_MoreAboutMeTab test case, after saving new values for 'My ideal relationship' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_MyIdealRelationship + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.MyPastRelationships;
                Assert.IsTrue(new_MyPastRelationships == stringToVerifyOnProfilePage, "In the Validate_EditProfile_MoreAboutMeTab test case, after saving new values for 'My past relationships' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_MyPastRelationships + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.YouShouldDefinitelyMessageMeIfYou;
                Assert.IsTrue(new_YouShouldDefinitelyMessageMeIfYou == stringToVerifyOnProfilePage, "In the Validate_EditProfile_InMyOwnWordsTab test case, after saving new values for 'You should definitely message me if you...' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_YouShouldDefinitelyMessageMeIfYou + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                // reinitialize to original values
                profile.AboutMe = "This is a test account operated by an employee of Spark Networks.";
                profile.MyLifeAndAmbitions = "I want to win the lottery";
                profile.ABriefHistoryOfMyLife = "Sex drugs and rock and roll";
                profile.MyPerfectFirstDate = "My perfect first date is going to dinner then a club for drinks and dancing. Lots of drinks. Don't judge me.";
                profile.OnOurFirstDateRemindMeToTellYouTheStoryAbout = "My trip to Ibiza";
                profile.TheThingsICouldNeverLiveWithout = "The 16 switches in my car";
                profile.MyFavoriteBooksMoviesTVShowsMusicAndFood = "Game of Thrones FTW";
                profile.TheCoolestPlacesIveVisited = "Paris";
                profile.ForFunILikeTo = "Drink hot sauce straight from the bottle";
                profile.OnFridayAndSaturdayNightsITypically = "Am beached on my couch";
                profile.ImLookingFor = "Someone outgoing who can do outdoor activities with me like camping and skydiving and fishing and stuff.";
                profile.MyIdealRelationship = "My ideal relationship would be with someone who likes to party. Someone who can hang all night long.";
                profile.MyPastRelationships = "I have had tons of past relationships. I don't like talking about my past on here though. More in person.";
                profile.YouShouldDefinitelyMessageMeIfYou = "Like turtles.";

                profile.Header.Logout_Click();

                TearDown();

                log.Info("END TEST - JDate/Regression/Member/EditProfile_InMyOwnWordsTab");
            }
            catch (Exception e)
            {
                CleanupFailedTestcase(e);
                throw;
            }
        }

        [Test]
        public void Validate_EditProfile_MoreAboutMeTab()
        {
            try
            {
                log.Info("START TEST - JDate/Regression/Member/EditProfile_MoreAboutMeTab");

                Setup();

                StartTest_UsingJDate();

                string login = System.Configuration.ConfigurationManager.AppSettings["JDate_RegisteredUser1_Email"];
                string password = System.Configuration.ConfigurationManager.AppSettings["JDate_RegisteredUser1_Password"];

                WebdriverAutomation.Tests.ReusableCode.LoggingInAndOut loggingInAndOut = new LoggingInAndOut();
                WebdriverAutomation.Framework.Site_Pages.Home home = loggingInAndOut.LogIn_ProfileComplete(driver, log, login, password);
                WebdriverAutomation.Framework.Site_Pages.YourProfile profile = home.Header.YourProfileHeaderLink_Click();

                List<string> new_MyPersonalityIsBestDescribedAs = new List<string>();
                new_MyPersonalityIsBestDescribedAs.Add("Artistic");
                new_MyPersonalityIsBestDescribedAs.Add("Talkative");

                List<string> new_InMyFreeTimeIEnjoy = new List<string>();
                new_InMyFreeTimeIEnjoy.Add("Dining Out");
                new_InMyFreeTimeIEnjoy.Add("Shopping");

                List<string> new_InMyFreeTimeILike = new List<string>();
                new_InMyFreeTimeILike.Add("Concerts");
                new_InMyFreeTimeILike.Add("Restaurants");

                List<string> new_MyFavoritePhysicalActivites = new List<string>();
                new_MyFavoritePhysicalActivites.Add("Aerobics");
                new_MyFavoritePhysicalActivites.Add("Martial Arts");

                List<string> new_MyFavoriteFoods = new List<string>();
                new_MyFavoriteFoods.Add("Continental");
                new_MyFavoriteFoods.Add("Seafood");

                List<string> new_MyFavoriteMusic = new List<string>();
                new_MyFavoriteMusic.Add("Modern Rock n' Roll");
                new_MyFavoriteMusic.Add("Punk");

                List<string> new_ILikeToRead = new List<string>();
                new_ILikeToRead.Add("Fiction");
                new_ILikeToRead.Add("Poetry");

                profile.MoreAboutMeTab_Click();

                // Overwrite new values to the page - the hoverclick is extremely fickle here especially so reload Profile Page if it doesn't work
                int loopMax = 5;

                for (int i = 0; i < 5; i++)
                {
                    try
                    {
                        profile.MyPersonalityIsBestDescribedAs = new_MyPersonalityIsBestDescribedAs;
                        profile.InMyFreeTimeIEnjoy_JDatePage = new_InMyFreeTimeIEnjoy;
                        profile.InMyFreeTimeILike = new_InMyFreeTimeILike;
                        profile.MyFavoritePhysicalActivities_JDatePage = new_MyFavoritePhysicalActivites;
                        profile.MyFavoriteFoods_JDatePage = new_MyFavoriteFoods;
                        profile.MyFavoriteMusic_JDatePage = new_MyFavoriteMusic;
                        profile.ILikeToRead = new_ILikeToRead;

                        break;
                    }
                    catch
                    {
                        profile = profile.Header.YourProfileHeaderLink_Click();

                        profile.MoreAboutMeTab_Click();
                    }

                    if (i == loopMax - 1)
                        Assert.Fail("Errors are occurring while trying to hover over certain areas of the In My Own Words section of the Profile Page to change some values.  We failed after trying " + loopMax + " times unsuccessfully.");
                }

                // Go to the Home Page and then come back to the Profile Page to verify the values saved correctly
                home = profile.Header.HomeHeaderLink_Click();
                profile = home.Header.YourProfileHeaderLink_Click();

                profile.MoreAboutMeTab_Click();

                string stringToVerifyOnProfilePage = string.Empty;
                List<string> listToVerifyOnProfilePage = new List<string>();

                listToVerifyOnProfilePage = profile.MyPersonalityIsBestDescribedAs;
                new_MyPersonalityIsBestDescribedAs.Sort();
                Assert.IsTrue(AreValuesAndCountEqual(new_MyPersonalityIsBestDescribedAs, listToVerifyOnProfilePage), "In the Validate_EditProfile_MoreAboutMeTab test case, after saving new values for 'My personality is best described as...' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + string.Join(", ", new_MyPersonalityIsBestDescribedAs.ToArray()) + "' \r\nValue currently in Profile Page: '" + string.Join(", ", listToVerifyOnProfilePage.ToArray()) + "'");

                listToVerifyOnProfilePage = profile.InMyFreeTimeIEnjoy_JDatePage;
                new_InMyFreeTimeIEnjoy.Sort();
                Assert.IsTrue(AreValuesAndCountEqual(new_InMyFreeTimeIEnjoy, listToVerifyOnProfilePage), "In the Validate_EditProfile_MoreAboutMeTab test case, after saving new values for 'In my spare time, I enjoy...' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + string.Join(", ", new_InMyFreeTimeIEnjoy.ToArray()) + "' \r\nValue currently in Profile Page: '" + string.Join(", ", listToVerifyOnProfilePage.ToArray()) + "'");

                listToVerifyOnProfilePage = profile.InMyFreeTimeILike;
                new_InMyFreeTimeILike.Sort();
                Assert.IsTrue(AreValuesAndCountEqual(new_InMyFreeTimeILike, listToVerifyOnProfilePage), "In the Validate_EditProfile_MoreAboutMeTab test case, after saving new values for 'In my free time, I like...' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + string.Join(", ", new_InMyFreeTimeILike.ToArray()) + "' \r\nValue currently in Profile Page: '" + string.Join(", ", listToVerifyOnProfilePage.ToArray()) + "'");

                listToVerifyOnProfilePage = profile.MyFavoritePhysicalActivities_JDatePage;
                new_MyFavoritePhysicalActivites.Sort();
                Assert.IsTrue(AreValuesAndCountEqual(new_MyFavoritePhysicalActivites, listToVerifyOnProfilePage), "In the Validate_EditProfile_MoreAboutMeTab test case, after saving new values for 'My favorite physical activities' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + string.Join(", ", new_MyFavoritePhysicalActivites.ToArray()) + "' \r\nValue currently in Profile Page: '" + string.Join(", ", listToVerifyOnProfilePage.ToArray()) + "'");

                listToVerifyOnProfilePage = profile.MyFavoriteFoods_JDatePage;
                new_MyFavoriteFoods.Sort();
                Assert.IsTrue(AreValuesAndCountEqual(new_MyFavoriteFoods, listToVerifyOnProfilePage), "In the Validate_EditProfile_MoreAboutMeTab test case, after saving new values for 'My favorite food(s)' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + string.Join(", ", new_MyFavoriteFoods.ToArray()) + "' \r\nValue currently in Profile Page: '" + string.Join(", ", listToVerifyOnProfilePage.ToArray()) + "'");

                listToVerifyOnProfilePage = profile.MyFavoriteMusic_JDatePage;
                new_MyFavoriteMusic.Sort();
                Assert.IsTrue(AreValuesAndCountEqual(new_MyFavoriteMusic, listToVerifyOnProfilePage), "In the Validate_EditProfile_MoreAboutMeTab test case, after saving new values for 'My favorite music' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + string.Join(", ", new_MyFavoriteMusic.ToArray()) + "' \r\nValue currently in Profile Page: '" + string.Join(", ", listToVerifyOnProfilePage.ToArray()) + "'");

                listToVerifyOnProfilePage = profile.ILikeToRead;
                new_ILikeToRead.Sort();
                Assert.IsTrue(AreValuesAndCountEqual(new_ILikeToRead, listToVerifyOnProfilePage), "In the Validate_EditProfile_MoreAboutMeTab test case, after saving new values for 'I like to read' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + string.Join(", ", new_ILikeToRead.ToArray()) + "' \r\nValue currently in Profile Page: '" + string.Join(", ", listToVerifyOnProfilePage.ToArray()) + "'");

                // reinitialize to original values
                for (int i = 0; i < 5; i++)
                {
                    try
                    {
                        List<string> orig_MyPersonalityIsBestDescribedAs = new List<string>();
                        orig_MyPersonalityIsBestDescribedAs.Add("Eccentric");
                        orig_MyPersonalityIsBestDescribedAs.Add("Outgoing");

                        profile.MyPersonalityIsBestDescribedAs = orig_MyPersonalityIsBestDescribedAs;

                        List<string> orig_InMyFreeTimeIEnjoy = new List<string>();
                        orig_InMyFreeTimeIEnjoy.Add("Investing");
                        orig_InMyFreeTimeIEnjoy.Add("Wine Tasting");

                        profile.InMyFreeTimeIEnjoy_JDatePage = orig_InMyFreeTimeIEnjoy;

                        List<string> orig_InMyFreeTimeILike = new List<string>();
                        orig_InMyFreeTimeILike.Add("Art Galleries");
                        orig_InMyFreeTimeILike.Add("Raves/Underground Parties");

                        profile.InMyFreeTimeILike = orig_InMyFreeTimeILike;

                        List<string> orig_MyFavoritePhysicalActivites = new List<string>();
                        orig_MyFavoritePhysicalActivites.Add("Cricket");
                        orig_MyFavoritePhysicalActivites.Add("Soccer");

                        profile.MyFavoritePhysicalActivities_JDatePage = orig_MyFavoritePhysicalActivites;

                        List<string> orig_MyFavoriteFoods = new List<string>();
                        orig_MyFavoriteFoods.Add("Italian");
                        orig_MyFavoriteFoods.Add("Soul Food");

                        profile.MyFavoriteFoods_JDatePage = orig_MyFavoriteFoods;

                        List<string> orig_MyFavoriteMusic = new List<string>();
                        orig_MyFavoriteMusic.Add("Dance/Electronica");
                        orig_MyFavoriteMusic.Add("Indie");

                        profile.MyFavoriteMusic_JDatePage = orig_MyFavoriteMusic;

                        List<string> orig_ILikeToRead = new List<string>();
                        orig_ILikeToRead.Add("Non-Fiction");
                        orig_ILikeToRead.Add("Comics");

                        profile.ILikeToRead = orig_ILikeToRead;


                        break;
                    }
                    catch
                    {
                        profile = profile.Header.YourProfileHeaderLink_Click();

                        profile.MoreAboutMeTab_Click();
                    }

                    if (i == loopMax - 1)
                        Assert.Fail("Errors are occurring while trying to hover over certain areas of the In My Own Words section of the Profile Page to change some values.  We failed after trying " + loopMax + " times unsuccessfully.");
                }

                profile.Header.Logout_Click();

                TearDown();

                log.Info("END TEST - JDate/Regression/Member/EditProfile_MoreAboutMeTab");
            }
            catch (Exception e)
            {
                CleanupFailedTestcase(e);
                throw;
            }
        }

        [Test]
        public void Validate_EditProfile_DetailsPhysicalInfo()
        {
            try
            {
                log.Info("START TEST - JDate/Regression/Member/EditProfile_DetailsPhysicalInfo");

                Setup();

                StartTest_UsingJDate();

                string login = System.Configuration.ConfigurationManager.AppSettings["JDate_RegisteredUser1_Email"];
                string password = System.Configuration.ConfigurationManager.AppSettings["JDate_RegisteredUser1_Password"];

                WebdriverAutomation.Tests.ReusableCode.LoggingInAndOut loggingInAndOut = new LoggingInAndOut();
                WebdriverAutomation.Framework.Site_Pages.Home home = loggingInAndOut.LogIn_ProfileComplete(driver, log, login, password);
                WebdriverAutomation.Framework.Site_Pages.YourProfile profile = home.Header.YourProfileHeaderLink_Click();

                string new_height = "4'6\"(137 cm)";
                string new_weight = "168 lbs (76 kg)";
                string new_bodyType = "Soft";
                string new_hairColor = "Red";
                string new_eyeColor = "Green";

                // Overwrite new values to the page
                profile.PhysicalInfo_ClickToEdit();

                profile.Height_InOverlay = new_height;
                profile.Weight_InOverlay = new_weight;
                profile.BodyType_InOverlay = new_bodyType;
                profile.HairColor_InOverlay = new_hairColor;
                profile.EyeColor_InOverlay = new_eyeColor;

                profile.PhysicalInfo_SaveChanges();

                // Go to the Home Page and then come back to the Profile Page to verify the values saved correctly
                home = profile.Header.HomeHeaderLink_Click();
                profile = home.Header.YourProfileHeaderLink_Click();

                string stringToVerifyOnProfilePage = string.Empty;

                stringToVerifyOnProfilePage = profile.Height;
                Assert.IsTrue(new_height.Replace(" ", "") == stringToVerifyOnProfilePage.Replace(" ", ""), "In the Validate_EditProfile_DetailsPhysicalInfo test case, after saving new values for 'Height' in the the Click to Edit overlay the values were not saved properly in the Profile Page \r\nValue we saved in Click to Edit overlay: '" + new_height + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.Weight;
                stringToVerifyOnProfilePage = stringToVerifyOnProfilePage.Replace("Pounds", "lbs");
                Assert.IsTrue(new_weight == stringToVerifyOnProfilePage, "In the Validate_EditProfile_DetailsPhysicalInfo test case, after saving new values for 'Weight' in the the Click to Edit overlay the values were not saved properly in the Profile Page \r\nValue we saved in Click to Edit overlay: '" + new_weight + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.BodyType;
                Assert.IsTrue(new_bodyType == stringToVerifyOnProfilePage, "In the Validate_EditProfile_DetailsPhysicalInfo test case, after saving new values for 'Body Type' in the the Click to Edit overlay the values were not saved properly in the Profile Page \r\nValue we saved in Click to Edit overlay: '" + new_bodyType + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.HairColor;
                Assert.IsTrue(new_hairColor == stringToVerifyOnProfilePage, "In the Validate_EditProfile_DetailsPhysicalInfo test case, after saving new values for 'Hair color' in the the Click to Edit overlay the values were not saved properly in the Profile Page \r\nValue we saved in Click to Edit overlay: '" + new_hairColor + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.EyeColor;
                Assert.IsTrue(new_eyeColor == stringToVerifyOnProfilePage, "In the Validate_EditProfile_DetailsPhysicalInfo test case, after saving new values for 'Eye color' in the the Click to Edit overlay the values were not saved properly in the Profile Page \r\nValue we saved in Click to Edit overlay: '" + new_eyeColor + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.Height_Dealbreakers;
                Assert.IsTrue(new_height.Replace(" ", "") == stringToVerifyOnProfilePage.Replace(" ", ""), "In the Validate_EditProfile_Dealbreakers test case, after saving new values for 'Height' the values were not saved properly in the Profile Page in the Dealbreakers section \r\nValue we saved in Edit Profile: '" + new_height + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                // reinitialize to original values
                profile.PhysicalInfo_ClickToEdit();

                profile.Height_InOverlay = "5'5\"(165 cm)";
                profile.Weight_InOverlay = "136 lbs (61.5 kg)";
                profile.BodyType_InOverlay = "Petite";
                profile.HairColor_InOverlay = "Black";
                profile.EyeColor_InOverlay = "Brown";

                profile.PhysicalInfo_SaveChanges();

                profile.Header.Logout_Click();

                TearDown();

                log.Info("END TEST - JDate/Regression/Member/EditProfile_DetailsPhysicalInfo");
            }
            catch (Exception e)
            {
                CleanupFailedTestcase(e);
                throw;
            }
        }

        [Test]
        public void Validate_EditProfile_DetailsLifestyle()
        {
            try
            {
                log.Info("START TEST - JDate/Regression/Member/EditProfile_DetailsLifestyle");

                Setup();

                StartTest_UsingJDate();

                string login = System.Configuration.ConfigurationManager.AppSettings["JDate_RegisteredUser1_Email"];
                string password = System.Configuration.ConfigurationManager.AppSettings["JDate_RegisteredUser1_Password"];

                WebdriverAutomation.Tests.ReusableCode.LoggingInAndOut loggingInAndOut = new LoggingInAndOut();
                WebdriverAutomation.Framework.Site_Pages.Home home = loggingInAndOut.LogIn_ProfileComplete(driver, log, login, password);
                WebdriverAutomation.Framework.Site_Pages.YourProfile profile = home.Header.YourProfileHeaderLink_Click();

                string new_MaritalStatus = "Widowed";
                string new_Relocation = "Yes";
                string new_HaveKids = "3 or More";
                string new_Custody = "Some live with me";
                string new_WantKids = "No";

                List<string> new_Pets = new List<string>();
                new_Pets.Add("Bird");
                new_Pets.Add("Rabbit");

                string new_Kosher = "At home only";
                string new_Synagogue = "Every Shabbat";
                string new_Smoke = "Non-Smoker";
                string new_Drink = "Never";
                string new_ActivityLevel = "Never Active";

                // Overwrite new values to the page
                profile.Lifestyle_ClickToEdit();

                profile.MaritalStatus_InOverlay = new_MaritalStatus;
                profile.Relocation_InOverlay = new_Relocation;
                profile.HaveKids_InOverlay = new_HaveKids;
                profile.Custody_InOverlay = new_Custody;
                profile.WantKids_InOverlay = new_WantKids;
                profile.Pets_InOverlay = new_Pets;
                profile.Kosher_InOverlay = new_Kosher;
                profile.Synagogue_InOverlay = new_Synagogue;
                profile.Smoking_InOverlay = new_Smoke;
                profile.Drinking_InOverlay = new_Drink;
                profile.ActivityLevel_InOverlay = new_ActivityLevel;

                profile.Lifestyle_SaveChanges();

                // Go to the Home Page and then come back to the Profile Page to verify the values saved correctly
                home = profile.Header.HomeHeaderLink_Click();
                profile = home.Header.YourProfileHeaderLink_Click();

                string stringToVerifyOnProfilePage = string.Empty;
                List<string> listToVerifyOnProfilePage = new List<string>();

                stringToVerifyOnProfilePage = profile.MaritalStatus;
                Assert.IsTrue(new_MaritalStatus == stringToVerifyOnProfilePage, "In the Validate_EditProfile_DetailsLifestyle test case, after saving new values for 'Marital Status' in the the Click to Edit overlay the values were not saved properly in the Profile Page \r\nValue we saved in Click to Edit overlay: '" + new_MaritalStatus + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.Relocation;
                Assert.IsTrue(new_Relocation == stringToVerifyOnProfilePage, "In the Validate_EditProfile_DetailsLifestyle test case, after saving new values for 'Relocation' in the the Click to Edit overlay the values were not saved properly in the Profile Page \r\nValue we saved in Click to Edit overlay: '" + new_Relocation + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.HaveKids;
                Assert.IsTrue(new_HaveKids == stringToVerifyOnProfilePage, "In the Validate_EditProfile_DetailsLifestyle test case, after saving new values for 'Have kids' in the the Click to Edit overlay the values were not saved properly in the Profile Page \r\nValue we saved in Click to Edit overlay: '" + new_HaveKids + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.Custody;
                Assert.IsTrue(new_Custody == stringToVerifyOnProfilePage, "In the Validate_EditProfile_DetailsLifestyle test case, after saving new values for 'Custody' in the the Click to Edit overlay the values were not saved properly in the Profile Page \r\nValue we saved in Click to Edit overlay: '" + new_Custody + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.WantKids;
                Assert.IsTrue(new_WantKids == stringToVerifyOnProfilePage, "In the Validate_EditProfile_DetailsLifestyle test case, after saving new values for 'Wants kids' in the the Click to Edit overlay the values were not saved properly in the Profile Page \r\nValue we saved in Click to Edit overlay: '" + new_WantKids + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                listToVerifyOnProfilePage = profile.Pets;
                new_Pets.Sort();
                Assert.IsTrue(AreValuesAndCountEqual(new_Pets, listToVerifyOnProfilePage), "In the Validate_EditProfile_MoreAboutMeTab test case, after saving new values for 'Pets' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + string.Join(", ", new_Pets.ToArray()) + "' \r\nValue currently in Profile Page: '" + string.Join(", ", listToVerifyOnProfilePage.ToArray()) + "'");

                stringToVerifyOnProfilePage = profile.Kosher;
                Assert.IsTrue(new_Kosher == stringToVerifyOnProfilePage, "In the Validate_EditProfile_DetailsLifestyle test case, after saving new values for 'Kosher' in the the Click to Edit overlay the values were not saved properly in the Profile Page \r\nValue we saved in Click to Edit overlay: '" + new_Kosher + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.Synagogue;
                Assert.IsTrue(new_Synagogue == stringToVerifyOnProfilePage, "In the Validate_EditProfile_DetailsLifestyle test case, after saving new values for 'Synagogue' in the the Click to Edit overlay the values were not saved properly in the Profile Page \r\nValue we saved in Click to Edit overlay: '" + new_Synagogue + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.Smoking;
                Assert.IsTrue(new_Smoke == stringToVerifyOnProfilePage, "In the Validate_EditProfile_DetailsLifestyle test case, after saving new values for 'Smoking' in the the Click to Edit overlay the values were not saved properly in the Profile Page \r\nValue we saved in Click to Edit overlay: '" + new_Smoke + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.Drinking;
                Assert.IsTrue(new_Drink == stringToVerifyOnProfilePage, "In the Validate_EditProfile_DetailsLifestyle test case, after saving new values for 'Drinking' in the the Click to Edit overlay the values were not saved properly in the Profile Page \r\nValue we saved in Click to Edit overlay: '" + new_MaritalStatus + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.HaveKids_Dealbreakers;
                Assert.IsTrue(new_HaveKids == stringToVerifyOnProfilePage, "In the Validate_EditProfile_Dealbreakers test case, after saving new values for 'Have kids' the values were not saved properly in the Profile Page in the Dealbreakers section \r\nValue we saved in Edit Profile: '" + new_HaveKids + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.WantsKids_Dealbreakers;
                Assert.IsTrue(new_WantKids == stringToVerifyOnProfilePage, "In the Validate_EditProfile_Dealbreakers test case, after saving new values for 'Wants kids' the values were not saved properly in the Profile Page in the Dealbreakers section \r\nValue we saved in Edit Profile: '" + new_WantKids + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.Smoking_Dealbreakers;
                Assert.IsTrue(new_Smoke == stringToVerifyOnProfilePage, "In the Validate_EditProfile_Dealbreakers test case, after saving new values for 'Smoking' the values were not saved properly in the Profile Page in the Dealbreakers section \r\nValue we saved in Edit Profile: '" + new_Smoke + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                // reinitialize to original values
                List<string> orig_Pets = new List<string>();
                orig_Pets.Add("Reptile");
                orig_Pets.Add("Hamster");

                profile.Lifestyle_ClickToEdit();

                profile.MaritalStatus_InOverlay = "Single";
                profile.Relocation_InOverlay = "No";
                profile.HaveKids_InOverlay = "None";
                profile.Custody_InOverlay = "I have no children";
                profile.WantKids_InOverlay = "Not sure";
                profile.Pets_InOverlay = orig_Pets;
                profile.Kosher_InOverlay = "Not at all";
                profile.Synagogue_InOverlay = "Never";
                profile.Smoking_InOverlay = "Regularly";
                profile.Drinking_InOverlay = "Frequently";
                profile.ActivityLevel_InOverlay = "Very Active";

                profile.Lifestyle_SaveChanges();

                profile.Header.Logout_Click();

                TearDown();

                log.Info("END TEST - JDate/Regression/Member/EditProfile_DetailsLifestyle");
            }
            catch (Exception e)
            {
                CleanupFailedTestcase(e);
                throw;
            }
        }

        [Test]
        public void Validate_EditProfile_DetailsBackground()
        {
            try
            {
                log.Info("START TEST - JDate/Regression/Member/EditProfile_DetailsBackground");

                Setup();

                StartTest_UsingJDate();

                string login = System.Configuration.ConfigurationManager.AppSettings["JDate_RegisteredUser1_Email"];
                string password = System.Configuration.ConfigurationManager.AppSettings["JDate_RegisteredUser1_Password"];

                WebdriverAutomation.Tests.ReusableCode.LoggingInAndOut loggingInAndOut = new LoggingInAndOut();
                WebdriverAutomation.Framework.Site_Pages.Home home = loggingInAndOut.LogIn_ProfileComplete(driver, log, login, password);
                WebdriverAutomation.Framework.Site_Pages.YourProfile profile = home.Header.YourProfileHeaderLink_Click();

                string new_GrewUpIn = "The old country";
                string new_MyEthnicityIs = "Sephardic";

                List<string> new_Languages = new List<string>();
                new_Languages.Add("Czech");
                new_Languages.Add("Tagalog");

                string new_Religion = "Hassidic";
                string new_Education = "Elementary";
                string new_AreaOfStudy = "Math";
                string new_Occuaption = "Retired";
                string new_WhatIDo = "Student";
                string new_AnnualIncome = "Under $15,000";
                string new_Politics = "Left Wing";
                string new_ZodiacSign = "Aries";

                // Overwrite new values to the page
                profile.Background_ClickToEdit();

                profile.GrewUpIn_InOverlay = new_GrewUpIn;
                profile.Ethnicity_InOverlay = new_MyEthnicityIs;
                profile.Languages_InOverlay = new_Languages;
                profile.Religion_InOverlay = new_Religion;
                profile.Education_InOverlay = new_Education;
                profile.AreaOfStudy_InOverlay = new_AreaOfStudy;
                profile.Occupation_InOverlay = new_Occuaption;
                profile.WhatIDo_InOverlay = new_WhatIDo;
                profile.AnnualIncome_InOverlay = new_AnnualIncome;
                profile.Politics_InOverlay = new_Politics;
                profile.ZodiacSign_InOverlay = new_ZodiacSign;

                profile.Background_SaveChanges();

                // Go to the Home Page and then come back to the Profile Page to verify the values saved correctly
                home = profile.Header.HomeHeaderLink_Click();
                profile = home.Header.YourProfileHeaderLink_Click();

                string stringToVerifyOnProfilePage = string.Empty;
                List<string> listToVerifyOnProfilePage = new List<string>();

                stringToVerifyOnProfilePage = profile.GrewUpIn;
                Assert.IsTrue(new_GrewUpIn == stringToVerifyOnProfilePage, "In the Validate_EditProfile_DetailsBackground test case, after saving new values for 'Grew up in' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_GrewUpIn + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.Ethnicity;
                Assert.IsTrue(new_MyEthnicityIs == stringToVerifyOnProfilePage, "In the Validate_EditProfile_DetailsBackground test case, after saving new values for 'Ethnicity' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_MyEthnicityIs + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                listToVerifyOnProfilePage = profile.Languages;
                new_Languages.Sort();
                Assert.IsTrue(AreValuesAndCountEqual(new_Languages, listToVerifyOnProfilePage), "In the Validate_EditProfile_MoreAboutMeTab test case, after saving new values for 'Languages' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + string.Join(", ", new_Languages.ToArray()) + "' \r\nValue currently in Profile Page: '" + string.Join(", ", listToVerifyOnProfilePage.ToArray()) + "'");

                stringToVerifyOnProfilePage = profile.Religion;
                Assert.IsTrue(new_Religion == stringToVerifyOnProfilePage, "In the Validate_EditProfile_DetailsBackground test case, after saving new values for 'Religion' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_Religion + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.Education;
                Assert.IsTrue(new_Education == stringToVerifyOnProfilePage, "In the Validate_EditProfile_DetailsBackground test case, after saving new values for 'Education' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_Education + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.AreaOfStudy;
                Assert.IsTrue(new_AreaOfStudy == stringToVerifyOnProfilePage, "In the Validate_EditProfile_DetailsBackground test case, after saving new values for 'Area of study' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_AreaOfStudy + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.Occupation;
                Assert.IsTrue(new_Occuaption == stringToVerifyOnProfilePage, "In the Validate_EditProfile_DetailsBackground test case, after saving new values for 'Occupation' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_Occuaption + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.WhatIDo;
                Assert.IsTrue(new_WhatIDo == stringToVerifyOnProfilePage, "In the Validate_EditProfile_DetailsBackground test case, after saving new values for 'What I do' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_WhatIDo + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.AnnualIncome;
                Assert.IsTrue(new_AnnualIncome == stringToVerifyOnProfilePage, "In the Validate_EditProfile_DetailsBackground test case, after saving new values for 'Annual income' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_AnnualIncome + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.Politics;
                Assert.IsTrue(new_Politics == stringToVerifyOnProfilePage, "In the Validate_EditProfile_DetailsBackground test case, after saving new values for 'Politics' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_Politics + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.ZodiacSign;
                Assert.IsTrue(new_ZodiacSign == stringToVerifyOnProfilePage, "In the Validate_EditProfile_DetailsBackground test case, after saving new values for 'Zodiac sign' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_ZodiacSign + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.Religion_Dealbreakers;
                Assert.IsTrue(new_Religion == stringToVerifyOnProfilePage, "In the Validate_EditProfile_DetailsBackground test case, after saving new values for 'Religion' the values were not saved properly in the Profile Page in the Dealbreakers section \r\nValue we saved in Edit Profile: '" + new_Religion + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.Education_Dealbreakers;
                Assert.IsTrue(new_Education == stringToVerifyOnProfilePage, "In the Validate_EditProfile_DetailsBackground test case, after saving new values for 'Education' the values were not saved properly in the Profile Page in the Dealbreakers section \r\nValue we saved in Edit Profile: '" + new_Education + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.Occupation_Dealbreakers;
                Assert.IsTrue(new_Occuaption == stringToVerifyOnProfilePage, "In the Validate_EditProfile_DetailsBackground test case, after saving new values for 'Occupation' the values were not saved properly in the Profile Page in the Dealbreakers section \r\nValue we saved in Edit Profile: '" + new_Occuaption + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                // reinitialize to original values
                List<string> orig_Languages = new List<string>();
                orig_Languages.Add("Chinese");
                orig_Languages.Add("Russian");

                profile.Background_ClickToEdit();

                profile.GrewUpIn_InOverlay = "The City";
                profile.Ethnicity_InOverlay = "Will tell you later";
                profile.Languages_InOverlay = orig_Languages;
                profile.Religion_InOverlay = "Willing to convert";
                profile.Education_InOverlay = "Master's Degree";
                profile.AreaOfStudy_InOverlay = "Video games";
                profile.Occupation_InOverlay = "Other";
                profile.WhatIDo_InOverlay = "Teacher";
                profile.AnnualIncome_InOverlay = "Average";
                profile.Politics_InOverlay = "Liberal";
                profile.ZodiacSign_InOverlay = "Sagittarius";

                profile.Background_SaveChanges();

                profile.Header.Logout_Click();

                TearDown();

                log.Info("END TEST - JDate/Regression/Member/EditProfile_DetailsBackground");
            }
            catch (Exception e)
            {
                CleanupFailedTestcase(e);
                throw;
            }
        }

        /// <summary>
        /// We do not test the Username field
        /// </summary>
        [Test]
        public void Validate_EditProfile_YourBasics()
        {
            try
            {
                log.Info("START TEST - JDate/Regression/Member/EditProfile_YourBasics");
                Setup();

                StartTest_UsingJDate();

                string login = System.Configuration.ConfigurationManager.AppSettings["JDate_RegisteredUser1_Email"];
                string password = System.Configuration.ConfigurationManager.AppSettings["JDate_RegisteredUser1_Password"];

                WebdriverAutomation.Tests.ReusableCode.LoggingInAndOut loggingInAndOut = new LoggingInAndOut();
                WebdriverAutomation.Framework.Site_Pages.Home home = loggingInAndOut.LogIn_ProfileComplete(driver, log, login, password);
                WebdriverAutomation.Framework.Site_Pages.YourProfile profile = home.Header.YourProfileHeaderLink_Click();

                string new_FirstName = "Mark";
                string new_LastName = "Azuras";
                string new_DateOfBirthMonth = "Sep";
                string new_DateOfBirthDay = "15";
                string new_DateOfBirthYear = "1980";
                string new_Country = "Canada";
                string new_Zipcode = "K8N5W6";
                string new_ImA = "woman seeking men";
                string new_RelationshipStatus = "Divorced";

                // Overwrite new values to the page
                profile.YourBasics_ClickToEdit();

                profile.FirstName_InOverlay = new_FirstName;
                profile.LastName_InOverlay = new_LastName;
                profile.YourDateOfBirthMonth_InOverlay = new_DateOfBirthMonth;
                profile.YourDateOfBirthDay_InOverlay = new_DateOfBirthDay;
                profile.YourDateOfBirthYear_InOverlay = new_DateOfBirthYear;
                profile.Country_InOverlay = new_Country;
                profile.ZipCode_InOverlay = new_Zipcode;
                profile.ImA_InOverlay = new_ImA;
                profile.RelationshipStatus_InOverlay = new_RelationshipStatus;

                profile.YourBasics_SaveChanges();

                // Go to the Home Page and then come back to the Profile Page to verify the values saved correctly
                home = profile.Header.HomeHeaderLink_Click();
                profile = home.Header.YourProfileHeaderLink_Click();

                string stringToVerifyOnProfilePage = string.Empty;

                stringToVerifyOnProfilePage = profile.RelationshipStatus;
                Assert.IsTrue(new_RelationshipStatus == stringToVerifyOnProfilePage, "In the Validate_EditProfile_YourBasics test case, after saving new values for 'Relationship status' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_RelationshipStatus + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.ImA;
                // stupid small discrepancy where the text is slightly different on the Profile Page than on the Edit Profile overlay
                if (new_ImA == "woman seeking men")
                    new_ImA = "Woman seeking a Man";

                Assert.IsTrue(new_ImA == stringToVerifyOnProfilePage, "In the Validate_EditProfile_YourBasics test case, after saving new values for 'I'm a' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_ImA + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                int ageOfUser = AgeOfUser(new_DateOfBirthMonth, new_DateOfBirthDay, new_DateOfBirthYear);
                stringToVerifyOnProfilePage = profile.Age;
                Assert.IsTrue(Convert.ToInt32(stringToVerifyOnProfilePage) == ageOfUser, "In the Validate_EditProfile_YourBasics test case, after saving new values for 'Age' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + ageOfUser + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.Country;
                Assert.IsTrue(new_Country == stringToVerifyOnProfilePage, "In the Validate_EditProfile_YourBasics test case, after saving new values for 'Country' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_Country + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                // reinitialize to original values
                profile.YourBasics_ClickToEdit();

                profile.FirstName_InOverlay = "Theo";
                profile.LastName_InOverlay = "Wang";
                profile.YourDateOfBirthMonth_InOverlay = "Dec";
                profile.YourDateOfBirthDay_InOverlay = "16";
                profile.YourDateOfBirthYear_InOverlay = "1979";
                profile.Country_InOverlay = "USA";
                profile.ZipCode_InOverlay = "94133";
                profile.ImA_InOverlay = "man seeking women";
                profile.RelationshipStatus_InOverlay = "Single";

                profile.YourBasics_SaveChanges();

                profile.Header.Logout_Click();

                TearDown();

                log.Info("END TEST - JDate/Regression/Member/EditProfile_YourBasics");
            }
            catch (Exception e)
            {
                CleanupFailedTestcase(e);
                throw;
            }
        }

        [Test]
        public void Validate_EditProfile_Dealbreakers()
        {
            try
            {
                log.Info("START TEST - JDate/Regression/Member/EditProfile_Dealbreakers");
                Setup();

                StartTest_UsingJDate();

                string login = System.Configuration.ConfigurationManager.AppSettings["JDate_RegisteredUser1_Email"];
                string password = System.Configuration.ConfigurationManager.AppSettings["JDate_RegisteredUser1_Password"];

                WebdriverAutomation.Tests.ReusableCode.LoggingInAndOut loggingInAndOut = new LoggingInAndOut();
                WebdriverAutomation.Framework.Site_Pages.Home home = loggingInAndOut.LogIn_ProfileComplete(driver, log, login, password);
                WebdriverAutomation.Framework.Site_Pages.YourProfile profile = home.Header.YourProfileHeaderLink_Click();

                string new_HaveKids = "3 or More";
                string new_WantsKids = "No";
                string new_height = "4'6\"(137 cm)";
                //string new_Ethnicity = "Ashkenazi";
                string new_Religion = "Reform";
                string new_Smoking = "Non-Smoker";
                //string new_Drinking = "Never";                
                string new_Education = "Elementary";
                string new_Occupation = "Retired";
                //string new_AnnualIncome = "Average";                 
                string new_Synagogue = "Every Shabbat";

                // Overwrite new values to the page
                profile.Dealbreakers_ClickToEdit();

                profile.HaveKids_InDealbreakersOverlay = new_HaveKids;
                profile.WantsKids_InDealbreakersOverlay = new_WantsKids;
                profile.Height_InDealbreakersOverlay = new_height;
                //profile.Ethnicity_InDealbreakersOverlay = new_Ethnicity;
                profile.Religion_InDealbreakersOverlay = new_Religion;
                profile.Smoking_InDealbreakersOverlay = new_Smoking;
                //profile.Drinking_InDealbreakersOverlay = new_Drinking;                
                profile.Education_InDealbreakersOverlay = new_Education;
                profile.Occupation_InDealbreakersOverlay = new_Occupation;
                //profile.AnnualIncome_InDealbreakersOverlay = new_AnnualIncome;       
                profile.Synagogue_InDealbreakersOverlay = new_Synagogue;

                profile.Dealbreakers_SaveChanges();

                // Go to the Home Page and then come back to the Profile Page to verify the values saved correctly
                home = profile.Header.HomeHeaderLink_Click();
                profile = home.Header.YourProfileHeaderLink_Click();

                string stringToVerifyOnProfilePage = string.Empty;

                stringToVerifyOnProfilePage = profile.HaveKids_Dealbreakers;
                Assert.IsTrue(new_HaveKids == stringToVerifyOnProfilePage, "In the Validate_EditProfile_Dealbreakers test case, after saving new values for 'Have kids' the values were not saved properly in the Profile Page in the Dealbreakers section \r\nValue we saved in Edit Profile: '" + new_HaveKids + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.WantsKids_Dealbreakers;
                Assert.IsTrue(new_WantsKids == stringToVerifyOnProfilePage, "In the Validate_EditProfile_Dealbreakers test case, after saving new values for 'Wants kids' the values were not saved properly in the Profile Page in the Dealbreakers section \r\nValue we saved in Edit Profile: '" + new_WantsKids + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.Height_Dealbreakers;
                Assert.IsTrue(new_height.Replace(" ", "") == stringToVerifyOnProfilePage.Replace(" ", ""), "In the Validate_EditProfile_Dealbreakers test case, after saving new values for 'Height' the values were not saved properly in the Profile Page in the Dealbreakers section \r\nValue we saved in Edit Profile: '" + new_height + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                //stringToVerifyOnProfilePage = profile.Ethnicity_Dealbreakers;
                //Assert.IsTrue(new_Ethnicity == stringToVerifyOnProfilePage, "In the Validate_EditProfile_Dealbreakers test case, after saving new values for 'Ethnicity' the values were not saved properly in the Profile Page in the Dealbreakers section \r\nValue we saved in Edit Profile: '" + new_Ethnicity + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.Religion_Dealbreakers;
                Assert.IsTrue(new_Religion == stringToVerifyOnProfilePage, "In the Validate_EditProfile_Dealbreakers test case, after saving new values for 'Religion' the values were not saved properly in the Profile Page in the Dealbreakers section \r\nValue we saved in Edit Profile: '" + new_Religion + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.Smoking_Dealbreakers;
                Assert.IsTrue(new_Smoking == stringToVerifyOnProfilePage, "In the Validate_EditProfile_Dealbreakers test case, after saving new values for 'Smoking' the values were not saved properly in the Profile Page in the Dealbreakers section \r\nValue we saved in Edit Profile: '" + new_Smoking + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                //stringToVerifyOnProfilePage = profile.Drinking_Dealbreakers;
                //Assert.IsTrue(new_Drinking == stringToVerifyOnProfilePage, "In the Validate_EditProfile_Dealbreakers test case, after saving new values for 'Drinking' the values were not saved properly in the Profile Page in the Dealbreakers section \r\nValue we saved in Edit Profile: '" + new_Drinking + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.Education_Dealbreakers;
                Assert.IsTrue(new_Education == stringToVerifyOnProfilePage, "In the Validate_EditProfile_Dealbreakers test case, after saving new values for 'Education' the values were not saved properly in the Profile Page in the Dealbreakers section \r\nValue we saved in Edit Profile: '" + new_Education + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.Occupation_Dealbreakers;
                Assert.IsTrue(new_Occupation == stringToVerifyOnProfilePage, "In the Validate_EditProfile_Dealbreakers test case, after saving new values for 'Occupation' the values were not saved properly in the Profile Page in the Dealbreakers section \r\nValue we saved in Edit Profile: '" + new_Occupation + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                //stringToVerifyOnProfilePage = profile.AnnualIncome_Dealbreakers;
                //Assert.IsTrue(new_AnnualIncome == stringToVerifyOnProfilePage, "In the Validate_EditProfile_Dealbreakers test case, after saving new values for 'Annual $' the values were not saved properly in the Profile Page in the Dealbreakers section \r\nValue we saved in Edit Profile: '" + new_AnnualIncome + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.Synagogue_Dealbreakers;
                Assert.IsTrue(new_Synagogue == stringToVerifyOnProfilePage, "In the Validate_EditProfile_Dealbreakers test case, after saving new values for 'Synagogue' the values were not saved properly in the Profile Page in the Dealbreakers section \r\nValue we saved in Edit Profile: '" + new_Synagogue + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.HaveKids;
                Assert.IsTrue(new_HaveKids == stringToVerifyOnProfilePage, "In the Validate_EditProfile_Dealbreakers test case, after saving new values for 'Have kids' the values were not saved properly in the Profile Page in the Details section \r\nValue we saved in Edit Profile: '" + new_HaveKids + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.WantKids;
                Assert.IsTrue(new_WantsKids == stringToVerifyOnProfilePage, "In the Validate_EditProfile_Dealbreakers test case, after saving new values for 'Wants kids' the values were not saved properly in the Profile Page in the Details section \r\nValue we saved in Edit Profile: '" + new_WantsKids + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.Height;
                Assert.IsTrue(new_height.Replace(" ", "") == stringToVerifyOnProfilePage.Replace(" ", ""), "In the Validate_EditProfile_Dealbreakers test case, after saving new values for 'Height' the values were not saved properly in the Profile Page in the Details section \r\nValue we saved in Edit Profile: '" + new_height + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                //stringToVerifyOnProfilePage = profile.Ethnicity;
                //Assert.IsTrue(new_Ethnicity == stringToVerifyOnProfilePage, "In the Validate_EditProfile_Dealbreakers test case, after saving new values for 'Ethnicity' the values were not saved properly in the Profile Page in the Details section \r\nValue we saved in Edit Profile: '" + new_Ethnicity + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.Religion;
                Assert.IsTrue(new_Religion == stringToVerifyOnProfilePage, "In the Validate_EditProfile_Dealbreakers test case, after saving new values for 'Religion' the values were not saved properly in the Profile Page in the Details section \r\nValue we saved in Edit Profile: '" + new_Religion + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.Smoking;
                Assert.IsTrue(new_Smoking == stringToVerifyOnProfilePage, "In the Validate_EditProfile_Dealbreakers test case, after saving new values for 'Smoking' the values were not saved properly in the Profile Page in the Details section \r\nValue we saved in Edit Profile: '" + new_Smoking + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                //stringToVerifyOnProfilePage = profile.Drinking;
                //Assert.IsTrue(new_Drinking == stringToVerifyOnProfilePage, "In the Validate_EditProfile_Dealbreakers test case, after saving new values for 'Drinking' the values were not saved properly in the Profile Page in the Details section \r\nValue we saved in Edit Profile: '" + new_Drinking + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.Education;
                Assert.IsTrue(new_Education == stringToVerifyOnProfilePage, "In the Validate_EditProfile_Dealbreakers test case, after saving new values for 'Education' the values were not saved properly in the Profile Page in the Details section \r\nValue we saved in Edit Profile: '" + new_Education + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.Occupation;
                Assert.IsTrue(new_Occupation == stringToVerifyOnProfilePage, "In the Validate_EditProfile_Dealbreakers test case, after saving new values for 'Occupation' the values were not saved properly in the Profile Page in the Details section \r\nValue we saved in Edit Profile: '" + new_Occupation + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                //stringToVerifyOnProfilePage = profile.AnnualIncome;
                //Assert.IsTrue(new_AnnualIncome == stringToVerifyOnProfilePage, "In the Validate_EditProfile_Dealbreakers test case, after saving new values for 'Annual $' the values were not saved properly in the Profile Page in the Details section \r\nValue we saved in Edit Profile: '" + new_Occupation + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.Synagogue;
                Assert.IsTrue(new_Synagogue == stringToVerifyOnProfilePage, "In the Validate_EditProfile_Dealbreakers test case, after saving new values for 'Synagogue' the values were not saved properly in the Profile Page in the Details section \r\nValue we saved in Edit Profile: '" + new_Synagogue + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");


                // reinitialize to original values
                profile.Dealbreakers_ClickToEdit();

                profile.HaveKids_InDealbreakersOverlay = "None";
                profile.WantsKids_InDealbreakersOverlay = "Yes";
                profile.Height_InDealbreakersOverlay = "5'5\"(165 cm)";
                //profile.Ethnicity_InDealbreakersOverlay = "Sephardic";
                profile.Religion_InDealbreakersOverlay = "Conservadox";
                profile.Smoking_InDealbreakersOverlay = "Occasionally";
                //profile.Drinking_InDealbreakersOverlay = "Frequently";                
                profile.Education_InDealbreakersOverlay = "High School";
                profile.Occupation_InDealbreakersOverlay = "Student";
                //profile.AnnualIncome_InDealbreakersOverlay = "Will tell you later";          
                profile.Synagogue_InDealbreakersOverlay = "Never";

                profile.Dealbreakers_SaveChanges();

                profile.Header.Logout_Click();

                TearDown();

                log.Info("END TEST - JDate/Regression/Member/EditProfile_Dealbreakers");
            }
            catch (Exception e)
            {
                CleanupFailedTestcase(e);
                throw;
            }
        }

        ///// <summary>
        ///// Setup:  The user that we login with for this test case should not have any photos currently uploaded. Else this test case will delete all existing photos
        ///// before starting.
        ///// </summary>
        //[Test]
        //public void Validate_UploadAndEditPhotos()
        //{
        //    try
        //    {
        //        log.Info("START TEST - JDate/Regression/Member/UploadAndEditPhotos");

        //        Setup();

        //        StartTest_UsingJDate();

        //        string login = System.Configuration.ConfigurationManager.AppSettings["JDate_RegisteredUser2_Email"];
        //        string password = System.Configuration.ConfigurationManager.AppSettings["JDate_RegisteredUser2_Password"];

        //        WebdriverAutomation.Tests.ReusableCode.LoggingInAndOut loggingInAndOut = new LoggingInAndOut();
        //        WebdriverAutomation.Framework.Site_Pages.Home home = loggingInAndOut.LogIn_ProfileComplete(driver, log, login, password);
        //        WebdriverAutomation.Framework.Site_Pages.YourProfile profile = home.Header.YourProfileHeaderLink_Click();
        //        Framework.Site_Pages.ManageYourPhotos manageYourPhotos = profile.ManageYourPhotosButton_Click();     

        //        // delete all existing photos except for the first one
        //        while (manageYourPhotos.Photo2PhotoExists())
        //        {
        //            manageYourPhotos.Photo1XButton_Click();
        //            manageYourPhotos.SaveChangesButton_Click();
        //        }

        //        // grab a random picture in our solution's file path and put it in the Upload a Photo textbox
        //        string strFileName = string.Empty;
        //        Random random = new Random();

        //        // instead of grabbing our profile picture from the solution now we are grabbing it from a networked folder both QA and I can access:
        //        //   S:\Quality Assurance\Automation Builds\PicsForAutomation
        //        //FileInfo[] files = HelperMethods.GetReqFileInfo(@"*.jpg");
        //        //if (files.Length > 0)
        //        //    strFileName = files[random.Next(0, files.Length - 1)].FullName;
        //        strFileName = "s:\\Quality Assurance\\Automation Builds\\PicsForAutomation\\ProfilePicture.jpg";

        //        string photoCaption = "I look better in this photo";

        //        manageYourPhotos.Photo2UploadAPhotoTextbox_Write(strFileName);
        //        manageYourPhotos.Photo2CaptionTextarea = photoCaption;
        //        manageYourPhotos.SaveChangesButton_Click();

        //        // verify the text, "Profile successfully changed" appears
        //        Assert.True(manageYourPhotos.NotificationText == "Profile successfully changed", "ERROR - MANAGE YOUR PHOTOS PAGE - After Uploading a photo and pressing the Save Changes button, the 'Profile successfuly changed' text did not appear");

        //        // Delete and then Undelete the newly uploaded photo
        //        manageYourPhotos.Photo2XButton_Click();
        //        Assert.True(manageYourPhotos.Photo2Notification.Contains("Photo 2 and its caption will be deleted"), "ERROR - MANAGE YOUR PHOTOS PAGE - After Uploading a photo and pressing the Delete link for Photo 2, the 'Photo 2 and its caption wil be deleted...' text did not appear");
        //        manageYourPhotos.Photo2UndeleteLink_Click();
        //        manageYourPhotos.SaveChangesButton_Click();

        //        Assert.True(manageYourPhotos.Photo2PhotoExists(), "ERROR - MANAGE YOUR PHOTOS PAGE - After Uploading a photo and pressing the Save Changes button, we deleted the photo then immediately undeleted it and pressed Save again \r\nThe photo that was uploaded and then undeleted is not found");
        //        Assert.True(manageYourPhotos.Photo2CaptionTextarea == photoCaption, "ERROR - MANAGE YOUR PHOTOS PAGE - After Uploading a photo and pressing the Save Changes button, we deleted the photo then immediately undeleted it and pressed Save again \r\nThe caption that was uploaded previously is not the same. Previous caption value: " + photoCaption + ". The caption value now: " + manageYourPhotos.Photo1CaptionTextarea);

        //        // go to the Home Page then back to Manage Your Photos and verify that a photo still exists (the Upload a New Photo image does not) 
        //        //   and that the caption is still there
        //        home = manageYourPhotos.Header.HomeHeaderLink_Click();
        //        profile = home.Header.YourProfileHeaderLink_Click();
        //        manageYourPhotos = profile.ManageYourPhotosButton_Click();  

        //        Assert.True(manageYourPhotos.Photo2PhotoExists(), "ERROR - MANAGE YOUR PHOTOS PAGE - After Uploading a photo and pressing the Save Changes button, we navigated back to the Home page and then back to the Manage Your Photos page. The photo that was uploaded previously cannot be found");
        //        Assert.True(manageYourPhotos.Photo2CaptionTextarea == photoCaption, "ERROR - MANAGE YOUR PHOTOS PAGE - After Uploading a photo and pressing the Save Changes button, we navigated back to the Home page and then back to the Manage Your Photos page. The caption that was uploaded previously is not the same. Previous caption value: " + photoCaption + ". The caption value now: " + manageYourPhotos.Photo1CaptionTextarea);

        //        // Reinitialize: Delete the photo we just added and verify that it really gets deleted
        //        manageYourPhotos.Photo2XButton_Click();
        //        manageYourPhotos.SaveChangesButton_Click();

        //        Assert.IsFalse(manageYourPhotos.Photo2PhotoExists(), "ERROR - MANAGE YOUR PHOTOS PAGE - After deleting a photo and pressing the Save Changes button, the photo was not deleted");

        //        manageYourPhotos.Header.Logout_Click();

        //        TearDown();

        //        log.Info("END TEST - JDate/Regression/Member/UploadAndEditPhotos");
        //    }
        //    catch (Exception e)
        //    {
        //        CleanupFailedTestcase(e);
        //        throw;
        //    }
        //}

        [Test]
        public void Validate_QuickSearchResults()
        {
            try
            {
                log.Info("START TEST - JDate/Regression/Member/QuickSearchResults");

                Setup();

                StartTest_UsingJDate();

                string login = System.Configuration.ConfigurationManager.AppSettings["JDate_RegisteredUser1_Email"];
                string password = System.Configuration.ConfigurationManager.AppSettings["JDate_RegisteredUser1_Password"];

                WebdriverAutomation.Tests.ReusableCode.LoggingInAndOut loggingInAndOut = new LoggingInAndOut();
                WebdriverAutomation.Framework.Site_Pages.Home home = loggingInAndOut.LogIn_ProfileComplete(driver, log, login, password);
                Framework.Site_Pages.QuickSearch quickSearch = home.Header.SearchHeaderLink_Click();

                string youreA = "Man";
                string seekingA = "Woman";
                string ageFrom = "20";
                string ageTo = "30";
                string locatedWithin = "5 Miles";
                string country = "USA";
                string zipCode = "94133";

                // add all cities located within 5 miles of 94133 (San Francisco)
                List<string> cities = new List<string>();
                cities.Add("Alameda");
                cities.Add("Albany");
                cities.Add("Berkeley");
                cities.Add("Broadmoor Vlg");
                cities.Add("Daly City");
                cities.Add("El Cerrito");
                cities.Add("Emeryville");
                cities.Add("Mill Valley");
                cities.Add("Oakland");
                cities.Add("Piedmont");
                cities.Add("Pt Richmond");
                cities.Add("Richmond");
                cities.Add("S San Fran");
                cities.Add("San Francisco");
                cities.Add("Sausalito");
                cities.Add("South San Francisco");
                cities.Add("Tiburon");
                cities.Add("UC Berkeley");

                quickSearch.YoureADropdown = youreA;
                quickSearch.SeekingADropdown = seekingA;
                quickSearch.AgeFromTextbox = ageFrom;
                quickSearch.AgeToTextbox = ageTo;
                quickSearch.LocatedWithinDropdown = locatedWithin;
                quickSearch.EditLink_Click();
                quickSearch.EditOverlay_ZipCodeTab_Click();
                quickSearch.EditOverlay_ZipCodeTab_CountryDropdown = country;
                quickSearch.EditOverlay_ZipCodeTab_ZipCodeTextbox = zipCode;
                quickSearch.EditOverlay_SaveButton_Click();
                quickSearch.SearchButton_Click();

                quickSearch.GalleryLink_Click();

                // GALLERY VIEW - verify that quick search results appear
                Assert.True(quickSearch.SearchResultsCount_GalleryView() > 0, "ERROR - QUICK SEARCH PAGE - After doing a quick search in Gallery View, no results appeared.  \r\nThe search was for " + youreA + " seeking a " + seekingA + " searching within " + locatedWithin + " of " + zipCode);

                // GALLERY VIEW - verify that the results are all within the correct age range
                Assert.True(quickSearch.SearchResultsWithinAgeRange_GalleryView(Convert.ToInt32(ageFrom), Convert.ToInt32(ageTo)), "ERROR - QUICK SEARCH PAGE - After doing a quick search in Gallery View, the ages of the search results were not in the correct range.  \r\nThe search was for ages " + ageFrom + " to " + ageTo);

                // GALLERY VIEW - verify that the results are all within the correct zip code (checking for 5 miles within 94133 - San Francisco)
                string listOfCities = cities[0];

                for (int i = 1; i < cities.Count; i++)
                    listOfCities = listOfCities + ", " + cities[i];

                Assert.True(quickSearch.SearchResultsWithinExpectedCities_GalleryView(cities), "ERROR - QUICK SEARCH PAGE - After doing a quick search in Gallery View, the location range of the search result was not correct.  \r\nThe search was for " + locatedWithin + " within " + zipCode + " and the search results should have all returned one of the following cities: " + listOfCities);

                quickSearch.ListLink_Click();

                // LIST VIEW - verify that quick search results appear
                Assert.True(quickSearch.SearchResultsCount_ListView() > 0, "ERROR - QUICK SEARCH PAGE - After doing a quick search in List View, no results appeared.  \r\nThe search was for " + youreA + " seeking a " + seekingA + " searching within " + locatedWithin + " of " + zipCode);

                // LIST VIEW - verify that the results are all within the correct age range
                Assert.True(quickSearch.SearchResultsWithinAgeRange_ListView(Convert.ToInt32(ageFrom), Convert.ToInt32(ageTo)), "ERROR - QUICK SEARCH PAGE - After doing a quick search in List View, the ages of the search results were not in the correct range.");

                // LIST VIEW - verify that the results are all within the correct zip code (checking for 5 miles within 94133 - San Francisco)
                Assert.True(quickSearch.SearchResultsWithinExpectedCities_ListView(cities), "ERROR - QUICK SEARCH PAGE - After doing a quick search in List View, the location range of the search result was not correct.  \r\nThe search was for " + locatedWithin + " within " + zipCode + " and the search results should have all returned one of the following cities: " + cities[0] + ", " + cities[1] + ", " + cities[2] + ", " + cities[3]);

                // LIST VIEW - verify that the results show the correct gender seeking the correct gender
                Assert.True(quickSearch.SearchResultsIAmASeekingA_ListView(youreA, seekingA), "ERROR - QUICK SEARCH PAGE - After doing a quick search in List View, the gender preference was not correct.  \r\nThe search should have returned matches for " + seekingA + " seeking a " + youreA);

                TearDown();

                log.Info("END TEST - JDate/Regression/Member/QuickSearchResults");
            }
            catch (Exception e)
            {
                CleanupFailedTestcase(e);
                throw;
            }
        }

        /// <summary>
        /// In order to verify pagination we need to ensure that our search results have enough results to span more than one page (12 results).
        /// Using a search of Located Within 160 miles of 90210 (Beverly Hills) for this test case.
        /// </summary>
        [Test]
        public void Validate_QuickSearchPagination()
        {
            try
            {
                log.Info("START TEST - JDate/Regression/Member/QuickSearchPagination");

                Setup();

                StartTest_UsingJDate();

                string login = System.Configuration.ConfigurationManager.AppSettings["JDate_RegisteredUser1_Email"];
                string password = System.Configuration.ConfigurationManager.AppSettings["JDate_RegisteredUser1_Password"];

                WebdriverAutomation.Tests.ReusableCode.LoggingInAndOut loggingInAndOut = new LoggingInAndOut();
                WebdriverAutomation.Framework.Site_Pages.Home home = loggingInAndOut.LogIn_ProfileComplete(driver, log, login, password);
                Framework.Site_Pages.QuickSearch quickSearch = home.Header.SearchHeaderLink_Click();

                string youreA = "Man";
                string seekingA = "Woman";
                string ageFrom = "20";
                string ageTo = "30";
                string locatedWithin = "200 Miles";
                string country = "USA";
                string zipCode = "90210";

                quickSearch.YoureADropdown = youreA;
                quickSearch.SeekingADropdown = seekingA;
                quickSearch.AgeFromTextbox = ageFrom;
                quickSearch.AgeToTextbox = ageTo;
                quickSearch.LocatedWithinDropdown = locatedWithin;
                quickSearch.EditLink_Click();
                quickSearch.EditOverlay_ZipCodeTab_Click();
                quickSearch.EditOverlay_ZipCodeTab_CountryDropdown = country;
                quickSearch.EditOverlay_ZipCodeTab_ZipCodeTextbox = zipCode;
                quickSearch.EditOverlay_SaveButton_Click();
                quickSearch.SearchButton_Click();

                quickSearch.GalleryLink_Click();

                // save all the usernames from our search in a list
                List<string> userNames1Through12 = quickSearch.GetAllNamesOfSearchResultsOnThisPage();
                Assert.IsFalse(userNames1Through12.Count < 12, "ERROR - QUICK SEARCH PAGE - We cannot test pagination because we are not getting enough results back in our quicksearch \r\nWe are currently searching for a " + seekingA + " between the ages of " + ageFrom + " to " + ageTo + " within " + locatedWithin + " miles of " + zipCode + ".\r\nThe number of search results returned:  " + userNames1Through12.Count);

                // go to the next page - users 13-24 and save those names in another list
                quickSearch = quickSearch.Pagination_Page2_Click();
                List<string> userNames13Through24 = quickSearch.GetAllNamesOfSearchResultsOnThisPage();

                // verify that none of the user names match - we truly have reached a new page
                bool doAnyNamesFromPage1MatchPage2 = quickSearch.DoAnyUserNamesMatchInOurLists(userNames1Through12, userNames13Through24);
                Assert.IsFalse(doAnyNamesFromPage1MatchPage2, "ERROR - QUICK SEARCH PAGE - After paging through to the 2nd page of results (13-24) a user from page 1 was found in page 2");

                // verify Previous link
                quickSearch = quickSearch.Pagination_PreviousLink_Click();
                List<string> userNamesOnCurrentPage = quickSearch.GetAllNamesOfSearchResultsOnThisPage();

                bool doALLNamesFromPage1MatchPage2 = quickSearch.DoAllUserNamesMatchInOurLists(userNames1Through12, userNamesOnCurrentPage);
                Assert.IsTrue(doALLNamesFromPage1MatchPage2, "ERROR - QUICK SEARCH PAGE - After navigating to page 2 (users 13-24) and clicking the Previous link the usernames we saved from page 1 do not match");

                // verify Next link
                quickSearch = quickSearch.Pagination_NextLink_Click();
                userNamesOnCurrentPage = quickSearch.GetAllNamesOfSearchResultsOnThisPage();

                doALLNamesFromPage1MatchPage2 = quickSearch.DoAllUserNamesMatchInOurLists(userNames13Through24, userNamesOnCurrentPage);
                Assert.IsTrue(doALLNamesFromPage1MatchPage2, "ERROR - QUICK SEARCH PAGE - After navigating to page 1 (users 1-12) and clicking the Next link the usernames we saved from page 2 do not match");

                // verify |< link
                quickSearch = quickSearch.Pagination_Page3_Click();
                quickSearch = quickSearch.Pagination_FirstPageLink_Click();
                userNamesOnCurrentPage = quickSearch.GetAllNamesOfSearchResultsOnThisPage();

                doALLNamesFromPage1MatchPage2 = quickSearch.DoAllUserNamesMatchInOurLists(userNames1Through12, userNamesOnCurrentPage);
                Assert.IsTrue(doALLNamesFromPage1MatchPage2, "ERROR - QUICK SEARCH PAGE - After navigating to page 3 (users 25-36) and clicking the |< link the usernames we saved from page 1 do not match");

                TearDown();

                log.Info("END TEST - JDate/Regression/Member/QuickSearchPagination");
            }
            catch (Exception e)
            {
                CleanupFailedTestcase(e);
                throw;
            }
        }

        /// <summary>
        /// Verify that the Secret Admirer "Y", "N" and "M" options are clickable.
        /// We don't verify that the user that has been secretly admired gets an email about it.
        /// </summary>
        [Test]
        public void Validate_SecretAdmirerOptionsAreClickable()
        {
            string errorLogNotes = string.Empty;

            try
            {
                log.Info("START TEST - JDate/Regression/Subscriber/SecretAdmirerOptionsAreClickable");

                Setup();

                StartTest_UsingJDate();

                string user1_login = System.Configuration.ConfigurationManager.AppSettings["JDate_RegisteredUser1_Email"];
                string user1_password = System.Configuration.ConfigurationManager.AppSettings["JDate_RegisteredUser1_Password"];
                string user1_Username = System.Configuration.ConfigurationManager.AppSettings["JDate_RegisteredUser1_Username"];

                string user2_MemberID = System.Configuration.ConfigurationManager.AppSettings["JDate_SubscribedUser1_MemberID"];

                // login as user 1 (a registered user)
                WebdriverAutomation.Tests.ReusableCode.LoggingInAndOut loggingInAndOut = new LoggingInAndOut();
                WebdriverAutomation.Framework.Site_Pages.Home home = loggingInAndOut.LogIn_ProfileComplete(driver, log, user1_login, user1_password);
                Framework.Site_Pages.QuickSearch quickSearch = home.Header.SearchHeaderLink_Click();

                // user 1 - search for user 2 (a subscribed user)
                Framework.Site_Pages.LookUpMember lookUpMember = quickSearch.Header.LookUpMemberSubHeaderLink_Click();

                lookUpMember.MemberNumberTextbox = user2_MemberID;
                Framework.Site_Pages.YourProfile yourProfile = lookUpMember.MemberNumberSearchButton_Click();

                yourProfile.SecretAdmirerYes_Click();
                IAlert alert = driver.SwitchTo().Alert();
                alert.Accept();

                yourProfile.SecretAdmirerNo_Click();
                alert = driver.SwitchTo().Alert();
                alert.Accept();

                yourProfile.SecretAdmirerMaybe_Click();
                alert = driver.SwitchTo().Alert();
                alert.Accept();

                loggingInAndOut.LogOut(driver, log);

                TearDown();

                log.Info("END TEST - JDate/Regression/Subscriber/SecretAdmirerOptionsAreClickable");
            }
            catch (Exception e)
            {
                CleanupFailedTestcase(e, errorLogNotes);
                throw;
            }
        }

        /// <summary>
        /// Verify that when you Favorite someone from their Profile that you appear in their "Favorited You" list when they log in.  We do not bother unfavoriting the person after we verify that the feature works because
        /// once a person is on your Favorited You list they never disappear, even if the original person who favorited you unfavorites you in the future.
        /// </summary>
        [Test]
        public void Validate_FavoriteCanBeSelectedAndUnselected()
        {
            string errorLogNotes = string.Empty;

            try
            {
                log.Info("START TEST - JDate/Regression/Subscriber/FavoriteCanBeSelectedAndUnselected");

                Setup();

                StartTest_UsingJDate();

                string user1_login = System.Configuration.ConfigurationManager.AppSettings["JDate_RegisteredUser1_Email"];
                string user1_password = System.Configuration.ConfigurationManager.AppSettings["JDate_RegisteredUser1_Password"];
                string user1_Username = System.Configuration.ConfigurationManager.AppSettings["JDate_RegisteredUser1_Username"];

                string user2_login = System.Configuration.ConfigurationManager.AppSettings["JDate_SubscribedUser1_Email"];
                string user2_password = System.Configuration.ConfigurationManager.AppSettings["JDate_SubscribedUser1_Password"];
                string user2_MemberID = System.Configuration.ConfigurationManager.AppSettings["JDate_SubscribedUser1_MemberID"];

                // login as user 1 (a registered user)
                WebdriverAutomation.Tests.ReusableCode.LoggingInAndOut loggingInAndOut = new LoggingInAndOut();
                WebdriverAutomation.Framework.Site_Pages.Home home = loggingInAndOut.LogIn_ProfileComplete(driver, log, user1_login, user1_password);
                Framework.Site_Pages.QuickSearch quickSearch = home.Header.SearchHeaderLink_Click();

                // user 1 - search for user 2 (a subscribed user)
                Framework.Site_Pages.LookUpMember lookUpMember = quickSearch.Header.LookUpMemberSubHeaderLink_Click();

                lookUpMember.MemberNumberTextbox = user2_MemberID;
                Framework.Site_Pages.YourProfile yourProfile = lookUpMember.MemberNumberSearchButton_Click();

                // we expect user 2 to already be a favorite of user 1. unfavorite user 2 and then favorite him again to make sure that the text that appears on the button is correct.
                if (yourProfile.IsUserCurrentlyAFavorite())
                    yourProfile.UnFavorite_Click();

                Assert.IsTrue(yourProfile.IsUserCurrentlyAFavorite() == false, "In the Favorite test case, user 2 should not currently be a favorite of user 1.  The button to favorite user 2 should be displayed as \"Favorite\" but is not.");

                yourProfile.Favorite_Click();

                Assert.IsTrue(yourProfile.IsUserCurrentlyAFavorite() == true, "In the Favorite test case, user 1 clicked on the Favorite button on user 2's Profile Page so now user 2 should be a favorite of user 1.  The button should now be displayed as \"★ Favorite\" but is not.");

                loggingInAndOut.LogOut(driver, log);

                TearDown();

                log.Info("END TEST - JDate/Regression/Subscriber/FavoriteCanBeSelectedAndUnselected");
            }
            catch (Exception e)
            {
                CleanupFailedTestcase(e, errorLogNotes);
                throw;
            }
        }

        //[Test]
        //public void Validate_CommunityLinks()
        //{
        //    string errorLogNotes = string.Empty;

        //    try
        //    {
        //        log.Info("START TEST - JDate/Regression/Subscriber/CommunityLinks");

        //        Setup();

        //        StartTest_UsingJDate();

        //        string user1_login = System.Configuration.ConfigurationManager.AppSettings["JDate_RegisteredUser1_Email"];
        //        string user1_password = System.Configuration.ConfigurationManager.AppSettings["JDate_RegisteredUser1_Password"];
        //        string user1_Username = System.Configuration.ConfigurationManager.AppSettings["JDate_RegisteredUser1_Username"];

        //        string user2_MemberID = System.Configuration.ConfigurationManager.AppSettings["JDate_SubscribedUser1_MemberID"];

        //        // login as user 1 (a registered user)
        //        WebdriverAutomation.Tests.ReusableCode.LoggingInAndOut loggingInAndOut = new LoggingInAndOut();
        //        WebdriverAutomation.Framework.Site_Pages.Home home = loggingInAndOut.LogIn_ProfileComplete(driver, log, user1_login, user1_password);

        //        Framework.Site_Pages.MembersOnline membersOnline = home.Header.MembersOnlineSubHeaderLink_Click();

        //        Framework.Site_Pages.KibitzCorner kibitzCorner = membersOnline.Header.KibitzCornerSubHeaderLink_Click();

        //        Framework.Site_Pages.SecretAdmirer secretAdmirer = kibitzCorner.Header.SecretAdmirerSubHeaderLink_Click();

        //        Framework.Site_Pages.ChatRooms chatRooms = secretAdmirer.Header.ChatRoomsSubHeaderLink_Click();

        //        Framework.Site_Pages.MessageBoards messageBoards = chatRooms.Header.MessageBoardsSubHeaderLink_Click();

        //        Framework.Site_Pages.ECards eCards = messageBoards.Header.ECardsSubHeaderLink_Click();

        //        Framework.Site_Pages.TravelAndEvents travelAndEvents = home.Header.TravelAndEventsSubHeaderLink_Click();

        //        Framework.Site_Pages.ReferAFriend referAFriend = travelAndEvents.Header.ReferAFriendSubHeaderLink_Click();

        //        TearDown();

        //        log.Info("END TEST - JDate/Regression/Subscriber/CommunityLinks");
        //    }
        //    catch (Exception e)
        //    {
        //        CleanupFailedTestcase(e, errorLogNotes);
        //        throw;
        //    }
        //}

        private int AgeOfUser(string birthdayMonth, string birthdayDay, string birthdayYear)
        {
            string myDateTimeString = string.Empty;
            myDateTimeString = birthdayDay + " " + birthdayMonth + "," + birthdayYear;

            DateTime birthday;
            birthday = Convert.ToDateTime(myDateTimeString);

            DateTime now = DateTime.Today;
            int age = now.Year - birthday.Year;
            if (birthday > now.AddYears(-age))
                age--;

            return age;
        }

        private string RemoveAllWhitespacesFromString(string originalString)
        {
            return Regex.Replace(originalString, @"\s", "");
        }

        private bool AreValuesAndCountEqual(List<string> list1, List<string> list2)
        {
            if (list1.Count != list2.Count)
                return false;

            for (int i = 0; i < list1.Count; i++)
            {
                if (list1[i] != list2[i])
                    return false;
            }

            return true;
        }

        private void RegisterNewUser()
        {
            WebdriverAutomation.Framework.Site_Pages.Splash splash = new WebdriverAutomation.Framework.Site_Pages.Splash(driver);   

            WebdriverAutomation.Framework.Site_Pages.Registration registration;

            #region JDate Registration Path B - Splash page has fields for You are a, Age Range, Country, Zip Code
            if (driver.PageSource.Contains("Splash Page Form No Email - GenderMask,AgeRange,RegionID"))
            {               
                string zipcode = "90210";

                splash.YouAreAList_Select(1);
                splash.AgeRangeList_Select(1);
                splash.ZipCodeTextbox_Write(zipcode);

                registration = splash.BrowseForFreeButton_Click();

                // page 1
                registration.WhatTypeOfRelationshipAreYouLookingForListBox_RegPathBandC_Click(3);
                registration.WhatTypeOfRelationshipAreYouLookingForListBox_RegPathBandC_Click(4);

                registration.WhatIsYourMaritalStatusDropdown_AltRegPage_Click(2);

                registration.ContinueButton_AltRegPage_Click();

                // page 2
                registration.YourSmokingHabitsListbox_AltRegPage_Select(3);
                registration.YourDrinkingHabitsListbox_AltRegPage_Select(4);
                registration.DoYouKeepKosher_RegPathBandC_Select(3);

                // page 3
                registration.YourEducationLevel_RegPathBandC_Select(5);
                registration.WhatIsYourOccupation_RegPathBandC_Select(2);
                registration.WhatIsYourEthnicity_RegPathBandC_Select(3);

                // page 4
                registration.WhatIsYourReligiousBackground_RegPathBandC_Select(5);
                registration.HowOftenDoYouGoToSynagogue_RegPathBandC_Select(3);

                // page 5
                registration.UserNameTextbox_Write("QAAutomation");

                registration.BirthDateMonthList_Select(4);
                registration.BirthDateDayList_Select(4);
                registration.BirthDateYearList_Select(4);

                registration.ContinueButton_AltRegPage_Click();

                // page 6
                string emailAddress = "QAAutomation_" + DateTime.Now.ToString() + "@gmail.com";
                emailAddress = emailAddress.Replace("/", "_").Replace(" ", "_").Replace(":", "_");
                registration.EmailAddressTextbox_Write(emailAddress);

                registration.ChooseYourPasswordTextbox_Write("Automation");

                registration.ContinueButton_AltRegPage_Click();

                // page 7
                string describeMyself = "food lover.  art lover.  dance lover.  music lover. ";
                registration.DescribeYourselfAndYourPersonalityTextArea_Write(describeMyself);

                registration.ContinueButton_AltRegPage_Click();

                // page 8 - commenting pictures out for now - we're getting too many profiles created with automation that aren't being deleted
                //// grab a random picture in our solution's file path and put it in the Upload a Photo textbox
                //string strFileName = string.Empty;
                //Random random = new Random();

                //FileInfo[] files = HelperMethods.GetReqFileInfo(@"*.jpg");
                //if (files.Length > 0)
                //    strFileName = files[random.Next(0, files.Length - 1)].FullName;

                //registration.UploadAPhotoTextbox_Write(strFileName);

                registration.ContinueButton_AltRegPage_Click();

                // page 9
                registration.PleaseEnterTheCodeShownBelow_Write("bananafish");

                //registration.IWouldLikeSpecialOffersAndAnnouncements_Click();

                registration.ContinueButton_AltRegPage_Click();

                // registration complete
                WebdriverAutomation.Framework.Site_Pages.RegistrationComplete registrationComplete = new WebdriverAutomation.Framework.Site_Pages.RegistrationComplete(driver);

            }
            #endregion
            #region JDate Registration Path C - Splash page has fields for You are a, Age Range, Country, Zip Code and Email address
            else if (driver.PageSource.Contains("Form w Email - GenderMask,AgeRange,RegionID,EmailAddress"))
            {
                string zipcode = "90210";

                splash.YouAreAList_Select(1);
                splash.AgeRangeList_Select(1);
                splash.ZipCodeTextbox_Write(zipcode);

                string emailAddress = "QAAutomation_" + DateTime.Now.ToString() + "@gmail.com";
                emailAddress = emailAddress.Replace("/", "_").Replace(" ", "_").Replace(":", "_");
                splash.EmailAddressTextbox_Write(emailAddress);

                registration = splash.BrowseForFreeButton_Click();

                // page 1
                registration.WhatTypeOfRelationshipAreYouLookingForListBox_RegPathBandC_Click(3);
                registration.WhatTypeOfRelationshipAreYouLookingForListBox_RegPathBandC_Click(4);

                registration.WhatIsYourMaritalStatusDropdown_AltRegPage_Click(2);

                registration.ContinueButton_AltRegPage_Click();

                // page 2
                registration.YourSmokingHabitsListbox_AltRegPage_Select(3);
                registration.YourDrinkingHabitsListbox_AltRegPage_Select(4);
                registration.DoYouKeepKosher_RegPathBandC_Select(3);

                // page 3
                registration.YourEducationLevel_RegPathBandC_Select(5);
                registration.WhatIsYourOccupation_RegPathBandC_Select(2);
                registration.WhatIsYourEthnicity_RegPathBandC_Select(3);

                // page 4
                registration.WhatIsYourReligiousBackground_RegPathBandC_Select(5);
                registration.HowOftenDoYouGoToSynagogue_RegPathBandC_Select(3);

                // page 5
                registration.UserNameTextbox_Write("QAAutomation");

                registration.BirthDateMonthList_Select(4);
                registration.BirthDateDayList_Select(4);
                registration.BirthDateYearList_Select(4);

                registration.ContinueButton_AltRegPage_Click();

                // page 6
                Assert.IsTrue(emailAddress == registration.EmailAddress, "In the Validate_Registrion test case for registration path C, the user enters in an email value in the Splash Screen then begins registration. During registration, the email value originally entered in should be repopulated here but is not.");
                registration.ChooseYourPasswordTextbox_Write("Automation");

                registration.ContinueButton_AltRegPage_Click();

                // page 7
                string describeMyself = "food lover.  art lover.  dance lover.  music lover. ";
                registration.DescribeYourselfAndYourPersonalityTextArea_Write(describeMyself);

                registration.ContinueButton_AltRegPage_Click();

                // page 8 - commenting pictures out for now - we're getting too many profiles created with automation that aren't being deleted
                //// grab a random picture in our solution's file path and put it in the Upload a Photo textbox
                //string strFileName = string.Empty;
                //Random random = new Random();

                //FileInfo[] files = HelperMethods.GetReqFileInfo(@"*.jpg");
                //if (files.Length > 0)
                //    strFileName = files[random.Next(0, files.Length - 1)].FullName;

                //registration.UploadAPhotoTextbox_Write(strFileName);

                registration.ContinueButton_AltRegPage_Click();

                // page 9
                registration.PleaseEnterTheCodeShownBelow_Write("bananafish");

                //registration.IWouldLikeSpecialOffersAndAnnouncements_Click();

                registration.ContinueButton_AltRegPage_Click();

                // registration complete
                WebdriverAutomation.Framework.Site_Pages.RegistrationComplete registrationComplete = new WebdriverAutomation.Framework.Site_Pages.RegistrationComplete(driver);

            }
            #endregion
            #region JDate - nothing on splash page (Splash Page Scenario 7) - user is taken to new Registration form with an overlay over a grayed out Your Matches Page
            else if (driver.Url.Contains("jdate.com") && driver.PageSource.Contains("s.pageName = \"Splash Page Scenario 7"))
            {
                registration = splash.BrowseForFreeLink_Click();

                string emailAddress = string.Empty;

                // page 1                 
                registration.IAmA_AltRegPage_Select(2);

                // page 2 
                registration.ZipCodeTextbox_Write("90210");
                registration.ContinueButton_AltRegPage_Click();

                // page 3
                registration.WhatTypeOfRelationshipAreYouLookingForListBox_RegPathBandC_Click(3);
                registration.WhatIsYourMaritalStatusDropdown_AltRegPage_Click(3);

                // page 4
                registration.YourSmokingHabitsDropdown_RegPathAandD_Select(2);
                registration.YourDrinkingHabitsDropdown_RegPathAandD_Select(2);
                registration.DoYouKeepKosher_RegPathBandC_Select(3);
                registration.ContinueButton_AltRegPage_Click();

                // page 5
                registration.YourEducationLevelListbox_AltRegPage_Select(4);
                registration.WhatIsYourOccupation_RegPathBandC_Select(2);
                registration.WhatIsYourEthnicity_RegPathBandC_Select(3);

                // page 6
                registration.WhatIsYourReligiousBackground_RegPathBandC_Select(2);
                registration.HowOftenDoYouGoToSynagogue_RegPathAandD_Select(3);

                // page 7
                registration.UserNameTextbox_Write("QAAutomation");

                registration.BirthDateMonthList_Select(4);
                registration.BirthDateDayList_Select(3);
                registration.BirthDateYearList_Select(5);
                registration.ContinueButton_AltRegPage_Click();

                // page 8 
                emailAddress = "QAAutomation_" + DateTime.Now.ToString() + "@gmail.com";
                emailAddress = emailAddress.Replace("/", "_").Replace(" ", "_").Replace(":", "_");
                registration.EmailAddressTextbox_Write(emailAddress);

                registration.ChooseYourPasswordTextbox_Write("automation");
                registration.ContinueButton_AltRegPage_Click();

                // page 9
                string describeMyself = "food lover.  art lover.  dance lover.  music lover. ";
                registration.DescribeYourselfAndYourPersonalityTextArea_Write(describeMyself);

                registration.ContinueButton_AltRegPage_Click();

                // page 10 - skipping pictures out for now - we're getting too many profiles created with automation that aren't being deleted
                registration.ContinueButton_AltRegPage_Click();

                // page 11 
                registration.FirstNameTextbox_JDate_Write("QAAutomation");

                registration.ContinueButton_AltRegPage_Click();

                // The registration complete page 
                WebdriverAutomation.Framework.Site_Pages.RegistrationComplete registrationComplete = new WebdriverAutomation.Framework.Site_Pages.RegistrationComplete(driver);

            }
            #endregion
            #region JDate - nothing on splash page (Splash Page Scenario 8) - user is taken to old Registration form
            else if (driver.Url.Contains("jdate.com") && driver.PageSource.Contains("s.pageName = \"Splash Page Scenario 8"))
            {
                registration = splash.BrowseForFreeLink_Click();

                // now we wade through the 8 billion different sub-scenarios to find out which reg path we take.  yay.
                if (driver.PageSource.Contains("s.eVar48 = (clearValue) ? \"\" : \"Scenario 9"))
                {
                    string emailAddress = string.Empty;
                    bool doEmailAndPasswordAppearFirst = false;

                    if (driver.PageSource.Contains("Registration Scenario 4 Step 1 - EmailAddress,Password"))
                        doEmailAndPasswordAppearFirst = true;

                    if (doEmailAndPasswordAppearFirst == true)
                    {
                        // page 1
                        emailAddress = "QAAutomation_" + DateTime.Now.ToString() + "@gmail.com";
                        emailAddress = emailAddress.Replace("/", "_").Replace(" ", "_").Replace(":", "_");
                        registration.EmailAddressTextbox_Write(emailAddress);

                        registration.ChooseYourPasswordTextbox_Write("Automation");

                        registration.ContinueButton_ClickUntilProgressBarChanges();
                    }

                    // page 2
                    registration.YouAreAListBox_RegPathAandD_Select(3);
                    registration.ContinueButton_ClickUntilProgressBarChanges();

                    // page 3
                    registration.ZipCodeTextbox_Write("90210");
                    registration.ContinueButton_OnZipCodePage_Click();

                    // page 4
                    registration.WhatTypeOfRelationshipAreYouLookingFor_Click(3);
                    registration.WhatTypeOfRelationshipAreYouLookingFor_Click(4);

                    registration.WhatIsYourCurrentRelationshipStatus_RegPathAandD_Select(2);

                    registration.ContinueButton_ClickUntilProgressBarChanges();

                    // page 5
                    registration.YourSmokingHabitsDropdown_RegPathAandD_Select(3);
                    registration.YourDrinkingHabitsDropdown_RegPathAandD_Select(4);
                    registration.DoYouKeepKosherScenarioDropdown_RegPathAandD_Select(3);
                    registration.ContinueButton_ClickUntilProgressBarChanges();

                    // page 6
                    registration.YourEducationLevel_RegPathBandC_Select(5);
                    registration.WhatDoYouDo_RegPathAandD_Select(2);
                    registration.YourEthnicity_RegPathAandD_Select(3);
                    registration.ContinueButton_ClickUntilProgressBarChanges();

                    // page 7
                    registration.YourReligiousBackground_RegPathAandD_Select(5);
                    registration.HowOftenDoYouGoToSynagogue_RegPathAandD_Select(3);
                    registration.ContinueButton_ClickUntilProgressBarChanges();

                    // page 8
                    string describeMyself = "food lover.  art lover.  dance lover.  music lover. ";
                    registration.DescribeYourselfAndYourPersonalityTextArea_Write(describeMyself);

                    registration.ContinueButton_ClickUntilProgressBarChanges();

                    // page 9
                    registration.UserNameTextbox_Write("QAAutomation");

                    registration.BirthDateMonthList_Select(4);
                    registration.BirthDateDayList_Select(4);
                    registration.BirthDateYearList_Select(4);

                    registration.ContinueButton_ClickUntilProgressBarChanges();

                    // page 10
                    if (doEmailAndPasswordAppearFirst == false)
                    {
                        emailAddress = "QAAutomation_" + DateTime.Now.ToString() + "@gmail.com";
                        emailAddress = emailAddress.Replace("/", "_").Replace(" ", "_").Replace(":", "_");
                        registration.EmailAddressTextbox_Write(emailAddress);

                        registration.ChooseYourPasswordTextbox_Write("Automation");

                        registration.ContinueButton_ClickUntilProgressBarChanges();
                    }

                    // page 11 - commenting pictures out for now - we're getting too many profiles created with automation that aren't being deleted
                    //// grab a random picture in our solution's file path and put it in the Upload a Photo textbox
                    //string strFileName = string.Empty;
                    //Random random = new Random();

                    //FileInfo[] files = HelperMethods.GetReqFileInfo(@"*.jpg");
                    //if (files.Length > 0)
                    //    strFileName = files[random.Next(0, files.Length - 1)].FullName;

                    //registration.UploadAPhotoTextbox_Write(strFileName);

                    registration.ContinueButton_ClickUntilProgressBarChanges();

                    // page 12        
                    //// CATPCHA is removed from JDate 8/1/11
                    //registration.PleaseEnterTheCodeShownBelow_Write("bananafish");

                    registration.IWouldLikeSpecialOffersAndAnnouncements_Click();

                    if (registration.DoesFirstNameAppearOnRegistrationScreen())
                    {
                        // now at the end of registration with the header "Create Your Profile - Lifestyle" still showing
                        string firstName = "Tommy";

                        registration.FirstNameTextbox_JDate_Write(firstName);
                        registration.ContinueButton_Click();

                        // at this point either the Registration Complete page appears OR the Subscribe Page appears.  either one is okay.
                        Thread.Sleep(3000);
                        string currentURL = driver.Url;

                        if (currentURL.Contains("RegistrationWelcome"))
                        {
                            // The registration complete page
                            WebdriverAutomation.Framework.Site_Pages.RegistrationComplete registrationComplete = new WebdriverAutomation.Framework.Site_Pages.RegistrationComplete(driver);
                        }
                        else
                        {
                            // The Subscribe page
                            WebdriverAutomation.Framework.Site_Pages.Subscribe subscribe = new WebdriverAutomation.Framework.Site_Pages.Subscribe(driver);
                        }
                    }
                    else
                    {
                        registration.ContinueButton_Click();

                        // The registration complete page 
                        WebdriverAutomation.Framework.Site_Pages.RegistrationComplete registrationComplete = new WebdriverAutomation.Framework.Site_Pages.RegistrationComplete(driver);

                        // Attempt to go to the Home Page and verify that it takes the user to the Please Update Your Profile page instead
                        driver.Navigate().GoToUrl(System.Configuration.ConfigurationManager.AppSettings["JDate"]);

                        string firstName = "Tommy";

                        WebdriverAutomation.Framework.Site_Pages.PleaseUpdateYourProfile pleaseUpdateYourProfile = new WebdriverAutomation.Framework.Site_Pages.PleaseUpdateYourProfile(driver);
                        pleaseUpdateYourProfile.EnterYourFirstNameTextbox_Write(firstName);
                        WebdriverAutomation.Framework.Site_Pages.Home home = pleaseUpdateYourProfile.SaveButton_Click();
                    }
                }
                else if (driver.PageSource.Contains("s.eVar48 = (clearValue) ? \"\" : \"Scenario 10"))
                {
                    string emailAddress = string.Empty;
                    bool doEmailAndPasswordAppearFirst = false;

                    if (driver.PageSource.Contains("Registration Scenario 4 Step 1 - EmailAddress,Password"))
                        doEmailAndPasswordAppearFirst = true;

                    if (doEmailAndPasswordAppearFirst == true)
                    {
                        // page 1
                        emailAddress = "QAAutomation_" + DateTime.Now.ToString() + "@gmail.com";
                        emailAddress = emailAddress.Replace("/", "_").Replace(" ", "_").Replace(":", "_");
                        registration.EmailAddressTextbox_Write(emailAddress);

                        registration.ChooseYourPasswordTextbox_Write("Automation");

                        registration.ContinueButton_ClickUntilProgressBarChanges();
                    }

                    // page 2
                    registration.YouAreAListBox_RegPathAandD_Select(3);
                    registration.WhatIsYourCurrentRelationshipStatus_RegPathAandD_Select(2);
                    registration.ContinueButton_ClickUntilProgressBarChanges();

                    // page 3
                    registration.ZipCodeTextbox_Write("90210");
                    registration.ContinueButton_OnZipCodePage_Click();

                    // page 4
                    registration.YourSmokingHabitsDropdown_RegPathAandD_Select(2);
                    registration.WhatIsYourHeight_Select(3);
                    registration.ContinueButton_ClickUntilProgressBarChanges();

                    // page 5
                    registration.YourEducationLevel_RegPathBandC_Select(5);
                    registration.WhatDoYouDo_RegPathAandD_Select(2);
                    registration.ContinueButton_ClickUntilProgressBarChanges();

                    // page 6
                    registration.YourReligiousBackground_RegPathAandD_Select(5);
                    registration.HowOftenDoYouGoToSynagogue_RegPathAandD_Select(3);
                    registration.ContinueButton_ClickUntilProgressBarChanges();

                    // page 7
                    registration.HowManyChildrenDoYouHaveListbox_Select(2);
                    registration.DoYouWantChildrenDropdown_Select(2);
                    registration.ContinueButton_ClickUntilProgressBarChanges();

                    // page 8
                    registration.FirstNameTextbox_JDate_Write("Tommy");

                    registration.UserNameTextbox_Write("QAAutomation");

                    registration.BirthDateMonthList_Select(4);
                    registration.BirthDateDayList_Select(4);
                    registration.BirthDateYearList_Select(4);

                    registration.ContinueButton_ClickUntilProgressBarChanges();

                    // page 8
                    emailAddress = "QAAutomation_" + DateTime.Now.ToString() + "@gmail.com";
                    emailAddress = emailAddress.Replace("/", "_").Replace(" ", "_").Replace(":", "_");
                    registration.EmailAddressTextbox_Write(emailAddress);

                    registration.ChooseYourPasswordTextbox_Write("Automation");

                    registration.ContinueButton_Click();

                    // The registration complete page
                    WebdriverAutomation.Framework.Site_Pages.RegistrationComplete registrationComplete = new WebdriverAutomation.Framework.Site_Pages.RegistrationComplete(driver);

                }
                else if (driver.PageSource.Contains("s.eVar48 = (clearValue) ? \"\" : \"Scenario 11"))
                {
                    string emailAddress = string.Empty;
                    bool doEmailAndPasswordAppearFirst = false;

                    if (driver.PageSource.Contains("Registration Scenario 4 Step 1 - EmailAddress,Password"))
                        doEmailAndPasswordAppearFirst = true;

                    if (doEmailAndPasswordAppearFirst == true)
                    {
                        // page 1
                        emailAddress = "QAAutomation_" + DateTime.Now.ToString() + "@gmail.com";
                        emailAddress = emailAddress.Replace("/", "_").Replace(" ", "_").Replace(":", "_");
                        registration.EmailAddressTextbox_Write(emailAddress);

                        registration.ChooseYourPasswordTextbox_Write("Automation");

                        registration.ContinueButton_ClickUntilProgressBarChanges();
                    }

                    // page 2
                    registration.YouAreAListBox_RegPathAandD_Select(3);
                    registration.WhatIsYourCurrentRelationshipStatus_RegPathAandD_Select(2);
                    registration.ContinueButton_ClickUntilProgressBarChanges();

                    // page 3
                    registration.ZipCodeTextbox_Write("90210");
                    registration.ContinueButton_OnZipCodePage_Click();

                    // page 4
                    registration.YourSmokingHabitsDropdown_RegPathAandD_Select(2);
                    registration.ContinueButton_ClickUntilProgressBarChanges();

                    // page 5
                    registration.YourEducationLevel_RegPathBandC_Select(5);
                    registration.WhatDoYouDo_RegPathAandD_Select(2);
                    registration.ContinueButton_ClickUntilProgressBarChanges();

                    // page 6
                    registration.YourReligiousBackground_RegPathAandD_Select(5);
                    registration.HowOftenDoYouGoToSynagogue_RegPathAandD_Select(3);
                    registration.ContinueButton_ClickUntilProgressBarChanges();

                    // page 7
                    registration.FirstNameTextbox_JDate_Write("Tommy");

                    registration.UserNameTextbox_Write("QAAutomation");

                    registration.BirthDateMonthList_Select(4);
                    registration.BirthDateDayList_Select(4);
                    registration.BirthDateYearList_Select(4);

                    registration.ContinueButton_ClickUntilProgressBarChanges();

                    // page 8
                    emailAddress = "QAAutomation_" + DateTime.Now.ToString() + "@gmail.com";
                    emailAddress = emailAddress.Replace("/", "_").Replace(" ", "_").Replace(":", "_");
                    registration.EmailAddressTextbox_Write(emailAddress);

                    registration.ChooseYourPasswordTextbox_Write("Automation");

                    registration.ContinueButton_Click();

                    // The registration complete page 
                    WebdriverAutomation.Framework.Site_Pages.RegistrationComplete registrationComplete = new WebdriverAutomation.Framework.Site_Pages.RegistrationComplete(driver);

                }
                else if (driver.PageSource.Contains("s.eVar48 = (clearValue) ? \"\" : \"1 – Test – 8 steps\""))
                {
                    string emailAddress = string.Empty;
                    bool doEmailAndPasswordAppearFirst = false;

                    if (driver.PageSource.Contains("Registration Scenario 4 Step 1 - EmailAddress,Password"))
                        doEmailAndPasswordAppearFirst = true;

                    if (doEmailAndPasswordAppearFirst == true)
                    {
                        // page 1
                        emailAddress = "QAAutomation_" + DateTime.Now.ToString() + "@gmail.com";
                        emailAddress = emailAddress.Replace("/", "_").Replace(" ", "_").Replace(":", "_");
                        registration.EmailAddressTextbox_Write(emailAddress);

                        registration.ChooseYourPasswordTextbox_Write("Automation");

                        registration.ContinueButton_ClickUntilProgressBarChanges();
                    }

                    // page 2
                    registration.YouAreAListBox_RegPathAandD_Select(3);
                    registration.ContinueButton_ClickUntilProgressBarChanges();

                    // page 3
                    registration.ZipCodeTextbox_Write("90210");
                    registration.ContinueButton_OnZipCodePage_Click();

                    // page 4
                    registration.WhatIsYourCurrentRelationshipStatus_RegPathAandD_Select(2);
                    registration.ContinueButton_ClickUntilProgressBarChanges();

                    // page 5
                    registration.YourSmokingHabitsDropdown_RegPathAandD_Select(3);
                    registration.ContinueButton_ClickUntilProgressBarChanges();

                    // page 6
                    registration.YourEducationLevel_RegPathBandC_Select(5);
                    registration.WhatDoYouDo_RegPathAandD_Select(2);
                    registration.ContinueButton_ClickUntilProgressBarChanges();

                    // page 7
                    registration.YourReligiousBackground_RegPathAandD_Select(5);
                    registration.HowOftenDoYouGoToSynagogue_RegPathAandD_Select(3);
                    registration.ContinueButton_ClickUntilProgressBarChanges();

                    // page 8
                    registration.FirstNameTextbox_JDate_Write("Tommy");
                    registration.UserNameTextbox_Write("QAAutomation");

                    registration.BirthDateMonthList_Select(4);
                    registration.BirthDateDayList_Select(4);
                    registration.BirthDateYearList_Select(4);

                    registration.ContinueButton_ClickUntilProgressBarChanges();

                    // page 9
                    if (doEmailAndPasswordAppearFirst == false)
                    {
                        emailAddress = "QAAutomation_" + DateTime.Now.ToString() + "@gmail.com";
                        emailAddress = emailAddress.Replace("/", "_").Replace(" ", "_").Replace(":", "_");
                        registration.EmailAddressTextbox_Write(emailAddress);

                        registration.ChooseYourPasswordTextbox_Write("Automation");

                        registration.ContinueButton_Click();
                    }

                    // The registration complete page 
                    WebdriverAutomation.Framework.Site_Pages.RegistrationComplete registrationComplete = new WebdriverAutomation.Framework.Site_Pages.RegistrationComplete(driver);

                }
                else if (driver.PageSource.Contains("s.eVar48 = (clearValue) ? \"\" : \"1 – Test – 9 steps w kids"))
                {
                    string emailAddress = string.Empty;
                    bool doEmailAndPasswordAppearFirst = false;

                    if (driver.PageSource.Contains("Registration Scenario 4 Step 1 - EmailAddress,Password"))
                        doEmailAndPasswordAppearFirst = true;

                    if (doEmailAndPasswordAppearFirst == true)
                    {
                        // page 1
                        emailAddress = "QAAutomation_" + DateTime.Now.ToString() + "@gmail.com";
                        emailAddress = emailAddress.Replace("/", "_").Replace(" ", "_").Replace(":", "_");
                        registration.EmailAddressTextbox_Write(emailAddress);

                        registration.ChooseYourPasswordTextbox_Write("Automation");

                        registration.ContinueButton_ClickUntilProgressBarChanges();
                    }

                    // page 2
                    registration.YouAreAListBox_RegPathAandD_Select(3);
                    registration.ContinueButton_ClickUntilProgressBarChanges();

                    // page 3
                    registration.ZipCodeTextbox_Write("90210");
                    registration.ContinueButton_OnZipCodePage_Click();

                    // page 4
                    registration.WhatIsYourCurrentRelationshipStatus_RegPathAandD_Select(2);
                    registration.ContinueButton_ClickUntilProgressBarChanges();

                    // page 5
                    registration.YourSmokingHabitsDropdown_RegPathAandD_Select(2);
                    registration.WhatIsYourHeight_Select(2);
                    registration.ContinueButton_ClickUntilProgressBarChanges();

                    // page 6
                    registration.YourEducationLevel_RegPathBandC_Select(5);
                    registration.WhatDoYouDo_RegPathAandD_Select(2);
                    registration.ContinueButton_ClickUntilProgressBarChanges();

                    // page 7
                    registration.YourReligiousBackground_RegPathAandD_Select(5);
                    registration.HowOftenDoYouGoToSynagogue_RegPathAandD_Select(3);
                    registration.ContinueButton_ClickUntilProgressBarChanges();

                    // page 8
                    registration.HowManyChildrenDoYouHaveListbox_Select(2);
                    registration.DoYouWantChildrenDropdown_Select(2);
                    registration.ContinueButton_ClickUntilProgressBarChanges();

                    // page 9
                    registration.FirstNameTextbox_JDate_Write("Tommy");

                    registration.UserNameTextbox_Write("QAAutomation");

                    registration.BirthDateMonthList_Select(4);
                    registration.BirthDateDayList_Select(4);
                    registration.BirthDateYearList_Select(4);

                    registration.ContinueButton_ClickUntilProgressBarChanges();

                    // page 10
                    emailAddress = "QAAutomation_" + DateTime.Now.ToString() + "@gmail.com";
                    emailAddress = emailAddress.Replace("/", "_").Replace(" ", "_").Replace(":", "_");
                    registration.EmailAddressTextbox_Write(emailAddress);

                    registration.ChooseYourPasswordTextbox_Write("Automation");

                    registration.ContinueButton_Click();

                    // The registration complete page 
                    WebdriverAutomation.Framework.Site_Pages.RegistrationComplete registrationComplete = new WebdriverAutomation.Framework.Site_Pages.RegistrationComplete(driver);

                }
                else if (driver.PageSource.Contains("s.eVar48 = (clearValue) ? \"\" : \"1 – Control - 11 steps\""))
                {
                    string emailAddress = string.Empty;
                    bool doEmailAndPasswordAppearFirst = false;

                    if (driver.PageSource.Contains("Registration Scenario 4 Step 1 - EmailAddress,Password"))
                        doEmailAndPasswordAppearFirst = true;

                    if (doEmailAndPasswordAppearFirst == true)
                    {
                        // page 1
                        emailAddress = "QAAutomation_" + DateTime.Now.ToString() + "@gmail.com";
                        emailAddress = emailAddress.Replace("/", "_").Replace(" ", "_").Replace(":", "_");
                        registration.EmailAddressTextbox_Write(emailAddress);

                        registration.ChooseYourPasswordTextbox_Write("Automation");

                        registration.ContinueButton_ClickUntilProgressBarChanges();
                    }

                    // page 2
                    registration.YouAreAListBox_RegPathAandD_Select(3);
                    registration.ContinueButton_ClickUntilProgressBarChanges();

                    // page 3
                    registration.ZipCodeTextbox_Write("90210");
                    registration.ContinueButton_OnZipCodePage_Click();

                    // page 4
                    registration.WhatTypeOfRelationshipAreYouLookingFor_Click(3);
                    registration.WhatIsYourCurrentRelationshipStatus_RegPathAandD_Select(2);
                    registration.ContinueButton_ClickUntilProgressBarChanges();

                    // page 5
                    registration.YourSmokingHabitsDropdown_RegPathAandD_Select(2);
                    registration.YourDrinkingHabitsDropdown_RegPathAandD_Select(2);
                    registration.DoYouKeepKosher_RegPathBandC_Select(2);
                    registration.ContinueButton_ClickUntilProgressBarChanges();

                    // page 6
                    registration.YourEducationLevel_RegPathBandC_Select(5);
                    registration.WhatDoYouDo_RegPathAandD_Select(2);
                    registration.WhatIsYourEthnicity_RegPathBandC_Select(2);
                    registration.ContinueButton_ClickUntilProgressBarChanges();

                    // page 7
                    registration.YourReligiousBackground_RegPathAandD_Select(5);
                    registration.HowOftenDoYouGoToSynagogue_RegPathAandD_Select(3);
                    registration.ContinueButton_ClickUntilProgressBarChanges();

                    // page 8                    
                    string describeMyself = "food lover.  art lover.  dance lover.  music lover. ";
                    registration.DescribeYourselfAndYourPersonalityTextArea_Write(describeMyself);

                    registration.ContinueButton_ClickUntilProgressBarChanges();

                    // page 9
                    registration.UserNameTextbox_Write("QAAutomation");

                    registration.BirthDateMonthList_Select(4);
                    registration.BirthDateDayList_Select(4);
                    registration.BirthDateYearList_Select(4);

                    registration.ContinueButton_ClickUntilProgressBarChanges();

                    // page 10
                    emailAddress = "QAAutomation_" + DateTime.Now.ToString() + "@gmail.com";
                    emailAddress = emailAddress.Replace("/", "_").Replace(" ", "_").Replace(":", "_");
                    registration.EmailAddressTextbox_Write(emailAddress);

                    registration.ChooseYourPasswordTextbox_Write("Automation");

                    registration.ContinueButton_ClickUntilProgressBarChanges();

                    // page 11 - commenting pictures out for now - we're getting too many profiles created with automation that aren't being deleted
                    //// grab a random picture in our solution's file path and put it in the Upload a Photo textbox
                    //string strFileName = string.Empty;
                    //Random random = new Random();

                    //FileInfo[] files = HelperMethods.GetReqFileInfo(@"*.jpg");
                    //if (files.Length > 0)
                    //    strFileName = files[random.Next(0, files.Length - 1)].FullName;

                    //registration.UploadAPhotoTextbox_Write(strFileName);

                    registration.ContinueButton_ClickUntilProgressBarChanges();

                    // page 12
                    registration.FirstNameTextbox_JDate_Write("Tommy");
                    registration.ContinueButton_Click();

                    // The registration complete page 
                    WebdriverAutomation.Framework.Site_Pages.RegistrationComplete registrationComplete = new WebdriverAutomation.Framework.Site_Pages.RegistrationComplete(driver);

                }
                else
                    Assert.Fail("In the JDate Registration case, within Registration Scenario 8 we do not recognize the corrent sub-registration flow we need to take.  The s.eVar48 variable currently on the pagesource is not familiar to us.");

            }
            #endregion
            #region JDate - nothing on splash page (Splash Page Scenario 9) - user is taken to old Registration form
            else if (driver.Url.Contains("jdate.com") && driver.PageSource.Contains("s.pageName = \"Splash Page Scenario 9"))
            {
                registration = splash.BrowseForFreeLink_Click();

                if (driver.PageSource.Contains("s.eVar48 = (clearValue) ? \"\" : \"1 – Test – 8 steps\""))
                {
                    string emailAddress = string.Empty;
                    bool doEmailAndPasswordAppearFirst = false;

                    if (driver.PageSource.Contains("Registration Scenario 4 Step 1 - EmailAddress,Password"))
                        doEmailAndPasswordAppearFirst = true;

                    if (doEmailAndPasswordAppearFirst == true)
                    {
                        // page 1
                        emailAddress = "QAAutomation_" + DateTime.Now.ToString() + "@gmail.com";
                        emailAddress = emailAddress.Replace("/", "_").Replace(" ", "_").Replace(":", "_");
                        registration.EmailAddressTextbox_Write(emailAddress);

                        registration.ChooseYourPasswordTextbox_Write("Automation");

                        registration.ContinueButton_ClickUntilProgressBarChanges();
                    }

                    // page 2
                    registration.YouAreAListBox_RegPathAandD_Select(3);
                    registration.ContinueButton_ClickUntilProgressBarChanges();

                    // page 3
                    registration.ZipCodeTextbox_Write("90210");
                    registration.ContinueButton_OnZipCodePage_Click();

                    // page 4
                    registration.WhatIsYourCurrentRelationshipStatus_RegPathAandD_Select(2);
                    registration.ContinueButton_ClickUntilProgressBarChanges();

                    // page 5
                    registration.YourSmokingHabitsDropdown_RegPathAandD_Select(3);
                    registration.ContinueButton_ClickUntilProgressBarChanges();

                    // page 6
                    registration.YourEducationLevel_RegPathBandC_Select(5);
                    registration.WhatDoYouDo_RegPathAandD_Select(2);
                    registration.ContinueButton_ClickUntilProgressBarChanges();

                    // page 7
                    registration.YourReligiousBackground_RegPathAandD_Select(5);
                    registration.HowOftenDoYouGoToSynagogue_RegPathAandD_Select(3);
                    registration.ContinueButton_ClickUntilProgressBarChanges();

                    // page 8
                    registration.FirstNameTextbox_JDate_Write("Tommy");
                    registration.UserNameTextbox_Write("QAAutomation");

                    registration.BirthDateMonthList_Select(4);
                    registration.BirthDateDayList_Select(4);
                    registration.BirthDateYearList_Select(4);

                    registration.ContinueButton_ClickUntilProgressBarChanges();

                    // page 9
                    if (doEmailAndPasswordAppearFirst == false)
                    {
                        emailAddress = "QAAutomation_" + DateTime.Now.ToString() + "@gmail.com";
                        emailAddress = emailAddress.Replace("/", "_").Replace(" ", "_").Replace(":", "_");
                        registration.EmailAddressTextbox_Write(emailAddress);

                        registration.ChooseYourPasswordTextbox_Write("Automation");

                        registration.ContinueButton_Click();
                    }

                    // The registration complete page 
                    WebdriverAutomation.Framework.Site_Pages.RegistrationComplete registrationComplete = new WebdriverAutomation.Framework.Site_Pages.RegistrationComplete(driver);

                }
                else if (driver.PageSource.Contains("s.eVar48 = (clearValue) ? \"\" : \"1 – Test – 9 steps w kids"))
                {
                    string emailAddress = string.Empty;
                    bool doEmailAndPasswordAppearFirst = false;

                    if (driver.PageSource.Contains("Registration Scenario 4 Step 1 - EmailAddress,Password"))
                        doEmailAndPasswordAppearFirst = true;

                    if (doEmailAndPasswordAppearFirst == true)
                    {
                        // page 1
                        emailAddress = "QAAutomation_" + DateTime.Now.ToString() + "@gmail.com";
                        emailAddress = emailAddress.Replace("/", "_").Replace(" ", "_").Replace(":", "_");
                        registration.EmailAddressTextbox_Write(emailAddress);

                        registration.ChooseYourPasswordTextbox_Write("Automation");

                        registration.ContinueButton_ClickUntilProgressBarChanges();
                    }

                    // page 2
                    registration.YouAreAListBox_RegPathAandD_Select(3);
                    registration.ContinueButton_ClickUntilProgressBarChanges();

                    // page 3
                    registration.ZipCodeTextbox_Write("90210");
                    registration.ContinueButton_OnZipCodePage_Click();

                    // page 4
                    registration.WhatIsYourCurrentRelationshipStatus_RegPathAandD_Select(2);
                    registration.ContinueButton_ClickUntilProgressBarChanges();

                    // page 5
                    registration.YourSmokingHabitsDropdown_RegPathAandD_Select(2);
                    registration.WhatIsYourHeight_Select(2);
                    registration.ContinueButton_ClickUntilProgressBarChanges();

                    // page 6
                    registration.YourEducationLevel_RegPathBandC_Select(5);
                    registration.WhatDoYouDo_RegPathAandD_Select(2);
                    registration.ContinueButton_ClickUntilProgressBarChanges();

                    // page 7
                    registration.YourReligiousBackground_RegPathAandD_Select(5);
                    registration.HowOftenDoYouGoToSynagogue_RegPathAandD_Select(3);
                    registration.ContinueButton_ClickUntilProgressBarChanges();

                    // page 8
                    registration.HowManyChildrenDoYouHaveListbox_Select(2);
                    registration.DoYouWantChildrenDropdown_Select(2);
                    registration.ContinueButton_ClickUntilProgressBarChanges();

                    // page 9
                    registration.FirstNameTextbox_JDate_Write("Tommy");

                    registration.UserNameTextbox_Write("QAAutomation");

                    registration.BirthDateMonthList_Select(4);
                    registration.BirthDateDayList_Select(4);
                    registration.BirthDateYearList_Select(4);

                    registration.ContinueButton_ClickUntilProgressBarChanges();

                    // page 10
                    emailAddress = "QAAutomation_" + DateTime.Now.ToString() + "@gmail.com";
                    emailAddress = emailAddress.Replace("/", "_").Replace(" ", "_").Replace(":", "_");
                    registration.EmailAddressTextbox_Write(emailAddress);

                    registration.ChooseYourPasswordTextbox_Write("Automation");

                    registration.ContinueButton_Click();

                    // The registration complete page 
                    WebdriverAutomation.Framework.Site_Pages.RegistrationComplete registrationComplete = new WebdriverAutomation.Framework.Site_Pages.RegistrationComplete(driver);

                }
                else if (driver.PageSource.Contains("s.eVar48 = (clearValue) ? \"\" : \"1 – Control - 11 steps\""))
                {
                    string emailAddress = string.Empty;
                    bool doEmailAndPasswordAppearFirst = false;

                    if (driver.PageSource.Contains("Registration Scenario 4 Step 1 - EmailAddress,Password"))
                        doEmailAndPasswordAppearFirst = true;

                    if (doEmailAndPasswordAppearFirst == true)
                    {
                        // page 1
                        emailAddress = "QAAutomation_" + DateTime.Now.ToString() + "@gmail.com";
                        emailAddress = emailAddress.Replace("/", "_").Replace(" ", "_").Replace(":", "_");
                        registration.EmailAddressTextbox_Write(emailAddress);

                        registration.ChooseYourPasswordTextbox_Write("Automation");

                        registration.ContinueButton_ClickUntilProgressBarChanges();
                    }

                    // page 2
                    registration.YouAreAListBox_RegPathAandD_Select(3);
                    registration.ContinueButton_ClickUntilProgressBarChanges();

                    // page 3
                    registration.ZipCodeTextbox_Write("90210");
                    registration.ContinueButton_OnZipCodePage_Click();

                    // page 4
                    registration.WhatTypeOfRelationshipAreYouLookingFor_Click(2);
                    registration.WhatIsYourCurrentRelationshipStatus_RegPathAandD_Select(2);
                    registration.ContinueButton_ClickUntilProgressBarChanges();

                    // page 5
                    registration.YourSmokingHabitsDropdown_RegPathAandD_Select(3);
                    registration.YourDrinkingHabitsDropdown_RegPathAandD_Select(2);
                    registration.DoYouKeepKosher_RegPathBandC_Select(2);
                    registration.ContinueButton_ClickUntilProgressBarChanges();

                    // page 6
                    registration.YourEducationLevel_RegPathBandC_Select(5);
                    registration.WhatDoYouDo_RegPathAandD_Select(2);
                    registration.WhatIsYourEthnicity_RegPathBandC_Select(2);
                    registration.ContinueButton_ClickUntilProgressBarChanges();

                    // page 7
                    registration.YourReligiousBackground_RegPathAandD_Select(5);
                    registration.HowOftenDoYouGoToSynagogue_RegPathAandD_Select(3);
                    registration.ContinueButton_ClickUntilProgressBarChanges();

                    // page 8
                    string describeMyself = "food lover.  art lover.  dance lover.  music lover. ";
                    registration.DescribeYourselfAndYourPersonalityTextArea_Write(describeMyself);

                    registration.ContinueButton_ClickUntilProgressBarChanges();

                    // page 9
                    registration.UserNameTextbox_Write("QAAutomation");

                    registration.BirthDateMonthList_Select(4);
                    registration.BirthDateDayList_Select(4);
                    registration.BirthDateYearList_Select(4);

                    registration.ContinueButton_ClickUntilProgressBarChanges();

                    // page 10
                    emailAddress = "QAAutomation_" + DateTime.Now.ToString() + "@gmail.com";
                    emailAddress = emailAddress.Replace("/", "_").Replace(" ", "_").Replace(":", "_");
                    registration.EmailAddressTextbox_Write(emailAddress);

                    registration.ChooseYourPasswordTextbox_Write("Automation");

                    registration.ContinueButton_ClickUntilProgressBarChanges();

                    // page 11 - commenting pictures out for now - we're getting too many profiles created with automation that aren't being deleted
                    //// grab a random picture in our solution's file path and put it in the Upload a Photo textbox
                    //string strFileName = string.Empty;
                    //Random random = new Random();

                    //FileInfo[] files = HelperMethods.GetReqFileInfo(@"*.jpg");
                    //if (files.Length > 0)
                    //    strFileName = files[random.Next(0, files.Length - 1)].FullName;

                    //registration.UploadAPhotoTextbox_Write(strFileName);

                    registration.ContinueButton_ClickUntilProgressBarChanges();

                    // page 12
                    registration.FirstNameTextbox_JDate_Write("Tommy");
                    registration.ContinueButton_Click();

                    // The registration complete page 
                    WebdriverAutomation.Framework.Site_Pages.RegistrationComplete registrationComplete = new WebdriverAutomation.Framework.Site_Pages.RegistrationComplete(driver);

                }
                else
                    Assert.Fail("In the JDate Registration case, within Registration Scenario 9 we do not recognize the corrent sub-registration flow we need to take.  The s.eVar48 variable currently on the pagesource is not familiar to us.");

            }
            #endregion
            #region JDate - nothing on splash page (Splash Page Test8Steps - ) - user is taken to old Registration form
            else if (driver.Url.Contains("jdate.com") && driver.PageSource.Contains("s.pageName = \"Splash Page Test8Steps - "))
            {
                registration = splash.BrowseForFreeLink_Click();

                string emailAddress = string.Empty;
                bool doEmailAndPasswordAppearFirst = false;

                if (driver.PageSource.Contains("Registration Scenario 4 Step 1 - EmailAddress,Password"))
                    doEmailAndPasswordAppearFirst = true;

                if (doEmailAndPasswordAppearFirst == true)
                {
                    // page 1
                    emailAddress = "QAAutomation_" + DateTime.Now.ToString() + "@gmail.com";
                    emailAddress = emailAddress.Replace("/", "_").Replace(" ", "_").Replace(":", "_");
                    errorLogNotes = "Email address used for registration: " + emailAddress;
                    registration.EmailAddressTextbox_Write(emailAddress);

                    registration.ChooseYourPasswordTextbox_Write("Automation");

                    registration.ContinueButton_ClickUntilProgressBarChanges();
                }

                // page 2
                registration.YouAreAListBox_RegPathAandD_Select(3);
                registration.ContinueButton_ClickUntilProgressBarChanges();

                // page 3
                registration.ZipCodeTextbox_Write("90210");
                registration.ContinueButton_OnZipCodePage_Click();

                // page 4
                registration.WhatIsYourCurrentRelationshipStatus_RegPathAandD_Select(2);
                registration.ContinueButton_ClickUntilProgressBarChanges();

                // page 5
                registration.YourSmokingHabitsDropdown_RegPathAandD_Select(3);
                registration.WhatIsYourHeight_Select(2);
                registration.ContinueButton_ClickUntilProgressBarChanges();

                // page 6
                registration.YourEducationLevel_RegPathBandC_Select(5);
                registration.WhatDoYouDo_RegPathAandD_Select(2);
                registration.ContinueButton_ClickUntilProgressBarChanges();

                // page 7
                registration.YourReligiousBackground_RegPathAandD_Select(5);
                registration.HowOftenDoYouGoToSynagogue_RegPathAandD_Select(3);
                registration.ContinueButton_ClickUntilProgressBarChanges();

                // page 8
                registration.HowManyChildrenDoYouHaveListbox_Select(2);
                registration.DoYouWantChildrenDropdown_Select(2);
                registration.ContinueButton_ClickUntilProgressBarChanges();

                // page 9
                //registration.FirstNameTextbox_JDate_Write("Tommy");
                registration.UserNameTextbox_Write("QAAutomation");

                registration.BirthDateMonthList_Select(4);
                registration.BirthDateDayList_Select(4);
                registration.BirthDateYearList_Select(4);

                registration.ContinueButton_ClickUntilProgressBarChanges();

                // page 9
                if (doEmailAndPasswordAppearFirst == false)
                {
                    emailAddress = "QAAutomation_" + DateTime.Now.ToString() + "@gmail.com";
                    emailAddress = emailAddress.Replace("/", "_").Replace(" ", "_").Replace(":", "_");
                    errorLogNotes = "Email address used for registration: " + emailAddress;
                    registration.EmailAddressTextbox_Write(emailAddress);

                    registration.ChooseYourPasswordTextbox_Write("Automation");

                    registration.ContinueButton_Click();
                }

                // The registration complete page 
                WebdriverAutomation.Framework.Site_Pages.RegistrationComplete registrationComplete = new WebdriverAutomation.Framework.Site_Pages.RegistrationComplete(driver);

            }
            #endregion
            #region JDate - nothing on splash page (Splash Page Test9StepsWithKids - ) - user is taken to old Registration form
            else if (driver.Url.Contains("jdate.com") && driver.PageSource.Contains("s.pageName = \"Splash Page Test9StepsWithKids - "))
            {
                registration = splash.BrowseForFreeLink_Click();

                string emailAddress = string.Empty;
                bool doEmailAndPasswordAppearFirst = false;

                if (driver.PageSource.Contains("Registration Scenario 4 Step 1 - EmailAddress,Password"))
                    doEmailAndPasswordAppearFirst = true;

                if (doEmailAndPasswordAppearFirst == true)
                {
                    // page 1
                    emailAddress = "QAAutomation_" + DateTime.Now.ToString() + "@gmail.com";
                    emailAddress = emailAddress.Replace("/", "_").Replace(" ", "_").Replace(":", "_");
                    errorLogNotes = "Email address used for registration: " + emailAddress;
                    registration.EmailAddressTextbox_Write(emailAddress);

                    registration.ChooseYourPasswordTextbox_Write("Automation");

                    registration.ContinueButton_ClickUntilProgressBarChanges();
                }

                // page 2
                registration.YouAreAListBox_RegPathAandD_Select(3);
                registration.ContinueButton_ClickUntilProgressBarChanges();

                // page 3
                registration.ZipCodeTextbox_Write("90210");
                registration.ContinueButton_OnZipCodePage_Click();

                // page 4
                registration.WhatIsYourCurrentRelationshipStatus_RegPathAandD_Select(2);
                registration.ContinueButton_ClickUntilProgressBarChanges();

                // page 5
                registration.YourSmokingHabitsDropdown_RegPathAandD_Select(2);
                registration.WhatIsYourHeight_Select(2);
                registration.ContinueButton_ClickUntilProgressBarChanges();

                // page 6
                registration.YourEducationLevel_RegPathBandC_Select(5);
                registration.WhatDoYouDo_RegPathAandD_Select(2);
                registration.ContinueButton_ClickUntilProgressBarChanges();

                // page 7
                registration.YourReligiousBackground_RegPathAandD_Select(5);
                registration.HowOftenDoYouGoToSynagogue_RegPathAandD_Select(3);
                registration.ContinueButton_ClickUntilProgressBarChanges();

                // page 8
                registration.HowManyChildrenDoYouHaveListbox_Select(2);
                registration.DoYouWantChildrenDropdown_Select(2);
                registration.ContinueButton_ClickUntilProgressBarChanges();

                // page 9
                //registration.FirstNameTextbox_JDate_Write("Tommy");

                registration.UserNameTextbox_Write("QAAutomation");

                registration.BirthDateMonthList_Select(4);
                registration.BirthDateDayList_Select(4);
                registration.BirthDateYearList_Select(4);

                registration.ContinueButton_ClickUntilProgressBarChanges();

                // page 10
                emailAddress = "QAAutomation_" + DateTime.Now.ToString() + "@gmail.com";
                emailAddress = emailAddress.Replace("/", "_").Replace(" ", "_").Replace(":", "_");
                errorLogNotes = "Email address used for registration: " + emailAddress;
                registration.EmailAddressTextbox_Write(emailAddress);

                registration.ChooseYourPasswordTextbox_Write("Automation");

                registration.ContinueButton_Click();

                // The registration complete page 
                WebdriverAutomation.Framework.Site_Pages.RegistrationComplete registrationComplete = new WebdriverAutomation.Framework.Site_Pages.RegistrationComplete(driver);

            }
            #endregion
            #region JDate - nothing on splash page (Splash Page Control11Steps - ) - user is taken to old Registration form
            else if (driver.Url.Contains("jdate.com") && driver.PageSource.Contains("s.pageName = \"Splash Page Control11Steps - "))
            {
                registration = splash.BrowseForFreeLink_Click();

                // now we wade through the 8 billion different sub-scenarios to find out which reg path we take.  yay.
                if (driver.PageSource.Contains("s.eVar48 = (clearValue) ? \"\" : \"Test8Steps"))
                {
                    string emailAddress = string.Empty;
                    bool doEmailAndPasswordAppearFirst = false;

                    if (driver.PageSource.Contains("Registration Scenario 4 Step 1 - EmailAddress,Password"))
                        doEmailAndPasswordAppearFirst = true;

                    if (doEmailAndPasswordAppearFirst == true)
                    {
                        // page 1
                        emailAddress = "QAAutomation_" + DateTime.Now.ToString() + "@gmail.com";
                        emailAddress = emailAddress.Replace("/", "_").Replace(" ", "_").Replace(":", "_");
                        errorLogNotes = "Email address used for registration: " + emailAddress;
                        registration.EmailAddressTextbox_Write(emailAddress);

                        registration.ChooseYourPasswordTextbox_Write("Automation");

                        registration.ContinueButton_ClickUntilProgressBarChanges();
                    }

                    // page 2
                    registration.YouAreAListBox_RegPathAandD_Select(3);
                    registration.ContinueButton_ClickUntilProgressBarChanges();

                    // page 3
                    registration.ZipCodeTextbox_Write("90210");
                    registration.ContinueButton_OnZipCodePage_Click();

                    // page 4
                    //registration.WhatTypeOfRelationshipAreYouLookingFor_Click(2);
                    registration.WhatIsYourCurrentRelationshipStatus_RegPathAandD_Select(2);
                    registration.ContinueButton_ClickUntilProgressBarChanges();

                    // page 5
                    registration.YourSmokingHabitsDropdown_RegPathAandD_Select(3);
                    //registration.YourDrinkingHabitsDropdown_RegPathAandD_Select(2);
                    //registration.DoYouKeepKosher_RegPathBandC_Select(2);

                    registration.ContinueButton_ClickUntilProgressBarChanges();

                    // page 6
                    registration.YourEducationLevel_RegPathBandC_Select(5);
                    registration.WhatDoYouDo_RegPathAandD_Select(2);
                    //registration.WhatIsYourEthnicity_RegPathBandC_Select(2);
                    registration.ContinueButton_ClickUntilProgressBarChanges();

                    // page 7
                    registration.YourReligiousBackground_RegPathAandD_Select(5);
                    registration.HowOftenDoYouGoToSynagogue_RegPathAandD_Select(3);
                    registration.ContinueButton_ClickUntilProgressBarChanges();

                    //// page 8
                    //string describeMyself = "food lover.  art lover.  dance lover.  music lover. ";
                    //registration.DescribeYourselfAndYourPersonalityTextArea_Write(describeMyself);

                    //registration.ContinueButton_ClickUntilProgressBarChanges();

                    // page 9
                    registration.FirstNameTextbox_Write("Tommy");
                    registration.UserNameTextbox_Write("QAAutomation");

                    registration.BirthDateMonthList_Select(4);
                    registration.BirthDateDayList_Select(4);
                    registration.BirthDateYearList_Select(4);

                    registration.ContinueButton_ClickUntilProgressBarChanges();

                    // page 10
                    emailAddress = "QAAutomation_" + DateTime.Now.ToString() + "@gmail.com";
                    emailAddress = emailAddress.Replace("/", "_").Replace(" ", "_").Replace(":", "_");
                    errorLogNotes = "Email address used for registration: " + emailAddress;
                    registration.EmailAddressTextbox_Write(emailAddress);

                    registration.ChooseYourPasswordTextbox_Write("Automation");

                    registration.ContinueButton_Click();

                    //// page 11 - commenting pictures out for now - we're getting too many profiles created with automation that aren't being deleted
                    ////// grab a random picture in our solution's file path and put it in the Upload a Photo textbox
                    ////string strFileName = string.Empty;
                    ////Random random = new Random();

                    ////FileInfo[] files = HelperMethods.GetReqFileInfo(@"*.jpg");
                    ////if (files.Length > 0)
                    ////    strFileName = files[random.Next(0, files.Length - 1)].FullName;

                    ////registration.UploadAPhotoTextbox_Write(strFileName);

                    //registration.ContinueButton_ClickUntilProgressBarChanges();

                    //// page 12
                    //registration.FirstNameTextbox_JDate_Write("Tommy");
                    //registration.ContinueButton_Click();

                    // The registration complete page 
                    WebdriverAutomation.Framework.Site_Pages.RegistrationComplete registrationComplete = new WebdriverAutomation.Framework.Site_Pages.RegistrationComplete(driver);
                }
                else if (driver.PageSource.Contains("s.eVar48 = (clearValue) ? \"\" : \"Test9StepsWithKids"))
                {
                    string emailAddress = string.Empty;
                    bool doEmailAndPasswordAppearFirst = false;

                    if (driver.PageSource.Contains("Registration Scenario 4 Step 1 - EmailAddress,Password"))
                        doEmailAndPasswordAppearFirst = true;

                    if (doEmailAndPasswordAppearFirst == true)
                    {
                        // page 1
                        emailAddress = "QAAutomation_" + DateTime.Now.ToString() + "@gmail.com";
                        emailAddress = emailAddress.Replace("/", "_").Replace(" ", "_").Replace(":", "_");
                        errorLogNotes = "Email address used for registration: " + emailAddress;
                        registration.EmailAddressTextbox_Write(emailAddress);

                        registration.ChooseYourPasswordTextbox_Write("Automation");

                        registration.ContinueButton_ClickUntilProgressBarChanges();
                    }

                    // page 2
                    registration.YouAreAListBox_RegPathAandD_Select(3);
                    registration.ContinueButton_ClickUntilProgressBarChanges();

                    // page 3
                    registration.ZipCodeTextbox_Write("90210");
                    registration.ContinueButton_OnZipCodePage_Click();

                    // page 4
                    //registration.WhatTypeOfRelationshipAreYouLookingFor_Click(2);
                    registration.WhatIsYourCurrentRelationshipStatus_RegPathAandD_Select(2);
                    registration.ContinueButton_ClickUntilProgressBarChanges();

                    // page 5
                    registration.YourSmokingHabitsDropdown_RegPathAandD_Select(3);
                    //registration.YourDrinkingHabitsDropdown_RegPathAandD_Select(2);
                    //registration.DoYouKeepKosher_RegPathBandC_Select(2);

                    registration.WhatIsYourHeight_Select(2);
                    registration.ContinueButton_ClickUntilProgressBarChanges();

                    // page 6
                    registration.YourEducationLevel_RegPathBandC_Select(5);
                    registration.WhatDoYouDo_RegPathAandD_Select(2);
                    //registration.WhatIsYourEthnicity_RegPathBandC_Select(2);
                    registration.ContinueButton_ClickUntilProgressBarChanges();

                    // page 7
                    registration.YourReligiousBackground_RegPathAandD_Select(5);
                    registration.HowOftenDoYouGoToSynagogue_RegPathAandD_Select(3);
                    registration.ContinueButton_ClickUntilProgressBarChanges();

                    //// page 8
                    registration.HowManyChildrenDoYouHaveListbox_Select(2);
                    registration.DoYouWantChildrenDropdown_Select(2);

                    //string describeMyself = "food lover.  art lover.  dance lover.  music lover. ";
                    //registration.DescribeYourselfAndYourPersonalityTextArea_Write(describeMyself);

                    registration.ContinueButton_ClickUntilProgressBarChanges();

                    // page 9
                    //registration.FirstNameTextbox_Write("Tommy");
                    registration.UserNameTextbox_Write("QAAutomation");

                    registration.BirthDateMonthList_Select(4);
                    registration.BirthDateDayList_Select(4);
                    registration.BirthDateYearList_Select(4);

                    registration.ContinueButton_ClickUntilProgressBarChanges();

                    // page 10
                    emailAddress = "QAAutomation_" + DateTime.Now.ToString() + "@gmail.com";
                    emailAddress = emailAddress.Replace("/", "_").Replace(" ", "_").Replace(":", "_");
                    errorLogNotes = "Email address used for registration: " + emailAddress;
                    registration.EmailAddressTextbox_Write(emailAddress);

                    registration.ChooseYourPasswordTextbox_Write("Automation");

                    registration.ContinueButton_Click();

                    //// page 11 - commenting pictures out for now - we're getting too many profiles created with automation that aren't being deleted
                    ////// grab a random picture in our solution's file path and put it in the Upload a Photo textbox
                    ////string strFileName = string.Empty;
                    ////Random random = new Random();

                    ////FileInfo[] files = HelperMethods.GetReqFileInfo(@"*.jpg");
                    ////if (files.Length > 0)
                    ////    strFileName = files[random.Next(0, files.Length - 1)].FullName;

                    ////registration.UploadAPhotoTextbox_Write(strFileName);

                    //registration.ContinueButton_ClickUntilProgressBarChanges();

                    //// page 12
                    //registration.FirstNameTextbox_JDate_Write("Tommy");
                    //registration.ContinueButton_Click();

                    // The registration complete page 
                    WebdriverAutomation.Framework.Site_Pages.RegistrationComplete registrationComplete = new WebdriverAutomation.Framework.Site_Pages.RegistrationComplete(driver);
                }
                else
                    Assert.Fail("In the JDate Registration case, within Registration Scenario 9 we do not recognize the corrent sub-registration flow we need to take.  The s.eVar48 variable currently on the pagesource is not familiar to us.");

            }
            #endregion
            #region JDate - brand new split from Bedrock Registration Page 08-08-12
            else if (driver.Url.Contains("jdate.com") && driver.PageSource.Contains("s.eVar48 = \"Scenario1\";"))
            {
                registration = splash.BrowseForFreeLink_Click();

                // page 2
                registration.YouAreA_NewRegNotOnBedrock_Select(3);
                registration.ContinueButton_NewRegNotOnBedrock_ClickUntilProgressBarChanges();

                // page 3
                registration.ZipCodeTextbox_NewRegNotOnBedrock_Write("90210");
                registration.ContinueButton_NewRegNotOnBedrock_ClickUntilProgressBarChanges();

                // page 4
                //registration.WhatTypeOfRelationshipAreYouLookingFor_Click(2);
                registration.WhatIsYourCurrentRelationshipStatus_NewRegNotOnBedrock_Select(2);
                registration.ContinueButton_NewRegNotOnBedrock_ClickUntilProgressBarChanges();

                // page 5
                registration.YourSmokingHabitsDropdown_NewRegNotOnBedrock_Select(3);
                registration.WhatIsYourHeight_NewRegNotOnBedrock_Select(3);
                //registration.YourDrinkingHabitsDropdown_RegPathAandD_Select(2);
                //registration.DoYouKeepKosher_RegPathBandC_Select(2);

                registration.ContinueButton_NewRegNotOnBedrock_ClickUntilProgressBarChanges();

                // page 6
                registration.YourEducationLevel_NewRegNotOnBedrock_Select(5);
                registration.WhatDoYouDo_NewRegNotOnBedrock_Select(2);
                //registration.WhatIsYourEthnicity_RegPathBandC_Select(2);
                registration.ContinueButton_NewRegNotOnBedrock_ClickUntilProgressBarChanges();

                // page 7
                registration.YourReligiousBackground_NewRegNotOnBedrock_Select(5);
                registration.HowOftenDoYouGoToSynagogue_NewRegNotOnBedrock_Select(3);
                registration.ContinueButton_NewRegNotOnBedrock_ClickUntilProgressBarChanges();

                // page 8
                registration.HowManyChildrenDoYouHaveListbox_NewRegNotOnBedrock_Select(2);
                registration.DoYouWantChildrenDropdown_NewRegNotOnBedrock_Select(2);
                registration.ContinueButton_NewRegNotOnBedrock_ClickUntilProgressBarChanges();



                //// page 8
                //string describeMyself = "food lover.  art lover.  dance lover.  music lover. ";
                //registration.DescribeYourselfAndYourPersonalityTextArea_Write(describeMyself);

                //registration.ContinueButton_ClickUntilProgressBarChanges();

                // page 9
                //registration.FirstNameTextbox_Write("Tommy");
                registration.UserNameTextbox_NewRegNotOnBedrock_Write("QAAutomation");

                registration.BirthDateMonthList_NewRegNotOnBedrock_Select(4);
                registration.BirthDateDayList_NewRegNotOnBedrock_Select(4);
                registration.BirthDateYearList_NewRegNotOnBedrock_Select(4);

                registration.ContinueButton_NewRegNotOnBedrock_ClickUntilProgressBarChanges();

                // page 10
                string emailAddress = string.Empty;

                emailAddress = "QAAutomation_" + DateTime.Now.ToString() + "@gmail.com";
                emailAddress = emailAddress.Replace("/", "_").Replace(" ", "_").Replace(":", "_");
                errorLogNotes = "Email address used for registration: " + emailAddress;
                registration.EmailAddressTextbox_NewRegNotOnBedrock_Write(emailAddress);

                registration.ChooseYourPasswordTextbox_NewRegNotOnBedrock_Write("Automation");

                registration.ContinueButton_NewRegNotOnBedrock_Click();

                //// page 11 - commenting pictures out for now - we're getting too many profiles created with automation that aren't being deleted
                ////// grab a random picture in our solution's file path and put it in the Upload a Photo textbox
                ////string strFileName = string.Empty;
                ////Random random = new Random();

                ////FileInfo[] files = HelperMethods.GetReqFileInfo(@"*.jpg");
                ////if (files.Length > 0)
                ////    strFileName = files[random.Next(0, files.Length - 1)].FullName;

                ////registration.UploadAPhotoTextbox_Write(strFileName);

                //registration.ContinueButton_ClickUntilProgressBarChanges();

                //// page 12
                //registration.FirstNameTextbox_JDate_Write("Tommy");
                //registration.ContinueButton_Click();

                // The registration complete page 
                WebdriverAutomation.Framework.Site_Pages.RegistrationComplete registrationComplete = new WebdriverAutomation.Framework.Site_Pages.RegistrationComplete(driver);


            }
            #endregion
            #region JDate - brand new split from Bedrock Registration Page 08-08-12 #2
            else if (driver.Url.Contains("jdate.com") && driver.PageSource.Contains("s.eVar48 = \"Scenario2\";"))
            {
                registration = splash.BrowseForFreeLink_Click();

                // page 2
                registration.YouAreA_NewRegNotOnBedrock_Select(3);
                registration.ContinueButton_NewRegNotOnBedrock_ClickUntilProgressBarChanges();

                // page 3
                registration.ZipCodeTextbox_NewRegNotOnBedrock_Write("90210");
                registration.ContinueButton_NewRegNotOnBedrock_ClickUntilProgressBarChanges();

                // page 4
                //registration.WhatTypeOfRelationshipAreYouLookingFor_Click(2);
                registration.WhatIsYourCurrentRelationshipStatus_NewRegNotOnBedrock_Select(2);
                registration.ContinueButton_NewRegNotOnBedrock_ClickUntilProgressBarChanges();

                // page 5
                registration.YourSmokingHabitsDropdown_NewRegNotOnBedrock_Select(3);
                //registration.YourDrinkingHabitsDropdown_RegPathAandD_Select(2);
                //registration.DoYouKeepKosher_RegPathBandC_Select(2);

                registration.ContinueButton_NewRegNotOnBedrock_ClickUntilProgressBarChanges();

                // page 6
                registration.YourEducationLevel_NewRegNotOnBedrock_Select(5);
                registration.WhatDoYouDo_NewRegNotOnBedrock_Select(2);
                //registration.WhatIsYourEthnicity_RegPathBandC_Select(2);
                registration.ContinueButton_NewRegNotOnBedrock_ClickUntilProgressBarChanges();

                // page 7
                registration.YourReligiousBackground_NewRegNotOnBedrock_Select(5);
                registration.HowOftenDoYouGoToSynagogue_NewRegNotOnBedrock_Select(3);
                registration.ContinueButton_NewRegNotOnBedrock_ClickUntilProgressBarChanges();

                //// page 8
                //string describeMyself = "food lover.  art lover.  dance lover.  music lover. ";
                //registration.DescribeYourselfAndYourPersonalityTextArea_Write(describeMyself);

                //registration.ContinueButton_ClickUntilProgressBarChanges();

                //// page 8
                //registration.HowManyChildrenDoYouHaveListbox_NewRegNotOnBedrock_Select(2);
                //registration.DoYouWantChildrenDropdown_NewRegNotOnBedrock_Select(2);
                //registration.ContinueButton_NewRegNotOnBedrock_ClickUntilProgressBarChanges();

                // page 9
                //registration.FirstNameTextbox_Write("Tommy");
                registration.UserNameTextbox_NewRegNotOnBedrock_Write("QAAutomation");

                registration.BirthDateMonthList_NewRegNotOnBedrock_Select(4);
                registration.BirthDateDayList_NewRegNotOnBedrock_Select(4);
                registration.BirthDateYearList_NewRegNotOnBedrock_Select(4);

                registration.ContinueButton_NewRegNotOnBedrock_ClickUntilProgressBarChanges();

                // page 10
                string emailAddress = string.Empty;

                emailAddress = "QAAutomation_" + DateTime.Now.ToString() + "@gmail.com";
                emailAddress = emailAddress.Replace("/", "_").Replace(" ", "_").Replace(":", "_");
                errorLogNotes = "Email address used for registration: " + emailAddress;
                registration.EmailAddressTextbox_NewRegNotOnBedrock_Write(emailAddress);

                registration.ChooseYourPasswordTextbox_NewRegNotOnBedrock_Write("Automation");

                registration.ContinueButton_NewRegNotOnBedrock_Click();

                //// page 11 - commenting pictures out for now - we're getting too many profiles created with automation that aren't being deleted
                ////// grab a random picture in our solution's file path and put it in the Upload a Photo textbox
                ////string strFileName = string.Empty;
                ////Random random = new Random();

                ////FileInfo[] files = HelperMethods.GetReqFileInfo(@"*.jpg");
                ////if (files.Length > 0)
                ////    strFileName = files[random.Next(0, files.Length - 1)].FullName;

                ////registration.UploadAPhotoTextbox_Write(strFileName);

                //registration.ContinueButton_ClickUntilProgressBarChanges();

                //// page 12
                //registration.FirstNameTextbox_JDate_Write("Tommy");
                //registration.ContinueButton_Click();

                // The registration complete page 
                WebdriverAutomation.Framework.Site_Pages.RegistrationComplete registrationComplete = new WebdriverAutomation.Framework.Site_Pages.RegistrationComplete(driver);


            }
            #endregion
            #region JDate - 3-Page Registration Page 10-25-12
            else if (driver.Url.Contains("jdate.com") && driver.PageSource.Contains("s.eVar48 = \"3PageRegFlow\";"))
            {
                registration = splash.BrowseForFreeLink_Click();

                // page 1
                registration.YouAreA_NewRegNotOnBedrock_Select(3);
                registration.ZipCodeTextbox_NewRegNotOnBedrock_Write("90210");
                registration.WhatIsYourCurrentRelationshipStatus_NewRegNotOnBedrock_Select(2);
                registration.YourSmokingHabitsDropdown_NewRegNotOnBedrock_Select(3);
                registration.WhatIsYourHeight_NewRegNotOnBedrock_Select(3);

                registration.ContinueButton_NewRegNotOnBedrock_ClickUntilProgressBarChanges();

                // page 2
                registration.YourEducationLevel_NewRegNotOnBedrock_Select(5);
                registration.WhatDoYouDo_NewRegNotOnBedrock_Select(2);
                registration.YourReligiousBackground_NewRegNotOnBedrock_Select(5);
                registration.HowOftenDoYouGoToSynagogue_NewRegNotOnBedrock_Select(3);
                registration.HowManyChildrenDoYouHaveListbox_NewRegNotOnBedrock_Select(2);
                registration.DoYouWantChildrenDropdown_NewRegNotOnBedrock_Select(2);
                
                registration.ContinueButton_NewRegNotOnBedrock_ClickUntilProgressBarChanges();

                // page 3
                registration.UserNameTextbox_NewRegNotOnBedrock_Write("QAAutomation");

                registration.BirthDateMonthList_NewRegNotOnBedrock_Select(4);
                registration.BirthDateDayList_NewRegNotOnBedrock_Select(4);
                registration.BirthDateYearList_NewRegNotOnBedrock_Select(4);

                string emailAddress = string.Empty;

                emailAddress = "QAAutomation_" + DateTime.Now.ToString() + "@gmail.com";
                emailAddress = emailAddress.Replace("/", "_").Replace(" ", "_").Replace(":", "_");
                errorLogNotes = "Email address used for registration: " + emailAddress;
                registration.EmailAddressTextbox_NewRegNotOnBedrock_Write(emailAddress);

                registration.ChooseYourPasswordTextbox_NewRegNotOnBedrock_Write("Automation");

                registration.ContinueButton_NewRegNotOnBedrock_Click();

                // The registration complete page 
                WebdriverAutomation.Framework.Site_Pages.RegistrationComplete registrationComplete = new WebdriverAutomation.Framework.Site_Pages.RegistrationComplete(driver);
                
            }
            #endregion
            #region JDate - Auto Advance Registration Page 1-11-13
            else if (driver.Url.Contains("jdate.com") && driver.PageSource.Contains("s.eVar48 = \"AutoAdvance\";"))
            {
                registration = splash.BrowseForFreeLink_Click();

                // page 1
                registration.YouAreA_NewRegNotOnBedrock_Select(3);

                // page 2
                registration.ZipCodeTextbox_NewRegNotOnBedrock_Write("90210");
                registration.ContinueButton_NewRegNotOnBedrock_ClickUntilProgressBarChanges();

                // page 3
                registration.WhatIsYourCurrentRelationshipStatus_NewRegNotOnBedrock_Select(2);

                // page 4
                registration.YourSmokingHabitsDropdown_NewRegNotOnBedrock_Select(3);

                // page 5
                registration.WhatIsYourHeight_NewRegNotOnBedrock_Select(3);

                // page 6
                registration.YourEducationLevel_NewRegNotOnBedrock_Select(5);

                // page 7
                registration.WhatDoYouDo_NewRegNotOnBedrock_Select(2);

                // page 8
                registration.YourReligiousBackground_NewRegNotOnBedrock_Select(5);

                // page 9
                registration.HowOftenDoYouGoToSynagogue_NewRegNotOnBedrock_Select(3);

                // page 10
                registration.HowManyChildrenDoYouHaveListbox_NewRegNotOnBedrock_Select(2);

                // page 11
                registration.DoYouWantChildrenDropdown_NewRegNotOnBedrock_Select(2);

                // page 12
                registration.UserNameTextbox_NewRegNotOnBedrock_Write("QAAutomation");

                registration.BirthDateMonthList_NewRegNotOnBedrock_Select(4);
                registration.BirthDateDayList_NewRegNotOnBedrock_Select(4);
                registration.BirthDateYearList_NewRegNotOnBedrock_Select(4);

                registration.ContinueButton_NewRegNotOnBedrock_ClickUntilProgressBarChanges();

                // page 13
                string emailAddress = string.Empty;

                emailAddress = "QAAutomation_" + DateTime.Now.ToString() + "@gmail.com";
                emailAddress = emailAddress.Replace("/", "_").Replace(" ", "_").Replace(":", "_");
                errorLogNotes = "Email address used for registration: " + emailAddress;
                registration.EmailAddressTextbox_NewRegNotOnBedrock_Write(emailAddress);

                registration.ChooseYourPasswordTextbox_NewRegNotOnBedrock_Write("Automation");

                registration.ContinueButton_NewRegNotOnBedrock_Click();

                // The registration complete page 
                WebdriverAutomation.Framework.Site_Pages.RegistrationComplete registrationComplete = new WebdriverAutomation.Framework.Site_Pages.RegistrationComplete(driver);

            }
            #endregion
            else
            {
                Assert.Fail("Currently we do not recognize which registration page will be reached if we click the Browse For Free button on the Splash page. Stopping the test.");
            }
        }

    }
}
