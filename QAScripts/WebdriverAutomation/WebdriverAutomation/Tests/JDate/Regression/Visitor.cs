﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.Events;
using NUnit.Framework;
using log4net;

namespace WebdriverAutomation.Tests.JDate.Regression
{
    [TestFixture]
    public class Visitor : BaseTest
    {

        [Test]
        public void Validate_Footers_NotLoggedIn()
        {
            try
            {
                log.Info("START TEST - JDate/Regression/Vistor/Footers");

                Setup();

                StartTest_UsingJDate();

                WebdriverAutomation.Tests.ReusableCode.Footers footers = new WebdriverAutomation.Tests.ReusableCode.Footers();

                footers.ValidateFooters_NotLoggedIn(driver, log);                

                TearDown();

                log.Info("END TEST - JDate/Regression/Vistor/Footers");
            }
            catch (Exception e)
            {
                CleanupFailedTestcase(e);
                throw;
            }
        }

        [Test]
        public void Validate_Footers_LoggedIn()
        {
            try
            {
                log.Info("START TEST - JDate/Regression/Vistor/Footers");

                Setup();

                StartTest_UsingJDate();

                WebdriverAutomation.Tests.ReusableCode.Footers footers = new WebdriverAutomation.Tests.ReusableCode.Footers();

                footers.ValidateFooters_LoggedIn(driver, log);

                TearDown();

                log.Info("END TEST - JDate/Regression/Vistor/Footers");
            }
            catch (Exception e)
            {
                CleanupFailedTestcase(e);
                throw;
            }
        }

        [Test]
        public void Validate_ProfileIsViewable()
        {
            try
            {
                log.Info("START TEST - JDate/Regression/Member/ProfileIsViewable");
                Setup();

                StartTest_UsingJDate();

                WebdriverAutomation.Framework.Site_Pages.Splash splash = new WebdriverAutomation.Framework.Site_Pages.Splash(driver);

                Framework.Site_Pages.Login login = splash.MemberLoginLink_Click();

                WebdriverAutomation.Framework.Site_Pages.AboutJDate aboutJDate = login.Footer.AboutJDateLink_Click();

                Framework.Site_Pages.MembersOnline membersOnline = aboutJDate.Header.MembersOnline_Click();

                Framework.Site_Pages.YourProfile_Visitor profile = membersOnline.ClickONFirstOnlineMemberInList_AsVisitor();

                TearDown();

                log.Info("END TEST - JDate/Regression/Member/ProfileIsViewable");
            }
            catch (Exception e)
            {
                CleanupFailedTestcase(e);
                throw;
            }
        }
    }
}
