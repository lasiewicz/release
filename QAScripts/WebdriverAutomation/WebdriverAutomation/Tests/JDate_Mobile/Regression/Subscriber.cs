﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.Events;
using NUnit.Framework;
using log4net;
using WebdriverAutomation.Tests.ReusableCode;
using WebdriverAutomation.Framework.Site_Pages;

namespace WebdriverAutomation.Tests.JDate_Mobile.Regression
{
    [TestFixture]
    public class Subscriber : BaseTest
    {

        [Test]
        public void Validate_Email_SubscribedUserToSubscribedUser()
        {
            try
            {
                log.Info("START TEST - JDate_Mobile/Regression/Member/Email");
                Setup();

                StartTest_UsingJDate_Mobile();

                string emailLogin_1 = System.Configuration.ConfigurationManager.AppSettings["JDate_SubscribedUser1_Email"];
                string password_1 = System.Configuration.ConfigurationManager.AppSettings["JDate_SubscribedUser1_Password"];

                string emailLogin_2 = System.Configuration.ConfigurationManager.AppSettings["JDate_SubscribedUser3_Email"];
                string password_2 = System.Configuration.ConfigurationManager.AppSettings["JDate_SubscribedUser3_Password"];
                string memberid_2 = System.Configuration.ConfigurationManager.AppSettings["JDate_SubscribedUser3_MemberID"];

                Framework.Mobile_Pages.Login login = new Framework.Mobile_Pages.Login(driver);

                login.EmailTextbox_Write(emailLogin_1);
                login.PasswordTextbox_Write(password_1);

                Framework.Mobile_Pages.Home home = login.LoginButton_Click(emailLogin_1);

                // jump straight to the mobile page of the user we want to email
                if (driver.Url.Contains("preprod.m.jdate.com"))
                    driver.Navigate().GoToUrl("http://preprod.m.jdate.com/profile/fullprofile/" + memberid_2);
                else
                    driver.Navigate().GoToUrl("http://m.jdate.com/profile/fullprofile/" + memberid_2);

                Framework.Mobile_Pages.YourProfile profile = new Framework.Mobile_Pages.YourProfile(driver);

                Framework.Mobile_Pages.EmailMessage emailMessage = profile.EmailLink_Click();

                // user 1 - message user 2          
                string subject = "m sub to sub " + DateTime.Now.ToString();
                string message = "Validate Mobile EMail Subscribed User To Subscribed User test case Message timestamp: " + DateTime.Now.ToString();

                emailMessage.SubjectTextbox_Write(subject);
                emailMessage.BodyTextbox_Write(message);
                Framework.Mobile_Pages.MessageSent messageSent = emailMessage.SubmitButton_Click();

                profile = messageSent.ReturnToProfileButton_Click();

                home = profile.Header.HomeLink_Click();

                Framework.Mobile_Pages.Inbox inbox = home.InboxLink_Click_JDateSubscribedUser();

                inbox.SentTab_Click();

                // look through list of messages and verify that a message with the subject we sent appeared                
                Assert.IsTrue(inbox.DoesMessageWithCorrectSubjectAppear_SentPage(subject), "ERROR - SENT PAGE - The email with the subject line that we expect to see in our inbox is not there.\r\nThe subject line we expect to see is '" + subject + "'.");
                
                // user 1 log out
                login = profile.Footer.LogoutLink_Click();

                // user 2 log in
                login.EmailTextbox_Write(emailLogin_2);
                login.PasswordTextbox_Write(password_2);

                home = login.LoginButton_Click(emailLogin_2);

                inbox = home.InboxLink_Click_JDateSubscribedUser();

                // look through list of messages and verify that a message with the subject we sent appeared                
                Assert.IsTrue(inbox.DoesMessageWithCorrectSubjectAppear_InboxPage(subject), "ERROR - INBOX PAGE - The email with the subject line that we expect to see in our inbox is not there.");

                Framework.Mobile_Pages.Message messagePage = inbox.Message_Click(subject);

                Assert.IsTrue(messagePage.DoesCorrectSubjectAppear(subject), "ERROR - INBOX PAGE - After clicking on the message with the correct subject line, the subject that we expect from the earlier sent email does not appear in the actual Message Page.");
                Assert.IsTrue(messagePage.DoesCorrectMessageAppear(message), "ERROR - INBOX PAGE - After clicking on the message with the correct subject line, the message that we expect from the earlier sent email does not appear in the actual Message Page.");

                // delete the message
                inbox = messagePage.Delete_Click();

                TearDown();

                log.Info("END TEST - JDate_Mobile/Regression/Member/Email");
            }
            catch (Exception e)
            {
                CleanupFailedTestcase(e);
                throw;
            }
        }


        /// <summary>
        /// Verify that when the user logs out the Login Page appears.
        /// </summary>
        [Test]
        public void Validate_Logout()
        {
            try
            {
                log.Info("START TEST - JDate_Mobile/Regression/Member/Logout");
                Setup();

                StartTest_UsingJDate_Mobile();

                string emailLogin_1 = System.Configuration.ConfigurationManager.AppSettings["JDate_SubscribedUser1_Email"];
                string password_1 = System.Configuration.ConfigurationManager.AppSettings["JDate_SubscribedUser1_Password"];

                Framework.Mobile_Pages.Login login = new Framework.Mobile_Pages.Login(driver);

                login.EmailTextbox_Write(emailLogin_1);
                login.PasswordTextbox_Write(password_1);

                Framework.Mobile_Pages.Home home = login.LoginButton_Click(emailLogin_1);

                login = home.Footer.LogoutLink_Click();

                TearDown();

                log.Info("END TEST - JDate_Mobile/Regression/Member/Logout");
            }
            catch (Exception e)
            {
                CleanupFailedTestcase(e);
                throw;
            }
        }


        [Test]
        public void Validate_Fullsite()
        {
            try
            {
                log.Info("START TEST - JDate_Mobile/Regression/Member/Fullsite");
                Setup();

                StartTest_UsingJDate_Mobile();

                string emailLogin_1 = System.Configuration.ConfigurationManager.AppSettings["JDate_SubscribedUser1_Email"];
                string password_1 = System.Configuration.ConfigurationManager.AppSettings["JDate_SubscribedUser1_Password"];

                Framework.Mobile_Pages.Login login = new Framework.Mobile_Pages.Login(driver);

                login.EmailTextbox_Write(emailLogin_1);
                login.PasswordTextbox_Write(password_1);

                Framework.Mobile_Pages.Home home = login.LoginButton_Click(emailLogin_1);

                Framework.Site_Pages.Login loginFullSite = home.Footer.FullSiteLink_Click();

                TearDown();

                log.Info("END TEST - JDate_Mobile/Regression/Member/Fullsite");
            }
            catch (Exception e)
            {
                CleanupFailedTestcase(e);
                throw;
            }
        }


        [Test]
        public void Validate_FooterLinks()
        {
            try
            {
                log.Info("START TEST - JDate_Mobile/Regression/Member/FooterLinks");
                Setup();

                StartTest_UsingJDate_Mobile();

                string emailLogin_1 = System.Configuration.ConfigurationManager.AppSettings["JDate_SubscribedUser1_Email"];
                string password_1 = System.Configuration.ConfigurationManager.AppSettings["JDate_SubscribedUser1_Password"];

                Framework.Mobile_Pages.Login login = new Framework.Mobile_Pages.Login(driver);

                login.EmailTextbox_Write(emailLogin_1);
                login.PasswordTextbox_Write(password_1);

                Framework.Mobile_Pages.Home home = login.LoginButton_Click(emailLogin_1);

                Framework.Mobile_Pages.ContactUs contactUs = home.Footer.ContactUsLink_Click();

                contactUs.Header.BackLink_Click();

                Framework.Mobile_Pages.Safety safety = home.Footer.SafetyLink_Click();

                contactUs.Header.BackLink_Click();

                Framework.Mobile_Pages.Privacy privacy = home.Footer.PrivacyLink_Click();

                contactUs.Header.BackLink_Click();

                Framework.Mobile_Pages.TermsOfService termsOfService = home.Footer.TermsOfServiceLink_Click();

                TearDown();

                log.Info("END TEST - JDate_Mobile/Regression/Member/FooterLinks");
            }
            catch (Exception e)
            {
                CleanupFailedTestcase(e);
                throw;
            }
        }


        [Test]
        public void Validate_UploadPhotosPageExists()
        {
            try
            {
                log.Info("START TEST - JDate_Mobile/Regression/Member/UploadPhotosPageExists");

                Setup();

                StartTest_UsingJDate_Mobile();

                string emailLogin = System.Configuration.ConfigurationManager.AppSettings["JDate_SubscribedUser1_Email"];
                string password = System.Configuration.ConfigurationManager.AppSettings["JDate_SubscribedUser1_Password"];

                Framework.Mobile_Pages.Login login = new Framework.Mobile_Pages.Login(driver);

                login.EmailTextbox_Write(emailLogin);
                login.PasswordTextbox_Write(password);

                Framework.Mobile_Pages.Home home = login.LoginButton_Click(emailLogin);

                Framework.Mobile_Pages.UploadPhotos uploadedPhotos = home.UploadPhotosLink_Click();

                TearDown();

                log.Info("END TEST - JDate_Mobile/Regression/Member/UploadPhotosPageExists");
            }
            catch (Exception e)
            {
                CleanupFailedTestcase(e);
                throw;
            }
        }


        [Test]
        public void Validate_YourProfile()
        {
            try
            {
                log.Info("START TEST - JDate_Mobile/Regression/Member/YourProfile");
                Setup();

                StartTest_UsingJDate_Mobile();

                string emailLogin = System.Configuration.ConfigurationManager.AppSettings["JDate_SubscribedUser1_Email"];
                string password = System.Configuration.ConfigurationManager.AppSettings["JDate_SubscribedUser1_Password"];

                Framework.Mobile_Pages.Login login = new Framework.Mobile_Pages.Login(driver);

                login.EmailTextbox_Write(emailLogin);
                login.PasswordTextbox_Write(password);

                Framework.Mobile_Pages.Home home = login.LoginButton_Click(emailLogin);

                Framework.Mobile_Pages.YourProfile yourProfile = home.YourProfileLink_Click();

                Assert.IsTrue(yourProfile.UsernameExists(), "ERROR - YOUR PROFILE PAGE - The user's username should appear on this page but does not.");

                Assert.IsTrue(yourProfile.AgeExists(), "ERROR - YOUR PROFILE PAGE - The user's age should appear on this page but does not.");

                Assert.IsTrue(yourProfile.CityExists(), "ERROR - YOUR PROFILE PAGE - The user's city should appear on this page but does not.");

                Assert.IsTrue(yourProfile.LastLoginExists(), "ERROR - YOUR PROFILE PAGE - The user's last login should appear on this page but does not.");

                Assert.IsTrue(yourProfile.MyDetailsExists(), "ERROR - YOUR PROFILE PAGE - My Details should appear on this page but does not.");

                Assert.IsTrue(yourProfile.InMyOwnWordsExists(), "ERROR - YOUR PROFILE PAGE - In My Own Words should appear on this page but does not.");

                Assert.IsTrue(yourProfile.MyIdealMatchExists(), "ERROR - YOUR PROFILE PAGE - My Ideal Match should appear on this page but does not.");

                TearDown();

                log.Info("END TEST - JDate_Mobile/Regression/Member/YourProfile");
            }
            catch (Exception e)
            {
                CleanupFailedTestcase(e);
                throw;
            }
        }


        [Test]
        public void Validate_FavoritedYouResults()
        {
            try
            {
                log.Info("START TEST - JDate_Mobile/Regression/Member/FavoritedYouResults");

                Setup();

                StartTest_UsingJDate_Mobile();

                string emailLogin = System.Configuration.ConfigurationManager.AppSettings["JDate_SubscribedUser1_Email"];
                string password = System.Configuration.ConfigurationManager.AppSettings["JDate_SubscribedUser1_Password"];

                Framework.Mobile_Pages.Login login = new Framework.Mobile_Pages.Login(driver);

                login.EmailTextbox_Write(emailLogin);
                login.PasswordTextbox_Write(password);

                Framework.Mobile_Pages.Home home = login.LoginButton_Click(emailLogin);

                Framework.Mobile_Pages.FavoritedYou favoritedYou = home.FavoritedYouLink_Click();

                // verify that results appear
                Assert.True(favoritedYou.SearchResultsCount() > 0, "ERROR - FAVORITED YOU PAGE - No results appeared.");

                // verify that the username appears for all users
                Assert.True(favoritedYou.DoAllResultsDisplayUsername(), "ERROR - FAVORITED YOU PAGE - Not all of our results display a username.");

                // verify that the Age appears for all users
                Assert.True(favoritedYou.DoAllResultsDisplayAge(), "ERROR - FAVORITED YOU PAGE - Not all of our results display an age.");

                // verify that the City appears for all users
                Assert.True(favoritedYou.DoAllResultsDisplayCity(), "ERROR - FAVORITED YOU PAGE - Not all of our results display a city.");

                // verify that the Last Login appears for all users
                Assert.True(favoritedYou.DoAllResultsDisplayLastLoggedIn(), "ERROR - FAVORITED YOU PAGE - Not all of our results display a last logged in date.");

                // select a member and verify that the user is taken to their Profile Page
                Framework.Mobile_Pages.YourProfile profile = favoritedYou.ViewedYouResults_Click(1);

                TearDown();

                log.Info("END TEST - JDate_Mobile/Regression/Member/FavoritedYouResults");
            }
            catch (Exception e)
            {
                CleanupFailedTestcase(e);
                throw;
            }
        }


        /// <summary>
        /// In order to verify pagination we need to ensure that our search results have enough results to span more than one page
        /// </summary>
        [Test]
        public void Validate_FavoritedYouPagination()
        {
            try
            {
                log.Info("START TEST - JDate_Mobile/Regression/Member/FavoritedYouPagination");
                Setup();

                StartTest_UsingJDate_Mobile();

                string emailLogin = System.Configuration.ConfigurationManager.AppSettings["JDate_SubscribedUser1_Email"];
                string password = System.Configuration.ConfigurationManager.AppSettings["JDate_SubscribedUser1_Password"];

                Framework.Mobile_Pages.Login login = new Framework.Mobile_Pages.Login(driver);

                login.EmailTextbox_Write(emailLogin);
                login.PasswordTextbox_Write(password);

                Framework.Mobile_Pages.Home home = login.LoginButton_Click(emailLogin);

                Framework.Mobile_Pages.FavoritedYou favoritedYou = home.FavoritedYouLink_Click();

                // verify that we have 10 people on the page
                int searchResultsCount = favoritedYou.SearchResultsCount();
                Assert.IsTrue(searchResultsCount == 10, "ERROR - VIEWED YOU PAGE - We should be getting back 10 results on our page from our results but we are not.\r\nWe are currently only getting " + searchResultsCount + " results.");

                // show more matches and verify that we now have more than 10 people on the page
                favoritedYou.ShowMoreMatchesButton_Click();
                searchResultsCount = favoritedYou.SearchResultsCount();
                Assert.IsTrue(searchResultsCount > 10, "ERROR - VIEWED YOU PAGE - We should be getting back more than 10 results on our page from our results but we are not.\r\nWe are currently only getting " + searchResultsCount + " results.");

                //// show more matches and verify that we now have more than 20 people on the page
                //viewedYou.ShowMoreMatchesButton_Click();
                //searchResultsCount = viewedYou.SearchResultsCount();
                //Assert.IsTrue(searchResultsCount == 30, "ERROR - VIEWED YOU PAGE - We should be getting back more than 20 results on our page from our results but we are not.\r\nWe are currently only getting " + searchResultsCount + " results.");

                TearDown();

                log.Info("END TEST - JDate_Mobile/Regression/Member/FavoritedYouPagination");
            }
            catch (Exception e)
            {
                CleanupFailedTestcase(e);
                throw;
            }
        }


        [Test]
        public void Validate_ViewedYouResults()
        {
            try
            {
                log.Info("START TEST - JDate_Mobile/Regression/Member/ViewedYouResults");

                Setup();

                StartTest_UsingJDate_Mobile();

                string emailLogin = System.Configuration.ConfigurationManager.AppSettings["JDate_SubscribedUser1_Email"];
                string password = System.Configuration.ConfigurationManager.AppSettings["JDate_SubscribedUser1_Password"];

                Framework.Mobile_Pages.Login login = new Framework.Mobile_Pages.Login(driver);

                login.EmailTextbox_Write(emailLogin);
                login.PasswordTextbox_Write(password);

                Framework.Mobile_Pages.Home home = login.LoginButton_Click(emailLogin);

                Framework.Mobile_Pages.ViewedYou viewedYou = home.ViewedYouLink_Click();

                // verify that results appear
                Assert.True(viewedYou.SearchResultsCount() > 0, "ERROR - VIEWED YOU PAGE - On the Viewed You Page, no results appeared.");

                // verify that the username appears for all users
                Assert.True(viewedYou.DoAllResultsDisplayUsername(), "ERROR - VIEWED YOU PAGE - On the Viewed You Page, not all of our search results display a username.");

                // verify that the Age appears for all users
                Assert.True(viewedYou.DoAllResultsDisplayAge(), "ERROR - VIEWED YOU PAGE - On the Viewed You Page, not all of our search results display a username.");

                // verify that the City appears for all users
                Assert.True(viewedYou.DoAllResultsDisplayCity(), "ERROR - VIEWED YOU PAGE - On the Viewed You Page, not all of our search results display a username.");

                // verify that the Last Login appears for all users
                Assert.True(viewedYou.DoAllResultsDisplayLastLoggedIn(), "ERROR - VIEWED YOU PAGE - On the Viewed You Page, not all of our search results display a username.");

                // select a member and verify that the user is taken to their Profile Page
                Framework.Mobile_Pages.YourProfile profile = viewedYou.ViewedYouResults_Click(1);

                TearDown();

                log.Info("END TEST - JDate_Mobile/Regression/Member/ViewedYouResults");
            }
            catch (Exception e)
            {
                CleanupFailedTestcase(e);
                throw;
            }
        }


        /// <summary>
        /// In order to verify pagination we need to ensure that our search results have enough results to span more than one page.
        /// </summary>
        [Test]
        public void Validate_ViewedYouPagination()
        {
            try
            {
                log.Info("START TEST - JDate_Mobile/Regression/Member/ViewedYouPagination");
                Setup();

                StartTest_UsingJDate_Mobile();

                string emailLogin = System.Configuration.ConfigurationManager.AppSettings["JDate_SubscribedUser1_Email"];
                string password = System.Configuration.ConfigurationManager.AppSettings["JDate_SubscribedUser1_Password"];

                Framework.Mobile_Pages.Login login = new Framework.Mobile_Pages.Login(driver);

                login.EmailTextbox_Write(emailLogin);
                login.PasswordTextbox_Write(password);

                Framework.Mobile_Pages.Home home = login.LoginButton_Click(emailLogin);

                Framework.Mobile_Pages.ViewedYou viewedYou = home.ViewedYouLink_Click();

                // verify that we have 10 people on the page
                int searchResultsCount = viewedYou.SearchResultsCount();
                Assert.IsTrue(searchResultsCount == 10, "ERROR - VIEWED YOU PAGE - We should be getting back 10 results on our page from our results but we are not.\r\nWe are currently only getting " + searchResultsCount + " results.");

                // show more matches and verify that we now have 20 people on the page
                viewedYou.ShowMoreMatchesButton_Click();
                searchResultsCount = viewedYou.SearchResultsCount();
                Assert.IsTrue(searchResultsCount == 20, "ERROR - VIEWED YOU PAGE - We should be getting back 20 results on our page from our results but we are not.\r\nWe are currently only getting " + searchResultsCount + " results.");

                // show more matches and verify that we now have 30 people on the page
                viewedYou.ShowMoreMatchesButton_Click();
                searchResultsCount = viewedYou.SearchResultsCount();
                Assert.IsTrue(searchResultsCount == 30, "ERROR - VIEWED YOU PAGE - We should be getting back 30 results on our page from our results but we are not.\r\nWe are currently only getting " + searchResultsCount + " results.");

                TearDown();

                log.Info("END TEST - JDate_Mobile/Regression/Member/ViewedYouPagination");
            }
            catch (Exception e)
            {
                CleanupFailedTestcase(e);
                throw;
            }
        }


        [Test]
        public void Validate_MemberProfile_Info()
        {
            try
            {
                log.Info("START TEST - JDate_Mobile/Regression/Member/MemberProfile");
                Setup();

                StartTest_UsingJDate_Mobile();

                string emailLogin = System.Configuration.ConfigurationManager.AppSettings["JDate_SubscribedUser1_Email"];
                string password = System.Configuration.ConfigurationManager.AppSettings["JDate_SubscribedUser1_Password"];

                Framework.Mobile_Pages.Login login = new Framework.Mobile_Pages.Login(driver);

                login.EmailTextbox_Write(emailLogin);
                login.PasswordTextbox_Write(password);

                Framework.Mobile_Pages.Home home = login.LoginButton_Click(emailLogin);

                Framework.Mobile_Pages.Search search = home.SearchLink_Click();

                // select a member and verify that the user is taken to their Profile Page
                Framework.Mobile_Pages.YourProfile yourProfile = search.SearchResults_Click(1);

                Assert.IsTrue(yourProfile.UsernameExists(), "ERROR - MEMBER PROFILE PAGE - The user's username should appear on this page but does not.");

                Assert.IsTrue(yourProfile.AgeExists(), "ERROR - MEMBER PROFILE PAGE - The user's age should appear on this page but does not.");

                Assert.IsTrue(yourProfile.CityExists(), "ERROR - MEMBER PROFILE PAGE - The user's city should appear on this page but does not.");

                Assert.IsTrue(yourProfile.LastLoginExists(), "ERROR - MEMBER PROFILE PAGE - The user's last login should appear on this page but does not.");

                Assert.IsTrue(yourProfile.MyDetailsExists(), "ERROR - MEMBER PROFILE PAGE - My Details should appear on this page but does not.");

                Assert.IsTrue(yourProfile.InMyOwnWordsExists(), "ERROR - MEMBER PROFILE PAGE - In My Own Words should appear on this page but does not.");

                Assert.IsTrue(yourProfile.MyIdealMatchExists(), "ERROR - MEMBER PROFILE PAGE - My Ideal Match should appear on this page but does not.");

                TearDown();

                log.Info("END TEST - JDate_Mobile/Regression/Member/MemberProfile");
            }
            catch (Exception e)
            {
                CleanupFailedTestcase(e);
                throw;
            }
        }


    }
}
