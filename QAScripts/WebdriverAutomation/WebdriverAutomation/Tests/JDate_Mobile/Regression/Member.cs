﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.Events;
using NUnit.Framework;
using log4net;
using WebdriverAutomation.Tests.ReusableCode;

namespace WebdriverAutomation.Tests.JDate_Mobile.Regression
{
    [TestFixture]
    public class Member : BaseTest
    {
        //  10/5/12 - Registering on the web with using the User Agent plugin takes us to the web version Reg now instead of the mobile version.  Ray will test this manually from now on.
        //[Test]
        //public void Validate_Registration()
        //{
        //    try
        //    {
        //        log.Info("START TEST - JDate_Mobile/Regression/Member/Registration");

        //        Setup();

        //        StartTest_UsingJDate_Mobile_WithDummyPRM();

        //        WebdriverAutomation.Framework.Mobile_Pages.Landing landing = new WebdriverAutomation.Framework.Mobile_Pages.Landing(driver);

        //        Framework.Mobile_Pages.Registration registration = landing.FreeToBrowseButton_Click();
        
                ////page 1
                //registration.YouAreARadioList_Select(1);

                //registration.ZipCodeTextbox_Write("90210");

                //string emailAddress = "QAAutomation_" + DateTime.Now.ToString() + "@gmail.com";
                //emailAddress = emailAddress.Replace("/", "_").Replace(" ", "_").Replace(":", "_");
                //registration.EmailTextbox_Write(emailAddress);

                //registration.ChooseAUsernameTextbox_Write("QAAutomation");
                //string password = "Automation";
                //registration.ChooseAPasswordTextbox_Write(password);

                //registration.BirthdateMonthDropdown_Select("Dec");
                //registration.BirthdateYearDropdown_Select("10");
                //registration.BirthdateYearTextbox_Select("1982");

                //registration.ContinueButton();

                //// page 2
                //List<string> lookingForAList = new List<string>();
                //lookingForAList.Add("A date");
                //lookingForAList.Add("Activity Partner");

                //registration.LookingForACheckboxes_Check = lookingForAList;

                //registration.RelationshipStatusDropdown_Select(2);
                //registration.SmokingHabitsDropdown_Select(2);
                ////registration.DrinkingHabitsDropdown_Select(2);
                ////registration.DoYouKeepKosherDropdown_Select(2);
                //registration.EducationDropdown_Select(2);

                //registration.ContinueButton_Step2();

                //// page 3
                //registration.OccupationDropdown_Select(2);
                ////registration.EthnicityDropdown_Select(2);
                //registration.ReligiousBackgroundDropdown_Select(2);
                //registration.DoYouGoToSynagogueDropdown_Select(2);

                ////registration.IWouldLikeToRecieveSpecialOffersCheckbox_Check();
                ////registration.IHaveReadAndAgreeToTheTermsAndConditionsCheckbox_Check();

                //Framework.Mobile_Pages.ReadyToIntroduceYourself readyToIntroduceYourself = registration.ContinueButton_Step3();

        //        Framework.Mobile_Pages.Home home = readyToIntroduceYourself.GoToTheHomePageButton_Click();

        //        //Remove the user we just created on the full jdate.com site
        //        driver.Navigate().GoToUrl(System.Configuration.ConfigurationManager.AppSettings["JDate"]);

        //        WebdriverAutomation.Tests.ReusableCode.LoggingInAndOut loggingInAndOut = new LoggingInAndOut();

        //        WebdriverAutomation.Framework.Site_Pages.PleaseUpdateYourProfile pleaseUpdateYourProfile = loggingInAndOut.LogIn_NoFirstName(driver, log, emailAddress, password);

        //        // Please Update Your Profile! page appears with Upload a photo / About Me 
        //        pleaseUpdateYourProfile.UpdateLaterLink_AboutMe_Click();

        //        //string firstName = "QA_Automation";
        //        //pleaseUpdateYourProfile.EnterYourFirstNameTextbox_Write(firstName);
        //        //pleaseUpdateYourProfile.SaveButton_Click_StayOnSamePage();

        //        // Click on "I Accept" to the "Important Notice" about promising never to sen money etc. to other members
        //        WebdriverAutomation.Framework.Site_Pages.AnImportantNotice anImportantNotice = new Framework.Site_Pages.AnImportantNotice(driver);

        //        WebdriverAutomation.Framework.Site_Pages.Home homeFullSite = anImportantNotice.IAccept_Click();

        //        WebdriverAutomation.Framework.Site_Pages.YourAccount yourAccount = homeFullSite.Header.YourAccountLink_Click();

        //        WebdriverAutomation.Framework.Site_Pages.RemoveMyProfile removeMyProfile = yourAccount.RemoveMyProfileLink_Click();

        //        removeMyProfile.PleaseTellUsWhyYouAreRemovingYourProfileRadioButton_Select(7);
        //        removeMyProfile.RemoveButton_Click();

        //        TearDown();

        //        log.Info("END TEST - JDate_Mobile/Regression/Member/Registration");
        //    }

        //    catch (Exception e)
        //    {
        //        CleanupFailedTestcase(e);
        //        throw;
        //    }
        //}



        [Test]
        public void Validate_OmnitureExistsInLandingAndRegPages()
        {
            string errorLogNotes = string.Empty;

            try
            {
                log.Info("START TEST - JDate/Regression/Member/OmnitureExistsInLandingAndRegPages");

                Setup();

                StartTest_UsingJDate_Mobile_UsingDummyPRM_AsMobileDevice();

                WebdriverAutomation.Framework.Mobile_Pages.Landing landing = new WebdriverAutomation.Framework.Mobile_Pages.Landing(driver);

                string omnitureSearchQuery1 = "s.prop";
                string omnitureSearchQuery2 = "s.eVar";

                if (!driver.PageSource.Contains(omnitureSearchQuery1) || !driver.PageSource.Contains(omnitureSearchQuery2))
                    Assert.Fail("In the Landing Page we are unable to find any omniture variables beginning with 's.prop' or 's.eVar' within the page source.");

                Framework.Mobile_Pages.Registration registration = landing.FreeToBrowseButton_Click();

                //page 1
                registration.YouAreARadioList_Select(1);

                registration.ZipCodeTextbox_Write("90210");

                string emailAddress = "QAAutomation_" + DateTime.Now.ToString() + "@gmail.com";
                emailAddress = emailAddress.Replace("/", "_").Replace(" ", "_").Replace(":", "_");
                registration.EmailTextbox_Write(emailAddress);

                registration.ChooseAUsernameTextbox_Write("QAAutomation");
                string password = "Automation";
                registration.ChooseAPasswordTextbox_Write(password);

                registration.BirthdateMonthDropdown_Select("Dec");
                registration.BirthdateYearDropdown_Select("10");
                registration.BirthdateYearTextbox_Select("1982");

                registration.ContinueButton();

                // page 2
                List<string> lookingForAList = new List<string>();
                lookingForAList.Add("A date");
                lookingForAList.Add("Activity Partner");

                registration.LookingForACheckboxes_Check = lookingForAList;

                registration.RelationshipStatusDropdown_Select(2);
                registration.SmokingHabitsDropdown_Select(2);
                //registration.DrinkingHabitsDropdown_Select(2);
                //registration.DoYouKeepKosherDropdown_Select(2);
                registration.EducationDropdown_Select(2);

                registration.ContinueButton_Step2();

                // page 3
                registration.OccupationDropdown_Select(2);
                //registration.EthnicityDropdown_Select(2);
                registration.ReligiousBackgroundDropdown_Select(2);
                registration.DoYouGoToSynagogueDropdown_Select(2);

                //registration.IWouldLikeToRecieveSpecialOffersCheckbox_Check();
                //registration.IHaveReadAndAgreeToTheTermsAndConditionsCheckbox_Check();

                Framework.Mobile_Pages.ReadyToIntroduceYourself readyToIntroduceYourself = registration.ContinueButton_Step3();

                if (!driver.PageSource.Contains(omnitureSearchQuery1) || !driver.PageSource.Contains(omnitureSearchQuery2))
                    Assert.Fail("In the Registration Page we are unable to find any omniture variables beginning with 's.prop' or 's.eVar' within the page source.");

                Framework.Mobile_Pages.Home home = readyToIntroduceYourself.GoToTheHomePageButton_Click();

                //Remove the user we just created on the full jdate.com site
                driver.Navigate().GoToUrl(System.Configuration.ConfigurationManager.AppSettings["JDate"]);

                WebdriverAutomation.Tests.ReusableCode.LoggingInAndOut loggingInAndOut = new LoggingInAndOut();

                WebdriverAutomation.Framework.Site_Pages.PleaseUpdateYourProfile pleaseUpdateYourProfile = loggingInAndOut.LogIn_NoFirstName(driver, log, emailAddress, password);

                // Please Update Your Profile! page appears with Upload a photo / About Me 
                pleaseUpdateYourProfile.UpdateLaterLink_AboutMe_Click();

                //string firstName = "QA_Automation";
                //pleaseUpdateYourProfile.EnterYourFirstNameTextbox_Write(firstName);
                //pleaseUpdateYourProfile.SaveButton_Click_StayOnSamePage();

                // Click on "I Accept" to the "Important Notice" about promising never to sen money etc. to other members
                WebdriverAutomation.Framework.Site_Pages.AnImportantNotice anImportantNotice = new Framework.Site_Pages.AnImportantNotice(driver);

                WebdriverAutomation.Framework.Site_Pages.Home homeFullSite = anImportantNotice.IAccept_Click();

                WebdriverAutomation.Framework.Site_Pages.YourAccount yourAccount = homeFullSite.Header.YourAccountLink_Click();

                WebdriverAutomation.Framework.Site_Pages.RemoveMyProfile removeMyProfile = yourAccount.RemoveMyProfileLink_Click();

                removeMyProfile.PleaseTellUsWhyYouAreRemovingYourProfileRadioButton_Select(7);
                removeMyProfile.RemoveButton_Click();

                TearDown();

                log.Info("END TEST - JDate/Regression/Member/OmnitureExistsInLandingAndRegPages");
            }
            catch (Exception e)
            {
                CleanupFailedTestcase(e, errorLogNotes);
                throw;
            }
        }


        [Test]
        public void Validate_Login()
        {
            try
            {
                log.Info("START TEST - JDate_Mobile/Regression/Member/Login");

                Setup();

                StartTest_UsingJDate_Mobile();

                string emailLogin = System.Configuration.ConfigurationManager.AppSettings["JDate_SanFranciscoRegisteredUser1_Email"];
                string password = System.Configuration.ConfigurationManager.AppSettings["JDate_SanFranciscoRegisteredUser1_Password"];

                Framework.Mobile_Pages.Login login = new Framework.Mobile_Pages.Login(driver);

                login.EmailTextbox_Write(emailLogin);
                login.PasswordTextbox_Write(password);

                Framework.Mobile_Pages.Home home = login.LoginButton_Click(emailLogin);

                TearDown();

                log.Info("END TEST - JDate_Mobile/Regression/Member/Login");
            }
            catch (Exception e)
            {
                CleanupFailedTestcase(e);
                throw;
            }
        }


        [Test]
        public void Validate_UploadPhotosPageExists()
        {
            try
            {
                log.Info("START TEST - JDate_Mobile/Regression/Member/UploadPhotosPageExists");

                Setup();

                StartTest_UsingJDate_Mobile();

                string emailLogin = System.Configuration.ConfigurationManager.AppSettings["JDate_RegisteredUser1_Email"];
                string password = System.Configuration.ConfigurationManager.AppSettings["JDate_RegisteredUser1_Password"];

                Framework.Mobile_Pages.Login login = new Framework.Mobile_Pages.Login(driver);

                login.EmailTextbox_Write(emailLogin);
                login.PasswordTextbox_Write(password);

                Framework.Mobile_Pages.Home home = login.LoginButton_Click(emailLogin);

                Framework.Mobile_Pages.UploadPhotos uploadedPhotos = home.UploadPhotosLink_Click();

                TearDown();

                log.Info("END TEST - JDate_Mobile/Regression/Member/UploadPhotosPageExists");
            }
            catch (Exception e)
            {
                CleanupFailedTestcase(e);
                throw;
            }
        }


        [Test]
        public void Validate_SearchResults()
        {
            try
            {
                log.Info("START TEST - JDate_Mobile/Regression/Member/SearchResults");

                Setup();

                StartTest_UsingJDate_Mobile();

                string emailLogin = System.Configuration.ConfigurationManager.AppSettings["JDate_SanFranciscoRegisteredUser1_Email"];
                string password = System.Configuration.ConfigurationManager.AppSettings["JDate_SanFranciscoRegisteredUser1_Password"];

                Framework.Mobile_Pages.Login login = new Framework.Mobile_Pages.Login(driver);

                login.EmailTextbox_Write(emailLogin);
                login.PasswordTextbox_Write(password);

                Framework.Mobile_Pages.Home home = login.LoginButton_Click(emailLogin);

                Framework.Mobile_Pages.Search search = home.SearchLink_Click();

                string youreA = "Man";
                string seekingA = "Woman";
                string ageFrom = "20";
                string ageTo = "30";
                string locatedWithin = "5 Miles";

                // add all cities located within 5 miles of the current user's location - it should be 94133 (San Francisco) or this test will fail
                List<string> cities = new List<string>();
                cities.Add("Alameda");
                cities.Add("Albany");
                cities.Add("Berkeley");
                cities.Add("Broadmoor Vlg");
                cities.Add("Daly City");
                cities.Add("El Cerrito");
                cities.Add("Emeryville");
                cities.Add("Mill Valley");
                cities.Add("Oakland");
                cities.Add("Piedmont");
                cities.Add("Pt Richmond");
                cities.Add("Richmond");
                cities.Add("S San Fran");
                cities.Add("San Francisco");
                cities.Add("Sausalito");
                cities.Add("South San Francisco");
                cities.Add("Tiburon");
                cities.Add("UC Berkeley");

                search.PreferencesTab_Click();

                search.YouAreAToggle_ManClick();
                search.SeekingAToggle_WomanClick();
                search.ToTextbox_Write(ageTo);
                search.AgeTextbox_Write(ageFrom);
                search.LocatedWithinDropdown_Select(locatedWithin);
                search.SearchButton_Click();

                // verify that quick search results appear
                Assert.True(search.SearchResultsCount() > 0, "ERROR - SEARCH PAGE - After doing a quick search, no results appeared.  \r\nThe search was for " + youreA + " seeking a " + seekingA + " searching within " + locatedWithin + " of the user's zip code");

                // verify that the username appears for all users
                Assert.True(search.DoAllResultsDisplayUsername(), "ERROR - SEARCH PAGE - After doing a quick search, not all of our search results display a username.");

                // verify that the results are all within the correct age range
                Assert.True(search.SearchResultsWithinAgeRange(Convert.ToInt32(ageFrom), Convert.ToInt32(ageTo)), "ERROR - SEARCH PAGE - After doing a quick search, the ages of the search results were not in the correct range.  \r\nThe search was for ages " + ageFrom + " to " + ageTo);

                // verify that the results are all within the correct zip code (checking for 5 miles within 94133 - San Francisco)
                string listOfCities = cities[0];

                for (int i = 1; i < cities.Count; i++)
                    listOfCities = listOfCities + ", " + cities[i];

                Assert.True(search.SearchResultsWithinExpectedCities(cities), "ERROR - SEARCH PAGE - After doing a quick search, the location range of the search result was not correct.  \r\nThe search was for " + locatedWithin + " within the user's zip code and the search results should have all returned one of the following cities: " + listOfCities);

                // verify that the Last Login appears for all users
                Assert.True(search.DoAllResultsDisplayLastLoggedIn(), "ERROR - SEARCH PAGE - After doing a quick search, not all of our search results display their last logged in date.");

                // select a member and verify that the user is taken to their Profile Page
                Framework.Mobile_Pages.YourProfile profile = search.SearchResults_Click(1);

                TearDown();

                log.Info("END TEST - JDate_Mobile/Regression/Member/SearchResults");
            }
            catch (Exception e)
            {
                CleanupFailedTestcase(e);
                throw;
            }
        }

        /// <summary>
        /// In order to verify pagination we need to ensure that our search results have enough results to span more than one page (12 results).
        /// Using a search of Located Within 500 miles of the user's current location for this test case.
        /// </summary>
        [Test]
        public void Validate_SearchPagination()
        {
            try
            {
                log.Info("START TEST - JDate_Mobile/Regression/Member/SearchPagination");
                Setup();

                StartTest_UsingJDate_Mobile();

                string emailLogin = System.Configuration.ConfigurationManager.AppSettings["JDate_AllAccessUser1_Email"];
                string password = System.Configuration.ConfigurationManager.AppSettings["JDate_AllAccessUser1_Password"];

                Framework.Mobile_Pages.Login login = new Framework.Mobile_Pages.Login(driver);

                login.EmailTextbox_Write(emailLogin);
                login.PasswordTextbox_Write(password);

                Framework.Mobile_Pages.Home home = login.LoginButton_Click(emailLogin);

                Framework.Mobile_Pages.Search search = home.SearchLink_Click();

                string seekingA = "Woman";
                string ageFrom = "20";
                string ageTo = "30";
                string locatedWithin = "100 Miles";

                search.PreferencesTab_Click();

                search.YouAreAToggle_ManClick();
                search.SeekingAToggle_WomanClick();
                search.ToTextbox_Write(ageTo);
                search.AgeTextbox_Write(ageFrom);
                search.LocatedWithinDropdown_Select(locatedWithin);
                search.SearchButton_Click();

                // verify that we have 10 people on the page
                int searchResultsCount = search.SearchResultsCount();
                Assert.IsTrue(searchResultsCount == 10, "ERROR - SEARCH PAGE - We should be getting back 10 results on our page from our search but we are not.\r\nWe are currently searching for a " + seekingA + " between the ages of " + ageFrom + " to " + ageTo + " within " + locatedWithin + " miles of our current location and only getting " + searchResultsCount + " results.");

                // show more matches and verify that we now have 20 people on the page
                search.ShowMoreMatchesButton_Click();
                searchResultsCount = search.SearchResultsCount();
                Assert.IsTrue(searchResultsCount == 20, "ERROR - SEARCH PAGE - We should be getting back 20 results on our page from our search but we are not.\r\nWe are currently searching for a " + seekingA + " between the ages of " + ageFrom + " to " + ageTo + " within " + locatedWithin + " miles of our current location and only getting " + searchResultsCount + " results.");

                // show more matches and verify that we now have 30 people on the page
                search.ShowMoreMatchesButton_Click();
                searchResultsCount = search.SearchResultsCount();
                Assert.IsTrue(searchResultsCount == 30, "ERROR - SEARCH PAGE - We should be getting back 30 results on our page from our search but we are not.\r\nWe are currently searching for a " + seekingA + " between the ages of " + ageFrom + " to " + ageTo + " within " + locatedWithin + " miles of our current location and only getting " + searchResultsCount + " results.");

                TearDown();

                log.Info("END TEST - JDate_Mobile/Regression/Member/SearchPagination");
            }
            catch (Exception e)
            {
                CleanupFailedTestcase(e);
                throw;
            }
        }


        [Test]
        public void Validate_ViewedYouResults()
        {
            try
            {
                log.Info("START TEST - JDate_Mobile/Regression/Member/ViewedYouResults");

                Setup();

                StartTest_UsingJDate_Mobile();

                string emailLogin = System.Configuration.ConfigurationManager.AppSettings["JDate_RegisteredUser1_Email"];
                string password = System.Configuration.ConfigurationManager.AppSettings["JDate_RegisteredUser1_Password"];

                Framework.Mobile_Pages.Login login = new Framework.Mobile_Pages.Login(driver);

                login.EmailTextbox_Write(emailLogin);
                login.PasswordTextbox_Write(password);

                Framework.Mobile_Pages.Home home = login.LoginButton_Click(emailLogin);

                Framework.Mobile_Pages.ViewedYou viewedYou = home.ViewedYouLink_Click();

                // verify that results appear
                Assert.True(viewedYou.SearchResultsCount() > 0, "ERROR - VIEWED YOU PAGE - On the Viewed You Page, no results appeared.");

                // verify that the username appears for all users
                Assert.True(viewedYou.DoAllResultsDisplayUsername(), "ERROR - VIEWED YOU PAGE - On the Viewed You Page, not all of our search results display a username.");

                // verify that the Age appears for all users
                Assert.True(viewedYou.DoAllResultsDisplayAge(), "ERROR - VIEWED YOU PAGE - On the Viewed You Page, not all of our search results display a username.");

                // verify that the City appears for all users
                Assert.True(viewedYou.DoAllResultsDisplayCity(), "ERROR - VIEWED YOU PAGE - On the Viewed You Page, not all of our search results display a username.");

                // verify that the Last Login appears for all users
                Assert.True(viewedYou.DoAllResultsDisplayLastLoggedIn(), "ERROR - VIEWED YOU PAGE - On the Viewed You Page, not all of our search results display a username.");

                // select a member and verify that the user is taken to their Profile Page
                Framework.Mobile_Pages.YourProfile profile = viewedYou.ViewedYouResults_Click(1);

                TearDown();

                log.Info("END TEST - JDate_Mobile/Regression/Member/ViewedYouResults");
            }
            catch (Exception e)
            {
                CleanupFailedTestcase(e);
                throw;
            }
        }


        /// <summary>
        /// In order to verify pagination we need to ensure that our search results have enough results to span more than one page.
        /// </summary>
        [Test]
        public void Validate_ViewedYouPagination()
        {
            try
            {
                log.Info("START TEST - JDate_Mobile/Regression/Member/ViewedYouPagination");
                Setup();

                StartTest_UsingJDate_Mobile();

                string emailLogin = System.Configuration.ConfigurationManager.AppSettings["JDate_RegisteredUser1_Email"];
                string password = System.Configuration.ConfigurationManager.AppSettings["JDate_RegisteredUser1_Password"];

                Framework.Mobile_Pages.Login login = new Framework.Mobile_Pages.Login(driver);

                login.EmailTextbox_Write(emailLogin);
                login.PasswordTextbox_Write(password);

                Framework.Mobile_Pages.Home home = login.LoginButton_Click(emailLogin);

                Framework.Mobile_Pages.ViewedYou viewedYou = home.ViewedYouLink_Click();

                // verify that we have 10 people on the page
                int searchResultsCount = viewedYou.SearchResultsCount();
                Assert.IsTrue(searchResultsCount == 10, "ERROR - VIEWED YOU PAGE - We should be getting back 10 results on our page from our results but we are not.\r\nWe are currently only getting " + searchResultsCount + " results.");

                // show more matches and verify that we now have 20 people on the page
                viewedYou.ShowMoreMatchesButton_Click();
                searchResultsCount = viewedYou.SearchResultsCount();
                Assert.IsTrue(searchResultsCount == 20, "ERROR - VIEWED YOU PAGE - We should be getting back 20 results on our page from our results but we are not.\r\nWe are currently only getting " + searchResultsCount + " results.");

                // show more matches and verify that we now have 30 people on the page
                viewedYou.ShowMoreMatchesButton_Click();
                searchResultsCount = viewedYou.SearchResultsCount();
                Assert.IsTrue(searchResultsCount == 30, "ERROR - VIEWED YOU PAGE - We should be getting back 30 results on our page from our results but we are not.\r\nWe are currently only getting " + searchResultsCount + " results.");

                TearDown();

                log.Info("END TEST - JDate_Mobile/Regression/Member/ViewedYouPagination");
            }
            catch (Exception e)
            {
                CleanupFailedTestcase(e);
                throw;
            }
        }


        [Test]
        public void Validate_FavoritedYouResults()
        {
            try
            {
                log.Info("START TEST - JDate_Mobile/Regression/Member/FavoritedYouResults");

                Setup();

                StartTest_UsingJDate_Mobile();

                string emailLogin = System.Configuration.ConfigurationManager.AppSettings["JDate_RegisteredUser1_Email"];
                string password = System.Configuration.ConfigurationManager.AppSettings["JDate_RegisteredUser1_Password"];

                Framework.Mobile_Pages.Login login = new Framework.Mobile_Pages.Login(driver);

                login.EmailTextbox_Write(emailLogin);
                login.PasswordTextbox_Write(password);

                Framework.Mobile_Pages.Home home = login.LoginButton_Click(emailLogin);

                Framework.Mobile_Pages.FavoritedYou favoritedYou = home.FavoritedYouLink_Click();

                // verify that results appear
                Assert.True(favoritedYou.SearchResultsCount() > 0, "ERROR - FAVORITED YOU PAGE - No results appeared.");

                // verify that the username appears for all users
                Assert.True(favoritedYou.DoAllResultsDisplayUsername(), "ERROR - FAVORITED YOU PAGE - Not all of our results display a username.");

                // verify that the Age appears for all users
                Assert.True(favoritedYou.DoAllResultsDisplayAge(), "ERROR - FAVORITED YOU PAGE - Not all of our results display an age.");

                // verify that the City appears for all users
                Assert.True(favoritedYou.DoAllResultsDisplayCity(), "ERROR - FAVORITED YOU PAGE - Not all of our results display a city.");

                // verify that the Last Login appears for all users
                Assert.True(favoritedYou.DoAllResultsDisplayLastLoggedIn(), "ERROR - FAVORITED YOU PAGE - Not all of our results display a last logged in date.");

                // select a member and verify that the user is taken to their Profile Page
                Framework.Mobile_Pages.YourProfile profile = favoritedYou.ViewedYouResults_Click(1);

                TearDown();

                log.Info("END TEST - JDate_Mobile/Regression/Member/FavoritedYouResults");
            }
            catch (Exception e)
            {
                CleanupFailedTestcase(e);
                throw;
            }
        }


        /// <summary>
        /// In order to verify pagination we need to ensure that our search results have enough results to span more than one page
        /// </summary>
        [Test]
        public void Validate_FavoritedYouPagination()
        {
            try
            {
                log.Info("START TEST - JDate_Mobile/Regression/Member/FavoritedYouPagination");
                Setup();

                StartTest_UsingJDate_Mobile();

                string emailLogin = System.Configuration.ConfigurationManager.AppSettings["JDate_RegisteredUser1_Email"];
                string password = System.Configuration.ConfigurationManager.AppSettings["JDate_RegisteredUser1_Password"];

                Framework.Mobile_Pages.Login login = new Framework.Mobile_Pages.Login(driver);

                login.EmailTextbox_Write(emailLogin);
                login.PasswordTextbox_Write(password);

                Framework.Mobile_Pages.Home home = login.LoginButton_Click(emailLogin);

                Framework.Mobile_Pages.FavoritedYou favoritedYou = home.FavoritedYouLink_Click();

                // verify that we have 10 people on the page
                int searchResultsCount = favoritedYou.SearchResultsCount();
                Assert.IsTrue(searchResultsCount == 10, "ERROR - VIEWED YOU PAGE - We should be getting back 10 results on our page from our results but we are not.\r\nWe are currently only getting " + searchResultsCount + " results.");

                // show more matches and verify that we now have more than 10 people on the page
                favoritedYou.ShowMoreMatchesButton_Click();
                searchResultsCount = favoritedYou.SearchResultsCount();
                Assert.IsTrue(searchResultsCount > 10, "ERROR - VIEWED YOU PAGE - We should be getting back more than 10 results on our page from our results but we are not.\r\nWe are currently only getting " + searchResultsCount + " results.");

                //// show more matches and verify that we now have more than 20 people on the page
                //viewedYou.ShowMoreMatchesButton_Click();
                //searchResultsCount = viewedYou.SearchResultsCount();
                //Assert.IsTrue(searchResultsCount == 30, "ERROR - VIEWED YOU PAGE - We should be getting back more than 20 results on our page from our results but we are not.\r\nWe are currently only getting " + searchResultsCount + " results.");

                TearDown();

                log.Info("END TEST - JDate_Mobile/Regression/Member/FavoritedYouPagination");
            }
            catch (Exception e)
            {
                CleanupFailedTestcase(e);
                throw;
            }
        }


        [Test]
        public void Validate_YourFavoritesResults()
        {
            try
            {
                log.Info("START TEST - JDate_Mobile/Regression/Member/YourFavoritesResults");

                Setup();

                StartTest_UsingJDate_Mobile();

                string emailLogin = System.Configuration.ConfigurationManager.AppSettings["JDate_SubscribedUser1_Email"];
                string password = System.Configuration.ConfigurationManager.AppSettings["JDate_SubscribedUser1_Password"];

                Framework.Mobile_Pages.Login login = new Framework.Mobile_Pages.Login(driver);

                login.EmailTextbox_Write(emailLogin);
                login.PasswordTextbox_Write(password);

                Framework.Mobile_Pages.Home home = login.LoginButton_Click(emailLogin);

                Framework.Mobile_Pages.YourFavorites yourFavorites = home.YourFavoritesLink_Click();

                // verify that results appear
                Assert.True(yourFavorites.SearchResultsCount() > 0, "ERROR - YOUR FAVORITES PAGE - No results appeared.");

                // verify that the username appears for all users
                Assert.True(yourFavorites.DoAllResultsDisplayUsername(), "ERROR - YOUR FAVORITES PAGE - Not all of our results display a username.");

                // verify that the Age appears for all users
                Assert.True(yourFavorites.DoAllResultsDisplayAge(), "ERROR - YOUR FAVORITES PAGE - Not all of our results display an age.");

                // verify that the City appears for all users
                Assert.True(yourFavorites.DoAllResultsDisplayCity(), "ERROR - YOUR FAVORITES PAGE - Not all of our results display a city.");

                // verify that the Last Login appears for all users
                Assert.True(yourFavorites.DoAllResultsDisplayLastLoggedIn(), "ERROR - YOUR FAVORITES PAGE - Not all of our results display a last logged in date.");

                // select a member and verify that the user is taken to their Profile Page
                Framework.Mobile_Pages.YourProfile profile = yourFavorites.ViewedYouResults_Click(1);

                TearDown();

                log.Info("END TEST - JDate_Mobile/Regression/Member/YourFavoritesResults");
            }
            catch (Exception e)
            {
                CleanupFailedTestcase(e);
                throw;
            }
        }


        /// <summary>
        /// In order to verify pagination we need to ensure that our search results have enough results to span more than one page
        /// </summary>
        [Test]
        public void Validate_YourFavoritesPagination()
        {
            try
            {
                log.Info("START TEST - JDate_Mobile/Regression/Member/YourFavoritesPagination");
                Setup();

                StartTest_UsingJDate_Mobile();

                string emailLogin = System.Configuration.ConfigurationManager.AppSettings["JDate_SubscribedUser1_Email"];
                string password = System.Configuration.ConfigurationManager.AppSettings["JDate_SubscribedUser1_Password"];

                Framework.Mobile_Pages.Login login = new Framework.Mobile_Pages.Login(driver);

                login.EmailTextbox_Write(emailLogin);
                login.PasswordTextbox_Write(password);

                Framework.Mobile_Pages.Home home = login.LoginButton_Click(emailLogin);

                Framework.Mobile_Pages.YourFavorites yourFavorites = home.YourFavoritesLink_Click();

                // verify that we have 10 people on the page
                int searchResultsCount = yourFavorites.SearchResultsCount();
                Assert.IsTrue(searchResultsCount == 10, "ERROR - YOUR FAVORITES PAGE - We should be getting back 10 results on our page from our results but we are not.\r\nWe are currently only getting " + searchResultsCount + " results.");

                // show more matches and verify that we now have more than 10 people on the page
                yourFavorites.ShowMoreMatchesButton_Click();
                searchResultsCount = yourFavorites.SearchResultsCount();
                Assert.IsTrue(searchResultsCount > 10, "ERROR - YOUR FAVORITES PAGE - We should be getting back more than 10 results on our page from our results but we are not.\r\nWe are currently only getting " + searchResultsCount + " results.");

                //// show more matches and verify that we now have more than 20 people on the page
                //viewedYou.ShowMoreMatchesButton_Click();
                //searchResultsCount = viewedYou.SearchResultsCount();
                //Assert.IsTrue(searchResultsCount == 30, "ERROR - VIEWED YOU PAGE - We should be getting back more than 20 results on our page from our results but we are not.\r\nWe are currently only getting " + searchResultsCount + " results.");

                TearDown();

                log.Info("END TEST - JDate_Mobile/Regression/Member/YourFavoritesPagination");
            }
            catch (Exception e)
            {
                CleanupFailedTestcase(e);
                throw;
            }
        }


        [Test]
        public void Validate_YourProfile()
        {
            try
            {
                log.Info("START TEST - JDate_Mobile/Regression/Member/YourProfile");
                Setup();

                StartTest_UsingJDate_Mobile();

                string emailLogin = System.Configuration.ConfigurationManager.AppSettings["JDate_RegisteredUser1_Email"];
                string password = System.Configuration.ConfigurationManager.AppSettings["JDate_RegisteredUser1_Password"];

                Framework.Mobile_Pages.Login login = new Framework.Mobile_Pages.Login(driver);

                login.EmailTextbox_Write(emailLogin);
                login.PasswordTextbox_Write(password);

                Framework.Mobile_Pages.Home home = login.LoginButton_Click(emailLogin);

                Framework.Mobile_Pages.YourProfile yourProfile = home.YourProfileLink_Click();

                Assert.IsTrue(yourProfile.UsernameExists(), "ERROR - YOUR PROFILE PAGE - The user's username should appear on this page but does not.");

                Assert.IsTrue(yourProfile.AgeExists(), "ERROR - YOUR PROFILE PAGE - The user's age should appear on this page but does not.");

                Assert.IsTrue(yourProfile.CityExists(), "ERROR - YOUR PROFILE PAGE - The user's city should appear on this page but does not.");

                Assert.IsTrue(yourProfile.LastLoginExists(), "ERROR - YOUR PROFILE PAGE - The user's last login should appear on this page but does not.");

                Assert.IsTrue(yourProfile.MyDetailsExists(), "ERROR - YOUR PROFILE PAGE - My Details should appear on this page but does not.");

                Assert.IsTrue(yourProfile.InMyOwnWordsExists(), "ERROR - YOUR PROFILE PAGE - In My Own Words should appear on this page but does not.");

                Assert.IsTrue(yourProfile.MyIdealMatchExists(), "ERROR - YOUR PROFILE PAGE - My Ideal Match should appear on this page but does not.");

                TearDown();

                log.Info("END TEST - JDate_Mobile/Regression/Member/YourProfile");
            }
            catch (Exception e)
            {
                CleanupFailedTestcase(e);
                throw;
            }
        }


        [Test]
        public void Validate_MemberProfile_Info()
        {
            try
            {
                log.Info("START TEST - JDate_Mobile/Regression/Member/MemberProfile");
                Setup();

                StartTest_UsingJDate_Mobile();

                string emailLogin = System.Configuration.ConfigurationManager.AppSettings["JDate_RegisteredUser1_Email"];
                string password = System.Configuration.ConfigurationManager.AppSettings["JDate_RegisteredUser1_Password"];

                Framework.Mobile_Pages.Login login = new Framework.Mobile_Pages.Login(driver);

                login.EmailTextbox_Write(emailLogin);
                login.PasswordTextbox_Write(password);

                Framework.Mobile_Pages.Home home = login.LoginButton_Click(emailLogin);

                Framework.Mobile_Pages.Search search = home.SearchLink_Click();

                // select a member and verify that the user is taken to their Profile Page
                Framework.Mobile_Pages.YourProfile yourProfile = search.SearchResults_Click(1);

                Assert.IsTrue(yourProfile.UsernameExists(), "ERROR - MEMBER PROFILE PAGE - The user's username should appear on this page but does not.");

                Assert.IsTrue(yourProfile.AgeExists(), "ERROR - MEMBER PROFILE PAGE - The user's age should appear on this page but does not.");

                Assert.IsTrue(yourProfile.CityExists(), "ERROR - MEMBER PROFILE PAGE - The user's city should appear on this page but does not.");

                Assert.IsTrue(yourProfile.LastLoginExists(), "ERROR - MEMBER PROFILE PAGE - The user's last login should appear on this page but does not.");

                Assert.IsTrue(yourProfile.MyDetailsExists(), "ERROR - MEMBER PROFILE PAGE - My Details should appear on this page but does not.");

                Assert.IsTrue(yourProfile.InMyOwnWordsExists(), "ERROR - MEMBER PROFILE PAGE - In My Own Words should appear on this page but does not.");

                Assert.IsTrue(yourProfile.MyIdealMatchExists(), "ERROR - MEMBER PROFILE PAGE - My Ideal Match should appear on this page but does not.");

                TearDown();

                log.Info("END TEST - JDate_Mobile/Regression/Member/MemberProfile");
            }
            catch (Exception e)
            {
                CleanupFailedTestcase(e);
                throw;
            }
        }


        [Test]
        public void Validate_FlirtWithAlreadyFlirtedUser()
        {
            try
            {
                log.Info("START TEST - JDate_Mobile/Regression/Member/FlirtWithAlreadyFlirtedUser");
                Setup();

                StartTest_UsingJDate_Mobile();

                string emailLogin_1 = System.Configuration.ConfigurationManager.AppSettings["JDate_SubscribedUser1_Email"];
                string password_1 = System.Configuration.ConfigurationManager.AppSettings["JDate_SubscribedUser1_Password"];

                string emailLogin_2 = System.Configuration.ConfigurationManager.AppSettings["JDate_SubscribedUser3_Email"];
                string password_2 = System.Configuration.ConfigurationManager.AppSettings["JDate_SubscribedUser3_Password"];
                string memberid_2 = System.Configuration.ConfigurationManager.AppSettings["JDate_SubscribedUser3_MemberID"];

                Framework.Mobile_Pages.Login login = new Framework.Mobile_Pages.Login(driver);

                login.EmailTextbox_Write(emailLogin_1);
                login.PasswordTextbox_Write(password_1);

                Framework.Mobile_Pages.Home home = login.LoginButton_Click(emailLogin_1);

                // jump straight to the mobile page of the user we want to email
                if (driver.Url.Contains("preprod.m.jdate.com"))
                    driver.Navigate().GoToUrl("http://preprod.m.jdate.com/profile/fullprofile/" + memberid_2);
                else
                    driver.Navigate().GoToUrl("http://m.jdate.com/profile/fullprofile/" + memberid_2);

                Framework.Mobile_Pages.YourProfile profile = new Framework.Mobile_Pages.YourProfile(driver);

                profile.FlirtLink_WithUserThatHasBeenFlirtedWithBefore_Click();

                // user 1 has already flirted with user 2 before so we expect an error message to appear.
                string flirtMessage = profile.FlirtMessage();
                Assert.IsTrue(flirtMessage == "Your flirt was not able to be sent.", "ERROR - PROFILE PAGE - The message 'Your flirt was not able to be sent' did not appear after sending the flirt. The message we see onscreen is " + flirtMessage + ".");

                //flirtMessage.CasualToggle_Click();
                ////string message = "Interested? I am";
                //flirtMessage.InterestedIAmRadiobox_Click();

                //Framework.Mobile_Pages.FlirtSent flirtSent = flirtMessage.SendFlirtButton_Click();

                //// Verify that the "Your flirt has been sent" message appears after sending the flirt
                //string flirtSentMessage = flirtSent.GetFlirtMessage();
                //Assert.IsTrue(flirtSentMessage.Contains("You have already flirted with this user"), "ERROR - FLIRT SENT PAGE - The message 'You have already flirted with this user' did not appear after sending the flirt with a user that has already been flirted with.");
                ////Assert.IsTrue(flirtSentMessage.Contains("Your flirt has been sent"), "ERROR - FLIRT SENT PAGE - The message 'Your flirt has been sent' did not appear after sending the flirt. Maybe we have already flirted with this user.");

                ////profile = flirtSent.ReturnToProfileButton_Click();

                ////// user 1 log out
                ////login = profile.Footer.LogoutLink_Click();

                ////// user 2 log in
                ////login.EmailTextbox_Write(emailLogin_2);
                ////login.PasswordTextbox_Write(password_2);

                ////home = login.LoginButton_Click(emailLogin_2);

                ////Framework.Mobile_Pages.Inbox inbox = home.InboxLink_Click_JDateSubscribedUser();

                ////// look through list of messages and verify that a message with the subject we sent appeared          
                ////string subject = "Flirt";
                ////Assert.IsTrue(inbox.DoesMessageWithCorrectSubjectAppear(subject), "ERROR - INBOX PAGE - The email with the subject line that we expect to see in our inbox is not there.");

                ////Framework.Mobile_Pages.Message messagePage = inbox.Message_Click(subject);

                ////Assert.IsTrue(messagePage.DoesCorrectSubjectAppear(subject), "ERROR - INBOX PAGE - After clicking on the message with the correct subject line, the subject that we expect from the earlier sent email does not appear in the actual Message Page.");
                ////Assert.IsTrue(messagePage.DoesCorrectMessageAppear(message), "ERROR - INBOX PAGE - After clicking on the message with the correct subject line, the message that we expect from the earlier sent email does not appear in the actual Message Page.");

                ////// delete the message
                ////inbox = messagePage.Delete_Click();

                TearDown();

                log.Info("END TEST - JDate_Mobile/Regression/Member/FlirtWithAlreadyFlirtedUser");
            }
            catch (Exception e)
            {
                CleanupFailedTestcase(e);
                throw;
            }
        }


        /// <summary>
        /// Verify that when you Favorite someone from their Profile that you appear in their "Favorited You" list when they log in.  We do not bother unfavoriting the person after we verify that the feature works because
        /// once a person is on your Favorited You list they never disappear, even if the original person who favorited you unfavorites you in the future.
        /// </summary>
        [Test]
        public void Validate_Favorite()
        {
            try
            {
                log.Info("START TEST - JDate_Mobile/Regression/Member/Favorite");
                Setup();

                StartTest_UsingJDate_Mobile();

                string emailLogin_1 = System.Configuration.ConfigurationManager.AppSettings["JDate_RegisteredUser2_Email"];
                string password_1 = System.Configuration.ConfigurationManager.AppSettings["JDate_RegisteredUser2_Password"];
                string username_1 = System.Configuration.ConfigurationManager.AppSettings["JDate_RegisteredUser2_Username"];

                string emailLogin_2 = System.Configuration.ConfigurationManager.AppSettings["JDate_RegisteredUser1_Email"];
                string password_2 = System.Configuration.ConfigurationManager.AppSettings["JDate_RegisteredUser1_Password"];
                string memberid_2 = System.Configuration.ConfigurationManager.AppSettings["JDate_RegisteredUser1_MemberID"];
                string username_2 = System.Configuration.ConfigurationManager.AppSettings["JDate_RegisteredUser1_Username"];

                Framework.Mobile_Pages.Login login = new Framework.Mobile_Pages.Login(driver);

                login.EmailTextbox_Write(emailLogin_1);
                login.PasswordTextbox_Write(password_1);

                Framework.Mobile_Pages.Home home = login.LoginButton_Click(emailLogin_1);

                // jump straight to the mobile page of the user we want to email
                if (driver.Url.Contains("preprod.m.jdate.com"))
                    driver.Navigate().GoToUrl("http://preprod.m.jdate.com/profile/fullprofile/" + memberid_2);
                else
                    driver.Navigate().GoToUrl("http://m.jdate.com/profile/fullprofile/" + memberid_2);

                Framework.Mobile_Pages.YourProfile profile = new Framework.Mobile_Pages.YourProfile(driver);

                if (profile.DoesFavoriteButtonAppear())
                    profile.Favorite_UnfavoriteButton_Click();

                // user 1 log out
                login = profile.Footer.LogoutLink_Click();

                // user 2 log in
                login.EmailTextbox_Write(emailLogin_2);
                login.PasswordTextbox_Write(password_2);

                home = login.LoginButton_Click(emailLogin_2);

                Framework.Mobile_Pages.FavoritedYou favoritedYou = home.FavoritedYouLink_Click();
                favoritedYou.ShowMoreMatchesButton_Click();   // our expected result is on page 2 since 10+ people have favorited us 7/11/12

                Assert.IsTrue(favoritedYou.DoesExpectedUserThatFavoritedYouAppear(username_1), "ERROR - FAVORITED YOU PAGE - The user " + username_1 + " favorited user " + username_2 + " but does not appear in " + username_2 + "'s Favorited You page.");

                TearDown();

                log.Info("END TEST - JDate_Mobile/Regression/Member/Favorite");
            }
            catch (Exception e)
            {
                CleanupFailedTestcase(e);
                throw;
            }
        }


        /// <summary>
        /// Verify that when the user logs out the Login Page appears.
        /// </summary>
        [Test]
        public void Validate_Logout()
        {
            try
            {
                log.Info("START TEST - JDate_Mobile/Regression/Member/Logout");
                Setup();

                StartTest_UsingJDate_Mobile();

                string emailLogin_1 = System.Configuration.ConfigurationManager.AppSettings["JDate_RegisteredUser1_Email"];
                string password_1 = System.Configuration.ConfigurationManager.AppSettings["JDate_RegisteredUser1_Password"];

                Framework.Mobile_Pages.Login login = new Framework.Mobile_Pages.Login(driver);

                login.EmailTextbox_Write(emailLogin_1);
                login.PasswordTextbox_Write(password_1);

                Framework.Mobile_Pages.Home home = login.LoginButton_Click(emailLogin_1);

                login = home.Footer.LogoutLink_Click();

                TearDown();

                log.Info("END TEST - JDate_Mobile/Regression/Member/Logout");
            }
            catch (Exception e)
            {
                CleanupFailedTestcase(e);
                throw;
            }
        }


        [Test]
        public void Validate_Fullsite()
        {
            try
            {
                log.Info("START TEST - JDate_Mobile/Regression/Member/Fullsite");
                Setup();

                StartTest_UsingJDate_Mobile();

                string emailLogin_1 = System.Configuration.ConfigurationManager.AppSettings["JDate_RegisteredUser1_Email"];
                string password_1 = System.Configuration.ConfigurationManager.AppSettings["JDate_RegisteredUser1_Password"];

                Framework.Mobile_Pages.Login login = new Framework.Mobile_Pages.Login(driver);

                login.EmailTextbox_Write(emailLogin_1);
                login.PasswordTextbox_Write(password_1);

                Framework.Mobile_Pages.Home home = login.LoginButton_Click(emailLogin_1);

                Framework.Site_Pages.Login loginFullSite = home.Footer.FullSiteLink_Click();

                TearDown();

                log.Info("END TEST - JDate_Mobile/Regression/Member/Fullsite");
            }
            catch (Exception e)
            {
                CleanupFailedTestcase(e);
                throw;
            }
        }


        [Test]
        public void Validate_FooterLinks()
        {
            try
            {
                log.Info("START TEST - JDate_Mobile/Regression/Member/FooterLinks");
                Setup();

                StartTest_UsingJDate_Mobile();

                string emailLogin_1 = System.Configuration.ConfigurationManager.AppSettings["JDate_RegisteredUser1_Email"];
                string password_1 = System.Configuration.ConfigurationManager.AppSettings["JDate_RegisteredUser1_Password"];

                Framework.Mobile_Pages.Login login = new Framework.Mobile_Pages.Login(driver);

                login.EmailTextbox_Write(emailLogin_1);
                login.PasswordTextbox_Write(password_1);

                Framework.Mobile_Pages.Home home = login.LoginButton_Click(emailLogin_1);

                Framework.Mobile_Pages.ContactUs contactUs = home.Footer.ContactUsLink_Click();

                contactUs.Header.BackLink_Click();

                Framework.Mobile_Pages.Safety safety = home.Footer.SafetyLink_Click();

                contactUs.Header.BackLink_Click();

                Framework.Mobile_Pages.Privacy privacy = home.Footer.PrivacyLink_Click();

                contactUs.Header.BackLink_Click();

                Framework.Mobile_Pages.TermsOfService termsOfService = home.Footer.TermsOfServiceLink_Click();

                TearDown();

                log.Info("END TEST - JDate_Mobile/Regression/Member/FooterLinks");
            }
            catch (Exception e)
            {
                CleanupFailedTestcase(e);
                throw;
            }
        }


        [Test]
        public void Validate_EMail_RegisteredUserToAnyUser()
        {
            try
            {
                log.Info("START TEST - JDate_Mobile/Regression/Member/Registration");

                Setup();

                StartTest_UsingJDate_Mobile();

                string emailLogin = System.Configuration.ConfigurationManager.AppSettings["JDate_RegisteredUser1_Email"];
                string password = System.Configuration.ConfigurationManager.AppSettings["JDate_RegisteredUser1_Password"];

                Framework.Mobile_Pages.Login login = new Framework.Mobile_Pages.Login(driver);

                login.EmailTextbox_Write(emailLogin);
                login.PasswordTextbox_Write(password);

                Framework.Mobile_Pages.Home home = login.LoginButton_Click(emailLogin);

                // just verify that a registered user cannot even check their inbox - the Subscribe page appears instead
                Framework.Mobile_Pages.Subscribe search = home.InboxLink_Click_JDateNonSubscribedUser();

                home = search.Header.HomeLink_Click();

                home.Footer.LogoutLink_Click();

                TearDown();

                log.Info("END TEST - JDate_Mobile/Regression/Member/EMail_RegisteredUserToAnyUser");
            }
            catch (Exception e)
            {
                CleanupFailedTestcase(e);
                throw;
            }
        }




    }
}
