﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.Events;
using NUnit.Framework;
using log4net;
using WebdriverAutomation.Tests.ReusableCode;
using WebdriverAutomation.Framework.Site_Pages;

namespace WebdriverAutomation.Tests.JDate_co_il.Regression
{
    [TestFixture]
    public class Subscriber : BaseTest
    {
        /// <summary>
        /// For JDate.co.il:
        /// A registered user who views any other profile and clicks on the Email Me Now successfully reaches the Compose page.  But if the user presses the
        /// Send button they get taken to the Subscribe Page.
        [Test]
        public void Validate_EMail_RegisteredUserToAnyUser()
        {
            try
            {
                log.Info("START TEST - JDate_Co_Il/Regression/Member/EMail_RegisteredUserToAnyUser");

                Setup();

                StartTest_UsingJDate_Co_Il();

                string login = System.Configuration.ConfigurationManager.AppSettings["JDate_co_il_RegisteredUser3_Email"];
                string password = System.Configuration.ConfigurationManager.AppSettings["JDate_co_il_RegisteredUser3_Password"];

                WebdriverAutomation.Tests.ReusableCode.LoggingInAndOut loggingInAndOut = new LoggingInAndOut();
                WebdriverAutomation.Framework.Site_Pages.Home home = loggingInAndOut.LogIn_ProfileComplete(driver, log, login, password);

                // user 1 - search for user 2 (jdatesub1@spark.net, a subscribed user)
                string user2MemberNumber = System.Configuration.ConfigurationManager.AppSettings["JDate_co_il_SubscribedUser1_MemberID"];

                Framework.Site_Pages.LookUpMember lookUpMember = home.Header.LookUpMemberSubHeaderLink_Click();

                lookUpMember.MemberNumberTextbox = user2MemberNumber;
                Framework.Site_Pages.YourProfile yourProfile = lookUpMember.MemberNumberSearchButton_Click();

                // just verify that a non-subscibed user who tries to send an email gets taken to the Subscribe Page
                Framework.Site_Pages.Subscribe subscribe = yourProfile.UpgradeNowButton_Click();

                loggingInAndOut.LogOut(driver, log);

                TearDown();

                log.Info("END TEST - JDate_Co_Il/Regression/Member/EMail_RegisteredUserToAnyUser");
            }
            catch (Exception e)
            {
                CleanupFailedTestcase(e);
                throw;
            }
        }

        /// <summary>
        /// For JDate.co.il:
        /// A subscribed user is allowed to send an email to anybody
        /// When the registered user gets the email they CAN enter their inbox and CAN see that they have emails waiting to be read. But they cannot see the subject and
        /// can only see the sender's member id. If they click on the email they are taken to a prompt that leads to the Subscribe Page.
        /// </summary>
        [Test]
        public void Validate_EMail_SubscribedUserToRegisteredUser()
        {
            try
            {
                log.Info("START TEST - JDate_Co_Il/Regression/Member/EMail_SubscribedUserToRegisteredUser");

                Setup();

                StartTest_UsingJDate_Co_Il();

                string user1_login = System.Configuration.ConfigurationManager.AppSettings["JDate_co_il_SubscribedUser1_Email"];
                string user1_password = System.Configuration.ConfigurationManager.AppSettings["JDate_co_il_SubscribedUser1_Password"];
                string user1_Username = System.Configuration.ConfigurationManager.AppSettings["JDate_co_il_SubscribedUser1_Username"];
                string user1_MemberID = System.Configuration.ConfigurationManager.AppSettings["JDate_co_il_SubscribedUser1_MemberID"];

                string user2_login = System.Configuration.ConfigurationManager.AppSettings["JDate_co_il_RegisteredUser3_Email"];
                string user2_password = System.Configuration.ConfigurationManager.AppSettings["JDate_co_il_RegisteredUser3_Password"];
                string user2_MemberID = System.Configuration.ConfigurationManager.AppSettings["JDate_co_il_RegisteredUser3_MemberID"];

                // login as user 1 (a subscribed user)
                WebdriverAutomation.Tests.ReusableCode.LoggingInAndOut loggingInAndOut = new LoggingInAndOut();
                WebdriverAutomation.Framework.Site_Pages.Home home = loggingInAndOut.LogIn_ProfileComplete(driver, log, user1_login, user1_password);
                Framework.Site_Pages.QuickSearch quickSearch = home.Header.SearchHeaderLink_Click();

                // user 1 - search for user 2 (a registered user)
                Framework.Site_Pages.LookUpMember lookUpMember = quickSearch.Header.LookUpMemberSubHeaderLink_Click();

                lookUpMember.MemberNumberTextbox = user2_MemberID;
                Framework.Site_Pages.YourProfile yourProfile = lookUpMember.MemberNumberSearchButton_Click();

                // user 1 - message user 2          
                string subject = "sub to reg " + DateTime.Now.ToString();
                string message = "Validate EMail Subscribed User To Registered User test case Message timestamp: " + DateTime.Now.ToString();

                Framework.Site_Pages.ComposeMessage composeMessage = yourProfile.EmailMeNowButton_Click();
                composeMessage.SubjectTextbox = subject;
                composeMessage.MessageTextarea = message;
                composeMessage.SendButton_Click_AllAccessUnchecked();
                Framework.Site_Pages.MessageSent messageSent = composeMessage.NoThanksLink_Click();

                // user 1 - verify that the "Your Message has been sent" text appears
                Assert.IsTrue(messageSent.VerificationMessage().Contains("ההודעה נשלחה בהצלחה."), "In the Validate_EMail_SubscribedUserToRegisteredUser test case, after Subscribed user1 emails Registered user2 the 'Your Message has been sent' message did not appear");

                // user 1 - log out user 1 and login user 2 (a non subscribed user)
                Framework.Site_Pages.Login login = loggingInAndOut.LogOut(driver, log);
                home = loggingInAndOut.LogIn_ProfileComplete(driver, log, user2_login, user2_password);

                // user 2 - go into the inbox and verify that the message just sent exists and that the subject cannot be seen
                Framework.Site_Pages.Messages messages = home.Header.InboxHeaderLink_Click();

                Assert.IsTrue(messages.DoesEmailExist(user1_MemberID, subject), "In the EMail_SubscribedUserToRegisteredUser test case, after Subscribed user1 emails Registered user2 and user2 logs in, the email that was sent does not appear in user2's inbox.  \r\nWe are looking for an email with user1_Username: '" + user1_MemberID + "' and subject: '" + subject + "'");
                
                // user 2 - click on the message and verify that the message sent is the same
                Framework.Site_Pages.Message messagePage = messages.EmailLink_Click(user1_MemberID, subject);
                Assert.IsTrue(messagePage.DoesEmailContainCorrectMessage(message), "In the EMail_SubscribedUserToRegisteredUser test case, after Subscribed user1 emails Registered user2 and user2 logs in, the email's MESSAGE that was sent is not the same message that was originally sent from user1.  \r\nOriginal message: '" + message + "'\r\nMessage currently onscreen: '" + messagePage.GetEmailMessage() + "'");

                // user 2 - verify that the message that was just opened now has an opened envelope icon next to it
                messages = messagePage.BackToMessagesLink_Click();
                Assert.IsTrue(messages.IsEnvelopeIconOpened(user1_MemberID, subject), "In the EMail_SubscribedUserToRegisteredUser test case, after Subscribed user1 emails Registered user2 and user2 logs in and opens the message, the envelope icon next to the email that was just opened does not appear as a green opened envelope");

                // user 2 - reininitialize - delete all messages and logout
                messages.EmailSelectAllCheckboxes_Check();
                messages.DeleteLink_Click();

                messages.Header.Logout_Click();

                TearDown();

                log.Info("END TEST - JDate_Co_Il/Regression/Member/EMail_SubscribedUserToRegisteredUser");
            }
            catch (Exception e)
            {
                CleanupFailedTestcase(e);
                throw;
            }
        }

        /// <summary>
        /// For JDate.co.il:
        /// A subscribed user is allowed to send an email to anybody
        /// When the subscribed user gets the email they can read and send emails back without restrictions.
        /// </summary>
        [Test]
        public void Validate_EMail_SubscribedUserToSubscribedUser()
        {
            try
            {
                log.Info("START TEST - JDate_Co_Il/Regression/Member/EMail_SubscribedUserToSubscribedUser");

                Setup();

                StartTest_UsingJDate_Co_Il();

                string user1_login = System.Configuration.ConfigurationManager.AppSettings["JDate_co_il_SubscribedUser2_Email"];
                string user1_password = System.Configuration.ConfigurationManager.AppSettings["JDate_co_il_SubscribedUser2_Password"];
                string user1_Username = System.Configuration.ConfigurationManager.AppSettings["JDate_co_il_SubscribedUser2_Username"];
                string user1_MemberID = System.Configuration.ConfigurationManager.AppSettings["JDate_co_il_SubscribedUser2_MemberID"];

                string user2_login = System.Configuration.ConfigurationManager.AppSettings["JDate_co_il_SubscribedUser1_Email"];
                string user2_password = System.Configuration.ConfigurationManager.AppSettings["JDate_co_il_SubscribedUser1_Password"];
                string user2_Username = System.Configuration.ConfigurationManager.AppSettings["JDate_co_il_SubscribedUser1_Username"];
                string user2_MemberID = System.Configuration.ConfigurationManager.AppSettings["JDate_co_il_SubscribedUser1_MemberID"];

                // login as user 1 (a subscribed user)
                WebdriverAutomation.Tests.ReusableCode.LoggingInAndOut loggingInAndOut = new LoggingInAndOut();
                WebdriverAutomation.Framework.Site_Pages.Home home = loggingInAndOut.LogIn_ProfileComplete(driver, log, user1_login, user1_password);
                Framework.Site_Pages.QuickSearch quickSearch = home.Header.SearchHeaderLink_Click();

                // user 1 - search for user 2 (a registered user)
                Framework.Site_Pages.LookUpMember lookUpMember = quickSearch.Header.LookUpMemberSubHeaderLink_Click();

                lookUpMember.MemberNumberTextbox = user2_MemberID;
                Framework.Site_Pages.YourProfile yourProfile = lookUpMember.MemberNumberSearchButton_Click();

                // user 1 - message user 2          
                string subject = "sub to reg " + DateTime.Now.ToString();
                string message = "Validate EMail Subscribed User To Registered User test case Message timestamp: " + DateTime.Now.ToString();

                Framework.Site_Pages.ComposeMessage composeMessage = yourProfile.EmailMeNowButton_Click();
                composeMessage.SubjectTextbox = subject;
                composeMessage.MessageTextarea = message;
                composeMessage.SendButton_Click_AllAccessUnchecked();
                Framework.Site_Pages.MessageSent messageSent = composeMessage.NoThanksLink_Click();

                // user 1 - verify that the "Your AllAccess Message has been sent" text appears
                Assert.IsTrue(messageSent.VerificationMessage().Contains("ההודעה נשלחה בהצלחה."), "In the EMail_AllAccessUserToRegisteredUser test case, after All Access user1 emails Registered user2 the 'Your AllAccess Message has been sent' message did not appear");

                // user 1 - log out user 1 and login user 2 (a non subscribed user)
                Framework.Site_Pages.Login login = loggingInAndOut.LogOut(driver, log);
                home = loggingInAndOut.LogIn_ProfileComplete(driver, log, user2_login, user2_password);

                // user 2 - go into the inbox and verify that the message just sent exists and that the subject cannot be seen
                Framework.Site_Pages.Messages messages = home.Header.InboxHeaderLink_Click();
                Assert.IsTrue(messages.DoesEmailExist(user1_Username, user1_MemberID, subject), "In the EMail_SubscribedUserToRegisteredUser test case, after Subscribed user1 emails Registered user2 and user2 logs in, the email that was sent does not appear in user2's inbox.  \r\nWe are looking for an email with user1_Username: '" + user1_MemberID + "' and subject: '" + subject + "'");

                // user 2 - verify that the message just sent has an unopened envelope icon next to it
                Assert.IsFalse(messages.IsEnvelopeIconOpened(user1_Username, user1_MemberID, subject), "In the EMail_SubscribedUserToSubscribedUser test case, after Subscribed user1 emails Subscribed user2 and user2 logs in, the envelope icon next to the email that was just sent does not appear as a closed envelope");

                // user 2 - click on the message and verify that the message sent is the same
                Framework.Site_Pages.Message messagePage = messages.EmailLink_Click(user1_Username, user1_MemberID, subject);
                Assert.IsTrue(messagePage.DoesEmailContainCorrectMessage(message), "In the EMail_SubscribedUserToSubscribedUser test case, after Subscribed user1 emails Subscribed user2 and user2 logs in, the email's MESSAGE that was sent is not the same message that was originally sent from user1.  \r\nOriginal message: '" + message + "'\r\nMessage currently onscreen: '" + messagePage.GetEmailMessage() + "'");

                // user 2 - read the message and verify that the subject and message are correct
                string emailSubject = messagePage.GetEmailSubject();
                string emailMessage = messagePage.GetEmailMessage();
                Assert.IsTrue(subject == emailSubject, "In the EMail_SubscribedUserToSubscribedUser test case, after Subscribed user1 emails Registered user2 and user2 logs in and goes to their Inbox and checks the email, the Subject does not match the subject user1 originally sent.  \r\nOriginal subject: '" + subject + "' \r\nAll Access subject: '" + emailSubject + "'");
                Assert.IsTrue(message == emailMessage, "In the EMail_SubscribedUserToSubscribedUser test case, after Subscribed user1 emails Registered user2 and user2 logs in and goes to their Inbox and checks the email, the Message does not match the message user1 originally sent.  \r\nOriginal message: '" + message + "' \r\nAll Access message: '" + emailMessage + "'");

                // user 2 - reply back to user1
                string replyMessage = "Validate EMail All Access Use To Registered User test case Message timestamp: " + DateTime.Now.ToString();

                messagePage.MessageTextarea = replyMessage;
                messageSent = messagePage.SendButton_Click_ReplyingToEmail_JDatecoil();

                // user 2 - verify that the "Your Message has been sent" text appears
                Assert.IsTrue(messageSent.VerificationMessage().Contains("ההודעה נשלחה בהצלחה."), "In the EMail_SubscribedUserToSubscribedUser test case, after Subscribed user1 emails Registered user2 the 'Your Message has been sent' message did not appear");

                // user 2 - verify that the message that was just opened now has an opened envelope icon next to it
                messages = messageSent.BackToInboxLink_Click();
                Assert.IsTrue(messages.IsEnvelopeIconOpened(user1_Username, user1_MemberID, subject), "In the EMail_SubscribedUserToSubscribedUser test case, after Subscribed user1 emails Subscribed user2 and user2 logs in and opens the message, the envelope icon next to the email that was just opened does not appear as a green opened envelope");

                // user 2 - reininitialize - delete all emails and logout
                messages.EmailSelectAllCheckboxes_Check();
                messages.DeleteLink_Click();

                // log out user 2 and login user 1 again
                login = loggingInAndOut.LogOut(driver, log);
                home = loggingInAndOut.LogIn_ProfileComplete(driver, log, user1_login, user1_password);

                // user 1 - go to the Inbox and verify that the message appears
                messages = home.Header.InboxHeaderLink_Click();
                Assert.IsTrue(messages.DoesEmailExist(user2_Username, user2_MemberID, subject), "In the EMail_SubscribedUserToSubscribedUser test case, after Subscribed user1 emails Subscribed user2 and user2 replies back, the email that was replied does not appear in user1's inbox.  \r\nWe are looking for an email with user2's MemberID: '" + user2_MemberID + "' and subject: '" + subject + "'");

                // user 2 - verify that the message just sent has an unopened envelope icon next to it
                Assert.IsFalse(messages.IsEnvelopeIconOpened(user2_Username, user2_MemberID, subject), "In the EMail_SubscribedUserToSubscribedUser test case, after Subscribed user1 emails Subscribed user2 and user2 replies back, the envelope icon next to the email that was replied sent does not appear as a closed envelope");

                // user 2 - click on the message and verify that the subject and message are correct
                messagePage = messages.EmailLink_Click(user2_Username, user2_MemberID, subject);
                Assert.IsTrue(messagePage.DoesEmailContainCorrectMessage(replyMessage), "In the EMail_SubscribedUserToSubscribedUser test case, after Subscribed user1 emails Subscribed user2 and user2 replies back, the email's MESSAGE that was replied is not the same message that was originally sent from user2  \r\nReply message that we are expecting: '" + replyMessage + "' \r\nMessage currently onscreen: '" + messagePage.GetEmailMessage() + "'");

                // user 2 - reininitialize - delete all All Access emails and logout

                // jdate.co.il survey seems to randomly appear somewhere around here.  if things go wrong, get rid of the survey and try again
                for (int i = 0; i < 2; i++)
                {
                    try
                    {
                        messages = messagePage.BackToMessagesLink_Click();

                        break;
                    }
                    catch
                    {
                        GetRidOfJdateCoIlSurvey();
                    }
                }
                
                messages.EmailSelectAllCheckboxes_Check();
                messages.DeleteLink_Click();

                loggingInAndOut.LogOut(driver, log);

                TearDown();

                log.Info("END TEST - JDate_Co_Il/Regression/Member/EMail_SubscribedUserToSubscribedUser");
            }
            catch (Exception e)
            {
                CleanupFailedTestcase(e);
                throw;
            }
        }

        /// <summary>
        /// All Access user 1 emails Registered user 2
        /// user 2 verifes email and replies back to user 1
        /// user 1 verifies email reply
        /// 
        /// In order for this test case to work, both user 1 and user 2 must not have any previous All Access messages sent to each other or the subject/messages will
        /// not match and the test case will fail.
        /// </summary>
        [Test]
        public void Validate_EMail_AllAccessUserToRegisteredUser()
        {
            try
            {
                log.Info("START TEST - JDate_Co_Il/Regression/Member/EMail_AllAccessUserToRegisteredUser");

                Setup();

                StartTest_UsingJDate_Co_Il();

                #region Reinitialize User 2's state: get rid of any existing All Access emails whether they've been read or not
                string user2_login = System.Configuration.ConfigurationManager.AppSettings["JDate_co_il_RegisteredUser2_Email"];
                string user2_password = System.Configuration.ConfigurationManager.AppSettings["JDate_co_il_RegisteredUser2_Password"];
                string user2_MemberNumber = System.Configuration.ConfigurationManager.AppSettings["JDate_co_il_RegisteredUser2_MemberID"];

                WebdriverAutomation.Tests.ReusableCode.LoggingInAndOut loggingInAndOut = new LoggingInAndOut();
                Framework.Site_Pages.Home home = loggingInAndOut.LogIn_ProfileComplete(driver, log, user2_login, user2_password);
                string pageWeAreOnAfterClickingInboxLink = home.Header.InboxHeaderLink_Click_ForJDateNonSubscribedUsersWhoDoNotKnowIfThereAreAllAccessMessagesWaiting();

                Framework.Site_Pages.Messages messages;
                Framework.Site_Pages.Message messagePage;

                if (pageWeAreOnAfterClickingInboxLink == "Messages")   
                {
                    // All Access messages exist.  Delete all of them before starting the test 
                    messages = new Messages(driver);

                    // get rid of overlay if it exists
                    if (messages.DoesYouRecievedAnAllAccessEMailOverlayExist())
                    {
                        messagePage = messages.ReadNowButton_Click();
                        messages = messagePage.BackToMessagesLink_Click();

                        messages.AllAccessEmailSelectAllCheckboxes_Check();
                        messages.DeleteLink_Click();
                    }
                }
                else if (pageWeAreOnAfterClickingInboxLink == "another page")
                    Assert.Fail("After logging in as a registered user and clicking on the Inbox header link we did not land on a page that we expected to.");

                // log out user 2 and login user 1
                Framework.Site_Pages.Login login = loggingInAndOut.LogOut(driver, log);
                #endregion

                string user1_login = System.Configuration.ConfigurationManager.AppSettings["JDate_co_il_AllAccessUser1_Email"];
                string user1_password = System.Configuration.ConfigurationManager.AppSettings["JDate_co_il_AllAccessUser1_Password"];
                string user1_Username = System.Configuration.ConfigurationManager.AppSettings["JDate_co_il_AllAccessUser1_Username"];

                // login as user 1 (an all access user)            
                home = loggingInAndOut.LogIn_ProfileComplete(driver, log, user1_login, user1_password);

                #region Reinitialize User 2's state: get rid of any existing All Access emails whether they've been read or not
                messages = home.Header.InboxHeaderLink_Click();

                // get rid of overlay if it exists
                if (messages.DoesYouRecievedAnAllAccessEMailOverlayExist())
                {
                    messagePage = messages.ReadNowButton_Click();
                    messages = messagePage.BackToMessagesLink_Click();

                    messages.AllAccessEmailSelectAllCheckboxes_Check();
                    messages.DeleteLink_Click();
                }
                #endregion

                Framework.Site_Pages.QuickSearch quickSearch = messages.Header.SearchHeaderLink_Click();

                // user 1 - search for user 2 (a registered user)              
                Framework.Site_Pages.LookUpMember lookUpMember = quickSearch.Header.LookUpMemberSubHeaderLink_Click();

                lookUpMember.MemberNumberTextbox = user2_MemberNumber;
                Framework.Site_Pages.YourProfile yourProfile = lookUpMember.MemberNumberSearchButton_Click();

                // user 1 - message user 2          
                string subject = "allaccess to reg " + DateTime.Now.ToString();
                string message = "Validate EMail All Access Use To Registered User test case Message timestamp: " + DateTime.Now.ToString();

                Framework.Site_Pages.ComposeMessage composeMessage = yourProfile.EmailMeNowButton_Click();
                composeMessage.SubjectTextbox = subject;
                composeMessage.MessageTextarea = message;
                composeMessage.SendAsAllAccessEmailCheckbox_Check();
                Framework.Site_Pages.MessageSent messageSent = composeMessage.SendButton_Click_AllAccessChecked();

                // user 1 - verify that the "Your AllAccess Message has been sent" text appears
                Assert.IsTrue(messageSent.VerificationMessage().Contains("תודה, הודעת ה-Power User נשלחה בהצלחה"), "In the EMail_AllAccessUserToRegisteredUser test case, after All Access user1 emails Registered user2 the 'Your AllAccess Message has been sent' message did not appear");

                // log out user 1 and login user 2
                login = loggingInAndOut.LogOut(driver, log);

                home = loggingInAndOut.LogIn_ProfileComplete(driver, log, user2_login, user2_password);

                // user 2 - go to the Inbox and verify that the All Access message appears
                messages = home.Header.InboxHeaderLink_Click();
                Assert.IsTrue(messages.DoesYouRecievedAnAllAccessEMailOverlayExist(), "In the EMail_AllAccessUserToRegisteredUser test case, after All Access user1 emails Registered user2 and user2 logs in and goes to their Inbox, the 'You've recieved an All Access Email' message does not appear");

                // user 2 - read the message and verify that the subject and message are correct
                messagePage = messages.ReadNowButton_Click();
                string allAccessEmailSubject = messagePage.GetEmailSubject();
                string allAccessEmailMessage = messagePage.GetEmailMessage();
                Assert.IsTrue(subject == allAccessEmailSubject, "In the EMail_AllAccessUserToRegisteredUser test case, after All Access user1 emails Registered user2 and user2 logs in and goes to their Inbox and checks the email, the Subject does not match the subject user1 originally sent.  \r\nOriginal subject: '" + subject + "' \r\nAll Access subject: '" + allAccessEmailSubject + "'");
                Assert.IsTrue(message == allAccessEmailMessage, "In the EMail_AllAccessUserToRegisteredUser test case, after All Access user1 emails Registered user2 and user2 logs in and goes to their Inbox and checks the email, the Message does not match the message user1 originally sent.  \r\nOriginal message: '" + message + "' \r\nAll Access message: '" + allAccessEmailMessage + "'");

                // user 2 - reply back to user1
                string replyMessage = "Validate EMail All Access Use To Registered User test case Message timestamp: " + DateTime.Now.ToString();
                messagePage.MessageTextarea = replyMessage;
                messageSent = messagePage.SendButton_Click_ReplyingToEmail();

                // user 2 - verify that the "Your AllAccess Message has been sent" text appears
                Assert.IsTrue(messageSent.VerificationMessage().Contains("תודה, הודעת ה-Power User נשלחה בהצלחה"), "In the EMail_AllAccessUserToRegisteredUser test case, after All Access user1 emails Registered user2 the 'Your AllAccess Message has been sent' message did not appear");

                messages = messageSent.BackToInboxLink_Click();

                // user 2 - reininitialize - delete all All Access emails and logout
                messages.AllAccessEmailSelectAllCheckboxes_Check();
                messages.DeleteLink_Click();

                // log out user 2 and login user 1 again
                login = loggingInAndOut.LogOut(driver, log);
                home = loggingInAndOut.LogIn_ProfileComplete(driver, log, user1_login, user1_password);

                // user 1 - go to the Inbox and verify that the All Access message appears
                messages = home.Header.InboxHeaderLink_Click();
                Assert.IsTrue(messages.DoesYouRecievedAnAllAccessEMailOverlayExist(), "In the EMail_AllAccessUserToRegisteredUser test case, after All Access user1 emails Registered user2 and user2 logs in and goes to their Inbox, the 'You've recieved an All Access Email' message does not appear");

                // user 1 - read the message and verify that the subject and message are correct
                messagePage = messages.ReadNowButton_Click();
                Assert.IsTrue(messagePage.DoesEmailContainCorrectMessage(replyMessage), "In the EMail_AllAccessUserToRegisteredUser test case, after All Access user1 emails Registered user2 and user2 logs in and goes to their Inbox and checks the email, the Message does not match the message user1 originally sent  \r\nReply message that we are expecting: '" + replyMessage + "' \r\nMessage currently onscreen: '" + messagePage.GetEmailMessage() + "'");

                // user 1 - reininitialize - delete all All Access emails and logout
                messages = messagePage.BackToMessagesLink_Click();
                messages.AllAccessEmailSelectAllCheckboxes_Check();
                messages.DeleteLink_Click();

                loggingInAndOut.LogOut(driver, log);

                TearDown();

                log.Info("END TEST - JDate_Co_Il/Regression/Member/EMail_AllAccessUserToRegisteredUser");
            }
            catch (Exception e)
            {
                CleanupFailedTestcase(e);
                throw;
            }
        }
        
        [Test]
        public void Validate_YourAccountLinks()
        {
            try
            {
                log.Info("START TEST - JDate_Co_Il/Regression/Member/YourAccountLinks");

                Setup();

                StartTest_UsingJDate_Co_Il();

                string login = System.Configuration.ConfigurationManager.AppSettings["JDate_co_il_AllAccessUser1_Email"];
                string password = System.Configuration.ConfigurationManager.AppSettings["JDate_co_il_AllAccessUser1_Password"];

                string yourAccountPageURL = string.Empty;

                if (driver.Url.Contains("preprod"))
                    yourAccountPageURL = @"http://preprod.jdate.co.il/Applications/MemberServices/MemberServices.aspx?NavPoint=top";
                else
                    yourAccountPageURL = @"http://www.jdate.co.il/Applications/MemberServices/MemberServices.aspx?NavPoint=top";

                WebdriverAutomation.Tests.ReusableCode.LoggingInAndOut loggingInAndOut = new LoggingInAndOut();
                WebdriverAutomation.Framework.Site_Pages.Home home = loggingInAndOut.LogIn_ProfileComplete(driver, log, login, password);

                WebdriverAutomation.Framework.Site_Pages.YourAccount yourAccount = home.Header.YourAccountLink_Click();

                WebdriverAutomation.Framework.Site_Pages.ProfileDisplaySettings profileDisplaySettings = yourAccount.ProfileDisplaySettingsLink_Click();
                yourAccount = profileDisplaySettings.Header.YourAccountLink_Click();

                WebdriverAutomation.Framework.Site_Pages.YourProfile profile = yourAccount.ChangeYourProfileLink_Click();
                yourAccount = profileDisplaySettings.Header.YourAccountLink_Click();

                WebdriverAutomation.Framework.Site_Pages.ChangeYourEmailOrPassword changeYourEmailOrPassword = yourAccount.ChangeYourEmailOrPasswordLink_Click();
                yourAccount = profileDisplaySettings.Header.YourAccountLink_Click();

                //WebdriverAutomation.Framework.Site_Pages.ColorCodeSettings colorCodeSettings = yourAccount.ColorCodeSettingsLink_Click();
                //yourAccount = profileDisplaySettings.Header.YourAccountLink_Click();

                WebdriverAutomation.Framework.Site_Pages.Subscribe subscribe = yourAccount.MembershipPlansAndCostsLink_Click();
                driver.Navigate().GoToUrl(yourAccountPageURL);

                WebdriverAutomation.Framework.Site_Pages.AutoRenewalSettings autoRenewalSettings = yourAccount.AutoRenewalSettingsLink_Click();
                yourAccount = profileDisplaySettings.Header.YourAccountLink_Click();

                subscribe = yourAccount.SubscribeLink_Click();
                driver.Navigate().GoToUrl(yourAccountPageURL);

                //WebdriverAutomation.Framework.Site_Pages.BuyOneGiveOneFreeSettings buyOneGiveOneFreeSettings = yourAccount.BuyOneGiveOneFreeSettingsLink_Click();
                //driver.Navigate().GoToUrl(yourAccountPageURL);

                //WebdriverAutomation.Framework.Site_Pages.GetMoreAttentionByAddingPremiumServices premiumServices = yourAccount.PremiumServicesLink_Click();
                //driver.Navigate().GoToUrl(yourAccountPageURL);

                //// the Premium Service Settings link is commented out for now.  the Premium Service Settings Page cannot be reached without having a subscribed account,
                ////    but due to David's script which deletes accounts that have been subscribed we are currently not testing this
                //WebdriverAutomation.Framework.Site_Pages.PremiumServiceSettings premiumServiceSettings = yourAccount.PremiumServiceSettingsLink_Click();
                //yourAccount = profileDisplaySettings.Header.YourAccountLink_Click();

                WebdriverAutomation.Framework.Site_Pages.RemoveMyProfile removeMyProfile = yourAccount.RemoveMyProfileLink_Click();
                yourAccount = profileDisplaySettings.Header.YourAccountLink_Click();

                WebdriverAutomation.Framework.Site_Pages.AccountHistory accountHistory = yourAccount.AccountInformationLink_Click();
                yourAccount = profileDisplaySettings.Header.YourAccountLink_Click();

                //WebdriverAutomation.Framework.Site_Pages.BillingInformation billingInformation = yourAccount.BillingInformationLink_Click();
                //driver.Navigate().GoToUrl(yourAccountPageURL);

                WebdriverAutomation.Framework.Site_Pages.VerifyYourEmailAddress verifyEmail = yourAccount.VerifyEmailLink_Click();
                yourAccount = profileDisplaySettings.Header.YourAccountLink_Click();

                //WebdriverAutomation.Framework.Site_Pages.Settings chatAndIMSettings = yourAccount.ChatAndIMSettingsLink_Click();
                //driver.Navigate().GoToUrl(yourAccountPageURL);

                WebdriverAutomation.Framework.Site_Pages.MessageSettings messageSettings = yourAccount.MessageSettingsLink_Click();
                yourAccount = profileDisplaySettings.Header.YourAccountLink_Click();

                messageSettings = yourAccount.OffSiteMessageSettingsLink_Click();
                yourAccount = profileDisplaySettings.Header.YourAccountLink_Click();

                WebdriverAutomation.Framework.Site_Pages.WhosCheckingYouOut whosCheckingYouOut = yourAccount.MembersWhoEmailedYouLink_Click();
                yourAccount = profileDisplaySettings.Header.YourAccountLink_Click();

                WebdriverAutomation.Framework.Site_Pages.ContactingMembers contactingMembers = yourAccount.NeverMissEmailsLink_Click();
                yourAccount = profileDisplaySettings.Header.YourAccountLink_Click();

                whosCheckingYouOut = yourAccount.MembersBlockedFromContactingYouLink_Click();
                yourAccount = profileDisplaySettings.Header.YourAccountLink_Click();

                WebdriverAutomation.Framework.Site_Pages.YoureCheckingOut youreCheckingOut = yourAccount.MembersBlockedFromAppearingInYourSearchResultsLink_Click();
                yourAccount = profileDisplaySettings.Header.YourAccountLink_Click();

                whosCheckingYouOut = yourAccount.MembersBlockedFromViewingYourProfileInHisHerSearchResults_Click();
                yourAccount = profileDisplaySettings.Header.YourAccountLink_Click();

                WebdriverAutomation.Framework.Site_Pages.FrequentlyAskedQuestions faq = yourAccount.FAQLink_Click();
                yourAccount = profileDisplaySettings.Header.YourAccountLink_Click();

                WebdriverAutomation.Framework.Site_Pages.ContactUs contactUs = yourAccount.ContactUsLink_Click();
                yourAccount = profileDisplaySettings.Header.YourAccountLink_Click();

                WebdriverAutomation.Framework.Site_Pages.Privacy privacyStatement = yourAccount.PrivacyStatementLink_Click();
                yourAccount = profileDisplaySettings.Header.YourAccountLink_Click();

                privacyStatement = yourAccount.TermsAndConditionsOfServiceLink_Click();
                yourAccount = profileDisplaySettings.Header.YourAccountLink_Click();

                privacyStatement = yourAccount.TermsAndConditionsOfPurchaseLink_Click();
                yourAccount = profileDisplaySettings.Header.YourAccountLink_Click();

                TearDown();

                log.Info("END TEST - JDate_Co_Il/Regression/Member/YourAccountLinks");
            }
            catch (Exception e)
            {
                CleanupFailedTestcase(e);
                throw;
            }
        }
    }
}
