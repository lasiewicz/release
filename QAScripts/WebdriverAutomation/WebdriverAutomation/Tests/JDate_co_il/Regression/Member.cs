﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.Events;
using NUnit.Framework;
using log4net;
using WebdriverAutomation.Tests.ReusableCode;

namespace WebdriverAutomation.Tests.JDate_co_il.Regression
{
    [TestFixture]
    public class Member : BaseTest
    {

        [Test]
        public void Validate_Registration_Version1_OnePage()
        {
            try
            {
                log.Info("START TEST - JDate_Co_Il/Regression/Member/Registration_Version1_OnePage");

                Setup();

                StartTest_UsingJDate_Co_Il_WithDummyPRM();

                WebdriverAutomation.Framework.Site_Pages.Splash splash = new WebdriverAutomation.Framework.Site_Pages.Splash(driver);

                // JDate.co.il Registration - I am a, Date of Birth, Religious Background, Country, City, Email, Password, Captcha, I would like to recieve special updates...,
                //    Terms of Service appear on splash page
                splash.YouAreAList_Select(1);

                splash.BirthDateYearList_Select(20);
                splash.BirthDateMonthList_Select(2);
                splash.BirthDateDayList_Select(5);

                splash.YourReligiousBackground_Select(3);

                //splash.Country_Select(2);

                splash.CityTextbox_Write("ירושלים");
                //splash.CityListBox_Select("ירושלים");

                string emailAddress = "QAAutomation_" + DateTime.Now.ToString() + "@gmail.com";
                emailAddress = emailAddress.Replace("/", "_").Replace(" ", "_").Replace(":", "_");
                splash.EmailAddressTextbox_Write(emailAddress);

                splash.ChooseYourPasswordTextbox_Write("automation");

                //splash.PleaseEnterTheCodeShownBelowTextbox_Write("bananafish");

                splash.IWouldLikeSpecialOffersAndAnnouncements_Click();

                WebdriverAutomation.Framework.Site_Pages.RegistrationComplete registrationComplete = splash.ContinueButton_Click_RegistrationComplete();

                Framework.Site_Pages.RegistrationJDateCoIlOnePage registrationOnePage = registrationComplete.CompleteProfileLink_Click();

                // jdate.co.il survey seems to randomly appear somewhere around here.  if things go wrong, get rid of the survey and try again
                for (int i = 0; i < 2; i++)
                {
                    try
                    {
                        // The registration one page.  go through all listboxes from top right to bottom left

                        // listbox 1 - Marital Status
                        registrationOnePage.MaritalStatusListBox_JDate_co_il_MultiPage_Select(2);

                        // listbox 2 - I Keep Kosher
                        registrationOnePage.IKeepKosherListBox_JDate_co_il_MultiPage_Select(2);

                        // listbox 3 - Smoking Habits
                        registrationOnePage.SmokingHabitsListBox_JDate_co_il_MultiPage_Select(2);

                        // listbox 4 - Drinking Habits
                        registrationOnePage.DrinkingHabitsListBox_JDate_co_il_MultiPage_Select(2);

                        // listbox 5 - Number of Children
                        registrationOnePage.NumberOfChildrenListBox_JDate_co_il_MultiPage_Select(2);

                        // listbox 6 - Origin
                        registrationOnePage.OriginListBox_JDate_co_il_MultiPage_Select(2);

                        // listbox 7 - Level of Education
                        registrationOnePage.LevelOfEducationListBox_JDate_co_il_MultiPage_Select(2);

                        // listbox 8 - Height
                        registrationOnePage.HeightListBox_JDate_co_il_MultiPage_Select(2);

                        registrationOnePage.SaveDetailsButton_JDate_co_il_MultiPage_Click();

                        break;
                    }
                    catch
                    {
                        GetRidOfJdateCoIlSurvey();
                    }
                }

                //Remove the user we just created
                WebdriverAutomation.Tests.ReusableCode.AccountSettings accountSettings = new AccountSettings();
                accountSettings.RemoveProfile_SkippingUpdateProfileAndImportantNoticePages(driver, log);

                TearDown();

                log.Info("END TEST - JDate_Co_Il/Regression/Member/Registration_Version1_OnePage");
            }
            catch (Exception e)
            {
                CleanupFailedTestcase(e);
                throw;
            }
        }

        [Test]
        public void Validate_Registration_Version2_MultiPage()
        {
            try
            {
                log.Info("START TEST - JDate_Co_Il/Regression/Member/Registration_Version2_MultiPage");

                Setup();

                StartTest_UsingJDate_Co_Il_WithDummyPRM();

                WebdriverAutomation.Framework.Site_Pages.Splash splash = new WebdriverAutomation.Framework.Site_Pages.Splash(driver);
                WebdriverAutomation.Framework.Site_Pages.Login login = splash.MemberLoginLink_Click();
                WebdriverAutomation.Framework.Site_Pages.Registration registration = login.ClickHereToJoinFreeLink_Click();

                // page 1
                registration.IAmA_NewRegNotOnBedrock_Select(1);
                registration.ContinueButton_NewRegNotOnBedrock_ClickUntilProgressBarChanges();

                // page 2
                registration.WhatTypeOfRelationshipAreYouLookingForCheckbox_NewRegNotOnBedrock_Click(2);
                registration.WhatIsYourCurrentRelationshipStatus_NewRegNotOnBedrock_Select(2);
                registration.ContinueButton_NewRegNotOnBedrock_ClickUntilProgressBarChanges();

                // page 3
                registration.BirthDateMonthList_NewRegNotOnBedrock_Select(5);
                registration.BirthDateDayList_NewRegNotOnBedrock_Select(4);
                registration.BirthDateYearList_NewRegNotOnBedrock_Select(15);
                registration.ContinueButton_NewRegNotOnBedrock_ClickUntilProgressBarChanges();

                // page 4
                //registration.AreaDropdown_NewRegNotOnBedrock_Select(5);
                registration.HometownTextbox_NewRegNotOnBedrock_Write("ירושלים");
                registration.ClickOnBackground();                
                registration.ContinueButton_NewRegNotOnBedrock_ClickUntilProgressBarChanges();

                // page 5
                registration.YourReligiousBackground_NewRegNotOnBedrock_Select(2);
                registration.ContinueButton_NewRegNotOnBedrock_ClickUntilProgressBarChanges();

                // page 6
                registration.YourEducationLevel_NewRegNotOnBedrock_Select(2);
                registration.WhatDoYouDo_NewRegNotOnBedrock_Select(2);
                registration.ContinueButton_NewRegNotOnBedrock_ClickUntilProgressBarChanges();

                // page 7
                //registration.UserNameTextbox_NewRegNotOnBedrock_Write("QAAutomation");
                //registration.ContinueButton_NewRegNotOnBedrock_ClickUntilProgressBarChanges();

                // page 8
                string emailAddress = "QAAutomation_" + DateTime.Now.ToString() + "@gmail.com";
                emailAddress = emailAddress.Replace("/", "_").Replace(" ", "_").Replace(":", "_");
                registration.EmailAddressTextbox_NewRegNotOnBedrock_Write(emailAddress);

                registration.ChooseYourPasswordTextbox_NewRegNotOnBedrock_Write("Automation");

                registration.ContinueButton_NewRegNotOnBedrock_ClickUntilProgressBarChanges();

                // page 9
                string describeMyself = "food lover.  art lover.  dance lover.  music lover. ";
                registration.DescribeYourselfAndYourPersonalityTextArea_NewRegNotOnBedrock_Write(describeMyself);

                registration.ContinueButton_NewRegNotOnBedrock_ClickUntilProgressBarChanges();

                // page 10
                registration.ContinueButton_NewRegNotOnBedrock_Click();                

                // The registration complete page 
                WebdriverAutomation.Framework.Site_Pages.RegistrationComplete registrationComplete = new WebdriverAutomation.Framework.Site_Pages.RegistrationComplete(driver);

                Framework.Site_Pages.RegistrationJDateCoIlOnePage registrationOnePage = registrationComplete.CompleteProfileLink_Click();

                WebdriverAutomation.Tests.ReusableCode.AccountSettings accountSettings = null;

                // jdate.co.il survey seems to randomly appear somewhere around here.  if things go wrong, get rid of the survey and try again
                for (int i = 0; i < 2; i++)
                {
                    try
                    {
                        // The registration one page.  go through all listboxes from top right to bottom left

                        // listbox 2 - I Keep Kosher
                        registrationOnePage.IKeepKosherListBox_JDate_co_il_MultiPage_Select(2);

                        // listbox 3 - Smoking Habits
                        registrationOnePage.SmokingHabitsListBox_JDate_co_il_MultiPage_Select(2);

                        // listbox 4 - Drinking Habits
                        registrationOnePage.DrinkingHabitsListBox_JDate_co_il_MultiPage_Select(2);

                        // listbox 5 - Number of Children
                        registrationOnePage.NumberOfChildrenListBox_JDate_co_il_MultiPage_Select(2);

                        // listbox 6 - Origin
                        registrationOnePage.OriginListBox_JDate_co_il_MultiPage_Select(2);

                        // listbox 8 - Height
                        registrationOnePage.HeightListBox_JDate_co_il_MultiPage_Select(2);

                        registrationOnePage.SaveDetailsButton_JDate_co_il_MultiPage_Click();

                        //Remove the user we just created
                        accountSettings = new AccountSettings();
                        accountSettings.RemoveProfile_SkippingUpdateProfileAndImportantNoticePages(driver, log);

                        break;
                    }
                    catch
                    {
                        GetRidOfJdateCoIlSurvey();
                    }
                }

                TearDown();

                log.Info("END TEST - JDate_Co_Il/Regression/Member/Registration_Version2_MultiPage");
            }
            catch (Exception e)
            {
                CleanupFailedTestcase(e);
                throw;
            }
        }


        // NOTES 03/22/12 - right now there are 3 distinct reg buckets for the overlay
        //   one ofthem looks like version1 with a lot of differnent options up front
        //   second one is where marital status exists in step 2.  this one works okay right now
        //   third one is where marital status does not exist in step 2.  this fails.
        //   need to find way to differentiate between these buckets
        [Test]
        public void Validate_Registration_Version3_RegOverlay()
        {
            try
            {
                log.Info("START TEST - JDate_Co_Il/Regression/Member/Registration_Version3_RegOverlay");

                Setup();

                StartTest_UsingJDate_Co_Il_WithDummyPRM();

                WebdriverAutomation.Framework.Site_Pages.Splash splash = new WebdriverAutomation.Framework.Site_Pages.Splash(driver);

                const string jdateCoIlRegOverlayRegistrationURL = @"jdate.co.il/?Scenario=Overlay%20Registration%20Short&prm=47896&lgid=[test]";

                if (driver.Url.Contains("www.jdate.co.il"))
                    driver.Navigate().GoToUrl("http://www." + jdateCoIlRegOverlayRegistrationURL);
                else                
                    driver.Navigate().GoToUrl("http://preprod." + jdateCoIlRegOverlayRegistrationURL);                

                Framework.Site_Pages.RegistrationJDateCoIlRegOverlay regOverlay = new Framework.Site_Pages.RegistrationJDateCoIlRegOverlay(driver);

                //// sometimes a page appears, "Still alone? Still did not sign up for JDate?" appears which we just get rid of 
                //if (regOverlay.DoesRegOverLayOpeningPageExist())
                //    regOverlay.ContinueButton_Click();

                // 7/12/12 DOESN'T SEEM TO HIT ANYMORE - Path A:  overlay looks like Splash page overlay, with many different fields and a captcha
                if (driver.PageSource.Contains("Overlay Reg w. OPR form V1") || driver.PageSource.Contains("MOL Visitor Search Limit Page Reg Apr 12"))
                {
                    regOverlay.YouAreAList_Select(1);

                    regOverlay.BirthDateYearList_Select(20);
                    regOverlay.BirthDateMonthList_Select(2);
                    regOverlay.BirthDateDayList_Select(5);

                    regOverlay.YourReligiousBackground_Select(3);

                    //regOverlay.Country_Select(2);

                    regOverlay.CityTextbox_Write("ירושלים");
                    regOverlay.CityListBox_Select("ירושלים");

                    string emailAddress = "QAAutomation_" + DateTime.Now.ToString() + "@gmail.com";
                    emailAddress = emailAddress.Replace("/", "_").Replace(" ", "_").Replace(":", "_");
                    regOverlay.EmailAddressTextbox_Write(emailAddress);

                    regOverlay.ChooseYourPasswordTextbox_Write("automation");

                    regOverlay.PleaseEnterTheCodeShownBelow_Write("bananafish");

                    regOverlay.IWouldLikeSpecialOffersAndAnnouncements_Click();

                    regOverlay.StartButton_Click();
                    Thread.Sleep(1000);

                    if (regOverlay.DoesUnableToProcessRegistrationErrorAppear())
                        Assert.Fail("We cannot continue with Registration because the following message appeared, 'We were unable to process your registration because of an error. Please try again later'");

                    Framework.Site_Pages.RegistrationJDateCoIlOnePage registrationOnePage = new Framework.Site_Pages.RegistrationJDateCoIlOnePage(driver);

                    // on the Registration (Subscription was entered) page.  go through all listboxes from top right to bottom left
                    // listbox 1 - Marital Status
                    registrationOnePage.MaritalStatusListBox_JDate_co_il_1Page_Select(2);

                    // listbox 2 - I Keep Kosher
                    registrationOnePage.IKeepKosherListBox_JDate_co_il_1Page_Select(2);

                    // listbox 3 - Smoking Habits
                    registrationOnePage.SmokingHabitsListBox_JDate_co_il_1Page_Select(2);

                    // listbox 4 - Drinking Habits
                    registrationOnePage.DrinkingHabitsListBox_JDate_co_il_1Page_Select(2);

                    // listbox 5 - Number of Children
                    registrationOnePage.NumberOfChildrenListBox_JDate_co_il_1Page_Select(2);

                    // listbox 6 - Origin
                    registrationOnePage.OriginListBox_JDate_co_il_1Page_Select(2);

                    // listbox 7 - Level of Education
                    registrationOnePage.LevelOfEducationListBox_JDate_co_il_1Page_Select(2);

                    // listbox 8 - Height
                    registrationOnePage.HeightListBox_JDate_co_il_1Page_Select(2);

                    registrationOnePage.SaveDetailsButton_JDate_co_il_1Page_Click();

                    registrationOnePage.OnlineFriends_JDate_co_il_1Page_Click();
                }
                // 7/12/12 DOESN'T SEEM TO HIT ANYMORE - Page B: Starts off with Overlay for the I Am A, You Are A fields - short version of normal registration
                else if (driver.PageSource.Contains("Overlay Registration Short"))
                {
                    // page 1
                    regOverlay.IAmAListBox_Select(1);
                    regOverlay.YouAreAListBox_Select(1);
                    Thread.Sleep(1000);

                    // page 2
                    regOverlay.WhatIsYourReligiousBackgroundDropdown_Select(1);
                    Thread.Sleep(1000);

                    // page 3
                    regOverlay.BirthDateDayDropdown_Select(5);
                    regOverlay.BirthDateMonthDropdown_Select(4);
                    regOverlay.BirthDateYearDropdown_Select(15);
                    Thread.Sleep(1000);

                    // page 4
                    regOverlay.HometownTextbox_Write("ירושלים");
                    regOverlay.HometownListBox_Select("ירושלים");
                    regOverlay.ContinueButton_Click();
                    Thread.Sleep(1000);

                    // page 5
                    string emailAddress = "QAAutomation_" + DateTime.Now.ToString() + "@gmail.com";
                    emailAddress = emailAddress.Replace("/", "_").Replace(" ", "_").Replace(":", "_");
                    regOverlay.EmailTextbox_Write(emailAddress);

                    regOverlay.PasswordTextbox_Write("Automation");

                    regOverlay.ContinueButton_Click();
                    Thread.Sleep(1000);

                    // page 6
                    regOverlay.StartButton_Click();
                    Thread.Sleep(1000);

                    //// The registration complete page - doesn't appear anymore 03/20/12
                    //WebdriverAutomation.Framework.Site_Pages.RegistrationComplete registrationComplete = new WebdriverAutomation.Framework.Site_Pages.RegistrationComplete(driver);
                    //Framework.Site_Pages.RegistrationJDateCoIlOnePage registrationOnePage = registrationComplete.CompleteProfileLink_Click();
                    //Thread.Sleep(2000);

                    Framework.Site_Pages.RegistrationJDateCoIlOnePage registrationOnePage = new Framework.Site_Pages.RegistrationJDateCoIlOnePage(driver);

                    // The registration one page.  go through all listboxes from top right to bottom left
                    // listbox 1 - Marital Status
                    registrationOnePage.MaritalStatusListBox_JDate_co_il_1Page_Select(2);

                    // listbox 2 - I Keep Kosher
                    registrationOnePage.IKeepKosherListBox_JDate_co_il_1Page_Select(2);

                    // listbox 3 - Smoking Habits
                    registrationOnePage.SmokingHabitsListBox_JDate_co_il_1Page_Select(2);

                    // listbox 4 - Drinking Habits
                    registrationOnePage.DrinkingHabitsListBox_JDate_co_il_1Page_Select(2);

                    // listbox 5 - Number of Children
                    registrationOnePage.NumberOfChildrenListBox_JDate_co_il_1Page_Select(2);

                    // listbox 6 - Origin
                    registrationOnePage.OriginListBox_JDate_co_il_1Page_Select(2);

                    // listbox 7 - Level of Education
                    registrationOnePage.LevelOfEducationListBox_JDate_co_il_1Page_Select(2);

                    // listbox 8 - Height
                    registrationOnePage.HeightListBox_JDate_co_il_1Page_Select(2);

                    registrationOnePage.SaveDetailsButton_JDate_co_il_1Page_Click();

                    registrationOnePage.OnlineFriends_JDate_co_il_1Page_Click();
                }
                //Path C: Starts off with Overlay for the I Am A, You Are A fields
                else if (driver.PageSource.Contains("s.eVar48 = (clearValue) ? \"\" : \"Reg Overlay Short\";"))
                {
                    // page 1
                    regOverlay.IAmAListBox_Select(1);
                    regOverlay.YouAreAListBox_Select(1);
                    Thread.Sleep(2000);

                    //// page 2 
                    //regOverlay.WhatIsYourMaritalStatusListBox_Select(1);
                    //Thread.Sleep(2000);

                    // page 3
                    regOverlay.WhatIsYourReligiousBackgroundDropdown_Select(1);
                    Thread.Sleep(2000);

                    // page 4
                    regOverlay.BirthDateDayDropdown_Select(5);
                    regOverlay.BirthDateMonthDropdown_Select(4);
                    regOverlay.BirthDateYearDropdown_Select(15);
                    Thread.Sleep(2000);

                    // page 5
                    regOverlay.HometownTextbox_Write("ירושלים");
                    regOverlay.HometownListBox_Select("ירושלים");
                    regOverlay.ContinueButton_Click();
                    Thread.Sleep(1000);

                    //// page 6
                    //regOverlay.UserNameTextbox_Write("QAAutomation");
                    //regOverlay.ContinueButton_Click();
                    //Thread.Sleep(2000);

                    // page 7
                    string emailAddress = "QAAutomation_" + DateTime.Now.ToString() + "@gmail.com";
                    emailAddress = emailAddress.Replace("/", "_").Replace(" ", "_").Replace(":", "_");
                    regOverlay.EmailTextbox_Write(emailAddress);

                    regOverlay.PasswordTextbox_Write("Automation");

                    regOverlay.ContinueButton_Click();
                    Thread.Sleep(2000);

                    //// page 8
                    //string describeMyself = "food lover.  art lover.  dance lover.  music lover. ";
                    //regOverlay.DescribeYourselfAndYourPersonalityTextArea_Write(describeMyself);

                    //regOverlay.ContinueButton_Click();
                    //Thread.Sleep(2000);

                    //// page 9
                    //regOverlay.ContinueButton_Click();
                    //Thread.Sleep(2000);

                    // page 10
                    regOverlay.StartButton_Click();
                    Thread.Sleep(2000);

                    //// The registration complete page - doesn't appear anymore 03/20/12
                    //WebdriverAutomation.Framework.Site_Pages.RegistrationComplete registrationComplete = new WebdriverAutomation.Framework.Site_Pages.RegistrationComplete(driver);
                    //Framework.Site_Pages.RegistrationJDateCoIlOnePage registrationOnePage = registrationComplete.CompleteProfileLink_Click();
                    //Thread.Sleep(2000);

                    Framework.Site_Pages.RegistrationJDateCoIlOnePage registrationOnePage = new Framework.Site_Pages.RegistrationJDateCoIlOnePage(driver);

                    // The registration one page.  go through all listboxes from top right to bottom left
                    // listbox 1 - Marital Status
                    registrationOnePage.MaritalStatusListBox_JDate_co_il_MultiPage_Select(2);

                    // listbox 2 - I Keep Kosher
                    registrationOnePage.IKeepKosherListBox_JDate_co_il_MultiPage_Select(2);

                    // listbox 3 - Smoking Habits
                    registrationOnePage.SmokingHabitsListBox_JDate_co_il_MultiPage_Select(2);

                    // listbox 4 - Drinking Habits
                    registrationOnePage.DrinkingHabitsListBox_JDate_co_il_MultiPage_Select(2);

                    // listbox 5 - Number of Children
                    registrationOnePage.NumberOfChildrenListBox_JDate_co_il_MultiPage_Select(2);

                    // listbox 6 - Origin
                    registrationOnePage.OriginListBox_JDate_co_il_MultiPage_Select(2);

                    // listbox 7 - Level of Education
                    registrationOnePage.LevelOfEducationListBox_JDate_co_il_MultiPage_Select(2);

                    // listbox 8 - Height
                    registrationOnePage.HeightListBox_JDate_co_il_MultiPage_Select(2);

                    registrationOnePage.SaveDetailsButton_JDate_co_il_MultiPage_Click();

                    registrationOnePage.OnlineFriends_JDate_co_il_1Page_Click();
                }
                //Path D: Starts off with I Am field (added 03-28-13)
                else
                {
                    // page 1
                    regOverlay.IAmAListBox_Select(1);
                    regOverlay.ContinueButton_Click();                    

                    //// page 2 
                    //regOverlay.WhatIsYourMaritalStatusListBox_Select(1);
                    //Thread.Sleep(2000);                    

                    // page 3
                    regOverlay.BirthDateDayDropdown_Select(5);
                    regOverlay.BirthDateMonthDropdown_Select(4);
                    regOverlay.BirthDateYearDropdown_Select(15);
                    regOverlay.ContinueButton_Click(); 

                    // page 4
                    //regOverlay.RegionDropdown_Select(5);
                    regOverlay.HometownTextbox_Write("ירושלים");
                    regOverlay.HometownListBox_Select("ירושלים");

                    //regOverlay.ContinueButton_Click();
                    Thread.Sleep(1000);

                    // page 5
                    regOverlay.WhatIsYourReligiousBackgroundDropdown_Select(1);
                    regOverlay.ContinueButton_Click(); 

                    //// page 6
                    //regOverlay.UserNameTextbox_Write("QAAutomation");
                    //regOverlay.ContinueButton_Click();
                    //Thread.Sleep(2000);

                    // page 7
                    string emailAddress = "QAAutomation_" + DateTime.Now.ToString() + "@gmail.com";
                    emailAddress = emailAddress.Replace("/", "_").Replace(" ", "_").Replace(":", "_");
                    regOverlay.EmailTextbox_Write(emailAddress);

                    regOverlay.PasswordTextbox_Write("Automation");

                    regOverlay.ContinueButton_Click();
                    Thread.Sleep(2000);

                    //// page 8
                    //string describeMyself = "food lover.  art lover.  dance lover.  music lover. ";
                    //regOverlay.DescribeYourselfAndYourPersonalityTextArea_Write(describeMyself);

                    //regOverlay.ContinueButton_Click();
                    //Thread.Sleep(2000);

                    // page 9 - due to more branching versions of this registration path we do a quick check here for the Start link
                    if (!regOverlay.StartButton_Exists_ForJDateCoIl())
                    {
                        regOverlay.ContinueButton_Click();
                        Thread.Sleep(2000);
                    }

                    // page 10
                    regOverlay.StartButton_Click();
                    Thread.Sleep(2000);

                    WebdriverAutomation.Framework.Site_Pages.RegistrationComplete registrationComplete = new WebdriverAutomation.Framework.Site_Pages.RegistrationComplete(driver);

                    Framework.Site_Pages.RegistrationJDateCoIlOnePage registrationOnePage = registrationComplete.CompleteProfileLink_Click();

                    // jdate.co.il survey seems to randomly appear somewhere around here.  if things go wrong, get rid of the survey and try again
                    for (int i = 0; i < 2; i++)
                    {
                        try
                        {
                            // The registration one page.  go through all listboxes from top right to bottom left

                            // listbox 1 - Marital Status
                            registrationOnePage.MaritalStatusListBox_JDate_co_il_MultiPage_Select(2);

                            // listbox 2 - I Keep Kosher
                            registrationOnePage.IKeepKosherListBox_JDate_co_il_MultiPage_Select(2);

                            // listbox 3 - Smoking Habits
                            registrationOnePage.SmokingHabitsListBox_JDate_co_il_MultiPage_Select(2);

                            // listbox 4 - Drinking Habits
                            registrationOnePage.DrinkingHabitsListBox_JDate_co_il_MultiPage_Select(2);

                            // listbox 5 - Number of Children
                            registrationOnePage.NumberOfChildrenListBox_JDate_co_il_MultiPage_Select(2);

                            // listbox 6 - Origin
                            registrationOnePage.OriginListBox_JDate_co_il_MultiPage_Select(2);

                            // listbox 7 - Level of Education
                            registrationOnePage.LevelOfEducationListBox_JDate_co_il_MultiPage_Select(2);

                            // listbox 8 - Height
                            registrationOnePage.HeightListBox_JDate_co_il_MultiPage_Select(2);

                            registrationOnePage.SaveDetailsButton_JDate_co_il_MultiPage_Click();

                            break;
                        }
                        catch
                        {
                            GetRidOfJdateCoIlSurvey();
                        }
                    }
                }
                //else
                //{
                //    Assert.Fail("Currently we do not recognize which registration page will be reached if we click the Browse For Free button on the Splash page. Stopping the test.");
                //}

                //Remove the user we just created
                WebdriverAutomation.Tests.ReusableCode.AccountSettings accountSettings = new AccountSettings();
                accountSettings.RemoveProfile_SkippingUpdateProfileAndImportantNoticePages(driver, log);

                TearDown();

                log.Info("END TEST - JDate_Co_Il/Regression/Member/Registration_Version3_RegOverlay");
            }
            catch (Exception e)
            {
                CleanupFailedTestcase(e);
                throw;
            }
        }

        [Test]
        public void Validate_Registration_Version4_Facebook_iFrame()
        {
            try
            {
                log.Info("START TEST - JDate_Co_Il/Regression/Member/Registration_Version4_Facebook_iFrame");

                Setup();

                StartTest_UsingJDate_Co_Il_WithDummyPRM();

                WebdriverAutomation.Framework.Site_Pages.Splash splash = new WebdriverAutomation.Framework.Site_Pages.Splash(driver);

                const string jdateCoIlRegOverlayRegistrationURL = @"jdate.co.il/?Scenario=FB%20REG%20IFRAME&PRM=47896&lgid=[test]";

                if (driver.Url.Contains("www.jdate.co.il"))
                    driver.Navigate().GoToUrl("http://www." + jdateCoIlRegOverlayRegistrationURL);
                else
                    driver.Navigate().GoToUrl("http://preprod." + jdateCoIlRegOverlayRegistrationURL);

                Framework.Site_Pages.RegistrationJDateCoIlFacebookiFrame facebookIFrame = new Framework.Site_Pages.RegistrationJDateCoIlFacebookiFrame(driver);

                facebookIFrame.IAmADropdown_Select(1);

                facebookIFrame.BirthDateYearDropdown_Select(20);
                facebookIFrame.BirthDateMonthDropdown_Select(2);
                facebookIFrame.BirthDateDayDropdown_Select(5);

                facebookIFrame.YourReligiousBackgroundDropdown_Select(2);

                facebookIFrame.CityTextbox_Write("ירושלים");
                facebookIFrame.CityListBox_SelectFirstValue();

                string emailAddress = "QAAutomation_" + DateTime.Now.ToString() + "@gmail.com";
                emailAddress = emailAddress.Replace("/", "_").Replace(" ", "_").Replace(":", "_");
                facebookIFrame.EmailAddressTextbox_Write(emailAddress);

                facebookIFrame.ChooseYourPasswordTextbox_Write("automation");

                facebookIFrame.IWouldLikeSpecialOffersAndAnnouncements_Click();

                Framework.Site_Pages.RegistrationJDateCoIlFacebookiFrameRegComplete facebookIFrameRegComplete = facebookIFrame.StartButton_Click();
                Thread.Sleep(1000);

                facebookIFrameRegComplete.ViewProfileButton_Click();

                Framework.Site_Pages.RegistrationJDateCoIlOnePage registrationOnePage = new Framework.Site_Pages.RegistrationJDateCoIlOnePage(driver);

                // on the Registration (Subscription was entered) page.  go through all listboxes from top right to bottom left
                // listbox 1 - Marital Status
                registrationOnePage.MaritalStatusListBox_JDate_co_il_MultiPage_Select(2);

                // listbox 2 - I Keep Kosher
                registrationOnePage.IKeepKosherListBox_JDate_co_il_MultiPage_Select(2);

                // listbox 3 - Smoking Habits
                registrationOnePage.SmokingHabitsListBox_JDate_co_il_MultiPage_Select(2);

                // listbox 4 - Drinking Habits
                registrationOnePage.DrinkingHabitsListBox_JDate_co_il_MultiPage_Select(2);

                // listbox 5 - Number of Children
                registrationOnePage.NumberOfChildrenListBox_JDate_co_il_MultiPage_Select(2);

                // listbox 6 - Origin
                registrationOnePage.OriginListBox_JDate_co_il_MultiPage_Select(2);

                // listbox 7 - Level of Education
                registrationOnePage.LevelOfEducationListBox_JDate_co_il_MultiPage_Select(2);

                // listbox 8 - Height
                registrationOnePage.HeightListBox_JDate_co_il_MultiPage_Select(2);

                registrationOnePage.SaveDetailsButton_JDate_co_il_MultiPage_Click();
               
                //Remove the user we just created
                WebdriverAutomation.Tests.ReusableCode.AccountSettings accountSettings = new AccountSettings();
                accountSettings.RemoveProfile_SkippingUpdateProfileAndImportantNoticePages(driver, log);

                TearDown();

                log.Info("END TEST - JDate_Co_Il/Regression/Member/Registration_Version4_Facebook_iFrame");
            }
            catch (Exception e)
            {
                CleanupFailedTestcase(e);
                throw;
            }
        }


        [Test]
        public void Validate_Registration_Version5_JMag_iFrame()
        {
            try
            {
                log.Info("START TEST - JDate_Co_Il/Regression/Member/Registration_Version5_JMag_iFrame");

                Setup();

                StartTest_UsingJDate_Co_Il_WithDummyPRM();

                WebdriverAutomation.Framework.Site_Pages.Splash splash = new WebdriverAutomation.Framework.Site_Pages.Splash(driver);

                const string jdateCoIlRegOverlayRegistrationURL = @"jdate.co.il/?Scenario=JMAG%20IL%20IFRAME&PRM=47896&lgid=";

                if (driver.Url.Contains("www.jdate.co.il"))
                    driver.Navigate().GoToUrl("http://www." + jdateCoIlRegOverlayRegistrationURL);
                else
                    driver.Navigate().GoToUrl("http://preprod." + jdateCoIlRegOverlayRegistrationURL);

                Framework.Site_Pages.RegistrationJDateCoIlJMagiFrame jmagIFrame = new Framework.Site_Pages.RegistrationJDateCoIlJMagiFrame(driver);

                jmagIFrame.IAmADropdown_Select(1);

                jmagIFrame.BirthDateYearDropdown_Select(20);
                jmagIFrame.BirthDateMonthDropdown_Select(2);
                jmagIFrame.BirthDateDayDropdown_Select(5);

                jmagIFrame.YourReligiousBackgroundDropdown_Select(2);

                jmagIFrame.CityTextbox_Write("ירושלים");
                jmagIFrame.CityListBox_SelectFirstValue();

                string emailAddress = "QAAutomation_" + DateTime.Now.ToString() + "@gmail.com";
                emailAddress = emailAddress.Replace("/", "_").Replace(" ", "_").Replace(":", "_");
                jmagIFrame.EmailAddressTextbox_Write(emailAddress);

                jmagIFrame.ChooseYourPasswordTextbox_Write("automation");

                jmagIFrame.IWouldLikeSpecialOffersAndAnnouncements_Click();

                Framework.Site_Pages.RegistrationJDateCoIlJMagiFrameRegComplete facebookIFrameRegComplete = jmagIFrame.StartButton_Click();
                Thread.Sleep(1000);

                facebookIFrameRegComplete.YourMatchesButton_Click();

                Framework.Site_Pages.RegistrationJDateCoIlOnePage registrationOnePage = new Framework.Site_Pages.RegistrationJDateCoIlOnePage(driver);

                // on the Registration (Subscription was entered) page.  go through all listboxes from top right to bottom left
                // listbox 1 - Marital Status
                registrationOnePage.MaritalStatusListBox_JDate_co_il_MultiPage_Select(2);

                // listbox 2 - I Keep Kosher
                registrationOnePage.IKeepKosherListBox_JDate_co_il_MultiPage_Select(2);

                // listbox 3 - Smoking Habits
                registrationOnePage.SmokingHabitsListBox_JDate_co_il_MultiPage_Select(2);

                // listbox 4 - Drinking Habits
                registrationOnePage.DrinkingHabitsListBox_JDate_co_il_MultiPage_Select(2);

                // listbox 5 - Number of Children
                registrationOnePage.NumberOfChildrenListBox_JDate_co_il_MultiPage_Select(2);

                // listbox 6 - Origin
                registrationOnePage.OriginListBox_JDate_co_il_MultiPage_Select(2);

                // listbox 7 - Level of Education
                registrationOnePage.LevelOfEducationListBox_JDate_co_il_MultiPage_Select(2);

                // listbox 8 - Height
                registrationOnePage.HeightListBox_JDate_co_il_MultiPage_Select(2);

                registrationOnePage.SaveDetailsButton_JDate_co_il_MultiPage_Click();

                //Remove the user we just created
                WebdriverAutomation.Tests.ReusableCode.AccountSettings accountSettings = new AccountSettings();
                accountSettings.RemoveProfile_SkippingUpdateProfileAndImportantNoticePages(driver, log);

                TearDown();

                log.Info("END TEST - JDate_Co_Il/Regression/Member/Registration_Version5_JMag_iFrame");
            }
            catch (Exception e)
            {
                CleanupFailedTestcase(e);
                throw;
            }
        }


        [Test]
        public void Validate_JMeterQuestionaire()
        {
            try
            {
                log.Info("START TEST - JDate_Co_Il/Regression/Member/JMeterQuestions");

                Setup();

                StartTest_UsingJDate_Co_Il_WithDummyPRM();

                #region Register new user
                WebdriverAutomation.Framework.Site_Pages.Splash splash = new WebdriverAutomation.Framework.Site_Pages.Splash(driver);

                // JDate.co.il Registration - I am a, Date of Birth, Religious Background, Country, City, Email, Password, Captcha, I would like to recieve special updates...,
                //    Terms of Service appear on splash page
                splash.YouAreAList_Select(1);

                splash.BirthDateYearList_Select(20);
                splash.BirthDateMonthList_Select(2);
                splash.BirthDateDayList_Select(5);

                splash.YourReligiousBackground_Select(3);

                //splash.Country_Select(2);

                splash.CityTextbox_Write("ירושלים");
                //splash.CityListBox_Select("ירושלים");

                string emailAddress = "QAAutomation_" + DateTime.Now.ToString() + "@gmail.com";
                emailAddress = emailAddress.Replace("/", "_").Replace(" ", "_").Replace(":", "_");
                splash.EmailAddressTextbox_Write(emailAddress);

                splash.ChooseYourPasswordTextbox_Write("automation");

                //splash.PleaseEnterTheCodeShownBelowTextbox_Write("bananafish");

                splash.IWouldLikeSpecialOffersAndAnnouncements_Click();

                WebdriverAutomation.Framework.Site_Pages.RegistrationComplete registrationComplete = splash.ContinueButton_Click_RegistrationComplete();

                Framework.Site_Pages.RegistrationJDateCoIlOnePage registrationOnePage = registrationComplete.CompleteProfileLink_Click();

                // jdate.co.il survey seems to randomly appear somewhere around here.  if things go wrong, get rid of the survey and try again
                for (int i = 0; i < 2; i++)
                {
                    try
                    {
                        // The registration one page.  go through all listboxes from top right to bottom left

                        // listbox 1 - Marital Status
                        registrationOnePage.MaritalStatusListBox_JDate_co_il_MultiPage_Select(2);

                        // listbox 2 - I Keep Kosher
                        registrationOnePage.IKeepKosherListBox_JDate_co_il_MultiPage_Select(2);

                        // listbox 3 - Smoking Habits
                        registrationOnePage.SmokingHabitsListBox_JDate_co_il_MultiPage_Select(2);

                        // listbox 4 - Drinking Habits
                        registrationOnePage.DrinkingHabitsListBox_JDate_co_il_MultiPage_Select(2);

                        // listbox 5 - Number of Children
                        registrationOnePage.NumberOfChildrenListBox_JDate_co_il_MultiPage_Select(2);

                        // listbox 6 - Origin
                        registrationOnePage.OriginListBox_JDate_co_il_MultiPage_Select(2);

                        // listbox 7 - Level of Education
                        registrationOnePage.LevelOfEducationListBox_JDate_co_il_MultiPage_Select(2);

                        // listbox 8 - Height
                        registrationOnePage.HeightListBox_JDate_co_il_MultiPage_Select(2);

                        registrationOnePage.SaveDetailsButton_JDate_co_il_MultiPage_Click();

                        break;
                    }
                    catch
                    {
                        GetRidOfJdateCoIlSurvey();
                    }
                }
                #endregion

                #region JMeter Questionaire
                WebdriverAutomation.Framework.Site_Pages.Home home = new Framework.Site_Pages.Home(driver);
                Framework.Site_Pages.JMeterWelcome jMeterWelcome = home.Header.JMeterLink_NotSubscribedToJMeter_Click();

                Framework.Site_Pages.JMeterPersonalityTest jMeterPersonalityTest = jMeterWelcome.Start_Click();

                int questionsAnswered = 0;

                // the questionaire progress bar ends after we pass 98% which is weird
                while (jMeterPersonalityTest.DoesQuestionaireStillAppear())
                {
                    try
                    {
                        jMeterPersonalityTest.SevenButton_Click();
                        questionsAnswered++;
                    }
                    catch
                    {
                        if (jMeterPersonalityTest.DoesQuestionaireStillAppear())
                            Assert.Fail("Even though we currently should still be on the Personality Test Page when we attempt to answer a question, we are unable to click one of the buttons for the test to continue.");
                    }

                    if (questionsAnswered == 250)
                        Assert.Fail("While attempting to complete the JMeter questionaire we clicked on over 200 answers without the progress bar reaching 100% which should not happen.");
                }

                Framework.Site_Pages.JMeterTestComplete jmeterTestComplete = new Framework.Site_Pages.JMeterTestComplete(driver);
                #endregion

                #region Self suspend user
                //Remove the user we just created
                //home = jmeterTestComplete.Header.HomeHeaderLink_Click();
                WebdriverAutomation.Tests.ReusableCode.AccountSettings accountSettings = new AccountSettings();
                accountSettings.RemoveProfile(driver, log);
                #endregion

                TearDown();

                log.Info("END TEST - JDate_Co_Il/Regression/Member/JMeterQuestions");                
            }
            catch (Exception e)
            {
                CleanupFailedTestcase(e);
                throw;
            }
        }


        [Test]
        public void Validate_JMeterSearchReturnsResults()
        {
            try
            {
                log.Info("START TEST - JDate_Co_Il/Regression/Member/JMeterSearchReturnsResults");

                Setup();

                StartTest_UsingJDate_Co_Il();

                string login = System.Configuration.ConfigurationManager.AppSettings["JDate_co_il_SubscribedUser1_Email"];
                string password = System.Configuration.ConfigurationManager.AppSettings["JDate_co_il_SubscribedUser1_Password"];

                WebdriverAutomation.Tests.ReusableCode.LoggingInAndOut loggingInAndOut = new LoggingInAndOut();
                WebdriverAutomation.Framework.Site_Pages.Home home = loggingInAndOut.LogIn_ProfileComplete(driver, log, login, password);
                Framework.Site_Pages.JMeterAdjustments jmeterAdjustments = home.Header.JMeterLink_SubscribedToJMeter_Click();

                Assert.IsTrue(jmeterAdjustments.DoResultsAppear(), "In the JMeter Results/adjustments Page we expect to see users matching our JMeter results but we do not.");
                
                TearDown();

                log.Info("END TEST - JDate_Co_Il/Regression/Member/JMeterSearchReturnsResults");
            }
            catch (Exception e)
            {
                CleanupFailedTestcase(e);
                throw;
            }
        }


        [Test]
        public void Validate_JMeter1On1PageExistsForJMeterUsers()
        {
            try
            {
                log.Info("START TEST - JDate_Co_Il/Regression/Member/JMeter1On1PageExistsForJMeterUsers");

                Setup();

                StartTest_UsingJDate_Co_Il();

                string user1_login = System.Configuration.ConfigurationManager.AppSettings["JDate_co_il_SubscribedUser1_Email"];
                string user1_password = System.Configuration.ConfigurationManager.AppSettings["JDate_co_il_SubscribedUser1_Password"];
                string user1_Username = System.Configuration.ConfigurationManager.AppSettings["JDate_co_il_SubscribedUser1_Username"];
                string user1_MemberID = System.Configuration.ConfigurationManager.AppSettings["JDate_co_il_SubscribedUser1_MemberID"];

                string user2_login = System.Configuration.ConfigurationManager.AppSettings["JDate_co_il_RegisteredUser3_Email"];
                string user2_password = System.Configuration.ConfigurationManager.AppSettings["JDate_co_il_RegisteredUser3_Password"];
                string user2_MemberID = System.Configuration.ConfigurationManager.AppSettings["JDate_co_il_RegisteredUser3_MemberID"];

                WebdriverAutomation.Tests.ReusableCode.LoggingInAndOut loggingInAndOut = new LoggingInAndOut();
                WebdriverAutomation.Framework.Site_Pages.Home home = loggingInAndOut.LogIn_ProfileComplete(driver, log, user1_login, user1_password);

                // user 1 - search for user 2 (a non-JMeter user)
                Framework.Site_Pages.LookUpMember lookUpMember = home.Header.LookUpMemberSubHeaderLink_Click();

                lookUpMember.MemberNumberTextbox = user2_MemberID;
                Framework.Site_Pages.YourProfile yourProfile = lookUpMember.MemberNumberSearchButton_Click();

                Framework.Site_Pages.JMeterOneOnONe jMeterOneOnOne = yourProfile.JMeterIcon_Click();

                TearDown();

                log.Info("END TEST - JDate_Co_Il/Regression/Member/JMeter1On1PageExistsForJMeterUsers");
            }
            catch (Exception e)
            {
                CleanupFailedTestcase(e);
                throw;
            }
        }


        [Test]
        public void Validate_EditProfile_InMyOwnWordsTab()
        {
            try
            {
                log.Info("START TEST - JDate_Co_Il/Regression/Member/EditProfile_InMyOwnWordsTab");

                Setup();

                StartTest_UsingJDate_Co_Il();

                string login = System.Configuration.ConfigurationManager.AppSettings["JDate_co_il_RegisteredUser3_Email"];
                string password = System.Configuration.ConfigurationManager.AppSettings["JDate_co_il_RegisteredUser3_Password"];

                WebdriverAutomation.Tests.ReusableCode.LoggingInAndOut loggingInAndOut = new LoggingInAndOut();
                WebdriverAutomation.Framework.Site_Pages.Home home = loggingInAndOut.LogIn_ProfileComplete(driver, log, login, password);
                WebdriverAutomation.Framework.Site_Pages.YourProfile profile = home.Header.YourProfileHeaderLink_Click();

                string new_AboutMe = "This is a test account operated by an employee of Spark Networks. Testing changes.";
                string new_MyLifeAndAmbitions = "I always want the big piece of chicken";
                string new_ABriefHistoryOfMyLife = "Born in NYC. Raised in SF. Living in LA.";
                string new_OnOurFirstDateRemindMeToTellYouTheStoryAbout = "The time we parachuted into Burning Man";
                string new_TheThingsICouldNeverLiveWithout = "My phone and lasagna";
                string new_MyFavoriteBooksMoviesTVShowsMusicAndFood = "Some of that oontz oontz";
                string new_TheCoolestPlacesIveVisited = "Thom Yorke's house";
                string new_ForFunILikeTo = "Sleep all day";
                string new_OnFridayAndSaturdayNightsITypically = "Am watching a show";
                string new_ImLookingFor = "I am looking for someone who likes dance, music, the arts, travel and having a good time. Spontanaeity helps.";
                string new_YouShouldDefinitelyMessageMeIfYou = "Are not crazy";

                //// jdate.co.il survey seems to randomly appear somewhere around here.  if things go wrong, get rid of the survey and try again
                //for (int i = 0; i < 2; i++)
                //{
                //    try
                //    {
                // Overwrite new values to the page
                profile.AboutMe = new_AboutMe;
                profile.MyLifeAndAmbitions = new_MyLifeAndAmbitions;
                profile.ABriefHistoryOfMyLife = new_ABriefHistoryOfMyLife;
                profile.OnOurFirstDateRemindMeToTellYouTheStoryAbout = new_OnOurFirstDateRemindMeToTellYouTheStoryAbout;
                profile.TheThingsICouldNeverLiveWithout = new_TheThingsICouldNeverLiveWithout;
                profile.MyFavoriteBooksMoviesTVShowsMusicAndFood = new_MyFavoriteBooksMoviesTVShowsMusicAndFood;
                profile.TheCoolestPlacesIveVisited = new_TheCoolestPlacesIveVisited;
                profile.ForFunILikeTo = new_ForFunILikeTo;
                profile.OnFridayAndSaturdayNightsITypically = new_OnFridayAndSaturdayNightsITypically;
                profile.ImLookingFor = new_ImLookingFor;
                profile.YouShouldDefinitelyMessageMeIfYou = new_YouShouldDefinitelyMessageMeIfYou;

                //        break;
                //    }
                //    catch
                //    {
                //        GetRidOfJdateCoIlSurvey();
                //    }
                //}

                // Go to the Home Page and then come back to the Profile Page to verify the values saved correctly
                home = profile.Header.HomeHeaderLink_Click();
                profile = home.Header.YourProfileHeaderLink_Click();

                string stringToVerifyOnProfilePage = string.Empty;

                stringToVerifyOnProfilePage = profile.AboutMe;
                Assert.IsTrue(new_AboutMe == stringToVerifyOnProfilePage, "In the Validate_EditProfile_InMyOwnWordsTab test case, after saving new values for 'About me' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_AboutMe + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.MyLifeAndAmbitions;
                Assert.IsTrue(new_MyLifeAndAmbitions == stringToVerifyOnProfilePage, "In the Validate_EditProfile_InMyOwnWordsTab test case, after saving new values for 'My life and ambitions' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_MyLifeAndAmbitions + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.ABriefHistoryOfMyLife;
                Assert.IsTrue(new_ABriefHistoryOfMyLife == stringToVerifyOnProfilePage, "In the Validate_EditProfile_InMyOwnWordsTab test case, after saving new values for 'A brief history of my life' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_ABriefHistoryOfMyLife + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.OnOurFirstDateRemindMeToTellYouTheStoryAbout;
                Assert.IsTrue(new_OnOurFirstDateRemindMeToTellYouTheStoryAbout == stringToVerifyOnProfilePage, "In the Validate_EditProfile_InMyOwnWordsTab test case, after saving new values for 'On our first date, remind me to tell you the story about...' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_OnOurFirstDateRemindMeToTellYouTheStoryAbout + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.TheThingsICouldNeverLiveWithout;
                Assert.IsTrue(new_TheThingsICouldNeverLiveWithout == stringToVerifyOnProfilePage, "In the Validate_EditProfile_InMyOwnWordsTab test case, after saving new values for 'The things I could never live without' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_TheThingsICouldNeverLiveWithout + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.MyFavoriteBooksMoviesTVShowsMusicAndFood;
                Assert.IsTrue(new_MyFavoriteBooksMoviesTVShowsMusicAndFood == stringToVerifyOnProfilePage, "In the Validate_EditProfile_InMyOwnWordsTab test case, after saving new values for 'My favorite books, movies, TV shows, music and food' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_MyFavoriteBooksMoviesTVShowsMusicAndFood + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.TheCoolestPlacesIveVisited;
                Assert.IsTrue(new_TheCoolestPlacesIveVisited == stringToVerifyOnProfilePage, "In the Validate_EditProfile_InMyOwnWordsTab test case, after saving new values for 'The coolest places I've visited' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_TheCoolestPlacesIveVisited + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.ForFunILikeTo;
                Assert.IsTrue(new_ForFunILikeTo == stringToVerifyOnProfilePage, "In the Validate_EditProfile_InMyOwnWordsTab test case, after saving new values for 'For fun, I like to...' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_ForFunILikeTo + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.OnFridayAndSaturdayNightsITypically;
                Assert.IsTrue(new_OnFridayAndSaturdayNightsITypically == stringToVerifyOnProfilePage, "In the Validate_EditProfile_InMyOwnWordsTab test case, after saving new values for 'On Friday and Saturday nights I typically...' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_OnFridayAndSaturdayNightsITypically + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.ImLookingFor;
                Assert.IsTrue(new_ImLookingFor == stringToVerifyOnProfilePage, "In the Validate_EditProfile_InMyOwnWordsTab test case, after saving new values for 'I'm looking for...' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_ImLookingFor + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.YouShouldDefinitelyMessageMeIfYou;
                Assert.IsTrue(new_YouShouldDefinitelyMessageMeIfYou == stringToVerifyOnProfilePage, "In the Validate_EditProfile_InMyOwnWordsTab test case, after saving new values for 'You should definitely message me if you...' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_YouShouldDefinitelyMessageMeIfYou + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                //// jdate.co.il survey seems to randomly appear somewhere around here.  if things go wrong, get rid of the survey and try again
                //for (int i = 0; i < 2; i++)
                //{
                //    try
                //    {
                // reinitialize to original values
                profile.AboutMe = "This is a test account operated by an employee of Spark Networks.";
                profile.MyLifeAndAmbitions = "I want to win the lottery";
                profile.ABriefHistoryOfMyLife = "Sex drugs and rock and roll";
                profile.OnOurFirstDateRemindMeToTellYouTheStoryAbout = "My trip to Ibiza";
                profile.TheThingsICouldNeverLiveWithout = "The 16 switches in my car";
                profile.MyFavoriteBooksMoviesTVShowsMusicAndFood = "Game of Thrones FTW";
                profile.TheCoolestPlacesIveVisited = "Paris";
                profile.ForFunILikeTo = "Drink hot sauce straight from the bottle";
                profile.OnFridayAndSaturdayNightsITypically = "Am beached on my couch";
                profile.ImLookingFor = "Someone outgoing who can do outdoor activities with me like camping and skydiving and fishing and stuff.";
                profile.YouShouldDefinitelyMessageMeIfYou = "Like turtles.";

                //        break;
                //    }
                //    catch
                //    {
                //        GetRidOfJdateCoIlSurvey();
                //    }
                //}

                profile.Header.Logout_Click();

                TearDown();

                log.Info("END TEST - JDate_Co_Il/Regression/Member/EditProfile_InMyOwnWordsTab");
            }
            catch (Exception e)
            {
                CleanupFailedTestcase(e);
                throw;
            }
        }


        [Test]
        public void Validate_EditProfile_MoreAboutMeTab()
        {
            try
            {
                log.Info("START TEST - JDate_Co_Il/Regression/Member/EditProfile_MoreAboutMeTab");

                Setup();

                StartTest_UsingJDate_Co_Il();

                string login = System.Configuration.ConfigurationManager.AppSettings["JDate_co_il_RegisteredUser3_Email"];
                string password = System.Configuration.ConfigurationManager.AppSettings["JDate_co_il_RegisteredUser3_Password"];

                WebdriverAutomation.Tests.ReusableCode.LoggingInAndOut loggingInAndOut = new LoggingInAndOut();
                WebdriverAutomation.Framework.Site_Pages.Home home = loggingInAndOut.LogIn_ProfileComplete(driver, log, login, password);
                WebdriverAutomation.Framework.Site_Pages.YourProfile profile = home.Header.YourProfileHeaderLink_Click();

                string new_MyPerfectFirstDate = "My perfect first date is with someone who really likes automation. Someone I can talk about code with.";
                string new_MyIdealRelationship = "My ideal relationship would be with someone who likes to rock climb. Because I like to rock climb too.";
                string new_MyPastRelationships = "I would love to talk about my past but it shouldn't be done here. Ask me in person and I will tell you.";

                List<string> new_MyPersonalityIsBestDescribedAs = new List<string>();
                new_MyPersonalityIsBestDescribedAs.Add("הרפתקני/ספונטני");
                new_MyPersonalityIsBestDescribedAs.Add("לא קונבנציונלי/ת");

                List<string> new_InMyFreeTimeIEnjoy = new List<string>();
                new_InMyFreeTimeIEnjoy.Add("משחקי לוח");
                new_InMyFreeTimeIEnjoy.Add("טלוויזיה/ קולנוע");

                List<string> new_InMyFreeTimeILike = new List<string>();
                new_InMyFreeTimeILike.Add("גלריות");
                new_InMyFreeTimeILike.Add("אירועי ספורט");

                List<string> new_MyFavoritePhysicalActivites = new List<string>();
                new_MyFavoritePhysicalActivites.Add("אירובי");
                new_MyFavoritePhysicalActivites.Add("יוגה/מדיטציה");

                List<string> new_MyFavoriteFoods = new List<string>();
                new_MyFavoriteFoods.Add("ברביקיו");
                new_MyFavoriteFoods.Add("צמחוני");

                List<string> new_MyFavoriteMusic = new List<string>();
                new_MyFavoriteMusic.Add("אלטרנטיבי");
                new_MyFavoriteMusic.Add("אחר");

                List<string> new_ILikeToRead = new List<string>();
                new_ILikeToRead.Add("ספרות יפה");
                new_ILikeToRead.Add("קומיקס");

                profile.MoreAboutMeTab_Click();

                //// jdate.co.il survey seems to randomly appear somewhere around here.  if things go wrong, get rid of the survey and try again
                //for (int i = 0; i < 2; i++)
                //{
                //    try
                //    {
                // Overwrite new values to the page
                profile.MyPerfectFirstDate = new_MyPerfectFirstDate;
                profile.MyIdealRelationship = new_MyIdealRelationship;
                profile.MyPastRelationships = new_MyPastRelationships;
                profile.MyPersonalityIsBestDescribedAs = new_MyPersonalityIsBestDescribedAs;
                profile.InMyFreeTimeIEnjoy_JDatePage = new_InMyFreeTimeIEnjoy;
                profile.InMyFreeTimeILike = new_InMyFreeTimeILike;
                profile.MyFavoritePhysicalActivities_JDatePage = new_MyFavoritePhysicalActivites;
                profile.MyFavoriteFoods_JDatePage = new_MyFavoriteFoods;
                profile.MyFavoriteMusic_JDatePage = new_MyFavoriteMusic;
                profile.ILikeToRead = new_ILikeToRead;

                //        break;
                //    }
                //    catch
                //    {
                //        GetRidOfJdateCoIlSurvey();
                //    }
                //}

                // Go to the Home Page and then come back to the Profile Page to verify the values saved correctly
                home = profile.Header.HomeHeaderLink_Click();
                profile = home.Header.YourProfileHeaderLink_Click();

                profile.MoreAboutMeTab_Click();

                string stringToVerifyOnProfilePage = string.Empty;
                List<string> listToVerifyOnProfilePage = new List<string>();

                stringToVerifyOnProfilePage = profile.MyPerfectFirstDate;
                Assert.IsTrue(new_MyPerfectFirstDate == stringToVerifyOnProfilePage, "In the Validate_EditProfile_MoreAboutMeTab test case, after saving new values for 'My perfect first date' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_MyPerfectFirstDate + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.MyIdealRelationship;
                Assert.IsTrue(new_MyIdealRelationship == stringToVerifyOnProfilePage, "In the Validate_EditProfile_MoreAboutMeTab test case, after saving new values for 'My ideal relationship' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_MyIdealRelationship + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.MyPastRelationships;
                Assert.IsTrue(new_MyPastRelationships == stringToVerifyOnProfilePage, "In the Validate_EditProfile_MoreAboutMeTab test case, after saving new values for 'My past relationships' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_MyPastRelationships + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                listToVerifyOnProfilePage = profile.MyPersonalityIsBestDescribedAs;
                new_MyPersonalityIsBestDescribedAs.Sort();
                Assert.IsTrue(AreValuesAndCountEqual(new_MyPersonalityIsBestDescribedAs, listToVerifyOnProfilePage), "In the Validate_EditProfile_MoreAboutMeTab test case, after saving new values for 'My personality is best described as...' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + string.Join(", ", new_MyPersonalityIsBestDescribedAs.ToArray()) + "' \r\nValue currently in Profile Page: '" + string.Join(", ", listToVerifyOnProfilePage.ToArray()) + "'");

                listToVerifyOnProfilePage = profile.InMyFreeTimeIEnjoy_JDatePage;
                new_InMyFreeTimeIEnjoy.Sort();
                Assert.IsTrue(AreValuesAndCountEqual(new_InMyFreeTimeIEnjoy, listToVerifyOnProfilePage), "In the Validate_EditProfile_MoreAboutMeTab test case, after saving new values for 'In my spare time, I enjoy...' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + string.Join(", ", new_InMyFreeTimeIEnjoy.ToArray()) + "' \r\nValue currently in Profile Page: '" + string.Join(", ", listToVerifyOnProfilePage.ToArray()) + "'");

                listToVerifyOnProfilePage = profile.InMyFreeTimeILike;
                new_InMyFreeTimeILike.Sort();
                Assert.IsTrue(AreValuesAndCountEqual(new_InMyFreeTimeILike, listToVerifyOnProfilePage), "In the Validate_EditProfile_MoreAboutMeTab test case, after saving new values for 'In my free time, I like...' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + string.Join(", ", new_InMyFreeTimeILike.ToArray()) + "' \r\nValue currently in Profile Page: '" + string.Join(", ", listToVerifyOnProfilePage.ToArray()) + "'");

                listToVerifyOnProfilePage = profile.MyFavoritePhysicalActivities_JDatePage;
                new_MyFavoritePhysicalActivites.Sort();
                Assert.IsTrue(AreValuesAndCountEqual(new_MyFavoritePhysicalActivites, listToVerifyOnProfilePage), "In the Validate_EditProfile_MoreAboutMeTab test case, after saving new values for 'My favorite physical activities' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + string.Join(", ", new_MyFavoritePhysicalActivites.ToArray()) + "' \r\nValue currently in Profile Page: '" + string.Join(", ", listToVerifyOnProfilePage.ToArray()) + "'");

                listToVerifyOnProfilePage = profile.MyFavoriteFoods_JDatePage;
                new_MyFavoriteFoods.Sort();
                Assert.IsTrue(AreValuesAndCountEqual(new_MyFavoriteFoods, listToVerifyOnProfilePage), "In the Validate_EditProfile_MoreAboutMeTab test case, after saving new values for 'My favorite food(s)' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + string.Join(", ", new_MyFavoriteFoods.ToArray()) + "' \r\nValue currently in Profile Page: '" + string.Join(", ", listToVerifyOnProfilePage.ToArray()) + "'");

                listToVerifyOnProfilePage = profile.MyFavoriteMusic_JDatePage;
                new_MyFavoriteMusic.Sort();
                Assert.IsTrue(AreValuesAndCountEqual(new_MyFavoriteMusic, listToVerifyOnProfilePage), "In the Validate_EditProfile_MoreAboutMeTab test case, after saving new values for 'My favorite music' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + string.Join(", ", new_MyFavoriteMusic.ToArray()) + "' \r\nValue currently in Profile Page: '" + string.Join(", ", listToVerifyOnProfilePage.ToArray()) + "'");

                listToVerifyOnProfilePage = profile.ILikeToRead;
                new_ILikeToRead.Sort();
                Assert.IsTrue(AreValuesAndCountEqual(new_ILikeToRead, listToVerifyOnProfilePage), "In the Validate_EditProfile_MoreAboutMeTab test case, after saving new values for 'I like to read' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + string.Join(", ", new_ILikeToRead.ToArray()) + "' \r\nValue currently in Profile Page: '" + string.Join(", ", listToVerifyOnProfilePage.ToArray()) + "'");

                //// jdate.co.il survey seems to randomly appear somewhere around here.  if things go wrong, get rid of the survey and try again
                //for (int i = 0; i < 2; i++)
                //{
                //    try
                //    {
                // reinitialize to original values
                profile.MyPerfectFirstDate = "My perfect first date is going to dinner then a club for drinks and dancing. Lots of drinks. Don't judge me.";
                profile.MyIdealRelationship = "My ideal relationship would be with someone who likes to party. Someone who can hang all night long.";
                profile.MyPastRelationships = "I have had tons of past relationships. I don't like talking about my past on here though. More in person.";

                List<string> orig_MyPersonalityIsBestDescribedAs = new List<string>();
                orig_MyPersonalityIsBestDescribedAs.Add("משכיל/ה");
                orig_MyPersonalityIsBestDescribedAs.Add("בעל/ת דרישות נמוכות");

                profile.MyPersonalityIsBestDescribedAs = orig_MyPersonalityIsBestDescribedAs;

                List<string> orig_InMyFreeTimeIEnjoy = new List<string>();
                orig_InMyFreeTimeIEnjoy.Add("ציור");
                orig_InMyFreeTimeIEnjoy.Add("מסיבות");

                profile.InMyFreeTimeIEnjoy_JDatePage = orig_InMyFreeTimeIEnjoy;

                List<string> orig_InMyFreeTimeILike = new List<string>();
                orig_InMyFreeTimeILike.Add("סרטים");
                orig_InMyFreeTimeILike.Add("מוזיאונים");

                profile.InMyFreeTimeILike = orig_InMyFreeTimeILike;

                List<string> orig_MyFavoritePhysicalActivites = new List<string>();
                orig_MyFavoritePhysicalActivites.Add("רולר בליידס / סקייטבורד");
                orig_MyFavoritePhysicalActivites.Add("החלקה על הקרח");

                profile.MyFavoritePhysicalActivities_JDatePage = orig_MyFavoritePhysicalActivites;

                List<string> orig_MyFavoriteFoods = new List<string>();
                orig_MyFavoriteFoods.Add("הודי");
                orig_MyFavoriteFoods.Add("איטלקי");

                profile.MyFavoriteFoods_JDatePage = orig_MyFavoriteFoods;

                List<string> orig_MyFavoriteMusic = new List<string>();
                orig_MyFavoriteMusic.Add("רוק'נרול");
                orig_MyFavoriteMusic.Add("עידן חדש");

                profile.MyFavoriteMusic_JDatePage = orig_MyFavoriteMusic;

                List<string> orig_ILikeToRead = new List<string>();
                orig_ILikeToRead.Add("ספרי עיון");
                orig_ILikeToRead.Add("שירה");

                profile.ILikeToRead = orig_ILikeToRead;

                //        break;
                //    }
                //    catch
                //    {
                //        GetRidOfJdateCoIlSurvey();
                //    }
                //}

                profile.Header.Logout_Click();

                TearDown();

                log.Info("END TEST - JDate_Co_Il/Regression/Member/EditProfile_MoreAboutMeTab");
            }
            catch (Exception e)
            {
                CleanupFailedTestcase(e);
                throw;
            }
        }


        [Test]
        public void Validate_EditProfile_DetailsPhysicalInfo()
        {
            try
            {
                log.Info("START TEST - JDate_Co_Il/Regression/Member/EditProfile_DetailsPhysicalInfo");

                Setup();

                StartTest_UsingJDate_Co_Il();

                string login = System.Configuration.ConfigurationManager.AppSettings["JDate_co_il_RegisteredUser3_Email"];
                string password = System.Configuration.ConfigurationManager.AppSettings["JDate_co_il_RegisteredUser3_Password"];

                WebdriverAutomation.Tests.ReusableCode.LoggingInAndOut loggingInAndOut = new LoggingInAndOut();
                WebdriverAutomation.Framework.Site_Pages.Home home = loggingInAndOut.LogIn_ProfileComplete(driver, log, login, password);
                WebdriverAutomation.Framework.Site_Pages.YourProfile profile = home.Header.YourProfileHeaderLink_Click();

                string new_height = "137ס\"מ";
                string new_weight = "38 ק\"ג";
                string new_bodyType = "עגלגל";
                string new_hairColor = "אדמוני";
                string new_eyeColor = "שחור";

                //// jdate.co.il survey seems to randomly appear somewhere around here.  if things go wrong, get rid of the survey and try again
                //for (int i = 0; i < 2; i++)
                //{
                //    try
                //    {
                // Overwrite new values to the page
                profile.PhysicalInfo_ClickToEdit();

                profile.Height_InOverlay = new_height;
                profile.Weight_InOverlay = new_weight;
                profile.BodyType_InOverlay = new_bodyType;
                profile.HairColor_InOverlay = new_hairColor;
                profile.EyeColor_InOverlay = new_eyeColor;

                //GetRidOfJdateCoIlSurvey();

                profile.PhysicalInfo_SaveChanges();

                //        break;
                //    }
                //    catch
                //    {
                //        GetRidOfJdateCoIlSurvey();
                //    }
                //}

                // Go to the Home Page and then come back to the Profile Page to verify the values saved correctly
                home = profile.Header.HomeHeaderLink_Click();
                profile = home.Header.YourProfileHeaderLink_Click();

                string stringToVerifyOnProfilePage = string.Empty;

                stringToVerifyOnProfilePage = profile.Height;
                Assert.IsTrue(new_height.Replace(" ", "") == stringToVerifyOnProfilePage.Replace(" ", ""), "In the Validate_EditProfile_DetailsPhysicalInfo test case, after saving new values for 'Height' in the the Click to Edit overlay the values were not saved properly in the Profile Page \r\nValue we saved in Click to Edit overlay: '" + new_height + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.Weight;
                stringToVerifyOnProfilePage = stringToVerifyOnProfilePage.Replace("Pounds", "lbs");
                Assert.IsTrue(new_weight == stringToVerifyOnProfilePage, "In the Validate_EditProfile_DetailsPhysicalInfo test case, after saving new values for 'Weight' in the the Click to Edit overlay the values were not saved properly in the Profile Page \r\nValue we saved in Click to Edit overlay: '" + new_weight + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.BodyType;
                Assert.IsTrue(new_bodyType == stringToVerifyOnProfilePage, "In the Validate_EditProfile_DetailsPhysicalInfo test case, after saving new values for 'Body Type' in the the Click to Edit overlay the values were not saved properly in the Profile Page \r\nValue we saved in Click to Edit overlay: '" + new_bodyType + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.HairColor;
                Assert.IsTrue(new_hairColor == stringToVerifyOnProfilePage, "In the Validate_EditProfile_DetailsPhysicalInfo test case, after saving new values for 'Hair color' in the the Click to Edit overlay the values were not saved properly in the Profile Page \r\nValue we saved in Click to Edit overlay: '" + new_hairColor + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.EyeColor;
                Assert.IsTrue(new_eyeColor == stringToVerifyOnProfilePage, "In the Validate_EditProfile_DetailsPhysicalInfo test case, after saving new values for 'Eye color' in the the Click to Edit overlay the values were not saved properly in the Profile Page \r\nValue we saved in Click to Edit overlay: '" + new_eyeColor + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.Height_Dealbreakers;
                Assert.IsTrue(new_height.Replace(" ", "") == stringToVerifyOnProfilePage.Replace(" ", ""), "In the Validate_EditProfile_Dealbreakers test case, after saving new values for 'Height' the values were not saved properly in the Profile Page in the Dealbreakers section \r\nValue we saved in Edit Profile: '" + new_height + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                //// jdate.co.il survey seems to randomly appear somewhere around here.  if things go wrong, get rid of the survey and try again
                //for (int i = 0; i < 2; i++)
                //{
                //    try
                //    {
                // reinitialize to original values
                profile.PhysicalInfo_ClickToEdit();

                profile.Height_InOverlay = "171ס\"מ";
                profile.Weight_InOverlay = "82 ק\"ג";
                profile.BodyType_InOverlay = "אצילי";
                profile.HairColor_InOverlay = "הרבה צבעים";
                profile.EyeColor_InOverlay = "טורקיז";

                //GetRidOfJdateCoIlSurvey();

                profile.PhysicalInfo_SaveChanges();

                //        break;
                //    }
                //    catch
                //    {
                //        GetRidOfJdateCoIlSurvey();
                //    }
                //}

                profile.Header.Logout_Click();

                TearDown();

                log.Info("END TEST - JDate_Co_Il/Regression/Member/EditProfile_DetailsPhysicalInfo");
            }
            catch (Exception e)
            {
                CleanupFailedTestcase(e);
                throw;
            }
        }

        [Test]
        public void Validate_EditProfile_DetailsLifestyle()
        {
            try
            {
                log.Info("START TEST - JDate_Co_Il/Regression/Member/EditProfile_DetailsLifestyle");

                Setup();

                StartTest_UsingJDate_Co_Il();

                string login = System.Configuration.ConfigurationManager.AppSettings["JDate_co_il_RegisteredUser3_Email"];
                string password = System.Configuration.ConfigurationManager.AppSettings["JDate_co_il_RegisteredUser3_Password"];

                WebdriverAutomation.Tests.ReusableCode.LoggingInAndOut loggingInAndOut = new LoggingInAndOut();
                WebdriverAutomation.Framework.Site_Pages.Home home = loggingInAndOut.LogIn_ProfileComplete(driver, log, login, password);
                WebdriverAutomation.Framework.Site_Pages.YourProfile profile = home.Header.YourProfileHeaderLink_Click();

                string new_MaritalStatus = "רווק/ה";
                string new_Relocation = "כן";
                string new_HaveKids = "אין לי ילדים";
                string new_Custody = "אין לי ילדים";
                string new_WantKids = "לא";

                List<string> new_Pets = new List<string>();
                new_Pets.Add("ציפור");
                new_Pets.Add("חיות זה לא אני");

                string new_Kosher = "רק בבית";
                string new_Synagogue = "כל שבת";
                string new_Smoke = "לא מעשן/ת";
                string new_Drink = "אף פעם";
                string new_ActivityLevel = "לא פעיל/ה";

                //// jdate.co.il survey seems to randomly appear somewhere around here.  if things go wrong, get rid of the survey and try again
                //for (int i = 0; i < 2; i++)
                //{
                //    try
                //    {
                // Overwrite new values to the page
                profile.Lifestyle_ClickToEdit();

                profile.MaritalStatus_InOverlay = new_MaritalStatus;
                profile.Relocation_InOverlay = new_Relocation;
                profile.HaveKids_InOverlay = new_HaveKids;
                profile.Custody_InOverlay = new_Custody;
                profile.WantKids_InOverlay = new_WantKids;
                profile.Pets_InOverlay = new_Pets;
                profile.Kosher_InOverlay = new_Kosher;
                profile.Synagogue_InOverlay = new_Synagogue;
                profile.Smoking_InOverlay = new_Smoke;
                profile.Drinking_InOverlay = new_Drink;
                profile.ActivityLevel_InOverlay = new_ActivityLevel;

                //GetRidOfJdateCoIlSurvey();

                profile.Lifestyle_SaveChanges();

                //        break;
                //    }
                //    catch
                //    {
                //        GetRidOfJdateCoIlSurvey();
                //    }
                //}

                // Go to the Home Page and then come back to the Profile Page to verify the values saved correctly
                home = profile.Header.HomeHeaderLink_Click();
                profile = home.Header.YourProfileHeaderLink_Click();

                string stringToVerifyOnProfilePage = string.Empty;
                List<string> listToVerifyOnProfilePage = new List<string>();

                stringToVerifyOnProfilePage = profile.MaritalStatus;
                Assert.IsTrue(new_MaritalStatus == stringToVerifyOnProfilePage.Replace(" ", string.Empty), "In the Validate_EditProfile_DetailsLifestyle test case, after saving new values for 'Marital Status' in the the Click to Edit overlay the values were not saved properly in the Profile Page \r\nValue we saved in Click to Edit overlay: '" + new_MaritalStatus + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.Relocation;
                Assert.IsTrue(new_Relocation == stringToVerifyOnProfilePage, "In the Validate_EditProfile_DetailsLifestyle test case, after saving new values for 'Relocation' in the the Click to Edit overlay the values were not saved properly in the Profile Page \r\nValue we saved in Click to Edit overlay: '" + new_Relocation + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.HaveKids;
                Assert.IsTrue(new_HaveKids == stringToVerifyOnProfilePage, "In the Validate_EditProfile_DetailsLifestyle test case, after saving new values for 'Have kids' in the the Click to Edit overlay the values were not saved properly in the Profile Page \r\nValue we saved in Click to Edit overlay: '" + new_HaveKids + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.Custody;
                Assert.IsTrue(new_Custody == stringToVerifyOnProfilePage, "In the Validate_EditProfile_DetailsLifestyle test case, after saving new values for 'Custody' in the the Click to Edit overlay the values were not saved properly in the Profile Page \r\nValue we saved in Click to Edit overlay: '" + new_Custody + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.WantKids;
                Assert.IsTrue(new_WantKids == stringToVerifyOnProfilePage, "In the Validate_EditProfile_DetailsLifestyle test case, after saving new values for 'Wants kids' in the the Click to Edit overlay the values were not saved properly in the Profile Page \r\nValue we saved in Click to Edit overlay: '" + new_WantKids + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                listToVerifyOnProfilePage = profile.Pets;
                new_Pets.Sort();
                Assert.IsTrue(AreValuesAndCountEqual(new_Pets, listToVerifyOnProfilePage), "In the Validate_EditProfile_MoreAboutMeTab test case, after saving new values for 'Pets' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + string.Join(", ", new_Pets.ToArray()) + "' \r\nValue currently in Profile Page: '" + string.Join(", ", listToVerifyOnProfilePage.ToArray()) + "'");

                stringToVerifyOnProfilePage = profile.Kosher;
                Assert.IsTrue(new_Kosher == stringToVerifyOnProfilePage, "In the Validate_EditProfile_DetailsLifestyle test case, after saving new values for 'Kosher' in the the Click to Edit overlay the values were not saved properly in the Profile Page \r\nValue we saved in Click to Edit overlay: '" + new_Kosher + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.Synagogue;
                Assert.IsTrue(new_Synagogue == stringToVerifyOnProfilePage, "In the Validate_EditProfile_DetailsLifestyle test case, after saving new values for 'Synagogue' in the the Click to Edit overlay the values were not saved properly in the Profile Page \r\nValue we saved in Click to Edit overlay: '" + new_Synagogue + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.Smoking;
                Assert.IsTrue(new_Smoke.Replace(" ", string.Empty) == stringToVerifyOnProfilePage.Replace(" ", string.Empty), "In the Validate_EditProfile_DetailsLifestyle test case, after saving new values for 'Smoking' in the the Click to Edit overlay the values were not saved properly in the Profile Page \r\nValue we saved in Click to Edit overlay: '" + new_Smoke + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.Drinking;
                Assert.IsTrue(new_Drink == stringToVerifyOnProfilePage, "In the Validate_EditProfile_DetailsLifestyle test case, after saving new values for 'Drinking' in the the Click to Edit overlay the values were not saved properly in the Profile Page \r\nValue we saved in Click to Edit overlay: '" + new_MaritalStatus + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.HaveKids_Dealbreakers;
                Assert.IsTrue(new_HaveKids == stringToVerifyOnProfilePage, "In the Validate_EditProfile_Dealbreakers test case, after saving new values for 'Have kids' the values were not saved properly in the Profile Page in the Dealbreakers section \r\nValue we saved in Edit Profile: '" + new_HaveKids + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.WantsKids_Dealbreakers;
                Assert.IsTrue(new_WantKids == stringToVerifyOnProfilePage, "In the Validate_EditProfile_Dealbreakers test case, after saving new values for 'Wants kids' the values were not saved properly in the Profile Page in the Dealbreakers section \r\nValue we saved in Edit Profile: '" + new_WantKids + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.Smoking_Dealbreakers;
                Assert.IsTrue(new_Smoke.Replace(" ", string.Empty) == stringToVerifyOnProfilePage.Replace(" ", string.Empty), "In the Validate_EditProfile_Dealbreakers test case, after saving new values for 'Smoking' the values were not saved properly in the Profile Page in the Dealbreakers section \r\nValue we saved in Edit Profile: '" + new_Smoke + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.Drinking_Dealbreakers;
                Assert.IsTrue(new_Drink == stringToVerifyOnProfilePage, "In the Validate_EditProfile_Dealbreakers test case, after saving new values for 'Drinking' the values were not saved properly in the Profile Page in the Dealbreakers section \r\nValue we saved in Edit Profile: '" + new_Drink + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                //// jdate.co.il survey seems to randomly appear somewhere around here.  if things go wrong, get rid of the survey and try again
                //for (int i = 0; i < 2; i++)
                //{
                //    try
                //    {
                // reinitialize to original values
                List<string> orig_Pets = new List<string>();
                orig_Pets.Add("זוחלים");
                orig_Pets.Add("אוגר");

                profile.Lifestyle_ClickToEdit();

                profile.MaritalStatus_InOverlay = "אלמן/ה";
                profile.Relocation_InOverlay = "לא בטוח";
                profile.HaveKids_InOverlay = "3 או יותר";
                profile.Custody_InOverlay = "גרים איתי";
                profile.WantKids_InOverlay = "אולי";
                profile.Pets_InOverlay = orig_Pets;
                profile.Kosher_InOverlay = "במידה מסוימת";
                profile.Synagogue_InOverlay = "אספר בהמשך";
                profile.Smoking_InOverlay = "מנסה להפסיק";
                profile.Drinking_InOverlay = "לעיתים קרובות";
                profile.ActivityLevel_InOverlay = "פעיל/ה מאוד";

                //GetRidOfJdateCoIlSurvey();

                profile.Lifestyle_SaveChanges();

                //        break;
                //    }
                //    catch
                //    {
                //        GetRidOfJdateCoIlSurvey();
                //    }
                //}

                profile.Header.Logout_Click();

                TearDown();

                log.Info("END TEST - JDate_Co_Il/Regression/Member/EditProfile_DetailsLifestyle");
            }
            catch (Exception e)
            {
                CleanupFailedTestcase(e);
                throw;
            }
        }

        [Test]
        public void Validate_EditProfile_DetailsBackground()
        {
            try
            {
                log.Info("START TEST - JDate_Co_Il/Regression/Member/EditProfile_DetailsBackground");

                Setup();

                StartTest_UsingJDate_Co_Il();

                string login = System.Configuration.ConfigurationManager.AppSettings["JDate_co_il_RegisteredUser3_Email"];
                string password = System.Configuration.ConfigurationManager.AppSettings["JDate_co_il_RegisteredUser3_Password"];

                WebdriverAutomation.Tests.ReusableCode.LoggingInAndOut loggingInAndOut = new LoggingInAndOut();
                WebdriverAutomation.Framework.Site_Pages.Home home = loggingInAndOut.LogIn_ProfileComplete(driver, log, login, password);
                WebdriverAutomation.Framework.Site_Pages.YourProfile profile = home.Header.YourProfileHeaderLink_Click();

                string new_GrewUpIn = "The old country";
                string new_MyEthnicityIs = "אשכנזי";

                List<string> new_Languages = new List<string>();
                new_Languages.Add("ערבית");
                new_Languages.Add("אחר");

                string new_Religion = "רפורמי";
                string new_Education = "יסודי";
                string new_AreaOfStudy = "Math";
                string new_Occuaption = "בגמלאות";
                string new_WhatIDo = "Student";
                string new_AnnualIncome = "אספר בהמשך";
                string new_Politics = "לא מוגדר";
                string new_ZodiacSign = "טלה";

                //// jdate.co.il survey seems to randomly appear somewhere around here.  if things go wrong, get rid of the survey and try again
                //for (int i = 0; i < 2; i++)
                //{
                //    try
                //    {
                // Overwrite new values to the page
                profile.Background_ClickToEdit();

                profile.GrewUpIn_InOverlay = new_GrewUpIn;
                profile.Ethnicity_InOverlay = new_MyEthnicityIs;
                profile.Languages_InOverlay = new_Languages;
                profile.Religion_InOverlay = new_Religion;
                profile.Education_InOverlay = new_Education;
                profile.AreaOfStudy_InOverlay = new_AreaOfStudy;
                profile.Occupation_InOverlay = new_Occuaption;
                profile.WhatIDo_InOverlay = new_WhatIDo;
                profile.AnnualIncome_InOverlay = new_AnnualIncome;
                profile.Politics_InOverlay = new_Politics;
                profile.ZodiacSign_InOverlay = new_ZodiacSign;

                //GetRidOfJdateCoIlSurvey();

                profile.Background_SaveChanges();

                //        break;
                //    }
                //    catch
                //    {
                //        GetRidOfJdateCoIlSurvey();
                //    }
                //}

                // Go to the Home Page and then come back to the Profile Page to verify the values saved correctly
                home = profile.Header.HomeHeaderLink_Click();
                profile = home.Header.YourProfileHeaderLink_Click();

                string stringToVerifyOnProfilePage = string.Empty;
                List<string> listToVerifyOnProfilePage = new List<string>();

                stringToVerifyOnProfilePage = profile.GrewUpIn;
                Assert.IsTrue(new_GrewUpIn == stringToVerifyOnProfilePage, "In the Validate_EditProfile_DetailsBackground test case, after saving new values for 'Grew up in' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_GrewUpIn + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.Ethnicity;
                Assert.IsTrue(new_MyEthnicityIs == stringToVerifyOnProfilePage, "In the Validate_EditProfile_DetailsBackground test case, after saving new values for 'Ethnicity' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_MyEthnicityIs + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                listToVerifyOnProfilePage = profile.Languages;
                new_Languages.Sort();
                Assert.IsTrue(AreValuesAndCountEqual(new_Languages, listToVerifyOnProfilePage), "In the Validate_EditProfile_MoreAboutMeTab test case, after saving new values for 'Languages' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + string.Join(", ", new_Languages.ToArray()) + "' \r\nValue currently in Profile Page: '" + string.Join(", ", listToVerifyOnProfilePage.ToArray()) + "'");

                stringToVerifyOnProfilePage = profile.Religion;
                Assert.IsTrue(new_Religion == stringToVerifyOnProfilePage, "In the Validate_EditProfile_DetailsBackground test case, after saving new values for 'Religion' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_Religion + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.Education;
                Assert.IsTrue(new_Education == stringToVerifyOnProfilePage, "In the Validate_EditProfile_DetailsBackground test case, after saving new values for 'Education' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_Education + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.AreaOfStudy;
                Assert.IsTrue(new_AreaOfStudy == stringToVerifyOnProfilePage, "In the Validate_EditProfile_DetailsBackground test case, after saving new values for 'Area of study' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_AreaOfStudy + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.Occupation;
                Assert.IsTrue(new_Occuaption.Replace(" ", string.Empty) == stringToVerifyOnProfilePage.Replace(" ", string.Empty), "In the Validate_EditProfile_DetailsBackground test case, after saving new values for 'Occupation' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_Occuaption + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.WhatIDo;
                Assert.IsTrue(new_WhatIDo == stringToVerifyOnProfilePage, "In the Validate_EditProfile_DetailsBackground test case, after saving new values for 'What I do' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_WhatIDo + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.AnnualIncome;
                Assert.IsTrue(new_AnnualIncome == stringToVerifyOnProfilePage, "In the Validate_EditProfile_DetailsBackground test case, after saving new values for 'Annual income' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_AnnualIncome + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.Politics;
                Assert.IsTrue(new_Politics == stringToVerifyOnProfilePage, "In the Validate_EditProfile_DetailsBackground test case, after saving new values for 'Politics' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_Politics + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.ZodiacSign;
                Assert.IsTrue(new_ZodiacSign == stringToVerifyOnProfilePage, "In the Validate_EditProfile_DetailsBackground test case, after saving new values for 'Zodiac sign' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_ZodiacSign + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.Ethnicity_Dealbreakers;
                Assert.IsTrue(new_MyEthnicityIs == stringToVerifyOnProfilePage, "In the Validate_EditProfile_DetailsBackground test case, after saving new values for 'Ethnicity' the values were not saved properly in the Profile Page in the Dealbreakers section \r\nValue we saved in Edit Profile: '" + new_MyEthnicityIs + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.Religion_Dealbreakers;
                Assert.IsTrue(new_Religion == stringToVerifyOnProfilePage, "In the Validate_EditProfile_DetailsBackground test case, after saving new values for 'Religion' the values were not saved properly in the Profile Page in the Dealbreakers section \r\nValue we saved in Edit Profile: '" + new_Religion + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.Education_Dealbreakers;
                Assert.IsTrue(new_Education == stringToVerifyOnProfilePage, "In the Validate_EditProfile_DetailsBackground test case, after saving new values for 'Education' the values were not saved properly in the Profile Page in the Dealbreakers section \r\nValue we saved in Edit Profile: '" + new_Education + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.Occupation_Dealbreakers;
                Assert.IsTrue(new_Occuaption == stringToVerifyOnProfilePage, "In the Validate_EditProfile_DetailsBackground test case, after saving new values for 'Occupation' the values were not saved properly in the Profile Page in the Dealbreakers section \r\nValue we saved in Edit Profile: '" + new_Occuaption + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.AnnualIncome_Dealbreakers;
                Assert.IsTrue(new_AnnualIncome == stringToVerifyOnProfilePage, "In the Validate_EditProfile_DetailsBackground test case, after saving new values for 'Annual Income the values were not saved properly in the Profile Page in the Dealbreakers section \r\nValue we saved in Edit Profile: '" + new_AnnualIncome + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                //// jdate.co.il survey seems to randomly appear somewhere around here.  if things go wrong, get rid of the survey and try again
                //for (int i = 0; i < 2; i++)
                //{
                //    try
                //    {
                // reinitialize to original values
                List<string> orig_Languages = new List<string>();
                orig_Languages.Add("עברית");
                orig_Languages.Add("איטלקית");

                profile.Background_ClickToEdit();

                profile.GrewUpIn_InOverlay = "The City";
                profile.Ethnicity_InOverlay = "אספר בהמשך";
                profile.Languages_InOverlay = orig_Languages;
                profile.Religion_InOverlay = "איני מוכן/ה להתגייר";
                profile.Education_InOverlay = "דוקטורט";
                profile.AreaOfStudy_InOverlay = "Video games";
                profile.Occupation_InOverlay = "טכני/מדעי/הנדסי";
                profile.WhatIDo_InOverlay = "Teacher";
                profile.AnnualIncome_InOverlay = "גבוהה";
                profile.Politics_InOverlay = "ימין";
                profile.ZodiacSign_InOverlay = "לא מאמין/ה בזה";

                //GetRidOfJdateCoIlSurvey();

                profile.Background_SaveChanges();

                //        break;
                //    }
                //    catch
                //    {
                //        GetRidOfJdateCoIlSurvey();
                //    }
                //}

                profile.Header.Logout_Click();

                TearDown();

                log.Info("END TEST - JDate_Co_Il/Regression/Member/EditProfile_DetailsBackground");
            }
            catch (Exception e)
            {
                CleanupFailedTestcase(e);
                throw;
            }
        }

        /// <summary>
        /// We do not test the Username field
        /// </summary>
        [Test]
        public void Validate_EditProfile_YourBasics()
        {
            try
            {
                log.Info("START TEST - JDate_Co_Il/Regression/Member/EditProfile_YourBasics");
                Setup();

                StartTest_UsingJDate_Co_Il();

                string login = System.Configuration.ConfigurationManager.AppSettings["JDate_co_il_RegisteredUser3_Email"];
                string password = System.Configuration.ConfigurationManager.AppSettings["JDate_co_il_RegisteredUser3_Password"];

                WebdriverAutomation.Tests.ReusableCode.LoggingInAndOut loggingInAndOut = new LoggingInAndOut();
                WebdriverAutomation.Framework.Site_Pages.Home home = loggingInAndOut.LogIn_ProfileComplete(driver, log, login, password);
                WebdriverAutomation.Framework.Site_Pages.YourProfile profile = home.Header.YourProfileHeaderLink_Click();

                string new_FirstName = "Mark";
                string new_LastName = "Azuras";
                string new_DateOfBirthMonth = "פברואר";
                string new_DateOfBirthDay = "15";
                string new_DateOfBirthYear = "1980";
                //string new_Country = "אוגנדה";
                //string new_Zipcode = "K8N5W6";
                string new_ImA = "גבר מחפש אישה";
                string new_RelationshipStatus = "רווק/ה";

                //// jdate.co.il survey seems to randomly appear somewhere around here.  if things go wrong, get rid of the survey and try again
                //for (int i = 0; i < 2; i++)
                //{
                //    try
                //    {
                // Overwrite new values to the page
                profile.YourBasics_ClickToEdit();

                profile.FirstName_InOverlay = new_FirstName;
                profile.LastName_InOverlay = new_LastName;
                profile.YourDateOfBirthMonth_InOverlay = new_DateOfBirthMonth;
                profile.YourDateOfBirthDay_InOverlay = new_DateOfBirthDay;
                profile.YourDateOfBirthYear_InOverlay = new_DateOfBirthYear;
                //profile.Country_InOverlay = new_Country;
                //profile.ZipCode_InOverlay = new_Zipcode;
                profile.ImA_InOverlay = new_ImA;
                profile.RelationshipStatus_InOverlay = new_RelationshipStatus;

                //GetRidOfJdateCoIlSurvey();

                profile.YourBasics_SaveChanges();

                //        break;
                //    }
                //    catch
                //    {
                //        GetRidOfJdateCoIlSurvey();
                //    }
                //}

                // Go to the Home Page and then come back to the Profile Page to verify the values saved correctly
                home = profile.Header.HomeHeaderLink_Click();
                profile = home.Header.YourProfileHeaderLink_Click();

                string stringToVerifyOnProfilePage = string.Empty;

                stringToVerifyOnProfilePage = profile.RelationshipStatus;
                Assert.IsTrue(new_RelationshipStatus == stringToVerifyOnProfilePage, "In the Validate_EditProfile_YourBasics test case, after saving new values for 'Relationship status' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_RelationshipStatus + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.ImA;
                // stupid small discrepancy where the text is slightly different on the Profile Page than on the Edit Profile overlay
                if (new_ImA == "גבר מחפש אישה")
                    new_ImA = "גבר המחפש/ת אשה";

                Assert.IsTrue(new_ImA == stringToVerifyOnProfilePage, "In the Validate_EditProfile_YourBasics test case, after saving new values for 'I'm a' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_ImA + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                int ageOfUser = AgeOfUser(new_DateOfBirthMonth, new_DateOfBirthDay, new_DateOfBirthYear);
                stringToVerifyOnProfilePage = profile.Age;
                Assert.IsTrue(Convert.ToInt32(stringToVerifyOnProfilePage) == ageOfUser, "In the Validate_EditProfile_YourBasics test case, after saving new values for 'Age' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + ageOfUser + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                //stringToVerifyOnProfilePage = profile.Country;
                //Assert.IsTrue(new_Country == stringToVerifyOnProfilePage, "In the Validate_EditProfile_YourBasics test case, after saving new values for 'Country' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_Country + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                //// jdate.co.il survey seems to randomly appear somewhere around here.  if things go wrong, get rid of the survey and try again
                //for (int i = 0; i < 2; i++)
                //{
                //    try
                //    {
                // reinitialize to original values
                profile.YourBasics_ClickToEdit();

                profile.FirstName_InOverlay = "Theo";
                profile.LastName_InOverlay = "Wang";
                profile.YourDateOfBirthMonth_InOverlay = "דצמבר";
                profile.YourDateOfBirthDay_InOverlay = "16";
                profile.YourDateOfBirthYear_InOverlay = "1979";
                //profile.Country_InOverlay = "USA";
                //profile.ZipCode_InOverlay = "90210";
                profile.ImA_InOverlay = "אישה מחפשת אישה";
                profile.RelationshipStatus_InOverlay = "אלמן/ה";

                //GetRidOfJdateCoIlSurvey();

                profile.YourBasics_SaveChanges();

                //        break;
                //    }
                //    catch
                //    {
                //        GetRidOfJdateCoIlSurvey();
                //    }
                //}

                profile.Header.Logout_Click();

                TearDown();

                log.Info("END TEST - JDate_Co_Il/Regression/Member/EditProfile_YourBasics");
            }
            catch (Exception e)
            {
                CleanupFailedTestcase(e);
                throw;
            }
        }

        [Test]
        public void Validate_EditProfile_Dealbreakers()
        {
            try
            {
                log.Info("START TEST - JDate_Co_Il/Regression/Member/EditProfile_Dealbreakers");
                Setup();

                StartTest_UsingJDate_Co_Il();

                string login = System.Configuration.ConfigurationManager.AppSettings["JDate_co_il_RegisteredUser3_Email"];
                string password = System.Configuration.ConfigurationManager.AppSettings["JDate_co_il_RegisteredUser3_Password"];

                WebdriverAutomation.Tests.ReusableCode.LoggingInAndOut loggingInAndOut = new LoggingInAndOut();
                WebdriverAutomation.Framework.Site_Pages.Home home = loggingInAndOut.LogIn_ProfileComplete(driver, log, login, password);
                WebdriverAutomation.Framework.Site_Pages.YourProfile profile = home.Header.YourProfileHeaderLink_Click();

                string new_HaveKids = "1";
                string new_WantsKids = "לא";
                string new_height = "137ס\"מ";
                string new_Ethnicity = "אשכנזי";
                string new_Religion = "רפורמי";
                string new_Smoking = "לא מעשן/ת";
                string new_Drinking = "אף פעם";
                string new_Education = "יסודי";
                string new_Occupation = "בגמלאות";
                string new_AnnualIncome = "אספר בהמשך";

                //// jdate.co.il survey seems to randomly appear somewhere around here.  if things go wrong, get rid of the survey and try again
                //for (int i = 0; i < 2; i++)
                //{
                //    try
                //    {
                // Overwrite new values to the page
                profile.Dealbreakers_ClickToEdit();

                profile.HaveKids_InDealbreakersOverlay = new_HaveKids;
                profile.WantsKids_InDealbreakersOverlay = new_WantsKids;
                profile.Height_InDealbreakersOverlay = new_height;
                profile.Ethnicity_InDealbreakersOverlay = new_Ethnicity;
                profile.Religion_InDealbreakersOverlay = new_Religion;
                profile.Smoking_InDealbreakersOverlay = new_Smoking;
                profile.Drinking_InDealbreakersOverlay = new_Drinking;
                profile.Education_InDealbreakersOverlay = new_Education;
                profile.Occupation_InDealbreakersOverlay = new_Occupation;
                profile.AnnualIncome_InDealbreakersOverlay = new_AnnualIncome;

                //GetRidOfJdateCoIlSurvey();

                profile.Dealbreakers_SaveChanges();

                //        break;
                //    }
                //    catch
                //    {
                //        GetRidOfJdateCoIlSurvey();
                //    }
                //}

                // Go to the Home Page and then come back to the Profile Page to verify the values saved correctly
                home = profile.Header.HomeHeaderLink_Click();
                profile = home.Header.YourProfileHeaderLink_Click();

                string stringToVerifyOnProfilePage = string.Empty;

                stringToVerifyOnProfilePage = profile.HaveKids_Dealbreakers;
                Assert.IsTrue(new_HaveKids == stringToVerifyOnProfilePage, "In the Validate_EditProfile_Dealbreakers test case, after saving new values for 'Have kids' the values were not saved properly in the Profile Page in the Dealbreakers section \r\nValue we saved in Edit Profile: '" + new_HaveKids + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.WantsKids_Dealbreakers;
                Assert.IsTrue(new_WantsKids == stringToVerifyOnProfilePage, "In the Validate_EditProfile_Dealbreakers test case, after saving new values for 'Wants kids' the values were not saved properly in the Profile Page in the Dealbreakers section \r\nValue we saved in Edit Profile: '" + new_WantsKids + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.Height_Dealbreakers;
                Assert.IsTrue(new_height.Replace(" ", "") == stringToVerifyOnProfilePage.Replace(" ", ""), "In the Validate_EditProfile_Dealbreakers test case, after saving new values for 'Height' the values were not saved properly in the Profile Page in the Dealbreakers section \r\nValue we saved in Edit Profile: '" + new_height + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.Ethnicity_Dealbreakers;
                Assert.IsTrue(new_Ethnicity == stringToVerifyOnProfilePage, "In the Validate_EditProfile_Dealbreakers test case, after saving new values for 'Ethnicity' the values were not saved properly in the Profile Page in the Dealbreakers section \r\nValue we saved in Edit Profile: '" + new_Ethnicity + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.Religion_Dealbreakers;
                Assert.IsTrue(new_Religion == stringToVerifyOnProfilePage, "In the Validate_EditProfile_Dealbreakers test case, after saving new values for 'Religion' the values were not saved properly in the Profile Page in the Dealbreakers section \r\nValue we saved in Edit Profile: '" + new_Religion + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.Smoking_Dealbreakers;
                Assert.IsTrue(new_Smoking.Replace(" ", string.Empty) == stringToVerifyOnProfilePage.Replace(" ", string.Empty), "In the Validate_EditProfile_Dealbreakers test case, after saving new values for 'Smoking' the values were not saved properly in the Profile Page in the Dealbreakers section \r\nValue we saved in Edit Profile: '" + new_Smoking + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.Drinking_Dealbreakers;
                Assert.IsTrue(new_Drinking == stringToVerifyOnProfilePage, "In the Validate_EditProfile_Dealbreakers test case, after saving new values for 'Drinking' the values were not saved properly in the Profile Page in the Dealbreakers section \r\nValue we saved in Edit Profile: '" + new_Drinking + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.Education_Dealbreakers;
                Assert.IsTrue(new_Education == stringToVerifyOnProfilePage, "In the Validate_EditProfile_Dealbreakers test case, after saving new values for 'Education' the values were not saved properly in the Profile Page in the Dealbreakers section \r\nValue we saved in Edit Profile: '" + new_Education + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.Occupation_Dealbreakers;
                Assert.IsTrue(new_Occupation == stringToVerifyOnProfilePage, "In the Validate_EditProfile_Dealbreakers test case, after saving new values for 'Occupation' the values were not saved properly in the Profile Page in the Dealbreakers section \r\nValue we saved in Edit Profile: '" + new_Occupation + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.AnnualIncome_Dealbreakers;
                Assert.IsTrue(new_AnnualIncome == stringToVerifyOnProfilePage, "In the Validate_EditProfile_Dealbreakers test case, after saving new values for 'Annual $' the values were not saved properly in the Profile Page in the Dealbreakers section \r\nValue we saved in Edit Profile: '" + new_AnnualIncome + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.HaveKids;
                Assert.IsTrue(new_HaveKids == stringToVerifyOnProfilePage, "In the Validate_EditProfile_Dealbreakers test case, after saving new values for 'Have kids' the values were not saved properly in the Profile Page in the Details section \r\nValue we saved in Edit Profile: '" + new_HaveKids + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.WantKids;
                Assert.IsTrue(new_WantsKids == stringToVerifyOnProfilePage, "In the Validate_EditProfile_Dealbreakers test case, after saving new values for 'Wants kids' the values were not saved properly in the Profile Page in the Details section \r\nValue we saved in Edit Profile: '" + new_WantsKids + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.Height;
                Assert.IsTrue(new_height.Replace(" ", "") == stringToVerifyOnProfilePage.Replace(" ", ""), "In the Validate_EditProfile_Dealbreakers test case, after saving new values for 'Height' the values were not saved properly in the Profile Page in the Details section \r\nValue we saved in Edit Profile: '" + new_height + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.Ethnicity;
                Assert.IsTrue(new_Ethnicity == stringToVerifyOnProfilePage, "In the Validate_EditProfile_Dealbreakers test case, after saving new values for 'Ethnicity' the values were not saved properly in the Profile Page in the Details section \r\nValue we saved in Edit Profile: '" + new_Ethnicity + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.Religion;
                Assert.IsTrue(new_Religion == stringToVerifyOnProfilePage, "In the Validate_EditProfile_Dealbreakers test case, after saving new values for 'Religion' the values were not saved properly in the Profile Page in the Details section \r\nValue we saved in Edit Profile: '" + new_Religion + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.Smoking;
                Assert.IsTrue(new_Smoking.Replace(" ", string.Empty) == stringToVerifyOnProfilePage.Replace(" ", string.Empty), "In the Validate_EditProfile_Dealbreakers test case, after saving new values for 'Smoking' the values were not saved properly in the Profile Page in the Details section \r\nValue we saved in Edit Profile: '" + new_Smoking + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.Drinking;
                Assert.IsTrue(new_Drinking == stringToVerifyOnProfilePage, "In the Validate_EditProfile_Dealbreakers test case, after saving new values for 'Drinking' the values were not saved properly in the Profile Page in the Details section \r\nValue we saved in Edit Profile: '" + new_Drinking + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.Education;
                Assert.IsTrue(new_Education == stringToVerifyOnProfilePage, "In the Validate_EditProfile_Dealbreakers test case, after saving new values for 'Education' the values were not saved properly in the Profile Page in the Details section \r\nValue we saved in Edit Profile: '" + new_Education + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.Occupation;
                Assert.IsTrue(new_Occupation == stringToVerifyOnProfilePage, "In the Validate_EditProfile_Dealbreakers test case, after saving new values for 'Occupation' the values were not saved properly in the Profile Page in the Details section \r\nValue we saved in Edit Profile: '" + new_Occupation + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.AnnualIncome;
                Assert.IsTrue(new_AnnualIncome == stringToVerifyOnProfilePage, "In the Validate_EditProfile_Dealbreakers test case, after saving new values for 'Annual $' the values were not saved properly in the Profile Page in the Details section \r\nValue we saved in Edit Profile: '" + new_Occupation + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                //// jdate.co.il survey seems to randomly appear somewhere around here.  if things go wrong, get rid of the survey and try again
                //for (int i = 0; i < 2; i++)
                //{
                //    try
                //    {
                // reinitialize to original values
                profile.Dealbreakers_ClickToEdit();

                profile.HaveKids_InDealbreakersOverlay = "3 או יותר";
                profile.WantsKids_InDealbreakersOverlay = "אולי";
                profile.Height_InDealbreakersOverlay = "244ס\"מ";
                profile.Ethnicity_InDealbreakersOverlay = "אספר בהמשך";
                profile.Religion_InDealbreakersOverlay = "חילוני";
                profile.Smoking_InDealbreakersOverlay = "מנסה להפסיק";
                profile.Drinking_InDealbreakersOverlay = "לעיתים קרובות";
                profile.Education_InDealbreakersOverlay = "דוקטורט";
                profile.Occupation_InDealbreakersOverlay = "סטודנט";
                profile.AnnualIncome_InDealbreakersOverlay = "גבוהה";

                //GetRidOfJdateCoIlSurvey();

                profile.Dealbreakers_SaveChanges();

                //    break;
                //}
                //catch
                //{
                //    GetRidOfJdateCoIlSurvey();
                //}
                //}

                profile.Header.Logout_Click();

                TearDown();

                log.Info("END TEST - JDate_Co_Il/Regression/Member/EditProfile_Dealbreakers");
            }
            catch (Exception e)
            {
                CleanupFailedTestcase(e);
                throw;
            }
        }

        ///// <summary>
        ///// Setup:  The user that we login with for this test case should not have any photos currently uploaded. Else this test case will delete all existing photos
        ///// before starting.
        ///// </summary>
        //[Test]
        //public void Validate_UploadAndEditPhotos()
        //{
        //    try
        //    {
        //        log.Info("START TEST - JDate_Co_Il/Regression/Member/UploadAndEditPhotos");

        //        Setup();

        //        StartTest_UsingJDate_Co_Il();

        //        string login = System.Configuration.ConfigurationManager.AppSettings["JDate_co_il_RegisteredUser2_Email"];
        //        string password = System.Configuration.ConfigurationManager.AppSettings["JDate_co_il_RegisteredUser2_Password"];

        //        WebdriverAutomation.Tests.ReusableCode.LoggingInAndOut loggingInAndOut = new LoggingInAndOut();
        //        WebdriverAutomation.Framework.Site_Pages.Home home = loggingInAndOut.LogIn_ProfileComplete(driver, log, login, password);
        //        WebdriverAutomation.Framework.Site_Pages.YourProfile profile = home.Header.YourProfileHeaderLink_Click();
        //        Framework.Site_Pages.ManageYourPhotos manageYourPhotos = profile.ManageYourPhotosButton_Click();

        //        // delete all existing photos
        //        while (manageYourPhotos.Photo2PhotoExists())
        //        {
        //            manageYourPhotos.Photo2XButton_Click();
        //            manageYourPhotos.SaveChangesButton_Click();
        //        }

        //        string photoCaption = string.Empty;

        //        // jdate.co.il survey seems to randomly appear somewhere around here.  if things go wrong, get rid of the survey and try again
        //        for (int i = 0; i < 2; i++)
        //        {
        //            try
        //            {
        //                // grab a random picture in our solution's file path and put it in the Upload a Photo textbox
        //                string strFileName = string.Empty;
        //                Random random = new Random();

        //                // instead of grabbing our profile picture from the solution now we are grabbing it from a networked folder both QA and I can access:
        //                //   S:\Quality Assurance\Automation Builds\PicsForAutomation
        //                //FileInfo[] files = HelperMethods.GetReqFileInfo(@"*.jpg");
        //                //if (files.Length > 0)
        //                //    strFileName = files[random.Next(0, files.Length - 1)].FullName;
        //                strFileName = "s:\\Quality Assurance\\Automation Builds\\PicsForAutomation\\ProfilePicture.jpg";

        //                photoCaption = "I look better in this photo";

        //                manageYourPhotos.Photo2UploadAPhotoTextbox_Write(strFileName);
        //                manageYourPhotos.Photo2CaptionTextarea = photoCaption;
        //                manageYourPhotos.SaveChangesButton_Click();

        //                break;
        //            }
        //            catch
        //            {
        //                GetRidOfJdateCoIlSurvey();
        //            }
        //        }

        //        // verify the text, "Profile successfully changed" appears
        //        Assert.True(manageYourPhotos.NotificationText == "הפרופיל עודכן בהצלחה", "ERROR - MANAGE YOUR PHOTOS PAGE - After Uploading a photo and pressing the Save Changes button, the 'Profile successfuly changed' text did not appear");

        //        // jdate.co.il survey seems to randomly appear somewhere around here.  if things go wrong, get rid of the survey and try again
        //        for (int i = 0; i < 2; i++)
        //        {
        //            try
        //            {
        //                // Delete and then Undelete the newly uploaded photo
        //                manageYourPhotos.Photo2XButton_Click();
        //                Assert.True(manageYourPhotos.Photo2Notification.Contains("תמונה 2 וההערה יימחקו ברגע הלחיצה על כפתור"), "ERROR - MANAGE YOUR PHOTOS PAGE - After Uploading a photo and pressing the Delete link for Photo 2, the 'Photo 2 and its caption wil be deleted...' text did not appear");
        //                manageYourPhotos.Photo2UndeleteLink_Click();
        //                manageYourPhotos.SaveChangesButton_Click();

        //                break;
        //            }
        //            catch
        //            {
        //                GetRidOfJdateCoIlSurvey();
        //            }
        //        }

        //        Assert.True(manageYourPhotos.Photo2PhotoExists(), "ERROR - MANAGE YOUR PHOTOS PAGE - After Uploading a photo and pressing the Save Changes button, we deleted the photo then immediately undeleted it and pressed Save again \r\nThe photo that was uploaded and then undeleted is not found");
        //        Assert.True(manageYourPhotos.Photo2CaptionTextarea == photoCaption, "ERROR - MANAGE YOUR PHOTOS PAGE - After Uploading a photo and pressing the Save Changes button, we deleted the photo then immediately undeleted it and pressed Save again \r\nThe caption that was uploaded previously is not the same. Previous caption value: " + photoCaption + ". The caption value now: " + manageYourPhotos.Photo1CaptionTextarea);

        //        // go to the Home Page then back to Manage Your Photos and verify that a photo still exists (the Upload a New Photo image does not) 
        //        //   and that the caption is still there
        //        home = manageYourPhotos.Header.HomeHeaderLink_Click();
        //        profile = home.Header.YourProfileHeaderLink_Click();
        //        manageYourPhotos = profile.ManageYourPhotosButton_Click();

        //        Assert.True(manageYourPhotos.Photo2PhotoExists(), "ERROR - MANAGE YOUR PHOTOS PAGE - After Uploading a photo and pressing the Save Changes button, we navigated back to the Home page and then back to the Manage Your Photos page. The photo that was uploaded previously cannot be found");
        //        Assert.True(manageYourPhotos.Photo2CaptionTextarea == photoCaption, "ERROR - MANAGE YOUR PHOTOS PAGE - After Uploading a photo and pressing the Save Changes button, we navigated back to the Home page and then back to the Manage Your Photos page. The caption that was uploaded previously is not the same. Previous caption value: " + photoCaption + ". The caption value now: " + manageYourPhotos.Photo1CaptionTextarea);

        //        // jdate.co.il survey seems to randomly appear somewhere around here.  if things go wrong, get rid of the survey and try again
        //        for (int i = 0; i < 2; i++)
        //        {
        //            try
        //            {
        //                // Reinitialize: Delete the photo we just added and verify that it really gets deleted
        //                manageYourPhotos.Photo2XButton_Click();
        //                manageYourPhotos.SaveChangesButton_Click();

        //                break;
        //            }
        //            catch
        //            {
        //                GetRidOfJdateCoIlSurvey();
        //            }
        //        }

        //        Assert.IsFalse(manageYourPhotos.Photo2PhotoExists(), "ERROR - MANAGE YOUR PHOTOS PAGE - After deleting a photo and pressing the Save Changes button, the photo was not deleted");

        //        manageYourPhotos.Header.Logout_Click();

        //        TearDown();

        //        log.Info("END TEST - JDate_Co_Il/Regression/Member/UploadAndEditPhotosn");
        //    }
        //    catch (Exception e)
        //    {
        //        CleanupFailedTestcase(e);
        //        throw;
        //    }
        //}

        // For JDate.co.il we currently don't check that the results are located x miles away from where we searched.
        [Test]
        public void Validate_QuickSearchResults()
        {
            try
            {
                log.Info("START TEST - JDate_Co_Il/Regression/Member/QuickSearchResults");

                Setup();

                StartTest_UsingJDate_Co_Il();

                string login = System.Configuration.ConfigurationManager.AppSettings["JDate_co_il_RegisteredUser3_Email"];
                string password = System.Configuration.ConfigurationManager.AppSettings["JDate_co_il_RegisteredUser3_Password"];

                WebdriverAutomation.Tests.ReusableCode.LoggingInAndOut loggingInAndOut = new LoggingInAndOut();
                WebdriverAutomation.Framework.Site_Pages.Home home = loggingInAndOut.LogIn_ProfileComplete(driver, log, login, password);
                Framework.Site_Pages.QuickSearch quickSearch = home.Header.SearchHeaderLink_Click();

                string youreA = "גבר";
                string seekingA = "אשה";
                string ageFrom = "20";
                string ageTo = "30";
                string locatedWithin = "8 ק\"מ";
                //string country = "ארצות הברית";
                //string zipCode = "94133";

                //// add all cities located within 5 miles of 94133 (San Francisco)
                //List<string> cities = new List<string>();
                //cities.Add("Alameda");
                //cities.Add("Albany");
                //cities.Add("Berkeley");
                //cities.Add("Broadmoor Vlg");
                //cities.Add("Daly City");
                //cities.Add("El Cerrito");
                //cities.Add("Emeryville");
                //cities.Add("Mill Valley");
                //cities.Add("Oakland");
                //cities.Add("Piedmont");
                //cities.Add("Pt Richmond");
                //cities.Add("Richmond");
                //cities.Add("S San Fran");
                //cities.Add("San Francisco");
                //cities.Add("Sausalito");
                //cities.Add("South San Francisco");
                //cities.Add("Tiburon");
                //cities.Add("UC Berkeley");

                quickSearch.YoureADropdown = youreA;
                quickSearch.SeekingADropdown = seekingA;
                quickSearch.AgeFromTextbox = ageFrom;
                quickSearch.AgeToTextbox = ageTo;
                quickSearch.LocatedWithinDropdown = locatedWithin;
                //quickSearch.EditLink_Click();
                //quickSearch.EditOverlay_ZipCodeTab_Click();
                //quickSearch.EditOverlay_ZipCodeTab_CountryDropdown = country;
                //quickSearch.EditOverlay_ZipCodeTab_ZipCodeTextbox = zipCode;
                //quickSearch.EditOverlay_SaveButton_Click();
                quickSearch.SearchButton_Click();

                // jdate.co.il survey seems to randomly appear somewhere around here.  if things go wrong, get rid of the survey and try again
                for (int i = 0; i < 2; i++)
                {
                    try
                    {
                        quickSearch.GalleryLink_Click();

                        // GALLERY VIEW - verify that quick search results appear
                        Assert.True(quickSearch.SearchResultsCount_GalleryView() > 0, "ERROR - QUICK SEARCH PAGE - After doing a quick search in Gallery View, no results appeared.  \r\nThe search was for " + youreA + " seeking a " + seekingA + " searching within " + locatedWithin + " of our search");

                        // GALLERY VIEW - verify that the results are all within the correct age range
                        Assert.True(quickSearch.SearchResultsWithinAgeRange_GalleryView(Convert.ToInt32(ageFrom), Convert.ToInt32(ageTo)), "ERROR - QUICK SEARCH PAGE - After doing a quick search in Gallery View, the ages of the search results were not in the correct range.  \r\nThe search was for ages " + ageFrom + " to " + ageTo);

                        //// GALLERY VIEW - verify that the results are all within the correct zip code (checking for 5 miles within 94133 - San Francisco)
                        //string listOfCities = cities[0];

                        //for (int i = 1; i < cities.Count; i++)
                        //    listOfCities = listOfCities + ", " + cities[i];

                        //Assert.True(quickSearch.SearchResultsWithinExpectedCities_GalleryView(cities), "ERROR - QUICK SEARCH PAGE - After doing a quick search in Gallery View, the location range of the search result was not correct.  \r\nThe search was for " + locatedWithin + " within " + zipCode + " and the search results should have all returned one of the following cities: " + listOfCities);

                        break;
                    }
                    catch
                    {
                        GetRidOfJdateCoIlSurvey();
                    }
                }

                // jdate.co.il survey seems to randomly appear somewhere around here.  if things go wrong, get rid of the survey and try again
                for (int i = 0; i < 2; i++)
                {
                    try
                    {
                        quickSearch.ListLink_Click();

                        // LIST VIEW - verify that quick search results appear
                        Assert.True(quickSearch.SearchResultsCount_ListView() > 0, "ERROR - QUICK SEARCH PAGE - After doing a quick search in List View, no results appeared.  \r\nThe search was for " + youreA + " seeking a " + seekingA + " searching within " + locatedWithin + " of our search");

                        // LIST VIEW - verify that the results are all within the correct age range
                        Assert.True(quickSearch.SearchResultsWithinAgeRange_ListView(Convert.ToInt32(ageFrom), Convert.ToInt32(ageTo)), "ERROR - QUICK SEARCH PAGE - After doing a quick search in List View, the ages of the search results were not in the correct range.");

                        //// LIST VIEW - verify that the results are all within the correct zip code (checking for 5 miles within 94133 - San Francisco)
                        //Assert.True(quickSearch.SearchResultsWithinExpectedCities_ListView(cities), "ERROR - QUICK SEARCH PAGE - After doing a quick search in List View, the location range of the search result was not correct.  \r\nThe search was for " + locatedWithin + " within " + zipCode + " and the search results should have all returned one of the following cities: " + cities[0] + ", " + cities[1] + ", " + cities[2] + ", " + cities[3]);

                        // LIST VIEW - verify that the results show the correct gender seeking the correct gender
                        Assert.True(quickSearch.SearchResultsIAmASeekingA_ListView(youreA, seekingA), "ERROR - QUICK SEARCH PAGE - After doing a quick search in List View, the gender preference was not correct.  \r\nThe search should have returned matches for " + seekingA + " seeking a " + youreA);

                        break;
                    }
                    catch
                    {
                        GetRidOfJdateCoIlSurvey();
                    }
                }

                TearDown();

                log.Info("END TEST - JDate_Co_Il/Regression/Member/QuickSearchResults");
            }
            catch (Exception e)
            {
                CleanupFailedTestcase(e);
                throw;
            }
        }

        /// <summary>
        /// In order to verify pagination we need to ensure that our search results have enough results to span more than one page (12 results).
        /// Using a search of Located Within 160 miles of 90210 (Beverly Hills) for this test case.
        /// </summary>
        [Test]
        public void Validate_QuickSearchPagination()
        {
            try
            {
                log.Info("START TEST - JDate_Co_Il/Regression/Member/QuickSearchPagination");

                Setup();

                StartTest_UsingJDate_Co_Il();

                string login = System.Configuration.ConfigurationManager.AppSettings["JDate_co_il_RegisteredUser3_Email"];
                string password = System.Configuration.ConfigurationManager.AppSettings["JDate_co_il_RegisteredUser3_Password"];

                WebdriverAutomation.Tests.ReusableCode.LoggingInAndOut loggingInAndOut = new LoggingInAndOut();
                WebdriverAutomation.Framework.Site_Pages.Home home = loggingInAndOut.LogIn_ProfileComplete(driver, log, login, password);
                Framework.Site_Pages.QuickSearch quickSearch = home.Header.SearchHeaderLink_Click();

                string youreA = "גבר";
                string seekingA = "אשה";
                string ageFrom = "20";
                string ageTo = "30";
                string locatedWithin = "8 ק\"מ";
                //string country = "ארצות הברית";
                //string zipCode = "94133";

                quickSearch.YoureADropdown = youreA;
                quickSearch.SeekingADropdown = seekingA;
                quickSearch.AgeFromTextbox = ageFrom;
                quickSearch.AgeToTextbox = ageTo;
                quickSearch.LocatedWithinDropdown = locatedWithin;
                //quickSearch.EditLink_Click();
                //quickSearch.EditOverlay_ZipCodeTab_Click();
                //quickSearch.EditOverlay_ZipCodeTab_CountryDropdown = country;
                //quickSearch.EditOverlay_ZipCodeTab_ZipCodeTextbox = zipCode;
                //quickSearch.EditOverlay_SaveButton_Click();
                quickSearch.SearchButton_Click();

                quickSearch.GalleryLink_Click();

                // save all the usernames from our search in a list
                List<string> userNames1Through12 = quickSearch.GetAllNamesOfSearchResultsOnThisPage();
                Assert.IsFalse(userNames1Through12.Count < 12, "ERROR - QUICK SEARCH PAGE - We cannot test pagination because we are not getting enough results back in our quicksearch \r\nWe are currently searching for a " + seekingA + " between the ages of " + ageFrom + " to " + ageTo + ".\r\nThe number of search results returned:  " + userNames1Through12.Count);

                // go to the next page - users 13-24 and save those names in another list
                quickSearch = quickSearch.Pagination_Page2_Click();
                List<string> userNames13Through24 = quickSearch.GetAllNamesOfSearchResultsOnThisPage();

                // verify that none of the user names match - we truly have reached a new page
                bool doAnyNamesFromPage1MatchPage2 = quickSearch.DoAnyUserNamesMatchInOurLists(userNames1Through12, userNames13Through24);
                Assert.IsFalse(doAnyNamesFromPage1MatchPage2, "ERROR - QUICK SEARCH PAGE - After paging through to the 2nd page of results (13-24) a user from page 1 was found in page 2");

                // verify Previous link
                quickSearch = quickSearch.Pagination_PreviousLink_Click();
                List<string> userNamesOnCurrentPage = quickSearch.GetAllNamesOfSearchResultsOnThisPage();

                bool doALLNamesFromPage1MatchPage2 = quickSearch.DoAllUserNamesMatchInOurLists(userNames1Through12, userNamesOnCurrentPage);
                Assert.IsTrue(doALLNamesFromPage1MatchPage2, "ERROR - QUICK SEARCH PAGE - After navigating to page 2 (users 13-24) and clicking the Previous link the usernames we saved from page 1 do not match");

                // verify Next link
                quickSearch = quickSearch.Pagination_NextLink_Click();
                userNamesOnCurrentPage = quickSearch.GetAllNamesOfSearchResultsOnThisPage();

                doALLNamesFromPage1MatchPage2 = quickSearch.DoAllUserNamesMatchInOurLists(userNames13Through24, userNamesOnCurrentPage);
                Assert.IsTrue(doALLNamesFromPage1MatchPage2, "ERROR - QUICK SEARCH PAGE - After navigating to page 1 (users 1-12) and clicking the Next link the usernames we saved from page 2 do not match");

                // verify |< link
                quickSearch = quickSearch.Pagination_Page3_Click();
                quickSearch = quickSearch.Pagination_FirstPageLink_Click();
                userNamesOnCurrentPage = quickSearch.GetAllNamesOfSearchResultsOnThisPage();

                doALLNamesFromPage1MatchPage2 = quickSearch.DoAllUserNamesMatchInOurLists(userNames1Through12, userNamesOnCurrentPage);
                Assert.IsTrue(doALLNamesFromPage1MatchPage2, "ERROR - QUICK SEARCH PAGE - After navigating to page 3 (users 25-36) and clicking the |< link the usernames we saved from page 1 do not match");

                TearDown();

                log.Info("END TEST - JDate_Co_Il/Regression/Member/QuickSearchPagination");
            }
            catch (Exception e)
            {
                CleanupFailedTestcase(e);
                throw;
            }
        }

        [Test]
        public void Validate_OmnitureExistsInRegPage_Version1_OnePage()
        {
            string errorLogNotes = string.Empty;

            try
            {
                log.Info("START TEST - JDate_Co_Il/Regression/Member/OmnitureExistsInRegPage_Version1_OnePage");

                Setup();

                StartTest_UsingJDate_Co_Il();

                WebdriverAutomation.Framework.Site_Pages.Splash splash = new WebdriverAutomation.Framework.Site_Pages.Splash(driver);

                string omnitureSearchQuery1 = "s.prop";
                string omnitureSearchQuery2 = "s.eVar";

                // Jan 2013 - omniture variables do not appear on the Splash Page anymore
                //if (!driver.PageSource.Contains(omnitureSearchQuery1) || !driver.PageSource.Contains(omnitureSearchQuery2))
                //    Assert.Fail("In the Landing Page we are unable to find any omniture variables beginning with 's.prop' or 's.eVar' within the page source.");

                // JDate.co.il Registration - I am a, Date of Birth, Religious Background, Country, City, Email, Password, Captcha, I would like to recieve special updates...,
                //    Terms of Service appear on splash page
                splash.YouAreAList_Select(1);

                splash.BirthDateYearList_Select(20);
                splash.BirthDateMonthList_Select(2);
                splash.BirthDateDayList_Select(5);

                splash.YourReligiousBackground_Select(3);

                //splash.Country_Select(2);

                splash.CityTextbox_Write("ירושלים");
                //splash.CityListBox_Select("ירושלים");

                string emailAddress = "QAAutomation_" + DateTime.Now.ToString() + "@gmail.com";
                emailAddress = emailAddress.Replace("/", "_").Replace(" ", "_").Replace(":", "_");
                splash.EmailAddressTextbox_Write(emailAddress);

                splash.ChooseYourPasswordTextbox_Write("automation");

                //if (splash.PleaseEnterTheCodeShownBelowTextboxExists())
                //    splash.PleaseEnterTheCodeShownBelowTextbox_Write("bananafish");

                splash.IWouldLikeSpecialOffersAndAnnouncements_Click();

                WebdriverAutomation.Framework.Site_Pages.RegistrationComplete registrationComplete = splash.ContinueButton_Click_RegistrationComplete();

                if (!driver.PageSource.Contains(omnitureSearchQuery1) || !driver.PageSource.Contains(omnitureSearchQuery2))
                    Assert.Fail("In the Registration Page we are unable to find any omniture variables beginning with 's.prop' or 's.eVar' within the page source.");

                TearDown();

                log.Info("END TEST - JDate_Co_Il/Regression/Member/OmnitureExistsInRegPage_Version1_OnePage");
            }
            catch (Exception e)
            {
                CleanupFailedTestcase(e, errorLogNotes);
                throw;
            }
        }

        private int AgeOfUser(string birthdayMonth, string birthdayDay, string birthdayYear)
        {
            string myDateTimeString = string.Empty;

            if (driver.Url.Contains("jdate.co.il"))
                birthdayMonth = ConvertMonthStringFromHebrewToEnglish(birthdayMonth);

            myDateTimeString = birthdayDay + " " + birthdayMonth + "," + birthdayYear;

            DateTime birthday;
            birthday = Convert.ToDateTime(myDateTimeString);

            DateTime now = DateTime.Today;
            int age = now.Year - birthday.Year;
            if (birthday > now.AddYears(-age))
                age--;

            return age;
        }

        private string ConvertMonthStringFromHebrewToEnglish(string birthdayMonth)
        {
            string convertedMonth = string.Empty;

            switch (birthdayMonth)
            {
                case "ינואר":
                    convertedMonth = "Jan";
                    break;
                case "פברואר":
                    convertedMonth = "Feb";
                    break;
                case "מרץ":
                    convertedMonth = "Mar";
                    break;
                case "אפריל":
                    convertedMonth = "Apr";
                    break;
                case "מאי":
                    convertedMonth = "May";
                    break;
                case "יוני":
                    convertedMonth = "Jun";
                    break;
                case "יולי":
                    convertedMonth = "Jul";
                    break;
                case "אוגוסט":
                    convertedMonth = "Aug";
                    break;
                case "ספטמבר":
                    convertedMonth = "Sep";
                    break;
                case "אוקטובר":
                    convertedMonth = "Oct";
                    break;
                case "נובמבר":
                    convertedMonth = "Nov";
                    break;
                case "דצמבר":
                    convertedMonth = "Dec";
                    break;
                default:
                    Assert.Fail("Cannot convert this month from Hebrew to English: " + birthdayMonth);
                    break;
            }

            return convertedMonth;
        }

        private string RemoveAllWhitespacesFromString(string originalString)
        {
            return Regex.Replace(originalString, @"\s", "");
        }

        private bool AreValuesAndCountEqual(List<string> list1, List<string> list2)
        {
            if (list1.Count != list2.Count)
                return false;

            list1.Sort();
            list2.Sort();

            for (int i = 0; i < list1.Count; i++)
            {
                if (list1[i] != list2[i])
                    return false;
            }

            return true;
        }
    }
}
