﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.Events;
using NUnit.Framework;
using log4net;
using WebdriverAutomation.Tests.ReusableCode;

namespace WebdriverAutomation.Tests.Black.Regression
{
    [TestFixture]
    public class Member : BaseTest
    {

        [Test]
        public void Validate_Registration()
        {
            try
            {
                log.Info("START TEST - Black/Regression/Member/Registration");

                Setup();

                StartTest_UsingBlack_WithDummyPRM();

                RegisterNewUser();

                //Remove the user we just created
                WebdriverAutomation.Tests.ReusableCode.AccountSettings accountSettings = new AccountSettings();
                accountSettings.RemoveProfile_SkippingUpdateProfileAndImportantNoticePages(driver, log);

                TearDown();

                log.Info("END TEST - Black/Regression/Member/Registration");
            }
            catch (Exception e)
            {
                CleanupFailedTestcase(e);
                throw;
            }
        }

        [Test]
        public void Validate_EditProfile_InMyOwnWordsTab()
        {
            try
            {
                log.Info("START TEST - Black/Regression/Member/EditProfile_InMyOwnWordsTab");

                Setup();

                StartTest_UsingBlack();

                string login = System.Configuration.ConfigurationManager.AppSettings["Black_RegisteredUser1_Email"];
                string password = System.Configuration.ConfigurationManager.AppSettings["Black_RegisteredUser1_Password"];

                WebdriverAutomation.Tests.ReusableCode.LoggingInAndOut loggingInAndOut = new LoggingInAndOut();
                WebdriverAutomation.Framework.Site_Pages.Home home = loggingInAndOut.LogIn_ProfileComplete(driver, log, login, password);
                WebdriverAutomation.Framework.Site_Pages.YourProfile profile = home.Header.YourProfileHeaderLink_Click();

                string new_MyIntroduction = "This is a test account operated by an employee of Spark Networks. Testing changes.";
                string new_WhatWouldILikeToDoOnAFirstDate = "Dinner and dancing.";
                string new_WhyYouShouldGetToKnowMe = "Because I am really really really good looking.";
                string new_WhatGoodThingsHavePastRelationshipsTaughtMe = "Don't cheat.";
                string new_SomeOfTheMostImportantThingsInMyLifeAre = "Good hot sauce and cold beer.";
                string new_HowWouldIDescribeMyPerfectDay = "Any day where I am traveling and not at home.";
                string new_AFewMoreThingsIWouldLikeToAdd = "I like turtles.";

                // Overwrite new values to the page - the hoverclick is extremely fickle here especially with My Introduction so reload Profile Page if it doesn't work
                int loopMax = 5;

                for (int i = 0; i < 5; i++)
                {
                    try
                    {
                        profile.MyIntroduction = new_MyIntroduction;
                        profile.WhatWouldILikeToDoOnAFirstDate = new_WhatWouldILikeToDoOnAFirstDate;
                        profile.WhyYouShouldGetToKnowMe = new_WhyYouShouldGetToKnowMe;
                        profile.WhatGoodThingsHavePastRelationshipsTaughtMe = new_WhatGoodThingsHavePastRelationshipsTaughtMe;
                        profile.SomeOfTheMostImportantThingsInMyLifeAre = new_SomeOfTheMostImportantThingsInMyLifeAre;
                        profile.HowWouldIDescribeMyPerfectDay = new_HowWouldIDescribeMyPerfectDay;
                        profile.AFewMoreThingsIWouldLikeToAdd = new_AFewMoreThingsIWouldLikeToAdd;

                        break;
                    }
                    catch
                    {
                        profile = profile.Header.YourProfileHeaderLink_Click();
                    }

                    if (i == loopMax - 1)
                        Assert.Fail("Errors are occurring while trying to hover over certain areas of the In My Own Words section of the Profile Page to change some values.  We failed after trying " + loopMax + " times unsuccessfully.");
                }

                // Go to the Home Page and then come back to the Profile Page to verify the values saved correctly
                home = profile.Header.HomeHeaderLink_Click();
                profile = home.Header.YourProfileHeaderLink_Click();

                string stringToVerifyOnProfilePage = string.Empty;

                stringToVerifyOnProfilePage = profile.MyIntroduction;
                Assert.IsTrue(new_MyIntroduction == stringToVerifyOnProfilePage, "In the Validate_EditProfile_InMyOwnWordsTab test case, after saving new values for 'My introduction' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_MyIntroduction + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.WhatWouldILikeToDoOnAFirstDate;
                Assert.IsTrue(new_WhatWouldILikeToDoOnAFirstDate == stringToVerifyOnProfilePage, "In the Validate_EditProfile_InMyOwnWordsTab test case, after saving new values for 'What would I like to do on a first date?' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_WhatWouldILikeToDoOnAFirstDate + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.WhyYouShouldGetToKnowMe;
                Assert.IsTrue(new_WhyYouShouldGetToKnowMe == stringToVerifyOnProfilePage, "In the Validate_EditProfile_InMyOwnWordsTab test case, after saving new values for 'Why you should get to know me' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_WhyYouShouldGetToKnowMe + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.WhatGoodThingsHavePastRelationshipsTaughtMe;
                Assert.IsTrue(new_WhatGoodThingsHavePastRelationshipsTaughtMe == stringToVerifyOnProfilePage, "In the Validate_EditProfile_InMyOwnWordsTab test case, after saving new values for 'What good things have past relationships taught me?' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_WhatGoodThingsHavePastRelationshipsTaughtMe + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.SomeOfTheMostImportantThingsInMyLifeAre;
                Assert.IsTrue(new_SomeOfTheMostImportantThingsInMyLifeAre == stringToVerifyOnProfilePage, "In the Validate_EditProfile_InMyOwnWordsTab test case, after saving new values for 'Some of the most important things in my life are' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_SomeOfTheMostImportantThingsInMyLifeAre + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.HowWouldIDescribeMyPerfectDay;
                Assert.IsTrue(new_HowWouldIDescribeMyPerfectDay == stringToVerifyOnProfilePage, "In the Validate_EditProfile_InMyOwnWordsTab test case, after saving new values for 'How would I describe my perfect day?' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_HowWouldIDescribeMyPerfectDay + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.AFewMoreThingsIWouldLikeToAdd;
                Assert.IsTrue(new_AFewMoreThingsIWouldLikeToAdd == stringToVerifyOnProfilePage, "In the Validate_EditProfile_InMyOwnWordsTab test case, after saving new values for 'A few more things I would like to add' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_AFewMoreThingsIWouldLikeToAdd + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                // reinitialize to original values
                for (int i = 0; i < 5; i++)
                {
                    try
                    {
                        profile.MyIntroduction = "This is a test account operated by an employee of Spark Networks.";
                        profile.WhatWouldILikeToDoOnAFirstDate = "Hit up a bar and drink until they kick us out.";
                        profile.WhyYouShouldGetToKnowMe = "Because I'm rich.";
                        profile.WhatGoodThingsHavePastRelationshipsTaughtMe = "Don't lie.";
                        profile.SomeOfTheMostImportantThingsInMyLifeAre = "A good subwoofer in the trunk.";
                        profile.HowWouldIDescribeMyPerfectDay = "Staying in bed all day doing absolutely nothing.";
                        profile.AFewMoreThingsIWouldLikeToAdd = "I am awesome.";

                        break;
                    }
                    catch
                    {
                        profile = profile.Header.YourProfileHeaderLink_Click();
                    }

                    if (i == loopMax - 1)
                        Assert.Fail("Errors are occurring while trying to hover over certain areas of the In My Own Words section of the Profile Page to change some values.  We failed after trying " + loopMax + " times unsuccessfully.");
                }

                profile.Header.Logout_Click();

                TearDown();

                log.Info("END TEST - Black/Regression/Member/EditProfile_InMyOwnWordsTab");
            }
            catch (Exception e)
            {
                CleanupFailedTestcase(e);
                throw;
            }
        }

        [Test]
        public void Validate_EditProfile_MoreAboutMeTab()
        {
            try
            {
                log.Info("START TEST - Black/Regression/Member/EditProfile_MoreAboutMeTab");

                Setup();

                StartTest_UsingBlack();

                string login = System.Configuration.ConfigurationManager.AppSettings["Black_RegisteredUser1_Email"];
                string password = System.Configuration.ConfigurationManager.AppSettings["Black_RegisteredUser1_Password"];

                WebdriverAutomation.Tests.ReusableCode.LoggingInAndOut loggingInAndOut = new LoggingInAndOut();
                WebdriverAutomation.Framework.Site_Pages.Home home = loggingInAndOut.LogIn_ProfileComplete(driver, log, login, password);
                WebdriverAutomation.Framework.Site_Pages.YourProfile profile = home.Header.YourProfileHeaderLink_Click();

                List<string> new_MyFavoriteMusic = new List<string>();
                new_MyFavoriteMusic.Add("Punk");
                new_MyFavoriteMusic.Add("Hard Rock & Metal");

                string new_FavoriteBandsAndMusicians = "nine inch nails";

                List<string> new_Movie = new List<string>();
                new_Movie.Add("Documentary");
                new_Movie.Add("Drama");

                string new_FavoriteMoviesAndActors = "The City of Lost Children";
                string new_FavoriteTVShows = "The Outer Limits";

                List<string> new_MyFavoritePhysicalActivites = new List<string>();
                new_MyFavoritePhysicalActivites.Add("Aerobics");
                new_MyFavoritePhysicalActivites.Add("Martial Arts");

                List<string> new_InMyFreeTimeIEnjoy = new List<string>();
                new_InMyFreeTimeIEnjoy.Add("Gambling");
                new_InMyFreeTimeIEnjoy.Add("Partying");

                string new_MyIdeaOfAGreatTrip = "Hiking at Joshua Tree";

                List<string> new_MyFavoriteFoods = new List<string>();
                new_MyFavoriteFoods.Add("Continental");
                new_MyFavoriteFoods.Add("Seafood");

                string new_FavoriteRestaurants = "Roscoe's";
                string new_SchoolsAttended = "Can't remember";
                string new_WhenGoingSomewhere = "I usually forget to show up";
                string new_AsForFashion = "I am a very trendy person";

                profile.MoreAboutMeTab_Click();

                // Overwrite new values to the page - the hoverclick is extremely fickle here especially with My Introduction so reload Profile Page if it doesn't work
                int loopMax = 5;

                for (int i = 0; i < 5; i++)
                {
                    try
                    {
                        profile.MyFavoriteMusic = new_MyFavoriteMusic;
                        profile.FavoriteBandsAndMusicians = new_FavoriteBandsAndMusicians;
                        profile.Movie = new_Movie;
                        profile.FavoriteMoviesAndActors = new_FavoriteMoviesAndActors;
                        profile.FavoriteTVShows = new_FavoriteTVShows;
                        profile.MyFavoritePhysicalActivities = new_MyFavoritePhysicalActivites;
                        profile.InMyFreeTimeIEnjoy = new_InMyFreeTimeIEnjoy;
                        profile.MyIdeaOfAGreatTrip = new_MyIdeaOfAGreatTrip;
                        profile.MyFavoriteFoods = new_MyFavoriteFoods;
                        profile.FavoriteRestaurants = new_FavoriteRestaurants;
                        profile.SchoolsAttended = new_SchoolsAttended;
                        profile.WhenGoingSomeWhere = new_WhenGoingSomewhere;
                        profile.AsForFashion = new_AsForFashion;

                        break;
                    }
                    catch
                    {
                        profile = profile.Header.YourProfileHeaderLink_Click();

                        profile.MoreAboutMeTab_Click();
                    }

                    if (i == loopMax - 1)
                        Assert.Fail("Errors are occurring while trying to hover over certain areas of the More About Me section of the Profile Page to change some values.  We failed after trying " + loopMax + " times unsuccessfully.");
                }

                // Go to the Home Page and then come back to the Profile Page to verify the values saved correctly
                home = profile.Header.HomeHeaderLink_Click();
                profile = home.Header.YourProfileHeaderLink_Click();

                profile.MoreAboutMeTab_Click();

                string stringToVerifyOnProfilePage = string.Empty;
                List<string> listToVerifyOnProfilePage = new List<string>();

                listToVerifyOnProfilePage = profile.MyFavoriteMusic;
                new_MyFavoriteMusic.Sort();
                Assert.IsTrue(AreValuesAndCountEqual(new_MyFavoriteMusic, listToVerifyOnProfilePage), "In the Validate_EditProfile_MoreAboutMeTab test case, after saving new values for 'My favorite music' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + string.Join(", ", new_MyFavoriteMusic.ToArray()) + "' \r\nValue currently in Profile Page: '" + string.Join(", ", listToVerifyOnProfilePage.ToArray()) + "'");

                stringToVerifyOnProfilePage = profile.FavoriteBandsAndMusicians;
                Assert.IsTrue(new_FavoriteBandsAndMusicians == stringToVerifyOnProfilePage, "In the Validate_EditProfile_MoreAboutMeTab test case, after saving new values for 'Favorite bands and musicians' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_MyFavoriteMusic + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                listToVerifyOnProfilePage = profile.Movie;
                new_Movie.Sort();
                Assert.IsTrue(AreValuesAndCountEqual(new_Movie, listToVerifyOnProfilePage), "In the Validate_EditProfile_MoreAboutMeTab test case, after saving new values for 'My Movie' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + string.Join(", ", new_Movie.ToArray()) + "' \r\nValue currently in Profile Page: '" + string.Join(", ", listToVerifyOnProfilePage.ToArray()) + "'");

                stringToVerifyOnProfilePage = profile.FavoriteMoviesAndActors;
                Assert.IsTrue(new_FavoriteMoviesAndActors == stringToVerifyOnProfilePage, "In the Validate_EditProfile_MoreAboutMeTab test case, after saving new values for 'Favorite movies and actors' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_FavoriteMoviesAndActors + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.FavoriteTVShows;
                Assert.IsTrue(new_FavoriteTVShows == stringToVerifyOnProfilePage, "In the Validate_EditProfile_MoreAboutMeTab test case, after saving new values for 'Favorite TV shows' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_FavoriteTVShows + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                listToVerifyOnProfilePage = profile.MyFavoritePhysicalActivities;
                new_MyFavoritePhysicalActivites.Sort();
                Assert.IsTrue(AreValuesAndCountEqual(new_MyFavoritePhysicalActivites, listToVerifyOnProfilePage), "In the Validate_EditProfile_MoreAboutMeTab test case, after saving new values for 'My favorite physical activities' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + string.Join(", ", new_MyFavoritePhysicalActivites.ToArray()) + "' \r\nValue currently in Profile Page: '" + string.Join(", ", listToVerifyOnProfilePage.ToArray()) + "'");

                listToVerifyOnProfilePage = profile.InMyFreeTimeIEnjoy;
                new_InMyFreeTimeIEnjoy.Sort();
                Assert.IsTrue(AreValuesAndCountEqual(new_InMyFreeTimeIEnjoy, listToVerifyOnProfilePage), "In the Validate_EditProfile_MoreAboutMeTab test case, after saving new values for 'In my free time, I enjoy...' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + string.Join(", ", new_InMyFreeTimeIEnjoy.ToArray()) + "' \r\nValue currently in Profile Page: '" + string.Join(", ", listToVerifyOnProfilePage.ToArray()) + "'");

                stringToVerifyOnProfilePage = profile.MyIdeaOfAGreatTrip;
                Assert.IsTrue(new_MyIdeaOfAGreatTrip == stringToVerifyOnProfilePage, "In the Validate_EditProfile_MoreAboutMeTab test case, after saving new values for 'My idea of a great trip' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_MyIdeaOfAGreatTrip + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.FavoriteRestaurants;
                Assert.IsTrue(new_FavoriteRestaurants == stringToVerifyOnProfilePage, "In the Validate_EditProfile_MoreAboutMeTab test case, after saving new values for 'Favorite restaurants' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_FavoriteRestaurants + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                listToVerifyOnProfilePage = profile.MyFavoriteFoods;
                new_MyFavoriteFoods.Sort();
                Assert.IsTrue(AreValuesAndCountEqual(new_MyFavoriteFoods, listToVerifyOnProfilePage), "In the Validate_EditProfile_MoreAboutMeTab test case, after saving new values for 'My favorite foods' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + string.Join(", ", new_MyFavoriteFoods.ToArray()) + "' \r\nValue currently in Profile Page: '" + string.Join(", ", listToVerifyOnProfilePage.ToArray()) + "'");

                stringToVerifyOnProfilePage = profile.SchoolsAttended;
                Assert.IsTrue(new_SchoolsAttended == stringToVerifyOnProfilePage, "In the Validate_EditProfile_MoreAboutMeTab test case, after saving new values for 'Schools attended' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_SchoolsAttended + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.WhenGoingSomeWhere;
                Assert.IsTrue(new_WhenGoingSomewhere == stringToVerifyOnProfilePage, "In the Validate_EditProfile_MoreAboutMeTab test case, after saving new values for 'When going somewhere' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_WhenGoingSomewhere + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.AsForFashion;
                Assert.IsTrue(new_AsForFashion == stringToVerifyOnProfilePage, "In the Validate_EditProfile_MoreAboutMeTab test case, after saving new values for 'As for fashion' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_AsForFashion + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                // reinitialize to original values
                for (int i = 0; i < 5; i++)
                {
                    try
                    {
                        List<string> orig_MyFavoriteMusic = new List<string>();
                        orig_MyFavoriteMusic.Add("Dance/Electronica");
                        orig_MyFavoriteMusic.Add("Soul/R&B");

                        List<string> orig_Movie = new List<string>();
                        orig_Movie.Add("Mystery");
                        orig_Movie.Add("Horror");

                        List<string> orig_MyFavoritePhysicalActivites = new List<string>();
                        orig_MyFavoritePhysicalActivites.Add("Dancing");
                        orig_MyFavoritePhysicalActivites.Add("Snow Skiing");

                        List<string> orig_InMyFreeTimeIEnjoy = new List<string>();
                        orig_InMyFreeTimeIEnjoy.Add("Dining Out");
                        orig_InMyFreeTimeIEnjoy.Add("Shopping");

                        List<string> orig_MyFavoriteFoods = new List<string>();
                        orig_MyFavoriteFoods.Add("Barbecue");
                        orig_MyFavoriteFoods.Add("Soul Food");

                        profile.MyFavoriteMusic = orig_MyFavoriteMusic;
                        profile.FavoriteBandsAndMusicians = "Portishead";
                        profile.Movie = orig_Movie;
                        profile.FavoriteMoviesAndActors = "Nightmare Before Christmas";
                        profile.FavoriteTVShows = "Married With Children";
                        profile.MyFavoritePhysicalActivities = orig_MyFavoritePhysicalActivites;
                        profile.InMyFreeTimeIEnjoy = orig_InMyFreeTimeIEnjoy;
                        profile.MyIdeaOfAGreatTrip = "Europe";
                        profile.MyFavoriteFoods = orig_MyFavoriteFoods;
                        profile.FavoriteRestaurants = "Lawrys";
                        profile.SchoolsAttended = "This and that";
                        profile.WhenGoingSomeWhere = "I am usually a little late";
                        profile.AsForFashion = "I dress to be comfortable";

                        break;
                    }
                    catch
                    {
                        profile = profile.Header.YourProfileHeaderLink_Click();

                        profile.MoreAboutMeTab_Click();
                    }

                    if (i == loopMax - 1)
                        Assert.Fail("Errors are occurring while trying to hover over certain areas of the More About Me section of the Profile Page to change some values.  We failed after trying " + loopMax + " times unsuccessfully.");
                }

                profile.Header.Logout_Click();

                TearDown();

                log.Info("END TEST - Black/Regression/Member/EditProfile_MoreAboutMeTab");
            }
            catch (Exception e)
            {
                CleanupFailedTestcase(e);
                throw;
            }
        }

        [Test]
        public void Validate_EditProfile_DetailsPhysicalInfo()
        {
            try
            {
                log.Info("START TEST - Black/Regression/Member/EditProfile_DetailsPhysicalInfo");

                Setup();

                StartTest_UsingBlack();

                string login = System.Configuration.ConfigurationManager.AppSettings["Black_RegisteredUser1_Email"];
                string password = System.Configuration.ConfigurationManager.AppSettings["Black_RegisteredUser1_Password"];

                WebdriverAutomation.Tests.ReusableCode.LoggingInAndOut loggingInAndOut = new LoggingInAndOut();
                WebdriverAutomation.Framework.Site_Pages.Home home = loggingInAndOut.LogIn_ProfileComplete(driver, log, login, password);
                WebdriverAutomation.Framework.Site_Pages.YourProfile profile = home.Header.YourProfileHeaderLink_Click();

                string new_height = "4'6\"(137 cm)";
                string new_bodyType = "Ripped";
                string new_hairColor = "Red";
                string new_eyeColor = "Green";

                // Overwrite new values to the page
                profile.PhysicalInfo_ClickToEdit();

                profile.Height_InOverlay = new_height;
                profile.BodyType_InOverlay = new_bodyType;
                profile.HairColor_InOverlay = new_hairColor;
                profile.EyeColor_InOverlay = new_eyeColor;

                profile.PhysicalInfo_SaveChanges();

                // Go to the Home Page and then come back to the Profile Page to verify the values saved correctly
                home = profile.Header.HomeHeaderLink_Click();
                profile = home.Header.YourProfileHeaderLink_Click();

                string stringToVerifyOnProfilePage = string.Empty;

                stringToVerifyOnProfilePage = profile.Height;
                Assert.IsTrue(new_height.Replace(" ", "") == stringToVerifyOnProfilePage.Replace(" ", ""), "In the Validate_EditProfile_DetailsPhysicalInfo test case, after saving new values for 'Height' in the the Click to Edit overlay the values were not saved properly in the Profile Page \r\nValue we saved in Click to Edit overlay: '" + new_height + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.BodyType;
                Assert.IsTrue(new_bodyType == stringToVerifyOnProfilePage, "In the Validate_EditProfile_DetailsPhysicalInfo test case, after saving new values for 'Body Type' in the the Click to Edit overlay the values were not saved properly in the Profile Page \r\nValue we saved in Click to Edit overlay: '" + new_bodyType + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.HairColor;
                Assert.IsTrue(new_hairColor == stringToVerifyOnProfilePage, "In the Validate_EditProfile_DetailsPhysicalInfo test case, after saving new values for 'Hair color' in the the Click to Edit overlay the values were not saved properly in the Profile Page \r\nValue we saved in Click to Edit overlay: '" + new_hairColor + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.EyeColor;
                Assert.IsTrue(new_eyeColor == stringToVerifyOnProfilePage, "In the Validate_EditProfile_DetailsPhysicalInfo test case, after saving new values for 'Eye color' in the the Click to Edit overlay the values were not saved properly in the Profile Page \r\nValue we saved in Click to Edit overlay: '" + new_eyeColor + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.Height_Dealbreakers;
                Assert.IsTrue(new_height.Replace(" ", "") == stringToVerifyOnProfilePage.Replace(" ", ""), "In the Validate_EditProfile_Dealbreakers test case, after saving new values for 'Height' the values were not saved properly in the Profile Page in the Dealbreakers section \r\nValue we saved in Edit Profile: '" + new_height + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.BodyType_Dealbreakers;
                Assert.IsTrue(new_bodyType == stringToVerifyOnProfilePage, "In the Validate_EditProfile_Dealbreakers test case, after saving new values for 'Body type' the values were not saved properly in the Profile Page in the Dealbreakers section \r\nValue we saved in Edit Profile: '" + new_bodyType + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                // reinitialize to original values
                profile.PhysicalInfo_ClickToEdit();

                profile.Height_InOverlay = "5'5\"(165 cm)";
                profile.BodyType_InOverlay = "Portly";
                profile.HairColor_InOverlay = "Black";
                profile.EyeColor_InOverlay = "Brown";

                profile.PhysicalInfo_SaveChanges();

                profile.Header.Logout_Click();

                TearDown();

                log.Info("END TEST - Black/Regression/Member/EditProfile_DetailsPhysicalInfo");
            }
            catch (Exception e)
            {
                CleanupFailedTestcase(e);
                throw;
            }
        }

        [Test]
        public void Validate_EditProfile_DetailsLifestyle()
        {
            try
            {
                log.Info("START TEST - Black/Regression/Member/EditProfile_DetailsLifestyle");

                Setup();

                StartTest_UsingBlack();

                string login = System.Configuration.ConfigurationManager.AppSettings["Black_RegisteredUser1_Email"];
                string password = System.Configuration.ConfigurationManager.AppSettings["Black_RegisteredUser1_Password"];

                WebdriverAutomation.Tests.ReusableCode.LoggingInAndOut loggingInAndOut = new LoggingInAndOut();
                WebdriverAutomation.Framework.Site_Pages.Home home = loggingInAndOut.LogIn_ProfileComplete(driver, log, login, password);
                WebdriverAutomation.Framework.Site_Pages.YourProfile profile = home.Header.YourProfileHeaderLink_Click();

                string new_MaritalStatus = "Widowed";
                string new_HaveKids = "3 or More";
                string new_Custody = "Sometimes";
                string new_WantKids = "No";
                string new_Smoking = "Non-Smoker";
                string new_Drinking = "Never";

                // Overwrite new values to the page
                profile.Lifestyle_ClickToEdit();

                profile.MaritalStatus_InOverlay = new_MaritalStatus;
                profile.HaveKids_InOverlay = new_HaveKids;
                profile.Custody_InOverlay = new_Custody;
                profile.WantKids_InOverlay = new_WantKids;
                profile.Smoking_InOverlay = new_Smoking;
                profile.Drinking_InOverlay = new_Drinking;

                profile.Lifestyle_SaveChanges();

                // Go to the Home Page and then come back to the Profile Page to verify the values saved correctly
                home = profile.Header.HomeHeaderLink_Click();
                profile = home.Header.YourProfileHeaderLink_Click();

                string stringToVerifyOnProfilePage = string.Empty;

                stringToVerifyOnProfilePage = profile.MaritalStatus;
                Assert.IsTrue(new_MaritalStatus == stringToVerifyOnProfilePage, "In the Validate_EditProfile_DetailsLifestyle test case, after saving new values for 'Marital Status' in the the Click to Edit overlay the values were not saved properly in the Profile Page \r\nValue we saved in Click to Edit overlay: '" + new_MaritalStatus + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.HaveKids;
                Assert.IsTrue(new_HaveKids == stringToVerifyOnProfilePage, "In the Validate_EditProfile_DetailsLifestyle test case, after saving new values for 'Have kids' in the the Click to Edit overlay the values were not saved properly in the Profile Page \r\nValue we saved in Click to Edit overlay: '" + new_HaveKids + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.Custody;
                Assert.IsTrue(new_Custody == stringToVerifyOnProfilePage, "In the Validate_EditProfile_DetailsLifestyle test case, after saving new values for 'Custody' in the the Click to Edit overlay the values were not saved properly in the Profile Page \r\nValue we saved in Click to Edit overlay: '" + new_Custody + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.WantKids;
                Assert.IsTrue(new_WantKids == stringToVerifyOnProfilePage, "In the Validate_EditProfile_DetailsLifestyle test case, after saving new values for 'Wants kids' in the the Click to Edit overlay the values were not saved properly in the Profile Page \r\nValue we saved in Click to Edit overlay: '" + new_WantKids + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.Smoking;
                Assert.IsTrue(new_Smoking == stringToVerifyOnProfilePage, "In the Validate_EditProfile_DetailsLifestyle test case, after saving new values for 'Smoking' in the the Click to Edit overlay the values were not saved properly in the Profile Page \r\nValue we saved in Click to Edit overlay: '" + new_Smoking + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.Drinking;
                Assert.IsTrue(new_Drinking == stringToVerifyOnProfilePage, "In the Validate_EditProfile_DetailsLifestyle test case, after saving new values for 'Drinking' in the the Click to Edit overlay the values were not saved properly in the Profile Page \r\nValue we saved in Click to Edit overlay: '" + new_MaritalStatus + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.Smoking_Dealbreakers;
                Assert.IsTrue(new_Smoking == stringToVerifyOnProfilePage, "In the Validate_EditProfile_Dealbreakers test case, after saving new values for 'Smoking' the values were not saved properly in the Profile Page in the Dealbreakers section \r\nValue we saved in Edit Profile: '" + new_Smoking + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.Drinking_Dealbreakers;
                Assert.IsTrue(new_Drinking == stringToVerifyOnProfilePage, "In the Validate_EditProfile_Dealbreakers test case, after saving new values for 'Drinking' the values were not saved properly in the Profile Page in the Dealbreakers section \r\nValue we saved in Edit Profile: '" + new_Drinking + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.HaveKids_Dealbreakers;
                Assert.IsTrue(new_HaveKids == stringToVerifyOnProfilePage, "In the Validate_EditProfile_Dealbreakers test case, after saving new values for 'Have kids' the values were not saved properly in the Profile Page in the Dealbreakers section \r\nValue we saved in Edit Profile: '" + new_HaveKids + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.WantsKids_Dealbreakers;
                Assert.IsTrue(new_WantKids == stringToVerifyOnProfilePage, "In the Validate_EditProfile_Dealbreakers test case, after saving new values for 'Wants kids' the values were not saved properly in the Profile Page in the Dealbreakers section \r\nValue we saved in Edit Profile: '" + new_WantKids + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                // reinitialize to original values
                profile.Lifestyle_ClickToEdit();

                profile.MaritalStatus_InOverlay = "Single";
                profile.HaveKids_InOverlay = "None";
                profile.Custody_InOverlay = "I have no kids";
                profile.WantKids_InOverlay = "Not sure";
                profile.Smoking_InOverlay = "Occasional Smoker";
                profile.Drinking_InOverlay = "Frequently";

                profile.Lifestyle_SaveChanges();

                profile.Header.Logout_Click();

                TearDown();

                log.Info("END TEST - Black/Regression/Member/EditProfile_DetailsLifestyle");
            }
            catch (Exception e)
            {
                CleanupFailedTestcase(e);
                throw;
            }
        }

        [Test]
        public void Validate_EditProfile_DetailsBackground()
        {
            try
            {
                log.Info("START TEST - Black/Regression/Member/EditProfile_DetailsBackground");

                Setup();

                StartTest_UsingBlack();

                string login = System.Configuration.ConfigurationManager.AppSettings["Black_RegisteredUser1_Email"];
                string password = System.Configuration.ConfigurationManager.AppSettings["Black_RegisteredUser1_Password"];

                WebdriverAutomation.Tests.ReusableCode.LoggingInAndOut loggingInAndOut = new LoggingInAndOut();
                WebdriverAutomation.Framework.Site_Pages.Home home = loggingInAndOut.LogIn_ProfileComplete(driver, log, login, password);
                WebdriverAutomation.Framework.Site_Pages.YourProfile profile = home.Header.YourProfileHeaderLink_Click();

                string new_MyEthnicityIs = "Hispanic";

                List<string> new_Languages = new List<string>();
                new_Languages.Add("Czech");
                new_Languages.Add("Tagalog");

                string new_Religion = "Hindu";
                string new_Education = "Elementary";
                string new_WhatIDo = "Student";
                string new_Politics = "Right Wing";

                // Overwrite new values to the page
                profile.Background_ClickToEdit();

                profile.Ethnicity_InOverlay = new_MyEthnicityIs;
                profile.Languages_InOverlay = new_Languages;
                profile.Religion_InOverlay = new_Religion;
                profile.Education_InOverlay = new_Education;
                profile.WhatIDo_InOverlay = new_WhatIDo;
                profile.Politics_InOverlay = new_Politics;

                profile.Background_SaveChanges();

                // Go to the Home Page and then come back to the Profile Page to verify the values saved correctly
                home = profile.Header.HomeHeaderLink_Click();
                profile = home.Header.YourProfileHeaderLink_Click();

                string stringToVerifyOnProfilePage = string.Empty;
                List<string> listToVerifyOnProfilePage = new List<string>();

                stringToVerifyOnProfilePage = profile.Ethnicity;
                Assert.IsTrue(new_MyEthnicityIs == stringToVerifyOnProfilePage, "In the Validate_EditProfile_DetailsBackground test case, after saving new values for 'Ethnicity' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_MyEthnicityIs + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                listToVerifyOnProfilePage = profile.Languages;
                new_Languages.Sort();
                Assert.IsTrue(AreValuesAndCountEqual(new_Languages, listToVerifyOnProfilePage), "In the Validate_EditProfile_MoreAboutMeTab test case, after saving new values for 'Languages' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + string.Join(", ", new_Languages.ToArray()) + "' \r\nValue currently in Profile Page: '" + string.Join(", ", listToVerifyOnProfilePage.ToArray()) + "'");

                stringToVerifyOnProfilePage = profile.Religion;
                Assert.IsTrue(new_Religion == stringToVerifyOnProfilePage, "In the Validate_EditProfile_DetailsBackground test case, after saving new values for 'Religion' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_Religion + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.Education;
                Assert.IsTrue(new_Education == stringToVerifyOnProfilePage, "In the Validate_EditProfile_DetailsBackground test case, after saving new values for 'Education' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_Education + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.WhatIDo;
                Assert.IsTrue(new_WhatIDo == stringToVerifyOnProfilePage, "In the Validate_EditProfile_DetailsBackground test case, after saving new values for 'What I do' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_WhatIDo + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.Politics;
                Assert.IsTrue(new_Politics == stringToVerifyOnProfilePage, "In the Validate_EditProfile_DetailsBackground test case, after saving new values for 'Politics' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_Politics + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.Ethnicity_Dealbreakers;
                Assert.IsTrue(new_MyEthnicityIs == stringToVerifyOnProfilePage, "In the Validate_EditProfile_Dealbreakers test case, after saving new values for 'Ethnicity' the values were not saved properly in the Profile Page in the Dealbreakers section \r\nValue we saved in Edit Profile: '" + new_MyEthnicityIs + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.Religion;
                Assert.IsTrue(new_Religion == stringToVerifyOnProfilePage, "In the Validate_EditProfile_Dealbreakers test case, after saving new values for 'Religion' the values were not saved properly in the Profile Page in the Details section \r\nValue we saved in Edit Profile: '" + new_Religion + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.Education;
                Assert.IsTrue(new_Education == stringToVerifyOnProfilePage, "In the Validate_EditProfile_Dealbreakers test case, after saving new values for 'Education' the values were not saved properly in the Profile Page in the Details section \r\nValue we saved in Edit Profile: '" + new_Education + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.WhatIDo;
                Assert.IsTrue(new_WhatIDo == stringToVerifyOnProfilePage, "In the Validate_EditProfile_Dealbreakers test case, after saving new values for 'What I do' the values were not saved properly in the Profile Page in the Details section \r\nValue we saved in Edit Profile: '" + new_WhatIDo + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                // reinitialize to original values
                List<string> orig_Languages = new List<string>();
                orig_Languages.Add("Chinese");
                orig_Languages.Add("Russian");

                profile.Background_ClickToEdit();

                profile.Ethnicity_InOverlay = "Asian";
                profile.Languages_InOverlay = orig_Languages;
                profile.Religion_InOverlay = "Atheist";
                profile.Education_InOverlay = "Doctorate";
                profile.WhatIDo_InOverlay = "Teacher";
                profile.Politics_InOverlay = "Liberal";

                profile.Background_SaveChanges();

                profile.Header.Logout_Click();

                TearDown();

                log.Info("END TEST - Black/Regression/Member/EditProfile_DetailsBackground");
            }
            catch (Exception e)
            {
                CleanupFailedTestcase(e);
                throw;
            }
        }

        /// <summary>
        /// We do not test the Username field
        /// </summary>
        [Test]
        public void Validate_EditProfile_YourBasics()
        {
            try
            {
                log.Info("START TEST - Black/Regression/Member/EditProfile_YourBasics");
                Setup();

                StartTest_UsingBlack();

                string login = System.Configuration.ConfigurationManager.AppSettings["Black_RegisteredUser1_Email"];
                string password = System.Configuration.ConfigurationManager.AppSettings["Black_RegisteredUser1_Password"];

                WebdriverAutomation.Tests.ReusableCode.LoggingInAndOut loggingInAndOut = new LoggingInAndOut();
                WebdriverAutomation.Framework.Site_Pages.Home home = loggingInAndOut.LogIn_ProfileComplete(driver, log, login, password);
                WebdriverAutomation.Framework.Site_Pages.YourProfile profile = home.Header.YourProfileHeaderLink_Click();

                string new_FirstName = "Mark";
                string new_LastName = "Azuras";
                string new_DateOfBirthMonth = "September";
                string new_DateOfBirthDay = "15";
                string new_DateOfBirthYear = "1980";
                string new_Country = "Canada";
                string new_Zipcode = "K8N5W6";
                string new_ImA = "woman seeking men";
                string new_RelationshipStatus = "Divorced";

                // Overwrite new values to the page
                profile.YourBasics_ClickToEdit();

                profile.FirstName_InOverlay = new_FirstName;
                profile.LastName_InOverlay = new_LastName;
                profile.YourDateOfBirthMonth_InOverlay = new_DateOfBirthMonth;
                profile.YourDateOfBirthDay_InOverlay = new_DateOfBirthDay;
                profile.YourDateOfBirthYear_InOverlay = new_DateOfBirthYear;
                profile.Country_InOverlay = new_Country;
                profile.ZipCode_InOverlay = new_Zipcode;
                profile.ImA_InOverlay = new_ImA;
                profile.RelationshipStatus_InOverlay = new_RelationshipStatus;

                profile.YourBasics_SaveChanges();

                // Go to the Home Page and then come back to the Profile Page to verify the values saved correctly
                home = profile.Header.HomeHeaderLink_Click();
                profile = home.Header.YourProfileHeaderLink_Click();

                string stringToVerifyOnProfilePage = string.Empty;

                stringToVerifyOnProfilePage = profile.RelationshipStatus;
                Assert.IsTrue(new_RelationshipStatus == stringToVerifyOnProfilePage, "In the Validate_EditProfile_YourBasics test case, after saving new values for 'Relationship status' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_RelationshipStatus + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.ImA;
                // stupid small discrepancy where the text is slightly different on the Profile Page than on the Edit Profile overlay
                if (new_ImA == "woman seeking men")
                    new_ImA = "Woman seeking Man";

                Assert.IsTrue(new_ImA == stringToVerifyOnProfilePage, "In the Validate_EditProfile_YourBasics test case, after saving new values for 'I'm a' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_ImA + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                int ageOfUser = AgeOfUser(new_DateOfBirthMonth, new_DateOfBirthDay, new_DateOfBirthYear);
                stringToVerifyOnProfilePage = profile.Age;
                Assert.IsTrue(Convert.ToInt32(stringToVerifyOnProfilePage) == ageOfUser, "In the Validate_EditProfile_YourBasics test case, after saving new values for 'Age' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + ageOfUser + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.Country;
                Assert.IsTrue(new_Country == stringToVerifyOnProfilePage, "In the Validate_EditProfile_YourBasics test case, after saving new values for 'Country' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_Country + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                // reinitialize to original values
                profile.YourBasics_ClickToEdit();

                profile.FirstName_InOverlay = "Theo";
                profile.LastName_InOverlay = "Wang";
                profile.YourDateOfBirthMonth_InOverlay = "December";
                profile.YourDateOfBirthDay_InOverlay = "16";
                profile.YourDateOfBirthYear_InOverlay = "1979";
                profile.Country_InOverlay = "USA";
                profile.ZipCode_InOverlay = "90210";
                profile.ImA_InOverlay = "man seeking women";
                profile.RelationshipStatus_InOverlay = "Single";

                profile.YourBasics_SaveChanges();

                profile.Header.Logout_Click();

                TearDown();

                log.Info("END TEST - Black/Regression/Member/EditProfile_YourBasics");
            }
            catch (Exception e)
            {
                CleanupFailedTestcase(e);
                throw;
            }
        }

        [Test]
        public void Validate_EditProfile_Dealbreakers()
        {
            try
            {
                log.Info("START TEST - Black/Regression/Member/EditProfile_Dealbreakers");
                Setup();

                StartTest_UsingBlack();

                string login = System.Configuration.ConfigurationManager.AppSettings["Black_RegisteredUser1_Email"];
                string password = System.Configuration.ConfigurationManager.AppSettings["Black_RegisteredUser1_Password"];

                WebdriverAutomation.Tests.ReusableCode.LoggingInAndOut loggingInAndOut = new LoggingInAndOut();
                WebdriverAutomation.Framework.Site_Pages.Home home = loggingInAndOut.LogIn_ProfileComplete(driver, log, login, password);
                WebdriverAutomation.Framework.Site_Pages.YourProfile profile = home.Header.YourProfileHeaderLink_Click();

                string new_height = "4'6\"(137 cm)";
                string new_bodyType = "Ripped";
                string new_Ethnicity = "Hispanic";
                string new_Smoking = "Non-Smoker";
                string new_Drinking = "Never";
                string new_Religion = "Hindu";
                string new_Education = "Elementary";
                string new_WhatIDo = "Student";
                string new_HaveKids = "3 or More";
                string new_WantsKids = "No";

                // Overwrite new values to the page
                profile.Dealbreakers_ClickToEdit();

                profile.Height_InDealbreakersOverlay = new_height;
                profile.BodyType_InDealbreakersOverlay = new_bodyType;
                profile.Ethnicity_InDealbreakersOverlay = new_Ethnicity;
                profile.Smoking_InDealbreakersOverlay = new_Smoking;
                profile.Drinking_InDealbreakersOverlay = new_Drinking;
                profile.Religion_InDealbreakersOverlay = new_Religion;
                profile.Education_InDealbreakersOverlay = new_Education;
                profile.WhatIDo_InDealbreakersOverlay = new_WhatIDo;
                profile.HaveKids_InDealbreakersOverlay = new_HaveKids;
                profile.WantsKids_InDealbreakersOverlay = new_WantsKids;

                profile.Dealbreakers_SaveChanges();

                // Go to the Home Page and then come back to the Profile Page to verify the values saved correctly
                home = profile.Header.HomeHeaderLink_Click();
                profile = home.Header.YourProfileHeaderLink_Click();

                string stringToVerifyOnProfilePage = string.Empty;

                stringToVerifyOnProfilePage = profile.Height_Dealbreakers;
                Assert.IsTrue(new_height.Replace(" ", "") == stringToVerifyOnProfilePage.Replace(" ", ""), "In the Validate_EditProfile_Dealbreakers test case, after saving new values for 'Height' the values were not saved properly in the Profile Page in the Dealbreakers section \r\nValue we saved in Edit Profile: '" + new_height + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.BodyType_Dealbreakers;
                Assert.IsTrue(new_bodyType == stringToVerifyOnProfilePage, "In the Validate_EditProfile_Dealbreakers test case, after saving new values for 'Body type' the values were not saved properly in the Profile Page in the Dealbreakers section \r\nValue we saved in Edit Profile: '" + new_bodyType + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.Ethnicity_Dealbreakers;
                Assert.IsTrue(new_Ethnicity == stringToVerifyOnProfilePage, "In the Validate_EditProfile_Dealbreakers test case, after saving new values for 'Ethnicity' the values were not saved properly in the Profile Page in the Dealbreakers section \r\nValue we saved in Edit Profile: '" + new_Ethnicity + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.Smoking_Dealbreakers;
                Assert.IsTrue(new_Smoking == stringToVerifyOnProfilePage, "In the Validate_EditProfile_Dealbreakers test case, after saving new values for 'Smoking' the values were not saved properly in the Profile Page in the Dealbreakers section \r\nValue we saved in Edit Profile: '" + new_Smoking + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.Drinking_Dealbreakers;
                Assert.IsTrue(new_Drinking == stringToVerifyOnProfilePage, "In the Validate_EditProfile_Dealbreakers test case, after saving new values for 'Drinking' the values were not saved properly in the Profile Page in the Dealbreakers section \r\nValue we saved in Edit Profile: '" + new_Drinking + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.Religion_Dealbreakers;
                Assert.IsTrue(new_Religion == stringToVerifyOnProfilePage, "In the Validate_EditProfile_Dealbreakers test case, after saving new values for 'Religion' the values were not saved properly in the Profile Page in the Dealbreakers section \r\nValue we saved in Edit Profile: '" + new_Religion + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.Education_Dealbreakers;
                Assert.IsTrue(new_Education == stringToVerifyOnProfilePage, "In the Validate_EditProfile_Dealbreakers test case, after saving new values for 'Education' the values were not saved properly in the Profile Page in the Dealbreakers section \r\nValue we saved in Edit Profile: '" + new_Education + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.WhatIDo_Dealbreakers;
                Assert.IsTrue(new_WhatIDo == stringToVerifyOnProfilePage, "In the Validate_EditProfile_Dealbreakers test case, after saving new values for 'What I do' the values were not saved properly in the Profile Page in the Dealbreakers section \r\nValue we saved in Edit Profile: '" + new_WhatIDo + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.HaveKids_Dealbreakers;
                Assert.IsTrue(new_HaveKids == stringToVerifyOnProfilePage, "In the Validate_EditProfile_Dealbreakers test case, after saving new values for 'Have kids' the values were not saved properly in the Profile Page in the Dealbreakers section \r\nValue we saved in Edit Profile: '" + new_HaveKids + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.WantsKids_Dealbreakers;
                Assert.IsTrue(new_WantsKids == stringToVerifyOnProfilePage, "In the Validate_EditProfile_Dealbreakers test case, after saving new values for 'Wants kids' the values were not saved properly in the Profile Page in the Dealbreakers section \r\nValue we saved in Edit Profile: '" + new_WantsKids + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.Height;
                Assert.IsTrue(new_height.Replace(" ", "") == stringToVerifyOnProfilePage.Replace(" ", ""), "In the Validate_EditProfile_Dealbreakers test case, after saving new values for 'Height' the values were not saved properly in the Profile Page in the Details section \r\nValue we saved in Edit Profile: '" + new_height + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.BodyType;
                Assert.IsTrue(new_bodyType == stringToVerifyOnProfilePage, "In the Validate_EditProfile_Dealbreakers test case, after saving new values for 'Body type' the values were not saved properly in the Profile Page in the Details section \r\nValue we saved in Edit Profile: '" + new_bodyType + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.Ethnicity;
                Assert.IsTrue(new_Ethnicity == stringToVerifyOnProfilePage, "In the Validate_EditProfile_Dealbreakers test case, after saving new values for 'Ethnicity' the values were not saved properly in the Profile Page in the Details section \r\nValue we saved in Edit Profile: '" + new_Ethnicity + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.Smoking;
                Assert.IsTrue(new_Smoking == stringToVerifyOnProfilePage, "In the Validate_EditProfile_Dealbreakers test case, after saving new values for 'Smoking' the values were not saved properly in the Profile Page in the Details section \r\nValue we saved in Edit Profile: '" + new_Smoking + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.Drinking;
                Assert.IsTrue(new_Drinking == stringToVerifyOnProfilePage, "In the Validate_EditProfile_Dealbreakers test case, after saving new values for 'Drinking' the values were not saved properly in the Profile Page in the Details section \r\nValue we saved in Edit Profile: '" + new_Drinking + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.Religion;
                Assert.IsTrue(new_Religion == stringToVerifyOnProfilePage, "In the Validate_EditProfile_Dealbreakers test case, after saving new values for 'Religion' the values were not saved properly in the Profile Page in the Details section \r\nValue we saved in Edit Profile: '" + new_Religion + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.Education;
                Assert.IsTrue(new_Education == stringToVerifyOnProfilePage, "In the Validate_EditProfile_Dealbreakers test case, after saving new values for 'Education' the values were not saved properly in the Profile Page in the Details section \r\nValue we saved in Edit Profile: '" + new_Education + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.WhatIDo;
                Assert.IsTrue(new_WhatIDo == stringToVerifyOnProfilePage, "In the Validate_EditProfile_Dealbreakers test case, after saving new values for 'What I do' the values were not saved properly in the Profile Page in the Details section \r\nValue we saved in Edit Profile: '" + new_WhatIDo + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.HaveKids;
                Assert.IsTrue(new_HaveKids == stringToVerifyOnProfilePage, "In the Validate_EditProfile_Dealbreakers test case, after saving new values for 'Have kids' the values were not saved properly in the Profile Page in the Details section \r\nValue we saved in Edit Profile: '" + new_HaveKids + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                stringToVerifyOnProfilePage = profile.WantKids;
                Assert.IsTrue(new_WantsKids == stringToVerifyOnProfilePage, "In the Validate_EditProfile_Dealbreakers test case, after saving new values for 'Wants kids' the values were not saved properly in the Profile Page in the Details section \r\nValue we saved in Edit Profile: '" + new_WantsKids + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

                // reinitialize to original values
                profile.Dealbreakers_ClickToEdit();

                profile.Height_InDealbreakersOverlay = "5'5\"(165 cm)";
                profile.BodyType_InDealbreakersOverlay = "Portly";
                profile.Ethnicity_InDealbreakersOverlay = "Asian";
                profile.Smoking_InDealbreakersOverlay = "Occasional Smoker";
                profile.Drinking_InDealbreakersOverlay = "Frequently";
                profile.Religion_InDealbreakersOverlay = "Atheist";
                profile.Education_InDealbreakersOverlay = "Doctorate";
                profile.WhatIDo_InDealbreakersOverlay = "Teacher";
                profile.HaveKids_InDealbreakersOverlay = "None";
                profile.WantsKids_InDealbreakersOverlay = "Yes";

                profile.Dealbreakers_SaveChanges();

                profile.Header.Logout_Click();

                TearDown();

                log.Info("END TEST - Black/Regression/Member/EditProfile_Dealbreakers");
            }
            catch (Exception e)
            {
                CleanupFailedTestcase(e);
                throw;
            }
        }

        ///// <summary>
        ///// This tests that Edit Profile - Introduction tab values can be saved and that they appear properly on the Profile page.
        /////    The big chunks of commented out code are for future use, in case I want to randomize all values that I verify.
        ///// NOT TESTED:
        /////    username - testing for changed usernames is not reliable using automation, "All new and updated usernames go through a quick review 
        /////       before being visible on our website. We temporarily display your member id instead while this review is taking place."
        /////    Country/Zip Code - this works for FF but for IE, when we switch out for example USA with another country, say Canada the Zip Code text box never
        /////       gets replaced by the appropriate other text box, say City or Postal Code.  Might have to do with the way Webdriver currently "drives" IE.
        ///// </summary>
        //[Test]
        //public void Validate_EditProfile_IntroductionTab()
        //{
        //    try
        //    {
        //        log.Info("START TEST - Black/Regression/Member/EditProfile_IntroductionTab");

        //        Setup();

        //        StartTest_UsingBlack();

        //        string login = System.Configuration.ConfigurationManager.AppSettings["Black_RegisteredUser1_Email"];
        //        string password = System.Configuration.ConfigurationManager.AppSettings["Black_RegisteredUser1_Password"];

        //        WebdriverAutomation.Tests.ReusableCode.LoggingInAndOut loggingInAndOut = new LoggingInAndOut();
        //        WebdriverAutomation.Framework.Site_Pages.Home home = loggingInAndOut.LogIn_ProfileComplete(driver, log, login, password);
        //        WebdriverAutomation.Framework.Site_Pages.YourProfile_old Profile = home.Header.YourProfileHeaderLink_OldProfile_Click();
        //        WebdriverAutomation.Framework.Site_Pages.EditYourProfile editProfile = Profile.EditButton_Click();

        //        //string new_Email = "automation";
        //        int new_youAreA = 1;
        //        //string new_country = "Canada";
        //        //string new_zipCode = "V6B1A7";
        //        string new_birthdayMonth = "January";
        //        string new_birthdayDay = "12";
        //        string new_birthdayYear = "1981";
        //        string new_aboutMe = "This is a test account operated by an employee of Spark Networks. Testing changes.";
        //        string new_firstName = "Randy";
        //        string new_lastName = "Johnson";                

        //        editProfile.Tab1_Click();

        //        // Overwrite new values to the page
        //        //editProfile.UsernameTextbox = new_Email;
        //        editProfile.YouAreARadioButton = new_youAreA;
        //        //editProfile.CountryDropdown = new_country;
        //        //editProfile.ZipCodeTextbox = new_zipCode;
        //        editProfile.BirthDateMonthDropdown = new_birthdayMonth;
        //        editProfile.BirthDateDayDropdown = new_birthdayDay;
        //        editProfile.BirthDateYearTextbox = new_birthdayYear; 
        //        editProfile.AboutMeTextbox = new_aboutMe;
        //        editProfile.FirstNameTextbox = new_firstName;
        //        editProfile.LastNameTextbox = new_lastName;

        //        editProfile.SaveChangesButton_Click();
        //        Framework.Site_Pages.YourProfile_old profile = editProfile.Header.YourProfileHeaderLink_OldProfile_Click();

        //        // verify values on Profile page
        //        profile.Tab1_Click();

        //        string stringToVerifyOnProfilePage = string.Empty;

        //        stringToVerifyOnProfilePage = profile.YouAreA;

        //        string iAmA = string.Empty;
        //        if (new_youAreA == 0)
        //            iAmA = "Man";
        //        else
        //            iAmA = "Woman";

        //        Assert.IsTrue(iAmA == stringToVerifyOnProfilePage, "In the Validate_EditProfile_Introduction test case, after saving new values for I Am A the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + iAmA + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

        //        stringToVerifyOnProfilePage = profile.YearsOld;

        //        //stringToVerifyOnProfilePage = profile.Location;
        //        ///Assert.IsTrue(new_country == stringToVerifyOnProfilePage, "In the Validate_EditProfile_Introduction test case, after saving new values for Country the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_country + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

        //        int ageOfUser = AgeOfUser(new_birthdayMonth, new_birthdayDay, new_birthdayYear);
        //        Assert.IsTrue(Convert.ToInt32(stringToVerifyOnProfilePage) == ageOfUser, "In the Validate_EditProfile_Introduction test case, after saving new values for the Age status the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + ageOfUser.ToString() + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

        //        stringToVerifyOnProfilePage = profile.AboutMe;
        //        Assert.IsTrue(new_aboutMe == stringToVerifyOnProfilePage, "In the Validate_EditProfile_Introduction test case, after saving new values for About Me the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_aboutMe + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

        //        editProfile = Profile.EditButton_Click();
        //        editProfile.Tab1_Click();

        //        // verify values on Edit Profile page's Introduction tab
        //        //Assert.IsTrue(editProfile.UsernameTextbox == new_Email, "In the Validate_EditProfile_IntroductionTab test case, after saving new values for Username and going back to the Edit Profile page the values were not saved properly \r\nValue we saved in Edit Profile: '" + new_Email + "' \r\nValue currently in Edit Profile Page: '" + editProfile.UsernameTextbox + "'");
        //        Assert.IsTrue(editProfile.YouAreARadioButton == new_youAreA, "In the Validate_EditProfile_IntroductionTab test case, after saving new values for You Are A and going back to the Edit Profile page the values were not saved properly \r\nValue we saved in Edit Profile: '" + new_youAreA + "' \r\nValue currently in Edit Profile Page: '" + editProfile.YouAreARadioButton + "'");
        //        //Assert.IsTrue(editProfile.CountryDropdown == new_country, "In the Validate_EditProfile_IntroductionTab test case, after saving new values for Country and going back to the Edit Profile page the values were not saved properly \r\nValue we saved in Edit Profile: '" + new_country + "' \r\nValue currently in Edit Profile Page: '" + editProfile.CountryDropdown + "'");
        //        //Assert.IsTrue(editProfile.ZipCodeTextbox == new_zipCode, "In the Validate_EditProfile_IntroductionTab test case, after saving new values for Zip Code and going back to the Edit Profile page the values were not saved properly \r\nValue we saved in Edit Profile: '" + new_zipCode + "' \r\nValue currently in Edit Profile Page: '" + editProfile.ZipCodeTextbox + "'");
        //        Assert.IsTrue(editProfile.BirthDateMonthDropdown.Contains(new_birthdayMonth), "In the Validate_EditProfile_IntroductionTab test case, after saving new values for Birthday Month and going back to the Edit Profile page the values were not saved properly \r\nValue we saved in Edit Profile: '" + new_birthdayMonth + "' \r\nValue currently in Edit Profile Page: '" + editProfile.BirthDateMonthDropdown + "'");
        //        Assert.IsTrue(editProfile.BirthDateDayDropdown == new_birthdayDay, "In the Validate_EditProfile_IntroductionTab test case, after saving new values for Birthday Day and going back to the Edit Profile page the values were not saved properly \r\nValue we saved in Edit Profile: '" + new_birthdayDay + "' \r\nValue currently in Edit Profile Page: '" + editProfile.BirthDateDayDropdown + "'");
        //        Assert.IsTrue(editProfile.BirthDateYearTextbox == new_birthdayYear, "In the Validate_EditProfile_IntroductionTab test case, after saving new values for Birthday Year and going back to the Edit Profile page the values were not saved properly \r\nValue we saved in Edit Profile: '" + new_birthdayYear + "' \r\nValue currently in Edit Profile Page: '" + editProfile.BirthDateYearTextbox + "'");
        //        Assert.IsTrue(editProfile.AboutMeTextbox == new_aboutMe, "In the Validate_EditProfile_IntroductionTab test case, after saving new values for About Me and going back to the Edit Profile page the values were not saved properly \r\nValue we saved in Edit Profile: '" + new_aboutMe + "' \r\nValue currently in Edit Profile Page: '" + editProfile.AboutMeTextbox + "'");
        //        Assert.IsTrue(editProfile.FirstNameTextbox == new_firstName, "In the Validate_EditProfile_IntroductionTab test case, after saving new values for First Name and going back to the Edit Profile page the values were not saved properly \r\nValue we saved in Edit Profile: '" + new_firstName + "' \r\nValue currently in Edit Profile Page: '" + editProfile.FirstNameTextbox + "'");
        //        Assert.IsTrue(editProfile.LastNameTextbox == new_lastName, "In the Validate_EditProfile_IntroductionTab test case, after saving new values for Last Name and going back to the Edit Profile page the values were not saved properly \r\nValue we saved in Edit Profile: '" + new_lastName + "' \r\nValue currently in Edit Profile Page: '" + editProfile.LastNameTextbox + "'");

        //        //editProfile.UsernameTextbox = old_Email;  
        //        editProfile.YouAreARadioButton = 0;
        //        //editProfile.CountryDropdown = "USA";
        //        //editProfile.ZipCodeTextbox = "94133";
        //        editProfile.BirthDateMonthDropdown = "December";
        //        editProfile.BirthDateDayDropdown = "16";
        //        editProfile.BirthDateYearTextbox = "1979";
        //        editProfile.AboutMeTextbox = "This is a test account operated by an employee of Spark Networks"; 
        //        editProfile.FirstNameTextbox = "Theo";
        //        editProfile.LastNameTextbox = "Wong";

        //        editProfile.SaveChangesButton_Click();
        //        profile = editProfile.Header.YourProfileHeaderLink_OldProfile_Click();

        //        profile.Header.Logout_Click();

        //        TearDown();

        //        log.Info("END TEST - Black/Regression/Member/EditProfile_IntroductionTab");
        //    }
        //    catch (Exception e)
        //    {
        //        CleanupFailedTestcase(e);
        //        throw;
        //    }
        //}

        ///// <summary>
        ///// NOT TESTED:
        /////    Zodiac Sign 
        ///// </summary>
        //[Test]
        //public void Validate_EditProfile_AboutMeTab()
        //{
        //    try
        //    {
        //        log.Info("START TEST - Black/Regression/Member/EditProfile_AboutMeTab");

        //        Setup();

        //        StartTest_UsingBlack();

        //        string login = System.Configuration.ConfigurationManager.AppSettings["Black_RegisteredUser1_Email"];
        //        string password = System.Configuration.ConfigurationManager.AppSettings["Black_RegisteredUser1_Password"];

        //        WebdriverAutomation.Tests.ReusableCode.LoggingInAndOut loggingInAndOut = new LoggingInAndOut();
        //        WebdriverAutomation.Framework.Site_Pages.Home home = loggingInAndOut.LogIn_ProfileComplete(driver, log, login, password);
        //        WebdriverAutomation.Framework.Site_Pages.YourProfile_old Profile = home.Header.YourProfileHeaderLink_OldProfile_Click();
        //        WebdriverAutomation.Framework.Site_Pages.EditYourProfile editProfile = Profile.EditButton_Click();

        //        string new_height = "4'6\"(137 cm)";
        //        string new_bodyStyle = "Ripped";
        //        string new_hair = "Red";
        //        string new_eyes = "Green";
        //        string new_RelationshipStatus = "Widowed";
        //        int new_WantKids = 1;
        //        string new_HaveKids = "3 or More";
        //        string new_Custody = "Sometimes";
        //        string new_ISmoke = "Non-Smoker";
        //        string new_MemberIDrink = "Never";
        //        string new_Religion = "Hindu";
        //        string new_Education = "Elementary";
        //        string new_Occupation = "Student";
        //        string new_MyEthnicityIs = "Hispanic";
        //        string new_Languages = "Czech";
        //        string new_SelfDescription = "This is a test account operated by an employee of Spark Networks. Testing changes.";
        //        string new_MyFavoriteMusic = "Country";
        //        string new_FavoriteBandsAndMusicians = "nine inch nails";
        //        string new_FavoriteTypesOfMovies = "Documentary";
        //        string new_FavoriteMoviesAndActors = "The City of Lost Children";
        //        string new_FavoriteTVShows = "The Outer Limits";
        //        string new_MyFavoritePhysicalActivites = "Aerobics";
        //        string new_MyFavoriteLeisureActivities = "Camping";
        //        string new_MyIdeaOfAGreatTrip = "Hiking at Joshua Tree";
        //        string new_MyFavoriteFoods = "Continental";
        //        string new_FavoriteRestaurants = "Roscoe's";
        //        string new_PoliticalOrientation = "Right Wing";
        //        string new_SchoolsAttended = "Can't remember";
        //        string new_WhenGoingSomewhere = "I usually forget to show up";
        //        string new_FashionSense = "I dont need advice";

        //        editProfile.Tab2_Click();

        //        // Overwrite new values to the page
        //        editProfile.HeightDropdown = new_height;
        //        editProfile.BodyStyleDropdown = new_bodyStyle;
        //        editProfile.MyHairIsDropdown = new_hair;
        //        editProfile.MyEyesAreDropdown = new_eyes;
        //        editProfile.RelationshipStatusDropdown = new_RelationshipStatus;
        //        editProfile.WantKidsRadioButton = new_WantKids;
        //        editProfile.HaveKidsDropdown = new_HaveKids;
        //        editProfile.CustodyDropdown = new_Custody;
        //        editProfile.ISmokeDropdown = new_ISmoke;
        //        editProfile.IDrinkDropdown = new_MemberIDrink;
        //        editProfile.ReligionDropdown = new_Religion;
        //        editProfile.EducationDropdown = new_Education;
        //        editProfile.OccupationDescriptionTextbox = new_Occupation;
        //        editProfile.MyEthnicityIsList = new_MyEthnicityIs;
        //        editProfile.ISpeakList = new_Languages;
        //        editProfile.SelfDescriptionTextbox = new_SelfDescription;
        //        editProfile.MyFavoriteMusicList = new_MyFavoriteMusic;
        //        editProfile.FavoriteBandsAndMusiciansTextbox = new_FavoriteBandsAndMusicians;
        //        editProfile.FavoriteTypesOfMoviesList = new_FavoriteTypesOfMovies;
        //        editProfile.FavoriteMoviesAndActorsTextbox = new_FavoriteMoviesAndActors;
        //        editProfile.FavoriteTVShowsTextbox = new_FavoriteTVShows;
        //        editProfile.MyFavoritePhysicalActivitesList = new_MyFavoritePhysicalActivites;
        //        editProfile.MyFavoriteLeisureActivitesList = new_MyFavoriteLeisureActivities;
        //        editProfile.MyIdeaOfAGreatTripTextbox = new_MyIdeaOfAGreatTrip;
        //        editProfile.MyFavoriteFoodsList = new_MyFavoriteFoods;
        //        editProfile.FavoriteRestaurantsTextbox = new_FavoriteRestaurants;
        //        editProfile.PoliticalOrientationDropdown = new_PoliticalOrientation;
        //        editProfile.SchoolsAttendedTextbox = new_SchoolsAttended;
        //        editProfile.WhenGoingSomewhereDropdown = new_WhenGoingSomewhere;
        //        editProfile.FashionSenseDropdown = new_FashionSense;

        //        editProfile.SaveChangesButton_Click();
        //        Framework.Site_Pages.YourProfile_old profile = editProfile.Header.YourProfileHeaderLink_OldProfile_Click();

        //        // verify values on Profile page
        //        profile.Tab2_Click();

        //        string stringToVerifyOnProfilePage = string.Empty;

        //        stringToVerifyOnProfilePage = profile.Height;
        //        Assert.IsTrue(RemoveAllWhitespacesFromString(new_height) == RemoveAllWhitespacesFromString(stringToVerifyOnProfilePage), "In the Validate_EditProfile_AboutMeTab test case, after saving new values for 'Height' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_height + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

        //        stringToVerifyOnProfilePage = profile.BodyStyle;
        //        Assert.IsTrue(new_bodyStyle == stringToVerifyOnProfilePage, "In the Validate_EditProfile_AboutMeTab test case, after saving new values for 'Build' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_bodyStyle + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

        //        stringToVerifyOnProfilePage = profile.MyHairIs;
        //        Assert.IsTrue(new_hair == stringToVerifyOnProfilePage, "In the Validate_EditProfile_AboutMeTab test case, after saving new values for 'Hair Color' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_height + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

        //        stringToVerifyOnProfilePage = profile.MyEyesAre;
        //        Assert.IsTrue(new_eyes == stringToVerifyOnProfilePage, "In the Validate_EditProfile_AboutMeTab test case, after saving new values for 'Eye Color' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_eyes + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

        //        stringToVerifyOnProfilePage = profile.RelationshipStatus;
        //        Assert.IsTrue(new_RelationshipStatus == stringToVerifyOnProfilePage, "In the Validate_EditProfile_AboutMeTab test case, after saving new values for 'Relationship Status' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_RelationshipStatus + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

        //        stringToVerifyOnProfilePage = profile.PlanOnHavingChildren;
        //        string planOnHavingChildrenText;
        //        if (new_WantKids == 0)
        //            planOnHavingChildrenText = "Yes";
        //        else if (new_WantKids == 1)
        //            planOnHavingChildrenText = "No";
        //        else
        //            planOnHavingChildrenText = "Not sure";
        //        Assert.IsTrue(planOnHavingChildrenText == stringToVerifyOnProfilePage, "In the Validate_EditProfile_AboutMeTab test case, after saving new values for 'Plan on Having More Children' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_WantKids + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

        //        stringToVerifyOnProfilePage = profile.Children;
        //        Assert.IsTrue(new_HaveKids == stringToVerifyOnProfilePage, "In the Validate_EditProfile_AboutMeTab test case, after saving new values for 'Children' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_HaveKids + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

        //        stringToVerifyOnProfilePage = profile.CustodySituation;
        //        Assert.IsTrue(new_Custody == stringToVerifyOnProfilePage, "In the Validate_EditProfile_AboutMeTab test case, after saving new values for 'Custody Situation' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_Custody + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

        //        stringToVerifyOnProfilePage = profile.SmokingHabits;
        //        Assert.IsTrue(new_ISmoke == stringToVerifyOnProfilePage, "In the Validate_EditProfile_AboutMeTab test case, after saving new values for 'Smoking' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_ISmoke + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

        //        stringToVerifyOnProfilePage = profile.DrinkingHabits;
        //        Assert.IsTrue(new_MemberIDrink == stringToVerifyOnProfilePage, "In the Validate_EditProfile_AboutMeTab test case, after saving new values for 'Drinking' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_MemberIDrink + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

        //        stringToVerifyOnProfilePage = profile.MyReligion;
        //        Assert.IsTrue(new_Religion == stringToVerifyOnProfilePage, "In the Validate_EditProfile_AboutMeTab test case, after saving new values for 'My Religion' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_Religion + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

        //        stringToVerifyOnProfilePage = profile.Education;
        //        Assert.IsTrue(new_Education == stringToVerifyOnProfilePage, "In the Validate_EditProfile_AboutMeTab test case, after saving new values for 'Eduction' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_Education + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

        //        stringToVerifyOnProfilePage = profile.FieldOfWork;
        //        Assert.IsTrue(new_Occupation == stringToVerifyOnProfilePage, "In the Validate_EditProfile_AboutMeTab test case, after saving new values for 'Field of Work' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_Occupation + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

        //        stringToVerifyOnProfilePage = profile.MyEthnicityIs;
        //        Assert.IsTrue(new_MyEthnicityIs == stringToVerifyOnProfilePage, "In the Validate_EditProfile_AboutMeTab test case, after saving new values for 'Ethnicity' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_MyEthnicityIs + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

        //        stringToVerifyOnProfilePage = profile.ISpeak;
        //        Assert.IsTrue(new_Languages == stringToVerifyOnProfilePage, "In the Validate_EditProfile_AboutMeTab test case, after saving new values for 'Languages Spoken' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_Languages + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

        //        stringToVerifyOnProfilePage = profile.SelfDescription;
        //        Assert.IsTrue(new_SelfDescription == stringToVerifyOnProfilePage, "In the Validate_EditProfile_AboutMeTab test case, after saving new values for 'Self-Description' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_SelfDescription + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

        //        stringToVerifyOnProfilePage = profile.MyFavoriteMusic;
        //        Assert.IsTrue(new_MyFavoriteMusic == stringToVerifyOnProfilePage, "In the Validate_EditProfile_AboutMeTab test case, after saving new values for 'Music' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_MyFavoriteMusic + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

        //        stringToVerifyOnProfilePage = profile.FavoriteBandsAndMusicians;
        //        Assert.IsTrue(new_FavoriteBandsAndMusicians == stringToVerifyOnProfilePage, "In the Validate_EditProfile_AboutMeTab test case, after saving new values for 'Favorite bands and musicians' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_FavoriteBandsAndMusicians + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

        //        stringToVerifyOnProfilePage = profile.Movie;
        //        Assert.IsTrue(new_FavoriteTypesOfMovies == stringToVerifyOnProfilePage, "In the Validate_EditProfile_AboutMeTab test case, after saving new values for 'Movie' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_FavoriteTypesOfMovies + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

        //        stringToVerifyOnProfilePage = profile.FavoriteMoviesAndActors;
        //        Assert.IsTrue(new_FavoriteMoviesAndActors == stringToVerifyOnProfilePage, "In the Validate_EditProfile_AboutMeTab test case, after saving new values for 'Favorite movies and actors' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_FavoriteMoviesAndActors + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

        //        stringToVerifyOnProfilePage = profile.FavoriteTVShows;
        //        Assert.IsTrue(new_FavoriteTVShows == stringToVerifyOnProfilePage, "In the Validate_EditProfile_AboutMeTab test case, after saving new values for 'Favorite TV shows' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_FavoriteTVShows + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

        //        stringToVerifyOnProfilePage = profile.MyFavoritePhysicalActivites;
        //        Assert.IsTrue(new_MyFavoritePhysicalActivites == stringToVerifyOnProfilePage, "In the Validate_EditProfile_AboutMeTab test case, after saving new values for 'Outdoor Activities' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_MyFavoritePhysicalActivites + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

        //        stringToVerifyOnProfilePage = profile.IndoorActivities;
        //        Assert.IsTrue(new_MyFavoriteLeisureActivities == stringToVerifyOnProfilePage, "In the Validate_EditProfile_AboutMeTab test case, after saving new values for 'Indoor Activities' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_MyFavoriteLeisureActivities + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

        //        stringToVerifyOnProfilePage = profile.MyIdeaOfAGreatTrip;
        //        Assert.IsTrue(new_MyIdeaOfAGreatTrip == stringToVerifyOnProfilePage, "In the Validate_EditProfile_AboutMeTab test case, after saving new values for 'My idea of a great trip' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_MyIdeaOfAGreatTrip + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

        //        stringToVerifyOnProfilePage = profile.MyFavoriteFoods;
        //        Assert.IsTrue(new_MyFavoriteFoods == stringToVerifyOnProfilePage, "In the Validate_EditProfile_AboutMeTab test case, after saving new values for 'Food' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_MyFavoriteFoods + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

        //        stringToVerifyOnProfilePage = profile.FavoriteRestaurants;
        //        Assert.IsTrue(new_FavoriteRestaurants == stringToVerifyOnProfilePage, "In the Validate_EditProfile_AboutMeTab test case, after saving new values for 'Favorite restaurants' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_FavoriteRestaurants + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

        //        stringToVerifyOnProfilePage = profile.PoliticalOrientation;
        //        Assert.IsTrue(new_PoliticalOrientation == stringToVerifyOnProfilePage, "In the Validate_EditProfile_AboutMeTab test case, after saving new values for 'Political orientation' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_PoliticalOrientation + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

        //        stringToVerifyOnProfilePage = profile.SchoolsAttended;
        //        Assert.IsTrue(new_SchoolsAttended == stringToVerifyOnProfilePage, "In the Validate_EditProfile_AboutMeTab test case, after saving new values for 'SchoolsAttended' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_SchoolsAttended + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

        //        stringToVerifyOnProfilePage = profile.Timeliness;
        //        Assert.IsTrue(new_WhenGoingSomewhere == stringToVerifyOnProfilePage, "In the Validate_EditProfile_AboutMeTab test case, after saving new values for 'Timeliness' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_WhenGoingSomewhere + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

        //        stringToVerifyOnProfilePage = profile.AsForFashion;
        //        Assert.IsTrue(new_FashionSense == stringToVerifyOnProfilePage, "In the Validate_EditProfile_AboutMeTab test case, after saving new values for 'As For Fashion' the values were not saved properly in the Profile Page \r\nValue we saved in Edit Profile: '" + new_FashionSense + "' \r\nValue currently in Profile Page: '" + stringToVerifyOnProfilePage + "'");

        //        editProfile = Profile.EditButton_Click();
        //        editProfile.Tab2_Click();

        //        // verify values on Edit Profile page's Who Am I tab
        //        Assert.IsTrue(editProfile.HeightDropdown == new_height, "In the Validate_EditProfile_AboutMeTab test case, after saving new values for 'Height' and going back to the Edit Profile page the values were not saved properly \r\nValue we saved in Edit Profile: '" + new_height + "' \r\nValue currently in Edit Profile Page: '" + editProfile.HeightDropdown + "'");
        //        Assert.IsTrue(editProfile.BodyStyleDropdown == new_bodyStyle, "In the Validate_EditProfile_AboutMeTab test case, after saving new values for 'Body type' and going back to the Edit Profile page the values were not saved properly \r\nValue we saved in Edit Profile: '" + new_bodyStyle + "' \r\nValue currently in Edit Profile Page: '" + editProfile.BodyStyleDropdown + "'");
        //        Assert.IsTrue(editProfile.MyHairIsDropdown == new_hair, "In the Validate_EditProfile_AboutMeTab test case, after saving new values for 'Hair color' and going back to the Edit Profile page the values were not saved properly \r\nValue we saved in Edit Profile: '" + new_hair + "' \r\nValue currently in Edit Profile Page: '" + editProfile.MyHairIsDropdown + "'");
        //        Assert.IsTrue(editProfile.MyEyesAreDropdown == new_eyes, "In the Validate_EditProfile_AboutMeTab test case, after saving new values for 'Eye color' and going back to the Edit Profile page the values were not saved properly \r\nValue we saved in Edit Profile: '" + new_eyes + "' \r\nValue currently in Edit Profile Page: '" + editProfile.MyEyesAreDropdown + "'");
        //        Assert.IsTrue(editProfile.RelationshipStatusDropdown == new_RelationshipStatus, "In the Validate_EditProfile_AboutMeTab test case, after saving new values for 'Height' and going back to the Edit Profile page the values were not saved properly \r\nValue we saved in Edit Profile: '" + new_RelationshipStatus + "' \r\nValue currently in Edit Profile Page: '" + editProfile.RelationshipStatusDropdown + "'");
        //        Assert.IsTrue(editProfile.WantKidsRadioButton == new_WantKids, "In the Validate_EditProfile_AboutMeTab test case, after saving new values for 'Want kids' and going back to the Edit Profile page the values were not saved properly \r\nValue we saved in Edit Profile: '" + new_WantKids + "' \r\nValue currently in Edit Profile Page: '" + editProfile.WantKidsRadioButton + "'");
        //        Assert.IsTrue(editProfile.HaveKidsDropdown == new_HaveKids, "In the Validate_EditProfile_AboutMeTab test case, after saving new values for 'Have kids' and going back to the Edit Profile page the values were not saved properly \r\nValue we saved in Edit Profile: '" + new_HaveKids + "' \r\nValue currently in Edit Profile Page: '" + editProfile.HaveKidsDropdown + "'");
        //        Assert.IsTrue(editProfile.CustodyDropdown == new_Custody, "In the Validate_EditProfile_AboutMeTab test case, after saving new values for 'Custody' and going back to the Edit Profile page the values were not saved properly \r\nValue we saved in Edit Profile: '" + new_Custody + "' \r\nValue currently in Edit Profile Page: '" + editProfile.CustodyDropdown + "'");
        //        Assert.IsTrue(editProfile.ISmokeDropdown == new_ISmoke, "In the Validate_EditProfile_AboutMeTab test case, after saving new values for 'Do you smoke?' and going back to the Edit Profile page the values were not saved properly \r\nValue we saved in Edit Profile: '" + new_ISmoke + "' \r\nValue currently in Edit Profile Page: '" + editProfile.ISmokeDropdown + "'");
        //        Assert.IsTrue(editProfile.IDrinkDropdown == new_MemberIDrink, "In the Validate_EditProfile_AboutMeTab test case, after saving new values for 'Do you drink?' and going back to the Edit Profile page the values were not saved properly \r\nValue we saved in Edit Profile: '" + new_MemberIDrink + "' \r\nValue currently in Edit Profile Page: '" + editProfile.IDrinkDropdown + "'");
        //        Assert.IsTrue(editProfile.ReligionDropdown == new_Religion, "In the Validate_EditProfile_AboutMeTab test case, after saving new values for 'Religion' and going back to the Edit Profile page the values were not saved properly \r\nValue we saved in Edit Profile: '" + new_Religion + "' \r\nValue currently in Edit Profile Page: '" + editProfile.ReligionDropdown + "'");
        //        Assert.IsTrue(editProfile.EducationDropdown == new_Education, "In the Validate_EditProfile_AboutMeTab test case, after saving new values for 'Education' and going back to the Edit Profile page the values were not saved properly \r\nValue we saved in Edit Profile: '" + new_Education + "' \r\nValue currently in Edit Profile Page: '" + editProfile.EducationDropdown + "'");
        //        Assert.IsTrue(editProfile.OccupationDescriptionTextbox == new_Occupation, "In the Validate_EditProfile_AboutMeTab test case, after saving new values for 'Occupation' and going back to the Edit Profile page the values were not saved properly \r\nValue we saved in Edit Profile: '" + new_Occupation + "' \r\nValue currently in Edit Profile Page: '" + editProfile.OccupationDescriptionTextbox + "'");
        //        Assert.IsTrue(editProfile.MyEthnicityIsList == new_MyEthnicityIs, "In the Validate_EditProfile_AboutMeTab test case, after saving new values for 'Ethnicity' and going back to the Edit Profile page the values were not saved properly \r\nValue we saved in Edit Profile: '" + new_MyEthnicityIs + "' \r\nValue currently in Edit Profile Page: '" + editProfile.MyEthnicityIsList + "'");
        //        Assert.IsTrue(editProfile.ISpeakList == new_Languages, "In the Validate_EditProfile_AboutMeTab test case, after saving new values for 'Languages' and going back to the Edit Profile page the values were not saved properly \r\nValue we saved in Edit Profile: '" + new_Languages + "' \r\nValue currently in Edit Profile Page: '" + editProfile.ISpeakList + "'");
        //        Assert.IsTrue(editProfile.SelfDescriptionTextbox == new_SelfDescription, "In the Validate_EditProfile_AboutMeTab test case, after saving new values for 'Self-description' and going back to the Edit Profile page the values were not saved properly \r\nValue we saved in Edit Profile: '" + new_SelfDescription + "' \r\nValue currently in Edit Profile Page: '" + editProfile.SelfDescriptionTextbox + "'");
        //        Assert.IsTrue(editProfile.MyFavoriteMusicList == new_MyFavoriteMusic, "In the Validate_EditProfile_AboutMeTab test case, after saving new values for 'Favorite types of music' and going back to the Edit Profile page the values were not saved properly \r\nValue we saved in Edit Profile: '" + new_MyFavoriteMusic + "' \r\nValue currently in Edit Profile Page: '" + editProfile.MyFavoriteMusicList + "'");
        //        Assert.IsTrue(editProfile.FavoriteBandsAndMusiciansTextbox == new_FavoriteBandsAndMusicians, "In the Validate_EditProfile_AboutMeTab test case, after saving new values for 'Favorite bands and musicians' and going back to the Edit Profile page the values were not saved properly \r\nValue we saved in Edit Profile: '" + new_FavoriteBandsAndMusicians + "' \r\nValue currently in Edit Profile Page: '" + editProfile.FavoriteBandsAndMusiciansTextbox + "'");
        //        Assert.IsTrue(editProfile.FavoriteTypesOfMoviesList == new_FavoriteTypesOfMovies, "In the Validate_EditProfile_AboutMeTab test case, after saving new values for 'Favorite types of movies' and going back to the Edit Profile page the values were not saved properly \r\nValue we saved in Edit Profile: '" + new_FavoriteTypesOfMovies + "' \r\nValue currently in Edit Profile Page: '" + editProfile.FavoriteTypesOfMoviesList + "'");
        //        Assert.IsTrue(editProfile.FavoriteMoviesAndActorsTextbox == new_FavoriteMoviesAndActors, "In the Validate_EditProfile_AboutMeTab test case, after saving new values for 'Favorite movies and actors' and going back to the Edit Profile page the values were not saved properly \r\nValue we saved in Edit Profile: '" + new_FavoriteMoviesAndActors + "' \r\nValue currently in Edit Profile Page: '" + editProfile.FavoriteMoviesAndActorsTextbox + "'");
        //        Assert.IsTrue(editProfile.FavoriteTVShowsTextbox == new_FavoriteTVShows, "In the Validate_EditProfile_AboutMeTab test case, after saving new values for 'Favorite TV shows' and going back to the Edit Profile page the values were not saved properly \r\nValue we saved in Edit Profile: '" + new_FavoriteTVShows + "' \r\nValue currently in Edit Profile Page: '" + editProfile.FavoriteTVShowsTextbox + "'");
        //        Assert.IsTrue(editProfile.MyFavoritePhysicalActivitesList == new_MyFavoritePhysicalActivites, "In the Validate_EditProfile_AboutMeTab test case, after saving new values for 'Favorite physical activities' and going back to the Edit Profile page the values were not saved properly \r\nValue we saved in Edit Profile: '" + new_MyFavoritePhysicalActivites + "' \r\nValue currently in Edit Profile Page: '" + editProfile.MyFavoritePhysicalActivitesList + "'");
        //        Assert.IsTrue(editProfile.MyFavoriteLeisureActivitesList == new_MyFavoriteLeisureActivities, "In the Validate_EditProfile_AboutMeTab test case, after saving new values for 'Favorite leisure activites' and going back to the Edit Profile page the values were not saved properly \r\nValue we saved in Edit Profile: '" + new_MyFavoriteLeisureActivities + "' \r\nValue currently in Edit Profile Page: '" + editProfile.MyFavoriteLeisureActivitesList + "'");
        //        Assert.IsTrue(editProfile.MyIdeaOfAGreatTripTextbox == new_MyIdeaOfAGreatTrip, "In the Validate_EditProfile_AboutMeTab test case, after saving new values for 'My idea of a great trip' and going back to the Edit Profile page the values were not saved properly \r\nValue we saved in Edit Profile: '" + new_MyIdeaOfAGreatTrip + "' \r\nValue currently in Edit Profile Page: '" + editProfile.MyIdeaOfAGreatTripTextbox + "'");
        //        Assert.IsTrue(editProfile.MyFavoriteFoodsList == new_MyFavoriteFoods, "In the Validate_EditProfile_AboutMeTab test case, after saving new values for 'Favorite types of food' and going back to the Edit Profile page the values were not saved properly \r\nValue we saved in Edit Profile: '" + new_MyFavoriteFoods + "' \r\nValue currently in Edit Profile Page: '" + editProfile.MyFavoriteFoodsList + "'");
        //        Assert.IsTrue(editProfile.FavoriteRestaurantsTextbox == new_FavoriteRestaurants, "In the Validate_EditProfile_AboutMeTab test case, after saving new values for 'Favorite restaurants' and going back to the Edit Profile page the values were not saved properly \r\nValue we saved in Edit Profile: '" + new_FavoriteRestaurants + "' \r\nValue currently in Edit Profile Page: '" + editProfile.FavoriteRestaurantsTextbox + "'");
        //        Assert.IsTrue(editProfile.PoliticalOrientationDropdown == new_PoliticalOrientation, "In the Validate_EditProfile_AboutMeTab test case, after saving new values for 'Political orientation' and going back to the Edit Profile page the values were not saved properly \r\nValue we saved in Edit Profile: '" + new_PoliticalOrientation + "' \r\nValue currently in Edit Profile Page: '" + editProfile.PoliticalOrientationDropdown + "'");
        //        Assert.IsTrue(editProfile.SchoolsAttendedTextbox == new_SchoolsAttended, "In the Validate_EditProfile_AboutMeTab test case, after saving new values for 'Schools attended' and going back to the Edit Profile page the values were not saved properly \r\nValue we saved in Edit Profile: '" + new_SchoolsAttended + "' \r\nValue currently in Edit Profile Page: '" + editProfile.SchoolsAttendedTextbox + "'");
        //        Assert.IsTrue(editProfile.WhenGoingSomewhereDropdown == new_WhenGoingSomewhere, "In the Validate_EditProfile_AboutMeTab test case, after saving new values for 'When going somewhere...' and going back to the Edit Profile page the values were not saved properly \r\nValue we saved in Edit Profile: '" + new_WhenGoingSomewhere + "' \r\nValue currently in Edit Profile Page: '" + editProfile.WhenGoingSomewhereDropdown + "'");
        //        Assert.IsTrue(editProfile.FashionSenseDropdown == new_FashionSense, "In the Validate_EditProfile_AboutMeTab test case, after saving new values for 'Fashion sense' and going back to the Edit Profile page the values were not saved properly \r\nValue we saved in Edit Profile: '" + new_FashionSense + "' \r\nValue currently in Edit Profile Page: '" + editProfile.FashionSenseDropdown + "'");

        //        editProfile.HeightDropdown = "5'5\"(165 cm)";
        //        editProfile.BodyStyleDropdown = "Portly";
        //        editProfile.MyHairIsDropdown = "Black";
        //        editProfile.MyEyesAreDropdown = "Brown";
        //        editProfile.RelationshipStatusDropdown = "Single";
        //        editProfile.WantKidsRadioButton = 2;
        //        editProfile.HaveKidsDropdown = "None";
        //        editProfile.CustodyDropdown = "I have no kids";
        //        editProfile.ISmokeDropdown = "Occasional Smoker";
        //        editProfile.IDrinkDropdown = "Frequently";
        //        editProfile.ReligionDropdown = "Atheist";
        //        editProfile.EducationDropdown = "Doctorate";
        //        editProfile.OccupationDescriptionTextbox = "Teacher";
        //        editProfile.MyEthnicityIsList = "Asian";
        //        editProfile.ISpeakList = "Dutch";
        //        editProfile.SelfDescriptionTextbox = "This is a test account operated by an employee of Spark Networks.";
        //        editProfile.MyFavoriteMusicList = "Dance/Electronica";
        //        editProfile.FavoriteBandsAndMusiciansTextbox = "Portishead";
        //        editProfile.FavoriteTypesOfMoviesList = "Action/Adventure";
        //        editProfile.FavoriteMoviesAndActorsTextbox = "Nightmare Before Christmas";
        //        editProfile.FavoriteTVShowsTextbox = "Married With Children";
        //        editProfile.MyFavoritePhysicalActivitesList = "Dancing";
        //        editProfile.MyFavoriteLeisureActivitesList = "Dining Out";
        //        editProfile.MyIdeaOfAGreatTripTextbox = "Europe";
        //        editProfile.MyFavoriteFoodsList = "Barbecue";
        //        editProfile.FavoriteRestaurantsTextbox = "Lawrys";
        //        editProfile.PoliticalOrientationDropdown = "Liberal";
        //        editProfile.SchoolsAttendedTextbox = "This and that";
        //        editProfile.WhenGoingSomewhereDropdown = "I am usually a little late";
        //        editProfile.FashionSenseDropdown = "I dress to be comfortable";

        //        editProfile.SaveChangesButton_Click();
        //        profile = editProfile.Header.YourProfileHeaderLink_OldProfile_Click();

        //        profile.Header.Logout_Click();

        //        TearDown();

        //        log.Info("END TEST - Black/Regression/Member/EditProfile_WhoIAmTab");
        //    }
        //    catch (Exception e)
        //    {
        //        CleanupFailedTestcase(e);
        //        throw;
        //    }
        //}
        
        /// <summary>
        /// In order to verify pagination we need to ensure that our search results have enough results to span more than one page (12 results).
        /// Using a search of Located Within 160 miles of 90210 (Beverly Hills) for this test case.
        /// 
        /// Note:  This test case is unchanged from the JDate test case.
        /// </summary>
        [Test]
        public void Validate_QuickSearchPagination()
        {
            try
            {
                log.Info("START TEST - Black/Regression/Member/QuickSearchPagination");

                Setup();

                StartTest_UsingBlack();

                string login = System.Configuration.ConfigurationManager.AppSettings["Black_RegisteredUser1_Email"];
                string password = System.Configuration.ConfigurationManager.AppSettings["Black_RegisteredUser1_Password"];

                WebdriverAutomation.Tests.ReusableCode.LoggingInAndOut loggingInAndOut = new LoggingInAndOut();
                WebdriverAutomation.Framework.Site_Pages.Home home = loggingInAndOut.LogIn_ProfileComplete(driver, log, login, password);
                Framework.Site_Pages.QuickSearch quickSearch = home.Header.SearchHeaderLink_Click();

                string youreA = "Man";
                string seekingA = "Woman";
                string ageFrom = "20";
                string ageTo = "30";
                string locatedWithin = "200 Miles";
                string country = "USA";
                string zipCode = "90210";

                quickSearch.YoureADropdown = youreA;
                quickSearch.SeekingADropdown = seekingA;
                quickSearch.AgeFromTextbox = ageFrom;
                quickSearch.AgeToTextbox = ageTo;
                quickSearch.LocatedWithinDropdown = locatedWithin;
                quickSearch.EditLink_Click();
                quickSearch.EditOverlay_ZipCodeTab_Click();
                quickSearch.EditOverlay_ZipCodeTab_CountryDropdown = country;
                quickSearch.EditOverlay_ZipCodeTab_ZipCodeTextbox = zipCode;
                quickSearch.EditOverlay_SaveButton_Click();
                quickSearch.SearchButton_Click();

                quickSearch.GalleryLink_Click();

                // save all the usernames from our search in a list
                List<string> userNames1Through12 = quickSearch.GetAllNamesOfSearchResultsOnThisPage();
                Assert.IsFalse(userNames1Through12.Count < 12, "ERROR - QUICK SEARCH PAGE - We cannot test pagination because we are not getting enough results back in our quicksearch.  \r\nWe are currently searching for a " + seekingA + " between the ages of " + ageFrom + " to " + ageTo + " within " + locatedWithin + " miles of " + zipCode + ".\r\nThe number of search results returned:  " + userNames1Through12.Count);

                // go to the next page - users 13-24 and save those names in another list
                quickSearch = quickSearch.Pagination_13To24_Click();
                List<string> userNames13Through24 = quickSearch.GetAllNamesOfSearchResultsOnThisPage();

                // verify that none of the user names match - we truly have reached a new page
                bool doAnyNamesFromPage1MatchPage2 = quickSearch.DoAnyUserNamesMatchInOurLists(userNames1Through12, userNames13Through24);
                Assert.IsFalse(doAnyNamesFromPage1MatchPage2, "ERROR - QUICK SEARCH PAGE - After paging through to the 2nd page of results (13-24) a user from page 1 was found in page 2");

                // verify Previous link
                quickSearch = quickSearch.Pagination_PreviousLink_Click();
                List<string> userNamesOnCurrentPage = quickSearch.GetAllNamesOfSearchResultsOnThisPage();

                bool doALLNamesFromPage1MatchPage2 = quickSearch.DoAllUserNamesMatchInOurLists(userNames1Through12, userNamesOnCurrentPage);
                Assert.IsTrue(doALLNamesFromPage1MatchPage2, "ERROR - QUICK SEARCH PAGE - After navigating to page 2 (users 13-24) and clicking the Previous link the usernames we saved from page 1 do not match");

                // verify Next link
                quickSearch = quickSearch.Pagination_NextLink_Click();
                userNamesOnCurrentPage = quickSearch.GetAllNamesOfSearchResultsOnThisPage();

                doALLNamesFromPage1MatchPage2 = quickSearch.DoAllUserNamesMatchInOurLists(userNames13Through24, userNamesOnCurrentPage);
                Assert.IsTrue(doALLNamesFromPage1MatchPage2, "ERROR - QUICK SEARCH PAGE - After navigating to page 1 (users 1-12) and clicking the Next link the usernames we saved from page 2 do not match");

                // verify |< link
                quickSearch = quickSearch.Pagination_25To36_Click();
                quickSearch = quickSearch.Pagination_FirstPageLink_Click();
                userNamesOnCurrentPage = quickSearch.GetAllNamesOfSearchResultsOnThisPage();

                doALLNamesFromPage1MatchPage2 = quickSearch.DoAllUserNamesMatchInOurLists(userNames1Through12, userNamesOnCurrentPage);
                Assert.IsTrue(doALLNamesFromPage1MatchPage2, "ERROR - QUICK SEARCH PAGE - After navigating to page 3 (users 25-36) and clicking the |< link the usernames we saved from page 1 do not match");

                TearDown();

                log.Info("END TEST - Black/Regression/Member/QuickSearchPagination");
            }
            catch (Exception e)
            {
                CleanupFailedTestcase(e);
                throw;
            }
        }

        [Test]
        public void Validate_QuickSearchResults()
        {
            try
            {
                log.Info("START TEST - Black/Regression/Member/QuickSearchResults");

                Setup();

                StartTest_UsingBlack();

                string login = System.Configuration.ConfigurationManager.AppSettings["Black_RegisteredUser1_Email"];
                string password = System.Configuration.ConfigurationManager.AppSettings["Black_RegisteredUser1_Password"];

                WebdriverAutomation.Tests.ReusableCode.LoggingInAndOut loggingInAndOut = new LoggingInAndOut();
                WebdriverAutomation.Framework.Site_Pages.Home home = loggingInAndOut.LogIn_ProfileComplete(driver, log, login, password);
                Framework.Site_Pages.QuickSearch quickSearch = home.Header.SearchHeaderLink_Click();

                string youreA = "Man";
                string seekingA = "Woman";
                string ageFrom = "20";
                string ageTo = "30";
                string locatedWithin = "10 Miles";
                string country = "USA";
                string zipCode = "94133";

                // add all cities located within 10 miles of 94133 (San Francisco)
                List<string> cities = new List<string>();
                cities.Add("Alameda");
                cities.Add("Albany");
                cities.Add("Berkeley");
                cities.Add("Broadmoor Vlg");
                cities.Add("Daly City");
                cities.Add("El Cerrito");
                cities.Add("Emeryville");
                cities.Add("Mill Valley");
                cities.Add("Oakland");
                cities.Add("Piedmont");
                cities.Add("Pt Richmond");
                cities.Add("Richmond");
                cities.Add("S San Fran");
                cities.Add("San Francisco");
                cities.Add("Sausalito");
                cities.Add("South San Francisco");
                cities.Add("Tiburon");
                cities.Add("UC Berkeley");

                quickSearch.YoureADropdown = youreA;
                quickSearch.SeekingADropdown = seekingA;
                quickSearch.AgeFromTextbox = ageFrom;
                quickSearch.AgeToTextbox = ageTo;
                quickSearch.LocatedWithinDropdown = locatedWithin;
                quickSearch.EditLink_Click();
                quickSearch.EditOverlay_ZipCodeTab_Click();
                quickSearch.EditOverlay_ZipCodeTab_CountryDropdown = country;
                quickSearch.EditOverlay_ZipCodeTab_ZipCodeTextbox = zipCode;
                quickSearch.EditOverlay_SaveButton_Click();
                quickSearch.SearchButton_Click();

                quickSearch.GalleryLink_Click();

                // GALLERY VIEW - verify that quick search results appear
                Assert.True(quickSearch.SearchResultsCount_GalleryView() > 0, "ERROR - QUICK SEARCH PAGE - After doing a quick search in Gallery View, no results appeared.  \r\nThe search was for " + youreA + " seeking a " + seekingA + " searching within " + locatedWithin + " of " + zipCode);

                // GALLERY VIEW - verify that the results are all within the correct age range
                Assert.True(quickSearch.SearchResultsWithinAgeRange_GalleryView(Convert.ToInt32(ageFrom), Convert.ToInt32(ageTo)), "ERROR - QUICK SEARCH PAGE - After doing a quick search in Gallery View, the ages of the search results were not in the correct range.  \r\nThe search was for ages " + ageFrom + " to " + ageTo);

                // GALLERY VIEW - verify that the results are all within the correct zip code (checking for 10 miles within 94133 - San Francisco)
                string listOfCities = cities[0];

                for (int i = 1; i < cities.Count; i++)
                    listOfCities = listOfCities + ", " + cities[i];

                Assert.True(quickSearch.SearchResultsWithinExpectedCities_GalleryView(cities), "ERROR - QUICK SEARCH PAGE - After doing a quick search in Gallery View, the location range of the search result was not correct.  \r\nThe search was for " + locatedWithin + " within " + zipCode + " and the search results should have all returned one of the following cities: " + listOfCities);

                quickSearch.ListLink_Click();

                // LIST VIEW - verify that quick search results appear
                Assert.True(quickSearch.SearchResultsCount_ListView() > 0, "ERROR - QUICK SEARCH PAGE - After doing a quick search in List View, no results appeared.  \r\nThe search was for " + youreA + " seeking a " + seekingA + " searching within " + locatedWithin + " of " + zipCode);

                // LIST VIEW - verify that the results are all within the correct age range
                Assert.True(quickSearch.SearchResultsWithinAgeRange_ListView(Convert.ToInt32(ageFrom), Convert.ToInt32(ageTo)), "ERROR - QUICK SEARCH PAGE - After doing a quick search in List View, the ages of the search results were not in the correct range.");

                // LIST VIEW - verify that the results are all within the correct zip code (checking for 5 miles within 94133 - San Francisco)
                Assert.True(quickSearch.SearchResultsWithinExpectedCities_ListView(cities), "ERROR - QUICK SEARCH PAGE - After doing a quick search in List View, the location range of the search result was not correct.  \r\nThe search was for " + locatedWithin + " within " + zipCode + " and the search results should have all returned one of the following cities: " + cities[0] + ", " + cities[1] + ", " + cities[2] + ", " + cities[3] + ", " + cities[4] + ", " + cities[5] + ", " + cities[6] + ", " + cities[7] + ", " + cities[8] + ", " + cities[9] + ", " + cities[10] + ", " + cities[11] + ", " + cities[12]);

                // LIST VIEW - verify that the results show the correct gender seeking the correct gender
                Assert.True(quickSearch.SearchResultsIAmASeekingA_ListView(youreA, seekingA), "ERROR - QUICK SEARCH PAGE - After doing a quick search in List View, the gender preference was not correct.  \r\nThe search should have returned matches for " + seekingA + " seeking a " + youreA);

                TearDown();

                log.Info("END TEST - Black/Regression/Member/QuickSearchResults");
            }
            catch (Exception e)
            {
                CleanupFailedTestcase(e);
                throw;
            }
        }

        ///// <summary>
        ///// Setup:  The user that we login with for this test case should have exactly 1 photo currently uploaded. Else this test case will delete all other existing photos
        ///// before starting.
        ///// </summary>
        //[Test]
        //public void Validate_UploadAndEditPhotos()
        //{
        //    try
        //    {
        //        log.Info("START TEST - Black/Regression/Member/UploadAndEditPhotos");

        //        Setup();

        //        StartTest_UsingBlack();

        //        string login = System.Configuration.ConfigurationManager.AppSettings["Black_RegisteredUser2_Email"];
        //        string password = System.Configuration.ConfigurationManager.AppSettings["Black_RegisteredUser2_Password"];

        //        WebdriverAutomation.Tests.ReusableCode.LoggingInAndOut loggingInAndOut = new LoggingInAndOut();
        //        WebdriverAutomation.Framework.Site_Pages.Home home = loggingInAndOut.LogIn_ProfileComplete(driver, log, login, password);
        //        WebdriverAutomation.Framework.Site_Pages.YourProfile profile = home.Header.YourProfileHeaderLink_Click();
        //        Framework.Site_Pages.ManageYourPhotos manageYourPhotos = profile.ManageYourPhotosButton_Click();

        //        // delete all existing photos except for the first one
        //        while (manageYourPhotos.Photo2PhotoExists())
        //        {
        //            manageYourPhotos.Photo1XButton_Click();
        //            manageYourPhotos.SaveChangesButton_Click();
        //        }

        //        // grab a random picture in our solution's file path and put it in the Upload a Photo textbox
        //        string strFileName = string.Empty;
        //        Random random = new Random();

        //        // instead of grabbing our profile picture from the solution now we are grabbing it from a networked folder both QA and I can access:
        //        //   S:\Quality Assurance\Automation Builds\PicsForAutomation
        //        //FileInfo[] files = HelperMethods.GetReqFileInfo(@"*.jpg");
        //        //if (files.Length > 0)
        //        //    strFileName = files[random.Next(0, files.Length - 1)].FullName;
        //        strFileName = "s:\\Quality Assurance\\Automation Builds\\PicsForAutomation\\ProfilePicture.jpg";

        //        string photoCaption = "I look better in this photo";

        //        manageYourPhotos.Photo2UploadAPhotoTextbox_Write(strFileName);
        //        manageYourPhotos.Photo2CaptionTextarea = photoCaption;
        //        manageYourPhotos.SaveChangesButton_Click();

        //        // verify the text, "Profile successfully changed" appears
        //        Assert.True(manageYourPhotos.NotificationText == "Profile successfully changed", "ERROR - MANAGE YOUR PHOTOS PAGE - After Uploading a photo and pressing the Save Changes button, the 'Profile successfuly changed' text did not appear");

        //        // Delete and then Undelete the newly uploaded photo
        //        manageYourPhotos.Photo2XButton_Click();
        //        Assert.True(manageYourPhotos.Photo2Notification.Contains("Photo 2 and its caption will be deleted"), "ERROR - MANAGE YOUR PHOTOS PAGE - After Uploading a photo and pressing the Delete link for Photo 2, the 'Photo 2 and its caption wil be deleted...' text did not appear");
        //        manageYourPhotos.Photo2UndeleteLink_Click();
        //        manageYourPhotos.SaveChangesButton_Click();

        //        Assert.True(manageYourPhotos.Photo2PhotoExists(), "ERROR - MANAGE YOUR PHOTOS PAGE - After Uploading a photo and pressing the Save Changes button, we deleted the photo then immediately undeleted it and pressed Save again \r\nThe photo that was uploaded and then undeleted is not found");
        //        Assert.True(manageYourPhotos.Photo2CaptionTextarea == photoCaption, "ERROR - MANAGE YOUR PHOTOS PAGE - After Uploading a photo and pressing the Save Changes button, we deleted the photo then immediately undeleted it and pressed Save again \r\nThe caption that was uploaded previously is not the same. Previous caption value: " + photoCaption + ". The caption value now: " + manageYourPhotos.Photo1CaptionTextarea);

        //        // go to the Home Page then back to Manage Your Photos and verify that a photo still exists (the Upload a New Photo image does not) 
        //        //   and that the caption is still there
        //        home = manageYourPhotos.Header.HomeHeaderLink_Click();
        //        profile = home.Header.YourProfileHeaderLink_Click();
        //        manageYourPhotos = profile.ManageYourPhotosButton_Click();

        //        Assert.True(manageYourPhotos.Photo2PhotoExists(), "ERROR - MANAGE YOUR PHOTOS PAGE - After Uploading a photo and pressing the Save Changes button, we navigated back to the Home page and then back to the Manage Your Photos page. The photo that was uploaded previously cannot be found");
        //        Assert.True(manageYourPhotos.Photo2CaptionTextarea == photoCaption, "ERROR - MANAGE YOUR PHOTOS PAGE - After Uploading a photo and pressing the Save Changes button, we navigated back to the Home page and then back to the Manage Your Photos page. The caption that was uploaded previously is not the same. Previous caption value: " + photoCaption + ". The caption value now: " + manageYourPhotos.Photo1CaptionTextarea);

        //        // Reinitialize: Delete the photo we just added and verify that it really gets deleted
        //        manageYourPhotos.Photo2XButton_Click();
        //        manageYourPhotos.SaveChangesButton_Click();

        //        Assert.IsFalse(manageYourPhotos.Photo2PhotoExists(), "ERROR - MANAGE YOUR PHOTOS PAGE - After deleting a photo and pressing the Save Changes button, the photo was not deleted");

        //        manageYourPhotos.Header.Logout_Click();

        //        TearDown();

        //        log.Info("END TEST - Black/Regression/Member/UploadAndEditPhotos()");
        //    }
        //    catch (Exception e)
        //    {
        //        CleanupFailedTestcase(e);
        //        throw;
        //    }
        //}

        [Test]
        public void Validate_OmnitureExistsInLandingAndRegPages()
        {
            string errorLogNotes = string.Empty;

            try
            {
                log.Info("START TEST - Black/Regression/Member/OmnitureExistsInLandingAndRegPages");

                Setup();

                StartTest_UsingBlack();

                WebdriverAutomation.Framework.Site_Pages.Splash splash = new WebdriverAutomation.Framework.Site_Pages.Splash(driver);

                string omnitureSearchQuery1 = "s.prop";
                string omnitureSearchQuery2 = "s.eVar";

                if (!driver.PageSource.Contains(omnitureSearchQuery1) || !driver.PageSource.Contains(omnitureSearchQuery2))
                    Assert.Fail("In the Landing Page we are unable to find any omniture variables beginning with 's.prop' or 's.eVar' within the page source.");

                RegisterNewUser();

                if (!driver.PageSource.Contains(omnitureSearchQuery1) || !driver.PageSource.Contains(omnitureSearchQuery2))
                    Assert.Fail("In the Registration Complete Page we are unable to find any omniture variables beginning with 's.prop' or 's.eVar' within the page source.");

                //Remove the user we just created
                WebdriverAutomation.Tests.ReusableCode.AccountSettings accountSettings = new AccountSettings();
                accountSettings.RemoveProfile_SkippingUpdateProfileAndImportantNoticePages(driver, log);

                TearDown();

                log.Info("END TEST - Black/Regression/Member/OmnitureExistsInLandingAndRegPages");
            }
            catch (Exception e)
            {
                CleanupFailedTestcase(e, errorLogNotes);
                throw;
            }
        }

        private int AgeOfUser(string birthdayMonth, string birthdayDay, string birthdayYear)
        {
            string myDateTimeString = string.Empty;
            myDateTimeString = birthdayDay + " " + birthdayMonth + "," + birthdayYear;

            DateTime birthday;
            birthday = Convert.ToDateTime(myDateTimeString);

            DateTime now = DateTime.Today;
            int age = now.Year - birthday.Year;
            if (birthday > now.AddYears(-age))
                age--;

            return age;
        }

        private string RemoveAllWhitespacesFromString(string originalString)
        {
            return Regex.Replace(originalString, @"\s", "");
        }

        private bool AreValuesAndCountEqual(List<string> list1, List<string> list2)
        {
            if (list1.Count != list2.Count)
                return false;

            for (int i = 0; i < list1.Count; i++)
            {
                if (list1[i] != list2[i])
                    return false;
            }

            return true;
        }


        private void RegisterNewUser()
        {
            WebdriverAutomation.Framework.Site_Pages.Splash splash = new WebdriverAutomation.Framework.Site_Pages.Splash(driver);

            WebdriverAutomation.Framework.Site_Pages.Registration registration;

            registration = splash.BrowseForFreeLink_Click();

            // page 1                 
            registration.IAmA_NewRegNotOnBedrock_Select(1);
            registration.ContinueButton_NewRegNotOnBedrock_ClickUntilProgressBarChanges();

            // page 2
            registration.WhatIsYourHeight_NewRegNotOnBedrock_Select(3);
            registration.ContinueButton_NewRegNotOnBedrock_ClickUntilProgressBarChanges();

            // page 3
            registration.WhatIsYourBodyTypeListbox_NewRegNotOnBedrock_Select(5);
            registration.WhatIsYourEyeColorListbox_NewRegNotOnBedrock_Select(2);
            registration.WhatIsYourHairColorListbox_NewRegNotOnBedrock_Select(2);
            registration.ContinueButton_NewRegNotOnBedrock_ClickUntilProgressBarChanges();

            // page 4
            registration.WhatIsYourEthnicity_NewRegNotOnBedrock_Select_Select(3);
            registration.YourEducationLevel_NewRegNotOnBedrock_Select(2);
            registration.WhatIsYourOccupationTextbox_NewRegNotOnBedrock_Write("Chef");
            registration.ContinueButton_NewRegNotOnBedrock_ClickUntilProgressBarChanges();

            // page 5
            registration.YourSmokingHabitsDropdown_NewRegNotOnBedrock_Select(4);
            registration.YourDrinkingHabitsDropdown_NewRegNotOnBedrock_Select(2);
            registration.ContinueButton_NewRegNotOnBedrock_ClickUntilProgressBarChanges();

            // page 6
            registration.WhatIsYourMaritalStatusCheckboxes_NewRegNotOnBedrock_Check(2);
            registration.HowManyChildrenDoYouHaveListbox_NewRegNotOnBedrock_Select(2);
            registration.WhatIsYourReligiousBackground_NewRegNotOnBedrock_Select(2);
            registration.ContinueButton_NewRegNotOnBedrock_ClickUntilProgressBarChanges();

            // page 7
            registration.ZipCodeTextbox_NewRegNotOnBedrock_Write("90210");
            registration.ContinueButton_NewRegNotOnBedrock_ClickUntilProgressBarChanges();

            // page 8
            registration.FirstNameTextbox_NewRegNotOnBedrock_Write("Tim");
            registration.LastNameNameTextbox_NewRegNotOnBedrock_Write("Gill");
            registration.BirthDateMonthList_NewRegNotOnBedrock_Select(4);
            registration.BirthDateDayList_NewRegNotOnBedrock_Select(6);
            registration.BirthDateYearList_NewRegNotOnBedrock_Select(15);
            registration.ContinueButton_NewRegNotOnBedrock_ClickUntilProgressBarChanges();

            // page 9
            registration.UserNameTextbox_NewRegNotOnBedrock_Write("QAAutomation");
            registration.ChooseYourPasswordTextbox_NewRegNotOnBedrock_Write("automation");
            registration.ContinueButton_NewRegNotOnBedrock_ClickUntilProgressBarChanges();

            // page 10
            string emailAddress = "QAAutomation_" + DateTime.Now.ToString() + "@gmail.com";
            emailAddress = emailAddress.Replace("/", "_").Replace(" ", "_").Replace(":", "_");

            registration.EmailAddressTextbox_NewRegNotOnBedrock_Write(emailAddress);
            registration.ContinueButton_NewRegNotOnBedrock_ClickUntilProgressBarChanges();

            // page 11
            string describeMyself = "food lover.  art lover.  dance lover.  music lover...";
            registration.DescribeYourselfAndYourPersonalityTextArea_NewRegNotOnBedrock_Write(describeMyself);
            registration.ContinueButton_NewRegNotOnBedrock_ClickUntilProgressBarChanges();

            // page 19
            registration.IWouldLikeSpecialOffersAndAnnouncements_NewRegNotOnBedrock_Click();
            registration.ContinueButton_NewRegNotOnBedrock_Click();

            // registration complete
            WebdriverAutomation.Framework.Site_Pages.RegistrationComplete registrationComplete = new WebdriverAutomation.Framework.Site_Pages.RegistrationComplete(driver);

        }
    }
}
