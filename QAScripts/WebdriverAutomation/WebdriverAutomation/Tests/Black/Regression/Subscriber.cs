﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.Events;
using NUnit.Framework;
using log4net;
using WebdriverAutomation.Tests.ReusableCode;
using WebdriverAutomation.Framework.Site_Pages;

namespace WebdriverAutomation.Tests.Black.Regression
{
    [TestFixture]
    public class Subscriber : BaseTest
    {
        /// <summary>
        /// For Black Singles:
        /// A registered user who views any other profile and clicks on the Email button gets taken to the Subscribe Page.
        /// </summary>
        [Test]
        public void Validate_EMail_RegisteredUserToAnyUser()
        {
            try
            {
                log.Info("START TEST - Black/Regression/Member/EMail_RegisteredUserToAnyUser");

                Setup();

                StartTest_UsingBlack();

                string login = System.Configuration.ConfigurationManager.AppSettings["Black_RegisteredUser1_Email"];
                string password = System.Configuration.ConfigurationManager.AppSettings["Black_RegisteredUser1_Password"];

                WebdriverAutomation.Tests.ReusableCode.LoggingInAndOut loggingInAndOut = new LoggingInAndOut();
                WebdriverAutomation.Framework.Site_Pages.Home home = loggingInAndOut.LogIn_ProfileComplete(driver, log, login, password);

                // user 1 - search for user 2 (blacksub1@spark.net, a subscribed user)
                string user2MemberNumber = System.Configuration.ConfigurationManager.AppSettings["Black_SubscribedUser1_MemberID"];

                Framework.Site_Pages.LookUpMember lookUpMember = home.Header.LookUpMemberSubHeaderLink_Click();

                lookUpMember.MemberNumberTextbox = user2MemberNumber;
                Framework.Site_Pages.YourProfile yourProfile = lookUpMember.MemberNumberSearchButton_Click();

                // just verify that a non-subscibed user who tries to send an email gets taken to the Subscribe Page
                Framework.Site_Pages.Subscribe subscribe = yourProfile.UpgradeNowButton_Click();

                loggingInAndOut.LogOut(driver, log);

                TearDown();

                log.Info("END TEST - Black/Regression/Member/EMail_RegisteredUserToAnyUser");
            }
            catch (Exception e)
            {
                CleanupFailedTestcase(e);
                throw;
            }
        }

        /// <summary>
        /// For Black Singles:
        /// A subscribed user is allowed to send an email to anybody
        /// When the registered user gets the email they CAN enter their inbox and CAN see that they have emails waiting to be read. But they cannot see the subject and
        /// can only see the sender's member id. If they click on the email they are taken to the Subscribe Page.
        /// </summary>
        [Test]
        public void Validate_EMail_SubscribedUserToRegisteredUser()
        {
            try
            {
                log.Info("START TEST - Black/Regression/Member/EMail_SubscribedUserToRegisteredUser");

                Setup();

                StartTest_UsingBlack();

                string user1_login = System.Configuration.ConfigurationManager.AppSettings["Black_SubscribedUser1_Email"];
                string user1_password = System.Configuration.ConfigurationManager.AppSettings["Black_SubscribedUser1_Password"];
                string user1_Username = System.Configuration.ConfigurationManager.AppSettings["Black_SubscribedUser1_Username"];
                string user1_MemberID = System.Configuration.ConfigurationManager.AppSettings["Black_SubscribedUser1_MemberID"];

                string user2_login = System.Configuration.ConfigurationManager.AppSettings["Black_RegisteredUser1_Email"];
                string user2_password = System.Configuration.ConfigurationManager.AppSettings["Black_RegisteredUser1_Password"];
                string user2_Username = System.Configuration.ConfigurationManager.AppSettings["Black_RegisteredUser1_Username"];
                string user2_MemberID = System.Configuration.ConfigurationManager.AppSettings["Black_RegisteredUser1_MemberID"];

                // login as user 1 (a subscribed user)
                WebdriverAutomation.Tests.ReusableCode.LoggingInAndOut loggingInAndOut = new LoggingInAndOut();
                WebdriverAutomation.Framework.Site_Pages.Home home = loggingInAndOut.LogIn_ProfileComplete(driver, log, user1_login, user1_password);
                Framework.Site_Pages.QuickSearch quickSearch = home.Header.SearchHeaderLink_Click();

                // user 1 - search for user 2 (a registered user)
                Framework.Site_Pages.LookUpMember lookUpMember = quickSearch.Header.LookUpMemberSubHeaderLink_Click();

                lookUpMember.MemberNumberTextbox = user2_MemberID;
                Framework.Site_Pages.YourProfile yourProfile = lookUpMember.MemberNumberSearchButton_Click();

                // user 1 - message user 2          
                string subject = "sub to reg " + DateTime.Now.ToString();
                string message = "Validate EMail Subscribed User To Any User test case Message timestamp: " + DateTime.Now.ToString();

                Framework.Site_Pages.ComposeMessage composeMessage = yourProfile.EmailMeNowButton_Click();
                composeMessage.SubjectTextbox = subject;
                composeMessage.MessageTextarea = message;
                Framework.Site_Pages.MessageSent messageSent = composeMessage.SendButton_Click();

                // user 1 - verify that the "Your Message has been sent" text appears
                Assert.IsTrue(messageSent.VerificationMessage().Contains("Your Message has been sent"), "In the EMail_SubscribedUserToRegisteredUser test case, after Subscribed user1 emails Registered user2 the 'Your Message has been sent' message did not appear");

                // user 1 - log out user 1 and login user 2 (a non subscribed user)
                Framework.Site_Pages.Login login = loggingInAndOut.LogOut(driver, log);

                home = loggingInAndOut.LogIn_ProfileComplete(driver, log, user2_login, user2_password);

                // user 2 - go into the inbox and verify that the message just sent exists and that the subject cannot be seen
                Framework.Site_Pages.Messages messages = home.Header.InboxHeaderLink_Click();

                string genericSubject = "Email from a member";
                Assert.IsTrue(messages.DoesEmailExist(user1_MemberID, genericSubject), "In the EMail_SubscribedUserToRegisteredUser test case, after Subscribed user1 emails Registered user2 and user2 logs in, the email that was sent does not appear in user2's inbox.  \r\nWe are looking for an email with user1_Username: '" + user1_MemberID + "' and subject: '" + genericSubject + "'");

                // user 2 - click on the message and verify that the user is taken to the Subscribe Page
                Framework.Site_Pages.Subscribe subscribe = messages.EmailLink_Click_RegisteredUser(user1_MemberID, genericSubject);
                loggingInAndOut.LogOut(driver, log);

                TearDown();

                log.Info("END TEST - Black/Regression/Member/EMail_SubscribedUserToRegisteredUser");
            }
            catch (Exception e)
            {
                CleanupFailedTestcase(e);
                throw;
            }
        }

        /// <summary>
        /// For Black Singles:
        /// A subscribed user is allowed to send an email to anybody
        /// When the subscribed user gets the email they can read and send emails back without restrictions.
        /// </summary>
        [Test]
        public void Validate_EMail_SubscribedUserToSubscribedUser()
        {
            try
            {
                log.Info("START TEST - Black/Regression/Member/EMail_SubscribedUserToSubscribedUser");

                Setup();

                StartTest_UsingBlack();

                string user1_login = System.Configuration.ConfigurationManager.AppSettings["Black_SubscribedUser1_Email"];
                string user1_password = System.Configuration.ConfigurationManager.AppSettings["Black_SubscribedUser1_Password"];
                string user1_Username = System.Configuration.ConfigurationManager.AppSettings["Black_SubscribedUser1_Username"];
                string user1_MemberID = System.Configuration.ConfigurationManager.AppSettings["Black_SubscribedUser1_MemberID"];

                string user2_login = System.Configuration.ConfigurationManager.AppSettings["Black_SubscribedUser2_Email"];
                string user2_password = System.Configuration.ConfigurationManager.AppSettings["Black_SubscribedUser2_Password"];
                string user2_MemberID = System.Configuration.ConfigurationManager.AppSettings["Black_SubscribedUser2_MemberID"];

                // login as user 1 (a subscribed user)
                WebdriverAutomation.Tests.ReusableCode.LoggingInAndOut loggingInAndOut = new LoggingInAndOut();
                WebdriverAutomation.Framework.Site_Pages.Home home = loggingInAndOut.LogIn_ProfileComplete(driver, log, user1_login, user1_password);
                Framework.Site_Pages.QuickSearch quickSearch = home.Header.SearchHeaderLink_Click();

                // user 1 - search for user 2 (also a subscribed user)
                Framework.Site_Pages.LookUpMember lookUpMember = quickSearch.Header.LookUpMemberSubHeaderLink_Click();

                lookUpMember.MemberNumberTextbox = user2_MemberID;
                Framework.Site_Pages.YourProfile yourProfile = lookUpMember.MemberNumberSearchButton_Click();

                // user 1 - message user 2          
                string subject = "sub to sub " + DateTime.Now.ToString();
                string message = "Validate EMail Subscribed User To Subscribed User test case Message timestamp: " + DateTime.Now.ToString();

                Framework.Site_Pages.ComposeMessage composeMessage = yourProfile.EmailMeNowButton_Click();
                composeMessage.SubjectTextbox = subject;
                composeMessage.MessageTextarea = message;
                Framework.Site_Pages.MessageSent messageSent = composeMessage.SendButton_Click();

                // user 1 - verify that the "Your Message has been sent" text appears
                Assert.IsTrue(messageSent.VerificationMessage().Contains("Your Message has been sent"), "In the EMail_SubscribedUserToSubscribedUser test case, after Subscribed user1 emails Subscribed user2 the 'Your Message has been sent' message did not appear");

                // log out user 1 and login user 2
                Framework.Site_Pages.Login login = loggingInAndOut.LogOut(driver, log);

                home = loggingInAndOut.LogIn_ProfileComplete(driver, log, user2_login, user2_password);

                // user 2 - go into the inbox and verify that the message just sent exists
                Framework.Site_Pages.Messages messages = home.Header.InboxHeaderLink_Click();
                Assert.IsTrue(messages.DoesEmailExist(user1_MemberID, subject), "In the EMail_SubscribedUserToSubscribedUser test case, after Subscribed user1 emails Subscribed user2 and user2 logs in, the email that was sent does not appear in user2's inbox.  \r\nWe are looking for an email with user1's MemberID: '" + user1_MemberID + "' and subject: '" + subject + "'");

                // user 2 - verify that the message just sent has an unopened envelope icon next to it
                Assert.IsFalse(messages.IsEnvelopeIconOpened(user1_MemberID, subject), "In the EMail_SubscribedUserToSubscribedUser test case, after Subscribed user1 emails Subscribed user2 and user2 logs in, the envelope icon next to the email that was just sent does not appear as a closed envelope");

                // user 2 - click on the message and verify that the message sent is the same
                Framework.Site_Pages.Message messagePage = messages.EmailLink_Click(user1_MemberID, subject);
                Assert.IsTrue(messagePage.DoesEmailContainCorrectMessage(message), "In the EMail_SubscribedUserToSubscribedUser test case, after Subscribed user1 emails Subscribed user2 and user2 logs in, the email's MESSAGE that was sent is not the same message that was originally sent from user1.  \r\nOriginal message: '" + message + "'\r\nMessage currently onscreen: '" + messagePage.GetEmailMessage() + "'");

                // user 2 - read the message and verify that the subject and message are correct
                string allAccessEmailSubject = messagePage.GetEmailSubject();
                string allAccessEmailMessage = messagePage.GetEmailMessage();
                Assert.IsTrue(subject == allAccessEmailSubject, "In the EMail_SubscribedUserToSubscribedUser test case, after Subscribed user1 emails Registered user2 and user2 logs in and goes to their Inbox and checks the email, the Subject does not match the subject user1 originally sent.  \r\nOriginal subject: '" + subject + "' \r\nAll Access subject: '" + allAccessEmailSubject + "'");
                Assert.IsTrue(message == allAccessEmailMessage, "In the EMail_SubscribedUserToSubscribedUser test case, after Subscribed user1 emails Registered user2 and user2 logs in and goes to their Inbox and checks the email, the Message does not match the message user1 originally sent.  \r\nOriginal message: '" + message + "' \r\nAll Access message: '" + allAccessEmailMessage + "'");

                // user 2 - reply back to user1
                string replyMessage = "Validate EMail All Access Use To Registered User test case Message timestamp: " + DateTime.Now.ToString();

                Framework.Site_Pages.ComposeMessage compose = messagePage.ReplyForFreeButton_Click();
                compose.SubjectTextbox = subject;
                compose.MessageTextarea = replyMessage;
                messageSent = compose.SendButton_Click_ReplyingToEmail();

                // user 2 - verify that the "Your Message has been sent" text appears
                Assert.IsTrue(messageSent.VerificationMessage().Contains("Your Message has been sent"), "In the EMail_SubscribedUserToSubscribedUser test case, after Subscribed user1 emails Registered user2 the 'Your Message has been sent' message did not appear");

                // user 2 - verify that the message that was just opened now has an opened envelope icon next to it
                messages = messageSent.BackToInboxLink_Click();
                Assert.IsTrue(messages.IsEnvelopeIconOpened(user1_MemberID, subject), "In the EMail_SubscribedUserToSubscribedUser test case, after Subscribed user1 emails Subscribed user2 and user2 logs in and opens the message, the envelope icon next to the email that was just opened does not appear as a green opened envelope");

                // user 2 - reininitialize - delete all emails and logout
                messages.EmailSelectAllCheckboxes_Check();
                messages.DeleteLink_Click();

                // log out user 2 and login user 1 again
                login = loggingInAndOut.LogOut(driver, log);
                home = loggingInAndOut.LogIn_ProfileComplete(driver, log, user1_login, user1_password);

                // user 1 - go to the Inbox and verify that the message appears
                messages = home.Header.InboxHeaderLink_Click();
                Assert.IsTrue(messages.DoesEmailExist(user2_MemberID, subject), "In the EMail_SubscribedUserToSubscribedUser test case, after Subscribed user1 emails Subscribed user2 and user2 replies back, the email that was replied does not appear in user1's inbox.  \r\nWe are looking for an email with user2's MemberID: '" + user2_MemberID + "' and subject: '" + subject + "'");

                // user 2 - verify that the message just sent has an unopened envelope icon next to it
                Assert.IsFalse(messages.IsEnvelopeIconOpened(user2_MemberID, subject), "In the EMail_SubscribedUserToSubscribedUser test case, after Subscribed user1 emails Subscribed user2 and user2 replies back, the envelope icon next to the email that was replied sent does not appear as a closed envelope");

                // user 2 - click on the message and verify that the subject and message are correct
                messagePage = messages.EmailLink_Click(user2_MemberID, subject);
                Assert.IsTrue(messagePage.DoesEmailContainCorrectMessage(replyMessage), "In the EMail_SubscribedUserToSubscribedUser test case, after Subscribed user1 emails Subscribed user2 and user2 replies back, the email's MESSAGE that was replied is not the same message that was originally sent from user2  \r\nReply message that we are expecting: '" + replyMessage + "' \r\nMessage currently onscreen: '" + messagePage.GetEmailMessage() + "'");

                // user 2 - reininitialize - delete all All Access emails and logout
                messages = messagePage.BackToMessagesLink_Click();
                messages.EmailSelectAllCheckboxes_Check();
                messages.DeleteLink_Click();

                loggingInAndOut.LogOut(driver, log);

                TearDown();

                log.Info("END TEST - Black/Regression/Member/EMail_SubscribedUserToSubscribedUser");
            }
            catch (Exception e)
            {
                CleanupFailedTestcase(e);
                throw;
            }
        }


        [Test]
        public void Validate_YourAccountLinks()
        {
            try
            {
                log.Info("START TEST - Black/Regression/Member/YourAccountLinks");

                Setup();

                StartTest_UsingBlack();

                string login = System.Configuration.ConfigurationManager.AppSettings["Black_SubscribedUser1_Email"];
                string password = System.Configuration.ConfigurationManager.AppSettings["Black_SubscribedUser1_Password"];

                string yourAccountPageURL = string.Empty;

                if (driver.Url.Contains("preprod"))
                    yourAccountPageURL = @"http://preprod.blacksingles.com/Applications/MemberServices/MemberServices.aspx?NavPoint=top";
                else
                    yourAccountPageURL = @"http://www.blacksingles.com/Applications/MemberServices/MemberServices.aspx?NavPoint=top";

                WebdriverAutomation.Tests.ReusableCode.LoggingInAndOut loggingInAndOut = new LoggingInAndOut();
                WebdriverAutomation.Framework.Site_Pages.Home home = loggingInAndOut.LogIn_ProfileComplete(driver, log, login, password);

                WebdriverAutomation.Framework.Site_Pages.YourAccount yourAccount = home.Header.YourAccountLink_Click();

                WebdriverAutomation.Framework.Site_Pages.ProfileDisplaySettings profileDisplaySettings = yourAccount.ProfileDisplaySettingsLink_Click();
                yourAccount = profileDisplaySettings.Header.YourAccountLink_Click();

                //WebdriverAutomation.Framework.Site_Pages.YourProfile profile = yourAccount.ChangeYourProfileLink_Click();
                //yourAccount = profileDisplaySettings.Header.YourAccountLink_Click();

                //WebdriverAutomation.Framework.Site_Pages.ChangeYourEmailOrPassword changeYourEmailOrPassword = yourAccount.ChangeYourEmailOrPasswordLink_Click();
                //yourAccount = profileDisplaySettings.Header.YourAccountLink_Click();

                //WebdriverAutomation.Framework.Site_Pages.ColorCodeSettings colorCodeSettings = yourAccount.ColorCodeSettingsLink_Click();
                //yourAccount = profileDisplaySettings.Header.YourAccountLink_Click();

                //WebdriverAutomation.Framework.Site_Pages.Subscribe subscribe = yourAccount.MembershipPlansAndCostsLink_Click();
                //driver.Navigate().GoToUrl(yourAccountPageURL);

                //WebdriverAutomation.Framework.Site_Pages.AutoRenewalSettings autoRenewalSettings = yourAccount.AutoRenewalSettingsLink_Click();
                //yourAccount = profileDisplaySettings.Header.YourAccountLink_Click();

                //subscribe = yourAccount.SubscribeLink_Click();
                //driver.Navigate().GoToUrl(yourAccountPageURL);

                //WebdriverAutomation.Framework.Site_Pages.GetMoreAttentionByAddingPremiumServices premiumServices = yourAccount.PremiumServicesLink_Click();
                //driver.Navigate().GoToUrl(yourAccountPageURL);

                //// no Premium Services Settings link

                //WebdriverAutomation.Framework.Site_Pages.RemoveMyProfile removeMyProfile = yourAccount.RemoveMyProfileLink_Click();
                //yourAccount = profileDisplaySettings.Header.YourAccountLink_Click();

                //WebdriverAutomation.Framework.Site_Pages.AccountHistory accountHistory = yourAccount.AccountInformationLink_Click();
                //yourAccount = profileDisplaySettings.Header.YourAccountLink_Click();

                //WebdriverAutomation.Framework.Site_Pages.VerifyYourEmailAddress verifyEmail = yourAccount.VerifyEmailLink_Click();
                //yourAccount = profileDisplaySettings.Header.YourAccountLink_Click();

                WebdriverAutomation.Framework.Site_Pages.Settings chatAndIMSettings = yourAccount.ChatAndIMSettingsLink_Click();
                driver.Navigate().GoToUrl(yourAccountPageURL);

                //WebdriverAutomation.Framework.Site_Pages.MobileAlertPreferences mobileAlertPreferences = yourAccount.MobileAlertsLink_Click();
                //yourAccount = profileDisplaySettings.Header.YourAccountLink_Click();

                WebdriverAutomation.Framework.Site_Pages.MessageSettings messageSettings = yourAccount.MessageSettingsLink_Click();
                yourAccount = profileDisplaySettings.Header.YourAccountLink_Click();

                messageSettings = yourAccount.OffSiteMessageSettingsLink_Click();
                yourAccount = profileDisplaySettings.Header.YourAccountLink_Click();

                WebdriverAutomation.Framework.Site_Pages.WhosCheckingYouOut whosCheckingYouOut = yourAccount.MembersWhoEmailedYouLink_Click();
                yourAccount = profileDisplaySettings.Header.YourAccountLink_Click();

                WebdriverAutomation.Framework.Site_Pages.ContactingMembers contactingMembers = yourAccount.NeverMissEmailsLink_Click();
                yourAccount = profileDisplaySettings.Header.YourAccountLink_Click();

                whosCheckingYouOut = yourAccount.MembersBlockedFromContactingYouLink_Click();
                yourAccount = profileDisplaySettings.Header.YourAccountLink_Click();

                WebdriverAutomation.Framework.Site_Pages.YoureCheckingOut youreCheckingOut = yourAccount.MembersBlockedFromAppearingInYourSearchResultsLink_Click();
                yourAccount = profileDisplaySettings.Header.YourAccountLink_Click();

                whosCheckingYouOut = yourAccount.MembersBlockedFromViewingYourProfileInHisHerSearchResults_Click();
                yourAccount = profileDisplaySettings.Header.YourAccountLink_Click();

                WebdriverAutomation.Framework.Site_Pages.FrequentlyAskedQuestions faq = yourAccount.FAQLink_Click();
                yourAccount = profileDisplaySettings.Header.YourAccountLink_Click();

                WebdriverAutomation.Framework.Site_Pages.ContactUs contactUs = yourAccount.ContactUsLink_Click();
                yourAccount = profileDisplaySettings.Header.YourAccountLink_Click();

                WebdriverAutomation.Framework.Site_Pages.Privacy privacyStatement = yourAccount.PrivacyStatementLink_Click();
                yourAccount = profileDisplaySettings.Header.YourAccountLink_Click();

                privacyStatement = yourAccount.TermsAndConditionsOfServiceLink_Click();
                yourAccount = profileDisplaySettings.Header.YourAccountLink_Click();

                privacyStatement = yourAccount.TermsAndConditionsOfPurchaseLink_Click();
                yourAccount = profileDisplaySettings.Header.YourAccountLink_Click();

                TearDown();

                log.Info("END TEST - Black/Regression/Member/YourAccountLinks");
            }
            catch (Exception e)
            {
                CleanupFailedTestcase(e);
                throw;
            }
        }
    }
}
