﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.Events;
using NUnit.Framework;
using log4net;
using System.Threading;
using System.IO;
using System.Diagnostics;
using System.Reflection;
using System.Drawing.Imaging;
using System.Collections;

namespace WebdriverAutomation.Tests
{
    public abstract class BaseTest
    {
        public BaseTest()
        { }

        public IWebDriver driver;
        public const int timeoutValue_Implicit = 5;                 // in seconds
        public const int timeoutValue_miliseconds = 150;            // in miliseconds

        public static readonly ILog log = LogManager.GetLogger(typeof(BaseTest));
        public StringBuilder verificationErrors;

        public string nameOfTestCurrentlyRunning = string.Empty;
        public string nameOfBrowserCurrentlyRunning = string.Empty;
        public string datetimeOfFailedTestCase = string.Empty;
        public string siteCurrentlyBeingTested = string.Empty;
        public string resultsPath = @"C:\Automation Results\";    
        public const string ffEmptyBrowser = "about:blank";

        public string errorLogNotes = string.Empty;

        public void Setup()
        {
            // Error logging and reporting
            verificationErrors = new StringBuilder();
            log4net.Config.XmlConfigurator.Configure();
            StoreCurrentlyRunningTestName();

            // Driver switching
            driver = new FirefoxDriver();
            nameOfBrowserCurrentlyRunning = "Firefox";
            //driver = new InternetExplorerDriver();         
            //nameOfBrowserCurrentlyRunning = "Internet Explorer";
            //driver = new RemoteWebDriver(DesiredCapabilities.HtmlUnit());
            //nameOfBrowserCurrentlyRunning = "Html Unit";

            //ICapabilities desiredCapabilities = DesiredCapabilities.HtmlUnit();
            //IWebDriver driver = new RemoteWebDriver(desiredCapabilities);


            // Implicit waits are here so Ajax objects are clicked on AFTER they load
            driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(timeoutValue_Implicit));            
        }
        
        public void Setup_WithFirefoxCustomProfile()
        {
            string customProfileName = "default";
            //string customProfileName = "Webdriver";

            // Firefox only - set up specific profile
            FirefoxProfileManager allProfiles = new FirefoxProfileManager();
            FirefoxProfile profile = allProfiles.GetProfile(customProfileName);
            //profile.SetPreference("foo.bar", 23);
            try
            {
                driver = new FirefoxDriver(profile);
            }
            catch
            {
                Assert.Fail("Unable to launch an instance of custom firefox profile \"" + customProfileName + "\".  If an instance of this custom profile already is open, close it and then try again.");
            }

            // if multiple windows are open, close them 
            List<string> handlers = new List<string>();
            foreach (string handler in driver.WindowHandles)
                handlers.Add(handler);

            while (driver.WindowHandles.Count > 1)
            {
                // when we click on this link from the footer a new window appears. currently having problems switching pages in IE.  for now if we fail just continue
                try
                {
                    driver.SwitchTo().Window(handlers[1]);
                    driver.Close();
                    driver.SwitchTo().Window(handlers[0]);
                }
                catch
                {
                }
            }

            // start off test with empty browser
            driver.Navigate().GoToUrl(ffEmptyBrowser);

            // Implicit waits are here so Ajax objects are clicked on AFTER they load
            driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(timeoutValue_Implicit));

            // Error logging and reporting
            verificationErrors = new StringBuilder();
            log4net.Config.XmlConfigurator.Configure();
            StoreCurrentlyRunningTestName();
        }

        public void TearDown()
        {
            driver.Quit();

            Assert.AreEqual("", verificationErrors.ToString());
        }

        public void StartTest_UsingJDate()
        {
            siteCurrentlyBeingTested = "JDate";
            driver.Navigate().GoToUrl(System.Configuration.ConfigurationManager.AppSettings["JDate"]);
            WebdriverAutomation.Tests.ReusableCode.LoggingInAndOut loggingInAndOut = new WebdriverAutomation.Tests.ReusableCode.LoggingInAndOut();
            loggingInAndOut.LogOutIfExists_StartAtSplashPage(driver, log);            
        }

        public void StartTest_UsingJDate_Production()
        {
            siteCurrentlyBeingTested = "JDate_Production";
            driver.Navigate().GoToUrl(System.Configuration.ConfigurationManager.AppSettings["JDate_Production"]);
            WebdriverAutomation.Tests.ReusableCode.LoggingInAndOut loggingInAndOut = new WebdriverAutomation.Tests.ReusableCode.LoggingInAndOut();
            loggingInAndOut.LogOutIfExists_StartAtSplashPage(driver, log);            
        }

        public void StartTest_UsingJDate_WithDummyPRM()
        {
            siteCurrentlyBeingTested = "JDate_UsingDummyPRM";
            driver.Navigate().GoToUrl(System.Configuration.ConfigurationManager.AppSettings["JDate_UsingDummyPRM"]);
            WebdriverAutomation.Tests.ReusableCode.LoggingInAndOut loggingInAndOut = new WebdriverAutomation.Tests.ReusableCode.LoggingInAndOut();
            loggingInAndOut.LogOutIfExists_StartAtSplashPage(driver, log);
        }

        public void StartTest_UsingJDate_Production_WithDummyPRM()
        {
            siteCurrentlyBeingTested = "JDate_Production_UsingDummyPRM";
            driver.Navigate().GoToUrl(System.Configuration.ConfigurationManager.AppSettings["JDate_Production_UsingDummyPRM"]);
            WebdriverAutomation.Tests.ReusableCode.LoggingInAndOut loggingInAndOut = new WebdriverAutomation.Tests.ReusableCode.LoggingInAndOut();
            loggingInAndOut.LogOutIfExists_StartAtSplashPage(driver, log);
        }

        public void StartTest_UsingSpark()
        {
            siteCurrentlyBeingTested = "Spark";
            driver.Navigate().GoToUrl(System.Configuration.ConfigurationManager.AppSettings["Spark"]);
            WebdriverAutomation.Tests.ReusableCode.LoggingInAndOut loggingInAndOut = new WebdriverAutomation.Tests.ReusableCode.LoggingInAndOut();
            loggingInAndOut.LogOutIfExists_StartAtSplashPage(driver, log);            
        }

        public void StartTest_UsingSpark_Production()
        {
            siteCurrentlyBeingTested = "Spark_Production";
            driver.Navigate().GoToUrl(System.Configuration.ConfigurationManager.AppSettings["Spark_Production"]);
            WebdriverAutomation.Tests.ReusableCode.LoggingInAndOut loggingInAndOut = new WebdriverAutomation.Tests.ReusableCode.LoggingInAndOut();
            loggingInAndOut.LogOutIfExists_StartAtSplashPage(driver, log);            
        }

        public void StartTest_UsingSpark_WithDummyPRM()
        {
            siteCurrentlyBeingTested = "Spark_UsingDummyPRM";
            driver.Navigate().GoToUrl(System.Configuration.ConfigurationManager.AppSettings["Spark_UsingDummyPRM"]);
            WebdriverAutomation.Tests.ReusableCode.LoggingInAndOut loggingInAndOut = new WebdriverAutomation.Tests.ReusableCode.LoggingInAndOut();
            loggingInAndOut.LogOutIfExists_StartAtSplashPage(driver, log);
        }

        public void StartTest_UsingBlack()
        {
            siteCurrentlyBeingTested = "Black Singles";
            driver.Navigate().GoToUrl(System.Configuration.ConfigurationManager.AppSettings["Black"]);
            WebdriverAutomation.Tests.ReusableCode.LoggingInAndOut loggingInAndOut = new WebdriverAutomation.Tests.ReusableCode.LoggingInAndOut();
            loggingInAndOut.LogOutIfExists_StartAtSplashPage(driver, log);            
        }

        public void StartTest_UsingBlack_Production()
        {
            siteCurrentlyBeingTested = "Black Singles_Production";
            driver.Navigate().GoToUrl(System.Configuration.ConfigurationManager.AppSettings["Black_Production"]);
            WebdriverAutomation.Tests.ReusableCode.LoggingInAndOut loggingInAndOut = new WebdriverAutomation.Tests.ReusableCode.LoggingInAndOut();
            loggingInAndOut.LogOutIfExists_StartAtSplashPage(driver, log);            
        }

        public void StartTest_UsingBlack_WithDummyPRM()
        {
            siteCurrentlyBeingTested = "Black_UsingDummyPRM";
            driver.Navigate().GoToUrl(System.Configuration.ConfigurationManager.AppSettings["Black_UsingDummyPRM"]);
            WebdriverAutomation.Tests.ReusableCode.LoggingInAndOut loggingInAndOut = new WebdriverAutomation.Tests.ReusableCode.LoggingInAndOut();
            loggingInAndOut.LogOutIfExists_StartAtSplashPage(driver, log);
        }

        public void StartTest_UsingBBW()
        {
            siteCurrentlyBeingTested = "BBW";
            driver.Navigate().GoToUrl(System.Configuration.ConfigurationManager.AppSettings["BBW"]);
            WebdriverAutomation.Tests.ReusableCode.LoggingInAndOut loggingInAndOut = new WebdriverAutomation.Tests.ReusableCode.LoggingInAndOut();
            loggingInAndOut.LogOutIfExists_StartAtSplashPage(driver, log);            
        }

        public void StartTest_UsingBBW_Production()
        {
            siteCurrentlyBeingTested = "BBW_Production";
            driver.Navigate().GoToUrl(System.Configuration.ConfigurationManager.AppSettings["BBW_Production"]);
            WebdriverAutomation.Tests.ReusableCode.LoggingInAndOut loggingInAndOut = new WebdriverAutomation.Tests.ReusableCode.LoggingInAndOut();
            loggingInAndOut.LogOutIfExists_StartAtSplashPage(driver, log);            
        }

        public void StartTest_UsingBBW_WithDummyPRM()
        {
            siteCurrentlyBeingTested = "BBW_UsingDummyPRM";
            driver.Navigate().GoToUrl(System.Configuration.ConfigurationManager.AppSettings["BBW_UsingDummyPRM"]);
            WebdriverAutomation.Tests.ReusableCode.LoggingInAndOut loggingInAndOut = new WebdriverAutomation.Tests.ReusableCode.LoggingInAndOut();
            loggingInAndOut.LogOutIfExists_StartAtSplashPage(driver, log);
        }

        public void StartTest_UsingJDate_Co_Il()
        {
            siteCurrentlyBeingTested = "JDate_Co_Il";
            driver.Navigate().GoToUrl(System.Configuration.ConfigurationManager.AppSettings["JDate_Co_Il"]);
            WebdriverAutomation.Tests.ReusableCode.LoggingInAndOut loggingInAndOut = new WebdriverAutomation.Tests.ReusableCode.LoggingInAndOut();
            loggingInAndOut.LogOutIfExists_StartAtSplashPage(driver, log);            
        }

        public void StartTest_UsingJDate_Co_Il_Production()
        {
            siteCurrentlyBeingTested = "JDate_Co_Il_Production";
            driver.Navigate().GoToUrl(System.Configuration.ConfigurationManager.AppSettings["JDate_Co_Il_Production"]);
            WebdriverAutomation.Tests.ReusableCode.LoggingInAndOut loggingInAndOut = new WebdriverAutomation.Tests.ReusableCode.LoggingInAndOut();
            loggingInAndOut.LogOutIfExists_StartAtSplashPage(driver, log);            
        }

        public void StartTest_UsingJDate_Co_Il_WithDummyPRM()
        {
            siteCurrentlyBeingTested = "JDate_Co_Il_UsingDummyPRM";
            driver.Navigate().GoToUrl(System.Configuration.ConfigurationManager.AppSettings["JDate_Co_Il_UsingDummyPRM"]);
            WebdriverAutomation.Tests.ReusableCode.LoggingInAndOut loggingInAndOut = new WebdriverAutomation.Tests.ReusableCode.LoggingInAndOut();
            loggingInAndOut.LogOutIfExists_StartAtSplashPage(driver, log);
        }
        
        public void StartTest_UsingJDate_Mobile()
        {
            siteCurrentlyBeingTested = "JDate_Mobile"; 
            driver.Navigate().GoToUrl(System.Configuration.ConfigurationManager.AppSettings["JDate_Mobile"]);
            //WebdriverAutomation.Tests.ReusableCode.LoggingInAndOut loggingInAndOut = new WebdriverAutomation.Tests.ReusableCode.LoggingInAndOut();
            //loggingInAndOut.LogOutIfExists_StartAtSplashPage(driver, log);            
        }

        // 10/8/12 This is used to view the Mobile Landing Page and Registration Pages "faking" the browser to treat the PC as a mobile device. Otherwise we get sent to the web version of these pages.
        public void StartTest_UsingJDate_Mobile_UsingDummyPRM_AsMobileDevice()
        {
            siteCurrentlyBeingTested = "JDate_Mobile_UsingDummyPRM_AsMobileDevice";
            driver.Navigate().GoToUrl(System.Configuration.ConfigurationManager.AppSettings["JDate_Mobile_UsingDummyPRM_AsMobileDevice"]);
            //WebdriverAutomation.Tests.ReusableCode.LoggingInAndOut loggingInAndOut = new WebdriverAutomation.Tests.ReusableCode.LoggingInAndOut();
            //loggingInAndOut.LogOutIfExists_StartAtSplashPage(driver, log);
        }

        public void StartTest_UsingAdminTool()
        {
            siteCurrentlyBeingTested = "AdminTool";
            driver.Navigate().GoToUrl(System.Configuration.ConfigurationManager.AppSettings["AdminTool"]);            
        }

        public void StoreCurrentlyRunningTestName()
        {
            StackTrace st = new StackTrace();
            StackFrame sf = st.GetFrame(2);
            MethodBase currentMethodName = sf.GetMethod();
            string[] splitMethodName = currentMethodName.ToString().Split(' ');
            nameOfTestCurrentlyRunning = splitMethodName[1];
        }

        public void TakeAndSaveScreenshot()
        {
            ITakesScreenshot screenshotCapableDriver = driver as ITakesScreenshot;
            Screenshot screenImage = screenshotCapableDriver.GetScreenshot();

            string screenshotName = "FAIL - " + datetimeOfFailedTestCase + "  " + siteCurrentlyBeingTested + " - " + nameOfTestCurrentlyRunning + " - " + nameOfBrowserCurrentlyRunning;
            string screenshotPath = resultsPath + screenshotName + ".jpg";

            screenImage.SaveAsFile(screenshotPath, ImageFormat.Jpeg);
        }

        public void SaveErrorLog(Exception e)
        {
            string[] lines = { "ERROR MESSAGE", e.Message, "", "", "STACK TRACE", e.StackTrace, "", "", errorLogNotes };

            string errorLogName = "FAIL - " + datetimeOfFailedTestCase + "  " + siteCurrentlyBeingTested + " - " + nameOfTestCurrentlyRunning + " - " + nameOfBrowserCurrentlyRunning;
            string errorLogPath = resultsPath + errorLogName + ".txt";

            System.IO.File.WriteAllLines(errorLogPath, lines);
        }

        /// <summary>
        /// Does nothing if a folder in the same path as locationForResults already exists
        /// </summary>
        public void CreateFolderForResults()
        {
            try
            {
                // Determine whether the directory exists.
                if (Directory.Exists(resultsPath))
                    return;

                // Try to create the directory.
                DirectoryInfo di = Directory.CreateDirectory(resultsPath);
            }
            catch (Exception e)
            {
                Console.WriteLine("The process of creating a new directory for " + resultsPath + " failed: {0}", e.ToString());
            } 

        }

        public void CleanupFailedTestcase(Exception e)
        {
            datetimeOfFailedTestCase = DateTime.Now.ToString().Replace(':', '-').Replace('/', '-');

            Console.WriteLine(nameOfTestCurrentlyRunning + " " + datetimeOfFailedTestCase + "ERROR MSG : " + e.Message);
            Console.WriteLine(nameOfTestCurrentlyRunning + " " + datetimeOfFailedTestCase + "STACK TRACE : " + e.StackTrace);

            CreateFolderForResults();
            SaveErrorLog(e);
            TakeAndSaveScreenshot();
            TearDown();
        }

        public void CleanupFailedTestcase(Exception e, string errorLogNotes)
        {
            this.errorLogNotes = errorLogNotes;

            datetimeOfFailedTestCase = DateTime.Now.ToString().Replace(':', '-').Replace('/', '-');

            Console.WriteLine(nameOfTestCurrentlyRunning + " " + datetimeOfFailedTestCase + "ERROR MSG : " + e.Message);
            Console.WriteLine(nameOfTestCurrentlyRunning + " " + datetimeOfFailedTestCase + "STACK TRACE : " + e.StackTrace);
            Console.WriteLine(nameOfTestCurrentlyRunning + " " + datetimeOfFailedTestCase + "ADDITIONAL ERROR LOG NOTES : " + errorLogNotes);

            CreateFolderForResults();
            SaveErrorLog(e);
            TakeAndSaveScreenshot();
            TearDown();
        }

        public void SwitchToNewWindow(List<string> handlers)
        {
            foreach (string handler in driver.WindowHandles)
                handlers.Add(handler);

            string parentWindow = driver.CurrentWindowHandle;

            driver.SwitchTo().Window(handlers[1]);
        }

        public void CloseNewWindow(List<string> handlers)
        {
            driver.Close();
            driver.SwitchTo().Window(handlers[0]);
        }

        // trying to kill the survey that randomly appears in jdate.co.il
        public void GetRidOfJdateCoIlSurvey()
        {    
            if (driver.FindElements(By.XPath("//div[@id='k_popup']")).Count > 0)
            {
                IWebElement element = driver.FindElement(By.XPath("//div[@id='k_pop_yes_no']/a[@id='k_pop_no_btn']"));
                if (element.Displayed == true)
                    element.Click();
            }
        }

        ////  below: messing around with a different way of reporting
        //public List<TestCase> tcList = new List<TestCase>();
        //public int tcCounter = 0;       // increments as we get a Pass or a Fail to record our results in our Testcase struct

        //public struct TestCase
        //{
        //    public string Description;
        //    public string ExpectedResult;
        //    public string Result;
        //}

        //public void AddTestVerification(string Description, string ExpectedResult)
        //{           
        //    TestCase tc = new TestCase();
        //    tc.Description = Description;
        //    tc.ExpectedResult = ExpectedResult;

        //    tcList.Add(tc);
           
        //}

        //public void Pass()
        //{

        //}

        //public void Fail()
        //{

        //}

        

    }
}


