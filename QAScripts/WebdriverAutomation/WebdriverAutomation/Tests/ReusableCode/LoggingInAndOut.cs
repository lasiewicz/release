﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.Events;
using NUnit.Framework;
using log4net;
using System.Threading;

namespace WebdriverAutomation.Tests.ReusableCode
{
    public class LoggingInAndOut
    {
        public const int timeoutValue_Implicit = 5;                 // in seconds             

        /// <summary>
        /// Logs the user in and takes the user to the Edit Profile Page; 1 of 2 possible places where the user can end up.
        /// The user using this login method has not completely filled out their Edit Profile information.
        /// </summary>
        /// <param name="driver"></param>
        /// <param name="log"></param>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public WebdriverAutomation.Framework.Site_Pages.EditYourProfile LogIn_ProfileIncomplete(IWebDriver driver, ILog log, string username, string password)
        {
            // start at www.jdate.com/
            driver.Navigate().GoToUrl(System.Configuration.ConfigurationManager.AppSettings["JDate"]);
            WebdriverAutomation.Framework.Site_Pages.Splash splash = new WebdriverAutomation.Framework.Site_Pages.Splash(driver);

            // log in if not logged in already
            WebdriverAutomation.Framework.Site_Pages.Login login = splash.MemberLoginLink_Click();

            login.EmailTextbox = username;
            login.PasswordTextbox = password;

            // for some reason on this page sometimes the email/pw is not inputted correctly.  make sure it is before clicking Login
            if (login.EmailTextbox != username)
                login.EmailTextbox = username;

            if (login.PasswordTextbox != password)
                login.PasswordTextbox = password;

            login.LoginButton_Click(username);

            return new Framework.Site_Pages.EditYourProfile(driver);
        }

        /// <summary>
        /// Logs the user in and takes the user to the Edit Profile Page; 1 of 2 possible places where the user can end up.
        /// The user using this login method has already completely filled out their Edit Profile information, thus ends up on the Home page after logging in.
        /// </summary>
        /// <param name="driver"></param>
        /// <param name="log"></param>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public WebdriverAutomation.Framework.Site_Pages.Home LogIn_ProfileComplete(IWebDriver driver, ILog log, string username, string password)
        {
            if (driver.Url.Contains("www.jdate.com"))
                driver.Navigate().GoToUrl(System.Configuration.ConfigurationManager.AppSettings["JDate_Production"]);
            else if (driver.Url.Contains("jdate.com"))
                driver.Navigate().GoToUrl(System.Configuration.ConfigurationManager.AppSettings["JDate"]);
            else if (driver.Url.Contains("www.spark.com"))
                driver.Navigate().GoToUrl(System.Configuration.ConfigurationManager.AppSettings["Spark_Production"]);
            else if (driver.Url.Contains("spark.com"))
                driver.Navigate().GoToUrl(System.Configuration.ConfigurationManager.AppSettings["Spark"]);
            else if (driver.Url.Contains("www.blacksingles.com"))
                driver.Navigate().GoToUrl(System.Configuration.ConfigurationManager.AppSettings["Black_Production"]);
            else if (driver.Url.Contains("blacksingles.com"))
                driver.Navigate().GoToUrl(System.Configuration.ConfigurationManager.AppSettings["Black"]);
            else if (driver.Url.Contains("www.bbwpersonalsplus.com"))
                driver.Navigate().GoToUrl(System.Configuration.ConfigurationManager.AppSettings["BBW_Production"]);
            else if (driver.Url.Contains("bbwpersonalsplus.com"))
                driver.Navigate().GoToUrl(System.Configuration.ConfigurationManager.AppSettings["BBW"]);
            else if (driver.Url.Contains("www.jdate.co.il"))
                driver.Navigate().GoToUrl(System.Configuration.ConfigurationManager.AppSettings["JDate_Co_Il_Production"]);
            else if (driver.Url.Contains("jdate.co.il"))
            {
                driver.Navigate().GoToUrl(System.Configuration.ConfigurationManager.AppSettings["JDate_Co_Il"]);
                Thread.Sleep(1000);    // sometimes the log in controls in the upper left corner of the Splash Page don't show up immediately
            }
            else
                Assert.Fail("While trying to logout we look at the current URL to see which Splash Page we should navigate to. The current URL does not contain any of the keywords we are looking for. Current URL: " + driver.Url);

            WebdriverAutomation.Framework.Site_Pages.Splash splash = new WebdriverAutomation.Framework.Site_Pages.Splash(driver);

            // log in if not logged in already
            WebdriverAutomation.Framework.Site_Pages.Login login = splash.MemberLoginLink_Click();

            int maxLoopCount = 5;

            // try a few times to get it right
            int i = 0;
            while (login.EmailTextbox != username || login.PasswordTextbox != password)
            {
                login.EmailTextbox = username;
                login.PasswordTextbox = password;

                if (i == maxLoopCount)
                    Assert.Fail("We are unable to correctly type out the value for the login/password on the Login Page after " + maxLoopCount + " tries.  Stopping the test.");

                i++;
            }

            login.LoginButton_Click(username);

            // error checking for invalid email
            if (driver.FindElements(By.XPath("//div[contains(@id, '_divNotification')]")).Count > 0)
            {
                IWebElement element = driver.FindElement(By.XPath("//div[contains(@id, '_divNotification')]"));

                if (element.Text == "You've provided an invalid e-mail address")
                    Assert.Fail("We are trying to log in with the invalid username/password combination: " + username + "/" + password);
            }

            // JDATE ONLY - Sometimes if we try to log in the user is taken to the Smart JDaters Subscribe Page instead.  
            if (driver.Url.Contains("jdate.com"))
            {
                for (int j = 0; j < maxLoopCount; j++)
                {
                    if (driver.FindElements(By.XPath("//div[@id='sub-dont-go']/h1")).Count > 0)
                    {
                        IWebElement element = driver.FindElement(By.XPath("//a[contains(@id, '_lnkCancel')]"));
                        element.Click();
                    }
                    else
                        break;

                    if (j == maxLoopCount - 1)
                        Assert.Fail("We are trying to log out of the UPS Page and instead end up on the Don't Go Page.  We try to exit this page as well to log out " + maxLoopCount + " times and are unsuccessful.");
                }
            }

            return new Framework.Site_Pages.Home(driver);
        }

        /// <summary>
        /// If the user logs in for the first time before inputting their first name in the user is directed to the Please Update Your Profile page instead of the Home Page
        /// </summary>
        /// <param name="driver"></param>
        /// <param name="log"></param>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public WebdriverAutomation.Framework.Site_Pages.PleaseUpdateYourProfile LogIn_NoFirstName(IWebDriver driver, ILog log, string username, string password)
        {
            if (driver.Url.Contains("www.jdate.com"))
                driver.Navigate().GoToUrl(System.Configuration.ConfigurationManager.AppSettings["JDate_Production"]);
            else if (driver.Url.Contains("jdate.com"))
                driver.Navigate().GoToUrl(System.Configuration.ConfigurationManager.AppSettings["JDate"]);
            else if (driver.Url.Contains("www.spark.com"))
                driver.Navigate().GoToUrl(System.Configuration.ConfigurationManager.AppSettings["Spark_Production"]);
            else if (driver.Url.Contains("spark.com"))
                driver.Navigate().GoToUrl(System.Configuration.ConfigurationManager.AppSettings["Spark"]);
            else if (driver.Url.Contains("www.blacksingles.com"))
                driver.Navigate().GoToUrl(System.Configuration.ConfigurationManager.AppSettings["Black_Production"]);
            else if (driver.Url.Contains("blacksingles.com"))
                driver.Navigate().GoToUrl(System.Configuration.ConfigurationManager.AppSettings["Black"]);
            else if (driver.Url.Contains("www.bbwpersonalsplus.com"))
                driver.Navigate().GoToUrl(System.Configuration.ConfigurationManager.AppSettings["BBW_Production"]);
            else if (driver.Url.Contains("bbwpersonalsplus.com"))
                driver.Navigate().GoToUrl(System.Configuration.ConfigurationManager.AppSettings["BBW"]);
            else if (driver.Url.Contains("www.jdate.co.il"))
                driver.Navigate().GoToUrl(System.Configuration.ConfigurationManager.AppSettings["JDate_Co_Il_Production"]);
            else if (driver.Url.Contains("jdate.co.il"))
                driver.Navigate().GoToUrl(System.Configuration.ConfigurationManager.AppSettings["JDate_Co_Il"]);
            else
                Assert.Fail("While trying to logout we look at the current URL to see which Splash Page we should navigate to. The current URL does not contain any of the keywords we are looking for. Current URL: " + driver.Url);

            WebdriverAutomation.Framework.Site_Pages.Splash splash = new WebdriverAutomation.Framework.Site_Pages.Splash(driver);

            // log in if not logged in already
            WebdriverAutomation.Framework.Site_Pages.Login login = splash.MemberLoginLink_Click();

            int maxLoopCount = 5;

            // try a few times to get it right
            int i = 0;
            while (login.EmailTextbox != username || login.PasswordTextbox != password)
            {
                login.EmailTextbox = username;
                login.PasswordTextbox = password;

                if (i == maxLoopCount)
                    Assert.Fail("We are unable to correctly type out the value for the login/password on the Login Page after " + maxLoopCount + " tries.  Stopping the test.");

                i++;
            }

            login.LoginButton_Click(username);

            return new Framework.Site_Pages.PleaseUpdateYourProfile(driver);
        }

        public WebdriverAutomation.Framework.Site_Pages.Login LogOut(IWebDriver driver, ILog log)
        {
            // start at the Home Page
            if (driver.Url.Contains("www.jdate.com") || driver.Url.Contains("/secure.spark.net/jdatecom"))
                driver.Navigate().GoToUrl(System.Configuration.ConfigurationManager.AppSettings["JDate_Production"]);
            else if (driver.Url.Contains("jdate.com") || driver.Url.Contains("spark.net/jdatecom"))
                driver.Navigate().GoToUrl(System.Configuration.ConfigurationManager.AppSettings["JDate"]);
            else if (driver.Url.Contains("www.spark.com") || driver.Url.Contains("/secure.spark.net/sparkcom"))
                driver.Navigate().GoToUrl(System.Configuration.ConfigurationManager.AppSettings["Spark_Production"]);
            else if (driver.Url.Contains("spark.com") || driver.Url.Contains("spark.net/sparkcom"))
                driver.Navigate().GoToUrl(System.Configuration.ConfigurationManager.AppSettings["Spark"]);
            else if (driver.Url.Contains("www.blacksingles.com") || driver.Url.Contains("/secure.spark.net/blacksinglescom"))
                driver.Navigate().GoToUrl(System.Configuration.ConfigurationManager.AppSettings["Black_Production"]);
            else if (driver.Url.Contains("blacksingles.com") || driver.Url.Contains("spark.net/blacksinglescom"))
                driver.Navigate().GoToUrl(System.Configuration.ConfigurationManager.AppSettings["Black"]);
            else if (driver.Url.Contains("www.bbwpersonalsplus.com") || driver.Url.Contains("/secure.spark.net/bbwpersonalspluscom"))
                driver.Navigate().GoToUrl(System.Configuration.ConfigurationManager.AppSettings["BBW_Production"]);
            else if (driver.Url.Contains("bbwpersonalsplus.com") || driver.Url.Contains("spark.net/bbwpersonalspluscom"))
                driver.Navigate().GoToUrl(System.Configuration.ConfigurationManager.AppSettings["BBW"]);
            else if (driver.Url.Contains("www.jdate.co.il") || driver.Url.Contains("/secure.spark.net/jdatecoil"))
                driver.Navigate().GoToUrl(System.Configuration.ConfigurationManager.AppSettings["JDate_Co_Il_Production"]);
            else if (driver.Url.Contains("jdate.co.il") || driver.Url.Contains("spark.net/jdatecoil"))
                driver.Navigate().GoToUrl(System.Configuration.ConfigurationManager.AppSettings["JDate_Co_Il"]);
            else
                Assert.Fail("While trying to logout we look at the current URL to see which Splash Page we should navigate to. The current URL does not contain any of the keywords we are looking for. Current URL: " + driver.Url);

            // JDATE ONLY - Sometimes if we try to log out while we're at the Subscribe page the user is taken to the Don't Go Page instead.  
            if (driver.Url.Contains("jdate.com") || driver.Url.Contains("jdate.co.il"))
            {
                int maxLoopCount = 5;

                for (int i = 0; i < maxLoopCount; i++)
                {
                    if (driver.FindElements(By.XPath("//div[@id='sub-dont-go']/h1")).Count > 0)
                    {
                        IWebElement element = driver.FindElement(By.XPath("//a[contains(@id, '_lnkCancel')]"));
                        element.Click();
                    }
                    else
                        break;

                    if (i == maxLoopCount - 1)
                        Assert.Fail("We are trying to log out of the UPS Page and instead end up on the Don't Go Page.  We try to exit this page as well to log out " + maxLoopCount + " times and are unsuccessful.");
                }
            }

            WebdriverAutomation.Framework.Site_Pages.Home home = new WebdriverAutomation.Framework.Site_Pages.Home(driver);
            home.Footer.LogoutLink_Click();

            // sometimes for some reason we don't logout when clicked through automation.  keep clicking until the "logout" text disappears.
            for (int tenthOfSecond = 0; tenthOfSecond < 50; tenthOfSecond++)
            {
                if (home.Footer.IsLogoutLinkVisible())
                    home.Footer.LogoutLink_Click();
                else
                    break;

                Thread.Sleep(100);
            }

            return new Framework.Site_Pages.Login(driver);
        }

        /// <summary>
        /// Used for initializing the beginning of the test. 
        /// Logs out of any account that is currently logged in and starts the user off at the JDate Splash Page
        /// </summary>
        /// <param name="driver"></param>
        /// <param name="log"></param>
        /// <returns></returns>
        public void LogOutIfExists_StartAtSplashPage(IWebDriver driver, ILog log)
        {
            driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(1));

            // if any ads exist on the Home Page, kill them
            ReusableCode.Ads ads = new ReusableCode.Ads();
            ads.KillAllAds(driver, log);

            try
            {
                // exits trycatch once logout link cannot be found
                for (int tenthOfSecond = 0; tenthOfSecond < 50; tenthOfSecond++)
                {
                    IWebElement logoutLink = driver.FindElement(By.XPath("//div[@id='header-nav']/ul[@id='nav-auxiliary']/li[contains(@id, '_rptMenuSocialNav__ctl4_liMenu')]/a"));

                    logoutLink.Click();

                    Thread.Sleep(100);
                }
            }
            catch { }

            if (driver.Url.Contains("www.jdate.com/?prm=47896"))
                driver.Navigate().GoToUrl(System.Configuration.ConfigurationManager.AppSettings["JDate_Production_UsingDummyPRM"]);
            else if (driver.Url.Contains("www.jdate.com"))
                driver.Navigate().GoToUrl(System.Configuration.ConfigurationManager.AppSettings["JDate_Production"]);
            else if (driver.Url.Contains("jdate.com/?prm=47896"))
                driver.Navigate().GoToUrl(System.Configuration.ConfigurationManager.AppSettings["JDate_UsingDummyPRM"]);
            else if (driver.Url.Contains("jdate.com"))
                driver.Navigate().GoToUrl(System.Configuration.ConfigurationManager.AppSettings["JDate"]);
            else if (driver.Url.Contains("www.spark.com"))
                driver.Navigate().GoToUrl(System.Configuration.ConfigurationManager.AppSettings["Spark_Production"]);
            else if (driver.Url.Contains("spark.com/?prm=47896"))
                driver.Navigate().GoToUrl(System.Configuration.ConfigurationManager.AppSettings["Spark_UsingDummyPRM"]);
            else if (driver.Url.Contains("spark.com"))
                driver.Navigate().GoToUrl(System.Configuration.ConfigurationManager.AppSettings["Spark"]);
            else if (driver.Url.Contains("www.blacksingles.com"))
                driver.Navigate().GoToUrl(System.Configuration.ConfigurationManager.AppSettings["Black_Production"]);
            else if (driver.Url.Contains("blacksingles.com/?prm=47896"))
                driver.Navigate().GoToUrl(System.Configuration.ConfigurationManager.AppSettings["Black_UsingDummyPRM"]);
            else if (driver.Url.Contains("blacksingles.com"))
                driver.Navigate().GoToUrl(System.Configuration.ConfigurationManager.AppSettings["Black"]);
            else if (driver.Url.Contains("www.bbwpersonalsplus.com"))
                driver.Navigate().GoToUrl(System.Configuration.ConfigurationManager.AppSettings["BBW_Production"]);
            else if (driver.Url.Contains("bbwpersonalsplus.com/?prm=47896"))
                driver.Navigate().GoToUrl(System.Configuration.ConfigurationManager.AppSettings["BBW_UsingDummyPRM"]);
            else if (driver.Url.Contains("bbwpersonalsplus.com"))
                driver.Navigate().GoToUrl(System.Configuration.ConfigurationManager.AppSettings["BBW"]);
            else if (driver.Url.Contains("www.jdate.co.il"))
                driver.Navigate().GoToUrl(System.Configuration.ConfigurationManager.AppSettings["JDate_Co_Il_Production"]);
            else if (driver.Url.Contains("jdate.co.il/?prm=47896"))
                driver.Navigate().GoToUrl(System.Configuration.ConfigurationManager.AppSettings["JDate_Co_Il_UsingDummyPRM"]);
            else if (driver.Url.Contains("jdate.co.il"))
                driver.Navigate().GoToUrl(System.Configuration.ConfigurationManager.AppSettings["JDate_Co_Il"]);
            else
                Assert.Fail("While trying to logout we look at the current URL to see which Splash Page we should navigate to. The current URL does not contain any of the keywords we are looking for. Current URL: " + driver.Url);

            driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(timeoutValue_Implicit));
        }
    }

}
