﻿using System;
using System.Net;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Text.RegularExpressions;
using System.Threading;
using System.IO;

namespace WebdriverAutomation.Tests.ReusableCode
{
    public class HelperMethods
    {
        /// <summary>
        /// Returns all files of a particular extension in a FileInfo array
        /// </summary>
        /// <param name="lookFor"></param>
        /// <returns></returns>
        internal static FileInfo[] GetReqFileInfo(string lookFor)
        {
            string constFileName = System.Reflection.Assembly.GetExecutingAssembly().EscapedCodeBase.ToLower();
            constFileName = constFileName.Replace("file:///", "").Replace("/", "\\");
            constFileName = constFileName.Replace("%20", " "); //
            constFileName = constFileName.ToLower().Replace("webdriverautomation.dll", "");

            constFileName = constFileName.Replace("\\bin\\x86\\debug", "");
            constFileName = constFileName.Replace("\\bin\\debug", "");
            constFileName = constFileName.Replace("\\bin\\release", "");

            DirectoryInfo di = new DirectoryInfo(constFileName);
            ArrayList myfileinfos = new ArrayList();

            string[] extensions = lookFor.Split(new char[] { ';' });

            foreach (string ext in extensions)
            {
                myfileinfos.AddRange(di.GetFiles(ext, SearchOption.AllDirectories));
            }

            FileInfo[] files = (FileInfo[])myfileinfos.ToArray(typeof(FileInfo));
            return files;
        }
    }
}
