﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.Events;
using NUnit.Framework;
using log4net;

namespace WebdriverAutomation.Tests.ReusableCode
{
    public class Footers
    {
        /// <summary>
        /// Goes through ALL the footers while logged in and verifies that they arrive at the correct pages
        /// </summary>
        /// <param name="driver"></param>
        /// <param name="log"></param>
        public void ValidateFooters_NotLoggedIn(IWebDriver driver, ILog log)
        {
            string loginPageURL = string.Empty;
            
            if (driver.Url.Contains("preprod"))
                loginPageURL = @"http://preprod.jdate.com/Applications/Logon/Logon.aspx";
            else
                loginPageURL = @"http://www.jdate.com/Applications/Logon/Logon.aspx";

            WebdriverAutomation.Framework.Site_Pages.Splash splash = new WebdriverAutomation.Framework.Site_Pages.Splash(driver);

            // the Splash page does not have the footer anymore so we will use the Login page's footers to test
            Framework.Site_Pages.Login login = splash.MemberLoginLink_Click();

            //// click Home (Splash) link
            //splash = login.Footer.HomeLink_Click();
            //login = splash.MemberLoginLink_Click();            

            //// click Contact Us link
            //WebdriverAutomation.Framework.Site_Pages.ContactUs contactUs = login.Footer.ContactUsLink_Click();

            //// click Login link
            //login = contactUs.Footer.LoginLink_Click();

            //// click Your Account link
            //WebdriverAutomation.Framework.Site_Pages.Registration yourAccount = login.Footer.YourAccountLink_LoggedOut_Click();
            //driver.Navigate().GoToUrl(loginPageURL);

            //// click Verify Email link
            //WebdriverAutomation.Framework.Site_Pages.Registration verifyEmail = login.Footer.VerifyEmailLink_LoggedOut_Click();
            //driver.Navigate().GoToUrl(loginPageURL);

            //// click Subscribe link
            //WebdriverAutomation.Framework.Site_Pages.Registration subscribe = login.Footer.SubscribeLink_LoggedOut_Click();
            //driver.Navigate().GoToUrl(loginPageURL);

            // click About JDate link
            WebdriverAutomation.Framework.Site_Pages.AboutJDate aboutJDate = login.Footer.AboutJDateLink_Click();
            driver.Navigate().GoToUrl(loginPageURL);

            // click Gift of JDate link
            WebdriverAutomation.Framework.Site_Pages.GiftOfJDate giftOfJDate = login.Footer.GiftOfJDateLink_Click();
            driver.Navigate().GoToUrl(loginPageURL);

            // click Video link
            WebdriverAutomation.Framework.Site_Pages.Video video = login.Footer.VideoLink_Click();
            driver.Navigate().GoToUrl(loginPageURL);

            // click JMag link
            WebdriverAutomation.Framework.Site_Pages.JMag jMag = login.Footer.JMagLink_Click();
            driver.Navigate().GoToUrl(loginPageURL);

            // click JBlog link
            WebdriverAutomation.Framework.Site_Pages.JBlog jBlog = login.Footer.JBlogLink_Click();
            driver.Navigate().GoToUrl(loginPageURL);

            // click Travel & Events link
            WebdriverAutomation.Framework.Site_Pages.TravelAndEvents travelAndEvents = login.Footer.TravelAndEventsLink_Click();
            driver.Navigate().GoToUrl(loginPageURL);

            // click Jewish Holiday Calendar link
            WebdriverAutomation.Framework.Site_Pages.JewishHolidayCalendar jewishHolidayCalendar = login.Footer.JewishHolidayCalendarLink_Click();
            driver.Navigate().GoToUrl(loginPageURL);

            // click Synagogue Directory link
            WebdriverAutomation.Framework.Site_Pages.SynagogueDirectory synagogueDirectory = login.Footer.SynagogueDirectoryLink_Click();
            driver.Navigate().GoToUrl(loginPageURL);

            // click Our Mission link
            WebdriverAutomation.Framework.Site_Pages.OurMission ourMission = login.Footer.OurMissionLink_Click();
            driver.Navigate().GoToUrl(loginPageURL);

            // click Safety link
            WebdriverAutomation.Framework.Site_Pages.Safety safety = login.Footer.SafetyLink_Click();
            driver.Navigate().GoToUrl(loginPageURL);

            // click Help & Advice link
            WebdriverAutomation.Framework.Site_Pages.HelpAndAdvice helpAndAdvice = login.Footer.HelpAndAdviceLink_Click();
            driver.Navigate().GoToUrl(loginPageURL);

            // click Site Map link
            WebdriverAutomation.Framework.Site_Pages.SiteMap siteMap = login.Footer.SiteMapLink_Click();
            driver.Navigate().GoToUrl(loginPageURL);

            // click About JDate Success link
            WebdriverAutomation.Framework.Site_Pages.JDateSuccess aboutSparkNetworks = login.Footer.JDateSuccessLink_Click();
            driver.Navigate().GoToUrl(loginPageURL);

            // click Spark Networks' Sites link
            WebdriverAutomation.Framework.Site_Pages.SparkNetworksSites sparkNetworksSites = login.Footer.SparkNetworksSitesLink_Click();
            driver.Navigate().GoToUrl(loginPageURL);

            // click Advertise With Us link
            WebdriverAutomation.Framework.Site_Pages.AdvertiseWithUs advertiseWithUs = login.Footer.AdvertiseWithUsLink_Click();
            driver.Navigate().GoToUrl(loginPageURL);

            // click Affiliate Program link
            WebdriverAutomation.Framework.Site_Pages.AffiliateProgram affiliateProgram = login.Footer.AffiliateProgramLink_Click();
            driver.Navigate().GoToUrl(loginPageURL);

            // click Investor Relations link
            WebdriverAutomation.Framework.Site_Pages.InvestorRelations investorRelations = login.Footer.InvestorRelationsLink_Click();
            driver.Navigate().GoToUrl(loginPageURL);

            // click Jobs link
            WebdriverAutomation.Framework.Site_Pages.Jobs jobs = login.Footer.JobsLink_Click();
            driver.Navigate().GoToUrl(loginPageURL);

            // click Privacy link
            WebdriverAutomation.Framework.Site_Pages.Privacy privacy = login.Footer.PrivacyLink_Click();
            driver.Navigate().GoToUrl(loginPageURL);

            // click Terms of Service link
            WebdriverAutomation.Framework.Site_Pages.TermsOfService termsOfService = login.Footer.TermsOfServiceLink_Click();
            driver.Navigate().GoToUrl(loginPageURL);

            // click Local Online Dating link
            WebdriverAutomation.Framework.Site_Pages.LocalOnlineDating localOnlineDating = login.Footer.LocalOnlineDatingLink_Click();
            driver.Navigate().GoToUrl(loginPageURL);

            // click Jewish Singles link
            WebdriverAutomation.Framework.Site_Pages.JewishSingles jewishSingles = login.Footer.JewishSinglesLink_Click();

            // click Our Intellectual Property link
            WebdriverAutomation.Framework.Site_Pages.OurIntellectualProperty ourIntellectualProperty = login.Footer.OurIntellectualPropertyLink_Click();
            driver.Navigate().GoToUrl(loginPageURL);

        }

        /// <summary>
        /// Goes through ONLY the footers which lead the user to different locations or appear when logged in as opposed to logged out
        /// Includes: Your Account, Verify Email, Subscribe, JDate Mobile, Logout
        /// </summary>
        /// <param name="driver"></param>
        /// <param name="log"></param>
        public void ValidateFooters_LoggedIn(IWebDriver driver, ILog log)
        {
            string aboutJDatePageURL = string.Empty;
            
            if (driver.Url.Contains("preprod"))
                aboutJDatePageURL = @"http://preprod.jdate.com/Applications/Article/ArticleView.aspx?CategoryID=1998&HideNav=True";
            else
                aboutJDatePageURL = @"http://www.jdate.com/Applications/Article/ArticleView.aspx?CategoryID=1998&HideNav=True";

            string registeredUserName = System.Configuration.ConfigurationManager.AppSettings["JDate_RegisteredUser1_Email"];
            string registeredUserPassword = System.Configuration.ConfigurationManager.AppSettings["JDate_RegisteredUser1_Password"];

            LoggingInAndOut loggingInAndOut = new LoggingInAndOut();

            WebdriverAutomation.Framework.Site_Pages.Home home = loggingInAndOut.LogIn_ProfileComplete(driver, log, registeredUserName, registeredUserPassword);

            // start off on a page that has the footer but is less heavy than the Home page
            WebdriverAutomation.Framework.Site_Pages.AboutJDate aboutJDate = home.Footer.AboutJDateLink_Click();

            // click Your Account link
            WebdriverAutomation.Framework.Site_Pages.YourAccount yourAccount = aboutJDate.Footer.YourAccountLink_LoggedIn_Click();
            //driver.Navigate().Back();
            driver.Navigate().GoToUrl(aboutJDatePageURL);

            // click Verify Email link
            WebdriverAutomation.Framework.Site_Pages.VerifyEmail verifyEmail = aboutJDate.Footer.VerifyEmailLink_LoggedIn_Click();
            driver.Navigate().GoToUrl(aboutJDatePageURL);

            // click Subscribe link
            WebdriverAutomation.Framework.Site_Pages.Subscribe subscribe = aboutJDate.Footer.SubscribeLink_LoggedIn_Click();
            driver.Navigate().GoToUrl(System.Configuration.ConfigurationManager.AppSettings["JDate"]);
            home = new Framework.Site_Pages.Home(driver);

            // click Logout link
            WebdriverAutomation.Framework.Site_Pages.Login login = aboutJDate.Footer.LogoutLink_Click();

            driver.Navigate().GoToUrl(System.Configuration.ConfigurationManager.AppSettings["JDate"]);
        }
    }


}
