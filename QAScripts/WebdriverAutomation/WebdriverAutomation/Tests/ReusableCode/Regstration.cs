﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.Events;
using NUnit.Framework;
using log4net;
using System.Threading;
using System.IO;

namespace WebdriverAutomation.Tests.ReusableCode
{
    public class Regstration
    {

        /// <summary>
        /// Start: user is LOGGED OUT and on the SPLASH page.
        /// End: user on the REGISTRATION COMPLETE page.
        /// </summary>
        /// <param name="driver"></param>
        /// <param name="log"></param>
        public void RegisterNewUser(IWebDriver driver, ILog log)
        {
            WebdriverAutomation.Framework.Site_Pages.Splash splash = new WebdriverAutomation.Framework.Site_Pages.Splash(driver);
            WebdriverAutomation.Framework.Site_Pages.Registration registration;

            #region Spark Registration - nothing on splash page
            if (driver.Url.Contains("spark.com"))
            {
                registration = splash.BrowseForFreeLink_Click();

                // page 1
                registration.YouAreAListBox_RegPathAandD_Select(2);
                registration.ContinueButton_Click();

                // page 2
                registration.WhatIsYourCurrentRelationshipStatus_RegPathAandD_Select(2);
                registration.ContinueButton_Click();

                // page 3
                registration.WhatIsYourEthnicity_SparkReg_Select(1);
                registration.ContinueButton_Click();

                // page 4
                registration.WhatIsYourReligiousBackground_SparkReg_Select(2);
                registration.ContinueButton_Click();

                // page 5
                registration.ZipCodeTextbox_Write("90210");
                registration.ContinueButton_OnZipCodePage_SparkReg_Click();

                // page 6
                registration.UserNameTextbox_Write("QAAutomation");

                registration.BirthDateMonthList_Select(4);
                registration.BirthDateDayList_Select(4);
                registration.BirthDateYearList_Select(4);

                registration.ContinueButton_Click();

                // page 7
                string emailAddress = "QAAutomation_" + DateTime.Now.ToString() + "@gmail.com";
                emailAddress = emailAddress.Replace("/", "_").Replace(" ", "_").Replace(":", "_");
                registration.EmailAddressTextbox_Write(emailAddress);

                registration.ChooseYourPasswordTextbox_Write("Automation");

                registration.ContinueButton_Click();

                // page 8
                registration.IWouldLikeSpecialOffersAndAnnouncements_Click();

                registration.ContinueButton_Click();

                // registration complete
                WebdriverAutomation.Framework.Site_Pages.RegistrationComplete registrationComplete = new WebdriverAutomation.Framework.Site_Pages.RegistrationComplete(driver);
            }
            #endregion
            #region Black Singles Registration - I am a, Age Range, Country, Zip Code and Email Address splash page - s.pageName = "Splash Page Scenario 22 - GenderMask,AgeRange,RegionID,EmailAddress";
            else if (driver.Url.Contains("blacksingles.com"))
            {
                string zipCode = "90210";

                splash.YouAreAList_Select(1);
                splash.AgeRangeList_Select(1);
                splash.ZipCodeTextbox_Write(zipCode);

                string emailAddress = "QAAutomation_" + DateTime.Now.ToString() + "@gmail.com";
                emailAddress = emailAddress.Replace("/", "_").Replace(" ", "_").Replace(":", "_");
                splash.EmailAddressTextbox_Write(emailAddress);

                registration = splash.BrowseForFreeButton_Click();

                if (registration.IsIAmAOnscreenCurrently_AltRegPage())
                {
                    // page 1                 
                    registration.IAmA_AltRegPage_Select(2);
                    registration.ContinueButton_AltRegPage_Click();
                }

                // page 2
                registration.WhatIsYourHeight_Select(3);

                // page 3
                registration.WhatIsYourBodyTypeListbox_Select(5);

                // page 4
                registration.WhatIsYourEyeColorListbox_Select(3);

                // page 5
                registration.WhatIsYourHairColorListbox_Select(4);

                // page 6
                registration.WhatIsYourEthnicity_Select(4);

                // page 7
                registration.YourEducationLevelListbox_AltRegPage_Select(3);

                // page 8
                registration.WhatIsYourOccupationTextbox_Write("Chef");
                registration.ContinueButton_AltRegPage_Click();

                // page 9
                registration.YourSmokingHabitsListbox_AltRegPage_Select(2);

                // page 10
                registration.YourDrinkingHabitsListbox_AltRegPage_Select(2);

                // page 11
                registration.WhatIsYourMaritalStatusDropdown_AltRegPage_Click(2);

                // page 12
                registration.HowManyChildrenDoYouHaveListbox_Select(1);

                // page 13
                registration.WhatIsYourReligiousBackground_AltRegPage_Select(3);

                // page 14 - Country and Zip Code should already be filled out with the values we inputted in the Splash Page
                Thread.Sleep(1000);   // to give time to let dropdown autopopulate correctly
                string countryDropdownValue = registration.CountryDropdown;
                Assert.IsTrue(countryDropdownValue == "USA", "In the Black Singles Validate_Registration test case, we input the country 'United States' in the Splash page and we expect for the Country dropdown to be autopopulated with the same value during the registration process. Instead of 'USA', the country dropdown shows the value: '" + countryDropdownValue + "'");
                Assert.IsTrue(registration.ZipCodeTextbox == zipCode, "In the Black Singles Validate_Registration test case, we input the zip code '90210' in the Splash page and we expect for the Zip Code textbox to be autopopulated with the same value during the registration process. Instead of '90210', the zip code textbox shows the value: '" + registration.ZipCodeTextbox + "'");
                registration.ContinueButton_AltRegPage_Click();

                // page 15
                registration.FirstNameTextbox_Write("Tim");
                registration.LastNameTextbox_Write("Gill");
                registration.BirthDateMonthList_Select(4);
                registration.BirthDateDayList_Select(6);
                registration.BirthDateYearList_Select(15);
                registration.ContinueButton_AltRegPage_Click();

                // page 16
                registration.UserNameTextbox_Write("QAAutomation");
                registration.ChooseYourPasswordTextbox_Write("automation");
                registration.ContinueButton_AltRegPage_Click();

                // page 17 - Email Address should already be filled out with the value we inputted in the Splash Page
                Assert.IsTrue(registration.EmailAddress == emailAddress, "In the Black Singles Validate_Registration test case, we input the email address " + emailAddress + " in the Splash page and we expect for the email address textbox to be autopopulated with the same value during the registration process. Instead of '" + emailAddress + "', the country dropdown shows the value: '" + registration.EmailAddress + "'");
                registration.ConfirmEmailTextbox_Write(emailAddress);
                registration.ContinueButton_AltRegPage_Click();

                // page 18
                string describeMyself = "food lover.  art lover.  dance lover.  music lover...";
                registration.DescribeYourselfAndYourPersonalityTextArea_Write(describeMyself);
                registration.ContinueButton_AltRegPage_Click();

                // page 19
                registration.IWouldLikeSpecialOffersAndAnnouncements_Click();
                registration.ContinueButton_AltRegPage_Click();

                // registration complete
                WebdriverAutomation.Framework.Site_Pages.RegistrationComplete registrationComplete = new WebdriverAutomation.Framework.Site_Pages.RegistrationComplete(driver);

            }
            #endregion
            #region BBW Registration - nothing on splash page
            else if (driver.Url.Contains("bbwpersonalsplus.com"))
            {
                registration = splash.BrowseForFreeLink_Click();

                string emailAddress = string.Empty;

                // page 1
                registration.YouAreAListBox_RegPathAandD_Select(2);
                registration.ContinueButton_ClickUntilProgressBarChanges();

                // page 2
                registration.WhatIsYourHeight_Select(3);
                registration.ContinueButton_ClickUntilProgressBarChanges();

                // page 3
                registration.WhatIsYourBodyTypeListbox_Select(2);
                registration.ContinueButton_ClickUntilProgressBarChanges();

                // page 4
                registration.WhatIsYourEyeColorListbox_Select(3);
                registration.ContinueButton_ClickUntilProgressBarChanges();

                // page 5
                registration.WhatIsYourHairColorListbox_Select(3);
                //registration.ContinueButton_ClickUntilProgressBarChanges();

                // page 6
                registration.WhatIsYourEthnicity_Select(3);
                //registration.ContinueButton_ClickUntilProgressBarChanges();

                // page 7
                registration.YourEducationLevel_RegPathBandC_Select(4);
                //registration.ContinueButton_ClickUntilProgressBarChanges();

                // page 8
                registration.WhatIsYourOccupationTextbox_Write("Underwater basketweaver");
                registration.ContinueButton_ClickUntilProgressBarChanges();

                // page 9
                registration.YourSmokingHabitsDropdown_RegPathAandD_Select(3);
                registration.ContinueButton_ClickUntilProgressBarChanges();

                // page 10
                registration.YourDrinkingHabitsDropdown_RegPathAandD_Select(3);
                //registration.ContinueButton_ClickUntilProgressBarChanges();

                // page 11
                registration.WhatIsYourMaritalStatusDropdown_AltRegPage_Click(3);
                //registration.ContinueButton_ClickUntilProgressBarChanges();

                // page 12
                registration.HowManyChildrenDoYouHaveListbox_Select(3);
                //registration.ContinueButton_ClickUntilProgressBarChanges();

                // page 13
                registration.WhatIsYourReligiousBackground_SparkReg_Select(3);
                registration.ContinueButton_ClickUntilProgressBarChanges();

                // page 14
                registration.ZipCodeTextbox_Write("90210");
                registration.ContinueButton_OnZipCodePage_Click();

                // page 15
                registration.FirstNameTextbox_Write("Bob");
                registration.LastNameTextbox_Write("Bobby");
                registration.BirthDateMonthList_Select(4);
                registration.BirthDateDayList_Select(3);
                registration.BirthDateYearList_Select(5);
                registration.ContinueButton_ClickUntilProgressBarChanges();

                // page 16
                registration.UserNameTextbox_Write("QAAutomation");
                registration.ChooseYourPasswordTextbox_Write("automation");
                registration.ContinueButton_ClickUntilProgressBarChanges();

                // page 17
                emailAddress = "QAAutomation_" + DateTime.Now.ToString() + "@gmail.com";
                emailAddress = emailAddress.Replace("/", "_").Replace(" ", "_").Replace(":", "_");
                registration.EmailAddressTextbox_Write(emailAddress);
                registration.ConfirmEmailTextbox_Write(emailAddress);

                registration.ContinueButton_ClickUntilProgressBarChanges();

                // page 18
                string describeMyself = "food lover.  art lover.  dance lover.  music lover. ";
                registration.DescribeYourselfAndYourPersonalityTextArea_Write(describeMyself);

                registration.ContinueButton_ClickUntilProgressBarChanges();

                // page 19
                registration.IWouldLikeSpecialOffersAndAnnouncements_Click();

                registration.ContinueButton_Click();

                // registration complete
                WebdriverAutomation.Framework.Site_Pages.RegistrationComplete registrationComplete = new WebdriverAutomation.Framework.Site_Pages.RegistrationComplete(driver);
            }
            #endregion
            else
            {
                Assert.Fail("Currently we do not recognize which registration page will be reached if we click the Browse For Free button on the Splash page. Stopping the test.");
            }
        }
    }
}
