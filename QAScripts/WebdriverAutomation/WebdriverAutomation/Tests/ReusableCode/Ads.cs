﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.Events;
using NUnit.Framework;
using log4net;

namespace WebdriverAutomation.Tests.ReusableCode
{
    public class Ads
    {
        IWebElement closeAdLink;

        /// <summary>
        /// Kills all ads that appear on the Home Page
        /// </summary>
        /// <param name="driver"></param>
        /// <param name="log"></param>
        public void KillAllAds(IWebDriver driver, ILog log)
        {
            bool adExists = false;
            string errorLogTrace = string.Empty;

            // check to see if an ad even exists
            errorLogTrace = "Initial check to see if ad exists  \r\n";
            if (driver.FindElements(By.XPath("//div[@id='blockUI_adunit']")).Count > 0)
            {
                errorLogTrace = "An ad exists but we don't know which one yet  \r\n";

                // Since ads don't show up on the Home Page immediately, there seems to be a race condition.  Thus we loop through all available ads twice before giving up.
                for (int i = 0; i <= 2; i++)
                {
                    errorLogTrace += "LOOP " + (i + 1) + " entered  \r\n";

                    closeAdLink = driver.FindElement(By.XPath("//div[@id='blockUI_adunit']"));
                    adExists = true;

                    errorLogTrace += "A div with 'blockUI_adunit' is found and assigned to an IWebElement  \r\n";

                    //   check for ALL ACCESS ad and close it if it exists
                    if (driver.FindElements(By.XPath("//div[@id='blockUI_adunit']/div/div/div/p[1]/a/strong")).Count > 0)
                    {
                        errorLogTrace += "Inside if statement for Ad #1.  FindElements count for this ad was more than 1  \r\n";

                        try
                        {
                            closeAdLink = driver.FindElement(By.XPath("//div[@id='blockUI_adunit']/div/div/div/p[1]/a/strong"));

                            // sometimes the ad doesn't click for some reason so we click the damn thing several times until it goes away
                            for (int j = 0; j < 3; j++)
                            {
                                try
                                {
                                    closeAdLink.Click();
                                }
                                catch
                                {
                                    break;
                                }
                            }

                            errorLogTrace += "Ad #1  found!  \r\n";

                            adExists = false;
                            break;
                        }
                        catch
                        { errorLogTrace += "Ad #1 click failed because of race condition, entered catch  \r\n"; }
                    }

                    // JPicks.com ad
                    if (driver.FindElements(By.XPath("//div[@id='blockUI_adunit']/div/a")).Count > 0)
                    {
                        errorLogTrace += "Inside if statement for Ad #2.  FindElements count for this ad was more than 1  \r\n";

                        try
                        {
                            closeAdLink = driver.FindElement(By.XPath("//div[@id='blockUI_adunit']/div/a"));

                            // sometimes the ad doesn't click for some reason so we click the damn thing several times until it goes away
                            for (int j = 0; j < 3; j++)
                            {
                                try
                                {
                                    closeAdLink.Click();
                                }
                                catch
                                {
                                    break;
                                }
                            }

                            errorLogTrace += "Ad #2 found!  \r\n";

                            adExists = false;
                            break;
                        }
                        catch
                        { errorLogTrace += "Ad #2 click failed because of race condition, entered catch  \r\n"; }
                    }

                    // JDate's NEW profile is beta than ever! ad
                    if (driver.FindElements(By.XPath("//div[@id='blockUI_adunit']/div/div/div[1]/div/p[3]/a")).Count > 0)
                    {
                        errorLogTrace += "Inside if statement for Ad #3.  FindElements count for this ad was more than 1  \r\n";

                        try
                        {
                            closeAdLink = driver.FindElement(By.XPath("//div[@id='blockUI_adunit']/div/div/div[1]/div/p[3]/a"));

                            // sometimes the ad doesn't click for some reason so we click the damn thing several times until it goes away
                            for (int j = 0; j < 3; j++)
                            {
                                try
                                {
                                    closeAdLink.Click();
                                }
                                catch
                                {
                                    break;
                                }
                            }

                            errorLogTrace += "Ad #3 found!  \r\n";

                            adExists = false;
                            break;
                        }
                        catch
                        { errorLogTrace += "Ad #3 click failed because of race condition, entered catch  \r\n"; }
                    }

                    //   check for YOUR NEXT OUTING COULD BE 50% OFF! ad and close it if it exists
                    if (driver.FindElements(By.XPath("//div[@id='blockUI_adunit']/div/div/p/a")).Count > 0)
                    {
                        errorLogTrace += "Inside if statement for Ad #4.  FindElements count for this ad was more than 1  \r\n";

                        try
                        {
                            closeAdLink = driver.FindElement(By.XPath("//div[@id='blockUI_adunit']/div/div/p/a"));

                            // sometimes the ad doesn't click for some reason so we click the damn thing several times until it goes away
                            for (int j = 0; j < 3; j++)
                            {
                                try
                                {
                                    closeAdLink.Click();
                                }
                                catch
                                {
                                    break;
                                }
                            }

                            errorLogTrace += "Ad #4 found!  \r\n";

                            adExists = false;
                            break;
                        }
                        catch
                        { errorLogTrace += "Ad #4 click failed because of race condition, entered catch  \r\n"; }
                    }

                    // Why is this site different from all other sites?  Take advantage of JDate's special Passover offer to find out for yourself...
                    if (driver.FindElements(By.XPath("//div[@id='blockUI_adunit']/div/div/div[2]/a")).Count > 0)
                    {
                        errorLogTrace += "Inside if statement for Ad #5.  FindElements count for this ad was more than 1  \r\n";

                        try
                        {
                            closeAdLink = driver.FindElement(By.XPath("//div[@id='blockUI_adunit']/div/div/div[2]/a"));

                            // sometimes the ad doesn't click for some reason so we click the damn thing several times until it goes away
                            for (int j = 0; j < 3; j++)
                            {
                                try
                                {
                                    closeAdLink.Click();
                                }
                                catch
                                {
                                    break;
                                }
                            }

                            errorLogTrace += "Ad #5 found!  \r\n";

                            adExists = false;
                            break;
                        }
                        catch
                        { errorLogTrace += "Ad #5 click failed because of race condition, entered catch  \r\n"; }
                    }

                    // Don't get burned.  Jdate Village CLub Med Ixtapa Pacfic ad
                    if (driver.FindElements(By.XPath("//div[@id='blockUI_adunit']/div/div/div[3]/a[1]")).Count > 0)
                    {
                        errorLogTrace += "Inside if statement for Ad #6.  FindElements count for this ad was more than 1  \r\n";

                        try
                        {
                            closeAdLink = driver.FindElement(By.XPath("//div[@id='blockUI_adunit']/div/div/div[3]/a[1]"));

                            // sometimes the ad doesn't click for some reason so we click the damn thing several times until it goes away
                            for (int j = 0; j < 3; j++)
                            {
                                try
                                {
                                    closeAdLink.Click();
                                }
                                catch
                                {
                                    break;
                                }
                            }

                            errorLogTrace += "Ad #6 found!  \r\n";

                            adExists = false;
                            break;
                        }
                        catch
                        { errorLogTrace += "Ad #6 click failed because of race condition, entered catch  \r\n"; }
                    }


                    // HUNDREDS of single Jews - JDate Village 2011 ad
                    if (driver.FindElements(By.XPath("//div[@id='blockUI_adunit']/div/div/div[3]/a")).Count > 0)
                    {
                        errorLogTrace += "Inside if statement for Ad #7.  FindElements count for this ad was more than 1  \r\n";

                        try
                        {
                            closeAdLink = driver.FindElement(By.XPath("//div[@id='blockUI_adunit']/div/div/div[3]/a"));

                            // sometimes the ad doesn't click for some reason so we click the damn thing several times until it goes away
                            for (int j = 0; j < 3; j++)
                            {
                                try
                                {
                                    closeAdLink.Click();
                                }
                                catch
                                {
                                    break;
                                }
                            }

                            errorLogTrace += "Ad #7 found!  \r\n";

                            adExists = false;
                            break;
                        }
                        catch
                        { errorLogTrace += "Ad #7 click failed because of race condition, entered catch  \r\n"; }
                    }

                    // Jdate.co.il ad
                    if (driver.FindElements(By.XPath("//div[@id='blockUI_adunit']/div/div/a[2]/img")).Count > 0)
                    {
                        errorLogTrace += "Inside if statement for Ad #8.  FindElements count for this ad was more than 1  \r\n";

                        try
                        {
                            closeAdLink = driver.FindElement(By.XPath("//div[@id='blockUI_adunit']/div/div/a[2]/img"));
                            //html/body/div/div/div/div/div/div[2]/div/div/a[2]/img
                            // sometimes the ad doesn't click for some reason so we click the damn thing several times until it goes away
                            for (int j = 0; j < 3; j++)
                            {
                                try
                                {
                                    closeAdLink.Click();
                                }
                                catch
                                {
                                    break;
                                }
                            }

                            errorLogTrace += "Ad #8 found!  \r\n";

                            adExists = false;
                            break;
                        }
                        catch
                        { errorLogTrace += "Ad #8 click failed because of race condition, entered catch  \r\n"; }
                    }

                    // Jdate.co.il img ad
                    if (driver.FindElements(By.XPath("//div[@id='blockUI_adunit']/div/div[1]/a/img")).Count > 0)
                    {
                        errorLogTrace += "Inside if statement for Ad #8.  FindElements count for this ad was more than 1  \r\n";

                        try
                        {
                            closeAdLink = driver.FindElement(By.XPath("//div[@id='blockUI_adunit']/div/div[1]/a/img"));

                            // sometimes the ad doesn't click for some reason so we click the damn thing several times until it goes away
                            for (int j = 0; j < 3; j++)
                            {
                                try
                                {
                                    closeAdLink.Click();
                                }
                                catch
                                {
                                    break;
                                }
                            }

                            errorLogTrace += "Ad #9 found!  \r\n";

                            adExists = false;
                            break;
                        }
                        catch
                        { errorLogTrace += "Ad #9 click failed because of race condition, entered catch  \r\n"; }
                    }

                    // Get Up to 30% off! - JDate ad
                    if (driver.FindElements(By.XPath("//div[@id='blockUI_adunit']/div/div[1]/a")).Count > 0)
                    {
                        errorLogTrace += "Inside if statement for Ad #8.  FindElements count for this ad was more than 1  \r\n";

                        try
                        {
                            closeAdLink = driver.FindElement(By.XPath("//div[@id='blockUI_adunit']/div/div[1]/a"));

                            // sometimes the ad doesn't click for some reason so we click the damn thing several times until it goes away
                            for (int j = 0; j < 3; j++)
                            {
                                try
                                {
                                    closeAdLink.Click();
                                }
                                catch
                                {
                                    break;
                                }
                            }

                            errorLogTrace += "Ad #10 found!  \r\n";

                            adExists = false;
                            break;
                        }
                        catch
                        { errorLogTrace += "Ad #10 click failed because of race condition, entered catch  \r\n"; }
                    }

                    if (driver.FindElements(By.XPath("//div[@id='blockUI_adunit']")).Count == 0)
                    {
                        errorLogTrace += "The ad was closed somehow.  Exiting the loop.  \r\n";

                        adExists = false;
                        break;
                    }

                    errorLogTrace += "END LOOP " + (i + 1) + "  \r\n";
                }

                // we have looped through all the ads that we know of several times (accounting for the possible race condition) and still can't get rid of the ad
                if (adExists == true)
                    Assert.Fail("An new ad exists on the Home Page or Profile Page that we do not know how to get rid of. We cannot continue this test case until we do so.  \r\nERRORLOG - " + errorLogTrace);

            }
        }

    }

}
