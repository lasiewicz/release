﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.Events;
using NUnit.Framework;
using log4net;

namespace WebdriverAutomation.Tests.ReusableCode
{
    public class AccountSettings
    {

        /// <summary>
        /// Hides the user from everything in Account Settings
        /// Start: user LOGGED IN
        /// End: Your Account > PROFILE DISPLAY SETTINGS page
        /// </summary>
        /// <param name="driver"></param>
        /// <param name="log"></param>
        public Framework.Site_Pages.ProfileDisplaySettings HideCurrentProfile(IWebDriver driver, ILog log)
        {
            driver.Navigate().GoToUrl(System.Configuration.ConfigurationManager.AppSettings["JDate"]);
            WebdriverAutomation.Framework.Site_Pages.Home home = new WebdriverAutomation.Framework.Site_Pages.Home(driver);

            WebdriverAutomation.Framework.Site_Pages.YourAccount yourAccount = home.Header.YourAccountLink_Click();

            WebdriverAutomation.Framework.Site_Pages.ProfileDisplaySettings profileDisplaySettings = yourAccount.ProfileDisplaySettingsLink_Click();

            profileDisplaySettings.ShowHideWhenYoureOnlineRadioButton_Select(1);
            profileDisplaySettings.ShowHideYourProfileInSearchesRadioButton_Select(1);
            profileDisplaySettings.YourPhotosRadioButton_Select(1);
            profileDisplaySettings.ShowHideWhenYouViewOrHotListMembersRadioButton_Select(1);

            profileDisplaySettings.SaveSettingsButton_Click();

            return new Framework.Site_Pages.ProfileDisplaySettings(driver);
        }

        /// <summary>
        /// Removes the current profile completely.  Used after registering a new user to not flood the site with bogus accounts.
        /// Start: user LOGGED IN
        /// End: Show My Profile page - do later.  maybe.
        /// </summary>
        /// <param name="driver"></param>
        /// <param name="log"></param>
        public void RemoveProfile_SkippingUpdateProfileAndImportantNoticePages(IWebDriver driver, ILog log)
        {
            if (driver.Url.Contains("www.jdate.com"))
                driver.Navigate().GoToUrl(System.Configuration.ConfigurationManager.AppSettings["JDate_Production"]);
            else if (driver.Url.Contains("jdate.com"))
                driver.Navigate().GoToUrl(System.Configuration.ConfigurationManager.AppSettings["JDate"]);
            else if (driver.Url.Contains("www.spark.com"))
                driver.Navigate().GoToUrl(System.Configuration.ConfigurationManager.AppSettings["Spark_Production"]);
            else if (driver.Url.Contains("spark.com"))
                driver.Navigate().GoToUrl(System.Configuration.ConfigurationManager.AppSettings["Spark"]);
            else if (driver.Url.Contains("www.blacksingles.com"))
                driver.Navigate().GoToUrl(System.Configuration.ConfigurationManager.AppSettings["Black_Production"]);
            else if (driver.Url.Contains("blacksingles.com"))
                driver.Navigate().GoToUrl(System.Configuration.ConfigurationManager.AppSettings["Black"]);
            else if (driver.Url.Contains("www.bbwpersonalsplus.com"))
                driver.Navigate().GoToUrl(System.Configuration.ConfigurationManager.AppSettings["BBW_Production"]);
            else if (driver.Url.Contains("bbwpersonalsplus.com"))
                driver.Navigate().GoToUrl(System.Configuration.ConfigurationManager.AppSettings["BBW"]);
            else if (driver.Url.Contains("www.jdate.co.il"))
                driver.Navigate().GoToUrl(System.Configuration.ConfigurationManager.AppSettings["JDate_Co_Il_Production"]);
            else if (driver.Url.Contains("jdate.co.il"))
                driver.Navigate().GoToUrl(System.Configuration.ConfigurationManager.AppSettings["JDate_Co_Il"]);
            else
                Assert.Fail("While trying to navigate to the Home Page but we cannot figure out which Home Page (JDate, Spark, etc) to go to based on our current URL");

            Tests.ReusableCode.Ads ads = new Tests.ReusableCode.Ads();
            ads.KillAllAds(driver, log);

            //if (driver.Url.Contains("jdate.com") || driver.Url.Contains("jdate.co.il"))
            //{
            //    // Please update your profile! page appears asking user to upload a photo
            //    Framework.Site_Pages.PleaseUpdateYourProfile pleaseUpdateYourProfile = new Framework.Site_Pages.PleaseUpdateYourProfile(driver);

            //    if (pleaseUpdateYourProfile.UpdateLater_UploadAPhoto_Exists())
            //        pleaseUpdateYourProfile.UpdateLaterLink_UploadAPhoto_Click();

            //    if (pleaseUpdateYourProfile.UpdateLater_AboutMe_Exists())
            //        pleaseUpdateYourProfile.UpdateLaterLink_AboutMe_Click();
            //}

            //// right now we could either be on the Home Page or the Important Notice Page
            //if (driver.Url.Contains("Applications"))
            //{
            //    // Click on "I Accept" to the "Important Notice" about promising never to sen money etc. to other members
            //    WebdriverAutomation.Framework.Site_Pages.AnImportantNotice anImportantNotice = new WebdriverAutomation.Framework.Site_Pages.AnImportantNotice(driver);
            //    anImportantNotice.IAccept_Click();
            //}

            WebdriverAutomation.Framework.Site_Pages.Home home = new Framework.Site_Pages.Home(driver);

            WebdriverAutomation.Framework.Site_Pages.YourAccount yourAccount = home.Header.YourAccountLink_Click();

            WebdriverAutomation.Framework.Site_Pages.RemoveMyProfile removeMyProfile = yourAccount.RemoveMyProfileLink_Click();

            removeMyProfile.PleaseTellUsWhyYouAreRemovingYourProfileRadioButton_Select(7);
            removeMyProfile.RemoveButton_Click();
        }

        public void RemoveProfile(IWebDriver driver, ILog log)
        {
            if (driver.Url.Contains("www.jdate.com"))
                driver.Navigate().GoToUrl(System.Configuration.ConfigurationManager.AppSettings["JDate_Production"]);
            else if (driver.Url.Contains("jdate.com"))
                driver.Navigate().GoToUrl(System.Configuration.ConfigurationManager.AppSettings["JDate"]);
            else if (driver.Url.Contains("www.spark.com"))
                driver.Navigate().GoToUrl(System.Configuration.ConfigurationManager.AppSettings["Spark_Production"]);
            else if (driver.Url.Contains("spark.com"))
                driver.Navigate().GoToUrl(System.Configuration.ConfigurationManager.AppSettings["Spark"]);
            else if (driver.Url.Contains("www.blacksingles.com"))
                driver.Navigate().GoToUrl(System.Configuration.ConfigurationManager.AppSettings["Black_Production"]);
            else if (driver.Url.Contains("blacksingles.com"))
                driver.Navigate().GoToUrl(System.Configuration.ConfigurationManager.AppSettings["Black"]);
            else if (driver.Url.Contains("www.bbwpersonalsplus.com"))
                driver.Navigate().GoToUrl(System.Configuration.ConfigurationManager.AppSettings["BBW_Production"]);
            else if (driver.Url.Contains("bbwpersonalsplus.com"))
                driver.Navigate().GoToUrl(System.Configuration.ConfigurationManager.AppSettings["BBW"]);
            else if (driver.Url.Contains("www.jdate.co.il"))
                driver.Navigate().GoToUrl(System.Configuration.ConfigurationManager.AppSettings["JDate_Co_Il_Production"]);
            else if (driver.Url.Contains("jdate.co.il"))
                driver.Navigate().GoToUrl(System.Configuration.ConfigurationManager.AppSettings["JDate_Co_Il"]);
            else
                Assert.Fail("While trying to navigate to the Home Page but we cannot figure out which Home Page (JDate, Spark, etc) to go to based on our current URL");

            Tests.ReusableCode.Ads ads = new Tests.ReusableCode.Ads();
            ads.KillAllAds(driver, log);
            
            WebdriverAutomation.Framework.Site_Pages.Home home = new Framework.Site_Pages.Home(driver);

            WebdriverAutomation.Framework.Site_Pages.YourAccount yourAccount = home.Header.YourAccountLink_Click();

            WebdriverAutomation.Framework.Site_Pages.RemoveMyProfile removeMyProfile = yourAccount.RemoveMyProfileLink_Click();

            removeMyProfile.PleaseTellUsWhyYouAreRemovingYourProfileRadioButton_Select(7);
            removeMyProfile.RemoveButton_Click();
        }
    }
}
