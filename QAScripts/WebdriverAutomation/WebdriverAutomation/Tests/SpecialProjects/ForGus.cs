﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.Events;
using NUnit.Framework;
using log4net;
using WebdriverAutomation.Tests.ReusableCode;
using System.Diagnostics;
using System.Reflection;
using System.Drawing.Imaging;
using System.Collections;

namespace WebdriverAutomation.Tests.SpecialProjects
{
    [TestFixture]
    public class ForGus : BaseTest
    {
        private string currentSiteBeingTested = string.Empty;
        private string currentDateTime = string.Empty;
        private string pageSource_StartOfTest = string.Empty;
        private string pageSource_Previous = string.Empty;
        private string pageSource_Current = string.Empty;

        private string automationResultsRelativePath = string.Empty;
        private string automationResultsAbsolutePath = string.Empty;

        /// <summary>
        /// This test logs into JDate with a special user in app.config and navigates to the UPS (Subscribe) Page.  
        /// We take a screenshot of the initial page load as well as the source code and save it to a special folder, then navigate back to the Home Page and then back
        /// again to the UPS Page.  Once we're back at the UPS Page again we take the source code and compare it to our previous version.  If it's the same we repeat (going
        /// back to the Home Page again) and if it's not the same we save the source codes and take another screen shot.
        /// Repeat for a predetermined time period which can be set in app.config.
        /// </summary>
        [Test]
        public void Validate_UPSPageSourcePersistsOverTime()
        {
            try
            {
                log.Info("START TEST - SpecialProjects/ForGus/Validate_UPSPageSourcePersistsOverTime");

                Setup();

                currentSiteBeingTested = System.Configuration.ConfigurationManager.AppSettings["CurrentSiteBeingTested"];
                if (currentSiteBeingTested == "Spark")
                    StartTest_UsingSpark();
                else if (currentSiteBeingTested == "BBW")
                    StartTest_UsingBBW();
                else if (currentSiteBeingTested == "Black")
                    StartTest_UsingBlack();
                else
                    StartTest_UsingJDate();

                automationResultsRelativePath = @"..\..\..\..\..\Automation Results - UPS Page Source Persistence\Validate " + currentSiteBeingTested + " UPS Source Code Persistence";

                currentDateTime = DateTime.Now.ToString().Replace(':', '-').Replace('/', '-');
                automationResultsRelativePath = automationResultsRelativePath + " " + currentDateTime + "\\";

                automationResultsAbsolutePath = System.IO.Path.GetFullPath(automationResultsRelativePath);
                resultsPath = automationResultsAbsolutePath;

                CreateAutomationResultsFolder();

                bool areThereAnyChangesToPageSource = false;

                // how long we want the test case to loop for.  timer is in minutes.
                int lengthOfTestCaseLoopTimer = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["UPSPageAutomationTimer"]);

                DateTime dtStart = DateTime.Now;
                DateTime dtEnd = dtStart.AddMinutes(lengthOfTestCaseLoopTimer);

                string login = string.Empty;
                string password = string.Empty;
                string homePageURL = string.Empty;

                if (currentSiteBeingTested == "Spark")
                {
                    login = System.Configuration.ConfigurationManager.AppSettings["Spark_UPSPageSourceTest_Email"];
                    password = System.Configuration.ConfigurationManager.AppSettings["Spark_UPSPageSourceTest_Password"];

                    homePageURL = @"http://www.spark.com/";
                }
                else if (currentSiteBeingTested == "BBW")
                {
                    login = System.Configuration.ConfigurationManager.AppSettings["BBW_UPSPageSourceTest_Email"];
                    password = System.Configuration.ConfigurationManager.AppSettings["BBW_UPSPageSourceTest_Password"];

                    homePageURL = @"http://www.bbwpersonalsplus.com/";
                }
                else if (currentSiteBeingTested == "Black")
                {
                    login = System.Configuration.ConfigurationManager.AppSettings["Black_UPSPageSourceTest_Email"];
                    password = System.Configuration.ConfigurationManager.AppSettings["Black_UPSPageSourceTest_Password"];

                    homePageURL = @"http://www.blacksingles.com/";
                }
                else
                {
                    login = System.Configuration.ConfigurationManager.AppSettings["JDate_UPSPageSourceTest_Email"];
                    password = System.Configuration.ConfigurationManager.AppSettings["JDate_UPSPageSourceTest_Password"];

                    homePageURL = @"http://www.jdate.com/";
                }

                WebdriverAutomation.Tests.ReusableCode.LoggingInAndOut loggingInAndOut = new LoggingInAndOut();
                WebdriverAutomation.Framework.Site_Pages.Home home = loggingInAndOut.LogIn_ProfileComplete(driver, log, login, password);
                WebdriverAutomation.Framework.Site_Pages.Subscribe subscribe = home.Header.SubscribeHeaderLink_Click();

                TakeAndSaveScreenshotOfInitialUPSPage();

                pageSource_StartOfTest = driver.PageSource;
                pageSource_Previous = pageSource_StartOfTest;

                pageSource_Previous = RemoveKnownSubstringsWhichAlwaysChangeEachPageLoad(pageSource_Previous);

                //  debug
                //SaveInitialPageSourceToAutomationResults(pageSource_Previous);
                SaveInitialPageSourceToAutomationResults(pageSource_StartOfTest);

                driver.Navigate().GoToUrl(homePageURL);
                driver.SwitchTo().DefaultContent();     // a bug in ff7 causes the error this.getWindow() is null after we sleep. using this after we get to a new page
                home = new WebdriverAutomation.Framework.Site_Pages.Home(driver);
                
                while (DateTime.Now < dtEnd)
                {
                    subscribe = home.Header.SubscribeHeaderLink_Click();
                    driver.SwitchTo().DefaultContent();     // a bug in ff7 causes the error this.getWindow() is null after we sleep. using this after we get to a new page

                    SaveLogTextFile();

                    string pageSourceBeforeRemovingSubstrings = driver.PageSource;

                    pageSource_Current = RemoveKnownSubstringsWhichAlwaysChangeEachPageLoad(pageSourceBeforeRemovingSubstrings);

                    // if the page source has changed at all, the payment plans might have changed for some reason.  take a screenshot and save the new page source.
                    if (pageSource_Current != pageSource_Previous)
                    {
                        areThereAnyChangesToPageSource = true;

                        currentDateTime = DateTime.Now.ToString().Replace(':', '-').Replace('/', '-');

                        TakeAndSaveScreenshotOfChangedUPSPage();

                        // debug
                        //SaveChangedPageSourceToAutomationResults(pageSource_Current);
                        SaveChangedPageSourceToAutomationResults(pageSourceBeforeRemovingSubstrings);
                    }

                    pageSource_Previous = pageSource_Current;

                    // run this loop twice a minute; sleep for 30 seconds
                    Thread.Sleep(30000);

                    driver.Navigate().GoToUrl(homePageURL);
                    driver.SwitchTo().DefaultContent();     // a bug in ff7 causes the error this.getWindow() is null after we sleep. using this after we get to a new page
                    home = new WebdriverAutomation.Framework.Site_Pages.Home(driver);                    
                }

                currentDateTime = DateTime.Now.ToString().Replace(':', '-').Replace('/', '-');
                SaveResultsTextFile(areThereAnyChangesToPageSource, lengthOfTestCaseLoopTimer);

                TearDown();

                log.Info("END TEST - SpecialProjects/ForGus/Validate_UPSPageSourcePersistsOverTime");
            }
            catch (Exception e)
            {
                CleanupFailedTestcase(e);
                throw;
            }
        }

        /// <summary>
        /// Does nothing if a folder in the same path as locationForResults already exists
        /// </summary>
        public void CreateAutomationResultsFolder()
        {
            try
            {
                // Determine whether the directory exists.
                if (Directory.Exists(automationResultsAbsolutePath))
                    return;

                // Try to create the directory.
                DirectoryInfo di = Directory.CreateDirectory(automationResultsAbsolutePath);
            }
            catch (Exception e)
            {
                Console.WriteLine("The process of creating a new directory for " + automationResultsAbsolutePath + " failed: {0}", e.ToString());
            }

        }

        public void TakeAndSaveScreenshotOfInitialUPSPage()
        {
            ITakesScreenshot screenshotCapableDriver = driver as ITakesScreenshot;
            Screenshot screenImage = screenshotCapableDriver.GetScreenshot();

            string screenshotName = "TEST STARTED - UPS Page initial screenshot - " + currentDateTime;
            string screenshotPath = automationResultsAbsolutePath + screenshotName + ".jpg";

            screenImage.SaveAsFile(screenshotPath, ImageFormat.Jpeg);
        }

        public void TakeAndSaveScreenshotOfChangedUPSPage()
        {
            ITakesScreenshot screenshotCapableDriver = driver as ITakesScreenshot;
            Screenshot screenImage = screenshotCapableDriver.GetScreenshot();

            string screenshotName = "Page source changed. New screenshot - " + currentDateTime;
            string screenshotPath = automationResultsAbsolutePath + screenshotName + ".jpg";

            screenImage.SaveAsFile(screenshotPath, ImageFormat.Jpeg);
        }

        public void SaveInitialPageSourceToAutomationResults(string pageSourceText)
        {
            string pageSourceName = "TEST STARTED - UPS Page initial page source - " + currentDateTime;
            string pageSourcePath = automationResultsAbsolutePath + pageSourceName + ".txt";

            System.IO.File.WriteAllText(pageSourcePath, pageSourceText);
        }

        public void SaveChangedPageSourceToAutomationResults(string pageSourceText)
        {
            string pageSourceName = "Page source changed. New page source - " + currentDateTime;
            string pageSourcePath = automationResultsAbsolutePath + pageSourceName + ".txt";

            System.IO.File.WriteAllText(pageSourcePath, pageSourceText);
        }

        public void SaveResultsTextFile(bool areThereAnyChangesToPageSource, int lengthOfTestCaseLoopTimer)
        {
            string pageSourceName = string.Empty;

            if (areThereAnyChangesToPageSource == false)
                pageSourceName = "TEST PASSED - " + lengthOfTestCaseLoopTimer + " min - " + currentDateTime;
            else
                pageSourceName = "TEST FAILED - " + lengthOfTestCaseLoopTimer + " min - " + currentDateTime;

            string pageSourcePath = automationResultsAbsolutePath + pageSourceName + ".txt";

            System.IO.File.WriteAllText(pageSourcePath, "");
        }

        public void SaveLogTextFile()
        {
            string logDateTime = DateTime.Now.ToString().Replace(':', '-').Replace('/', '-');
            string pageSourceName = "UPS Page reached successfully - " + logDateTime;
            string pageSourcePath = automationResultsAbsolutePath + pageSourceName + ".txt";

            System.IO.File.WriteAllText(pageSourcePath, "");
        }

        /// <summary>
        /// Since we are comparing the page source for the UPS page we need to get rid of substrings that we already know will change every time upon page load for 
        /// example parts of the page source with encryption in it.
        /// Currently there are 5 known substrings we need to get rid of.
        /// </summary>
        public string RemoveKnownSubstringsWhichAlwaysChangeEachPageLoad(string oldPageSource)
        {
            int index;
            string[] splitSource;
            string tempSource = string.Empty;
            string newPageSource = string.Empty;

            // get rid of 1st substring (line starting with 's.prop29') unless we are currently testing Black Singles or BBW
            if (currentSiteBeingTested == "Black" || currentSiteBeingTested == "BBW")
                newPageSource = oldPageSource;
            else
            {
                tempSource = oldPageSource;

                splitSource = tempSource.Split(new string[] { "s.prop29" }, StringSplitOptions.None);
                index = splitSource[1].IndexOf(@";");
                splitSource[1] = splitSource[1].Remove(0, index);

                newPageSource = splitSource[0] + splitSource[1];
            }

            // get rid of 2nd substring (line starting with 's.eVar3=')
            tempSource = newPageSource;

            splitSource = tempSource.Split(new string[] { "s.eVar3=" }, StringSplitOptions.None);
            index = splitSource[1].IndexOf(@";");
            splitSource[1] = splitSource[1].Remove(0, index);

            newPageSource = splitSource[0] + splitSource[1];

            // get rid of 3rd substring (line starting with '</script><div id="site-container">')
            tempSource = newPageSource;

            if (currentSiteBeingTested == "Black" || currentSiteBeingTested == "Spark")
                splitSource = tempSource.Split(new string[] { "<form id=\"aspnetForm\" action=\"/Purchase/CreditCardPurchase\" method=\"post\" name=\"aspnetForm\">" }, StringSplitOptions.None);
            else
                splitSource = tempSource.Split(new string[] { "</script><div id=\"site-container\">" }, StringSplitOptions.None);

            index = splitSource[1].IndexOf(@";");
            splitSource[1] = splitSource[1].Remove(0, index);

            newPageSource = splitSource[0] + splitSource[1];

            // get rid of 4th substring (line starting with '<p class="terms-date')
            tempSource = newPageSource;

            splitSource = tempSource.Split(new string[] { "<p class=\"terms-date" }, StringSplitOptions.None);
            index = splitSource[1].IndexOf(@"</p>");
            splitSource[1] = splitSource[1].Remove(0, index);

            newPageSource = splitSource[0] + splitSource[1];

            // get rid of everything after the closing form tag </form>
            tempSource = newPageSource;

            splitSource = tempSource.Split(new string[] { "</form>" }, StringSplitOptions.None);

            newPageSource = splitSource[0];

            return newPageSource;
        }
    }
}
