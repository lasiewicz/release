﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.Events;
using NUnit.Framework;
using log4net;
using WebdriverAutomation.Tests.ReusableCode;
using System.Diagnostics;
using System.Reflection;
using System.Drawing.Imaging;
using System.Collections;
using System.Net.Mail;
using System.Net.Mime;
//using System.DirectoryServices;

namespace WebdriverAutomation.Tests.SpecialProjects
{
    [TestFixture]
    public class UPSPagePromos : BaseTest
    {
        private string currentSiteBeingTested = string.Empty;
        private string currentDateTime_originalCharacters = string.Empty;
        private string currentDateTime = string.Empty;
        private string pageSource = string.Empty;
        private string login = string.Empty;

        int actualPromoID;
        //int expectedPromoID;

        private string automationResultsRelativePath = string.Empty;
        private string automationResultsAbsolutePath = string.Empty;

        // used for sending email
        const string smtpClientAddress = "LAEX01.matchnet.com";
        MailMessage mail;
        AlternateView plainView;
        AlternateView htmlView;
        string screenshotPath = string.Empty;
        string emailHTML = string.Empty;
        List<promoAccounts> promoAccountsList;

        public class promoAccounts
        {
            string userName = string.Empty;
            string password = "automation1";
            int promoID;
            string subStatus = string.Empty;
            int memberID;
            string upsPageScreenshotLocation = string.Empty;

            public string UserName
            {
                get
                {
                    return userName;
                }
                set
                {
                    userName = value;
                }
            }

            public string Password
            {
                get
                {
                    return password;
                }
            }

            public int PromoID
            {
                get
                {
                    return promoID;
                }
                set
                {
                    promoID = value;
                }
            }

            public string SubStatus
            {
                get
                {
                    return subStatus;
                }
                set
                {
                    subStatus = value;
                }
            }

            public int MemberID
            {
                get
                {
                    return memberID;
                }
                set
                {
                    memberID = value;
                }
            }

            public string UPSPageScreenshotLocation
            {
                get
                {
                    return upsPageScreenshotLocation;
                }
                set
                {
                    upsPageScreenshotLocation = value;
                }
            }
        }

        /// <summary>
        /// This test logs into JDate with a special user in app.config and navigates to the UPS (Subscribe) Page.  
        /// We take a screenshot of the initial page load as well as the source code and save it to a special folder, then navigate back to the Home Page and then back
        /// again to the UPS Page.  Once we're back at the UPS Page again we take the source code and compare it to our previous version.  If it's the same we repeat (going
        /// back to the Home Page again) and if it's not the same we save the source codes and take another screen shot.
        /// Repeat for a predetermined time period which can be set in app.config.
        /// </summary>
        [Test]
        public void Validate_JDate_UPSPageSourcePromosAreCorrect()
        {
            try
            {
                log.Info("START TEST - SpecialProjects/ForGus/Validate_JDate_UPSPageSourcePromosAreCorrect");

                currentSiteBeingTested = "JDate";
                string password = string.Empty;

                #region Setup, start test with correct site, create automation results folder
                Setup();
                
                if (currentSiteBeingTested == "Spark")
                    StartTest_UsingSpark_Production();
                else if (currentSiteBeingTested == "BBW")
                    StartTest_UsingBBW_Production();
                else if (currentSiteBeingTested == "Black")
                    StartTest_UsingBlack_Production();
                else
                    StartTest_UsingJDate_Production();

                automationResultsRelativePath = @"C:\Automation Results - UPS Page Promo Screenshots and Results\Validate " + currentSiteBeingTested + " UPS Page Promos Are Correct";
                //automationResultsRelativePath = @"..\..\..\..\..\Automation Results - UPS Page Promos Screenshots and Results\Validate " + currentSiteBeingTested + " UPS Page Promos Are Correct";

                currentDateTime_originalCharacters = DateTime.Now.ToString();
                currentDateTime = currentDateTime_originalCharacters.Replace(':', '-').Replace('/', '-');
                automationResultsRelativePath = automationResultsRelativePath + " " + currentDateTime + "\\";

                automationResultsAbsolutePath = System.IO.Path.GetFullPath(automationResultsRelativePath);
                resultsPath = automationResultsAbsolutePath;

                CreateAutomationResultsFolder();
                #endregion

                InitializeEmail();

                emailHTML = "The following is a list of accounts that we log into on JDate.com using automation to verify that the Promo IDs that we expect on the UPS Page are the same ones that we actually end up getting.<br><br>";

                #region Populate list of accounts to test
                promoAccounts NewYorkAccount1 = new promoAccounts();
                NewYorkAccount1.UserName = "nonsubNYa@spark.net";
                NewYorkAccount1.PromoID = 4414;

                promoAccounts NewYorkAccount2 = new promoAccounts();
                NewYorkAccount2.UserName = "nonsubNYb@spark.net";
                NewYorkAccount2.PromoID = 4417;

                promoAccounts NewYorkAccount3 = new promoAccounts();
                NewYorkAccount3.UserName = "exsubNYa@spark.net";
                NewYorkAccount3.PromoID = 4431;

                promoAccounts NewYorkAccount4 = new promoAccounts();
                NewYorkAccount4.UserName = "exsubNYb@spark.net";
                NewYorkAccount4.PromoID = 4457;

                promoAccounts SeattleAccount1 = new promoAccounts();
                SeattleAccount1.UserName = "nonsubWAa@spark.net";
                SeattleAccount1.PromoID = 4430;

                promoAccounts SeattleAccount2 = new promoAccounts();
                SeattleAccount2.UserName = "nonsubWAb@spark.net";
                SeattleAccount2.PromoID = 4506;

                promoAccounts SeattleAccount3 = new promoAccounts();
                SeattleAccount3.UserName = "exsubWAa@spark.net";
                SeattleAccount3.PromoID = 4398;

                promoAccounts SeattleAccount4 = new promoAccounts();
                SeattleAccount4.UserName = "exsubWAb@spark.net";
                SeattleAccount4.PromoID = 4423;

                promoAccounts SeattleAccount5 = new promoAccounts();
                SeattleAccount5.UserName = "subWAa@spark.net";
                SeattleAccount5.PromoID = 4401;

                promoAccounts SeattleAccount6 = new promoAccounts();
                SeattleAccount6.UserName = "subWAb@spark.net";
                SeattleAccount6.PromoID = 4446;

                promoAccounts LosAngelesAccount1 = new promoAccounts();
                LosAngelesAccount1.UserName = "nonsubLAa@spark.net";
                LosAngelesAccount1.PromoID = 4171;

                promoAccounts LosAngelesAccount2 = new promoAccounts();
                LosAngelesAccount2.UserName = "nonsubLAb@spark.net";                

                promoAccounts LosAngelesAccount3 = new promoAccounts();
                LosAngelesAccount3.UserName = "exsubLAb@spark.net";
                LosAngelesAccount3.PromoID = 4164;

                promoAccounts LosAngelesAccount4 = new promoAccounts();
                LosAngelesAccount4.UserName = "exsubLAa@spark.net";
                LosAngelesAccount4.PromoID = 4113;

                promoAccounts BostonAccount1 = new promoAccounts();
                BostonAccount1.UserName = "nonsubBOSa@spark.net";
                BostonAccount1.PromoID = 4489;

                promoAccounts BostonAccount2 = new promoAccounts();
                BostonAccount2.UserName = "nonsubBOSb@spark.net";
                BostonAccount2.PromoID = 4541;

                promoAccounts BostonAccount3 = new promoAccounts();
                BostonAccount3.UserName = "exsubBOSa@spark.net";
                BostonAccount3.PromoID = 4495;

                promoAccounts BostonAccount4 = new promoAccounts();
                BostonAccount4.UserName = "exsubBOSb@spark.net";
                BostonAccount4.PromoID = 4521;

                promoAccounts AllAccount1 = new promoAccounts();
                AllAccount1.UserName = "nonsubALLa@spark.net";
                AllAccount1.PromoID = 4480;

                promoAccounts AllAccount2 = new promoAccounts();
                AllAccount2.UserName = "nonsubALLb@spark.net";

                promoAccounts AllAccount3 = new promoAccounts();
                AllAccount3.UserName = "exsubALLa@spark.net";

                promoAccounts AllAccount4 = new promoAccounts();
                AllAccount4.UserName = "exsubALLb@spark.net";
                AllAccount4.PromoID = 4477;

                promoAccounts AllAccount5 = new promoAccounts();
                AllAccount5.UserName = "subALLa@spark.net";
                AllAccount5.PromoID = 4424;

                promoAccounts AllAccount6 = new promoAccounts();
                AllAccount6.UserName = "subALLb@spark.net";
                AllAccount6.PromoID = 4378;

                promoAccountsList = new List<promoAccounts>();
                promoAccountsList.Add(NewYorkAccount1);
                promoAccountsList.Add(NewYorkAccount2);
                promoAccountsList.Add(NewYorkAccount3);
                promoAccountsList.Add(NewYorkAccount4);
                promoAccountsList.Add(SeattleAccount1);
                promoAccountsList.Add(SeattleAccount2);
                promoAccountsList.Add(SeattleAccount3);
                promoAccountsList.Add(SeattleAccount4);
                promoAccountsList.Add(SeattleAccount5);
                promoAccountsList.Add(SeattleAccount6);
                promoAccountsList.Add(LosAngelesAccount1);
                promoAccountsList.Add(LosAngelesAccount2);
                promoAccountsList.Add(LosAngelesAccount3);
                promoAccountsList.Add(LosAngelesAccount4);
                promoAccountsList.Add(BostonAccount1);
                promoAccountsList.Add(BostonAccount2);
                promoAccountsList.Add(BostonAccount3);
                promoAccountsList.Add(BostonAccount4);
                promoAccountsList.Add(AllAccount1);
                promoAccountsList.Add(AllAccount2);
                promoAccountsList.Add(AllAccount3);
                promoAccountsList.Add(AllAccount4);
                promoAccountsList.Add(AllAccount5);
                promoAccountsList.Add(AllAccount6);
                #endregion

                WebdriverAutomation.Tests.ReusableCode.LoggingInAndOut loggingInAndOut = new LoggingInAndOut();

                for (int i = 0; i < promoAccountsList.Count; i++)
                {
                    login = promoAccountsList[i].UserName;
                    password = promoAccountsList[i].Password;
                    //expectedPromoID = promoAccountsList[i].PromoID;

                    WebdriverAutomation.Framework.Site_Pages.Home home = loggingInAndOut.LogIn_ProfileComplete(driver, log, login, password);
                    WebdriverAutomation.Framework.Site_Pages.Subscribe subscribe = home.Header.SubscribeHeaderLink_Click();

                    driver.SwitchTo().DefaultContent();     // a bug in ff7 causes the error this.getWindow() is null after we sleep. using this after we get to a new page
                    pageSource = driver.PageSource;
                    actualPromoID = GetCurrentPromoNumber(pageSource);

                    TakeAndSaveScreenshotUPSPage();

                    //if (expectedPromoID == actualPromoID)
                    emailHTML += "<hr /><br>Screenshot for account " + promoAccountsList[i].UserName + "<br>Promo ID on UPS Page: " + actualPromoID + "<br><br><img src=cid:UPSPageScreenshot" + i + "> <br><br><br>";
                    //else
                    //    emailHTML += "<hr /><br><font color=\"red\"><b>The promo ID taken from the page source of the folowing account does not match the promo ID expected.</b></font><br>Screenshot for account " + promoAccountsList[i].UserName + "<br>Promo ID expected: " + expectedPromoID + "<br>Promo ID on UPS Page: <font color=\"red\"><b>" + actualPromoID + "</b></font><br><br><img src=cid:UPSPageScreenshot" + i + "> <br><br><br>";


                    promoAccountsList[i].UPSPageScreenshotLocation = screenshotPath;

                    WebdriverAutomation.Framework.Site_Pages.Login loginPage = loggingInAndOut.LogOut(driver, log);
                }

                TearDown();

                AddHTMLAndScreenshotsToEmailInProgress();
                CompleteAndSendEmailAndScreenshots();

                log.Info("END TEST - SpecialProjects/ForGus/Validate_JDate_UPSPageSourcePromosAreCorrect");
            }
            catch (Exception e)
            {
                AddHTMLAndScreenshotsToEmailInProgress();
                CompleteAndSendEmailAndScreenshots();

                CleanupFailedTestcase(e);
                throw;
            }
        }

        /// <summary>
        /// Does nothing if a folder in the same path as locationForResults already exists
        /// </summary>
        public void CreateAutomationResultsFolder()
        {
            try
            {
                // Determine whether the directory exists.
                if (Directory.Exists(automationResultsAbsolutePath))
                    return;

                // Try to create the directory.
                DirectoryInfo di = Directory.CreateDirectory(automationResultsAbsolutePath);
            }
            catch (Exception e)
            {
                Console.WriteLine("The process of creating a new directory for " + automationResultsAbsolutePath + " failed: {0}", e.ToString());
            }

        }

        public void TakeAndSaveScreenshotUPSPage()
        {
            ITakesScreenshot screenshotCapableDriver = driver as ITakesScreenshot;
            Screenshot screenImage = screenshotCapableDriver.GetScreenshot();

            string screenshotName = "SCREENSHOT - " + login + " - Actual ID " + actualPromoID + " - " + currentDateTime;
            screenshotPath = automationResultsAbsolutePath + screenshotName + ".jpg";

            screenImage.SaveAsFile(screenshotPath, ImageFormat.Jpeg);
        }

        public int GetCurrentPromoNumber(string oldPageSource)
        {
            int index;
            string[] splitSource;
            string tempSource = string.Empty;

            // get the value for s.eVar4
            tempSource = oldPageSource;

            splitSource = tempSource.Split(new string[] { "s.eVar4" }, StringSplitOptions.None);
            index = splitSource[1].IndexOf(@";");
            splitSource[1] = splitSource[1].Remove(index, splitSource[1].Length - index);

            // remove all non numeric charcters from string
            string promoNumberString = Regex.Replace(splitSource[1], "[^.0-9]", "");
            int promoNumber = Convert.ToInt32(promoNumberString);

            return promoNumber;
        }

        public void InitializeEmail()
        {
            string emailAddress_From = "qaautomation_donotreply@spark.net";
            string emailAddress_To1 = "lbenedicto@spark.net";
            //string emailAddress_To1 = "twoo@spark.net";
            string emailAddress_To2 = "aosband@spark.net";
            string emailAddress_CC1 = "qa@spark.net";

            //create the mail message
            mail = new MailMessage();

            //set the addresses
            mail.From = new MailAddress(emailAddress_From);
            mail.To.Add(emailAddress_To1);
            mail.To.Add(emailAddress_To2);
            mail.CC.Add(emailAddress_CC1);

            //set the content
            mail.Subject = currentSiteBeingTested + " UPS Page Promos - Automation Results - " + currentDateTime_originalCharacters;

            //first we create the Plain Text part
            plainView = AlternateView.CreateAlternateViewFromString("Here are the results for the UPS Page Promo automation, run on " + currentSiteBeingTested, null, "text/plain");

        }

        public void AddHTMLAndScreenshotsToEmailInProgress()
        {
            // add final image to bottom of page
            emailHTML += "<hr /><br>List of accounts:<br><br><img src=cid:AccountListFromQA>";

            //then we create the Html part
            //to embed images, we need to use the prefix 'cid' in the img src value
            //the cid value will map to the Content-Id of a Linked resource.
            //thus <img src='cid:companylogo'> will map to a LinkedResource with a ContentId of 'companylogo'
            htmlView = AlternateView.CreateAlternateViewFromString(emailHTML, null, "text/html");

            for (int i = 0; i < promoAccountsList.Count; i++)
            {
                //create the LinkedResource (embedded image)
                LinkedResource lResource;

                if (promoAccountsList[i].UPSPageScreenshotLocation == string.Empty)
                    break;
                else
                    lResource = new LinkedResource(promoAccountsList[i].UPSPageScreenshotLocation);

                lResource.ContentId = "UPSPageScreenshot" + i;
                //add the LinkedResource to the appropriate view
                htmlView.LinkedResources.Add(lResource);
            }

            // add the accounts that Lawrence gave me to the bottom of the page
            string accountListLocation = @"S:\Quality Assurance\Automation Builds\listOfJDateUPSPageAccounts_new3.png";

            LinkedResource finalScreenshot = new LinkedResource(accountListLocation);
            finalScreenshot.ContentId = "AccountListFromQA";
            //add the LinkedResource to the appropriate view
            htmlView.LinkedResources.Add(finalScreenshot);

        }

        public void CompleteAndSendEmailAndScreenshots()
        {
            //add the views
            mail.AlternateViews.Add(plainView);
            mail.AlternateViews.Add(htmlView);

            //send the message
            SmtpClient smtp = new SmtpClient(smtpClientAddress); //specify the mail server address
            smtp.Send(mail);
        }

    }
}
