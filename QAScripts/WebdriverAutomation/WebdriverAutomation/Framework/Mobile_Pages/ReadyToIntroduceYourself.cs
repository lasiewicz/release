﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.Events;
using NUnit.Framework;
using log4net;

// PAGE URL:  http://m.jdate.com/registration/confirmation

namespace WebdriverAutomation.Framework.Mobile_Pages
{
    public class ReadyToIntroduceYourself : BasePage
    {
        public ReadyToIntroduceYourself(IWebDriver driver)
            : base(driver)
        {
            Footer = new WebdriverAutomation.Framework.Mobile_Parts.Footer(driver);

            // verify we are on the correct page       
            IWebElement textOnPage = null;

            textOnPage = WaitUntilPageVerificationObjectExists(By.XPath("//html/body/div/header/div/h2"), "READY TO INTRODUCE YOURSELF PAGE");

            if (textOnPage.Text != (textThatVerifiesThisPageIsCorrect))
                log.Error("ERROR - READY TO INTRODUCE YOURSELF PAGE - We should be on this page but the page verification failed.");

            Assert.True(textOnPage.Text == textThatVerifiesThisPageIsCorrect, "ERROR - READY TO INTRODUCE YOURSELF PAGE - We should be on this page but the page verification failed.");

        }

        private static readonly ILog log = LogManager.GetLogger(typeof(Landing));
        public WebdriverAutomation.Framework.Mobile_Parts.Footer Footer;

        private const string textThatVerifiesThisPageIsCorrect = "Thank you for registering!";

        public Framework.Mobile_Pages.Home GoToTheHomePageButton_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//html/body/div/header/a/span"));
            ClickUntilURLChanges(element);
            
            return new Home(driver);
        }
    }
}
