﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.Events;
using NUnit.Framework;
using log4net;

// PAGE URL:  http://m.jdate.com/home/landing?destinationURL=http%3a%2f%2fm.jdate.com%2f (after deleting cookies)

namespace WebdriverAutomation.Framework.Mobile_Pages
{
    public class Landing : BasePage
    {
        public Landing(IWebDriver driver)
            : base(driver)
        {
            Footer = new WebdriverAutomation.Framework.Mobile_Parts.Footer(driver);

            // verify we are on the correct page       
            IWebElement textOnPage = null;

            textOnPage = WaitUntilPageVerificationObjectExists(By.XPath("//html/body/div[1]/div/div/h2"), "LANDING PAGE");

            if (textOnPage.Text != (textThatVerifiesThisPageIsCorrect))
                log.Error("ERROR - LANDING PAGE - We should be on this page but the page verification failed.");

            Assert.True(textOnPage.Text == textThatVerifiesThisPageIsCorrect, "ERROR - LANDING PAGE - We should be on this page but the page verification failed.");

        }

        private static readonly ILog log = LogManager.GetLogger(typeof(Landing));
        public WebdriverAutomation.Framework.Mobile_Parts.Footer Footer;

        private const string textThatVerifiesThisPageIsCorrect = "Meet Jewish Singles";

        public Framework.Mobile_Pages.Login LoginButton_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//html/body/div[1]/header/a/span/span[1]"));
            ClickUntilURLChanges(element);

            return new Login(driver);
        }

        public Framework.Mobile_Pages.Registration FreeToBrowseButton_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//div[@class='lp-content']/a"));
            ClickUntilURLChanges(element);

            return new Registration(driver);
        }
    }
}
