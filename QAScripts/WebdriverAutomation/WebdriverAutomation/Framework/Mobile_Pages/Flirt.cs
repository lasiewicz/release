﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.Events;
using NUnit.Framework;
using log4net;
using System.Threading;

// ex PAGE URL:  http://m.jdate.com/mail/viewteases/118876656

namespace WebdriverAutomation.Framework.Mobile_Pages
{
    public class Flirt : BasePage
    {
        public Flirt(IWebDriver driver)
            : base(driver)
        {
            Header = new WebdriverAutomation.Framework.Mobile_Parts.Header(driver);

            // verify we are on the correct page       
            IWebElement textOnPage = null;
            
            textOnPage = WaitUntilPageVerificationObjectExists(By.XPath("//form/div/div/h3/a/span/span"), "FLIRT PAGE");
            //html/body/div[5]/div/form/div/div/h3/a/span
            //html/body/div[5]/div/form/div/div/h3/a/span/span
            if (!textOnPage.Text.Contains(textThatVerifiesThisPageIsCorrect))
                log.Error("ERROR - FLIRT PAGE - We should be on this page but the page verification failed.");

            Assert.True(textOnPage.Text.Contains(textThatVerifiesThisPageIsCorrect), "ERROR - FLIRT PAGE - We should be on this page but the page verification failed.");

        }

        private static readonly ILog log = LogManager.GetLogger(typeof(Landing));
        public WebdriverAutomation.Framework.Mobile_Parts.Header Header;

        private const string textThatVerifiesThisPageIsCorrect = "Romantic";

        public void CasualToggle_Click()
        {
            IWebElement element = driver.FindElement(By.XPath("//form/div/div[3]/h3/a/span"));
            element.Click();
        }

        public void InterestedIAmRadiobox_Click()
        {
            IWebElement element = driver.FindElement(By.XPath("//form/div/div[3]/div/div[5]/label/span"));
            element.Click();
        }

        public Framework.Mobile_Pages.YourProfile CancelButton_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//html/body/div[4]/div/form/fieldset/div/a/span"));
            ClickUntilURLChanges(element);

            return new Framework.Mobile_Pages.YourProfile(driver);
        }

        public Framework.Mobile_Pages.FlirtSent SendFlirtButton_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//form/fieldset/div[2]/div/button"));
            ClickUntilURLChanges(element);

            return new Framework.Mobile_Pages.FlirtSent(driver);
        }
    }
}
