﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.Events;
using NUnit.Framework;
using log4net;
using System.Threading;

// ex PAGE URL:  http://m.jdate.com/mail/compose/120050738

namespace WebdriverAutomation.Framework.Mobile_Pages
{
    public class EmailMessage : BasePage
    {
        public EmailMessage(IWebDriver driver)
            : base(driver)
        {
            Header = new WebdriverAutomation.Framework.Mobile_Parts.Header(driver);

            // verify we are on the correct page       
            IWebElement textOnPage = null;

            textOnPage = WaitUntilPageVerificationObjectExists(By.XPath("//html/body/div/div/form/fieldset/label"), "EMAIL MESSAGE PAGE");

            if (!textOnPage.Text.Contains(textThatVerifiesThisPageIsCorrect))
                log.Error("ERROR - EMAIL MESSAGE PAGE - We should be on this page but the page verification failed.");

            Assert.True(textOnPage.Text.Contains(textThatVerifiesThisPageIsCorrect), "ERROR - EMAIL MESSAGE PAGE - We should be on this page but the page verification failed.");

        }

        private static readonly ILog log = LogManager.GetLogger(typeof(Landing));
        public WebdriverAutomation.Framework.Mobile_Parts.Header Header;

        private const string textThatVerifiesThisPageIsCorrect = "Subject";

        public void SubjectTextbox_Write(string text)
        {
            WaitUntilElementIsWritable(By.XPath("//input[@id='Subject']"), text);
        }

        public void BodyTextbox_Write(string text)
        {
            WaitUntilElementIsWritable(By.XPath("//textarea[@id='Body']"), text);
        }

        public void CancelButton_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//a[contains(@class, 'btn-cancel')]"));
            ClickUntilURLChanges(element);            
        }

        public Framework.Mobile_Pages.MessageSent SubmitButton_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//button[contains(@class, 'ui-btn-hidden')]"));
            ClickUntilURLChanges(element);

            return new Framework.Mobile_Pages.MessageSent(driver);
        }
    }
}
