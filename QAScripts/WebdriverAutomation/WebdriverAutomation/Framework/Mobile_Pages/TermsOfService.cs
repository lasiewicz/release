﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.Events;
using NUnit.Framework;
using log4net;
using System.Threading;

// PAGE URL:  http://m.jdate.com/home/termsandconditions/

namespace WebdriverAutomation.Framework.Mobile_Pages
{
    public class TermsOfService : BasePage
    {
        public TermsOfService(IWebDriver driver)
            : base(driver)
        {
            Header = new WebdriverAutomation.Framework.Mobile_Parts.Header(driver);
            Footer = new WebdriverAutomation.Framework.Mobile_Parts.Footer(driver);

            // verify we are on the correct page       
            IWebElement textOnPage = null;

            textOnPage = WaitUntilPageVerificationObjectExists(By.XPath("//div[@class='page-title ui-header ui-bar-b']/h2"), "TERMS OF SERVICE PAGE");

            if (textOnPage.Text != textThatVerifiesThisPageIsCorrect)
                log.Error("ERROR - TERMS OF SERVICE PAGE - We should be on this page but the page verification failed.");

            Assert.True(textOnPage.Text == textThatVerifiesThisPageIsCorrect, "ERROR - TERMS OF SERVICE PAGE - We should be on this page but the page verification failed.");

        }

        private static readonly ILog log = LogManager.GetLogger(typeof(Landing));
        public WebdriverAutomation.Framework.Mobile_Parts.Header Header;
        public WebdriverAutomation.Framework.Mobile_Parts.Footer Footer;

        private const string textThatVerifiesThisPageIsCorrect = "Terms of Service";
        
    }
}
