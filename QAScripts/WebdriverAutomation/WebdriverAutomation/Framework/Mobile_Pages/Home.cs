﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.Events;
using NUnit.Framework;
using log4net;
using System.Threading;

// PAGE URL:  http://m.jdate.com/ (after logging in)

namespace WebdriverAutomation.Framework.Mobile_Pages
{
    public class Home : BasePage
    {
        public Home(IWebDriver driver)
            : base(driver)
        {
            Footer = new WebdriverAutomation.Framework.Mobile_Parts.Footer(driver);

            // verify we are on the correct page       
            IWebElement textOnPage = null;

            textOnPage = WaitUntilPageVerificationObjectExists(By.XPath("//li[contains(@class, 'homelink-search ui-btn')]/div/div/a"), "HOME PAGE");

            if (textOnPage.Text != (textThatVerifiesThisPageIsCorrect))
                log.Error("ERROR - HOME PAGE - We should be on this page but the page verification failed.");

            Assert.True(textOnPage.Text == textThatVerifiesThisPageIsCorrect, "ERROR - HOME PAGE - We should be on this page but the page verification failed.");

        }

        private static readonly ILog log = LogManager.GetLogger(typeof(Landing));
        public WebdriverAutomation.Framework.Mobile_Parts.Footer Footer;

        private const string textThatVerifiesThisPageIsCorrect = "Search";

        public Framework.Mobile_Pages.Search SearchLink_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//li[contains(@class, 'homelink-search ui-btn')]/div/div/a"));
            ClickUntilURLChanges(element);

            // It takes a little time to load new pages from the Home Page
            Thread.Sleep(1000);

            return new Framework.Mobile_Pages.Search(driver);
        }

        public Framework.Mobile_Pages.Subscribe InboxLink_Click_JDateNonSubscribedUser()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//li[contains(@class, 'homelink-inbox ui-btn')]/div/div/a"));
            ClickUntilURLChanges(element);

            // It takes a little time to load new pages from the Home Page
            Thread.Sleep(1000);

            return new Framework.Mobile_Pages.Subscribe(driver);
        }

        public Framework.Mobile_Pages.Inbox InboxLink_Click_JDateSubscribedUser()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//li[contains(@class, 'homelink-inbox ui-btn')]/div/div/a"));
            ClickUntilURLChanges(element);

            // It takes a little time to load new pages from the Home Page
            Thread.Sleep(1000);

            return new Framework.Mobile_Pages.Inbox(driver);
        }

        public Framework.Mobile_Pages.ViewedYou ViewedYouLink_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//li[contains(@class, 'homelink-viewedyou ui-btn')]/div/div/a"));
            ClickUntilURLChanges(element);

            // It takes a little time to load new pages from the Home Page
            Thread.Sleep(1000);

            return new Framework.Mobile_Pages.ViewedYou(driver);
        }

        public Framework.Mobile_Pages.FavoritedYou FavoritedYouLink_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//li[contains(@class, 'homelink-favoritedyou ui-btn')]/div/div/a"));
            ClickUntilURLChanges(element);

            // It takes a little time to load new pages from the Home Page
            Thread.Sleep(1000);

            return new Framework.Mobile_Pages.FavoritedYou(driver);
        }

        public Framework.Mobile_Pages.YourFavorites YourFavoritesLink_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//li[contains(@class, 'homelink-yourfavorites ui-btn')]/div/div/a"));
            ClickUntilURLChanges(element);

            // It takes a little time to load new pages from the Home Page
            Thread.Sleep(1000);

            return new Framework.Mobile_Pages.YourFavorites(driver);
        }

        public Framework.Mobile_Pages.YourProfile YourProfileLink_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//li[contains(@class, 'homelink-yourprofile ui-btn')]/div/div/a"));
            ClickUntilURLChanges(element);

            // It takes a little time to load new pages from the Home Page
            Thread.Sleep(1000);

            return new Framework.Mobile_Pages.YourProfile(driver);
        }

        public Framework.Mobile_Pages.UploadPhotos UploadPhotosLink_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//li[contains(@class, 'homelink-uploadphoto ui-btn')]/div/div/a"));
            ClickUntilURLChanges(element);

            // It takes a little time to load new pages from the Home Page
            Thread.Sleep(1000);

            return new Framework.Mobile_Pages.UploadPhotos(driver);
        }

    }
}
