﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.Events;
using NUnit.Framework;
using log4net;
using System.Threading;

// ex PAGE URL:  http://m.jdate.com/mail/viewteases/118876656#&ui-state=dialog

namespace WebdriverAutomation.Framework.Mobile_Pages
{
    public class FlirtSent : BasePage
    {
        public FlirtSent(IWebDriver driver)
            : base(driver)
        {
            Header = new WebdriverAutomation.Framework.Mobile_Parts.Header(driver);

            // verify we are on the correct page       
            IWebElement textOnPage = null;

            textOnPage = WaitUntilPageVerificationObjectExists(By.XPath("//div[@class='ui-body ui-body-e confirmation']"), "FLIRT SENT PAGE");

            if (!textOnPage.Text.Contains(textThatVerifiesThisPageIsCorrect1) && !textOnPage.Text.Contains(textThatVerifiesThisPageIsCorrect2))
                log.Error("ERROR - FLIRT SENT PAGE - We should be on this page but the page verification failed.");

            Assert.True(textOnPage.Text.Contains(textThatVerifiesThisPageIsCorrect1) || textOnPage.Text.Contains(textThatVerifiesThisPageIsCorrect2), "ERROR - FLIRT SENT PAGE - We should be on this page but the page verification failed.");

        }

        private static readonly ILog log = LogManager.GetLogger(typeof(Landing));
        public WebdriverAutomation.Framework.Mobile_Parts.Header Header;

        private const string textThatVerifiesThisPageIsCorrect1 = "Your flirt has been sent";
        private const string textThatVerifiesThisPageIsCorrect2 = "Failed to send flirt. You have already flirted with this user";

        public string GetFlirtMessage()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//div[@class='ui-body ui-body-e confirmation']"));



            return element.Text;
        }

        public Framework.Mobile_Pages.YourProfile ReturnToProfileButton_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//html/body/div[4]/div/div/a/span"));
            ClickUntilURLChanges(element);

            return new Framework.Mobile_Pages.YourProfile(driver);
        }
    }
}
