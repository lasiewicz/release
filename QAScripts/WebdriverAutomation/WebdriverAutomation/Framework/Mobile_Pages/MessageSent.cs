﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.Events;
using NUnit.Framework;
using log4net;
using System.Threading;

// ex PAGE URL:  http://m.jdate.com/mail/compose/120050738

namespace WebdriverAutomation.Framework.Mobile_Pages
{
    public class MessageSent : BasePage
    {
        public MessageSent(IWebDriver driver)
            : base(driver)
        {
            Header = new WebdriverAutomation.Framework.Mobile_Parts.Header(driver);

            // verify we are on the correct page       
            IWebElement textOnPage = null;

            textOnPage = WaitUntilPageVerificationObjectExists(By.XPath("//div[@class='ui-body ui-body-e confirmation']/p"), "MESSAGE SENT PAGE");

            if (textOnPage.Text != textThatVerifiesThisPageIsCorrect)
                log.Error("ERROR - MESSAGE SENT PAGE - We should be on this page but the page verification failed.");

            Assert.True(textOnPage.Text == textThatVerifiesThisPageIsCorrect, "ERROR - MESSAGE SENT PAGE - We should be on this page but the page verification failed.");

        }

        private static readonly ILog log = LogManager.GetLogger(typeof(Landing));
        public WebdriverAutomation.Framework.Mobile_Parts.Header Header;

        private const string textThatVerifiesThisPageIsCorrect = "Your message has been sent.";

        public Framework.Mobile_Pages.YourProfile ReturnToProfileButton_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//div[@class='ui-corner-bottom ui-content ui-body-c']/a"));        
            ClickUntilURLChanges(element);

            return new Framework.Mobile_Pages.YourProfile(driver);
        }
    }
}
