﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.Events;
using NUnit.Framework;
using log4net;

// PAGE URL:  http://m.jdate.com/logon/logon/

namespace WebdriverAutomation.Framework.Mobile_Pages
{
    public class Login : BasePage
    {
        public Login(IWebDriver driver)
            : base(driver)
        {
            Footer = new WebdriverAutomation.Framework.Mobile_Parts.Footer(driver);

            // verify we are on the correct page       
            IWebElement textOnPage = null;

            textOnPage = WaitUntilPageVerificationObjectExists(By.XPath("//div[@class='non-member']/h2"), "LOGIN PAGE");

            if (textOnPage.Text != (textThatVerifiesThisPageIsCorrect))
                log.Error("ERROR - LOGIN PAGE - We should be on this page but the page verification failed.");

            Assert.True(textOnPage.Text == textThatVerifiesThisPageIsCorrect, "ERROR - LOGIN PAGE - We should be on this page but the page verification failed.");

        }

        private static readonly ILog log = LogManager.GetLogger(typeof(Landing));
        public WebdriverAutomation.Framework.Mobile_Parts.Footer Footer;

        private const string textThatVerifiesThisPageIsCorrect = "Not a member yet? Join Now!";

        public void EmailTextbox_Write(string text)
        {
            string thisXPath = "//input[@id='Email']";

            WaitUntilElementValueCanBeCleared(By.XPath(thisXPath));

            IWebElement element = WaitUntilElementExists(By.XPath(thisXPath));
            SendKeys(element, text);
        }

        public void PasswordTextbox_Write(string text)
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//input[@id='Password']"));
            SendKeys(element, text);
        }

        public Framework.Mobile_Pages.Home LoginButton_Click(string username)
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//a[@id='mainloginbutton']/span"));
            try
            {
                ClickUntilURLChanges(element);
            }
            catch
            {
                IWebElement errorField = WaitUntilElementExists(By.XPath("//form[@id='loginformmain']/div/ul/li"));

                if (errorField.Text == "Authentication system failure. Unabled to complete API login call")
                    Assert.Fail("We are trying to login to the mobile site but are failing due to the error, \"Authentication system failure.  Unabled to complete API login call\"");
                else
                    Assert.Fail("We are trying to login to the mobile site but are failing due to an error.");
            }
            
            return new Framework.Mobile_Pages.Home(driver);
        }
    }
}
