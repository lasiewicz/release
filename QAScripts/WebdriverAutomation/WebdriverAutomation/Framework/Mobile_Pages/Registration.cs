﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.Events;
using NUnit.Framework;
using log4net;

// PAGE URL:  http://m.jdate.com/registration/step1

namespace WebdriverAutomation.Framework.Mobile_Pages
{
    public class Registration : BasePage
    {
        public Registration(IWebDriver driver)
            : base(driver)
        {
            Footer = new WebdriverAutomation.Framework.Mobile_Parts.Footer(driver);

            // verify we are on the correct page       
            IWebElement textOnPage = null;

            textOnPage = WaitUntilPageVerificationObjectExists(By.XPath("//html/body/div[1]/header/div/h2"), "REGISTRATION PAGE");

            if (!textOnPage.Text.Contains(textThatVerifiesThisPageIsCorrect))
                log.Error("ERROR - REGISTRATION PAGE - We should be on this page but the page verification failed.");

            Assert.True(textOnPage.Text.Contains(textThatVerifiesThisPageIsCorrect), "ERROR - REGISTRATION PAGE - We should be on this page but the page verification failed.");

        }

        private static readonly ILog log = LogManager.GetLogger(typeof(Landing));
        public WebdriverAutomation.Framework.Mobile_Parts.Footer Footer;

        private const string textThatVerifiesThisPageIsCorrect = "Registration - ";

        //// 10/9/12 You are a / seeking a radio buttons replaced by select list 
        //public int YouAreARadioButton
        //{
        //    get
        //    {
        //        IWebElement element = WaitUntilElementExists(By.XPath("//html/body/div[1]/div/form/div[1]/fieldset[1]/div[2]/div[1]/label/span"));

        //        if (element.Selected)
        //            return 0;

        //        return 1;
        //    }
        //    set
        //    {
        //        IWebElement element;

        //        if (value == 0)
        //        {
        //            element = WaitUntilElementExists(By.XPath("//html/body/div[1]/div/form/div[1]/fieldset[1]/div[2]/div[1]/label/span"));
        //            element.Click();
        //        }
        //        else
        //        {
        //            element = WaitUntilElementExists(By.XPath("//html/body/div[1]/div/form/div[1]/fieldset[1]/div[2]/div[2]/label/span"));
        //            element.Click();
        //        }
        //    }
        //}

        public void YouAreARadioList_Select(int index)
        {
            SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[@id='idgendermask']")));
            select.SelectByIndex(index);
        }

        //// 10/9/12 You are a / seeking a radio buttons replaced by select list 
        //public int SeekingARadioButton
        //{
        //    get
        //    {
        //        IWebElement element = WaitUntilElementExists(By.XPath("//html/body/div[1]/div/form/div[1]/fieldset[2]/div[2]/div[1]/label/span"));

        //        if (element.Selected)
        //            return 0;

        //        return 1;
        //    }
        //    set
        //    {
        //        IWebElement element;

        //        if (value == 0)
        //        {
        //            element = WaitUntilElementExists(By.XPath("//html/body/div[1]/div/form/div[1]/fieldset[2]/div[2]/div[1]/label/span"));
        //            element.Click();
        //        }
        //        else
        //        {
        //            element = WaitUntilElementExists(By.XPath("//html/body/div[1]/div/form/div[1]/fieldset[2]/div[2]/div[2]/label/span"));
        //            element.Click();
        //        }
        //    }
        //}

        public void ZipCodeTextbox_Write(string text)
        {
            WaitUntilElementIsWritable(By.XPath("//input[@id='regionzipcode']"), text);
        }

        public void EmailTextbox_Write(string text)
        {
            WaitUntilElementIsWritable(By.XPath("//input[@id='idemailaddress']"), text);
        }

        public void ChooseAUsernameTextbox_Write(string text)
        {
            WaitUntilElementIsWritable(By.XPath("//input[@id='idusername']"), text);
        }

        public void ChooseAPasswordTextbox_Write(string text)
        {
            WaitUntilElementIsWritable(By.XPath("//input[@id='idpassword']"), text);
        }

        public void BirthdateMonthDropdown_Select(string value)
        {
            SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[@id='birthdayMonth']")));
            select.SelectByText(value);
        }

        public void BirthdateYearDropdown_Select(string value)
        {
            SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[@id='birthdayDay']")));
            select.SelectByText(value);
        }

        public void BirthdateYearTextbox_Select(string value)
        {
            SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[@id='birthdayYear']")));
            select.SelectByText(value);
        }

        public void ContinueButton()
        {
            WaitUntilElementIsClickable(By.XPath("//button[@id='formContinue']"));

            Wait(1000);
        }

        public List<string> LookingForACheckboxes_Check
        {
            set
            {
                int checkboxCount = 6;
                IWebElement checkbox = null;
                IWebElement label = null;

                // uncheck everything initially
                for (int i = 0; i < checkboxCount; i++)
                {
                    checkbox = WaitUntilElementExists(By.XPath("//html/body/div/section/form/div/div/fieldset/div[2]/div[" + (i + 2) + "]/label/span/span[2]"));

                    if (checkbox.GetAttribute("class").Contains("checkbox-on"))
                        checkbox.Click();
                }

                // check only what is passed in
                for (int i = 0; i < checkboxCount; i++)
                {
                    checkbox = WaitUntilElementExists(By.XPath("//html/body/div/section/form/div/div/fieldset/div[2]/div[" + (i + 2) + "]/label/span/span[2]"));
                    label = WaitUntilElementExists(By.XPath("//html/body/div/section/form/div/div/fieldset/div[2]/div[" + (i + 2) + "]/label/span/span"));

                    foreach (string currentValue in value)
                    {
                        if (label.Text.Contains(currentValue) && checkbox.Selected == false)
                            checkbox.Click();
                    }
                }
            }
        }

        public void RelationshipStatusDropdown_Select(int index)
        {
            SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[@id='idmaritalstatus']")));
            select.SelectByIndex(index);
        }

        public void SmokingHabitsDropdown_Select(int index)
        {
            SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[@id='idsmokinghabits']")));
            select.SelectByIndex(index);
        }

        public void DrinkingHabitsDropdown_Select(int index)
        {
            SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[@id='registration-drinking']")));
            select.SelectByIndex(index);
        }

        public void DoYouKeepKosherDropdown_Select(int index)
        {
            SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[@id='registration-kosher']")));
            select.SelectByIndex(index);
        }

        public void EducationDropdown_Select(int index)
        {
            SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[@id='ideducationlevel']")));
            select.SelectByIndex(index);
        }

        public void ContinueButton_Step2()
        {
            WaitUntilElementIsClickable(By.XPath("//button[@id='formContinue']"));

            Wait(1000);
        }

        public void OccupationDropdown_Select(int index)
        {
            SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[@id='idoccupation']")));
            select.SelectByIndex(index);
        }

        public void EthnicityDropdown_Select(int index)
        {
            SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[@id='registration-ethnicity']")));
            select.SelectByIndex(index);
        }

        public void ReligiousBackgroundDropdown_Select(int index)
        {
            SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[@id='idjdatereligion']")));
            select.SelectByIndex(index);
        }

        public void DoYouGoToSynagogueDropdown_Select(int index)
        {
            SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[@id='idsynagogueattendance']")));
            select.SelectByIndex(index);
        }

        public void IWouldLikeToRecieveSpecialOffersCheckbox_Check()
        {
            IWebElement checkbox = WaitUntilElementExists(By.XPath("//input[@id='registration-offers']"));

            if (checkbox.Selected == false)
                checkbox.Click();
        }

        public void IHaveReadAndAgreeToTheTermsAndConditionsCheckbox_Check()
        {
            IWebElement checkbox = WaitUntilElementExists(By.XPath("//input[@id='registration-terms']"));

            if (checkbox.Selected == false)
                checkbox.Click();
        }

        public Framework.Mobile_Pages.ReadyToIntroduceYourself ContinueButton_Step3()
        {
            WaitUntilElementIsClickable(By.XPath("//button[@id='formContinue']"));

            // The confirmation page (Ready To Introduce Yourself) takes forever to appear so we wait a while for it to appear before failing out if it doesn't
            for (int i = 0; i < 10; i++)
            {
                if (driver.FindElements(By.XPath("//html/body/div/div/a[2]")).Count > 0)
                    break;

                Wait(1000);

                if (i == 9)
                    Assert.Fail("In mobile registration we click the Continue button on the final registration page (page 3) but the registration complete - Ready to Introduce Yourself page never appears");
            }

            return new ReadyToIntroduceYourself(driver);
        }
    }
}
