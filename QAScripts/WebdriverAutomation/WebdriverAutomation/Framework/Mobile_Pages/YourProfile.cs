﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.Events;
using NUnit.Framework;
using log4net;
using System.Threading;

// example PAGE URL:  http://m.jdate.com/profile/fullprofile/127337890

namespace WebdriverAutomation.Framework.Mobile_Pages
{
    public class YourProfile : BasePage
    {
        public YourProfile(IWebDriver driver)
            : base(driver)
        {
            Header = new WebdriverAutomation.Framework.Mobile_Parts.Header(driver);
            Footer = new WebdriverAutomation.Framework.Mobile_Parts.Footer(driver);

            // verify we are on the correct page       
            IWebElement textOnPage = null;

            if (driver.FindElements(By.XPath("//div[@class='ui-collapsible-set']/div[1]/h3/a/span/span[1]")).Count > 0)
            {
                textOnPage = WaitUntilPageVerificationObjectExists(By.XPath("//div[@class='ui-collapsible-set']/div[1]/h3/a/span/span[1]"), "YOUR PROFILE PAGE");

                // if textOnPage is exists but is string.empty, it is because we've come to this page before.
                if (textOnPage.Text == string.Empty)
                    textOnPage = WaitUntilPageVerificationObjectExists(By.XPath("//html/body/div[4]/div/div[3]/div/h3/a/span"), "YOUR PROFILE PAGE");
            }
            else
                textOnPage = WaitUntilPageVerificationObjectExists(By.XPath("//html/body/div[4]/div/div[3]/div/h3/a/span"), "YOUR PROFILE PAGE");

            if (!textOnPage.Text.Contains(textThatVerifiesThisPageIsCorrect))
                log.Error("ERROR - YOUR PROFILE PAGE - We should be on this page but the page verification failed.");

            Assert.True(textOnPage.Text.Contains(textThatVerifiesThisPageIsCorrect), "ERROR - YOUR PROFILE PAGE - We should be on this page but the page verification failed.");

        }

        private static readonly ILog log = LogManager.GetLogger(typeof(Landing));
        public WebdriverAutomation.Framework.Mobile_Parts.Header Header;
        public WebdriverAutomation.Framework.Mobile_Parts.Footer Footer;

        private const string textThatVerifiesThisPageIsCorrect = "My Details";

        public Framework.Mobile_Pages.EmailMessage EmailLink_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//a[contains(@class, 'icon-email')]"));
            ClickUntilURLChanges(element);

            return new Framework.Mobile_Pages.EmailMessage(driver);
        }

        public Framework.Mobile_Pages.Flirt FlirtLink_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//a[contains(@class, 'icon-flirt')]"));
            ClickUntilURLChanges(element);

            return new Framework.Mobile_Pages.Flirt(driver);
        }

        /// <summary>
        /// This is for if we click on Flirt and expect an error message to appear on the page we're currently on.
        /// </summary>
        /// <returns></returns>
        public void FlirtLink_WithUserThatHasBeenFlirtedWithBefore_Click()
        {
            IWebElement statusMessage = WaitUntilElementExists(By.XPath("//span[contains(@class, 'profile-status-message')]"));
            IWebElement element = null;

            int count = 5;

            for (int i = 0; i < count; count++)
            {
                if (!statusMessage.Displayed)
                {
                    element = WaitUntilElementExists(By.XPath("//a[contains(@class, 'icon-flirt')]"));
                    element.Click();                    

                    i++;

                    Thread.Sleep(1000);
                }
                else
                    break;
            }
        }

        public bool DoesFlirtErrorMessageAppear()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//span[@id='_118915212']/span"));

            if (element.Text == "Your flirt was not able to be sent.")
                return true;

            return false;
        }

        public string FlirtMessage()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//span[@id='_118915212']/span"));
            return element.Text;
        }

        /// <summary>
        /// If the Favorite button does not appear, we assume "Unfavorite" is currently showing
        /// </summary>
        public bool DoesFavoriteButtonAppear()
        {
            IWebElement element = driver.FindElement(By.XPath("//a[contains(@class, 'icon-favorite')]/span"));

            if (element.Text.Contains("Unfavorite"))
                return false;

            return true;
        }

        public void Favorite_UnfavoriteButton_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//a[contains(@class, 'icon-favorite')]"));
            element.Click();
        }

        public bool UsernameExists()
        {
            IWebElement element = null;
            string xpathValue = "//div[@class='profile-top']//h2";

            if (driver.FindElements(By.XPath(xpathValue)).Count > 0)
            {
                element = WaitUntilElementExists(By.XPath(xpathValue));

                if (element.Text != string.Empty)
                    return true;
            }

            return false;
        }

        public bool AgeExists()
        {
            IWebElement element = null;
            string[] ageAndLocation;
            string xpathValue = "//div[@class='profile-top']/div[2]";

            if (driver.FindElements(By.XPath(xpathValue)).Count > 0)
            {
                element = WaitUntilElementExists(By.XPath(xpathValue));

                ageAndLocation = element.Text.Split(',');

                if (ageAndLocation[0] != string.Empty)
                    return true;
            }

            return false;
        }

        public bool CityExists()
        {
            IWebElement element = null;
            string[] ageAndLocation;
            string xpathValue = "//div[@class='profile-top']/div[2]";

            if (driver.FindElements(By.XPath(xpathValue)).Count > 0)
            {
                element = WaitUntilElementExists(By.XPath(xpathValue));

                ageAndLocation = element.Text.Split(',');

                if (ageAndLocation[1] != string.Empty)
                    return true;
            }

            return false;
        }

        public bool LastLoginExists()
        {
            IWebElement element = null;
            string xpathValue = "//div[@class='timestamp']";

            if (driver.FindElements(By.XPath(xpathValue)).Count > 0)
            {
                element = WaitUntilElementExists(By.XPath(xpathValue));

                if (element.Text != string.Empty)
                    return true;
            }

            return false;
        }

        public void MyDetails_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//div[@class='ui-collapsible-set']/div[1]/h3/a"));
            element.Click();
        }

        public bool MyDetailsExists()
        {
            IWebElement element = null;
            string xpath = "//div[@class='ui-collapsible-set']/div[1]/h3/a";

            if (driver.FindElements(By.XPath(xpath)).Count > 0)
            {
                element = WaitUntilElementExists(By.XPath(xpath));

                if (element.Text.Contains("My Details"))
                    return true;
            }

            return false;
        }

        public bool PhysicalInfo_Height_Exists()
        {
            IWebElement label = null;
            string xpathLabel = "//div[@class='ui-collapsible-set']/div/div/dl/dt";

            if (driver.FindElements(By.XPath(xpathLabel)).Count > 0)
                label = WaitUntilElementExists(By.XPath(xpathLabel));
            else
                return false;

            IWebElement value = null;
            string xpathValue = "//div[@class='ui-collapsible-set']/div/div/dl/dd/span";

            if (driver.FindElements(By.XPath(xpathValue)).Count > 0)
                value = WaitUntilElementExists(By.XPath(xpathValue));
            else
                return false;

            if (label.Text.Contains("Height") && value.Text != string.Empty)
                return true;

            return false;
        }

        public bool PhysicalInfo_IWeigh_Exists()
        {
            IWebElement label = null;
            string xpathLabel = "//div[@class='ui-collapsible-set']/div/div/dl/dt[2]";

            if (driver.FindElements(By.XPath(xpathLabel)).Count > 0)
                label = WaitUntilElementExists(By.XPath(xpathLabel));
            else
                return false;

            IWebElement value = null;
            string xpathValue = "//div[@class='ui-collapsible-set']/div/div/dl/dd[2]/span";

            if (driver.FindElements(By.XPath(xpathValue)).Count > 0)
                value = WaitUntilElementExists(By.XPath(xpathValue));
            else
                return false;

            if (label.Text.Contains("I weigh") && value.Text != string.Empty)
                return true;

            return false;
        }

        public bool PhysicalInfo_MyHairIs_Exists()
        {
            IWebElement label = null;
            string xpathLabel = "//div[@class='ui-collapsible-set']/div/div/dl/dt[3]";

            if (driver.FindElements(By.XPath(xpathLabel)).Count > 0)
                label = WaitUntilElementExists(By.XPath(xpathLabel));
            else
                return false;

            IWebElement value = null;
            string xpathValue = "//div[@class='ui-collapsible-set']/div/div/dl/dd[3]/span";

            if (driver.FindElements(By.XPath(xpathValue)).Count > 0)
                value = WaitUntilElementExists(By.XPath(xpathValue));
            else
                return false;

            if (label.Text.Contains("My hair is") && value.Text != string.Empty)
                return true;

            return false;
        }

        public bool PhysicalInfo_MyEyesAre_Exists()
        {
            IWebElement label = null;
            string xpathLabel = "//div[@class='ui-collapsible-set']/div/div/dl/dt[4]";

            if (driver.FindElements(By.XPath(xpathLabel)).Count > 0)
                label = WaitUntilElementExists(By.XPath(xpathLabel));
            else
                return false;

            IWebElement value = null;
            string xpathValue = "//div[@class='ui-collapsible-set']/div/div/dl/dd[4]/span";

            if (driver.FindElements(By.XPath(xpathValue)).Count > 0)
                value = WaitUntilElementExists(By.XPath(xpathValue));
            else
                return false;

            if (label.Text.Contains("My eyes are") && value.Text != string.Empty)
                return true;

            return false;
        }

        public bool PhysicalInfo_BodyStyle_Exists()
        {
            IWebElement label = null;
            string xpathLabel = "//div[@class='ui-collapsible-set']/div/div/dl/dt[5]";

            if (driver.FindElements(By.XPath(xpathLabel)).Count > 0)
                label = WaitUntilElementExists(By.XPath(xpathLabel));
            else
                return false;

            IWebElement value = null;
            string xpathValue = "//div[@class='ui-collapsible-set']/div/div/dl/dd[5]/span";

            if (driver.FindElements(By.XPath(xpathValue)).Count > 0)
                value = WaitUntilElementExists(By.XPath(xpathValue));
            else
                return false;

            if (label.Text.Contains("Body style") && value.Text != string.Empty)
                return true;

            return false;
        }

        public void InMyOwnWords_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//div[@class='ui-collapsible-set']/div[2]/h3/a"));
            element.Click();
        }

        public bool InMyOwnWordsExists()
        {
            IWebElement element = null;
            string xpath = "//div[@class='ui-collapsible-set']/div[2]/h3/a";

            if (driver.FindElements(By.XPath(xpath)).Count > 0)
            {
                element = WaitUntilElementExists(By.XPath(xpath));

                if (element.Text.Contains("In My Own Words"))
                    return true;
            }

            return false;
        }

        public void MyIdealMatch_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//div[@class='ui-collapsible-set']/div[3]/h3/a"));
            element.Click();
        }

        public bool MyIdealMatchExists()
        {
            IWebElement element = null;
            string xpath = "//div[@class='ui-collapsible-set']/div[3]/h3/a";

            if (driver.FindElements(By.XPath(xpath)).Count > 0)
            {
                element = WaitUntilElementExists(By.XPath(xpath));

                if (element.Text.Contains("My Ideal Match"))
                    return true;
            }

            return false;
        }


    }
}
