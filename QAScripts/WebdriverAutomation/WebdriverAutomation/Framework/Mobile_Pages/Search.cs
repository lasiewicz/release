﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.Events;
using NUnit.Framework;
using log4net;
using System.Threading;

// PAGE URL:  http://m.jdate.com/search/results#/search/results

namespace WebdriverAutomation.Framework.Mobile_Pages
{
    public class Search : BasePage
    {
        public Search(IWebDriver driver)
            : base(driver)
        {
            Header = new WebdriverAutomation.Framework.Mobile_Parts.Header(driver);
            Footer = new WebdriverAutomation.Framework.Mobile_Parts.Footer(driver);

            // verify we are on the correct page       
            IWebElement textOnPage = null;

            textOnPage = WaitUntilPageVerificationObjectExists(By.XPath("//a[contains(@class, 'ui-btn-active ui-state-persist ui-btn')]/span/span"), "SEARCH PAGE");
                
            if (!textOnPage.Text.Contains(textThatVerifiesThisPageIsCorrect))
                log.Error("ERROR - SEARCH PAGE - We should be on this page but the page verification failed.");

            Assert.True(textOnPage.Text.Contains(textThatVerifiesThisPageIsCorrect), "ERROR - SEARCH PAGE - We should be on this page but the page verification failed.");

        }

        private static readonly ILog log = LogManager.GetLogger(typeof(Landing));
        public WebdriverAutomation.Framework.Mobile_Parts.Header Header;
        public WebdriverAutomation.Framework.Mobile_Parts.Footer Footer;

        private const string textThatVerifiesThisPageIsCorrect = "Results";

        public void ResultsTab_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//div[contains(@class, 'ui-page ui-body-c ui-page-active')]/div/div/ul/li/a/span"));
            ClickUntilURLChanges(element);
        }

        public void PreferencesTab_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//div[contains(@class, 'ui-page ui-body-c ui-page-active')]/div/div/ul/li[2]/a"));            
            ClickUntilURLChanges(element);
        }

        public void YouAreAToggle_WomanClick()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//div[contains(@class, 'ui-page ui-body-c ui-page-active')]/div/form/div[1]/fieldset[1]/div[2]/div[1]/label/span"));
            element.Click();
        }

        public void YouAreAToggle_ManClick()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//div[contains(@class, 'ui-page ui-body-c ui-page-active')]/div/form/div[1]/fieldset[1]/div[2]/div[2]/label/span"));
            element.Click();
        }

        public void SeekingAToggle_WomanClick()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//div[contains(@class, 'ui-page ui-body-c ui-page-active')]/div/form/div[1]/fieldset[2]/div[2]/div[1]/label/span"));
            element.Click();
        }

        public void SeekingAToggle_ManClick()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//div[contains(@class, 'ui-page ui-body-c ui-page-active')]/div/form/div[1]/fieldset[2]/div[2]/div[2]/label/span"));
            element.Click();
        }

        public void AgeTextbox_Write(string text)
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//input[@id='search-preferences-minage']"));
            element.SendKeys(Keys.Backspace);
            element.SendKeys(Keys.Backspace);
            SendKeys(element, text);
        }

        public void ToTextbox_Write(string text)
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//input[@id='search-preferences-maxage']"));
            element.SendKeys(Keys.Backspace);
            element.SendKeys(Keys.Backspace);
            SendKeys(element, text);
        }

        public void LocatedWithinDropdown_Select(string value)
        {
            SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[@id='search-preferences-location-range']")));
            select.SelectByText(value);
        }

        public void ShowOnlyProfilesOfJewishMembersToggle_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//div[contains(@class, 'ui-page ui-body-c ui-page-active')]/div/form/div[4]/fieldset[1]/div/div[3]/a"));
            element.Click();
        }

        public void ShowOnlyProfilesWithPhotosToggle_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//div[contains(@class, 'ui-page ui-body-c ui-page-active')]/div/form/div[4]/fieldset[2]/div/div[3]/a"));
            element.Click();
        }

        public void SearchButton_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//div[contains(@class, 'ui-page ui-body-c ui-page-active')]/div/form/div[5]/input"));
            ClickUntilURLChanges(element);

            // wait for results to be r
            Thread.Sleep(1000);
        }

        public int SearchResultsCount()
        {
            IList<IWebElement> searchResults = driver.FindElements(By.XPath("//li[contains(@class, 'search-profile ui-btn')]"));
            return searchResults.Count;
        }

        public bool DoAllResultsDisplayUsername()
        {
            IWebElement usernameElement;
            IList<IWebElement> searchResults = driver.FindElements(By.XPath("//li[contains(@class, 'search-profile ui-btn')]"));

            for (int i = 0; i < searchResults.Count; i++)
            {
                usernameElement = WaitUntilElementExists(By.XPath("//ul[@id='search-results-list']/li[" + (i + 1) + "]/div/div/a/h3"));

                if (usernameElement.Text == string.Empty)
                    return false;
            }

            return true;
        }

        public bool SearchResultsWithinAgeRange(int minAge, int maxAge)
        {
            IWebElement ageElement;
            string[] ageAndLocation;
            string[] ageAndOtherText;
            int ageOfCurrentProfile;
            IList<IWebElement> searchResults = driver.FindElements(By.XPath("//li[contains(@class, 'search-profile ui-btn')]"));

            for (int i = 0; i < searchResults.Count; i++)
            {
                ageElement = WaitUntilElementExists(By.XPath("//ul[@id='search-results-list']/li[" + (i + 1) + "]/div/div/a/p[1]"));

                ageAndLocation = ageElement.Text.Split(',');
                ageAndOtherText = ageAndLocation[0].Split(' ');
                ageOfCurrentProfile = Convert.ToInt32(ageAndOtherText[0]);

                if (ageOfCurrentProfile < minAge || ageOfCurrentProfile > maxAge)
                    return false;
            }

            return true;
        }


        /// <summary>
        /// This is if we expect all results to be within multiple cities
        /// </summary>
        /// <param name="expectedCity"></param>
        /// <returns></returns>
        public bool SearchResultsWithinExpectedCities(List<string> expectedCities)
        {
            IWebElement ageElement;
            string[] ageAndLocation;
            string locationOfCurrentProfile;
            IList<IWebElement> searchResults = driver.FindElements(By.XPath("//li[contains(@class, 'search-profile ui-btn')]"));

            for (int i = 0; i < searchResults.Count; i++)
            {
                ageElement = WaitUntilElementExists(By.XPath("//ul[@id='search-results-list']/li[" + (i + 1) + "]/div/div/a/p[1]"));
                ageAndLocation = ageElement.Text.Split(',');

                locationOfCurrentProfile = ageAndLocation[1].Remove(0, 1);

                if (!expectedCities.Exists(element => element == locationOfCurrentProfile))
                    return false;
            }

            return true;
        }

        public bool DoAllResultsDisplayLastLoggedIn()
        {
            IWebElement lastLoggedInElement;
            IList<IWebElement> searchResults = driver.FindElements(By.XPath("//li[contains(@class, 'search-profile ui-btn')]"));

            for (int i = 0; i < searchResults.Count; i++)
            {
                lastLoggedInElement = WaitUntilElementExists(By.XPath("//ul[@id='search-results-list']/li[" + (i + 1) + "]/div/div/a/p[2]"));

                if (lastLoggedInElement.Text == string.Empty)
                    return false;
            }

            return true;
        }

        /// <summary>
        /// Clicks on a member that was returned in the search results.  
        /// Index starts at 1.
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public Framework.Mobile_Pages.YourProfile SearchResults_Click(int index)
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//ul[@id='search-results-list']/li[" + index.ToString() + "]/div/div/a/p[2]"));
            ClickUntilURLChanges(element);

            return new Framework.Mobile_Pages.YourProfile(driver);
        }

        public void ShowMoreMatchesButton_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//a[contains(@class, 'btn-showMore')]"));
            element.Click();

            // wait for results to be returned
            Thread.Sleep(1000);
        }

    }
}
