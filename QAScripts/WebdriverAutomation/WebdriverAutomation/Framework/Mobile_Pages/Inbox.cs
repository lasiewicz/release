﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.Events;
using NUnit.Framework;
using log4net;
using System.Threading;

// PAGE URL:  http://m.jdate.com/mail/Inbox

namespace WebdriverAutomation.Framework.Mobile_Pages
{
    public class Inbox : BasePage
    {
        public Inbox(IWebDriver driver)
            : base(driver)
        {
            Header = new WebdriverAutomation.Framework.Mobile_Parts.Header(driver);

            // verify we are on the correct page       
            if (!driver.Url.Contains(urlThatVerifiesThisPageIsCorrect1) && !driver.Url.Contains(urlThatVerifiesThisPageIsCorrect2))
                log.Error("ERROR - INBOX PAGE - We should be on this page but the page verification failed.");

            Assert.True(driver.Url.Contains(urlThatVerifiesThisPageIsCorrect1) || driver.Url.Contains(urlThatVerifiesThisPageIsCorrect2), "ERROR - INBOX PAGE - We should be on this page but the page verification failed.");

        }

        private static readonly ILog log = LogManager.GetLogger(typeof(Landing));
        public WebdriverAutomation.Framework.Mobile_Parts.Header Header;

        private const string urlThatVerifiesThisPageIsCorrect1 = "m.jdate.com/mail/Inbox";
        private const string urlThatVerifiesThisPageIsCorrect2 = "m.jdate.com/mail/message/deletemessage/inbox";

        public bool DoesMessageWithCorrectSubjectAppear_InboxPage(string subject)
        {
            int messageListCount = driver.FindElements(By.XPath("//ul[@id='inbox-message-list']/li[contains(@id, 'message-')]")).Count;
            IWebElement currentMessage = null;

            for (int i = 0; i < messageListCount; i++)
            {
                currentMessage = WaitUntilElementExists(By.XPath("//ul[@id='inbox-message-list']/li[" + (i + 1) + "]/div/div/a/p"));

                if (currentMessage.Text == subject)
                    return true;
            }

            return false;
        }
        
        public bool DoesMessageWithCorrectSubjectAppear_SentPage(string subject)
        {
            int messageListCount = driver.FindElements(By.XPath("//ul[@id='sent-message-list']/li[contains(@id, 'message-')]")).Count;
            IWebElement currentMessage = null;

            for (int i = 0; i < messageListCount; i++)
            {
                currentMessage = WaitUntilElementExists(By.XPath("//ul[@id='sent-message-list']/li[" + (i + 1) + "]/div/div/a/p"));

                if (currentMessage.Text == subject)
                    return true;
            }

            return false;
        }

        public Framework.Mobile_Pages.Message Message_Click(string subject)
        {
            int messageListCount = driver.FindElements(By.XPath("//ul[@id='inbox-message-list']/li[contains(@id, 'message-')]")).Count;
            IWebElement currentMessage = null;

            for (int i = 0; i < messageListCount; i++)
            {
                currentMessage = WaitUntilElementExists(By.XPath("//ul[@id='inbox-message-list']/li[" + (i + 1) + "]/div/div/a/p"));

                if (currentMessage.Text == subject)
                {
                    currentMessage.Click();
                    break;
                }

                if (i == messageListCount - 1)
                    Assert.Fail("ERROR - INBOX PAGE - The email with the subject line: '" + subject + "' that we expect to see in our inbox is not there.");
            }

            return new Framework.Mobile_Pages.Message(driver);
        }

        public void SentTab_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//a[contains(@class, 'nav-sent ui-btn')]/span"));                  
            element.Click();
        }
    }
}
