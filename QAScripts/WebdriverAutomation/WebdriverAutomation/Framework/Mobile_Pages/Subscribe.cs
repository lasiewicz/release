﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.Events;
using NUnit.Framework;
using log4net;
using System.Threading;

// PAGE URL:  https://secure.spark.net/jdatecom

namespace WebdriverAutomation.Framework.Mobile_Pages
{
    public class Subscribe : BasePage
    {
        public Subscribe(IWebDriver driver)
            : base(driver)
        {
            Header = new WebdriverAutomation.Framework.Mobile_Parts.Header(driver);

            // verify we are on the correct page       
            // jdate.com has 8 thousand different versions of this page so we're just going to use the URL to do page verifications of this
            string jdateComURL_preprod = @"https://ppsecure.spark.net/jdatecom";
            string jdateComURL_production = @"https://secure.spark.net/jdatecom";

            if ((driver.Url != jdateComURL_preprod) && driver.Url != jdateComURL_production)
                log.Error("ERROR - SUBSCRIBE PAGE - We should be on this page but the page verification failed.");

            Assert.True((driver.Url == jdateComURL_preprod) || (driver.Url == jdateComURL_production), "ERROR - SUBSCRIBE PAGE - We should be on this page but the page verification failed.");
        }

        private static readonly ILog log = LogManager.GetLogger(typeof(Landing));
        public WebdriverAutomation.Framework.Mobile_Parts.Header Header;

    }
}
