﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.Events;
using NUnit.Framework;
using log4net;
using System.Threading;

// ex PAGE URL:  http://m.jdate.com/mail/message/inbox/872215300

namespace WebdriverAutomation.Framework.Mobile_Pages
{
    public class Message : BasePage
    {
        public Message(IWebDriver driver)
            : base(driver)
        {
            Header = new WebdriverAutomation.Framework.Mobile_Parts.Header(driver);
            Footer = new WebdriverAutomation.Framework.Mobile_Parts.Footer(driver);

            // verify we are on the correct page       
            IWebElement textOnPage = null;

            textOnPage = WaitUntilPageVerificationObjectExists(By.XPath("//div[@class='message-view-content']/h3"), "MESSAGE PAGE");

            if (!textOnPage.Text.Contains(textThatVerifiesThisPageIsCorrect))
                log.Error("ERROR - HOME PAGE - We should be on this page but the page verification failed.");

            Assert.True(textOnPage.Text.Contains(textThatVerifiesThisPageIsCorrect), "ERROR - MESSAGE PAGE - We should be on this page but the page verification failed.");

        }

        private static readonly ILog log = LogManager.GetLogger(typeof(Landing));
        public WebdriverAutomation.Framework.Mobile_Parts.Header Header;
        public WebdriverAutomation.Framework.Mobile_Parts.Footer Footer;

        private const string textThatVerifiesThisPageIsCorrect = "Subject:";

        public bool DoesCorrectSubjectAppear(string subject)
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//div[@class='message-view-content']/h3"));

            if (element.Text.Contains(subject))
                return true;

            return false;
        }

        public bool DoesCorrectMessageAppear(string message)
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//div[@class='message-view-content']/p"));

            if (element.Text.Contains(message))
                return true;

            return false;
        }

        public Framework.Mobile_Pages.Inbox Delete_Click()
        {
            IWebElement element = null;

            if (driver.FindElements(By.XPath("//html/body/div[6]/div/div/ul/li[3]/a")).Count > 0)
            {
                element = WaitUntilElementExists(By.XPath("//html/body/div[6]/div/div/ul/li[3]/a"));
                ClickUntilURLChanges(element);

                // taken to a confirmation page, "Are you sure you want to delete this message?"  Click Delete.
                element = WaitUntilElementExists(By.XPath("//html/body/div[6]/div/div[2]/a"));
                ClickUntilURLChanges(element);
            }
            else
            {
                element = WaitUntilElementExists(By.XPath("//html/body/div[7]/div/div/ul/li[3]/a"));
                ClickUntilURLChanges(element);

                // taken to a confirmation page, "Are you sure you want to delete this message?"  Click Delete.
                element = WaitUntilElementExists(By.XPath("//html/body/div[7]/div/div[2]/a"));
                ClickUntilURLChanges(element);
            }

            return new Framework.Mobile_Pages.Inbox(driver);
        }
    }
}
