﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.Events;
using NUnit.Framework;
using log4net;
using System.Threading;

// PAGE URL:  http://m.jdate.com/#/hotlist/YourFavorites

namespace WebdriverAutomation.Framework.Mobile_Pages
{
    public class YourFavorites : BasePage
    {
        public YourFavorites(IWebDriver driver)
            : base(driver)
        {
            Header = new WebdriverAutomation.Framework.Mobile_Parts.Header(driver);

            // verify we are on the correct page       
            IWebElement textOnPage = null;

            textOnPage = WaitUntilPageVerificationObjectExists(By.XPath("//h2[@class='ui-title']"), "YOUR FAVORITES PAGE");

            if (!textOnPage.Text.Contains(textThatVerifiesThisPageIsCorrect))
                log.Error("ERROR - YOUR FAVORITES PAGE - We should be on this page but the page verification failed.");

            Assert.True(textOnPage.Text.Contains(textThatVerifiesThisPageIsCorrect), "ERROR - YOUR FAVORITES PAGE - We should be on this page but the page verification failed.");

        }

        private static readonly ILog log = LogManager.GetLogger(typeof(Landing));
        public WebdriverAutomation.Framework.Mobile_Parts.Header Header;

        private const string textThatVerifiesThisPageIsCorrect = "Your Favorites";

        public int SearchResultsCount()
        {
            IList<IWebElement> searchResults = driver.FindElements(By.XPath("//li[contains(@class, 'list-profile ui-btn')]"));
            return searchResults.Count;
        }

        public bool DoAllResultsDisplayUsername()
        {
            IWebElement element;
            IList<IWebElement> searchResults = driver.FindElements(By.XPath("//li[contains(@class, 'list-profile ui-btn')]"));

            for (int i = 0; i < searchResults.Count; i++)
            {
                element = WaitUntilElementExists(By.XPath("//ul[@id='yourfavorites-hotlist']/li[" + (i + 1) + "]/div/div/a/h3"));

                if (element.Text == string.Empty)
                    return false;
            }

            return true;
        }

        public bool DoAllResultsDisplayAge()
        {
            IWebElement ageElement;
            string[] ageAndLocation;
            IList<IWebElement> searchResults = driver.FindElements(By.XPath("//li[contains(@class, 'list-profile ui-btn')]"));

            for (int i = 0; i < searchResults.Count; i++)
            {
                ageElement = WaitUntilElementExists(By.XPath("//ul[@id='yourfavorites-hotlist']/li[" + (i + 1) + "]/div/div/a/p[1]"));

                ageAndLocation = ageElement.Text.Split(',');

                if (!ageAndLocation[0].Contains("years old"))
                    return false;
            }

            return true;
        }

        public bool DoAllResultsDisplayCity()
        {
            IWebElement element;
            string[] ageAndLocation;
            IList<IWebElement> searchResults = driver.FindElements(By.XPath("//li[contains(@class, 'list-profile ui-btn')]"));

            for (int i = 0; i < searchResults.Count; i++)
            {
                element = WaitUntilElementExists(By.XPath("//ul[@id='yourfavorites-hotlist']/li[" + (i + 1) + "]/div/div/a/p[1]"));

                ageAndLocation = element.Text.Split(',');

                if (ageAndLocation[1] == string.Empty)
                    return false;
            }

            return true;
        }

        public bool DoAllResultsDisplayLastLoggedIn()
        {
            IWebElement element;
            IList<IWebElement> searchResults = driver.FindElements(By.XPath("//li[contains(@class, 'list-profile ui-btn')]"));

            for (int i = 0; i < searchResults.Count; i++)
            {
                element = WaitUntilElementExists(By.XPath("//ul[@id='yourfavorites-hotlist']/li[" + (i + 1) + "]/div/div/a/p[2]"));

                if (element.Text == string.Empty)
                    return false;
            }

            return true;
        }

        /// <summary>
        /// Clicks on a member that was returned in the viewed you results.  
        /// Index starts at 1.
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public Framework.Mobile_Pages.YourProfile ViewedYouResults_Click(int index)
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//ul[@id='yourfavorites-hotlist']/li[" + index.ToString() + "]/div/div/a/p[2]"));
            ClickUntilURLChanges(element);

            return new Framework.Mobile_Pages.YourProfile(driver);
        }

        public void ShowMoreMatchesButton_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//a[contains(@class, 'btn-showMore')]"));
            element.Click();

            // wait for results to be returned
            Thread.Sleep(1000);
        }

    }
}
