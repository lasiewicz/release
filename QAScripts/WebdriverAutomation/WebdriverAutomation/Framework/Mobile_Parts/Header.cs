﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.Events;
using NUnit.Framework;

namespace WebdriverAutomation.Framework.Mobile_Parts
{
    public class Header : BasePage
    {
        public Header(IWebDriver driver)
            : base(driver)
        {
            this.driver = driver;

        }
        
        public void BackLink_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//div[contains(@class, 'ui-page ui-body-c ui-page-active')]/div/a"));
            ClickUntilURLChanges(element);
        }

        public Framework.Mobile_Pages.Home BackLink_ToHomePage_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//header[@class='ui-header ui-bar-j']/a[1]"));
            ClickUntilURLChanges(element);

            return new Mobile_Pages.Home(driver);
        }

        public Framework.Mobile_Pages.Home HomeLink_Click()
        {
            //IWebElement element = null;            

            //if (driver.FindElements(By.XPath("//html/body/div[4]/header/a[contains(@class, 'ui-btn-right ui-btn')]")).Count > 0)
            //    element = WaitUntilElementExists(By.XPath("//html/body/div[4]/header/a[contains(@class, 'ui-btn-right ui-btn')]"));
            //else
            //    element = WaitUntilElementExists(By.XPath("//a[contains(@class, 'ui-btn-right ui-btn')]"));

            //ClickUntilURLChanges(element);

            //  10/5/12 the home link which should take us to m.jdate.com has been changed for us since we are accessing it through web and not a phone.  skip directly to m.jdate.com/home in this case.
            string mobileHomeURL = string.Empty;

            if (driver.Url.Contains("preprod"))
                mobileHomeURL = "preprod.m.jdate.com/home";
            else
                mobileHomeURL = "m.jdate.com/home";

            driver.Navigate().GoToUrl(mobileHomeURL);

            return new Framework.Mobile_Pages.Home(driver);
        }

    }
}
