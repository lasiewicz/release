﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.Events;
using NUnit.Framework;

namespace WebdriverAutomation.Framework.Mobile_Parts
{
    public class Footer : BasePage
    {
        public Footer(IWebDriver driver)
            : base(driver)
        {
            this.driver = driver;

        }

        public Framework.Mobile_Pages.Login LogoutLink_Click()
        {
            // if the link is not displayed try a second xpath with a different parent div
            IWebElement element = WaitUntilElementExists(By.XPath("//a[contains(@class, 'logout-link')]"));

            if (element.Displayed)
                ClickUntilURLChanges(element);
            else if (driver.FindElements(By.XPath("//html/body/div[6]/footer/div/ul/li/a")).Count > 0)
            {
                element = WaitUntilElementExists(By.XPath("//html/body/div[6]/footer/div/ul/li/a"));
                ClickUntilURLChanges(element);
            }
            else
            {
                element = WaitUntilElementExists(By.XPath("//html/body/div[7]/footer/div/ul/li/a"));
                ClickUntilURLChanges(element);
            }

            return new Framework.Mobile_Pages.Login(driver);
        }

        public Framework.Site_Pages.Login FullSiteLink_Click()
        {
            // if the link is not displayed try a second xpath with a different parent div
            IWebElement element = WaitUntilElementExists(By.XPath("//a[contains(@class, 'fullsite-link')]"));

            if (element.Displayed)
                ClickUntilURLChanges(element);
            else
            {
                element = WaitUntilElementExists(By.XPath("//html/body/div[4]/footer/div/ul/li[2]/a/span"));
                ClickUntilURLChanges(element);
            }

            return new Framework.Site_Pages.Login(driver);
        }

        public Framework.Mobile_Pages.ContactUs ContactUsLink_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//ul[@class='footer-links']/li/a"));           
            ClickUntilURLChanges(element);
            
            return new Framework.Mobile_Pages.ContactUs(driver);
        }

        public Framework.Mobile_Pages.Safety SafetyLink_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//ul[@class='footer-links']/li[2]/a"));
            ClickUntilURLChanges(element);

            return new Framework.Mobile_Pages.Safety(driver);
        }

        public Framework.Mobile_Pages.Privacy PrivacyLink_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//ul[@class='footer-links']/li[3]/a"));
            ClickUntilURLChanges(element);

            return new Framework.Mobile_Pages.Privacy(driver);
        }

        public Framework.Mobile_Pages.TermsOfService TermsOfServiceLink_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//ul[@class='footer-links']/li[4]/a"));
            ClickUntilURLChanges(element);

            return new Framework.Mobile_Pages.TermsOfService(driver);
        }

    }
}
