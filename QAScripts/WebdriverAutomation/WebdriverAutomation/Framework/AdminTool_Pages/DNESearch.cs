﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.Events;
using NUnit.Framework;
using log4net;

// PAGE URL:  http://admintool.matchnet.com/DNE/Search

namespace WebdriverAutomation.Framework.AdminTool_Pages
{
    public class DNESearch : BasePage
    {
        public DNESearch(IWebDriver driver)
            : base(driver)
        {
            Header = new WebdriverAutomation.Framework.AdminTool_Parts.Header(driver);

            // verify we are on the correct page       
            //IWebElement textOnPage = null;

            if (!driver.Url.Contains("admintool.matchnet.com/DNE/Search"))
                log.Error("ERROR - ADMIN TOOL - DNE SEARCH PAGE - We should be on this page but the page verification failed.");

            Assert.True(driver.Url.Contains("admintool.matchnet.com/DNE/Search"), "ERROR - ADMIN TOOL - DNE SEARCH PAGE - We should be on this page but the page verification failed.");

        }

        private static readonly ILog log = LogManager.GetLogger(typeof(FreeTextApproval));
        public WebdriverAutomation.Framework.AdminTool_Parts.Header Header;

    }
}
