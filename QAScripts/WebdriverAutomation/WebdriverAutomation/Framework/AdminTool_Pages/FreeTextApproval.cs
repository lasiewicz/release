﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.Events;
using NUnit.Framework;
using log4net;

// PAGE URL:  http://admintool.matchnet.com/Approval/ViewTextByLanguage/2
//     we only deal with the English language page

namespace WebdriverAutomation.Framework.AdminTool_Pages
{
    public class FreeTextApproval : BasePage
    {
        public FreeTextApproval(IWebDriver driver)
            : base(driver)
        {
            Header = new WebdriverAutomation.Framework.AdminTool_Parts.Header(driver);

            // verify we are on the correct page       
            if (!driver.Url.Contains("ViewTextByLanguage"))
                log.Error("ERROR - ADMIN TOOL - FREE TEXT APPROVAL PAGE - We should be on this page but the page verification failed.");

            Assert.True(driver.Url.Contains("ViewTextByLanguage"), "ERROR - ADMIN TOOL - FREE TEXT APPROVAL PAGE - We should be on this page but the page verification failed.");
        }

        private static readonly ILog log = LogManager.GetLogger(typeof(FreeTextApproval));
        public WebdriverAutomation.Framework.AdminTool_Parts.Header Header; 

    }
}
