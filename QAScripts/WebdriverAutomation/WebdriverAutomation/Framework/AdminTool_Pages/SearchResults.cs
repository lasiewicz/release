﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.Events;
using NUnit.Framework;
using log4net;

// PAGE URL example:  http://admintool.matchnet.com/Member/View/9041/126715966

namespace WebdriverAutomation.Framework.AdminTool_Pages
{
    public class SearchResults : BasePage
    {
        public SearchResults(IWebDriver driver)
            : base(driver)
        {
            Header = new WebdriverAutomation.Framework.AdminTool_Parts.Header(driver);

            // verify we are on the correct page       
            IWebElement textOnPage = null;

            textOnPage = WaitUntilPageVerificationObjectExists(By.XPath("//li[@id='AdminSuspendedDiv']/label"), "ADMIN TOOL - SEARCH RESULTS PAGE");

            if (textOnPage.Text != (textThatVerifiesThisPageIsCorrect))
                log.Error("ERROR - ADMIN TOOL - APPROVAL COUNTS PAGE - We should be on this page but the page verification failed.");

            Assert.True(textOnPage.Text == textThatVerifiesThisPageIsCorrect, "ERROR - ADMIN TOOL - SEARCH RESULTS PAGE - We should be on this page but the page verification failed.");

        }

        private static readonly ILog log = LogManager.GetLogger(typeof(FreeTextApproval));
        public WebdriverAutomation.Framework.AdminTool_Parts.Header Header;

        private const string textThatVerifiesThisPageIsCorrect = "Admin Suspended:";

    }
}
