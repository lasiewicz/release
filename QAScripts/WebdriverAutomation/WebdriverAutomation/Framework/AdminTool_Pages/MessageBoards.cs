﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.Events;
using NUnit.Framework;
using log4net;

// PAGE URL:  http://connect.jdate.com/boards/index.html

namespace WebdriverAutomation.Framework.AdminTool_Pages
{
    public class MessageBoards : BasePage
    {
        public MessageBoards(IWebDriver driver)
            : base(driver)
        {
            // verify we are on the correct page       
            IWebElement textOnPage = null;

            textOnPage = WaitUntilPageVerificationObjectExists(By.XPath("//div[@id='body_column']/h1"), "MESSAGE BOARDS PAGE");

            if (textOnPage.Text != (textThatVerifiesThisPageIsCorrect))
                log.Error("ERROR - MESSAGE BOARDS PAGE - We should be on this page but the page verification failed.");

            Assert.True(textOnPage.Text == textThatVerifiesThisPageIsCorrect, "ERROR - MESSAGE BOARDS PAGE - We should be on this page but the page verification failed.");
        }

        private static readonly ILog log = LogManager.GetLogger(typeof(MessageBoards));

        private const string textThatVerifiesThisPageIsCorrect = "Message Boards";



    }
}
