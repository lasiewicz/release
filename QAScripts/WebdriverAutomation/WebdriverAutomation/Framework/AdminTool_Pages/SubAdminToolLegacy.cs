﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.Events;
using NUnit.Framework;
using log4net;

// PAGE URL:  http://admin.jdate.com/applications/admin/report/actionreport.aspx

namespace WebdriverAutomation.Framework.AdminTool_Pages
{
    public class SubAdminToolLegacy : BasePage
    {
        public SubAdminToolLegacy(IWebDriver driver)
            : base(driver)
        {
            Header = new WebdriverAutomation.Framework.Site_Parts.Header(driver);

            // verify we are on the correct page       
            IWebElement textOnPage = null;

            textOnPage = WaitUntilPageVerificationObjectExists(By.XPath("//table[contains(@id, '_rblAction')]/tbody/tr[1]/td/label"), "ADMIN TOOL - SUB ADMIN TOOL (LEGACY) PAGE");

            if (textOnPage.Text != (textThatVerifiesThisPageIsCorrect))
                log.Error("ERROR - ADMIN TOOL - SUB ADMIN TOOL (LEGACY) PAGE - We should be on this page but the page verification failed.");

            Assert.True(textOnPage.Text == textThatVerifiesThisPageIsCorrect, "ERROR - ADMIN TOOL - SUB ADMIN TOOL (LEGACY) PAGE - We should be on this page but the page verification failed.");

        }

        private static readonly ILog log = LogManager.GetLogger(typeof(FreeTextApproval));
        public WebdriverAutomation.Framework.Site_Parts.Header Header;

        private const string textThatVerifiesThisPageIsCorrect = "Create a new subscription plan";

    }
}
