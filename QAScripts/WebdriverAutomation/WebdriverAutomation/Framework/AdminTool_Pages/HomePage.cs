﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.Events;
using NUnit.Framework;
using log4net;

// PAGE URL:  http://admintool.matchnet.com/

namespace WebdriverAutomation.Framework.AdminTool_Pages
{
    public class HomePage : BasePage
    {
        public HomePage(IWebDriver driver)
            : base(driver)
        {
            Header = new WebdriverAutomation.Framework.AdminTool_Parts.Header(driver);

            // verify we are on the correct page       
            IWebElement textOnPage = null;

            textOnPage = WaitUntilPageVerificationObjectExists(By.XPath("//div[@id='member-search-sidebar']/p[1]"), "ADMIN TOOL - HOME PAGE");

            if (textOnPage.Text != (textThatVerifiesThisPageIsCorrect))
                log.Error("ERROR - ADMIN TOOL - HOME PAGE - We should be on this page but the page verification failed.");

            Assert.True(textOnPage.Text == textThatVerifiesThisPageIsCorrect, "ERROR - ADMIN TOOL - HOME PAGE - We should be on this page but the page verification failed.");
        }

        private static readonly ILog log = LogManager.GetLogger(typeof(HomePage));
        public WebdriverAutomation.Framework.AdminTool_Parts.Header Header; 

        private const string textThatVerifiesThisPageIsCorrect = "Search Criteria";

        public void EmailTextbox_Write(string text)
        {
            string thisXPath = "//input[@id='Email']";

            WaitUntilElementValueCanBeCleared(By.XPath(thisXPath));

            IWebElement element = WaitUntilElementExists(By.XPath(thisXPath));
            SendKeys(element, text);
        }

        public void UsernameTextbox_Write(string text)
        {
            string thisXPath = "//input[@id='UserName']";

            WaitUntilElementValueCanBeCleared(By.XPath(thisXPath));

            IWebElement element = WaitUntilElementExists(By.XPath(thisXPath));
            SendKeys(element, text);
        }

        public void CheckToSearchExactUsernameCheckbox_Check()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//input[@id='ExactUsernameOnly']"));

            if (element.Selected == false)
                element.Click();
        }

        public void MemberIDTextbox_Write(string text)
        {
            string thisXPath = "//input[@id='MemberID']";

            WaitUntilElementValueCanBeCleared(By.XPath(thisXPath));

            IWebElement element = WaitUntilElementExists(By.XPath(thisXPath));
            SendKeys(element, text);
        }
        

        public void PhoneTextbox_Write(string text)
        {
            string thisXPath = "//input[@id='PhoneNumber']";

            WaitUntilElementValueCanBeCleared(By.XPath(thisXPath));

            IWebElement element = WaitUntilElementExists(By.XPath(thisXPath));
            SendKeys(element, text);
        }

        public Framework.AdminTool_Pages.SearchResults SearchButton_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//div[@id='member-search-main']/dl[1]/dd[5]/input[1]"));
            ClickUntilURLChanges(element);

            return new SearchResults(driver);
        }

        public void ClearButton_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//div[@id='member-search-main']/dl[1]/dd[5]/input[2]"));
            element.Click();
        }

    }
}
