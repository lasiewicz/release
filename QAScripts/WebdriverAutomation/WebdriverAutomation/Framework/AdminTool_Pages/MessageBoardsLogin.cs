﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.Events;
using NUnit.Framework;
using log4net;

// PAGE URL:  http://connect.jdate.com/admin_login.html
//     note: when the link for this is clicked a new window appears
//     we only deal with the JDate site

namespace WebdriverAutomation.Framework.AdminTool_Pages
{
    public class MessageBoardsLogin : BasePage
    {
        public MessageBoardsLogin(IWebDriver driver)
            : base(driver)
        {
            // verify we are on the correct page      
            IWebElement textOnPage = null;

            textOnPage = WaitUntilPageVerificationObjectExists(By.XPath("//form[@id='loginform']/fieldset/p"), "ADMIN TOOL - MESSAGE BOARDS LOGIN PAGE");

            if (textOnPage.Text != (textThatVerifiesThisPageIsCorrect))
                log.Error("ERROR - ADMIN TOOL - MESSAGE BOARDS LOGIN PAGE - We should be on this page but the page verification failed.");

            Assert.True(textOnPage.Text == textThatVerifiesThisPageIsCorrect, "ERROR - ADMIN TOOL - MESSAGE BOARDS LOGIN PAGE - We should be on this page but the page verification failed.");

        }

        private static readonly ILog log = LogManager.GetLogger(typeof(MessageBoardsLogin));

        private const string textThatVerifiesThisPageIsCorrect = "Please enter your username and password";

        public string EmailTextbox
        {
            get
            {
                string thisXPath = "//input[@id='email']";

                IWebElement emailTextbox = WaitUntilElementExists(By.XPath(thisXPath));
                return emailTextbox.GetAttribute("value");
            }
            set
            {
                string thisXPath = "//input[@id='email']";

                WaitUntilElementValueCanBeCleared(By.XPath(thisXPath));

                IWebElement emailTextbox = WaitUntilElementExists(By.XPath(thisXPath));
                SendKeys(emailTextbox, value);
            }
        }

        public string PasswordTextbox
        {
            get
            {
                string thisXPath = "//input[@id='password']";

                IWebElement passwordTextbox = WaitUntilElementExists(By.XPath(thisXPath));
                return passwordTextbox.GetAttribute("value");
            }
            set
            {
                string thisXPath = "//input[@id='password']";

                WaitUntilElementValueCanBeCleared(By.XPath(thisXPath));

                IWebElement passwordTextbox = WaitUntilElementExists(By.XPath(thisXPath));
                SendKeys(passwordTextbox, value);
            }
        }

        public WebdriverAutomation.Framework.AdminTool_Pages.MessageBoards LogInButton_Click()
        {
            IWebElement loginButton = WaitUntilElementExists(By.XPath("//input[@id='submit']"));
            ClickUntilURLChanges(loginButton);

            return new WebdriverAutomation.Framework.AdminTool_Pages.MessageBoards(driver);
        }
    }
}
