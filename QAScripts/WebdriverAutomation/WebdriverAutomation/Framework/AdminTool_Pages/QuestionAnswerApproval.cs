﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.Events;
using NUnit.Framework;
using log4net;

// PAGE URL:  http://admintool.matchnet.com/Approval/ViewQATextByLanguage/2
//     we only deal with the English site

namespace WebdriverAutomation.Framework.AdminTool_Pages
{
    public class QuestionAnswerApproval : BasePage
    {
        public QuestionAnswerApproval(IWebDriver driver)
            : base(driver)
        {
            Header = new WebdriverAutomation.Framework.AdminTool_Parts.Header(driver);

            // verify we are on the correct page       
            if (!driver.Url.Contains("ViewQATextByLanguage/2"))
                log.Error("ERROR - ADMIN TOOL - QUESTION/ANSWER APPROVAL PAGE - We should be on this page but the page verification failed.");

            Assert.True(driver.Url.Contains("ViewQATextByLanguage/2"), "ERROR - ADMIN TOOL - QUESTION/ANSWER APPROVAL PAGE - We should be on this page but the page verification failed.");
        }

        private static readonly ILog log = LogManager.GetLogger(typeof(QuestionAnswerApproval));
        public WebdriverAutomation.Framework.AdminTool_Parts.Header Header; 

    }
}
