﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.Events;
using NUnit.Framework;
using log4net;

// PAGE URL:  http://admintool.matchnet.com/Approval/ViewPhotosBySite/103
//     we only deal with the JDate.com site

namespace WebdriverAutomation.Framework.AdminTool_Pages
{
    public class PhotoApproval : BasePage
    {
        public PhotoApproval(IWebDriver driver)
            : base(driver)
        {
            Header = new WebdriverAutomation.Framework.AdminTool_Parts.Header(driver);

            // verify we are on the correct page       
            if (!driver.Url.Contains("ViewPhotosBySite/103"))
                log.Error("ERROR - ADMIN TOOL - FREE TEXT APPROVAL PAGE - We should be on this page but the page verification failed.");

            Assert.True(driver.Url.Contains("ViewPhotosBySite/103"), "ERROR - ADMIN TOOL - FREE TEXT APPROVAL PAGE - We should be on this page but the page verification failed.");
        }

        private static readonly ILog log = LogManager.GetLogger(typeof(PhotoApproval));
        public WebdriverAutomation.Framework.AdminTool_Parts.Header Header; 

    }
}
