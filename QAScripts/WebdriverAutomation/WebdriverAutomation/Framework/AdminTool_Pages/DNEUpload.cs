﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.Events;
using NUnit.Framework;
using log4net;

// PAGE URL:  http://admintool.matchnet.com/DNE/Upload

namespace WebdriverAutomation.Framework.AdminTool_Pages
{
    public class DNEUpload : BasePage
    {
        public DNEUpload(IWebDriver driver)
            : base(driver)
        {
            // verify we are on the correct page       
            if (!driver.Url.Contains("admintool.matchnet.com/DNE/Upload"))
                log.Error("ERROR - ADMIN TOOL - DNE UPLOAD PAGE - We should be on this page but the page verification failed.");

            Assert.True(driver.Url.Contains("admintool.matchnet.com/DNE/Upload"), "ERROR - ADMIN TOOL - DNE UPLOAD PAGE - We should be on this page but the page verification failed.");
        }

        private static readonly ILog log = LogManager.GetLogger(typeof(FreeTextApproval));

    }
}
