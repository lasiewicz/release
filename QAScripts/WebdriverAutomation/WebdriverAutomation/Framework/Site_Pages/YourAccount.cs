﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.Events;
using NUnit.Framework;
using log4net;

// PAGE URL:  http://www.jdate.com/Applications/MemberServices/MemberServices.aspx (while logged in)

namespace WebdriverAutomation.Framework.Site_Pages
{
    public class YourAccount : BasePage
    {
        public YourAccount(IWebDriver driver)
            : base(driver)
        {
            Header = new WebdriverAutomation.Framework.Site_Parts.Header(driver);
            Footer = new WebdriverAutomation.Framework.Site_Parts.Footer(driver);

            Tests.ReusableCode.Ads ads = new Tests.ReusableCode.Ads();
            ads.KillAllAds(driver, log);

            if (driver.Url.Contains("blacksingles.com") || driver.Url.Contains("bbwpersonalsplus.com"))
                textThatVerifiesThisPageIsCorrect = "Member Services";
            else if (driver.Url.Contains("jdate.co.il"))
                textThatVerifiesThisPageIsCorrect = "כלים והגדרות";
            else
                textThatVerifiesThisPageIsCorrect = "Your Account";

            // verify we are on the correct page       
            IWebElement textOnPage = null;

            textOnPage = WaitUntilPageVerificationObjectExists(By.XPath("//div[@id='min-max-container']/div[@id='content-container']/div[2]/div[@id='content-main']/div[@id='page-container']/h1"), "YOUR ACCOUNT PAGE");

            if (textOnPage.Text != textThatVerifiesThisPageIsCorrect)
                log.Error("ERROR - YOUR ACCOUNT PAGE - We should be on this page but the page verification failed.");

            Assert.True(textOnPage.Text == textThatVerifiesThisPageIsCorrect, "ERROR - YOUR ACCOUNT PAGE - We should be on this page but the page verification failed.");

        }

        private static readonly ILog log = LogManager.GetLogger(typeof(YourAccount));
        public WebdriverAutomation.Framework.Site_Parts.Header Header;
        public WebdriverAutomation.Framework.Site_Parts.Footer Footer;

        private string textThatVerifiesThisPageIsCorrect = string.Empty;

        #region Profile
        public WebdriverAutomation.Framework.Site_Pages.ProfileDisplaySettings ProfileDisplaySettingsLink_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//div[@id='page-container']/ul[1]/li[1]/a"));
            ClickUntilURLChanges(element);

            return new WebdriverAutomation.Framework.Site_Pages.ProfileDisplaySettings(driver);
        }

        public WebdriverAutomation.Framework.Site_Pages.YourProfile_old ChangeYourProfileLink_ExpectingOldProfile_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//div[@id='page-container']/ul[1]/li[2]/a"));
            ClickUntilURLChanges(element);

            return new WebdriverAutomation.Framework.Site_Pages.YourProfile_old(driver);
        }

        public WebdriverAutomation.Framework.Site_Pages.YourProfile ChangeYourProfileLink_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//div[@id='page-container']/ul[1]/li[2]/a"));
            ClickUntilURLChanges(element);

            return new WebdriverAutomation.Framework.Site_Pages.YourProfile(driver);
        }

        public WebdriverAutomation.Framework.Site_Pages.ChangeYourEmailOrPassword ChangeYourEmailOrPasswordLink_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//div[@id='page-container']/ul[1]/li[3]/a"));
            ClickUntilURLChanges(element);

            return new WebdriverAutomation.Framework.Site_Pages.ChangeYourEmailOrPassword(driver);
        }

        public WebdriverAutomation.Framework.Site_Pages.ColorCodeSettings ColorCodeSettingsLink_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//div[@id='page-container']/ul[1]/li[4]/a"));
            ClickUntilURLChanges(element);

            return new WebdriverAutomation.Framework.Site_Pages.ColorCodeSettings(driver);
        }
        #endregion

        #region Membership Management
        public WebdriverAutomation.Framework.Site_Pages.Subscribe MembershipPlansAndCostsLink_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//div[@id='page-container']/ul[2]/li[1]/a"));
            ClickUntilURLChanges(element);

            return new WebdriverAutomation.Framework.Site_Pages.Subscribe(driver);
        }

        public WebdriverAutomation.Framework.Site_Pages.AutoRenewalSettings AutoRenewalSettingsLink_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//div[@id='page-container']/ul[2]/li[2]/a"));
            ClickUntilURLChanges(element);

            return new WebdriverAutomation.Framework.Site_Pages.AutoRenewalSettings(driver);
        }

        public WebdriverAutomation.Framework.Site_Pages.Subscribe SubscribeLink_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//div[@id='page-container']/ul[2]/li[3]/a"));

            if (element.Text != "Subscribe" && element.Text != "לרכישת מנוי")
                element = WaitUntilElementExists(By.XPath("//div[@id='page-container']/ul[2]/li[2]/a"));

            ClickUntilURLChanges(element);

            return new WebdriverAutomation.Framework.Site_Pages.Subscribe(driver);
        }

        public WebdriverAutomation.Framework.Site_Pages.BuyOneGiveOneFreeSettings BuyOneGiveOneFreeSettingsLink_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//div[@id='page-container']/ul[2]/li[4]/a"));
            ClickUntilURLChanges(element);

            return new WebdriverAutomation.Framework.Site_Pages.BuyOneGiveOneFreeSettings(driver);
        }

        public WebdriverAutomation.Framework.Site_Pages.GetMoreAttentionByAddingPremiumServices PremiumServicesLink_Click()
        {
            IWebElement element = null;

            if (driver.Url.Contains("jdate.com"))
                element = WaitUntilElementExists(By.XPath("//div[@id='page-container']/ul[2]/li[5]/a"));
            else
                element = WaitUntilElementExists(By.XPath("//div[@id='page-container']/ul[2]/li[4]/a"));

            ClickUntilURLChanges(element);

            return new WebdriverAutomation.Framework.Site_Pages.GetMoreAttentionByAddingPremiumServices(driver);
        }

        public WebdriverAutomation.Framework.Site_Pages.PremiumServiceSettings PremiumServiceSettingsLink_Click()
        {
            IWebElement element = null;

            if (driver.Url.Contains("jdate.com"))
                element = WaitUntilElementExists(By.XPath("//div[@id='page-container']/ul[2]/li[6]/a"));
            else
                element = WaitUntilElementExists(By.XPath("//div[@id='page-container']/ul[2]/li[5]/a"));
            ClickUntilURLChanges(element);

            return new WebdriverAutomation.Framework.Site_Pages.PremiumServiceSettings(driver);
        }

        public WebdriverAutomation.Framework.Site_Pages.RemoveMyProfile RemoveMyProfileLink_Click()
        {
            IWebElement element = null;

            string expectedText = string.Empty;

            if (driver.Url.Contains("jdate.co.il"))
                expectedText = "הסרת הפרופיל";
            else
                expectedText = "Remove My Profile";

            element = WaitUntilElementExists(By.XPath("//div[@id='page-container']/ul[2]/li[3]/a"));

            if (element.Text != expectedText)
            {
                element = WaitUntilElementExists(By.XPath("//div[@id='page-container']/ul[2]/li[4]/a"));

                if (element.Text != expectedText)
                    element = WaitUntilElementExists(By.XPath("//div[@id='page-container']/ul[2]/li[5]/a"));

                if (element.Text != expectedText)
                    element = WaitUntilElementExists(By.XPath("//div[@id='page-container']/ul[2]/li[6]/a"));

                if (element.Text != expectedText)
                    element = WaitUntilElementExists(By.XPath("//div[@id='page-container']/ul[2]/li[7]/a"));
            }

            ClickUntilURLChanges(element);

            return new WebdriverAutomation.Framework.Site_Pages.RemoveMyProfile(driver);
        }

        public WebdriverAutomation.Framework.Site_Pages.AccountHistory AccountInformationLink_Click()
        {
            IWebElement element = null;

            element = WaitUntilElementExists(By.XPath("//div[@id='page-container']/ul[2]/li[7]/a"));

            if (element.Text != "Account Information" && element.Text != "חיובים קודמים")
                element = WaitUntilElementExists(By.XPath("//div[@id='page-container']/ul[2]/li[6]/a"));

            if (element.Text != "Account Information" && element.Text != "חיובים קודמים")
                element = WaitUntilElementExists(By.XPath("//div[@id='page-container']/ul[2]/li[8]/a"));

            ClickUntilURLChanges(element);

            return new WebdriverAutomation.Framework.Site_Pages.AccountHistory(driver);
        }

        public WebdriverAutomation.Framework.Site_Pages.BillingInformation BillingInformationLink_Click()
        {
            IWebElement element = null;

            element = WaitUntilElementExists(By.XPath("//div[@id='page-container']/ul[2]/li[9]/a"));

            ClickUntilURLChanges(element);

            return new WebdriverAutomation.Framework.Site_Pages.BillingInformation(driver);
        }

        public WebdriverAutomation.Framework.Site_Pages.VerifyYourEmailAddress VerifyEmailLink_Click()
        {
            IWebElement element = null;

            element = WaitUntilElementExists(By.XPath("//div[@id='page-container']/ul[2]/li[7]/a"));

            if (element.Text != "Verify Email" && element.Text != "אימות כתובת דואר אלקטרוני")
                element = WaitUntilElementExists(By.XPath("//div[@id='page-container']/ul[2]/li[8]/a"));

            if (element.Text != "Verify Email" && element.Text != "אימות כתובת דואר אלקטרוני")
                element = WaitUntilElementExists(By.XPath("//div[@id='page-container']/ul[2]/li[9]/a"));

            if (element.Text != "Verify Email" && element.Text != "אימות כתובת דואר אלקטרוני")
                element = WaitUntilElementExists(By.XPath("//div[@id='page-container']/ul[2]/li[10]/a"));

            ClickUntilURLChanges(element);

            return new WebdriverAutomation.Framework.Site_Pages.VerifyYourEmailAddress(driver);
        }
        #endregion

        #region Tools and Settings
        public WebdriverAutomation.Framework.Site_Pages.Settings ChatAndIMSettingsLink_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//div[@id='page-container']/ul[3]/li[1]/a"));
            ClickUntilURLChanges(element);

            return new WebdriverAutomation.Framework.Site_Pages.Settings(driver);
        }

        public WebdriverAutomation.Framework.Site_Pages.JDateMobile JDateMobileSettingsLink_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//div[@id='page-container']/ul[3]/li[2]/a"));
            ClickUntilURLChanges(element);

            return new WebdriverAutomation.Framework.Site_Pages.JDateMobile(driver);
        }

        public WebdriverAutomation.Framework.Site_Pages.MobileAlertPreferences MobileAlertsLink_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//div[@id='page-container']/ul[3]/li[2]/a"));
            ClickUntilURLChanges(element);

            return new WebdriverAutomation.Framework.Site_Pages.MobileAlertPreferences(driver);
        }

        public WebdriverAutomation.Framework.Site_Pages.MessageSettings MessageSettingsLink_Click()
        {
            IWebElement element = null;

            if (driver.Url.Contains("spark.com") || driver.Url.Contains("jdate.co.il"))
                element = WaitUntilElementExists(By.XPath("//div[@id='page-container']/ul[3]/li[1]/a"));
            else if (driver.Url.Contains("bbwpersonalsplus.com") || driver.Url.Contains("blacksingles.com"))
                element = WaitUntilElementExists(By.XPath("//div[@id='page-container']/ul[3]/li[2]/a"));
            else
                element = WaitUntilElementExists(By.XPath("//div[@id='page-container']/ul[3]/li[3]/a"));

            ClickUntilURLChanges(element);

            return new WebdriverAutomation.Framework.Site_Pages.MessageSettings(driver);
        }

        public WebdriverAutomation.Framework.Site_Pages.MessageSettings OffSiteMessageSettingsLink_Click()
        {
            IWebElement element = null;

            if (driver.Url.Contains("spark.com") || driver.Url.Contains("jdate.co.il"))
                element = WaitUntilElementExists(By.XPath("//div[@id='page-container']/ul[3]/li[2]/a"));
            else if (driver.Url.Contains("jdate.com") || driver.Url.Contains("bbwpersonalsplus.com") || driver.Url.Contains("blacksingles.com"))
                element = WaitUntilElementExists(By.XPath("//div[@id='page-container']/ul[3]/li[3]/a"));
            else
                element = WaitUntilElementExists(By.XPath("//div[@id='page-container']/ul[3]/li[4]/a"));

            ClickUntilURLChanges(element);

            Assert.True(driver.Url.Contains("#OffSite"), "ERROR - OFF-SITE MESSAGE SETTINGS PAGE - We should have jumped to the Off-Site Message from JDate to You section but the link does not.");

            return new WebdriverAutomation.Framework.Site_Pages.MessageSettings(driver);
        }

        public WebdriverAutomation.Framework.Site_Pages.WhosCheckingYouOut MembersWhoEmailedYouLink_Click()
        {
            IWebElement element = null;

            if (driver.Url.Contains("spark.com") || driver.Url.Contains("jdate.co.il"))
                element = WaitUntilElementExists(By.XPath("//div[@id='page-container']/ul[3]/li[3]/a"));
            else if (driver.Url.Contains("jdate.com") || driver.Url.Contains("bbwpersonalsplus.com") || driver.Url.Contains("blacksingles.com"))
                element = WaitUntilElementExists(By.XPath("//div[@id='page-container']/ul[3]/li[4]/a"));
            else
                element = WaitUntilElementExists(By.XPath("//div[@id='page-container']/ul[3]/li[5]/a"));

            ClickUntilURLChanges(element);

            Assert.True(driver.Url.Contains("CategoryID=-4"), "ERROR - WHO'S CHECKING YOU OUT PAGE - We should be on this page with the Emailed You List tab selected but the page verification failed.");

            return new WebdriverAutomation.Framework.Site_Pages.WhosCheckingYouOut(driver);
        }

        public WebdriverAutomation.Framework.Site_Pages.ContactingMembers NeverMissEmailsLink_Click()
        {
            IWebElement element = null;

            if (driver.Url.Contains("spark.com") || driver.Url.Contains("jdate.co.il"))
                element = WaitUntilElementExists(By.XPath("//div[@id='page-container']/ul[3]/li[4]/a"));
            else if (driver.Url.Contains("jdate.com") || driver.Url.Contains("bbwpersonalsplus.com") || driver.Url.Contains("blacksingles.com"))
                element = WaitUntilElementExists(By.XPath("//div[@id='page-container']/ul[3]/li[5]/a"));
            else
                element = WaitUntilElementExists(By.XPath("//div[@id='page-container']/ul[3]/li[6]/a"));

            ClickUntilURLChanges(element);

            return new WebdriverAutomation.Framework.Site_Pages.ContactingMembers(driver);
        }
        #endregion

        #region Blocked Member Lists
        public WebdriverAutomation.Framework.Site_Pages.WhosCheckingYouOut MembersBlockedFromContactingYouLink_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//div[@id='page-container']/ul[4]/li[1]/a"));
            ClickUntilURLChanges(element);

            Assert.True(driver.Url.Contains("CategoryID=-13"), "ERROR - WHO'S CHECKING YOU OUT PAGE - We should be on this page with the Can't Contact You List tab selected but the page verification failed.");

            return new WebdriverAutomation.Framework.Site_Pages.WhosCheckingYouOut(driver);
        }

        public WebdriverAutomation.Framework.Site_Pages.YoureCheckingOut MembersBlockedFromAppearingInYourSearchResultsLink_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//div[@id='page-container']/ul[4]/li[2]/a"));
            ClickUntilURLChanges(element);

            Assert.True(driver.Url.Contains("CategoryID=-34"), "ERROR - YOU'RE CHECKING OUT PAGE with the Blocked From Search List tab selected - We should be on this page but the page verification failed.");

            return new WebdriverAutomation.Framework.Site_Pages.YoureCheckingOut(driver);
        }

        public WebdriverAutomation.Framework.Site_Pages.WhosCheckingYouOut MembersBlockedFromViewingYourProfileInHisHerSearchResults_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//div[@id='page-container']/ul[4]/li[3]/a"));
            ClickUntilURLChanges(element);

            Assert.True(driver.Url.Contains("CategoryID=-35"), "ERROR - WHO'S CHECKING YOU OUT PAGE with the Can't See You List tab selected - We should be on this page but the page verification failed.");

            return new WebdriverAutomation.Framework.Site_Pages.WhosCheckingYouOut(driver);
        }
        #endregion

        #region Help
        public WebdriverAutomation.Framework.Site_Pages.FrequentlyAskedQuestions FAQLink_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//div[@id='page-container']/ul[5]/li[1]/a"));
            ClickUntilURLChanges(element);

            return new WebdriverAutomation.Framework.Site_Pages.FrequentlyAskedQuestions(driver);
        }

        public WebdriverAutomation.Framework.Site_Pages.ContactUs ContactUsLink_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//div[@id='page-container']/ul[5]/li[2]/a"));
            ClickUntilURLChanges(element);

            return new WebdriverAutomation.Framework.Site_Pages.ContactUs(driver);
        }
        #endregion

        #region Legal
        public WebdriverAutomation.Framework.Site_Pages.Privacy PrivacyStatementLink_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//div[@id='page-container']/ul[6]/li[1]/a"));
            ClickUntilURLChanges(element);

            return new WebdriverAutomation.Framework.Site_Pages.Privacy(driver);
        }

        public WebdriverAutomation.Framework.Site_Pages.Privacy TermsAndConditionsOfServiceLink_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//div[@id='page-container']/ul[6]/li[2]/a"));
            ClickUntilURLChanges(element);

            Assert.True(driver.Url.Contains("#service"), "ERROR - PRIVACY STATEMENT PAGE - We should have jumped to the Terms and Conditions of Service section but the link does not.");

            return new WebdriverAutomation.Framework.Site_Pages.Privacy(driver);
        }

        public WebdriverAutomation.Framework.Site_Pages.Privacy TermsAndConditionsOfPurchaseLink_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//div[@id='page-container']/ul[6]/li[3]/a"));
            ClickUntilURLChanges(element);

            Assert.True(driver.Url.Contains("#purchase"), "ERROR - PRIVACY STATEMENT PAGE - We should have jumped to the Terms and Conditions of Purchase section but the link does not.");

            return new WebdriverAutomation.Framework.Site_Pages.Privacy(driver);
        }
        #endregion
    }
}
