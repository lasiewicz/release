﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.Events;
using NUnit.Framework;
using log4net;

// ex. PAGE URL:  http://connect.jdate.com/inbox/message.html?cardSent=102665989&MessageID=996624734&toUserID=118863348
//   this is the page that appears when the user clicks on an ECard in their Messages Page inbox list

namespace WebdriverAutomation.Framework.Site_Pages
{
    public class ECard : BasePage
    {
        public ECard(IWebDriver driver)
            : base(driver)
        {
            Header = new WebdriverAutomation.Framework.Site_Parts.Header(driver);
            Footer = new WebdriverAutomation.Framework.Site_Parts.Footer(driver);

            // verify we are on the correct page       
            IWebElement textOnPage = null;

            textOnPage = WaitUntilPageVerificationObjectExists(By.XPath("//html/body/div/div[2]/div[2]/div/div/div/div/div/div[2]/ul/li[2]/a"), "ECARD PAGE");

            if (textOnPage.Text != (textThatVerifiesThisPageIsCorrect))
                log.Error("ERROR - ECARD PAGE - We should be on this page but the page verification failed.");

            Assert.True(textOnPage.Text == textThatVerifiesThisPageIsCorrect, "ERROR - ECARD PAGE - We should be on this page but the page verification failed.");

        }

        private static readonly ILog log = LogManager.GetLogger(typeof(AboutJDate));
        public WebdriverAutomation.Framework.Site_Parts.Header Header;
        public WebdriverAutomation.Framework.Site_Parts.Footer Footer;

        private const string textThatVerifiesThisPageIsCorrect = "Reply with an E-card";

        public bool IsCardFromCorrectUser(string expectedUser)
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//dl[@id='user_info']/dt/a"));

            if (element.Text == expectedUser)
                return true;

            return false;

        }
    }
}
