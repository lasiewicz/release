﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.Events;
using NUnit.Framework;
using log4net;

// PAGE URL:  http://www.jdate.com/Applications/Logon/Logon.aspx

namespace WebdriverAutomation.Framework.Site_Pages
{
    public class Login : BasePage
    {
        public Login(IWebDriver driver)
            : base(driver)
        {
            Footer = new WebdriverAutomation.Framework.Site_Parts.Footer(driver);

            // verify we are on the correct page       
            IWebElement textOnPage = null;

            if (driver.Url.Contains("jdate.co.il"))
            {
                textThatVerifiesThisPageIsCorrect = "כניסה לחברים";
                textOnPage = WaitUntilPageVerificationObjectExists(By.XPath("//div[@id='page-container']/div[1]/fieldset[@id='loginBox']"), "LOGIN PAGE");
            }
            else
            {
                textThatVerifiesThisPageIsCorrect = "Login now";
                textOnPage = WaitUntilPageVerificationObjectExists(By.XPath("//fieldset[@id='loginBox']/h2/b"), "LOGIN PAGE");
            }

            if (!textOnPage.Text.Contains(textThatVerifiesThisPageIsCorrect))
                log.Error("ERROR - LOGIN PAGE - We should be on this page but the page verification failed.");

            Assert.True(textOnPage.Text.Contains(textThatVerifiesThisPageIsCorrect), "ERROR - LOGIN PAGE - We should be on this page but the page verification failed.");

        }

        private static readonly ILog log = LogManager.GetLogger(typeof(Login));
        public WebdriverAutomation.Framework.Site_Parts.Footer Footer;

        private string textThatVerifiesThisPageIsCorrect = string.Empty;

        internal IWebElement emailTextbox = null;
        internal IWebElement passwordTextbox = null;
        internal IWebElement loginButton = null;

        public void EmailTextbox_Write(string text)
        {
            string thisXPath = "//input[contains(@id, 'LogonControl_EmailAddress')]";

            WaitUntilElementValueCanBeCleared(By.XPath(thisXPath));

            emailTextbox = WaitUntilElementExists(By.XPath(thisXPath));
            SendKeys(emailTextbox, text);
        }

        public void PasswordTextbox_Write(string text)
        {
            passwordTextbox = WaitUntilElementExists(By.XPath("//input[contains(@id, 'LogonControl_Password')]"));
            SendKeys(passwordTextbox, text);
        }

        public string EmailTextbox
        {
            get
            {
                string thisXPath = "//input[contains(@id, 'LogonControl_EmailAddress')]";

                emailTextbox = WaitUntilElementExists(By.XPath(thisXPath));
                return emailTextbox.GetAttribute("value");
            }
            set
            {
                string thisXPath = "//input[contains(@id, 'LogonControl_EmailAddress')]";

                WaitUntilElementValueCanBeCleared(By.XPath(thisXPath));

                emailTextbox = WaitUntilElementExists(By.XPath(thisXPath));
                SendKeys(emailTextbox, value);
            }
        }

        public string PasswordTextbox
        {
            get
            {
                string thisXPath = "//input[contains(@id, 'LogonControl_Password')]";

                passwordTextbox = WaitUntilElementExists(By.XPath(thisXPath));
                return passwordTextbox.GetAttribute("value");
            }
            set
            {
                string thisXPath = "//input[contains(@id, 'LogonControl_Password')]";

                WaitUntilElementValueCanBeCleared(By.XPath(thisXPath));

                passwordTextbox = WaitUntilElementExists(By.XPath(thisXPath));
                SendKeys(passwordTextbox, value);
            }
        }

        public void LoginButton_Click(string username)
        {
            loginButton = WaitUntilElementExists(By.XPath("//input[contains(@id, 'LogonControl_Login')]"));
            loginButton.Click();

            // sometimes this button doesn't click the first time for some reason. if the notification div has not appeared, try again
            if ((driver.FindElements(By.XPath("//input[contains(@id, 'LogonControl_Login')]")).Count > 0) && (driver.FindElements(By.XPath("//div[contains(@id, 'divNotification')]")).Count == 0))
            {
                loginButton = driver.FindElement(By.XPath("//input[contains(@id, 'LogonControl_Login')]"));
                loginButton.Click();
            }

            // if we still see a notification div maybe our account is suspended.  
            if (((driver.FindElements(By.XPath("//input[contains(@id, 'LogonControl_Login')]")).Count > 0) && driver.FindElements(By.XPath("//div[contains(@id, 'divNotification')]")).Count > 0))
            {
                IWebElement accountSuspendedText = WaitUntilElementExists(By.XPath("//div[contains(@id, 'divNotification')]"));

                if (accountSuspendedText.Text.Contains("Your account has been suspended") || accountSuspendedText.Text.Contains("הפרופיל שלך הוקפא אדמיניסטרטיבית, אנא פנה למחלקת שירות החברים Help@"))
                {
                    log.Error("ERROR - We cannot login because the account in this test case has been suspended. Unsuspend the account for '" + username + "' and restart the test case.");
                    Assert.Fail("ERROR - We cannot login because the account in this test case has been suspended. Unsuspend the account for '" + username + "' and restart the test case.");
                }
            }
        }
        
        public Framework.Site_Pages.Registration ClickHereToJoinFreeLink_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//div[@id='page-container']/div[2]/div/a[2]"));
            ClickUntilURLChanges(element);

            return new WebdriverAutomation.Framework.Site_Pages.Registration(driver);
        }

        public void RememberMeCheckbox_Uncheck()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//input[contains(@id, '_LogonControl_CheckBoxAutoLogin')]"));

            if (element.Selected == true)
                element.Click();
        }
    }
}
