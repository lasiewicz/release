﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.Events;
using NUnit.Framework;
using log4net;
using System.Threading;

// PAGE URL:  http://www.jdate.co.il/Applications/MemberProfile/RegistrationWelcomeOnePage.aspx

namespace WebdriverAutomation.Framework.Site_Pages
{
    public class RegistrationJDateCoIlRegOverlay : BasePage
    {
        public RegistrationJDateCoIlRegOverlay(IWebDriver driver)
            : base(driver)
        {
            Tests.ReusableCode.Ads ads = new Tests.ReusableCode.Ads();
            ads.KillAllAds(driver, log);

            // verify we are on the correct page       
            IWebElement textOnPage = null;

            textOnPage = WaitUntilPageVerificationObjectExists(By.XPath("//div[@class='overlay']/a"), "REGISTRATION REG OVERLAY FOR JDATE.CO.IL PAGE");

            if (!textOnPage.Text.Contains(textThatVerifiesThisPageIsCorrect))
                log.Error("ERROR - REGISTRATION REG OVERLAY FOR JDATE.CO.IL PAGE - We should be on this page but the page verification failed.");

            Assert.True(textOnPage.Text.Contains(textThatVerifiesThisPageIsCorrect), "ERROR - REGISTRATION REG OVERLAY FOR JDATE.CO.IL PAGE - We should be on this page but the page verification failed.");

        }

        private static readonly ILog log = LogManager.GetLogger(typeof(AboutJDate));

        private const string textThatVerifiesThisPageIsCorrect = "כניסה לחברים";

        public void ContinueButton_Click()
        {
            Wait(1000);

            WaitUntilElementIsClickable(By.XPath("//button[@id='btnContinue']"));

            Wait(1000);
        }


        public void StartButton_Click()
        {
            Wait(1000);

            WaitUntilElementIsClickable(By.XPath("//button[@id='btnContinue']"));

            Wait(1000);
        }

        public bool StartButton_Exists_ForJDateCoIl()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//button[@id='btnContinue']"));

            if (element.Text == "מתחילים »")
                return true;

            return false;
        }

        ///// <summary>
        ///// Another method to help with the phantom clicks.  This method clicks the Continue button until the Progress Bar changes values.
        ///// NOTE:  Doesn't seem to work with this page because the progress bar path doesn't work like the other reg pages.  Will need to come back to this if phantom clicks appear.
        ///// </summary>
        //public void ContinueButton_ClickUntilProgressBarChanges()
        //{
        //    Wait(1000);

        //    string progressBarXPath = "//div[@id='progressbar']";

        //    IWebElement element = WaitUntilElementExists(By.XPath("//a[@id='nextBtn']"));
        //    ClickUntilObjectTextChanges(element, By.XPath(progressBarXPath));

        //    Wait(1000);
        //}

        public bool DoesRegOverLayOpeningPageExist()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//a[@id='nextBtn']"));

            if (element.Text == "להרשמה חינם ◄")
                return true;

            return false;
        }

        public void IAmAListBox_Select(int index)
        {
            SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[@id='idgendermask']")));
            select.SelectByIndex(index);
        }

        public void YouAreAListBox_Select(int index)
        {
            SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, '_idSeekingGender_AttributeOptions')]")));
            select.SelectByIndex(index);
        }

        public void WhatIsYourMaritalStatusListBox_Select(int index)
        {
            SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, 'idMaritalStatus_AttributeOptions')]")));
            select.SelectByIndex(index);
        }

        public bool DoesMaritalStatusListBoxExist()
        {
            if (driver.FindElements(By.XPath("//select[contains(@id, 'idMaritalStatus_AttributeOptions')]")).Count > 0)
                return true;

            return false;
        }

        public void WhatIsYourReligiousBackgroundDropdown_Select(int index)
        {
            SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[@id='idjdatereligion']")));
            select.SelectByIndex(index);
        }

        public void BirthDateMonthDropdown_Select(int index)
        {
            SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[@id='birthdayMonth']")));
            select.SelectByIndex(index);
        }

        public void BirthDateDayDropdown_Select(int index)
        {
            SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[@id='birthdayDay']")));
            select.SelectByIndex(index);
        }

        public void BirthDateYearDropdown_Select(int index)
        {
            SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[@id='birthdayYear']")));
            select.SelectByIndex(index);
        }

        public void RegionDropdown_Select(int index)
        {
            SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[@id='regionStateId']")));
            select.SelectByIndex(index);
        }

        public void HometownTextbox_Write(string text)
        {
            WaitUntilElementIsWritable(By.XPath("//input[contains(@id, 'regionCitiesByRegionId')]"), text);
        }

        public void HometownListBox_Select(string value)
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//input[contains(@id, 'regionCitiesByRegionId')]"));

            Thread.Sleep(1000);
            element.SendKeys(Keys.Down);

            Thread.Sleep(1000);
            element.SendKeys(Keys.Return);

            //SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//div[@class='regionCitiesByRegionId']")));
            //select.SelectByText(value);
        }

        public void CountryDropdown_Select(string value)
        {
            SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, 'idRegion_country')]")));
            select.SelectByText(value);
        }

        public void UserNameTextbox_Write(string text)
        {
            WaitUntilElementIsWritable(By.XPath("//input[contains(@id, 'idUserName_AttributeTextControl')]"), text);
        }

        public void EmailTextbox_Write(string text)
        {
            WaitUntilElementIsWritable(By.XPath("//input[@id='idemailaddress']"), text);
        }

        public void PasswordTextbox_Write(string text)
        {
            WaitUntilElementIsWritable(By.XPath("//input[@id='idpassword']"), text);
        }

        public void DescribeYourselfAndYourPersonalityTextArea_Write(string text)
        {
            if (driver.ToString() == System.Configuration.ConfigurationManager.AppSettings["IE_driverString"])
                WaitUntilElementValueCanBeCleared(By.XPath("//textarea[contains(@id, 'idAboutMe_AttributeTextControl')]"));

            WaitUntilElementIsWritable(By.XPath("//textarea[contains(@id, 'idAboutMe_AttributeTextControl')]"), text);
        }

        public void YouAreAList_Select(int index)
        {
            SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, 'idGender_AttributeOptions')]")));
            select.SelectByIndex(index);
        }

        public void BirthDateMonthList_Select(int index)
        {
            SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, 'BirthDate_ddlBirthDateMonthUS')]")));
            select.SelectByIndex(index);
        }

        public void BirthDateDayList_Select(int index)
        {
            SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, 'BirthDate_ddlBirthDateDayOnePage')]")));
            select.SelectByIndex(index);
        }

        public void BirthDateYearList_Select(int index)
        {
            SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, 'BirthDate_ddlBirthDateYearUS')]")));
            select.SelectByIndex(index);
        }

        public void YourReligiousBackground_Select(int index)
        {
            SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, 'JDateReligion_AttributeOptions')]")));
            select.SelectByIndex(index);
        }

        public void Country_Select(int index)
        {
            SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, 'idRegion_country')]")));
            select.SelectByIndex(index);
        }

        public void CityTextbox_Write(string text)
        {
            WaitUntilElementIsWritable(By.XPath("//input[contains(@id, 'idRegion_city')]"), text);
        }

        public void CityListBox_Select(string value)
        {
            SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//html/body/div[2]/select")));
            select.SelectByText(value);
        }

        public void EmailAddressTextbox_Write(string text)
        {
            WaitUntilElementIsWritable(By.XPath("//input[contains(@id, 'idEmailAddress_AttributeTextControl')]"), text);
            IWebElement zipCodeTextbox = WaitUntilElementExists(By.XPath("//input[contains(@id, 'idEmailAddress_AttributeTextControl')]"));
        }

        public void PleaseEnterTheCodeShownBelow_Write(string text)
        {
            if (driver.ToString() == System.Configuration.ConfigurationManager.AppSettings["IE_driverString"])
                WaitUntilElementValueCanBeCleared(By.XPath("//input[contains(@id, 'Captcha_AttributeCaptchaControl')]"));

            WaitUntilElementIsWritable(By.XPath("//input[contains(@id, 'idCaptcha_AttributeCaptchaControl')]"), text);
        }

        public void ChooseYourPasswordTextbox_Write(string text)
        {
            WaitUntilElementIsWritable(By.XPath("//input[contains(@id, 'idPassword_AttributeTextControl')]"), text);
        }

        public void IWouldLikeSpecialOffersAndAnnouncements_Click()
        {
            string xPathOfSelectedIndex = "//input[contains(@id, 'idPartnersOffers2_AttributeOptions')]";

            WaitUntilElementIsClickable(By.XPath(xPathOfSelectedIndex));
        }

        public bool DoesUnableToProcessRegistrationErrorAppear()
        {
            if (driver.FindElements(By.XPath("//div[@id='step_0']/div[11]")).Count > 0)
            {
                IWebElement element = WaitUntilElementExists(By.XPath("//div[@id='step_0']/div[11]"));

                if (element.Text.Contains("We were unable to process your registration because of an error"))
                    return true;
            }

            return false;
        }
    }
}
