﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.Events;
using NUnit.Framework;
using log4net;

// PAGE URL:  http://www.jdate.com/Applications/Email/MailBox.aspx?NavPoint=top (while logged in)

namespace WebdriverAutomation.Framework.Site_Pages
{
    public class Messages : BasePage
    {
        public Messages(IWebDriver driver)
            : base(driver)
        {
            Header = new WebdriverAutomation.Framework.Site_Parts.Header(driver);
            Footer = new WebdriverAutomation.Framework.Site_Parts.Footer(driver);

            Tests.ReusableCode.Ads ads = new Tests.ReusableCode.Ads();
            ads.KillAllAds(driver, log);

            // verify we are on the correct page       
            IWebElement textOnPage = null;

            textOnPage = WaitUntilPageVerificationObjectExists(By.XPath("//span[contains(@id, 'txtMailBox')]"), "MESSAGES PAGE");

            if (driver.Url.Contains("jdate.co.il"))
                textThatVerifiesThisPageIsCorrect = "דואר נכנס";
            else
                textThatVerifiesThisPageIsCorrect = "Messages";

            if (textOnPage.Text != (textThatVerifiesThisPageIsCorrect))
                log.Error("ERROR - MESSAGES PAGE - We should be on this page but the page verification failed.");

            Assert.True(textOnPage.Text == textThatVerifiesThisPageIsCorrect, "ERROR - MESSAGES PAGE - We should be on this page but the page verification failed.");

        }

        private static readonly ILog log = LogManager.GetLogger(typeof(Subscribe));
        public WebdriverAutomation.Framework.Site_Parts.Header Header;
        public WebdriverAutomation.Framework.Site_Parts.Footer Footer;

        private string textThatVerifiesThisPageIsCorrect = string.Empty;

        public void UpgradeNowAd_Close()
        {
            IWebElement closeAdLink = null;

            bool adExists = false;
            string errorLogTrace = string.Empty;

            //// check to see if an ad even exists
            //errorLogTrace = "Initial check to see if ad exists  \r\n";
            //if (driver.FindElements(By.XPath("//div[@id='blockUI_adunit']/div/div/p/a/img")).Count > 0)
            //{
            //    errorLogTrace = "An ad exists but we don't know which one yet  \r\n";

            // Since ads don't show up on the Home Page immediately, there seems to be a race condition.  Thus we loop through all available ads twice before giving up.
            for (int i = 0; i <= 2; i++)
            {
                errorLogTrace += "Loop " + (i + 1) + " entered  \r\n";

                closeAdLink = driver.FindElement(By.XPath("//div[@id='blockUI_adunit']/div/div/p/a/img"));
                adExists = true;

                errorLogTrace += "Check for loop" + (i + 1) + ". A div with 'blockUI_adunit' is found an assigned to an IWebElement  \r\n";

                //   check for Upgrade Now! ad and close it if it exists
                if (driver.FindElements(By.XPath("//div[@id='blockUI_adunit']/div/div/p/a/img")).Count > 0)
                {
                    errorLogTrace += "Inside if statement for Ad #1.  FindElements count for this ad was more than 1";

                    try
                    {
                        closeAdLink = driver.FindElement(By.XPath("//div[@id='blockUI_adunit']/div/div/p/a/img"));
                        closeAdLink.Click();

                        errorLogTrace += "Ad #1  found!  \r\n";

                        adExists = false;
                        break;
                    }
                    catch
                    { errorLogTrace += "Ad #1 click failed because of race condition, entered catch  \r\n"; }
                }

                errorLogTrace += "LOOP " + (i + 1) + "  \r\n";
                //}

                // we have looped through all the ads that we know of several times (accounting for the possible race condition) and still can't get rid of the ad
                if (adExists == true)
                    Assert.Fail("An ad exists in the Messages Page that we do not know how to get rid of. We cannot continue this test case until we do so.  \r\nERRORLOG - " + errorLogTrace);

            }

        }

        public void DeleteLink_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//input[contains(@id, 'deleteMessage')]"));
            element.Click();
        }

        public bool DoesEmailExist(string sender, string subject)
        {
            IWebElement currentEmailSender;
            IWebElement currentEmailSubject;
            IList<IWebElement> emails;

            if (driver.Url.Contains("blacksingles.com") || driver.Url.Contains("bbwpersonalsplus.com"))
            {
                emails = driver.FindElements(By.XPath("//tr[contains(@class, 'subscribetoview')]"));

                if (emails.Count == 0)
                    emails = driver.FindElements(By.XPath("//tr[contains(@class, 'unopened')]"));
            }
            else
                emails = driver.FindElements(By.XPath("//tr[contains(@class, 'unopened')]"));

            for (int i = 0; i < emails.Count; i++)
            {
                currentEmailSender = WaitUntilElementExists(By.XPath("//a[contains(@id, 'mailDataGrid__ctl" + (i + 2) + "_itemFromLink')]"));
                currentEmailSubject = WaitUntilElementExists(By.XPath("//a[contains(@id, 'mailDataGrid__ctl" + (i + 2) + "_itemSubjectLink')]"));

                if (currentEmailSender.Text == sender && currentEmailSubject.Text == subject)
                    return true;
            }

            return false;
        }

        /// <summary>
        /// jdate.co.il only so far: Overloaded version for senders who might need to strip their usernames down because their names are too long such as "...jdateco"
        /// </summary>
        public bool DoesEmailExist(string sender, string memberID, string subject)
        {
            IWebElement currentEmailSender;
            IWebElement currentEmailSubject;
            IList<IWebElement> emails;

            if (driver.Url.Contains("blacksingles.com") || driver.Url.Contains("bbwpersonalsplus.com"))
            {
                emails = driver.FindElements(By.XPath("//tr[contains(@class, 'subscribetoview')]"));

                if (emails.Count == 0)
                    emails = driver.FindElements(By.XPath("//tr[contains(@class, 'unopened')]"));
            }
            else
                emails = driver.FindElements(By.XPath("//tr[contains(@class, 'unopened')]"));

            for (int i = 0; i < emails.Count; i++)
            {
                currentEmailSender = WaitUntilElementExists(By.XPath("//a[contains(@id, 'mailDataGrid__ctl" + (i + 2) + "_itemFromLink')]"));
                currentEmailSubject = WaitUntilElementExists(By.XPath("//a[contains(@id, 'mailDataGrid__ctl" + (i + 2) + "_itemSubjectLink')]"));

                if ((currentEmailSender.Text == sender || currentEmailSender.Text == memberID) && currentEmailSubject.Text.Contains(subject))
                    return true;
                
                // sometimes the sender's username is shown with the first 7 characters and the rest of the username stripped, such as "...jdateco"
                string firstSevenDigitsOfUsername = sender.Substring(0, 7);

                if (currentEmailSender.Text.Contains(firstSevenDigitsOfUsername))
                    return true;
            }

            return false;
        }

        /// <summary>
        /// jdate.co.il only so far: Overloaded version for senders who might need to strip their usernames down because their names are too long such as "...jdateco"
        /// </summary>
        public Framework.Site_Pages.Message EmailLink_Click(string sender, string memberID, string subject)
        {
            IWebElement currentEmailSender;
            IWebElement currentEmailSubject;
            IList<IWebElement> emails;

            if (driver.Url.Contains("blacksingles.com"))
            {
                emails = driver.FindElements(By.XPath("//tr[contains(@class, 'subscribetoview')]"));

                if (emails.Count == 0)
                    emails = driver.FindElements(By.XPath("//tr[contains(@class, 'unopened')]"));
            }
            else
                emails = driver.FindElements(By.XPath("//tr[contains(@class, 'unopened')]"));

            for (int i = 0; i < emails.Count; i++)
            {
                currentEmailSender = WaitUntilElementExists(By.XPath("//a[contains(@id, 'mailDataGrid__ctl" + (i + 2) + "_itemFromLink')]"));
                currentEmailSubject = WaitUntilElementExists(By.XPath("//a[contains(@id, 'mailDataGrid__ctl" + (i + 2) + "_itemSubjectLink')]"));

                if ((currentEmailSender.Text == sender || currentEmailSender.Text == memberID) && currentEmailSubject.Text.Contains(subject))
                {
                    ClickUntilURLChanges(currentEmailSubject);
                    return new Message(driver);
                }

                // sometimes the sender's username is shown with the first 7 characters and the rest of the username stripped, such as "...jdateco"
                string firstSevenDigitsOfUsername = sender.Substring(0, 7);

                if (currentEmailSender.Text.Contains(firstSevenDigitsOfUsername))
                {
                    ClickUntilURLChanges(currentEmailSubject);
                    return new Message(driver);
                }
            }

            Assert.Fail("In the MESSAGES PAGE, we tried to click on the email with sender: '" + sender + "' and subject: '" + subject + "' but the email was not found");
            return new Message(driver);
        }

        /// <summary>
        /// Overloaded version for senders who might need to strip their usernames down because their names are too long such as "...jdateco"
        /// </summary>
        public Framework.Site_Pages.Message EmailLink_Click(string sender, string subject)
        {
            IWebElement currentEmailSender;
            IWebElement currentEmailSubject;
            IList<IWebElement> emails;

            if (driver.Url.Contains("blacksingles.com"))
            {
                emails = driver.FindElements(By.XPath("//tr[contains(@class, 'subscribetoview')]"));

                if (emails.Count == 0)
                    emails = driver.FindElements(By.XPath("//tr[contains(@class, 'unopened')]"));
            }
            else
                emails = driver.FindElements(By.XPath("//tr[contains(@class, 'unopened')]"));

            for (int i = 0; i < emails.Count; i++)
            {
                currentEmailSender = WaitUntilElementExists(By.XPath("//a[contains(@id, 'mailDataGrid__ctl" + (i + 2) + "_itemFromLink')]"));
                currentEmailSubject = WaitUntilElementExists(By.XPath("//a[contains(@id, 'mailDataGrid__ctl" + (i + 2) + "_itemSubjectLink')]"));

                if (currentEmailSender.Text == sender && currentEmailSubject.Text == subject)
                {
                    ClickUntilURLChanges(currentEmailSubject);
                    return new Message(driver);
                }
            }

            Assert.Fail("In the MESSAGES PAGE, we tried to click on the email with sender: '" + sender + "' and subject: '" + subject + "' but the email was not found");
            return new Message(driver);
        }

        /// <summary>
        /// As a Registered user, for certain sites clicking on an email takes the user to the Subscribe Page instead of the Message Page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="subject"></param>
        /// <returns></returns>
        public Framework.Site_Pages.Subscribe EmailLink_Click_RegisteredUser(string sender, string subject)
        {
            IWebElement currentEmailSender;
            IWebElement currentEmailSubject;
            IList<IWebElement> emails;

            if (driver.Url.Contains("blacksingles.com") || driver.Url.Contains("bbwpersonalsplus.com"))
                emails = driver.FindElements(By.XPath("//tr[contains(@class, 'subscribetoview')]"));
            else
                emails = driver.FindElements(By.XPath("//tr[contains(@class, 'unopened')]"));

            for (int i = 0; i < emails.Count; i++)
            {
                currentEmailSender = WaitUntilElementExists(By.XPath("//a[contains(@id, 'mailDataGrid__ctl" + (i + 2) + "_itemFromLink')]"));
                currentEmailSubject = WaitUntilElementExists(By.XPath("//a[contains(@id, 'mailDataGrid__ctl" + (i + 2) + "_itemSubjectLink')]"));

                if (currentEmailSender.Text == sender && currentEmailSubject.Text == subject)
                {
                    ClickUntilURLChanges(currentEmailSubject);
                    return new Subscribe(driver);
                }
            }

            Assert.Fail("In the MESSAGES PAGE, we tried to click on the email with sender: '" + sender + "' and subject: '" + subject + "' but the email was not found");
            return new Subscribe(driver);
        }

        /// <summary>
        /// In JDate.co.il, when a registered user clicks on an email a prompt appears trying to get them to Subscribe
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="subject"></param>
        public void EmailLink_Click_RegisteredUser_JDate_co_il(string sender, string subject)
        {
            IWebElement currentEmailSender;
            IWebElement currentEmailSubject;
            IList<IWebElement> emails;

            bool emailFound = false;

            if (driver.Url.Contains("blacksingles.com") || driver.Url.Contains("bbwpersonalsplus.com"))
                emails = driver.FindElements(By.XPath("//tr[contains(@class, 'subscribetoview')]"));
            else
                emails = driver.FindElements(By.XPath("//tr[contains(@class, 'unopened')]"));

            for (int i = 0; i < emails.Count; i++)
            {
                currentEmailSender = WaitUntilElementExists(By.XPath("//a[contains(@id, 'mailDataGrid__ctl" + (i + 2) + "_itemFromLink')]"));
                currentEmailSubject = WaitUntilElementExists(By.XPath("//a[contains(@id, 'mailDataGrid__ctl" + (i + 2) + "_itemSubjectLink')]"));

                if (currentEmailSender.Text == sender && currentEmailSubject.Text == subject)
                {
                    currentEmailSubject.Click();

                    emailFound = true;
                    break;
                }
            }

            if (emailFound == false)
                Assert.Fail("In the MESSAGES PAGE, we tried to click on the email with sender: '" + sender + "' and subject: '" + subject + "' but the email was not found");
        }

        /// <summary>
        /// Standard emails, not All Access emails
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="subject"></param>
        public void EmailCheckbox_Check(string sender, string subject)
        {
            IWebElement currentEmailSender;
            IWebElement currentEmailSubject;
            IWebElement checkbox;
            IList<IWebElement> emails = driver.FindElements(By.XPath("//table[contains(@id, 'mailDataGrid')]/tbody/tr[contains(@class, 'opened')]"));

            for (int i = 0; i < emails.Count; i++)
            {
                currentEmailSender = WaitUntilElementExists(By.XPath("//a[contains(@id, 'mailDataGrid__ctl" + (i + 2) + "_itemFromLink')]"));
                currentEmailSubject = WaitUntilElementExists(By.XPath("//a[contains(@id, 'mailDataGrid__ctl" + (i + 2) + "_itemSubjectLink')]"));

                if (currentEmailSender.Text == sender && currentEmailSubject.Text == subject)
                {
                    checkbox = WaitUntilElementExists(By.XPath("//input[contains(@id, 'mailDataGrid__ctl" + (i + 2) + "_itemCheckBox')]"));

                    if (checkbox.Selected == false)
                    {
                        checkbox.Click();
                        break;
                    }
                }
            }
        }

        /// <summary>
        /// Standard emails, not All Access emails
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="subject"></param>
        public void EmailCheckbox_Uncheck(string sender, string subject)
        {
            IWebElement currentEmailSender;
            IWebElement currentEmailSubject;
            IWebElement checkbox;
            IList<IWebElement> emails = driver.FindElements(By.XPath("////table[contains(@id, 'mailDataGrid')]/tbody/tr[contains(@class, 'opened')]"));

            for (int i = 0; i < emails.Count; i++)
            {
                currentEmailSender = WaitUntilElementExists(By.XPath("//a[contains(@id, 'mailDataGrid__ctl" + (i + 2) + "_itemFromLink')]"));
                currentEmailSubject = WaitUntilElementExists(By.XPath("//a[contains(@id, 'mailDataGrid__ctl" + (i + 2) + "_itemSubjectLink')]"));

                if (currentEmailSender.Text == sender && currentEmailSubject.Text == subject)
                {
                    checkbox = WaitUntilElementExists(By.XPath("//input[contains(@id, 'mailDataGrid__ctl" + (i + 2) + "_itemCheckBox')]"));

                    if (checkbox.Selected == true)
                    {
                        checkbox.Click();
                        break;
                    }
                }
            }
        }

        /// <summary>
        /// Standard emails, not All Access Emails
        /// This selects the checkbox that also checks all individual Standard emails so we can delete them all at once
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="subject"></param>
        public void EmailSelectAllCheckboxes_Check()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//table[contains(@id, 'mailDataGrid')]/tbody/tr[1]/td[1]/input"));

            if (element.Selected == false)
                element.Click();
        }

        /// <summary>
        /// All Access emails, not Standard emails
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="subject"></param>
        public void AllAccessEmailCheckbox_Check(string sender, string subject)
        {
            IWebElement currentEmailSender;
            IWebElement currentEmailSubject;
            IWebElement checkbox;
            IList<IWebElement> emails = driver.FindElements(By.XPath("//table[contains(@id, 'dgVIPMail')]/tbody/tr[contains(@class, 'opened')]"));

            for (int i = 0; i < emails.Count; i++)
            {
                currentEmailSender = WaitUntilElementExists(By.XPath("//a[contains(@id, 'dgVIPMail__ctl" + (i + 2) + "_itemFromLink')]"));
                currentEmailSubject = WaitUntilElementExists(By.XPath("//a[contains(@id, 'dgVIPMail__ctl" + (i + 2) + "_itemSubjectLink')]"));

                if (currentEmailSender.Text == sender && currentEmailSubject.Text == subject)
                {
                    checkbox = WaitUntilElementExists(By.XPath("//input[contains(@id, 'dgVIPMail__ctl" + (i + 2) + "_itemCheckBox')]"));

                    if (checkbox.Selected == false)
                    {
                        checkbox.Click();
                        break;
                    }
                }
            }
        }

        /// <summary>
        /// All Access emails, not Standard emails
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="subject"></param>
        public void AllAccessEmailCheckbox_Uncheck(string sender, string subject)
        {
            IWebElement currentEmailSender;
            IWebElement currentEmailSubject;
            IWebElement checkbox;
            IList<IWebElement> emails = driver.FindElements(By.XPath("//table[contains(@id, 'dgVIPMail')]/tbody/tr[contains(@class, 'opened')]"));

            for (int i = 0; i < emails.Count; i++)
            {
                currentEmailSender = WaitUntilElementExists(By.XPath("//a[contains(@id, 'dgVIPMail__ctl" + (i + 2) + "_itemFromLink')]"));
                currentEmailSubject = WaitUntilElementExists(By.XPath("//a[contains(@id, 'dgVIPMail__ctl" + (i + 2) + "_itemSubjectLink')]"));

                if (currentEmailSender.Text == sender && currentEmailSubject.Text == subject)
                {
                    checkbox = WaitUntilElementExists(By.XPath("//input[contains(@id, 'dgVIPMail__ctl" + (i + 2) + "_itemCheckBox')]"));

                    if (checkbox.Selected == true)
                    {
                        checkbox.Click();
                        break;
                    }
                }
            }
        }

        /// <summary>
        /// All Access emails, not Standard emails
        /// This selects the checkbox that also checks all individual All Access emails so we can delete them all at once
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="subject"></param>
        public void AllAccessEmailSelectAllCheckboxes_Check()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//table[contains(@id, 'dgVIPMailFooter')]/tbody/tr/td[1]/input"));

            if (element.Selected == false)
                element.Click();
        }

        /// <summary>
        /// Looks for the email with the matching sending and subject.  Returns true if a green opened envelope icon is next to the message (the message has been read)
        /// or false if a closed envelope icon is next to the message (the message has not been read)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="subject"></param>
        /// <returns></returns>
        public bool IsEnvelopeIconOpened(string sender, string subject)
        {
            IWebElement currentEmailSender;
            IWebElement currentEmailSubject;
            //IWebElement envelopeIcon;
            IList<IWebElement> emails = driver.FindElements(By.XPath("//tr[contains(@class, 'opened')]"));

            for (int i = 0; i < emails.Count; i++)
            {
                currentEmailSender = WaitUntilElementExists(By.XPath("//a[contains(@id, 'mailDataGrid__ctl" + (i + 2) + "_itemFromLink')]"));
                currentEmailSubject = WaitUntilElementExists(By.XPath("//a[contains(@id, 'mailDataGrid__ctl" + (i + 2) + "_itemSubjectLink')]"));

                if (currentEmailSender.Text == sender && currentEmailSubject.Text == subject)
                {
                    // JDate, Spark and Black Singles - opened envelope icon
                    if (driver.FindElements(By.XPath("//a[contains(@id, 'mailDataGrid__ctl" + (i + 2) + "_itemImageOpenedLink')]")).Count > 0)
                        return true;
                    // Spark only - opened envelope icon with red reply arrow on it which appears if a user replied back to email
                    else if (driver.FindElements(By.XPath("//a[contains(@id, 'mailDataGrid__ctl" + (i + 2) + "_itemImageRepliedLink')]")).Count > 0)
                        return true;

                    // JDate, Spark and Black Singles - unopened envelope icon
                    if (driver.FindElements(By.XPath("//a[contains(@id, 'mailDataGrid__ctl" + (i + 2) + "_itemImageUnopenedLink')]")).Count > 0)
                        return false;
                    // Spark only - opened envelope icon with red reply arrow on it which appears if a user replied back to email
                    else if (driver.FindElements(By.XPath("//img[contains(@id, 'mailDataGrid__ctl" + (i + 2) + "_Icon_Email_Unopened')]")).Count > 0)
                        return false;
                }
            }

            Assert.Fail("In the MESSAGES PAGE, we tried to find the email with sender: '" + sender + "' and subject: '" + subject + "' but the email was not found");
            return false;
        }

        /// <summary>
        /// jdate.co.il only so far: Overloaded version for senders who might need to strip their usernames down because their names are too long such as "...jdateco"
        /// </summary>
        public bool IsEnvelopeIconOpened(string sender, string memberID, string subject)
        {
            IWebElement currentEmailSender;
            IWebElement currentEmailSubject;
            //IWebElement envelopeIcon;
            IList<IWebElement> emails = driver.FindElements(By.XPath("//tr[contains(@class, 'opened')]"));

            for (int i = 0; i < emails.Count; i++)
            {
                currentEmailSender = WaitUntilElementExists(By.XPath("//a[contains(@id, 'mailDataGrid__ctl" + (i + 2) + "_itemFromLink')]"));
                currentEmailSubject = WaitUntilElementExists(By.XPath("//a[contains(@id, 'mailDataGrid__ctl" + (i + 2) + "_itemSubjectLink')]"));

                if ((currentEmailSender.Text == sender || currentEmailSender.Text == memberID) && currentEmailSubject.Text.Contains(subject))
                {
                    // JDate, Spark and Black Singles - opened envelope icon
                    if (driver.FindElements(By.XPath("//a[contains(@id, 'mailDataGrid__ctl" + (i + 2) + "_itemImageOpenedLink')]")).Count > 0)
                        return true;
                    // Spark only - opened envelope icon with red reply arrow on it which appears if a user replied back to email
                    else if (driver.FindElements(By.XPath("//a[contains(@id, 'mailDataGrid__ctl" + (i + 2) + "_itemImageRepliedLink')]")).Count > 0)
                        return true;

                    // JDate, Spark and Black Singles - unopened envelope icon
                    if (driver.FindElements(By.XPath("//a[contains(@id, 'mailDataGrid__ctl" + (i + 2) + "_itemImageUnopenedLink')]")).Count > 0)
                        return false;
                    // Spark only - opened envelope icon with red reply arrow on it which appears if a user replied back to email
                    else if (driver.FindElements(By.XPath("//img[contains(@id, 'mailDataGrid__ctl" + (i + 2) + "_Icon_Email_Unopened')]")).Count > 0)
                        return false;
                }

                // sometimes the sender's username is shown with the first 7 characters and the rest of the username stripped, such as "...jdateco"
                string firstSevenDigitsOfUsername = sender.Substring(0, 7);

                if (currentEmailSender.Text.Contains(firstSevenDigitsOfUsername))
                {
                    // JDate, Spark and Black Singles - opened envelope icon
                    if (driver.FindElements(By.XPath("//a[contains(@id, 'mailDataGrid__ctl" + (i + 2) + "_itemImageOpenedLink')]")).Count > 0)
                        return true;
                    // Spark only - opened envelope icon with red reply arrow on it which appears if a user replied back to email
                    else if (driver.FindElements(By.XPath("//a[contains(@id, 'mailDataGrid__ctl" + (i + 2) + "_itemImageRepliedLink')]")).Count > 0)
                        return true;

                    // JDate, Spark and Black Singles - unopened envelope icon
                    if (driver.FindElements(By.XPath("//a[contains(@id, 'mailDataGrid__ctl" + (i + 2) + "_itemImageUnopenedLink')]")).Count > 0)
                        return false;
                    // Spark only - opened envelope icon with red reply arrow on it which appears if a user replied back to email
                    else if (driver.FindElements(By.XPath("//img[contains(@id, 'mailDataGrid__ctl" + (i + 2) + "_Icon_Email_Unopened')]")).Count > 0)
                        return false;
                }
            }

            Assert.Fail("In the MESSAGES PAGE, we tried to find the email with sender: '" + sender + "' and subject: '" + subject + "' but the email was not found");
            return false;
        }

        #region All Access
        public bool DoesYouRecievedAnAllAccessEMailOverlayExist()
        {
            try
            {
                IWebElement element = WaitUntilElementExists(By.XPath("//div[@id='vip-inbox-overlay']/div[2]"));
                return true;
            }
            catch { }

            return false;
        }

        public Framework.Site_Pages.Message ReadNowButton_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//a[contains(@id, 'vipOverlay_lnkReadMessage')]"));

            // using ClickUntilURLChanges() fails sometimes due to timing issue because the Message/Email page doesn't immediately appear.  using this unless i see phantom clicks start occurring.
            element.Click();

            return new Message(driver);
        }
        #endregion

        /// <summary>
        /// JDate.co.il only
        /// This prompt appears when a registered user tries to open an email
        /// </summary>
        public bool DoesSubscribePromptAppear()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//div[@id='inbox-shutdown-inbox-overlay']"));

            if (element.Displayed)
                return true;

            return false;
        }

        /// <summary>
        /// JDate.co.il only
        /// This prompt appears when a registered user tries to open an email
        /// </summary>
        public void SubscribePromptClose()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//img[contains(@id, '_imgClose')]"));

            element.Click();
        }

        public Framework.Site_Pages.ECard ECardLink_Click(string sender, string subject)
        {
            IWebElement currentEmailSender;
            IWebElement currentEmailSubject;
            IList<IWebElement> emails;

            if (driver.Url.Contains("blacksingles.com"))
            {
                emails = driver.FindElements(By.XPath("//tr[contains(@class, 'subscribetoview')]"));

                if (emails.Count == 0)
                    emails = driver.FindElements(By.XPath("//tr[contains(@class, 'unopened')]"));
            }
            else
                emails = driver.FindElements(By.XPath("//tr[contains(@class, 'unopened')]"));

            for (int i = 0; i < emails.Count; i++)
            {
                currentEmailSender = WaitUntilElementExists(By.XPath("//a[contains(@id, 'mailDataGrid__ctl" + (i + 2) + "_itemFromLink')]"));
                currentEmailSubject = WaitUntilElementExists(By.XPath("//a[contains(@id, 'mailDataGrid__ctl" + (i + 2) + "_itemSubjectLink')]"));

                if (currentEmailSender.Text == sender && currentEmailSubject.Text == subject)
                {
                    ClickUntilURLChanges(currentEmailSubject);
                    return new ECard(driver);
                }
            }

            Assert.Fail("In the MESSAGES PAGE, we tried to click on the ECard with sender: '" + sender + "' and subject: '" + subject + "' but the ECard was not found");
            return new ECard(driver);
        }
    }
}
