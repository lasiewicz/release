﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.Events;
using NUnit.Framework;
using log4net;

// PAGE URL:  http://www.jdate.com/Applications/MemberServices/ChemistrySettings.aspx

namespace WebdriverAutomation.Framework.Site_Pages
{
    public class AutoRenewalSettings : BasePage
    {
        public AutoRenewalSettings(IWebDriver driver)
            : base(driver)
        {
            Header = new WebdriverAutomation.Framework.Site_Parts.Header(driver);
            Footer = new WebdriverAutomation.Framework.Site_Parts.Footer(driver);

            // verify we are on the correct page       
            IWebElement textOnPage = null;

            if (driver.Url.Contains("jdate.co.il"))
            {
                textThatVerifiesThisPageIsCorrect = "שירות החידוש האוטומטי";
                textOnPage = WaitUntilPageVerificationObjectExists(By.XPath("//div[@class='auto-renewal-confirm']/p[2]"), "AUTO-RENEWAL SETTINGS PAGE");
                Assert.True(textOnPage.Text.Contains(textThatVerifiesThisPageIsCorrect), "ERROR - AUTO-RENEWAL SETTINGS PAGE - We should be on this page but the page verification failed.");
            }
            else
            {
                textThatVerifiesThisPageIsCorrect = "Auto-renewal Settings";
                textOnPage = WaitUntilPageVerificationObjectExists(By.XPath("//div[@id='page-container']/h1"), "AUTO-RENEWAL SETTINGS PAGE");
                Assert.True(textOnPage.Text == textThatVerifiesThisPageIsCorrect, "ERROR - AUTO-RENEWAL SETTINGS PAGE - We should be on this page but the page verification failed.");
            }

        }

        private static readonly ILog log = LogManager.GetLogger(typeof(AutoRenewalSettings));
        public WebdriverAutomation.Framework.Site_Parts.Header Header;
        public WebdriverAutomation.Framework.Site_Parts.Footer Footer;

        private string textThatVerifiesThisPageIsCorrect = "Auto-renewal Settings";

    }
}
