﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.Events;
using NUnit.Framework;
using log4net;

// PAGE URL:  https://secure.spark.net/jdatecom
//   this page appears when you click on the Yes link whenever you message another user and the prompt appears, "Want to guarantee -user- reads this email?"

namespace WebdriverAutomation.Framework.Site_Pages
{    
    public class GuaranteeMoreConnectionsWithAllAccess : BasePage
    {
        public GuaranteeMoreConnectionsWithAllAccess(IWebDriver driver)
            : base(driver)
        {
            Header = new WebdriverAutomation.Framework.Site_Parts.Header(driver);
            Footer = new WebdriverAutomation.Framework.Site_Parts.Footer(driver);

            // verify we are on the correct page       
            IWebElement textOnPage = null;

            textOnPage = WaitUntilPageVerificationObjectExists(By.XPath("//div[@id='content-main']/div[1]/div/h1"), "GUARANTEE MORE CONNECTIONS WITH ALL ACCESS PAGE");

            if (textOnPage.Text != (textThatVerifiesThisPageIsCorrect))
                log.Error("ERROR - GUARANTEE MORE CONNECTIONS WITH ALL ACCESS PAGE - We should be on this page but the page verification failed.");

            Assert.True(textOnPage.Text == textThatVerifiesThisPageIsCorrect, "ERROR - GUARANTEE MORE CONNECTIONS WITH ALL ACCESS PAGE - We should be on this page but the page verification failed.");

        }

        private static readonly ILog log = LogManager.GetLogger(typeof(Subscribe));
        public WebdriverAutomation.Framework.Site_Parts.Header Header;
        public WebdriverAutomation.Framework.Site_Parts.Footer Footer;

        private const string textThatVerifiesThisPageIsCorrect = "Compose Message";

        public string SubjectTextbox
        {
            get
            {
                IWebElement element = WaitUntilElementExists(By.XPath("//input[contains(@id, 'subjectTextField')]"));
                return element.GetAttribute("value");
            }
            set
            {
                string theXpath = "//input[contains(@id, 'subjectTextField')]";

                WaitUntilElementValueCanBeCleared(By.XPath(theXpath));
                SendKeys(WaitUntilElementExists(By.XPath(theXpath)), value);
            }
        }

        public string MessageTextarea
        {
            get
            {
                IWebElement element = WaitUntilElementExists(By.XPath("//textarea[contains(@id, 'messageBodyTextField')]"));
                return element.GetAttribute("value");
            }
            set
            {
                string theXpath = "//textarea[contains(@id, 'messageBodyTextField')]";

                WaitUntilElementValueCanBeCleared(By.XPath(theXpath));
                SendKeys(WaitUntilElementExists(By.XPath(theXpath)), value);
            }
        }

        public void SendAsAllAccessEmailCheckbox_Check()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//input[contains(@id, 'cbxSendAsVIP')]"));
            element.Click();
        }

        public void SendAsAllAccessEmailCheckbox_Uncheck()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//input[contains(@id, 'cbxSendAsVIP')]"));
            element.Click();
        }

        public Framework.Site_Pages.YourProfile_old SaveAsADraftButton_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//input[contains(@id, 'saveDraftButton')]"));
            element.Click();

            return new YourProfile_old(driver);
        }                

        public Framework.Site_Pages.YourProfile_old SendButton_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//input[contains(@id, 'sendVIPMessageButton')]"));
            element.Click();

            return new YourProfile_old(driver);
        }

        #region Want to guarantee -user- reads this email? popup
        public void YesLink_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//input[contains(@id, 'btnYes')]"));
            element.Click();
        }

        public void NoThanksLink_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//input[contains(@id, 'btnNo')]"));
            element.Click();
        }
        #endregion

    }
}
