﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.Events;
using NUnit.Framework;
using log4net;

// PAGE URL:  http://www.jdate.com/Applications/Email/Compose.aspx?MemberId=118863348&ReturnToMember=True&ActionCallPage=View+Profile&Action=Compose&ActionCallPageDetail=Basic%20Tab%201

namespace WebdriverAutomation.Framework.Site_Pages
{
    public class ComposeMessage : BasePage
    {
        public ComposeMessage(IWebDriver driver)
            : base(driver)
        {
            Header = new WebdriverAutomation.Framework.Site_Parts.Header(driver);
            Footer = new WebdriverAutomation.Framework.Site_Parts.Footer(driver);

            // verify we are on the correct page       
            IWebElement textOnPage = null;

            textOnPage = WaitUntilPageVerificationObjectExists(By.XPath("//div[@id='content-main']/div[1]/h1"), "COMPOSE MESSAGE PAGE");

            if (driver.Url.Contains("jdate.co.il"))
            {
                if (textOnPage.Text != textThatVerifiesThisPageIsCorrect_jdatecoil)
                    log.Error("ERROR - COMPOSE MESSAGE PAGE - We should be on this page but the page verification failed.");

                Assert.True(textOnPage.Text == textThatVerifiesThisPageIsCorrect_jdatecoil, "ERROR - COMPOSE MESSAGE PAGE - We should be on this page but the page verification failed.");

            }
            else
            {
                if (textOnPage.Text != textThatVerifiesThisPageIsCorrect1 && textOnPage.Text != textThatVerifiesThisPageIsCorrect2)
                    log.Error("ERROR - COMPOSE MESSAGE PAGE - We should be on this page but the page verification failed.");

                Assert.True(textOnPage.Text == textThatVerifiesThisPageIsCorrect1 || textOnPage.Text == textThatVerifiesThisPageIsCorrect2, "ERROR - COMPOSE MESSAGE PAGE - We should be on this page but the page verification failed.");
            }

        }

        private static readonly ILog log = LogManager.GetLogger(typeof(Subscribe));
        public WebdriverAutomation.Framework.Site_Parts.Header Header;
        public WebdriverAutomation.Framework.Site_Parts.Footer Footer;

        private const string textThatVerifiesThisPageIsCorrect1 = "Email Me";
        private const string textThatVerifiesThisPageIsCorrect2 = "Compose Message";
        private const string textThatVerifiesThisPageIsCorrect_jdatecoil = "כתבו לי";

        public string SubjectTextbox
        {
            get
            {
                IWebElement element = WaitUntilElementExists(By.XPath("//input[contains(@id, 'subjectTextField')]"));
                return element.GetAttribute("value");
            }
            set
            {
                string theXpath = "//input[contains(@id, 'subjectTextField')]";

                WaitUntilElementValueCanBeCleared(By.XPath(theXpath));
                SendKeys(WaitUntilElementExists(By.XPath(theXpath)), value);
            }
        }

        public string MessageTextarea
        {
            get
            {
                IWebElement element = WaitUntilElementExists(By.XPath("//textarea[contains(@id, 'messageBodyTextField')]"));
                return element.GetAttribute("value");
            }
            set
            {
                string theXpath = "//textarea[contains(@id, 'messageBodyTextField')]";

                WaitUntilElementValueCanBeCleared(By.XPath(theXpath));
                SendKeys(WaitUntilElementExists(By.XPath(theXpath)), value);
            }
        }

        public void SendAsAllAccessEmailCheckbox_Check()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//input[contains(@id, 'cbxSendAsVIP')]"));
            element.Click();
        }

        public void SendAsAllAccessEmailCheckbox_Uncheck()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//input[contains(@id, 'cbxSendAsVIP')]"));
            element.Click();
        }

        public Framework.Site_Pages.YourProfile_old SaveAsADraftButton_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//input[contains(@id, 'saveDraftButton')]"));
            element.Click();

            return new YourProfile_old(driver);
        }

        public Framework.Site_Pages.MessageSent SendButton_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//input[contains(@id, 'sendMessageButton')]"));
            ClickUntilURLChanges(element);

            return new Framework.Site_Pages.MessageSent(driver);
        }

        /// <summary>
        /// For Spark users who are not subscribed, attempting to send an email to another person takes the user to the Subscribe Page
        /// </summary>
        /// <returns></returns>
        public Framework.Site_Pages.Subscribe SendButton_NonSubscribedSparkUser_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//input[contains(@id, 'sendMessageButton')]"));
            element.Click();

            return new Subscribe(driver);
        }

        public void SendButton_Click_AllAccessUnchecked()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//input[contains(@id, 'sendVIPMessageButton')]"));
            element.Click();
        }

        public Framework.Site_Pages.MessageSent SendButton_Click_AllAccessChecked()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//input[contains(@id, 'sendVIPMessageButton')]"));
            element.Click();

            return new MessageSent(driver);
        }

        /// <summary>
        /// This is the Send button that appears when replying back to an email
        /// </summary>
        /// <returns></returns>
        public Framework.Site_Pages.MessageSent SendButton_Click_ReplyingToEmail()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//input[contains(@id, 'sendMessageButton')]"));
            element.Click();

            return new MessageSent(driver);
        }

        #region Want to guarantee -user- reads this email? popup
        public Framework.Site_Pages.GuaranteeMoreConnectionsWithAllAccess YesLink_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//input[contains(@id, 'btnYes')]"));
            element.Click();

            return new GuaranteeMoreConnectionsWithAllAccess(driver);
        }

        public Framework.Site_Pages.MessageSent NoThanksLink_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//input[contains(@id, 'btnNo')]"));
            element.Click();

            // this pop up sometimes does not want to go away.  try a second time to get rid of it 
            try
            {
                element = WaitUntilElementExists(By.XPath("//input[contains(@id, 'btnNo')]"));
                element.Click();
            }
            catch { }

            return new MessageSent(driver);
        }
        #endregion

    }
}
