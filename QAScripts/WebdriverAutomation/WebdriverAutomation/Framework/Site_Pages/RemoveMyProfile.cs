﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.Events;
using NUnit.Framework;
using log4net;

// PAGE URL:  http://www.jdate.com/Applications/MemberServices/Suspend.aspx (while logged in)

namespace WebdriverAutomation.Framework.Site_Pages
{
    public class RemoveMyProfile : BasePage
    {
        public RemoveMyProfile(IWebDriver driver)
            : base(driver)
        {
            Header = new WebdriverAutomation.Framework.Site_Parts.Header(driver); 
            Footer = new WebdriverAutomation.Framework.Site_Parts.Footer(driver);

            // verify we are on the correct page       
            IWebElement textOnPage = null;

            textOnPage = WaitUntilPageVerificationObjectExists(By.XPath("//div[@id='page-container']/h1"), "REMOVE MY PROFILE PAGE");

            if (driver.Url.Contains("jdate.co.il"))
                textThatVerifiesThisPageIsCorrect = "הסרת הפרופיל";
            else
                textThatVerifiesThisPageIsCorrect = "Remove My Profile";

            if (textOnPage.Text != textThatVerifiesThisPageIsCorrect)
                log.Error("ERROR - REMOVE MY PROFILE PAGE - We should be on this page but the page verification failed.");

            Assert.True(textOnPage.Text == textThatVerifiesThisPageIsCorrect, "ERROR - REMOVE MY PROFILE PAGE - We should be on this page but the page verification failed.");

        }

        private static readonly ILog log = LogManager.GetLogger(typeof(YourAccount));
        public WebdriverAutomation.Framework.Site_Parts.Header Header; 
        public WebdriverAutomation.Framework.Site_Parts.Footer Footer;

        private string textThatVerifiesThisPageIsCorrect = "Remove My Profile";

        public void PleaseTellUsWhyYouAreRemovingYourProfileRadioButton_Select(int index)
        {
            IWebElement element;

            switch (index)
            {
                case 0:
                    element = WaitUntilElementExists(By.XPath("//input[contains(@id, 'lstReasons_0')]"));
                    element.Click();
                    break;
                case 1:
                    element = WaitUntilElementExists(By.XPath("//input[contains(@id, 'lstReasons_1')]"));
                    element.Click();
                    break;
                case 2:
                    element = WaitUntilElementExists(By.XPath("//input[contains(@id, 'lstReasons_2')]"));
                    element.Click();
                    break;
                case 3:
                    element = WaitUntilElementExists(By.XPath("//input[contains(@id, 'lstReasons_3')]"));
                    element.Click();
                    break;
                case 4:
                    element = WaitUntilElementExists(By.XPath("//input[contains(@id, 'lstReasons_4')]"));
                    element.Click();
                    break;
                case 5:
                    element = WaitUntilElementExists(By.XPath("//input[contains(@id, 'lstReasons_5')]"));
                    element.Click();
                    break;
                case 6:
                    element = WaitUntilElementExists(By.XPath("//input[contains(@id, 'lstReasons_6')]"));
                    element.Click();
                    break;
                case 7:
                    element = WaitUntilElementExists(By.XPath("//input[contains(@id, 'lstReasons_7')]"));
                    element.Click();
                    break;
                default:
                    break;
            }
        }

        public void RemoveButton_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//div[@id='page-container']/div[2]/input[contains(@id, 'btnSuspend')]"));
            element.Click();

        }
    }
}
