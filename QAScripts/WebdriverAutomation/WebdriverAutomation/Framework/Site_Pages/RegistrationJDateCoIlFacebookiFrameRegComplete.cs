﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.Events;
using NUnit.Framework;
using log4net;
using System.Threading;

// PAGE URL:  http://www.jdate.co.il/Confirmation/?Scenario=FB%20REG%20IFRAME&PRM=47896&lgid=[test]&SPWSCYPHER=KccYNwt7X7Kh/2VW9quuB8krzxEDtExbKvaQd6FrAdxrAEx0OswLcBXHzStMf2FO8iqLLPv0HPD0tDhN5ocVAvmIipj/xIxjVozDZclkXFw=

namespace WebdriverAutomation.Framework.Site_Pages
{
    public class RegistrationJDateCoIlFacebookiFrameRegComplete : BasePage
    {
        public RegistrationJDateCoIlFacebookiFrameRegComplete(IWebDriver driver)
            : base(driver)
        {
            // verify we are on the correct page       
            if (driver.FindElements(By.XPath("//section[@class='editorial']")).Count == 0)
                log.Error("ERROR - REGISTRATION FACEBOOK IFRAME REGISTRATION COMPLETE FOR JDATE.CO.IL PAGE - We should be on this page but the page verification failed.");

            Assert.True(driver.FindElements(By.XPath("//section[@class='editorial']")).Count > 0, "ERROR - REGISTRATION FACEBOOK IFRAME REGISTRATION COMPLETE FOR JDATE.CO.IL PAGE - We should be on this page but the page verification failed.");

        }

        private static readonly ILog log = LogManager.GetLogger(typeof(RegistrationJDateCoIlFacebookiFrameRegComplete));

        public void ViewProfileButton_Click()
        {
            WaitUntilElementIsClickable(By.XPath("//a[@class='complete']"));

            // clicking the above button opens a new browser page. switch to the new page and kill the current one.
            List<string> handlers = new List<string>();
            foreach (string handler in driver.WindowHandles)
                handlers.Add(handler);

            driver.Close();

            driver.SwitchTo().Window(handlers[1]);
        }

    }
}
