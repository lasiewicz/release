﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.Events;
using NUnit.Framework;
using log4net;

// PAGE URL:  http://www.jdate.com/Applications/MemberServices/DisplaySettings.aspx (while logged in)

namespace WebdriverAutomation.Framework.Site_Pages
{
    public class ProfileDisplaySettings : BasePage
    {
        public ProfileDisplaySettings(IWebDriver driver)
            : base(driver)
        {
            Header = new WebdriverAutomation.Framework.Site_Parts.Header(driver);
            Footer = new WebdriverAutomation.Framework.Site_Parts.Footer(driver);

            //JDate sure likes putting ads everywhere
            if (driver.Url.Contains("jdate.com"))
            {
                Tests.ReusableCode.Ads ads = new Tests.ReusableCode.Ads();
                ads.KillAllAds(driver, log);
            }

            // verify we are on the correct page     
            IWebElement textOnPage = null;

            if (driver.Url.Contains("jdate.co.il"))
                textThatVerifiesThisPageIsCorrect = "העדפות הצגת הפרופיל";
            else
                textThatVerifiesThisPageIsCorrect = "Profile Display Settings";

            textOnPage = WaitUntilPageVerificationObjectExists(By.XPath("//div[@id='page-container']/h1"), "PROFILE DISPLAY SETTINGS PAGE");

            if (textOnPage.Text != textThatVerifiesThisPageIsCorrect)
                log.Error("ERROR - PROFILE DISPLAY SETTINGS PAGE - We should be on this page but the page verification failed.");

            Assert.True(textOnPage.Text == textThatVerifiesThisPageIsCorrect, "ERROR - PROFILE DISPLAY SETTINGS PAGE - We should be on this page but the page verification failed.");

        }

        private static readonly ILog log = LogManager.GetLogger(typeof(YourAccount));
        public WebdriverAutomation.Framework.Site_Parts.Header Header; 
        public WebdriverAutomation.Framework.Site_Parts.Footer Footer;

        private string textThatVerifiesThisPageIsCorrect = string.Empty;

        internal IWebElement showHideWhenYoureOnlineRadioButton = null;
        internal IWebElement showHideYourProfileInSearchesRadioButton = null;
        internal IWebElement yourPhotosRadioButton = null;
        internal IWebElement showHideWhenYouViewOrHotListMembersRadioButton = null;
        internal IWebElement saveSettingsButton = null;

        public void ShowHideWhenYoureOnlineRadioButton_Select(int index)
        {
            if (index == 0)
            {
                showHideWhenYoureOnlineRadioButton = WaitUntilElementExists(By.XPath("//input[contains(@id, 'rblMembersOnline_0')]"));
                showHideWhenYoureOnlineRadioButton.Click();
            }
            else
            {
                showHideWhenYoureOnlineRadioButton = WaitUntilElementExists(By.XPath("//input[contains(@id, 'rblMembersOnline_1')]"));
                showHideWhenYoureOnlineRadioButton.Click();
            }
        }

        public void ShowHideYourProfileInSearchesRadioButton_Select(int index)
        {
            if (index == 0)
            {
                showHideYourProfileInSearchesRadioButton = WaitUntilElementExists(By.XPath("//input[contains(@id, 'rblSearches_0')]"));
                showHideYourProfileInSearchesRadioButton.Click();
            }
            else
            {
                showHideYourProfileInSearchesRadioButton = WaitUntilElementExists(By.XPath("//input[contains(@id, 'rblSearches_1')]"));
                showHideYourProfileInSearchesRadioButton.Click();
            }
        }

        public void YourPhotosRadioButton_Select(int index)
        {
            if (index == 0)
            {
                yourPhotosRadioButton = WaitUntilElementExists(By.XPath("//input[contains(@id 'AllUsers')]"));
                yourPhotosRadioButton.Click();
            }
            else
            {
                yourPhotosRadioButton = WaitUntilElementExists(By.XPath("//input[contains(@id, 'MembersOnly')]"));
                yourPhotosRadioButton.Click();
            }
        }

        public void ShowHideWhenYouViewOrHotListMembersRadioButton_Select(int index)
        {
            if (index == 0)
            {
                showHideWhenYouViewOrHotListMembersRadioButton = WaitUntilElementExists(By.XPath("//input[contains(@id, 'rblHotList_0')]"));
                showHideWhenYouViewOrHotListMembersRadioButton.Click();
            }
            else
            {
                showHideWhenYouViewOrHotListMembersRadioButton = WaitUntilElementExists(By.XPath("//input[contains(@id, 'rblHotList_1')]"));
                showHideWhenYouViewOrHotListMembersRadioButton.Click();
            }
        }

        public void SaveSettingsButton_Click()
        {
            saveSettingsButton = WaitUntilElementExists(By.XPath("//a[contains(@id, 'btnSaveSettings')]"));
            saveSettingsButton.Click();
        }
    }
}
