﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.Events;
using NUnit.Framework;
using log4net;
using System.Threading;

// PAGE URL:  http://www.jdate.com/ (while not logged in)

namespace WebdriverAutomation.Framework.Site_Pages
{
    public class Splash : BasePage
    {
        public Splash(IWebDriver driver)
            : base(driver)
        {
            Footer = new WebdriverAutomation.Framework.Site_Parts.Footer(driver);

            if (driver.Url.Contains("jdate.com"))
                textThatVerifiesThisPageIsCorrect = "Welcome to JDate, the premier Jewish singles community online. Connect with thousands of members in your area and around the world - photos, email, chat, IM and more.";
            else if (driver.Url.Contains("spark.com"))
                textThatVerifiesThisPageIsCorrect = "Online Dating at Spark.com";
            else if (driver.Url.Contains("blacksingles.com"))
                textThatVerifiesThisPageIsCorrect = "Member Login";  //black singles has a unique xpath for login
            else if (driver.Url.Contains("bbwpersonalsplus.com"))
                textThatVerifiesThisPageIsCorrect = "Why join BBW Personals Plus?";
            else if (driver.Url.Contains("jdate.co.il"))
                textThatVerifiesThisPageIsCorrect = "כניסה לחברים";
            else
                Assert.Fail("ERROR - SPLASH PAGE - We should be on this page but the URL does not contain the text for any known Spark sites that have been automated");

            // verify we are on the correct page       
            IWebElement textOnPage = null;

            if (driver.Url.Contains("jdate.com"))
                textOnPage = WaitUntilPageVerificationObjectExists(By.XPath("//div[@id='header-logo']/p"), "SPLASH PAGE");
            else if (driver.Url.Contains("spark.com"))
                textOnPage = WaitUntilPageVerificationObjectExists(By.XPath("//div[@id='vis-info']/div[1]/h1"), "SPLASH PAGE");
            else if (driver.Url.Contains("blacksingles.com"))
                textOnPage = WaitUntilPageVerificationObjectExists(By.XPath("//div[@id='headerLogin']/a"), "SPLASH PAGE");
            else if (driver.Url.Contains("bbwpersonalsplus.com"))
                textOnPage = WaitUntilPageVerificationObjectExists(By.XPath("//div[@class='vis-info']/h1"), "SPLASH PAGE");
            else if (driver.Url.Contains("jdate.co.il"))
                textOnPage = WaitUntilPageVerificationObjectExists(By.XPath("//a[@id='btnMemberLogin']"), "SPLASH PAGE");

            if (textOnPage.Text != (textThatVerifiesThisPageIsCorrect))
                log.Error("ERROR - SPLASH PAGE - We should be on this page but the page verification failed.");

            Assert.True(textOnPage.Text == textThatVerifiesThisPageIsCorrect, "ERROR - SPLASH PAGE - We should be on this page but the page verification failed.");

        }

        private static readonly ILog log = LogManager.GetLogger(typeof(Splash));
        public WebdriverAutomation.Framework.Site_Parts.Footer Footer;

        private string textThatVerifiesThisPageIsCorrect = string.Empty;

        internal IWebElement memberLoginLink = null;
        internal IWebElement browseForFreeLink = null;

        public WebdriverAutomation.Framework.Site_Pages.Login MemberLoginLink_Click()
        {
            if (driver.Url.Contains("jdate.com"))
                memberLoginLink = WaitUntilElementExists(By.XPath("//div[@id='min-max-container']/div[@id='header']/div[@id='header-login']/a"));
            else if (driver.Url.Contains("spark.com"))
                memberLoginLink = WaitUntilElementExists(By.XPath("//ul[@id='header-nav']/li[2]/a"));
            else if (driver.Url.Contains("blacksingles.com"))
                memberLoginLink = WaitUntilElementExists(By.XPath("//div[@id='headerLogin']/a"));
            else if (driver.Url.Contains("bbwpersonalsplus.com"))
                memberLoginLink = WaitUntilElementExists(By.XPath("//div[@id='header-login']/a"));
            else if (driver.Url.Contains("jdate.co.il"))
            {
                // 02/11/12 - on the Splash Page clicking the Login Button below is causing the error: Element cannot be scrolled into view:javascript:showLogin();
                //   This happens for every page that doesn't completely fit on the screen and for which the script tries to click an element. Maximizing the screen size to get around this issue.
                driver.Manage().Window.Maximize();

                memberLoginLink = WaitUntilElementExists(By.XPath("//a[@id='btnMemberLogin']"));
                //element.Click();

                //memberLoginLink = WaitUntilElementExists(By.XPath("//img[contains(@id, '_Image2')]"));
            }

            ClickUntilURLChanges(memberLoginLink);

            return new WebdriverAutomation.Framework.Site_Pages.Login(driver);
        }

        public WebdriverAutomation.Framework.Site_Pages.Registration BrowseForFreeLink_Click()
        {
            if (driver.Url.Contains("spark.com"))
                browseForFreeLink = WaitUntilElementExists(By.XPath("//div[@id='content-main']/a/img"));
            else if (driver.Url.Contains("bbwpersonalsplus.com"))
                browseForFreeLink = WaitUntilElementExists(By.XPath("//div[@id='reglink-wrap']/a"));
            else if (driver.Url.Contains("blacksingles.com"))
                browseForFreeLink = WaitUntilElementExists(By.XPath("//div[@id='content-main']/div/div[2]/div/a"));
            else
            {
                if (driver.FindElements(By.XPath("//div[@id='cta']/a")).Count > 0)
                    browseForFreeLink = WaitUntilElementExists(By.XPath("//div[@id='cta']/a"));
                else
                    browseForFreeLink = WaitUntilElementExists(By.XPath("//div[@id='cta']/div/input[contains(@id, '_btnContinue')]"));
            }

            ClickUntilURLChanges(browseForFreeLink);

            return new WebdriverAutomation.Framework.Site_Pages.Registration(driver);
        }

        #region Registration Scenario B and C objects
        public void YouAreAList_Select(int index)
        {
            SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[@id='idgendermask']")));
            select.SelectByIndex(index);
        }

        public void AgeRangeList_Select(int index)
        {
            SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, 'idAgeRange_AttributeOptions')]")));
            select.SelectByIndex(index);
        }

        public void CountryList_Select(int index)
        {
            SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, 'idRegion_country')]")));
            select.SelectByIndex(index);
        }

        public void ZipCodeTextbox_Write(string text)
        {
            WaitUntilElementIsWritable(By.XPath("//input[contains(@id, 'idRegion_postalCode')]"), text);
            IWebElement zipCodeTextbox = WaitUntilElementExists(By.XPath("//input[contains(@id, 'idRegion_postalCode')]"));
        }

        public void EmailAddressTextbox_Write(string text)
        {
            WaitUntilElementIsWritable(By.XPath("//input[@id='idemailaddress']"), text);
            IWebElement zipCodeTextbox = WaitUntilElementExists(By.XPath("//input[@id='idemailaddress']"));
        }

        public WebdriverAutomation.Framework.Site_Pages.Registration BrowseForFreeButton_Click()
        {
            WaitUntilElementIsClickable(By.XPath("//input[contains(@id, 'btnContinue')]"));

            return new WebdriverAutomation.Framework.Site_Pages.Registration(driver);
        }
        #endregion

        #region jdate.co.il
        public void BirthDateMonthList_Select(int index)
        {
            SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[@id='birthdayMonth']")));
            select.SelectByIndex(index);
        }

        public void BirthDateDayList_Select(int index)
        {
            SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[@id='birthdayDay']")));
            select.SelectByIndex(index);
        }

        public void BirthDateYearList_Select(int index)
        {
            SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[@id='birthdayYear']")));
            select.SelectByIndex(index);
        }

        public void YourReligiousBackground_Select(int index)
        {
            if (driver.Url.Contains("jdate.co.il"))
            {
                SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[@id='idjdatereligion']")));
                select.SelectByIndex(index);
            }
            else
            {
                SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[@id='idreligion']")));
                select.SelectByIndex(index);
            }
        }

        public void Country_Select(int index)
        {
            SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[@id='idregioncountryid']")));
            select.SelectByIndex(index);
        }

        public void CityTextbox_Write(string text)
        {
            WaitUntilElementIsWritable(By.XPath("//input[@id='regionCitiesByRegionId']"), text);

            Thread.Sleep(1000);

            string xPathOfSelectedIndex = "//html/body/ul/li/a";
            WaitUntilElementIsClickable(By.XPath(xPathOfSelectedIndex));
        }

        public void CityListBox_Select(string value)
        {
            SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//html/body/div/select")));
            select.SelectByText(value);
        }

        public bool PleaseEnterTheCodeShownBelowTextboxExists()
        {
            if (driver.FindElements(By.XPath("//input[contains(@id, 'idCaptcha_AttributeCaptchaControl')]")).Count > 0)
                return true;

            return false;
        }

        public void PleaseEnterTheCodeShownBelowTextbox_Write(string text)
        {
            if (driver.ToString() == System.Configuration.ConfigurationManager.AppSettings["IE_driverString"])
                WaitUntilElementValueCanBeCleared(By.XPath("//input[contains(@id, 'Captcha_AttributeCaptchaControl')]"));

            WaitUntilElementIsWritable(By.XPath("//input[contains(@id, 'idCaptcha_AttributeCaptchaControl')]"), text);
        }

        public void ChooseYourPasswordTextbox_Write(string text)
        {
            WaitUntilElementIsWritable(By.XPath("//input[@id='idpassword']"), text);
        }

        public void IWouldLikeSpecialOffersAndAnnouncements_Click()
        {
            string xPathOfSelectedIndex = "//input[@id='idnewslettermask']";

            WaitUntilElementIsClickable(By.XPath(xPathOfSelectedIndex));
        }

        public void IConfirmThatIHaveReadAndAgreedToTheTermsAndConditions_Click()
        {
            string xPathOfSelectedIndex = "//input[contains(@id, 'idTermsAndConditions_AttributeCheckBoxControl')]";

            WaitUntilElementIsClickable(By.XPath(xPathOfSelectedIndex));
        }

        public WebdriverAutomation.Framework.Site_Pages.RegistrationComplete ContinueButton_Click_RegistrationComplete()
        {
            string xPathOfSelectedIndex = "//div[@id='one-page-reg-form']/div[11]";

            try
            {
                WaitUntilElementIsClickable(By.XPath(xPathOfSelectedIndex));
            }
            catch
            {
                xPathOfSelectedIndex = "//button[@id='btnContinue']";
                WaitUntilElementIsClickable(By.XPath(xPathOfSelectedIndex));
            }

            //the registration page sometimes takes awhile to appear so wait for it
            for (int i = 0; i < 30; i++)
            {
                //// old?
                //if (driver.Url.Contains("jdate.co.il/Applications/MemberProfile/RegistrationWelcomeOnePage.aspx"))
                //    break;

                //// new 12/19/12 on preprod
                //if (driver.Url.Contains("jdate.co.il/Confirmation/?Scenario=Autumn%20Splash%20page"))
                //    break;

                if (driver.Url.Contains("jdate.co.il/Confirmation"))
                    break;

                Thread.Sleep(1000);
            }

            return new Framework.Site_Pages.RegistrationComplete(driver);
        }

        public Framework.Site_Pages.RegistrationJDateCoIlOnePage ContinueButton_Click()
        {
            string xPathOfSelectedIndex = "//div[@id='one-page-reg-form']/div[11]";

            try
            {
                WaitUntilElementIsClickable(By.XPath(xPathOfSelectedIndex));
            }
            catch
            {
                xPathOfSelectedIndex = "//button[@id='btnContinue']";
                WaitUntilElementIsClickable(By.XPath(xPathOfSelectedIndex));
            }

            //the registration page sometimes takes awhile to appear so wait for it
            for (int i = 0; i < 30; i++)
            {
                //// old?
                //if (driver.Url.Contains("jdate.co.il/Applications/MemberProfile/RegistrationWelcomeOnePage.aspx"))
                //    break;

                //// new 12/19/12 on preprod
                //if (driver.Url.Contains("jdate.co.il/Confirmation/?Scenario=Autumn%20Splash%20page"))
                //    break;

                if (driver.Url.Contains("jdate.co.il/Confirmation"))
                    break;

                Thread.Sleep(1000);
            }

            return new Framework.Site_Pages.RegistrationJDateCoIlOnePage(driver);
        }
        #endregion
    }
}
