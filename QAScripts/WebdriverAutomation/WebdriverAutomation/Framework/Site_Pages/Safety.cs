﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.Events;
using NUnit.Framework;
using log4net;
using System.Threading;

// PAGE URL:  http://www.jdate.com/Applications/Article/ArticleView.aspx?CategoryID=114&HideNav=true

namespace WebdriverAutomation.Framework.Site_Pages
{
    public class Safety : BasePage
    {
        public Safety(IWebDriver driver)
            : base(driver)
        {
            // wait for the page to load a little or else sometimes we end up verifying a blank page and the page verification fails
            Thread.Sleep(1500);

            // verify we are on the correct page      
            List<string> handlers = new List<string>();
            foreach (string handler in driver.WindowHandles)
                handlers.Add(handler);

            driver.SwitchTo().Window(handlers[1]);

            // the Safety Page is just a PDF so we can't verify any of the content, just verify that the URL is what we expect
            string expectedURLSegment = "com/safety/";

            if (!driver.Url.Contains(expectedURLSegment))
                log.Error("ERROR - SAFETY PAGE - We should be on this page but the page verification failed.");

            Assert.True(driver.Url.Contains(expectedURLSegment), "ERROR - SAFETY PAGE - We should be on this page but the page verification failed.");

            driver.Close();
            driver.SwitchTo().Window(handlers[0]);
        }

        private static readonly ILog log = LogManager.GetLogger(typeof(Safety));

        private const string textThatVerifiesThisPageIsCorrect = "Safety";



    }
}
