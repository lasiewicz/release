﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.Events;
using NUnit.Framework;
using log4net;
using System.Threading;

// PAGE URL:  https://secure.spark.net/jdatecom (while logged in)

namespace WebdriverAutomation.Framework.Site_Pages
{
    public class Subscribe : BasePage
    {
        public Subscribe(IWebDriver driver)
            : base(driver)
        {
            Header = new WebdriverAutomation.Framework.Site_Parts.Header(driver);
            Footer = new WebdriverAutomation.Framework.Site_Parts.Footer(driver);

            // verify we are on the correct page    
            IWebElement textOnPage = null;

            // there still appears to be a race condition on this page where we can't determine the url value if this script runs too quickly
            WaitUntilURLExists();

            if (driver.Url.Contains("jdatecoil"))
            {
                textThatVerifiesThisPageIsCorrect = "שלב 2: בחירת אופן תשלום";
                textThatVerifiesThisPageIsCorrect2 = "בחירת אמצעי תשלום";
                textThatVerifiesThisPageIsCorrect3 = "שלב 2 : בחירת אמצעי תשלום";
                textOnPage = WaitUntilPageVerificationObjectExists(By.XPath("//div[@id='content-main']/div[2]/div[@id='divSubscriptionStep2']/h2/span"), "SUBSCRIBE PAGE");
            }
            else
            {
                //textThatVerifiesThisPageIsCorrect = "Step 1: Select a Plan";
                //textOnPage = WaitUntilPageVerificationObjectExists(By.XPath("//div[@id='PanelNormalPlanSelection']/h2/span"), "SUBSCRIBE PAGE");
                textThatVerifiesThisPageIsCorrect = "Step 2: Billing Information";
                textOnPage = WaitUntilPageVerificationObjectExists(By.XPath("//div[@id='divSubscriptionStep2']/h2/span"), "SUBSCRIBE PAGE");
            }

            // jdate.com has 8 thousand different versions of this page so we're just going to use the URL to do page verifications of this
            string jdateComURL_preprod = @"https://ppsecure.spark.net/jdatecom";
            string jdateComURL_production = @"https://secure.spark.net/jdatecom";

            if (driver.Url.Contains("jdatecom"))
            {
                if ((driver.Url != jdateComURL_preprod) && driver.Url != jdateComURL_production)
                    log.Error("ERROR - SUBSCRIBE PAGE - We should be on this page but the page verification failed.");

                Assert.True((driver.Url == jdateComURL_preprod) || (driver.Url == jdateComURL_production), "ERROR - SUBSCRIBE PAGE - We should be on this page but the page verification failed.");
            }
            // all other non jdate.com sites
            else
            {
                if (textOnPage.Text != (textThatVerifiesThisPageIsCorrect) && textOnPage.Text != (textThatVerifiesThisPageIsCorrect2) && textOnPage.Text != (textThatVerifiesThisPageIsCorrect3))
                    log.Error("ERROR - SUBSCRIBE PAGE - We should be on this page but the page verification failed.");

                Assert.True(textOnPage.Text == textThatVerifiesThisPageIsCorrect || textOnPage.Text == textThatVerifiesThisPageIsCorrect2 || textOnPage.Text == textThatVerifiesThisPageIsCorrect3, "ERROR - SUBSCRIBE PAGE - We should be on this page but the page verification failed.");
            }

        }

        private static readonly ILog log = LogManager.GetLogger(typeof(Subscribe));
        public WebdriverAutomation.Framework.Site_Parts.Header Header;
        public WebdriverAutomation.Framework.Site_Parts.Footer Footer;

        private string textThatVerifiesThisPageIsCorrect = string.Empty;
        private string textThatVerifiesThisPageIsCorrect2 = string.Empty;
        private string textThatVerifiesThisPageIsCorrect3 = string.Empty;

        public enum SubscriptionPlans { Premium_6Months, Standard_6Months, Standard_3Months, Standard_1Month };

        /// <summary>
        /// This is the header message bar that appears above, "Subscribe now to connect to Jewish singles like you!"
        /// Normally this header reads, "Home > Subscribe" but if a registered user attempts to access their Inbox after being messaged it should
        /// read, "Want to read your message? Become a Premium Member today!"
        /// </summary>
        /// <returns></returns>
        public string HeaderStatusMessage()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//div[@id='tblHeaderAlternate']"));
            return element.Text;
        }

        #region Select a Plan
        public void SelectAPlanRadioButton_Select(SubscriptionPlans plan)
        {
            IWebElement element;

            switch (plan)
            {
                case SubscriptionPlans.Premium_6Months:
                    element = WaitUntilElementExists(By.XPath("//div[@id='pnlPlanDisplay']/ul[1]/li[1]/input[@id='Plan']"));
                    element.Click();
                    break;
                case SubscriptionPlans.Standard_6Months:
                    element = WaitUntilElementExists(By.XPath("//div[@id='pnlPlanDisplay']/ul[2]/li[1]/input[@id='Plan']"));
                    element.Click();
                    break;
                case SubscriptionPlans.Standard_3Months:
                    element = WaitUntilElementExists(By.XPath("//div[@id='pnlPlanDisplay']/ul[2]/li[2]/input[@id='Plan']"));
                    element.Click();
                    break;
                case SubscriptionPlans.Standard_1Month:
                    element = WaitUntilElementExists(By.XPath("//div[@id='pnlPlanDisplay']/ul[2]/li[3]/input[@id='Plan']"));
                    element.Click();
                    break;
            }
        }
        #endregion

        #region Name and Address
        public string FirstNameTextbox
        {
            get
            {
                IWebElement element = WaitUntilElementExists(By.XPath("//input[@id='firstName']"));
                return element.GetAttribute("value");
            }
            set
            {
                string theXpath = "//input[@id='firstName']";

                WaitUntilElementValueCanBeCleared(By.XPath(theXpath));
                SendKeys(WaitUntilElementExists(By.XPath(theXpath)), value);

                // the page takes time to verify the zip/postal code which sometimes skips the next few lines of code
                Wait(1000);
            }
        }

        public string LastNameTextbox
        {
            get
            {
                IWebElement element = WaitUntilElementExists(By.XPath("//input[@id='lastName']"));
                return element.GetAttribute("value");
            }
            set
            {
                string theXpath = "//input[@id='lastName']";

                WaitUntilElementValueCanBeCleared(By.XPath(theXpath));
                SendKeys(WaitUntilElementExists(By.XPath(theXpath)), value);

                // the page takes time to verify the zip/postal code which sometimes skips the next few lines of code
                Wait(1000);
            }
        }

        public string AddressTextbox
        {
            get
            {
                IWebElement element = WaitUntilElementExists(By.XPath("////input[@id='address']"));
                return element.GetAttribute("value");
            }
            set
            {
                string theXpath = "//input[@id='address']";

                WaitUntilElementValueCanBeCleared(By.XPath(theXpath));
                SendKeys(WaitUntilElementExists(By.XPath(theXpath)), value);

                // the page takes time to verify the zip/postal code which sometimes skips the next few lines of code
                Wait(1000);
            }
        }

        public string CityTextbox
        {
            get
            {
                IWebElement element = WaitUntilElementExists(By.XPath("//input[@id='city']"));
                return element.GetAttribute("value");
            }
            set
            {
                string theXpath = "//input[@id='city']";

                WaitUntilElementValueCanBeCleared(By.XPath(theXpath));
                SendKeys(WaitUntilElementExists(By.XPath(theXpath)), value);

                // the page takes time to verify the zip/postal code which sometimes skips the next few lines of code
                Wait(1000);
            }
        }

        public string StateTextbox
        {
            get
            {
                IWebElement element = WaitUntilElementExists(By.XPath("//input[@id='state']"));
                return element.GetAttribute("value");
            }
            set
            {
                string theXpath = "//input[@id='state']";

                WaitUntilElementValueCanBeCleared(By.XPath(theXpath));
                SendKeys(WaitUntilElementExists(By.XPath(theXpath)), value);

                // the page takes time to verify the zip/postal code which sometimes skips the next few lines of code
                Wait(1000);
            }
        }

        public string ZipTextbox
        {
            get
            {
                IWebElement element = WaitUntilElementExists(By.XPath("//input[@id='zip']"));
                return element.GetAttribute("value");
            }
            set
            {
                string theXpath = "//input[@id='zip']";

                WaitUntilElementValueCanBeCleared(By.XPath(theXpath));
                SendKeys(WaitUntilElementExists(By.XPath(theXpath)), value);

                // the page takes time to verify the zip/postal code which sometimes skips the next few lines of code
                Wait(1000);
            }
        }

        public string CountryDropdown
        {
            get
            {
                string theXpath = "//select[@id='country']";
                return ReturnTextOfSelectedValueInList(By.XPath(theXpath));
            }
            set
            {
                SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[@id='country']")));
                select.SelectByText(value);

                // the page takes time to display the correct zip/postal/etc. code textbox
                Wait(1000);
            }
        }

        public string PhoneNumberTextbox
        {
            get
            {
                IWebElement element = WaitUntilElementExists(By.XPath("//input[@id='phone']"));
                return element.GetAttribute("value");
            }
            set
            {
                string theXpath = "//input[@id='phone']";

                WaitUntilElementValueCanBeCleared(By.XPath(theXpath));
                SendKeys(WaitUntilElementExists(By.XPath(theXpath)), value);

                // the page takes time to verify the zip/postal code which sometimes skips the next few lines of code
                Wait(1000);
            }
        }
        #endregion

        #region Credit Card Information
        public string CreditCardTypeDropdown
        {
            get
            {
                string theXpath = "//select[@id='cardtype']";
                return ReturnTextOfSelectedValueInList(By.XPath(theXpath));
            }
            set
            {
                SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[@id='cardtype']")));
                select.SelectByText(value);

                // the page takes time to display the correct zip/postal/etc. code textbox
                Wait(1000);
            }
        }

        public string CardNumberTextbox
        {
            get
            {
                IWebElement element = WaitUntilElementExists(By.XPath("//input[@id='ccnumber']"));
                return element.GetAttribute("value");
            }
            set
            {
                string theXpath = "//input[@id='ccnumber']";

                WaitUntilElementValueCanBeCleared(By.XPath(theXpath));
                SendKeys(WaitUntilElementExists(By.XPath(theXpath)), value);

                // the page takes time to verify the zip/postal code which sometimes skips the next few lines of code
                Wait(1000);
            }
        }

        public string ExpirationMonthDropdown
        {
            get
            {
                string theXpath = "//select[@id='expMonth']";
                return ReturnTextOfSelectedValueInList(By.XPath(theXpath));
            }
            set
            {
                SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[@id='expMonth']")));
                select.SelectByText(value);

                // the page takes time to display the correct zip/postal/etc. code textbox
                Wait(1000);
            }
        }

        public string ExpirationYearDropdown
        {
            get
            {
                string theXpath = "//select[@id='expYear']";
                return ReturnTextOfSelectedValueInList(By.XPath(theXpath));
            }
            set
            {
                SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[@id='expYear']")));
                select.SelectByText(value);

                // the page takes time to display the correct zip/postal/etc. code textbox
                Wait(1000);
            }
        }

        public string CSCTextbox
        {
            get
            {
                IWebElement element = WaitUntilElementExists(By.XPath("//input[@id='securitycode']"));
                return element.GetAttribute("value");
            }
            set
            {
                string theXpath = "//input[@id='securitycode']";

                WaitUntilElementValueCanBeCleared(By.XPath(theXpath));
                SendKeys(WaitUntilElementExists(By.XPath(theXpath)), value);

                // the page takes time to verify the zip/postal code which sometimes skips the next few lines of code
                Wait(1000);
            }
        }
        #endregion

        public Framework.Site_Pages.SubscribeConfirmation ProcessOrderButton_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//input[@id='lbtnProcessOrder']"));
            element.Click();

            return new SubscribeConfirmation(driver);
        }
    }
}
