﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.Events;
using NUnit.Framework;
using log4net;

// PAGE URL:  http://www.jdate.com/Applications/LookupProfile/LookupProfile.aspx?NavPoint=sub

namespace WebdriverAutomation.Framework.Site_Pages
{
    public class LookUpMember : BasePage
    {
        public LookUpMember(IWebDriver driver)
            : base(driver)
        {
            Header = new WebdriverAutomation.Framework.Site_Parts.Header(driver);
            Footer = new WebdriverAutomation.Framework.Site_Parts.Footer(driver);

            Tests.ReusableCode.Ads ads = new Tests.ReusableCode.Ads();
            ads.KillAllAds(driver, log);

            // verify we are on the correct page       
            IWebElement textOnPage = null;

            if (driver.Url.Contains("jdate.co.il"))
                textThatVerifiesThisPageIsCorrect = "איתור פרופיל";
            else
                textThatVerifiesThisPageIsCorrect = "Look Up Member";

            textOnPage = WaitUntilPageVerificationObjectExists(By.XPath("//div[@id='content-main']/h1"), "LOOK UP MEMBER PAGE");

            if (textOnPage.Text != (textThatVerifiesThisPageIsCorrect))
                log.Error("ERROR - LOOK UP MEMBER PAGE - We should be on this page but the page verification failed.");

            Assert.True(textOnPage.Text == textThatVerifiesThisPageIsCorrect, "ERROR - LOOK UP MEMBER PAGE - We should be on this page but the page verification failed.");

        }

        private static readonly ILog log = LogManager.GetLogger(typeof(Subscribe));
        public WebdriverAutomation.Framework.Site_Parts.Header Header;
        public WebdriverAutomation.Framework.Site_Parts.Footer Footer;

        private string textThatVerifiesThisPageIsCorrect = string.Empty;

        public string MemberNumberTextbox
        {
            get
            {
                IWebElement element = WaitUntilElementExists(By.XPath("//input[contains(@id, 'LookupByMemberID_LookupMemberID')]"));
                return element.GetAttribute("value");
            }
            set
            {
                string theXpath = "//input[contains(@id, 'LookupByMemberID_LookupMemberID')]";

                WaitUntilElementValueCanBeCleared(By.XPath(theXpath));
                SendKeys(WaitUntilElementExists(By.XPath(theXpath)), value);
            }
        }

        public Framework.Site_Pages.YourProfile_old MemberNumberTextbox_EnterButtonPress()
        {
            string theXpath = "//input[contains(@id, 'LookupByMemberID_LookupMemberID')]";
            WaitUntilElementExists(By.XPath(theXpath)).SendKeys(Keys.Enter);

            return new Framework.Site_Pages.YourProfile_old(driver);
        }

        public Framework.Site_Pages.YourProfile_old MemberNumberSearchButton_OldProfile_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//input[contains(@id, 'LookupByMemberID_btnSearchByMemberNumber')]"));
            ClickUntilURLChanges(element);

            return new YourProfile_old(driver);
        }

        public Framework.Site_Pages.YourProfile MemberNumberSearchButton_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//input[contains(@id, 'LookupByMemberID_btnSearchByMemberNumber')]"));
            ClickUntilURLChanges(element);

            return new YourProfile(driver);
        }
        
        public string UsernameTextbox
        {
            get
            {
                IWebElement element = WaitUntilElementExists(By.XPath("//input[contains(@id, 'LookupByUsername_LookupUserName')]"));
                return element.GetAttribute("value");
            }
            set
            {
                string theXpath = "//input[contains(@id, 'LookupByUsername_LookupUserName')]";

                WaitUntilElementValueCanBeCleared(By.XPath(theXpath));
                SendKeys(WaitUntilElementExists(By.XPath(theXpath)), value);
            }
        }

        public Framework.Site_Pages.YourProfile_old UsernameSearchButton_OldProfile_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//input[contains(@id, 'LookupByUsername_btnLookupByUserName')]"));
            ClickUntilURLChanges(element);

            return new YourProfile_old(driver);
        }

        public Framework.Site_Pages.YourProfile UsernameSearchButton_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//input[contains(@id, 'LookupByUsername_btnLookupByUserName')]"));
            ClickUntilURLChanges(element);

            return new YourProfile(driver);
        }

    }
}
