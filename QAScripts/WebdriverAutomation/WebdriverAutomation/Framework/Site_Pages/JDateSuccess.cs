﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.Events;
using NUnit.Framework;
using log4net;

// PAGE URL:  http://www.jdate.com/Applications/SiteMap/SiteMap.aspx

namespace WebdriverAutomation.Framework.Site_Pages
{
    public class JDateSuccess : BasePage
    {
        public JDateSuccess(IWebDriver driver)
            : base(driver)
        {
            Footer = new WebdriverAutomation.Framework.Site_Parts.Footer(driver);

            // verify we are on the correct page       
            if (!driver.Url.Contains("jdate-infographic"))
                log.Error("ERROR - SITE MAP PAGE - We should be on this page but the page verification failed.");

            Assert.True(driver.Url.Contains("jdate-infographic"), "ERROR - SITE MAP PAGE - We should be on this page but the page verification failed.");

        }

        private static readonly ILog log = LogManager.GetLogger(typeof(SiteMap)); 
        public WebdriverAutomation.Framework.Site_Parts.Footer Footer;


    }
}
