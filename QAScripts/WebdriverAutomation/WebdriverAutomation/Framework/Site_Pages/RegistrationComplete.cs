﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.Events;
using NUnit.Framework;
using log4net;
using System.Threading;

// PAGE URL:  http://www.jdate.com/Applications/MemberProfile/RegistrationWelcome.aspx?registrationMode=1&&DestinationURL= (not logged in)

namespace WebdriverAutomation.Framework.Site_Pages
{
    public class RegistrationComplete : BasePage
    {
        public RegistrationComplete(IWebDriver driver)
            : base(driver)
        {
            Footer = new WebdriverAutomation.Framework.Site_Parts.Footer(driver);

            if (driver.Url.Contains("jdate.com"))
                textThatVerifiesThisPageIsCorrect1 = "Thank you for registering on JDate";
            else if (driver.Url.Contains("spark.com"))
                textThatVerifiesThisPageIsCorrect1 = "Thank you for registering on Spark.com!";
            else if (driver.Url.Contains("blacksingles.com"))
                textThatVerifiesThisPageIsCorrect1 = "Thank you for registering on BlackSingles.com!";
            else if (driver.Url.Contains("bbwpersonalsplus.com"))
                textThatVerifiesThisPageIsCorrect1 = "Thank you for registering on BBWPersonalsPlus.com!";
            else if (driver.Url.Contains("jdate.co.il"))
            {
                textThatVerifiesThisPageIsCorrect1 = "הרשמתך נקלטה. תודה.";
                textThatVerifiesThisPageIsCorrect2 = "נרשמת בהצלחה!";
            }

            // verify we are on the correct page       
            IWebElement textOnPage = null;

            int timer = 5;

            // we have a timing issue where sometimes we check for the Registration Page verification object (which maps to "Create Your Profile") before the Registration Complete Page has time to appear
            for (int i = 0; i < timer; i++)
            {
                if (driver.FindElements(By.XPath("//div[@id='container']/div/h1")).Count > 0)
                {
                    textOnPage = WaitUntilPageVerificationObjectExists(By.XPath("//div[@id='container']/div/h1"), "REGISTRATION COMPLETE PAGE");

                    // 10/30/12 still getting the Element not found in the cache error when seeing if textonpage.text.contains("create your profile").  creating a catch to try to narrow down why.
                    try
                    {
                        if (driver.Url.Contains("jdate.co.il"))
                        {
                            if (!textOnPage.Text.Contains("הרשמה"))
                                break;
                        }
                        else
                        {
                            if (!textOnPage.Text.Contains("Create Your Profile"))
                                break;
                        }
                    }
                    catch
                    {
                        string blarg = textOnPage.Text;
                        Assert.Fail("Element not found in the cache - perhaps the page has changed since it was looked up - catch statement reached.");
                    }
                }

                Thread.Sleep(1000);
            }

            if (!textOnPage.Text.Contains(textThatVerifiesThisPageIsCorrect1) && !textOnPage.Text.Contains(textThatVerifiesThisPageIsCorrect2))
                log.Error("ERROR - REGISTRATION COMPLETE PAGE - We should be on this page but the page verification failed.");

            Assert.True(textOnPage.Text.Contains(textThatVerifiesThisPageIsCorrect1) || textOnPage.Text.Contains(textThatVerifiesThisPageIsCorrect2), "ERROR - REGISTRATION COMPLETE PAGE - We should be on this page but the page verification failed." + "Page verification variables:   textOnPage.text = " + textOnPage.Text + "   textThatVerifiesThisPageIsCorrect1 = " + textThatVerifiesThisPageIsCorrect1 + "   textThatVerifiesThisPageIsCorrect12 = " + textThatVerifiesThisPageIsCorrect2);

        }

        private static readonly ILog log = LogManager.GetLogger(typeof(Splash));
        public WebdriverAutomation.Framework.Site_Parts.Footer Footer;

        private string textThatVerifiesThisPageIsCorrect1 = string.Empty;
        private string textThatVerifiesThisPageIsCorrect2 = string.Empty;

        public Framework.Site_Pages.RegistrationJDateCoIlOnePage CompleteProfileLink_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//div[@id='container']/div/aside/section[2]/div/a"));
            ClickUntilURLChanges(element);

            return new WebdriverAutomation.Framework.Site_Pages.RegistrationJDateCoIlOnePage(driver);
        }
    }

}

