﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.Events;
using NUnit.Framework;
using log4net;

// PAGE URL:  http://www.jdate.com/singles/

namespace WebdriverAutomation.Framework.Site_Pages
{
    public class JewishSingles : BasePage
    {
        public JewishSingles(IWebDriver driver)
            : base(driver)
        {
            // verify we are on the correct page      
            List<string> handlers = new List<string>();
            foreach (string handler in driver.WindowHandles)
                handlers.Add(handler);

            string parentWindow = driver.CurrentWindowHandle;

            // when we click on this link from the footer a new window appears. currently having problems switching pages in IE.  for now if we fail just continue
            try
            {
                driver.SwitchTo().Window(handlers[1]);

                IWebElement textOnPage = null;

                textOnPage = WaitUntilPageVerificationObjectExists(By.XPath("//div[@id='titles']/h1/div"), "JEWISH SINGLES PAGE");

                if (textOnPage.Text != (textThatVerifiesThisPageIsCorrect))
                    log.Error("ERROR - JEWISH SINGLES PAGE - We should be on this page but the page verification failed.");

                Assert.True(textOnPage.Text == textThatVerifiesThisPageIsCorrect, "ERROR - JEWISH SINGLES PAGE - We should be on this page but the page verification failed.");

                driver.Close();
                driver.SwitchTo().Window(handlers[0]);
            }
            catch
            {
            }


        }

        private static readonly ILog log = LogManager.GetLogger(typeof(JewishSingles));

        private const string textThatVerifiesThisPageIsCorrect = "Meet and Communicate with Fun Singles Near You on JDate";




    }
}
