﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.Events;
using NUnit.Framework;
using log4net;

// PAGE URL:  http://admintool.matchnet.com/

namespace WebdriverAutomation.Framework.Pages
{
    public class AdminTool_HomePage : BasePage
    {
        public AdminTool_HomePage(IWebDriver driver)
            : base(driver)
        {
            Header = new WebdriverAutomation.Framework.Parts.AdminTool_Header(driver);

            // verify we are on the correct page       
            IWebElement textOnPage = null;

            textOnPage = WaitUntilPageVerificationObjectExists(By.XPath("//div[@id='member-search-sidebar']/p[1]"), "ADMIN TOOL - HOME PAGE");

            if (textOnPage.Text != (textThatVerifiesThisPageIsCorrect))
                log.Error("ERROR - ADMIN TOOL - HOME PAGE - We should be on this page but the page verification failed.");

            Assert.True(textOnPage.Text == textThatVerifiesThisPageIsCorrect, "ERROR - ADMIN TOOL - HOME PAGE - We should be on this page but the page verification failed.");
        }

        private static readonly ILog log = LogManager.GetLogger(typeof(AdvertiseWithUs));
        public WebdriverAutomation.Framework.Parts.AdminTool_Header Header; 

        private const string textThatVerifiesThisPageIsCorrect = "Search Criteria";

    }
}
