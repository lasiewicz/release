﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.Events;
using NUnit.Framework;
using log4net;

// PAGE URL:  https://affiliates.spark.net/en/

namespace WebdriverAutomation.Framework.Site_Pages
{
    public class AffiliateProgram : BasePage
    {
        public AffiliateProgram(IWebDriver driver)
            : base(driver)
        {
            // verify we are on the correct page       
            IWebElement textOnPage = null;

            textOnPage = WaitUntilPageVerificationObjectExists(By.XPath("//div[@id='content']/div[@id='background']/div[1]/div/h2"), "AFFILIATE PROGRAM PAGE");

            if (textOnPage.Text != (textThatVerifiesThisPageIsCorrect))
                log.Error("ERROR - AFFILIATE PROGRAM PAGE - We should be on this page but the page verification failed.");

            Assert.True(textOnPage.Text == textThatVerifiesThisPageIsCorrect, "ERROR - AFFILIATE PROGRAM PAGE - We should be on this page but the page verification failed.");
        }

        private static readonly ILog log = LogManager.GetLogger(typeof(AffiliateProgram)); 

        private const string textThatVerifiesThisPageIsCorrect = "Ready to harness the earning power of over 30 online communities to drive your personal bottom line?";

    }
}
