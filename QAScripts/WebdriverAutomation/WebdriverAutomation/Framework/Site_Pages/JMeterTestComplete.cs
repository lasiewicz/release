﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.Events;
using NUnit.Framework;
using log4net;
using System.Threading;

// PAGE URL:  http://www.jdate.co.il/Applications/CompatibilityMeter/Completion.aspx?SendMail=1

namespace WebdriverAutomation.Framework.Site_Pages
{
    public class JMeterTestComplete : BasePage
    {
        public JMeterTestComplete(IWebDriver driver)
            : base(driver)
        {
            Header = new WebdriverAutomation.Framework.Site_Parts.Header(driver);

            // verify we are on the correct page       
            IWebElement textOnPage = null;

            textOnPage = WaitUntilPageVerificationObjectExists(By.XPath("//div[@id='div_print']/div/h1"), "JMETER TEST COMPLETE PAGE");

            if (!textOnPage.Text.Contains(textThatVerifiesThisPageIsCorrect))
                log.Error("ERROR - JMETER TEST COMPLETE PAGE - We should be on this page but the page verification failed.");

            Assert.True(textOnPage.Text.Contains(textThatVerifiesThisPageIsCorrect), "ERROR - JMETER TEST COMPLETE PAGE - We should be on this page but the page verification failed.");

        }

        private static readonly ILog log = LogManager.GetLogger(typeof(JMeterWelcome));
        public WebdriverAutomation.Framework.Site_Parts.Header Header;

        private const string textThatVerifiesThisPageIsCorrect = "סיימת את השאלון בהצלחה ♥";

    }
}

