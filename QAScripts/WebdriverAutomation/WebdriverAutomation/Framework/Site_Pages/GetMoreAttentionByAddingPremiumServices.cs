﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.Events;
using NUnit.Framework;
using log4net;

// PAGE URL:  https://secure.spark.net/jdatecom  (with subscription bought)

namespace WebdriverAutomation.Framework.Site_Pages
{
    public class GetMoreAttentionByAddingPremiumServices : BasePage
    {
        public GetMoreAttentionByAddingPremiumServices(IWebDriver driver)
            : base(driver)
        {
            // verify we are on the correct page       
            IWebElement textOnPage = null;

            textOnPage = WaitUntilPageVerificationObjectExists(By.XPath("//span[@id='lblHedaerTxt']"), "GET MORE ATTENTION BY ADDING PREMIUM SERVICES! PAGE");

            if (textOnPage.Text != (textThatVerifiesThisPageIsCorrect))
                log.Error("ERROR - GET MORE ATTENTION BY ADDING PREMIUM SERVICES! PAGE - We should be on this page but the page verification failed.");

            Assert.True(textOnPage.Text == textThatVerifiesThisPageIsCorrect, "ERROR - GET MORE ATTENTION BY ADDING PREMIUM SERVICES! PAGE - We should be on this page but the page verification failed.");

        }

        private static readonly ILog log = LogManager.GetLogger(typeof(GetMoreAttentionByAddingPremiumServices));

        private const string textThatVerifiesThisPageIsCorrect = "Get More Attention by Adding Premium Services!";

    }
}
