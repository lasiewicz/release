﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.Events;
using NUnit.Framework;
using log4net;

// ex PAGE URL:  http://www.jdate.co.il/Applications/CompatibilityMeter/OneOnOne.aspx?MemberID=119533446

namespace WebdriverAutomation.Framework.Site_Pages
{
    public class JMeterOneOnONe : BasePage
    {
        public JMeterOneOnONe(IWebDriver driver)
            : base(driver)
        {
            Footer = new WebdriverAutomation.Framework.Site_Parts.Footer(driver);

            // verify we are on the correct page       
            IWebElement textOnPage = null;

            textOnPage = WaitUntilPageVerificationObjectExists(By.XPath("//div[@id='div_print']/div/h1"), "JMETER ONE ON ONE PAGE");
            
            if (textOnPage.Text != (textThatVerifiesThisPageIsCorrect))
                log.Error("ERROR - JMETER ONE ON ONE PAGE - We should be on this page but the page verification failed.");

            Assert.True(textOnPage.Text == textThatVerifiesThisPageIsCorrect, "ERROR - JMETER ONE ON ONE PAGE - We should be on this page but the page verification failed.");

        }

        private static readonly ILog log = LogManager.GetLogger(typeof(AboutJDate));
        public WebdriverAutomation.Framework.Site_Parts.Footer Footer;

        private const string textThatVerifiesThisPageIsCorrect = "התאמה אישית";

    }
}
