﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.Events;
using NUnit.Framework;
using log4net;
using System.Threading;

// PAGE URL:  http://www.jdate.co.il/Applications/CompatibilityMeter/PersonalityTest.aspx

namespace WebdriverAutomation.Framework.Site_Pages
{
    public class JMeterPersonalityTest : BasePage
    {
        public JMeterPersonalityTest(IWebDriver driver)
            : base(driver)
        {
            Header = new WebdriverAutomation.Framework.Mobile_Parts.Header(driver);

            // verify we are on the correct page       
            if (!driver.Url.Contains(urlThatVerifiesThisPageIsCorrect))
                log.Error("ERROR - JMETER PERSONALITY TEST PAGE - We should be on this page but the page verification failed.");

            Assert.True(driver.Url.Contains(urlThatVerifiesThisPageIsCorrect), "ERROR - JMETER PERSONALITY TEST PAGE - We should be on this page but the page verification failed.");

        }

        private static readonly ILog log = LogManager.GetLogger(typeof(JMeterPersonalityTest));
        public WebdriverAutomation.Framework.Mobile_Parts.Header Header;

        private const string urlThatVerifiesThisPageIsCorrect = "CompatibilityMeter/PersonalityTest.aspx";
        
        public void SevenButton_Click()
        {
            // switch to iFrame
            driver.SwitchTo().Frame(driver.FindElement(By.Id("_ctl0__ctl3_ifrm")));

            IWebElement element = WaitUntilElementExists(By.XPath("//a[@id='But7']"));
            element.Click();
            Thread.Sleep(1500);

            // switch back
            driver.SwitchTo().DefaultContent();
        }

        public bool IsProgressBarLessThan99Percent()
        {
            // switch to iFrame
            driver.SwitchTo().Frame(driver.FindElement(By.Id("_ctl0__ctl3_ifrm")));

            IWebElement element = WaitUntilElementExists(By.XPath("//span[@id='ProgressBar1_Pres']"));

            int percentComplete = Convert.ToInt32(element.Text.Substring(0, element.Text.Length - 1));            

            // switch back
            driver.SwitchTo().DefaultContent();

            if (percentComplete < 98)
                return true;
            else
                return false;

        }

        public bool DoesQuestionaireStillAppear()
        {
            if (driver.FindElements(By.Id("_ctl0__ctl3_ifrm")).Count > 0)
                return true;
            else
                return false;

        }

    }
}

