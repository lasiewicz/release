﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.Events;
using NUnit.Framework;
using log4net;

// PAGE URL:  http://www.jdate.com/Applications/MemberProfile/ViewProfile.aspx?Mode=Edit (while logged in)

namespace WebdriverAutomation.Framework.Site_Pages
{
    public class EditYourProfile : BasePage
    {
        public EditYourProfile(IWebDriver driver)
            : base(driver)
        {
            Header = new WebdriverAutomation.Framework.Site_Parts.Header(driver);
            Footer = new WebdriverAutomation.Framework.Site_Parts.Footer(driver);

            Tests.ReusableCode.Ads ads = new Tests.ReusableCode.Ads();
            ads.KillAllAds(driver, log);

            // verify that our account isn't suspended
            string notificationBarText = string.Empty;

            try
            {
                IWebElement accountSuspendedText = driver.FindElement(By.XPath("//div[contains(@id, 'divNotification')]"));
                notificationBarText = accountSuspendedText.Text;
            }
            catch { }

            if (notificationBarText.Contains("Your account has been suspended"))
            {
                log.Error("ERROR - We cannot login because the account in this test case has been suspended.");
                Assert.Fail("ERROR - We cannot login because the account in this test case has been suspended.");
            }

            // verify we are on the correct page       
            IWebElement textOnPage = null;

            textOnPage = WaitUntilPageVerificationObjectExists(By.XPath("//div[@id='edit-profile-save-upper']/div"), "EDIT YOUR PROFILE PAGE");

            if (textOnPage.Text != (textThatVerifiesThisPageIsCorrect))
                log.Error("ERROR - EDIT YOUR PROFILE PAGE - We should be on this page but the page verification failed.");

            Assert.True(textOnPage.Text == textThatVerifiesThisPageIsCorrect, "ERROR - EDIT YOUR PROFILE PAGE - We should be on this page but the page verification failed.");

        }

        private static readonly ILog log = LogManager.GetLogger(typeof(Login));
        public WebdriverAutomation.Framework.Site_Parts.Header Header;
        public WebdriverAutomation.Framework.Site_Parts.Footer Footer;

        private const string textThatVerifiesThisPageIsCorrect = "Edit this profile tab below and don't forget to save your changes when you are done.";

        // Basics / Introduction etc.
        public void Tab1_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//a[contains(@id, 'DynamicTabGroup1_repeaterTabs__ctl1_tabLink')]"));
            element.Click();
        }

        // Lifestyle / Who I Am... etc.
        public void Tab2_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//a[contains(@id, 'DynamicTabGroup1_repeaterTabs__ctl2_tabLink')]"));
            element.Click();
        }

        // Interests / I'm Looking For... etc.
        public void Tab3_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//a[contains(@id, 'DynamicTabGroup1_repeaterTabs__ctl3_tabLink')]"));
            element.Click();
        }

        // Relationship / Compatability etc.
        public void Tab4_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//a[contains(@id, 'DynamicTabGroup1_repeaterTabs__ctl4_tabLink')]"));
            element.Click();
        }

        // Color Code / Q & A etc.
        public void Tab5_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//a[contains(@id, 'DynamicTabGroup1_repeaterTabs__ctl5_tabLink')]"));
            element.Click();
        }

        // Kibitz Tab / Photos (Spark) etc.
        public void Tab6_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//a[contains(@id, 'DynamicTabGroup1_repeaterTabs__ctl6_tabLink')]"));
            element.Click();
        }

        // Photos (JDate)
        public void Tab7_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//a[contains(@id, 'DynamicTabGroup1_repeaterTabs__ctl7_tabLink')]"));
            element.Click();
        }

        /// <summary>
        /// Something weird is going on. Sometimes when the user Saves Changes, the Profile Page appears but sometimes it kicks the user back into the Edit Profile
        /// > Basics tab page instead.  
        /// Making this button return void instead of Framework.Site_Pages.YourProfile.
        /// </summary>
        /// <returns></returns>
        public void SaveChangesButton_Click()
        {
            Wait(1000);

            IWebElement element = WaitUntilElementExists(By.XPath("//input[contains(@id, 'DynamicTabGroup1_btnSave1')]"));
            element.Click();

            Wait(1000);

            Tests.ReusableCode.Ads ads = new Tests.ReusableCode.Ads();
            ads.KillAllAds(driver, log);
        }

        public string UsernameTextbox
        {
            get
            {
                IWebElement element = WaitUntilElementExists(By.XPath("//input[contains(@id, 'username')]"));
                return element.GetAttribute("value");
            }
            set
            {
                string theXpath = "//input[contains(@id, 'username')]";

                WaitUntilElementValueCanBeCleared(By.XPath(theXpath));
                SendKeys(WaitUntilElementExists(By.XPath(theXpath)), value);
            }
        }

        public string FirstNameTextbox
        {
            get
            {
                IWebElement element = WaitUntilElementExists(By.XPath("//input[contains(@id, 'sitefirstname')]"));
                return element.GetAttribute("value");
            }
            set
            {
                string theXpath = "//input[contains(@id, 'sitefirstname')]";

                WaitUntilElementValueCanBeCleared(By.XPath(theXpath));

                SendKeys(WaitUntilElementExists(By.XPath(theXpath)), value);
            }
        }

        public string LastNameTextbox
        {
            get
            {
                IWebElement element = WaitUntilElementExists(By.XPath("//input[contains(@id, 'sitelastname')]"));
                return element.GetAttribute("value");
            }
            set
            {
                string theXpath = "//input[contains(@id, 'sitelastname')]";

                WaitUntilElementValueCanBeCleared(By.XPath(theXpath));
                SendKeys(WaitUntilElementExists(By.XPath(theXpath)), value);
            }
        }

        public string BirthDateMonthDropdown
        {
            get
            {
                string theXpath = "//select[contains(@id, 'age-month')]";
                return ReturnTextOfSelectedValueInList(By.XPath(theXpath));
            }
            set
            {
                SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, 'age-month')]")));
                select.SelectByText(value);
            }
        }

        public string BirthDateDayDropdown
        {
            get
            {
                string theXpath = "//select[contains(@id, 'age-day')]";
                return ReturnTextOfSelectedValueInList(By.XPath(theXpath));
            }
            set
            {
                SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, 'age-day')]")));
                select.SelectByText(value);
            }
        }

        public string BirthDateYearTextbox
        {
            get
            {
                IWebElement element = WaitUntilElementExists(By.XPath("//input[contains(@id, 'age-year')]"));
                return element.GetAttribute("value");
            }
            set
            {
                string theXpath = "//input[contains(@id, 'age-year')]";

                WaitUntilElementValueCanBeCleared(By.XPath(theXpath));
                SendKeys(WaitUntilElementExists(By.XPath(theXpath)), value);
            }
        }

        public string CountryDropdown
        {
            get
            {
                string theXpath = "//select[contains(@id, 'ddlCountries')]";
                return ReturnTextOfSelectedValueInList(By.XPath(theXpath));
            }
            set
            {
                SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, 'ddlCountries')]")));
                select.SelectByText(value);

                // the page takes time to display the correct zip/postal/etc. code textbox
                Wait(1000);
            }
        }

        public string ZipCodeTextbox
        {
            get
            {
                IWebElement element = WaitUntilElementExists(By.XPath("//input[contains(@id, 'location-zipcode')]"));
                return element.GetAttribute("value");
            }
            set
            {
                string theXpath = "//input[contains(@id, 'location-zipcode')]";

                WaitUntilElementValueCanBeCleared(By.XPath(theXpath));
                SendKeys(WaitUntilElementExists(By.XPath(theXpath)), value);

                // the page takes time to verify the zip/postal code which sometimes skips the next few lines of code
                Wait(1000);
            }
        }

        public string CityTextbox
        {
            get
            {
                IWebElement element = WaitUntilElementExists(By.XPath("//input[contains(@id, 'location-city')]"));
                return element.GetAttribute("value");
            }
            set
            {
                string theXpath = "//input[contains(@id, 'location-city')]";

                WaitUntilElementValueCanBeCleared(By.XPath(theXpath));
                SendKeys(WaitUntilElementExists(By.XPath(theXpath)), value);

                // the page takes time to verify the zip/postal code which sometimes skips the next few lines of code
                Wait(1000);
            }
        }

        public int YouAreARadioButton
        {
            get
            {
                IWebElement element = null;

                if (driver.Url.Contains("blacksingles.com") || driver.Url.Contains("bbwpersonalsplus.com"))
                    element = WaitUntilElementExists(By.XPath("//input[contains(@id, 'gender_0')]"));
                else
                    element = WaitUntilElementExists(By.XPath("//input[contains(@id, 'gendermask_0')]"));

                if (element.Selected)
                    return 0;

                return 1;
            }
            set
            {
                IWebElement element;

                if (driver.Url.Contains("blacksingles.com") || driver.Url.Contains("bbwpersonalsplus.com"))
                {
                    if (value == 0)
                    {
                        element = WaitUntilElementExists(By.XPath("//input[contains(@id, 'gender_0')]"));
                        element.Click();
                    }
                    else
                    {
                        element = WaitUntilElementExists(By.XPath("//input[contains(@id, 'gender_1')]"));
                        element.Click();
                    }
                }
                else
                {
                    if (value == 0)
                    {
                        element = WaitUntilElementExists(By.XPath("//input[contains(@id, 'gendermask_0')]"));
                        element.Click();
                    }
                    else
                    {
                        element = WaitUntilElementExists(By.XPath("//input[contains(@id, 'gendermask_1')]"));
                        element.Click();
                    }
                }
            }
        }

        public int LookingForARadioButton
        {
            get
            {
                IWebElement element = WaitUntilElementExists(By.XPath("//input[contains(@id, 'seekingmask_0')]"));
                if (element.Selected)
                    return 0;

                return 1;
            }
            set
            {
                IWebElement element;

                if (value == 0)
                {
                    element = WaitUntilElementExists(By.XPath("//input[contains(@id, 'seekingmask_0')]"));
                    element.Click();
                }
                else
                {
                    element = WaitUntilElementExists(By.XPath("//input[contains(@id, 'seekingmask_1')]"));
                    element.Click();
                }
            }
        }

        public string ForList
        {
            get
            {
                string theXpath = "//select[contains(@id, 'lookingfor')]";
                return ReturnTextOfSelectedValueInList(By.XPath(theXpath));
            }
            set
            {
                SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, 'lookingfor')]")));
                IList<IWebElement> options = select.AllSelectedOptions;

                // deselect all before selecting value
                foreach (IWebElement option in options)
                {
                    select.DeselectByText(option.Text);
                }

                select.SelectByText(value);
            }
        }

        public string RelationshipStatusDropdown
        {
            get
            {
                string theXpath = "//select[contains(@id, 'MaritalStatus')]";
                return ReturnTextOfSelectedValueInList(By.XPath(theXpath));
            }
            set
            {
                SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, 'MaritalStatus')]")));
                select.SelectByText(value);
            }
        }

        public string AboutMeTextbox
        {
            get
            {
                IWebElement element = WaitUntilElementExists(By.XPath("//textarea[contains(@id, 'AboutMe')]"));
                return element.GetAttribute("value");
            }
            set
            {
                string theXpath = "//textarea[contains(@id, 'AboutMe')]";

                WaitUntilElementValueCanBeCleared(By.XPath(theXpath));
                SendKeys(WaitUntilElementExists(By.XPath(theXpath)), value);
            }
        }

        public string HeightDropdown
        {
            get
            {
                string theXpath = "//select[contains(@id, 'height')]";
                return ReturnTextOfSelectedValueInList(By.XPath(theXpath));
            }
            set
            {
                SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, 'height')]")));
                select.SelectByText(value);
            }
        }

        public string IWeighDropdown
        {
            get
            {
                string theXpath = "//select[contains(@id, 'weight')]";
                return ReturnTextOfSelectedValueInList(By.XPath(theXpath));
            }
            set
            {
                SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, 'weight')]")));
                select.SelectByText(value);
            }
        }

        public string MyHairIsDropdown
        {
            get
            {
                string theXpath = "//select[contains(@id, 'HairColor')]";
                return ReturnTextOfSelectedValueInList(By.XPath(theXpath));
            }
            set
            {
                SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, 'HairColor')]")));
                select.SelectByText(value);
            }
        }

        public string MyEyesAreDropdown
        {
            get
            {
                string theXpath = "//select[contains(@id, 'EyeColor')]";
                return ReturnTextOfSelectedValueInList(By.XPath(theXpath));
            }
            set
            {
                SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, 'EyeColor')]")));
                select.SelectByText(value);
            }
        }

        public string BodyStyleDropdown
        {
            get
            {
                string theXpath = "//select[contains(@id, 'BodyType')]";
                return ReturnTextOfSelectedValueInList(By.XPath(theXpath));
            }
            set
            {
                SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, 'BodyType')]")));
                select.SelectByText(value);
            }
        }

        public string RelocationDropdown
        {
            get
            {
                string theXpath = "//select[contains(@id, 'relocateFlag')]";
                return ReturnTextOfSelectedValueInList(By.XPath(theXpath));
            }
            set
            {
                SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, 'relocateFlag')]")));
                select.SelectByText(value);
            }
        }

        public string HaveKidsDropdown
        {
            get
            {
                string theXpath = "//select[contains(@id, 'ChildrenCount')]";
                return ReturnTextOfSelectedValueInList(By.XPath(theXpath));
            }
            set
            {
                SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, 'ChildrenCount')]")));
                select.SelectByText(value);
            }
        }

        /// <summary>
        /// Spark.com
        /// </summary>
        public string HasKidsList_ImLookingForTab
        {
            get
            {
                string theXpath = "//select[contains(@id, 'DesiredChildrenCount')]";
                return ReturnTextOfSelectedValueInList(By.XPath(theXpath));
            }
            set
            {
                SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, 'DesiredChildrenCount')]")));
                IList<IWebElement> options = select.AllSelectedOptions;

                // deselect all before selecting value
                foreach (IWebElement option in options)
                {
                    select.DeselectByText(option.Text);
                }

                select.SelectByText(value);
            }
        }

        public string CustodyDropdown
        {
            get
            {
                string theXpath = "//select[contains(@id, 'Custody')]";
                return ReturnTextOfSelectedValueInList(By.XPath(theXpath));
            }
            set
            {
                SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, 'Custody')]")));
                select.SelectByText(value);
            }
        }

        public int WantKidsRadioButton
        {
            get
            {
                // if nothing is selected return a default of 2 - Not Sure
                IWebElement element1 = WaitUntilElementExists(By.XPath("//input[contains(@id, 'MoreChildrenFlag_0')]"));
                IWebElement element2 = WaitUntilElementExists(By.XPath("//input[contains(@id, 'MoreChildrenFlag_1')]"));

                if (element1.Selected)
                    return 0;
                else if (element2.Selected)
                    return 1;

                return 2;
            }
            set
            {
                IWebElement element;

                if (value == 0)
                {
                    element = WaitUntilElementExists(By.XPath("//input[contains(@id, 'MoreChildrenFlag_0')]"));
                    element.Click();
                }
                else if (value == 1)
                {
                    element = WaitUntilElementExists(By.XPath("//input[contains(@id, 'MoreChildrenFlag_1')]"));
                    element.Click();
                }
                else
                {
                    element = WaitUntilElementExists(By.XPath("//input[contains(@id, 'MoreChildrenFlag_2')]"));
                    element.Click();
                }
            }
        }

        /// <summary>
        /// Spark.com
        /// </summary>
        public int WantsKidsRadioButton_ImLookingForTab
        {
            get
            {
                // if nothing is selected return a default of 2 - Not Sure
                IWebElement element1 = WaitUntilElementExists(By.XPath("//input[contains(@id, 'DesiredMoreChildrenFlag_0')]"));
                IWebElement element2 = WaitUntilElementExists(By.XPath("//input[contains(@id, 'DesiredMoreChildrenFlag_1')]"));

                if (element1.Selected)
                    return 0;
                else if (element2.Selected)
                    return 1;

                return 2;
            }
            set
            {
                IWebElement element;

                if (value == 0)
                {
                    element = WaitUntilElementExists(By.XPath("//input[contains(@id, 'DesiredMoreChildrenFlag_0')]"));
                    element.Click();
                }
                else if (value == 1)
                {
                    element = WaitUntilElementExists(By.XPath("//input[contains(@id, 'DesiredMoreChildrenFlag_1')]"));
                    element.Click();
                }
                else
                {
                    element = WaitUntilElementExists(By.XPath("//input[contains(@id, 'DesiredMoreChildrenFlag_2')]"));
                    element.Click();
                }
            }
        }

        public string IKeepKosherDropdown
        {
            get
            {
                string theXpath = "//select[contains(@id, 'KeepKosher')]";
                return ReturnTextOfSelectedValueInList(By.XPath(theXpath));
            }
            set
            {
                SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, 'KeepKosher')]")));
                select.SelectByText(value);
            }
        }

        public string IGoToSynagogueDropdown
        {
            get
            {
                string theXpath = "//select[contains(@id, 'SynagogueAttendance')]";
                return ReturnTextOfSelectedValueInList(By.XPath(theXpath));
            }
            set
            {
                SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, 'SynagogueAttendance')]")));
                select.SelectByText(value);
            }
        }

        public string ISmokeDropdown
        {
            get
            {
                string theXpath = "//select[contains(@id, 'SmokingHabits')]";
                return ReturnTextOfSelectedValueInList(By.XPath(theXpath));
            }
            set
            {
                SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, 'SmokingHabits')]")));
                select.SelectByText(value);
            }
        }

        public string IDrinkDropdown
        {
            get
            {
                string theXpath = "//select[contains(@id, 'DrinkingHabits')]";
                return ReturnTextOfSelectedValueInList(By.XPath(theXpath));
            }
            set
            {
                SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, 'DrinkingHabits')]")));
                select.SelectByText(value);
            }
        }

        public string ZodiacSignDropdown
        {
            get
            {
                string theXpath = "//select[contains(@id, 'Zodiac')]";
                return ReturnTextOfSelectedValueInList(By.XPath(theXpath));
            }
            set
            {
                SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, 'Zodiac')]")));
                select.SelectByText(value);
            }
        }

        public string ActivityLevelDropdown
        {
            get
            {
                string theXpath = "//select[contains(@id, 'ActivityLevel')]";
                return ReturnTextOfSelectedValueInList(By.XPath(theXpath));
            }
            set
            {
                SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, 'ActivityLevel')]")));
                select.SelectByText(value);
            }
        }

        public string IGrewUpInTextbox
        {
            get
            {
                IWebElement element = WaitUntilElementExists(By.XPath("//input[contains(@id, 'GrewUpIn')]"));
                return element.GetAttribute("value");
            }
            set
            {
                string theXpath = "//input[contains(@id, 'GrewUpIn')]";

                WaitUntilElementValueCanBeCleared(By.XPath(theXpath));
                SendKeys(WaitUntilElementExists(By.XPath(theXpath)), value);
            }
        }

        public string MyEthnicityIsDropdown
        {
            get
            {
                string theXpath = "//select[contains(@id, 'Ethnicity')]";
                return ReturnTextOfSelectedValueInList(By.XPath(theXpath));
            }
            set
            {
                SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, 'Ethnicity')]")));
                select.SelectByText(value);
            }
        }

        public string MyEthnicityIsList
        {
            get
            {
                string theXpath = "//select[contains(@id, 'Ethnicity')]";
                return ReturnTextOfSelectedValueInList(By.XPath(theXpath));
            }
            set
            {
                SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, 'Ethnicity')]")));
                IList<IWebElement> options = select.AllSelectedOptions;

                // deselect all before selecting value
                foreach (IWebElement option in options)
                {
                    select.DeselectByText(option.Text);
                }

                select.SelectByText(value);
            }
        }

        public string MyEthnicityIs_JDateOnly_Dropdown
        {
            get
            {
                string theXpath = "//select[contains(@id, 'JDateEthnicity')]";
                return ReturnTextOfSelectedValueInList(By.XPath(theXpath));
            }
            set
            {
                SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, 'JDateEthnicity')]")));
                select.SelectByText(value);
            }
        }

        public string ISpeakList
        {
            get
            {
                string theXpath = "//select[contains(@id, 'LanguageMask')]";
                return ReturnTextOfSelectedValueInList(By.XPath(theXpath));
            }
            set
            {
                SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, 'LanguageMask')]")));
                IList<IWebElement> options = select.AllSelectedOptions;

                // deselect all before selecting value
                foreach (IWebElement option in options)
                {
                    select.DeselectByText(option.Text);
                }

                select.SelectByText(value);
            }
        }

        public string IStudiedOrAmInterestedInTextbox
        {
            get
            {
                IWebElement element = WaitUntilElementExists(By.XPath("//input[contains(@id, 'StudiesEmphasis')]"));
                return element.GetAttribute("value");
            }
            set
            {
                string theXpath = "//input[contains(@id, 'StudiesEmphasis')]";

                WaitUntilElementValueCanBeCleared(By.XPath(theXpath));
                SendKeys(WaitUntilElementExists(By.XPath(theXpath)), value);
            }
        }

        public string EducationDropdown
        {
            get
            {
                string theXpath = "//select[contains(@id, 'EducationLevel')]";
                return ReturnTextOfSelectedValueInList(By.XPath(theXpath));
            }
            set
            {
                SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, 'EducationLevel')]")));
                select.SelectByText(value);
            }
        }

        /// <summary>
        /// Spark.com only
        /// </summary>
        public string EducationDropdown_WhoIAmTab
        {
            get
            {
                string theXpath = "//select[contains(@id, 'ctl9_EducationLevel')]";
                return ReturnTextOfSelectedValueInList(By.XPath(theXpath));
            }
            set
            {
                SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, 'ctl9_EducationLevel')]")));
                select.SelectByText(value);
            }
        }

        public string OccupationDropdown
        {
            get
            {
                string theXpath = "//select[contains(@id, 'IndustryType')]";
                return ReturnTextOfSelectedValueInList(By.XPath(theXpath));
            }
            set
            {
                SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, 'IndustryType')]")));
                select.SelectByText(value);
            }
        }

        /// <summary>
        /// Spark.com only
        /// </summary>
        public string OccupationDropdown_WhoIAmTab
        {
            get
            {
                string theXpath = "//select[contains(@id, 'ctl11_IndustryType')]";
                return ReturnTextOfSelectedValueInList(By.XPath(theXpath));
            }
            set
            {
                SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, 'ctl11_IndustryType')]")));
                select.SelectByText(value);
            }
        }

        public string OccupationDescriptionTextbox
        {
            get
            {
                IWebElement element = WaitUntilElementExists(By.XPath("//input[contains(@id, 'OccupationDescription')]"));
                return element.GetAttribute("value");
            }
            set
            {
                string theXpath = "//input[contains(@id, 'OccupationDescription')]";

                WaitUntilElementValueCanBeCleared(By.XPath(theXpath));
                SendKeys(WaitUntilElementExists(By.XPath(theXpath)), value);
            }
        }

        public string AnnualIncomeDropdown
        {
            get
            {
                string theXpath = "//select[contains(@id, 'IncomeLevel')]";
                return ReturnTextOfSelectedValueInList(By.XPath(theXpath));
            }
            set
            {
                SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, 'IncomeLevel')]")));
                select.SelectByText(value);
            }
        }

        public string AnnualIncomeDropdown_WhoIAmTab
        {
            get
            {
                string theXpath = "//select[contains(@id, 'ctl13_IncomeLevel')]";
                return ReturnTextOfSelectedValueInList(By.XPath(theXpath));
            }
            set
            {
                SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, 'ctl13_IncomeLevel')]")));
                select.SelectByText(value);
            }
        }

        public string PoliticalOrientationDropdown
        {
            get
            {
                string theXpath = "//select[contains(@id, 'PoliticalOrientation')]";
                return ReturnTextOfSelectedValueInList(By.XPath(theXpath));
            }
            set
            {
                SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, 'PoliticalOrientation')]")));
                select.SelectByText(value);
            }
        }

        public string WhenGoingSomewhereDropdown
        {
            get
            {
                string theXpath = "//select[contains(@id, 'Timeliness')]";
                return ReturnTextOfSelectedValueInList(By.XPath(theXpath));
            }
            set
            {
                SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, 'Timeliness')]")));

                if (driver.Url.Contains("bbwpersonalsplus.com"))
                {
                    IList<IWebElement> options = select.AllSelectedOptions;

                    // deselect all before selecting value
                    foreach (IWebElement option in options)
                    {
                        select.DeselectByText(option.Text);
                    }
                }

                select.SelectByText(value);
            }
        }

        public string FashionSenseDropdown
        {
            get
            {
                string theXpath = "//select[contains(@id, 'Fashion')]";
                return ReturnTextOfSelectedValueInList(By.XPath(theXpath));
            }
            set
            {
                SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, 'Fashion')]")));

                if (driver.Url.Contains("bbwpersonalsplus.com"))
                {
                    IList<IWebElement> options = select.AllSelectedOptions;

                    // deselect all before selecting value
                    foreach (IWebElement option in options)
                    {
                        select.DeselectByText(option.Text);
                    }
                }

                select.SelectByText(value);
            }
        }

        public string ReligionDropdown
        {
            get
            {
                string theXpath = "//select[contains(@id, 'eligion')]";
                return ReturnTextOfSelectedValueInList(By.XPath(theXpath));
            }
            set
            {
                SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, 'eligion')]")));
                select.SelectByText(value);
            }
        }

        public string MyPersonalityIsBestDescribedAsList
        {
            get
            {
                string theXpath = "//select[contains(@id, 'PersonalityTrait')]";
                return ReturnTextOfSelectedValueInList(By.XPath(theXpath));
            }
            set
            {
                SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, 'PersonalityTrait')]")));
                IList<IWebElement> options = select.AllSelectedOptions;

                // deselect all before selecting value
                foreach (IWebElement option in options)
                {
                    select.DeselectByText(option.Text);
                }

                select.SelectByText(value);
            }
        }

        public string InMyFreeTimeIEnjoyList
        {
            get
            {
                string theXpath = "//select[contains(@id, 'LeisureActivity')]";
                return ReturnTextOfSelectedValueInList(By.XPath(theXpath));
            }
            set
            {
                SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, 'LeisureActivity')]")));
                IList<IWebElement> options = select.AllSelectedOptions;

                // deselect all before selecting value
                foreach (IWebElement option in options)
                {
                    select.DeselectByText(option.Text);
                }

                select.SelectByText(value);
            }
        }

        public string InMyFreeTimeILikeToGoToList
        {
            get
            {
                string theXpath = "//select[contains(@id, 'EntertainmentLocation')]";
                return ReturnTextOfSelectedValueInList(By.XPath(theXpath));
            }
            set
            {
                SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, 'EntertainmentLocation')]")));
                IList<IWebElement> options = select.AllSelectedOptions;

                // deselect all before selecting value
                foreach (IWebElement option in options)
                {
                    select.DeselectByText(option.Text);
                }

                select.SelectByText(value);
            }
        }

        public string MyFavoritePhysicalActivitesList
        {
            get
            {
                string theXpath = "//select[contains(@id, 'PhysicalActivity')]";
                return ReturnTextOfSelectedValueInList(By.XPath(theXpath));
            }
            set
            {
                SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, 'PhysicalActivity')]")));
                IList<IWebElement> options = select.AllSelectedOptions;

                // deselect all before selecting value
                foreach (IWebElement option in options)
                {
                    select.DeselectByText(option.Text);
                }

                select.SelectByText(value);
            }
        }

        public string MyFavoriteLeisureActivitesList
        {
            get
            {
                string theXpath = "//select[contains(@id, 'LeisureActivity')]";
                return ReturnTextOfSelectedValueInList(By.XPath(theXpath));
            }
            set
            {
                SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, 'LeisureActivity')]")));
                IList<IWebElement> options = select.AllSelectedOptions;

                // deselect all before selecting value
                foreach (IWebElement option in options)
                {
                    select.DeselectByText(option.Text);
                }

                select.SelectByText(value);
            }
        }

        public string IOwnThesePetsList
        {
            get
            {
                string theXpath = "//select[contains(@id, 'Pets')]";
                return ReturnTextOfSelectedValueInList(By.XPath(theXpath));
            }
            set
            {
                SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, 'Pets')]")));
                IList<IWebElement> options = select.AllSelectedOptions;

                // deselect all before selecting value
                foreach (IWebElement option in options)
                {
                    select.DeselectByText(option.Text);
                }

                select.SelectByText(value);
            }
        }

        public string MyFavoriteFoodsList
        {
            get
            {
                string theXpath = "//select[contains(@id, 'Cuisine')]";
                return ReturnTextOfSelectedValueInList(By.XPath(theXpath));
            }
            set
            {
                SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, 'Cuisine')]")));
                IList<IWebElement> options = select.AllSelectedOptions;

                // deselect all before selecting value
                foreach (IWebElement option in options)
                {
                    select.DeselectByText(option.Text);
                }

                select.SelectByText(value);
            }
        }

        public string MyFavoriteMusicList
        {
            get
            {
                string theXpath = "//select[contains(@id, 'Music')]";
                return ReturnTextOfSelectedValueInList(By.XPath(theXpath));
            }
            set
            {
                SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, 'Music')]")));
                IList<IWebElement> options = select.AllSelectedOptions;

                // deselect all before selecting value
                foreach (IWebElement option in options)
                {
                    select.DeselectByText(option.Text);
                }

                select.SelectByText(value);
            }
        }

        public string ILikeToReadList
        {
            get
            {
                string theXpath = "//select[contains(@id, 'Reading')]";
                return ReturnTextOfSelectedValueInList(By.XPath(theXpath));
            }
            set
            {
                SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, 'Reading')]")));
                IList<IWebElement> options = select.AllSelectedOptions;

                // deselect all before selecting value
                foreach (IWebElement option in options)
                {
                    select.DeselectByText(option.Text);
                }

                select.SelectByText(value);
            }
        }

        public string MyIdealRelationshipTextArea
        {
            get
            {
                IWebElement element = WaitUntilElementExists(By.XPath("//textarea[contains(@id, 'IdealRelationshipEssay')]"));
                return element.GetAttribute("value");
            }
            set
            {
                string theXpath = "//textarea[contains(@id, 'IdealRelationshipEssay')]";

                WaitUntilElementValueCanBeCleared(By.XPath(theXpath));
                SendKeys(WaitUntilElementExists(By.XPath(theXpath)), value);

                // the page takes time to verify the zip/postal code which sometimes skips the next few lines of code
                Wait(1000);
            }
        }

        /// <summary>
        /// Spark.com only
        /// </summary>
        public string MyIdealRelationshipTextArea_ImLookingForTab
        {
            get
            {
                IWebElement element = WaitUntilElementExists(By.XPath("//textarea[contains(@id, 'RepeaterSections__ctl2_RepeaterSectionItems__ctl1_IdealRelationshipEssay')]"));
                return element.GetAttribute("value");
            }
            set
            {
                string theXpath = "//textarea[contains(@id, 'RepeaterSections__ctl2_RepeaterSectionItems__ctl1_IdealRelationshipEssay')]";

                WaitUntilElementValueCanBeCleared(By.XPath(theXpath));
                SendKeys(WaitUntilElementExists(By.XPath(theXpath)), value);

                // the page takes time to verify the zip/postal code which sometimes skips the next few lines of code
                Wait(1000);
            }
        }

        public string MyPastRelationshipsTextArea
        {
            get
            {
                IWebElement element = WaitUntilElementExists(By.XPath("//textarea[contains(@id, 'LearnFromThePastEssay')]"));
                return element.GetAttribute("value");
            }
            set
            {
                string theXpath = "//textarea[contains(@id, 'LearnFromThePastEssay')]";

                WaitUntilElementValueCanBeCleared(By.XPath(theXpath));
                SendKeys(WaitUntilElementExists(By.XPath(theXpath)), value);

                // the page takes time to verify the zip/postal code which sometimes skips the next few lines of code
                Wait(1000);
            }
        }

        /// <summary>
        /// Spark.com only
        /// </summary>
        public string MyPastRelationshipsTextArea_ImLookingForTab
        {
            get
            {
                IWebElement element = WaitUntilElementExists(By.XPath("//textarea[contains(@id, 'RepeaterSections__ctl3_RepeaterSectionItems__ctl1_LearnFromThePastEssay')]"));
                return element.GetAttribute("value");
            }
            set
            {
                string theXpath = "//textarea[contains(@id, 'RepeaterSections__ctl3_RepeaterSectionItems__ctl1_LearnFromThePastEssay')]";

                WaitUntilElementValueCanBeCleared(By.XPath(theXpath));
                SendKeys(WaitUntilElementExists(By.XPath(theXpath)), value);

                // the page takes time to verify the zip/postal code which sometimes skips the next few lines of code
                Wait(1000);
            }
        }

        public string IAmLookingForATextArea
        {
            get
            {
                IWebElement element = WaitUntilElementExists(By.XPath("//textarea[contains(@id, 'PerfectMatchEssay')]"));
                return element.GetAttribute("value");
            }
            set
            {
                string theXpath = "//textarea[contains(@id, 'PerfectMatchEssay')]";

                WaitUntilElementValueCanBeCleared(By.XPath(theXpath));
                SendKeys(WaitUntilElementExists(By.XPath(theXpath)), value);

                // the page takes time to verify the zip/postal code which sometimes skips the next few lines of code
                Wait(1000);
            }
        }

        /// <summary>
        /// Spark.com only
        /// </summary>
        public string IAmLookingForATextArea_ImLookingForTab
        {
            get
            {
                IWebElement element = WaitUntilElementExists(By.XPath("//textarea[contains(@id, 'RepeaterSections__ctl0_RepeaterSectionItems__ctl1_PerfectMatchEssay')]"));
                return element.GetAttribute("value");
            }
            set
            {
                string theXpath = "//textarea[contains(@id, 'RepeaterSections__ctl0_RepeaterSectionItems__ctl1_PerfectMatchEssay')]";

                WaitUntilElementValueCanBeCleared(By.XPath(theXpath));
                SendKeys(WaitUntilElementExists(By.XPath(theXpath)), value);

                // the page takes time to verify the zip/postal code which sometimes skips the next few lines of code
                Wait(1000);
            }
        }

        public string MyPerfectFirstDateTextArea
        {
            get
            {
                IWebElement element = WaitUntilElementExists(By.XPath("//textarea[contains(@id, 'PerfectFirstDateEssay')]"));
                return element.GetAttribute("value");
            }
            set
            {
                string theXpath = "//textarea[contains(@id, 'PerfectFirstDateEssay')]";

                WaitUntilElementValueCanBeCleared(By.XPath(theXpath));
                SendKeys(WaitUntilElementExists(By.XPath(theXpath)), value);

                // the page takes time to verify the zip/postal code which sometimes skips the next few lines of code
                Wait(1000);
            }
        }

        /// <summary>
        /// Spark.com only
        /// </summary>
        public string MyPerfectFirstDateTextArea_ImLookingForTab
        {
            get
            {
                IWebElement element = WaitUntilElementExists(By.XPath("//textarea[contains(@id, 'RepeaterSections__ctl1_RepeaterSectionItems__ctl1_PerfectFirstDateEssay')]"));
                return element.GetAttribute("value");
            }
            set
            {
                string theXpath = "//textarea[contains(@id, 'RepeaterSections__ctl1_RepeaterSectionItems__ctl1_PerfectFirstDateEssay')]";

                WaitUntilElementValueCanBeCleared(By.XPath(theXpath));
                SendKeys(WaitUntilElementExists(By.XPath(theXpath)), value);

                // the page takes time to verify the zip/postal code which sometimes skips the next few lines of code
                Wait(1000);
            }
        }

        public string AgeRange_FromTextbox
        {
            get
            {
                IWebElement element = WaitUntilElementExists(By.XPath("//input[contains(@id, 'DesiredMinAge-minAge')]"));
                return element.GetAttribute("value");
            }
            set
            {
                string theXpath = "//input[contains(@id, 'DesiredMinAge-minAge')]";

                WaitUntilElementValueCanBeCleared(By.XPath(theXpath));
                SendKeys(WaitUntilElementExists(By.XPath(theXpath)), value);

                // the page takes time to verify the zip/postal code which sometimes skips the next few lines of code
                Wait(1000);
            }
        }

        public string AgeRange_ToTextbox
        {
            get
            {
                IWebElement element = WaitUntilElementExists(By.XPath("//input[contains(@id, 'DesiredMinAge-maxAge')]"));
                return element.GetAttribute("value");
            }
            set
            {
                string theXpath = "//input[contains(@id, 'DesiredMinAge-maxAge')]";

                WaitUntilElementValueCanBeCleared(By.XPath(theXpath));
                SendKeys(WaitUntilElementExists(By.XPath(theXpath)), value);

                // the page takes time to verify the zip/postal code which sometimes skips the next few lines of code
                Wait(1000);
            }
        }

        public string RelationshipStatusList
        {
            get
            {
                string theXpath = "//select[contains(@id, 'DesiredMaritalStatus')]";
                return ReturnTextOfSelectedValueInList(By.XPath(theXpath));
            }
            set
            {
                SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, 'DesiredMaritalStatus')]")));
                IList<IWebElement> options = select.AllSelectedOptions;

                // deselect all before selecting value
                foreach (IWebElement option in options)
                {
                    select.DeselectByText(option.Text);
                }

                select.SelectByText(value);
            }
        }

        public string ReligiousBackgroundList_JDate
        {
            get
            {
                string theXpath = "//select[contains(@id, 'DesiredJDateReligion')]";
                return ReturnTextOfSelectedValueInList(By.XPath(theXpath));
            }
            set
            {
                SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, 'DesiredJDateReligion')]")));
                IList<IWebElement> options = select.AllSelectedOptions;

                // deselect all before selecting value
                foreach (IWebElement option in options)
                {
                    select.DeselectByText(option.Text);
                }

                select.SelectByText(value);
            }
        }

        public string ReligiousBackgroundList
        {
            get
            {
                string theXpath = "//select[contains(@id, 'DesiredReligious')]";
                return ReturnTextOfSelectedValueInList(By.XPath(theXpath));
            }
            set
            {
                SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, 'DesiredReligious')]")));
                IList<IWebElement> options = select.AllSelectedOptions;

                // deselect all before selecting value
                foreach (IWebElement option in options)
                {
                    select.DeselectByText(option.Text);
                }

                select.SelectByText(value);
            }
        }

        public string EducationLevelList
        {
            get
            {
                string theXpath = "//select[contains(@id, 'DesiredEducationLevel')]";
                return ReturnTextOfSelectedValueInList(By.XPath(theXpath));
            }
            set
            {
                SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, 'DesiredEducationLevel')]")));
                IList<IWebElement> options = select.AllSelectedOptions;

                // deselect all before selecting value
                foreach (IWebElement option in options)
                {
                    select.DeselectByText(option.Text);
                }

                select.SelectByText(value);
            }
        }

        public string DrinkingHabitsList
        {
            get
            {
                string theXpath = "//select[contains(@id, 'DesiredDrinkingHabits')]";
                return ReturnTextOfSelectedValueInList(By.XPath(theXpath));
            }
            set
            {
                SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, 'DesiredDrinkingHabits')]")));
                IList<IWebElement> options = select.AllSelectedOptions;

                // deselect all before selecting value
                foreach (IWebElement option in options)
                {
                    select.DeselectByText(option.Text);
                }

                select.SelectByText(value);
            }
        }

        public string SmokingHabitsList
        {
            get
            {
                string theXpath = "//select[contains(@id, 'DesiredSmokingHabits')]";
                return ReturnTextOfSelectedValueInList(By.XPath(theXpath));
            }
            set
            {
                SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, 'DesiredSmokingHabits')]")));
                IList<IWebElement> options = select.AllSelectedOptions;

                // deselect all before selecting value
                foreach (IWebElement option in options)
                {
                    select.DeselectByText(option.Text);
                }

                select.SelectByText(value);
            }
        }

        public Framework.Site_Pages.ManageYourPhotos ManagePhotosButton_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//a[contains(@id, 'PhotoTab.ascx_lnkManagePhotos')]"));
            ClickUntilURLChanges(element);

            return new ManageYourPhotos(driver);
        }

        public Framework.Site_Pages.ManageYourPhotos EditPhotoButton_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//a[contains(@id, 'PhotoTab.ascx_lnkEditPhotos')]"));
            element.Click();

            return new ManageYourPhotos(driver);
        }

        public string SelfDescriptionTextbox
        {
            get
            {
                IWebElement element = WaitUntilElementExists(By.XPath("//input[contains(@id, 'selfdescription')]"));
                return element.GetAttribute("value");
            }
            set
            {
                string theXpath = "//input[contains(@id, 'selfdescription')]";

                WaitUntilElementValueCanBeCleared(By.XPath(theXpath));
                SendKeys(WaitUntilElementExists(By.XPath(theXpath)), value);
            }
        }

        public string FavoriteBandsAndMusiciansTextbox
        {
            get
            {
                IWebElement element = WaitUntilElementExists(By.XPath("//input[contains(@id, 'FavoriteBands')]"));
                return element.GetAttribute("value");
            }
            set
            {
                string theXpath = "//input[contains(@id, 'FavoriteBands')]";

                WaitUntilElementValueCanBeCleared(By.XPath(theXpath));
                SendKeys(WaitUntilElementExists(By.XPath(theXpath)), value);
            }
        }

        public string FavoriteTypesOfMoviesList
        {
            get
            {
                string theXpath = "//select[contains(@id, 'MovieGenreMask')]";
                return ReturnTextOfSelectedValueInList(By.XPath(theXpath));
            }
            set
            {
                SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, 'MovieGenreMask')]")));
                IList<IWebElement> options = select.AllSelectedOptions;

                // deselect all before selecting value
                foreach (IWebElement option in options)
                {
                    select.DeselectByText(option.Text);
                }

                select.SelectByText(value);
            }
        }

        public string FavoriteMoviesAndActorsTextbox
        {
            get
            {
                IWebElement element = WaitUntilElementExists(By.XPath("//input[contains(@id, 'FavoriteMovies')]"));
                return element.GetAttribute("value");
            }
            set
            {
                string theXpath = "//input[contains(@id, 'FavoriteMovies')]";

                WaitUntilElementValueCanBeCleared(By.XPath(theXpath));
                SendKeys(WaitUntilElementExists(By.XPath(theXpath)), value);
            }
        }

        public string FavoriteTVShowsTextbox
        {
            get
            {
                IWebElement element = WaitUntilElementExists(By.XPath("//input[contains(@id, 'FavoriteTV')]"));
                return element.GetAttribute("value");
            }
            set
            {
                string theXpath = "//input[contains(@id, 'FavoriteTV')]";

                WaitUntilElementValueCanBeCleared(By.XPath(theXpath));
                SendKeys(WaitUntilElementExists(By.XPath(theXpath)), value);
            }
        }

        public string MyIdeaOfAGreatTripTextbox
        {
            get
            {
                IWebElement element = WaitUntilElementExists(By.XPath("//input[contains(@id, 'MyGreatTripIdea')]"));
                return element.GetAttribute("value");
            }
            set
            {
                string theXpath = "//input[contains(@id, 'MyGreatTripIdea')]";

                WaitUntilElementValueCanBeCleared(By.XPath(theXpath));
                SendKeys(WaitUntilElementExists(By.XPath(theXpath)), value);
            }
        }

        public string FavoriteRestaurantsTextbox
        {
            get
            {
                IWebElement element = WaitUntilElementExists(By.XPath("//input[contains(@id, 'FavoriteRestaurant')]"));
                return element.GetAttribute("value");
            }
            set
            {
                string theXpath = "//input[contains(@id, 'FavoriteRestaurant')]";

                WaitUntilElementValueCanBeCleared(By.XPath(theXpath));
                SendKeys(WaitUntilElementExists(By.XPath(theXpath)), value);
            }
        }

        public string SchoolsAttendedTextbox
        {
            get
            {
                IWebElement element = WaitUntilElementExists(By.XPath("//input[contains(@id, 'SchoolsAttended')]"));
                return element.GetAttribute("value");
            }
            set
            {
                string theXpath = "//input[contains(@id, 'SchoolsAttended')]";

                WaitUntilElementValueCanBeCleared(By.XPath(theXpath));
                SendKeys(WaitUntilElementExists(By.XPath(theXpath)), value);
            }
        }

        public string WhatWouldILikeToDoOnAFirstDateTextarea
        {
            get
            {
                IWebElement element = WaitUntilElementExists(By.XPath("//textarea[contains(@id, '__ctl1_ESSAYFIRSTDATE')]"));
                return element.Text;
            }
            set
            {
                string theXpath = "//textarea[contains(@id, '__ctl1_ESSAYFIRSTDATE')]";

                WaitUntilElementValueCanBeCleared(By.XPath(theXpath));
                SendKeys(WaitUntilElementExists(By.XPath(theXpath)), value);
            }
        }

        public string WhyYouShouldGetToKnowMeTextarea
        {
            get
            {
                IWebElement element = WaitUntilElementExists(By.XPath("//textarea[contains(@id, '__ctl2_ESSAYGETTOKNOWME')]"));
                return element.Text;
            }
            set
            {
                string theXpath = "//textarea[contains(@id, '__ctl2_ESSAYGETTOKNOWME')]";

                WaitUntilElementValueCanBeCleared(By.XPath(theXpath));
                SendKeys(WaitUntilElementExists(By.XPath(theXpath)), value);
            }
        }

        public string WhatGoodThingsHavePastRelationshipsTaughtMeTextarea
        {
            get
            {
                IWebElement element = WaitUntilElementExists(By.XPath("//textarea[contains(@id, '__ctl3_ESSAYPASTRELATIONSHIP')]"));
                return element.Text;
            }
            set
            {
                string theXpath = "//textarea[contains(@id, '__ctl3_ESSAYPASTRELATIONSHIP')]";

                WaitUntilElementValueCanBeCleared(By.XPath(theXpath));
                SendKeys(WaitUntilElementExists(By.XPath(theXpath)), value);
            }
        }

        public string SomeOfTheMostImportantThingsInMyLifeAreTextarea
        {
            get
            {
                IWebElement element = WaitUntilElementExists(By.XPath("//textarea[contains(@id, '__ctl4_ESSAYIMPORTANTTHINGS')]"));
                return element.Text;
            }
            set
            {
                string theXpath = "//textarea[contains(@id, '__ctl4_ESSAYIMPORTANTTHINGS')]";

                WaitUntilElementValueCanBeCleared(By.XPath(theXpath));
                SendKeys(WaitUntilElementExists(By.XPath(theXpath)), value);
            }
        }

        public string HowWouldIDescribeMyPerfectDayTextarea
        {
            get
            {
                IWebElement element = WaitUntilElementExists(By.XPath("//textarea[contains(@id, '__ctl5_ESSAYPERFECTDAY')]"));
                return element.Text;
            }
            set
            {
                string theXpath = "//textarea[contains(@id, '__ctl5_ESSAYPERFECTDAY')]";

                WaitUntilElementValueCanBeCleared(By.XPath(theXpath));
                SendKeys(WaitUntilElementExists(By.XPath(theXpath)), value);
            }
        }

        public string AFewMoreThingsIWouldLikeToAddTextarea
        {
            get
            {
                IWebElement element = WaitUntilElementExists(By.XPath("//textarea[contains(@id, '__ctl6_ESSAYMORETHINGSTOADD')]"));
                return element.Text;
            }
            set
            {
                string theXpath = "//textarea[contains(@id, '__ctl6_ESSAYMORETHINGSTOADD')]";

                WaitUntilElementValueCanBeCleared(By.XPath(theXpath));
                SendKeys(WaitUntilElementExists(By.XPath(theXpath)), value);
            }
        }
    }
}
