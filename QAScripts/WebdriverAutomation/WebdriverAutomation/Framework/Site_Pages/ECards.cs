﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.Events;
using NUnit.Framework;
using log4net;

// ex. PAGE URL:  http://connect.jdate.com/cards/categories.html?to=alyce123&MemberID=127397770&return=1&Action=ECard&ActionCallPage=View+Profile+-+

namespace WebdriverAutomation.Framework.Site_Pages
{
    public class ECards : BasePage
    {
        public ECards(IWebDriver driver)
            : base(driver)
        {
            Header = new WebdriverAutomation.Framework.Site_Parts.Header(driver);
            Footer = new WebdriverAutomation.Framework.Site_Parts.Footer(driver);

            // verify we are on the correct page       
            IWebElement textOnPage = null;

            textOnPage = WaitUntilPageVerificationObjectExists(By.XPath("//div[@id='content-main']/h1"), "ECARDS PAGE");

            if (textOnPage.Text != (textThatVerifiesThisPageIsCorrect))
                log.Error("ERROR - ECARDS PAGE - We should be on this page but the page verification failed.");

            Assert.True(textOnPage.Text == textThatVerifiesThisPageIsCorrect, "ERROR - ECARDS PAGE - We should be on this page but the page verification failed.");

        }

        private static readonly ILog log = LogManager.GetLogger(typeof(AboutJDate));
        public WebdriverAutomation.Framework.Site_Parts.Header Header;
        public WebdriverAutomation.Framework.Site_Parts.Footer Footer;

        private const string textThatVerifiesThisPageIsCorrect = "E-Cards";

        public void MostPopularECard_ClickFirstCard()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//tr[@class='nobottom']/td/div/a/img"));
            element.Click();
        }
        
        public Framework.Site_Pages.EditECard PersonalizeAndSend_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//div[@class='card_bottom_nav']/a[2]"));
            element.Click();

            return new Framework.Site_Pages.EditECard(driver);
        }

        public string VerificationMessage()
        {
            IWebElement element = null;

            if (driver.FindElements(By.XPath("//li[@class='warning']")).Count > 0)
                element = WaitUntilElementExists(By.XPath("//li[@class='warning']"));
            else
                Assert.Fail("In the ECards test case, after user1 sends an E-Card to Subscribed user2 the 'The Card was sent to *user*' message did not appear");

            return element.Text;
        }
    }
}
