﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.Events;
using NUnit.Framework;
using log4net;

// PAGE URL:  http://www.jdate.com/Applications/Subscription/SubscriptionConfirmation.aspx?oid=1108217027&DestinationURL=http://www.jdate.com%2fApplications%2fHome%2fDefault.aspx&purchasedUpsale=true (while logged in)

namespace WebdriverAutomation.Framework.Site_Pages
{    
    public class AllAccessConfirmation : BasePage
    {
        public AllAccessConfirmation(IWebDriver driver)
            : base(driver)
        {
            // verify we are on the correct page       
            IWebElement textOnPage = null;

            textOnPage = WaitUntilPageVerificationObjectExists(By.XPath("//div[@id='content-main']/h1"), "ALL ACCESS CONFIRMATION PAGE");

            if (textOnPage.Text != (textThatVerifiesThisPageIsCorrect))
                log.Error("ERROR - ALL ACCESS CONFIRMATION PAGE - We should be on this page but the page verification failed.");

            Assert.True(textOnPage.Text == textThatVerifiesThisPageIsCorrect, "ERROR - ALL ACCESS CONFIRMATION PAGE - We should be on this page but the page verification failed.");

        }

        private static readonly ILog log = LogManager.GetLogger(typeof(Subscribe));

        private const string textThatVerifiesThisPageIsCorrect = "Congratulations on Your Subscription!";

        public Framework.Site_Pages.Home ContinueButton_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//input[contains(@id, 'ProcessAttributesAndContinue')]"));
            element.Click();

            return new Framework.Site_Pages.Home(driver);
        }

    }
}
