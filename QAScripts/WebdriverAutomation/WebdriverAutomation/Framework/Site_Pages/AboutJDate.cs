﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.Events;
using NUnit.Framework;
using log4net;

// PAGE URL:  http://www.jdate.com/Applications/Article/ArticleView.aspx?CategoryID=1998&HideNav=True

namespace WebdriverAutomation.Framework.Site_Pages
{
    public class AboutJDate : BasePage
    {
        public AboutJDate(IWebDriver driver)
            : base(driver)
        {
            Header = new WebdriverAutomation.Framework.Site_Parts.Header(driver);
            Footer = new WebdriverAutomation.Framework.Site_Parts.Footer(driver);

            Tests.ReusableCode.Ads ads = new Tests.ReusableCode.Ads();
            ads.KillAllAds(driver, log);

            // verify we are on the correct page       
            IWebElement textOnPage = null;

            textOnPage = WaitUntilPageVerificationObjectExists(By.XPath("//span[contains(@id, 'lblTitle')]"), "ABOUT JDATE PAGE");

            if (textOnPage.Text != (textThatVerifiesThisPageIsCorrect))
                log.Error("ERROR - ABOUT JDATE PAGE - We should be on this page but the page verification failed.");

            Assert.True(textOnPage.Text == textThatVerifiesThisPageIsCorrect, "ERROR - ABOUT JDATE PAGE - We should be on this page but the page verification failed.");

        }

        private static readonly ILog log = LogManager.GetLogger(typeof(AboutJDate));
        public WebdriverAutomation.Framework.Site_Parts.Header Header;
        public WebdriverAutomation.Framework.Site_Parts.Footer Footer;

        private const string textThatVerifiesThisPageIsCorrect = "About Us";

    }
}
