﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.Events;
using NUnit.Framework;
using log4net;

// PAGE URL:  http://www.jdate.com/Applications/Events/Events.aspx

namespace WebdriverAutomation.Framework.Site_Pages
{
    public class TravelAndEvents : BasePage
    {
        public TravelAndEvents(IWebDriver driver)
            : base(driver)
        {
            Header = new WebdriverAutomation.Framework.Site_Parts.Header(driver);
            Footer = new WebdriverAutomation.Framework.Site_Parts.Footer(driver);

            // verify we are on the correct page       
            IWebElement textOnPage = null;

            // switch to iFrame
            driver.SwitchTo().Frame(driver.FindElement(By.XPath("//html/body/div/form/div/div[2]/div[2]/div/div/iframe")));

            textOnPage = WaitUntilPageVerificationObjectExists(By.XPath("//html/body/div/div[2]/h1"), "TRAVEL AND EVENTS PAGE");

            if (textOnPage.Text != (textThatVerifiesThisPageIsCorrect))
                log.Error("ERROR - TRAVEL AND EVENTS PAGE - We should be on this page but the page verification failed.");

            Assert.True(textOnPage.Text == textThatVerifiesThisPageIsCorrect, "ERROR - TRAVEL AND EVENTS PAGE - We should be on this page but the page verification failed.");

            // switch back
            driver.SwitchTo().DefaultContent();
        }

        private static readonly ILog log = LogManager.GetLogger(typeof(TravelAndEvents));
        public WebdriverAutomation.Framework.Site_Parts.Header Header; 
        public WebdriverAutomation.Framework.Site_Parts.Footer Footer;

        private const string textThatVerifiesThisPageIsCorrect = "Travel & Events";



    }
}
