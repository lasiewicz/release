﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.Events;
using NUnit.Framework;
using log4net;
using System.Threading;

// PAGE URL:  https://secure.spark.net/jdatecom (while logged in)

namespace WebdriverAutomation.Framework.Site_Pages
{
    public class QuickSearch : BasePage
    {
        public QuickSearch(IWebDriver driver)
            : base(driver)
        {
            Header = new WebdriverAutomation.Framework.Site_Parts.Header(driver);
            Footer = new WebdriverAutomation.Framework.Site_Parts.Footer(driver);

            Tests.ReusableCode.Ads ads = new Tests.ReusableCode.Ads();
            ads.KillAllAds(driver, log);

            // verify we are on the correct page       
            IWebElement textOnPage = null;

            if (driver.Url.Contains("jdate.co.il"))
                textThatVerifiesThisPageIsCorrect = "חיפוש מהיר";
            else
                textThatVerifiesThisPageIsCorrect = "Quick Search";

            textOnPage = WaitUntilPageVerificationObjectExists(By.XPath("//div[@id='content-main']/div[1]/h1"), "QUICK SEARCH PAGE");

            if (textOnPage.Text != (textThatVerifiesThisPageIsCorrect))
                log.Error("ERROR - QUICK SEARCH PAGE - We should be on this page but the page verification failed.");

            Assert.True(textOnPage.Text == textThatVerifiesThisPageIsCorrect, "ERROR - QUICK SEARCH PAGE - We should be on this page but the page verification failed.");

        }

        private static readonly ILog log = LogManager.GetLogger(typeof(Subscribe));
        public WebdriverAutomation.Framework.Site_Parts.Header Header;
        public WebdriverAutomation.Framework.Site_Parts.Footer Footer;

        private string textThatVerifiesThisPageIsCorrect = string.Empty;

        public string YoureADropdown
        {
            get
            {
                string theXpath = "//select[contains(@id, 'ucQuickSearchEdit_ddlGender')]";
                return ReturnTextOfSelectedValueInList(By.XPath(theXpath));
            }
            set
            {
                SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, 'ucQuickSearchEdit_ddlGender')]")));
                select.SelectByText(value);
            }
        }

        public string SeekingADropdown
        {
            get
            {
                string theXpath = "//select[contains(@id, 'ucQuickSearchEdit_ddlSeekingGender')]";
                return ReturnTextOfSelectedValueInList(By.XPath(theXpath));
            }
            set
            {
                SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, 'ucQuickSearchEdit_ddlSeekingGender')]")));
                select.SelectByText(value);
            }
        }

        public string AgeFromTextbox
        {
            get
            {
                IWebElement element = WaitUntilElementExists(By.XPath("//input[contains(@id, 'ucQuickSearchEdit_tbAgeMin')]"));
                return element.GetAttribute("value");
            }
            set
            {
                string theXpath = "//input[contains(@id, 'ucQuickSearchEdit_tbAgeMin')]";

                WaitUntilElementValueCanBeCleared(By.XPath(theXpath));
                SendKeys(WaitUntilElementExists(By.XPath(theXpath)), value);
            }
        }

        public string AgeToTextbox
        {
            get
            {
                IWebElement element = WaitUntilElementExists(By.XPath("//input[contains(@id, 'ucQuickSearchEdit_tbAgeMax')]"));
                return element.GetAttribute("value");
            }
            set
            {
                string theXpath = "//input[contains(@id, 'ucQuickSearchEdit_tbAgeMax')]";

                WaitUntilElementValueCanBeCleared(By.XPath(theXpath));
                SendKeys(WaitUntilElementExists(By.XPath(theXpath)), value);
            }
        }

        public string LocatedWithinDropdown
        {
            get
            {
                string theXpath = "//select[contains(@id, 'ucQuickSearchEdit_ddlDistance')]";
                return ReturnTextOfSelectedValueInList(By.XPath(theXpath));
            }
            set
            {
                SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, 'ucQuickSearchEdit_ddlDistance')]")));
                select.SelectByText(value);
            }
        }

        public void EditLink_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//a[contains(@id, 'ucQuickSearchEdit_lnkLocation')]/span"));
            element.Click();
        }

        public void EditOverlay_ZipCodeTab_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//a[contains(@id, 'ucQuickSearchEdit_prSearchRegion_rptTabs__ctl1_lnkTab')]"));
            element.Click();
        }

        public string EditOverlay_ZipCodeTab_CountryDropdown
        {
            get
            {
                string theXpath = "//select[contains(@id, 'ucQuickSearchEdit_prSearchRegion_ddlPostalCodeCountry')]";
                return ReturnTextOfSelectedValueInList(By.XPath(theXpath));
            }
            set
            {
                SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, 'ucQuickSearchEdit_prSearchRegion_ddlPostalCodeCountry')]")));
                select.SelectByText(value);
            }
        }

        public string EditOverlay_ZipCodeTab_ZipCodeTextbox
        {
            get
            {
                IWebElement element = WaitUntilElementExists(By.XPath("//input[contains(@id, 'ucQuickSearchEdit_prSearchRegion_tbPostalCode')]"));
                return element.GetAttribute("value");
            }
            set
            {
                string theXpath = "//input[contains(@id, 'ucQuickSearchEdit_prSearchRegion_tbPostalCode')]";

                WaitUntilElementValueCanBeCleared(By.XPath(theXpath));
                SendKeys(WaitUntilElementExists(By.XPath(theXpath)), value);
            }
        }

        public void EditOverlay_SaveButton_Click()
        {
            //IWebElement editOverlay = null;

            IWebElement element = WaitUntilElementExists(By.XPath("//input[contains(@id, 'ucQuickSearchEdit_prSearchRegion_btnSave')]"));
            element.Click();

            //// Wait until the Edit Overlay is closed before continuing
            //for (int second = 0; ; second++)
            //{
            //    if (second >= timeoutValue_sleep) Assert.Fail("We timed out while waiting for the Edit Overlay in the Quick Search page to disappear after we clicked on the Save button.");

            //    editOverlay = driver.FindElement(By.XPath("//div[@id='shadowBoxContent']"));

            //    if (editOverlay.Text == string.Empty)
            //        break;

            //    Thread.Sleep(100);
            //}

            // sometimes randomly getting element not in cache when trying to click on Search after this.  
            Wait(2000);
        }

        public void EditOverlay_CloseLink_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//a[contains(@id, 'ucQuickSearchEdit_prSearchRegion_lnkClose')]"));
            element.Click();
        }

        public void ShowOnlyProfilesOfJewishMembersCheckbox_Check()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//input[contains(@id, 'ucQuickSearchEdit_cbJewishOnly')]"));
            element.Click();
        }

        public void ShowOnlyProfilesOfJewishMembersCheckbox_Uncheck()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//input[contains(@id, 'ucQuickSearchEdit_cbJewishOnly')]"));
            element.Click();
        }

        public void ShowProfilesWithPhotosOnlyCheckbox_Check()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//input[contains(@id, 'ucQuickSearchEdit_cbHasPhoto')]"));
            element.Click();
        }

        public void ShowProfilesWithPhotosOnlyCheckbox_Uncheck()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//input[contains(@id, 'ucQuickSearchEdit_cbHasPhoto')]"));
            element.Click();
        }

        public void SearchButton_Click()
        {
            IWebElement elementToClick = WaitUntilElementExists(By.XPath("//input[contains(@id, 'ucQuickSearchEdit_btnSave')]"));

            // gallery view
            if (driver.FindElements(By.XPath("//a[contains(@id, 'MemberRepeater__ctl0_GalleryMiniProfile_lnkUserName')]")).Count > 0)
                ClickUntilURLChanges(elementToClick);
            // list view
            else
                ClickUntilURLChanges(elementToClick);

            Tests.ReusableCode.Ads ads = new Tests.ReusableCode.Ads();
            ads.KillAllAds(driver, log);
        }

        public void SearchByNewestTab_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//a[contains(@id, 'rptSearchOption__ctl0_lnkSort')]"));
            element.Click();
        }

        public void SearchByMostActiveTab_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//a[contains(@id, 'rptSearchOption__ctl1_lnkSort')]"));
            element.Click();
        }

        public void SearchByClosestToYouTab_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//a[contains(@id, 'rptSearchOption__ctl2_lnkSort')]"));
            element.Click();
        }

        public void SearchByMostPopularTab_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//a[contains(@id, 'rptSearchOption__ctl3_lnkSort')]"));
            element.Click();
        }

        public void ListLink_Click()
        {
            IWebElement element = null;

            int maxLoopIterations = 5;

            for (int i = 0; i < maxLoopIterations; i++)
            {
                if (driver.FindElements(By.XPath("//a[contains(@id, 'MemberRepeater__ctl0_GalleryMiniProfile_lnkUserName')]")).Count > 0)
                {
                    if (driver.Url.Contains("jdate.com"))
                        element = WaitUntilElementExists(By.XPath("//a[contains(@id, 'idResultsViewType_lnkViewType')]/span[2]"));
                    else
                        element = WaitUntilElementExists(By.XPath("//a[contains(@id, 'idResultsViewType_lnkViewType')]/span"));

                    element.Click();

                    Thread.Sleep(1000);
                }
                else
                    break;

                if (i == 4)
                    Assert.Fail("We tried to click on List Link " + maxLoopIterations + " times but the List view did not appear.");
            }
        }

        public void GalleryLink_Click()
        {
            IWebElement element = null;

            int maxLoopIterations = 5;

            for (int i = 0; i < maxLoopIterations; i++)
            {
                if (driver.FindElements(By.XPath("//div[@class='results list-view']")).Count > 0)
                {
                    if (driver.Url.Contains("jdate.com"))
                        element = WaitUntilElementExists(By.XPath("//a[contains(@id, 'idResultsViewType_lnkViewType')]/span[2]"));
                    else
                        element = WaitUntilElementExists(By.XPath("//a[contains(@id, 'idResultsViewType_lnkViewType')]/span"));

                    element.Click();

                    Thread.Sleep(1000);
                }
                else
                    break;

                if (i == 4)
                    Assert.Fail("We tried to click on Gallery Link " + maxLoopIterations + " times but the Gallery view did not appear.");
            }
        }

        /// <summary>
        /// The link |< which appears after we get to search results 25-36 or higher
        /// </summary>
        /// <returns></returns>
        public QuickSearch Pagination_FirstPageLink_Click()
        {
            IWebElement element = null;

            if (driver.Url.Contains("jdate.com") || driver.Url.Contains("spark.com") || driver.Url.Contains("bbwpersonalsplus.com") || driver.Url.Contains("blacksingles.com"))
                element = WaitUntilElementExists(By.PartialLinkText("«"));
            else if (driver.Url.Contains("jdate.co.il"))
                element = WaitUntilElementExists(By.XPath("//span[contains(@id, '_lblListNavigationTop')]/a[1]"));
            else
                element = WaitUntilElementExists(By.PartialLinkText("|<"));

            ClickUntilURLChanges(element);

            return new QuickSearch(driver);
        }

        public QuickSearch Pagination_PreviousLink_Click()
        {
            IWebElement element = null;

            if (driver.Url.Contains("jdate.com"))
                element = WaitUntilElementExists(By.PartialLinkText("Prev"));
            else if (driver.Url.Contains("spark.com") || driver.Url.Contains("bbwpersonalsplus.com") || driver.Url.Contains("blacksingles.com"))
                element = WaitUntilElementExists(By.PartialLinkText("< Prev"));
            else if (driver.Url.Contains("jdate.co.il"))
                element = WaitUntilElementExists(By.PartialLinkText("« קודם"));
            else
                element = WaitUntilElementExists(By.PartialLinkText("Previous"));

            ClickUntilURLChanges(element);

            return new QuickSearch(driver);
        }

        public QuickSearch Pagination_NextLink_Click()
        {
            IWebElement element = null;

            if (driver.Url.Contains("spark.com") || driver.Url.Contains("bbwpersonalsplus.com") || driver.Url.Contains("blacksingles.com"))
                element = WaitUntilElementExists(By.PartialLinkText("Next >"));
            else if (driver.Url.Contains("jdate.co.il"))
                element = WaitUntilElementExists(By.PartialLinkText("הבא »"));
            else
                element = WaitUntilElementExists(By.PartialLinkText("Next"));

            ClickUntilURLChanges(element);

            return new QuickSearch(driver);
        }

        public QuickSearch Pagination_1To12_Click()
        {
            IWebElement element = null;

            if (driver.Url.Contains("spark.com") || driver.Url.Contains("bbwpersonalsplus.com"))
                element = WaitUntilElementExists(By.PartialLinkText("1"));
            else
                element = WaitUntilElementExists(By.PartialLinkText("1-12"));
            ClickUntilURLChanges(element);

            return new QuickSearch(driver);
        }

        public QuickSearch Pagination_13To24_Click()
        {
            IWebElement element = null;

            //if (driver.Url.Contains("spark.com") || driver.Url.Contains("bbwpersonalsplus.com"))
            //    element = WaitUntilElementExists(By.PartialLinkText("2"));
            if (driver.Url.Contains("blacksingles.com") || driver.Url.Contains("spark.com") || driver.Url.Contains("bbwpersonalsplus.com"))
            {
                element = WaitUntilElementExists(By.XPath("//div[@id='content-main']/div[1]/div/span[contains(@id, 'lblListNavigationTop')]/a[2]"));

                if (element.Text != "2")
                    element = WaitUntilElementExists(By.XPath("//div[@id='content-main']/div[1]/div/span[contains(@id, 'lblListNavigationTop')]/a[4]"));
            }
            else
                element = WaitUntilElementExists(By.PartialLinkText("13-24"));

            ClickUntilURLChanges(element);

            return new QuickSearch(driver);
        }

        public QuickSearch Pagination_25To36_Click()
        {
            IWebElement element = null;

            if (driver.Url.Contains("bbwpersonalsplus.com") || driver.Url.Contains("blacksingles.com") || driver.Url.Contains("spark.com"))
            {
                element = WaitUntilElementExists(By.XPath("//div[@id='content-main']/div[1]/div/span[contains(@id, 'lblListNavigationTop')]/a[5]"));

                if (element.Text != "3")
                    element = WaitUntilElementExists(By.XPath("//div[@id='content-main']/div[1]/div/span[contains(@id, 'lblListNavigationTop')]/a[3]"));
            }
            else
                element = WaitUntilElementExists(By.PartialLinkText("25-36"));

            ClickUntilURLChanges(element);

            return new QuickSearch(driver);
        }

        // results 1-12
        public QuickSearch Pagination_Page1_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//span[contains(@id, 'lblListNavigationTop')]/a[1]"));

            if (element.Text == "«")
                element = WaitUntilElementExists(By.XPath("//span[contains(@id, 'lblListNavigationTop')]/a[3]"));

            ClickUntilURLChanges(element);

            return new QuickSearch(driver);
        }

        // results 13-24
        public QuickSearch Pagination_Page2_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//span[contains(@id, 'lblListNavigationTop')]/a[2]"));

            if (element.Text == "<\r\nPrev")
                element = WaitUntilElementExists(By.XPath("//span[contains(@id, 'lblListNavigationTop')]/a[4]"));

            ClickUntilURLChanges(element);

            return new QuickSearch(driver);
        }

        // results 25-36
        public QuickSearch Pagination_Page3_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//span[contains(@id, 'lblListNavigationTop')]/a[3]"));

            if (element.Text == "1")
                element = WaitUntilElementExists(By.XPath("//span[contains(@id, 'lblListNavigationTop')]/a[5]"));
            else if (element.Text == "13-24")
                element = WaitUntilElementExists(By.XPath("//span[contains(@id, 'lblListNavigationTop')]/a[4]"));

            ClickUntilURLChanges(element);

            return new QuickSearch(driver);
        }

        public int SearchResultsCount_GalleryView()
        {
            IList<IWebElement> searchResults = driver.FindElements(By.XPath("//div[contains(@id, 'GalleryMiniProfile_divHistory')]"));
            return searchResults.Count;
        }

        public bool SearchResultsWithinAgeRange_GalleryView(int minAge, int maxAge)
        {
            IWebElement ageElement;
            string[] ageAndLocation;
            int ageOfCurrentProfile;
            IList<IWebElement> searchResults = driver.FindElements(By.XPath("//div[contains(@id, 'GalleryMiniProfile_divHistory')]"));

            for (int i = 0; i < searchResults.Count; i++)
            {
                if (driver.Url.Contains("jdate.co.il"))
                    ageElement = WaitUntilElementExists(By.XPath("//div[contains(@id, 'ctl" + i + "_GalleryMiniProfile_boxContainerGallery')]/div[1]/div[2]/div[4]/p[1]"));
                else
                    ageElement = WaitUntilElementExists(By.XPath("//div[contains(@id, 'ctl" + i + "_GalleryMiniProfile_boxContainerGallery')]/div[1]/div[2]/div[2]/p[1]"));

                ageAndLocation = ageElement.Text.Split(',');

                if (driver.Url.Contains("blacksingles.com"))
                    ageOfCurrentProfile = Convert.ToInt32(ageAndLocation[0].Remove(0, ageAndLocation[0].Length - 2));
                else
                    ageOfCurrentProfile = Convert.ToInt32(ageAndLocation[0]);

                if (ageOfCurrentProfile < minAge || ageOfCurrentProfile > maxAge)
                    return false;
            }

            return true;
        }

        /// <summary>
        /// This is if we expect all results to be within a single city
        /// </summary>
        /// <param name="expectedCity"></param>
        /// <returns></returns>
        public bool SearchResultsWithinExpectedCity_GalleryView(string expectedCity)
        {
            IWebElement ageElement;
            string[] ageAndLocation;
            string locationOfCurrentProfile;
            IList<IWebElement> searchResults = driver.FindElements(By.XPath("//div[contains(@id, 'GalleryMiniProfile_divHistory')]"));

            for (int i = 0; i < searchResults.Count; i++)
            {
                ageElement = WaitUntilElementExists(By.XPath("//div[contains(@id, 'ctl" + i + "_GalleryMiniProfile_boxContainerGallery')]/div[1]/div[2]/div[2]/p[1]"));
                ageAndLocation = ageElement.Text.Split(',');

                if (driver.Url.Contains("blacksingles.com"))
                    locationOfCurrentProfile = ageAndLocation[1].Remove(0, 8);
                else
                    locationOfCurrentProfile = ageAndLocation[1].Remove(0, 1);

                if (locationOfCurrentProfile != expectedCity)
                    return false;
            }

            return true;
        }

        /// <summary>
        /// This is if we expect all results to be within multiple cities
        /// </summary>
        /// <param name="expectedCity"></param>
        /// <returns></returns>
        public bool SearchResultsWithinExpectedCities_GalleryView(List<string> expectedCities)
        {
            IWebElement ageElement;
            string[] ageAndLocation;
            string locationOfCurrentProfile;
            IList<IWebElement> searchResults = driver.FindElements(By.XPath("//div[contains(@id, 'GalleryMiniProfile_divHistory')]"));

            for (int i = 0; i < searchResults.Count; i++)
            {
                ageElement = WaitUntilElementExists(By.XPath("//div[contains(@id, 'ctl" + i + "_GalleryMiniProfile_boxContainerGallery')]/div[1]/div[2]/div[2]/p[1]"));
                ageAndLocation = ageElement.Text.Split(',');

                if (driver.Url.Contains("blacksingles.com"))
                    locationOfCurrentProfile = ageAndLocation[1].Remove(0, 8);
                else
                    locationOfCurrentProfile = ageAndLocation[1].Remove(0, 1);

                if (!expectedCities.Exists(element => element == locationOfCurrentProfile))
                    return false;
            }

            return true;
        }

        public int SearchResultsCount_ListView()
        {
            IList<IWebElement> searchResults = driver.FindElements(By.XPath("//div[contains(@class, 'results list-view')]"));
            return searchResults.Count;
        }

        public bool SearchResultsWithinAgeRange_ListView(int minAge, int maxAge)
        {
            IWebElement ageElement;
            string[] ageAndLocation;
            int ageOfCurrentProfile;
            IList<IWebElement> searchResults = driver.FindElements(By.XPath("//div[contains(@class, 'results list-view')]"));

            for (int i = 0; i < searchResults.Count; i++)
            {
                if (driver.Url.Contains("jdate.co.il"))
                    ageElement = WaitUntilElementExists(By.XPath("//div[@id='content-main']/div[@id='results-container']/div[" + (i + 1) + "]/div[2]/div[5]/div[2]/p"));
                else
                    ageElement = WaitUntilElementExists(By.XPath("//div[@id='content-main']/div[@id='results-container']/div[" + (i + 1) + "]/div[2]/div[3]/div[2]/p"));

                ageAndLocation = ageElement.Text.Split(',');
                ageOfCurrentProfile = Convert.ToInt32(ageAndLocation[0]);

                if (ageOfCurrentProfile < minAge || ageOfCurrentProfile > maxAge)
                    return false;
            }

            return true;
        }

        /// <summary>
        /// This is if we expect all results to be within a single city
        /// </summary>
        /// <param name="expectedCity"></param>
        /// <returns></returns>
        public bool SearchResultsWithinExpectedCity_ListView(string expectedCity)
        {
            IWebElement cityElement;
            string[] ageAndLocation;
            string locationOfCurrentProfile;
            IList<IWebElement> searchResults = driver.FindElements(By.XPath("//div[contains(@class, 'results list-view')]"));

            for (int i = 0; i < searchResults.Count; i++)
            {
                cityElement = WaitUntilElementExists(By.XPath("//div[@id='content-main']/div[@id='results-container']/div[" + (i + 1) + "]/div[2]/div[3]/div[2]/p"));
                ageAndLocation = cityElement.Text.Split(',', '\n');
                locationOfCurrentProfile = ageAndLocation[2];

                if (locationOfCurrentProfile != expectedCity)
                    return false;
            }

            return true;
        }

        /// <summary>
        /// This is if we expect all results to be within multiple cities
        /// </summary>
        /// <param name="expectedCity"></param>
        /// <returns></returns>
        public bool SearchResultsWithinExpectedCities_ListView(List<string> expectedCities)
        {
            IWebElement cityElement;
            string[] ageAndLocation;
            string locationOfCurrentProfile;
            IList<IWebElement> searchResults = driver.FindElements(By.XPath("//div[contains(@class, 'results list-view')]"));

            for (int i = 0; i < searchResults.Count; i++)
            {
                cityElement = WaitUntilElementExists(By.XPath("//div[@id='content-main']/div[@id='results-container']/div[" + (i + 1) + "]/div[2]/div[3]/div[2]/p"));
                ageAndLocation = cityElement.Text.Split(',', '\n');
                locationOfCurrentProfile = ageAndLocation[2];

                if (!expectedCities.Exists(element => element == locationOfCurrentProfile))
                    return false;
            }

            return true;
        }

        public bool SearchResultsIAmASeekingA_ListView(string youreA, string seekingA)
        {
            IWebElement genderPreferenceElement;
            string[] ageGenderPreferenceAndLocation;
            string genderPrefernceOfSearchResult;
            string expectedGenderPrefernceOfSearchResult = string.Empty;

            if (driver.Url.Contains("jdate.com"))
                expectedGenderPrefernceOfSearchResult = seekingA + " seeking a " + youreA;
            else if (driver.Url.Contains("jdate.co.il"))
                expectedGenderPrefernceOfSearchResult = seekingA.TrimEnd() + " המחפש/ת " + youreA.TrimEnd();
            else
                expectedGenderPrefernceOfSearchResult = seekingA + " seeking " + youreA;



            IList<IWebElement> searchResults = driver.FindElements(By.XPath("//div[contains(@class, 'results list-view')]"));

            for (int i = 0; i < searchResults.Count; i++)
            {
                if (driver.Url.Contains("jdate.co.il"))
                    genderPreferenceElement = WaitUntilElementExists(By.XPath("//div[@id='content-main']/div[@id='results-container']/div[" + (i + 1) + "]/div[2]/div[5]/div[2]/p"));
                else
                    genderPreferenceElement = WaitUntilElementExists(By.XPath("//div[@id='content-main']/div[@id='results-container']/div[" + (i + 1) + "]/div[2]/div[3]/div[2]/p"));

                ageGenderPreferenceAndLocation = genderPreferenceElement.Text.Split(',', '\r');
                genderPrefernceOfSearchResult = ageGenderPreferenceAndLocation[1].Remove(0, 1);

                if (genderPrefernceOfSearchResult != expectedGenderPrefernceOfSearchResult)
                    return false;
            }

            return true;
        }

        /// <summary>
        /// Returns a list of all the names on the CURRENT PAGE only
        /// </summary>
        /// <returns></returns>
        public List<string> GetAllNamesOfSearchResultsOnThisPage()
        {
            List<string> names = new List<string>();
            IWebElement usernameElement;
            IList<IWebElement> searchResults = driver.FindElements(By.XPath("//div[contains(@id, 'GalleryMiniProfile_divHistory')]"));

            for (int i = 0; i < searchResults.Count; i++)
            {
                usernameElement = WaitUntilElementExists(By.XPath("//a[contains(@id, 'ctl" + i + "_GalleryMiniProfile_lnkUserName')]"));
                names.Add(usernameElement.Text);
            }

            return names;
        }

        /// <summary>
        /// Gets the username of the search result of the index passed in.  Index begins at 0.
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public string GetUsernameByIndex(int index)
        {
            string username = string.Empty;


            return username;
        }

        /// <summary>
        /// Our list should be a list of usernames that we took off of a previous page.  This function returns whether or not a username is found within our list
        /// of usernames.
        /// </summary>
        /// <param name="username"></param>
        /// <param name="list"></param>
        /// <returns></returns>
        public bool IsUsernameInList(string username, List<string> list)
        {

            return true;
        }

        /// <summary>
        /// Returns true if ANY names on the second page match the first page
        /// </summary>
        /// <param name="firstPage"></param>
        /// <param name="secondPage"></param>
        /// <returns></returns>
        public bool DoAnyUserNamesMatchInOurLists(List<string> firstPage, List<string> secondPage)
        {
            for (int i = 0; i < firstPage.Count; i++)
            {
                for (int j = 0; j < secondPage.Count; j++)
                {
                    if (firstPage[i] == secondPage[j])
                        return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Returns true if ALL names on the second page match the first page
        /// </summary>
        /// <param name="firstPage"></param>
        /// <param name="secondPage"></param>
        /// <returns></returns>
        public bool DoAllUserNamesMatchInOurLists(List<string> firstPage, List<string> secondPage)
        {
            for (int i = 0; i < firstPage.Count; i++)
            {
                if (firstPage[i] != secondPage[i])
                    return false;
            }

            return true;
        }

    }
}
