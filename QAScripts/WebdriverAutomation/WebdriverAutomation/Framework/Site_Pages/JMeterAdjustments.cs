﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.Events;
using NUnit.Framework;
using log4net;
using System.Threading;

// PAGE URL:  http://www.jdate.co.il/Applications/CompatibilityMeter/Matches.aspx?NavPoint=sub

namespace WebdriverAutomation.Framework.Site_Pages
{
    public class JMeterAdjustments : BasePage
    {
        public JMeterAdjustments(IWebDriver driver)
            : base(driver)
        {
            Header = new WebdriverAutomation.Framework.Mobile_Parts.Header(driver);

            // verify we are on the correct page       
            IWebElement textOnPage = null;

            // check to see if the JMeter page for non JMeter subs appears
            if (driver.FindElements(By.XPath("//html/body/div/form/div/div[2]/div[2]/div/div/div/div/div[2]/h1")).Count > 0)
            {
                textOnPage = WaitUntilPageVerificationObjectExists(By.XPath("//html/body/div/form/div/div[2]/div[2]/div/div/div/div/div[2]/h1"), "JMETER ADJUSTMENTS PAGE");

                if (textOnPage.Text == "הכלי החכם למציאת זוגיות איכותית")
                    Assert.Fail("We clicked on the JMeter link in the header and have reached the JMeter Page for non JMeter subscribers.  Perhaps the current login for this test case needs to resubscribe.");
            }
            else
                textOnPage = WaitUntilPageVerificationObjectExists(By.XPath("//div[@id='jmeter-main']/div[2]/div/h1"), "JMETER ADJUSTMENTS PAGE");

            if (!textOnPage.Text.Contains(textThatVerifiesThisPageIsCorrect))
                log.Error("ERROR - JMETER ADJUSTMENTS PAGE - We should be on this page but the page verification failed.");

            Assert.True(textOnPage.Text.Contains(textThatVerifiesThisPageIsCorrect), "ERROR - JMETER ADJUSTMENTS PAGE - We should be on this page but the page verification failed.");

        }

        private static readonly ILog log = LogManager.GetLogger(typeof(JMeterWelcome));
        public WebdriverAutomation.Framework.Mobile_Parts.Header Header;

        private const string textThatVerifiesThisPageIsCorrect = "התאמות";

        /// <summary>
        /// We verify that at least one member is returned and that they have a username
        /// </summary>
        /// <returns></returns>
        public bool DoResultsAppear()
        {
            if (driver.FindElements(By.XPath("//div[@class='results list-view']")).Count == 0)
                return false;

            IWebElement element = WaitUntilElementExists(By.XPath("//a[contains(@id, '_MembersMatcesList_jmeterRepeater__ctl0_jmeterMiniProfile_LinkUserName')]"));

            if (element.Text != string.Empty)
                return true;

            return false;
        }
    }
}

