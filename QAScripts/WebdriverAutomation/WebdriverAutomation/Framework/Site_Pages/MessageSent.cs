﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.Events;
using NUnit.Framework;
using log4net;

// PAGE URL:  http://www.jdate.com/Applications/Email/MessageSent.aspx?
//   this is the page that appears AFTER a message is sent.  for the inbox page look for the Messages class (plural)

namespace WebdriverAutomation.Framework.Site_Pages
{    
    public class MessageSent : BasePage
    {
        public MessageSent(IWebDriver driver)
            : base(driver)
        {
            Header = new WebdriverAutomation.Framework.Site_Parts.Header(driver);
            Footer = new WebdriverAutomation.Framework.Site_Parts.Footer(driver);

            Tests.ReusableCode.Ads ads = new Tests.ReusableCode.Ads();
            ads.KillAllAds(driver, log);

            // verify we are on the correct page       
            IWebElement textOnPage = null;

            textOnPage = WaitUntilPageVerificationObjectExists(By.XPath("//div[@id='page-container']/h1"), "MESSAGE SENT PAGE");

            if (driver.Url.Contains("jdate.co.il"))
            {
                textThatVerifiesThisPageIsCorrect = "הודעה";

                if (textOnPage.Text != (textThatVerifiesThisPageIsCorrect))
                    log.Error("ERROR - MESSAGE SENT PAGE - We should be on this page but the page verification failed.");

                Assert.True(textOnPage.Text == textThatVerifiesThisPageIsCorrect, "ERROR - MESSAGE SENT PAGE - We should be on this page but the page verification failed.");
            
            }
            else
            {
                textThatVerifiesThisPageIsCorrect = "Message";

                if (textOnPage.Text != (textThatVerifiesThisPageIsCorrect))
                    log.Error("ERROR - MESSAGE SENT PAGE - We should be on this page but the page verification failed.");

                Assert.True(textOnPage.Text == textThatVerifiesThisPageIsCorrect, "ERROR - MESSAGE SENT PAGE - We should be on this page but the page verification failed.");
            }

        }

        private static readonly ILog log = LogManager.GetLogger(typeof(Subscribe));
        public WebdriverAutomation.Framework.Site_Parts.Header Header;
        public WebdriverAutomation.Framework.Site_Parts.Footer Footer;

        private string textThatVerifiesThisPageIsCorrect = string.Empty;

        public Framework.Site_Pages.Messages BackToInboxLink_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//a[contains(@id, 'backToProfileLink')]"));
            ClickUntilURLChanges(element);

            return new Messages(driver);
        }

        public string VerificationMessage()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//div[@id='page-container']/div[1]/div/h2"));
            return element.Text;
        }
    }
}
