﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.Events;
using NUnit.Framework;
using log4net;
using System.Threading;
using System.IO;
using System.Drawing.Imaging;

namespace WebdriverAutomation.Framework
{
    public abstract class BasePage
    {
        public BasePage(IWebDriver thisDriver)
        {
            driver = thisDriver;

            CheckForPageErrors();
        }

        public IWebDriver driver;
        public const int timeoutValue_Implicit = 1;                 // in seconds
        public const int timeoutValue_sleep = 3;

        /// <summary>
        /// If we error out trying to find a body chances are that the server cannot be found because there is no page source.  
        ///    try refreshing several times and fail out gracefully if we still don't see a body.
        /// </summary>
        private void CheckForPageErrors()
        {
            IWebElement element = null;

            try
            {
                element = driver.FindElement(By.XPath("//body"));  
            }
            catch
            {
                for (int i = 0; i < 10; i++)
                {
                    Thread.Sleep(2000);

                    driver.Navigate().Refresh();

                    try
                    {
                        element = driver.FindElement(By.XPath("//body"));
                        break;
                    }
                    catch { }

                    if (i == 9)
                        Assert.Fail("We are tried to load the page '" + driver.Url + "' 10 times but failed as we are unable to find any elements with the \"//body\" xpath.  The server might not be found for this URL at this point.");
                }
            }
        }

        #region Wait methods
        // can't get WaitDriverWait to work.  i don't even know if it's correctly implemented yet for C# so using these in the meantime to grab the  
        //    element over and over again 3 times (~15 seconds) until it appears or until it times out
        public IWebElement WaitUntilElementExists(By by)
        {
            IWebElement element = null;
            for (int second = 0; ; second++)
            {
                if (second >= timeoutValue_sleep) Assert.Fail("We timed out while waiting for an object to appear onscreen. Either we are not on the correct page where the object exists or the object we are looking for cannot be found");

                if (driver.FindElements(by).Count > 0)
                {
                    element = driver.FindElement(by);
                    break;
                }

                Thread.Sleep(100);
            }

            return element;
        }

        public IWebElement WaitUntilPageVerificationObjectExists(By by, string pageName)
        {
            IWebElement element = null;
            for (int second = 0; ; second++)
            {
                if (second >= timeoutValue_sleep) Assert.Fail("ERROR - " + pageName + " - The header or object that we are trying to find to verify that we are currently on the correct page does not exist.");

                if (driver.FindElements(by).Count > 0)
                {
                    element = driver.FindElement(by);
                    break;
                }

                Thread.Sleep(100);
            }

            return element;
        }

        public void WaitUntilURLExists()
        {
            for (int second = 0; ; second++)
            {
                if (second >= timeoutValue_sleep) Assert.Fail("We timed out while waiting for the URL value to be something besides an empty string.");

                if (driver.Url != string.Empty)
                    break;

                Thread.Sleep(100);
            }
        }

        /// <summary>
        /// This method is used to try to combat the "phantom clicks" i've been seeing by Webdriver since they seem to be taking their bloody time fixing the bug.
        /// </summary>
        /// <param name="by"></param>
        /// <returns></returns>
        public void ClickUntilURLChanges(IWebElement element)
        {
            string currentURL = driver.Url;
            int sleepTime = 1000;

            // we check for the jdate.co.il survey and try to get rid of it if we see it.
            if (driver.Url.Contains("jdate.co.il"))
            {
                for (int second = 0; ; second++)
                {
                    GetRidOfJdateCoIlSurvey();

                    if (second >= (timeoutValue_sleep + 2)) Assert.Fail("We timed out trying to click an object onscreen. Each time we click the object we expect the URL to change but it never does.");

                    // sometimes there is a timing issue where the currentURL isn't updated immediately after we click the first time so we check again before clicking
                    if (currentURL == driver.Url)
                        element.Click();

                    if (currentURL != driver.Url)
                        break;

                    Thread.Sleep(sleepTime);
                }
            }
            else
            {
                for (int second = 0; ; second++)
                {
                    if (second >= (timeoutValue_sleep + 2)) Assert.Fail("We timed out trying to click an object onscreen. Each time we click the object we expect the URL to change but it never does.");

                    // sometimes there is a timing issue where the currentURL isn't updated immediately after we click the first time so we check twice before clicking
                    try
                    {
                        if (currentURL == driver.Url)
                        {
                            Thread.Sleep(sleepTime);

                            if (currentURL == driver.Url)                            
                                    element.Click();                                
                            
                            else
                                break;
                        }
                        else
                            break;
                    }
                    catch
                    {
                        Thread.Sleep(5000);

                        if (currentURL == driver.Url)
                        {
                            // TESTING AGAIN FOR STUPID ERROR GET RID OF TRY STATEMENT BELOW LATER
                            try
                            {
                                element.Click();
                            }
                            catch
                            {
                                Assert.Fail("We are in ClickUntilURLChanges() and for some reason still getting failures after verifying the current url is the same as the previous one and clicking on an element.  Maybe the element's xpath needs to be changed or a timing issue exists between the time it takes to check the url and the time it takes to click the element.");
                            }
                        }

                        else
                            break;
                    }

                    Thread.Sleep(sleepTime);
                }
            }
        }

        /// <summary>
        /// This clicks an element until it is not displayed on the screen anymore.  
        /// </summary>
        /// <param name="by"></param>
        public void ClickUntilCurrentElementIsNotDisplayedAnymore(By by)
        {
            int maxLoopCount = 5;

            IWebElement element = driver.FindElement(by);

            element.Click();

            for (int i = 0; i < maxLoopCount; i++)
            {
                if (driver.FindElements(by).Count > 0)
                {
                    element = driver.FindElement(by);

                    if (element.Displayed)
                        element.Click();
                    else
                        break;

                    if (i == maxLoopCount - 1)
                        Assert.Fail("We fail try to click an element until it is not displayed anymore onscreen.");
                }
            }
        }

        /// <summary>
        /// Another phantom click method. This one clicks a link or a button until another object's text changes. This one is used for pages where the URL stays the same
        /// such as the Quick Search or Registration pages.
        /// </summary>
        /// <param name="by"></param>
        /// <returns></returns>
        public void ClickUntilObjectTextChanges(IWebElement elementToClick, By byOfElementWithExpectedChangedText)
        {
            IWebElement elementWithExpectedChangedText = WaitUntilElementExists(byOfElementWithExpectedChangedText);
            string originalText = elementWithExpectedChangedText.Text;

            for (int second = 0; ; second++)
            {
                if (second >= timeoutValue_sleep) Assert.Fail("We timed out trying to click a link or button onscreen. Each time we click the object we expect the another element's text to change to verify that the orignal link or button was correctly clicked but it never does.");

                elementToClick.Click();

                // sleep for a sec else we might get a stale element
                Thread.Sleep(1000);

                elementWithExpectedChangedText = WaitUntilElementExists(byOfElementWithExpectedChangedText);

                if (originalText != elementWithExpectedChangedText.Text)
                    break;

                Thread.Sleep(100);
            }
        }

        public string WaitUntilElementTextValueIsPresent(By by)
        {
            string textOfElement = string.Empty;
            for (int second = 0; ; second++)
            {
                if (second >= timeoutValue_sleep) Assert.Fail("We timed out while waiting for an object to appear onscreen. Either we are not on the correct page where the object exists or the object we are looking for cannot be found");

                if (driver.FindElements(by).Count > 0)
                {
                    textOfElement = driver.FindElement(by).GetAttribute("value");
                    if (!string.IsNullOrEmpty(textOfElement))
                        break;
                }

                Thread.Sleep(100);
            }

            return textOfElement;
        }

        public void WaitUntilElementIsWritable(By by, string stringToWrite)
        {
            string textOfElement = string.Empty;

            driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(1));
            for (int second = 0; ; second++)
            {
                if (second >= timeoutValue_sleep) Assert.Fail("We timed out while waiting for an object to appear onscreen. Either we are not on the correct page where the object exists or the object we are looking for cannot be found");

                if (driver.FindElements(by).Count > 0)
                {
                    driver.FindElement(by).SendKeys(stringToWrite);
                    textOfElement = driver.FindElement(by).GetAttribute("value");
                    if (textOfElement.ToLower() == stringToWrite.ToLower())
                        break;

                    // if a value was written that does not match what we intended to write, it may be because the values aren't initally cleared
                    WaitUntilElementValueCanBeCleared(by);
                }

                Thread.Sleep(100);
            }

            driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(timeoutValue_Implicit));

        }

        public void WaitUntilElementValueCanBeCleared(By by)
        {
            string textOfElement = string.Empty;

            driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(1));
            for (int second = 0; ; second++)
            {
                if (second >= timeoutValue_sleep) Assert.Fail("We timed out while waiting for an object to appear onscreen. Either we are not on the correct page where the object exists or the object we are looking for cannot be found");

                if (driver.FindElements(by).Count > 0)
                {
                    driver.FindElement(by).Clear();

                    break;
                }

                Thread.Sleep(100);
            }

            driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(timeoutValue_Implicit));

        }

        public void WaitUntilElementIsCheckable(By by)
        {
            string textOfElement = string.Empty;

            driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(1));

            Thread.Sleep(100);
            for (int second = 0; ; second++)
            {
                if (second >= timeoutValue_sleep) Assert.Fail("We timed out while waiting for an object to appear onscreen. Either we are not on the correct page where the object exists or the object we are looking for cannot be found");

                if (driver.FindElements(by).Count > 0)
                {
                    if (!driver.FindElement(by).Selected)
                        driver.FindElement(by).Click();

                    // in IE, sometimes checkboxes don't check correctly.  Workaround
                    if (driver.FindElement(by).Selected)
                        break;
                }

                Thread.Sleep(100);
            }

            driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(timeoutValue_Implicit));

        }

        public void WaitUntilElementIsClickable(By by)
        {
            string textOfElement = string.Empty;

            driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(1));

            Thread.Sleep(100);
            for (int second = 0; ; second++)
            {
                if (second >= timeoutValue_sleep) Assert.Fail("We timed out while waiting for an object to appear onscreen. Either we are not on the correct page where the object exists or the object we are looking for cannot be found");

                if (driver.FindElements(by).Count > 0)
                {
                    driver.FindElement(by).Click();

                    break;
                }

                Thread.Sleep(100);
            }

            driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(timeoutValue_Implicit));

        }

        public void WaitUntilElementIsSelectable(By by)
        {
            string textOfElement = string.Empty;

            driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(1));

            Thread.Sleep(100);
            for (int second = 0; ; second++)
            {
                if (second >= timeoutValue_sleep) Assert.Fail("We timed out while waiting for an object to appear onscreen. Either we are not on the correct page where the object exists or the object we are looking for cannot be found");

                if (driver.FindElements(by).Count > 0)
                {
                    driver.FindElement(by).Click();

                    break;
                }

                Thread.Sleep(100);
            }

            driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(timeoutValue_Implicit));

        }

        public void Wait(int waitTime)
        {
            Thread.Sleep(waitTime);
        }
        #endregion

        /// <summary>
        /// Returns the text of the value currently selected in a list or dropdown
        /// </summary>
        /// <param name="by"></param>
        /// <returns></returns>
        public string ReturnTextOfSelectedValueInList(By by)
        {
            IWebElement element = driver.FindElement(by);
            string selectedValue = element.GetAttribute("value");

            SelectElement select = new SelectElement(driver.FindElement(by));
            IList<IWebElement> options = select.Options;

            foreach (IWebElement option in options)
            {
                if (option.GetAttribute("value") == selectedValue)
                    return option.Text;
            }

            // failure
            return string.Empty;
        }

        /// <summary>
        /// Sometimes Webdriver does not send the correct string when sending keypresses if the user is using the keyboard at the same time.  This should hopefully
        /// correct the issue
        /// </summary>
        /// <param name="value"></param>
        public void SendKeys(IWebElement element, string value)
        {
            int maxLoopCount = 10;

            // try a few times to get it right
            for (int i = 0; i < maxLoopCount; i++)
            {
                // 8/27/12 focus on the textbox element so that SendKeys does not accidentally hover over the Header and click one of the Header links 
                RemoteWebElement gainFocus = (RemoteWebElement)element;
                var hack = gainFocus.LocationOnScreenOnceScrolledIntoView; 

                gainFocus.Click();

                element.Clear();

                element.SendKeys(value);

                if (element.GetAttribute("value") == value)
                    break;

                Thread.Sleep(1000);

                // sometimes we can't correctly sendkeys because webdriver for some reason opens multiple windows on its own.  stupid webdriver.
                // if multiple windows are open, close them 
                List<string> handlers = new List<string>();
                foreach (string handler in driver.WindowHandles)
                    handlers.Add(handler);

                while (driver.WindowHandles.Count > 1)
                {
                    // when we click on this link from the footer a new window appears. currently having problems switching pages in IE.  for now if we fail just continue
                    try
                    {
                        driver.SwitchTo().Window(handlers[1]);
                        driver.Close();
                        driver.SwitchTo().Window(handlers[0]);
                    }
                    catch
                    {
                    }
                }

                if (i == maxLoopCount - 1)
                {
                    Assert.Fail("We are unable to correctly type out the value '" + value + "' after trying " + maxLoopCount.ToString() + " times.  Stopping the test here.");
                }
            }
        }

        /// <summary>
        /// A brute force method to click objects we need to hover over first.
        /// </summary>
        /// <param name="elementToClick"></param>
        public void HoverClick(RemoteWebElement elementToClick)
        {
            int loopCount = 10;

            for (int i = 0; i < loopCount; i++)
            {
                try
                {
                    elementToClick.Click();
                }
                catch
                {
                    break;
                }

                Thread.Sleep(500);

                if (i == 9)
                    Assert.Fail("We are unable to hover and click over an object after trying " + loopCount + " times.");
            }
        }

        public string resultsPath = @"C:\Automation Results\";

        // mostly used for debugging
        public void TakeAndSaveScreenshot()
        {
            ITakesScreenshot screenshotCapableDriver = driver as ITakesScreenshot;
            Screenshot screenImage = screenshotCapableDriver.GetScreenshot();

            string currentDatetime = DateTime.Now.ToString().Replace(':', '-').Replace('/', '-');

            string screenshotName = "DEBUGGING screenshot " + currentDatetime;
            string screenshotPath = resultsPath + screenshotName + ".jpg";

            screenImage.SaveAsFile(screenshotPath, ImageFormat.Jpeg);
        }

        // trying to kill the survey that randomly appears in jdate.co.il
        public void GetRidOfJdateCoIlSurvey()
        {
            if (driver.FindElements(By.XPath("//div[@id='k_popup']")).Count > 0)
            {
                IWebElement element = WaitUntilElementExists(By.XPath("//div[@id='k_pop_yes_no']/a[@id='k_pop_no_btn']"));
                if (element.Displayed == true)
                    element.Click();
            }
        }
    }
}
