﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.Events;
using NUnit.Framework;
using log4net;

// PAGE URL:  http://www.blacksingles.com/Applications/MemberServices/SMSAlert.aspx  (for non JDate sites)
// this page is similar to the JDate version of this:  http://www.jdate.com/Applications/Mobile/MobileSettings.aspx  but I'm keeping the pages seperate right now.

namespace WebdriverAutomation.Framework.Site_Pages
{
    public class MobileAlertPreferences : BasePage
    {
        public MobileAlertPreferences(IWebDriver driver)
            : base(driver)
        {
            Footer = new WebdriverAutomation.Framework.Site_Parts.Footer(driver);

            // verify we are on the correct page       
            IWebElement textOnPage = null;

            textOnPage = WaitUntilPageVerificationObjectExists(By.XPath("//div[@id='page-container']/h1"), "MOBILE ALERT PREFERENCES PAGE");

            if (textOnPage.Text != (textThatVerifiesThisPageIsCorrect))
                log.Error("ERROR - MOBILE ALERT PREFERENCES PAGE - We should be on this page but the page verification failed.");

            Assert.True(textOnPage.Text == textThatVerifiesThisPageIsCorrect, "ERROR - MOBILE ALERT PREFERENCES PAGE - We should be on this page but the page verification failed.");

        }

        private static readonly ILog log = LogManager.GetLogger(typeof(MobileAlertPreferences));
        public WebdriverAutomation.Framework.Site_Parts.Footer Footer;

        private const string textThatVerifiesThisPageIsCorrect = "Mobile Alert Preferences";

    }
}
