﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.Events;
using NUnit.Framework;
using log4net;
using System.Threading;

// PAGE URL:  http://www.jdate.com/Applications/Email/ViewMessage.aspx?MemberMailID=1048562141&MemberFolderID=1&OrderBy=insertDate%20desc&CurrentMessageNumber=1&MessageCount=3
//   this is the page that appears AFTER a message is sent.  for the inbox page look for the Messages class (plural)

namespace WebdriverAutomation.Framework.Site_Pages
{
    public class Message : BasePage
    {
        public Message(IWebDriver driver)
            : base(driver)
        {
            Header = new WebdriverAutomation.Framework.Site_Parts.Header(driver);
            Footer = new WebdriverAutomation.Framework.Site_Parts.Footer(driver);

            Tests.ReusableCode.Ads ads = new Tests.ReusableCode.Ads();
            ads.KillAllAds(driver, log);

            // verify we are on the correct page       
            IWebElement textOnPage = null;

            textOnPage = WaitUntilPageVerificationObjectExists(By.XPath("//div[@id='page-container']/h1"), "MESSAGE PAGE");

            if (driver.Url.Contains("jdate.co.il"))
            {
                if (!textOnPage.Text.Contains(textThatVerifiesThisPageIsCorrect_jdatecoil1) && !textOnPage.Text.Contains(textThatVerifiesThisPageIsCorrect_jdatecoil2))
                    log.Error("ERROR - MESSAGE PAGE - We should be on this page but the page verification failed.");

                Assert.True((textOnPage.Text.Contains(textThatVerifiesThisPageIsCorrect_jdatecoil1) || textOnPage.Text.Contains(textThatVerifiesThisPageIsCorrect_jdatecoil2)), "ERROR - MESSAGE PAGE - We should be on this page but the page verification failed.");

            }
            else
            {
                if (!textOnPage.Text.Contains(textThatVerifiesThisPageIsCorrect1) && !textOnPage.Text.Contains(textThatVerifiesThisPageIsCorrect2))
                    log.Error("ERROR - MESSAGE PAGE - We should be on this page but the page verification failed.");

                Assert.True((textOnPage.Text.Contains(textThatVerifiesThisPageIsCorrect1) || textOnPage.Text.Contains(textThatVerifiesThisPageIsCorrect2)), "ERROR - MESSAGE PAGE - We should be on this page but the page verification failed.");
            }

        }

        private static readonly ILog log = LogManager.GetLogger(typeof(Subscribe));
        public WebdriverAutomation.Framework.Site_Parts.Header Header;
        public WebdriverAutomation.Framework.Site_Parts.Footer Footer;

        private const string textThatVerifiesThisPageIsCorrect1 = "Message";
        private const string textThatVerifiesThisPageIsCorrect2 = "Email";
        private const string textThatVerifiesThisPageIsCorrect_jdatecoil1 = "הודעות";
        private const string textThatVerifiesThisPageIsCorrect_jdatecoil2 = "הודעת Power User";

        public string GetEmailSubject()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//div[@id='message-control-header']/div/div/p[1]"));

            if (driver.Url.Contains("jdate.co.il"))
                return element.Text.Remove(0, 7).Trim();
            else
                return element.Text.Remove(0, 9).Trim();
        }

        public bool DoesEmailContainCorrectSubject(string subject)
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//div[@id='message-control-header']/div/p[1]"));

            if (element.Text.Contains(subject))
                return true;

            return false;
        }

        public string GetEmailMessage()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//div[@id='message-view']"));
            return element.Text;
        }

        public bool DoesEmailContainCorrectMessage(string message)
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//div[@id='message-view']"));

            if (element.Text.Contains(message))
                return true;

            return false;
        }

        public Framework.Site_Pages.Messages BackToMessagesLink_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//a[contains(@id, 'viewMessageLink')]/span[2]"));
            element.Click();

            return new Messages(driver);
        }

        public Framework.Site_Pages.ComposeMessage ReplyForFreeButton_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//input[contains(@id, 'btnReply')]"));
            element.Click();

            return new ComposeMessage(driver);
        }

        public string MessageTextarea
        {
            get
            {
                IWebElement element = WaitUntilElementExists(By.XPath("//textarea[contains(@id, 'messageBodyTextField')]"));
                return element.GetAttribute("value");
            }
            set
            {
                string theXpath = "//textarea[contains(@id, 'messageBodyTextField')]";

                WaitUntilElementValueCanBeCleared(By.XPath(theXpath));
                SendKeys(WaitUntilElementExists(By.XPath(theXpath)), value);
            }
        }

        /// <summary>
        /// This is the Send button that appears when replying back to a JDate All Access email
        /// </summary>
        /// <returns></returns>
        public Framework.Site_Pages.MessageSent SendButton_Click_ReplyingToEmail()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//input[contains(@id, 'sendVIPMessageButton')]"));
            ClickUntilURLChanges(element);

            // there seems to be a race condition in that the Message Sent page doesn't appear fast enough and the page verification fails.  adding a sleep.
            Thread.Sleep(1000);

            return new MessageSent(driver);
        }

        /// <summary>
        /// JDate.co.il only so far:  This is the Send button that appears when replying back to a normal email
        /// </summary>
        /// <returns></returns>
        public Framework.Site_Pages.MessageSent SendButton_Click_ReplyingToEmail_JDatecoil()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//input[contains(@id, 'sendMessageButton')]"));
            ClickUntilURLChanges(element);

            // there seems to be a race condition in that the Message Sent page doesn't appear fast enough and the page verification fails.  adding a sleep.
            Thread.Sleep(1000);

            return new MessageSent(driver);
        }
    }
}
