﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.Events;
using NUnit.Framework;
using log4net;
using System.Threading;

// PAGE URL:  http://www.jdate.co.il/Confirmation/?Scenario=JMAG%20IL%20IFRAME&PRM=47896&lgid=&SPWSCYPHER=3e+zLOuqoIOZoIgqCtJkiPN3RjaIM/naD5Ef6HS+8c31wlMhJiW6dzhCtLRboRMHhoLDLFr3w+E3Qi7RoX3FSxceuX2l0wxvdbYJgI61ZIo=

namespace WebdriverAutomation.Framework.Site_Pages
{
    public class RegistrationJDateCoIlJMagiFrameRegComplete : BasePage
    {
        public RegistrationJDateCoIlJMagiFrameRegComplete(IWebDriver driver)
            : base(driver)
        {
            // verify we are on the correct page       
            if (driver.FindElements(By.XPath("//div[@class='jmag-container']")).Count == 0)
                log.Error("ERROR - REGISTRATION FACEBOOK JMAG REGISTRATION COMPLETE FOR JDATE.CO.IL PAGE - We should be on this page but the page verification failed.");

            Assert.True(driver.FindElements(By.XPath("//div[@class='jmag-container']")).Count > 0, "ERROR - REGISTRATION FACEBOOK JMAG REGISTRATION COMPLETE FOR JDATE.CO.IL PAGE - We should be on this page but the page verification failed.");

        }

        private static readonly ILog log = LogManager.GetLogger(typeof(RegistrationJDateCoIlFacebookiFrameRegComplete));

        public void YourMatchesButton_Click()
        {
            WaitUntilElementIsClickable(By.XPath("//a[@class='matches']"));

            // clicking the above button opens a new browser page. switch to the new page and kill the current one.
            List<string> handlers = new List<string>();
            foreach (string handler in driver.WindowHandles)
                handlers.Add(handler);

            driver.Close();

            driver.SwitchTo().Window(handlers[1]);
        }

    }
}
