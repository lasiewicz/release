﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.Events;
using NUnit.Framework;
using log4net;

// An Important Notice: I promise to never send money, ask other members for money, or share financial information with other members....."
// PAGE URL:  http://www.bbwpersonalsplus.com/Applications/MemberProfile/AttributeEdit.aspx?Attribute=PledgeToCounterFroud&DestinationURL=%2fDefault.aspx

namespace WebdriverAutomation.Framework.Site_Pages
{
    public class AnImportantNotice : BasePage
    {
        public AnImportantNotice(IWebDriver driver)
            : base(driver)
        {
            Footer = new WebdriverAutomation.Framework.Site_Parts.Footer(driver);

            Tests.ReusableCode.Ads ads = new Tests.ReusableCode.Ads();
            ads.KillAllAds(driver, log);

            // verify we are on the correct page       
            IWebElement textOnPage = null;

            if (driver.Url.Contains("jdate.co.il"))
                textThatVerifiesThisPageIsCorrect = "הודעה חשובה:";
            else
                textThatVerifiesThisPageIsCorrect = "An Important Notice:";

            textOnPage = WaitUntilPageVerificationObjectExists(By.XPath("//div[@id='pledge']/h1"), "AN IMPORTANT NOTICE PAGE");

            if (textOnPage.Text != (textThatVerifiesThisPageIsCorrect))
                log.Error("ERROR - AN IMPORTANT NOTICE PAGE - We should be on this page but the page verification failed.");

            Assert.True(textOnPage.Text == textThatVerifiesThisPageIsCorrect, "ERROR - AN IMPORTANT NOTICE PAGE - We should be on this page but the page verification failed.");

        }

        private static readonly ILog log = LogManager.GetLogger(typeof(AnImportantNotice));
        public WebdriverAutomation.Framework.Site_Parts.Footer Footer;

        private string textThatVerifiesThisPageIsCorrect = string.Empty;

        public WebdriverAutomation.Framework.Site_Pages.Home IAccept_Click()
        {
            // notice that there is a timing issue in between when the I Accept button is clicked and the Home Page appears
            ClickUntilCurrentElementIsNotDisplayedAnymore(By.XPath("//input[contains(@id, '_btnPledgeOK')]"));

            return new Home(driver);
        }
    }

}
