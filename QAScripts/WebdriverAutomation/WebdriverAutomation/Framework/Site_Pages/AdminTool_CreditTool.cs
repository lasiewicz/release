﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.Events;
using NUnit.Framework;
using log4net;

// PAGE URL:  http://credittool.matchnet.com/
//     note: when the link for this is clicked a new window appears

namespace WebdriverAutomation.Framework.Pages
{
    public class AdminTool_CreditTool : BasePage
    {
        public AdminTool_CreditTool(IWebDriver driver)
            : base(driver)
        {
            // verify we are on the correct page      
            List<string> handlers = new List<string>();
            foreach (string handler in driver.GetWindowHandles())
                handlers.Add(handler);

            string parentWindow = driver.GetWindowHandle();

            // when we click on this link from the footer a new window appears. currently having problems switching pages in IE.  for now if we fail just continue
            try
            {
                driver.SwitchTo().Window(handlers[1]);

                if (!driver.Url.Contains("credittool.matchnet.com"))
                    log.Error("ERROR - ADMIN TOOL - CREDIT TOOL PAGE - We should be on this page but the page verification failed.");

                Assert.True(driver.Url.Contains("credittool.matchnet.com"), "ERROR - ADMIN TOOL - CREDIT TOOL PAGE - We should be on this page but the page verification failed.");

                driver.Close();
                driver.SwitchTo().Window(handlers[0]);
            }
            catch
            {
            }

        }

        private static readonly ILog log = LogManager.GetLogger(typeof(AdvertiseWithUs));

    }
}
