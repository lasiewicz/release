﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.Events;
using NUnit.Framework;
using log4net;
using System.Threading;

// PAGE URL:  http://www.jdate.co.il/?Scenario=FB%20REG%20IFRAME&PRM=47896&lgid=[test]

namespace WebdriverAutomation.Framework.Site_Pages
{
    public class RegistrationJDateCoIlFacebookiFrame : BasePage
    {
        public RegistrationJDateCoIlFacebookiFrame(IWebDriver driver)
            : base(driver)
        {
            // verify we are on the correct page       
            IWebElement textOnPage = null;

            textOnPage = WaitUntilPageVerificationObjectExists(By.XPath("//div[@id='formItem_newslettermask']/label"), "REGISTRATION FACEBOOK IFRAME FOR JDATE.CO.IL PAGE");

            if (!textOnPage.Text.Contains(textThatVerifiesThisPageIsCorrect))
                log.Error("ERROR - REGISTRATION FACEBOOK IFRAME FOR JDATE.CO.IL PAGE - We should be on this page but the page verification failed.");

            Assert.True(textOnPage.Text.Contains(textThatVerifiesThisPageIsCorrect), "ERROR - REGISTRATION FACEBOOK IFRAME FOR JDATE.CO.IL PAGE - We should be on this page but the page verification failed.");

        }

        private static readonly ILog log = LogManager.GetLogger(typeof(RegistrationJDateCoIlFacebookiFrame));

        private const string textThatVerifiesThisPageIsCorrect = "אני מעוניינ/ת לקבל למייל מסרים שיווקים בדבר מוצרים או שירותים שונים.";

        public void MaritalStatusListBox_JDate_co_il_1Page_Select(int index)
        {
            string xPathOfSelectedIndex = "//section/div[3]/div[2]/ul/li[" + (index + 1).ToString() + "]";
            WaitUntilElementIsClickable(By.XPath(xPathOfSelectedIndex));
        }

        public void IAmADropdown_Select(int index)
        {
            SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[@id='idgendermask']")));
            select.SelectByIndex(index);
        }

        public void BirthDateMonthDropdown_Select(int index)
        {
            SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[@id='birthdayMonth']")));
            select.SelectByIndex(index);
        }

        public void BirthDateDayDropdown_Select(int index)
        {
            SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[@id='birthdayDay']")));
            select.SelectByIndex(index);
        }

        public void BirthDateYearDropdown_Select(int index)
        {
            SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[@id='birthdayYear']")));
            select.SelectByIndex(index);
        }

        public void YourReligiousBackgroundDropdown_Select(int index)
        {
            SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[@id='idjdatereligion']")));
            select.SelectByIndex(index);
        }

        public void CityTextbox_Write(string text)
        {
            WaitUntilElementIsWritable(By.XPath("//input[@id='regionCitiesByRegionId']"), text);
        }

        public void CityListBox_SelectFirstValue()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//input[contains(@id, 'regionCitiesByRegionId')]"));

            Thread.Sleep(1000);
            element.SendKeys(Keys.Down);

            Thread.Sleep(1000);
            element.SendKeys(Keys.Return);

            //SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//div[@class='regionCitiesByRegionId']")));
            //select.SelectByText(value);
        }

        public void EmailAddressTextbox_Write(string text)
        {
            WaitUntilElementIsWritable(By.XPath("//input[contains(@id, 'idemailaddress')]"), text);
        }

        public void ChooseYourPasswordTextbox_Write(string text)
        {
            WaitUntilElementIsWritable(By.XPath("//input[contains(@id, 'idpassword')]"), text);
        }

        public void IWouldLikeSpecialOffersAndAnnouncements_Click()
        {
            string xPathOfSelectedIndex = "//input[@id='idnewslettermask']";

            WaitUntilElementIsClickable(By.XPath(xPathOfSelectedIndex));
        }

        public RegistrationJDateCoIlFacebookiFrameRegComplete StartButton_Click()
        {
            Wait(1000);

            WaitUntilElementIsClickable(By.XPath("//button[@id='btnContinue']"));

            // wait until the next page (which should have the text 'confirmation' in the URL) appears before verifying if that page is the correct page
            int count = 10;

            for (int i = 0; i < count; i++)
            {
                Wait(1000);

                if (driver.Url.Contains("Confirmation"))
                    break;

                if (i == count - 1)
                    Assert.Fail("After clicking on the Start Button in the JDate.co.il iFrame Registration page we expect to reach a Registration Complete page with the string 'Confirmation' in the URL but we do not after " + count + " seconds.");
            }

            return new RegistrationJDateCoIlFacebookiFrameRegComplete(driver);
        }

    }
}
