﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.Events;
using NUnit.Framework;
using log4net;

// PAGE URL:  https://secure.spark.net/jdatecom (while logged in)

namespace WebdriverAutomation.Framework.Site_Pages
{    
    public class SubscribeConfirmation : BasePage
    {
        public SubscribeConfirmation(IWebDriver driver)
            : base(driver)
        {
            // verify that the, "We are unable to process your request. Please check your account information and try again..." page does not appear
            string notificationBarText = string.Empty;

            try
            {
                IWebElement accountSuspendedText = WaitUntilElementExists(By.XPath("//div[@id='content-main']/div[contains(@id, 'divParentContainer')]/h1/span"));
                notificationBarText = accountSuspendedText.Text;
            }
            catch { }

            if (notificationBarText.Contains("We are unable to process your request"))
            {
                log.Error("ERROR - We cannot purchase a subscription because the credit card we used did not go through.");
                Assert.Fail("ERROR - We cannot purchase a subscription because the credit card we used did not go through.");
            }

            // verify we are on the correct page       
            IWebElement textOnPage = null;

            textOnPage = WaitUntilPageVerificationObjectExists(By.XPath("//div[@id='content-main']/div[2]/h1[1]n"), "SUBSCRIBE CONFIRMATION PAGE");

            if (textOnPage.Text != (textThatVerifiesThisPageIsCorrect))
                log.Error("ERROR - SUBSCRIBE CONFIRMATION PAGE - We should be on this page but the page verification failed.");

            Assert.True(textOnPage.Text == textThatVerifiesThisPageIsCorrect, "ERROR - SUBSCRIBE CONFIRMATION PAGE - We should be on this page but the page verification failed.");

        }

        private static readonly ILog log = LogManager.GetLogger(typeof(Subscribe));

        private const string textThatVerifiesThisPageIsCorrect = "Congratulations! Your Subscription Has Been Processed.";

        public Framework.Site_Pages.Home AddAllAccessButton_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//input[@id='lbtnProcessOrder']"));
            element.Click();

            return new Framework.Site_Pages.Home(driver);
        }
        
        public Framework.Site_Pages.Home ContinueWithoutAddingAllAccessLink_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//input[@id='nothanks-btn']"));
            element.Click();

            return new Framework.Site_Pages.Home(driver);
        }
    }
}
