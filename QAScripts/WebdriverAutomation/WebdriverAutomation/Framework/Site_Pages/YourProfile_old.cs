﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.Events;
using NUnit.Framework;
using log4net;

// PAGE URL:  http://www.jdate.com/Applications/MemberProfile/ViewProfile.aspx?EntryPoint=99999&NavPoint=top (while logged in)
//  This is the page for both the user's Profile page and any otehr Profile that you are currently viewing besides your page.
//  

namespace WebdriverAutomation.Framework.Site_Pages
{
    public class YourProfile_old : BasePage
    {
        public YourProfile_old(IWebDriver driver)
            : base(driver)
        {
            Header = new WebdriverAutomation.Framework.Site_Parts.Header(driver);
            Footer = new WebdriverAutomation.Framework.Site_Parts.Footer(driver);

            Tests.ReusableCode.Ads ads = new Tests.ReusableCode.Ads();
            ads.KillAllAds(driver, log);

            // initially assume that we are on an old profile and click on the Basics tab and verify that the URL contains the text, 
            //   "About Me".  if we cannot find the "About Me" element we are on the new profile.
            IWebElement element;

            if (driver.FindElements(By.XPath("//a[contains(@id, 'DynamicTabGroup1_repeaterTabs__ctl1_tabLink')]")).Count > 0)
            {
                element = WaitUntilElementExists(By.XPath("//a[contains(@id, 'DynamicTabGroup1_repeaterTabs__ctl1_tabLink')]"));
                element.Click();

                isOldProfile = true;
            }
            else
                isOldProfile = false;

            // verify we are on the correct page 
            if (isOldProfile == true)
            {
                IWebElement textOnPage = null;

                if (driver.Url.Contains("jdate.com"))
                {
                    textThatVerifiesThisPageIsCorrect = "About Me";

                    textOnPage = WaitUntilPageVerificationObjectExists(By.XPath("//span[contains(@id, 'BasicsTab.ascx__ctl0_RepeaterSections__ctl4_RepeaterSectionItems__ctl0_lblSectionHeader')]"), "OLD PROFILE PAGE");
                }
                else if (driver.Url.Contains("spark.com"))
                {
                    textThatVerifiesThisPageIsCorrect = "About Me";

                    textOnPage = WaitUntilPageVerificationObjectExists(By.XPath("//span[contains(@id, 'BasicsTab.ascx__ctl0_RepeaterSections__ctl5_RepeaterSectionItems__ctl0_lblSectionHeader')]"), "OLD PROFILE PAGE");
                }
                else if (driver.Url.Contains("blacksingles.com"))
                {
                    textThatVerifiesThisPageIsCorrect = "My Introduction";

                    textOnPage = WaitUntilPageVerificationObjectExists(By.XPath("//span[contains(@id, 'BasicsTab.ascx__ctl0_RepeaterSections__ctl2_RepeaterSectionItems__ctl0_lblSectionHeader')]"), "OLD PROFILE PAGE");
                }
                else if (driver.Url.Contains("bbwpersonalsplus.com"))
                {
                    textThatVerifiesThisPageIsCorrect = "My Introduction";

                    textOnPage = WaitUntilPageVerificationObjectExists(By.XPath("//span[contains(@id, 'BasicsTab.ascx__ctl0_RepeaterSections__ctl2_RepeaterSectionItems__ctl0_lblSectionHeader')]"), "OLD PROFILE PAGE");
                }

                if (!textOnPage.Text.Contains(textThatVerifiesThisPageIsCorrect))
                    log.Error("ERROR - OLD PROFILE PAGE - We should be on this page but the page verification failed.");

                Assert.True(textOnPage.Text.Contains(textThatVerifiesThisPageIsCorrect), "ERROR - OLD PROFILE PAGE - We should be on this page but the page verification failed.");
            }
            else
            {
                // the new (currently beta as of 06-14-11) Profile Page has it's own class, "YourProfile.cs".  
                Assert.Fail("We are currently viewing a new Profile Beta site or a site that is not the Profile Page at all.  If we intend to be viewing the old Profile Page, turn Profile Beta off, then try running this test case again");

            }
        }

        private static readonly ILog log = LogManager.GetLogger(typeof(Login));
        public WebdriverAutomation.Framework.Site_Parts.Header Header;
        public WebdriverAutomation.Framework.Site_Parts.Footer Footer;

        private string textThatVerifiesThisPageIsCorrect = string.Empty;
        private bool isOldProfile;

        // Basics / Introduction etc.
        public void Tab1_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//a[contains(@id, 'DynamicTabGroup1_repeaterTabs__ctl1_tabLink')]"));
            element.Click();
        }

        // Lifestyle / Who I Am... etc.
        public void Tab2_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//a[contains(@id, 'DynamicTabGroup1_repeaterTabs__ctl2_tabLink')]"));
            element.Click();
        }

        // Interests / I'm Looking For... etc.
        public void Tab3_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//a[contains(@id, 'DynamicTabGroup1_repeaterTabs__ctl3_tabLink')]"));
            element.Click();
        }

        // Relationship / Compatability etc.
        public void Tab4_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//a[contains(@id, 'DynamicTabGroup1_repeaterTabs__ctl4_tabLink')]"));
            element.Click();
        }

        // Color Code / Q & A etc.
        public void Tab5_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//a[contains(@id, 'DynamicTabGroup1_repeaterTabs__ctl5_tabLink')]"));
            element.Click();
        }

        // Kibitz Tab / Photos (Spark) etc.
        public void Tab6_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//a[contains(@id, 'DynamicTabGroup1_repeaterTabs__ctl6_tabLink')]"));
            element.Click();
        }

        // Photos (JDate)
        public void Tab7_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//a[contains(@id, 'DynamicTabGroup1_repeaterTabs__ctl7_tabLink')]"));
            element.Click();
        }

        #region Objects that appear when viewing your own profile.  Edit, etc.
        public Framework.Site_Pages.EditYourProfile EditButton_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//a[contains(@id, 'DynamicTabGroup1_ProfileTabCommunicationButtons1_lnkEditProfileSection')]"));
            element.Click();

            return new EditYourProfile(driver);
        }
        #endregion

        /// <summary>
        /// Tab 1:  Basics / Intoduction....
        /// This is for the Relationship status that appears in the upper portion of the Profile Page information
        /// For example:  Divorced, Woman seeking Man
        /// </summary>
        public string RelationshipStatus_Tab1
        {
            get
            {
                string[] words = null;

                if (driver.Url.Contains("jdate.com"))
                {
                    IWebElement element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'BasicsTab.ascx_panelBasicsTab')]/div/ul[1]/li[2]/span"));

                    // parse out the relationship status
                    words = element.Text.Split(',');
                }
                else if (driver.Url.Contains("spark.com"))
                {
                    IWebElement element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'BasicsTab.ascx_panelBasicsTab')]/div/ul[1]/li[4]/span"));

                    // parse out the relationship status
                    words = element.Text.Split(',');

                }

                return words[0];
            }
        }

        /// <summary>
        /// Tab 1:  Basics / Intoduction....
        /// This is for the Relationshiop status that appears in the lower portion of the Profile Page information
        /// This seems to only appear for Spark.com
        /// </summary>
        public string RelationshipStatus_Tab1_Spark
        {
            get
            {
                IWebElement element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'BasicsTab.ascx_panelBasicsTab')]/div/dl/dd[1]"));

                return element.Text;
            }
        }

        public string YouAreALookingForA
        {
            get
            {
                string[] words = null;

                if (driver.Url.Contains("jdate.com"))
                {
                    IWebElement element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'BasicsTab.ascx_panelBasicsTab')]/div/ul[1]/li[2]/span"));

                    // parse out the relationship status
                    words = element.Text.Split(',');
                }
                else if (driver.Url.Contains("spark.com"))
                {
                    IWebElement element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'BasicsTab.ascx_panelBasicsTab')]/div/ul[1]/li[4]/span"));

                    // parse out the relationship status
                    words = element.Text.Split(',');

                }

                return words[1].Trim();
            }
        }

        public string ForA
        {
            get
            {
                string sub = string.Empty;

                if (driver.Url.Contains("jdate.com"))
                {
                    IWebElement element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'BasicsTab.ascx_panelBasicsTab')]/div/ul[1]/li[3]/span"));

                    sub = element.Text.Substring(4, element.Text.Length - 4);
                }
                else if (driver.Url.Contains("spark.com"))
                {
                    IWebElement element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'BasicsTab.ascx_panelBasicsTab')]/div/ul[1]/li[5]/span"));

                    sub = element.Text.Substring(4, element.Text.Length - 4);
                }

                return sub;
            }
        }

        public string YearsOld
        {
            get
            {
                IWebElement element = null;

                if (driver.Url.Contains("jdate.com"))
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'BasicsTab.ascx_panelBasicsTab')]/div/ul[1]/li[4]/span"));
                else if (driver.Url.Contains("spark.com"))
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'BasicsTab.ascx_panelBasicsTab')]/div/ul[1]/li[2]/span"));
                else if (driver.Url.Contains("blacksingles.com"))
                    element = WaitUntilElementExists(By.XPath("//div[@id='profile-cont']/div[2]/ul[1]/li[3]"));
                else if (driver.Url.Contains("bbwpersonalsplus.com"))
                    element = WaitUntilElementExists(By.XPath("//div[@id='profile-cont']/div[2]/ul[1]/li[3]"));

                string sub = element.Text.Substring(0, element.Text.Length - 10);
                return sub;
            }
        }

        public string Location
        {
            get
            {
                IWebElement element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'BasicsTab.ascx_panelBasicsTab')]/div/ul[1]/li[5]/span"));

                // parse out the City and only return the Country
                string[] words = element.Text.Split(',');

                return words[1].Trim();
            }
        }

        public string AboutMe
        {
            get
            {
                IWebElement element = null;

                if (driver.Url.Contains("jdate.com"))
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'BasicsTab.ascx_panelBasicsTab')]/div/p"));
                else if (driver.Url.Contains("spark.com"))
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'BasicsTab.ascx_panelBasicsTab')]/div/p[1]"));
                else if (driver.Url.Contains("blacksingles.com"))
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'BasicsTab.ascx_panelBasicsTab')]/div/p"));
                else if (driver.Url.Contains("bbwpersonalsplus.com"))
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'BasicsTab.ascx_panelBasicsTab')]/div/p"));

                return element.Text;
            }
        }

        /// <summary>
        /// Spark.com only
        /// </summary>
        public string IAmLookingFor
        {
            get
            {
                IWebElement element = null;

                element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'BasicsTab.ascx_panelBasicsTab')]/div/p[2]"));

                return element.Text;
            }
        }

        public string Height
        {
            get
            {
                IWebElement element = null;

                if (driver.Url.Contains("jdate.com"))
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'BasicsTab.ascx_panelBasicsTab')]/div/ul[3]/li[1]/span"));
                else if (driver.Url.Contains("spark.com"))
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'BasicsTab.ascx_panelBasicsTab')]/div/dl/dd[5]"));
                else if (driver.Url.Contains("blacksingles.com") || driver.Url.Contains("bbwpersonalsplus.com"))
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'LifestyleTab.ascx_panelLifestyleTab')]/div/dl/dd[1]"));

                return element.Text;
            }
        }

        public string Weight
        {
            get
            {
                IWebElement element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'BasicsTab.ascx_panelBasicsTab')]/div/ul[3]/li[2]/span"));
                return element.Text;
            }
        }

        public string MyHairIs
        {
            get
            {
                IWebElement element = null;

                if (driver.Url.Contains("jdate.com"))
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'BasicsTab.ascx_panelBasicsTab')]/div/ul[3]/li[3]/span"));
                else if (driver.Url.Contains("spark.com"))
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'LifestyleTab.ascx_panelLifestyleTab')]/div/dl/dd[1]"));
                else if (driver.Url.Contains("blacksingles.com") || driver.Url.Contains("bbwpersonalsplus.com"))
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'LifestyleTab.ascx_panelLifestyleTab')]/div/dl/dd[3]"));

                return element.Text;
            }
        }

        public string MyEyesAre
        {
            get
            {
                IWebElement element = null;

                if (driver.Url.Contains("jdate.com"))
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'BasicsTab.ascx_panelBasicsTab')]/div/ul[3]/li[4]/span"));
                else if (driver.Url.Contains("spark.com"))
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'LifestyleTab.ascx_panelLifestyleTab')]/div/dl/dd[2]"));
                else if (driver.Url.Contains("blacksingles.com") || driver.Url.Contains("bbwpersonalsplus.com"))
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'LifestyleTab.ascx_panelLifestyleTab')]/div/dl/dd[4]"));

                return element.Text;
            }
        }

        public string BodyStyle
        {
            get
            {
                IWebElement element = null;

                if (driver.Url.Contains("jdate.com"))
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'BasicsTab.ascx_panelBasicsTab')]/div/ul[3]/li[5]/span"));
                else if (driver.Url.Contains("spark.com"))
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'BasicsTab.ascx_panelBasicsTab')]/div/dl/dd[6]"));
                else if (driver.Url.Contains("blacksingles.com") || driver.Url.Contains("bbwpersonalsplus.com"))
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'LifestyleTab.ascx_panelLifestyleTab')]/div/dl/dd[2]"));

                return element.Text;
            }
        }


        public string Relocation
        {
            get
            {
                IWebElement element = null;

                if (driver.Url.Contains("jdate.com"))
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'LifestyleTab.ascx_panelLifestyleTab')]/div/ul[1]/li[1]/span"));
                else if (driver.Url.Contains("spark.com"))
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'LifestyleTab.ascx_panelLifestyleTab')]/div/dl/dd[5]"));

                return element.Text;
            }
        }

        public string Children
        {
            get
            {
                IWebElement element = null;

                if (driver.Url.Contains("jdate.com"))
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'LifestyleTab.ascx_panelLifestyleTab')]/div/ul[1]/li[2]/span"));
                else if (driver.Url.Contains("spark.com"))
                {
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'BasicsTab.ascx_panelBasicsTab')]/div/dl/dd[2]"));

                    if (element.Text == "")
                        element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'RelationshipTab.ascx_panelRelationshipTab')]/div/ul/li[4]/span"));
                }
                else if (driver.Url.Contains("blacksingles.com") || driver.Url.Contains("bbwpersonalsplus.com"))
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'LifestyleTab.ascx_panelLifestyleTab')]/div/dl/dd[7]"));

                return element.Text;
            }
        }

        public string CustodySituation
        {
            get
            {
                IWebElement element = null;

                if (driver.Url.Contains("jdate.com"))
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'LifestyleTab.ascx_panelLifestyleTab')]/div/ul[1]/li[3]/span"));
                else if (driver.Url.Contains("spark.com"))
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'BasicsTab.ascx_panelBasicsTab')]/div/dl/dd[3]"));
                else if (driver.Url.Contains("blacksingles.com") || driver.Url.Contains("bbwpersonalsplus.com"))
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'LifestyleTab.ascx_panelLifestyleTab')]/div/dl/dd[8]"));

                return element.Text;
            }
        }

        public string PlanOnHavingChildren
        {
            get
            {
                IWebElement element = null;

                if (driver.Url.Contains("jdate.com"))
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'LifestyleTab.ascx_panelLifestyleTab')]/div/ul[1]/li[4]/span"));
                else if (driver.Url.Contains("spark.com"))
                {
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'BasicsTab.ascx_panelBasicsTab')]/div/dl/dd[4]"));

                    if (element.Text == "")
                        element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'RelationshipTab.ascx_panelRelationshipTab')]/div/ul/li[5]/span"));
                }
                else if (driver.Url.Contains("blacksingles.com") || driver.Url.Contains("bbwpersonalsplus.com"))
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'LifestyleTab.ascx_panelLifestyleTab')]/div/dl/dd[6]"));

                return element.Text;
            }
        }

        public string IKeepKosher
        {
            get
            {
                IWebElement element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'LifestyleTab.ascx_panelLifestyleTab')]/div/ul[1]/li[5]/span"));
                return element.Text;
            }
        }

        public string IGoToSynagogue
        {
            get
            {
                IWebElement element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'LifestyleTab.ascx_panelLifestyleTab')]/div/ul[1]/li[6]/span"));
                return element.Text;
            }
        }

        public string ISmoke
        {
            get
            {
                IWebElement element = null;

                if (driver.Url.Contains("jdate.com"))
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'LifestyleTab.ascx_panelLifestyleTab')]/div/ul[1]/li[7]/span"));
                else if (driver.Url.Contains("spark.com"))
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'BasicsTab.ascx_panelBasicsTab')]/div/dl/dd[9]"));

                return element.Text;
            }
        }

        public string IDrink
        {
            get
            {
                IWebElement element = null;

                if (driver.Url.Contains("jdate.com"))
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'LifestyleTab.ascx_panelLifestyleTab')]/div/ul[1]/li[8]/span"));
                else if (driver.Url.Contains("spark.com"))
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'BasicsTab.ascx_panelBasicsTab')]/div/dl/dd[10]"));

                return element.Text;
            }
        }

        public string ZodiacSign
        {
            get
            {
                IWebElement element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'LifestyleTab.ascx_panelLifestyleTab')]/div/ul[1]/li[9]/span"));
                return element.Text;
            }
        }

        public string ActivityLevel
        {
            get
            {
                IWebElement element = null;

                if (driver.Url.Contains("jdate.com"))
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'LifestyleTab.ascx_panelLifestyleTab')]/div/ul[1]/li[10]/span"));
                else if (driver.Url.Contains("spark.com"))
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'LifestyleTab.ascx_panelLifestyleTab')]/div/dl/dd[4]"));

                return element.Text;
            }
        }

        public string IGrewUpIn
        {
            get
            {
                IWebElement element = null;

                if (driver.Url.Contains("jdate.com"))
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'LifestyleTab.ascx_panelLifestyleTab')]/div/ul[2]/li[1]/span"));
                else if (driver.Url.Contains("spark.com"))
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'LifestyleTab.ascx_panelLifestyleTab')]/div/dl/dd[6]"));

                return element.Text;
            }
        }

        public string MyEthnicityIs
        {
            get
            {
                IWebElement element = null;

                if (driver.Url.Contains("jdate.com"))
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'LifestyleTab.ascx_panelLifestyleTab')]/div/ul[2]/li[2]/span"));
                else if (driver.Url.Contains("spark.com"))
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'BasicsTab.ascx_panelBasicsTab')]/div/dl/dd[7]"));
                if (driver.Url.Contains("blacksingles.com") || driver.Url.Contains("bbwpersonalsplus.com"))
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'LifestyleTab.ascx_panelLifestyleTab')]/div/dl/dd[14]"));
                return element.Text;
            }
        }

        public string ISpeak
        {
            get
            {
                IWebElement element = null;

                if (driver.Url.Contains("jdate.com"))
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'LifestyleTab.ascx_panelLifestyleTab')]/div/ul[2]/li[3]/span"));
                else if (driver.Url.Contains("spark.com"))
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'LifestyleTab.ascx_panelLifestyleTab')]/div/dl/dd[7]"));
                else if (driver.Url.Contains("blacksingles.com") || driver.Url.Contains("bbwpersonalsplus.com"))
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'LifestyleTab.ascx_panelLifestyleTab')]/div/dl/dd[15]"));
                return element.Text;
            }
        }

        public string SelfDescription
        {
            get
            {
                IWebElement element = null;

                if (driver.Url.Contains("blacksingles.com") || driver.Url.Contains("bbwpersonalsplus.com"))
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'LifestyleTab.ascx_panelLifestyleTab')]/div/ul[1]/li[1]/span"));

                return element.Text;
            }
        }

        public string IStudiedOrAmInterestedIn
        {
            get
            {
                IWebElement element = null;

                if (driver.Url.Contains("jdate.com"))
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'LifestyleTab.ascx_panelLifestyleTab')]/div/ul[2]/li[4]/span"));
                else if (driver.Url.Contains("spark.com"))
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'LifestyleTab.ascx_panelLifestyleTab')]/div/dl/dd[9]"));

                return element.Text;
            }
        }

        public string Education
        {
            get
            {
                IWebElement element = null;

                if (driver.Url.Contains("jdate.com"))
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'LifestyleTab.ascx_panelLifestyleTab')]/div/ul[2]/li[5]/span"));
                else if (driver.Url.Contains("spark.com"))
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'BasicsTab.ascx_panelBasicsTab')]/div/dl/dd[11]"));
                else if (driver.Url.Contains("blacksingles.com") || driver.Url.Contains("bbwpersonalsplus.com"))
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'LifestyleTab.ascx_panelLifestyleTab')]/div/dl/dd[12]"));

                return element.Text;
            }
        }

        public string FieldOfWork
        {
            get
            {
                IWebElement element = null;

                if (driver.Url.Contains("blacksingles.com") || driver.Url.Contains("bbwpersonalsplus.com"))
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'LifestyleTab.ascx_panelLifestyleTab')]/div/dl/dd[13]"));

                return element.Text;
            }
        }

        public string Occupation
        {
            get
            {
                IWebElement element = null;

                if (driver.Url.Contains("jdate.com"))
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'LifestyleTab.ascx_panelLifestyleTab')]/div/ul[2]/li[6]/span"));
                else if (driver.Url.Contains("spark.com"))
                {
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'BasicsTab.ascx_panelBasicsTab')]/div/dl/dd[12]"));

                    if (element.Text == "")
                        element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'LifestyleTab.ascx_panelLifestyleTab')]/div/dl/dd[10]"));
                }
                return element.Text;
            }
        }

        public string OccupationDescription
        {
            get
            {
                IWebElement element = null;

                if (driver.Url.Contains("jdate.com"))
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'LifestyleTab.ascx_panelLifestyleTab')]/div/ul[2]/li[7]/span"));
                else if (driver.Url.Contains("spark.com"))
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, '_LifestyleTab.ascx_panelLifestyleTab')]/div/dl/dd[11]"));

                return element.Text;
            }
        }

        public string AnnualIncome
        {
            get
            {
                IWebElement element = null;

                if (driver.Url.Contains("jdate.com"))
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'LifestyleTab.ascx_panelLifestyleTab')]/div/ul[2]/li[8]/span"));
                else if (driver.Url.Contains("spark.com"))
                {
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'BasicsTab.ascx_panelBasicsTab')]/div/dl/dd[13]"));

                    if (element.Text == "")
                        element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'LifestyleTab.ascx_panelLifestyleTab')]/div/dl/dd[12]"));
                }

                return element.Text;
            }
        }

        public string PoliticalOrientation
        {
            get
            {
                IWebElement element = null;

                if (driver.Url.Contains("jdate.com"))
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'LifestyleTab.ascx_panelLifestyleTab')]/div/ul[2]/li[9]/span"));
                else if (driver.Url.Contains("spark.com"))
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, '_LifestyleTab.ascx_panelLifestyleTab')]/div/dl/dd[13]"));
                else if (driver.Url.Contains("blacksingles.com") || driver.Url.Contains("bbwpersonalsplus.com"))
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'LifestyleTab.ascx_panelLifestyleTab')]/div/ul[1]/li[12]/span"));

                return element.Text;
            }
        }

        public string MyReligion
        {
            get
            {
                IWebElement element = null;

                if (driver.Url.Contains("jdate.com"))
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'LifestyleTab.ascx_panelLifestyleTab')]/div/ul[2]/li[10]/span"));
                else if (driver.Url.Contains("spark.com"))
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'BasicsTab.ascx_panelBasicsTab')]/div/dl/dd[8]"));
                else if (driver.Url.Contains("blacksingles.com") || driver.Url.Contains("bbwpersonalsplus.com"))
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'LifestyleTab.ascx_panelLifestyleTab')]/div/dl/dd[11]"));

                return element.Text;
            }
        }

        public string MyPersonalityIsBestDescribedAs
        {
            get
            {
                IWebElement element = null;

                if (driver.Url.Contains("jdate.com"))
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'InterestsTab.ascx_panelInterestsTab')]/div/ul/li[1]/span"));
                else if (driver.Url.Contains("spark.com"))
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'LifestyleTab.ascx_panelLifestyleTab')]/div/ul/li[1]/span"));

                return element.Text;
            }
        }

        public string InMyFreeTimeIEnjoy
        {
            get
            {
                IWebElement element = null;

                if (driver.Url.Contains("jdate.com"))
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'InterestsTab.ascx_panelInterestsTab')]/div/ul/li[2]/span"));
                else if (driver.Url.Contains("spark.com"))
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'LifestyleTab.ascx_panelLifestyleTab')]/div/ul/li[2]/span"));

                return element.Text;
            }
        }

        public string InMyFreeTimeILikeToGoTo
        {
            get
            {
                IWebElement element = null;

                if (driver.Url.Contains("jdate.com"))
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'InterestsTab.ascx_panelInterestsTab')]/div/ul/li[3]/span"));
                else if (driver.Url.Contains("spark.com"))
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'LifestyleTab.ascx_panelLifestyleTab')]/div/ul/li[3]/span"));

                return element.Text;
            }
        }

        public string MyFavoritePhysicalActivites
        {
            get
            {
                IWebElement element = null;

                if (driver.Url.Contains("jdate.com"))
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'InterestsTab.ascx_panelInterestsTab')]/div/ul/li[4]/span"));
                else if (driver.Url.Contains("spark.com"))
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'LifestyleTab.ascx_panelLifestyleTab')]/div/ul/li[4]/span"));
                else if (driver.Url.Contains("blacksingles.com") || driver.Url.Contains("bbwpersonalsplus.com"))
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'LifestyleTab.ascx_panelLifestyleTab')] /div/ul[1]/li[7]/span"));

                return element.Text;
            }
        }

        public string IOwnThesePets
        {
            get
            {
                IWebElement element = null;

                if (driver.Url.Contains("jdate.com"))
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'InterestsTab.ascx_panelInterestsTab')]/div/ul/li[5]/span"));
                else if (driver.Url.Contains("spark.com"))
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'LifestyleTab.ascx_panelLifestyleTab')]/div/ul/li[8]/span"));

                return element.Text;
            }
        }

        public string MyFavoriteFoods
        {
            get
            {
                IWebElement element = null;

                if (driver.Url.Contains("jdate.com"))
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'InterestsTab.ascx_panelInterestsTab')]/div/ul/li[6]/span"));
                else if (driver.Url.Contains("spark.com"))
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'LifestyleTab.ascx_panelLifestyleTab')]/div/ul/li[5]/span"));
                else if (driver.Url.Contains("blacksingles.com") || driver.Url.Contains("bbwpersonalsplus.com"))
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'LifestyleTab.ascx_panelLifestyleTab')]/div/ul[1]/li[10]/span"));

                return element.Text;
            }
        }

        public string MyFavoriteMusic
        {
            get
            {
                IWebElement element = null;

                if (driver.Url.Contains("jdate.com"))
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'InterestsTab.ascx_panelInterestsTab')]/div/ul/li[7]/span"));
                else if (driver.Url.Contains("spark.com"))
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'LifestyleTab.ascx_panelLifestyleTab')]/div/ul/li[6]/span"));
                else if (driver.Url.Contains("blacksingles.com") || driver.Url.Contains("bbwpersonalsplus.com"))
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'LifestyleTab.ascx_panelLifestyleTab')]/div/ul[1]/li[2]/span"));

                return element.Text;
            }
        }

        public string FavoriteBandsAndMusicians
        {
            get
            {
                IWebElement element = null;

                if (driver.Url.Contains("blacksingles.com") || driver.Url.Contains("bbwpersonalsplus.com"))
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'LifestyleTab.ascx_panelLifestyleTab')]/div/ul[1]/li[3]/span"));

                return element.Text;
            }
        }

        public string Movie
        {
            get
            {
                IWebElement element = null;

                if (driver.Url.Contains("blacksingles.com") || driver.Url.Contains("bbwpersonalsplus.com"))
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'LifestyleTab.ascx_panelLifestyleTab')]/div/ul[1]/li[4]/span"));

                return element.Text;
            }
        }

        public string FavoriteMoviesAndActors
        {
            get
            {
                IWebElement element = null;

                if (driver.Url.Contains("blacksingles.com") || driver.Url.Contains("bbwpersonalsplus.com"))
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'LifestyleTab.ascx_panelLifestyleTab')]/div/ul[1]/li[5]/span"));

                return element.Text;
            }
        }

        public string FavoriteTVShows
        {
            get
            {
                IWebElement element = null;

                if (driver.Url.Contains("blacksingles.com") || driver.Url.Contains("bbwpersonalsplus.com"))
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'LifestyleTab.ascx_panelLifestyleTab')]/div/ul[1]/li[6]/span"));

                return element.Text;
            }
        }

        public string IndoorActivities
        {
            get
            {
                IWebElement element = null;

                if (driver.Url.Contains("blacksingles.com") || driver.Url.Contains("bbwpersonalsplus.com"))
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'LifestyleTab.ascx_panelLifestyleTab')]/div/ul[1]/li[8]/span"));

                return element.Text;
            }
        }

        public string MyIdeaOfAGreatTrip
        {
            get
            {
                IWebElement element = null;

                if (driver.Url.Contains("blacksingles.com") || driver.Url.Contains("bbwpersonalsplus.com"))
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'LifestyleTab.ascx_panelLifestyleTab')]/div/ul[1]/li[9]/span"));

                return element.Text;
            }
        }

        public string FavoriteRestaurants
        {
            get
            {
                IWebElement element = null;

                if (driver.Url.Contains("blacksingles.com") || driver.Url.Contains("bbwpersonalsplus.com"))
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'LifestyleTab.ascx_panelLifestyleTab')]/div/ul[1]/li[11]/span"));

                return element.Text;
            }
        }

        public string SchoolsAttended
        {
            get
            {
                IWebElement element = null;

                if (driver.Url.Contains("blacksingles.com") || driver.Url.Contains("bbwpersonalsplus.com"))
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'LifestyleTab.ascx_panelLifestyleTab')]/div/ul[1]/li[13]/span"));

                return element.Text;
            }
        }

        public string Timeliness
        {
            get
            {
                IWebElement element = null;

                if (driver.Url.Contains("blacksingles.com") || driver.Url.Contains("bbwpersonalsplus.com"))
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'LifestyleTab.ascx_panelLifestyleTab')]/div/ul[1]/li[14]/span"));

                return element.Text;
            }
        }

        public string AsForFashion
        {
            get
            {
                IWebElement element = null;

                if (driver.Url.Contains("blacksingles.com") || driver.Url.Contains("bbwpersonalsplus.com"))
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'LifestyleTab.ascx_panelLifestyleTab')]/div/ul[1]/li[15]/span"));

                return element.Text;
            }
        }

        public string ILikeToRead
        {
            get
            {
                IWebElement element = null;

                if (driver.Url.Contains("jdate.com"))
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'InterestsTab.ascx_panelInterestsTab')]/div/ul/li[8]/span"));
                else if (driver.Url.Contains("spark.com"))
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'LifestyleTab.ascx_panelLifestyleTab')]/div/ul/li[7]/span"));

                return element.Text;
            }
        }

        public string MyIdealRelationship
        {
            get
            {
                IWebElement element = null;

                if (driver.Url.Contains("jdate.com"))
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'RelationshipTab.ascx_panelRelationshipTab')]/div/p[1]"));
                else if (driver.Url.Contains("spark.com"))
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'RelationshipTab.ascx_panelRelationshipTab')]/div/p[3]"));

                return element.Text;
            }
        }

        public string MyPastRelationships
        {
            get
            {
                IWebElement element = null;

                if (driver.Url.Contains("jdate.com"))
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'RelationshipTab.ascx_panelRelationshipTab')]/div/p[2]"));
                else if (driver.Url.Contains("spark.com"))
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'RelationshipTab.ascx_panelRelationshipTab')]/div/p[4]"));

                return element.Text;
            }
        }

        public string IAmLookingForA
        {
            get
            {
                IWebElement element = null;

                if (driver.Url.Contains("jdate.com"))
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'RelationshipTab.ascx_panelRelationshipTab')]/div/p[3]"));
                else if (driver.Url.Contains("spark.com"))
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'RelationshipTab.ascx_panelRelationshipTab')]/div/p[1]"));

                return element.Text;
            }
        }

        public string MyPerfectFirstDate
        {
            get
            {
                IWebElement element = null;

                if (driver.Url.Contains("jdate.com"))
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'RelationshipTab.ascx_panelRelationshipTab')]/div/p[4]"));
                else if (driver.Url.Contains("spark.com"))
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'RelationshipTab.ascx_panelRelationshipTab')]/div/p[2]"));

                return element.Text;
            }
        }

        public string AgeRange
        {
            get
            {
                IWebElement element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'RelationshipTab.ascx_panelRelationshipTab')]/div/ul/li[1]/span"));
                return element.Text;
            }
        }

        public string RelationshipStatus
        {
            get
            {
                IWebElement element = null;

                if (driver.Url.Contains("blacksingles.com") || driver.Url.Contains("bbwpersonalsplus.com"))
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'LifestyleTab.ascx_panelLifestyleTab')]/div/dl/dd[5]"));
                else
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'RelationshipTab.ascx_panelRelationshipTab')]/div/ul/li[2]/span"));

                return element.Text;
            }
        }

        public string ReligiousBackground
        {
            get
            {
                IWebElement element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'RelationshipTab.ascx_panelRelationshipTab')]/div/ul/li[3]/span"));
                return element.Text;
            }
        }

        public string EducationLevel
        {
            get
            {
                IWebElement element = null;

                if (driver.Url.Contains("jdate.com"))
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'RelationshipTab.ascx_panelRelationshipTab')]/div/ul/li[4]/span"));
                else if (driver.Url.Contains("spark.com"))
                {
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'LifestyleTab.ascx_panelLifestyleTab')]/div/dl/dd[8]"));

                    if (element.Text == "")
                        element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'RelationshipTab.ascx_panelRelationshipTab')]/div/ul/li[6]/span"));
                }
                return element.Text;
            }
        }

        public string DrinkingHabits
        {
            get
            {
                IWebElement element = null;

                if (driver.Url.Contains("jdate.com"))
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'RelationshipTab.ascx_panelRelationshipTab')]/div/ul/li[5]/span"));
                else if (driver.Url.Contains("spark.com"))
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'RelationshipTab.ascx_panelRelationshipTab')]/div/ul/li[7]/span"));
                else if (driver.Url.Contains("blacksingles.com") || driver.Url.Contains("bbwpersonalsplus.com"))
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'LifestyleTab.ascx_panelLifestyleTab')]/div/dl/dd[10]"));

                return element.Text;
            }
        }

        public string SmokingHabits
        {
            get
            {
                IWebElement element = null;

                if (driver.Url.Contains("jdate.com"))
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'RelationshipTab.ascx_panelRelationshipTab')]/div/ul/li[6]/span"));
                else if (driver.Url.Contains("spark.com"))
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'RelationshipTab.ascx_panelRelationshipTab')]/div/ul/li[8]/span"));
                else if (driver.Url.Contains("blacksingles.com") || driver.Url.Contains("bbwpersonalsplus.com"))
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'LifestyleTab.ascx_panelLifestyleTab')]/div/dl/dd[9]"));

                return element.Text;
            }
        }

        /// <summary>
        /// BlackSingles only
        /// </summary>
        public string YouAreA
        {
            get
            {
                IWebElement element = WaitUntilElementExists(By.XPath("//div[@id='profile-cont']/div[2]/ul[1]/li[1]"));
                return element.Text;
            }
        }

        public string WhatWouldILikeToDoOnAFirstDate
        {
            get
            {
                IWebElement element = null;

                element = WaitUntilElementExists(By.XPath("//dl[@id='essays-view-profile']/dd[1]"));

                return element.Text.Replace("A. ", "");
            }
        }

        public string WhyYouShouldGetToKnowMe
        {
            get
            {
                IWebElement element = null;

                element = WaitUntilElementExists(By.XPath("//dl[@id='essays-view-profile']/dd[2]"));

                return element.Text.Replace("A. ", "");
            }
        }

        public string WhatGoodThingsHavePastRelationshipsTaughtMe
        {
            get
            {
                IWebElement element = null;

                element = WaitUntilElementExists(By.XPath("//dl[@id='essays-view-profile']/dd[3]"));

                return element.Text.Replace("A. ", "");
            }
        }

        public string SomeOfTheMostImportantThingsInMyLifeAre
        {
            get
            {
                IWebElement element = null;

                element = WaitUntilElementExists(By.XPath("//dl[@id='essays-view-profile']/dd[4]"));

                return element.Text.Replace("A. ", "");
            }
        }

        public string HowWouldIDescribeMyPerfectDay
        {
            get
            {
                IWebElement element = null;

                element = WaitUntilElementExists(By.XPath("//dl[@id='essays-view-profile']/dd[5]"));

                return element.Text.Replace("A. ", "");
            }
        }

        public string AFewMoreThingsIWouldLikeToAdd
        {
            get
            {
                IWebElement element = null;

                element = WaitUntilElementExists(By.XPath("//dl[@id='essays-view-profile']/dd[6]"));

                return element.Text.Replace("A. ", "");
            }
        }

        #region Objects that appear when viewing other peoples' profile pages - Email Me Now!, Flirt, E-Card, Secret Admirer, etc.
        public Framework.Site_Pages.ComposeMessage EmailMeNowButton_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//img[contains(@id, 'DynamicTabGroup1_ProfileTabCommunicationButtons1_ImageEmailMe')]"));
            ClickUntilURLChanges(element);

            return new ComposeMessage(driver);
        }

        /// <summary>
        /// For Black Singles users who are not subscribed, attempting to email another person takes the user to the Subscribe Page
        /// </summary>
        /// <returns></returns>
        public Framework.Site_Pages.Subscribe EmailButton_NonSubscribedBlackUser_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//a[contains(@id, 'MingleTop1_lnkEmailMe')]/span[2]"));
            ClickUntilURLChanges(element);

            return new Subscribe(driver);
        }

        public Framework.Site_Pages.ComposeMessage EmailButton_SubscribedBlackUser_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//a[contains(@id, 'MingleTop1_lnkEmailMe')]/span[2]"));
            ClickUntilURLChanges(element);

            return new ComposeMessage(driver);
        }

        /// <summary>
        /// For BBW users who are not subscribed, attempting to email another person takes the user to the Subscribe Page
        /// </summary>
        /// <returns></returns>
        public Framework.Site_Pages.Subscribe EmailButton_NonSubscribedBBWUser_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//a[contains(@id, 'MingleTop1_lnkEmailMe')]/span[2]"));
            ClickUntilURLChanges(element);

            return new Subscribe(driver);
        }

        public Framework.Site_Pages.ComposeMessage EmailButton_SubscribedBBWUser_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//a[contains(@id, 'MingleTop1_lnkEmailMe')]/span[2]"));
            ClickUntilURLChanges(element);

            return new ComposeMessage(driver);
        }

        /// <summary>
        /// For Black Singles users who are not subscribed, attempting to leave a comment on another person's page takes the user to the Subscribe Page
        /// </summary>
        /// <returns></returns>
        public Framework.Site_Pages.Subscribe UpgradeNowToSendAMessageButton_NonSubscribedBlackUser_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'MingleTop1_panelUnSub')]/div/a"));
            ClickUntilURLChanges(element);

            return new Subscribe(driver);
        }
        #endregion

        public Framework.Site_Pages.YourProfile TryOurProfileBetaNow()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//a[contains(@id, '_DynamicTabGroup1_lnkProfile30BetaToggler')]"));
            ClickUntilURLChanges(element);

            return new YourProfile(driver);
        }
    }
}
