﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.Events;
using NUnit.Framework;
using log4net;

// PAGE URL:  http://www.jdate.com/jewish-holidays/

namespace WebdriverAutomation.Framework.Site_Pages
{
    public class JewishHolidayCalendar : BasePage
    {
        public JewishHolidayCalendar(IWebDriver driver)
            : base(driver)
        {
            // verify we are on the correct page       
            IWebElement textOnPage = null;

            textOnPage = WaitUntilPageVerificationObjectExists(By.XPath("//div[@id='nav_container']/div[@id='nav_main']/h1"), "JEWISH HOLIDAY CALENDAR PAGE");

            if (!textOnPage.Text.Contains(textThatVerifiesThisPageIsCorrect))
                log.Error("ERROR - JEWISH HOLIDAY CALENDAR PAGE - We should be on this page but the page verification failed.");

            Assert.True(textOnPage.Text.Contains(textThatVerifiesThisPageIsCorrect), "ERROR - JEWISH HOLIDAY CALENDAR PAGE - We should be on this page but the page verification failed.");

        }

        private static readonly ILog log = LogManager.GetLogger(typeof(JewishHolidayCalendar));

        private const string textThatVerifiesThisPageIsCorrect = "Jewish Holidays:";



    }
}
