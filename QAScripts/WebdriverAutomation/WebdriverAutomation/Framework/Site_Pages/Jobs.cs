﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.Events;
using NUnit.Framework;
using log4net;

// PAGE URL:  http://www.spark.net/careers.htm

namespace WebdriverAutomation.Framework.Site_Pages
{
    public class Jobs : BasePage
    {
        public Jobs(IWebDriver driver)
            : base(driver)
        {
            // verify we are on the correct page       
            IWebElement textOnPage = null;

            textOnPage = WaitUntilPageVerificationObjectExists(By.XPath("//div[@id='main']/div/h1"), "JOBS PAGE");
            
            if (textOnPage.Text != (textThatVerifiesThisPageIsCorrect))
                log.Error("ERROR - JOBS PAGE - We should be on this page but the page verification failed.");

            Assert.True(textOnPage.Text == textThatVerifiesThisPageIsCorrect, "ERROR - JOBS PAGE - We should be on this page but the page verification failed.");
        }

        private static readonly ILog log = LogManager.GetLogger(typeof(Jobs));

        private const string textThatVerifiesThisPageIsCorrect = "Ignite Your Career Passion at Spark!";



    }
}
