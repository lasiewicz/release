﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.Events;
using NUnit.Framework;
using log4net;
using System.Threading;

// PAGE URL:  http://www.jdate.co.il/Applications/CompatibilityMeter/Welcome.aspx

namespace WebdriverAutomation.Framework.Site_Pages
{
    public class JMeterWelcome : BasePage
    {
        public JMeterWelcome(IWebDriver driver)
            : base(driver)
        {
            Header = new WebdriverAutomation.Framework.Mobile_Parts.Header(driver);

            // verify we are on the correct page       
            IWebElement textOnPage = null;

            textOnPage = WaitUntilPageVerificationObjectExists(By.XPath("//div[@id='jmeter-main']/div[2]/div/h1[2]"), "JMETER WELCOME PAGE");
            
            if (!textOnPage.Text.Contains(textThatVerifiesThisPageIsCorrect))
                log.Error("ERROR - JMETER WELCOME PAGE - We should be on this page but the page verification failed.");

            Assert.True(textOnPage.Text.Contains(textThatVerifiesThisPageIsCorrect), "ERROR - JMETER WELCOME PAGE - We should be on this page but the page verification failed.");

        }

        private static readonly ILog log = LogManager.GetLogger(typeof(JMeterWelcome));
        public WebdriverAutomation.Framework.Mobile_Parts.Header Header;

        private const string textThatVerifiesThisPageIsCorrect = "ברוכים הבאים ל-";

        public Framework.Site_Pages.JMeterPersonalityTest Start_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//input[contains(@id, '_btnStartTest')]"));
            ClickUntilURLChanges(element);

            return new Framework.Site_Pages.JMeterPersonalityTest(driver);
        }


    }
}
