﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.Events;
using NUnit.Framework;
using log4net;
using System.Threading;

// PAGE URL:  http://www.jdate.com/Applications/Home/default.aspx?NavPoint=top (while logged in)

namespace WebdriverAutomation.Framework.Site_Pages
{
    public class Home : BasePage
    {
        public Home(IWebDriver driver)
            : base(driver)
        {
            // JDATE ONLY - Sometimes if we end up at Home it's because we're coming back from the Subscribe Page and instead the Smart JDaters Subscribe Page 
            //    appears instead.  
            int maxLoopCount = 5;

            if (driver.Url.Contains("jdate.com"))
            {
                for (int j = 0; j < maxLoopCount; j++)
                {
                    if (driver.FindElements(By.XPath("//div[@id='sub-dont-go']/h1")).Count > 0)
                    {
                        IWebElement element = driver.FindElement(By.XPath("//a[contains(@id, '_lnkCancel')]"));
                        element.Click();
                    }
                    else
                        break;

                    if (j == maxLoopCount - 1)
                        Assert.Fail("We are trying to log out of the UPS Page and instead end up on the Don't Go Page.  We try to exit this page as well to log out " + maxLoopCount + " times and are unsuccessful.");
                }
            }

            Header = new WebdriverAutomation.Framework.Site_Parts.Header(driver);
            Footer = new WebdriverAutomation.Framework.Site_Parts.Footer(driver);

            if (driver.Url.Contains("jdate.co.il"))
                textThatVerifiesThisPageIsCorrect = "שלום";
            else
                textThatVerifiesThisPageIsCorrect = "Welcome";

            Tests.ReusableCode.Ads ads = new Tests.ReusableCode.Ads();
            ads.KillAllAds(driver, log);

            // JDate women of certain member ID numbers also get another popup "We've made connecting easy! Be bold and send a FREE Flirt to up to five matches at once!"
            //     Get rid of this too.
            if (driver.Url.Contains("jdate.com"))
            {
                if (driver.FindElements(By.XPath("//div[@id='pushFlirtsContainer']")).Count > 0)
                {
                    if (driver.FindElement(By.XPath("//div[@id='pushFlirtsContainer']")).Displayed == true)
                    {
                        IWebElement xButton = null;

                        if (driver.FindElements(By.XPath("//html/body/div[2]/div[1]/a/span")).Count > 0)                        
                            xButton = driver.FindElement(By.XPath("//html/body/div[2]/div[1]/a/span"));
                        else
                            xButton = driver.FindElement(By.XPath("//span[@class='ui-icon ui-icon-closethick']"));

                        xButton.Click();
                    }
                }
            }

            // Get rid of "Are You a Secret Admirer?"
            if (driver.Url.Contains("jdate.com"))
            {
                if (driver.FindElements(By.XPath("//div[@id='slideshowSecretAdmirerPopup']")).Count > 0)
                {
                    if (driver.FindElement(By.XPath("//div[@id='slideshowSecretAdmirerPopup']")).Displayed == true)
                    {
                        IWebElement element = driver.FindElement(By.XPath("//div[@class='ui-dialog-plain-close link-style']"));
                        element.Click();
                    }
                }
            }

            // verify we are on the correct page       
            IWebElement textOnPage = null;

            textOnPage = WaitUntilPageVerificationObjectExists(By.XPath("//ul[@id='nav-auxiliary']/li[1]"), "HOME PAGE");

            if (!textOnPage.Text.Contains(textThatVerifiesThisPageIsCorrect))
                log.Error("ERROR - HOME PAGE - We should be on this page but the page verification failed.");

            Assert.True(textOnPage.Text.Contains(textThatVerifiesThisPageIsCorrect), "ERROR - HOME PAGE - We should be on this page but the page verification failed.");

        }

        private static readonly ILog log = LogManager.GetLogger(typeof(Login));
        public WebdriverAutomation.Framework.Site_Parts.Header Header;
        public WebdriverAutomation.Framework.Site_Parts.Footer Footer;

        private string textThatVerifiesThisPageIsCorrect = string.Empty;
        #region Who's Checking You Out? (left panel)

        #endregion

        #region Shortcuts (left panel)

        #endregion
    }
}
