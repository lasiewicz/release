﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.Events;
using NUnit.Framework;
using log4net;
using System.Threading;

// PAGE URL:  http://www.jdate.com/Applications/MemberProfile/ViewProfile.aspx?EntryPoint=99999&NavPoint=top (while logged in)
//  This is the page for both the user's Profile page and any otehr Profile that you are currently viewing besides your page.
//  This is also specifically for the (currently beta) new Profile page

namespace WebdriverAutomation.Framework.Site_Pages
{
    public class YourProfile : BasePage
    {
        public YourProfile(IWebDriver driver)
            : base(driver)
        {
            Header = new WebdriverAutomation.Framework.Site_Parts.Header(driver);
            Footer = new WebdriverAutomation.Framework.Site_Parts.Footer(driver);

            Tests.ReusableCode.Ads ads = new Tests.ReusableCode.Ads();
            ads.KillAllAds(driver, log);

            // initially assume that we are on an new profile and see if the "In My Own Words" tab exists
            IWebElement textOnPage = null;

            if (driver.FindElements(By.XPath("//div[@id='beta-toggler']/a[contains(@id, '_DynamicTabGroup1_lnkProfile30BetaToggler')]")).Count > 0)
                Assert.Fail("We are currently viewing a old Profile Beta site.  If we intend to be viewing the new Profile Page, turn Profile Beta on, then try running this test case again");

            if (driver.Url.Contains("jdate.co.il"))
                textThatVerifiesThisPageIsCorrectNewProfile = "מי אני?";
            else
                textThatVerifiesThisPageIsCorrectNewProfile = "In My Own Words";

            textOnPage = WaitUntilPageVerificationObjectExists(By.XPath("//li[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabs__ctl0_tabItem')]/a"), "PROFILE PAGE");

            if (textOnPage.Text != textThatVerifiesThisPageIsCorrectNewProfile)
                log.Error("ERROR - NEW PROFILE PAGE - We should be on this page but the page verification failed.");

            Assert.True(textOnPage.Text == textThatVerifiesThisPageIsCorrectNewProfile, "ERROR - NEW PROFILE PAGE - We should be on this page but the page verification failed.");
        }

        private static readonly ILog log = LogManager.GetLogger(typeof(Login));
        public WebdriverAutomation.Framework.Site_Parts.Header Header;
        public WebdriverAutomation.Framework.Site_Parts.Footer Footer;

        private string textThatVerifiesThisPageIsCorrectNewProfile = string.Empty;

        public void MoreAboutMeTab_Click()
        {
            int loopCount = 5;

            for (int i = 0; i < loopCount; i++)
            {
                IWebElement element = WaitUntilElementExists(By.XPath("//li[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabs__ctl1_tabItem')]/a"));
                element.Click();

                IWebElement firstItemInTab = WaitUntilElementExists(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl0_essayItem')]/h3"));

                if (firstItemInTab.Text == "My personality is best described as…" || firstItemInTab.Text == "Favorite types of music" || firstItemInTab.Text == "My perfect first date" || firstItemInTab.Text == "הדייט המושלם בעיני")
                    break;

                if (i == loopCount - 2)
                {
                    IAlert alert = driver.SwitchTo().Alert();
                    alert.Dismiss();
                }

                if (i == loopCount - 1)
                    // if it gets to this point it could be because a messagebox with the text "error" appears across it
                    Assert.Fail("We tried to click on the More Than Me tab " + loopCount.ToString() + " times but failed.");
            }
        }

        public Framework.Site_Pages.ManageYourPhotos ManageYourPhotosButton_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//a[contains(@id, '_StandardProfileTemplate1_PhotoSlider1_lnkEditPhotos')]"));
            ClickUntilURLChanges(element);

            return new ManageYourPhotos(driver);
        }

        public Framework.Site_Pages.Subscribe UpgradeNowButton_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_m2mCommunication1_panelNonSub')]/div/a/em"));
            ClickUntilURLChanges(element);

            return new Subscribe(driver);
        }

        public Framework.Site_Pages.ComposeMessage EmailMeNowButton_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//a[contains(@id, '_StandardProfileTemplate1_lnkEmailMeBottom')]"));
            ClickUntilURLChanges(element);

            return new ComposeMessage(driver);
        }

        public string SendMeAnEmailTextarea
        {
            get
            {
                IWebElement element = WaitUntilElementExists(By.XPath("//textarea[@id='_ctl0__ctl4_StandardProfileTemplate1_m2mCommunication1_txtEmailMessage_WatermarkTextBox1']"));
                return element.GetAttribute("value");
            }
            set
            {
                string theXpath = "//textarea[@id='_ctl0__ctl4_StandardProfileTemplate1_m2mCommunication1_txtEmailMessage_WatermarkTextBox1']";

                WaitUntilElementValueCanBeCleared(By.XPath(theXpath));
                SendKeys(WaitUntilElementExists(By.XPath(theXpath)), value);
            }
        }

        public Framework.Site_Pages.Subscribe SendButton_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//input[@id='_ctl0__ctl4_StandardProfileTemplate1_m2mCommunication1_buttonSendEmail']"));
            ClickUntilURLChanges(element);

            return new Subscribe(driver);
        }

        #region In My Own Words tab
        public string MyIntroduction
        {
            get
            {
                IWebElement element = WaitUntilElementExists(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl0__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl0_essayItem')]/p"));
                return element.Text;
            }
            set
            {
                IWebElement element = null;

                if (driver.Url.Contains("blacksingles.com"))
                    element = driver.FindElement(By.XPath("//div[@id='tab-Essays']/div[@id='_ctl0__ctl3_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl0__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl0_essayItem']"));
                else
                    element = driver.FindElement(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl0__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl0_essayItem')]"));

                RemoteWebElement clickToEdit = (RemoteWebElement)element;
                HoverClick(clickToEdit);

                string theXpath = "//textarea[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl0__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl0__ctl0_txtEdit')]";

                WaitUntilElementValueCanBeCleared(By.XPath(theXpath));
                SendKeys(WaitUntilElementExists(By.XPath(theXpath)), value);

                IWebElement save = driver.FindElement(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl0__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl0_btnEditSave')]"));
                save.Click();
            }
        }

        public string WhatWouldILikeToDoOnAFirstDate
        {
            get
            {
                IWebElement element = WaitUntilElementExists(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl0__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl1_essayItem')]/p"));
                return element.Text;
            }
            set
            {
                IWebElement element = driver.FindElement(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl0__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl1_essayItem')]/h3"));
                RemoteWebElement clickToEdit = (RemoteWebElement)element;
                HoverClick(clickToEdit);

                string theXpath = "//textarea[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl0__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl1__ctl0_txtEdit')]";

                WaitUntilElementValueCanBeCleared(By.XPath(theXpath));
                SendKeys(WaitUntilElementExists(By.XPath(theXpath)), value);

                IWebElement save = driver.FindElement(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl0__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl1_btnEditSave')]"));
                save.Click();
            }
        }

        public string WhyYouShouldGetToKnowMe
        {
            get
            {
                IWebElement element = WaitUntilElementExists(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl0__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl2_essayItem')]/p"));
                return element.Text;
            }
            set
            {
                IWebElement element = driver.FindElement(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl0__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl2_essayItem')]/h3"));
                RemoteWebElement clickToEdit = (RemoteWebElement)element;
                HoverClick(clickToEdit);

                string theXpath = "//textarea[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl0__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl2__ctl0_txtEdit')]";

                WaitUntilElementValueCanBeCleared(By.XPath(theXpath));
                SendKeys(WaitUntilElementExists(By.XPath(theXpath)), value);

                IWebElement save = driver.FindElement(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl0__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl2_btnEditSave')]"));
                save.Click();
            }
        }

        public string WhatGoodThingsHavePastRelationshipsTaughtMe
        {
            get
            {
                IWebElement element = WaitUntilElementExists(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl0__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl3_essayItem')]/p"));
                return element.Text;
            }
            set
            {
                IWebElement element = driver.FindElement(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl0__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl3_essayItem')]/h3"));
                RemoteWebElement clickToEdit = (RemoteWebElement)element;
                HoverClick(clickToEdit);

                string theXpath = "//textarea[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl0__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl3__ctl0_txtEdit')]";
                WaitUntilElementValueCanBeCleared(By.XPath(theXpath));
                SendKeys(WaitUntilElementExists(By.XPath(theXpath)), value);

                IWebElement save = driver.FindElement(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl0__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl3_btnEditSave')]"));
                save.Click();
            }
        }

        public string SomeOfTheMostImportantThingsInMyLifeAre
        {
            get
            {
                IWebElement element = WaitUntilElementExists(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl0__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl4_essayItem')]/p"));
                return element.Text;
            }
            set
            {
                IWebElement element = null;

                if (driver.Url.Contains("blacksingles.com"))
                    element = driver.FindElement(By.XPath("//div[@id='tab-Essays']/div[@id='_ctl0__ctl3_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl0__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl4_essayItem']"));
                else
                    element = driver.FindElement(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl0__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl4_essayItem')]/h3"));

                RemoteWebElement clickToEdit = (RemoteWebElement)element;
                HoverClick(clickToEdit);

                string theXpath = "//textarea[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl0__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl4__ctl0_txtEdit')]";

                WaitUntilElementValueCanBeCleared(By.XPath(theXpath));
                SendKeys(WaitUntilElementExists(By.XPath(theXpath)), value);

                IWebElement save = driver.FindElement(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl0__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl4_btnEditSave')]"));
                save.Click();
            }
        }

        public string HowWouldIDescribeMyPerfectDay
        {
            get
            {
                IWebElement element = WaitUntilElementExists(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl0__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl5_essayItem')]/p"));
                return element.Text;
            }
            set
            {
                IWebElement element = driver.FindElement(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl0__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl5_essayItem')]/h3"));
                RemoteWebElement clickToEdit = (RemoteWebElement)element;
                HoverClick(clickToEdit);

                string theXpath = "//textarea[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl0__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl5__ctl0_txtEdit')]";

                WaitUntilElementValueCanBeCleared(By.XPath(theXpath));
                SendKeys(WaitUntilElementExists(By.XPath(theXpath)), value);

                IWebElement save = driver.FindElement(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl0__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl5_btnEditSave')]"));
                save.Click();
            }
        }

        public string AFewMoreThingsIWouldLikeToAdd
        {
            get
            {
                IWebElement element = WaitUntilElementExists(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl0__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl6_essayItem')]/p"));
                return element.Text;
            }
            set
            {
                IWebElement element = null;

                if (driver.Url.Contains("blacksingles.com"))
                    element = driver.FindElement(By.XPath("//div[@id='tab-Essays']/div[@id='_ctl0__ctl3_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl0__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl6_essayItem']"));
                else
                    element = driver.FindElement(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl0__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl6_essayItem')]/h3"));

                RemoteWebElement clickToEdit = (RemoteWebElement)element;
                HoverClick(clickToEdit);

                string theXpath = "//textarea[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl0__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl6__ctl0_txtEdit')]";

                WaitUntilElementValueCanBeCleared(By.XPath(theXpath));
                SendKeys(WaitUntilElementExists(By.XPath(theXpath)), value);

                IWebElement save = driver.FindElement(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl0__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl6_btnEditSave')]"));
                save.Click();
            }
        }
        #endregion

        #region In My Own Words tab (JDate)
        public string AboutMe
        {
            get
            {
                IWebElement element = WaitUntilElementExists(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl0__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl0_essayItem')]/p"));
                return element.Text;
            }
            set
            {
                IWebElement element = driver.FindElement(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl0__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl0_essayItem')]/h3"));
                RemoteWebElement clickToEdit = (RemoteWebElement)element;
                HoverClick(clickToEdit);

                string theXpath = "//textarea[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl0__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl0__ctl0_txtEdit')]";

                WaitUntilElementValueCanBeCleared(By.XPath(theXpath));
                SendKeys(WaitUntilElementExists(By.XPath(theXpath)), value);

                IWebElement save = driver.FindElement(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl0__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl0_btnEditSave')]"));
                save.Click();
            }
        }

        public string MyLifeAndAmbitions
        {
            get
            {
                IWebElement element = WaitUntilElementExists(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl0__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl1_essayItem')]/p"));
                return element.Text;
            }
            set
            {
                IWebElement element = driver.FindElement(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl0__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl1_essayItem')]/h3"));
                RemoteWebElement clickToEdit = (RemoteWebElement)element;
                HoverClick(clickToEdit);

                string theXpath = "//textarea[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl0__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl1__ctl0_txtEdit')]";

                WaitUntilElementValueCanBeCleared(By.XPath(theXpath));
                SendKeys(WaitUntilElementExists(By.XPath(theXpath)), value);

                IWebElement save = driver.FindElement(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl0__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl1_btnEditSave')]"));
                save.Click();
            }
        }

        public string ABriefHistoryOfMyLife
        {
            get
            {
                IWebElement element = WaitUntilElementExists(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl0__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl2_essayItem')]/p"));
                return element.Text;
            }
            set
            {
                IWebElement element = driver.FindElement(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl0__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl2_essayItem')]/h3"));
                RemoteWebElement clickToEdit = (RemoteWebElement)element;
                HoverClick(clickToEdit);

                string theXpath = "//textarea[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl0__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl2__ctl0_txtEdit')]";

                WaitUntilElementValueCanBeCleared(By.XPath(theXpath));
                SendKeys(WaitUntilElementExists(By.XPath(theXpath)), value);

                IWebElement save = driver.FindElement(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl0__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl2_btnEditSave')]"));
                save.Click();
            }
        }

        public string OnOurFirstDateRemindMeToTellYouTheStoryAbout
        {
            get
            {
                if (driver.Url.Contains("jdate.com"))
                {
                    IWebElement element = WaitUntilElementExists(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl0__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl4_essayItem')]/p"));
                    return element.Text;
                }
                else
                {
                    IWebElement element = WaitUntilElementExists(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl0__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl3_essayItem')]/p"));
                    return element.Text;
                }
            }
            set
            {
                if (driver.Url.Contains("jdate.com"))
                {
                    IWebElement element = driver.FindElement(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl0__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl4_essayItem')]/h3"));
                    RemoteWebElement clickToEdit = (RemoteWebElement)element;
                    HoverClick(clickToEdit);

                    string theXpath = "//textarea[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl0__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl4__ctl0_txtEdit')]";

                    WaitUntilElementValueCanBeCleared(By.XPath(theXpath));
                    SendKeys(WaitUntilElementExists(By.XPath(theXpath)), value);

                    IWebElement save = driver.FindElement(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl0__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl4_btnEditSave')]"));
                    save.Click();
                }
                else
                {
                    IWebElement element = driver.FindElement(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl0__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl3_essayItem')]/h3"));
                    RemoteWebElement clickToEdit = (RemoteWebElement)element;
                    HoverClick(clickToEdit);

                    string theXpath = "//textarea[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl0__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl3__ctl0_txtEdit')]";

                    WaitUntilElementValueCanBeCleared(By.XPath(theXpath));
                    SendKeys(WaitUntilElementExists(By.XPath(theXpath)), value);

                    IWebElement save = driver.FindElement(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl0__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl3_btnEditSave')]"));
                    save.Click();
                }
            }
        }

        public string TheThingsICouldNeverLiveWithout
        {
            get
            {
                if (driver.Url.Contains("jdate.com"))
                {
                    IWebElement element = WaitUntilElementExists(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl0__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl5_essayItem')]/p"));
                    return element.Text;
                }
                else
                {
                    IWebElement element = WaitUntilElementExists(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl0__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl4_essayItem')]/p"));
                    return element.Text;
                }
            }
            set
            {
                if (driver.Url.Contains("jdate.com"))
                {
                    IWebElement element = driver.FindElement(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl0__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl5_essayItem')]/h3"));
                    RemoteWebElement clickToEdit = (RemoteWebElement)element;
                    HoverClick(clickToEdit);

                    string theXpath = "//textarea[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl0__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl5__ctl0_txtEdit')]";

                    WaitUntilElementValueCanBeCleared(By.XPath(theXpath));
                    SendKeys(WaitUntilElementExists(By.XPath(theXpath)), value);

                    IWebElement save = driver.FindElement(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl0__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl5_btnEditSave')]"));
                    save.Click();
                }
                else
                {
                    IWebElement element = driver.FindElement(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl0__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl4_essayItem')]/h3"));
                    RemoteWebElement clickToEdit = (RemoteWebElement)element;
                    HoverClick(clickToEdit);

                    string theXpath = "//textarea[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl0__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl4__ctl0_txtEdit')]";

                    WaitUntilElementValueCanBeCleared(By.XPath(theXpath));
                    SendKeys(WaitUntilElementExists(By.XPath(theXpath)), value);

                    IWebElement save = driver.FindElement(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl0__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl4_btnEditSave')]"));
                    save.Click();
                }
            }
        }

        public string MyFavoriteBooksMoviesTVShowsMusicAndFood
        {
            get
            {
                if (driver.Url.Contains("jdate.com"))
                {
                    IWebElement element = WaitUntilElementExists(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl0__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl6_essayItem')]/p"));
                    return element.Text;
                }
                else
                {
                    IWebElement element = WaitUntilElementExists(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl0__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl5_essayItem')]/p"));
                    return element.Text;
                }
            }
            set
            {
                if (driver.Url.Contains("jdate.com"))
                {
                    IWebElement element = driver.FindElement(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl0__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl6_essayItem')]/h3"));
                    RemoteWebElement clickToEdit = (RemoteWebElement)element;
                    HoverClick(clickToEdit);

                    string theXpath = "//textarea[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl0__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl6__ctl0_txtEdit')]";

                    WaitUntilElementValueCanBeCleared(By.XPath(theXpath));
                    SendKeys(WaitUntilElementExists(By.XPath(theXpath)), value);

                    IWebElement save = driver.FindElement(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl0__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl6_btnEditSave')]"));
                    save.Click();
                }
                else
                {
                    IWebElement element = driver.FindElement(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl0__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl5_essayItem')]/h3"));
                    RemoteWebElement clickToEdit = (RemoteWebElement)element;
                    HoverClick(clickToEdit);

                    string theXpath = "//textarea[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl0__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl5__ctl0_txtEdit')]";

                    WaitUntilElementValueCanBeCleared(By.XPath(theXpath));
                    SendKeys(WaitUntilElementExists(By.XPath(theXpath)), value);

                    IWebElement save = driver.FindElement(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl0__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl5_btnEditSave')]"));
                    save.Click();
                }
            }
        }

        public string TheCoolestPlacesIveVisited
        {
            get
            {
                if (driver.Url.Contains("jdate.com"))
                {
                    IWebElement element = WaitUntilElementExists(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl0__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl7_essayItem')]/p"));
                    return element.Text;
                }
                else
                {
                    IWebElement element = WaitUntilElementExists(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl0__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl6_essayItem')]/p"));
                    return element.Text;
                }
            }
            set
            {
                if (driver.Url.Contains("jdate.com"))
                {
                    IWebElement element = driver.FindElement(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl0__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl7_essayItem')]/h3"));
                    RemoteWebElement clickToEdit = (RemoteWebElement)element;
                    HoverClick(clickToEdit);

                    string theXpath = "//textarea[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl0__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl7__ctl0_txtEdit')]";

                    WaitUntilElementValueCanBeCleared(By.XPath(theXpath));
                    SendKeys(WaitUntilElementExists(By.XPath(theXpath)), value);

                    IWebElement save = driver.FindElement(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl0__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl7_btnEditSave')]"));
                    save.Click();
                }
                else
                {
                    IWebElement element = driver.FindElement(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl0__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl6_essayItem')]/h3"));
                    RemoteWebElement clickToEdit = (RemoteWebElement)element;
                    HoverClick(clickToEdit);

                    string theXpath = "//textarea[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl0__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl6__ctl0_txtEdit')]";

                    WaitUntilElementValueCanBeCleared(By.XPath(theXpath));
                    SendKeys(WaitUntilElementExists(By.XPath(theXpath)), value);

                    IWebElement save = driver.FindElement(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl0__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl6_btnEditSave')]"));
                    save.Click();
                }
            }
        }

        public string ForFunILikeTo
        {
            get
            {
                if (driver.Url.Contains("jdate.com"))
                {
                    IWebElement element = WaitUntilElementExists(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl0__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl8_essayItem')]/p"));
                    return element.Text;
                }
                else
                {
                    IWebElement element = WaitUntilElementExists(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl0__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl7_essayItem')]/p"));
                    return element.Text;
                }
            }
            set
            {
                if (driver.Url.Contains("jdate.com"))
                {
                    IWebElement element = driver.FindElement(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl0__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl8_essayItem')]/h3"));
                    RemoteWebElement clickToEdit = (RemoteWebElement)element;
                    HoverClick(clickToEdit);

                    string theXpath = "//textarea[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl0__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl8__ctl0_txtEdit')]";

                    WaitUntilElementValueCanBeCleared(By.XPath(theXpath));
                    SendKeys(WaitUntilElementExists(By.XPath(theXpath)), value);

                    IWebElement save = driver.FindElement(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl0__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl8_btnEditSave')]"));
                    save.Click();
                }
                else
                {
                    IWebElement element = driver.FindElement(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl0__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl7_essayItem')]/h3"));
                    RemoteWebElement clickToEdit = (RemoteWebElement)element;
                    HoverClick(clickToEdit);

                    string theXpath = "//textarea[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl0__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl7__ctl0_txtEdit')]";

                    WaitUntilElementValueCanBeCleared(By.XPath(theXpath));
                    SendKeys(WaitUntilElementExists(By.XPath(theXpath)), value);

                    IWebElement save = driver.FindElement(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl0__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl7_btnEditSave')]"));
                    save.Click();
                }
            }
        }

        public string OnFridayAndSaturdayNightsITypically
        {
            get
            {
                if (driver.Url.Contains("jdate.com"))
                {
                    IWebElement element = WaitUntilElementExists(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl0__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl9_essayItem')]/p"));
                    return element.Text;
                }
                else
                {
                    IWebElement element = WaitUntilElementExists(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl0__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl8_essayItem')]/p"));
                    return element.Text;
                }
            }
            set
            {
                if (driver.Url.Contains("jdate.com"))
                {
                    IWebElement element = driver.FindElement(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl0__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl9_essayItem')]/h3"));
                    RemoteWebElement clickToEdit = (RemoteWebElement)element;
                    HoverClick(clickToEdit);

                    string theXpath = "//textarea[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl0__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl9__ctl0_txtEdit')]";

                    WaitUntilElementValueCanBeCleared(By.XPath(theXpath));
                    SendKeys(WaitUntilElementExists(By.XPath(theXpath)), value);

                    IWebElement save = driver.FindElement(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl0__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl9_btnEditSave')]"));
                    save.Click();
                }
                else
                {
                    IWebElement element = driver.FindElement(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl0__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl8_essayItem')]/h3"));
                    RemoteWebElement clickToEdit = (RemoteWebElement)element;
                    HoverClick(clickToEdit);

                    string theXpath = "//textarea[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl0__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl8__ctl0_txtEdit')]";

                    WaitUntilElementValueCanBeCleared(By.XPath(theXpath));
                    SendKeys(WaitUntilElementExists(By.XPath(theXpath)), value);

                    IWebElement save = driver.FindElement(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl0__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl8_btnEditSave')]"));
                    save.Click();
                }
            }
        }

        public string ImLookingFor
        {
            get
            {
                if (driver.Url.Contains("jdate.com"))
                {
                    IWebElement element = WaitUntilElementExists(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl0__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl10_essayItem')]/p"));
                    return element.Text;
                }
                else
                {
                    IWebElement element = WaitUntilElementExists(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl0__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl9_essayItem')]/p"));
                    return element.Text;
                }
            }
            set
            {
                if (driver.Url.Contains("jdate.com"))
                {
                    IWebElement element = driver.FindElement(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl0__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl10_essayItem')]/h3"));
                    RemoteWebElement clickToEdit = (RemoteWebElement)element;
                    HoverClick(clickToEdit);

                    string theXpath = "//textarea[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl0__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl10__ctl0_txtEdit')]";

                    WaitUntilElementValueCanBeCleared(By.XPath(theXpath));
                    SendKeys(WaitUntilElementExists(By.XPath(theXpath)), value);

                    IWebElement save = driver.FindElement(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl0__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl10_btnEditSave')]"));
                    save.Click();
                }
                else
                {
                    IWebElement element = driver.FindElement(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl0__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl9_essayItem')]/h3"));
                    RemoteWebElement clickToEdit = (RemoteWebElement)element;
                    HoverClick(clickToEdit);

                    string theXpath = "//textarea[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl0__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl9__ctl0_txtEdit')]";

                    WaitUntilElementValueCanBeCleared(By.XPath(theXpath));
                    SendKeys(WaitUntilElementExists(By.XPath(theXpath)), value);

                    IWebElement save = driver.FindElement(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl0__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl9_btnEditSave')]"));
                    save.Click();
                }
            }
        }

        public string YouShouldDefinitelyMessageMeIfYou
        {
            get
            {
                if (driver.Url.Contains("jdate.com"))
                {
                    IWebElement element = WaitUntilElementExists(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl0__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl13_essayItem')]/p"));
                    return element.Text;
                }
                else
                {
                    IWebElement element = WaitUntilElementExists(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl0__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl10_essayItem')]/p"));
                    return element.Text;
                }
            }
            set
            {
                if (driver.Url.Contains("jdate.com"))
                {
                    IWebElement element = driver.FindElement(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl0__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl13_essayItem')]/h3"));
                    RemoteWebElement clickToEdit = (RemoteWebElement)element;
                    HoverClick(clickToEdit);

                    string theXpath = "//textarea[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl0__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl13__ctl0_txtEdit')]";

                    WaitUntilElementValueCanBeCleared(By.XPath(theXpath));
                    SendKeys(WaitUntilElementExists(By.XPath(theXpath)), value);

                    IWebElement save = driver.FindElement(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl0__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl13_btnEditSave')]"));
                    save.Click();
                }
                else
                {
                    IWebElement element = driver.FindElement(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl0__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl10_essayItem')]/h3"));
                    RemoteWebElement clickToEdit = (RemoteWebElement)element;
                    HoverClick(clickToEdit);

                    string theXpath = "//textarea[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl0__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl10__ctl0_txtEdit')]";

                    WaitUntilElementValueCanBeCleared(By.XPath(theXpath));
                    SendKeys(WaitUntilElementExists(By.XPath(theXpath)), value);

                    IWebElement save = driver.FindElement(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl0__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl10_btnEditSave')]"));
                    save.Click();
                }
            }
        }







        #endregion

        #region More About Me tab
        public List<string> MyFavoriteMusic
        {
            get
            {
                IWebElement element = WaitUntilElementExists(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl0_essayItem')]/p"));
                List<string> selectedValues = element.Text.Split(',').ToList();

                for (int i = 0; i < selectedValues.Count; i++)
                    selectedValues[i] = selectedValues[i].Trim();

                return selectedValues;
            }
            set
            {
                IWebElement element = driver.FindElement(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl0_essayItem')]/h3"));
                RemoteWebElement clickToEdit = (RemoteWebElement)element;
                HoverClick(clickToEdit);

                int checkboxCount = 30;
                IWebElement checkbox = null;
                IWebElement label = null;

                // uncheck everything initially
                for (int i = 0; i < checkboxCount; i++)
                {
                    if (i < checkboxCount / 2)
                    {
                        checkbox = WaitUntilElementExists(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl0__ctl0_repeaterEditItemDoubleA__ctl" + (i % (checkboxCount / 2)).ToString() + "_chkItemEdit')]"));

                        if (checkbox.Selected == true)
                            checkbox.Click();
                    }
                    else
                    {
                        checkbox = WaitUntilElementExists(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl0__ctl0_repeaterEditItemDoubleB__ctl" + (i % (checkboxCount / 2)).ToString() + "_chkItemEdit')]"));

                        if (checkbox.Selected == true)
                            checkbox.Click();
                    }
                }

                // check only what is passed in
                for (int i = 0; i < checkboxCount; i++)
                {
                    if (i < checkboxCount / 2)
                    {
                        checkbox = WaitUntilElementExists(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl0__ctl0_repeaterEditItemDoubleA__ctl" + (i % (checkboxCount / 2)).ToString() + "_chkItemEdit')]"));
                        label = WaitUntilElementExists(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl0_editItemContainer')]/div/fieldset/ul[1]/li[" + ((i + 1) % ((checkboxCount / 2) + 1)).ToString() + "]/label"));
                    }
                    else
                    {
                        checkbox = WaitUntilElementExists(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl0__ctl0_repeaterEditItemDoubleB__ctl" + (i % (checkboxCount / 2)).ToString() + "_chkItemEdit')]"));
                        label = WaitUntilElementExists(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl0_editItemContainer')]/div/fieldset/ul[2]/li[" + ((i + 2) % ((checkboxCount / 2) + 1)).ToString() + "]/label"));
                    }

                    foreach (string currentValue in value)
                    {
                        if (label.Text == currentValue && checkbox.Selected == false)
                            checkbox.Click();
                    }
                }

                IWebElement save = driver.FindElement(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl0_btnEditSave')]"));
                save.Click();

            }
        }

        public string FavoriteBandsAndMusicians
        {
            get
            {
                IWebElement element = WaitUntilElementExists(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl1_essayItem')]/p"));
                return element.Text;
            }
            set
            {
                IWebElement element = driver.FindElement(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl1_essayItem')]/h3"));
                RemoteWebElement clickToEdit = (RemoteWebElement)element;
                HoverClick(clickToEdit);

                string theXpath = "//textarea[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl1__ctl0_txtEdit')]";

                WaitUntilElementValueCanBeCleared(By.XPath(theXpath));
                SendKeys(WaitUntilElementExists(By.XPath(theXpath)), value);

                IWebElement save = driver.FindElement(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl1_btnEditSave')]"));
                save.Click();
            }
        }

        public List<string> Movie
        {
            get
            {
                IWebElement element = WaitUntilElementExists(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl2_essayItem')]/p"));
                List<string> selectedValues = element.Text.Split(',').ToList();

                for (int i = 0; i < selectedValues.Count; i++)
                    selectedValues[i] = selectedValues[i].Trim();

                return selectedValues;
            }
            set
            {
                IWebElement element = driver.FindElement(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl2_essayItem')]/h3"));
                RemoteWebElement clickToEdit = (RemoteWebElement)element;
                HoverClick(clickToEdit);

                int checkboxCount = 14;
                IWebElement checkbox = null;
                IWebElement label = null;

                // uncheck everything initially
                for (int i = 0; i < checkboxCount; i++)
                {
                    if (i < checkboxCount / 2)
                    {
                        checkbox = WaitUntilElementExists(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl2__ctl0_repeaterEditItemDoubleA__ctl" + (i % (checkboxCount / 2)).ToString() + "_chkItemEdit')]"));

                        if (checkbox.Selected == true)
                            checkbox.Click();
                    }
                    else
                    {
                        checkbox = WaitUntilElementExists(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl2__ctl0_repeaterEditItemDoubleB__ctl" + (i % (checkboxCount / 2)).ToString() + "_chkItemEdit')]"));

                        if (checkbox.Selected == true)
                            checkbox.Click();
                    }

                    if (i == 7)
                        i = 7;
                }

                // check only the what is passed in
                for (int i = 0; i < checkboxCount; i++)
                {
                    if (i < checkboxCount / 2)
                    {
                        checkbox = WaitUntilElementExists(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl2__ctl0_repeaterEditItemDoubleA__ctl" + (i % (checkboxCount / 2)).ToString() + "_chkItemEdit')]"));
                        label = WaitUntilElementExists(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl2_editItemContainer')]/div/fieldset/ul[1]/li[" + ((i + 1) % ((checkboxCount / 2) + 1)).ToString() + "]/label"));
                    }
                    else
                    {
                        checkbox = WaitUntilElementExists(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl2__ctl0_repeaterEditItemDoubleB__ctl" + (i % (checkboxCount / 2)).ToString() + "_chkItemEdit')]"));
                        label = WaitUntilElementExists(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl2_editItemContainer')]/div/fieldset/ul[2]/li[" + ((i + 2) % ((checkboxCount / 2) + 1)).ToString() + "]/label"));
                    }

                    foreach (string currentValue in value)
                    {
                        if (label.Text == currentValue && checkbox.Selected == false)
                            checkbox.Click();
                    }

                }

                IWebElement save = driver.FindElement(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl2_btnEditSave')]"));
                save.Click();

            }
        }

        public string FavoriteMoviesAndActors
        {
            get
            {
                IWebElement element = WaitUntilElementExists(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl3_essayItem')]/p"));
                return element.Text;
            }
            set
            {
                IWebElement element = driver.FindElement(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl3_essayItem')]/h3"));
                RemoteWebElement clickToEdit = (RemoteWebElement)element;
                HoverClick(clickToEdit);

                string theXpath = "//textarea[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl3__ctl0_txtEdit')]";

                WaitUntilElementValueCanBeCleared(By.XPath(theXpath));
                SendKeys(WaitUntilElementExists(By.XPath(theXpath)), value);

                IWebElement save = driver.FindElement(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl3_btnEditSave')]"));
                save.Click();
            }
        }

        public string FavoriteTVShows
        {
            get
            {
                IWebElement element = WaitUntilElementExists(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl4_essayItem')]/p"));
                return element.Text;
            }
            set
            {
                IWebElement element = driver.FindElement(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl4_essayItem')]/h3"));
                RemoteWebElement clickToEdit = (RemoteWebElement)element;
                HoverClick(clickToEdit);

                string theXpath = "//textarea[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl4__ctl0_txtEdit')]";

                WaitUntilElementValueCanBeCleared(By.XPath(theXpath));
                SendKeys(WaitUntilElementExists(By.XPath(theXpath)), value);

                IWebElement save = driver.FindElement(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl4_btnEditSave')]"));
                save.Click();
            }
        }

        public List<string> MyFavoritePhysicalActivities
        {
            get
            {
                IWebElement element = WaitUntilElementExists(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl5_essayItem')]/p"));
                List<string> selectedValues = element.Text.Split(',').ToList();

                for (int i = 0; i < selectedValues.Count; i++)
                    selectedValues[i] = selectedValues[i].Trim();

                return selectedValues;
            }
            set
            {
                IWebElement element = driver.FindElement(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl5_essayItem')]/h3"));
                RemoteWebElement clickToEdit = (RemoteWebElement)element;
                HoverClick(clickToEdit);

                int checkboxCount = 31;
                IWebElement checkbox = null;
                IWebElement label = null;

                // uncheck everything initially
                for (int i = 0; i < checkboxCount; i++)
                {
                    if (i < (checkboxCount / 2) + 1)
                    {
                        checkbox = WaitUntilElementExists(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl5__ctl0_repeaterEditItemDoubleA__ctl" + (i % ((checkboxCount + 1) / 2)).ToString() + "_chkItemEdit')]"));

                        if (checkbox.Selected == true)
                            checkbox.Click();
                    }
                    else
                    {
                        checkbox = WaitUntilElementExists(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl5__ctl0_repeaterEditItemDoubleB__ctl" + (i % (checkboxCount / 2)).ToString() + "_chkItemEdit')]"));

                        if (checkbox.Selected == true)
                            checkbox.Click();
                    }

                }

                // check only the what is passed in
                for (int i = 0; i < checkboxCount; i++)
                {
                    if (i < (checkboxCount / 2) + 1)
                    {
                        checkbox = WaitUntilElementExists(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl5__ctl0_repeaterEditItemDoubleA__ctl" + (i % ((checkboxCount + 1) / 2)).ToString() + "_chkItemEdit')]"));
                        label = WaitUntilElementExists(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl5_editItemContainer')]/div/fieldset/ul[1]/li[" + ((i + 1) % ((checkboxCount / 2) + 2)).ToString() + "]/label"));
                    }
                    else
                    {
                        checkbox = WaitUntilElementExists(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl5__ctl0_repeaterEditItemDoubleB__ctl" + ((i - 1) % (checkboxCount / 2)).ToString() + "_chkItemEdit')]"));
                        label = WaitUntilElementExists(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl5_editItemContainer')]/div/fieldset/ul[2]/li[" + ((i + 1) % ((checkboxCount / 2) + 1)).ToString() + "]/label"));
                    }

                    foreach (string currentValue in value)
                    {
                        if (label.Text == currentValue && checkbox.Selected == false)
                            checkbox.Click();
                    }

                }

                IWebElement save = driver.FindElement(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl5_btnEditSave')]"));
                save.Click();

            }
        }

        public List<string> InMyFreeTimeIEnjoy
        {
            get
            {
                IWebElement element = WaitUntilElementExists(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl6_essayItem')]/p"));
                List<string> selectedValues = element.Text.Split(',').ToList();

                for (int i = 0; i < selectedValues.Count; i++)
                    selectedValues[i] = selectedValues[i].Trim();

                return selectedValues;
            }
            set
            {
                IWebElement element = driver.FindElement(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl6_essayItem')]/h3"));
                RemoteWebElement clickToEdit = (RemoteWebElement)element;
                HoverClick(clickToEdit);

                int checkboxCount = 30;
                IWebElement checkbox = null;
                IWebElement label = null;

                // uncheck everything initially
                for (int i = 0; i < checkboxCount; i++)
                {
                    if (i < checkboxCount / 2)
                    {
                        checkbox = WaitUntilElementExists(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl6__ctl0_repeaterEditItemDoubleA__ctl" + (i % (checkboxCount / 2)).ToString() + "_chkItemEdit')]"));

                        if (checkbox.Selected == true)
                            checkbox.Click();
                    }
                    else
                    {
                        checkbox = WaitUntilElementExists(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl6__ctl0_repeaterEditItemDoubleB__ctl" + (i % (checkboxCount / 2)).ToString() + "_chkItemEdit')]"));

                        if (checkbox.Selected == true)
                            checkbox.Click();
                    }

                }

                // check only the what is passed in
                for (int i = 0; i < checkboxCount; i++)
                {
                    if (i < checkboxCount / 2)
                    {
                        checkbox = WaitUntilElementExists(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl6__ctl0_repeaterEditItemDoubleA__ctl" + (i % (checkboxCount / 2)).ToString() + "_chkItemEdit')]"));
                        label = WaitUntilElementExists(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl6_editItemContainer')]/div/fieldset/ul[1]/li[" + ((i + 1) % ((checkboxCount / 2) + 1)).ToString() + "]/label"));
                    }
                    else
                    {
                        checkbox = WaitUntilElementExists(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl6__ctl0_repeaterEditItemDoubleB__ctl" + (i % (checkboxCount / 2)).ToString() + "_chkItemEdit')]"));
                        label = WaitUntilElementExists(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl6_editItemContainer')]/div/fieldset/ul[2]/li[" + ((i + 2) % ((checkboxCount / 2) + 1)).ToString() + "]/label"));
                    }

                    foreach (string currentValue in value)
                    {
                        if (label.Text == currentValue && checkbox.Selected == false)
                            checkbox.Click();
                    }

                }

                IWebElement save = driver.FindElement(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl6_btnEditSave')]"));
                save.Click();

            }
        }

        public string MyIdeaOfAGreatTrip
        {
            get
            {
                IWebElement element = WaitUntilElementExists(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl7_essayItem')]/p"));
                return element.Text;
            }
            set
            {
                IWebElement element = driver.FindElement(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl7_essayItem')]/h3"));
                RemoteWebElement clickToEdit = (RemoteWebElement)element;
                HoverClick(clickToEdit);

                string theXpath = "//textarea[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl7__ctl0_txtEdit')]";

                WaitUntilElementValueCanBeCleared(By.XPath(theXpath));
                SendKeys(WaitUntilElementExists(By.XPath(theXpath)), value);

                IWebElement save = driver.FindElement(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl7_btnEditSave')]"));
                save.Click();
            }
        }

        public List<string> MyFavoriteFoods
        {
            get
            {
                IWebElement element = WaitUntilElementExists(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl8_essayItem')]/p"));
                List<string> selectedValues = element.Text.Split(',').ToList();

                for (int i = 0; i < selectedValues.Count; i++)
                    selectedValues[i] = selectedValues[i].Trim();

                return selectedValues;
            }
            set
            {
                IWebElement element = driver.FindElement(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl8_essayItem')]/h3"));
                RemoteWebElement clickToEdit = (RemoteWebElement)element;
                HoverClick(clickToEdit);

                int checkboxCount = 30;
                IWebElement checkbox = null;
                IWebElement label = null;

                // uncheck everything initially
                for (int i = 0; i < checkboxCount; i++)
                {
                    if (i < checkboxCount / 2)
                    {
                        checkbox = WaitUntilElementExists(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl8__ctl0_repeaterEditItemDoubleA__ctl" + (i % (checkboxCount / 2)).ToString() + "_chkItemEdit')]"));

                        if (checkbox.Selected == true)
                            checkbox.Click();
                    }
                    else
                    {
                        checkbox = WaitUntilElementExists(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl8__ctl0_repeaterEditItemDoubleB__ctl" + (i % (checkboxCount / 2)).ToString() + "_chkItemEdit')]"));

                        if (checkbox.Selected == true)
                            checkbox.Click();
                    }

                }

                // check only the what is passed in
                for (int i = 0; i < checkboxCount; i++)
                {
                    if (i < checkboxCount / 2)
                    {
                        checkbox = WaitUntilElementExists(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl8__ctl0_repeaterEditItemDoubleA__ctl" + (i % (checkboxCount / 2)).ToString() + "_chkItemEdit')]"));
                        label = WaitUntilElementExists(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl8_editItemContainer')]/div/fieldset/ul[1]/li[" + ((i + 1) % ((checkboxCount / 2) + 1)).ToString() + "]/label"));
                    }
                    else
                    {
                        checkbox = WaitUntilElementExists(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl8__ctl0_repeaterEditItemDoubleB__ctl" + (i % (checkboxCount / 2)).ToString() + "_chkItemEdit')]"));
                        label = WaitUntilElementExists(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl8_editItemContainer')]/div/fieldset/ul[2]/li[" + ((i + 2) % ((checkboxCount / 2) + 1)).ToString() + "]/label"));
                    }

                    foreach (string currentValue in value)
                    {
                        if (label.Text == currentValue && checkbox.Selected == false)
                            checkbox.Click();
                    }

                }

                IWebElement save = driver.FindElement(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl8_btnEditSave')]"));
                save.Click();

            }
        }

        public string FavoriteRestaurants
        {
            get
            {
                IWebElement element = WaitUntilElementExists(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl9_essayItem')]/p"));
                return element.Text;
            }
            set
            {
                IWebElement element = driver.FindElement(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl9_essayItem')]/h3"));
                RemoteWebElement clickToEdit = (RemoteWebElement)element;
                HoverClick(clickToEdit);

                string theXpath = "//textarea[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl9__ctl0_txtEdit')]";

                WaitUntilElementValueCanBeCleared(By.XPath(theXpath));
                SendKeys(WaitUntilElementExists(By.XPath(theXpath)), value);

                IWebElement save = driver.FindElement(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl9_btnEditSave')]"));
                save.Click();
            }
        }

        public string SchoolsAttended
        {
            get
            {
                IWebElement element = WaitUntilElementExists(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl10_essayItem')]/p"));
                return element.Text;
            }
            set
            {
                IWebElement element = driver.FindElement(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl10_essayItem')]/h3"));
                RemoteWebElement clickToEdit = (RemoteWebElement)element;
                HoverClick(clickToEdit);

                string theXpath = "//textarea[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl10__ctl0_txtEdit')]";

                WaitUntilElementValueCanBeCleared(By.XPath(theXpath));
                SendKeys(WaitUntilElementExists(By.XPath(theXpath)), value);

                IWebElement save = driver.FindElement(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl10_btnEditSave')]"));
                save.Click();
            }
        }

        public string WhenGoingSomeWhere
        {
            get
            {
                IWebElement element = driver.FindElement(By.XPath("//select[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl11__ctl0_ddlEdit')]"));
                string selectedValue = element.GetAttribute("value");

                if (selectedValue == "0")
                    return string.Empty;
                else if (selectedValue == "16")
                    return "I am usually early";
                else if (selectedValue == "1")
                    return "I am usually a little late";
                else if (selectedValue == "2")
                    return "I am usually on time";
                else if (selectedValue == "4")
                    return "I usually forget to show up";
                else if (selectedValue == "8")
                    return "I am the star of the show";

                return string.Empty;
            }
            set
            {
                IWebElement element = driver.FindElement(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl11_essayItem')]/h3"));
                RemoteWebElement clickToEdit = (RemoteWebElement)element;
                HoverClick(clickToEdit);

                SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl11__ctl0_ddlEdit')]")));
                select.SelectByText(value);

                IWebElement save = driver.FindElement(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl11_btnEditSave')]"));
                save.Click();

            }
        }

        public string AsForFashion
        {
            get
            {
                IWebElement element = driver.FindElement(By.XPath("//select[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl12__ctl0_ddlEdit')]"));
                string selectedValue = element.GetAttribute("value");

                if (selectedValue == "0")
                    return string.Empty;
                else if (selectedValue == "1")
                    return "I don't need advice";
                else if (selectedValue == "2")
                    return "I don't care about the latest fashions";
                else if (selectedValue == "4")
                    return "I dress to be comfortable";
                // weird.  i don't know how to tell apart the two selections below since they have the same value.  don't use the below two for testing.
                else if (selectedValue == "8")
                    return "I am a somewhat fashionable person";
                else if (selectedValue == "16")
                    return "I am a very trendy person";

                return string.Empty;
            }
            set
            {
                IWebElement element = driver.FindElement(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl12_essayItem')]/h3"));
                RemoteWebElement clickToEdit = (RemoteWebElement)element;
                HoverClick(clickToEdit);

                SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl12__ctl0_ddlEdit')]")));
                select.SelectByText(value);

                IWebElement save = driver.FindElement(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl12_btnEditSave')]"));
                save.Click();

            }
        }
        #endregion

        #region More About Me (JDate)
        public string MyPerfectFirstDate
        {
            get
            {
                if (driver.Url.Contains("jdate.com"))
                {
                    IWebElement element = WaitUntilElementExists(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl0__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl3_essayItem')]/p"));
                    return element.Text;
                }
                else
                {
                    IWebElement element = WaitUntilElementExists(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl0_essayItem')]/p"));
                    return element.Text;
                }
            }
            set
            {
                if (driver.Url.Contains("jdate.com"))
                {
                    IWebElement element = driver.FindElement(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl0__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl3_essayItem')]/h3"));
                    RemoteWebElement clickToEdit = (RemoteWebElement)element;
                    HoverClick(clickToEdit);

                    string theXpath = "//textarea[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl0__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl3__ctl0_txtEdit')]";

                    WaitUntilElementValueCanBeCleared(By.XPath(theXpath));
                    SendKeys(WaitUntilElementExists(By.XPath(theXpath)), value);

                    IWebElement save = driver.FindElement(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl0__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl3_btnEditSave')]"));
                    save.Click();
                }
                else
                {
                    IWebElement element = driver.FindElement(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl0_essayItem')]/h3"));
                    RemoteWebElement clickToEdit = (RemoteWebElement)element;
                    HoverClick(clickToEdit);

                    string theXpath = "//textarea[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl0__ctl0_txtEdit')]";

                    WaitUntilElementValueCanBeCleared(By.XPath(theXpath));
                    SendKeys(WaitUntilElementExists(By.XPath(theXpath)), value);

                    IWebElement save = driver.FindElement(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl0_btnEditSave')]"));
                    save.Click();
                }
            }
        }

        public string MyIdealRelationship
        {
            get
            {
                if (driver.Url.Contains("jdate.com"))
                {
                    IWebElement element = WaitUntilElementExists(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl0__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl11_essayItem')]/p"));
                    return element.Text;
                }
                else
                {
                    IWebElement element = WaitUntilElementExists(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl1_essayItem')]/p"));
                    return element.Text;
                }
            }
            set
            {
                if (driver.Url.Contains("jdate.com"))
                {
                    IWebElement element = driver.FindElement(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl0__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl11_essayItem')]/h3"));
                    RemoteWebElement clickToEdit = (RemoteWebElement)element;
                    HoverClick(clickToEdit);

                    string theXpath = "//textarea[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl0__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl11__ctl0_txtEdit')]";

                    WaitUntilElementValueCanBeCleared(By.XPath(theXpath));
                    SendKeys(WaitUntilElementExists(By.XPath(theXpath)), value);

                    IWebElement save = driver.FindElement(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl0__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl11_btnEditSave')]"));
                    save.Click();
                }
                else
                {
                    IWebElement element = driver.FindElement(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl1_essayItem')]/h3"));
                    RemoteWebElement clickToEdit = (RemoteWebElement)element;
                    HoverClick(clickToEdit);

                    string theXpath = "//textarea[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl1__ctl0_txtEdit')]";

                    WaitUntilElementValueCanBeCleared(By.XPath(theXpath));
                    SendKeys(WaitUntilElementExists(By.XPath(theXpath)), value);

                    IWebElement save = driver.FindElement(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl1_btnEditSave')]"));
                    save.Click();
                }
            }
        }

        public string MyPastRelationships
        {
            get
            {
                if (driver.Url.Contains("jdate.com"))
                {
                    IWebElement element = WaitUntilElementExists(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl0__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl12_essayItem')]/p"));
                    return element.Text;
                }
                else
                {
                    IWebElement element = WaitUntilElementExists(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl2_essayItem')]/p"));
                    return element.Text;
                }
            }
            set
            {
                if (driver.Url.Contains("jdate.com"))
                {
                    IWebElement element = driver.FindElement(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl0__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl12_essayItem')]/h3"));
                    RemoteWebElement clickToEdit = (RemoteWebElement)element;
                    HoverClick(clickToEdit);

                    string theXpath = "//textarea[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl0__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl12__ctl0_txtEdit')]";

                    WaitUntilElementValueCanBeCleared(By.XPath(theXpath));
                    SendKeys(WaitUntilElementExists(By.XPath(theXpath)), value);

                    IWebElement save = driver.FindElement(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl0__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl12_btnEditSave')]"));
                    save.Click();
                }
                else
                {
                    IWebElement element = driver.FindElement(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl2_essayItem')]/h3"));
                    RemoteWebElement clickToEdit = (RemoteWebElement)element;
                    HoverClick(clickToEdit);

                    string theXpath = "//textarea[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl2__ctl0_txtEdit')]";

                    WaitUntilElementValueCanBeCleared(By.XPath(theXpath));
                    SendKeys(WaitUntilElementExists(By.XPath(theXpath)), value);

                    IWebElement save = driver.FindElement(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl2_btnEditSave')]"));
                    save.Click();
                }
            }
        }

        public List<string> MyPersonalityIsBestDescribedAs
        {
            get
            {
                IWebElement element = null;

                if (driver.Url.Contains("jdate.com"))
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl0_essayItem')]/p"));
                else
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl3_essayItem')]/p"));

                List<string> selectedValues = element.Text.Split(',').ToList();

                for (int i = 0; i < selectedValues.Count; i++)
                    selectedValues[i] = selectedValues[i].Trim();

                return selectedValues;
            }
            set
            {
                if (driver.Url.Contains("jdate.com"))
                {
                    IWebElement element = driver.FindElement(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl0_essayItem')]/h3"));
                    RemoteWebElement clickToEdit = (RemoteWebElement)element;
                    HoverClick(clickToEdit);

                    int checkboxCount = 30;
                    IWebElement checkbox = null;
                    IWebElement label = null;

                    // uncheck everything initially
                    for (int i = 0; i < checkboxCount; i++)
                    {
                        if (i < checkboxCount / 2)
                        {
                            checkbox = WaitUntilElementExists(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl0__ctl0_repeaterEditItemDoubleA__ctl" + (i % (checkboxCount / 2)).ToString() + "_chkItemEdit')]"));

                            if (checkbox.Selected == true)
                                checkbox.Click();
                        }
                        else
                        {
                            checkbox = WaitUntilElementExists(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl0__ctl0_repeaterEditItemDoubleB__ctl" + (i % (checkboxCount / 2)).ToString() + "_chkItemEdit')]"));

                            if (checkbox.Selected == true)
                                checkbox.Click();
                        }

                    }

                    // check only the what is passed in
                    for (int i = 0; i < checkboxCount; i++)
                    {
                        if (i < checkboxCount / 2)
                        {
                            checkbox = WaitUntilElementExists(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl0__ctl0_repeaterEditItemDoubleA__ctl" + (i % (checkboxCount / 2)).ToString() + "_chkItemEdit')]"));
                            label = WaitUntilElementExists(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl0_editItemContainer')]/div/fieldset/ul[1]/li[" + ((i + 1) % ((checkboxCount / 2) + 1)).ToString() + "]/label"));
                        }
                        else
                        {
                            checkbox = WaitUntilElementExists(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl0__ctl0_repeaterEditItemDoubleB__ctl" + (i % (checkboxCount / 2)).ToString() + "_chkItemEdit')]"));
                            label = WaitUntilElementExists(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl0_editItemContainer')]/div/fieldset/ul[2]/li[" + ((i + 2) % ((checkboxCount / 2) + 1)).ToString() + "]/label"));
                        }

                        foreach (string currentValue in value)
                        {
                            if (label.Text == currentValue && checkbox.Selected == false)
                                checkbox.Click();
                        }

                    }

                    IWebElement save = driver.FindElement(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl0_btnEditSave')]"));
                    save.Click();
                }
                else
                {
                    IWebElement element = driver.FindElement(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl3_essayItem')]/h3"));
                    RemoteWebElement clickToEdit = (RemoteWebElement)element;
                    HoverClick(clickToEdit);

                    int checkboxCount = 30;
                    IWebElement checkbox = null;
                    IWebElement label = null;

                    // uncheck everything initially
                    for (int i = 0; i < checkboxCount; i++)
                    {
                        if (i < checkboxCount / 2)
                        {
                            checkbox = WaitUntilElementExists(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl3__ctl0_repeaterEditItemDoubleA__ctl" + (i % (checkboxCount / 2)).ToString() + "_chkItemEdit')]"));

                            if (checkbox.Selected == true)
                                checkbox.Click();
                        }
                        else
                        {
                            checkbox = WaitUntilElementExists(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl3__ctl0_repeaterEditItemDoubleB__ctl" + (i % (checkboxCount / 2)).ToString() + "_chkItemEdit')]"));

                            if (checkbox.Selected == true)
                                checkbox.Click();
                        }

                    }

                    // check only the what is passed in
                    for (int i = 0; i < checkboxCount; i++)
                    {
                        if (i < checkboxCount / 2)
                        {
                            checkbox = WaitUntilElementExists(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl3__ctl0_repeaterEditItemDoubleA__ctl" + (i % (checkboxCount / 2)).ToString() + "_chkItemEdit')]"));
                            label = WaitUntilElementExists(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl3_editItemContainer')]/div/fieldset/ul[1]/li[" + ((i + 1) % ((checkboxCount / 2) + 1)).ToString() + "]/label"));
                        }
                        else
                        {
                            checkbox = WaitUntilElementExists(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl3__ctl0_repeaterEditItemDoubleB__ctl" + (i % (checkboxCount / 2)).ToString() + "_chkItemEdit')]"));
                            label = WaitUntilElementExists(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl3_editItemContainer')]/div/fieldset/ul[2]/li[" + ((i + 2) % ((checkboxCount / 2) + 1)).ToString() + "]/label"));
                        }

                        foreach (string currentValue in value)
                        {
                            if (label.Text == currentValue && checkbox.Selected == false)
                                checkbox.Click();
                        }

                    }

                    IWebElement save = driver.FindElement(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl3_btnEditSave')]"));
                    save.Click();
                }
            }
        }

        public List<string> InMyFreeTimeIEnjoy_JDatePage
        {
            get
            {
                IWebElement element = null;

                if (driver.Url.Contains("jdate.com"))
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl1_essayItem')]/p"));
                else
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl4_essayItem')]/p"));

                List<string> selectedValues = element.Text.Split(',').ToList();

                for (int i = 0; i < selectedValues.Count; i++)
                    selectedValues[i] = selectedValues[i].Trim();

                return selectedValues;
            }
            set
            {
                if (driver.Url.Contains("jdate.com"))
                {
                    IWebElement element = driver.FindElement(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl1_essayItem')]/h3"));
                    RemoteWebElement clickToEdit = (RemoteWebElement)element;
                    HoverClick(clickToEdit);

                    int checkboxCount;

                    checkboxCount = 30;

                    IWebElement checkbox = null;
                    IWebElement label = null;

                    // uncheck everything initially
                    for (int i = 0; i < checkboxCount; i++)
                    {
                        if (i < checkboxCount / 2)
                        {
                            checkbox = WaitUntilElementExists(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl1__ctl0_repeaterEditItemDoubleA__ctl" + (i % (checkboxCount / 2)).ToString() + "_chkItemEdit')]"));

                            if (checkbox.Selected == true)
                                checkbox.Click();
                        }
                        else
                        {
                            checkbox = WaitUntilElementExists(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl1__ctl0_repeaterEditItemDoubleB__ctl" + (i % (checkboxCount / 2)).ToString() + "_chkItemEdit')]"));

                            if (checkbox.Selected == true)
                                checkbox.Click();
                        }

                    }

                    // check only the what is passed in
                    for (int i = 0; i < checkboxCount; i++)
                    {
                        if (i < checkboxCount / 2)
                        {
                            checkbox = WaitUntilElementExists(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl1__ctl0_repeaterEditItemDoubleA__ctl" + (i % (checkboxCount / 2)).ToString() + "_chkItemEdit')]"));
                            label = WaitUntilElementExists(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl1_editItemContainer')]/div/fieldset/ul[1]/li[" + ((i + 1) % ((checkboxCount / 2) + 1)).ToString() + "]/label"));
                        }
                        else
                        {
                            checkbox = WaitUntilElementExists(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl1__ctl0_repeaterEditItemDoubleB__ctl" + (i % (checkboxCount / 2)).ToString() + "_chkItemEdit')]"));
                            label = WaitUntilElementExists(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl1_editItemContainer')]/div/fieldset/ul[2]/li[" + ((i + 2) % ((checkboxCount / 2) + 1)).ToString() + "]/label"));
                        }

                        foreach (string currentValue in value)
                        {
                            if (label.Text == currentValue && checkbox.Selected == false)
                                checkbox.Click();
                        }

                    }

                    IWebElement save = driver.FindElement(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl1_btnEditSave')]"));
                    save.Click();
                }
                else
                {
                    IWebElement element = driver.FindElement(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl4_essayItem')]/h3"));
                    RemoteWebElement clickToEdit = (RemoteWebElement)element;
                    HoverClick(clickToEdit);

                    int checkboxCount;

                    if (driver.Url.Contains("jdate.co.il"))
                        checkboxCount = 22;
                    else
                        checkboxCount = 30;

                    IWebElement checkbox = null;
                    IWebElement label = null;

                    // uncheck everything initially
                    for (int i = 0; i < checkboxCount; i++)
                    {
                        if (i < checkboxCount / 2)
                        {
                            checkbox = WaitUntilElementExists(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl4__ctl0_repeaterEditItemDoubleA__ctl" + (i % (checkboxCount / 2)).ToString() + "_chkItemEdit')]"));

                            if (checkbox.Selected == true)
                                checkbox.Click();
                        }
                        else
                        {
                            checkbox = WaitUntilElementExists(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl4__ctl0_repeaterEditItemDoubleB__ctl" + (i % (checkboxCount / 2)).ToString() + "_chkItemEdit')]"));

                            if (checkbox.Selected == true)
                                checkbox.Click();
                        }

                    }

                    // check only the what is passed in
                    for (int i = 0; i < checkboxCount; i++)
                    {
                        if (i < checkboxCount / 2)
                        {
                            checkbox = WaitUntilElementExists(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl4__ctl0_repeaterEditItemDoubleA__ctl" + (i % (checkboxCount / 2)).ToString() + "_chkItemEdit')]"));
                            label = WaitUntilElementExists(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl4_editItemContainer')]/div/fieldset/ul[1]/li[" + ((i + 1) % ((checkboxCount / 2) + 1)).ToString() + "]/label"));
                        }
                        else
                        {
                            checkbox = WaitUntilElementExists(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl4__ctl0_repeaterEditItemDoubleB__ctl" + (i % (checkboxCount / 2)).ToString() + "_chkItemEdit')]"));
                            label = WaitUntilElementExists(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl4_editItemContainer')]/div/fieldset/ul[2]/li[" + ((i + 2) % ((checkboxCount / 2) + 1)).ToString() + "]/label"));
                        }

                        foreach (string currentValue in value)
                        {
                            if (label.Text == currentValue && checkbox.Selected == false)
                                checkbox.Click();
                        }

                    }

                    IWebElement save = driver.FindElement(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl4_btnEditSave')]"));
                    save.Click();
                }
            }
        }

        public List<string> InMyFreeTimeILike
        {
            get
            {
                IWebElement element = null;

                if (driver.Url.Contains("jdate.com"))
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl2_essayItem')]/p"));
                else
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl5_essayItem')]/p"));

                List<string> selectedValues = element.Text.Split(',').ToList();

                for (int i = 0; i < selectedValues.Count; i++)
                    selectedValues[i] = selectedValues[i].Trim();

                return selectedValues;
            }
            set
            {
                if (driver.Url.Contains("jdate.com"))
                {
                    IWebElement element = driver.FindElement(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl2_essayItem')]/h3"));
                    RemoteWebElement clickToEdit = (RemoteWebElement)element;
                    HoverClick(clickToEdit);

                    int checkboxCount;

                    checkboxCount = 26;

                    IWebElement checkbox = null;
                    IWebElement label = null;

                    // uncheck everything initially
                    for (int i = 0; i < checkboxCount; i++)
                    {
                        if (i < checkboxCount / 2)
                        {
                            checkbox = WaitUntilElementExists(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl2__ctl0_repeaterEditItemDoubleA__ctl" + (i % (checkboxCount / 2)).ToString() + "_chkItemEdit')]"));

                            if (checkbox.Selected == true)
                                checkbox.Click();
                        }
                        else
                        {
                            checkbox = WaitUntilElementExists(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl2__ctl0_repeaterEditItemDoubleB__ctl" + (i % (checkboxCount / 2)).ToString() + "_chkItemEdit')]"));

                            if (checkbox.Selected == true)
                                checkbox.Click();
                        }

                    }

                    // check only the what is passed in
                    for (int i = 0; i < checkboxCount; i++)
                    {
                        if (i < checkboxCount / 2)
                        {
                            checkbox = WaitUntilElementExists(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl2__ctl0_repeaterEditItemDoubleA__ctl" + (i % (checkboxCount / 2)).ToString() + "_chkItemEdit')]"));
                            label = WaitUntilElementExists(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl2_editItemContainer')]/div/fieldset/ul[1]/li[" + ((i + 1) % ((checkboxCount / 2) + 1)).ToString() + "]/label"));
                        }
                        else
                        {
                            checkbox = WaitUntilElementExists(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl2__ctl0_repeaterEditItemDoubleB__ctl" + (i % (checkboxCount / 2)).ToString() + "_chkItemEdit')]"));
                            label = WaitUntilElementExists(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl2_editItemContainer')]/div/fieldset/ul[2]/li[" + ((i + 2) % ((checkboxCount / 2) + 1)).ToString() + "]/label"));
                        }

                        foreach (string currentValue in value)
                        {
                            if (label.Text == currentValue && checkbox.Selected == false)
                                checkbox.Click();
                        }

                    }

                    IWebElement save = driver.FindElement(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl2_btnEditSave')]"));
                    save.Click();
                }
                else
                {
                    IWebElement element = driver.FindElement(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl5_essayItem')]/h3"));
                    RemoteWebElement clickToEdit = (RemoteWebElement)element;
                    HoverClick(clickToEdit);

                    int checkboxCount;

                    if (driver.Url.Contains("jdate.co.il"))
                        checkboxCount = 14;
                    else
                        checkboxCount = 26;

                    IWebElement checkbox = null;
                    IWebElement label = null;

                    // uncheck everything initially
                    for (int i = 0; i < checkboxCount; i++)
                    {
                        if (i < checkboxCount / 2)
                        {
                            checkbox = WaitUntilElementExists(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl5__ctl0_repeaterEditItemDoubleA__ctl" + (i % (checkboxCount / 2)).ToString() + "_chkItemEdit')]"));

                            if (checkbox.Selected == true)
                                checkbox.Click();
                        }
                        else
                        {
                            checkbox = WaitUntilElementExists(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl5__ctl0_repeaterEditItemDoubleB__ctl" + (i % (checkboxCount / 2)).ToString() + "_chkItemEdit')]"));

                            if (checkbox.Selected == true)
                                checkbox.Click();
                        }

                    }

                    // check only the what is passed in
                    for (int i = 0; i < checkboxCount; i++)
                    {
                        if (i < checkboxCount / 2)
                        {
                            checkbox = WaitUntilElementExists(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl5__ctl0_repeaterEditItemDoubleA__ctl" + (i % (checkboxCount / 2)).ToString() + "_chkItemEdit')]"));
                            label = WaitUntilElementExists(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl5_editItemContainer')]/div/fieldset/ul[1]/li[" + ((i + 1) % ((checkboxCount / 2) + 1)).ToString() + "]/label"));
                        }
                        else
                        {
                            checkbox = WaitUntilElementExists(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl5__ctl0_repeaterEditItemDoubleB__ctl" + (i % (checkboxCount / 2)).ToString() + "_chkItemEdit')]"));
                            label = WaitUntilElementExists(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl5_editItemContainer')]/div/fieldset/ul[2]/li[" + ((i + 2) % ((checkboxCount / 2) + 1)).ToString() + "]/label"));
                        }

                        foreach (string currentValue in value)
                        {
                            if (label.Text == currentValue && checkbox.Selected == false)
                                checkbox.Click();
                        }

                    }

                    IWebElement save = driver.FindElement(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl5_btnEditSave')]"));
                    save.Click();
                }
            }
        }

        public List<string> MyFavoritePhysicalActivities_JDatePage
        {
            get
            {
                IWebElement element = null;

                if (driver.Url.Contains("jdate.com"))
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl3_essayItem')]/p"));
                else
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl6_essayItem')]/p"));

                List<string> selectedValues = element.Text.Split(',').ToList();

                for (int i = 0; i < selectedValues.Count; i++)
                    selectedValues[i] = selectedValues[i].Trim();

                return selectedValues;
            }
            set
            {
                if (driver.Url.Contains("jdate.com"))
                {
                    IWebElement element = driver.FindElement(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl3_essayItem')]/h3"));
                    RemoteWebElement clickToEdit = (RemoteWebElement)element;
                    HoverClick(clickToEdit);

                    int checkboxCount;

                    checkboxCount = 31;

                    IWebElement checkbox = null;
                    IWebElement label = null;

                    // uncheck everything initially
                    for (int i = 0; i < checkboxCount; i++)
                    {
                        if (i < (checkboxCount / 2) + 1)
                        {
                            checkbox = WaitUntilElementExists(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl3__ctl0_repeaterEditItemDoubleA__ctl" + (i % ((checkboxCount + 1) / 2)).ToString() + "_chkItemEdit')]"));

                            if (checkbox.Selected == true)
                                checkbox.Click();
                        }
                        else
                        {
                            checkbox = WaitUntilElementExists(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl3__ctl0_repeaterEditItemDoubleB__ctl" + (i % (checkboxCount / 2)).ToString() + "_chkItemEdit')]"));

                            if (checkbox.Selected == true)
                                checkbox.Click();
                        }

                    }

                    // check only the what is passed in
                    for (int i = 0; i < checkboxCount; i++)
                    {
                        if (i < (checkboxCount / 2) + 1)
                        {
                            checkbox = WaitUntilElementExists(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl3__ctl0_repeaterEditItemDoubleA__ctl" + (i % ((checkboxCount + 1) / 2)).ToString() + "_chkItemEdit')]"));
                            label = WaitUntilElementExists(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl3_editItemContainer')]/div/fieldset/ul[1]/li[" + ((i + 1) % ((checkboxCount / 2) + 2)).ToString() + "]/label"));
                        }
                        else
                        {
                            checkbox = WaitUntilElementExists(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl3__ctl0_repeaterEditItemDoubleB__ctl" + ((i - 1) % (checkboxCount / 2)).ToString() + "_chkItemEdit')]"));
                            label = WaitUntilElementExists(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl3_editItemContainer')]/div/fieldset/ul[2]/li[" + ((i + 1) % ((checkboxCount / 2) + 1)).ToString() + "]/label"));
                        }

                        foreach (string currentValue in value)
                        {
                            if (label.Text == currentValue && checkbox.Selected == false)
                                checkbox.Click();
                        }

                    }

                    IWebElement save = driver.FindElement(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl3_btnEditSave')]"));
                    save.Click();
                }
                else
                {
                    IWebElement element = driver.FindElement(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl6_essayItem')]/h3"));
                    RemoteWebElement clickToEdit = (RemoteWebElement)element;
                    HoverClick(clickToEdit);

                    int checkboxCount;

                    if (driver.Url.Contains("jdate.co.il"))
                        checkboxCount = 30;
                    else
                        checkboxCount = 31;

                    IWebElement checkbox = null;
                    IWebElement label = null;

                    if (driver.Url.Contains("jdate.co.il"))
                    {
                        // uncheck everything initially
                        for (int i = 0; i < checkboxCount; i++)
                        {
                            if (i < (checkboxCount / 2))
                            {
                                checkbox = WaitUntilElementExists(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl6__ctl0_repeaterEditItemDoubleA__ctl" + (i % ((checkboxCount + 1) / 2)).ToString() + "_chkItemEdit')]"));

                                if (checkbox.Selected == true)
                                    checkbox.Click();
                            }
                            else
                            {
                                checkbox = WaitUntilElementExists(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl6__ctl0_repeaterEditItemDoubleB__ctl" + (i % (checkboxCount / 2)).ToString() + "_chkItemEdit')]"));

                                if (checkbox.Selected == true)
                                    checkbox.Click();
                            }

                        }

                        // check only what is passed in
                        for (int i = 0; i < checkboxCount; i++)
                        {
                            if (i < (checkboxCount / 2))
                            {
                                checkbox = WaitUntilElementExists(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl6__ctl0_repeaterEditItemDoubleA__ctl" + (i % ((checkboxCount + 1) / 2)).ToString() + "_chkItemEdit')]"));
                                label = WaitUntilElementExists(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl6_editItemContainer')]/div/fieldset/ul[1]/li[" + ((i + 1) % ((checkboxCount / 2) + 2)).ToString() + "]/label"));
                            }
                            else
                            {
                                checkbox = WaitUntilElementExists(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl6__ctl0_repeaterEditItemDoubleB__ctl" + (i % (checkboxCount / 2)).ToString() + "_chkItemEdit')]"));
                                label = WaitUntilElementExists(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl6_editItemContainer')]/div/fieldset/ul[2]/li[" + ((i + 2) % ((checkboxCount / 2) + 1)).ToString() + "]/label"));
                            }

                            foreach (string currentValue in value)
                            {
                                if (label.Text == currentValue && checkbox.Selected == false)
                                    checkbox.Click();
                            }

                        }
                    }
                    else
                    {
                        // uncheck everything initially
                        for (int i = 0; i < checkboxCount; i++)
                        {
                            if (i < (checkboxCount / 2) + 1)
                            {
                                checkbox = WaitUntilElementExists(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl6__ctl0_repeaterEditItemDoubleA__ctl" + (i % ((checkboxCount + 1) / 2)).ToString() + "_chkItemEdit')]"));

                                if (checkbox.Selected == true)
                                    checkbox.Click();
                            }
                            else
                            {
                                checkbox = WaitUntilElementExists(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl6__ctl0_repeaterEditItemDoubleB__ctl" + (i % (checkboxCount / 2)).ToString() + "_chkItemEdit')]"));

                                if (checkbox.Selected == true)
                                    checkbox.Click();
                            }

                        }

                        // check only the what is passed in
                        for (int i = 0; i < checkboxCount; i++)
                        {
                            if (i < (checkboxCount / 2) + 1)
                            {
                                checkbox = WaitUntilElementExists(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl6__ctl0_repeaterEditItemDoubleA__ctl" + (i % ((checkboxCount + 1) / 2)).ToString() + "_chkItemEdit')]"));
                                label = WaitUntilElementExists(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl6_editItemContainer')]/div/fieldset/ul[1]/li[" + ((i + 1) % ((checkboxCount / 2) + 2)).ToString() + "]/label"));
                            }
                            else
                            {
                                checkbox = WaitUntilElementExists(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl6__ctl0_repeaterEditItemDoubleB__ctl" + ((i - 1) % (checkboxCount / 2)).ToString() + "_chkItemEdit')]"));
                                label = WaitUntilElementExists(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl6_editItemContainer')]/div/fieldset/ul[2]/li[" + ((i + 1) % ((checkboxCount / 2) + 1)).ToString() + "]/label"));
                            }

                            foreach (string currentValue in value)
                            {
                                if (label.Text == currentValue && checkbox.Selected == false)
                                    checkbox.Click();
                            }

                        }
                    }

                    IWebElement save = driver.FindElement(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl6_btnEditSave')]"));
                    save.Click();
                }
            }
        }

        public List<string> MyFavoriteFoods_JDatePage
        {
            get
            {
                IWebElement element = null;

                if (driver.Url.Contains("jdate.com"))
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl4_essayItem')]/p"));
                else
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl7_essayItem')]/p"));

                List<string> selectedValues = element.Text.Split(',').ToList();

                for (int i = 0; i < selectedValues.Count; i++)
                    selectedValues[i] = selectedValues[i].Trim();

                return selectedValues;
            }
            set
            {
                if (driver.Url.Contains("jdate.com"))
                {
                    IWebElement element = driver.FindElement(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl4_essayItem')]/h3"));
                    RemoteWebElement clickToEdit = (RemoteWebElement)element;
                    HoverClick(clickToEdit);

                    int checkboxCount;

                    if (driver.Url.Contains("jdate.co.il"))
                        checkboxCount = 24;
                    else
                        checkboxCount = 30;

                    IWebElement checkbox = null;
                    IWebElement label = null;

                    // uncheck everything initially
                    for (int i = 0; i < checkboxCount; i++)
                    {
                        if (i < checkboxCount / 2)
                        {
                            checkbox = WaitUntilElementExists(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl4__ctl0_repeaterEditItemDoubleA__ctl" + (i % (checkboxCount / 2)).ToString() + "_chkItemEdit')]"));

                            if (checkbox.Selected == true)
                                checkbox.Click();
                        }
                        else
                        {
                            checkbox = WaitUntilElementExists(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl4__ctl0_repeaterEditItemDoubleB__ctl" + (i % (checkboxCount / 2)).ToString() + "_chkItemEdit')]"));

                            if (checkbox.Selected == true)
                                checkbox.Click();
                        }

                    }

                    // check only the what is passed in
                    for (int i = 0; i < checkboxCount; i++)
                    {
                        if (i < checkboxCount / 2)
                        {
                            checkbox = WaitUntilElementExists(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl4__ctl0_repeaterEditItemDoubleA__ctl" + (i % (checkboxCount / 2)).ToString() + "_chkItemEdit')]"));
                            label = WaitUntilElementExists(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl4_editItemContainer')]/div/fieldset/ul[1]/li[" + ((i + 1) % ((checkboxCount / 2) + 1)).ToString() + "]/label"));
                        }
                        else
                        {
                            checkbox = WaitUntilElementExists(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl4__ctl0_repeaterEditItemDoubleB__ctl" + (i % (checkboxCount / 2)).ToString() + "_chkItemEdit')]"));
                            label = WaitUntilElementExists(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl4_editItemContainer')]/div/fieldset/ul[2]/li[" + ((i + 2) % ((checkboxCount / 2) + 1)).ToString() + "]/label"));
                        }

                        foreach (string currentValue in value)
                        {
                            if (label.Text == currentValue && checkbox.Selected == false)
                                checkbox.Click();
                        }

                    }

                    IWebElement save = driver.FindElement(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl4_btnEditSave')]"));
                    save.Click();
                }
                else
                {
                    IWebElement element = driver.FindElement(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl7_essayItem')]/h3"));
                    RemoteWebElement clickToEdit = (RemoteWebElement)element;
                    HoverClick(clickToEdit);

                    int checkboxCount;

                    if (driver.Url.Contains("jdate.co.il"))
                        checkboxCount = 24;
                    else
                        checkboxCount = 30;

                    IWebElement checkbox = null;
                    IWebElement label = null;

                    // uncheck everything initially
                    for (int i = 0; i < checkboxCount; i++)
                    {
                        if (i < checkboxCount / 2)
                        {
                            checkbox = WaitUntilElementExists(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl7__ctl0_repeaterEditItemDoubleA__ctl" + (i % (checkboxCount / 2)).ToString() + "_chkItemEdit')]"));

                            if (checkbox.Selected == true)
                                checkbox.Click();
                        }
                        else
                        {
                            checkbox = WaitUntilElementExists(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl7__ctl0_repeaterEditItemDoubleB__ctl" + (i % (checkboxCount / 2)).ToString() + "_chkItemEdit')]"));

                            if (checkbox.Selected == true)
                                checkbox.Click();
                        }

                    }

                    // check only the what is passed in
                    for (int i = 0; i < checkboxCount; i++)
                    {
                        if (i < checkboxCount / 2)
                        {
                            checkbox = WaitUntilElementExists(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl7__ctl0_repeaterEditItemDoubleA__ctl" + (i % (checkboxCount / 2)).ToString() + "_chkItemEdit')]"));
                            label = WaitUntilElementExists(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl7_editItemContainer')]/div/fieldset/ul[1]/li[" + ((i + 1) % ((checkboxCount / 2) + 1)).ToString() + "]/label"));
                        }
                        else
                        {
                            checkbox = WaitUntilElementExists(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl7__ctl0_repeaterEditItemDoubleB__ctl" + (i % (checkboxCount / 2)).ToString() + "_chkItemEdit')]"));
                            label = WaitUntilElementExists(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl7_editItemContainer')]/div/fieldset/ul[2]/li[" + ((i + 2) % ((checkboxCount / 2) + 1)).ToString() + "]/label"));
                        }

                        foreach (string currentValue in value)
                        {
                            if (label.Text == currentValue && checkbox.Selected == false)
                                checkbox.Click();
                        }

                    }

                    IWebElement save = driver.FindElement(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl7_btnEditSave')]"));
                    save.Click();
                }
            }
        }

        public List<string> MyFavoriteMusic_JDatePage
        {
            get
            {
                IWebElement element = null;

                if (driver.Url.Contains("jdate.com"))
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl5_essayItem')]/p"));
                else
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl8_essayItem')]/p"));

                List<string> selectedValues = element.Text.Split(',').ToList();

                for (int i = 0; i < selectedValues.Count; i++)
                    selectedValues[i] = selectedValues[i].Trim();

                return selectedValues;
            }
            set
            {
                if (driver.Url.Contains("jdate.com"))
                {
                    IWebElement element = driver.FindElement(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl5_essayItem')]/h3"));
                    RemoteWebElement clickToEdit = (RemoteWebElement)element;
                    HoverClick(clickToEdit);

                    int checkboxCount = 29;
                    IWebElement checkbox = null;
                    IWebElement label = null;

                    // uncheck everything initially
                    for (int i = 0; i < checkboxCount; i++)
                    {
                        if (i < (checkboxCount / 2) + 1)
                        {
                            checkbox = WaitUntilElementExists(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl5__ctl0_repeaterEditItemDoubleA__ctl" + (i % ((checkboxCount + 1) / 2)).ToString() + "_chkItemEdit')]"));

                            if (checkbox.Selected == true)
                                checkbox.Click();
                        }
                        else
                        {
                            checkbox = WaitUntilElementExists(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl5__ctl0_repeaterEditItemDoubleB__ctl" + (i % (checkboxCount / 2)).ToString() + "_chkItemEdit')]"));

                            if (checkbox.Selected == true)
                                checkbox.Click();
                        }

                    }

                    // check only the what is passed in
                    for (int i = 0; i < checkboxCount; i++)
                    {
                        if (i < (checkboxCount / 2) + 1)
                        {
                            checkbox = WaitUntilElementExists(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl5__ctl0_repeaterEditItemDoubleA__ctl" + (i % ((checkboxCount + 1) / 2)).ToString() + "_chkItemEdit')]"));
                            label = WaitUntilElementExists(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl5_editItemContainer')]/div/fieldset/ul[1]/li[" + ((i + 1) % ((checkboxCount / 2) + 2)).ToString() + "]/label"));
                        }
                        else
                        {
                            checkbox = WaitUntilElementExists(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl5__ctl0_repeaterEditItemDoubleB__ctl" + ((i - 1) % (checkboxCount / 2)).ToString() + "_chkItemEdit')]"));
                            label = WaitUntilElementExists(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl5_editItemContainer')]/div/fieldset/ul[2]/li[" + ((i + 1) % ((checkboxCount / 2) + 1)).ToString() + "]/label"));
                        }

                        foreach (string currentValue in value)
                        {
                            if (label.Text == currentValue && checkbox.Selected == false)
                                checkbox.Click();
                        }

                    }

                    IWebElement save = driver.FindElement(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl5_btnEditSave')]"));
                    save.Click();
                }
                else
                {
                    IWebElement element = driver.FindElement(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl8_essayItem')]/h3"));
                    RemoteWebElement clickToEdit = (RemoteWebElement)element;
                    HoverClick(clickToEdit);

                    int checkboxCount = 29;
                    IWebElement checkbox = null;
                    IWebElement label = null;

                    // uncheck everything initially
                    for (int i = 0; i < checkboxCount; i++)
                    {
                        if (i < (checkboxCount / 2) + 1)
                        {
                            checkbox = WaitUntilElementExists(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl8__ctl0_repeaterEditItemDoubleA__ctl" + (i % ((checkboxCount + 1) / 2)).ToString() + "_chkItemEdit')]"));

                            if (checkbox.Selected == true)
                                checkbox.Click();
                        }
                        else
                        {
                            checkbox = WaitUntilElementExists(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl8__ctl0_repeaterEditItemDoubleB__ctl" + (i % (checkboxCount / 2)).ToString() + "_chkItemEdit')]"));

                            if (checkbox.Selected == true)
                                checkbox.Click();
                        }

                    }

                    // check only the what is passed in
                    for (int i = 0; i < checkboxCount; i++)
                    {
                        if (i < (checkboxCount / 2) + 1)
                        {
                            checkbox = WaitUntilElementExists(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl8__ctl0_repeaterEditItemDoubleA__ctl" + (i % ((checkboxCount + 1) / 2)).ToString() + "_chkItemEdit')]"));
                            label = WaitUntilElementExists(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl8_editItemContainer')]/div/fieldset/ul[1]/li[" + ((i + 1) % ((checkboxCount / 2) + 2)).ToString() + "]/label"));
                        }
                        else
                        {
                            checkbox = WaitUntilElementExists(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl8__ctl0_repeaterEditItemDoubleB__ctl" + ((i - 1) % (checkboxCount / 2)).ToString() + "_chkItemEdit')]"));
                            label = WaitUntilElementExists(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl8_editItemContainer')]/div/fieldset/ul[2]/li[" + ((i + 1) % ((checkboxCount / 2) + 1)).ToString() + "]/label"));
                        }

                        foreach (string currentValue in value)
                        {
                            if (label.Text == currentValue && checkbox.Selected == false)
                                checkbox.Click();
                        }

                    }

                    IWebElement save = driver.FindElement(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl8_btnEditSave')]"));
                    save.Click();
                }
            }
        }

        public List<string> ILikeToRead
        {
            get
            {
                IWebElement element = null;

                if (driver.Url.Contains("jdate.com"))
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl6_essayItem')]/p"));
                else
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl9_essayItem')]/p"));

                List<string> selectedValues = element.Text.Split(',').ToList();

                for (int i = 0; i < selectedValues.Count; i++)
                    selectedValues[i] = selectedValues[i].Trim();

                return selectedValues;
            }
            set
            {
                if (driver.Url.Contains("jdate.com"))
                {
                    IWebElement element = driver.FindElement(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl6_essayItem')]/h3"));
                    RemoteWebElement clickToEdit = (RemoteWebElement)element;
                    HoverClick(clickToEdit);

                    int checkboxCount = 7;
                    IWebElement checkbox = null;
                    IWebElement label = null;

                    // uncheck everything initially
                    for (int i = 0; i < checkboxCount; i++)
                    {
                        if (i < (checkboxCount / 2) + 1)
                        {
                            checkbox = WaitUntilElementExists(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl6__ctl0_repeaterEditItemDoubleA__ctl" + (i % ((checkboxCount + 1) / 2)).ToString() + "_chkItemEdit')]"));

                            if (checkbox.Selected == true)
                                checkbox.Click();
                        }
                        else
                        {
                            checkbox = WaitUntilElementExists(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl6__ctl0_repeaterEditItemDoubleB__ctl" + (i % (checkboxCount / 2)).ToString() + "_chkItemEdit')]"));

                            if (checkbox.Selected == true)
                                checkbox.Click();
                        }

                    }

                    // check only the what is passed in
                    for (int i = 0; i < checkboxCount; i++)
                    {
                        if (i < (checkboxCount / 2) + 1)
                        {
                            checkbox = WaitUntilElementExists(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl6__ctl0_repeaterEditItemDoubleA__ctl" + (i % ((checkboxCount + 1) / 2)).ToString() + "_chkItemEdit')]"));
                            label = WaitUntilElementExists(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl6_editItemContainer')]/div/fieldset/ul[1]/li[" + ((i + 1) % ((checkboxCount / 2) + 2)).ToString() + "]/label"));
                        }
                        else
                        {
                            checkbox = WaitUntilElementExists(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl6__ctl0_repeaterEditItemDoubleB__ctl" + ((i - 1) % (checkboxCount / 2)).ToString() + "_chkItemEdit')]"));
                            label = WaitUntilElementExists(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl6_editItemContainer')]/div/fieldset/ul[2]/li[" + ((i + 1) % ((checkboxCount / 2) + 1)).ToString() + "]/label"));
                        }

                        foreach (string currentValue in value)
                        {
                            if (label.Text == currentValue && checkbox.Selected == false)
                                checkbox.Click();
                        }

                    }

                    IWebElement save = driver.FindElement(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl6_btnEditSave')]"));
                    save.Click();
                }
                else
                {
                    IWebElement element = driver.FindElement(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl9_essayItem')]/h3"));
                    RemoteWebElement clickToEdit = (RemoteWebElement)element;
                    HoverClick(clickToEdit);

                    int checkboxCount = 7;
                    IWebElement checkbox = null;
                    IWebElement label = null;

                    // uncheck everything initially
                    for (int i = 0; i < checkboxCount; i++)
                    {
                        if (i < (checkboxCount / 2) + 1)
                        {
                            checkbox = WaitUntilElementExists(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl9__ctl0_repeaterEditItemDoubleA__ctl" + (i % ((checkboxCount + 1) / 2)).ToString() + "_chkItemEdit')]"));

                            if (checkbox.Selected == true)
                                checkbox.Click();
                        }
                        else
                        {
                            checkbox = WaitUntilElementExists(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl9__ctl0_repeaterEditItemDoubleB__ctl" + (i % (checkboxCount / 2)).ToString() + "_chkItemEdit')]"));

                            if (checkbox.Selected == true)
                                checkbox.Click();
                        }

                    }

                    // check only the what is passed in
                    for (int i = 0; i < checkboxCount; i++)
                    {
                        if (i < (checkboxCount / 2) + 1)
                        {
                            checkbox = WaitUntilElementExists(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl9__ctl0_repeaterEditItemDoubleA__ctl" + (i % ((checkboxCount + 1) / 2)).ToString() + "_chkItemEdit')]"));
                            label = WaitUntilElementExists(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl9_editItemContainer')]/div/fieldset/ul[1]/li[" + ((i + 1) % ((checkboxCount / 2) + 2)).ToString() + "]/label"));
                        }
                        else
                        {
                            checkbox = WaitUntilElementExists(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl9__ctl0_repeaterEditItemDoubleB__ctl" + ((i - 1) % (checkboxCount / 2)).ToString() + "_chkItemEdit')]"));
                            label = WaitUntilElementExists(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl9_editItemContainer')]/div/fieldset/ul[2]/li[" + ((i + 1) % ((checkboxCount / 2) + 1)).ToString() + "]/label"));
                        }

                        foreach (string currentValue in value)
                        {
                            if (label.Text == currentValue && checkbox.Selected == false)
                                checkbox.Click();
                        }

                    }

                    IWebElement save = driver.FindElement(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl9_btnEditSave')]"));
                    save.Click();
                }
            }
        }
        #endregion

        #region Details - Physical Info
        public void PhysicalInfo_ClickToEdit()
        {
            int loopCount = 3;

            // we can't tell if the overlay appears because all objects exist before we can see them so we'll see if Save Changes is displayed once we click.
            for (int i = 0; i < loopCount; i++)
            {
                IWebElement element = driver.FindElement(By.XPath("//div[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl0_nameValueDataSection_viewSectionContainer')]"));
                IWebElement saveChanges = driver.FindElement(By.XPath("//input[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl0_nameValueDataSection_buttonEditSave')]"));

                RemoteWebElement clickToEdit = (RemoteWebElement)element;
                clickToEdit.Click();

                Thread.Sleep(1000);

                if (saveChanges.Displayed == true)
                    break;
            }
        }

        public void PhysicalInfo_SaveChanges()
        {
            IWebElement element = driver.FindElement(By.XPath("//input[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl0_nameValueDataSection_buttonEditSave')]"));
            element.Click();

            Thread.Sleep(1000);

            // sometimes for some reason this Save button doesn't get clicked correctly the first time around.  if it still exists, click it again
            element = driver.FindElement(By.XPath("//input[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl0_nameValueDataSection_buttonEditSave')]"));

            if (element.Displayed == true)
            {
                element.Click();
                Thread.Sleep(1000);
            }
        }

        public string Height_InOverlay
        {
            get
            {
                string theXpath = "//select[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl0_nameValueDataSection__ctl0_ddlEdit')]";
                return ReturnTextOfSelectedValueInList(By.XPath(theXpath));
            }
            set
            {
                SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl0_nameValueDataSection__ctl0_ddlEdit')]")));
                select.SelectByText(value);
            }
        }

        public string Weight_InOverlay
        {
            get
            {
                string theXpath = "//select[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl0_nameValueDataSection__ctl1_ddlEdit')]";
                return ReturnTextOfSelectedValueInList(By.XPath(theXpath));
            }
            set
            {
                SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl0_nameValueDataSection__ctl1_ddlEdit')]")));
                select.SelectByText(value);
            }
        }

        public string BodyType_InOverlay
        {
            get
            {
                string theXpath = null;

                if (driver.Url.Contains("jdate.com") || driver.Url.Contains("jdate.co.il"))
                    theXpath = "//select[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl0_nameValueDataSection__ctl2_ddlEdit')]";
                else
                    theXpath = "//select[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl0_nameValueDataSection__ctl1_ddlEdit')]";

                return ReturnTextOfSelectedValueInList(By.XPath(theXpath));
            }
            set
            {
                SelectElement select = null;

                if (driver.Url.Contains("jdate.com") || driver.Url.Contains("jdate.co.il"))
                    select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl0_nameValueDataSection__ctl2_ddlEdit')]")));
                else
                    select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl0_nameValueDataSection__ctl1_ddlEdit')]")));

                select.SelectByText(value);
            }
        }

        public string HairColor_InOverlay
        {
            get
            {
                string theXpath = null;

                if (driver.Url.Contains("jdate.com") || driver.Url.Contains("jdate.co.il"))
                    theXpath = "//select[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl0_nameValueDataSection__ctl3_ddlEdit')]";
                else
                    theXpath = "//select[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl0_nameValueDataSection__ctl2_ddlEdit')]";

                return ReturnTextOfSelectedValueInList(By.XPath(theXpath));
            }
            set
            {
                SelectElement select = null;

                if (driver.Url.Contains("jdate.com") || driver.Url.Contains("jdate.co.il"))
                    select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl0_nameValueDataSection__ctl3_ddlEdit')]")));
                else
                    select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl0_nameValueDataSection__ctl2_ddlEdit')]")));

                select.SelectByText(value);
            }
        }

        public string EyeColor_InOverlay
        {
            get
            {
                string theXpath = null;

                if (driver.Url.Contains("jdate.com") || driver.Url.Contains("jdate.co.il"))
                    theXpath = "//select[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl0_nameValueDataSection__ctl4_ddlEdit')]";
                else
                    theXpath = "//select[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl0_nameValueDataSection__ctl3_ddlEdit')]";

                return ReturnTextOfSelectedValueInList(By.XPath(theXpath));
            }
            set
            {
                SelectElement select = null;

                if (driver.Url.Contains("jdate.com") || driver.Url.Contains("jdate.co.il"))
                    select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl0_nameValueDataSection__ctl4_ddlEdit')]")));
                else
                    select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl0_nameValueDataSection__ctl3_ddlEdit')]")));

                select.SelectByText(value);
            }
        }

        public string Height
        {
            get
            {
                IWebElement element = driver.FindElement(By.XPath("//div[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl0_nameValueDataSection_viewSectionContainer')]/dl[@class='dl-form form-inside clearfix']/dd[1]"));
                return element.Text;
            }
        }

        public string Weight
        {
            get
            {
                IWebElement element = driver.FindElement(By.XPath("//div[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl0_nameValueDataSection_viewSectionContainer')]/dl[@class='dl-form form-inside clearfix']/dd[2]"));
                return element.Text;
            }
        }

        public string BodyType
        {
            get
            {
                IWebElement element = null;

                if (driver.Url.Contains("jdate.com") || driver.Url.Contains("jdate.co.il"))
                    element = driver.FindElement(By.XPath("//div[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl0_nameValueDataSection_viewSectionContainer')]/dl[@class='dl-form form-inside clearfix']/dd[3]"));
                else
                    element = driver.FindElement(By.XPath("//div[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl0_nameValueDataSection_viewSectionContainer')]/dl[@class='dl-form form-inside clearfix']/dd[2]"));

                return element.Text;
            }
        }

        public string HairColor
        {
            get
            {
                IWebElement element = null;

                if (driver.Url.Contains("jdate.com") || driver.Url.Contains("jdate.co.il"))
                    element = driver.FindElement(By.XPath("//div[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl0_nameValueDataSection_viewSectionContainer')]/dl[@class='dl-form form-inside clearfix']/dd[4]"));
                else
                    element = driver.FindElement(By.XPath("//div[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl0_nameValueDataSection_viewSectionContainer')]/dl[@class='dl-form form-inside clearfix']/dd[3]"));

                return element.Text;
            }
        }

        public string EyeColor
        {
            get
            {
                IWebElement element = null;

                if (driver.Url.Contains("jdate.com") || driver.Url.Contains("jdate.co.il"))
                    element = driver.FindElement(By.XPath("//div[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl0_nameValueDataSection_viewSectionContainer')]/dl[@class='dl-form form-inside clearfix']/dd[5]"));
                else
                    element = driver.FindElement(By.XPath("//div[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl0_nameValueDataSection_viewSectionContainer')]/dl[@class='dl-form form-inside clearfix']/dd[4]"));

                return element.Text;
            }
        }
        #endregion

        #region Details - Lifestyle
        public void Lifestyle_ClickToEdit()
        {
            int loopCount = 3;

            // we can't tell if the overlay appears because all objects exist before we can see them so we'll see if Save Changes is displayed once we click.
            for (int i = 0; i < loopCount; i++)
            {
                IWebElement element = driver.FindElement(By.XPath("//div[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl1_nameValueDataSection_viewSectionContainer')]"));
                IWebElement saveChanges = driver.FindElement(By.XPath("//input[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl1_nameValueDataSection_buttonEditSave')]"));

                RemoteWebElement clickToEdit = (RemoteWebElement)element;
                clickToEdit.Click();

                Thread.Sleep(1000);

                if (saveChanges.Displayed == true)
                    break;
            }
        }

        public void Lifestyle_SaveChanges()
        {
            IWebElement element = driver.FindElement(By.XPath("//input[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl1_nameValueDataSection_buttonEditSave')]"));
            element.Click();

            Thread.Sleep(1000);

            // sometimes for some reason this Save button doesn't get clicked correctly the first time around.  if it still exists, click it again
            element = driver.FindElement(By.XPath("//input[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl1_nameValueDataSection_buttonEditSave')]"));

            if (element.Displayed == true)
            {
                element.Click();
                Thread.Sleep(1000);
            }
        }

        public string MaritalStatus_InOverlay
        {
            get
            {
                string theXpath = "//select[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl1_nameValueDataSection__ctl0_ddlEdit')]";
                return ReturnTextOfSelectedValueInList(By.XPath(theXpath));
            }
            set
            {
                SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl1_nameValueDataSection__ctl0_ddlEdit')]")));
                select.SelectByText(value);
            }
        }

        public string Relocation_InOverlay
        {
            get
            {
                string theXpath = "//select[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl1_nameValueDataSection__ctl1_ddlEdit')]";
                return ReturnTextOfSelectedValueInList(By.XPath(theXpath));
            }
            set
            {
                SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl1_nameValueDataSection__ctl1_ddlEdit')]")));
                select.SelectByText(value);
            }
        }

        public string HaveKids_InOverlay
        {
            get
            {
                string theXpath = null;

                if (driver.Url.Contains("jdate.com") || driver.Url.Contains("spark.com") || driver.Url.Contains("jdate.co.il"))
                    theXpath = "//select[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl1_nameValueDataSection__ctl2_ddlEdit')]";
                else
                    theXpath = "//select[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl1_nameValueDataSection__ctl1_ddlEdit')]";

                return ReturnTextOfSelectedValueInList(By.XPath(theXpath));
            }
            set
            {
                SelectElement select = null;

                if (driver.Url.Contains("jdate.com") || driver.Url.Contains("spark.com") || driver.Url.Contains("jdate.co.il"))
                    select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl1_nameValueDataSection__ctl2_ddlEdit')]")));
                else
                    select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl1_nameValueDataSection__ctl1_ddlEdit')]")));

                select.SelectByText(value);
            }
        }

        public string Custody_InOverlay
        {
            get
            {
                string theXpath = null;

                if (driver.Url.Contains("jdate.com") || driver.Url.Contains("spark.com") || driver.Url.Contains("jdate.co.il"))
                    theXpath = "//select[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl1_nameValueDataSection__ctl3_ddlEdit')]";
                else
                    theXpath = "//select[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl1_nameValueDataSection__ctl2_ddlEdit')]";

                return ReturnTextOfSelectedValueInList(By.XPath(theXpath));
            }
            set
            {
                SelectElement select = null;

                if (driver.Url.Contains("jdate.com") || driver.Url.Contains("spark.com") || driver.Url.Contains("jdate.co.il"))
                    select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl1_nameValueDataSection__ctl3_ddlEdit')]")));
                else
                    select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl1_nameValueDataSection__ctl2_ddlEdit')]")));

                select.SelectByText(value);
            }
        }

        public List<string> Pets_InOverlay
        {
            get
            {
                IWebElement element = WaitUntilElementExists(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl5_essayItem')]/p"));
                List<string> selectedValues = element.Text.Split(',').ToList();

                for (int i = 0; i < selectedValues.Count; i++)
                    selectedValues[i] = selectedValues[i].Trim();

                return selectedValues;
            }
            set
            {
                int checkboxCount = 9;
                IWebElement checkbox = null;
                IWebElement label = null;

                // uncheck everything initially
                for (int i = 0; i < checkboxCount; i++)
                {
                    if (i < (checkboxCount / 2) + 1)
                    {
                        checkbox = WaitUntilElementExists(By.XPath("//input[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl1_nameValueDataSection__ctl5_repeaterDTCheckboxEditDoubleA__ctl" + (i % ((checkboxCount + 1) / 2)).ToString() + "_chkItemEdit')]"));

                        if (checkbox.Selected == true)
                            checkbox.Click();
                    }
                    else
                    {
                        checkbox = WaitUntilElementExists(By.XPath("//input[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl1_nameValueDataSection__ctl5_repeaterDTCheckboxEditDoubleB__ctl" + (i % (checkboxCount / 2)).ToString() + "_chkItemEdit')]"));

                        if (checkbox.Selected == true)
                            checkbox.Click();
                    }

                }

                // check only the what is passed in
                for (int i = 0; i < checkboxCount; i++)
                {
                    if (i < (checkboxCount / 2) + 1)
                    {
                        checkbox = WaitUntilElementExists(By.XPath("//input[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl1_nameValueDataSection__ctl5_repeaterDTCheckboxEditDoubleA__ctl" + (i % ((checkboxCount + 1) / 2)).ToString() + "_chkItemEdit')]"));
                        label = WaitUntilElementExists(By.XPath("//div[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl1_nameValueDataSection_editSectionContainer')]/fieldset/div[6]/div[2]/ul[1]/li[" + (i + 1) + "]/label"));
                    }
                    else
                    {
                        checkbox = WaitUntilElementExists(By.XPath("//input[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl1_nameValueDataSection__ctl5_repeaterDTCheckboxEditDoubleB__ctl" + ((i - 1) % (checkboxCount / 2)).ToString() + "_chkItemEdit')]"));
                        label = WaitUntilElementExists(By.XPath("//div[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl1_nameValueDataSection_editSectionContainer')]/fieldset/div[6]/div[2]/ul[2]/li[" + ((i + 1) % ((checkboxCount / 2) + 1)).ToString() + "]/label"));
                    }

                    foreach (string currentValue in value)
                    {
                        if (label.Text == currentValue && checkbox.Selected == false)
                            checkbox.Click();
                    }

                }
            }
        }

        // this is weird.  sometimes this is a dropdown, sometimes this is a radiobutton
        public string WantKids_InOverlay
        {
            get
            {
                // dropdown
                if (driver.FindElements(By.XPath("//select[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl1_nameValueDataSection__ctl3_ddlEdit')]")).Count > 0)
                {
                    string theXpath = null;

                    if (driver.Url.Contains("jdate.com") || driver.Url.Contains("spark.com") || driver.Url.Contains("jdate.co.il"))
                        theXpath = "//select[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl1_nameValueDataSection__ctl4_ddlEdit')]";
                    else
                        theXpath = "//select[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl1_nameValueDataSection__ctl3_ddlEdit')]";

                    return ReturnTextOfSelectedValueInList(By.XPath(theXpath));
                }
                // radiobutton
                else
                {
                    // if nothing is selected return a default of 2 - Not Sure
                    IWebElement element1 = WaitUntilElementExists(By.XPath("//input[@id='radioItemEdit_TheDetails_sectionLifestyleObj_MoreChildrenFlag0']"));
                    IWebElement element2 = WaitUntilElementExists(By.XPath("//input[@id='radioItemEdit_TheDetails_sectionLifestyleObj_MoreChildrenFlag1']"));

                    if (element1.Selected)
                        return "0";
                    else if (element2.Selected)
                        return "1";

                    return "2";
                }
            }
            set
            {
                // dropdown
                if (driver.FindElements(By.XPath("//select[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl1_nameValueDataSection__ctl3_ddlEdit')]")).Count > 0)
                {
                    SelectElement select = null;

                    if (driver.Url.Contains("jdate.com") || driver.Url.Contains("spark.com") || driver.Url.Contains("jdate.co.il"))
                        select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl1_nameValueDataSection__ctl4_ddlEdit')]")));
                    else
                        select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl1_nameValueDataSection__ctl3_ddlEdit')]")));

                    select.SelectByText(value);
                }
                // radiobutton
                else
                {
                    IWebElement element;

                    if (value == "0")
                    {
                        element = WaitUntilElementExists(By.XPath("//input[@id='radioItemEdit_TheDetails_sectionLifestyleObj_MoreChildrenFlag0']"));
                        element.Click();
                    }
                    else if (value == "1")
                    {
                        element = WaitUntilElementExists(By.XPath("//input[@id='radioItemEdit_TheDetails_sectionLifestyleObj_MoreChildrenFlag1']"));
                        element.Click();
                    }
                    else
                    {
                        element = WaitUntilElementExists(By.XPath("//input[@id='radioItemEdit_TheDetails_sectionLifestyleObj_MoreChildrenFlag2']"));
                        element.Click();
                    }
                }
            }
        }

        public string Kosher_InOverlay
        {
            get
            {
                string theXpath = "//select[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl1_nameValueDataSection__ctl6_ddlEdit')]";
                return ReturnTextOfSelectedValueInList(By.XPath(theXpath));
            }
            set
            {
                SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl1_nameValueDataSection__ctl6_ddlEdit')]")));
                select.SelectByText(value);
            }
        }

        public string Synagogue_InOverlay
        {
            get
            {
                string theXpath = "//select[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl1_nameValueDataSection__ctl7_ddlEdit')]";
                return ReturnTextOfSelectedValueInList(By.XPath(theXpath));
            }
            set
            {
                SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl1_nameValueDataSection__ctl7_ddlEdit')]")));
                select.SelectByText(value);
            }
        }

        public string Smoking_InOverlay
        {
            get
            {
                string theXpath = null;

                if (driver.Url.Contains("jdate.com") || driver.Url.Contains("jdate.co.il"))
                    theXpath = "//select[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl1_nameValueDataSection__ctl8_ddlEdit')]";
                else if (driver.Url.Contains("spark.com"))
                    theXpath = "//select[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl1_nameValueDataSection__ctl6_ddlEdit')]";
                else
                    theXpath = "//select[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl1_nameValueDataSection__ctl4_ddlEdit')]";

                return ReturnTextOfSelectedValueInList(By.XPath(theXpath));
            }
            set
            {
                SelectElement select = null;

                if (driver.Url.Contains("jdate.com") || driver.Url.Contains("jdate.co.il"))
                    select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl1_nameValueDataSection__ctl8_ddlEdit')]")));
                else if (driver.Url.Contains("spark.com"))
                    select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl1_nameValueDataSection__ctl6_ddlEdit')]")));
                else
                    select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl1_nameValueDataSection__ctl4_ddlEdit')]")));

                select.SelectByText(value);
            }
        }

        public string Drinking_InOverlay
        {
            get
            {
                string theXpath = null;

                if (driver.Url.Contains("jdate.com") || driver.Url.Contains("jdate.co.il"))
                    theXpath = "//select[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl1_nameValueDataSection__ctl9_ddlEdit')]";
                else if (driver.Url.Contains("spark.com"))
                    theXpath = "//select[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl1_nameValueDataSection__ctl7_ddlEdit')]";
                else
                    theXpath = "//select[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl1_nameValueDataSection__ctl5_ddlEdit')]";

                return ReturnTextOfSelectedValueInList(By.XPath(theXpath));
            }
            set
            {
                SelectElement select = null;

                if (driver.Url.Contains("jdate.com") || driver.Url.Contains("jdate.co.il"))
                    select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl1_nameValueDataSection__ctl9_ddlEdit')]")));
                else if (driver.Url.Contains("spark.com"))
                    select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl1_nameValueDataSection__ctl7_ddlEdit')]")));
                else
                    select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl1_nameValueDataSection__ctl5_ddlEdit')]")));

                select.SelectByText(value);
            }
        }

        public string ActivityLevel_InOverlay
        {
            get
            {
                string theXpath = null;

                if (driver.Url.Contains("jdate.com") || driver.Url.Contains("jdate.co.il"))
                    theXpath = "//select[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl1_nameValueDataSection__ctl10_ddlEdit')]";
                else
                    theXpath = "//select[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl1_nameValueDataSection__ctl8_ddlEdit')]";

                return ReturnTextOfSelectedValueInList(By.XPath(theXpath));
            }
            set
            {
                SelectElement select = null;

                if (driver.Url.Contains("jdate.com") || driver.Url.Contains("jdate.co.il"))
                    select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl1_nameValueDataSection__ctl10_ddlEdit')]")));
                else
                    select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl1_nameValueDataSection__ctl8_ddlEdit')]")));

                select.SelectByText(value);
            }
        }

        public string MaritalStatus
        {
            get
            {
                IWebElement element = driver.FindElement(By.XPath("//div[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl1_nameValueDataSection_viewSectionContainer')]/dl[@class='dl-form form-inside clearfix']/dd[1]"));
                return element.Text;
            }
        }

        public string Relocation
        {
            get
            {
                IWebElement element = driver.FindElement(By.XPath("//div[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl1_nameValueDataSection_viewSectionContainer')]/dl[@class='dl-form form-inside clearfix']/dd[2]"));
                return element.Text;
            }
        }

        public string HaveKids
        {
            get
            {
                IWebElement element = null;

                if (driver.Url.Contains("jdate.com") || driver.Url.Contains("spark.com") || driver.Url.Contains("jdate.co.il"))
                    element = driver.FindElement(By.XPath("//div[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl1_nameValueDataSection_viewSectionContainer')]/dl[@class='dl-form form-inside clearfix']/dd[3]"));
                else
                    element = driver.FindElement(By.XPath("//div[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl1_nameValueDataSection_viewSectionContainer')]/dl[@class='dl-form form-inside clearfix']/dd[2]"));

                return element.Text;
            }
        }

        public string Custody
        {
            get
            {
                IWebElement element = null;

                if (driver.Url.Contains("jdate.com") || driver.Url.Contains("spark.com") || driver.Url.Contains("jdate.co.il"))
                    element = driver.FindElement(By.XPath("//div[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl1_nameValueDataSection_viewSectionContainer')]/dl[@class='dl-form form-inside clearfix']/dd[4]"));
                else
                    element = driver.FindElement(By.XPath("//div[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl1_nameValueDataSection_viewSectionContainer')]/dl[@class='dl-form form-inside clearfix']/dd[3]"));

                return element.Text;
            }
        }

        public string WantKids
        {
            get
            {
                IWebElement element = null;

                if (driver.Url.Contains("jdate.com") || driver.Url.Contains("spark.com") || driver.Url.Contains("jdate.co.il"))
                    element = driver.FindElement(By.XPath("//div[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl1_nameValueDataSection_viewSectionContainer')]/dl[@class='dl-form form-inside clearfix']/dd[5]"));
                else
                    element = driver.FindElement(By.XPath("//div[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl1_nameValueDataSection_viewSectionContainer')]/dl[@class='dl-form form-inside clearfix']/dd[4]"));

                return element.Text;
            }
        }

        public List<string> Pets
        {
            get
            {
                IWebElement element = driver.FindElement(By.XPath("//div[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl1_nameValueDataSection_viewSectionContainer')]/dl[@class='dl-form form-inside clearfix']/dd[6]"));
                List<string> selectedValues = element.Text.Split(',').ToList();

                for (int i = 0; i < selectedValues.Count; i++)
                    selectedValues[i] = selectedValues[i].Trim();

                return selectedValues;
            }
        }

        public string Kosher
        {
            get
            {
                IWebElement element = driver.FindElement(By.XPath("//div[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl1_nameValueDataSection_viewSectionContainer')]/dl[@class='dl-form form-inside clearfix']/dd[7]"));
                return element.Text;
            }
        }

        public string Synagogue
        {
            get
            {
                IWebElement element = driver.FindElement(By.XPath("//div[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl1_nameValueDataSection_viewSectionContainer')]/dl[@class='dl-form form-inside clearfix']/dd[8]"));
                return element.Text;
            }
        }

        public string Smoking
        {
            get
            {
                IWebElement element = null;

                if (driver.Url.Contains("jdate.com") || driver.Url.Contains("jdate.co.il"))
                    element = driver.FindElement(By.XPath("//div[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl1_nameValueDataSection_viewSectionContainer')]/dl[@class='dl-form form-inside clearfix']/dd[9]"));
                else if (driver.Url.Contains("spark.com"))
                    element = driver.FindElement(By.XPath("//div[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl1_nameValueDataSection_viewSectionContainer')]/dl[@class='dl-form form-inside clearfix']/dd[7]"));
                else
                    element = driver.FindElement(By.XPath("//div[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl1_nameValueDataSection_viewSectionContainer')]/dl[@class='dl-form form-inside clearfix']/dd[5]"));

                return element.Text;
            }
        }

        public string Drinking
        {
            get
            {
                IWebElement element = null;

                if (driver.Url.Contains("jdate.com") || driver.Url.Contains("jdate.co.il"))
                    element = driver.FindElement(By.XPath("//div[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl1_nameValueDataSection_viewSectionContainer')]/dl[@class='dl-form form-inside clearfix']/dd[10]"));
                else if (driver.Url.Contains("spark.com"))
                    element = driver.FindElement(By.XPath("//div[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl1_nameValueDataSection_viewSectionContainer')]/dl[@class='dl-form form-inside clearfix']/dd[8]"));
                else
                    element = driver.FindElement(By.XPath("//div[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl1_nameValueDataSection_viewSectionContainer')]/dl[@class='dl-form form-inside clearfix']/dd[6]"));

                return element.Text;
            }
        }

        public string ActivityLevel
        {
            get
            {
                IWebElement element = driver.FindElement(By.XPath("//div[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl1_nameValueDataSection_viewSectionContainer')]/dl[@class='dl-form form-inside clearfix']/dd[11]"));
                return element.Text;
            }
        }
        #endregion

        #region Details - Background
        public void Background_ClickToEdit()
        {
            int loopCount = 3;

            // we can't tell if the overlay appears because all objects exist before we can see them so we'll see if Save Changes is displayed once we click.
            for (int i = 0; i < loopCount; i++)
            {
                IWebElement element = driver.FindElement(By.XPath("//div[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl2_nameValueDataSection_viewSectionContainer')]"));
                IWebElement saveChanges = driver.FindElement(By.XPath("//input[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl2_nameValueDataSection_buttonEditSave')]"));

                RemoteWebElement clickToEdit = (RemoteWebElement)element;
                clickToEdit.Click();

                Thread.Sleep(1000);

                if (saveChanges.Displayed == true)
                    break;
            }
        }

        public void Background_SaveChanges()
        {
            IWebElement element = driver.FindElement(By.XPath("//input[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl2_nameValueDataSection_buttonEditSave')]"));
            element.Click();

            Thread.Sleep(1000);

            // sometimes for some reason this Save button doesn't get clicked correctly the first time around.  if it still exists, click it again
            element = driver.FindElement(By.XPath("//input[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl2_nameValueDataSection_buttonEditSave')]"));

            if (element.Displayed == true)
            {
                element.Click();
                Thread.Sleep(1000);
            }
        }

        public string GrewUpIn_InOverlay
        {
            get
            {
                IWebElement element = WaitUntilElementExists(By.XPath("//input[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl2_nameValueDataSection__ctl0_txtEdit')]"));
                return element.GetAttribute("value");
            }
            set
            {
                string theXpath = "//input[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl2_nameValueDataSection__ctl0_txtEdit')]";

                WaitUntilElementValueCanBeCleared(By.XPath(theXpath));
                SendKeys(WaitUntilElementExists(By.XPath(theXpath)), value);
            }
        }

        public string Ethnicity_InOverlay
        {
            get
            {
                string theXpath = null;

                if (driver.Url.Contains("jdate.com") || driver.Url.Contains("spark.com") || driver.Url.Contains("jdate.co.il"))
                    theXpath = "//select[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl2_nameValueDataSection__ctl1_ddlEdit')]";
                else
                    theXpath = "//select[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl2_nameValueDataSection__ctl0_ddlEdit')]";

                return ReturnTextOfSelectedValueInList(By.XPath(theXpath));
            }
            set
            {
                SelectElement select = null;

                if (driver.Url.Contains("jdate.com") || driver.Url.Contains("spark.com") || driver.Url.Contains("jdate.co.il"))
                    select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl2_nameValueDataSection__ctl1_ddlEdit')]")));
                else
                    select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl2_nameValueDataSection__ctl0_ddlEdit')]")));

                select.SelectByText(value);
            }
        }

        public List<string> Languages_InOverlay
        {
            //get
            //{
            //    IWebElement element = WaitUntilElementExists(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_ProfileTabGroup1_repeaterTabContent__ctl1__ctl0_essaysDataGroup_repeaterSection__ctl0_repeaterItem__ctl5_essayItem')]/p"));
            //    List<string> selectedValues = element.Text.Split(',').ToList();

            //    for (int i = 0; i < selectedValues.Count; i++)
            //        selectedValues[i] = selectedValues[i].Trim();

            //    return selectedValues;
            //}
            set
            {
                int checkboxCount;

                if (driver.Url.Contains("jdate.co.il"))
                    checkboxCount = 16;
                else
                    checkboxCount = 31;

                IWebElement checkbox = null;
                IWebElement label = null;

                int loopCount;

                if (driver.Url.Contains("jdate.co.il"))
                    loopCount = checkboxCount / 2;
                else
                    loopCount = (checkboxCount / 2) + 1;

                // uncheck everything initially
                for (int i = 0; i < checkboxCount; i++)
                {
                    if (i < loopCount)
                    {
                        if (driver.Url.Contains("jdate.com") || driver.Url.Contains("spark.com") || driver.Url.Contains("jdate.co.il"))
                            checkbox = WaitUntilElementExists(By.XPath("//input[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl2_nameValueDataSection__ctl2_repeaterDTCheckboxEditDoubleA__ctl" + (i % ((checkboxCount + 1) / 2)).ToString() + "_chkItemEdit')]"));
                        else
                            checkbox = WaitUntilElementExists(By.XPath("//input[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl2_nameValueDataSection__ctl1_repeaterDTCheckboxEditDoubleA__ctl" + (i % ((checkboxCount + 1) / 2)).ToString() + "_chkItemEdit')]"));

                        if (checkbox.Selected == true)
                            checkbox.Click();
                    }
                    else
                    {
                        if (driver.Url.Contains("jdate.com") || driver.Url.Contains("spark.com") || driver.Url.Contains("jdate.co.il"))
                            checkbox = WaitUntilElementExists(By.XPath("//input[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl2_nameValueDataSection__ctl2_repeaterDTCheckboxEditDoubleB__ctl" + (i % ((checkboxCount + 1) / 2)).ToString() + "_chkItemEdit')]"));
                        else
                            checkbox = WaitUntilElementExists(By.XPath("//input[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl2_nameValueDataSection__ctl1_repeaterDTCheckboxEditDoubleB__ctl" + (i % (checkboxCount / 2)).ToString() + "_chkItemEdit')]"));

                        if (checkbox.Selected == true)
                            checkbox.Click();
                    }

                }

                // check only the what is passed in
                for (int i = 0; i < checkboxCount; i++)
                {
                    if (i < loopCount)
                    {
                        if (driver.Url.Contains("jdate.com") || driver.Url.Contains("spark.com") || driver.Url.Contains("jdate.co.il"))
                        {
                            checkbox = WaitUntilElementExists(By.XPath("//input[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl2_nameValueDataSection__ctl2_repeaterDTCheckboxEditDoubleA__ctl" + (i % ((checkboxCount + 1) / 2)).ToString() + "_chkItemEdit')]"));
                            label = WaitUntilElementExists(By.XPath("//div[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl2_nameValueDataSection_editSectionContainer')]/fieldset/div[3]/div[2]/ul[1]/li[" + (i + 1) + "]/label"));
                        }
                        else
                        {
                            checkbox = WaitUntilElementExists(By.XPath("//input[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl2_nameValueDataSection__ctl1_repeaterDTCheckboxEditDoubleA__ctl" + (i % ((checkboxCount + 1) / 2)).ToString() + "_chkItemEdit')]"));
                            label = WaitUntilElementExists(By.XPath("//div[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl2_nameValueDataSection_editSectionContainer')]/fieldset[@class='padding-heavy']/div[@class='form-item'][2]/div[@class='form-input']/ul[@class='two-column checkbox-list'][1]/li[" + (i + 1) + "]/label"));
                        }
                    }
                    else
                    {
                        if (driver.Url.Contains("jdate.com") || driver.Url.Contains("spark.com"))
                        {
                            checkbox = WaitUntilElementExists(By.XPath("//input[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl2_nameValueDataSection__ctl2_repeaterDTCheckboxEditDoubleB__ctl" + ((i - 1) % (checkboxCount / 2)).ToString() + "_chkItemEdit')]"));
                            label = WaitUntilElementExists(By.XPath("//div[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl2_nameValueDataSection_editSectionContainer')]/fieldset/div[3]/div[2]/ul[2]/li[" + ((i + 1) % ((checkboxCount / 2) + 1)).ToString() + "]/label"));
                        }
                        else if (driver.Url.Contains("jdate.co.il"))
                        {
                            checkbox = WaitUntilElementExists(By.XPath("//input[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl2_nameValueDataSection__ctl2_repeaterDTCheckboxEditDoubleB__ctl" + ((i) % (checkboxCount / 2)).ToString() + "_chkItemEdit')]"));
                            label = WaitUntilElementExists(By.XPath("//div[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl2_nameValueDataSection_editSectionContainer')]/fieldset/div[3]/div[2]/ul[2]/li[" + ((i + 2) % ((checkboxCount / 2) + 1)).ToString() + "]/label"));
                        }
                        else
                        {
                            checkbox = WaitUntilElementExists(By.XPath("//input[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl2_nameValueDataSection__ctl1_repeaterDTCheckboxEditDoubleB__ctl" + ((i - 1) % (checkboxCount / 2)).ToString() + "_chkItemEdit')]"));
                            label = WaitUntilElementExists(By.XPath("//div[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl2_nameValueDataSection_editSectionContainer')]/fieldset[@class='padding-heavy']/div[@class='form-item'][2]/div[@class='form-input']/ul[@class='two-column checkbox-list'][2]/li[" + ((i + 1) % ((checkboxCount / 2) + 1)).ToString() + "]/label"));
                        }
                    }

                    foreach (string currentValue in value)
                    {
                        if (label.Text == currentValue && checkbox.Selected == false)
                            checkbox.Click();
                    }

                }
            }
        }

        public string Religion_InOverlay
        {
            get
            {
                string theXpath = null;

                if (driver.Url.Contains("jdate.com") || driver.Url.Contains("spark.com") || driver.Url.Contains("jdate.co.il"))
                    theXpath = "//select[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl2_nameValueDataSection__ctl3_ddlEdit')]";
                else
                    theXpath = "//select[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl2_nameValueDataSection__ctl2_ddlEdit')]";

                return ReturnTextOfSelectedValueInList(By.XPath(theXpath));
            }
            set
            {
                SelectElement select = null;

                if (driver.Url.Contains("jdate.com") || driver.Url.Contains("spark.com") || driver.Url.Contains("jdate.co.il"))
                    select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl2_nameValueDataSection__ctl3_ddlEdit')]")));
                else
                    select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl2_nameValueDataSection__ctl2_ddlEdit')]")));

                select.SelectByText(value);
            }
        }

        public string Education_InOverlay
        {
            get
            {
                string theXpath = null;

                if (driver.Url.Contains("jdate.com") || driver.Url.Contains("spark.com") || driver.Url.Contains("jdate.co.il"))
                    theXpath = "//select[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl2_nameValueDataSection__ctl4_ddlEdit')]";
                else
                    theXpath = "//select[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl2_nameValueDataSection__ctl3_ddlEdit')]";

                return ReturnTextOfSelectedValueInList(By.XPath(theXpath));
            }
            set
            {
                SelectElement select = null;

                if (driver.Url.Contains("jdate.com") || driver.Url.Contains("spark.com") || driver.Url.Contains("jdate.co.il"))
                    select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl2_nameValueDataSection__ctl4_ddlEdit')]")));
                else
                    select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl2_nameValueDataSection__ctl3_ddlEdit')]")));

                select.SelectByText(value);
            }
        }

        public string AreaOfStudy_InOverlay
        {
            get
            {
                IWebElement element = WaitUntilElementExists(By.XPath("//input[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl2_nameValueDataSection__ctl5_txtEdit')]"));
                return element.GetAttribute("value");
            }
            set
            {
                string theXpath = "//input[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl2_nameValueDataSection__ctl5_txtEdit')]";

                WaitUntilElementValueCanBeCleared(By.XPath(theXpath));
                SendKeys(WaitUntilElementExists(By.XPath(theXpath)), value);
            }
        }

        public string Occupation_InOverlay
        {
            get
            {
                string theXpath = "//select[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl2_nameValueDataSection__ctl6_ddlEdit')]";
                return ReturnTextOfSelectedValueInList(By.XPath(theXpath));
            }
            set
            {
                SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl2_nameValueDataSection__ctl6_ddlEdit')]")));
                select.SelectByText(value);
            }
        }

        public string WhatIDo_InOverlay
        {
            get
            {
                IWebElement element = null;

                if (driver.Url.Contains("jdate.com") || driver.Url.Contains("spark.com") || driver.Url.Contains("jdate.co.il"))
                    element = WaitUntilElementExists(By.XPath("//input[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl2_nameValueDataSection__ctl7_txtEdit')]"));
                else
                    element = WaitUntilElementExists(By.XPath("//input[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl2_nameValueDataSection__ctl4_txtEdit')]"));

                return element.GetAttribute("value");
            }
            set
            {
                string theXpath = null;

                if (driver.Url.Contains("jdate.com") || driver.Url.Contains("spark.com") || driver.Url.Contains("jdate.co.il"))
                    theXpath = "//input[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl2_nameValueDataSection__ctl7_txtEdit')]";
                else
                    theXpath = "//input[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl2_nameValueDataSection__ctl4_txtEdit')]";

                WaitUntilElementValueCanBeCleared(By.XPath(theXpath));
                SendKeys(WaitUntilElementExists(By.XPath(theXpath)), value);
            }
        }

        public string AnnualIncome_InOverlay
        {
            get
            {
                string theXpath = null;

                if (driver.Url.Contains("jdate.com") || driver.Url.Contains("spark.com") || driver.Url.Contains("jdate.co.il"))
                    theXpath = "//select[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl2_nameValueDataSection__ctl8_ddlEdit')]";
                else
                    theXpath = "//select[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl2_nameValueDataSection__ctl2_ddlEdit')]";

                return ReturnTextOfSelectedValueInList(By.XPath(theXpath));
            }
            set
            {
                SelectElement select = null;

                if (driver.Url.Contains("jdate.com") || driver.Url.Contains("spark.com") || driver.Url.Contains("jdate.co.il"))
                    select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl2_nameValueDataSection__ctl8_ddlEdit')]")));
                else
                    select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl2_nameValueDataSection__ctl2_ddlEdit')]")));

                select.SelectByText(value);
            }
        }

        public string Politics_InOverlay
        {
            get
            {
                string theXpath = null;

                if (driver.Url.Contains("jdate.com") || driver.Url.Contains("spark.com") || driver.Url.Contains("jdate.co.il"))
                    theXpath = "//select[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl2_nameValueDataSection__ctl9_ddlEdit')]";
                else
                    theXpath = "//select[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl2_nameValueDataSection__ctl5_ddlEdit')]";

                return ReturnTextOfSelectedValueInList(By.XPath(theXpath));
            }
            set
            {
                SelectElement select = null;

                if (driver.Url.Contains("jdate.com") || driver.Url.Contains("spark.com") || driver.Url.Contains("jdate.co.il"))
                    select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl2_nameValueDataSection__ctl9_ddlEdit')]")));
                else
                    select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl2_nameValueDataSection__ctl5_ddlEdit')]")));

                select.SelectByText(value);
            }
        }

        public string ZodiacSign_InOverlay
        {
            get
            {
                string theXpath = "//select[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl2_nameValueDataSection__ctl10_ddlEdit')]";
                return ReturnTextOfSelectedValueInList(By.XPath(theXpath));
            }
            set
            {
                SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl2_nameValueDataSection__ctl10_ddlEdit')]")));
                select.SelectByText(value);
            }
        }

        public string GrewUpIn
        {
            get
            {
                IWebElement element = WaitUntilElementExists(By.XPath("//div[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl2_nameValueDataSection_viewSectionContainer')]/dl/dd[1]"));
                return element.Text;
            }
        }

        public string Ethnicity
        {
            get
            {
                IWebElement element = null;

                if (driver.Url.Contains("jdate.com") || driver.Url.Contains("spark.com") || driver.Url.Contains("jdate.co.il"))
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl2_nameValueDataSection_viewSectionContainer')]/dl/dd[2]"));
                else
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl2_nameValueDataSection_viewSectionContainer')]/dl/dd[1]"));

                return element.Text;
            }
        }

        public List<string> Languages
        {
            get
            {
                IWebElement element = null;

                if (driver.Url.Contains("jdate.com") || driver.Url.Contains("spark.com") || driver.Url.Contains("jdate.co.il"))
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl2_nameValueDataSection_viewSectionContainer')]/dl/dd[3]"));
                else
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl2_nameValueDataSection_viewSectionContainer')]/dl/dd[2]"));

                List<string> selectedValues = element.Text.Split(',').ToList();

                for (int i = 0; i < selectedValues.Count; i++)
                    selectedValues[i] = selectedValues[i].Trim();

                return selectedValues;
            }
        }

        public string Religion
        {
            get
            {
                IWebElement element = null;

                if (driver.Url.Contains("jdate.com") || driver.Url.Contains("spark.com") || driver.Url.Contains("jdate.co.il"))
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl2_nameValueDataSection_viewSectionContainer')]/dl/dd[4]"));
                else
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl2_nameValueDataSection_viewSectionContainer')]/dl/dd[3]"));

                return element.Text;
            }
        }

        public string Education
        {
            get
            {
                IWebElement element = null;

                if (driver.Url.Contains("jdate.com") || driver.Url.Contains("spark.com") || driver.Url.Contains("jdate.co.il"))
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl2_nameValueDataSection_viewSectionContainer')]/dl/dd[5]"));
                else
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl2_nameValueDataSection_viewSectionContainer')]/dl/dd[4]"));

                return element.Text;
            }
        }

        public string AreaOfStudy
        {
            get
            {
                IWebElement element = WaitUntilElementExists(By.XPath("//div[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl2_nameValueDataSection_viewSectionContainer')]/dl/dd[6]"));
                return element.Text;
            }
        }

        public string Occupation
        {
            get
            {
                IWebElement element = WaitUntilElementExists(By.XPath("//div[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl2_nameValueDataSection_viewSectionContainer')]/dl/dd[7]"));
                return element.Text;
            }
        }

        public string WhatIDo
        {
            get
            {
                IWebElement element = null;

                if (driver.Url.Contains("jdate.com") || driver.Url.Contains("spark.com") || driver.Url.Contains("jdate.co.il"))
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl2_nameValueDataSection_viewSectionContainer')]/dl/dd[8]"));
                else
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl2_nameValueDataSection_viewSectionContainer')]/dl/dd[5]"));

                return element.Text;
            }
        }

        public string AnnualIncome
        {
            get
            {
                IWebElement element = WaitUntilElementExists(By.XPath("//div[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl2_nameValueDataSection_viewSectionContainer')]/dl/dd[9]"));
                return element.Text;
            }
        }

        public string Politics
        {
            get
            {
                IWebElement element = null;

                if (driver.Url.Contains("jdate.com") || driver.Url.Contains("spark.com") || driver.Url.Contains("jdate.co.il"))
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl2_nameValueDataSection_viewSectionContainer')]/dl/dd[10]"));
                else
                    element = WaitUntilElementExists(By.XPath("//div[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl2_nameValueDataSection_viewSectionContainer')]/dl/dd[6]"));

                return element.Text;
            }
        }

        public string ZodiacSign
        {
            get
            {
                IWebElement element = WaitUntilElementExists(By.XPath("//div[contains(@id, '_profileDetails1_nvDataDetailsInfo_repeaterSectionOneColumn__ctl2_nameValueDataSection_viewSectionContainer')]/dl/dd[11]"));
                return element.Text;
            }
        }
        #endregion

        #region Your Basics
        public void YourBasics_ClickToEdit()
        {
            int loopCount = 3;

            // we can't tell if the overlay appears because all objects exist before we can see them so we'll see if Save Changes is displayed once we click.
            for (int i = 0; i < loopCount; i++)
            {
                IWebElement element = driver.FindElement(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_basicInfo1_profileSectionBasic')]"));
                IWebElement saveChanges = driver.FindElement(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_basicInfo1_btnEditSave')]"));

                RemoteWebElement clickToEdit = (RemoteWebElement)element;
                clickToEdit.Click();

                Thread.Sleep(1000);

                if (saveChanges.Displayed == true)
                    break;
            }
        }

        public void YourBasics_SaveChanges()
        {
            IWebElement element = null;

            if (driver.Url.Contains("jdate.com"))
                element = driver.FindElement(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_basicInfo1_btnEditSave')]"));
            else
                element = driver.FindElement(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_basicInfo1_btnEditSave')]"));

            element.Click();

            Thread.Sleep(1000);

            // sometimes for some reason this Save button doesn't get clicked correctly the first time around.  if it still exists, click it again
            if (driver.Url.Contains("jdate.com"))
                element = driver.FindElement(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_basicInfo1_btnEditSave')]"));
            else
                element = driver.FindElement(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_basicInfo1_btnEditSave')]"));

            if (element.Displayed == true)
            {
                element.Click();
                Thread.Sleep(1000);
            }
        }

        public string FirstName_InOverlay
        {
            get
            {
                IWebElement element = null;

                if (driver.Url.Contains("jdate.com"))
                    element = WaitUntilElementExists(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_basicInfo1__ctl4_txtEdit')]"));
                else
                    element = WaitUntilElementExists(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_basicInfo1__ctl4_txtEdit')]"));

                return element.GetAttribute("value");
            }
            set
            {
                string theXpath = null;

                if (driver.Url.Contains("jdate.com"))
                    theXpath = "//input[contains(@id, '_StandardProfileTemplate1_basicInfo1__ctl4_txtEdit')]";
                else
                    theXpath = "//input[contains(@id, '_StandardProfileTemplate1_basicInfo1__ctl4_txtEdit')]";

                WaitUntilElementValueCanBeCleared(By.XPath(theXpath));
                SendKeys(WaitUntilElementExists(By.XPath(theXpath)), value);
            }
        }

        public string LastName_InOverlay
        {
            get
            {
                IWebElement element = null;

                if (driver.Url.Contains("jdate.com"))
                    element = WaitUntilElementExists(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_basicInfo1__ctl5_txtEdit')]"));
                else
                    element = WaitUntilElementExists(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_basicInfo1__ctl5_txtEdit')]"));

                return element.GetAttribute("value");
            }
            set
            {
                string theXpath = null;

                if (driver.Url.Contains("jdate.com"))
                    theXpath = "//input[contains(@id, '_StandardProfileTemplate1_basicInfo1__ctl5_txtEdit')]";
                else
                    theXpath = "//input[contains(@id, '_StandardProfileTemplate1_basicInfo1__ctl5_txtEdit')]";

                WaitUntilElementValueCanBeCleared(By.XPath(theXpath));
                SendKeys(WaitUntilElementExists(By.XPath(theXpath)), value);
            }
        }

        public string YourDateOfBirthMonth_InOverlay
        {
            get
            {
                string theXpath = null;

                if (driver.Url.Contains("jdate.com"))
                    theXpath = "//select[contains(@id, '_StandardProfileTemplate1_basicInfo1__ctl6_ddlEditBirthdayMonth')]";
                else
                    theXpath = "//select[contains(@id, '_StandardProfileTemplate1_basicInfo1__ctl6_ddlEditBirthdayMonth')]";

                return ReturnTextOfSelectedValueInList(By.XPath(theXpath));
            }
            set
            {
                SelectElement select = null;

                if (driver.Url.Contains("jdate.com"))
                    select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, '_StandardProfileTemplate1_basicInfo1__ctl6_ddlEditBirthdayMonth')]")));
                else
                    select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, '_StandardProfileTemplate1_basicInfo1__ctl6_ddlEditBirthdayMonth')]")));

                select.SelectByText(value);
            }
        }

        public string YourDateOfBirthDay_InOverlay
        {
            get
            {
                string theXpath = null;

                if (driver.Url.Contains("jdate.com"))
                    theXpath = "//select[contains(@id, '_StandardProfileTemplate1_basicInfo1__ctl6_ddlEditBirthdayDay')]";
                else
                    theXpath = "//select[contains(@id, '_StandardProfileTemplate1_basicInfo1__ctl6_ddlEditBirthdayDay')]";
                return ReturnTextOfSelectedValueInList(By.XPath(theXpath));
            }
            set
            {
                SelectElement select = null;

                if (driver.Url.Contains("jdate.com"))
                    select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, '_StandardProfileTemplate1_basicInfo1__ctl6_ddlEditBirthdayDay')]")));
                else
                    select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, '_StandardProfileTemplate1_basicInfo1__ctl6_ddlEditBirthdayDay')]")));
                select.SelectByText(value);
            }
        }

        public string YourDateOfBirthYear_InOverlay
        {
            get
            {
                IWebElement element = null;

                if (driver.Url.Contains("jdate.com"))
                    element = WaitUntilElementExists(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_basicInfo1__ctl6_txtEditBirthdayYear')]"));
                else
                    element = WaitUntilElementExists(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_basicInfo1__ctl6_txtEditBirthdayYear')]"));

                return element.GetAttribute("value");
            }
            set
            {
                string theXpath = null;

                if (driver.Url.Contains("jdate.com"))
                    theXpath = "//input[contains(@id, '_StandardProfileTemplate1_basicInfo1__ctl6_txtEditBirthdayYear')]";
                else
                    theXpath = "//input[contains(@id, '_StandardProfileTemplate1_basicInfo1__ctl6_txtEditBirthdayYear')]";

                WaitUntilElementValueCanBeCleared(By.XPath(theXpath));
                SendKeys(WaitUntilElementExists(By.XPath(theXpath)), value);
            }
        }

        public string Country_InOverlay
        {
            get
            {
                string theXpath = null;

                if (driver.Url.Contains("jdate.com"))
                    theXpath = "//select[contains(@id, '_StandardProfileTemplate1_basicInfo1__ctl7_ddlEditCountry')]";
                else
                    theXpath = "//select[contains(@id, '_StandardProfileTemplate1_basicInfo1__ctl7_ddlEditCountry')]";

                return ReturnTextOfSelectedValueInList(By.XPath(theXpath));
            }
            set
            {
                SelectElement select = null;

                if (driver.Url.Contains("jdate.com"))
                    select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, '_StandardProfileTemplate1_basicInfo1__ctl7_ddlEditCountry')]")));
                else
                    select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, '_StandardProfileTemplate1_basicInfo1__ctl7_ddlEditCountry')]")));

                select.SelectByText(value);
            }
        }

        public string ZipCode_InOverlay
        {
            get
            {
                IWebElement element = null;

                if (driver.Url.Contains("jdate.com"))
                    element = WaitUntilElementExists(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_basicInfo1__ctl7_txtEditZipCode')]"));
                else
                    element = WaitUntilElementExists(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_basicInfo1__ctl7_txtEditZipCode')]"));

                return element.GetAttribute("value");
            }
            set
            {
                string theXpath = null;

                if (driver.Url.Contains("jdate.com"))
                    theXpath = "//input[contains(@id, '_StandardProfileTemplate1_basicInfo1__ctl7_txtEditZipCode')]";
                else
                    theXpath = "//input[contains(@id, '_StandardProfileTemplate1_basicInfo1__ctl7_txtEditZipCode')]";

                WaitUntilElementValueCanBeCleared(By.XPath(theXpath));
                SendKeys(WaitUntilElementExists(By.XPath(theXpath)), value);
            }
        }

        public string ImA_InOverlay
        {
            get
            {
                string theXpath = null;

                if (driver.Url.Contains("jdate.com"))
                    theXpath = "//select[contains(@id, '_StandardProfileTemplate1_basicInfo1__ctl8_ddlEdit')]";
                else
                    theXpath = "//select[contains(@id, '_StandardProfileTemplate1_basicInfo1__ctl8_ddlEdit')]";

                return ReturnTextOfSelectedValueInList(By.XPath(theXpath));
            }
            set
            {
                SelectElement select = null;

                if (driver.Url.Contains("jdate.com"))
                    select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, '_StandardProfileTemplate1_basicInfo1__ctl8_ddlEdit')]")));
                else
                    select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, '_StandardProfileTemplate1_basicInfo1__ctl8_ddlEdit')]")));

                select.SelectByText(value);
            }
        }

        public string RelationshipStatus_InOverlay
        {
            get
            {
                string theXpath = null;

                if (driver.Url.Contains("jdate.com"))
                    theXpath = "//select[contains(@id, '_StandardProfileTemplate1_basicInfo1__ctl9_ddlEdit')]";
                else
                    theXpath = "//select[contains(@id, '_StandardProfileTemplate1_basicInfo1__ctl9_ddlEdit')]";

                return ReturnTextOfSelectedValueInList(By.XPath(theXpath));
            }
            set
            {
                SelectElement select = null;

                if (driver.Url.Contains("jdate.com"))
                    select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, '_StandardProfileTemplate1_basicInfo1__ctl9_ddlEdit')]")));
                else
                    select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, '_StandardProfileTemplate1_basicInfo1__ctl9_ddlEdit')]")));

                select.SelectByText(value);
            }
        }

        public string RelationshipStatus
        {
            get
            {
                IWebElement element = WaitUntilElementExists(By.XPath("//ul[@class='interest']/li[1]"));

                List<string> selectedValues = element.Text.Split(',').ToList();

                for (int i = 0; i < selectedValues.Count; i++)
                    selectedValues[i] = selectedValues[i].Trim();

                return selectedValues[0];
            }
        }

        public string ImA
        {
            get
            {
                IWebElement element = WaitUntilElementExists(By.XPath("//ul[@class='interest']/li[1]"));

                List<string> selectedValues = element.Text.Split(',').ToList();

                for (int i = 0; i < selectedValues.Count; i++)
                    selectedValues[i] = selectedValues[i].Trim();

                return selectedValues[1];
            }
        }

        public string Age
        {
            get
            {
                IWebElement element = WaitUntilElementExists(By.XPath("//ul[@class='interest']/li[2]"));

                List<string> selectedValues = element.Text.Split(' ').ToList();

                for (int i = 0; i < selectedValues.Count; i++)
                    selectedValues[i] = selectedValues[i].Trim();

                if (driver.Url.Contains("jdate.co.il"))
                    return selectedValues[1];
                else
                    return selectedValues[0];
            }
        }

        public string Country
        {
            get
            {
                IWebElement element = WaitUntilElementExists(By.XPath("//ul[@class='interest']/li[2]"));

                List<string> selectedValues = element.Text.Split(' ').ToList();

                for (int i = 0; i < selectedValues.Count; i++)
                    selectedValues[i] = selectedValues[i].Trim();

                return selectedValues[5];
            }
        }
        #endregion

        #region Dealbreakers
        public void Dealbreakers_ClickToEdit()
        {
            int loopCount = 3;

            // we can't tell if the overlay appears because all objects exist before we can see them so we'll see if Save Changes is displayed once we click.
            for (int i = 0; i < loopCount; i++)
            {
                IWebElement element = driver.FindElement(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_nvDataGroupDealBreakerInfo_repeaterSectionTwoColumn__ctl0_nameValueDataSection_viewSectionContainer')]"));
                IWebElement saveChanges = driver.FindElement(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_nvDataGroupDealBreakerInfo_repeaterSectionTwoColumn__ctl0_nameValueDataSection_buttonEditSave')]"));

                RemoteWebElement clickToEdit = (RemoteWebElement)element;
                clickToEdit.Click();

                Thread.Sleep(1000);

                if (saveChanges.Displayed == true)
                    break;
            }
        }

        public void Dealbreakers_SaveChanges()
        {
            IWebElement element = null;

            element = driver.FindElement(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_nvDataGroupDealBreakerInfo_repeaterSectionTwoColumn__ctl0_nameValueDataSection_buttonEditSave')]"));

            element.Click();

            Thread.Sleep(1000);

            // sometimes for some reason this Save button doesn't get clicked correctly the first time around.  if it still exists, click it again
            element = driver.FindElement(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_nvDataGroupDealBreakerInfo_repeaterSectionTwoColumn__ctl0_nameValueDataSection_buttonEditSave')]"));

            if (element.Displayed == true)
            {
                element.Click();
                Thread.Sleep(1000);
            }
        }

        public string Height_InDealbreakersOverlay
        {
            get
            {
                string theXpath = null;

                if (driver.Url.Contains("jdate.com") || driver.Url.Contains("spark.com") || driver.Url.Contains("jdate.co.il"))
                    theXpath = "//select[contains(@id, '_StandardProfileTemplate1_nvDataGroupDealBreakerInfo_repeaterSectionTwoColumn__ctl0_nameValueDataSection__ctl2_ddlEdit')]";
                else
                    theXpath = "//select[contains(@id, '_StandardProfileTemplate1_nvDataGroupDealBreakerInfo_repeaterSectionTwoColumn__ctl0_nameValueDataSection__ctl0_ddlEdit')]";

                return ReturnTextOfSelectedValueInList(By.XPath(theXpath));
            }
            set
            {
                SelectElement select = null;

                if (driver.Url.Contains("jdate.com") || driver.Url.Contains("spark.com") || driver.Url.Contains("jdate.co.il"))
                    select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, '_StandardProfileTemplate1_nvDataGroupDealBreakerInfo_repeaterSectionTwoColumn__ctl0_nameValueDataSection__ctl2_ddlEdit')]")));
                else
                    select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, '_StandardProfileTemplate1_nvDataGroupDealBreakerInfo_repeaterSectionTwoColumn__ctl0_nameValueDataSection__ctl0_ddlEdit')]")));

                select.SelectByText(value);
            }
        }

        public string BodyType_InDealbreakersOverlay
        {
            get
            {
                string theXpath = "//select[contains(@id, '_StandardProfileTemplate1_nvDataGroupDealBreakerInfo_repeaterSectionTwoColumn__ctl0_nameValueDataSection__ctl1_ddlEdit')]";
                return ReturnTextOfSelectedValueInList(By.XPath(theXpath));
            }
            set
            {
                SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, '_StandardProfileTemplate1_nvDataGroupDealBreakerInfo_repeaterSectionTwoColumn__ctl0_nameValueDataSection__ctl1_ddlEdit')]")));
                select.SelectByText(value);
            }
        }

        public string Ethnicity_InDealbreakersOverlay
        {
            get
            {
                string theXpath = null;

                if (driver.Url.Contains("jdate.com") || driver.Url.Contains("spark.com") || driver.Url.Contains("jdate.co.il"))
                    theXpath = "//select[contains(@id, '_StandardProfileTemplate1_nvDataGroupDealBreakerInfo_repeaterSectionTwoColumn__ctl0_nameValueDataSection__ctl3_ddlEdit')]";
                else
                    theXpath = "//select[contains(@id, '_StandardProfileTemplate1_nvDataGroupDealBreakerInfo_repeaterSectionTwoColumn__ctl0_nameValueDataSection__ctl2_ddlEdit')]";

                return ReturnTextOfSelectedValueInList(By.XPath(theXpath));
            }
            set
            {
                SelectElement select = null;

                if (driver.Url.Contains("jdate.com") || driver.Url.Contains("spark.com") || driver.Url.Contains("jdate.co.il"))
                    select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, '_StandardProfileTemplate1_nvDataGroupDealBreakerInfo_repeaterSectionTwoColumn__ctl0_nameValueDataSection__ctl3_ddlEdit')]")));
                else
                    select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, '_StandardProfileTemplate1_nvDataGroupDealBreakerInfo_repeaterSectionTwoColumn__ctl0_nameValueDataSection__ctl2_ddlEdit')]")));

                select.SelectByText(value);
            }
        }

        public string Smoking_InDealbreakersOverlay
        {
            get
            {
                string theXpath = null;

                if (driver.Url.Contains("jdate.com"))
                    theXpath = "//select[contains(@id, '_StandardProfileTemplate1_nvDataGroupDealBreakerInfo_repeaterSectionTwoColumn__ctl0_nameValueDataSection__ctl4_ddlEdit')]";
                else if (driver.Url.Contains("spark.com") || driver.Url.Contains("jdate.co.il"))
                    theXpath = "//select[contains(@id, '_StandardProfileTemplate1_nvDataGroupDealBreakerInfo_repeaterSectionTwoColumn__ctl0_nameValueDataSection__ctl5_ddlEdit')]";
                else
                    theXpath = "//select[contains(@id, '_StandardProfileTemplate1_nvDataGroupDealBreakerInfo_repeaterSectionTwoColumn__ctl0_nameValueDataSection__ctl3_ddlEdit')]";

                return ReturnTextOfSelectedValueInList(By.XPath(theXpath));
            }
            set
            {
                SelectElement select = null;

                if (driver.Url.Contains("jdate.com"))
                    select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, '_StandardProfileTemplate1_nvDataGroupDealBreakerInfo_repeaterSectionTwoColumn__ctl0_nameValueDataSection__ctl4_ddlEdit')]")));
                else if (driver.Url.Contains("spark.com") || driver.Url.Contains("jdate.co.il"))
                    select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, '_StandardProfileTemplate1_nvDataGroupDealBreakerInfo_repeaterSectionTwoColumn__ctl0_nameValueDataSection__ctl5_ddlEdit')]")));
                else
                    select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, '_StandardProfileTemplate1_nvDataGroupDealBreakerInfo_repeaterSectionTwoColumn__ctl0_nameValueDataSection__ctl3_ddlEdit')]")));

                select.SelectByText(value);
            }
        }

        public string Drinking_InDealbreakersOverlay
        {
            get
            {
                string theXpath = null;

                if (driver.Url.Contains("jdate.com") || driver.Url.Contains("spark.com") || driver.Url.Contains("jdate.co.il"))
                    theXpath = "//select[contains(@id, '_StandardProfileTemplate1_nvDataGroupDealBreakerInfo_repeaterSectionTwoColumn__ctl0_nameValueDataSection__ctl6_ddlEdit')]";
                else
                    theXpath = "//select[contains(@id, '_StandardProfileTemplate1_nvDataGroupDealBreakerInfo_repeaterSectionTwoColumn__ctl0_nameValueDataSection__ctl4_ddlEdit')]";

                return ReturnTextOfSelectedValueInList(By.XPath(theXpath));
            }
            set
            {
                SelectElement select = null;

                if (driver.Url.Contains("jdate.com") || driver.Url.Contains("spark.com") || driver.Url.Contains("jdate.co.il"))
                    select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, '_StandardProfileTemplate1_nvDataGroupDealBreakerInfo_repeaterSectionTwoColumn__ctl0_nameValueDataSection__ctl6_ddlEdit')]")));
                else
                    select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, '_StandardProfileTemplate1_nvDataGroupDealBreakerInfo_repeaterSectionTwoColumn__ctl0_nameValueDataSection__ctl4_ddlEdit')]")));

                select.SelectByText(value);
            }
        }

        public string Religion_InDealbreakersOverlay
        {
            get
            {
                string theXpath = null;

                if (driver.Url.Contains("jdate.com"))
                    theXpath = "//select[contains(@id, '_StandardProfileTemplate1_nvDataGroupDealBreakerInfo_repeaterSectionTwoColumn__ctl0_nameValueDataSection__ctl3_ddlEdit')]";
                else if (driver.Url.Contains("spark.com") || driver.Url.Contains("jdate.co.il"))
                    theXpath = "//select[contains(@id, '_StandardProfileTemplate1_nvDataGroupDealBreakerInfo_repeaterSectionTwoColumn__ctl0_nameValueDataSection__ctl4_ddlEdit')]";
                else
                    theXpath = "//select[contains(@id, '_StandardProfileTemplate1_nvDataGroupDealBreakerInfo_repeaterSectionTwoColumn__ctl0_nameValueDataSection__ctl5_ddlEdit')]";

                return ReturnTextOfSelectedValueInList(By.XPath(theXpath));
            }
            set
            {
                SelectElement select = null;

                if (driver.Url.Contains("jdate.com"))
                    select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, '_StandardProfileTemplate1_nvDataGroupDealBreakerInfo_repeaterSectionTwoColumn__ctl0_nameValueDataSection__ctl3_ddlEdit')]")));
                else if (driver.Url.Contains("spark.com") || driver.Url.Contains("jdate.co.il"))
                    select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, '_StandardProfileTemplate1_nvDataGroupDealBreakerInfo_repeaterSectionTwoColumn__ctl0_nameValueDataSection__ctl4_ddlEdit')]")));
                else
                    select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, '_StandardProfileTemplate1_nvDataGroupDealBreakerInfo_repeaterSectionTwoColumn__ctl0_nameValueDataSection__ctl5_ddlEdit')]")));

                select.SelectByText(value);
            }
        }

        public string Education_InDealbreakersOverlay
        {
            get
            {
                string theXpath = null;

                if (driver.Url.Contains("jdate.com"))
                    theXpath = "//select[contains(@id, '_StandardProfileTemplate1_nvDataGroupDealBreakerInfo_repeaterSectionTwoColumn__ctl0_nameValueDataSection__ctl5_ddlEdit')]";
                else if (driver.Url.Contains("spark.com") || driver.Url.Contains("jdate.co.il"))
                    theXpath = "//select[contains(@id, '_StandardProfileTemplate1_nvDataGroupDealBreakerInfo_repeaterSectionTwoColumn__ctl0_nameValueDataSection__ctl7_ddlEdit')]";
                else
                    theXpath = "//select[contains(@id, '_StandardProfileTemplate1_nvDataGroupDealBreakerInfo_repeaterSectionTwoColumn__ctl0_nameValueDataSection__ctl6_ddlEdit')]";

                return ReturnTextOfSelectedValueInList(By.XPath(theXpath));
            }
            set
            {
                SelectElement select = null;

                if (driver.Url.Contains("jdate.com"))
                    select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, '_StandardProfileTemplate1_nvDataGroupDealBreakerInfo_repeaterSectionTwoColumn__ctl0_nameValueDataSection__ctl5_ddlEdit')]")));
                else if (driver.Url.Contains("spark.com") || driver.Url.Contains("jdate.co.il"))
                    select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, '_StandardProfileTemplate1_nvDataGroupDealBreakerInfo_repeaterSectionTwoColumn__ctl0_nameValueDataSection__ctl7_ddlEdit')]")));
                else
                    select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, '_StandardProfileTemplate1_nvDataGroupDealBreakerInfo_repeaterSectionTwoColumn__ctl0_nameValueDataSection__ctl6_ddlEdit')]")));

                select.SelectByText(value);
            }
        }

        public string Occupation_InDealbreakersOverlay
        {
            get
            {
                string theXpath = null;

                if (driver.Url.Contains("spark.com") || driver.Url.Contains("jdate.co.il"))
                    theXpath = "//select[contains(@id, '_StandardProfileTemplate1_nvDataGroupDealBreakerInfo_repeaterSectionTwoColumn__ctl0_nameValueDataSection__ctl8_ddlEdit')]";
                else
                    theXpath = "//select[contains(@id, '_StandardProfileTemplate1_nvDataGroupDealBreakerInfo_repeaterSectionTwoColumn__ctl0_nameValueDataSection__ctl6_ddlEdit')]";

                return ReturnTextOfSelectedValueInList(By.XPath(theXpath));
            }
            set
            {
                SelectElement select = null;

                if (driver.Url.Contains("spark.com") || driver.Url.Contains("jdate.co.il"))
                    select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, '_StandardProfileTemplate1_nvDataGroupDealBreakerInfo_repeaterSectionTwoColumn__ctl0_nameValueDataSection__ctl8_ddlEdit')]")));
                else
                    select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, '_StandardProfileTemplate1_nvDataGroupDealBreakerInfo_repeaterSectionTwoColumn__ctl0_nameValueDataSection__ctl6_ddlEdit')]")));

                select.SelectByText(value);
            }
        }

        public string AnnualIncome_InDealbreakersOverlay
        {
            get
            {
                string theXpath = "//select[contains(@id, '_StandardProfileTemplate1_nvDataGroupDealBreakerInfo_repeaterSectionTwoColumn__ctl0_nameValueDataSection__ctl9_ddlEdit')]";
                return ReturnTextOfSelectedValueInList(By.XPath(theXpath));
            }
            set
            {
                SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, '_StandardProfileTemplate1_nvDataGroupDealBreakerInfo_repeaterSectionTwoColumn__ctl0_nameValueDataSection__ctl9_ddlEdit')]")));
                select.SelectByText(value);
            }
        }

        public string Synagogue_InDealbreakersOverlay
        {
            get
            {
                string theXpath = "//select[contains(@id, '_StandardProfileTemplate1_nvDataGroupDealBreakerInfo_repeaterSectionTwoColumn__ctl0_nameValueDataSection__ctl7_ddlEdit')]";
                return ReturnTextOfSelectedValueInList(By.XPath(theXpath));
            }
            set
            {
                SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, '_StandardProfileTemplate1_nvDataGroupDealBreakerInfo_repeaterSectionTwoColumn__ctl0_nameValueDataSection__ctl7_ddlEdit')]")));
                select.SelectByText(value);
            }
        }

        public string WhatIDo_InDealbreakersOverlay
        {
            get
            {
                IWebElement element = WaitUntilElementExists(By.XPath("//input[contains(@id, '_StandardProfileTemplate1_nvDataGroupDealBreakerInfo_repeaterSectionTwoColumn__ctl0_nameValueDataSection__ctl7_txtEdit')]"));
                return element.GetAttribute("value");
            }
            set
            {
                string theXpath = "//input[contains(@id, '_StandardProfileTemplate1_nvDataGroupDealBreakerInfo_repeaterSectionTwoColumn__ctl0_nameValueDataSection__ctl7_txtEdit')]";

                WaitUntilElementValueCanBeCleared(By.XPath(theXpath));
                SendKeys(WaitUntilElementExists(By.XPath(theXpath)), value);
            }
        }

        public string HaveKids_InDealbreakersOverlay
        {
            get
            {
                string theXpath = null;

                if (driver.Url.Contains("jdate.com") || driver.Url.Contains("spark.com") || driver.Url.Contains("jdate.co.il"))
                    theXpath = "//select[contains(@id, '_StandardProfileTemplate1_nvDataGroupDealBreakerInfo_repeaterSectionTwoColumn__ctl0_nameValueDataSection__ctl0_ddlEdit')]";
                else
                    theXpath = "//select[contains(@id, '_StandardProfileTemplate1_nvDataGroupDealBreakerInfo_repeaterSectionTwoColumn__ctl0_nameValueDataSection__ctl8_ddlEdit')]";

                return ReturnTextOfSelectedValueInList(By.XPath(theXpath));
            }
            set
            {
                SelectElement select = null;

                if (driver.Url.Contains("jdate.com") || driver.Url.Contains("spark.com") || driver.Url.Contains("jdate.co.il"))
                    select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, '_StandardProfileTemplate1_nvDataGroupDealBreakerInfo_repeaterSectionTwoColumn__ctl0_nameValueDataSection__ctl0_ddlEdit')]")));
                else
                    select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, '_StandardProfileTemplate1_nvDataGroupDealBreakerInfo_repeaterSectionTwoColumn__ctl0_nameValueDataSection__ctl8_ddlEdit')]")));

                select.SelectByText(value);
            }
        }

        public string WantsKids_InDealbreakersOverlay
        {
            get
            {
                string theXpath = null;

                if (driver.Url.Contains("jdate.com") || driver.Url.Contains("spark.com") || driver.Url.Contains("jdate.co.il"))
                    theXpath = "//select[contains(@id, '_StandardProfileTemplate1_nvDataGroupDealBreakerInfo_repeaterSectionTwoColumn__ctl0_nameValueDataSection__ctl1_ddlEdit')]";
                else
                    theXpath = "//select[contains(@id, '_StandardProfileTemplate1_nvDataGroupDealBreakerInfo_repeaterSectionTwoColumn__ctl0_nameValueDataSection__ctl9_ddlEdit')]";

                return ReturnTextOfSelectedValueInList(By.XPath(theXpath));
            }
            set
            {
                SelectElement select = null;

                if (driver.Url.Contains("jdate.com") || driver.Url.Contains("spark.com") || driver.Url.Contains("jdate.co.il"))
                    select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, '_StandardProfileTemplate1_nvDataGroupDealBreakerInfo_repeaterSectionTwoColumn__ctl0_nameValueDataSection__ctl1_ddlEdit')]")));
                else
                    select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, '_StandardProfileTemplate1_nvDataGroupDealBreakerInfo_repeaterSectionTwoColumn__ctl0_nameValueDataSection__ctl9_ddlEdit')]")));

                select.SelectByText(value);
            }
        }

        public string Height_Dealbreakers
        {
            get
            {
                IWebElement element = null;

                if (driver.Url.Contains("jdate.com") || driver.Url.Contains("spark.com") || driver.Url.Contains("jdate.co.il"))
                    element = driver.FindElement(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_nvDataGroupDealBreakerInfo_repeaterSectionTwoColumn__ctl0_nameValueDataSection_viewSectionContainer')]/dl[@class='dl-form form-inside clearfix'][1]/dd[3]"));
                else
                    element = driver.FindElement(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_nvDataGroupDealBreakerInfo_repeaterSectionTwoColumn__ctl0_nameValueDataSection_viewSectionContainer')]/dl[@class='dl-form form-inside clearfix'][1]/dd[1]"));

                return element.Text;
            }
        }

        public string BodyType_Dealbreakers
        {
            get
            {
                IWebElement element = driver.FindElement(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_nvDataGroupDealBreakerInfo_repeaterSectionTwoColumn__ctl0_nameValueDataSection_viewSectionContainer')]/dl[@class='dl-form form-inside clearfix'][1]/dd[2]"));
                return element.Text;
            }
        }

        public string Ethnicity_Dealbreakers
        {
            get
            {
                IWebElement element = null;

                if (driver.Url.Contains("jdate.com") || driver.Url.Contains("spark.com") || driver.Url.Contains("jdate.co.il"))
                    element = driver.FindElement(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_nvDataGroupDealBreakerInfo_repeaterSectionTwoColumn__ctl0_nameValueDataSection_viewSectionContainer')]/dl[@class='dl-form form-inside clearfix'][1]/dd[4]"));
                else
                    element = driver.FindElement(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_nvDataGroupDealBreakerInfo_repeaterSectionTwoColumn__ctl0_nameValueDataSection_viewSectionContainer')]/dl[@class='dl-form form-inside clearfix'][1]/dd[3]"));

                return element.Text;
            }
        }

        public string Smoking_Dealbreakers
        {
            get
            {
                IWebElement element = null;

                if (driver.Url.Contains("jdate.com") || driver.Url.Contains("spark.com") || driver.Url.Contains("jdate.co.il"))
                    element = driver.FindElement(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_nvDataGroupDealBreakerInfo_repeaterSectionTwoColumn__ctl0_nameValueDataSection_viewSectionContainer')]/dl[@class='dl-form form-inside clearfix'][2]/dd[1]"));
                else
                    element = driver.FindElement(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_nvDataGroupDealBreakerInfo_repeaterSectionTwoColumn__ctl0_nameValueDataSection_viewSectionContainer')]/dl[@class='dl-form form-inside clearfix'][1]/dd[4]"));

                return element.Text;
            }
        }

        public string Drinking_Dealbreakers
        {
            get
            {
                IWebElement element = null;

                if (driver.Url.Contains("jdate.com") || driver.Url.Contains("spark.com") || driver.Url.Contains("jdate.co.il"))
                    element = driver.FindElement(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_nvDataGroupDealBreakerInfo_repeaterSectionTwoColumn__ctl0_nameValueDataSection_viewSectionContainer')]/dl[@class='dl-form form-inside clearfix'][2]/dd[2]"));
                else
                    element = driver.FindElement(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_nvDataGroupDealBreakerInfo_repeaterSectionTwoColumn__ctl0_nameValueDataSection_viewSectionContainer')]/dl[@class='dl-form form-inside clearfix'][1]/dd[5]"));

                return element.Text;
            }
        }

        public string Religion_Dealbreakers
        {
            get
            {
                IWebElement element = null;

                if (driver.Url.Contains("jdate.com"))
                    element = driver.FindElement(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_nvDataGroupDealBreakerInfo_repeaterSectionTwoColumn__ctl0_nameValueDataSection_viewSectionContainer')]/dl[@class='dl-form form-inside clearfix'][1]/dd[4]"));
                else if (driver.Url.Contains("spark.com") || driver.Url.Contains("jdate.co.il"))
                    element = driver.FindElement(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_nvDataGroupDealBreakerInfo_repeaterSectionTwoColumn__ctl0_nameValueDataSection_viewSectionContainer')]/dl[@class='dl-form form-inside clearfix'][1]/dd[5]"));
                else
                    element = driver.FindElement(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_nvDataGroupDealBreakerInfo_repeaterSectionTwoColumn__ctl0_nameValueDataSection_viewSectionContainer')]/dl[@class='dl-form form-inside clearfix'][2]/dd[1]"));

                return element.Text;
            }
        }

        public string Education_Dealbreakers
        {
            get
            {
                IWebElement element = null;

                if (driver.Url.Contains("spark.com") || driver.Url.Contains("jdate.co.il"))
                    element = driver.FindElement(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_nvDataGroupDealBreakerInfo_repeaterSectionTwoColumn__ctl0_nameValueDataSection_viewSectionContainer')]/dl[@class='dl-form form-inside clearfix'][2]/dd[3]"));
                else
                    element = driver.FindElement(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_nvDataGroupDealBreakerInfo_repeaterSectionTwoColumn__ctl0_nameValueDataSection_viewSectionContainer')]/dl[@class='dl-form form-inside clearfix'][2]/dd[2]"));

                return element.Text;
            }
        }

        public string Occupation_Dealbreakers
        {
            get
            {
                IWebElement element = null;

                if (driver.Url.Contains("jdate.com"))
                    element = driver.FindElement(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_nvDataGroupDealBreakerInfo_repeaterSectionTwoColumn__ctl0_nameValueDataSection_viewSectionContainer')]/dl[@class='dl-form form-inside clearfix'][2]/dd[3]"));
                else if (driver.Url.Contains("spark.com") || driver.Url.Contains("jdate.co.il"))
                    element = driver.FindElement(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_nvDataGroupDealBreakerInfo_repeaterSectionTwoColumn__ctl0_nameValueDataSection_viewSectionContainer')]/dl[@class='dl-form form-inside clearfix'][2]/dd[4]"));
                else
                    element = driver.FindElement(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_nvDataGroupDealBreakerInfo_repeaterSectionTwoColumn__ctl0_nameValueDataSection_viewSectionContainer')]/dl[@class='dl-form form-inside clearfix'][2]/dd[2]"));

                return element.Text;
            }
        }

        public string AnnualIncome_Dealbreakers
        {
            get
            {
                IWebElement element = null;

                if (driver.Url.Contains("jdate.com") || driver.Url.Contains("spark.com") || driver.Url.Contains("jdate.co.il"))
                    element = driver.FindElement(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_nvDataGroupDealBreakerInfo_repeaterSectionTwoColumn__ctl0_nameValueDataSection_viewSectionContainer')]/dl[@class='dl-form form-inside clearfix'][2]/dd[5]"));
                else
                    element = driver.FindElement(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_nvDataGroupDealBreakerInfo_repeaterSectionTwoColumn__ctl0_nameValueDataSection_viewSectionContainer')]/dl[@class='dl-form form-inside clearfix'][2]/dd[2]"));

                return element.Text;
            }
        }

        public string Synagogue_Dealbreakers
        {
            get
            {
                IWebElement element = null;

                element = driver.FindElement(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_nvDataGroupDealBreakerInfo_repeaterSectionTwoColumn__ctl0_nameValueDataSection_viewSectionContainer')]/dl[@class='dl-form form-inside clearfix'][2]/dd[4]"));

                return element.Text;
            }
        }

        public string WhatIDo_Dealbreakers
        {
            get
            {
                IWebElement element = driver.FindElement(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_nvDataGroupDealBreakerInfo_repeaterSectionTwoColumn__ctl0_nameValueDataSection_viewSectionContainer')]/dl[@class='dl-form form-inside clearfix'][2]/dd[3]"));
                return element.Text;
            }
        }

        public string HaveKids_Dealbreakers
        {
            get
            {
                IWebElement element = null;

                if (driver.Url.Contains("jdate.com") || driver.Url.Contains("spark.com") || driver.Url.Contains("jdate.co.il"))
                    element = driver.FindElement(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_nvDataGroupDealBreakerInfo_repeaterSectionTwoColumn__ctl0_nameValueDataSection_viewSectionContainer')]/dl[@class='dl-form form-inside clearfix'][1]/dd[1]"));
                else
                    element = driver.FindElement(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_nvDataGroupDealBreakerInfo_repeaterSectionTwoColumn__ctl0_nameValueDataSection_viewSectionContainer')]/dl[@class='dl-form form-inside clearfix'][2]/dd[4]"));

                return element.Text;
            }
        }

        public string WantsKids_Dealbreakers
        {
            get
            {
                IWebElement element = null;

                if (driver.Url.Contains("jdate.com") || driver.Url.Contains("spark.com") || driver.Url.Contains("jdate.co.il"))
                    element = driver.FindElement(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_nvDataGroupDealBreakerInfo_repeaterSectionTwoColumn__ctl0_nameValueDataSection_viewSectionContainer')]/dl[@class='dl-form form-inside clearfix'][1]/dd[2]"));
                else
                    element = driver.FindElement(By.XPath("//div[contains(@id, '_StandardProfileTemplate1_nvDataGroupDealBreakerInfo_repeaterSectionTwoColumn__ctl0_nameValueDataSection_viewSectionContainer')]/dl[@class='dl-form form-inside clearfix'][2]/dd[5]"));

                return element.Text;
            }
        }
        #endregion


        /// <summary>
        /// This is only for clicking on the JMeter icon for users who HAVE ALREADY answered the JMeter questionaire
        /// </summary>
        public JMeterOneOnONe JMeterIcon_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//img[contains(@id, 'ucMatchMeterInfoDisplay_imgJmeter')]"));
            ClickUntilURLChanges(element);

            return new JMeterOneOnONe(driver);
        }

        /// <summary>
        /// This is only for clicking on the JMeter icon for users who have NOT YET answered the JMeter questionaire
        /// </summary>
        public void JMeterIcon_Click_AndSendJMeterInvitation()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//img[contains(@id, 'ucMatchMeterInfoDisplay_imgJmeter')]"));
            element.Click();

            List<string> handlers = new List<string>();
            foreach (string handler in driver.WindowHandles)
                handlers.Add(handler);

            driver.SwitchTo().Window(handlers[1]);

            // send invitation
            IWebElement invitation = WaitUntilElementExists(By.XPath("//img[contains(@id, '_imgSendInvitation')]"));
            invitation.Click();

            driver.Close();
            driver.SwitchTo().Window(handlers[0]);

        }

        public void IM_Click()
        {
            if (driver.FindElements(By.XPath("//a[contains(@id, '_m2mCommunication1_lnkIMOnline')]")).Count == 0)
                Assert.Fail("The random profile we selected might have their IM button greyed out which means we cannot check that the IM window appears correct for this profile. Run the test again to select another user.");

            IWebElement element = WaitUntilElementExists(By.XPath("//a[contains(@id, '_m2mCommunication1_lnkIMOnline')]"));
            element.Click();

            // wait for IM window
            Thread.Sleep(1000);
        }

        /// <summary>
        /// Since the IM window is flash we cannot verify anything inside the window, we just verify that the window's URL is correct
        /// </summary>
        /// <returns></returns>
        public bool IMWindowExists()
        {
            List<string> handlers = new List<string>();
            foreach (string handler in driver.WindowHandles)
                handlers.Add(handler);

            driver.SwitchTo().Window(handlers[1]);

            if (driver.Url.Contains("/webchat/"))
                return true;

            return false;

        }

        public void IMWindowClose()
        {
            List<string> handlers = new List<string>();
            foreach (string handler in driver.WindowHandles)
                handlers.Add(handler);

            driver.SwitchTo().Window(handlers[1]);

            driver.Close();
            driver.SwitchTo().Window(handlers[0]);

        }

        public void Flirt_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//a[contains(@id, '_m2mCommunication1_lnkFlirt')]"));
            element.Click();

            // wait for IM window
            Thread.Sleep(1000);
        }

        public bool FlirtOverlayExists()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//span[@id='ui-dialog-title-flirtContent']"));

            if (element.Text == "Send a Flirt")
                return true;

            return false;
        }

        public Framework.Site_Pages.ECards ECard_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//a[contains(@id, '_m2mCommunication1_lnkEcard')]"));
            element.Click();

            return new Framework.Site_Pages.ECards(driver);
        }

        public void SecretAdmirerYes_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//span[contains(@id, '_m2mCommunication1_spanYes')]"));
            element.Click();
        }

        public void SecretAdmirerNo_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//span[contains(@id, '_m2mCommunication1_spanNo')]"));
            element.Click();
        }

        public void SecretAdmirerMaybe_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//span[contains(@id, '_m2mCommunication1_spanMaybe')]"));
            element.Click();
        }

        public bool IsUserCurrentlyAFavorite()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//span[contains(@id, 'spanProfileAddedIcon')]"));

            if (element.Displayed == true)
                return true;

            return false;

        }

        public void Favorite_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//a[contains(@id, 'ajaxFavorites')]"));
            element.Click();

            // wait for page to reload
            Thread.Sleep(2000);
        }

        public void UnFavorite_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//span[contains(@id, 'spanProfileAddedIcon')]"));
            element.Click();

            // wait for page to reload
            Thread.Sleep(2000);
        }
    }
}
