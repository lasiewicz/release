﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.Events;
using NUnit.Framework;
using log4net;

// ex. PAGE URL:  http://connect.jdate.com/cards/send.html?catid=44&subcatid=116&cardid=2578&message=&address=bashreg2&MemberID=118876656&send=Personalize+and+Send

namespace WebdriverAutomation.Framework.Site_Pages
{
    public class EditECard : BasePage
    {
        public EditECard(IWebDriver driver)
            : base(driver)
        {
            Header = new WebdriverAutomation.Framework.Site_Parts.Header(driver);
            Footer = new WebdriverAutomation.Framework.Site_Parts.Footer(driver);

            // verify we are on the correct page       
            IWebElement textOnPage = null;

            textOnPage = WaitUntilPageVerificationObjectExists(By.XPath("//html/body/div/div[2]/div[2]/div/h1"), "EDIT ECARD PAGE");

            if (textOnPage.Text != (textThatVerifiesThisPageIsCorrect))
                log.Error("ERROR - EDIT ECARD PAGE - We should be on this page but the page verification failed.");

            Assert.True(textOnPage.Text == textThatVerifiesThisPageIsCorrect, "ERROR - EDIT ECARD PAGE - We should be on this page but the page verification failed.");

        }

        private static readonly ILog log = LogManager.GetLogger(typeof(AboutJDate));
        public WebdriverAutomation.Framework.Site_Parts.Header Header;
        public WebdriverAutomation.Framework.Site_Parts.Footer Footer;

        private const string textThatVerifiesThisPageIsCorrect = "Edit E-card";

        public void ToTextbox_Write(string value)
        {
            string theXpath = "//input[@id='address']";

            WaitUntilElementValueCanBeCleared(By.XPath(theXpath));
            SendKeys(WaitUntilElementExists(By.XPath(theXpath)), value);
        }

        public Framework.Site_Pages.ECards SendECard_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//div[@class='btn_type_1']"));
            element.Click();

            return new Framework.Site_Pages.ECards(driver);
        }
    }
}
