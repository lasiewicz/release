﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.Events;
using NUnit.Framework;
using log4net;

// PAGE URL:  http://www.jdate.com/Applications/MemberProfile/AttributeEdit.aspx?Attribute=SITEFIRSTNAME
//     during registration, after entering in captcha this page is reached, asking the user to enter their first name in instead of the registration complete page

namespace WebdriverAutomation.Framework.Site_Pages
{
    public class PleaseUpdateYourProfile : BasePage
    {
        public PleaseUpdateYourProfile(IWebDriver driver)
            : base(driver)
        {
            Footer = new WebdriverAutomation.Framework.Site_Parts.Footer(driver);

            Tests.ReusableCode.Ads ads = new Tests.ReusableCode.Ads();
            ads.KillAllAds(driver, log);

            // verify we are on the correct page       
            IWebElement textOnPage = null;

            textOnPage = WaitUntilPageVerificationObjectExists(By.XPath("//div[@id='page-container']/h1"), "PLEASE UPDATE YOUR PROFILE PAGE");

            if (driver.Url.Contains("jdate.co.il"))
                textThatVerifiesThisPageIsCorrect = "עדכון פרופיל!";
            else
                textThatVerifiesThisPageIsCorrect = "Please update your profile!";

            if (textOnPage.Text != (textThatVerifiesThisPageIsCorrect))
                log.Error("ERROR - PLEASE UPDATE YOUR PROFILE PAGE - We should be on this page but the page verification failed.");

            Assert.True(textOnPage.Text == textThatVerifiesThisPageIsCorrect, "ERROR - PLEASE UPDATE YOUR PROFILE PAGE - We should be on this page but the page verification failed.");

        }

        private static readonly ILog log = LogManager.GetLogger(typeof(PleaseUpdateYourProfile));
        public WebdriverAutomation.Framework.Site_Parts.Footer Footer;

        private string textThatVerifiesThisPageIsCorrect = string.Empty;

        public void EnterYourFirstNameTextbox_Write(string value)
        {
            string theXpath = "//input[contains(@id, 'SiteFirstName')]";

            WaitUntilElementValueCanBeCleared(By.XPath(theXpath));
            SendKeys(WaitUntilElementExists(By.XPath(theXpath)), value);
        }

        public WebdriverAutomation.Framework.Site_Pages.Home SaveButton_Click()
        {
            WaitUntilElementIsClickable(By.XPath("//input[contains(@id, 'btnSave')]"));

            return new WebdriverAutomation.Framework.Site_Pages.Home(driver);
        }

        public void SaveButton_Click_StayOnSamePage()
        {
            WaitUntilElementIsClickable(By.XPath("//input[contains(@id, 'btnSave')]"));
        }

        public WebdriverAutomation.Framework.Site_Pages.AnImportantNotice UpdateLaterLink_Click()
        {
            WaitUntilElementIsClickable(By.XPath("//input[contains(@id, '_btnAboutMeUpdateLater')]"));

            return new WebdriverAutomation.Framework.Site_Pages.AnImportantNotice(driver);
        }

        public bool UpdateLater_UploadAPhoto_Exists()
        {
            if (driver.FindElements(By.XPath("//input[contains(@id, 'btnPhotoUpdateLater')]")).Count > 0)
                return true;

            return false;
        }

        public WebdriverAutomation.Framework.Site_Pages.AnImportantNotice UpdateLaterLink_UploadAPhoto_Click()
        {
            WaitUntilElementIsClickable(By.XPath("//input[contains(@id, 'btnPhotoUpdateLater')]"));

            return new WebdriverAutomation.Framework.Site_Pages.AnImportantNotice(driver);
        }

        public bool UpdateLater_AboutMe_Exists()
        {
            if (driver.FindElements(By.XPath("//input[contains(@id, 'btnAboutMeUpdateLater')]")).Count > 0)
                return true;

            return false;
        }

        public WebdriverAutomation.Framework.Site_Pages.AnImportantNotice UpdateLaterLink_AboutMe_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//input[contains(@id, 'btnAboutMeUpdateLater')]"));
            ClickUntilURLChanges(element);

            return new WebdriverAutomation.Framework.Site_Pages.AnImportantNotice(driver);
        }
    }
}
