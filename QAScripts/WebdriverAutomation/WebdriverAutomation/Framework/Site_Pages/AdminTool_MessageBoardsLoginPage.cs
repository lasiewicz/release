﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.Events;
using NUnit.Framework;
using log4net;

// PAGE URL:  http://connect.jdate.com/admin_login.html
//     note: when the link for this is clicked a new window appears
//     we only deal with the JDate site

namespace WebdriverAutomation.Framework.Pages
{
    public class AdminTool_MessageBoardsLoginPage : BasePage
    {
        public AdminTool_MessageBoardsLoginPage(IWebDriver driver)
            : base(driver)
        {
            // verify we are on the correct page      
            List<string> handlers = new List<string>();
            foreach (string handler in driver.GetWindowHandles())
                handlers.Add(handler);

            string parentWindow = driver.GetWindowHandle();

            // when we click on this link from the footer a new window appears. currently having problems switching pages in IE.  for now if we fail just continue
            try
            {
                driver.SwitchTo().Window(handlers[1]);

                // verify we are on the correct page       
                IWebElement textOnPage = null;

                textOnPage = WaitUntilPageVerificationObjectExists(By.XPath("//form[@id='loginform']/fieldset/p"), "ADMIN TOOL - MESSAGE BOARDS LOGIN PAGE");

                if (textOnPage.Text != (textThatVerifiesThisPageIsCorrect))
                    log.Error("ERROR - ADMIN TOOL - MESSAGE BOARDS LOGIN PAGE - We should be on this page but the page verification failed.");

                Assert.True(textOnPage.Text == textThatVerifiesThisPageIsCorrect, "ERROR - ADMIN TOOL - MESSAGE BOARDS LOGIN PAGE - We should be on this page but the page verification failed.");

                driver.Close();
                driver.SwitchTo().Window(handlers[0]);
            }
            catch
            {
            }

        }

        private static readonly ILog log = LogManager.GetLogger(typeof(AdvertiseWithUs));

        private const string textThatVerifiesThisPageIsCorrect = "Please enter your username and password";
    }
}
