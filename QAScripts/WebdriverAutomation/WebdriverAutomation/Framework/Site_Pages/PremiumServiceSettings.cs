﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.Events;
using NUnit.Framework;
using log4net;

// PAGE URL:  http://www.jdate.com/Applications/MemberServices/PremiumServiceSettings.aspx  (with subscription and premium sevices bought)

namespace WebdriverAutomation.Framework.Site_Pages
{
    public class PremiumServiceSettings : BasePage
    {
        public PremiumServiceSettings(IWebDriver driver)
            : base(driver)
        {
            Header = new WebdriverAutomation.Framework.Site_Parts.Header(driver);
            Footer = new WebdriverAutomation.Framework.Site_Parts.Footer(driver);

            // verify we are on the correct page       
            IWebElement textOnPage = null;

            textOnPage = WaitUntilPageVerificationObjectExists(By.XPath("//div[@id='content-main']/h1"), "PREMIUM SERVICE SETTINGS PAGE");

            if (textOnPage.Text != (textThatVerifiesThisPageIsCorrect))
                log.Error("ERROR - PREMIUM SERVICE SETTINGS PAGE - We should be on this page but the page verification failed.");

            Assert.True(textOnPage.Text == textThatVerifiesThisPageIsCorrect, "ERROR - PREMIUM SERVICE SETTINGS PAGE - We should be on this page but the page verification failed.");

        }

        private static readonly ILog log = LogManager.GetLogger(typeof(PremiumServiceSettings));
        public WebdriverAutomation.Framework.Site_Parts.Header Header; 
        public WebdriverAutomation.Framework.Site_Parts.Footer Footer;

        private const string textThatVerifiesThisPageIsCorrect = "Premium Service Settings";

    }
}
