﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.Events;
using NUnit.Framework;
using log4net;

// PAGE URL:  http://brb.jdate.com/JD/error.htm
//   this is the error page that appears when the site is down.  might throw this into the Basepage later to test for errors if the site goes down again.

namespace WebdriverAutomation.Framework.Site_Pages
{
    public class ErrorPage : BasePage
    {
        public ErrorPage(IWebDriver driver)
            : base(driver)
        {
            Footer = new WebdriverAutomation.Framework.Site_Parts.Footer(driver);

            // verify we are on the correct page       
            IWebElement textOnPage = null;

            textOnPage = WaitUntilPageVerificationObjectExists(By.XPath("//div[@id='content_container']/div/div[2]/h1"), "ERROR PAGE");

            if (textOnPage.Text != (textThatVerifiesThisPageIsCorrect))
                log.Error("ERROR - ERROR PAGE - We should be on this page but the page verification failed.");

            Assert.True(textOnPage.Text == textThatVerifiesThisPageIsCorrect, "ERROR - ERROR PAGE - We should be on this page but the page verification failed.");

        }

        private static readonly ILog log = LogManager.GetLogger(typeof(OurMission)); 
        public WebdriverAutomation.Framework.Site_Parts.Footer Footer;

        private const string textThatVerifiesThisPageIsCorrect = "Sorry, the site is momentarily down";

        public void ReturnToHomepageButton_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//div[@id='content_container']/div/div[2]/p[2]/a/img"));
            element.Click();
        }

    }
}
