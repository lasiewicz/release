﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.Events;
using NUnit.Framework;
using log4net;

// PAGE URL:  http://www.jdate.com/Applications/Email/MessageSettings.aspx

namespace WebdriverAutomation.Framework.Site_Pages
{
    public class MessageSettings : BasePage
    {
        public MessageSettings(IWebDriver driver)
            : base(driver)
        {
            Header = new WebdriverAutomation.Framework.Site_Parts.Header(driver); 
            Footer = new WebdriverAutomation.Framework.Site_Parts.Footer(driver);

            // verify we are on the correct page       
            IWebElement textOnPage = null;

            if (driver.Url.Contains("jdate.co.il"))
                textThatVerifiesThisPageIsCorrect = "הגדרות דואר";
            else
                textThatVerifiesThisPageIsCorrect = "Message Settings";

            textOnPage = WaitUntilPageVerificationObjectExists(By.XPath("//div[@id='page-container']/h1"), "MESSAGE SETTINGS PAGE");

            if (!textOnPage.Text.Contains(textThatVerifiesThisPageIsCorrect))
                log.Error("ERROR - MESSAGE SETTINGS PAGE - We should be on this page but the page verification failed.");

            Assert.True(textOnPage.Text.Contains(textThatVerifiesThisPageIsCorrect), "ERROR - MESSAGE SETTINGS PAGE - We should be on this page but the page verification failed.");

        }

        private static readonly ILog log = LogManager.GetLogger(typeof(MessageSettings));
        public WebdriverAutomation.Framework.Site_Parts.Header Header; 
        public WebdriverAutomation.Framework.Site_Parts.Footer Footer;

        private string textThatVerifiesThisPageIsCorrect = string.Empty;

    }
}
