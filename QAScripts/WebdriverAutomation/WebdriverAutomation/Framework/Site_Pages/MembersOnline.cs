﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.Events;
using NUnit.Framework;
using log4net;

// PAGE URL:  http://www.jdate.com/Applications/MembersOnline/MembersOnline.aspx

namespace WebdriverAutomation.Framework.Site_Pages
{
    public class MembersOnline : BasePage
    {
        public MembersOnline(IWebDriver driver)
            : base(driver)
        {
            Header = new WebdriverAutomation.Framework.Site_Parts.Header(driver);
            Footer = new WebdriverAutomation.Framework.Site_Parts.Footer(driver);

            Tests.ReusableCode.Ads ads = new Tests.ReusableCode.Ads();
            ads.KillAllAds(driver, log);

            // verify we are on the correct page       
            IWebElement textOnPage = null;

            textOnPage = WaitUntilPageVerificationObjectExists(By.XPath("//div[contains(@class, 'header-options clearfix')]/h1"), "MEMBERS ONLINE PAGE");

            if (textOnPage.Text != (textThatVerifiesThisPageIsCorrect))
                log.Error("ERROR - MEMBERS ONLINE PAGE - We should be on this page but the page verification failed.");

            Assert.True(textOnPage.Text == textThatVerifiesThisPageIsCorrect, "ERROR - MEMBERS ONLINE PAGE - We should be on this page but the page verification failed.");

        }

        private static readonly ILog log = LogManager.GetLogger(typeof(AboutJDate));
        public WebdriverAutomation.Framework.Site_Parts.Header Header;
        public WebdriverAutomation.Framework.Site_Parts.Footer Footer;

        private const string textThatVerifiesThisPageIsCorrect = "Members Online";

        public Framework.Site_Pages.YourProfile ClickONFirstOnlineMemberInList()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//a[contains(@id, '__ctl0_GalleryMiniProfile_lnkUserName')]"));
            ClickUntilURLChanges(element);

            return new Framework.Site_Pages.YourProfile(driver);
        }

        public Framework.Site_Pages.YourProfile_Visitor ClickONFirstOnlineMemberInList_AsVisitor()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//a[contains(@id, '__ctl0_GalleryMiniProfile_lnkUserName')]"));
            ClickUntilURLChanges(element);

            return new Framework.Site_Pages.YourProfile_Visitor(driver);
        }
    }
}
