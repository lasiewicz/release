﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.Events;
using NUnit.Framework;
using log4net;
using System.Threading;

// PAGE URL:  http://www.jdate.com/Applications/Registration/Registration.aspx (not logged in)

namespace WebdriverAutomation.Framework.Site_Pages
{
    public class Registration : BasePage
    {
        public Registration(IWebDriver driver)
            : base(driver)
        {
            IWebElement registrationPageText = null;

            // this page does page verification different than all the others so we'll sleep first to avoid potential race conditions
            Thread.Sleep(1000);

            // verify we are on the correct page - Registration Paths A, D, E?
            if (driver.FindElements(By.XPath("//div[@id='min-max-container']/div[@id='content-container']/div[@id='content-main']/div[1]/h1")).Count > 0)
                registrationPageText = WaitUntilElementExists(By.XPath("//div[@id='min-max-container']/div[@id='content-container']/div[@id='content-main']/div[1]/h1"));
            // new registration separated from Bedrock as of 8/8/12
            else if (driver.FindElements(By.XPath("//html/body/div/div/h1")).Count > 0)
                registrationPageText = WaitUntilElementExists(By.XPath("//html/body/div/div/h1"));
            else if (driver.Url.Contains("jdate.co.il/Applications/MemberProfile/RegistrationWelcomeOnePage"))
                registrationPageText = WaitUntilElementExists(By.XPath("//div[@id='title']"));
            else if (driver.Url.Contains("jdate.co.il/Registration/?Scenario=Scen2"))
                registrationPageText = WaitUntilElementExists(By.XPath("//h1[@id='regFlowTitle']"));
            // Registration Paths B, C take a few seconds for the overlay to show up.  Wait 10 seconds for it to show up and if it doesn't, fail out.
            else
            {
                for (int i = 0; i < 5; i++)
                {
                    try
                    {
                        registrationPageText = WaitUntilElementExists(By.XPath("//div[@id='overlaybox-header-content']"));

                        if (registrationPageText.Text != string.Empty)
                            break;
                    }
                    catch { };

                    Thread.Sleep(100);
                }
            }

            if (driver.Url.Contains("blacksingles.com"))
                textThatVerifiesThisPageIsCorrect = "Create Your Profile — Basics";
            else if (driver.Url.Contains("jdate.co.il/Applications/MemberProfile/RegistrationWelcomeOnePage"))
                textThatVerifiesThisPageIsCorrect = "הרשמתך נקלטה. תודה.";
            else if (driver.Url.Contains("jdate.co.il/Applications/Registration/Registration"))
                textThatVerifiesThisPageIsCorrect = "הרשמה - פרטים אישיים";
            else if (driver.Url.Contains("jdate.co.il/Registration/?Scenario=Scen2"))
                textThatVerifiesThisPageIsCorrect = "הרשמה - פרטים אישיים";
            else
                textThatVerifiesThisPageIsCorrect = "Create Your Profile";

            if (!registrationPageText.Text.Contains(textThatVerifiesThisPageIsCorrect))
                log.Error("ERROR - REGISTRATION PAGE - We should be on this page but the page verification failed.");

            Assert.True(registrationPageText.Text.Contains(textThatVerifiesThisPageIsCorrect), "ERROR - REGISTRATION PAGE - We should be on this page but the page verification failed.");

        }

        private static readonly ILog log = LogManager.GetLogger(typeof(Splash));

        private string textThatVerifiesThisPageIsCorrect = string.Empty;

        IWebElement continueButton;

        public WebdriverAutomation.Framework.Site_Pages.Login MemberLoginButton_Click()
        {
            IWebElement element = driver.FindElement(By.XPath("//div[contains(@id, '_ctl0_headerLogin')]/a"));
            ClickUntilURLChanges(element);

            return new WebdriverAutomation.Framework.Site_Pages.Login(driver);
        }

        #region Registration Paths A, D that do not have any fields to enter personal info on the Splash page (old registration paths)
        /// <summary>
        /// The continue button is shared among all pages within Registration.  Since objects are EXTREMELY finnicky before and after Continue is pressed, wait before
        /// and after using it so that objects don't erroneously error out as being stale when you use them.
        /// </summary>
        public void ContinueButton_Click()
        {
            Wait(1000);

            WaitUntilElementIsClickable(By.XPath("//input[contains(@id, 'btnContinue')]"));

            Wait(1000);
        }

        public void ContinueButton_NewRegNotOnBedrock_Click()
        {
            Wait(1000);

            WaitUntilElementIsClickable(By.XPath("//button[@id='btnContinue']"));

            Wait(2000);
        }

        /// <summary>
        /// Another method to help with the phantom clicks.  This method clicks the Continue button until the Progress Bar changes values.
        /// </summary>
        public void ContinueButton_ClickUntilProgressBarChanges()
        {
            Wait(1000);

            string progressBarXPath = "//div[@id='content-main']/div[2]/div/span";

            IWebElement element = WaitUntilElementExists(By.XPath("//input[contains(@id, 'btnContinue')]"));
            ClickUntilObjectTextChanges(element, By.XPath(progressBarXPath));

            Wait(1000);
        }

        /// <summary>
        /// Another method to help with the phantom clicks.  This method clicks the Continue button until the Progress Bar changes values.
        /// </summary>
        public void ContinueButton_NewRegNotOnBedrock_ClickUntilProgressBarChanges()
        {
            Wait(1000);

            string progressBarXPath = string.Empty;

            if (driver.Url.Contains("jdate.com") || driver.Url.Contains("spark.com") || driver.Url.Contains("bbwpersonalsplus.com") || driver.Url.Contains("blacksingles.com") || driver.Url.Contains("jdate.co.il"))
                progressBarXPath = "//span[@id='progress-position']";
            else
                progressBarXPath = "//em[@id='progress-position']";

            IWebElement element = WaitUntilElementExists(By.XPath("//button[@id='btnContinue']"));
            ClickUntilObjectTextChanges(element, By.XPath(progressBarXPath));

            Wait(1000);
        }

        /// <summary>
        /// The Continue Button on this page is testy.  Since there seems to be a timing issue with the page trying to verify your zip code, sometimes clicking on
        /// the Continue button does not work.  So click until we get to the next page, trying 5 times over and over.
        /// </summary>
        public void ContinueButton_OnZipCodePage_Click()
        {
            Wait(1000);

            int countLimit = 5;

            for (int count = 0; count < countLimit; count++)
            {
                continueButton = driver.FindElement(By.XPath("//input[contains(@id, 'btnContinue')]"));
                continueButton.Click();

                Wait(1000);

                try
                {
                    // most registration pages
                    if (driver.FindElements(By.XPath("//div[contains(@id, 'idRelationshipmask_divControl')]/h2")).Count > 0)
                    {
                        IWebElement label = driver.FindElement(By.XPath("//div[contains(@id, 'idRelationshipmask_divControl')]/h2"));

                        if (label.Text.Contains("What type of relationship are you looking for"))
                            break;
                    }
                    // BBW
                    else
                    {
                        IWebElement label = driver.FindElement(By.XPath("//div[contains(@id, 'idFirstName_divControl')]/label/sub"));

                        if (label.Text.Contains("Your name will not be publicly displayed."))
                            break;
                    }
                }
                catch { }
            }

            Wait(1000);
        }

        public void ZipCodeTextbox_Write(string text)
        {
            WaitUntilElementIsWritable(By.XPath("//input[contains(@id, 'idRegion_postalCode')]"), text);
        }

        public void ZipCodeTextbox_NewRegNotOnBedrock_Write(string text)
        {
            WaitUntilElementIsWritable(By.XPath("//input[@id='regionZipId']"), text);
        }

        public void WhatTypeOfRelationshipAreYouLookingFor_Click(int index)
        {
            switch (index)
            {
                case 1:
                    WaitUntilElementIsCheckable(By.XPath("//input[contains(@id, 'idRelationshipmask_AttributeMaskControl_rptOptions__ctl1_cbOption')]"));
                    break;
                case 2:
                    WaitUntilElementIsCheckable(By.XPath("//input[contains(@id, 'idRelationshipmask_AttributeMaskControl_rptOptions__ctl2_cbOption')]"));
                    break;
                case 3:
                    WaitUntilElementIsCheckable(By.XPath("//input[contains(@id, 'idRelationshipmask_AttributeMaskControl_rptOptions__ctl3_cbOption')]"));
                    break;
                case 4:
                    WaitUntilElementIsCheckable(By.XPath("//input[contains(@id, 'idRelationshipmask_AttributeMaskControl_rptOptions__ctl4_cbOption')]"));
                    break;
                case 5:
                    WaitUntilElementIsCheckable(By.XPath("//input[contains(@id, 'idRelationshipmask_AttributeMaskControl_rptOptions__ctl5_cbOption')]"));
                    break;
                case 6:
                    WaitUntilElementIsCheckable(By.XPath("//input[contains(@id, 'idRelationshipmask_AttributeMaskControl_rptOptions__ctl6_cbOption')]"));
                    break;
                default:
                    log.Error("The index for Registration.WhatTypeOfRelationshipAreYouLookingFor_Click() was outside the bounds of what's available");
                    break;
            }
        }

        public void WhatTypeOfRelationshipAreYouLookingForCheckbox_NewRegNotOnBedrock_Click(int index)
        {
            switch (index)
            {
                case 1:
                    WaitUntilElementIsCheckable(By.XPath("//input[@id='id75960']"));
                    break;
                case 2:
                    WaitUntilElementIsCheckable(By.XPath("//input[@id='id100032']"));
                    break;
                case 3:
                    WaitUntilElementIsCheckable(By.XPath("//input[@id='id75964']"));
                    break;
                case 4:
                    WaitUntilElementIsCheckable(By.XPath("//input[@id='id75968']"));
                    break;
                case 5:
                    WaitUntilElementIsCheckable(By.XPath("//input[@id='id100018']"));
                    break;
                default:
                    log.Error("The index for Registration.WhatTypeOfRelationshipAreYouLookingFor_NewRegNotOnBedrock_Click() was outside the bounds of what's available");
                    break;
            }
        }

        public void UserNameTextbox_Write(string text)
        {
            // in IE, we need to clear the textbox first or else the text appends to anything existing
            if (driver.ToString() == System.Configuration.ConfigurationManager.AppSettings["IE_driverString"])
                WaitUntilElementValueCanBeCleared(By.XPath("//input[contains(@id, 'idUserName_AttributeTextControl')]"));

            WaitUntilElementIsWritable(By.XPath("//input[contains(@id, 'idUserName_AttributeTextControl')]"), text);
        }

        public void UserNameTextbox_NewRegNotOnBedrock_Write(string text)
        {
            WaitUntilElementIsWritable(By.XPath("//input[contains(@id, 'idusername')]"), text);
        }

        public void BirthDateMonthList_Select(int index)
        {
            SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, 'BirthDate_ddlBirthDateMonthUS')]")));
            select.SelectByIndex(index);
        }

        public void BirthDateMonthList_NewRegNotOnBedrock_Select(int index)
        {
            SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[@id='birthdayMonth']")));
            select.SelectByIndex(index);
        }

        public void BirthDateDayList_Select(int index)
        {
            SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, 'BirthDate_ddlBirthDateDayUS')]")));
            select.SelectByIndex(index);
        }

        public void BirthDateDayList_NewRegNotOnBedrock_Select(int index)
        {
            SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[@id='birthdayDay']")));
            select.SelectByIndex(index);
        }

        public void BirthDateYearList_Select(int index)
        {
            SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, 'BirthDate_ddlBirthDateYearUS')]")));
            select.SelectByIndex(index);
        }

        public void BirthDateYearList_NewRegNotOnBedrock_Select(int index)
        {
            SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[@id='birthdayYear']")));
            select.SelectByIndex(index);
        }

        public void EmailAddressTextbox_Write(string text)
        {
            if (driver.ToString() == System.Configuration.ConfigurationManager.AppSettings["IE_driverString"])
                WaitUntilElementValueCanBeCleared(By.XPath("//input[contains(@id, 'EmailAddress_AttributeTextControl')]"));

            WaitUntilElementIsWritable(By.XPath("//input[contains(@id, 'EmailAddress_AttributeTextControl')]"), text);
        }

        public void EmailAddressTextbox_NewRegNotOnBedrock_Write(string text)
        {
            WaitUntilElementIsWritable(By.XPath("//input[@id='idemailaddress']"), text);
        }

        public string EmailAddress
        {
            get
            {
                string emailAddressText = WaitUntilElementTextValueIsPresent(By.XPath("//input[contains(@id, 'EmailAddress_AttributeTextControl')]"));

                return emailAddressText;
            }
        }

        public void ChooseYourPasswordTextbox_Write(string text)
        {
            if (driver.ToString() == System.Configuration.ConfigurationManager.AppSettings["IE_driverString"])
                WaitUntilElementValueCanBeCleared(By.XPath("//input[contains(@id, 'Password_AttributeTextControl')]"));

            WaitUntilElementIsWritable(By.XPath("//input[contains(@id, 'Password_AttributeTextControl')]"), text);
        }

        public void ChooseYourPasswordTextbox_NewRegNotOnBedrock_Write(string text)
        {
            WaitUntilElementIsWritable(By.XPath("//input[@id='idpassword']"), text);
        }

        public void DescribeYourselfAndYourPersonalityTextArea_Write(string text)
        {
            if (driver.ToString() == System.Configuration.ConfigurationManager.AppSettings["IE_driverString"])
                WaitUntilElementValueCanBeCleared(By.XPath("//textarea[contains(@id, 'idAboutMe_AttributeTextControl')]"));

            WaitUntilElementIsWritable(By.XPath("//textarea[contains(@id, 'idAboutMe_AttributeTextControl')]"), text);
        }

        public void DescribeYourselfAndYourPersonalityTextArea_NewRegNotOnBedrock_Write(string text)
        {
            WaitUntilElementIsWritable(By.XPath("//textarea[@id='idaboutme']"), text);
        }

        public void UploadAPhotoTextbox_Write(string text)
        {
            if (driver.ToString() == System.Configuration.ConfigurationManager.AppSettings["IE_driverString"])
                WaitUntilElementValueCanBeCleared(By.XPath("//input[contains(@id, 'idPhoto_fileupload')]"));

            WaitUntilElementIsWritable(By.XPath("//input[contains(@id, 'idPhoto_fileupload')]"), text);
        }

        public void PleaseEnterTheCodeShownBelow_Write(string text)
        {
            if (driver.ToString() == System.Configuration.ConfigurationManager.AppSettings["IE_driverString"])
                WaitUntilElementValueCanBeCleared(By.XPath("//input[contains(@id, 'Captcha_AttributeCaptchaControl')]"));

            WaitUntilElementIsWritable(By.XPath("//input[contains(@id, 'Captcha_AttributeCaptchaControl')]"), text);
        }

        public void IWouldLikeSpecialOffersAndAnnouncements_Click()
        {
            string xPathOfSelectedIndex = "//div[contains(@id, 'PartnersOffers2_divControl')]/span[contains(@id, 'PartnersOffers2_AttributeOptions')]/input[contains(@id, 'PartnersOffers2_AttributeOptions_0')]";

            WaitUntilElementIsClickable(By.XPath(xPathOfSelectedIndex));
        }

        public void IWouldLikeSpecialOffersAndAnnouncements_NewRegNotOnBedrock_Click()
        {
            string xPathOfSelectedIndex = "//input[@id='idnewslettermask']";

            WaitUntilElementIsClickable(By.XPath(xPathOfSelectedIndex));
        }

        public void IConfirmThatIHaveReadAndAgreedToTheTermsAndConditions_Click()
        {
            string xPathOfSelectedIndex = "//div[contains(@id, 'TermsAndConditions_divControl')]/input[contains(@id, 'TermsAndConditions_AttributeCheckBoxControl')]";

            WaitUntilElementIsClickable(By.XPath(xPathOfSelectedIndex));
        }

        public void YouAreAListBox_RegPathAandD_Select(int index)
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'Gender_divControl')]/select[contains(@id, 'Gender_AttributeOptions')]"));

            for (int i = 0; i < index; i++)
                element.SendKeys(Keys.Down);
        }

        public void WhatIsYourCurrentRelationshipStatus_RegPathAandD_Select(int index)
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//select[contains(@id, 'idMaritalStatus_AttributeOptions')]"));

            for (int i = 0; i < index; i++)
                element.SendKeys(Keys.Down);
        }

        public void WhatIsYourCurrentRelationshipStatus_NewRegNotOnBedrock_Select(int index)
        {
            SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[@id='idmaritalstatus']")));
            select.SelectByIndex(index);
        }

        public void YourSmokingHabitsDropdown_RegPathAandD_Select(int index)
        {
            if (driver.Url.Contains("jdate.com"))
            {
                SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, 'SmokingHabits_AttributeOptions')]")));
                select.SelectByIndex(index);
            }
            else
            {
                IWebElement element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'SmokingHabits_divControl')]/select[contains(@id, 'SmokingHabits_AttributeOptions')]"));

                for (int i = 0; i < index; i++)
                    element.SendKeys(Keys.Down);
            }
        }

        public void YourSmokingHabitsDropdown_NewRegNotOnBedrock_Select(int index)
        {
            SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[@id='idsmokinghabits']")));
            select.SelectByIndex(index);
        }

        public void YourDrinkingHabitsDropdown_RegPathAandD_Select(int index)
        {
            if (driver.Url.Contains("jdate.com"))
            {
                SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, 'DrinkingHabits_AttributeOptions')]")));
                select.SelectByIndex(index);
            }
            else
            {
                IWebElement element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'DrinkingHabits_divControl')]/select[contains(@id, 'DrinkingHabits_AttributeOptions')]"));

                for (int i = 0; i < index; i++)
                    element.SendKeys(Keys.Down);

                // in BBW the stupid Continue button won't press no matter how many times we click it so we're bypassing it by using Select
                SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//div[contains(@id, 'DrinkingHabits_divControl')]/select[contains(@id, 'DrinkingHabits_AttributeOptions')]")));
                select.SelectByIndex(index);

                Wait(2000);
            }
        }

        public void YourDrinkingHabitsDropdown_NewRegNotOnBedrock_Select(int index)
        {
            SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[@id='iddrinkinghabits']")));
            select.SelectByIndex(index);
        }

        public void DoYouKeepKosherScenarioDropdown_RegPathAandD_Select(int index)
        {
            SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//div[contains(@id, 'KeepKosher_divControl')]/select[contains(@id, 'KeepKosher_AttributeOptions')]")));
            select.SelectByIndex(index);
        }

        public void WhatDoYouDo_RegPathAandD_Select(int index)
        {
            SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, 'Occupation_AttributeOptions')]")));
            select.SelectByIndex(index);
        }

        public void WhatDoYouDo_NewRegNotOnBedrock_Select(int index)
        {
            SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[@id='idoccupation']")));
            select.SelectByIndex(index);
        }

        public void YourEthnicity_RegPathAandD_Select(int index)
        {
            SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, 'JDateEthnicity_AttributeOptions')]")));
            select.SelectByIndex(index);
        }

        public void YourReligiousBackground_RegPathAandD_Select(int index)
        {
            SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, 'JDateReligion_AttributeOptions')]")));
            select.SelectByIndex(index);
        }

        public void YourReligiousBackground_NewRegNotOnBedrock_Select(int index)
        {
            SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[@id='idjdatereligion']")));
            select.SelectByIndex(index);
        }

        public void HowOftenDoYouGoToSynagogue_RegPathAandD_Select(int index)
        {
            SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, 'SynagogueAttendance_AttributeOptions')]")));
            select.SelectByIndex(index);
        }

        public void HowOftenDoYouGoToSynagogue_NewRegNotOnBedrock_Select(int index)
        {
            SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[@id='idsynagogueattendance']")));
            select.SelectByIndex(index);
        }

        /// <summary>
        /// For the past few months JDate has been playing around with forcing users to add their first name before completing registration.  Currently there are two 
        /// different ways we are doing this and this method checks to see if the request for the user to add their First Name appears during the "Create Your Profile -
        /// Lifestyle" page at the very end of registration.
        /// </summary>
        /// <returns></returns>
        public bool DoesFirstNameAppearOnRegistrationScreen()
        {
            if (driver.FindElements(By.XPath("//div[contains(@id, '_idFirstName_divControl')]/label[2]")).Count > 0 && driver.FindElement(By.XPath("//div[contains(@id, '_idFirstName_divControl')]/label[2]")).Text == "First name:")
                return true;

            return false;
        }

        public void FirstNameTextbox_JDate_Write(string text)
        {
            WaitUntilElementIsWritable(By.XPath("//input[contains(@id, '_idFirstName_AttributeTextControl')]"), text);
        }
        #endregion


        #region Registration Scenario B and C only objects - this different Registration overlay has different xpaths for the same objects
        public void WhatTypeOfRelationshipAreYouLookingForListBox_RegPathBandC_Click(int index)
        {
            SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, 'idRelationshipmask_AttributeOptions')]")));
            select.SelectByIndex(index);
        }

        public void DoYouKeepKosher_RegPathBandC_Select(int index)
        {
            SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, 'idKeepKosher_AttributeOptions')]")));
            select.SelectByIndex(index);
        }

        public void YourEducationLevel_RegPathBandC_Select(int index)
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//select[contains(@id, 'idEducationLevel_AttributeOptions')]"));

            for (int i = 0; i < index; i++)
                element.SendKeys(Keys.Down);

            // in BBW the stupid Continue button won't press no matter how many times we click it so we're bypassing it by using Select
            SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, 'idEducationLevel_AttributeOptions')]")));
            select.SelectByIndex(index);

            Wait(2000);
        }

        public void YourEducationLevel_NewRegNotOnBedrock_Select(int index)
        {
            SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[@id='ideducationlevel']")));
            select.SelectByIndex(index);
        }

        public void WhatIsYourOccupation_RegPathBandC_Select(int index)
        {
            SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, 'idOccupation_AttributeOptions')]")));
            select.SelectByIndex(index);
        }

        public void WhatIsYourEthnicity_RegPathBandC_Select(int index)
        {
            SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, 'idJDateEthnicity_AttributeOptions')]")));
            select.SelectByIndex(index);
        }

        public void WhatIsYourReligiousBackground_RegPathBandC_Select(int index)
        {
            SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, 'idJDateReligion_AttributeOptions')]")));
            select.SelectByIndex(index);
        }

        public void HowOftenDoYouGoToSynagogue_RegPathBandC_Select(int index)
        {
            SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, 'idSynagogueAttendance_AttributeOptions')]")));
            select.SelectByIndex(index);
        }
        #endregion


        #region Spark Registration only objects
        public void WhatIsYourEthnicity_SparkReg_Select(int index)
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//select[contains(@id, 'idEthnicity_AttributeOptions')]"));

            for (int i = 0; i < index; i++)
                element.SendKeys(Keys.Down);
        }

        public void WhatIsYourReligiousBackground_SparkReg_Select(int index)
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//select[contains(@id, 'idReligion_AttributeOptions')]"));

            for (int i = 0; i < index; i++)
                element.SendKeys(Keys.Down);
        }

        public void WhatIsYourReligiousBackground_NewRegNotOnBedrock_Select(int index)
        {
            SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[@id='idreligion']")));
            select.SelectByIndex(index);
        }

        /// <summary>
        /// The Continue Button on this page is testy.  Since there seems to be a timing issue with the page trying to verify your zip code, sometimes clicking on
        /// the Continue button does not work.  So click until we get to the next page, trying 5 times over and over.
        /// </summary>
        public void ContinueButton_OnZipCodePage_SparkReg_Click()
        {
            Wait(1000);

            int countLimit = 5;

            for (int count = 0; count < countLimit; count++)
            {
                continueButton = driver.FindElement(By.XPath("//input[contains(@id, 'btnContinue')]"));
                continueButton.Click();

                Wait(1000);

                try
                {
                    IWebElement label = driver.FindElement(By.XPath("//div[contains(@id, 'idUserName_divControl')]/label"));

                    if (label.Text.Contains("User Name"))
                        break;
                }
                catch { }
            }

            Wait(1000);
        }
        #endregion


        #region other misc. objects found on the Registration overlay page.  this is the page with the Register overlay on top of the Your Searches page
        public void WhatIsYourEthnicity_Select(int index)
        {
            if (driver.Url.Contains("blacksingles.com"))
            {
                SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, 'idEthnicity_AttributeOptions')]")));
                select.SelectByIndex(index);
            }
            else
            {
                IWebElement element = WaitUntilElementExists(By.XPath("//select[contains(@id, 'idEthnicity_AttributeOptions')]"));

                for (int i = 0; i < index; i++)
                    element.SendKeys(Keys.Down);
            }

            if (driver.Url.Contains("bbwpersonalsplus.com"))
            {
                // in BBW the stupid Continue button won't press no matter how many times we click it so we're bypassing it by using Select
                SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, 'idEthnicity_AttributeOptions')]")));
                select.SelectByIndex(index);

                Wait(2000);
            }
        }

        public void WhatIsYourEthnicity_NewRegNotOnBedrock_Select_Select(int index)
        {
            SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[@id='idethnicity']")));
            select.SelectByIndex(index);
        }

        public void WhatIsYourHeight_Select(int index)
        {
            if (driver.Url.Contains("blacksingles.com"))
            {
                SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, 'idheight_AttributeOptions')]")));
                select.SelectByIndex(index);
            }
            else
            {
                IWebElement element = WaitUntilElementExists(By.XPath("//select[contains(@id, 'idheight_AttributeOptions')]"));

                for (int i = 0; i < index; i++)
                    element.SendKeys(Keys.Down);
            }
        }

        public void WhatIsYourHeight_NewRegNotOnBedrock_Select(int index)
        {
            SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[@id='idheightcm']")));
            select.SelectByIndex(index);
        }

        public void WhatIsYourBodyTypeListbox_Select(int index)
        {
            if (driver.Url.Contains("blacksingles.com"))
            {
                SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, 'idbodytype_AttributeOptions')]")));
                select.SelectByIndex(index);
            }
            else
            {
                IWebElement element = WaitUntilElementExists(By.XPath("//select[contains(@id, 'idbodytype_AttributeOptions')]"));

                for (int i = 0; i < index; i++)
                    element.SendKeys(Keys.Down);
            }
        }

        public void WhatIsYourBodyTypeListbox_NewRegNotOnBedrock_Select(int index)
        {
            SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[@id='idbodytype']")));
            select.SelectByIndex(index);
        }

        public void WhatIsYourEyeColorListbox_Select(int index)
        {
            if (driver.Url.Contains("blacksingles.com"))
            {
                SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, 'idEyeColor_AttributeOptions')]")));
                select.SelectByIndex(index);
            }
            else
            {
                IWebElement element = WaitUntilElementExists(By.XPath("//select[contains(@id, 'idEyeColor_AttributeOptions')]"));

                for (int i = 0; i < index; i++)
                    element.SendKeys(Keys.Down);
            }
        }

        public void WhatIsYourEyeColorListbox_NewRegNotOnBedrock_Select(int index)
        {
            SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[@id='ideyecolor']")));
            select.SelectByIndex(index);
        }

        public void WhatIsYourHairColorListbox_Select(int index)
        {
            if (driver.Url.Contains("blacksingles.com"))
            {
                SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, 'idHairColor_AttributeOptions')]")));
                select.SelectByIndex(index);
            }
            else
            {
                IWebElement element = WaitUntilElementExists(By.XPath("//select[contains(@id, 'idHairColor_AttributeOptions')]"));

                for (int i = 0; i < index; i++)
                    element.SendKeys(Keys.Down);
            }

            if (driver.Url.Contains("bbwpersonalsplus.com"))
            {
                // in BBW the stupid Continue button won't press no matter how many times we click it so we're bypassing it by using Select
                SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, 'idHairColor_AttributeOptions')]")));
                select.SelectByIndex(index);

                Wait(2000);
            }
        }

        public void WhatIsYourHairColorListbox_NewRegNotOnBedrock_Select(int index)
        {
            SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[@id='idhaircolor']")));
            select.SelectByIndex(index);
        }

        public void WhatIsYourOccupationTextbox_Write(string text)
        {
            WaitUntilElementIsWritable(By.XPath("//input[contains(@id, 'idOccupationDescription')]"), text);
        }

        public void WhatIsYourOccupationTextbox_NewRegNotOnBedrock_Write(string text)
        {
            WaitUntilElementIsWritable(By.XPath("//input[@id='idoccupationdescription']"), text);
        }

        public void HowManyChildrenDoYouHaveListbox_Select(int index)
        {
            if (driver.Url.Contains("blacksingles.com"))
            {
                SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, 'idChildrenCount_AttributeOptions')]")));
                select.SelectByIndex(index);
            }
            else
            {
                IWebElement element = WaitUntilElementExists(By.XPath("//select[contains(@id, 'idChildrenCount_AttributeOptions')]"));

                for (int i = 0; i < index; i++)
                    element.SendKeys(Keys.Down);
            }

            if (driver.Url.Contains("bbwpersonalsplus.com"))
            {
                // in BBW the stupid Continue button won't press no matter how many times we click it so we're bypassing it by using Select
                SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, 'idChildrenCount_AttributeOptions')]")));
                select.SelectByIndex(index);

                Wait(2000);
            }
        }

        public void HowManyChildrenDoYouHaveListbox_NewRegNotOnBedrock_Select(int index)
        {
            SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[@id='idchildren']")));
            select.SelectByIndex(index);
        }

        public void DoYouWantChildrenDropdown_Select(int index)
        {
            SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, 'idMoreChildrenFlag_AttributeOptions')]")));
            select.SelectByIndex(index);
        }

        public void DoYouWantChildrenDropdown_NewRegNotOnBedrock_Select(int index)
        {
            SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[@id='idmorechildrenflag']")));
            select.SelectByIndex(index);
        }

        public string CountryDropdown
        {
            get
            {
                SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, 'idRegion_country')]")));

                return select.SelectedOption.Text;
            }
        }

        public string ZipCodeTextbox
        {
            get
            {
                IWebElement element = WaitUntilElementExists(By.XPath("//input[contains(@id, 'idRegion_postalCode')]"));
                return element.GetAttribute("value");
            }
        }

        public void FirstNameTextbox_Write(string text)
        {
            WaitUntilElementIsWritable(By.XPath("//input[contains(@id, 'idFirstName')]"), text);
        }

        public void FirstNameTextbox_NewRegNotOnBedrock_Write(string text)
        {
            WaitUntilElementIsWritable(By.XPath("//input[contains(@id, 'idfirstname')]"), text);
        }

        public void LastNameTextbox_Write(string text)
        {
            WaitUntilElementIsWritable(By.XPath("//input[contains(@id, 'idLastName')]"), text);
        }

        public void LastNameNameTextbox_NewRegNotOnBedrock_Write(string text)
        {
            WaitUntilElementIsWritable(By.XPath("//input[contains(@id, 'idlastname')]"), text);
        }

        public void ConfirmEmailTextbox_Write(string text)
        {
            WaitUntilElementIsWritable(By.XPath("//input[contains(@id, 'idEmailAddress_AttributeTextControlConfirm')]"), text);
        }

        //public void YourEducationLevelListbox_AltRegPage_Select(int index)
        //{
        //    string xPathOfSelectedIndex = "//div[contains(@id, 'EducationLevel_divControl')]/select[contains(@id, 'EducationLevel_AttributeOptions')]/option[" + index.ToString() + "]";

        //    WaitUntilElementIsSelectable(By.XPath(xPathOfSelectedIndex));
        //}

        public void YourEducationLevelListbox_AltRegPage_Select(int index)
        {
            SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//div[contains(@id, 'EducationLevel_divControl')]/select[contains(@id, 'EducationLevel_AttributeOptions')]")));
            select.SelectByIndex(index);
        }

        /// <summary>
        /// The continue button is shared among all pages within Registration.  Since objects are EXTREMELY finnicky before and after Continue is pressed, wait before
        /// and after using it so that objects don't erroneously error out as being stale when you use them.
        /// </summary>
        public void ContinueButton_AltRegPage_Click()
        {
            Wait(1000);

            WaitUntilElementIsClickable(By.XPath("//a[@id='nextBtn']/img"));

            Wait(1000);
        }

        public void YourSmokingHabitsListbox_AltRegPage_Select(int index)
        {
            SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, 'idSmokingHabits_AttributeOptions')]")));
            select.SelectByIndex(index);
        }

        public void YourDrinkingHabitsListbox_AltRegPage_Select(int index)
        {
            SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, 'idDrinkingHabits_AttributeOptions')]")));
            select.SelectByIndex(index);
        }

        public void WhatIsYourMaritalStatusDropdown_AltRegPage_Click(int index)
        {
            if (driver.Url.Contains("blacksingles.com") || driver.Url.Contains("jdate.com"))
            {
                SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, 'idMaritalStatus_AttributeOptions')]")));
                select.SelectByIndex(index);
            }
            else
            {
                IWebElement element = WaitUntilElementExists(By.XPath("//select[contains(@id, 'idMaritalStatus_AttributeOptions')]"));

                for (int i = 0; i < index; i++)
                    element.SendKeys(Keys.Down);
            }

            if (driver.Url.Contains("bbwpersonalsplus.com"))
            {
                // in BBW the stupid Continue button won't press no matter how many times we click it so we're bypassing it by using Select
                SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, 'idMaritalStatus_AttributeOptions')]")));
                select.SelectByIndex(index);

                Wait(2000);
            }
        }

        public void WhatIsYourReligiousBackground_AltRegPage_Select(int index)
        {
            SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, 'idReligion_AttributeOptions')]")));
            select.SelectByIndex(index);
        }

        public void YouAreA_AltRegPage_Select(int index)
        {
            SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, '_idSeekingGender_AttributeOptions')]")));
            select.SelectByIndex(index);
        }

        public void YouAreA_NewRegNotOnBedrock_Select(int index)
        {
            SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[@id='idgendermask']")));
            select.SelectByIndex(index);
        }

        public void IAmA_AltRegPage_Select(int index)
        {
            SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, 'idGender_AttributeOptions')]")));
            select.SelectByIndex(index);
        }

        public void IAmA_NewRegNotOnBedrock_Select(int index)
        {
            SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[@id='idgendermask']")));
            select.SelectByIndex(index);
        }

        public bool IsIAmAOnscreenCurrently_AltRegPage()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'idGender_divControl')]/label"));

            if (element.Text == "")
                return false;

            return true;
        }
        #endregion

        public void WhatIsYourMaritalStatusCheckboxes_Check(int index)
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//input[contains(@id, 'idMaritalStatus_AttributeOptions_" + index + "')]"));
            element.Click();
        }

        public void WhatIsYourMaritalStatusCheckboxes_NewRegNotOnBedrock_Check(int index)
        {
            SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[@id='idmaritalstatus']")));
            select.SelectByIndex(index);
        }

        public void AreaDropdown_Select(int index)
        {
            SelectElement select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, '_idRegion_state')]")));
            select.SelectByIndex(index);

            Thread.Sleep(1000);
        }

        public void AreaDropdown_NewRegNotOnBedrock_Select(int index)
        {
            SelectElement select = null;

            if (driver.FindElements(By.XPath("//select[contains(@id, 'regionStateId')]")).Count > 0)            
                select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, 'regionStateId')]")));
            else            
                select = new SelectElement(WaitUntilElementExists(By.XPath("//select[contains(@id, 'idregioncountryid')]")));

            select.SelectByIndex(index);

            Thread.Sleep(1000);
        }

        public void HometownTextbox_Write(string text)
        {
            WaitUntilElementIsWritable(By.XPath("//input[contains(@id, '_idRegion_city')]"), text);
        }

        public void HometownTextbox_NewRegNotOnBedrock_Write(string text)
        {
            WaitUntilElementIsWritable(By.XPath("//input[contains(@id, 'regionCitiesByRegionId')]"), text);
        }

        public void ClickOnBackground()
        {
            Thread.Sleep(2000);

            IWebElement element = WaitUntilElementExists(By.XPath("//form[@id='regFlow']"));
            element.Click();
        }
    }
}

