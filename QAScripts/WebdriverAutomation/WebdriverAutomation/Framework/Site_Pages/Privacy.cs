﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.Events;
using NUnit.Framework;
using log4net;

// PAGE URL:  http://www.jdate.com/Applications/Article/ArticleView.aspx?CategoryID=1948&ArticleID=6498&HideNav=True#privacy

namespace WebdriverAutomation.Framework.Site_Pages
{
    public class Privacy : BasePage
    {
        public Privacy(IWebDriver driver)
            : base(driver)
        {
            // verify we are on the correct page       
            IWebElement textOnPage = null;

            if (driver.Url.Contains("jdate.co.il"))
            {
                textThatVerifiesThisPageIsCorrect = "מדיניות שמירה על פרטיות";
                textOnPage = WaitUntilPageVerificationObjectExists(By.XPath("//div[@id='article']/table[@id='Table1']/tbody/tr/td/h1[2]"), "PRIVACY PAGE");
            }
            else
            {
                textThatVerifiesThisPageIsCorrect = "Privacy Statement";
                textOnPage = WaitUntilPageVerificationObjectExists(By.XPath("//div[@id='content-main']/div[@id='page-container']/div[2]/h1[1]/strong"), "PRIVACY PAGE");
            }            

            if (textOnPage.Text != (textThatVerifiesThisPageIsCorrect))
                log.Error("ERROR - PRIVACY PAGE - We should be on this page but the page verification failed.");

            Assert.True(textOnPage.Text == textThatVerifiesThisPageIsCorrect, "ERROR - PRIVACY PAGE - We should be on this page but the page verification failed.");

        }

        private static readonly ILog log = LogManager.GetLogger(typeof(Privacy));

        private string textThatVerifiesThisPageIsCorrect = string.Empty;



    }
}
