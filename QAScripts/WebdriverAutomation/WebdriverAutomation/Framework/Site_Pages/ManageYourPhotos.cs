﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.Events;
using NUnit.Framework;
using log4net;
using System.Threading;

// PAGE URL:  http://www.jdate.com/Applications/MemberProfile/MemberPhotoEdit.aspx (while logged in)

namespace WebdriverAutomation.Framework.Site_Pages
{
    public class ManageYourPhotos : BasePage
    {
        public ManageYourPhotos(IWebDriver driver)
            : base(driver)
        {
            Header = new WebdriverAutomation.Framework.Site_Parts.Header(driver);
            Footer = new WebdriverAutomation.Framework.Site_Parts.Footer(driver);

            // verify we are on the correct page       
            IWebElement textOnPage = null;

            if (driver.Url.Contains("jdate.co.il"))
                textThatVerifiesThisPageIsCorrect = "ניהול התמונות שלך";
            else
                textThatVerifiesThisPageIsCorrect = "Manage Your Photos";

            textOnPage = WaitUntilPageVerificationObjectExists(By.XPath("//div[@id='page-container']/h1"), "MANAGE YOUR PHOTOS PAGE");

            if (textOnPage.Text != (textThatVerifiesThisPageIsCorrect))
                log.Error("ERROR - MANAGE YOUR PHOTOS PAGE - We should be on this page but the page verification failed.");

            Assert.True(textOnPage.Text == textThatVerifiesThisPageIsCorrect, "ERROR - MANAGE YOUR PHOTOS - We should be on this page but the page verification failed.");

        }

        private static readonly ILog log = LogManager.GetLogger(typeof(Login));
        public WebdriverAutomation.Framework.Site_Parts.Header Header;
        public WebdriverAutomation.Framework.Site_Parts.Footer Footer;

        private string textThatVerifiesThisPageIsCorrect = string.Empty;

        public bool Photo1PhotoExists()
        {
            IWebElement element;

            try
            {
                element = WaitUntilElementExists(By.XPath("//table[@id='pics-management']/tbody[@id='pics-cont-row']/tr[@id='row-1']/td[2]/div/img[contains(@id, 'rptPhotos__ctl1_thumbPhoto')]"));
                return true;                
            }
            catch 
            {
                return false;
            }            
        }

        public void Photo1UploadAPhotoTextbox_Write(string value)
        {
            string theXpath = "//input[contains(@id, 'rptPhotos__ctl1_upBrowse')]";

            WaitUntilElementValueCanBeCleared(By.XPath(theXpath));
            WaitUntilElementExists(By.XPath(theXpath)).SendKeys(value);
        }

        public string Photo1CaptionTextarea
        {
            get
            {
                IWebElement element = WaitUntilElementExists(By.XPath("//textarea[contains(@id, 'rptPhotos__ctl1_txtCaption')]"));
                return element.Text;
            }
            set
            {
                string theXpath = "//textarea[contains(@id, 'rptPhotos__ctl1_txtCaption')]";

                WaitUntilElementValueCanBeCleared(By.XPath(theXpath));
                SendKeys(WaitUntilElementExists(By.XPath(theXpath)), value);
            }
        }

        public void Photo1XButton_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//table[@id='pics-management']/tbody[@id='pics-cont-row']/tr[@id='row-1']/td[5]/div[1]/a[1]"));
            element.Click();

            Wait(1000);
        }

        public void Photo1UndeleteLink_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//table[@id='pics-management']/tbody[@id='pics-cont-row']/tr[@id='row-1']/td[5]/div[1]/a[2]"));
            element.Click();

            Wait(1000);

            // sometimes this doesn't work the first time around so if it still exists, click the link again
            if (driver.FindElements(By.XPath("//table[@id='pics-management']/tbody[@id='pics-cont-row']/tr[@id='row-1']/td[1]/div/div")).Count > 0)
            {
                element = WaitUntilElementExists(By.XPath("//table[@id='pics-management']/tbody[@id='pics-cont-row']/tr[@id='row-1']/td[5]/div[1]/a[2]"));
                element.Click();
            }

            Wait(2000);
        }

        public string Photo1Notification
        {
            get
            {
                IWebElement element = WaitUntilElementExists(By.XPath("//table[@id='pics-management']/tbody[@id='pics-cont-row']/tr[@id='row-1']/td[1]/div/div"));
                return element.Text;
            }
        }

        public bool Photo2PhotoExists()
        {
            IWebElement element;

            if (driver.FindElements(By.XPath("//img[contains(@id, '_rptPhotos__ctl2_thumbPhoto')]")).Count > 0)
            {
                element = WaitUntilElementExists(By.XPath("//img[contains(@id, '_rptPhotos__ctl2_thumbPhoto')]"));
                return true;
            }
            else
                return false; 
        }

        public void Photo2UploadAPhotoTextbox_Write(string value)
        {
            string theXpath = "//input[contains(@id, 'rptPhotos__ctl2_upBrowse')]";

            WaitUntilElementValueCanBeCleared(By.XPath(theXpath));
            WaitUntilElementExists(By.XPath(theXpath)).SendKeys(value);
        }

        public string Photo2CaptionTextarea
        {
            get
            {
                IWebElement element = WaitUntilElementExists(By.XPath("//textarea[contains(@id, 'rptPhotos__ctl2_txtCaption')]"));
                return element.Text;
            }
            set
            {
                string theXpath = "//textarea[contains(@id, 'rptPhotos__ctl2_txtCaption')]";

                WaitUntilElementValueCanBeCleared(By.XPath(theXpath));
                SendKeys(WaitUntilElementExists(By.XPath(theXpath)), value);
            }
        }

        public void Photo2XButton_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//table[@id='pics-management']/tbody[@id='pics-cont-row']/tr[@id='row-2']/td[5]/div[1]/a[1]"));
            element.Click();

            Wait(1000);
        }

        public void Photo2UndeleteLink_Click()
        {
            int loopCount = 10;

            Wait(1000);

            for (int i = 0; i < loopCount; i++)
            {
                IWebElement element = WaitUntilElementExists(By.XPath("//table[@id='pics-management']/tbody[@id='pics-cont-row']/tr[@id='row-2']/td[5]/div[1]/a[2]"));
                element.Click();

                Wait(1000);

                if (driver.FindElements(By.XPath("//table[@id='pics-management']/tbody[@id='pics-cont-row']/tr[@id='row-2']/td[1]/div/div")).Count == 0)
                    break;

                if (i == loopCount - 1)
                    Assert.Fail("We tried to click the undelete link " + loopCount + " times but was unable to.");
            }

            Wait(2000);
        }

        public string Photo2Notification
        {
            get
            {
                IWebElement element = WaitUntilElementExists(By.XPath("//table[@id='pics-management']/tbody[@id='pics-cont-row']/tr[@id='row-2']/td[1]/div/div"));
                return element.Text;
            }
        }

        public void SaveChangesButton_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//img[contains(@id, 'btnSaveImage')]"));
            ClickUntilObjectTextChanges(element, By.XPath("//div[@id='page-container']/div[1]/div[1]/div/div/div"));

            // been seeing test cases get stuck here because it takes a few seconds to save.  so we're gonna wait a few seconds.
            Thread.Sleep(3000);
        }

        public string NotificationText
        {
            get
            {
                IWebElement element = WaitUntilElementExists(By.XPath("//div[contains(@id, 'divNotification')]"));
                return element.Text;
            }
        }
    }
}
