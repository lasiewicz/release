﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.Events;
using NUnit.Framework;
using log4net;
using System.Threading;

// PAGE URL:  http://www.jdate.co.il/Applications/MemberProfile/RegistrationWelcomeOnePage.aspx

namespace WebdriverAutomation.Framework.Site_Pages
{
    public class RegistrationJDateCoIlOnePage : BasePage
    {
        public RegistrationJDateCoIlOnePage(IWebDriver driver)
            : base(driver)
        {
            Footer = new WebdriverAutomation.Framework.Site_Parts.Footer(driver);

            Tests.ReusableCode.Ads ads = new Tests.ReusableCode.Ads();
            ads.KillAllAds(driver, log);

            // verify we are on the correct page       
            IWebElement textOnPage = null;

            if (driver.FindElements(By.XPath("//div[@id='complete-profile']/span")).Count > 0)
                textOnPage = WaitUntilPageVerificationObjectExists(By.XPath("//div[@id='complete-profile']/span"), "REGISTRATION ONEPAGE FOR JDATE.CO.IL PAGE");
            else if (driver.FindElements(By.XPath("//html/body/div/div/section/div")).Count > 0)
                textOnPage = WaitUntilPageVerificationObjectExists(By.XPath("//html/body/div/div/section/div"), "REGISTRATION ONEPAGE FOR JDATE.CO.IL PAGE");
            else
                textOnPage = WaitUntilPageVerificationObjectExists(By.XPath("//html/body/div/div/div/div/section/div"), "REGISTRATION ONEPAGE FOR JDATE.CO.IL PAGE");

            if (!textOnPage.Text.Contains(textThatVerifiesThisPageIsCorrect1) && !textOnPage.Text.Contains(textThatVerifiesThisPageIsCorrect2))
                log.Error("ERROR - REGISTRATION ONEPAGE FOR JDATE.CO.IL PAGE - We should be on this page but the page verification failed.");

            Assert.True(textOnPage.Text.Contains(textThatVerifiesThisPageIsCorrect1) || textOnPage.Text.Contains(textThatVerifiesThisPageIsCorrect2), "ERROR - REGISTRATION ONEPAGE FOR JDATE.CO.IL PAGE - We should be on this page but the page verification failed.");

        }

        private static readonly ILog log = LogManager.GetLogger(typeof(AboutJDate));
        public WebdriverAutomation.Framework.Site_Parts.Footer Footer;

        private const string textThatVerifiesThisPageIsCorrect1 = "מהאתר עלייך למלא את הפרטים הבאים:";
        private const string textThatVerifiesThisPageIsCorrect2 = "         כדי שתוכל להופיע בתוצאות החיפוש של חברים אחרים וליהנות";

        public void MaritalStatusListBox_JDate_co_il_1Page_Select(int index)
        {
            string xPathOfSelectedIndex = "//section/div[3]/div[2]/ul/li[" + (index + 1).ToString() + "]";
            WaitUntilElementIsClickable(By.XPath(xPathOfSelectedIndex));
        }

        public void MaritalStatusListBox_JDate_co_il_MultiPage_Select(int index)
        {
            string xPathOfSelectedIndex = "//div[@id='first-row']/div[5]/ul/li[" + (index + 1).ToString() + "]";
            WaitUntilElementIsClickable(By.XPath(xPathOfSelectedIndex));
        }

        public void IKeepKosherListBox_JDate_co_il_1Page_Select(int index)
        {
            string xPathOfSelectedIndex = "//section/div[4]/div[2]/ul/li[" + (index + 1).ToString() + "]";
            WaitUntilElementIsClickable(By.XPath(xPathOfSelectedIndex));
        }

        public void IKeepKosherListBox_JDate_co_il_MultiPage_Select(int index)
        {
            string xPathOfSelectedIndex = "//div[@id='first-row']/div[6]/ul/li[" + (index + 1).ToString() + "]";            
            WaitUntilElementIsClickable(By.XPath(xPathOfSelectedIndex));
        }

        public void SmokingHabitsListBox_JDate_co_il_1Page_Select(int index)
        {
            string xPathOfSelectedIndex = "//section/div[5]/div[2]/ul/li[" + (index + 1).ToString() + "]";

            WaitUntilElementIsClickable(By.XPath(xPathOfSelectedIndex));
        }

        public void SmokingHabitsListBox_JDate_co_il_MultiPage_Select(int index)
        {
            string xPathOfSelectedIndex = "//div[@id='first-row']/div[7]/ul/li[" + (index + 1).ToString() + "]";

            WaitUntilElementIsClickable(By.XPath(xPathOfSelectedIndex));
        }

        public void DrinkingHabitsListBox_JDate_co_il_1Page_Select(int index)
        {
            string xPathOfSelectedIndex = "//section/div[6]/div[2]/ul/li[" + (index + 1).ToString() + "]";
            
            WaitUntilElementIsClickable(By.XPath(xPathOfSelectedIndex));
        }

        public void DrinkingHabitsListBox_JDate_co_il_MultiPage_Select(int index)
        {
            string xPathOfSelectedIndex = "//div[@id='first-row']/div[8]/ul/li[" + (index + 1).ToString() + "]";

            WaitUntilElementIsClickable(By.XPath(xPathOfSelectedIndex));
        }

        public void NumberOfChildrenListBox_JDate_co_il_1Page_Select(int index)
        {
            string xPathOfSelectedIndex = "//section/div[7]/div[2]/ul/li[" + (index + 1).ToString() + "]";

            WaitUntilElementIsClickable(By.XPath(xPathOfSelectedIndex));
        }

        public void NumberOfChildrenListBox_JDate_co_il_MultiPage_Select(int index)
        {
            string xPathOfSelectedIndex = "//div[@id='second-row']/div[5]/ul/li[" + (index + 1).ToString() + "]";
            
            WaitUntilElementIsClickable(By.XPath(xPathOfSelectedIndex));
        }

        public void OriginListBox_JDate_co_il_1Page_Select(int index)
        {
            string xPathOfSelectedIndex = "//section/div[8]/div[2]/ul/li[" + (index + 1).ToString() + "]";

            WaitUntilElementIsClickable(By.XPath(xPathOfSelectedIndex));
        }

        public void OriginListBox_JDate_co_il_MultiPage_Select(int index)
        {
            string xPathOfSelectedIndex = "//div[@id='second-row']/div[6]/ul/li[" + (index + 1).ToString() + "]";

            WaitUntilElementIsClickable(By.XPath(xPathOfSelectedIndex));
        }

        public void LevelOfEducationListBox_JDate_co_il_1Page_Select(int index)
        {
            string xPathOfSelectedIndex = "//section/div[9]/div[2]/ul/li[" + (index + 1).ToString() + "]";

            WaitUntilElementIsClickable(By.XPath(xPathOfSelectedIndex));
        }

        public void LevelOfEducationListBox_JDate_co_il_MultiPage_Select(int index)
        {
            string xPathOfSelectedIndex = "//div[@id='second-row']/div[7]/ul/li[" + (index + 1).ToString() + "]";

            WaitUntilElementIsClickable(By.XPath(xPathOfSelectedIndex));
        }

        public void HeightListBox_JDate_co_il_1Page_Select(int index)
        {
            string xPathOfSelectedIndex = "//section/div[10]/div[2]/ul/li[" + (index + 1).ToString() + "]";

            WaitUntilElementIsClickable(By.XPath(xPathOfSelectedIndex));
        }

        public void HeightListBox_JDate_co_il_MultiPage_Select(int index)
        {
            string xPathOfSelectedIndex = "//div[@id='second-row']/div[8]/ul/li[" + (index + 1).ToString() + "]";

            WaitUntilElementIsClickable(By.XPath(xPathOfSelectedIndex));
        }

        public void SaveDetailsButton_JDate_co_il_1Page_Click()
        {
            string xPathOfSelectedIndex = "//div[@class='save-details enable']";

            WaitUntilElementIsClickable(By.XPath(xPathOfSelectedIndex));

            // Sometimes if we do not wait for a second for the changes after they are saved, the 1-Page reappears when we navigate to the next page.
            Thread.Sleep(1000);
        }

        public void SaveDetailsButton_JDate_co_il_MultiPage_Click()
        {
            string xPathOfSelectedIndex = "//div[@id='send-attr-btn']";

            WaitUntilElementIsClickable(By.XPath(xPathOfSelectedIndex));
        }

        public void SubscriptionLink_JDate_co_il_1Page_Click()
        {
            string xPathOfSelectedIndex = "//a[@id='subscribe-btn']";

            WaitUntilElementIsClickable(By.XPath(xPathOfSelectedIndex));
        }

        public void YourAdjustmentsLink_JDate_co_il_1Page_Click()
        {
            string xPathOfSelectedIndex = "//section[2]/div/a";

            WaitUntilElementIsClickable(By.XPath(xPathOfSelectedIndex));
        }
        
        public void OnlineFriends_JDate_co_il_1Page_Click()
        {
            string xPathOfSelectedIndex = "//a[@id='A1']";

            if (driver.FindElements(By.XPath(xPathOfSelectedIndex)).Count == 0)
                Assert.Fail("At the JDate.co.il One Page Registration Page, we are done saving our registration values and we try to click on the Online Friends link to continue but it does not exist.");

            WaitUntilElementIsClickable(By.XPath(xPathOfSelectedIndex));
        }
    }
}
