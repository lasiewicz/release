﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.Events;
using NUnit.Framework;
using System.Threading;

namespace WebdriverAutomation.Framework.AdminTool_Parts
{
    public class Header : BasePage
    {
        public Header(IWebDriver driver)
            : base(driver)
        {
            this.driver = driver;
        }

        int loopCount = 5;

        #region Home Page
        public WebdriverAutomation.Framework.AdminTool_Pages.FreeTextApproval FreeTextApproval_EnglishLink_Click()
        {
            IWebElement header = WaitUntilElementExists(By.XPath("//html/body/div/form/div/div/div/table/tbody/tr/td"));
            header.Click();

            IWebElement subheader = WaitUntilElementExists(By.XPath("//table[@id='menu_2_0']/tbody/tr/td[2]"));
            subheader.Click();

            IWebElement language = WaitUntilElementExists(By.XPath("//table[@id='sub_menu_1_0']/tbody/tr/td[2]"));
            ClickUntilURLChanges(language);

            return new WebdriverAutomation.Framework.AdminTool_Pages.FreeTextApproval(driver);
        }

        public WebdriverAutomation.Framework.AdminTool_Pages.PhotoApproval PhotoApproval_JDatecomLink_Click()
        {
            IWebElement subheader = null;

            for (int i = 0; i < loopCount; i++)
            {
                IWebElement header = WaitUntilElementExists(By.XPath("//div[@id='member-search-nav']/ul/li[2]/a"));
                header.Click();

                Thread.Sleep(2000);

                subheader = WaitUntilElementExists(By.XPath("//div[@id='member-search-nav']/ul/li[2]/ul/li[8]/a"));

                if (subheader.Text != string.Empty)
                    break;

                if (i == (loopCount - 1))
                    Thread.Sleep(1);
            }

            ClickUntilURLChanges(subheader);

            return new WebdriverAutomation.Framework.AdminTool_Pages.PhotoApproval(driver);
        }

        public WebdriverAutomation.Framework.AdminTool_Pages.QuestionAnswerApproval QuestionAnswerApproval_EnglishLink_Click()
        {
            IWebElement subheader = null;

            for (int i = 0; i < loopCount; i++)
            {
                IWebElement header = WaitUntilElementExists(By.XPath("//div[@id='member-search-nav']/ul/li[3]/a"));
                header.Click();

                Thread.Sleep(2000);

                subheader = WaitUntilElementExists(By.XPath("//div[@id='member-search-nav']/ul/li[3]/ul/li[2]/a"));

                if (subheader.Text != string.Empty)
                    break;

                if (i == (loopCount - 1))
                    Thread.Sleep(1);
            }

            ClickUntilURLChanges(subheader);

            return new WebdriverAutomation.Framework.AdminTool_Pages.QuestionAnswerApproval(driver);
        }

        public WebdriverAutomation.Framework.AdminTool_Pages.CreditTool CreditTool_Click()
        {
            for (int i = 0; i < loopCount; i++)
            {
                IWebElement header = WaitUntilElementExists(By.XPath("//div[@id='member-search-nav']/ul/li[4]/a"));
                header.Click();

                // this should open up a new window
                List<string> handlers = new List<string>();

                foreach (string handler in driver.WindowHandles)
                    handlers.Add(handler);

                if (handlers.Count > 1)
                    break;

                if (i == (loopCount - 1))
                    Thread.Sleep(1);
            }

            return new WebdriverAutomation.Framework.AdminTool_Pages.CreditTool(driver);
        }

        public void MessageBoards_JDate_Click()
        {
            IWebElement subheader = null;

            for (int i = 0; i < loopCount; i++)
            {
                IWebElement header = WaitUntilElementExists(By.XPath("//div[@id='member-search-nav']/ul/li[5]/a"));
                header.Click();

                Thread.Sleep(2000);

                subheader = WaitUntilElementExists(By.XPath("//div[@id='member-search-nav']/ul/li[5]/ul/li[4]/a"));

                if (subheader.Text != string.Empty)
                {
                    subheader.Click();

                    // this should open up a new window
                    List<string> handlers = new List<string>();

                    foreach (string handler in driver.WindowHandles)
                        handlers.Add(handler);

                    if (handlers.Count > 1)
                        break;
                }              

                if (i == (loopCount - 1))
                    Thread.Sleep(1);
            }

        }

        public void Reports_AdminActionLog_Click()
        {
            IWebElement subheader = null;

            for (int i = 0; i < loopCount; i++)
            {
                IWebElement header = WaitUntilElementExists(By.XPath("//div[@id='member-search-nav']/ul/li[6]/a"));
                header.Click();

                Thread.Sleep(2000);

                subheader = WaitUntilElementExists(By.XPath("//div[@id='member-search-nav']/ul/li[6]/ul/li[1]/a"));

                if (subheader.Text != string.Empty)
                {
                    subheader.Click();

                    // this should open up a new window
                    List<string> handlers = new List<string>();

                    foreach (string handler in driver.WindowHandles)
                        handlers.Add(handler);

                    if (handlers.Count > 1)
                        break;
                }

                if (i == (loopCount - 1))
                    Thread.Sleep(1);
            }

            // Currently there is a bug and sometimes the Registration Page appears instead of the Login Page
            if (driver.FindElements(By.XPath("//div[@id='min-max-container']/div[@id='content-container']/div[@id='content-main']/div[1]/h1")).Count > 0 &&
                driver.FindElement(By.XPath("//div[@id='min-max-container']/div[@id='content-container']/div[@id='content-main']/div[1]/h1")).Text.Contains("Create Your Profile"))
            {
                WebdriverAutomation.Framework.Site_Pages.Registration registration = new WebdriverAutomation.Framework.Site_Pages.Registration(driver);
                registration.MemberLoginButton_Click();
            }

        }

        public WebdriverAutomation.Framework.AdminTool_Pages.ApprovalCounts Reports_ApprovalCounts_Click()
        {
            IWebElement subheader = null;

            for (int i = 0; i < loopCount; i++)
            {
                IWebElement header = WaitUntilElementExists(By.XPath("//div[@id='member-search-nav']/ul/li[6]/a"));
                header.Click();

                Thread.Sleep(2000);

                subheader = WaitUntilElementExists(By.XPath("//div[@id='member-search-nav']/ul/li[6]/ul/li[2]/a"));

                if (subheader.Text != string.Empty)
                    break;

                if (i == (loopCount - 1))
                    Thread.Sleep(1);
            }

            ClickUntilURLChanges(subheader);

            return new WebdriverAutomation.Framework.AdminTool_Pages.ApprovalCounts(driver);
        }

        public WebdriverAutomation.Framework.AdminTool_Pages.ViewQueueCounts Reports_QueueStats_Click()
        {
            IWebElement subheader = null;

            for (int i = 0; i < loopCount; i++)
            {
                IWebElement header = WaitUntilElementExists(By.XPath("//div[@id='member-search-nav']/ul/li[6]/a"));
                header.Click();

                Thread.Sleep(2000);

                subheader = WaitUntilElementExists(By.XPath("//div[@id='member-search-nav']/ul/li[6]/ul/li[3]/a"));

                if (subheader.Text != string.Empty)
                    break;

                if (i == (loopCount - 1))
                    Thread.Sleep(1);
            }

            ClickUntilURLChanges(subheader);

            return new WebdriverAutomation.Framework.AdminTool_Pages.ViewQueueCounts(driver);
        }

        public WebdriverAutomation.Framework.AdminTool_Pages.BulkMailBatchReport Reports_BulkMailBatch_Click()
        {
            IWebElement subheader = null;

            for (int i = 0; i < loopCount; i++)
            {
                IWebElement header = WaitUntilElementExists(By.XPath("//div[@id='member-search-nav']/ul/li[6]/a"));
                header.Click();

                Thread.Sleep(2000);

                subheader = WaitUntilElementExists(By.XPath("//div[@id='member-search-nav']/ul/li[6]/ul/li[4]/a"));

                if (subheader.Text != string.Empty)
                    break;

                if (i == (loopCount - 1))
                    Thread.Sleep(1);
            }

            ClickUntilURLChanges(subheader);

            return new WebdriverAutomation.Framework.AdminTool_Pages.BulkMailBatchReport(driver);
        }

        public WebdriverAutomation.Framework.AdminTool_Pages.PhotosQueue Reports_PhotosQueue_Click()
        {
            IWebElement subheader = null;

            for (int i = 0; i < loopCount; i++)
            {
                IWebElement header = WaitUntilElementExists(By.XPath("//div[@id='member-search-nav']/ul/li[6]/a"));
                header.Click();

                Thread.Sleep(2000);

                subheader = WaitUntilElementExists(By.XPath("//div[@id='member-search-nav']/ul/li[6]/ul/li[5]/a"));

                if (subheader.Text != string.Empty)
                    break;

                if (i == (loopCount - 1))
                    Thread.Sleep(1);
            }

            ClickUntilURLChanges(subheader);

            return new WebdriverAutomation.Framework.AdminTool_Pages.PhotosQueue(driver);
        }

        public WebdriverAutomation.Framework.AdminTool_Pages.PromoSearch SubscriptionAdmin_PromoSearch_Click()
        {
            IWebElement subheader = null;

            for (int i = 0; i < loopCount; i++)
            {
                IWebElement header = WaitUntilElementExists(By.XPath("//div[@id='member-search-nav']/ul/li[7]/a"));
                header.Click();

                Thread.Sleep(2000);

                subheader = WaitUntilElementExists(By.XPath("//div[@id='member-search-nav']/ul/li[7]/ul/li[1]/a"));

                if (subheader.Text != string.Empty)
                    break;

                if (i == (loopCount - 1))
                    Thread.Sleep(1);
            }

            ClickUntilURLChanges(subheader);

            return new WebdriverAutomation.Framework.AdminTool_Pages.PromoSearch(driver);
        }

        public void SubscriptionAdmin_SubAdminToolLegacy_Click()
        {
            IWebElement subheader = null;

            for (int i = 0; i < loopCount; i++)
            {
                IWebElement header = WaitUntilElementExists(By.XPath("//div[@id='member-search-nav']/ul/li[7]/a"));
                header.Click();

                Thread.Sleep(2000);

                subheader = WaitUntilElementExists(By.XPath("//div[@id='member-search-nav']/ul/li[7]/ul/li[2]/a"));

                if (subheader.Text != string.Empty)
                {
                    subheader.Click();

                    // this should open up a new window
                    List<string> handlers = new List<string>();

                    foreach (string handler in driver.WindowHandles)
                        handlers.Add(handler);

                    if (handlers.Count > 1)
                        break;
                }

                if (i == (loopCount - 1))
                    Thread.Sleep(1);
            }

            // Currently there is a bug and sometimes the Registration Page appears instead of the Login Page
            if (driver.FindElements(By.XPath("//div[@id='min-max-container']/div[@id='content-container']/div[@id='content-main']/div[1]/h1")).Count > 0 &&
                driver.FindElement(By.XPath("//div[@id='min-max-container']/div[@id='content-container']/div[@id='content-main']/div[1]/h1")).Text.Contains("Create Your Profile"))
            {
                WebdriverAutomation.Framework.Site_Pages.Registration registration = new WebdriverAutomation.Framework.Site_Pages.Registration(driver);
                registration.MemberLoginButton_Click();
            }

        }

        public WebdriverAutomation.Framework.AdminTool_Pages.CreatePlan SubscriptionAdmin_CreatePlan_Click()
        {
            IWebElement subheader = null;

            for (int i = 0; i < loopCount; i++)
            {
                IWebElement header = WaitUntilElementExists(By.XPath("//div[@id='member-search-nav']/ul/li[7]/a"));
                header.Click();

                Thread.Sleep(1000);

                subheader = WaitUntilElementExists(By.XPath("//div[@id='member-search-nav']/ul/li[7]/ul/li[3]/a"));

                if (subheader.Text != string.Empty)
                    break;

                if (i == (loopCount - 1))
                    Thread.Sleep(1);
            }

            ClickUntilURLChanges(subheader);

            return new WebdriverAutomation.Framework.AdminTool_Pages.CreatePlan(driver);
        }

        public WebdriverAutomation.Framework.AdminTool_Pages.DNESearch DNE_Search_Click()
        {
            IWebElement subheader = null;

            for (int i = 0; i < loopCount; i++)
            {
                IWebElement header = WaitUntilElementExists(By.XPath("//div[@id='member-search-nav']/ul/li[8]/a"));
                header.Click();

                Thread.Sleep(2000);

                subheader = WaitUntilElementExists(By.XPath("//div[@id='member-search-nav']/ul/li[8]/ul/li[1]/a"));

                if (subheader.Text != string.Empty)
                    break;

                if (i == (loopCount - 1))
                    Thread.Sleep(1);
            }

            ClickUntilURLChanges(subheader);

            return new WebdriverAutomation.Framework.AdminTool_Pages.DNESearch(driver);
        }

        public WebdriverAutomation.Framework.AdminTool_Pages.DNEDownload DNE_Download_Click()
        {
            IWebElement subheader = null;

            for (int i = 0; i < loopCount; i++)
            {
                IWebElement header = WaitUntilElementExists(By.XPath("//div[@id='member-search-nav']/ul/li[8]/a"));
                header.Click();

                Thread.Sleep(2000);

                subheader = WaitUntilElementExists(By.XPath("//div[@id='member-search-nav']/ul/li[8]/ul/li[2]/a"));

                if (subheader.Text != string.Empty)
                    break;

                if (i == (loopCount - 1))
                    Thread.Sleep(1);
            }

            ClickUntilURLChanges(subheader);

            return new WebdriverAutomation.Framework.AdminTool_Pages.DNEDownload(driver);
        }

        public void DNE_Upload_Click()
        {
            IWebElement subheader = null;

            for (int i = 0; i < loopCount; i++)
            {
                IWebElement header = WaitUntilElementExists(By.XPath("//div[@id='member-search-nav']/ul/li[8]/a"));
                header.Click();

                Thread.Sleep(2000);

                subheader = WaitUntilElementExists(By.XPath("//div[@id='member-search-nav']/ul/li[8]/ul/li[3]/a"));

                if (subheader.Text != string.Empty)
                {
                    subheader.Click();

                    // this should open up a new window
                    List<string> handlers = new List<string>();

                    foreach (string handler in driver.WindowHandles)
                        handlers.Add(handler);

                    if (handlers.Count > 1)
                        break;
                }

                if (i == (loopCount - 1))
                    Thread.Sleep(1);
            }

        }

        #endregion

    }
}
