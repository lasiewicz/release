﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.Events;
using NUnit.Framework;

namespace WebdriverAutomation.Framework.Site_Parts
{
    public class Footer : BasePage
    {
        public Footer(IWebDriver driver)
            : base(driver)
        {
            this.driver = driver;

        }

        internal IWebElement homeLink = null;
        internal IWebElement loginLink = null;
        internal IWebElement logoutLink = null;
        internal IWebElement contactUsLink = null;
        internal IWebElement yourAccountLink = null;
        internal IWebElement verifyEmailLink = null;
        internal IWebElement subscribeLink = null;
        internal IWebElement aboutJDateLink = null;
        internal IWebElement giftOfJDateLink = null;
        internal IWebElement videoLink = null;
        internal IWebElement jMagLink = null;
        internal IWebElement jBlogLink = null;
        internal IWebElement travelAndEventsLink = null;
        internal IWebElement jDateMobileLink = null;
        internal IWebElement jewishHolidayCalendarLink = null;
        internal IWebElement synagogueDirectoryLink = null;
        internal IWebElement ourMissionLink = null;
        internal IWebElement safetyLink = null;
        internal IWebElement helpAndAdviceLink = null;
        internal IWebElement siteMapLink = null;
        internal IWebElement aboutSparkNetworksLink = null;
        internal IWebElement sparkNetworksSitesLink = null;
        internal IWebElement advertiseWithUsLink = null;
        internal IWebElement affiliateProgramLink = null;
        internal IWebElement investorRelationsLink = null;
        internal IWebElement jobsLink = null;
        internal IWebElement privacyLink = null;
        internal IWebElement termsOfServiceLink = null;
        internal IWebElement localOnlineDatingLink = null;
        internal IWebElement jewishSinglesLink = null;
        internal IWebElement ourIntellectualPropertyLink = null;

        public WebdriverAutomation.Framework.Site_Pages.Splash HomeLink_Click()
        {
            homeLink = WaitUntilElementExists(By.XPath("//div[contains(@id, 'footer')]/ul[1]/li[1]/a"));
            ClickUntilURLChanges(homeLink);

            return new WebdriverAutomation.Framework.Site_Pages.Splash(driver);
        }

        public WebdriverAutomation.Framework.Site_Pages.Login LoginLink_Click()
        {
            loginLink = WaitUntilElementExists(By.XPath("//div[contains(@id, 'footer')]/ul[1]/li[2]/a"));
            ClickUntilURLChanges(loginLink);

            return new WebdriverAutomation.Framework.Site_Pages.Login(driver);
        }

        public WebdriverAutomation.Framework.Site_Pages.Login LogoutLink_Click()
        {
            if (driver.Url.Contains("jdate.com"))
                logoutLink = WaitUntilElementExists(By.XPath("//div[@id='footer']/div[@id='footer-container']/ul[1]/li[2]/a[contains(@id, 'LogonBox1_changeStatusLink')]"));
            else if (driver.Url.Contains("spark.com"))
                logoutLink = WaitUntilElementExists(By.XPath("//div[@id='footer-columns']/ul[1]/li[3]/a[contains(@id, 'LogonBox3_changeStatusLink')]"));
            else if (driver.Url.Contains("blacksingles.com"))
                logoutLink = WaitUntilElementExists(By.XPath("//a[contains(@id, 'Footer20_LogonBox1_changeStatusLink')]"));
            else if (driver.Url.Contains("bbwpersonalsplus.com"))
                logoutLink = WaitUntilElementExists(By.XPath("//a[contains(@id, 'Footer20_LogonBox1_changeStatusLink')]"));
            else if (driver.Url.Contains("jdate.co.il"))
                logoutLink = WaitUntilElementExists(By.XPath("//a[contains(@id, 'Footer20_LogonBox2_changeStatusLink')]"));

            ClickUntilURLChanges(logoutLink);

            return new WebdriverAutomation.Framework.Site_Pages.Login(driver);
        }

        public bool IsLogoutLinkVisible()
        {
            try
            {
                if (driver.Url.Contains("jdate.com"))
                    logoutLink = WaitUntilElementExists(By.XPath("//div[@id='footer']/div[@id='footer-container']/ul[1]/li[2]/a[contains(@id, 'LogonBox1_changeStatusLink')]"));
                else if (driver.Url.Contains("spark.com"))
                    logoutLink = WaitUntilElementExists(By.XPath("//div[@id='footer-columns']/ul[1]/li[3]/a[contains(@id, 'LogonBox3_changeStatusLink')]"));
                else if (driver.Url.Contains("blacksingles.com"))
                    logoutLink = WaitUntilElementExists(By.XPath("//a[contains(@id, 'Footer20_LogonBox1_changeStatusLink')]"));
                else if (driver.Url.Contains("bbwpersonalsplus.com"))
                    logoutLink = WaitUntilElementExists(By.XPath("//a[contains(@id, 'Footer20_LogonBox1_changeStatusLink')]"));
                else if (driver.Url.Contains("jdate.co.il"))
                    logoutLink = WaitUntilElementExists(By.XPath("//a[contains(@id, 'Footer20_LogonBox2_changeStatusLink')]"));
            }
            catch
            {
                return false;
            }

            if (logoutLink.Displayed && logoutLink.Text == "Logout")
                return true;

            return false;
        }

        public WebdriverAutomation.Framework.Site_Pages.ContactUs ContactUsLink_Click()
        {
            contactUsLink = WaitUntilElementExists(By.XPath("//div[contains(@id, 'footer')]/ul[1]/li[3]/a"));
            ClickUntilURLChanges(contactUsLink);

            return new WebdriverAutomation.Framework.Site_Pages.ContactUs(driver);
        }

        public WebdriverAutomation.Framework.Site_Pages.Registration YourAccountLink_LoggedOut_Click()
        {
            yourAccountLink = WaitUntilElementExists(By.XPath("//div[contains(@id, 'footer')]/ul[1]/li[4]/a"));
            ClickUntilURLChanges(yourAccountLink);

            return new WebdriverAutomation.Framework.Site_Pages.Registration(driver);
        }

        public WebdriverAutomation.Framework.Site_Pages.YourAccount YourAccountLink_LoggedIn_Click()
        {
            yourAccountLink = WaitUntilElementExists(By.XPath("//div[contains(@id, 'footer')]/ul[1]/li[4]/a"));
            ClickUntilURLChanges(yourAccountLink);

            return new WebdriverAutomation.Framework.Site_Pages.YourAccount(driver);
        }

        public WebdriverAutomation.Framework.Site_Pages.Registration VerifyEmailLink_LoggedOut_Click()
        {
            verifyEmailLink = WaitUntilElementExists(By.XPath("//div[contains(@id, 'footer')]/ul[1]/li[5]/a"));
            ClickUntilURLChanges(verifyEmailLink);

            return new WebdriverAutomation.Framework.Site_Pages.Registration(driver);
        }

        public WebdriverAutomation.Framework.Site_Pages.VerifyEmail VerifyEmailLink_LoggedIn_Click()
        {
            verifyEmailLink = WaitUntilElementExists(By.XPath("//div[contains(@id, 'footer')]/ul[1]/li[5]/a"));
            ClickUntilURLChanges(verifyEmailLink);

            return new WebdriverAutomation.Framework.Site_Pages.VerifyEmail(driver);
        }

        public WebdriverAutomation.Framework.Site_Pages.Registration SubscribeLink_LoggedOut_Click()
        {
            subscribeLink = WaitUntilElementExists(By.XPath("//div[contains(@id, 'footer')]/ul[1]/li[6]/a"));
            ClickUntilURLChanges(subscribeLink);

            return new WebdriverAutomation.Framework.Site_Pages.Registration(driver);
        }

        public WebdriverAutomation.Framework.Site_Pages.Subscribe SubscribeLink_LoggedIn_Click()
        {
            subscribeLink = WaitUntilElementExists(By.XPath("//div[contains(@id, 'footer')]/ul[1]/li[6]/a"));
            ClickUntilURLChanges(subscribeLink);

            return new WebdriverAutomation.Framework.Site_Pages.Subscribe(driver);
        }

        public WebdriverAutomation.Framework.Site_Pages.AboutJDate AboutJDateLink_Click()
        {
            aboutJDateLink = WaitUntilElementExists(By.XPath("//div[contains(@id, 'footer')]/ul[2]/li[1]/a"));
            ClickUntilURLChanges(aboutJDateLink);

            return new WebdriverAutomation.Framework.Site_Pages.AboutJDate(driver);
        }

        public WebdriverAutomation.Framework.Site_Pages.GiftOfJDate GiftOfJDateLink_Click()
        {
            giftOfJDateLink = WaitUntilElementExists(By.XPath("//div[contains(@id, 'footer')]/ul[2]/li[2]/a"));
            ClickUntilURLChanges(giftOfJDateLink);

            return new WebdriverAutomation.Framework.Site_Pages.GiftOfJDate(driver);
        }

        public WebdriverAutomation.Framework.Site_Pages.Video VideoLink_Click()
        {
            videoLink = WaitUntilElementExists(By.XPath("//div[contains(@id, 'footer')]/ul[2]/li[3]/a"));
            ClickUntilURLChanges(videoLink);

            return new WebdriverAutomation.Framework.Site_Pages.Video(driver);
        }

        public WebdriverAutomation.Framework.Site_Pages.JMag JMagLink_Click()
        {
            jMagLink = WaitUntilElementExists(By.XPath("//div[contains(@id, 'footer')]/ul[2]/li[4]/a"));
            ClickUntilURLChanges(jMagLink);

            return new WebdriverAutomation.Framework.Site_Pages.JMag(driver);
        }

        public WebdriverAutomation.Framework.Site_Pages.JBlog JBlogLink_Click()
        {
            jBlogLink = WaitUntilElementExists(By.XPath("//div[contains(@id, 'footer')]/ul[2]/li[5]/a"));
            ClickUntilURLChanges(jBlogLink);

            return new WebdriverAutomation.Framework.Site_Pages.JBlog(driver);
        }

        public WebdriverAutomation.Framework.Site_Pages.TravelAndEvents TravelAndEventsLink_Click()
        {
            travelAndEventsLink = WaitUntilElementExists(By.XPath("//div[contains(@id, 'footer')]/ul[2]/li[6]/a"));
            ClickUntilURLChanges(travelAndEventsLink);

            return new WebdriverAutomation.Framework.Site_Pages.TravelAndEvents(driver);
        }

        public WebdriverAutomation.Framework.Site_Pages.Registration JDateMobileLink_LoggedOut_Click()
        {
            jDateMobileLink = WaitUntilElementExists(By.XPath("//div[contains(@id, 'footer')]/ul[2]/li[7]/a"));
            ClickUntilURLChanges(jDateMobileLink);

            return new WebdriverAutomation.Framework.Site_Pages.Registration(driver);
        }

        public WebdriverAutomation.Framework.Site_Pages.JDateMobile JDateMobileLink_LoggedIn_Click()
        {
            jDateMobileLink = WaitUntilElementExists(By.XPath("//div[contains(@id, 'footer')]/ul[2]/li[7]/a"));
            ClickUntilURLChanges(jDateMobileLink);

            return new WebdriverAutomation.Framework.Site_Pages.JDateMobile(driver);
        }

        public WebdriverAutomation.Framework.Site_Pages.JewishHolidayCalendar JewishHolidayCalendarLink_Click()
        {
            jewishHolidayCalendarLink = WaitUntilElementExists(By.XPath("//div[contains(@id, 'footer')]/ul[2]/li[8]/a"));
            ClickUntilURLChanges(jewishHolidayCalendarLink);

            return new WebdriverAutomation.Framework.Site_Pages.JewishHolidayCalendar(driver);
        }

        public WebdriverAutomation.Framework.Site_Pages.SynagogueDirectory SynagogueDirectoryLink_Click()
        {
            synagogueDirectoryLink = WaitUntilElementExists(By.XPath("//div[contains(@id, 'footer')]/ul[2]/li[9]/a"));
            ClickUntilURLChanges(synagogueDirectoryLink);

            return new WebdriverAutomation.Framework.Site_Pages.SynagogueDirectory(driver);
        }

        public WebdriverAutomation.Framework.Site_Pages.OurMission OurMissionLink_Click()
        {
            ourMissionLink = WaitUntilElementExists(By.XPath("//div[contains(@id, 'footer')]/ul[2]/li[10]/a"));
            ClickUntilURLChanges(ourMissionLink);

            return new WebdriverAutomation.Framework.Site_Pages.OurMission(driver);
        }

        public WebdriverAutomation.Framework.Site_Pages.Safety SafetyLink_Click()
        {
            safetyLink = WaitUntilElementExists(By.XPath("//div[contains(@id, 'footer')]/ul[2]/li[11]/a"));
            safetyLink.Click();

            return new WebdriverAutomation.Framework.Site_Pages.Safety(driver);
        }

        public WebdriverAutomation.Framework.Site_Pages.HelpAndAdvice HelpAndAdviceLink_Click()
        {
            helpAndAdviceLink = WaitUntilElementExists(By.XPath("//div[contains(@id, 'footer')]/ul[2]/li[12]/a"));
            ClickUntilURLChanges(helpAndAdviceLink);

            return new WebdriverAutomation.Framework.Site_Pages.HelpAndAdvice(driver);
        }

        public WebdriverAutomation.Framework.Site_Pages.SiteMap SiteMapLink_Click()
        {
            siteMapLink = WaitUntilElementExists(By.XPath("//div[contains(@id, 'footer')]/ul[2]/li[13]/a"));
            ClickUntilURLChanges(siteMapLink);

            return new WebdriverAutomation.Framework.Site_Pages.SiteMap(driver);
        }

        public WebdriverAutomation.Framework.Site_Pages.JDateSuccess JDateSuccessLink_Click()
        {
            siteMapLink = WaitUntilElementExists(By.XPath("//div[contains(@id, 'footer')]/ul[2]/li[14]/a"));
            ClickUntilURLChanges(siteMapLink);

            return new WebdriverAutomation.Framework.Site_Pages.JDateSuccess(driver);
        }

        public WebdriverAutomation.Framework.Site_Pages.AboutSparkNetworks AboutSparkNetworksLink_Click()
        {
            aboutSparkNetworksLink = WaitUntilElementExists(By.XPath("//div[contains(@id, 'footer')]/ul[2]/li[15]/a"));
            ClickUntilURLChanges(aboutSparkNetworksLink);

            return new WebdriverAutomation.Framework.Site_Pages.AboutSparkNetworks(driver);
        }

        public WebdriverAutomation.Framework.Site_Pages.SparkNetworksSites SparkNetworksSitesLink_Click()
        {
            sparkNetworksSitesLink = WaitUntilElementExists(By.XPath("//div[contains(@id, 'footer')]/ul[2]/li[16]/a"));
            ClickUntilURLChanges(sparkNetworksSitesLink);

            return new WebdriverAutomation.Framework.Site_Pages.SparkNetworksSites(driver);
        }

        public WebdriverAutomation.Framework.Site_Pages.AdvertiseWithUs AdvertiseWithUsLink_Click()
        {
            advertiseWithUsLink = WaitUntilElementExists(By.XPath("//div[contains(@id, 'footer')]/ul[2]/li[17]/a"));
            ClickUntilURLChanges(advertiseWithUsLink);

            return new WebdriverAutomation.Framework.Site_Pages.AdvertiseWithUs(driver);
        }

        public WebdriverAutomation.Framework.Site_Pages.AffiliateProgram AffiliateProgramLink_Click()
        {
            affiliateProgramLink = WaitUntilElementExists(By.XPath("//div[contains(@id, 'footer')]/ul[2]/li[18]/a"));
            ClickUntilURLChanges(affiliateProgramLink);

            return new WebdriverAutomation.Framework.Site_Pages.AffiliateProgram(driver);
        }

        public WebdriverAutomation.Framework.Site_Pages.InvestorRelations InvestorRelationsLink_Click()
        {
            investorRelationsLink = WaitUntilElementExists(By.XPath("//div[contains(@id, 'footer')]/ul[2]/li[19]/a"));
            ClickUntilURLChanges(investorRelationsLink);

            return new WebdriverAutomation.Framework.Site_Pages.InvestorRelations(driver);
        }

        public WebdriverAutomation.Framework.Site_Pages.Jobs JobsLink_Click()
        {
            jobsLink = WaitUntilElementExists(By.XPath("//div[contains(@id, 'footer')]/ul[2]/li[20]/a"));
            ClickUntilURLChanges(jobsLink);

            return new WebdriverAutomation.Framework.Site_Pages.Jobs(driver);
        }

        public WebdriverAutomation.Framework.Site_Pages.Privacy PrivacyLink_Click()
        {
            privacyLink = WaitUntilElementExists(By.XPath("//div[contains(@id, 'footer')]/ul[2]/li[21]/a"));
            ClickUntilURLChanges(privacyLink);

            return new WebdriverAutomation.Framework.Site_Pages.Privacy(driver);
        }

        public WebdriverAutomation.Framework.Site_Pages.TermsOfService TermsOfServiceLink_Click()
        {
            termsOfServiceLink = WaitUntilElementExists(By.XPath("//div[contains(@id, 'footer')]/ul[2]/li[22]/a"));
            ClickUntilURLChanges(termsOfServiceLink);

            return new WebdriverAutomation.Framework.Site_Pages.TermsOfService(driver);
        }

        public WebdriverAutomation.Framework.Site_Pages.LocalOnlineDating LocalOnlineDatingLink_Click()
        {
            localOnlineDatingLink = WaitUntilElementExists(By.XPath("//div[contains(@id, 'footer')]/ul[2]/li[23]/a"));
            ClickUntilURLChanges(localOnlineDatingLink);

            return new WebdriverAutomation.Framework.Site_Pages.LocalOnlineDating(driver);
        }

        public WebdriverAutomation.Framework.Site_Pages.JewishSingles JewishSinglesLink_Click()
        {
            jewishSinglesLink = WaitUntilElementExists(By.XPath("//div[contains(@id, 'footer')]/ul[2]/li[24]/a"));
            jewishSinglesLink.Click();

            return new WebdriverAutomation.Framework.Site_Pages.JewishSingles(driver);
        }

        public WebdriverAutomation.Framework.Site_Pages.OurIntellectualProperty OurIntellectualPropertyLink_Click()
        {
            ourIntellectualPropertyLink = WaitUntilElementExists(By.XPath("//div[contains(@id, 'footer')]/ul[2]/li[25]/a"));
            ClickUntilURLChanges(ourIntellectualPropertyLink);

            return new WebdriverAutomation.Framework.Site_Pages.OurIntellectualProperty(driver);
        }

    }
}
