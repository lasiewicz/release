﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.Events;
using NUnit.Framework;
using System.Threading;
// the below 3 are used as a workaround for hovering over the Header before clicking on subheaders because .hover was deprecated
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Interactions.Internal;
using System.Drawing;

namespace WebdriverAutomation.Framework.Site_Parts
{
    public class Header : BasePage
    {
        public Header(IWebDriver driver)
            : base(driver)
        {
            this.driver = driver;

        }

        internal IWebElement JDateIcon = null;
        internal IWebElement yourAccountLink = null;

        #region Top Header - JDate icon, Welcome username, Your Account, Help, Events, Logout
        public WebdriverAutomation.Framework.Site_Pages.Home JDateIcon_Click()
        {
            JDateIcon = WaitUntilElementExists(By.XPath("//img[contains(@id, 'topAuxNav_imgLogoNo')]"));
            ClickUntilURLChanges(JDateIcon);

            return new WebdriverAutomation.Framework.Site_Pages.Home(driver);
        }

        public WebdriverAutomation.Framework.Site_Pages.YourAccount YourAccountLink_Click()
        {
            if (driver.Url.Contains("jdate.co.il"))
                yourAccountLink = WaitUntilElementExists(By.XPath("//li[@id='_ctl0_topAuxNav_rptMenuSocialNav__ctl1_liMenu']/a"));
            else if (driver.FindElements(By.XPath("//li[@id='_ctl0_topAuxNav_rptMenuSocialNav__ctl0_liMenu']/a")).Count > 0)
            {
                yourAccountLink = WaitUntilElementExists(By.XPath("//li[@id='_ctl0_topAuxNav_rptMenuSocialNav__ctl0_liMenu']/a"));

                // 8/1/11 JDATE ONLY: Clicking on Your Account was taking the user 10% of the time to Profile Display Settings page instead. Haven't seen it since but keeping this here plus the breakpoint so if it happens again we can debug it easier
                if (yourAccountLink.Text != "Your Account" && yourAccountLink.Text != "Member Services")
                    yourAccountLink.Click();              // breakpoint - our link is not Account Settings
            }
            // JDate Chat & IM Settings page
            else
                yourAccountLink = WaitUntilElementExists(By.XPath("//li[@id='topAuxNavMenu_rptMenuSocialNav__ctl0_liMenu']/a"));

            ClickUntilURLChanges(yourAccountLink);

            return new WebdriverAutomation.Framework.Site_Pages.YourAccount(driver);
        }

        public WebdriverAutomation.Framework.Site_Pages.Login Logout_Click()
        {
            IWebElement element = null;

            if (driver.Url.Contains("jdate.co.il"))
                element = WaitUntilElementExists(By.XPath("//li[contains(@id, 'topAuxNav_rptMenuSocialNav__ctl5_liMenu')]/a"));
            else
                element = WaitUntilElementExists(By.XPath("//li[contains(@id, 'topAuxNav_rptMenuSocialNav__ctl4_liMenu')]/a"));

            ClickUntilURLChanges(element);

            return new WebdriverAutomation.Framework.Site_Pages.Login(driver);
        }
        #endregion

        #region Main Header
        public Framework.Site_Pages.Home HomeHeaderLink_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//li[contains(@id, 'Header__ctl0_rptMenu__ctl0_liMenu')]/a"));

            // for some strange reason sometimes when we try to click on Your Profile in the header the Your Profile dropdown gets highlighted but the link doesn't
            //    get clicked, especially on JDate.  if we fail, just go straight to the Your Profile Link
            try
            {
                ClickUntilURLChanges(element);
            }
            catch
            {
                if (driver.Url.Contains("jdate.com"))
                {
                    if (driver.Url.Contains("preprod"))
                        driver.Navigate().GoToUrl("http://preprod.jdate.com/Applications/Home/Default.aspx");
                    else
                        driver.Navigate().GoToUrl("http://www.jdate.com/Applications/Home/Default.aspx");
                }
                else if (driver.Url.Contains("spark.com"))
                {
                    if (driver.Url.Contains("preprod"))
                        driver.Navigate().GoToUrl("http://preprod.spark.com/Applications/Home/default.aspx?NavPoint=top");
                    else
                        driver.Navigate().GoToUrl("http://www.spark.com/Applications/Home/default.aspx?NavPoint=top");
                }
                else if (driver.Url.Contains("blacksingles.com"))
                {
                    if (driver.Url.Contains("preprod"))
                        driver.Navigate().GoToUrl("http://preprod.blacksingles.com/Applications/Home/Default.aspx");
                    else
                        driver.Navigate().GoToUrl("http://www.blacksingles.com/Applications/Home/Default.aspx");
                }
                else if (driver.Url.Contains("bbwpersonalsplus.com"))
                {
                    if (driver.Url.Contains("preprod"))
                        driver.Navigate().GoToUrl("http://preprod.bbwpersonalsplus.com/Applications/Home/Default.aspx");
                    else
                        driver.Navigate().GoToUrl("http://www.bbwpersonalsplus.com/Applications/Home/Default.aspx");
                }
                else if (driver.Url.Contains("jdate.co.il"))
                {
                    if (driver.Url.Contains("preprod"))
                        driver.Navigate().GoToUrl("http://preprod.jdate.co.il/Applications/Home/Default.aspx");
                    else
                        driver.Navigate().GoToUrl("http://www.jdate.co.il/Applications/Home/Default.aspx");
                }
            }

            return new Framework.Site_Pages.Home(driver);
        }

        /// <summary>
        /// JDate: For registered users who are not subscribed, clicking on the Inbox link in the header should take them to the Subscribe payment page instead 
        /// of their Inbox.
        /// </summary>
        /// <returns></returns>
        public Framework.Site_Pages.Subscribe InboxHeaderLink_Click_JDateNonSubscribedUser()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//li[contains(@id, 'rptMenu__ctl1_liMenu')]/a"));
            ClickUntilURLChanges(element);

            return new Framework.Site_Pages.Subscribe(driver);
        }

        /// <summary>
        /// JDate:  For suscribed users and registered users who are recipients of All Access messages, clicking on the Inbox link in the header 
        /// should take them to their Inbox instead of the Subscribe page
        /// 
        /// All other non JDate sites:  Clicking on the Inbox header should bring the user to the Inbox page.
        /// </summary>
        /// <returns></returns>
        public Framework.Site_Pages.Messages InboxHeaderLink_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//li[contains(@id, 'rptMenu__ctl1_liMenu')]/a"));
            ClickUntilURLChanges(element);

            return new Framework.Site_Pages.Messages(driver);
        }

        /// <summary>
        /// Registered (non subscribed) users who click on the Inbox end up in the Subscribe page but if you have an All Access email waiting you end up on the 
        /// Inbox page.
        /// If we do not know if we have an All Access email use this method.  
        /// The string returned will be the page we land on and we can instantiate our new Page in our test case immediately afterwards.
        /// </summary>
        /// <returns></returns>
        public string InboxHeaderLink_Click_ForJDateNonSubscribedUsersWhoDoNotKnowIfThereAreAllAccessMessagesWaiting()
        {
            IWebElement element;
            string textThatVerifiesThisPage = string.Empty;

            element = WaitUntilElementExists(By.XPath("//li[contains(@id, 'rptMenu__ctl1_liMenu')]/a"));
            ClickUntilURLChanges(element);

            // if we end up on the Subscribe page it might take some time for it to redirect so sleep first
            Thread.Sleep(3000);

            // test to see if we land on the Subscribe Page
            try
            {
                if (driver.Url.Contains("secure.spark.net"))
                    return "Subscribe";
            }
            catch { }

            // test to see if we land on the Inbox Page
            try
            {
                if (driver.Url.Contains("jdate.co.il"))
                    textThatVerifiesThisPage = "דואר נכנס";
                else
                    textThatVerifiesThisPage = "Messages";

                element = WaitUntilElementExists(By.XPath("//span[contains(@id, 'txtMailBox')]"));

                if (element.Text == textThatVerifiesThisPage)
                    return "Messages";
            }
            catch { }

            return "another page";
        }

        public Framework.Site_Pages.QuickSearch SearchHeaderLink_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//li[contains(@id, 'rptMenu__ctl3_liMenu')]/a"));
            ClickUntilURLChanges(element);

            return new Framework.Site_Pages.QuickSearch(driver);
        }

        /// <summary>
        /// Hover doesn't seem to be implemented for Webdriver so as a workaround we're just going straight to the correct link.
        /// </summary>
        /// <returns></returns>
        public Framework.Site_Pages.LookUpMember LookUpMemberSubHeaderLink_Click()
        {
            //IWebElement element = WaitUntilElementExists(By.XPath("//li[contains(@id, 'rptMenu__ctl3_rptSubMenu__ctl3_liMenu')]/a"));
            //ClickUntilURLChanges(element);

            string lookUpMemberURL = string.Empty;

            if (driver.Url.Contains("jdate.com"))
            {
                if (driver.Url.Contains("preprod"))
                    lookUpMemberURL = "http://preprod.jdate.com/Applications/LookupProfile/LookupProfile.aspx?NavPoint=sub";
                else
                    lookUpMemberURL = "http://www.jdate.com/Applications/LookupProfile/LookupProfile.aspx?NavPoint=sub";
            }
            else if (driver.Url.Contains("spark.com"))
            {
                if (driver.Url.Contains("preprod"))
                    lookUpMemberURL = "http://preprod.spark.com/Applications/LookupProfile/LookupProfile.aspx?NavPoint=sub";
                else
                    lookUpMemberURL = "http://www.spark.com/Applications/LookupProfile/LookupProfile.aspx?NavPoint=sub";
            }
            else if (driver.Url.Contains("blacksingles.com"))
            {
                if (driver.Url.Contains("preprod"))
                    lookUpMemberURL = "http://preprod.blacksingles.com/Applications/LookupProfile/LookupProfile.aspx?NavPoint=sub";
                else
                    lookUpMemberURL = "http://www.blacksingles.com/Applications/LookupProfile/LookupProfile.aspx?NavPoint=sub";
            }
            else if (driver.Url.Contains("bbwpersonalsplus.com"))
            {
                if (driver.Url.Contains("preprod"))
                    lookUpMemberURL = "http://preprod.bbwpersonalsplus.com/Applications/LookupProfile/LookupProfile.aspx?NavPoint=sub";
                else
                    lookUpMemberURL = "http://www.bbwpersonalsplus.com/Applications/LookupProfile/LookupProfile.aspx?NavPoint=sub";
            }
            else if (driver.Url.Contains("jdate.co.il"))
            {
                if (driver.Url.Contains("preprod"))
                    lookUpMemberURL = "http://preprod.jdate.co.il/Applications/LookupProfile/LookupProfile.aspx?NavPoint=sub";
                else
                    lookUpMemberURL = "http://www.jdate.co.il/Applications/LookupProfile/LookupProfile.aspx?NavPoint=sub";
            }



            driver.Navigate().GoToUrl(lookUpMemberURL);

            return new Framework.Site_Pages.LookUpMember(driver);
        }

        public Framework.Site_Pages.MembersOnline MembersOnlineSubHeaderLink_Click()
        {
            // 10/24/12 The header has changed where if we hover over a link like Community Links the sub header in Firebug does not show any of the sub items as displayed or visible.  
            //   Thus every time we hover using Webdriver nothing ever seems to appear and for now we will just go to the URL as a workaround.


            //// different headers have different initial xpaths for the header link we will hover over
            //IWebElement communityLinksHeader = null;
            //Actions elementToHover = new Actions(driver);
            //Actions subElementToClick = null;


            //if (driver.FindElements(By.XPath("//li[@id='_ctl0_Header__ctl0_rptMenu__ctl5_liMenu']/a")).Count > 0)
            //{
            //    //// alt xpath for community header:  //html/body/div/form/div/div[2]/div/ul/li[6]/a

            //    //// #1 using chain actions
            //    ////IWebElement firstElement = WaitUntilElementExists(By.XPath("//li[@class='menu-item community']"));
            //    //IWebElement firstElement = WaitUntilElementExists(By.XPath("//html/body/div/form/div/div[2]/div/ul/li[6]/a"));
            //    //IWebElement secondElement = WaitUntilElementExists(By.XPath("//html/body/div/form/div/div[2]/div/ul/li[6]/ul/li/a"));

            //    //Actions myAction = new Actions(driver)
            //    //    .MoveToElement(firstElement)
            //    //    .Click(secondElement);

            //    //myAction.Build();
            //    //myAction.Perform();




            //    //// #2 using clickAndHold
            //    ////IWebElement firstElement = WaitUntilElementExists(By.XPath("//li[@class='menu-item community']"));
            //    //IWebElement firstElement2 = WaitUntilElementExists(By.XPath("//html/body/div/form/div/div[2]/div/ul/li[6]/a"));
            //    //IWebElement secondElement2 = WaitUntilElementExists(By.XPath("//html/body/div/form/div/div[2]/div/ul/li[6]/ul/li/a"));

            //    //Actions myAction2 = new Actions(driver);

            //    //myAction.ClickAndHold(firstElement2).Perform();



            //    //// #2.5
            //    ////IWebElement firstElement = WaitUntilElementExists(By.XPath("//li[@class='menu-item community']"));
            //    //IWebElement firstElement2 = WaitUntilElementExists(By.XPath("//html/body/div/form/div/div[2]/div/ul/li[6]/a"));
            //    //IWebElement secondElement2 = WaitUntilElementExists(By.XPath("//html/body/div/form/div/div[2]/div/ul/li[6]/ul/li/a"));
            //    //Actions builder = new Actions(driver);
            //    //builder.MoveToElement(firstElement2).MoveToElement(secondElement2).Click().Build().Perform();

                
            //    //// #3 using HoverClick
            //    ////IWebElement element = driver.FindElement(By.XPath("//li[@class='menu-item community']")); 
            //    //IWebElement element = driver.FindElement(By.XPath("//html/body/div/form/div/div[2]/div/ul/li[6]/a"));
                
            //    //RemoteWebElement clickToEdit = (RemoteWebElement)element;               
            //    //HoverClick(clickToEdit);




            //    //// #4 original version
            //    //// different headers have different initial xpaths for the header link we will hover over
            //    ////IWebElement communityLinksHeader = null;
            //    ////Actions elementToHover = new Actions(driver);
            //    ////Actions subElementToClick = null;

            //    //if (driver.FindElements(By.XPath("//li[@id='_ctl0_Header__ctl0_rptMenu__ctl5_liMenu']/a")).Count > 0)
            //    //{
            //    //    communityLinksHeader = WaitUntilElementExists(By.XPath("//li[@id='_ctl0_Header__ctl0_rptMenu__ctl5_liMenu']/a"));
            //    //    elementToHover.MoveToElement(communityLinksHeader).Perform();
            //    //    subElementToClick = elementToHover.MoveToElement(WaitUntilElementExists(By.XPath("//li[@id='_ctl0_Header__ctl0_rptMenu__ctl5_liMenu']/ul/li/a")));
            //    //}
            //    //else
            //    //{
            //    //    communityLinksHeader = WaitUntilElementExists(By.XPath("//li[@id='HeaderMenu_rptMenu__ctl5_liMenu']/a"));
            //    //    elementToHover.MoveToElement(communityLinksHeader).Perform();
            //    //    subElementToClick = elementToHover.MoveToElement(WaitUntilElementExists(By.XPath("//li[@id='HeaderMenu_rptMenu__ctl5_liMenu']/ul/li/a")));
            //    //}

            //    //subElementToClick.Click();
            //    //subElementToClick.Perform();




            //    ////# 5 using javascript
            //    ////communityLinksHeader = WaitUntilElementExists(By.XPath("//li[@class='menu-item community']"));
            //    //communityLinksHeader = WaitUntilElementExists(By.XPath("//html/body/div/form/div/div[2]/div/ul/li[6]/a"));
            //    //elementToHover.MoveToElement(communityLinksHeader).Perform();

            //    ////   http://www.learnseleniumtesting.com/mouse-hover-and-other-mouse-events-in-webdriver/

            //    //string javaScript = "var evObj = document.createEvent('MouseEvents');" +
            //    //                "evObj.initMouseEvent(\"mouseover\",true, false, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);" +
            //    //                "arguments[0].dispatchEvent(evObj);";
            //    //IJavaScriptExecutor js = driver as IJavaScriptExecutor;
            //    //js.ExecuteScript(javaScript, communityLinksHeader);


            //    //// #6 trying to maximize ff before clicking
            //    //Size currentWinSize = driver.Manage().Window.Size;
            //    //driver.Manage().Window.Maximize();

            //    //OpenQA.Selenium.Interactions.Actions builder = new OpenQA.Selenium.Interactions.Actions(driver);
            //    //builder.MoveToElement(communityLinksHeader).Build().Perform();



            //    builder.ContextClick(communityLinksHeader).Build().Perform();



            //    subElementToClick = elementToHover.MoveToElement(WaitUntilElementExists(By.XPath("//li[@id='_ctl0_Header__ctl0_rptMenu__ctl5_liMenu']/ul/li/a")));
            //}
            //else
            //{
            //    communityLinksHeader = WaitUntilElementExists(By.XPath("//li[@id='HeaderMenu_rptMenu__ctl5_liMenu']/a"));
            //    elementToHover.MoveToElement(communityLinksHeader).Perform();
            //    subElementToClick = elementToHover.MoveToElement(WaitUntilElementExists(By.XPath("//li[@id='HeaderMenu_rptMenu__ctl5_liMenu']/ul/li/a")));
            //}

            //subElementToClick.Click();
            //subElementToClick.Perform();

            string currentURL = driver.Url;
            string[] domain = currentURL.Split('/');

            string membersOnlineLocation = "/Applications/MembersOnline/MembersOnline.aspx?NavPoint=top";
            string membersOnlineURL = domain[2] + membersOnlineLocation;
            driver.Navigate().GoToUrl(membersOnlineURL);

            return new Framework.Site_Pages.MembersOnline(driver);
        }

        public Framework.Site_Pages.KibitzCorner KibitzCornerSubHeaderLink_Click()
        {
            // different headers have different initial xpaths for the header link we will hover over
            IWebElement communityLinksHeader = null;
            Actions elementToHover = new Actions(driver);
            Actions subElementToClick = null;

            if (driver.FindElements(By.XPath("//li[@id='_ctl0_Header__ctl0_rptMenu__ctl5_liMenu']/a")).Count > 0)
            {
                communityLinksHeader = WaitUntilElementExists(By.XPath("//li[@id='_ctl0_Header__ctl0_rptMenu__ctl5_liMenu']/a"));
                elementToHover.MoveToElement(communityLinksHeader).Perform();
                subElementToClick = elementToHover.MoveToElement(WaitUntilElementExists(By.XPath("//li[@id='_ctl0_Header__ctl0_rptMenu__ctl5_liMenu']/ul/li[2]/a")));
            }
            else
            {
                communityLinksHeader = WaitUntilElementExists(By.XPath("//li[@id='HeaderMenu_rptMenu__ctl5_liMenu']/a"));
                elementToHover.MoveToElement(communityLinksHeader).Perform();
                subElementToClick = elementToHover.MoveToElement(WaitUntilElementExists(By.XPath("//li[@id='HeaderMenu_rptMenu__ctl5_liMenu']/ul/li[2]/a")));
            }

            subElementToClick.Click();
            subElementToClick.Perform();

            return new Framework.Site_Pages.KibitzCorner(driver);
        }

        public Framework.Site_Pages.SecretAdmirer SecretAdmirerSubHeaderLink_Click()
        {
            // different headers have different initial xpaths for the header link we will hover over
            IWebElement communityLinksHeader = null;
            Actions elementToHover = new Actions(driver);
            Actions subElementToClick = null;

            if (driver.FindElements(By.XPath("//li[@id='_ctl0_Header__ctl0_rptMenu__ctl5_liMenu']/a")).Count > 0)
            {
                communityLinksHeader = WaitUntilElementExists(By.XPath("//li[@id='_ctl0_Header__ctl0_rptMenu__ctl5_liMenu']/a"));
                elementToHover.MoveToElement(communityLinksHeader).Perform();
                subElementToClick = elementToHover.MoveToElement(WaitUntilElementExists(By.XPath("//li[@id='_ctl0_Header__ctl0_rptMenu__ctl5_liMenu']/ul/li[3]/a")));
            }
            else
            {
                communityLinksHeader = WaitUntilElementExists(By.XPath("//li[@id='HeaderMenu_rptMenu__ctl5_liMenu']/a"));
                elementToHover.MoveToElement(communityLinksHeader).Perform();
                subElementToClick = elementToHover.MoveToElement(WaitUntilElementExists(By.XPath("//li[@id='HeaderMenu_rptMenu__ctl5_liMenu']/ul/li[3]/a")));
            }

            subElementToClick.Click();
            subElementToClick.Perform();

            return new Framework.Site_Pages.SecretAdmirer(driver);
        }

        public Framework.Site_Pages.ChatRooms ChatRoomsSubHeaderLink_Click()
        {
            // different headers have different initial xpaths for the header link we will hover over
            IWebElement communityLinksHeader = null;
            Actions elementToHover = new Actions(driver);
            Actions subElementToClick = null;

            if (driver.FindElements(By.XPath("//li[@id='_ctl0_Header__ctl0_rptMenu__ctl5_liMenu']/a")).Count > 0)
            {
                communityLinksHeader = WaitUntilElementExists(By.XPath("//li[@id='_ctl0_Header__ctl0_rptMenu__ctl5_liMenu']/a"));
                elementToHover.MoveToElement(communityLinksHeader).Perform();
                subElementToClick = elementToHover.MoveToElement(WaitUntilElementExists(By.XPath("//li[@id='_ctl0_Header__ctl0_rptMenu__ctl5_liMenu']/ul/li[4]/a")));
            }
            else
            {
                communityLinksHeader = WaitUntilElementExists(By.XPath("//li[@id='HeaderMenu_rptMenu__ctl5_liMenu']/a"));
                elementToHover.MoveToElement(communityLinksHeader).Perform();
                subElementToClick = elementToHover.MoveToElement(WaitUntilElementExists(By.XPath("//li[@id='HeaderMenu_rptMenu__ctl5_liMenu']/ul/li[4]/a")));
            }

            subElementToClick.Click();
            subElementToClick.Perform();

            return new Framework.Site_Pages.ChatRooms(driver);
        }

        public Framework.Site_Pages.MessageBoards MessageBoardsSubHeaderLink_Click()
        {
            // different headers have different initial xpaths for the header link we will hover over
            IWebElement communityLinksHeader = null;
            Actions elementToHover = new Actions(driver);
            Actions subElementToClick = null;

            if (driver.FindElements(By.XPath("//li[@id='_ctl0_Header__ctl0_rptMenu__ctl5_liMenu']/a")).Count > 0)
            {
                communityLinksHeader = WaitUntilElementExists(By.XPath("//li[@id='_ctl0_Header__ctl0_rptMenu__ctl5_liMenu']/a"));
                elementToHover.MoveToElement(communityLinksHeader).Perform();
                subElementToClick = elementToHover.MoveToElement(WaitUntilElementExists(By.XPath("//li[@id='_ctl0_Header__ctl0_rptMenu__ctl5_liMenu']/ul/li[5]/a")));
            }
            else
            {
                communityLinksHeader = WaitUntilElementExists(By.XPath("//li[@id='HeaderMenu_rptMenu__ctl5_liMenu']/a"));
                elementToHover.MoveToElement(communityLinksHeader).Perform();
                subElementToClick = elementToHover.MoveToElement(WaitUntilElementExists(By.XPath("//li[@id='HeaderMenu_rptMenu__ctl5_liMenu']/ul/li[5]/a")));
            }

            subElementToClick.Click();
            subElementToClick.Perform();

            return new Framework.Site_Pages.MessageBoards(driver);
        }

        public Framework.Site_Pages.ECards ECardsSubHeaderLink_Click()
        {
            // different headers have different initial xpaths for the header link we will hover over
            IWebElement communityLinksHeader = null;
            Actions elementToHover = new Actions(driver);
            Actions subElementToClick = null;

            if (driver.FindElements(By.XPath("//li[@id='_ctl0_Header__ctl0_rptMenu__ctl5_liMenu']/a")).Count > 0)
            {
                communityLinksHeader = WaitUntilElementExists(By.XPath("//li[@id='_ctl0_Header__ctl0_rptMenu__ctl5_liMenu']/a"));
                elementToHover.MoveToElement(communityLinksHeader).Perform();
                subElementToClick = elementToHover.MoveToElement(WaitUntilElementExists(By.XPath("//li[@id='_ctl0_Header__ctl0_rptMenu__ctl5_liMenu']/ul/li[6]/a")));
            }
            else
            {
                communityLinksHeader = WaitUntilElementExists(By.XPath("//li[@id='HeaderMenu_rptMenu__ctl5_liMenu']/a"));
                elementToHover.MoveToElement(communityLinksHeader).Perform();
                subElementToClick = elementToHover.MoveToElement(WaitUntilElementExists(By.XPath("//li[@id='HeaderMenu_rptMenu__ctl5_liMenu']/ul/li[6]/a")));
            }

            subElementToClick.Click();
            subElementToClick.Perform();

            return new Framework.Site_Pages.ECards(driver);
        }

        public Framework.Site_Pages.TravelAndEvents TravelAndEventsSubHeaderLink_Click()
        {
            // different headers have different initial xpaths for the header link we will hover over
            IWebElement communityLinksHeader = null;
            Actions elementToHover = new Actions(driver);
            Actions subElementToClick = null;

            if (driver.FindElements(By.XPath("//li[@id='_ctl0_Header__ctl0_rptMenu__ctl5_liMenu']/a")).Count > 0)
            {
                communityLinksHeader = WaitUntilElementExists(By.XPath("//li[@id='_ctl0_Header__ctl0_rptMenu__ctl5_liMenu']/a"));
                elementToHover.MoveToElement(communityLinksHeader).Perform();
                subElementToClick = elementToHover.MoveToElement(WaitUntilElementExists(By.XPath("//li[@id='_ctl0_Header__ctl0_rptMenu__ctl5_liMenu']/ul/li[7]/a")));
            }
            else
            {
                communityLinksHeader = WaitUntilElementExists(By.XPath("//li[@id='HeaderMenu_rptMenu__ctl5_liMenu']/a"));
                elementToHover.MoveToElement(communityLinksHeader).Perform();
                subElementToClick = elementToHover.MoveToElement(WaitUntilElementExists(By.XPath("//li[@id='HeaderMenu_rptMenu__ctl5_liMenu']/ul/li[7]/a")));
            }

            subElementToClick.Click();
            subElementToClick.Perform();

            return new Framework.Site_Pages.TravelAndEvents(driver);
        }

        public Framework.Site_Pages.ReferAFriend ReferAFriendSubHeaderLink_Click()
        {
            // different headers have different initial xpaths for the header link we will hover over
            IWebElement communityLinksHeader = null;
            Actions elementToHover = new Actions(driver);
            Actions subElementToClick = null;

            if (driver.FindElements(By.XPath("//li[@id='_ctl0_Header__ctl0_rptMenu__ctl5_liMenu']/a")).Count > 0)
            {
                communityLinksHeader = WaitUntilElementExists(By.XPath("//li[@id='_ctl0_Header__ctl0_rptMenu__ctl5_liMenu']/a"));
                elementToHover.MoveToElement(communityLinksHeader).Perform();
                subElementToClick = elementToHover.MoveToElement(WaitUntilElementExists(By.XPath("//li[@id='_ctl0_Header__ctl0_rptMenu__ctl5_liMenu']/ul/li[8]/a")));
            }
            else
            {
                communityLinksHeader = WaitUntilElementExists(By.XPath("//li[@id='HeaderMenu_rptMenu__ctl5_liMenu']/a"));
                elementToHover.MoveToElement(communityLinksHeader).Perform();
                subElementToClick = elementToHover.MoveToElement(WaitUntilElementExists(By.XPath("//li[@id='HeaderMenu_rptMenu__ctl5_liMenu']/ul/li[8]/a")));
            }

            subElementToClick.Click();
            subElementToClick.Perform();

            return new Framework.Site_Pages.ReferAFriend(driver);
        }

        /// <summary>
        /// This is for the "new" Profile Pages (as of 2011)
        /// </summary>
        /// <returns></returns>
        public Framework.Site_Pages.YourProfile YourProfileHeaderLink_Click()
        {
            IWebElement element = null;

            if (driver.Url.Contains("jdate.com"))
                element = WaitUntilElementExists(By.XPath("//li[contains(@id, 'rptMenu__ctl6_liMenu')]/a"));
            else if (driver.Url.Contains("jdate.co.il"))
                element = WaitUntilElementExists(By.XPath("//li[contains(@id, 'rptMenu__ctl8_liMenu')]/a"));
            else if (driver.Url.Contains("spark.com"))
                element = WaitUntilElementExists(By.XPath("//li[contains(@id, 'rptMenu__ctl7_liMenu')]/a"));
            else if (driver.Url.Contains("blacksingles.com"))
                element = WaitUntilElementExists(By.XPath("//li[contains(@id, 'rptMenu__ctl6_liMenu')]/a"));
            else if (driver.Url.Contains("bbwpersonalsplus.com"))
                element = WaitUntilElementExists(By.XPath("//li[contains(@id, 'rptMenu__ctl6_liMenu')]/a"));

            // for some strange reason sometimes when we try to click on Your Profile in the header the Your Profile dropdown gets highlighted but the link doesn't
            //    get clicked, especially on JDate.  if we fail, just go straight to the Your Profile Link
            try
            {
                // if we are currently reloading the Profile Page we don't expect the URL to change
                if (driver.Url.Contains("ViewProfile"))
                    element.Click();
                else
                    ClickUntilURLChanges(element);

                // 8/24/12 as of the new deployment the Your Profile header dropdown stays "dropped down" which affects clicking things such as Your Basics on the Profile Page. Click a random other spot on the page
                //    to get rid of this
                IWebElement randomOtherSpot = WaitUntilElementExists(By.XPath("//div[@id='profile30Comm']"));
                randomOtherSpot.Click();
            }
            catch
            {
                if (driver.Url.Contains("jdate.com"))
                {
                    if (driver.Url.Contains("preprod"))
                        driver.Navigate().GoToUrl("http://preprod.jdate.com/Applications/MemberProfile/ViewProfile.aspx?EntryPoint=99999&NavPoint=top");
                    else
                        driver.Navigate().GoToUrl("http://www.jdate.com/Applications/MemberProfile/ViewProfile.aspx?EntryPoint=99999&NavPoint=top");
                }
                else if (driver.Url.Contains("spark.com"))
                {
                    if (driver.Url.Contains("preprod"))
                        driver.Navigate().GoToUrl("http://preprod.spark.com/Applications/MemberProfile/ViewProfile.aspx?EntryPoint=99999&NavPoint=top");
                    else
                        driver.Navigate().GoToUrl("http://www.spark.com/Applications/MemberProfile/ViewProfile.aspx?EntryPoint=99999&NavPoint=top");
                }
                else if (driver.Url.Contains("blacksingles.com"))
                {
                    if (driver.Url.Contains("preprod"))
                        driver.Navigate().GoToUrl("http://preprod.blacksingles.com/Applications/MemberProfile/ViewProfile.aspx?EntryPoint=99999&NavPoint=top");
                    else
                        driver.Navigate().GoToUrl("http://www.blacksingles.com/Applications/MemberProfile/ViewProfile.aspx?EntryPoint=99999&NavPoint=top");
                }
                else if (driver.Url.Contains("bbwpersonalsplus.com"))
                {
                    if (driver.Url.Contains("preprod"))
                        driver.Navigate().GoToUrl("http://preprod.bbwpersonalsplus.com/Applications/MemberProfile/ViewProfile.aspx?EntryPoint=99999&NavPoint=top");
                    else
                        driver.Navigate().GoToUrl("http://www.bbwpersonalsplus.com/Applications/MemberProfile/ViewProfile.aspx?EntryPoint=99999&NavPoint=top");
                }
                else if (driver.Url.Contains("jdate.co.il"))
                {
                    if (driver.Url.Contains("preprod"))
                        driver.Navigate().GoToUrl("http://preprod.jdate.co.il/Applications/MemberProfile/ViewProfile.aspx?EntryPoint=99999&NavPoint=top");
                    else
                        driver.Navigate().GoToUrl("http://www.jdate.co.il/Applications/MemberProfile/ViewProfile.aspx?EntryPoint=99999&NavPoint=top");
                }
            }

            return new Framework.Site_Pages.YourProfile(driver);
        }

        /// <summary>
        /// Use this method when expecting the Old Profile Page to appear
        /// </summary>
        /// <returns></returns>
        public Framework.Site_Pages.YourProfile_old YourProfileHeaderLink_OldProfile_Click()
        {
            IWebElement element = null;

            if (driver.Url.Contains("jdate.com"))
                element = WaitUntilElementExists(By.XPath("//li[contains(@id, 'rptMenu__ctl6_liMenu')]/a"));
            else if (driver.Url.Contains("spark.com"))
                element = WaitUntilElementExists(By.XPath("//li[contains(@id, 'rptMenu__ctl7_liMenu')]/a"));
            else if (driver.Url.Contains("blacksingles.com"))
                element = WaitUntilElementExists(By.XPath("//li[contains(@id, 'rptMenu__ctl6_liMenu')]/a"));
            else if (driver.Url.Contains("bbwpersonalsplus.com"))
                element = WaitUntilElementExists(By.XPath("//li[contains(@id, 'rptMenu__ctl6_liMenu')]/a"));

            ClickUntilURLChanges(element);

            return new Framework.Site_Pages.YourProfile_old(driver);
        }

        // Not working right now that we have a new header and Hover does not work
        public Framework.Site_Pages.EditYourProfile EditYourProfileSubHeaderLink_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//li[@id='_ctl0_Header__ctl0_rptMenu__ctl6_rptSubMenu__ctl0_liMenu']/a"));
            ClickUntilURLChanges(element);

            return new Framework.Site_Pages.EditYourProfile(driver);
        }

        /// <summary>
        /// For registered users but not subscribed ones
        /// </summary>
        /// <returns></returns>
        public Framework.Site_Pages.Subscribe SubscribeHeaderLink_Click()
        {
            IWebElement element = null;

            if (driver.FindElements(By.XPath("//li[contains(@id, 'rptMenu__ctl7_liMenu')]/a")).Count > 0)
            {
                element = WaitUntilElementExists(By.XPath("//li[contains(@id, 'rptMenu__ctl7_liMenu')]/a"));

                if (element.Text != "Subscribe" && element.Text != "Upgrade")
                    element = null;
            }

            if (element == null && driver.FindElements(By.XPath("//li[contains(@id, 'rptMenu__ctl8_liMenu')]/a")).Count > 0)
            {
                element = WaitUntilElementExists(By.XPath("//li[contains(@id, 'rptMenu__ctl8_liMenu')]/a"));

                if (element.Text != "Subscribe" && element.Text != "Upgrade")
                    element = null;
            }

            if (element == null && driver.FindElements(By.XPath("//li[contains(@id, 'rptMenu__ctl9_liMenu')]/a")).Count > 0)
            {
                element = WaitUntilElementExists(By.XPath("//li[contains(@id, 'rptMenu__ctl9_liMenu')]/a"));

                if (element.Text != "Subscribe" && element.Text != "Upgrade")
                    element = null;
            }
            //WaitUntilElementExists(By.XPath("//li[contains(@id, 'rptMenu__ctl9_liMenu')]/a"));

            try
            {
                ClickUntilURLChanges(element);
            }
            catch
            {
                // if we can't click subscribe for whatever reason we'll try to reload the page and try again
                Framework.Site_Pages.Home home = HomeHeaderLink_Click();

                Framework.Site_Pages.Subscribe subscribe = SubscribeHeaderLink_Click();
            }

            return new Framework.Site_Pages.Subscribe(driver);
        }

        public bool SubscribeHeaderLinkExists()
        {
            IWebElement element;

            try
            {
                element = WaitUntilElementExists(By.XPath("//li[contains(@id, 'rptMenu__ctl9_liMenu')]/a"));
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// For subscribed users that have not bought everything there is to buy
        /// </summary>
        /// <returns></returns>
        public Framework.Site_Pages.Subscribe UpgradeHeaderLink_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//li[@id='_ctl0_Header__ctl0_rptMenu__ctl8_liMenu']/a"));
            ClickUntilURLChanges(element);

            return new Framework.Site_Pages.Subscribe(driver);
        }

        public bool UpgradeHeaderLinkExists()
        {
            IWebElement element;

            try
            {
                element = WaitUntilElementExists(By.XPath("//li[@id='_ctl0_Header__ctl0_rptMenu__ctl8_liMenu']/a"));
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// JDate.co.il only
        /// </summary>
        /// <returns></returns>
        public Framework.Site_Pages.JMeterWelcome JMeterLink_NotSubscribedToJMeter_Click()
        {
            string lookUpMemberURL = string.Empty;

            if (driver.Url.Contains("preprod"))
                lookUpMemberURL = "http://preprod.jdate.co.il/Applications/CompatibilityMeter/Matches.aspx?NavPoint=sub";
            else
                lookUpMemberURL = "http://www.jdate.co.il/Applications/CompatibilityMeter/Matches.aspx?NavPoint=sub";

            driver.Navigate().GoToUrl(lookUpMemberURL);

            return new Framework.Site_Pages.JMeterWelcome(driver);
        }

        /// <summary>
        /// JDate.co.il only
        /// </summary>
        /// <returns></returns>
        public Framework.Site_Pages.JMeterAdjustments JMeterLink_SubscribedToJMeter_Click()
        {
            string lookUpMemberURL = string.Empty;

            if (driver.Url.Contains("preprod"))
                lookUpMemberURL = "http://preprod.jdate.co.il/Applications/CompatibilityMeter/Matches.aspx?NavPoint=sub";
            else
                lookUpMemberURL = "http://www.jdate.co.il/Applications/CompatibilityMeter/Matches.aspx?NavPoint=sub";

            driver.Navigate().GoToUrl(lookUpMemberURL);

            return new Framework.Site_Pages.JMeterAdjustments(driver);
        }

        public Framework.Site_Pages.MembersOnline MembersOnline_Click()
        {
            IWebElement element = WaitUntilElementExists(By.XPath("//a[contains(@id, '_lnkMembersOnline')]/span"));
            ClickUntilURLChanges(element);

            return new Framework.Site_Pages.MembersOnline(driver);
        }

        #endregion
    }
}
