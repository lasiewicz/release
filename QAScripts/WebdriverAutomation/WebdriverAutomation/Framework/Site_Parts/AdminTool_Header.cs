﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.Events;
using NUnit.Framework;
using System.Threading;

namespace WebdriverAutomation.Framework.Parts
{
    public class AdminTool_Header : BasePage
    {
        public AdminTool_Header(IWebDriver driver)
            : base(driver)
        {
            this.driver = driver;
        }

        #region Home Page
        public WebdriverAutomation.Framework.Pages.AdminTool_FreeTextApproval FreeTextApproval_EnglishLink_Click()
        {
            IWebElement header = WaitUntilElementExists(By.XPath("//div[@id='member-search-nav']/ul/li[1]/a"));
            header.Click();

            Thread.Sleep(1000);

            IWebElement subheader = WaitUntilElementExists(By.XPath("//div[@id='member-search-nav']/ul/li[1]/ul/li[1]/a"));
            ClickUntilURLChanges(subheader);

            //string URL = @"http://admintool.matchnet.com/Approval/ViewTextByLanguage/2";
            //driver.Navigate().GoToUrl(URL);

            return new WebdriverAutomation.Framework.Pages.AdminTool_FreeTextApproval(driver);
        }

        public WebdriverAutomation.Framework.Pages.AdminTool_PhotoApproval PhotoApproval_JDatecomLink_Click()
        {
            IWebElement header = WaitUntilElementExists(By.XPath("//div[@id='member-search-nav']/ul/li[2]/a"));
            header.Click();

            Thread.Sleep(1000);

            IWebElement subheader = WaitUntilElementExists(By.XPath("//div[@id='member-search-nav']/ul/li[2]/ul/li[8]/a"));
            ClickUntilURLChanges(subheader);

            //string URL = @"http://admintool.matchnet.com/Approval/ViewPhotosBySite/103";
            //driver.Navigate().GoToUrl(URL);

            return new WebdriverAutomation.Framework.Pages.AdminTool_PhotoApproval(driver);
        }

        public WebdriverAutomation.Framework.Pages.AdminTool_QuestionAnswerApproval QuestionAnswerApproval_EnglishLink_Click()
        {
            IWebElement header = WaitUntilElementExists(By.XPath("//div[@id='member-search-nav']/ul/li[3]/a"));
            header.Click();

            Thread.Sleep(1000);

            IWebElement subheader = WaitUntilElementExists(By.XPath("//div[@id='member-search-nav']/ul/li[3]/ul/li[2]/a"));
            ClickUntilURLChanges(subheader);

            return new WebdriverAutomation.Framework.Pages.AdminTool_QuestionAnswerApproval(driver);
        }

        public WebdriverAutomation.Framework.Pages.AdminTool_CreditTool CreditTool_Click()
        {
            IWebElement header = WaitUntilElementExists(By.XPath("//div[@id='member-search-nav']/ul/li[4]/a"));
            header.Click();

            return new WebdriverAutomation.Framework.Pages.AdminTool_CreditTool(driver);
        }

        public WebdriverAutomation.Framework.Pages.AdminTool_MessageBoardsLoginPage MessageBoards_JDate_Click()
        {
            IWebElement header = WaitUntilElementExists(By.XPath("//div[@id='member-search-nav']/ul/li[5]/a"));
            header.Click();

            Thread.Sleep(1000);

            IWebElement subheader = WaitUntilElementExists(By.XPath("//div[@id='member-search-nav']/ul/li[5]/ul/li[4]/a"));
            subheader.Click();

            return new WebdriverAutomation.Framework.Pages.AdminTool_MessageBoardsLoginPage(driver);
        }




        #endregion

    }
}
