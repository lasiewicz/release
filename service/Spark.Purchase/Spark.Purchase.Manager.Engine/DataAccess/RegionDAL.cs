﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Matchnet.Data;
using Spark.Purchase.ValueObjects;
using Spark.UnifiedPurchaseSystem.Lib.Exceptions;
using Spark.UnifiedPurchaseSystem.Lib.Common;
using Spark.UnifiedPurchaseSystem.Lib.Logging;

namespace Spark.Purchase.Manager.Engine.DataAccess
{
    internal class RegionDAL
    {
        internal int GetRegionID(decimal latitude, decimal longitude)
        {
            int regionID = Constants.NULL_INT;
            SqlDataReader dataReader = null;

            try
            {
                TraceLog.Enter("RegionDAL.GetRegionID");
                TraceLog.WriteLine("Begin RegionDAL.GetRegionID({0}, {1})", latitude, longitude);

                Command command = new Command("epRegion", "dbo.up_Region_Select", 0);
                command.AddParameter("@Latitude", SqlDbType.Decimal, ParameterDirection.Input, latitude);
                command.AddParameter("@Longitude", SqlDbType.Decimal, ParameterDirection.Input, longitude);
                dataReader = Client.Instance.ExecuteReader(command);

                if (dataReader.Read())
                {
                    regionID = dataReader.GetInt32(dataReader.GetOrdinal("RegionID"));
                    TraceLog.WriteLine("Region ID [{0}] found for latitude [{1}] and longitude [{2}]", regionID, latitude, longitude);
                }

                return regionID;
            }
            catch (Exception exception)
            {
                TraceLog.WriteErrorLine(exception, "Error in RegionDAL.GetRegionID(), Error: " + exception.Message);

                /*
                The exception thrown by Hydra has the following
                    The exception.Message contains the descriptive message of the error that was detailed
                    out by Hydra  
                    The exception.InnerException contains the original exception in Hydra which may be
                    a SqlException
                */
                SqlException sqlException = exception.InnerException as SqlException;
                if (sqlException != null)
                {
                    throw new DALException("RegionDAL.GetRegionID() error.", exception, exception.Message, ExceptionUtility.DetermineSQLDBExceptionLevel(sqlException));
                }
                else
                {
                    throw new DALException("RegionDAL.GetRegionID() error.", exception, exception.Message);
                }
            }
            finally
            {
                if (dataReader != null)
                {
                    dataReader.Close();
                }

                TraceLog.WriteLine("End RegionDAL.GetRegionID()");
                TraceLog.Leave("RegionDAL.GetRegionID");
            }
        }
    }
}
