﻿using System;
using System.Data;
using System.Data.SqlClient;
using Matchnet.Data;
using Spark.Purchase.ValueObjects;
using Spark.UnifiedPurchaseSystem.Lib.Exceptions;

namespace Spark.Purchase.Manager.Engine.DataAccess
{
    internal class PaymentProfileMapperDAL
    {
        internal void SetMemberPaymentProfile(int CustomerID, int CallingSystemID, string PaymentProfileID, string PaymentProfileIdType)
        {
            try
            {
                Command command = new Command("epOrder", "dbo.up_MemberPaymentProfile_Insert", 0);
                command.AddParameter("@CustomerID", SqlDbType.Int, ParameterDirection.Input, CustomerID);
                command.AddParameter("@CallingSystemID", SqlDbType.Int, ParameterDirection.Input, CallingSystemID);
                command.AddParameter("@PaymentProfileID", SqlDbType.NVarChar, ParameterDirection.Input, PaymentProfileID);
                command.AddParameter("@PaymentProfileType", SqlDbType.NVarChar, ParameterDirection.Input, PaymentProfileIdType);
                command.AddParameter("@IsDefault", SqlDbType.Bit, ParameterDirection.Input, 1);
                Client.Instance.ExecuteNonQuery(command);
            }
            catch (Exception exception)
            {
                /*
                The exception thrown by Hydra has the following
                    The exception.Message contains the descriptive message of the error that was detailed
                    out by Hydra  
                    The exception.InnerException contains the original exception in Hydra which may be
                    a SqlException
                */
                SqlException sqlException = exception.InnerException as SqlException;
                if (sqlException != null)
                {
                    throw new DALException("PaymentProfileMapperDAL.SetMemberPaymentProfile() error.", exception, exception.Message, ExceptionUtility.DetermineSQLDBExceptionLevel(sqlException));
                }
                else
                {
                    throw new DALException("PaymentProfileMapperDAL.SetMemberPaymentProfile() error.", exception, exception.Message);
                }
            }
        }

        internal PaymentProfileInfo GetMemberDefaultPaymentProfile(int CustomerID, int CallingSystemID)
        {
            SqlDataReader dataReader = null;
            PaymentProfileInfo DefaultPaymentProfileID = null;

            try
            {
                Command command = new Command("epOrder", "dbo.up_MemberDefaultPaymentProfile_Get", 0);
                command.AddParameter("@CustomerID", SqlDbType.Int, ParameterDirection.Input, CustomerID);
                command.AddParameter("@CallingSystemID", SqlDbType.Int, ParameterDirection.Input, CallingSystemID);
                dataReader = Client.Instance.ExecuteReader(command);

                if (dataReader.Read())
                {
                    DefaultPaymentProfileID = LoadDefaultPaymentProfile(dataReader);
                }
            }
            catch (Exception exception)
            {
                /*
                The exception thrown by Hydra has the following
                    The exception.Message contains the descriptive message of the error that was detailed
                    out by Hydra  
                    The exception.InnerException contains the original exception in Hydra which may be
                    a SqlException
                */
                SqlException sqlException = exception.InnerException as SqlException;
                if (sqlException != null)
                {
                    throw new DALException("PaymentProfileMapperDAL.GetMemberDefaultPaymentProfile() error.", exception, exception.Message, ExceptionUtility.DetermineSQLDBExceptionLevel(sqlException));
                }
                else
                {
                    throw new DALException("PaymentProfileMapperDAL.GetMemberDefaultPaymentProfile() error.", exception, exception.Message);
                }
            }
            finally
            {
                if (dataReader != null)
                {
                    dataReader.Close();
                }
            }

            return DefaultPaymentProfileID;
        }

        private PaymentProfileInfo LoadDefaultPaymentProfile(SqlDataReader dataReader)
        {
            PaymentProfileInfo profile = new PaymentProfileInfo();

            profile.PaymentProfileID = dataReader.GetString(dataReader.GetOrdinal("UserPaymentGUID"));
            profile.CallingsystemID = dataReader.GetInt32(dataReader.GetOrdinal("CallingSystemID"));
            profile.CustomerID = dataReader.GetInt32(dataReader.GetOrdinal("CustomerID"));
            profile.InsertDate = dataReader.GetDateTime(dataReader.GetOrdinal("InsertDate"));
            profile.UpdatedDate = dataReader.GetDateTime(dataReader.GetOrdinal("UpdateDate"));
            profile.IsDefault = dataReader.GetBoolean(dataReader.GetOrdinal("DefaultProfile"));
            profile.PaymentProfileType = dataReader.GetString(dataReader.GetOrdinal("PaymentProfileType"));

            return profile;
        }
    }
}
