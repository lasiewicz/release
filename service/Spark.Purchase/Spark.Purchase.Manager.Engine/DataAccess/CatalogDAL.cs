﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Matchnet.Data;
using Spark.Purchase.ValueObjects;
using Spark.UnifiedPurchaseSystem.Lib.Exceptions;
using Spark.UnifiedPurchaseSystem.Lib.Common;

namespace Spark.Purchase.Manager.Engine.DataAccess
{
    internal class CatalogDAL
    {
        internal Package GetPackage(int packageID)
        {
            Package package = null;
            SqlDataReader dataReader = null;

            try
            {
                Command command = new Command("epProductService", "dbo.up_PackageDetails_Get", 0);
                command.AddParameter("@PackageID", SqlDbType.Int, ParameterDirection.Input, packageID);
                dataReader = Client.Instance.ExecuteReader(command);

                if (dataReader.Read())
                {
                    package = LoadPackageObject(dataReader);
                }

                return package;
            }
            catch (Exception exception)
            {
                /*
                The exception thrown by Hydra has the following
                    The exception.Message contains the descriptive message of the error that was detailed
                    out by Hydra  
                    The exception.InnerException contains the original exception in Hydra which may be
                    a SqlException
                */
                SqlException sqlException = exception.InnerException as SqlException;
                if (sqlException != null)
                {
                    throw new DALException("CatalogDAL.GetPackage() error.", exception, exception.Message, ExceptionUtility.DetermineSQLDBExceptionLevel(sqlException));
                }
                else
                {
                    throw new DALException("CatalogDAL.GetPackage() error.", exception, exception.Message);
                }
            }
            finally
            {
                if (dataReader != null)
                {
                    dataReader.Close();
                }
            }
        }

        private Package LoadPackageObject(SqlDataReader reader)
        {
            Package package = new Package();
            package.ID = reader.GetInt32(reader.GetOrdinal("PackageID"));
            if (!reader.IsDBNull(reader.GetOrdinal("PackageDescription")))
            {
                package.Description = reader.GetString(reader.GetOrdinal("PackageDescription"));
            }
            package.SystemID = reader.GetInt32(reader.GetOrdinal("SiteID"));
            package.Status = (PackageStatus)Enum.Parse(typeof(PackageStatus), reader.GetInt32(reader.GetOrdinal("PackageStatusID")).ToString());
            package.StartDate = reader.GetDateTime(reader.GetOrdinal("PackageStartDate"));
            if (!reader.IsDBNull(reader.GetOrdinal("PackageExpirationDate")))
            {
                package.ExpiredDate = reader.GetDateTime(reader.GetOrdinal("PackageExpirationDate"));
            }
            // Populated later by looking at the first item in this package  
            //package.CurrencyType = CurrencyType.USD;
            if (reader.NextResult())
            {
                package.Items = LoadItemObjects(ref package, reader);
            }
            return package;
        }

        private List<Item> LoadItemObjects(ref Package package, SqlDataReader reader)
        {
            List<Item> items = new List<Item>();
            while (reader.Read())
            {
                Item item = new Item();
                item.ID = reader.GetInt32(reader.GetOrdinal("ItemID"));
                if (!reader.IsDBNull(reader.GetOrdinal("ItemDescription")))
                {
                    item.Description = reader.GetString(reader.GetOrdinal("ItemDescription"));
                }
                item.ItemType = (ItemType)Enum.Parse(typeof(ItemType), reader.GetInt32(reader.GetOrdinal("ItemTypeID")).ToString());
                if (!reader.IsDBNull(reader.GetOrdinal("DefaultDurationTypeID")))
                {
                    item.DefaultDurationType = (DurationType)Enum.Parse(typeof(DurationType), reader.GetInt32(reader.GetOrdinal("DefaultDurationTypeID")).ToString());
                }
                if (!reader.IsDBNull(reader.GetOrdinal("DefaultDuration")))
                {
                    item.DefaultDuration = reader.GetInt32(reader.GetOrdinal("DefaultDuration"));
                }
                if (!reader.IsDBNull(reader.GetOrdinal("DefaultAmount")))
                {
                    item.DefaultAmount = Math.Round(reader.GetDecimal(reader.GetOrdinal("DefaultAmount")), 2, MidpointRounding.ToEven);
                }
                if (!reader.IsDBNull(reader.GetOrdinal("DefaultCurrencyID")))
                {
                    item.DefaultCurrencyType = (CurrencyType)Enum.Parse(typeof(CurrencyType), reader.GetInt32(reader.GetOrdinal("DefaultCurrencyID")).ToString());
                }
                item.IsRenewable = reader.GetBoolean(reader.GetOrdinal("isRenewable"));
                if (!reader.IsDBNull(reader.GetOrdinal("DurationTypeID")))
                {
                    item.DurationType = (DurationType)Enum.Parse(typeof(DurationType), reader.GetInt32(reader.GetOrdinal("DurationTypeID")).ToString());
                }
                if (!reader.IsDBNull(reader.GetOrdinal("Duration")))
                {
                    item.Duration = reader.GetInt32(reader.GetOrdinal("Duration"));
                }
                if (!reader.IsDBNull(reader.GetOrdinal("Amount")))
                {
                    item.Amount = Math.Round(reader.GetDecimal(reader.GetOrdinal("Amount")), 2, MidpointRounding.ToEven);
                }
                if (!reader.IsDBNull(reader.GetOrdinal("CurrencyID")))
                {
                    item.CurrencyType = (CurrencyType)Enum.Parse(typeof(CurrencyType), reader.GetInt32(reader.GetOrdinal("CurrencyID")).ToString());

                    if (package.CurrencyType == CurrencyType.None)
                    {
                        // Set the package currency type from one of its package item currency types  
                        // All the package item currency types for a single package must have the same currency type  
                        package.CurrencyType = item.CurrencyType;
                    }
                }
                if (!reader.IsDBNull(reader.GetOrdinal("RenewDurationTypeID")))
                {
                    item.RenewalDurationType = (DurationType)Enum.Parse(typeof(DurationType), reader.GetInt32(reader.GetOrdinal("RenewDurationTypeID")).ToString());
                }
                if (!reader.IsDBNull(reader.GetOrdinal("RenewalDuration")))
                {
                    item.RenewalDuration = reader.GetInt32(reader.GetOrdinal("RenewalDuration"));
                }
                if (!reader.IsDBNull(reader.GetOrdinal("RenewalAmount")))
                {
                    item.RenewalAmount = Math.Round(reader.GetDecimal(reader.GetOrdinal("RenewalAmount")), 2, MidpointRounding.ToEven);
                }
                if (!reader.IsDBNull(reader.GetOrdinal("RenewalCurrencyID")))
                {
                    item.RenewalCurrencyType = (CurrencyType)Enum.Parse(typeof(CurrencyType), reader.GetInt32(reader.GetOrdinal("RenewalCurrencyID")).ToString());
                }

                items.Add(item);
            }

            if (reader.NextResult())
            {
                LoadItemTaxTypesList(ref items, reader);
            }

            return items;
        }

        private void LoadItemTaxTypesList(ref List<Item> items, SqlDataReader reader)
        {
            Item foundItemInList = null;
            ItemTaxType taxType = ItemTaxType.None;
            
            List<ItemTaxType> itemTaxTypes = new List<ItemTaxType>();
            while (reader.Read())
            {
                foundItemInList = items.Find(delegate(Item item) { return item.ID == reader.GetInt32(reader.GetOrdinal("ItemID")); });
                if (foundItemInList != null)
                {
                    taxType = (ItemTaxType)Enum.ToObject(typeof(ItemTaxType), reader.GetInt32(reader.GetOrdinal("ItemTaxTypeID")));
                    foundItemInList.ItemTaxTypes.Add(taxType);
                }
            }
        }
    }
}
