﻿using System;
using System.Collections.Generic;
using Matchnet.Data;
using Spark.Purchase.ValueObjects;
using Spark.UnifiedPurchaseSystem.Lib.Exceptions;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using Spark.UnifiedPurchaseSystem.Lib.Common;

namespace Spark.Purchase.Manager.Engine.DataAccess
{
    internal class OrderHistoryDAL
    {
        #region Public Methods
        internal List<OrderInfo> GetMemberOrderHistoryByMemberID(int CustomerID, int CallingSystemID, int PageSize, int PageNumber)
        {
            SqlDataReader dataReader = null;
            List<OrderInfo> history = new List<OrderInfo>();

            try
            {
                Command command = new Command("epOrder", "dbo.up_OrderHistoryByMemberID_Get", 0);
                command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, CustomerID);
                command.AddParameter("@CallingSystemID", SqlDbType.Int, ParameterDirection.Input, CallingSystemID);
                command.AddParameter("@Page", SqlDbType.Int, ParameterDirection.Input, PageNumber);
                command.AddParameter("@PageSize", SqlDbType.Int, ParameterDirection.Input, PageSize);
                dataReader = Client.Instance.ExecuteReader(command);

                while (dataReader.Read())
                {
                    history.Add(LoadOrderInfo(dataReader));
                }

                dataReader.NextResult();

                while (dataReader.Read())
                {
                    AddDetailToOrderHistory(ref history, LoadOrderDetailinfo(dataReader));
                }
            }
            catch (Exception exception)
            {
                /*
                The exception thrown by Hydra has the following
                    The exception.Message contains the descriptive message of the error that was detailed
                    out by Hydra  
                    The exception.InnerException contains the original exception in Hydra which may be
                    a SqlException
                */
                SqlException sqlException = exception.InnerException as SqlException;
                if (sqlException != null)
                {
                    throw new DALException("OrderHistoryDAL.GetMemberOrderHistoryByMemberID() error.", exception, exception.Message, ExceptionUtility.DetermineSQLDBExceptionLevel(sqlException));
                }
                else
                {
                    throw new DALException("OrderHistoryDAL.GetMemberOrderHistoryByMemberID() error.", exception, exception.Message);
                }
            }
            finally
            {
                if (dataReader != null)
                {
                    dataReader.Close();
                }
            }

            return history;
        }

        internal CustomerInfo GetCustomerInfoByOrderID(int OrderID)
        {
            SqlDataReader dataReader = null;
            CustomerInfo customer = null;

            try
            {
                Command command = new Command("epOrder", "dbo.up_MemberInformatioByOrderID_Get", 0);
                command.AddParameter("@OrderID", SqlDbType.Int, ParameterDirection.Input, OrderID);
                dataReader = Client.Instance.ExecuteReader(command);

                while (dataReader.Read())
                {
                    customer = LoadCustomerInfo(dataReader);
                }
            }
            catch (Exception exception)
            {
                /*
                The exception thrown by Hydra has the following
                    The exception.Message contains the descriptive message of the error that was detailed
                    out by Hydra  
                    The exception.InnerException contains the original exception in Hydra which may be
                    a SqlException
                */
                SqlException sqlException = exception.InnerException as SqlException;
                if (sqlException != null)
                {
                    throw new DALException("OrderHistoryDAL.GetCustomerInfoByOrderID() error.", exception, exception.Message, ExceptionUtility.DetermineSQLDBExceptionLevel(sqlException));
                }
                else
                {
                    throw new DALException("OrderHistoryDAL.GetCustomerInfoByOrderID() error.", exception, exception.Message);
                }
            }
            finally
            {
                if (dataReader != null)
                {
                    dataReader.Close();
                }
            }

            return customer;
        }

        internal CustomerInfo GetCustomerInfoByPaymentID(int PaymentID)
        {
            SqlDataReader dataReader = null;
            CustomerInfo customer = null;

            try
            {
                Command command = new Command("epOrder", "dbo.up_MemberInformationByPaymentID_Get", 0);
                command.AddParameter("@PaymentID", SqlDbType.Int, ParameterDirection.Input, PaymentID);
                dataReader = Client.Instance.ExecuteReader(command);

                while (dataReader.Read())
                {
                    customer = LoadCustomerInfo(dataReader);
                }
            }
            catch (Exception exception)
            {
                /*
                The exception thrown by Hydra has the following
                    The exception.Message contains the descriptive message of the error that was detailed
                    out by Hydra  
                    The exception.InnerException contains the original exception in Hydra which may be
                    a SqlException
                */
                SqlException sqlException = exception.InnerException as SqlException;
                if (sqlException != null)
                {
                    throw new DALException("OrderHistoryDAL.GetCustomerInfoByOrderID() error.", exception, exception.Message, ExceptionUtility.DetermineSQLDBExceptionLevel(sqlException));
                }
                else
                {
                    throw new DALException("OrderHistoryDAL.GetCustomerInfoByOrderID() error.", exception, exception.Message);
                }
            }
            finally
            {
                if (dataReader != null)
                {
                    dataReader.Close();
                }
            }

            return customer;
        }

        internal OrderInfo GetOrderInfo(int orderID)
        {
            OrderInfo orderInfo = new OrderInfo();
            SqlDataReader dataReader = null;

            try
            {
                Command command = new Command("epOrder", "dbo.up_OrderByOrderID_Get", 0);
                command.AddParameter("@OrderID", SqlDbType.Int, ParameterDirection.Input, orderID);
                dataReader = Client.Instance.ExecuteReader(command);

                //OrderInfo
                while (dataReader.Read())
                {
                    orderInfo = LoadOrderInfo(dataReader);
                    break;
                }

                //OrderDetailInfo
                dataReader.NextResult();

                Dictionary<int, OrderDetailInfo> orderInfoDictionary = new Dictionary<int, OrderDetailInfo>();
                while (dataReader.Read())
                {
                    OrderDetailInfo odInfo = LoadOrderDetailinfo(dataReader);
                    orderInfoDictionary.Add(odInfo.OrderDetailID, odInfo);
                }

                //OrderTaxInfo
                dataReader.NextResult();
                while (dataReader.Read())
                {
                    OrderTaxInfo otInfo = LoadOrderTaxInfo(dataReader);
                    orderInfoDictionary[otInfo.OrderDetailID].OrderTax.Add(otInfo);
                }

                foreach (OrderDetailInfo i in orderInfoDictionary.Values)
                {
                    orderInfo.OrderDetail.Add(i);
                }

                //OrderUserPaymentInfo
                dataReader.NextResult();
                while (dataReader.Read())
                {
                    orderInfo.OrderUserPayment.Add(LoadOrderUserPaymentInfo(dataReader));
                }

            }
            catch (Exception exception)
            {
                /*
                The exception thrown by Hydra has the following
                    The exception.Message contains the descriptive message of the error that was detailed
                    out by Hydra  
                    The exception.InnerException contains the original exception in Hydra which may be
                    a SqlException
                */
                SqlException sqlException = exception.InnerException as SqlException;
                if (sqlException != null)
                {
                    throw new DALException("OrderHistoryDAL.GetOrderInfo() error.", exception, exception.Message, ExceptionUtility.DetermineSQLDBExceptionLevel(sqlException));
                }
                else
                {
                    throw new DALException("OrderHistoryDAL.GetOrderInfo() error.", exception, exception.Message);
                }
            }
            finally
            {
                if (dataReader != null)
                {
                    dataReader.Close();
                }
            }


            return orderInfo;
        }

        internal List<OrderInfo> SearchOrderHistory(int memberID, int callingSystemID, int orderID, int paymentID)
        {
            List<OrderInfo> orderInfos = new List<OrderInfo>();
            SqlDataReader dataReader = null;

            try
            {
                Command command = new Command("epOrder", "dbo.up_SearchOrderHistory", 0);
                if (memberID != Constants.NULL_INT)
                    command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
                if (callingSystemID != Constants.NULL_INT)
                    command.AddParameter("@CallingSystemID", SqlDbType.Int, ParameterDirection.Input, callingSystemID);
                if (orderID != Constants.NULL_INT)
                    command.AddParameter("@OrderID", SqlDbType.Int, ParameterDirection.Input, orderID);
                if (paymentID != Constants.NULL_INT)
                    command.AddParameter("@PaymentID", SqlDbType.Int, ParameterDirection.Input, paymentID);

                dataReader = Client.Instance.ExecuteReader(command);

                while (dataReader.Read())
                {
                    orderInfos.Add(LoadOrderInfo(dataReader));
                }

                //dataReader.NextResult();

            }
            catch (Exception exception)
            {
                /*
                The exception thrown by Hydra has the following
                    The exception.Message contains the descriptive message of the error that was detailed
                    out by Hydra  
                    The exception.InnerException contains the original exception in Hydra which may be
                    a SqlException
                */
                SqlException sqlException = exception.InnerException as SqlException;
                if (sqlException != null)
                {
                    throw new DALException("OrderHistoryDAL.SearchOrderHistory() error.", exception, exception.Message, ExceptionUtility.DetermineSQLDBExceptionLevel(sqlException));
                }
                else
                {
                    throw new DALException("OrderHistoryDAL.SearchOrderHistory() error.", exception, exception.Message);
                }
            }
            finally
            {
                if (dataReader != null)
                {
                    dataReader.Close();
                }
            }

            return orderInfos;
        }

        #endregion

        #region Private Methods
        private CustomerInfo LoadCustomerInfo(SqlDataReader dataReader)
        {
            CustomerInfo info = new CustomerInfo();
            info.CustomerID = dataReader.IsDBNull(dataReader.GetOrdinal("CustomerID")) ? int.MinValue : dataReader.GetInt32(dataReader.GetOrdinal("CustomerID"));
            info.CallingSystemID = dataReader.IsDBNull(dataReader.GetOrdinal("CallingSystemID")) ? int.MinValue : dataReader.GetInt32(dataReader.GetOrdinal("CallingSystemID"));
            return info;
        }


        private void AddDetailToOrderHistory(ref List<OrderInfo> history, OrderDetailInfo orderDetailInfo)
        {
            var orders = from o in history where o.OrderID == orderDetailInfo.OrderId select o;
            foreach (OrderInfo order in orders)
            {
                order.OrderDetail.Add(orderDetailInfo);
            }
        }

        private OrderTaxInfo LoadOrderTaxInfo(SqlDataReader dataReader)
        {
            OrderTaxInfo detailInfo = new OrderTaxInfo();
            detailInfo.Amount = dataReader.IsDBNull(dataReader.GetOrdinal("Amount")) ? decimal.MinValue : dataReader.GetDecimal(dataReader.GetOrdinal("Amount"));
            detailInfo.AuthorityCode = dataReader.IsDBNull(dataReader.GetOrdinal("AuthorityCode")) ? String.Empty : dataReader.GetString(dataReader.GetOrdinal("AuthorityCode"));
            detailInfo.AuthorityID = dataReader.IsDBNull(dataReader.GetOrdinal("AuthorityID")) ? int.MinValue : dataReader.GetInt32(dataReader.GetOrdinal("AuthorityID"));
            detailInfo.CurrencyID = dataReader.IsDBNull(dataReader.GetOrdinal("CurrencyID")) ? int.MinValue : dataReader.GetInt32(dataReader.GetOrdinal("CurrencyID"));
            detailInfo.InsertDate = dataReader.GetDateTime(dataReader.GetOrdinal("InsertDate"));
            detailInfo.ItemTaxTypeID = dataReader.IsDBNull(dataReader.GetOrdinal("ItemTaxTypeID")) ? int.MinValue : dataReader.GetInt32(dataReader.GetOrdinal("ItemTaxTypeID"));
            detailInfo.OrderDetailID = dataReader.IsDBNull(dataReader.GetOrdinal("OrderDetailID")) ? int.MinValue : dataReader.GetInt32(dataReader.GetOrdinal("OrderDetailID"));
            detailInfo.OrderTaxID = dataReader.IsDBNull(dataReader.GetOrdinal("OrderTaxID")) ? int.MinValue : dataReader.GetInt32(dataReader.GetOrdinal("OrderTaxID"));
            detailInfo.RegionID = dataReader.IsDBNull(dataReader.GetOrdinal("RegionID")) ? int.MinValue : dataReader.GetInt32(dataReader.GetOrdinal("RegionID"));
            return detailInfo;
        }

        private OrderDetailInfo LoadOrderDetailinfo(SqlDataReader dataReader)
        {
            OrderDetailInfo detailInfo = new OrderDetailInfo();
            detailInfo.Amount = dataReader.IsDBNull(dataReader.GetOrdinal("Amount")) ? decimal.MinValue : dataReader.GetDecimal(dataReader.GetOrdinal("Amount"));
            detailInfo.CurrencyID = dataReader.IsDBNull(dataReader.GetOrdinal("CurrencyID")) ? int.MinValue : dataReader.GetInt32(dataReader.GetOrdinal("CurrencyID"));
            detailInfo.Duration = dataReader.IsDBNull(dataReader.GetOrdinal("Duration")) ? int.MinValue : dataReader.GetInt32(dataReader.GetOrdinal("Duration"));
            detailInfo.DurationTypeID = dataReader.IsDBNull(dataReader.GetOrdinal("DurationTypeID")) ? int.MinValue : dataReader.GetInt32(dataReader.GetOrdinal("DurationTypeID"));
            detailInfo.InsertDate = dataReader.IsDBNull(dataReader.GetOrdinal("InsertDate")) ? DateTime.MinValue : dataReader.GetDateTime(dataReader.GetOrdinal("InsertDate"));
            detailInfo.ItemID = dataReader.IsDBNull(dataReader.GetOrdinal("ItemID")) ? int.MinValue : dataReader.GetInt32(dataReader.GetOrdinal("ItemID"));
            detailInfo.OrderDetailID = dataReader.IsDBNull(dataReader.GetOrdinal("OrderDetailID")) ? int.MinValue : dataReader.GetInt32(dataReader.GetOrdinal("OrderDetailID"));
            detailInfo.OrderId = dataReader.IsDBNull(dataReader.GetOrdinal("OrderID")) ? int.MinValue : dataReader.GetInt32(dataReader.GetOrdinal("OrderID"));
            detailInfo.OrderTypeID = dataReader.IsDBNull(dataReader.GetOrdinal("OrderTypeID")) ? int.MinValue : dataReader.GetInt32(dataReader.GetOrdinal("OrderTypeID"));
            detailInfo.PackageID = dataReader.IsDBNull(dataReader.GetOrdinal("PackageID")) ? int.MinValue : dataReader.GetInt32(dataReader.GetOrdinal("PackageID"));
            detailInfo.PromoID = dataReader.IsDBNull(dataReader.GetOrdinal("PromoID")) ? int.MinValue : dataReader.GetInt32(dataReader.GetOrdinal("PromoID"));
            detailInfo.UpdateDate = dataReader.IsDBNull(dataReader.GetOrdinal("UpdateDate")) ? DateTime.MinValue : dataReader.GetDateTime(dataReader.GetOrdinal("UpdateDate"));
            detailInfo.ReferenceOrderDetailID = dataReader.IsDBNull(dataReader.GetOrdinal("ReferenceOrderDetailID")) ? int.MinValue : dataReader.GetInt32(dataReader.GetOrdinal("ReferenceOrderDetailID"));
            return detailInfo;
        }

        private OrderInfo LoadOrderInfo(SqlDataReader dataReader)
        {
            OrderInfo info = new OrderInfo();
            info.AdminUserID = dataReader.IsDBNull(dataReader.GetOrdinal("AdminUserID")) ? int.MinValue : dataReader.GetInt32(dataReader.GetOrdinal("AdminUserID"));
            info.AdminUserName = dataReader.IsDBNull(dataReader.GetOrdinal("AdminUserName")) ? String.Empty : dataReader.GetString(dataReader.GetOrdinal("AdminUserName"));
            info.CallingSystemID = dataReader.IsDBNull(dataReader.GetOrdinal("CallingSystemID")) ? int.MinValue : dataReader.GetInt32(dataReader.GetOrdinal("CallingSystemID"));
            info.CallingSystemTypeID = dataReader.IsDBNull(dataReader.GetOrdinal("CallingSystemTypeID")) ? int.MinValue : dataReader.GetInt32(dataReader.GetOrdinal("CallingSystemTypeID"));
            info.CurrencyID = dataReader.IsDBNull(dataReader.GetOrdinal("CurrencyID")) ? int.MinValue : dataReader.GetInt32(dataReader.GetOrdinal("CurrencyID"));
            info.CustomerID = dataReader.IsDBNull(dataReader.GetOrdinal("CustomerID")) ? int.MinValue : dataReader.GetInt32(dataReader.GetOrdinal("CustomerID"));
            info.CustomerIP = dataReader.IsDBNull(dataReader.GetOrdinal("CustomerIP")) ? String.Empty : dataReader.GetString(dataReader.GetOrdinal("CustomerIP"));
            info.InsertDate = dataReader.IsDBNull(dataReader.GetOrdinal("InsertDate")) ? DateTime.MinValue : dataReader.GetDateTime(dataReader.GetOrdinal("InsertDate"));
            info.InternalResponseStatusID = dataReader.IsDBNull(dataReader.GetOrdinal("InternalResponseStatusID")) ? int.MinValue : dataReader.GetInt32(dataReader.GetOrdinal("InternalResponseStatusID"));
            info.OrderID = dataReader.IsDBNull(dataReader.GetOrdinal("OrderID")) ? int.MinValue : dataReader.GetInt32(dataReader.GetOrdinal("OrderID"));
            info.OrderReasonID = dataReader.IsDBNull(dataReader.GetOrdinal("OrderReasonID")) ? int.MinValue : dataReader.GetInt32(dataReader.GetOrdinal("OrderReasonID"));
            info.OrderStatusID = dataReader.IsDBNull(dataReader.GetOrdinal("OrderStatusID")) ? int.MinValue : dataReader.GetInt32(dataReader.GetOrdinal("OrderStatusID"));
            //info.PmotionID = dataReader.IsDBNull(dataReader.GetOrdinal("PmotionID")) ? int.MinValue : dataReader.GetInt32(dataReader.GetOrdinal("PmotionID"));
            info.PromoID = dataReader.IsDBNull(dataReader.GetOrdinal("PromoID")) ? int.MinValue : dataReader.GetInt32(dataReader.GetOrdinal("PromoID"));
            info.ReferenceOrderID = dataReader.IsDBNull(dataReader.GetOrdinal("ReferenceOrderID")) ? int.MinValue : dataReader.GetInt32(dataReader.GetOrdinal("ReferenceOrderID"));
            info.RegionID = dataReader.IsDBNull(dataReader.GetOrdinal("RegionID")) ? int.MinValue : dataReader.GetInt32(dataReader.GetOrdinal("RegionID"));
            info.TotalAmount = dataReader.IsDBNull(dataReader.GetOrdinal("TotalAmount")) ? decimal.MinValue : dataReader.GetDecimal(dataReader.GetOrdinal("TotalAmount"));
            info.UpdateDate = dataReader.IsDBNull(dataReader.GetOrdinal("UpdateDate")) ? DateTime.MinValue : dataReader.GetDateTime(dataReader.GetOrdinal("UpdateDate"));
            info.UserPaymentGuid = dataReader.IsDBNull(dataReader.GetOrdinal("UserPaymentGUID")) ? String.Empty : dataReader.GetString(dataReader.GetOrdinal("UserPaymentGUID"));
            return info;
        }

        private OrderUserPaymentInfo LoadOrderUserPaymentInfo(SqlDataReader dataReader)
        {
            OrderUserPaymentInfo detailInfo = new OrderUserPaymentInfo();
            detailInfo.CallingSystemID = dataReader.IsDBNull(dataReader.GetOrdinal("CallingSystemID")) ? int.MinValue : dataReader.GetInt32(dataReader.GetOrdinal("CallingSystemID"));
            detailInfo.CallingSystemTypeID = dataReader.IsDBNull(dataReader.GetOrdinal("CallingSystemTypeID")) ? int.MinValue : dataReader.GetInt32(dataReader.GetOrdinal("CallingSystemTypeID"));
            detailInfo.ChargeID = dataReader.IsDBNull(dataReader.GetOrdinal("ChargeID")) ? int.MinValue : dataReader.GetInt32(dataReader.GetOrdinal("ChargeID"));
            detailInfo.ChargeStatusID = dataReader.IsDBNull(dataReader.GetOrdinal("ChargeStatusID")) ? int.MinValue : dataReader.GetInt32(dataReader.GetOrdinal("ChargeStatusID"));
            detailInfo.InsertDate = dataReader.IsDBNull(dataReader.GetOrdinal("InsertDate")) ? DateTime.MinValue : dataReader.GetDateTime(dataReader.GetOrdinal("InsertDate"));
            detailInfo.ChargeTypeID = dataReader.IsDBNull(dataReader.GetOrdinal("ChargeTypeID")) ? int.MinValue : dataReader.GetInt32(dataReader.GetOrdinal("ChargeTypeID"));
            detailInfo.OrderID = dataReader.IsDBNull(dataReader.GetOrdinal("OrderID")) ? int.MinValue : dataReader.GetInt32(dataReader.GetOrdinal("OrderID"));
            detailInfo.UpdateDate = dataReader.IsDBNull(dataReader.GetOrdinal("UpdateDate")) ? DateTime.MinValue : dataReader.GetDateTime(dataReader.GetOrdinal("UpdateDate"));
            return detailInfo;
        }
        #endregion
    }
}
