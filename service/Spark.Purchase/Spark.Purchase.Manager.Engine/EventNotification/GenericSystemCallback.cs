﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Spark.Purchase.Manager.Engine.Helpers;
using System.Net;
using System.IO;
using System.Collections.Specialized;
using Spark.UnifiedPurchaseSystem.Lib.Logging;
using System.Threading;

namespace Spark.Purchase.Manager.Engine.EventNotification
{


    internal abstract class GenericCallbackBase
    {
        protected int retryCountAllowed = Convert.ToInt32(System.Configuration.ConfigurationSettings.AppSettings["CallbackRetry"]);
        protected int retryWaitTime = Convert.ToInt32(System.Configuration.ConfigurationSettings.AppSettings["CallbackRetryWaitTime"]); //milliseconds

        internal void SendJsonPost(string url, string data)
        {
            Uri address = new Uri(url);

            // Create the web request  
            HttpWebRequest request = WebRequest.Create(address) as HttpWebRequest;
            //request.PreAuthenticate = true;
            //request.Credentials = new NetworkCredential("devsvc", "!SP@rksvc", "matchnet");

            //request.Credentials = System.Net.CredentialCache.DefaultCredentials;

            // Set type to POST  
            request.Method = "POST";
            request.ContentType = "text/x-json";

            // Create a byte array of the data we want to send  
            byte[] byteData = UTF8Encoding.UTF8.GetBytes(data);

            // Set the content length in the request headers  
            request.ContentLength = byteData.Length;

            // Write data  
            using (Stream postStream = request.GetRequestStream())
            {
                postStream.Write(byteData, 0, byteData.Length);
            }

            // Get response  
            using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
            {
                // Get the response stream  
                StreamReader reader = new StreamReader(response.GetResponseStream());
            }
        }

        internal void SendFormPost(string url, NameValueCollection data)
        {
            Uri address = new Uri(url);

            // Create the web request  
            HttpWebRequest request = WebRequest.Create(address) as HttpWebRequest;

            // Set type to POST  
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";

            // Create a byte array of the data we want to send
            StringBuilder sbData = new StringBuilder();
            foreach (string key in data.AllKeys)
            {
                sbData.Append("&" + key + "=" + System.Web.HttpUtility.UrlEncode(data.Get(key)));
            }
            byte[] byteData = UTF8Encoding.UTF8.GetBytes(sbData.ToString().Substring(1));

            // Set the content length in the request headers  
            request.ContentLength = byteData.Length;

            // Write data  
            using (Stream postStream = request.GetRequestStream())
            {
                postStream.Write(byteData, 0, byteData.Length);
            }

            // Get response  
            using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
            {
                // Get the response stream  
                StreamReader reader = new StreamReader(response.GetResponseStream());
            }
        }

    }

    internal class GenericSystemCallback : GenericCallbackBase, ISystemCallback
    {
        private int callingSystemID;
        private CallbackMethodType methodType = CallbackMethodType.Form;
        private string url;

        public GenericSystemCallback(int CallingSystemID, CallbackMethodType methodType, string url)
        {
            this.callingSystemID = CallingSystemID;
            this.methodType = methodType;
            this.url = url;
        }

        #region ISiteCallback Members

        public void ProcessCallback(CallbackEvent Event)
        {
            if (Event != null && !String.IsNullOrEmpty(url))
            {
                //we will retry a few times in case there was a glitch or calling system was busy
                for (int iRetryCount = 0; iRetryCount <= this.retryCountAllowed; iRetryCount++)
                {
                    try
                    {
                        if (methodType == CallbackMethodType.JSON)
                        {
                            SendJsonPost(url, JSONHelper.Serialize<CallbackEvent>(Event));
                        }
                        else
                        {
                            SendFormPost(url, Event.GetNameValueCollection());
                        }

                        //success, exit retry
                        break;
                    }
                    catch (Exception ex)
                    {
                        if (iRetryCount < this.retryCountAllowed)
                        {
                            TraceLog.WriteLine("ProcessCallback Retry " + (iRetryCount + 1).ToString() + ", CallingSystemID: " + this.callingSystemID.ToString() + ", URL: " + this.url + ". Exception: " + ex.Message);
                            Thread.Sleep(this.retryWaitTime);
                        }
                        else
                        {
                            throw ex;
                        }
                    }
                }
            }
            else
            {
                throw new ApplicationException("Missing Callback URL or CallbackEvent is null for callingSystemID: " + this.callingSystemID);
            }
        }

        #endregion
    }
}
