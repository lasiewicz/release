﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Spark.Purchase.Manager.Engine.EventNotification
{
    internal interface ISystemCallback
    {
        void ProcessCallback(CallbackEvent Event);
    }

    internal class SystemCallbackFactory
    {
        public static ISystemCallback GetSystemCallback(int CallingSystemID)
        {
            ISystemCallback SystemCallBack = null;
            bool hasSettings = false;

            //site settings will be set in config file, we can move these settings to a database later if needed
            string setting = System.Configuration.ConfigurationSettings.AppSettings["CallbackSetting" + CallingSystemID.ToString()];
            if (!String.IsNullOrEmpty(setting))
            {
                string[] settings = setting.Split(new char[] { ';' });
                if (settings.Length == 2)
                {
                    SystemCallBack = new GenericSystemCallback(CallingSystemID, (CallbackMethodType)Enum.Parse(typeof(CallbackMethodType), settings[1]), settings[0]);
                    hasSettings = true;
                }
            }

            if (!hasSettings)
                throw new ApplicationException(String.Format("Purchase Callback has not been configured for Calling System ID: " + CallingSystemID.ToString()));

            return SystemCallBack;
        }

    }

    internal enum CallbackMethodType
    {
        JSON,
        Form
    }
}
