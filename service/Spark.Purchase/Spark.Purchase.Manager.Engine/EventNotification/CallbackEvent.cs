﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.Specialized;

namespace Spark.Purchase.Manager.Engine.EventNotification
{
    /// <summary>
    /// Represents the minimum response data sent back to the calling system
    /// Note: We should strive to keep the data that we sent back the same to all clients, instead of trying to create
    /// custom responses per client, though if the case arises then we should derive from this class for those instances.
    /// </summary>
    internal class CallbackEvent
    {
        public int Version { get { return 1; } }
        public int OrderID { get; set; }
        public string ResponseCode { get; set; }
        public string ResponseMessage { get; set; }
        public int TransactionTypeID { get; set; }
        public int CustomerID { get; set; }
        public int CallingSystemID { get; set; }
        public string CallingSystemCustomData { get; set; }

        public NameValueCollection GetNameValueCollection()
        {
            NameValueCollection nv = new NameValueCollection();
            nv.Add("Version", Version.ToString());
            nv.Add("OrderID", OrderID.ToString());
            nv.Add("ResponseCode", ResponseCode);
            nv.Add("ResponseMessage", ResponseMessage);
            nv.Add("TransactionTypeID", TransactionTypeID.ToString());
            nv.Add("CustomerID", CustomerID.ToString());
            nv.Add("CallingSystemID", CallingSystemID.ToString());
            nv.Add("CallingSystemCustomData", CallingSystemCustomData);

            return nv;
        }
    }

}
