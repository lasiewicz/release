﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Spark.Purchase.Manager.Engine.BusinessLogic;
using Spark.Purchase.ValueObjects;
using Spark.Purchase.Manager.Engine.Helpers;
using Spark.Purchase.Tax.Engine.BusinessLogic;
using Spark.Purchase.Order.Engine.BusinessLogic;
using Spark.Purchase.Manager.Engine.PaymentWrapperService;
using Spark.UnifiedPurchaseSystem.Lib.ServiceCallResponse;
using Spark.UnifiedPurchaseSystem.Lib.Exceptions;
using System.Runtime.Serialization;
using Spark.UnifiedPurchaseSystem.Lib.Common;
using Spark.UnifiedPurchaseSystem.Lib.InternalResponse.DataAccess;
using Spark.UnifiedPurchaseSystem.Lib.InternalResponse;
using Spark.Purchase.ValueObjects.Credit;
using Spark.Purchase.Manager.Engine.Adapter;
using Spark.UnifiedPurchaseSystem.Lib.Logging;
using Spark.Purchase.Manager.Engine.EventNotification;

namespace Spark.Purchase.Manager.Engine.ServiceManagers
{
    public class PurchaseManagerSM
    {
        PaymentWrapperServiceAdapter _PaymentWrapperServiceAdapter = new PaymentWrapperServiceAdapter();

        public PurchaseManagerSM()
        {

        }

        public string CreateCart(int customerID, int systemID, int callingSystemTypeID, int regionID, string orderAttributes)
        {
            PurchaseResponse purchaseResponse = new PurchaseResponse();
            Cart userCart = null;

            try
            {
                PurchaseManagerBL bl = new PurchaseManagerBL();
                userCart = bl.CreateCart(customerID, systemID, (SystemType)Enum.ToObject(typeof(SystemType), callingSystemTypeID), regionID, ref purchaseResponse, orderAttributes);
                purchaseResponse.UserCart = userCart;
                //Cart userCart = bl.CreateCart(customerID, systemID, (SystemType)Enum.ToObject(typeof(SystemType), callingSystemTypeID), (CurrencyType)Enum.Parse(typeof(CurrencyType), currencyType, true), regionID);
                //purchaseResponse = JSONHelper.Serialize<Cart>(userCart);

                SetSuccessfulCall(ref purchaseResponse);
            }
            catch (Exception ex)
            {
                SMException smException = new SMException("PurchaseManagerSM.CreateCart() error.", ex);
                ExceptionUtility.LogException(smException, typeof(PurchaseManagerSM), "Failed on CreateCart()");
                SetFailedCall(ref purchaseResponse);
            }

            return SerializeResponse(purchaseResponse);
        }

        public string AddPackageToCart(string cartToken, int packageID)
        {
            PurchaseResponse purchaseResponse = new PurchaseResponse();

            try
            {
                PurchaseManagerBL bl = new PurchaseManagerBL();
                Package package = bl.GetPackageDetails(packageID);
                if (package != null)
                {
                    Cart userCart = DeserializeCart(cartToken);
                    bl.AddPackageToCart(ref userCart, package);
                    purchaseResponse.UserCart = userCart;
                }

                SetSuccessfulCall(ref purchaseResponse);
            }
            catch (Exception ex)
            {
                SMException smException = new SMException("PurchaseManagerSM.AddPackageToCart() error.", ex);
                ExceptionUtility.LogException(smException, typeof(PurchaseManagerSM), "Failed on AddPackageToCart()");
                SetFailedCall(ref purchaseResponse);
            }

            return SerializeResponse(purchaseResponse);
        }

        public string RemovePackageFromCart(string cartToken, int packageID)
        {
            PurchaseResponse purchaseResponse = new PurchaseResponse();

            try
            {
                PurchaseManagerBL bl = new PurchaseManagerBL();
                Cart userCart = JSONHelper.Deserialize<Cart>(cartToken);

                bl.RemovePackageFromCart(ref userCart, packageID);
                purchaseResponse.UserCart = userCart;

                SetSuccessfulCall(ref purchaseResponse);
            }
            catch (Exception ex)
            {
                SMException smException = new SMException("PurchaseManagerSM.PurchaseOrder() error.", ex);
                ExceptionUtility.LogException(smException, typeof(PurchaseManagerSM), "Failed on PurchaseOrder()");
                SetFailedCall(ref purchaseResponse);
            }

            return SerializeResponse(purchaseResponse);
        }

        public string PriceCart(string cartToken)
        {
            PurchaseResponse purchaseResponse = new PurchaseResponse();

            try
            {
                Cart userCart = JSONHelper.Deserialize<Cart>(cartToken);
                PurchaseTaxBL taxBL = new PurchaseTaxBL();
                userCart = taxBL.PriceCart(userCart, false);
                purchaseResponse.UserCart = userCart;

                SetSuccessfulCall(ref purchaseResponse);
            }
            catch (Exception ex)
            {
                SMException smException = new SMException("PurchaseManagerSM.PriceCart() error.", ex);
                ExceptionUtility.LogException(smException, typeof(PurchaseManagerSM), "Failed on PriceCart()");
                SetFailedCall(ref purchaseResponse);
            }

            return SerializeResponse(purchaseResponse);
        }

        public string PurchaseOrder(string paymentProfileID, string cartToken, string transactionType, string paymentType)
        { 
            PurchaseResponse purchaseResponse = new PurchaseResponse();

            try
            {
                TraceLog.Enter("PurchaseManagerSM.PurchaseOrder");
                TraceLog.WriteLine("Begin PurchaseManagerSM.PurchaseOrder({0}, {1}, {2}, {3})",
                    paymentProfileID,
                    cartToken,
                    transactionType,
                    paymentType);

                // Deserialized the cart from the UI
                PurchaseManagerBL purchaseManagerBL = new PurchaseManagerBL();
                Cart cart = JSONHelper.Deserialize<Cart>(cartToken);
                TraceLog.WriteLine("Deserialized the cart string provided from the client application");

                // Prepare the cart for pricing by setting all relevant tax items
                PurchaseTaxBL purchaseTaxBL = new PurchaseTaxBL();
                cart = purchaseTaxBL.PriceCart(cart, false);
                TraceLog.WriteLine("Cart populated with taxes");
                purchaseResponse.UserCart = cart;

                // Create a new order by saving the cart in the database  
                PurchaseOrderBL purchaseOrderBL = new PurchaseOrderBL();
                if (purchaseOrderBL.UserAllowedToPurchaseFromSite(cart.UserID, cart.SystemID))
                {
                    TraceLog.WriteLine("Customer ID [{0}] does not have a pending order and is allowed to purchase in system ID [{1}]", cart.UserID, cart.SystemID);
                    // No member order lock exists for this site
                    // Allow the member to create a new order for this site
                    ValueObjects.Order newOrder = purchaseOrderBL.CreateOrderFromCart(cart, paymentProfileID, EnumHelper.ConvertTransactionTypeStringToEnum(transactionType));
                    TraceLog.WriteLine("Order object created from cart object");
                    newOrder = purchaseOrderBL.ProcessOrder(newOrder, EnumHelper.ConvertTransactionTypeStringToEnum(transactionType));
                    TraceLog.WriteLine("New order saved to the database");
                    purchaseResponse.OrderID = Convert.ToString(newOrder.OrderID);

                    string paymentResponse = Constants.NULL_STRING;
                    ServiceCallResult serviceCallResult;
                    if (newOrder.TotalAmount > 0)
                    {
                        try
                        {
                            // Bill this order 
                            string[] requiredValidations = purchaseManagerBL.GetRequiredValidations(EnumHelper.ConvertTransactionTypeStringToEnum(transactionType), newOrder.CallingSystemType, EnumHelper.ConvertPaymentTypeStringToEnum(paymentType), newOrder.SystemID);
                            TraceLog.WriteLine("Got the required validations for billing the order: [{0}]", (requiredValidations != null ? "[" + String.Join(",", requiredValidations) + "]" : null));
                            //PaymentWrapperServiceClient paymentWrapperServiceClient = new PaymentWrapperServiceClient();
                            paymentResponse = _PaymentWrapperServiceAdapter.GetProxyInstance().BillPaymentProfile(paymentProfileID,
                                                                                        newOrder.TotalAmount,
                                                                                        Enum.GetName(typeof(CurrencyType), newOrder.CurrencyType),
                                                                                        newOrder.OrderID,
                                                                                        newOrder.SystemID,
                                                                                        (int)newOrder.CallingSystemType,
                                                                                        Constants.NULL_STRING,
                                                                                        Constants.NULL_STRING,
                                                                                        (int)EnumHelper.ConvertTransactionTypeStringToEnum(transactionType),
                                                                                        requiredValidations);
                        }
                        catch (Exception ex)
                        {
                            // A new order exists in the pending state.  
                            // If this new order cannot complete through the payment service, than immediately
                            // set the order as failed so that the user is not locked out from retrying at 
                            // a later time.  
                            // Failed charge on this new order   
                            purchaseOrderBL.UpdateOrderStatus(newOrder.OrderID, OrderStatus.Failed, (int)ServiceStatusType.SystemError);
                            TraceLog.WriteLine("Update the status of Order ID {0} to failed in the database", newOrder.OrderID);
                            throw ex;
                        }

                        serviceCallResult = PaymentResponseHelper.LoadResultObject(paymentResponse);
                        TraceLog.WriteLine("Called payment wrapper service to bill the order, Internal response code: {0}, Internal response message: {1}", serviceCallResult.responseCode, serviceCallResult.responseMessage);

                        if (serviceCallResult.responseCode == "0")
                        {
                            // Successful charge on this new order
                            purchaseOrderBL.UpdateOrderStatus(newOrder.OrderID, OrderStatus.Successful, Convert.ToInt32(serviceCallResult.responseCode));
                            TraceLog.WriteLine("Update the status of Order ID {0} to successful in the database", newOrder.OrderID);
                        }
                        else
                        {
                            // Failed charge on this new order  
                            purchaseOrderBL.UpdateOrderStatus(newOrder.OrderID, OrderStatus.Failed, Convert.ToInt32(serviceCallResult.responseCode));
                            TraceLog.WriteLine("Update the status of Order ID {0} to failed in the database", newOrder.OrderID);
                            purchaseResponse.InternalResponse = new InternalResponse(serviceCallResult.responseCode, serviceCallResult.responseMessage);
                        }
                    }
                    else
                    {
                        //did not call payment cause amount was 0 or less
                        serviceCallResult = LoadNoCall();

                        // Successful process on this new order
                        purchaseOrderBL.UpdateOrderStatus(newOrder.OrderID, OrderStatus.Successful, Convert.ToInt32(serviceCallResult.responseCode));
                        TraceLog.WriteLine("Update the status of Order ID {0} to successful in the database", newOrder.OrderID);
                    }

                    //Perform callback
                    DoPurchaseCallback(newOrder.SystemID,
                        serviceCallResult.responseCode,
                        serviceCallResult.responseMessage,
                        (int)EnumHelper.ConvertTransactionTypeStringToEnum(transactionType),
                        newOrder.UserID,
                        newOrder.OrderID,
                        "");
                }
                else
                {
                    TraceLog.WriteLine("Customer ID [{0}] has a pending order and is not allowed to purchase in system ID [{1}]", cart.UserID, cart.SystemID);
                    // Member order lock exists for this site
                    // Do not allow the member to create another order for this site
                    purchaseResponse.InternalResponse = InternalResponseDAL.GetInternalResponse(ServiceStatusType.PendingTransactionExistsForMemberOnThisSite);
                }

                SetSuccessfulCall(ref purchaseResponse);
            }
            catch (Exception ex)
            {
                SMException smException = new SMException("PurchaseManagerSM.PurchaseOrder() error.", ex);
                TraceLog.WriteErrorLine(smException, "Error in PurchaseManagerSM.PurchaseOrder(), Error: " + ex.Message);
                ExceptionUtility.LogException(smException, typeof(PurchaseManagerSM), "Failed on PurchaseOrder()");
                SetFailedCall(ref purchaseResponse);
            }
            finally
            {
                _PaymentWrapperServiceAdapter.CloseProxyInstance();

                TraceLog.WriteLine("End of PurchaseManagerSM.PurchaseOrder()");
                TraceLog.Leave("PurchaseManagerSM.PurchaseOrder");
            }

            return SerializeResponse(purchaseResponse);
        }

        #region Validation Methods

        public string GetRequiredValidations(string transactionType, int systemType, string paymentType, int systemID)
        {
            PurchaseResponse purchaseResponse = new PurchaseResponse();

            try
            {
                PurchaseManagerBL purchaseManagerBL = new PurchaseManagerBL();
                purchaseResponse.RequiredValidations = purchaseManagerBL.GetRequiredValidations(EnumHelper.ConvertTransactionTypeStringToEnum(transactionType), (SystemType)systemType, EnumHelper.ConvertPaymentTypeStringToEnum(paymentType), systemID);

                SetSuccessfulCall(ref purchaseResponse);
            }
            catch (Exception ex)
            {
                SMException smException = new SMException("PurchaseManagerSM.GetRequiredValidations() error.", ex);
                ExceptionUtility.LogException(smException, typeof(PurchaseManagerSM), "Failed on GetRequiredValidations()");
                SetFailedCall(ref purchaseResponse);
            }

            return SerializeResponse(purchaseResponse);
        }

        #endregion

        public void SetMemberPaymentProfile(int CustomerID, int CallingSystemID, string PaymentProfileID, string PaymentProfileIdType)
        {
            PaymentProfileMapperBL bl = new PaymentProfileMapperBL();
            bl.SetMemberPaymentProfile(CustomerID, CallingSystemID, PaymentProfileID, PaymentProfileIdType);
        }

        public PaymentProfileInfo GetMemberDefaultPaymentProfile(int CustomerID, int CallingSystemID)
        {
            PaymentProfileMapperBL bl = new PaymentProfileMapperBL();
            return bl.GetMemberDefaultPaymentProfile(CustomerID, CallingSystemID);
        }

        public bool IsMemberPaymentProfileValid(int CustomerID, int CallingSystemID)
        {
            PurchaseResponse purchaseResponse = new PurchaseResponse();

            PaymentProfileMapperBL bl = new PaymentProfileMapperBL();
            PaymentProfileInfo paymentProfile = bl.GetMemberDefaultPaymentProfile(CustomerID, CallingSystemID);
            bool isProfileValid = false;
            if (paymentProfile != null)
            {
                string wrapperResponse = string.Empty;

                try
                {
                    //IPaymentWrapperService paymentWrapper = new PaymentWrapperServiceClient();
                    wrapperResponse = _PaymentWrapperServiceAdapter.GetProxyInstance().IsPaymentProfileValid(paymentProfile.PaymentProfileID, CallingSystemID.ToString());
                }
                catch (Exception ex)
                {
                    SMException smException = new SMException("PurchaseManagerSM.IsMemberProfileValid() error.", ex);
                    ExceptionUtility.LogException(smException, typeof(PurchaseManagerSM), "Failed on IsMemberProfileValid()");
                    SetFailedCall(ref purchaseResponse);
                }
                ServiceCallResult serviceCallResult = PaymentResponseHelper.LoadResultObject(wrapperResponse);
                if (serviceCallResult.responseCode == "0")
                {
                    SetSuccessfulCall(ref purchaseResponse);
                }
                else
                {
                    purchaseResponse.InternalResponse = new InternalResponse(serviceCallResult.responseCode, serviceCallResult.responseMessage);
                }
            }
            else
            {
                isProfileValid = false;
            }

            //Temporary hack to return true
            isProfileValid = true;

            _PaymentWrapperServiceAdapter.CloseProxyInstance();

            return isProfileValid;
        }

        public List<OrderInfo> GetMemberOrderHistoryByMemberID(int CustomerID, int CallingSystemID, int PageSize, int PageNumber)
        {
            OrderHistoryBL bl = new OrderHistoryBL();
            return bl.GetMemberOrderHistoryByMemberID(CustomerID, CallingSystemID, PageSize, PageNumber);
        }

        public List<OrderInfo> GetMemberOrderHistoryByOrderID(int OrderID, int PageSize, int PageNumber)
        {
            OrderHistoryBL bl = new OrderHistoryBL();
            return bl.GetMemberOrderHistoryByOrderID(OrderID, PageSize, PageNumber);
        }

        public List<OrderInfo> GetMemberOrderHistoryByPaymentID(int PaymentID, int PageSize, int PageNumber)
        {
            OrderHistoryBL bl = new OrderHistoryBL();
            return bl.GetMemberOrderHistoryByPaymentID(PaymentID, PageSize, PageNumber);
        }

        public CreditOrderResponse CreditOrder(ValueObjects.Order order, TransactionType transactionType, int paymentID, int paymentTypeID)
        {
            CreditOrderResponse creditResponse = new CreditOrderResponse();

            try
            {
                TraceLog.Enter("PurchaseManagerSM.CreditOrder");
                TraceLog.WriteLine("Begin PurchaseManagerSM.CreditOrder({0}, {1}, {2}, {3})",
                    "order object",
                    transactionType.ToString(),
                    paymentID.ToString(),
                    paymentTypeID.ToString());

                PurchaseManagerBL purchaseManagerBL = new PurchaseManagerBL();
                PurchaseOrderBL purchaseOrderBL = new PurchaseOrderBL();
                if (purchaseOrderBL.UserAllowedToPurchaseFromSite(order.UserID, order.SystemID))
                {
                    //Save Order
                    ValueObjects.Order newOrder = purchaseOrderBL.ProcessOrder(order, transactionType);

                    string paymentResponse = Constants.NULL_STRING;
                    try
                    {
                        //Credit or Void Order 
                        //TODO: Do we need validation for crediting? Where to get paymentTypeID
                        string[] requiredValidations = new string[] { };// purchaseManagerBL.GetRequiredValidations(transactionType, newOrder.CallingSystemType, (PaymentType)Enum.ToObject(typeof(PaymentType), paymentTypeID), newOrder.SystemID);
                        paymentResponse = _PaymentWrapperServiceAdapter.GetProxyInstance().CreditTransaction(newOrder.OrderID,
                                                                                    newOrder.TotalAmount,
                                                                                    newOrder.SystemID,
                                                                                    paymentID,
                                                                                    (int)newOrder.CallingSystemType,
                                                                                    (int)transactionType,
                                                                                    requiredValidations);
                    }
                    catch (Exception ex)
                    {
                        // A new order exists in the pending state.  
                        // If this new order cannot complete through the payment service, than immediately
                        // set the order as failed so that the user is not locked out from retrying at 
                        // a later time.  
                        // Failed charge on this new order   
                        purchaseOrderBL.UpdateOrderStatus(newOrder.OrderID, OrderStatus.Failed, (int)ServiceStatusType.SystemError);
                        throw ex;
                    }

                    ServiceCallResult serviceCallResult = PaymentResponseHelper.LoadResultObject(paymentResponse);
                    if (serviceCallResult.responseCode == "0")
                    {
                        // Successful charge on this new order; order lock will also be removed
                        purchaseOrderBL.UpdateOrderStatus(newOrder.OrderID, OrderStatus.Successful, Convert.ToInt32(serviceCallResult.responseCode));

                        // Update Order Type if Void was actually performed instead of a Credit, and vice-versa
                        string paymentTransactionType = serviceCallResult.responseValues["paymentEngineOperation"].ToString();
                        if (transactionType == TransactionType.Credit && paymentTransactionType != "Credit")
                        {
                            purchaseOrderBL.UpdateOrderType(newOrder.OrderID, TransactionType.Void);
                        }
                        else if (transactionType == TransactionType.Void && paymentTransactionType != "Void")
                        {
                            purchaseOrderBL.UpdateOrderType(newOrder.OrderID, TransactionType.Credit);
                        }

                        creditResponse.InternalResponse = new InternalResponse(serviceCallResult.responseCode, serviceCallResult.responseMessage);

                    }
                    else
                    {
                        // Failed charge on this new order; order lock will also be removed
                        purchaseOrderBL.UpdateOrderStatus(newOrder.OrderID, OrderStatus.Failed, Convert.ToInt32(serviceCallResult.responseCode));
                        creditResponse.InternalResponse = new InternalResponse(serviceCallResult.responseCode, serviceCallResult.responseMessage);
                    }

                    // Retrieve Updated OrderInfo for response
                    if (newOrder.OrderID != Constants.NULL_INT)
                    {
                        OrderHistoryBL OrderHistorybl = new OrderHistoryBL();
                        creditResponse.OrderInfo = OrderHistorybl.GetOrderInfo(newOrder.OrderID);
                    }
                }
                else
                {
                    // Member order lock exists for this site
                    // Do not allow the member to create another order for this site
                    creditResponse.InternalResponse = InternalResponseDAL.GetInternalResponse(ServiceStatusType.PendingTransactionExistsForMemberOnThisSite);
                }
            }
            catch (Exception ex)
            {
                SMException smException = new SMException("PurchaseManagerSM.CreditOrder() error.", ex);
                ExceptionUtility.LogException(smException, typeof(PurchaseManagerSM), "Failed on CreditOrder()");
                creditResponse.InternalResponse = InternalResponseDAL.GetInternalResponse(ServiceStatusType.SystemError);
            }
            finally
            {
                _PaymentWrapperServiceAdapter.CloseProxyInstance();
            }

            return creditResponse;
        }

        public CreditOrderResponse CreditOrderFromOrderInfo(ValueObjects.OrderInfo orderInfo, TransactionType transactionType, int paymentID, int paymentTypeID)
        {
            //convert orderInfo to an Order
            PurchaseOrderBL purchaseOrderBL = new PurchaseOrderBL();
            ValueObjects.Order order = purchaseOrderBL.CreateOrderFromOrderInfo(orderInfo);

            //perform credit
            return CreditOrder(order, transactionType, paymentID, paymentTypeID);
        }

        public bool IsCreditSetToVoid(int paymentID, int callingSystemID)
        {
            bool isOrderVoidable = false;
            try
            {
                string paymentResponse = _PaymentWrapperServiceAdapter.GetProxyInstance().IsCreditSetToVoid(paymentID, callingSystemID);
                ServiceCallResult serviceCallResult = PaymentResponseHelper.LoadResultObject(paymentResponse);
                if (serviceCallResult.responseCode == "0")
                {
                    isOrderVoidable = Convert.ToBoolean(serviceCallResult.responseValues["isCreditSetToVoid"].ToString());

                }
            }
            catch (Exception ex)
            {
                SMException smException = new SMException("PurchaseManagerSM.IsCreditSetToVoid() error.", ex);
                ExceptionUtility.LogException(smException, typeof(PurchaseManagerSM), "Failed on PurchaseManagerSM.IsCreditSetToVoid()");
            }
            finally
            {
                _PaymentWrapperServiceAdapter.CloseProxyInstance();
            }
            return isOrderVoidable;
        }

        public OrderInfo GetOrderInfo(int orderID)
        {
            OrderHistoryBL bl = new OrderHistoryBL();
            return bl.GetOrderInfo(orderID);
        }

        public List<OrderInfo> SearchOrderHistory(int memberID, int callingSystemID, int orderID, int paymentID)
        {
            OrderHistoryBL bl = new OrderHistoryBL();
            return bl.SearchOrderHistory(memberID, callingSystemID, orderID, paymentID);
        }

        public DataContractHelper GetDataContractHelper()
        {
            return new DataContractHelper();
        }

        public int GetRegionID(decimal latitude, decimal longitude)
        {
            int regionID = Constants.NULL_INT;
            try
            {
                TraceLog.Enter("PurchaseManagerSM.GetRegionID");
                TraceLog.WriteLine("Begin PurchaseManagerSM.GetRegionID({0}, {1})", latitude, longitude);

                PurchaseManagerBL purchaseManagerBL = new PurchaseManagerBL();
                regionID = purchaseManagerBL.GetRegionID(latitude, longitude);
                TraceLog.WriteLine("Return Region ID [{0}]", regionID);
            }
            catch (Exception ex)
            {
                SMException smException = new SMException("PurchaseManagerSM.GetRegionID() error.", ex);
                ExceptionUtility.LogException(smException, typeof(PurchaseManagerSM), "Failed on PurchaseManagerSM.GetRegionID()");
            }
            finally
            {
                TraceLog.WriteLine("End of PurchaseManagerSM.GetRegionID()");
                TraceLog.Leave("PurchaseManagerSM.GetRegionID");
            }

            return regionID;
        }

        public void RenewPackage(int[] packageIDs, int customerID, string paymentProfileID, int systemID, int callingSystemTypeID, int regionID)
        {
            PurchaseResponse purchaseResponse = new PurchaseResponse();
            TransactionType tranType = TransactionType.Renewal; //TODO: Look into correct transaction type for renewal, once transaction list is completed
            PaymentType payType = PaymentType.None;

            try
            {
                string packageIDString = "";
                if (packageIDs.Length > 0)
                {
                    foreach (int i in packageIDs)
                    {
                        packageIDString += i.ToString() + "|";
                    }
                }

                TraceLog.Enter("PurchaseManagerSM.RenewPackage");
                TraceLog.WriteLine("Begin PurchaseManagerSM.RenewPackage({0}, {1}, {2}, {3}, {4}, {5})",
                    packageIDString,
                    customerID.ToString(),
                    paymentProfileID,
                    systemID.ToString(),
                    callingSystemTypeID.ToString(),
                    regionID.ToString());

                if (packageIDs.Length > 0)
                {
                    //Get payment type
                    PaymentProfileMapperBL bl = new PaymentProfileMapperBL();
                    PaymentProfileInfo paymentProfileInfo = bl.GetMemberDefaultPaymentProfile(customerID, systemID);
                    payType = (PaymentType)Enum.ToObject(typeof(PaymentType), Convert.ToInt32(paymentProfileInfo.PaymentProfileType));

                    // Create cart for renewal
                    Cart cart = null;
                    PurchaseManagerBL purchaseManagerBL = new PurchaseManagerBL();
                    cart = purchaseManagerBL.CreateCart(customerID, systemID, (SystemType)Enum.ToObject(typeof(SystemType), callingSystemTypeID), regionID, ref purchaseResponse, "");
                    TraceLog.WriteLine("Cart created for renewal");

                    // Add packages to cart for renewal
                    foreach (int i in packageIDs)
                    {
                        Package package = purchaseManagerBL.GetPackageDetails(i);
                        if (package != null)
                        {
                            purchaseManagerBL.AddPackageToCart(ref cart, package);
                        }
                        else
                        {
                            // Missing package for renewal
                            throw new Exception("Missing package for renewal, packageID was: " + i.ToString());
                        }
                    }

                    // Prepare the cart for pricing by setting all relevant tax items
                    PurchaseTaxBL purchaseTaxBL = new PurchaseTaxBL();
                    cart = purchaseTaxBL.PriceCart(cart, true);
                    TraceLog.WriteLine("Cart populated with taxes");

                    // Create a new order by saving the cart in the database  
                    PurchaseOrderBL purchaseOrderBL = new PurchaseOrderBL();
                    if (purchaseOrderBL.UserAllowedToPurchaseFromSite(cart.UserID, cart.SystemID))
                    {
                        TraceLog.WriteLine("Customer ID [{0}] does not have a pending order and is allowed to purchase in system ID [{1}]", cart.UserID, cart.SystemID);
                        // No member order lock exists for this site
                        // Allow the member to create a new order for this site
                        ValueObjects.Order newOrder = purchaseOrderBL.CreateOrderFromCart(cart, paymentProfileID, tranType);
                        TraceLog.WriteLine("Order object created from cart object");
                        newOrder = purchaseOrderBL.ProcessOrder(newOrder, tranType);
                        TraceLog.WriteLine("New order saved to the database");
                        purchaseResponse.OrderID = Convert.ToString(newOrder.OrderID);

                        string paymentResponse = Constants.NULL_STRING;
                        ServiceCallResult serviceCallResult;

                        if (newOrder.TotalAmount > 0)
                        {
                            try
                            {
                                // Bill this order 
                                string[] requiredValidations = purchaseManagerBL.GetRequiredValidations(tranType, newOrder.CallingSystemType, payType, newOrder.SystemID);
                                TraceLog.WriteLine("Got the required validations for billing the order: [{0}]", (requiredValidations != null ? "[" + String.Join(",", requiredValidations) + "]" : null));
                                paymentResponse = _PaymentWrapperServiceAdapter.GetProxyInstance().BillPaymentProfile(paymentProfileID,
                                                                                            newOrder.TotalAmount,
                                                                                            Enum.GetName(typeof(CurrencyType), newOrder.CurrencyType),
                                                                                            newOrder.OrderID,
                                                                                            newOrder.SystemID,
                                                                                            (int)newOrder.CallingSystemType,
                                                                                            Constants.NULL_STRING,
                                                                                            Constants.NULL_STRING,
                                                                                            (int)tranType,
                                                                                            requiredValidations);
                            }
                            catch (Exception ex)
                            {
                                // A new order exists in the pending state.  
                                // If this new order cannot complete through the payment service, than immediately
                                // set the order as failed so that the user is not locked out from retrying at 
                                // a later time.  
                                // Failed charge on this new order   
                                purchaseOrderBL.UpdateOrderStatus(newOrder.OrderID, OrderStatus.Failed, (int)ServiceStatusType.SystemError);
                                TraceLog.WriteLine("Update the status of Order ID {0} to failed in the database", newOrder.OrderID);
                                throw ex;
                            }

                            serviceCallResult = PaymentResponseHelper.LoadResultObject(paymentResponse);
                            TraceLog.WriteLine("Called payment wrapper service to bill the order, Internal response code: {0}, Internal response message: {1}", serviceCallResult.responseCode, serviceCallResult.responseMessage);

                            if (serviceCallResult.responseCode == "0")
                            {
                                // Successful charge on this new order
                                purchaseOrderBL.UpdateOrderStatus(newOrder.OrderID, OrderStatus.Successful, Convert.ToInt32(serviceCallResult.responseCode));
                                TraceLog.WriteLine("Update the status of Order ID {0} to successful in the database", newOrder.OrderID);
                            }
                            else
                            {
                                // Failed charge on this new order  
                                purchaseOrderBL.UpdateOrderStatus(newOrder.OrderID, OrderStatus.Failed, Convert.ToInt32(serviceCallResult.responseCode));
                                TraceLog.WriteLine("Update the status of Order ID {0} to failed in the database", newOrder.OrderID);
                                purchaseResponse.InternalResponse = new InternalResponse(serviceCallResult.responseCode, serviceCallResult.responseMessage);
                            }
                        }
                        else
                        {
                            //did not call payment cause amount was 0 or less
                            serviceCallResult = LoadNoCall();

                            // Successful process on this new order
                            purchaseOrderBL.UpdateOrderStatus(newOrder.OrderID, OrderStatus.Successful, (int)ServiceStatusType.NegativeOrderAmount);
                            TraceLog.WriteLine("Update the status of Order ID {0} to successful in the database", newOrder.OrderID);
                        }

                        //Perform callback
                        DoPurchaseCallback(newOrder.SystemID, 
                            serviceCallResult.responseCode, 
                            serviceCallResult.responseMessage,
                            (int)tranType,
                            newOrder.UserID,
                            newOrder.OrderID,
                            "");
                    }
                    else
                    {
                        TraceLog.WriteLine("Customer ID [{0}] has a pending order and is not allowed to purchase in system ID [{1}]", cart.UserID, cart.SystemID);
                        // Member order lock exists for this site
                        // Do not allow the member to create another order for this site
                        purchaseResponse.InternalResponse = InternalResponseDAL.GetInternalResponse(ServiceStatusType.PendingTransactionExistsForMemberOnThisSite);
                    }

                    SetSuccessfulCall(ref purchaseResponse);
                }
            }
            catch (Exception ex)
            {
                SMException smException = new SMException("PurchaseManagerSM.RenewPackage() error.", ex);
                TraceLog.WriteErrorLine(smException, "Error in PurchaseManagerSM.RenewPackage(), Error: " + ex.Message);
                ExceptionUtility.LogException(smException, typeof(PurchaseManagerSM), "Failed on RenewPackage()");
                SetFailedCall(ref purchaseResponse);
            }
            finally
            {
                _PaymentWrapperServiceAdapter.CloseProxyInstance();

                TraceLog.WriteLine("End of PurchaseManagerSM.RenewPackage()");
                TraceLog.Leave("PurchaseManagerSM.RenewPackage");
            }

        }

        #region Helper Methods

        private Cart DeserializeCart(string serializedCart)
        {
            Cart cart = null;
            try
            {
                cart = JSONHelper.Deserialize<Cart>(serializedCart);
            }
            catch (SerializationException serex)
            {
                SMException smException = new SMException("Error deserializeng cart.", serex);
                ExceptionUtility.LogException(smException, typeof(PurchaseManagerSM), "Failed deserializeng cart");
            }
            return cart;
        }

        public string SerializeCart(Cart cart)
        {
            string serializedCart = string.Empty;
            try
            {
                serializedCart = JSONHelper.Serialize<Cart>(cart);
            }
            catch (SerializationException serex)
            {
                SMException smException = new SMException("Error serializing cart.", serex);
                ExceptionUtility.LogException(smException, typeof(PurchaseManagerSM), "Failed serializing cart");
            }
            return serializedCart;
        }

        public PurchaseResponse DeserializeResponse(string serializedResponse)
        {
            PurchaseResponse purchaseResponse = null;
            try
            {
                purchaseResponse = JSONHelper.Deserialize<PurchaseResponse>(serializedResponse);
            }
            catch (SerializationException serex)
            {
                SMException smException = new SMException("Error deserializeng response.", serex);
                ExceptionUtility.LogException(smException, typeof(PurchaseManagerSM), "Failed deserializeng response");
            }
            return purchaseResponse;
        }

        private string SerializeResponse(PurchaseResponse response)
        {
            string serializedResponse = string.Empty;
            try
            {
                serializedResponse = JSONHelper.Serialize<PurchaseResponse>(response);
            }
            catch (SerializationException serex)
            {
                SMException smException = new SMException("Error serializing response.", serex);
                ExceptionUtility.LogException(smException, typeof(PurchaseManagerSM), "Failed serializing response");
            }
            return serializedResponse;
        }

        private void SetSuccessfulCall(ref PurchaseResponse purchaseResponse)
        {
            // To report generically on any system or database errors without providing
            // details of the error  
            if (purchaseResponse.InternalResponse == null)
            {
                //InternalResponse internalResponse = new InternalResponse();
                //internalResponse.InternalResponseCode = "0";
                //internalResponse.InternalResponseDescription = "Success";
                //purchaseResponse.InternalResponse = internalResponse;
                //return;

                // Only set an internal response if there is no internal response set already
                // Internal response may be set in validations or communication to the gateway
                purchaseResponse.InternalResponse = InternalResponseDAL.GetInternalResponse(ServiceStatusType.Success);
            }
        }

        private void SetFailedCall(ref PurchaseResponse purchaseResponse)
        {
            // To report generically on any system or database errors without providing
            // details of the error  
            // The internal response gets set only when an error is thrown and caught at the sevice manager  
            purchaseResponse.InternalResponse = InternalResponseDAL.GetInternalResponse(ServiceStatusType.SystemError);
        }

        private ServiceCallResult LoadNoCall()
        {
            ServiceCallResult result = new ServiceCallResult();

            //does there need to be a custom response code to represent a no-call to payment, but successful order?
            InternalResponse internalResponse = InternalResponseDAL.GetInternalResponse(ServiceStatusType.NegativeOrderAmount);
            result.responseCode = internalResponse.InternalResponseCode;  
            result.responseMessage = internalResponse.InternalResponseDescription;

            return result;
        }

        /// <summary>
        /// Performs a callback to notify client systems that an order has been processe by UPS
        /// </summary>
        /// <param name="callingSystemID"></param>
        private void DoPurchaseCallback(int callingSystemID, string responseCode, string responseMessage, int tranTypeID, int customerID, int orderID, string callingSystemCustomData)
        {
            try
            {
                ISystemCallback systemCallback = SystemCallbackFactory.GetSystemCallback(callingSystemID);
                CallbackEvent callbackEvent = new CallbackEvent();
                callbackEvent.CallingSystemID = callingSystemID;
                callbackEvent.ResponseCode = responseCode;
                callbackEvent.ResponseMessage = responseMessage;
                callbackEvent.TransactionTypeID = tranTypeID;
                callbackEvent.CustomerID = customerID;
                callbackEvent.OrderID = orderID;
                callbackEvent.CallingSystemCustomData = callingSystemCustomData;

                systemCallback.ProcessCallback(callbackEvent);
            }
            catch (Exception ex)
            {
                SMException smException = new SMException("PurchaseSM.DoPurchaseCallback() error.", ex);
                TraceLog.WriteErrorLine(smException, "Error in PurchaseSM.DoPurchaseCallback, error:" + ex.Message);
                ExceptionUtility.LogException(smException, typeof(PurchaseManagerSM), "Error on Purchase Callback.");
            }
        }

        #endregion
    }
}
