﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Spark.Purchase.Manager.Engine.PaymentWrapperService;

namespace Spark.Purchase.Manager.Engine.Adapter
{
    /// <summary>
    /// Adapter for interfacing with Purchase Service Proxy
    /// For now it'll just manage the proxy instance, but we can later expand it to map interfaces as an adapter if needed.
    /// 
    /// Type: instance; which means the Service Manager needs to instantiate this adapter and call its closeProxyInstance()
    /// at the end of the SM process.
    /// </summary>
    public class PaymentWrapperServiceAdapter
    {
        private PaymentWrapperServiceClient _Client = null;

        public PaymentWrapperServiceClient GetProxyInstance()
        {
            try
            {
                if (_Client != null)
                {
                    if (_Client.State == System.ServiceModel.CommunicationState.Closed)
                    {
                        _Client = new PaymentWrapperServiceClient();
                    }
                    else if (_Client.State == System.ServiceModel.CommunicationState.Faulted)
                    {
                        _Client.Abort();
                        _Client = new PaymentWrapperServiceClient();
                    }
                }
                else
                {
                    _Client = new PaymentWrapperServiceClient();
                }

            }
            catch (Exception ex)
            {
                string s = ex.Message;
            }

            return _Client;

        }

        public void CloseProxyInstance()
        {
            if (_Client != null)
            {
                try
                {
                    _Client.Close();
                }
                catch (Exception ex)
                {
                    _Client.Abort();
                }
            }
        }
    }
}
