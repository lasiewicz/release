﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Spark.Purchase.Manager.Engine.DataAccess;
using Spark.Purchase.ValueObjects;

namespace Spark.Purchase.Manager.Engine.BusinessLogic
{
    internal class PaymentProfileMapperBL
    {
        internal void SetMemberPaymentProfile(int CustomerID, int CallingSystemID, string PaymentProfileID, string PaymentProfileIdType)
        {
            PaymentProfileMapperDAL dal = new PaymentProfileMapperDAL();
            dal.SetMemberPaymentProfile(CustomerID, CallingSystemID, PaymentProfileID, PaymentProfileIdType);
        }

        internal PaymentProfileInfo GetMemberDefaultPaymentProfile(int CustomerID, int CallingSystemID)
        {
            PaymentProfileMapperDAL dal = new PaymentProfileMapperDAL();
            return dal.GetMemberDefaultPaymentProfile(CustomerID, CallingSystemID);
        }
    }
}
